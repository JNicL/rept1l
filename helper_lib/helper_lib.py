#==============================================================================#
#                                  helper lib                                  #
#==============================================================================#
from __future__ import print_function

#============#
#  Includes  #
#============#

import sys
import os
import re
from six import with_metaclass
from builtins import input
from cloudpickle import load

try:
  from types import ListType
  list_type = ListType
except ImportError:
  list_type = list

from sympy import numbered_symbols, symbols
from sympy import Symbol, I, sympify, nsimplify

#===========#
#  Methods  #
#===========#

#from http://stackoverflow.com/questions/2158395/
#     flatten-an-irregular-list-of-lists-in-python

flatten = lambda *n: (e for a in n
                      for e in (flatten(*a) if isinstance(a, (tuple, list))
                                else (a,)))

def flatten_with_level(args, max_level=1):
  for a in args:
    for e in (flatten_with_level((v for v in a), max_level=max_level-1)
              if isinstance(a, (tuple, list)) and max_level > 0 else (a,)):
      yield e


def file_exists(filepath, with_content=True):
  try:
    if os.stat(filepath).st_size > 0:
      return True
    else:
      # file exists but is empty
      return not with_content
  except OSError:
    # file doesnt exist
    return False

def hashfile(afile, hasher, blocksize=65536):
  """ Memory efficient hashing of large files.
  http://stackoverflow.com/questions/3431825/generating-a-md5-checksum-of-a-file

  Files are read in as chunks instead of the whole file.

  :param afile: The file to-be hashed opened with `open`
  :type  afile: file

  :param hasher: hash function
  :type  hasher: func

  """
  buf = afile.read(blocksize)
  while len(buf) > 0:
    hasher.update(buf)
    buf = afile.read(blocksize)
  return hasher.digest()


def query_yes_no(question, default="yes"):
  """ Ask a yes/no question via input() and return their answer.

  :question: string that is presented to the user.

  :default: is the presumed answer if the user just hits <Enter>.
            It must be "yes" (the default), "no" or None (meaning an answer is
            required of the user).

  The `answer` return value is True for "yes" or False for "no".
  http://stackoverflow.com/questions/3041986/python-command-line-yes-no-input
  """
  valid = {"yes": True, "y": True, "ye": True,
           "no": False, "n": False}
  if default is None:
    prompt = " [y/n] "
  elif default == "yes":
    prompt = " [Y/n] "
  elif default == "no":
    prompt = " [y/N] "
  else:
    raise ValueError("invalid default answer: '%s'" % default)

  while True:
    sys.stdout.write(question + prompt)
    choice = input().lower()
    if default is not None and choice == '':
      return valid[default]
    elif choice in valid:
      return valid[choice]
    else:
      sys.stdout.write("Please respond with 'yes' or 'no' "
                       "(or 'y' or 'n').\n")


def get_particle_mixings(particles):
  from rept1l.counterterms import Counterterms
  particle_mixbacks = {}
  for particle in particles:
    if hasattr(particle, 'counterterm'):
      mixback = ((particle in mparticle.counterterm, mparticle)
                 for mparticle in particle.counterterm.keys()
                 if hasattr(mparticle, 'counterterm') and mparticle != particle)

      mixback = [u[1] for u in mixback if u[1]]
      if len(mixback) > 0:
        particle_mixbacks[particle.name] = mixback
  return particle_mixbacks


def get_mixback_wavefunctions(vertex):
  """ Returns the wavefunctions which mix back to `vertex`. It is made sure that
  corresponding vertices exist such that this is possible.

  :param vertex: a FeynRules Vertex
  :type  vertex: Vertex
  """
  particles = vertex.particles
  pmixings = get_particle_mixings(particles)
  mixbacks = set()
  for ppos, particle in enumerate(vertex.particles):
    if particle.name in pmixings:
      for mparticle in pmixings[particle.name]:
        new_particles = particles[0:ppos] + [mparticle] + particles[ppos+1:]
        try:
          # make sure the Vertex or a TreeInduced Vertex exists.
          # TODO: (nick 2016-04-05) In the presence of real CTVertices this
          # might cause problems
          find_vertex([u.name for u in new_particles], CT=True)

          # extract wavefunction symbols
          wfs = (SympyParsing.parse_to_sympy(u)
                 for u in mparticle.counterterm[particle].keys())
          wfs = set(flatten([[u for u in wf.atoms() if type(u) is Symbol]
                             for wf in wfs]))
          # add them to the result
          mixbacks = mixbacks | wfs
        except UnknownVertex:
          pass
  return mixbacks


def get_vertex_mixing(particles, pmixings):
  vertices = []
  for ppos, particle in enumerate(particles):
    if particle.name in pmixings:
      for mparticle in pmixings[particle.name]:
        new_particles = particles[0:ppos] + [mparticle] + particles[ppos+1:]
        vertices.append(new_particles)

  found = []
  not_found = []
  for particles in vertices:
    vert = [u.name for u in particles]
    try:
      find_vertex(vert, CT=True)
      found.append(vert)
    except:
      not_found.append(vert)

  found = set(tuple(sorted(u)) for u in found)
  return found


class UnknownVertex(Exception):
  """ UnknownVertex exception is raised if the requested Vertex is not found by
  means of `find_vertex` """
  def __init__(self, value):
    self.value = value

  def __str__(self):
    return repr(self.value)

def find_vertex(p_key_str, CT=False):
  """ Lookup for a vertex specified by a list of particles (strings) in
  FeynRules convention.

  :param p_key_str: List of particle strings
  :type  p_key_str: list

  :return: FeynRules Vertex instance
  """
  p_key_str_sorted = sorted(p_key_str)
  import rept1l.Model as Model
  model = Model.model
  model_objects = model.object_library
  found = [u for u in model_objects.all_vertices
           if sorted(v.name for v in u.particles) == p_key_str_sorted]
  if CT:
    found += [u for u in model_objects.all_CTvertices
              if sorted(v.name for v in u.particles) == p_key_str_sorted]
  if len(found) > 0:
    return found
  else:
    raise UnknownVertex('Vertex ' + ' '.join(p_key_str) + ' not found')


def get_anti_particle(particle, all_particles):
  """ Returns the antiparticle given `particle` and the list of `all_particles`.
  """
  if (particle.selfconjugate):
    return particle
  else:
    for particle_tmp in all_particles:
      if (particle_tmp.name == particle.antiname):
        return particle_tmp


def is_anti_particle(particle):
  """ Anti-particles are defined as `+`, `~` or `bar`.  """
  pattern1 = r'\+'
  pattern2 = r'\~'
  pattern3 = r'bar'
  patterns = [pattern1, pattern2, pattern3]
  if any(re.search(p, particle) for p in patterns):
    return True
  else:
    return False


def export_particle_name(particle, cut_suffix=False):
  """ Particle name is changed in order to be able to define a fortran variable
  named by the particle name.
  >>> export_particle_name("H+")
  'HPlus'
  >>> export_particle_name("H++")
  'HPlusPlus'
  """
  rules = dict()
  if cut_suffix:
    rules['+'] = ''
    rules['-'] = ''
    rules['~'] = ''
  else:
    rules['+'] = 'Plus'
    rules['-'] = 'Minus'
    rules['~'] = 'Anti'
  particle = string_subs(particle, rules)

  return particle


def get_particle(particle_name, all_particles):
  for particle in all_particles:
    if (particle.name == particle_name):
      return particle

  return None

def export_fortran(expr):
  """ Manipulate expr for making an evalution in Fortran possible.

  >>> expr = 'cmath.sqrt(2)'
  >>> export_fortran(expr)
  'sqrt(2d0)'
  >>> expr = 'I*cmath.sqrt(2*alphas*I)*I1a33*I'
  >>> export_fortran(expr)
  'cima*sqrt(2d0*alphas*cima)*I1a33*cima'
  >>> expr = 'alphas**(2/3)*5'
  >>> export_fortran(expr)
  'alphas**(2d0/3d0)*5d0'
  >>> expr = '2/3'
  >>> export_fortran(expr)
  '2d0/3d0'
  >>> expr = '2/3d0'
  >>> export_fortran(expr)
  '2d0/3d0'
  >>> expr = '2*dMZ2/2'
  >>> export_fortran(expr)
  '2d0*dMZ2/2d0'
  >>> expr = '-40*Nc*T0'
  >>> export_fortran(expr)
  '-40d0*Nc*T0'
  >>> export_fortran('1.16637e-05*200')
  '1.16637d-05*200d0'
  """
  # remove cmatch prefix
  expr = string_subs(expr, {'cmath.': ''})

  # replace sqrt(digit) with sqrt(digit.)
  #complex_pattern = re.compile(r'(sqrt\((\d+)\))')
  #matches = complex_pattern.findall(expr)
  #for match in matches:
    #cc = 'sqrt(' + match[1] + 'd0)'
    #expr = re.sub(re.escape(match[0]), cc, expr)

  operator = '|'.join([re.escape('*'), re.escape('/')])
  operator = '|\*|\+|\-|\/|\('
  digit = '((^|\s' + operator + ')(\d+\.?\d*e?)(?!d0))'
  digit_pattern = re.compile(digit)
  matches = digit_pattern.findall(expr)
  global next_is_exponent
  next_is_exponent = False

  def digit_case(m):
    match = m.groups()
    match_str = match[2]
    global next_is_exponent
    if match_str[-1] == '.':
      match_str = match_str[:-1] + 'd0'
      next_is_exponent = False
    elif match_str[-1] == 'e':
      match_str = match_str[:-1] + 'd'
      next_is_exponent = True
    elif next_is_exponent:
      next_is_exponent = False
    else:
      match_str = match_str + 'd0'
      next_is_exponent = False
    match_str = match[1] + match_str
    return match_str

  expr = re.sub(digit_pattern, digit_case, expr)

  iunit_pattern = r'([\W]|^)I([\W]|$)'
  expr = re.sub(iunit_pattern, r'\1cima\2', expr)

  return expr


class StorageMeta(type):
  """
  Providing canoncial methods for storing class attributes to the hard disk
  """

  def __init__(cls, name, bases, dct):
    super(StorageMeta, cls).__init__(name, bases, dct)
    try:
      assert(hasattr(cls, 'storage'))
    except:
      raise Exception('Class has no `storage` attribute')

    cls.init_storage_dict()
    cls.loaded = False

  def __iter__(cls):
    if hasattr(cls, '_registry'):
      return iter(cls._registry)
    else:
      raise Exception('Class has not registry')

  def init_storage_dict(cls):
    """ Initializes all storage attributes to an empty dict. """
    for store in cls.storage:
      setattr(cls, store, {})

  def store(cls, timestamp=None, force=False):
    """ Stores all the entered informations on the hard disk.  """

    if not cls.loaded:
      if hasattr(cls, 'name'):
        name = cls.name
      else:
        name = 'Unknown Storage'
      if not force:
        answer = query_yes_no('The system tries to store `' + name +
                              '` without having loaded. ' +
                              'Data might get lost. Do you want to continue?')
        if not answer:
          sys.exit()
    try:
      path = cls.path
    except:
      raise Exception('Class has no `path` attribute')

    if not os.path.exists(path):
        os.makedirs(path)
    data = {store: getattr(cls, store) if hasattr(cls, store) else {}
            for store in cls.storage}
    from cloudpickle import dump
    if timestamp:
      data['timestamp'] = timestamp
    if hasattr(cls, 'name'):
      filepath = path + '/' + getattr(cls, 'name')
    else:
      filepath = path + '/' + cls.__class__.__name__ + '_data.txt'
    #print "storing path:", filepath
    with open(filepath, 'wb') as f:
      dump(data, f)

  def load(cls, load_all=True):
    """ Loads stored information into the class.
    :load_all: True if to load the complete storage. Can be set to a list of
               storage strings which should be loaded.  """

    # do not load if already loaded
    if cls.loaded:
      return

    try:
      path = cls.path
    except:
      raise Exception('Class has no `path` attribute')

    if hasattr(cls, 'name'):
      filepath = path + '/' + getattr(cls, 'name')
    else:
      filepath = path + '/' + cls.__class__.__name__ + '_data.txt'

    try:
      with open(filepath, 'rb') as f:
        data = load(f)
    except EOFError as e:
      print('Could not load storage. No modelfiles or corrupted: ' + str(e))
      # you cannot restore the data anyways -> hard reset
      cls.loaded = True
      cls.store()
      return
    except IOError:
      # File does not exist and should be created.
      if hasattr(cls, 'name'):
        print('No modelfiles for ' + getattr(cls, 'name') + '.' +
              ' Setting up modelfiles.')
      else:
        print('Could not load storage. No modelfiles.')
      data = {'_' + u.lstrip('_'): {} for u in cls.storage}
      cls.loaded = True
      #cls.store()
      #return
    except Exception as e:
      print('Could not load storage. No modelfiles or corrupted: ' + str(e))
      print('Resetting modelfile.')
      cls.loaded = True
      cls.store()
      return
      #print "cls.__name__:", cls.__name__
      #data = {'_' + u.lstrip('_'): {} for u in cls.storage}

    for data_key in data:
      if load_all is True:
        value = data[data_key]
        if value != {}:
          if data_key.startswith('_'):
            setattr(cls, '_' + data_key.lstrip('_'), value)
          else:
            setattr(cls, data_key, value)
      elif data_key in load_all:
        value = data[data_key]
        if value != {}:
          if data_key.startswith('_'):
            setattr(cls, '_' + data_key.lstrip('_'), value)
          else:
            setattr(cls, data_key, value)

    cls.loaded = True

  def clear(cls, *selection, **kwargs):
    if 'force' in kwargs:
      force = kwargs['force']
    else:
      force = False
    if len(selection) == 0:
      for data_key in cls.storage:
        setattr(cls, data_key, {})
    else:
      cls.load()
      for data_key in selection:
        if data_key.startswith('_'):
          setattr(cls, '_' + data_key.lstrip('_'), {})
        else:
          setattr(cls, '_' + data_key, {})
    cls.store(force=force)


class Storage(with_metaclass(StorageMeta)):
  storage = []

  def __init__(self, *args, **kwargs):
    super(Storage, self).__init__(*args, **kwargs)


class StorageProperty(StorageMeta):
  """ The metaclass makes sure that the registry is loaded """

  def __init__(cls, *args, **kwargs):
    # storage attributes are stored under new attributes starting with an
    # (additional) underscore.
    cls.storage = [u if u.startswith('_') else '_' + u for u in cls.storage]
    super(StorageProperty, cls).__init__(*args, **kwargs)
    for store in cls.storage:
      setattr(cls, store, {})

      # for each storage attribute we define a storage access function. The
      # original storage attribute (without (additional) underscore) is then
      # defined as a property of that method

      # wrapped function due to lexical closures (otherwise `store` gets
      # overwritten and is shared among all functions)
      def construct_store_func(store):
        def store_func(cls):
          """ Any access to a storage attribute invokes this function. """
          try:
            cls.load()
          except IOError:
            pass
            #cls.store()
          return getattr(cls, store)
        return store_func

      meta = type(cls)
      assert(store.startswith('_'))
      setattr(meta, store.lstrip('_'), property(construct_store_func(store)))


class StorageDict(StorageProperty):
  """ Provides the magic for returning registry instance by invoking __getitem__
  method (`[]`) which is defined on class itself. This class assumes that the
  `registry` attribute has been set in storage. """
  def __init__(cls, *args, **kwargs):
    if 'registry' not in cls.storage:
      print('`registry` should be part of the storage for class ' + cls.__name__)
    else:
      cls.registry = {}

  def __getitem__(cls, name):
    """
    """
    entries = cls.registry
    if name in entries:
      return entries[name]
    else:
      raise Exception('No entry with name ' + str(name) + ' in registry.')

  def __setitem__(cls, name, value):
    """
    """
    entries = cls.registry
    entries[name] = value


class SympyParsing(object):

  function_pattern = re.compile(r'(\b[^()]+\((.*)\)$)')

  def function(i=-1):
    while True:
      i += 1
      yield "F" + str(i)

  f_sym_gen = function()

  @classmethod
  def parse_to_sympy(self, expr):
    """
    >>> SympyParsing.parse_to_sympy('a*b')
    a*b
    >>> SympyParsing.parse_to_sympy('f(a)')
    f(a)
    """
    return parse_UFO(expr)

def replace_sqrt(expr):
  pattern = re.compile(r'(cmath.sqrt\((\d+\.*\d*)\))')
  for match in set(re.findall(pattern, expr)):
    repl, arg = match
    newarg = str(nsimplify(arg, rational=True))
    expr = re.sub(re.escape(repl), 'cmath.sqrt(' + newarg + ')', expr)
  return expr

def replace_complex(expr):
  pattern = re.compile(r'(complex\((\d+),(\s*\d+)\))')
  for match in set(re.findall(pattern, expr)):
    repl, arg1, arg2 = match
    newarg1 = nsimplify(arg1, rational=True)
    newarg2 = nsimplify(arg2, rational=True)
    cc = newarg1 + I * newarg2
    expr = re.sub(re.escape(match[0]), '(' + str(cc) + ')', expr)
  return expr

def parse_UFO(expr):
  """ Parses a UFO expression string or a list of strings to sympy instances.
  The sympy internal parse_expr is used.

  :param expr: Expression to be parsed
  :type  expr: String or list of strings

  :return: Sympy expression of expr

  >>> parse_UFO('a*b')
  a*b
  >>> parse_UFO('f(a)')
  f(a)
  >>> parse_UFO('2/3')
  2/3
  >>> parse_UFO('-((ca*ee**2*complex(0,1)*cmath.sqrt(0.6666666666666666))/cw)')
  -sqrt(6)*I*ca*ee**2/(3*cw)
  """
  if isinstance(expr, str):
    expr = replace_sqrt(expr)
    expr = replace_complex(expr)
    expr = string_subs(expr, {'i_': 'I', '^': '**', 'cmath.': '',
                              'complexconjugate': 'conjugate'})

    return nsimplify(expr, rational=True)

  elif isinstance(expr, int) or isinstance(expr, float):
    return nsimplify(expr, rational=True)
  elif isinstance(expr, list_type):
    return [parse_UFO(u) for u in expr]
  else:
    print("expr:", expr)
    raise TypeError('parse_to_sympy requires string or list of strings. ' +
                    'Given:' + str(type(expr)))


def string_subs(string, dict_repl):
  if len(dict_repl) > 0:
    p = (re.compile('|'.join(re.escape(key)
         for key in dict_repl.keys()), re.S))
    return p.sub(lambda x: dict_repl[x.group()], string)
  else:
    return string


def is_plus_minus(char):
  """
  char: char

  returns: True if -/+, else False
  """
  if char == '-':
    return True
  if char == '+':
    return True
  return False


def isdigit(string):
  """
  string: string

  returns: True if string is digit, else False
  """
  return string.replace("-", "", 1).replace(".", "").isdigit()


def spin_to_lorentz(spins, return_as_list=False):
  """
  spins: [s1, s2, s3, ...], where s_i =  { 1 = scalar,
                                           2 = fermion,
                                           3 = vector,
                                          -1 = ghost   }
  """
  if(not return_as_list):
    tmp = ''
    for i in spins:
      if(i == 1):
        tmp += 'S'
      elif(i == 2):
        tmp += 'F'
      elif(i == 3):
        tmp += 'V'
      elif(i == -1):
        tmp += 'U'
    return tmp
  else:
    tmp = []
    for i in spins:
      if(i == 1):
        tmp.append('S')
      elif(i == 2):
        tmp.append('F')
      elif(i == 3):
        tmp.append('V')
      elif(i == -1):
        tmp.append('U')
    return tmp


def get_wavefunction_dim_from_spin(spin):
  if (spin == -1):
    return 1
  elif (spin == 1):
    return 1
  elif (spin == 2):
    return 4
  elif (spin == 3):
    return 4


def is_vector(expr, non_vector_quantities=[]):
  args = expr.args
  if (len(args) > 0):
    for term in expr.args:
      if term not in non_vector_quantities:
        if is_vector(term):
          return True

    return False
  else:
    # either the expression is alrdy declared as vector
    # or the expression does not carry indices
    if expr in non_vector_quantities:
      return False

    # just to make sure that the object can be handled by sympy. should never
    # cause any problems, since the number are multiplied with sympy symbols,
    # thus they inherit the sympy types.
    expr = sympify(expr)
    if expr.is_Number:
      return False

    parenthesis1 = re.compile("\(i\)")
    parenthesis2 = re.compile("\(i\,")
    parenthesis3 = re.compile("\,i\)")
    parenthesis4 = re.compile("\,i\,")

    match = True
    vector_parser = [parenthesis1, parenthesis2, parenthesis3, parenthesis4]
    for parser in vector_parser:
      match = match and (parser.search(str(expr)) is None)

    if match:
      return False
    else:
      return True


def subs_arguments(expression, repl_dict):
  """
  Perform substitution of arguments in expression defined by repl_dict.
  Arguments are defined by being enclosed by prenthesis.

  :param expression: An expression as String
  :type  expression: str

  :param repl_dict: Replacement dict. See re.sub.
  :type  repl_dict: dict, keys and values are str

  >>> expr = 'wl(i, rank)*wi'
  >>> repl_dict = {'i': ':'}
  >>> subs_arguments(expr, repl_dict)
  'wl(:, rank)*wi'
  """
  keys = repl_dict.keys()
  new_dict = dict()
  for key in keys:
    key_to_arg = ['\,\s*' + key + '\s*\)', '\(\s*' + key + '\s*\)',
                  '\(\s*' + key + '\s*\,', '\,\s*' + key + '\s*\,']
    for arg in key_to_arg:
      pattern = re.compile(arg)
      match = pattern.search(expression)
      if match is not None:
        expr = match.group(0)
        new_res = re.sub(key, repl_dict[key], expr)
        expression = re.sub(arg, new_res, expression)
  return expression


def check_vector_valued_expressions(precomp):
  """
  Takes the subexpression from sympys cse function and checks whether
  subexpressions are vector-valued. If so, the expression, say x, is replaced by
  xvec. Quantities depending on that x are automatically vectorized in the same
  way.

  :param precomp: List of subexpression as returned from the Sympys cse method
  :type  precomp: List of tuples of Sympy expressions.

  :return: replacement dict (repl_dict), number of non vector quantities,
           number vector quantities

  >>> x0, x1, cima, w2, wl = symbols('x0 x1 cima w2 wl')

  w2 should be a scalar
  >>> w2.name = 'w2(0)'

  wl is a index valued vector
  >>> wl.name = 'wl(i,rank)'
  >>> precomp = [(x0, cima*w2), (x1, cima*wl)]

  repl_dict, len(non_vector_quantities_repl), len(vector_quantities_repl)

  >>> repl, nvec, vec = check_vector_valued_expressions(precomp)
  >>> repl[x0]
  x0
  >>> repl[x1]
  x0vec
  >>> nvec, vec
  (1, 1)

  Bugfix 8.12.2016 - recursive check for vector-valued
  >>> x1, x5 = symbols('x1 x5')
  >>> wl1 = Symbol('wl(1,rank)')
  >>> co = Symbol('co(1)')
  >>> w2i = Symbol('w2(i)')
  >>> pc = [(x1, co*w2i), (x5, wl1*x1)]
  >>> repl, nvec, vec = check_vector_valued_expressions(pc)
  >>> nvec
  0
  >>> vec
  2
  >>> repl[x5]
  x1vec
  >>> repl[x1]
  x0vec
  """
  # For all k: precomp[k][0] is subdivided in vector_valued quantities and
  # non-vector-valued ones
  vector_quantities = []
  non_vector_quantities = []
  for pre in precomp:
    novecdep = all(u not in vector_quantities for u in pre[1].free_symbols)
    if not is_vector(pre[1], non_vector_quantities) and novecdep:
      non_vector_quantities.append(pre[0])
    else:
      vector_quantities.append(pre[0])

  # numbered_symbols generator for symbols: x1, x2, x3, ...
  syms = numbered_symbols()
  non_vector_quantities_repl = []
  length_non_vec = len(non_vector_quantities)
  # We create new symbols for the both. In both cases we want to start with x0,
  # x0vec respectively.
  for sym in syms:
    if len(non_vector_quantities_repl) >= length_non_vec:
      break
    else:
      non_vector_quantities_repl.append(sym)

  syms = numbered_symbols()
  vector_quantities_repl = []
  length_vec = len(vector_quantities)
  for sym in syms:
    if len(vector_quantities_repl) >= length_vec:
      break
    else:
      vector_quantities_repl.append(sym)

  for k in range(len(vector_quantities_repl)):
    vector_quantities_repl[k] = symbols(str(vector_quantities_repl[k]) + 'vec')

  # The following dicts introduce replacement rules. For instance, assume only
  # x0 is vector valued. Then x0 -> x0vec, but x1 -> x0 and x2 -> x1 and so on.
  repl_dict = dict()
  for k in range(len(vector_quantities_repl)):
    repl_dict[vector_quantities[k]] = vector_quantities_repl[k]

  for k in range(len(non_vector_quantities_repl)):
    repl_dict[non_vector_quantities[k]] = non_vector_quantities_repl[k]

  return repl_dict, len(non_vector_quantities_repl), len(vector_quantities_repl)


def substitute_integer_string(string, nsubs):
  """ Substitutes appearing positiv integers in a string. The integers should be
  surrounded by parenthesis and separated by comma in the following form

    'any string here (i1, i2, ,..,in) any string there (k1, k2, ... )'

  The method ignores negative integers (whitespace between - and integer leads
  to mistakes)

  >>> substitute_integer_string('f(1 , 2,3)',{1:3,3:2})
  'f(3 , 2,2)'
  >>> substitute_integer_string('gamma(-1,1,2)*gamma(-1,3,4)',{4:3,1:2,2:1,3:4})
  'gamma(-1,2,1)*gamma(-1,4,3)'
  """
  if len(nsubs) == 0:
    return string
  # Define the substitution of integers due to permutation

  nsubs = {str(k): str(nsubs[k]) for k in nsubs}

  pospat = r'[\(,]?\s*\-?\d+'
  pospat = re.compile(pospat)

  negpat = r'\-\d+'
  negpat = re.compile(negpat)

  matches = pospat.findall(string)

  # Define the substitution of positive inters as they appear in the expression
  psubs = {}
  for match in matches:
    if not negpat.search(match):
      psubs[match] = string_subs(match, nsubs)

  return string_subs(string, psubs)


if __name__ == "__main__":
  import doctest
  doctest.testmod()

  #from sympy import symbols, Symbol
  #x0, x1, x2, x3, x4, x5 = symbols('x0 x1 x2 x3 x4 x5')
  #p11 = Symbol('P1(1)')
  #p13 = Symbol('P1(3)')
  #wl1 = Symbol('wl(1,rank)')
  #wl3 = Symbol('wl(3,rank)')
  #co = Symbol('co(1)')
  #w2i = Symbol('w2(i)')
  ##print export_fortran('1.16637e-05')
  #pc = [(x1, p11*co*w2i), (x5, wl1*x1)]
  #print check_vector_valued_expressions(pc)
