###############################################################################
#                              argparse_setup.py                              #
###############################################################################

#############
#  Imports  #
#############

import sys
from argparse import ArgumentParser, ArgumentError
from argparse import RawDescriptionHelpFormatter
from argparse import SUPPRESS, OPTIONAL, ZERO_OR_MORE

#######################
#  Globals & Methods  #
#######################

usage = './\033[1;30m%(prog)s [\033[1;moptions\033[1;30m]\033[1;m'

class BlankLinesHelp(RawDescriptionHelpFormatter):
  def _split_lines(self, text, width):
      return super(BlankLinesHelp, self)._split_lines(text, width) + ['']

  def _get_help_string(self, action):
        help = action.help
        if '%(default)' not in action.help:
            if action.default is not SUPPRESS:
                defaulting_nargs = [OPTIONAL, ZERO_OR_MORE]
                if action.option_strings or action.nargs in defaulting_nargs:
                    help += ' (default: %(default)s)'
        return help


class Rept1lArgParser(ArgumentParser):

  """ Default argument parser for REPT1L scripts. """

  def __init__(self, description):
    """

    :param description: general user instructions
    :type  description: str

    """
    super(Rept1lArgParser, self).__init__(usage=usage,
                                          description=description,
                                          formatter_class=BlankLinesHelp)

    self._usage = usage
    self._description = description

  def error(self, message):
    sys.stderr.write('error: %s\n' % message)
    self.print_help()
    sys.exit(2)

