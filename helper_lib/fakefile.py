#==============================================================================#
#                                 fakefile.py                                  #
#==============================================================================#

#============#
#  Includes  #
#============#

import os
import tempfile
from contextlib import contextmanager

#==============================================================================#
#                                   fakefile                                   #
#==============================================================================#

@contextmanager
def fakefile(input):
  """ Fakefile generator which can be used within doctests. """
  assert(type(input) is str)
  f = tempfile.NamedTemporaryFile(delete=False)
  f.write(input.encode())
  f.close()
  yield f.name
  os.unlink(f.name)
