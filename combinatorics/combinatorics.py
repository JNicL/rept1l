#==============================================================================#
#                                combinatorics                                 #
#==============================================================================#

#============#
#  Includes  #
#============#


import re
import os
from builtins import dict
from six import with_metaclass
from past.builtins import xrange, range
from future.utils import implements_iterator
from builtins import filter
from itertools import product
from sympy import (Matrix, symbols, Symbol, I, Rational, zeros,
                   simplify, cse, pprint, Poly, sympify)
from math import factorial

from rept1l.pyfort import *
from rept1l.helper_lib import StorageProperty
try:
  from types import ListType
  list_type = ListType
except ImportError:
  list_type = list

#===========#
#  Globals  #
#===========#

model_tmp_path = '/home/nick/Dropbox/recola_feynrules_wrapper/ModelTMP/'
# used in feynrules_wrapper as tensor indices
free_indices = ['mu', 'nu', 'ro', 'ta', 'te', 'tu', 'tt']
ri_indices = ['j', 'k', 'p', 'q', 'r', 's']

#===========#
#  Methods  #
#===========#

#==============================================================================#
#                                  TypeError                                   #
#==============================================================================#

class TypeError(Exception):
  """
  TypeError exception is raised if arguments with the wrong type is passed to
  a function.
  """
  def __init__(self, value):
    self.value = value

  def __str__(self):
    return repr(self.value)
#==============================================================================#
#                                   ArgError                                   #
#==============================================================================#


class ArgError(Exception):
  """
  ArgError exception is raised if a given argument is expected to be something
  else.
  """
  def __init__(self, value):
    self.value = value

  def __str__(self):
    return repr(self.value)

#==============================================================================#
#                                   flatten                                    #
#==============================================================================#

#------------------------------------------------------------------------------#
# Generator for list flattening
# from http://stackoverflow.com/questions/2158395/
#     flatten-an-irregular-list-of-lists-in-python

flatten = lambda *n: (e for a in n
                      for e in (flatten(*a) if isinstance(a, (tuple, list)) else
                                (a,)))

#==============================================================================#
#                                     fold                                     #
#==============================================================================#

def fold(f, l, a):
    """
    f: the function to apply
    l: the list to fold
    a: the accumulator, who is also the 'zero' on the first call
    >>> fold(lambda x,y: x + y, [1,1,2], 0)
    4
    """
    return a if(len(l) == 0) else fold(f, l[1:], f(a, l[0]))


#==============================================================================#
#                             isCyclicPermutation                              #
#==============================================================================#

def isCyclicPermutation(permutation):
  """
  permutation: List of non-degenerate integers without gaps
  return: True if the list is a cyclc permutation, else False
  """
  ret = True
  pLen = len(permutation)
  for i in range(pLen):
    ret = ret and (permutation[i] % pLen == (permutation[i-1]+1) % pLen)

  return ret

#==============================================================================#
#                                binomialCoeff                                 #
#==============================================================================#

def binomialCoeff(n, k):
  """ Computes the binomial coefficient
  http://rosettacode.org/wiki/Evaluate_binomial_coefficients#Python
  >>> binomialCoeff(3, 2)
  3
  >>> binomialCoeff(3, 3)
  1
  """
  result = 1
  for i in xrange(1, k+1):
      result = result * (n-i+1) // i
  return result

#==============================================================================#
#                             unique_permutations                              #
#==============================================================================#

def unique_permutations(t, no_identity=False):
  """ Generates unique permutations recursively from a given list t

  :param t: the list to be mad to be permuted
  :type  t: list of anything which can be compared (__eq__, __ne__)

  :param no_identity: Does not generate the element `t` itself
  :type  no_identity: bool

  >>> [u for u in unique_permutations([1,1,2])]
  [[1, 1, 2], [1, 2, 1], [2, 1, 1]]
  >>> [u for u in unique_permutations([1,1,2], no_identity=True)]
  [[1, 2, 1], [2, 1, 1]]

  >>> [u for u in unique_permutations([1])]
  [[1]]
  >>> [u for u in unique_permutations([1], no_identity=True)]
  []
  """
  lt = list(t)
  lnt = len(lt)
  if lnt == 1:
    if no_identity:
      pass
    else:
      yield lt
  st = set(t)
  for d in st:
    lt.remove(d)
    for perm in unique_permutations(lt):
      ret = [d] + perm
      if (not no_identity) or ret != list(t):
        yield [d]+perm
    lt.append(d)


def permute_spins(spins, permutation, position=False):
  """ Permutes the list `spins` according to an element of the permutation
  group. The identity is given by id = [0, 1, 2, ..].

  :param spins: s = [s1, s2, s3, ...],
                where the s_i are can be anything.
  :param permutation:  i = [i1, i2, i3, ...] where i_j = {0, 1, 2, ...}

  :position: toggle the way the permutation is done. If False the mapping is:
             s1 -> pos.i1
             s2 -> pos.i2
             s3 -> pos.i3
             If True the mapping is:
             s[i1] -> pos.i1

  >>> permute_spins([1, 2, 3], [1, 0, 2])
  [2, 1, 3]
  >>> permute_spins([1, 2, 3], [1, 2, 0])
  [3, 1, 2]
  >>> permute_spins(['a', 'a', 'Z'], [1, 2, 0], position=True)
  ['a', 'Z', 'a']
  """
  tmp = [None] * len(spins)
  for i in range(len(spins)):
    if position:
      tmp[i] = spins[permutation[i]]
    else:
      tmp[permutation[i]] = spins[i]
  return tmp


permute_elements = permute_spins
perm_elems = permute_spins


def permute_particles(elements, permutation, shift=+1):
  """ Permutes only particle indices, i.e. positive integers greater zero.

  shift = +1 is used when
  permutations are assumed to with 0

  >>> permute_particles([1,2,3,4], [1,2,0,3])
  [2, 3, 1, 4]
  >>> permute_particles([[1,2],[3,4]], [1,2,0,3])
  [[2, 3], [1, 4]]
  >>> permute_particles([[[1],2],[3,[4]]], [2,3,1,0])
  [[[3], 4], [2, [1]]]
  >>> permute_particles([0,2,1], [2,0,1],shift=0)
  [2, 1, 0]
  """
  try:
    if type(elements) is int:
      if type(elements) is int:
        if elements >= shift:
          return permutation[elements - shift] + shift
        else:
          return elements
      elif type(elements) is str:
        return elements
    elif type(elements) is list:
      return [permute_particles(elem, permutation, shift) for elem in elements]
  except:
    print("elements", elements)
    print("permutation", permutation)
    print("shift", shift)
    raise


def permute_string(string, permutation):
  """ Permutes appearing integers in a string. The integers should be
  surrounded by parenthesis and separated by comma in the following form

    'any string here (i1, i2, ,..,in) any string there (k1, k2, ... )'

  The method ignores negative integers (whitespace between - and integer leads
  to mistakes)

  >>> permute_string('f(1 , 2,3)',[2,0,1])
  'f(3 , 1,2)'
  >>> permute_string('f(-1,1,2)*f(3,4,-1)', [1, 2, 0, 3])
  'f(-1,2,3)*f(1,4,-1)'
  """
  from rept1l.helper_lib import string_subs
  # Define the substitution of integers due to permutation
  nsubs = {str(pos + 1): str(val + 1) for pos, val in enumerate(permutation)}

  pospat = r'[\(,]?\s*\-?\d+'
  pospat = re.compile(pospat)

  negpat = r'\-\d+'
  negpat = re.compile(negpat)

  matches = pospat.findall(string)

  # Define the substitution of positive inters as they appear in the expression
  psubs = {}
  for match in matches:
    if not negpat.search(match):
      psubs[match] = string_subs(match, nsubs)

  return string_subs(string, psubs)

#==============================================================================#
#                         get_permutations_from_spins                          #
#==============================================================================#

def get_permutations_from_spins(spins):
  """
  Given a spin configuration 'spins' the procedure computes all
  non-equivalent permutations in form of elements of the perumation group:

  :param spins: [s1, s2, s3, ...],
                where the s_i are integers. The convention is:
                1: scalar,
                2: fermion,
                3: vector

  :return: [p(s1),p(s2), p(s3), ...], p(si) denotes the new position of the
            particle corresponding to the spin si.

  >>> spins = [2,1,1]
  >>> get_permutations_from_spins(spins)
  [[2, 0, 1], [1, 0, 2], [0, 1, 2]]
  >>> perms = get_permutations_from_spins([2,1,1])
  >>> [permute_spins(spins, perm) for perm in perms]
  [[1, 1, 2], [1, 2, 1], [2, 1, 1]]
  """
  if len(spins) == 1:
    return [[0]]
  perm = []
  for new_conf in unique_permutations(spins):
    new_perm = [None]*len(spins)
    for j in range(0, len(spins)):
      k = 0
      try:
        while (spins[j] != new_conf[k]):
          k = k+1
      except:
        print("spins:", spins)
        print("new_conf:", new_conf)
        sys.exit()

      new_perm[k] = j
      new_conf[k] = 0

    inv_perm = [None]*len(spins)

    for i in range(len(new_perm)):
      inv_perm[new_perm[i]] = i

    perm.append(inv_perm)

  return perm

def gen_permutations(ints, no_identity=False):
  """ Takes a list of positive integers and generates unique permutation leading
  to different states

  :param ints: iterable of positive integer
  :type  ints: iterable

  :param no_identity: Does not include the element `ints` itself
  :type  no_identity: bool.

  :return: [p(s1),p(s2), p(s3), ...], p(si) denotes the new position of the
            particle corresponding to the spin si.

  >>> spins = [2,1,1]
  >>> perms = [u for u in gen_permutations(spins)]
  >>> perms
  [[2, 0, 1], [1, 0, 2], [0, 1, 2]]
  >>> [permute_spins(spins, perm) for perm in perms]
  [[1, 1, 2], [1, 2, 1], [2, 1, 1]]

  >>> perms = gen_permutations(spins, no_identity=True)
  >>> [permute_spins(spins, perm) for perm in perms]
  [[1, 1, 2], [1, 2, 1]]
  """
  if len(ints) == 1:
    if no_identity:
      yield []
    else:
      yield [0]
  perm = []
  for new_conf in unique_permutations(ints, no_identity=no_identity):
    new_perm = [None]*len(ints)
    for j in range(0, len(ints)):
      k = 0
      try:
        while (ints[j] != new_conf[k]):
          k = k+1
      except:
        print("ints:", ints)
        print("new_conf:", new_conf)
        sys.exit()

      new_perm[k] = j
      new_conf[k] = 0

    inv_perm = [None]*len(ints)

    for i in range(len(new_perm)):
      inv_perm[new_perm[i]] = i

    yield inv_perm

###############################################################################
#                      compute_permutation_from_elements                      #
###############################################################################

def compute_permutation_from_elements(elem1, elem2):
  """ Returns a permutation which translates elem1 into elem2
  >>> c0,c1,c2,c3,c5 = symbols('c0 c1 c2 c3 c5')
  >>> e1 = [['c0'], ['c1'], ['c2'], ['c3']]
  >>> e2 = [['c1'], ['c2'], ['c0'], ['c3']]
  >>> compute_permutation_from_elements(e1, e2)
  [2, 0, 1, 3]

  >>> e1 = [[c0 + c2, -c0 - c2], [c2 + 3*c5], [c2 - c5], [-2*c5], [-c3 + c5,\
c3 - c5], [c2 - c5], [-c2 + c5], [-c2 - c5], [-c2 + c5]]
  >>> e2 = [[c2 - c5], [c2 - c5], [-c2 + c5], [-c2 - c5], [-c2 + c5], [-2*c5],\
[c2 + 3*c5], [c0 + c2, -c0 - c2], [-c3 + c5, c3 - c5]]
  >>> perm = compute_permutation_from_elements(e1, e2)
  >>> permute_spins(e2, perm, position=True) == e1
  True
  """
  assert(len(elem1) == len(elem2))
  e2offset = 0
  ret = []
  for elem in elem1:
    e2offset = 0
    while elem2[e2offset] != elem or e2offset in ret:
      e2offset += 1
    ret.append(e2offset)
  return ret

#==============================================================================#
#                                 s_to_p_perm                                  #
#==============================================================================#

def s_to_p_perm(spin_perm):
  """
  Transforms a spin permutation to a particle permutation.

  >>> s_perm = [1, 2, 0, 3]
  >>> p_perm = s_to_p_perm(s_perm)
  >>> p_perm
  [2, 0, 1, 3]

  The spin permutation s_perm says that the element at position 0(1, 2, 3)
  moves to 1(2, 0, 3). The particle permutation [2, 0, 1, 3] does the same,
  however, the interpretation is different. It says that after permuting
  position 2(0, 1, 3) is occupied by the elem at position 0(1, 2, 3).

  >>> elem = ['a', 'b', 'c', 'd']
  >>> permute_spins(elem, s_perm) == permute_spins(elem, p_perm, position=True)
  True
  """
  return [v[0] for v in sorted([(pos, u) for pos, u in enumerate(spin_perm)],
          key=lambda x: x[1])]

#==============================================================================#
#                         generate_unique_permutation                          #
#==============================================================================#

#------------------------------------------------------------------------------#
def generate_unique_permutations(elements):
  """
  Given a list of elements generate_unique_permutations generates unique
  permutations from that list.

  :param elements: elements to be permuted
  :type  elements: list of strings or integers
  """
  for new_conf in unique_permutations(elements):
    new_perm = [None]*len(elements)
    for j in range(0, len(elements)):
      k = 0
      while (elements[j] != new_conf[k]):
        k = k+1

      new_perm[k] = j
      new_conf[k] = 0

    inv_perm = [None]*len(elements)

    for i in range(len(new_perm)):
      inv_perm[new_perm[i]] = i

    yield inv_perm

#------------------------------------------------------------------------------#

#==============================================================================#
#                          order_grouped_permutations                          #
#==============================================================================#

#------------------------------------------------------------------------------#
def order_grouped_permutations(expression):
  """
  Given an

  expression: arbitrarily grouped (nested) list containing only integers,

  this function assigns a value to the list. The assigned value is higher the
  less the list is diordered. Comparing two different lists, the first
  non-matching integer (lists are flattened) determines to which one of the
  lists the higher value is assigned.
  """
  ret = 0
  flattened_expr = [u for u in flatten(expression)]
  base = len(flattened_expr)+1
  for i in range(len(flattened_expr)):
    ret += (flattened_expr[i]+1)*base**(base - i - 2)
  return ret

#------------------------------------------------------------------------------#

def order_expression(seq, base, weight=1, neg_weight_zero=False):
  """ Computes a weight for each list elements.

  :param seq: sequence of aribtrarily nested integers
  :type  seq: list, integers

  :param base: base to which the values are normalized
  :type  base: integer

  :param weight: if positiv, lower values yield lower weights and vice versa
  :type  weight: integer
  """
  if (base == 0):
    base = 1
  ret = 0
  #for i in range(len(seq)):
  for i, s in enumerate(flatten(seq)):
    #if seq[i] >= 0 or not neg_weight_zero:
    if s >= 0 or not neg_weight_zero:
      ret += (abs(s) + 1) * base**(weight*(base - i - 2))

  return ret

#==============================================================================#
#                          interchange_transformation                          #
#==============================================================================#

#------------------------------------------------------------------------------#
def interchange_transformation(grouped_permutations, symmtry_transformations):
  """
  Given grouped_permutation permutations (symmtry_transformations) between same
  lorentz_structures can be eliminated. This function computes explicitly these
  permutation.
  """
  #new_grouped_permutations = []
  for grouped_permutation in grouped_permutations:
    new_grouped_permutation = tuple([grouped_permutation])
    for sym_transf in symmtry_transformations:
      new_grouped_permutation += tuple([permute_spins(
                                       grouped_permutation, sym_transf)])
    new_grouped_permutation = tuple(sorted([u for u in
                                            new_grouped_permutation],
                                           key=order_grouped_permutations))

    yield new_grouped_permutation
    #new_grouped_permutations.append(new_grouped_permutation)
#
  #return new_grouped_permutations
#------------------------------------------------------------------------------#

#==============================================================================#
#                          construct_group_symmetries                          #
#==============================================================================#

#------------------------------------------------------------------------------#
def construct_group_symmetries(group_sizes, positions, symmetries):
  """ This function allows to derive the basis of products of lorentz structures
  with invariance respecting internal symmetries.

  group_sizes: size of the groups
  positions: usually range(len(group_sizes))  # allows the possibilty to permute
                                              # lorentz structures, but not used
                                              # yet
  symmetries: element of the permutation group which permutes the arguments of a
              lorentz structure. The range is (0:len(lorentz arguments)-1)

  The symmetries are translated to permutation group elements for the product of
  lorentz structures. This firstly demands to embed the permutation in a higher
  permutation (product space) and then compute all products of these permutation
  which usually yield new invariance permutation for the product of lorentz
  structures. The identity permutation is not included in the result.
  """
  size = fold(lambda x, y: x + y, group_sizes, 0)
  offsets = []
  for position in positions:
    offsets.append(fold(lambda x, y: x + y, group_sizes[:position], 0))

  symmetry_permutatons = []
  for i in range(len(symmetries)):
    for symmetry in symmetries[i]:
      symmetry_tmp = [x + y for x, y in zip(symmetry,
                                            [offsets[i]]*len(symmetry))]
      tmp = range(size)
      # TODO: nick found bug here. Fr 22 Aug 2014 11:30:39 CEST
      # the index of group_sizes was set to position which is actually not
      # set, the bug appeared then for differently large group size where it
      # matters what the index is
      tmp[offsets[i]:offsets[i]+group_sizes[i]] = symmetry_tmp
      symmetry_permutatons.append(tmp)

  ret = symmetry_permutatons[:]
  #TODO: nick bug here. not all symmetries are generated Sa 19 Jul 2014 17:00:41
  # CEST
  #TODO: nick bug should be fixed - check Do 31 Jul 2014 17:23:57 CEST
  if len(symmetry_permutatons) > 0:
    ret.extend(compute_permutations(symmetry_permutatons, symmetry_permutatons,
                                    0, len(group_sizes)))

    identity = range(len(ret[0]))
    for i in range(len(ret)):
      if (not ret[i] in ret[0:i] and ret[i] != identity):
        yield ret[i]
#------------------------------------------------------------------------------#

def compute_permutations(sp1, sp2, start_index, recursive):
  if recursive == 1:
    return sp1
  ps = []
  for k in range(len(sp1)):
    for j in range(start_index, len(sp2)):
      tmp1 = permute_spins(sp1[k], sp2[j])
      if tmp1 not in ps:
        ps.append(tmp1)
  tmp2 = compute_permutations(ps, sp2, k, recursive - 1)
  if len(tmp2) > 0:
    for tmp in tmp2:
      if tmp not in ps:
        ps.append(tmp)

  return ps

#==============================================================================#
#                              group_permutations                              #
#==============================================================================#

#------------------------------------------------------------------------------#
def group_permutations(permutations, group_sizes, symmetries=[],
                       index_repl=[]):
  """
  Given a set of permutation of the same size, the function groups the
  permutation according to group_sizes with the contraint: len(permutations[i])
  = sum(group_sizes[:]) for all permutation.
  For every group_sizes[i] one can define a set of invariance permutation which
  reflects the invariance of argument permutations.
  For instance, given g^{\mu \nu} g^{\alpha \beta}. One would group as
  group_sizes = [2, 2] and since the metrics are symmetric under argument
  permutation one would add symmetries = [  [ [1,0] ] ,   [[1,0]]  ] which act
  as follows:
  [1,0]: g^{\mu \nu} -> g^{\nu \mu}
  The first dimension is the dimension of the groups. The second allows to
  define argument permutations. In this example we only have one non-trivial
  argument permutation. The third dimension is simple the permutation group
  element.
  """
  if (len(symmetries) > 0):
    # TODO: nick bug in consturct_group_symmetries for ggg Sa 19 Jul 2014
    # 16:50:04 CEST
    # the case where all groups are permuted is missing
    symmetries = [u for u in construct_group_symmetries(group_sizes,
                                                        range(len(group_sizes)),
                                                        symmetries)]
  grouped_permutations = []
  for permutation in permutations:
    size_tmp = 0
    grouped_permutation = []
    if (len(symmetries) == 0):
      for size in group_sizes:
        tmp = permutation[size_tmp:size_tmp + size]
        if (len(index_repl) > 0):
          for i in range(len(tmp)):
            tmp[i] = index_repl[tmp[i]]
        grouped_permutation.append(tmp)
        size_tmp += size

      grouped_permutations.append(grouped_permutation)
    else:
      permutation_sym = [permutation]
      for sym in symmetries:
        permutation_sym.append(permute_spins(permutation, sym))

      grouped_permutation = []
      for perm_sym in permutation_sym:
        same_group = []
        size_tmp = 0
        for size in group_sizes:
          tmp = perm_sym[size_tmp:size_tmp + size]
          if (len(index_repl) > 0):
            for i in range(len(tmp)):
              tmp[i] = index_repl[tmp[i]]

          same_group.append(tmp)
          size_tmp += size

        grouped_permutation.append(same_group)

      grouped_permutation = sorted(grouped_permutation,
                                   key=order_grouped_permutations)
      grouped_permutations.append(tuple(grouped_permutation))

  return grouped_permutations
#------------------------------------------------------------------------------#

#==============================================================================#
#                           remove_redundant_transf                            #
#==============================================================================#

#------------------------------------------------------------------------------#
def remove_redundant_transf(expr):
  """
  After performing the interchange_transformation, the left non-trivial
  permutation can be obtained by selecting only one of the combined
  permutations. We choose the first one. Further, since in the expression double
  counting occurs, we need to make sure not to include a permutation twice.
  """
  n = []
  for i, exp in enumerate(expr):
    #if expr[i] not in expr[0:i]:
    if exp not in n:
      n.append(exp)
      base = sum(len(u) for u in exp)+1
      exp_sorted = sorted(exp, key=lambda x: order_expression(x, base, 1))
      #yield expr[i][0]
      yield exp_sorted[0]
#------------------------------------------------------------------------------#

#==============================================================================#
#                             reduce_equal_indices                             #
#==============================================================================#

#------------------------------------------------------------------------------#

def reduce_equal_indices(permutation, equal_indices):
  """
  Given a product of lorentz structures in sum n arguments, if arguments happen
  to be the same the cardinality of the invariance space is reduced. This
  function allows to consider same indices. The idea is as follows:
  In the permutations the indices which are supposed to be the same are ordered
  from small to high values. Consequently, permutation where the same indices
  are permuted in a certain way results always in the same permutation. Finally,
  one has to eliminate double occurence of permutations.
  """
  not_used = equal_indices[:]
  replaced = []
  for i in range(len(permutation)):
    if (permutation[i] in not_used or permutation[i] in replaced):
      replaced.append(not_used[0])
      permutation[i] = not_used[0]
      not_used.pop(0)

  return permutation
#------------------------------------------------------------------------------#

def get_partitions(n):
  if n == 0:
    yield []
    return

  for p in get_partitions(n-1):
    yield p + [1]
    if p and (len(p) < 2 or p[-2] > p[-1]):
      yield p[:-1] + [p[-1] + 1]


#==============================================================================#
#                                    m_of_n                                    #
#==============================================================================#

#------------------------------------------------------------------------------#
def m_of_n(expr, m):
  """Docstring for m_of_n.

  :expr: list
  :returns: list of lists consiting of m of the former list. The elements in the
            list of lists are ordered as in expr and are treated as commutative.

  """
  ret = []
  if (m == len(expr)):
    return [expr]
  elif (m == 0 or m > len(expr)):
    return []
  elif (m == 1):
    return [[u] for u in expr]
  else:
    for i in range(len(expr)):
      before = expr[0:i]
      after = expr[i+1:len(expr)]
      for mm1 in m_of_n(after, m-1):
        ret.append([expr[i]] + mm1)
  return ret
#------------------------------------------------------------------------------#

#==============================================================================#
#                            construct_tensor_keys                             #
#==============================================================================#

#------------------------------------------------------------------------------#
def construct_tensor_keys(max_rank, rank):
  """
  >>> construct_tensor_keys(3, 0)
  ['0']
  >>> construct_tensor_keys(2, 1)
  ['mu', 'nu']
  >>> construct_tensor_keys(1, 1)
  ['mu']
  >>> construct_tensor_keys(2, 2)
  ['munu']
  >>> construct_tensor_keys(3, 2)
  ['munu', 'muro', 'nuro']
  """
  if (rank == 0):
    return ['0']
  else:
    ret = []
    for keys in m_of_n(free_indices[:max_rank], rank):
      ret.append(fold(lambda x, y: x + y, keys, ''))

    return ret
#------------------------------------------------------------------------------#


#==============================================================================#
#                            shift_tensor_keys_dict                            #
#==============================================================================#

#------------------------------------------------------------------------------#
def shift_tensor_keys_dict(max_rank, rank):
  ret = dict()
  if (rank == 0):
    repl_dict = dict()
    repl_dict['0'] = '0'
    ret['0'] = repl_dict
  else:
    for keys in m_of_n(free_indices[:max_rank], rank):
      repl_dict = dict()
      for k in range(len(keys)):
        repl_dict[keys[k]] = free_indices[k]

      key = fold(lambda x, y: x + y, keys, '')
      ret[key] = repl_dict

    return ret
#------------------------------------------------------------------------------#


#==============================================================================#
#                            get_indices_additional                            #
#==============================================================================#

#------------------------------------------------------------------------------#
def get_indices_additional(additional, additionals_ranked):
  """
  Determines the rank of addtional.

  additional: sympy symbol
  additionals_ranked: a dictionary listing symbols with their rank
  returns list of tensor indices and a possibly updated additionals_ranked dict

  >>> x = Symbol('x'); cima = Symbol('cima'); x.name = 'wl(mu,rank)'
  >>> get_indices_additional(x*cima, {})
  (['mu'], {cima: ([], False), wl(mu,rank): (['mu'], True)})
  """

  tensor_indices = []
  args = additional.args
  if (len(args) > 0):
    for term in args:
      if term in additionals_ranked.keys():
        tensor_indices.extend(additionals_ranked[term][0])
      else:
        indices_retrieved, additionals_ranked = (get_indices_additional(term,
                                                 additionals_ranked))
        if len(indices_retrieved) > 0:
          tensor_indices.extend(indices_retrieved)
  else:
    if (additional in additionals_ranked.keys()):
      tensor_indices.extend(additionals_ranked[term])
    else:
      for index in free_indices:
        index_rules1 = re.compile("\(" + index + "\)")
        index_rules2 = re.compile("\(" + index + "\,")
        index_rules3 = re.compile("\," + index + "\)")
        index_rules4 = re.compile("\," + index + "\,")
        match = (index_rules1.search(str(additional)) or
                 index_rules2.search(str(additional)) or
                 index_rules3.search(str(additional)) or
                 index_rules4.search(str(additional)))
        if match:
          tensor_indices.append(index)

      is_ranked_rule = re.compile("rank")
      match = is_ranked_rule.search(str(additional))
      if match:
        additionals_ranked[additional] = (tensor_indices, True)
      else:
        additionals_ranked[additional] = (tensor_indices, False)

  return tensor_indices, additionals_ranked
#------------------------------------------------------------------------------#

#==============================================================================#
#                           rank_sorted_additionals                            #
#==============================================================================#

#------------------------------------------------------------------------------#
def rank_sorted_additionals(additionals):
  """ Additionals are computed before the currents. In loop currents addtionals
  can be dependent on the incoming rank and on lorentz indices.

  :param additionals: List of subexpression as returned from the Sympys cse
                      method
  :type  additionals: List of tuples of Sympy expressions.

  :return: dictionary where the key is the rank increase (int) and the values
           are lists of tuples. The first entry in the tuple is the additonal
           variable and the second entry is True if the additional variable
           depends on lorentz indices.

  >>> x0, x1 = symbols('x0 x1')
  >>> y1, y2, y3, y4 = symbols('y1 y2 y3 y4')
  >>> y1.name = 'cima'; y2.name= 'w1(mu)'
  >>> y3.name = 'w2(0)'; y4.name = 'w3(rank)';

  >>> rsa = rank_sorted_additionals([(x0, y1*y2), (x1, y3*y4)])
  >>> rsa[0]
  [(x1, True)]
  >>> rsa[1]
  [(x0, False)]

  The first expression does in increase the rank by one whereas the second does
  only depend on the incoming rank, but does not carry a lorentz index.
  """
  additionals_ranked = dict()
  for additional in additionals:
    indices, additionals_ranked = get_indices_additional(additional[1],
                                                         additionals_ranked)
    additional_indices = []
    additional_ranked = False
    if (additional[0] in additionals_ranked.keys()):
      additional_indices, is_ranked = additionals_ranked[additional[0]]
      if is_ranked:
        additional_ranked = True
      for index in indices:
        if (index not in additional_indices):
          additional_indices.append(index)
    else:
      for key in additionals_ranked.keys():
        val = additionals_ranked[key]
        if str(key) in str(additional[1]):
          additional_ranked = additional_ranked or val[1]
      additional_indices = set(indices)

    additionals_ranked[additional[0]] = (additional_indices, additional_ranked)

  ret = dict()
  for additional in additionals:
    key, is_ranked = (len(additionals_ranked[additional[0]][0]),
                      additionals_ranked[additional[0]][1])
    if key not in ret.keys():
      ret[key] = []
    ret[key].append((additional[0], is_ranked))

  return ret
#------------------------------------------------------------------------------#


#==============================================================================#
#                                pair_arguments                                #
#==============================================================================#

#------------------------------------------------------------------------------#
def pair_arguments(arg_lists, as_list=True):
  """
  arg_lists = [ list1, list2 , ...]
  returns list of (a,b, ..) for a in list1, b in list 2 etc.
  >>> pair_arguments([[1, 2], [3, 4]])
  [(1, 3), (1, 4), (2, 3), (2, 4)]
  """
  if as_list:
    return list(product(*arg_lists))
  else:
    return product(*arg_lists)
#------------------------------------------------------------------------------#

#==============================================================================#
#                              class lorentz_base                              #
#==============================================================================#

class lorentz_base(object):
  order = {}
  n_base = 1
  args = {'lorentz': 'lorentz',
          'lorentzNC': None,           # non-contracted lorentz index
          'spinor': 'antispinor',
          'antispinor': 'spinor',
          'particle_id': None}

  open_args = {'lorentz': True,
               'lorentzNC': True,
               'spinor': True,
               'antispinor': True,
               'particle_id': False}

  bases = []
  lorentz_base = {}
  contractions = {}

  max_particles = 4

  def __init__(self, symbol, arg_types, arg_domain, arg_symmetry=[]):
    if len(arg_symmetry) > 0:
      if len(arg_symmetry[0]) > 0:
        assert(len(arg_types) == len(arg_symmetry[0]))
    for arg in arg_types:
      assert(arg in lorentz_base.args)

    lorentz_base.order[symbol] = lorentz_base.n_base
    lorentz_base.n_base += 1

    self.symbol = symbol
    self.arg_range = len(arg_types)
    self.arg_types = arg_types
    # todo: Open args has two meanings. One is that the the index can in
    # principle be contracted. The other one (here) means that the index is
    # not expected to be contracted (which is always the case for the metric)
    self.open_args = len([1 for arg in arg_types if lorentz_base.args[arg]])
    self.arg_domain = arg_domain
    self.arg_symmetry = arg_symmetry

    # Derive all possible contraction between same and different structure
    # keys:   primary structure
    # values: dictionary where the keys are the secondary strutures with which
    #         the primary one is contracted with. The values is the dict contr
    #         explained below.
    # lorentz_base = {Lp: {Ls : {argp : [args1, args2, ..], ... }, ...}, ... }
    lorentz_base.contractions[self] = {}

    # contraction the structures
    # contr a dictionary which keys are the argument indices and the values are
    # list of integers representing with wich argument index the former ones can
    # be contracted with
    contr = {}
    for arg_pos, arg_type in enumerate(self.arg_types):
      carg = lorentz_base.args[arg_type]
      if carg:
        indices = [i for i, x in enumerate(self.arg_types) if x == carg]
        contr[arg_pos] = indices
    if contr:
      lorentz_base.contractions[self].update({self: contr})

    # contractions of different structures
    for prev_base in lorentz_base.bases:
      contr = {}
      for arg_pos, arg_type in enumerate(self.arg_types):
        carg = lorentz_base.args[arg_type]
        if (carg) and (carg in prev_base.arg_types):
          indices = [i for i, x in enumerate(prev_base.arg_types) if x == carg]
          contr[arg_pos] = indices
      if contr:
        lorentz_base.contractions[self].update({prev_base: contr})

      contr = {}
      for arg_pos, arg_type in enumerate(prev_base.arg_types):
        carg = lorentz_base.args[arg_type]
        if (carg) and (carg in self.arg_types):
          indices = [i for i, x in enumerate(self.arg_types) if x == carg]
          contr[arg_pos] = indices
      if contr:
        lorentz_base.contractions[prev_base].update({self: contr})
    if not lorentz_base.contractions[self]:
      del lorentz_base.contractions[self]

    lorentz_base.bases.append(self)

  @classmethod
  def get_base(self, base_symbol):
    sym_base = [u.symbol for u in self.bases]
    if base_symbol in sym_base:
      pos = sym_base.index(base_symbol)
      return self.bases[pos]
    else:
      raise ValueError('Base: ' + base_symbol.name + ' not found.')

  def __repr__(self):
    return self.symbol.name

  def __str__(self):
    return self.symbol.name

  @classmethod
  def get_open_args(cls, lb, args):
    """ Returns the arguments `args` of the lorentz base `lb` which are true
    open arguments (can be multiplied wich a tensor carrying a suited argument)
    """
    base = cls.get_base(lb)
    arg_types = base.arg_types
    return [arg for apos, arg in enumerate(args)
            if cls.open_args[arg_types[apos]]]

    #self.open_args = len([1 for arg in arg_types if lorentz_base.args[arg]])

#==================#
#  lorentz strucs  #
#==================#

scalar = 1
P, g = symbols('P Metric')
GammaM, GammaP, Gamma, Gamma5 = symbols('GammaM GammaP Gamma Gamma5')
ProjM, ProjP = symbols('ProjM ProjP')
Epsilon = Symbol('Epsilon')
Identity = Symbol('Identity')
Sigma = Symbol('Sigma')

def order_strucs(strucs):
  return sorted(strucs, key=lambda struc: lorentz_base.order[struc])

P_lb = lorentz_base(P, ['lorentz', 'particle_id'], range(-5, 6, 1))
g_lb = lorentz_base(g, ['lorentzNC', 'lorentzNC'], range(5), [[1, 0]])
GammaP_lb = lorentz_base(GammaP, ['lorentz', 'antispinor', 'spinor'], range(5))
GammaM_lb = lorentz_base(GammaM, ['lorentz', 'antispinor', 'spinor'], range(5))
Gamma_lb = lorentz_base(Gamma, ['lorentz', 'antispinor', 'spinor'], range(5))
ProjP_lb = lorentz_base(ProjP, ['antispinor', 'spinor'], range(5))
ProjM_lb = lorentz_base(ProjM, ['antispinor', 'spinor'], range(5))
Identity_lb = lorentz_base(Identity, ['antispinor', 'spinor'], range(5))
Gamma5_lb = lorentz_base(Gamma5, ['antispinor', 'spinor'], range(5))
Sigma_lb = lorentz_base(Sigma, ['lorentzNC', 'lorentzNC',
                                'antispinor', 'spinor'], range(5))
Epsilon4 = lorentz_base(Epsilon,
                        ['lorentz', 'lorentz', 'lorentz', 'lorentz'], range(4))

def composed_lorentz_base(lbs):
  """ DEPRECATED - Use composed_lorentz_base_brute instead which is much faster.
  Computes the unique lorentz bases by exploiting the group and lorentz
  symmetries provided by the user.

  :param lbs: the lorentz base
  :type  lbs: list of lorentz_base instances
  """
  base_ordered = {}
  for i in range(lorentz_base.n_base):
    base_ordered[i] = []
  n_arg = 0
  for base in lbs:
    base_ordered[lorentz_base.order[base.symbol]].append(base)
    n_arg += base.arg_range

  symmetries = []
  group_sizes = []
  has_internal_sym = False
  for order in base_ordered:
    for base in base_ordered[order]:
      symmetries.append(base.arg_symmetry)
      if len(base.arg_symmetry) > 0:
        has_internal_sym = True
      group_sizes.append(base.arg_range)
  perms = group_permutations(gen_permutations(range(n_arg)),
                             group_sizes=group_sizes,
                             symmetries=symmetries,
                             index_repl=range(1, n_arg + 1))
  if has_internal_sym:
    pre_base = [u for u in remove_redundant_transf(perms)]
  else:
    pre_base = perms

  offset = 0
  symmetries = []
  group_sizes = []
  has_group_sym = False
  for order in base_ordered:
    glen = len(base_ordered[order])
    if glen > 0:
      has_group_sym = True
      group_sizes.append(glen)
      sym = gen_permutations(range(glen), no_identity=True)
      symmetries.append(sym)
  if has_group_sym:
    sym = construct_group_symmetries(group_sizes, range(len(group_sizes)),
                                     symmetries)
    sym = [u for u in sym]
    pre_base = interchange_transformation(pre_base, sym)
    return [u for u in remove_redundant_transf(pre_base)]
  else:
    return pre_base


#==============================================================================#
#                         composed_lorentz_base_brute                          #
#==============================================================================#

def composed_lorentz_base_brute(lbs):
  """ Computes the unique lorentz bases by exploiting the group and lorentz
  symmetries provided by the user.

  :param lbs: the lorentz base
  :type  lbs: list of lorentz_base instances

  :return: list of list of list of integers.
           First list order are the different lorentz bases.
           Second list is grouped according to the number of lorentz bases in
           lbs.
           Third list are explicit arguments of the lorentz bases represented as
           integers.


            given lbs = [ L1, L2, L3, ..., LN]

            return = [ e1, e2, ..., eK]
            ei     = [ ei1, ei2, ei3, ..., eiN] matches the number of L in lbs
            eij    = [eij1, eij2, ..., eijP]    where eijk integers
                                                P equals the number of arguments
                                                of Lj

           example:  g*g -> lbs = [g,g]
           base:     [g^{12}*g^{34}, g^{13}*g^{24}, g^{14}*g^{23}]
                     = [[[0, 1], [2, 3]], [[0, 2], [1, 3]], [[0, 3], [1, 2]]]

  """
  base_ordered = {}
  for i in range(lorentz_base.n_base):
    base_ordered[i] = []
  # retrieve the total number of arguments
  n_arg = 0
  for base in lbs:
    base_ordered[lorentz_base.order[base.symbol]].append(base)
    n_arg += base.arg_range

  # The group is equivalent to the lorentz base, and we have groups = len(lbs).
  # Each group size is given by the number of arguments of that lorentz base
  group_sizes = []
  has_internal_sym = False
  for order in base_ordered:
    for base in base_ordered[order]:
      group_sizes.append(base.arg_range)

  base_ordered_expanded = [u for u in flatten(
                           [base_ordered[i] for i in range(len(base_ordered))])]

  res = []
  # TODO: (nick 2014-11-17) bug in generate.. why do I use this function and not
  # unique_permutation?
  #for perm in generate_unique_permutations(range(n_arg)):
  for perm in unique_permutations(range(1, n_arg+1)):
    grouped_perm = []
    size_tmp = 0
    for size in group_sizes:
      tmp = perm[size_tmp:size_tmp + size]
      #if (len(index_repl) > 0):
        #for i in range(len(tmp)):
          #tmp[i] = index_repl[tmp[i]]
      grouped_perm.append(tmp)
      size_tmp += size
    # exploit internal symmetries
    for pos, group in enumerate(grouped_perm):
      if base_ordered_expanded[pos].arg_symmetry:
        representants = [(permute_elements(group, sym))
                         for sym in
                         base_ordered_expanded[pos].arg_symmetry] + [group]
        ordered_representants = [order_grouped_permutations(u)
                                 for u in representants]
        choice = min(ordered_representants)
        choice = ordered_representants.index(choice)
        representant = representants[choice]
        grouped_perm[pos] = representant

    offset = 0
    # exploit symmetries due to products of same group members <->
    # interchangability
    for i in base_ordered:
      if base_ordered[i]:
        groups = grouped_perm[offset:offset+len(base_ordered[i])]

        base = sum(group_sizes)
        base_order = lambda x: order_expression(x, base, 1)
        groups = sorted(groups, key=base_order)
        for pos, group in enumerate(groups):
          grouped_perm[offset + pos] = group
        offset = len(base_ordered[i])

    if grouped_perm not in res:
      res.append(grouped_perm)
  return res

#==============================================================================#
#                             permute_lorentz_base                             #
#==============================================================================#

def permute_lorentz_base(lb, permutation):
  """
  Permutes a lorentz base according to `permutation`.

  Examples:

    g^12 * g^34
  Constructing the lorentz base
  >>> g = Symbol('Metric')
  >>> ls_key = 4*g*g
  >>> arg_index = lorentz_base_dict[ls_key].index([[1, 2], [3, 4]])
  >>> prefactor = 1
  >>> lb = [ls_key, (arg_index, prefactor)]

  First consider the permutation exchange the fist and second particle and the
  third and fourth.  According to the group symmetries nothing should change
  since g^\mu\nu is symmetric.
  >>> perm = [1, 0, 2, 3]
  >>> lb == permute_lorentz_base(lb, perm)
  True

  Exchanging the first with the third and the second with the fourth does
  neither give another base because the lorentz structures commute as they are
  only numbers. (g12*g34 = g34*g12)
  >>> perm = [2, 3, 0, 1]
  >>> lb == permute_lorentz_base(lb, perm)
  True

  Nontrivial permutation
  >>> perm = [2, 1, 0, 3]
  >>> lb == permute_lorentz_base(lb, perm)
  False
  """
  ls_key = lb[0]
  # no arguments in scalar case, thus no permutation
  if type(ls_key) is int:
    return lb

  arg_index, prefactor = lb[1]
  args = lorentz_base_dict[ls_key][arg_index]
  new_args = permute_particles(args, permutation)
  lbs, _ = get_lorentz_key_components(ls_key)
  lbs = order_strucs(lbs)
  representant = get_representative(lbs, new_args)
  from rept1l.baseutils import CurrentBase
  arg_index = CurrentBase.get_arg_id(ls_key, representant)
  return [ls_key, (arg_index, prefactor)]

def order_contraction_args(args):
  """ Given a list of args `args`, contraction integers are relabeled according
  to their occurence in `args`

  :param args: List of arguments.
  :type  args: list

  >>> order_contraction_args([[-2,1,3,-4],[-4,2,-2]])
  [[-1, 1, 3, -2], [-2, 2, -1]]
  """
  contr_map = {}
  for arg in args:
    for u in arg:
      if u < 0 and u not in contr_map:
        contr_map[u] = -len(contr_map) - 1

  if len(contr_map) == 0:
    return args
  else:
    new_args = []
    for arg in args:
      new_args.append([contr_map[u] if u in contr_map else u for u in arg])
    return new_args

#==============================================================================#
#                              get_representative                              #
#==============================================================================#

def get_representative(lbs, grouped_args):
  """
  Returns the base elements for a given product base. The product is represented
  as a list of the primitive structures, i.e. lorentz_base instances.
  Alternatively, for convenience it is possible to specify the lorentz base by
  its symbol.

  :param lbs: Product of primitive structures
  :type  lbs: List of lorentz_base or Symbol instances. If specified as symbol,
              the corresponding lorentz_base instances must already been
              initialized.

  :param grouped_args: list of arguments connected to the primitive structures,
                       where an argument is a list of integeres which length
                       must match the number of args of the primitive
                       structure.
  :type  grouped_args: list of list of integers


  examples:

  Identity

  >>> get_representative([GammaM], [[3,1,2]])
  [[3, 1, 2]]

  Permutation symmetry

  >>> get_representative([P,P,P], [[2,3],[1,2],[3,4]])
  [[1, 2], [2, 3], [3, 4]]

  is just the same as

  >>> get_representative([P_lb,P_lb,P_lb], [[2,3],[1,2],[3,4]])
  [[1, 2], [2, 3], [3, 4]]

  Wrong order and internal symmetry
  >>> get_representative([g, P], [[2, 1], [3, 1]])
  [[3, 1], [1, 2]]

  Epsilon asymmetry is not implemented
  >>> get_representative([P, Epsilon], [[-1, 1], [-1, 1, 3, 2]])
  [[-1, 1], [-1, 1, 3, 2]]

  Relabel contraction indices
  >>> get_representative([P, Epsilon], [[-3, 1], [-3, 1, 3, 2]])
  [[-1, 1], [-1, 1, 3, 2]]

  >>> get_representative([Epsilon, P], [[-3, 1, 3, 2], [-3, 1]])
  [[-1, 1], [-1, 1, 3, 2]]
  """
  try:
    assert(isinstance(lbs, list_type))
    try:
      symbols_given = all(isinstance(u, Symbol) for u in lbs)
      assert(symbols_given or
             all(isinstance(u, lorentz_base) for u in lbs))
      if symbols_given:
        known_ls_sym = [u.symbol for u in lorentz_base.bases]
        try:
          assert(all(v in known_ls_sym for v in lbs))
          lbs = [lorentz_base.bases[known_ls_sym.index(v)] for v in lbs]
        except:
          raise TypeError('Unknown symbols in lbs')
    except TypeError:
      raise
    except:
      raise TypeError('List entries must be either Symbol ' +
                      'or lorenz_base instances')
  except TypeError:
    raise
  except:
    raise TypeError('lbs not a list in get_representative')
  base_ordered = {}
  base_ordered_args = {}
  for i in range(lorentz_base.n_base):
    base_ordered[i] = []
    base_ordered_args[i] = []

  # retrieve the total number of arguments
  n_arg = 0
  for pos, base in enumerate(lbs):
    base_ordered[lorentz_base.order[base.symbol]].append(base)
    base_ordered_args[lorentz_base.order[base.symbol]].append(grouped_args[pos])
    n_arg += base.arg_range
  # The group is equivalent to the lorentz base, and we have groups = len(lbs).
  # Each group size is given by the number of arguments of that lorentz base
  group_sizes = []
  has_internal_sym = False
  for order in base_ordered:
    for base in base_ordered[order]:
      group_sizes.append(base.arg_range)

  base_ordered_expanded = [u for u in flatten(
                           [base_ordered[i] for i in range(len(base_ordered))])]

  grouped_args = []
  for order in base_ordered_args:
    for args in base_ordered_args[order]:
      grouped_args.append(args)

  ret = grouped_args[:]
  # internal symmetries
  for pos, group in enumerate(ret):
    if base_ordered_expanded[pos].arg_symmetry:
      representants = [(permute_spins(group, sym))
                       for sym in
                       base_ordered_expanded[pos].arg_symmetry] + [group]
      ordered_representants = [order_grouped_permutations(u)
                               for u in representants]
      choice = min(ordered_representants)
      choice = ordered_representants.index(choice)
      representant = representants[choice]
      ret[pos] = representant

  offset = 0
  # exploit symmetries due to products of same group members <->
  # interchangability
  for i in base_ordered:
    if base_ordered[i]:
      groups = ret[offset:offset+len(base_ordered[i])]

      base = sum(group_sizes)
      base_order = lambda x: order_expression(x, base, 1, neg_weight_zero=True)
      groups = sorted(groups, key=base_order)
      for pos, group in enumerate(groups):
        ret[offset + pos] = group
      offset = len(base_ordered[i])

  # make sure contraction args are in the correct order
  ret = order_contraction_args(ret)
  return ret

#==============================================================================#
#                              perform_base_contr                              #
#==============================================================================#

def perform_base_contr(base, base_strucs, args, contraction=-1):
  base_new = []
  for contr in m_of_n(args, 2):
    repl = {key: contraction for key in contr}
    for vec in base:

      # check if arguments are available (may already been contracted)
      # any checks if the index in contr is contained in the
      # elements of vec.
      # all then requires that all the indices in contr must be somewhere in vec
      indices_available = all(any(index in vec_comp for vec_comp in vec)
                              for index in contr)
      if not indices_available:
        continue

      # relate the argument to the lorentz struc
      struc_indices = [[index in vec_comp for vec_comp in vec].index(True)
                       for index in contr]
      # obtain the argument slot
      index_pos = [vec[struc_indices[pos]].index(index)
                   for pos, index in enumerate(contr)]

      # no selfcontraction inside one structure allowed
      if len(struc_indices) == len(set(struc_indices)):
        primary_base = base_strucs[struc_indices[0]]
        secondary_base = base_strucs[struc_indices[1]]

        # check that the first base can be contracted
        if primary_base in lorentz_base.contractions:
          prim_base_contr = lorentz_base.contractions[primary_base]

          # check if contraction with second base possible
          if secondary_base in prim_base_contr:
            index_contr = prim_base_contr[secondary_base]

            # check that the index type is a possible candidate for contraction
            if index_pos[0] in index_contr:
              # check that the index of the second base is in the possible
              # domain for contraction
              if index_pos[1] in index_contr[index_pos[0]]:
                new_vec = [[repl[v] if v in repl else v for v in u]
                           for u in vec]
                if new_vec not in base_new:
                  base_new.append(new_vec)
  return base_new

#==============================================================================#
#                             compute_product_base                             #
#==============================================================================#

def compute_product_base(lbs):
  # typecheck
  try:
    assert(all(isinstance(base, lorentz_base) for base in lbs))
  except:
   raise TypeError('compute_product_base requires list of lorentz_base' +
                   'instances. Given:' + str(type(expr)))

  particles = sum(u.arg_range for u in lbs)
  args = range(1, particles+1)

  base_id = fold(lambda x, y: x*y, [u.symbol for u in lbs], 1)
  open_args_pos = [u.open_args for u in lbs]
  non_scalars = sum(open_args_pos)
  base = composed_lorentz_base_brute(lbs)

  if base:
    if non_scalars <= lorentz_base.max_particles:
      lorentz_base.lorentz_base[particles*base_id] = base

    n_contractions = 0
    base_contr = base
    while non_scalars - 2*n_contractions >= 2:
      base_contr = perform_base_contr(base_contr, lbs, args,
                                      contraction=-(n_contractions + 1))
      if base_contr:
        if non_scalars <= lorentz_base.max_particles:
          if lorentz_base.lorentz_base[particles*base_id]:
            lorentz_base.lorentz_base[particles*base_id].extend(base_contr)
          else:
            lorentz_base.lorentz_base[particles*base_id] = (base_contr)

      # A contraction involves two indices. We filter the results and look there
      # are solutions for a less number of scalar particles. This is done by
      # reducing the number of arguments (args_new) and checking whether the
      # full set of arguments (args_new) is contained in a certain base element.
      for i in range(1, 3):
        args_new = args[:-i-2*n_contractions]
        base_contr_red = []
        for vec in base_contr:
          indices = [u for u in flatten(vec)]
          args_in_base = all(arg in indices for arg in args_new)
          if args_in_base:
            if vec not in base_contr_red:
              base_contr_red.append(vec)
        if base_contr_red:
          if non_scalars - i - 2*n_contractions <= lorentz_base.max_particles:
            lorentz_base.lorentz_base[(particles - i - 2*n_contractions) *
                                      base_id] = base_contr_red
      n_contractions += 1
    closed_args_n = particles - non_scalars
    sc_base = []
    #import gc
    #gc.disable()
    for vec in base:
      closed_args = [u for u in
                     flatten([[v for pos_arg, v in enumerate(elem)
                              if not pos_arg != open_args_pos[pos_struc]]
                              for pos_struc, elem in enumerate(vec)])]
      for arg_comb in pair_arguments([args]*closed_args_n, as_list=False):
        new_vec = [[arg_comb[closed_args.index(arg)] if arg in closed_args
                    else arg for arg in elem] for elem in vec]
        if new_vec not in sc_base:
          print("new_vec:", new_vec)
          sc_base.append(new_vec)
          sys.exit()
    #gc.enable()

    #for vec in base_contr:
      #closed_args = [u for u in
                     #flatten([[v for pos_arg, v in enumerate(elem)
                              #if not pos_arg != open_args_pos[pos_struc]]
                              #for pos_struc, elem in enumerate(vec)])]
      #for arg_comb in pair_arguments([args]*closed_args_n):
        #new_vec = [[arg_comb[closed_args.index(arg)] if arg in closed_args
                    #else arg for arg in elem ] for elem in vec]
        #if not new_vec in base_contr:
          #base_contr.append(new_vec)

base_strucs = [P_lb, P_lb, P_lb]

#print get_representative([P,P,P], [[2,3],[1,2],[3,4]])

#compute_product_base(base_strucs)

#dump( lorentz_base.lorentz_base, open(model_tmp_path+"lorentz_base.txt", "wb"))

#print "lorentz_base.lorentz_base:", lorentz_base.lorentz_base.values()
#if file_exists(model_tmp_path+"lorentz_base.txt"):
  #lorentz_base.lorentz_base = load(open(model_tmp_path +
                                        #"lorentz_base.txt", "rb"))

    #print "vec:", vec
    #print "new_vec:", new_vec
  #rem_args = [ item for i,item in enumerate(args) if i not in contr ]
  #print "rem_args:", rem_args

#print "base:", base
#base_new = perform_base_contr(base, base_strucs, args)
#print "base_new:", base_new
#base_new = perform_base_contr(base_new, base_strucs, args, -2)
#print "base_new:", base_new

#composed_lorentz_base_brute([g_lb, g_lb, g_lb, g_lb])
#t = composed_lorentz_base([P_lb, P_lb, P_lb, P_lb])
#t = composed_lorentz_base_brute([P_lb, P_lb, P_lb, P_lb])
#for tt in t:
  #print tt

#lorentz_base_structures = [P, g, GammaM, GammaP, Gamma, ProjM, ProjP]
lorentz_combine_base = [P*g, g*g, P]


lorentz_sympy_dict = dict()
lorentz_sympy_dict['P'] = P
lorentz_sympy_dict['Metric'] = g
lorentz_sympy_dict['GammaM'] = GammaM
lorentz_sympy_dict['GammaP'] = GammaP
lorentz_sympy_dict['Gamma'] = Gamma
lorentz_sympy_dict['ProjM'] = ProjM
lorentz_sympy_dict['ProjP'] = ProjP
lorentz_sympy_dict['Epsilon'] = Epsilon
lorentz_sympy_dict['Sigma'] = Sigma
lorentz_sympy_dict['Identity'] = Identity


lorentz_symmtries = dict()
lorentz_base_dict = dict()

lorentz_symmtries[P] = [[]]
lorentz_symmtries[ProjM] = [[]]
lorentz_symmtries[ProjP] = [[]]
lorentz_symmtries[Gamma] = [[]]
lorentz_symmtries[GammaM] = [[]]
lorentz_symmtries[GammaP] = [[]]
lorentz_symmtries[g] = [[1, 0]]
lorentz_symmtries[Epsilon] = [[]]


ggg_base = group_permutations(gen_permutations([0, 1, 2, 3, 4, 5]),
                              group_sizes=[2, 2, 2],
                              symmetries=[[[1, 0]], [[1, 0]], [[1, 0]]])

ggg_base = [u for u in remove_redundant_transf(ggg_base)]
sym = gen_permutations([0, 1, 2], no_identity=True)
ggg_base = interchange_transformation(ggg_base, sym)
ggg_base = [u for u in remove_redundant_transf(ggg_base)]

#print "ggg base:"
#for i in ggg_base:
  #print i
#print ""


#print "ggg group symmetries"
#print [u for u in construct_group_symmetries([2, 2, 2], range(3), [[ [1,0]
  #],[[1,0]],[[1,0]]])]

#===============#
#  scalar base  #
#===============#

scalar_base = []


#===================#
#  g base VVS VVSS  #
#===================#

ir3 = m_of_n(range(1, 4), 2)
ir4 = m_of_n(range(1, 5), 2)
g_base_3 = []
g_base_4 = []
for ir in ir3:
  tmp = group_permutations(gen_permutations([0, 1]),
                           group_sizes=[2],
                           symmetries=[[[1, 0]]], index_repl=ir)
  g_base_3.extend([u for u in remove_redundant_transf(tmp)])

for ir in ir4:
  tmp = group_permutations(gen_permutations([0, 1]),
                           group_sizes=[2],
                           symmetries=[[[1, 0]]], index_repl=ir)
  g_base_4.extend([u for u in remove_redundant_transf(tmp)])

#print "g base 3:"
#for i in g_base_3:
  #print i
#print ""

#print "g base 4:"
#for i in g_base_4:
  #print i
#print ""

#=================#
#  proj base FF   #
#=================#
particles = 2
indices = 2
index_range = range(1, particles + 1)
ir3 = m_of_n(index_range, indices)
proj_base_2 = []
proj_base_tmp = []
for ir in ir3:
  proj_base_tmp.extend(group_permutations(gen_permutations([0, 1]),
                       group_sizes=[2],
                       symmetries=[[[]]], index_repl=ir))


for i in proj_base_tmp:
  if i not in proj_base_2:
    proj_base_2.append(i)

#print "proj base:"
#for i in proj_base_2:
  #print i
#print ""


#=================#
#  proj base FFV  #
#=================#
particles = 3
indices = 2
index_range = range(1, particles + 1)
ir3 = m_of_n(index_range, indices)
proj_base_3 = []
proj_base_tmp = []
for ir in ir3:
  proj_base_tmp.extend(group_permutations(gen_permutations([0, 1]),
                       group_sizes=[2],
                       symmetries=[[[]]], index_repl=ir))


for i in proj_base_tmp:
  if i not in proj_base_3:
    proj_base_3.append(i)
#print "proj base:"
#for i in proj_base:
  #print i
#print ""


#=====================#
#  Epsilon base VVVV  #
#=====================#

Epsilon_base_4 = [[[1, 2, 3, 4]]]

#======================#
#  P Epsilon base VVV  #
#======================#

PEpsilon_base_3 = [[[-1, 1], [-1, 1, 2, 3]], [[-1, 3], [-1, 1, 2, 3]],
                   [[-1, 0], [-1, 1, 2, 3]], [[-1, 2], [-1, 1, 2, 3]]]

#=============#
#  g base VV  #
#=============#

g_base_2 = [[[1, 2]]]

#=================#
#  PP Base VV/SS  #
#=================#
# first index is lorentz index
# P1^\mu1 * P1^\mu2, p1.p1, P1**2
PP_base_2 = [[[1, 1], [2, 1]], [[1, 2], [2, 2]],
             [[-1, 1], [-1, 1]], [[-1, 2], [-1, 2]]]

#=============#
#  P base VS  #
#=============#

P_base_2 = [[[1, 1]], [[2, 2]], [[1, 2]], [[2, 1]], [[3, 1]], [[4, 1]]]

#===============#
#  PPg base VV  #
#===============#

PPg_base_2 = [[[-1, 1], [-1, 1], [1, 2]], [[-1, 2], [-1, 2], [1, 2]]]

#================#
#  gg base VVVV  #
#================#

#print "gg base"
ir = range(1, 5)
gg_base = group_permutations(gen_permutations([0, 1, 2, 3]),
                             group_sizes=[2, 2],
                             symmetries=[[[1, 0]], [[1, 0]]], index_repl=ir)

gg_base = [u for u in remove_redundant_transf(gg_base)]
gg_base = interchange_transformation(gg_base, [[1, 0]])
gg_base = [u for u in remove_redundant_transf(gg_base)]
#print "gg base:"
#for i in gg_base:
  #print i
#print ""


#===============#
#  Pg base VVV  #
#===============#

# 4 indices, but two are the same
indices = range(3)
index_repls = []
for i in indices:
  index_repl = range(1, 5)
  index_repl[-1] = i + 1
  index_repls.append(index_repl)

Pg_base_3 = []

for ir in index_repls:
  Pg_base_3 += group_permutations(gen_permutations([0, 1, 2, 3]),
                                  group_sizes=[2, 2], symmetries=[[], [[1, 0]]],
                                  index_repl=ir)

Pg_base_3 = [u for u in remove_redundant_transf(Pg_base_3)]
#print "Pg_base:", Pg_base

not_allowed = []
for i in indices:
  not_allowed.append([i + 1, i + 1])

for na in not_allowed:
  Pg_base_3 = list(filter(lambda x: na not in x, Pg_base_3))


Pg_base_3.append([[1, 1], [2, 3]])
Pg_base_3.append([[2, 2], [1, 3]])
Pg_base_3.append([[3, 3], [1, 2]])

#print "Pg base:"
#for i in Pg_base:
  #print i
#print ""

#================#
#  PP base VVVV  #
#================#

# 4 indices
indices = range(4)
#for i in indices:
index_repl = range(1, 5)

PP_base = []

PP_base = group_permutations(gen_permutations([0, 1, 2, 3]),
                             group_sizes=[2, 2], symmetries=[],
                             index_repl=index_repl)


PP_base = interchange_transformation(PP_base, [[1, 0]])
PP_base = [u for u in remove_redundant_transf(PP_base)]
#print "PP base:"
#for i in PP_base:
  #print i
#print ""

#for na in not_allowed:
  #Pg_base = filter(lambda x: not na in x, Pg_base)

#==================#
#  P base UUV/SSV  #
#==================#

particles = 3
indices = 2
index_range = range(1, particles + 1)
double_occurence = 1
index_repls = []
for i in index_range:
  tmp = range(1, particles + double_occurence + 1)
  tmp[-1] = i
  ir3 = m_of_n(tmp, indices)
  for ir in ir3:
    if ir not in index_repls:
      index_repls.append(ir)


P_base_3 = []
P_base_3_tmp = []
for ir in index_repls:
  P_base_3_tmp += group_permutations(gen_permutations([0, 1]),
                                     group_sizes=[2],
                                     symmetries=[[[]]], index_repl=ir)

for i in P_base_3_tmp:
  if i not in P_base_3:
      P_base_3.append(i)
# use his only if a symm. transf. has done! Otherwise the list structure is
# manipulated
# P_base_3 = [u for u in remove_redundant_transf(P_base_3)]

#print "P base:"
#for i in P_base_3:
  #print i
#print ""


#===================#
#  GammaM base FFV  #
#===================#
ir = range(1, 4)
Gamma_base_3 = group_permutations(gen_permutations([0, 1, 2]),
                                  group_sizes=[3],
                                  symmetries=[[[]]], index_repl=ir)

#====================#
#  P*Gamma base FF   #
#====================#

# TODO: nick added permutation which are in fact the same due to momentum
# conservation  Mo 27 Okt 2014 10:41:11 CET
PGamma_base_2 = [[[-1, 1], [-1, 1, 2]], [[-1, 1], [-1, 2, 1]],
                 [[-1, 2], [-1, 1, 2]], [[-1, 2], [-1, 2, 1]]
                 ]


#print "Gamma base:"
#for i in Gamma_base_3:
  #print i
#print ""

lorentz_base_dict[1] = [[]]
lorentz_base_dict[2] = [[]]
lorentz_base_dict[3] = [[]]
lorentz_base_dict[4] = [[]]
lorentz_base_dict[5] = [[]]
lorentz_base_dict[6] = [[]]
lorentz_base_dict[7] = [[]]
lorentz_base_dict[8] = [[]]
lorentz_base_dict[2*g] = g_base_2
lorentz_base_dict[3*g] = g_base_3
lorentz_base_dict[4*g] = g_base_4
lorentz_base_dict[4*g*g] = gg_base
lorentz_base_dict[3*scalar] = scalar_base
lorentz_base_dict[4*scalar] = scalar_base
lorentz_base_dict[3*P*g] = Pg_base_3
lorentz_base_dict[4*P*P] = PP_base
lorentz_base_dict[2*P*P] = PP_base_2
lorentz_base_dict[2*P*P*g] = PPg_base_2
lorentz_base_dict[2*P] = P_base_2
lorentz_base_dict[3*P] = P_base_3
lorentz_base_dict[2*P*Gamma] = PGamma_base_2
lorentz_base_dict[2*P*GammaM] = PGamma_base_2
lorentz_base_dict[2*P*GammaP] = PGamma_base_2
lorentz_base_dict[3*GammaM] = Gamma_base_3
lorentz_base_dict[3*GammaP] = Gamma_base_3
lorentz_base_dict[3*Gamma] = Gamma_base_3
lorentz_base_dict[2*ProjM] = proj_base_2
lorentz_base_dict[2*ProjP] = proj_base_2
lorentz_base_dict[3*ProjM] = proj_base_3
lorentz_base_dict[3*ProjP] = proj_base_3
lorentz_base_dict[4*Epsilon] = Epsilon_base_4
lorentz_base_dict[3*P*Epsilon] = PEpsilon_base_3

#------------------------------------------------------------------------------#

class LSBaseCacheMETA(StorageProperty):
  """ Meta storage for lorentz structures, providing getter and settter methods
  on the class itself. """
  def __contains__(cls, expr):
    return str(expr) in cls.registry

  def __getitem__(cls, expr):
    if expr in cls:
      return cls.registry[str(expr)]

  def __setitem__(cls, expr, value):
    # trigger that rept1l_model_path is set
    if expr in cls:
      pass
    cls.registry[str(expr)] = value

#------------------------------------------------------------------------------#

class LSBaseCache(with_metaclass(LSBaseCacheMETA)):
  """ Class which caches the elements of the lorentz basis. The results are
  accessed via `LSBaseCache[ls_key]`.
  """
  storage = ['registry']
  path = os.path.join(os.environ['REPT1L_CONFIG'], 'CachedStructures')
  name = 'cached_structures.dat'

  def __init__(self, *args, **kwargs):
    super(LSBaseCache, self).__init__(*args, **kwargs)

  @classmethod
  def add_item(cls, ls_key, arg_repr):
    if ls_key not in cls:
      cls.registry[str(ls_key)] = []

    if arg_repr not in cls.registry[str(ls_key)]:
      cls.registry[str(ls_key)].append(arg_repr)

# make sure that all lornetz bases are in LSBaseCache
for ls_key in lorentz_base_dict:
  if ls_key not in LSBaseCache:
    LSBaseCache[ls_key] = lorentz_base_dict[ls_key]

# add additional lorentz bases
for ls_key in LSBaseCache.registry:
  # Identity is reserved by Sympy
  if 'Identity' in ls_key:
    ls_key_tmp = sympify(ls_key.replace('Identity', 'Idd')).subs({Symbol('Idd'): Symbol('Identity')})
    lorentz_base_dict[ls_key_tmp] = LSBaseCache[ls_key]
  else:
    lorentz_base_dict[sympify(ls_key)] = LSBaseCache[ls_key]

#==============================================================================#
#                          get_lorentz_key_components                          #
#==============================================================================#

def get_lorentz_key_components(ls_key):
  """
  Returns the primitive structures of a product base as a list and the number of
  external particles (prefactor).

  :param ls_key: lorentz_base_dict key
  :type  ls_key: sympy.core.mul.Mul

  >>> get_lorentz_key_components(3*P*P*P)
  ([P, P, P], 3)
  >>> get_lorentz_key_components(4*g*g)
  ([Metric, Metric], 4)
  """
  lorentz_base_structures = list(lorentz_base.order.keys())
  ls_struc = Poly(ls_key, lorentz_base_structures).as_dict()
  keys = list(ls_struc.keys())
  struc = []
  for key in keys:
    tmp = []
    for i in xrange(len(key)):
      if (key[i] > 0):
        tmp.extend([lorentz_base_structures[i]]*key[i])
    struc.extend(tmp)
    particles = ls_struc[key]

    return struc, particles

#------------------------------------------------------------------------------#

def merge_structures(ls_key1, arg1, ls_key2, arg2, index_combination={}):
  """
  Computes the product of two lorentz structures.

  :ls_struc1: First lorentz structure
  :ls_struc2: Second lorentz structure
  :index_combination: Specify the contraction. keys are referred to ls_struc1
                      and values are referred to ls_struc2.


  >>> prefactor = 1
  >>> base_id = lorentz_base_dict[3*Symbol('P')*Symbol('Metric')].index([[3, 1],
  ...                                                                   [1, 2]])
  >>> ls_struc = [3*Symbol('P')*Symbol('Metric'), (base_id, prefactor)]
  >>> ls_key, _ = get_lorentz_key_components(ls_struc[0])
  >>> ls_key = order_strucs(ls_key)
  >>> arg = lorentz_base_dict[ls_struc[0]][ls_struc[1][0]]
  >>> merge_structures(ls_key, arg, ls_key, arg, index_combination={3:3})
  ([P, P, Metric, Metric], [[-1, 1], [-1, 1], [1, 2], [1, 2]])
  """

  if len(index_combination) > 0:
    contraction_index = min([u for u in flatten(arg1 + arg2)]) - 1
    if contraction_index != 0:
      contraction_index += 1

    def contr(i=contraction_index):
      while True:
        i -= 1
        yield i

    get_contr = contr()
    for left in index_combination:
      right = index_combination[left]
      c_i = next(get_contr)

      # todo: could make the contraction even more safe. check if the arg
      # types of arg1 and arg2 match
      arg1_new = []
      for pos, arg in enumerate(arg1):
        arg_types = lorentz_base.get_base(ls_key1[pos]).arg_types
        contr_allowed = [True if lorentz_base.args[arg_type] is not None
                         else False for arg_type in arg_types]
        arg_new = [c_i if (u == left and contr_allowed[i]) else u
                   for i, u in enumerate(arg)]
        arg1_new.append(arg_new)

      arg2_new = []
      for pos, arg in enumerate(arg2):
        arg_types = lorentz_base.get_base(ls_key2[pos]).arg_types
        contr_allowed = [True if lorentz_base.args[arg_type] is not None
                         else False for arg_type in arg_types]
        arg_new = [c_i if (u == right and contr_allowed[i]) else u
                   for i, u in enumerate(arg)]
        arg2_new.append(arg_new)
      arg1 = arg1_new
      arg2 = arg2_new
    ls_struc = order_strucs(ls_key1 + ls_key2)
    representative = get_representative(ls_key1 + ls_key2, arg1 + arg2)
    return ls_struc, representative

#==============================================================================#
#                             reconstruct_lorentz                              #
#==============================================================================#

#------------------------------------------------------------------------------#
def reconstruct_lorentz(lorentz_keys, arguments, prefactors):
  """
  reconstruct_lorentz turns base elements back to lorentz structure strings

  lorentz_keys: Different lorentz_base keys, e.g.
                lorentz_keys = [3*GammaM, 3*GammaP]
  arguments:    lorentz_base values ids. Given arguments[i][j][k]
                i identifies the lorentz_base key, e.g.
                    [ 3*GammaM, 3*GammaP]
                i =       1         2
                j identifies a given set of arguments for which the lorentz_keys
                are additively connected, e.g.
                    g(1,2)*g(3,4) + g(1,3)*g(2,4) corresponding to [4*g*g]
                j =       1               2
                k subdivides the arguments for multiplicatively connected
                lorentz_keys, e.g.
                    g(1,2)*g(3,4) + g(1,3)*g(2,4)
                k =    1      2        1      2

  Scalar Scalar counterterm
  >>> ls_keys = [2, 2*P**2]
  >>> arguments = [[[]], [[[-1, 1], [-1, 1]]]]
  >>> prefactors = [[1], [1]]
  >>> strucs, particles = reconstruct_lorentz(ls_keys, arguments, prefactors)
  >>> strucs
  [['1'], ['P(-1,1)*P(-1,1)']]
  >>> particles
  2

  Vector Vector counterterm
  >>> ls_keys = [2*P**2, 2*P**2*g]
  >>> arguments = [[[[1, 1], [2, 1]]], [[[-1, 1], [-1, 1], [1, 2]]]]
  >>> prefactors = [[-1], [1]]
  >>> strucs, particles = reconstruct_lorentz(ls_keys, arguments, prefactors)
  >>> strucs
  [['- P(1,1)*P(2,1)'], ['P(-1,1)*P(-1,1)*Metric(1,2)']]
  >>> particles
  2

  >>> ls_keys = [3*Epsilon*P]
  >>> arguments = [[[[-1, 3], [-1, 1, 2, 3]]]]
  >>> prefactors =[[1]]
  >>> strucs, particles = reconstruct_lorentz(ls_keys, arguments, prefactors)
  >>> strucs
  [['P(-1,3)*Epsilon(-1,1,2,3)']]
  >>> particles
  3
  """
  ret = []
  particles = 0
  for j in range(len(lorentz_keys)):
    lorentz_key = lorentz_keys[j]
    struc, particles = get_lorentz_key_components(lorentz_key)
    lorentz_sympy_dict_inv = {lorentz_sympy_dict[v]: v
                              for v in lorentz_sympy_dict}
    struc = [lorentz_sympy_dict_inv[u] for u in order_strucs(struc)]
    struc_add = []
    if arguments is not None:
      for k in range(len(arguments[j])):
        args = arguments[j][k]
        struc_product = ''
        args_nonzero = [u for u in flatten(args)]

        # capture scalar case
        if args == []:
          struc_add.append('1')
          continue
        for i in range(len(args)):
          arg = args[i]
          arg_tmp = '(' + fold(lambda x, y: str(x) + ',' + str(y),
                               arg[1:len(arg)], arg[0]) + ')'
          struc_product += struc[i] + arg_tmp + '*'

        tmp = Rational(prefactors[j][k]).as_numer_denom()
        # case without divisor
        if (tmp[1] == 1):
          if tmp[0] == 1:
            struc_product = struc_product[:-1]
          elif tmp[0] == -1:
            struc_product = '- ' + struc_product[:-1]
          else:
            struc_product = str(tmp[0]) + '*' + struc_product[:-1]
        # case with divisor
        else:
          if tmp[0] == -1:
            struc_product = ('- ' + struc_product[:-1] + '/' + str(tmp[1]))
          else:
            struc_product = (str(tmp[0]) + '*' + struc_product[:-1] +
                             '/' + str(tmp[1]))

        struc_add.append(struc_product)

      ret.append(struc_add)
    else:
      ret = [['1']]

  return ret, particles
#------------------------------------------------------------------------------#

#==============================================================================#
#                        reconstruct_lorentz_from_base                         #
#==============================================================================#

#------------------------------------------------------------------------------#
def reconstruct_lorentz_from_base(ls_struc):
  """
  In contrast to reconstruct_lorentz the input does not need to be sorted
  according to unique ls_strucs which is done here.

  ls_struc: return of get_lorentz_base_structure
  returns:
  """
  strucs = [ls_struc[u][0] for u in range(len(ls_struc))]
  base_ids = [ls_struc[u][1][0] for u in range(len(ls_struc))]
  prefactors = [ls_struc[u][1][1] for u in range(len(ls_struc))]
  args = [lorentz_base_dict[x[0]][x[1]] for x in zip(strucs, base_ids)]
  # before reconstructing, different ls must be separated without having
  # multiple times the same ls

  strucs_dict = dict()

  for x in zip(strucs, args, prefactors):
    if not x[0] in strucs_dict.keys():
      strucs_dict[x[0]] = [(x[1], x[2])]
    else:
      tmp = strucs_dict[x[0]]
      tmp.append((x[1], x[2]))
      strucs_dict[x[0]] = tmp

  strucs_sorted = strucs_dict.keys()
  args_sorted = []
  prefactors_sorted = []

  for key in strucs_sorted:
    tmp = strucs_dict[key]
    arg = [u[0] for u in tmp]
    pref = [u[1] for u in tmp]
    args_sorted.append(arg)
    prefactors_sorted.append(pref)

  ls_strucs, particles = reconstruct_lorentz(strucs_sorted, args_sorted,
                                             prefactors_sorted)

  return ls_strucs, particles
#------------------------------------------------------------------------------#

#max_rank = 4
#rank = 0
#print binomialCoeff(max_rank, rank)
#print free_indices[:max_rank]
#print m_of_n(free_indices[:max_rank], rank)


#==============================================================================#
#                            insert_partition_index                            #
#==============================================================================#

#------------------------------------------------------------------------------#
def insert_partition_index(index, list_ordered_lists):
  """
  index: can be anything
  list_ordered_lists: list of tuples where the first element of the tuple is a
                      list and the second element is an integer

                     [ ([a,b,c], i), .... ]
  """

  # return array - to-be filled with tuples
  ret = []
  for k in range(len(list_ordered_lists)):

    # for every tuple in list_ordered_lists there is a bound up to which the
    # index is allowed to be inserted
    bound = list_ordered_lists[k][1]
    l = list_ordered_lists[k][0]
    for j in range(bound + 1):
      # A new list is created and the bound is updated to the insertion position
      # of the index
      ret.append((l[0:j] + [index] + l[j:], j))

  return ret
#------------------------------------------------------------------------------#

#==============================================================================#
#                           combine_partition_groups                           #
#==============================================================================#

#------------------------------------------------------------------------------#
def combine_partition_groups(list_ordered, list_ordered_lists):
  """
  merging and creating new lists out of list_ordered and list_ordered_lists

  list_ordered : [a, b, c, ...]
  list_ordered_lists: list of tuples where the first element of the tuple is a
                      list and the second element is an integer

                     [ ([a,b,c, ...], i), .... ]
  """
  ret = list_ordered_lists[:]
  # looping inversely starting with the last element
  for k in range(len(list_ordered) - 1, -1, -1):
    ret = insert_partition_index(list_ordered[k], ret)

  return ret
#------------------------------------------------------------------------------#


#==============================================================================#
#                            get_tensor_summations                             #
#==============================================================================#

#------------------------------------------------------------------------------#
def get_tensor_summations(rank):
  """  Derives summation bounds and symmetry factors for computing the increase
  of rank of tensor integrals

  Returns a list of tuples. The first element is a partition of rank in form of
  a list. The second element is the symmetry factor. The third element is a list
  of list where every list has the length of the partition and the entries are
  free_indices.

  >>> get_tensor_summations(0)
  [([], 1, [])]

  >>> get_tensor_summations(1)
  [([1], 1, [['mu']])]

  >>> get_tensor_summations(2)
  [([1, 1], 2, [['mu', 'nu']]), ([2], 1, [['mu']])]
  """
  index_sums = []
  for partition in get_partitions(rank):
    indices = []
    for k in range(len(partition)):
      for j in range(partition[k]):
        indices.append(free_indices[k])

    groups = dict()
    for index in free_indices:
      n = indices.count(index)
      if (n > 0):
        if n not in groups.keys():
          groups[n] = []

        if index not in groups[n]:
          groups[n].append(index)

    ret = []
    for key in groups.keys():
      ret.append([p for p in groups[key]])

    index_sum = []
    if len(ret) > 1:
      last_group = [(ret[-1], len(ret[-1]))]
      for k in range(len(ret) - 2, -1, -1):
        last_group = combine_partition_groups(ret[k], last_group)
      index_sum.extend([p[0] for p in last_group])
    else:
      index_sum.extend(ret)
    tmp = factorial(rank)
    for j in partition:
      tmp //= factorial(j)

    index_sums.append((partition, tmp, index_sum))

  return index_sums
#------------------------------------------------------------------------------#


#==============================================================================#
#                           construct_tensor_pyfort                            #
#==============================================================================#

#------------------------------------------------------------------------------#
def construct_tensor_pyfort(max_rank, indentation=0):
  """@todo: Docstring for construct_tensor_pyfort.

  :arg1: @todo
  :returns: @todo

  """
  if max_rank < 2:
    return None, None
  tensor_summations = get_tensor_summations(max_rank-1)
  tensor_contraction = PyFort("test")
  sums = tensor_summations[0][0]
  indices = ['3'] + free_indices[:max_rank-1]
  do_loops = []
  factors = dict()

  for k in range(len(sums)):
    tmp = PyFort(header='', basetype="do",
                 arg=indices[k + 1] + " = 0, " + indices[k],
                 s_tabs=indentation)
    tmp2 = PyFort(header='', basetype=None, arg='', s_tabs=indentation)
    indentation += 1
    do_loops.append([tmp, tmp2])

  do_loops[0][0].statements.append(do_loops[0][1])
  for j in range(1, len(do_loops)):
    do_loops[j-1][0].statements.append(do_loops[j][0])
    do_loops[j][0].statements.append(do_loops[j][1])

  for u in tensor_summations:
    sums = u[0]
    factor = u[1]
    indices = u[2]

    for j in range(len(indices)):
      prev_ri = 'j'
      for k in range(len(sums)):
        indentation += 1
        for p in range(sums[k]):
          prev_ri = "incRI(" + indices[j][k] + ", " + prev_ri + ")"

      first_ri = "firstRI" + prev_ri[5:]
      do_loops[len(sums) - 1][0].addStatement("riOut = " + prev_ri, 1)
      factors[len(sums) - 1] = factor
      do_loops[len(sums) - 1][0].addLine()
      do_loops[len(sums) - 1].append(first_ri)

  return do_loops, factors
#------------------------------------------------------------------------------#

if __name__ == "__main__":
  import doctest
  doctest.testmod()

  #Sigma = Symbol('Sigma')
  #ls_key = [Sigma, Sigma]
  #args = [[-1, -2, 2, 4], [-2, -1, 3, 1]]
  #new_args = get_representative(ls_key, args)
  #print("new_args:", new_args)
  #args = [[-1, -2, 3, 1], [-2, -1, 2, 4]]
  #new_args = get_representative(ls_key, args)
  #print("new_args:", new_args)

  #ls_keys = [3*Epsilon*P]
  #arguments = [[[[-1, 3], [-1, 1, 2, 3]]]]
  #prefactors =[[1]]
  #strucs, particles = reconstruct_lorentz(ls_keys, arguments, prefactors)

  #g = Symbol('g')
  #lb = [4*g*g, (0, 1)]
  #perm = [1, 0, 2, 3]
  #print permute_lorentz_base(lb, perm)
  #get_representative([P, Epsilon], [[-1, 1], [-1, 1, 3, 2]])

  #g = Symbol('g')
  #ls_key = 4*g*g
  #arg_index = lorentz_base_dict[ls_key].index([[1, 2], [3, 4]])
  #prefactor = 1
  #lb = [ls_key, (arg_index, prefactor)]

  #perm = [1, 0, 2, 3]
  #print "lb:", lb
  #lb == permute_lorentz_base(lb, perm)

  #prefactors = [[1], [1], [1], [1]]
  #ls_strucs = [2*Symbol('GammaP')*Symbol('P'), 2*Symbol('GammaM')*Symbol('P'),
               #2*Symbol('ProjP'), 2*Symbol('ProjM')]
  #arguments = [[[[-1, 2], [-1, 1, 2]]], [[[-1, 2], [-1, 1, 2]]], [[[1, 2]]],
               #[[[1, 2]]]]

  #strucs, particles = reconstruct_lorentz(ls_strucs, arguments, prefactors)

  #print "strucs:", strucs

  #perm = [2, 3, 0, 1]
  #lb == permute_lorentz_base(lb, perm)

  #perm = [2, 1, 0, 3]
  #lb == permute_lorentz_base(lb, perm)

  #pkey = ['S', 'S', 'V1', 'V2']
  #struc_args = [[3, 4]]
  #g = Symbol('g')
  #ls_key = 4*g
  #arg_index = lorentz_base_dict[ls_key].index(struc_args)
  #prefactor = 1
  #lb = [ls_key, (arg_index, prefactor)]

  #for s_perm in get_permutations_from_spins(pkey):
  ##permutation = [0, 2, 1]
    #new_pkey = tuple(permute_spins(pkey, s_perm))
    #print "new_pkey:", new_pkey
    #print permute_particles(struc_args, s_perm)
    #new_lb = permute_lorentz_base(lb, s_perm)
    #new_args = lorentz_base_dict[ls_key][new_lb[1][0]]
    #print "new_args:", new_args
  #g = Symbol('g')
  #ls_key = 4*g*g
  #arg_index = lorentz_base_dict[ls_key].index([[1, 2], [3, 4]])
  #prefactor = 1
  #lb = [ls_key, (arg_index, prefactor)]
  #perm = [1, 0, 2, 3]
  #lb == permute_lorentz_base(lb, perm)
  #print("lb:", lb)
  #prefactor = 1
  #base_id = lorentz_base_dict[3*Symbol('P')*Symbol('g')].index([[3, 1],
                                                                #[1, 2]])
  #ls_struc = [3*Symbol('P')*Symbol('g'), (base_id, prefactor)]
  #ls_key, _ = get_lorentz_key_components(ls_struc[0])
  #ls_key = order_strucs(ls_key)
  #arg = lorentz_base_dict[ls_struc[0]][ls_struc[1][0]]
  #print("ls_key:", ls_key)
  #print("arg:", arg)
  #merge_structures(ls_key, arg, ls_key, arg, index_combination={3:3})

  #n_g = 2
  #n_p = 1
  #irs = m_of_n(range(1, 2*n_g+n_p+1), 2*n_g)
  #g_base = [Symbol('Metric')]*n_g
  #perms = gen_permutations(range(2*n_g))
  #g_args_sum = []
  #for ir in irs:
    #g_args_s = set()
    #for perm in perms:
      #irp = permute_spins(ir, perm)
      #g_args = [(irp[i], irp[i+1]) for i in range(0, len(irp), 2)]
      #r = tuple(tuple(u) for u in get_representative(g_base, g_args))
      #g_args_s.add(r)
    #p_args = [v for v in range(1, 2*n_g+n_p+1) if v not in ir]
    #g_args_sum.append({'Metrics': g_args_s, 'Momenta': p_args})
  #print("g_args_sum:", g_args_sum)

  #doloops, factors = construct_tensor_pyfort(3)
  #print("factors:", factors)
  #print("doloops:", doloops)
  #doloop1 = doloops[0][0]
  #print("doloop1:", doloop1)
  #doloop1.printStatment()
  #doloop2 = doloops[0][1]
  #print("doloop2:", doloop2)
  #doloop2.printStatment()
  #print("doloop1:", doloop1)
  #print("doloop1:", doloop1)
