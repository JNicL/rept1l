#==============================================================================#
#                           renormalize_template.py                            #
#==============================================================================#

""" A template class for the renormalization procedure. """

#===========#
#  Imports  #
#===========#

from sympy import Symbol

from rept1l.logging_setup import log
from rept1l.combinatorics import flatten
from rept1l.parsing import suppress_stdout_stderr, FormProcess
from .renormalize_utils import solve_renormalization
from rept1l.helper_lib import find_vertex

import rept1l.Model as Model
model = Model.model

#=============================#
#  class RenormalizeTemplate  #
#=============================#

class RenormalizeTemplate(object):

  optional_args = {'renoscheme': ''}
  schemes = {}

  def __init__(self, **kwargs):
    self.set_optional_args(**kwargs)

  def set_optional_args(self, **kwargs):
    """ Sets optional arguments as attributes. """

    for o_arg in self.optional_args:
      if o_arg in kwargs:
        setattr(self, o_arg, kwargs[o_arg])
      else:
        setattr(self, o_arg, self.optional_args[o_arg])

    # optional arguments which are forwarded to Regularize
    self.reg_kwargs = {u: kwargs[u] for u in kwargs
                       if u not in self.optional_args}

  def set_process_defs(self, particle, *particles, **kwargs):
    """ Constructs the process definition passed to FormProcess. """

    logmsg = {1: 'tadpole', 2: 'selfenergy'}
    self.npoint = 1 + len(particles)
    self.particles = [particle] + list(particles)
    if self.npoint in logmsg:
      logmsg = logmsg[self.npoint]
    else:
      logmsg = str(self.npoint) + '-Vertex'

    log('Computing ' + logmsg + ' for ' +
        ','.join(u.name for u in self.particles), 'info')

    if self.npoint == 1:
      self.particles += [particle]
    elif self.npoint == 2:
      self.mixing = self.particles[-1] != self.particles[0]
    else:
      self.mixing = False

    self.force_massless = ('force_massless' in kwargs and
                           kwargs['force_massless'])

    process_defs = {'masscut_frm': False,
                    'masscut': 0.}  # masscut to zero
    if self.npoint == 1:
      process_defs['compute_tadpole'] = True

    if 'select_power_LO' in kwargs:
      process_defs['select_power_LO'] = kwargs['select_power_LO']
    if 'unselect_power_LO' in kwargs:
      process_defs['unselect_power_LO'] = kwargs['unselect_power_LO']

    if 'select_power_NLO' in kwargs:
      process_defs['select_power_NLO'] = kwargs['select_power_NLO']
    if 'unselect_power_NLO' in kwargs:
      process_defs['unselect_power_NLO'] = kwargs['unselect_power_NLO']

    if 'cutparticle' in kwargs:
      process_defs['cutparticle'] = kwargs['cutparticle']

    self.process_defs = process_defs

    # Mass regularization -> Particle is considered massless and a small mass
    # is used as regulator. The flag only invokes a check if the limit of
    # vanishing mass exists, i.e. if no mass counterterm is needed in the
    # limit m -> 0. Otherwise the particle cannot be considered massless.
    if self.npoint == 2:
      if(hasattr(self.particles[-1], 'light_particle') and
         self.particles[-1].light_particle and self.mass_reg):
        self.reg_kwargs['mass_reg'] = True
      else:
        self.reg_kwargs['mass_reg'] = False

  def set_regularization_defs(self):
    """ Constructs additional optional arguments passed to the regularization
    method. """

    if 'mass' in self.reg_kwargs and self.reg_kwargs['mass'] != 'M0':
      particle_mass_zero = False
    else:
      particle_mass_zero = (self.particles[-1].mass == model.param.ZERO or
                            self.particles[-1].mass.value == 0)

    if(self.npoint == 2 and self.reg_kwargs['mass_reg'] and
       particle_mass_zero and not self.force_massless):
       raise ValueError('Particle  `' + self.particles[-1].name +
                        '`must have a nonzero mass to be mass regularized.')

    # checks if user input is correct
    scheme_exists = self.renoscheme in self.schemes
    if not scheme_exists:
      raise Exception('Scheme ' + str(self.renoscheme) +
                      ' is not implemented')

    scheme_choice = self.schemes[self.renoscheme]
    if type(scheme_choice) is list:
      self.particle_scheme = scheme_choice[0]
      if len(scheme_choice) == 2:
        self.reg_kwargs.update(scheme_choice[1])
    else:
      if self.particles[0].spin not in scheme_choice:
        raise Exception('Scheme ' + str(self.renoscheme) +
                        ' does not support particle spin ' +
                        str(self.particles[0].spin))

      self.particle_scheme = scheme_choice[self.particles[-1].spin]
      scheme_mass_dep = type(self.particle_scheme) is dict
      if scheme_mass_dep and particle_mass_zero not in self.particle_scheme:

        mass_case = 'massless' if particle_mass_zero else 'massive'
        raise Exception('Scheme ' + str(self.renoscheme) +
                        ' does not support particle mass case: ' + mass_case)

      # do not use massless regularization methods when computing mixed
      # selfenergies. massless checks that the selfenergie is proportional to
      # p^2
      if scheme_mass_dep and not self.mixing:
        self.particle_scheme = self.particle_scheme[particle_mass_zero]
      elif scheme_mass_dep and self.mixing:
        self.particle_scheme = self.particle_scheme[self.massive]

      # apply renoscheme
      if 'kwargs' in scheme_choice:
        self.reg_kwargs.update(scheme_choice['kwargs'])

    # overwrite mass in `p^2 = mass^2` if set by user
    if 'mass' not in self.reg_kwargs:
      mass = 'M0' if particle_mass_zero else self.particles[-1].mass.name
      self.reg_kwargs['mass'] = mass

    self.unstable = ((not particle_mass_zero) and
                     (self.particles[-1].width != model.param.ZERO))
    # if the particle is unstable renormalize the selfenergy at p^2 =
    # real(M)^2 which corresponds to the approximate CMS.
    if self.npoint == 2 and self.unstable and self.realmomentum:
      self.reg_kwargs['realmomentum'] = True
    else:
      self.reg_kwargs['realmomentum'] = False

    if self.npoint == 1:
      p_key_str = [self.particles[0].name]*3
      V = find_vertex(p_key_str)[0]
      C = list(V.couplings.values())[0]
      self.reg_kwargs['prop_coupling'] = Symbol(C.name)

    debug_msg = ('Starting regularization in ' + self.particle_scheme +
                 ' with optional arguments: ' + str(self.reg_kwargs))
    log(debug_msg, 'debug')

  def regularize(self):
    """ Perform regularization of the vertex function. """
    if hasattr(self, 'vertex'):
      FP_args = {'vertex': self.vertex}
    else:
      FP_args = {'particles': self.particles}

    if hasattr(self, 'cutparticle'):
      FP_args['cutparticle'] = self.cutparticle

    FP_args.update(self.process_defs)

    if not self.npoint == 1:
      FP_args['vertex_functions'] = True
    with FormProcess(**FP_args) as processed_file:
      self.processed_file = processed_file

      # load treevertices. Needed to assign the couplings.
      from rept1l.vertices_rc import Vertex
      Vertex.load()

      # parse and regularize the amplitudes
      from rept1l.regularize import Regularize as RR
      self.Reg = RR(Vertex.treevertices, Vertex.ctvertices)

      RR.__dict__[self.particle_scheme](self.Reg, self.processed_file,
                                        **self.reg_kwargs)
      msg = 'Regularization finished'
      log(msg, 'info')

  def setup_renormalization(self):
    """ Prepares the renormalization conditions to get solved. Substitutes for
    coupling expressions, revealing the counterterm dependence. """

    # find all the counterterm couplings, i.e. couplings containing one or
    # more counterterm parameters in renos
    # counterterm couplings are replaced by explicit expressions:
    # GC_xxx -> pi*e*dZe
    from rept1l.coupling import RCoupling
    # get all the couplings in the renormalization conditions
    c_set = [v for v in flatten([RCoupling.get_couplings_from_expr(u)
                                for u in self.Reg.renorm_conditions.values()])]
    # find those which contain counterterm parameter
    from rept1l.counterterms import Counterterms
    all_ct = [u for u in Counterterms.get_all_ct()]
    coupl_repl = {c: RCoupling.find_expression(all_ct, c)
                  for c in c_set}

    # substitute derived ct parameters
    ct_deriv_repl = {}
    ct_derv = Counterterms.get_derived_ct()
    for c in coupl_repl:
      for s in coupl_repl[c].free_symbols:
        if (s not in ct_deriv_repl) and (s in ct_derv):
          ct_deriv_repl[s] = Counterterms.derived_ct_values[s.name]

    from rept1l.particles import ParticleMass
    ParticleMass.load()
    pmasses = ParticleMass.masses.values()
    # substitute real mass expressions by complex ones
    mass_repl = {Symbol(u.name): Symbol('c' + u.name)
                 for u in pmasses}
    mass_repl.update({Symbol(u.name + '2'): Symbol('c' + u.name)**2
                      for u in pmasses})
    mass_repl.update({Symbol('c' + u.name + '2'): Symbol('c' + u.name)**2
                      for u in pmasses})

    # perform coupling and mass substitutions
    self.renorm_conditions = [u.subs(coupl_repl).subs(ct_deriv_repl).subs(mass_repl)
                              for u in self.Reg.renorm_conditions.values()]
    ct_renorm = set(list(flatten([[v for v in u.free_symbols if v in all_ct]
                                  for u in self.renorm_conditions])))

    if len(ct_renorm) > 0:
      msg = ('CT expressions in renormalization conditions: ' +
             ','.join(str(u) for u in ct_renorm))
      log(msg, 'info')

    self.ct_renorm = ct_renorm

  def solve_renormalization(self):
    """ Solves the renormalization conditions in terms of the ct parameters. """
    solutions = solve_renormalization(self.renorm_conditions, self.renos)
    nreno = str(len(self.renorm_conditions))
    msg = ('Attempt to solve ' + nreno + ' renormalization conditions for: '
           + ','.join(str(u) for u in self.renos))
    log(msg, 'info')
    if len(solutions) == len(self.renos):
      msg = 'Found solution.'
      log(msg, 'info')
      self.status = 'success'
      self.solutions = solutions
    else:
      log('Solution not found.', 'warning')
      msg = 'renorm_conditions:' + str(self.renorm_conditions)
      log(msg, 'debug')
      msg = 'len(renorm_conditions):' + str(len(self.renorm_conditions))
      log(msg, 'debug')
      log('Renos:' + str(self.renos), 'debug')
      self.status = 'failed'


