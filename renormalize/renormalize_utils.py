#==============================================================================#
#                             renormalize_utils.py                             #
#==============================================================================#

""" Utility methods used during renormalization """

#============#
#  Includes  #
#============#

from sympy import Symbol, Poly, simplify, Matrix, factor
from sympy.solvers.solvers import solve_linear_system
from sympy.solvers import solve
from rept1l.logging_setup import log
from rept1l.helper_lib import export_fortran

#===========#
#  Methods  #
#===========#

def new_coupling(expr, base=[], flag=''):
  """ Defines a new coupling from the expression `expr`. If base elements are
  provided only the coefficients of the base elements are defined as new
  couplings. """
  from rept1l.coupling import RCoupling
  if len(base) > 0:
    pexpr = Poly(expr, base).as_dict()
    for k, val in pexpr.iteritems():
      c = RCoupling(val, flag, subs_cvalues=False,
                    check_coupling=False).return_coupling()
      pexpr[k] = c
    return Poly(pexpr, base).as_expr()
  else:
    return RCoupling(expr, flag, subs_cvalues=False,
                     check_coupling=False).return_coupling()

#------------------------------------------------------------------------------#

def export_expression(expr, expand=True, plotProgress=True, external=True):
  """ Writes the expression `expr` in terms of external parameter. """
  from sympy import together, sympify
  from rept1l.coupling import RCoupling as FRC

  from rept1l.particles import ParticleMass
  ParticleMass.load()
  mass_repl = {Symbol(u.name): Symbol('c' + u.name)
               for u in ParticleMass.masses.values()}

  mass_squared = {Symbol('c' + u.name + '2'): Symbol('c' + u.name)**2
                  for u in ParticleMass.masses.values()}

  ret = 0
  if expand and not (expr.is_Symbol or expr.is_number or expr.expand().is_Mul):
    expr_args = expr.expand().args
    from rept1l.pyfort import ProgressBar
    if plotProgress:
      PG = ProgressBar(len(expr_args))
    for i, arg in enumerate(expr_args):
      if plotProgress:
        PG.advanceAndPlot(status='simplifying')
      arg_red = FRC.reduce_to_parameter(arg, external=external,
                                        replace_yukawa=True,
                                        zero_tadpole=True,
                                        complex_mass=True,
                                        is_expr=True)
      r = simplify(arg_red)
      ret += r
  else:
    ret = FRC.reduce_to_parameter(expr, external=external,
                                  replace_yukawa=True,
                                  zero_tadpole=True,
                                  complex_mass=True,
                                  is_expr=True)

  ret = FRC.complex_mass_couplings(ret)
  expr = together(ret)
  return expr

#------------------------------------------------------------------------------#

def solve_renormalization_system(system, renos):
  """ Solves the renormalization conditions. The sympy expression is transformed
  explicitly into a system of linear equations. The system is then solved via
  Sympys solve_linear_system method.

  >>> from sympy import symbols
  >>> x, y = symbols('x y')
  >>> system = [x + y + 3, x - y - 1]
  >>> solve_renormalization_system(system, [x, y])
  {x: -1, y: -2}
  """
  l_system = []
  key_len = len(renos)
  for eq in system:
    eq_poly = Poly(eq, renos).as_dict()
    key_gen = (tuple([0]*i + [1] + [0]*(key_len - 1 - i))
               for i in range(key_len))
    eq_vector = [eq_poly[key] if key in eq_poly else 0 for key in key_gen]
    zero_key = tuple([0]*key_len)
    eq_vector += [-eq_poly[zero_key]] if zero_key in eq_poly else [0]
    l_system.append(eq_vector)
  l_system = Matrix(l_system)
  return solve_linear_system(l_system, *renos)

#------------------------------------------------------------------------------#

def solve_renormalization(eqs, vars):
  """ Solves the of equations without assuming any relations between constants.

  :param eqs: system of equations in the form [a, b, c, ..] such that
              a = 0, b = 0, c = 0
  :type  eqs: list of sympy expressions

  :param vars: renormalization constants
  :type  vars: list of sympy symbols

  >>> from sympy import symbols
  >>> a, b, c = symbols('a b c')
  >>> eqs = [2*a+b+1, 2*b+c+1, 2*c+a+2]; vars = [a, b, c]
  >>> solve_renormalization(eqs, vars)
  {c: -7/9, b: -1/9, a: -4/9}
  """

  def factors(i=-1):
    while True:
      i += 1
      yield Symbol("a" + str(i))

  facs = factors()
  simple_eqs = []
  expr_dict = {}

  for eq in eqs:
    eq_poly = Poly(eq, vars).as_dict()
    expr = 0
    for key in eq_poly:
      fac = next(facs)
      expr_dict[fac] = eq_poly[key]
      if sum(key) == 0:
        expr += fac
      else:
        b = 1
        for pos, power in enumerate(key):
          b *= pow(vars[pos], power)
        expr += b*fac

    simple_eqs.append(expr)
  sols = solve(simple_eqs, vars)
  for sol in sols:
    sols[sol] = sols[sol].subs(expr_dict)

  return sols

#==============================================================================#
#                                cms_correction                                #
#==============================================================================#

def cms_correction(ctexpr, mass):
  """ Determines the necessary corrections to the naive complex-mass
  scheme expansion. It is assumed that the mass counterterm is determined
  onshell for real momentum. The routine corrects the integrals DB0,
  DB1 and DB11 to first non-vanishing order.

  :param ctexpr: The ct parameter solution in terms of scalar integrals and
                 couplings
  :type  ctexpr: symbol

  :param mass: The mass name of the particle,  e.g. `MW`
  :param mass: str
  """

  known_sints = ['B0', 'B1', 'B00', 'B11', 'DB0', 'DB1', 'DB00', 'DB11']

  rmass = 'r' + mass
  cmass = 'c' + mass
  cmscorr = {'DB0': {'massless1': 1/Symbol(rmass)**2,
                     'massless2': 1/Symbol(rmass)**2},
             'DB1': {'massless2': -1/Symbol(rmass)**2},
             'DB11': {'massless2': 1/Symbol(rmass)**2}}

  # First step: Find critical scalar integrals in the expression, namely those
  # terms proportional to derivatives
  #ctval = renodict['value']
  tgen = (u.name for u in ctexpr.free_symbols if u.name.startswith('T'))
  from rept1l.regularize import Regularize
  Regularize.load()
  ctvalexp = ctexpr.expand()
  totalcorr = 0
  for t in tgen:
    if t not in Regularize.tens_dict:
      log('Did not found ' + t + ' in tens_dict!', 'error')

    tens = Regularize.tens_dict[t]

    # integral needs to be a derivative
    cond1 = (tens[0] == 'DB')
    if not cond1:
      continue

    # only B0, B1, B11 receive corrections, and not B00
    cond2 = tens[1] == '0' or tens[1] == '1' or tens[1] == '11'
    if not cond2:
      continue
    targs = tens[2]
    # correction only applied if the momentum is real, and the corresponding
    # particle runs in the loop in addition to a massless one.
    cond3 = targs[0] == rmass
    if not cond3:
      continue
    cond4 = ((targs[1] == 'M0' and targs[2] == mass) or
             (targs[2] == 'M0' and targs[1] == mass))
    if not cond4:
      continue

    texpr = tens[0] + tens[1]
    masscase = 'massless1' if targs[1] == 'M0' else 'massless2'
    cond5 = (texpr in cmscorr) and (masscase in cmscorr[texpr])
    if not cond5:
      continue

    corrfac = cmscorr[texpr][masscase]

    # a clean expansion needs to rewrite the expression in terms of \Gamma/M
    coeff = export_expression(ctvalexp.coeff(t))

    IGM = Symbol('IGM')
    exprule = {Symbol(cmass): Symbol(rmass)*(1-IGM/2)}
    coeff = coeff.subs(exprule)
    coeffp = Poly(coeff, [IGM])

    check = [sum(u) for u in coeffp.monoms()]

    if any(u < 1 for u in check):
      errmsg = ('CMS expansion does not make sense. ' +
                'Correction would be of leading order')
      raise Exception(errmsg)
    if 1 not in check:
      log('No CMS expansion needed', 'info')

    # IGM corresponds to I \Gamma/M = (M^2 - cM^2)/M^2
    IGM = (1 - Symbol(cmass)**2/Symbol(rmass)**2)
    corr = factor(coeffp.coeff_monomial((1,)) * corrfac * IGM)
    totalcorr += corr

  if totalcorr != 0:
    log('Found cms expansion correction.', 'info')
    log('Total correction reads: ' + str(totalcorr), 'debug')
    return totalcorr
  else:
    log('No CMS expansion correction.', 'info')

#==============================================================================#
#                                 fortran_test                                 #
#==============================================================================#

def gen_ct_test_file(filepath, parametervalues={}):
  """ Generates a test file evaluating all counterterms. Compilation of this
  file only requires the model file without recola.

  :param filepath: Absolute file path which is filled with fortran code.
  :type  filepath: str

  :param parametervalues: A dictionary of (string) parameters and values.
  :type  parametervalues: dict
  """
  from rept1l.pyfort import progBlock

  # build program and header
  test_CT = progBlock('test_CT')
  tct = test_CT
  tct.addPrefix('use constants_mdl')
  tct.addPrefix('use fill_couplings')
  tct.addPrefix('use class_particles')
  tct.addPrefix('use class_couplings')
  tct.addPrefix('use scalar_functions')
  tct.addType(('real(dp)', 'massreg=1d-3'))
  tct.addType(('integer', 'i,nn'))
  tct.addType(('complex(dp), allocatable', 'm2(:)'))
  tct.addLine()

  # set parameter values. Scales are handled separately.
  scales = ['muUV', 'muIR', 'muMS', 'lambda', 'massreg']
  for scale in scales:
    if scale in parametervalues:
      tct + (scale + ' = ' + parametervalues[scale])
      del parametervalues[scale]
    else:
      if scale != 'massreg':  # default value for massreg 1d-3 alrdy set
        tct + (scale + ' = 80.370881514409731d0')

  for p, val in parametervalues.iteritems():
    tct + ('call set_parameter_mdl("' + p + '",complex(' + val + ', 0d0))')

  test_CT + 'deltaUV = 0d0'
  test_CT + 'deltaIR = 0d0'
  test_CT + 'deltaIR2 = 0d0'
  test_CT + 'masscut = 3d0'
  test_CT + 'call init_cmasses_mdl()'
  test_CT + 'call fill_couplings_mdl()'

  # init coli version
  (test_CT > 0) + '#ifdef USE_COLI_REPTIL'
  test_CT + 'call initcoli'
  test_CT + 'call setdeltauv(deltaUV)'
  test_CT + 'call setdeltair(deltaIR,deltaIR2)'
  test_CT + 'call setmuir2(muIR**2)'
  test_CT + 'call setmuuv2(muUV**2)'

  test_CT + 'call clearminf2'
  test_CT + 'do i = 1,n_particles'
  test_CT + '  if (get_particle_mass_reg_mdl(i) .eq. 2 .and. is_particle_mdl(i)) then'
  test_CT + '    call addminf2(get_particle_cmass2_reg_mdl(i))'
  test_CT + '  endif'
  test_CT + 'enddo'
  (test_CT > 0) + '#endif'

  # init collier version
  (test_CT > 0) + '#ifdef USE_COLLIER_REPTIL'
  test_CT + 'call Init_cll(2,noreset=.true.)'
  test_CT + 'call SetMode_cll(1)'
  test_CT + 'call SetDeltaUV_cll (deltaUV)'
  test_CT + 'call SetDeltaIR_cll (deltaIR,deltaIR2)'

  test_CT + 'call SetMuIR2_cll(muIR**2)'
  test_CT + 'call SetMuUV2_cll(muUV**2)'
  test_CT + 'nn = 0'
  test_CT + 'do i = 1,n_particles'
  test_CT + '  if (get_particle_mass_reg_mdl(i) .eq. 2 .and. is_particle_mdl(i)) nn = nn + 1'
  test_CT + 'end do'

  test_CT + 'allocate (m2(nn))'
  test_CT + 'nn = 0'
  test_CT + 'do i = 1,n_particles'
  test_CT + '  if (get_particle_mass_reg_mdl(i) .eq. 2 .and. is_particle_mdl(i)) then'
  test_CT + '    nn = nn + 1'
  test_CT + '    m2(nn) = get_particle_cmass2_reg_mdl(i)'
  test_CT + '  endif'
  test_CT + 'enddo'
  test_CT + 'call setminf2_cll (nn,m2)'
  test_CT + 'deallocate (m2)'
  (test_CT > 0) + '#endif'

  test_CT + '\n'
  test_CT + '! Precomputation of scalar integrals'

  # Perform substitutions for masses and scalar integrals
  from rept1l.particles import ParticleMass
  ParticleMass.load()
  tens_repl = {}  # substitute the symbols T_i to Ten_i, in order to avoid
                  # conflict with user defined t_i in the fortran code

  # build scalar integral header
  from rept1l.regularize import Regularize
  if len(Regularize.tens_dict) > 0:
    tens_keys = Regularize.tens_dict.keys()
    for u in tens_keys:
      tens_repl[Symbol(u)] = Symbol('Ten' + u[1:])
    tens_keys = ['Ten' + u[1:] for u in tens_keys]
    # scalar function variables
    var1 = ', '.join(tens_keys)
    tct.addType(('complex(dp)', var1))
    tct.addLine()

  tens_dict = {'Ten' + u[1:]: Regularize.tens_dict[u]
               for u in Regularize.tens_dict}

  # evaluate scalar integrals
  from rept1l.parsing import derive_tensor_expr
  tensors_evaluated = derive_tensor_expr(tens_dict, tabs=0)
  tct + tensors_evaluated
  tct + '\n'

  # evaluate counterterms
  from .renoconst import RenormalizationConstant

  # Get the counterterms in a suited order, respecting dependencies, for direct
  # evaluation
  renos = RenormalizationConstant.renosdict_ordered()
  reno_vars = ', '.join([u.name + '_test' for u in renos])
  tct.addType(('complex(dp)', reno_vars))
  renos_repl = {u: Symbol(u.name + '_test') for u in renos}
  for c in renos:
    rds = RenormalizationConstant[c]
    if 'value' in rds:
      rds = {rds['renoscheme']: rds}

    for scheme in rds:
      val = rds[scheme]['value']
      is_real = 'real' in rds[scheme] and rds[scheme]['real']
      has_scaledep = ('scaledep' in rds[scheme] and
                      rds[scheme]['scaledep'] is not None)
      if has_scaledep:
        scaledep = rds[scheme]['scaledep']
        scaleshift = 'log(muUV**2/' + scaledep + '**2)'

      coupl_repl = {u: Symbol('coupl(n' + u.name + ')')
                    for u in val.free_symbols
                    if u.name.startswith('GC_')}
      val = (val.xreplace(tens_repl).
             xreplace(renos_repl).xreplace(coupl_repl))

      c_val = export_fortran(str(val))
      c_name = c.name + '_test'
      test_CT + ('! ' + c.name + ': ' + scheme)
      if has_scaledep:
        test_CT += 'DeltaUV = DeltaUV + ' + scaleshift
      test_CT += (c_name + ' = ' + c_val)
      if is_real:
        test_CT += (c_name + ' = cone*real(' + c_name + ',kind=dp)')
      if has_scaledep:
        test_CT += 'DeltaUV = DeltaUV - ' + scaleshift
      test_CT + ('write (*,*) "' + str(c) + ':", ' + c_name)

  with open(filepath, 'w') as f:
    test_CT.printStatment()
    test_CT.printStatment(f)
