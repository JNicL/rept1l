################################################################################
#                              renormalize_msbar                               #
################################################################################

##############
#  Includes  #
##############

from sympy import Symbol
from rept1l.helper_lib import find_vertex
from rept1l.renormalize import RenormalizeVertex
from rept1l.renormalize import RenoConst as RC
from six import iteritems

#############
#  Methods  #
#############

def renormalize_msbar(Renormalize, particles, var, scaledep='muMS',
                      renoscheme='MS', splitvar=False, simplify=True,
                      default=False, **kwargs):
  Renormalize.load_storage()
  varsymbol = Symbol(var)
  assert(type(particles) is list)

  # collect all ct associated to the vertex
  vertices = find_vertex(particles, CT=True)
  msrenos = set()
  for vertex in vertices:
    msrenos = msrenos.union(set(RenormalizeVertex.get_vertex_counterterms(vertex)))
    msrenos = msrenos.union(set(RenormalizeVertex.get_vertex_wavefunctions(vertex)))
  msrenos.remove(varsymbol)

  vertex = vertices[0]
  reno_sols = RC.build_ms_ct_solutions(msrenos,
                                       plugin_ms_values=False,
                                       renoscheme=renoscheme,
                                       scaledep=scaledep)

  ctsol = RenormalizeVertex(vertex, renoscheme='MS', ct=varsymbol,
                            solve_eq=True, reno_sols=reno_sols,
                            compute_wavefunctions=False,
                            **kwargs)

  if splitvar:
    ct = ctsol.solutions.keys()[0]
    ct_ms = Symbol(ct.name + '_' + renoscheme)
    Renormalize.update_renos({ct_ms: ctsol.solutions[ct]},
                             renoscheme=renoscheme,
                             real=True, scaledep=scaledep, simplify=simplify)
    Renormalize.update_renos({ct: ct_ms}, renoscheme=renoscheme,
                             real=True, simplify=simplify, default=default)
  else:
    Renormalize.update_renos(ctsol.solutions, renoscheme=renoscheme, real=True,
                             scaledep=scaledep, simplify=simplify, default=default)


def substitute_msbar_parameter(Reno, param_indep, param_msbar, simplify_func=None,
                               scaledep_ms='muMS', renoscheme_ms='MS',
                               renoscheme=None, simplify=True):

  # final name scheme
  if renoscheme is None:
    renoscheme = param_msbar + '_' + renoscheme_ms

  # get msbar value for independent parameter
  dparam_indep_s = Symbol('d' + param_indep)
  dparam_indep_ms_sol = RC.build_ms_ct_solutions([dparam_indep_s],
                                                 plugin_ms_values=False,
                                                 scaledep=scaledep_ms,
                                                 renoscheme=renoscheme_ms)

  # build ct parameter dependence of param_msbar
  from rept1l.counterterms import Counterterms
  param_msbar_s = Symbol(param_msbar)
  ctbase = [u for u in Counterterms.get_all_ct()]
  from sympy import Poly
  dparam_msbar = Poly(Counterterms.ctdict[param_msbar_s], ctbase).as_dict()
  zerokey = tuple([0] * len(ctbase))
  del dparam_msbar[zerokey]

  # get msbar value for other parameter dependence
  ct_dep = [ctbase[u.index(1)] for u in dparam_msbar]
  reno_sols = RC.build_ms_ct_solutions(ct_dep,
                                       plugin_ms_values=False,
                                       scaledep=scaledep_ms,
                                       renoscheme=renoscheme_ms)



  if simplify_func is None:
    from rept1l.particles import ParticleMass
    ParticleMass.load()
    pmasses = ParticleMass.masses.values()
    # substitute real mass expressions by complex ones
    mass_repl = {Symbol(u.name): Symbol('c' + u.name)
                 for u in pmasses}
    simplify_func = lambda x: x.subs(mass_repl)

  dparam_msbar = {key: simplify_func(val) for key, val in iteritems(dparam_msbar)}

  param_indep_pos = ctbase.index(dparam_indep_s)
  param_indep_key = tuple(0 if i != param_indep_pos else 1 for i in range(len(ctbase)))
  param_indep_coeff = simplify_func(dparam_msbar[param_indep_key])
  del dparam_msbar[param_indep_key]

  # construct substitution for finite parts:
  # delta_x -> delta_x -delta_x^MSbar
  reno_sols = {key: key - val for key, val in iteritems(reno_sols)}
  dparam_msbar = -sum(ctbase[key.index(1)].subs(reno_sols) * val
                      for key, val in iteritems(dparam_msbar)) / param_indep_coeff

  dparam_indep_fin = Symbol('d' + param_indep + '_' + param_msbar + '_fin')
  Reno.update_renos({dparam_indep_fin: dparam_msbar},
                    renoscheme=renoscheme,
                    simplify=simplify)


  Reno.update_renos({dparam_indep_s: dparam_indep_fin +
                                     dparam_indep_s.subs(dparam_indep_ms_sol)},
                    renoscheme=renoscheme,
                    simplify=simplify)

