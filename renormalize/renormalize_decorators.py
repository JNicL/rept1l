################################################################################
#                          renormalize_decorators.py                           #
################################################################################

def renomethod(renoid, *sols):
  """ Decorate a renormalization procedure with the solution attribute which
  tells which solutions the method provides after execution.  """
  def attr_decorator(fn):
    setattr(fn, 'renoid', renoid)
    setattr(fn, 'solutions', sols)
    return fn

  return attr_decorator
