#==============================================================================#
#                                renormalize.py                                #
#==============================================================================#
from __future__ import print_function

#============#
#  Includes  #
#============#

import sys
try:
  from types import ListType
  list_type = ListType
except ImportError:
  list_type = list
from six import with_metaclass, iteritems
from past.builtins import xrange
from functools import reduce
from sympy import Symbol, gcd_terms, factor_terms, pi, Poly, together, sympify
from sympy.solvers import solve

from rept1l.helper_lib import StorageProperty, find_vertex
from rept1l.logging_setup import log
import rept1l.Model as Model
model = Model.model


def ctsolutions(*sols):
  """ Decorate a renormalization procedure with the solution attribute which
  tells which solutions the method provides after execution.  """
  def attr_decorator(fn):
    setattr(fn, 'solutions', sols)
    return fn

  return attr_decorator


#==============================================================================#
#                              class Renormalize                               #
#==============================================================================#

class Renormalize(with_metaclass(StorageProperty)):
  """ Demonstration for a typical renormalization procedure.  The user iterates
  over the particles and specifies renormlization condition for each of them.
  For instance, particles can be defined in the onshell
  scheme. Alike vertices can be renormalized. """
  # StorageMeta attributes. Those attributes are stored when `store` is called.
  storage = ['reno_repl']
  # StorageMeta storage path
  #path = os.path.dirname(os.path.realpath(__file__)) + '/ModelRenormalization'
  mpath, _ = model.get_modelpath()
  path = mpath + '/ModelCounterterms'
  # StorageMeta filename
  name = 'renormalization_substitutions.txt'

  def __init__(self, particles, RenoConst, store=True, load=False, solve=True,
               **kwargs):
    """
    :particles: list of UFO particles
    :vertex: UF vertex
    :store: If true stores the determined from the renormalization conditions
    """

    self.RenoConst = RenoConst
    self.load_storage()

    for particle in particles:
      self.renormalize_particle(particle, store=store, **kwargs)

  @classmethod
  def load_storage(cls):
    """ Loads previously calculated results, expressions for scalar integral
    expressions and couplings. """
    from rept1l.coupling import RCoupling
    from rept1l.regularize import Regularize
    RCoupling.load()
    cls.load()
    Regularize.load()

  def renormalize_particle(self, particle, store=True, simplify=True, **kwargs):
    """ Renormalizes the particle `particle` onshell. The particle `particle`
    can also be a list of particles in which case a mixing  between all
    particles is taken into account.

    :param particle: Particle or list of particles
    :type  particle: (list of) UFO particle(s)
    """
    from .renormalize_methods import RenormalizeSelfenergy
    from .renormalize_utils import solve_renormalization

    # Passing a single particle, no mixing is assumed
    if isinstance(particle, model.Particle):
      log('Performing on-shell renormalization', 'info')
      reno = RenormalizeSelfenergy(particle, renoscheme='onshell',
                                   simplify_expr=self.simplify_expr,
                                   solve_eq=True,
                                   **kwargs)
      if reno.status == 'success':
        # onshell scheme if particle is stable -> take the real part after
        # renormalization
        real = False if reno.unstable else True
        self.update_renos(reno.solutions, store=store, renoscheme='onshell',
                          real=real, simplify=simplify)
        if(hasattr(particle, 'light_particle') and
           particle.light_particle):
          log('Particle ' + particle.name + ' tagged as light particle', 'info')
          log('Performing on-shell renormalization in mass regularization',
              'info')
          from rept1l.counterterms import get_particle_ct
          mct = get_particle_ct(particle, wavefunction=False, mass=True)[0]
          wct = get_particle_ct(particle, wavefunction=True, mass=False)
          renomassreg = RenormalizeSelfenergy(particle,
                                              renoscheme='onshell',
                                              solve_eq=True,
                                              mass_reg=True,
                                              realmomentum=False,
                                              cts=wct,
                                              **kwargs)
          renomassreg.solutions[mct] = sympify('0')
          self.update_renos(renomassreg.solutions, store=store,
                            real=True, renoscheme='massreg', simplify=False)
          log('Performing on-shell renormalization in dimensional ' +
              'regularization', 'info')
          renodimreg = RenormalizeSelfenergy(particle, renoscheme='onshell',
                                             simplify_expr=self.simplify_expr,
                                             solve_eq=True,
                                             dimreg=True,
                                             force_massless=True,
                                             **kwargs)
          wcts = get_particle_ct(particle, wavefunction=True, mass=False,
                                 only_diagonal=True)
          assert(all(wct in renodimreg.solutions for wct in wcts))

          for wct in wcts:
            self.update_renos({wct: renodimreg.solutions[wct]}, store=store,
                              renoscheme='dimreg', simplify=simplify)
          self.update_renos({mct: sympify('0')}, store=store,
                            real=True, renoscheme='dimreg', simplify=False)
      else:
        errmsg = ('Failed on-shell renormalization for particle: ' +
                  particle.name)
        log(errmsg, 'error')

    # List of particles, mixing is assumed
    elif isinstance(particle, list_type):
      particle_mixing = False
      particle_mixing = all([isinstance(p, model.Particle)
                            for p in particle])
      particle_mixing = particle_mixing and (len(particle) == 2)
      if particle_mixing:
        conditions = []
        renos = []

        # digagonal: standard onshell renormalization
        for p in particle:
          c = RenormalizeSelfenergy(p,
                                    renoscheme='onshell',
                                    solve_eq=False,
                                    simplify_expr=self.simplify_expr,
                                    **kwargs)

          if c.status == 'success':
            conditions.extend(c.renorm_conditions)
          else:
            raise Exception('Failed mixing for ' + p.name +
                            ' -> ' + p.name + '.')
          for reno in c.renos:
            if reno not in renos:
              renos.append(reno)

        # non-diagonal: require that the mixings vanish
        from rept1l.combinatorics import m_of_n
        for p1, p2 in m_of_n(particle, 2):
          c1 = RenormalizeSelfenergy(p1,
                                     particle_out=p2,
                                     renoscheme='onshell',
                                     solve_eq=False,
                                     simplify_expr=self.simplify_expr,
                                     residue=False,
                                     **kwargs)
          if c1.status == 'success':
            conditions.extend(c1.renorm_conditions)
          else:
            raise Exception('Failed mixing for ' + p1.name +
                            ' -> ' + p2.name + '.')
          c2 = RenormalizeSelfenergy(p2,
                                     particle_out=p1,
                                     renoscheme='onshell',
                                     solve_eq=False,
                                     simplify_expr=self.simplify_expr,
                                     residue=False,
                                     **kwargs
                                     )
          if c2.status == 'success':
            conditions.extend(c2.renorm_conditions)
          else:
            raise Exception('Failed mixing for ' + p2.name +
                            ' -> ' + p1.name + '.')

          for reno in c1.renos:
            if reno not in renos:
              renos.append(reno)
          for reno in c2.renos:
            if reno not in renos:
              renos.append(reno)
        if solve:
          solutions = solve_renormalization(conditions, renos)
          self.update_renos(solutions, renoscheme='onshell', store=store,
                            simplify=simplify)
        else:
          self.update_renos({'renorm_conditions': conditions,
                             'renos': renos}, store, simplify=simplify)
      else:
        raise TypeError('Particle mixing has wrong type')
    else:
      raise TypeError('Particles array has wrong type')

  @classmethod
  def update_renos(cls, renos, renoscheme, real=False, scaledep=None,
                   store=True, default=False, simplify=True):
    """ Registers and stores counterterm parameters associated to the scheme
    `renoscheme`.

    :param renos: A dicitonary of ct symbols and corresponding values
    :type  renos: dict

    :param renoscheme: The renormalization scheme associated to the renos
    :type  renoscheme: str

    :param real: Set if the renormalization constant should forced to be real
                 (taking the real part).
    :type  real: bool

    :param scaledep: Sets a parameter which parametrizes
                     the scale dependence. The parameter should be passed as
                     string (or None) and should be present in the model.
                     Only affects parameters depending on DeltaUV.
    :type  scaledep: str

    :param default: Default scheme choice (see RenoConst)
    :type  default: bool
    """
    for reno in renos:
      if simplify:
        valsimp = cls.simpct2(renos[reno], status='simp ' + reno.name,
                              plotProgress=True)
      else:
        valsimp = renos[reno]
      cls.RenoConst(reno, valsimp, renoscheme=renoscheme, real=real,
                    scaledep=scaledep, store=store, default=default)

    if store:
      from rept1l.regularize import Regularize
      Regularize.store()

  @staticmethod
  def simplify_expr(expr, **kwargs):
    return expr

  @classmethod
  def renormalize_phi3(cls):
    """ Renormalizes the Phi3 theory onshell.  """
    from renormalize_methods import RenormalizeVertex

    scalars = [model.P.phi]
    cls(scalars, None)

    msrenos = []
    vertex = find_vertex(['phi', 'phi', 'phi'])
    assert(len(vertex) == 1)
    vertex = vertex[0]
    msrenos += RenormalizeVertex.get_vertex_counterterms(vertex)
    msrenos += RenormalizeVertex.get_vertex_wavefunctions(vertex)
    msrenos.remove(Symbol('dZl'))

    reno_sols = cls.RenoConst.build_ms_ct_solutions(msrenos,
                                                    plugin_ms_values=False)

    dZlsol = RenormalizeVertex(vertex, renoscheme='MS', ct=Symbol('dZl'),
                               solve_eq=True, reno_sols=reno_sols,
                               compute_wavefunctions=False)

    cls.update_renos(dZlsol.solutions, renoscheme='MS')

  @classmethod
  def renormalize_phi4(cls):
    """ Renormalizes the Phi4 theory onshell.  """
    from renormalize_methods import RenormalizeVertex

    scalars = [model.P.phi]
    cls(scalars, None)

    msrenos = []
    vertex = find_vertex(['phi', 'phi', 'phi', 'phi'])
    assert(len(vertex) == 1)
    vertex = vertex[0]
    msrenos += RenormalizeVertex.get_vertex_counterterms(vertex)
    msrenos += RenormalizeVertex.get_vertex_wavefunctions(vertex)
    msrenos.remove(Symbol('dZl'))

    reno_sols = cls.RenoConst.build_ms_ct_solutions(msrenos,
                                                    plugin_ms_values=True)

    dZlsol = RenormalizeVertex(vertex, renoscheme='MS', ct=Symbol('dZl'),
                               solve_eq=True, reno_sols=reno_sols,
                               compute_wavefunctions=False)

    cls.update_renos(dZlsol.solutions, renoscheme='MS')

  @staticmethod
  def simplify_t(expr, tens_base):
    if len(tens_base) > 0:
      tensor_poly = Renormalize.tensor_poly(expr, tens_base)
      simplified_poly = Renormalize.simplify_tensor_poly(tensor_poly)
      return Poly(simplified_poly, tens_base).as_expr()
    else:
      return factor_terms(expr)

  @staticmethod
  def tensor_poly(expr, tens_base):
    from sympy import expand
    return Poly(expand(expr), tens_base).as_dict()

  @staticmethod
  def simplify_tensor_poly(t_poly):
    for key in t_poly:
      t_poly[key] = factor_terms(t_poly[key])
    return t_poly

  @classmethod
  def simpct2(cls, expr, ctbase=None, status=None, plotProgress=False):
    """ Generic method to simplify counterterm expressions. Generates a
    polynomial of scalar integrals, counterterm parameter and DeltaUV and
    simplifies all coefficients. The coefficients are in addition checked
    whether they are zero (analytically). The result is put together in the end.

    :param expr: The value of the counterterm parameter
    :type  expr: sympy

    :param status: Status to be printed in the progressbar
    :type  status: str
    """
    from rept1l.regularize import Regularize as R
    from rept1l.coupling import RCoupling as FRC

    DeltaUV = Symbol('DeltaUV')
    tens_base = [u for u in expr.free_symbols if u.name.startswith('T')]

    if ctbase is None:
      from .renoconst import RenormalizationConstant as RC
      ctbase = set(u.name for u in RC.renosdict(multi_reno=True))

    ctbase = [u for u in expr.free_symbols if u.name in ctbase or u in ctbase]

    is_pure_ms = False
    if len(tens_base) + len(ctbase) == 0:
      is_pure_ms = True

    GCMI = Symbol('GC_MI')
    GCI = Symbol('GC_I')
    GCM1 = Symbol('GC_M1')
    sq2 = Symbol('sq2')
    simple_coupling_subs = {GCMI: FRC.coupling_values[GCMI.name],
                            GCI: FRC.coupling_values[GCI.name],
                            GCM1: FRC.coupling_values[GCM1.name],
                            sq2**2: 2,
                            }

    from rept1l import Model as Model
    all_particles = Model.model.object_library.all_particles
    ZERO = Model.model.param.ZERO
    for p in (u for u in all_particles if u.mass != ZERO):
      imn = 'GC_' + p.mass.name
      if imn in FRC.coupling_values:
        simple_coupling_subs[Symbol(imn)] = FRC.coupling_values[imn]

    expr = expr.subs(simple_coupling_subs)

    base = [v for v in tens_base]
    base += [DeltaUV]
    base += ctbase

    if not is_pure_ms:
      expre = expr.expand()
      termsTens = {}
      termsUV = 0
      termsCT = {}
      termsR = 0

      if len(base) > 1 and plotProgress:
        from rept1l.pyfort import ProgressBar
        PG = ProgressBar(len(base), status=status)
      for b in base:
        c = expre.coeff(b)
        c_val = FRC.expression_value(c, {Symbol('Nc'): 3})
        try:
          c_val = abs(c_val)
          # check if coeff is zero
          if(c_val < 10**-15 and R.test_vanish(c)):
            if plotProgress:
              PG.advanceAndPlot()
            continue
        except TypeError:
          pass
        if b in tens_base:
          termsTens[b] = together(c * 16 * pi**2)
        elif b == DeltaUV:
          termsUV = together(c * 16 * pi**2) * DeltaUV
        elif b in ctbase:
          termsCT[b] = together(c)

      termsR = (expre.subs({u: 0 for u in base}) * 16 * pi**2)
      termsTens = (sum(k * v for k, v in iteritems(termsTens)))
      termsCT = (sum(k * v for k, v in iteritems(termsCT)))
      expr = (termsTens + termsUV + termsR) / (16 * pi**2)
      expr += termsCT
    else:
      expr = together((expr * 16 * pi**2).expand()) / (16 * pi**2)
    return expr

  @classmethod
  def split_renos(cls, rdicts, reno_repl={}, ctbase=None, simplify=True):
    """ Splits the renormalization constant in terms of fundamental orders. In
    case the renormalization constant has multiple renormalization schemes the
    splits are merged.

    :param rdicts: Dictionary of ct dictionaries. See return of
                   RenoConst['ctname']
    :type  rdicts: dict

    :param reno_repl: Substitution rules for the expression
    :type  reno_repl: dict
    """
    from sympy import together
    from rept1l.coupling import RCoupling as FRC
    basenames = set(u['basename'] for u in rdicts.values())
    assert(len(basenames) == 1)
    reno = basenames.pop()
    scheme_splits = {}
    for scheme, rdict in iteritems(rdicts):
      try:
        value = rdict['value'].xreplace(reno_repl)
      except AttributeError:
        value = sympify(rdict['value']).xreplace(reno_repl)

      reno_split = (FRC(value, 'CT', opt_name=reno, check_coupling=False,
                        subs_cvalues=False, forbid_update=False,
                        add_order=True, simp_func=together).return_coupling())
      scheme_vals = {}
      if reno_split.is_Add:
        for new_reno in reno_split.free_symbols:
          val = FRC.coupling_values[new_reno.name]
          val = FRC.substitute_original(val)
          if simplify:
            val = cls.simpct2(val, ctbase=ctbase)
          scheme_vals[new_reno.name] = val

        # not tested and not sure what this does ?!
        if len(scheme_vals) == 1:
          log('Warning entering untested block in register CT', 'warning')
          FRC.remove_couplings(scheme_vals.keys()[0])
          reno_split = (FRC(scheme_vals.values()[0], 'CT', opt_name=reno,
                        check_coupling=False, subs_cvalues=False,
                        forbid_update=False, add_order=True).return_coupling())
      else:
        val = FRC.coupling_values[reno_split.name]
        val = FRC.substitute_original(val)
        if simplify:
          val = cls.simpct2(val, ctbase=ctbase)
        if val != 0:
          scheme_vals[reno_split.name] = val
        else:
          FRC.remove_couplings(reno_split)
      scheme_splits[scheme] = scheme_vals

    if len(scheme_splits) > 1:

      # check that the splits are the same, otherwise need to add `0` splits
      splits = [set(u.keys()) for u in scheme_splits.values()]
      if all(splits[0] == v for v in splits[1:]):
        return scheme_splits
      else:
        common = set([])
        for split in splits:
          common = common | split
        for scheme, rdict in iteritems(scheme_splits):

          for split in common:
            if split not in rdict:
              scheme_splits[scheme][split] = sympify('0')

    return scheme_splits

  @classmethod
  def register_ctparameter(cls, simplify=True, force=False, zero_hints=[]):
    """ Derives substitution rules used to derive the order of counterterms.
    Each ct is split into fundamental orders (e.g. the order in QCD or QED).
    New ct are have a unique order and the the sum of the new ct is the
    oridingal ct, i.e. the split

    ct = ct_QED2 + ct_QCD2 + ct_QED3 + ct_QCD4 + ...

    is performed for all occuring order.
    This split is used for all following ct and the ct are split accordingly
    """
    from rept1l.counterterms import Counterterms
    cls.load()
    #RenormalizationConstant.load()
    Counterterms.load()

    # We use the RCoupling class to determine the order of the
    # counterterm parameters
    from rept1l.coupling import RCoupling as FRC

    from .renoconst import RenormalizationConstant as RC
    from .renoconst import CountertermError

    # Check that all parameters have a valued assigned
    pcts = Counterterms.get_all_ct(derived_ct=False)
    found_all_cts = True
    for pct in pcts:
      try:
        RC[pct]
      except CountertermError as e:
        print(e)
        print('The system did not find a renormalization for: ' + str(pct))
        found_all_cts = False

    if (not found_all_cts and not force):
      from rept1l.helper_lib import query_yes_no
      answer = query_yes_no('Not all parameters have been renormalized. ' +
                            'Do you want to go on anyway?')
      if not answer:
        sys.exit()

    # set regged couplings to zero which is necessary if processes have been
    # computed in the same run
    FRC.couplings_regged = {}

    from rept1l.pyfort import ProgressBar
    # Get all cts in the order of their dependence
    renoslist = RC.renosdict_ordered()
    levels = len(renoslist)
    if levels > 0:
      pg = ProgressBar(levels)
      pg.plotProgress()

    # build up the list of new ct expressions
    ctbase = set()
    # build up the repl dict: old ct -> sum of new ct
    new_reno_repl = {}
    for reno in renoslist:
      rdict = RC[reno]
      # check if a single ct, then the 'value is found in rdict
      if 'value' in rdict:
        # for compatibility we add the basename because some derived
        # counterterms have no basename assigned.
        if 'basename' not in rdict:
          rdict['basename'] = reno.name
        rdicts = {rdict['renoscheme']: rdict}
      # rdict has multiple ct with different renoschemes
      else:
        rdicts = rdict

      # split the ct in different fundamental couplings for ever renoscheme of
      # the ct `reno` (rdict is the corresponding dict)
      for scheme, rdict in iteritems(cls.split_renos(rdicts,
                                     reno_repl=new_reno_repl,
                                     ctbase=ctbase,
                                     simplify=simplify)):
        # reconstruct the substitution rule: ct -> ct_ORD1 + ct_ORD2 + ...
        reno_split = sum(Symbol(u) for u in rdict)

        # take the original ct dict as template to build new ones
        renodict = RC.registry[scheme][reno]
        renodict['active'] = False

        from rept1l.regularize import Regularize as R
        # for each order in fundamental couplings a new ct is introduced
        for new_reno in rdict.keys():
          val = rdict[new_reno]
          if new_reno in zero_hints:
            # replace counterterm dependence by corresponding values
            while any(u in RC for u in val.free_symbols):
              for u in val.free_symbols:
                if u in RC:
                  val = val.subs({u: RC[u]['value']})
            if R.test_vanish(val):
              log('Confirmed zero component of counterterm parameter/scheme: ' +
                  new_reno + ', ' + scheme, 'info')
              reno_split = reno_split.subs({new_reno: 0})
          else:
            new_renodict = renodict.copy()
            new_renodict['value'] = val
            new_renodict['active'] = True
            new_renodict['original'] = False
            RC[new_reno] = new_renodict
            ctbase.add(new_reno)

        new_reno_repl[reno] = reno_split

        if levels:
          pg.advanceAndPlot(status=reno.name)

    # next we register derived counterterm parameters
    log('Register derived ct parameter values and orders', 'info')
    renoslist = Counterterms.derived_ct_values.copy()
    levels = len(renoslist)
    if levels > 0:
      pg = ProgressBar(levels)

    for reno in renoslist:
      value = Counterterms.derived_ct_values[reno].xreplace(new_reno_repl)
      reno_split = (FRC(value, 'CT', opt_name=reno, check_coupling=False,
                        subs_cvalues=False, forbid_update=False,
                        add_order=True, simp_func=together).return_coupling())
      for new_reno in reno_split.free_symbols:
        val = FRC.coupling_values[new_reno.name]
        val = FRC.substitute_original(val)
        if simplify:
          val = cls.simpct2(val, ctbase=ctbase)
        if val != 0:
          Counterterms.derived_ct_values[new_reno.name] = val
        else:
          FRC.remove_couplings(new_reno)
          reno_split = reno_split.subs({new_reno.name: 0})

      new_reno_repl[reno] = reno_split

      if levels:
        pg.advanceAndPlot(status=reno)

    cls._reno_repl = new_reno_repl.copy()
    log('Register ct parameter values and orders', 'info')
    levels = len(FRC.couplings_regged)
    pg = ProgressBar(levels)
    pg.plotProgress()
    for reno in FRC.couplings_regged:
      Counterterms.renos_order[reno] = FRC.coupling_orders[reno]
      pg.advanceAndPlot()

    cls.store(force=force)
    cls.RenoConst.store(force=force)
    Counterterms.store(force=force)
    log('CT parameters registered and order assigned.', 'info')

  @classmethod
  def leading_order_wavefunction(self, wave):
    from rept1l.parsing import Regularize
    tens_base = [Symbol(u) for u in Regularize.tens_dict]
    tens_repl = {}
    for tens in (tens for tens in tens_base if tens in self.renos[wave]):
      momentum_arg = Regularize.tens_dict[tens.name][2][0]
      if momentum_arg != 'M0':
        new_tens = Regularize.tens_dict[tens.name]
        # tensor not already declared with real momentum
        if not new_tens[0][-1] == 'R':
          new_tens = (new_tens[0] + 'R', new_tens[1], new_tens[2])
          if new_tens not in Regularize.tens_dict.values():
            tens_sym = 'TR' + str(len(Regularize.tens_dict))
            Regularize.tens_dict[tens_sym] = new_tens
            tens_repl[tens] = Symbol(tens_sym)
          else:
            tens_index = Regularize.tens_dict.values().index(new_tens)
            tens_sym = Regularize.tens_dict.keys()[tens_index]
            tens_repl[tens] = Symbol(tens_sym)
    self.renos[wave] = self.renos[wave].subs(tens_repl)

if __name__ == "__main__":
  import doctest
  doctest.testmod()
