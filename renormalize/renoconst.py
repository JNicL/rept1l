#==============================================================================#
#                                 renoconst.py                                 #
#==============================================================================#

from __future__ import print_function
from six import iteritems

from sympy import Symbol, sympify
import rept1l.Model as Model
from rept1l.helper_lib import StorageProperty
from six import with_metaclass

model = Model.model


class CountertermError(Exception):
  """ CountertermError exception is raised if a Counterterm parameter is not
  found.  """
  def __init__(self, value):
    self.value = value

  def __str__(self):
    return repr(self.value)


class ReturnConstantAndStorage(StorageProperty):
  """ Magic for returning counterterm parameter by __getitem__ defined on class
  itself.
  """
  def __init__(cls, *args, **kwargs):
    super(ReturnConstantAndStorage, cls).__init__(*args, **kwargs)

  def __getitem__(cls, ctparam):
    """ Searches for active `ctparam` and returns it. If ambigious, meaning that
    the same ctparam is renormalized differently and set active, dict of
    different renormalizations with values is returned.
    """
    ret = {}
    for renoscheme in cls.registry:
      try:
        ct = cls.return_ct(ctparam, renoscheme=renoscheme)
        if ct['active']:
          ret[renoscheme] = ct
      except Exception:
        pass

    if len(ret) == 1:
      return list(ret.values())[0]
    elif len(ret) == 0:
      raise CountertermError('No counterterm with name ' + str(ctparam) +
                             ' found.')
    else:
      return ret

  def __setitem__(cls, ctparam, ctdict):
    """  Sets the counterterm parameter dict.  """
    if type(ctparam) is str:
      symbol = Symbol(ctparam)
    elif type(ctparam) is Symbol:
      symbol = ctparam
    else:
      raise Exception('symbol must be str or Symbol type')

    renoscheme = ctdict['renoscheme']
    if renoscheme not in cls.registry:
      cls.registry[renoscheme] = {}

    cls.registry[renoscheme][symbol] = ctdict

  def __contains__(cls, ctparam):
    if type(ctparam) is str:
      symbol = Symbol(ctparam)
    elif type(ctparam) is Symbol:
      symbol = ctparam
    else:
      return False

    return symbol in cls.renosdict()


class RenormalizationConstant(with_metaclass(ReturnConstantAndStorage)):

  """ Storing renormalization constants """
  # StorageMeta attributes. Those attributes are stored when `store` is called.
  storage = ['registry']
  # StorageMeta storage path
  mpath, _ = model.get_modelpath()
  path = mpath + '/ModelRenormalization'
  # StorageMeta filename
  name = 'renormalization_constants.txt'

  def __init__(self, symbol, value, active=True, renoscheme='', real=False,
               store=True, original=True, scaledep=None,
               default=False):
    """ Initializes a new counterterm parameter.

    :param symbol: symbol of the renormalization constant
    :type  symbol: str or Symbol

    :param value: value in terms couplings and scalar integrals
    :type  value: sympy expression

    :param renoscheme: specify which scheme has been used to compute the
                       renormalization constant
    :type  renoscheme: str

    :param real: Set if the renormalization constant should forced to be real.
    :type  real: bool

    :param scaledep: Set the scale as string parameter at which the counterterm
                     has been defined. Only affects counterterms dependening on
                     DeltaUV.
    :type  scaledep: str

    :param default: If True this scheme is set as the default scheme in Recola2
                    in case multiple schemes are supported.
    :type  default: bool
    """

    if type(symbol) is str:
      self.symbol = Symbol(symbol)
    elif type(symbol) is Symbol:
      self.symbol = symbol
    else:
      raise Exception('symbol must be str or Symbol type')

    param_dict = {'value': value, 'renoscheme': renoscheme, 'active': active,
                  'basename': self.symbol.name, 'real': real,
                  'scaledep': scaledep, 'default': default}

    parent = self.__class__
    # load existing counterterms
    if not self.loaded:
      try:
        parent.load()
      except IOError:
        pass

    if renoscheme not in parent.registry:
      parent.registry[renoscheme] = {}

    parent.registry[renoscheme][self.symbol] = param_dict
    if store:
      parent.store()

  def __repr__(self):
    return self.symbol.name

  @classmethod
  def return_ct(cls, sym, renoscheme='', **kwargs):
    if type(sym) is str:
      symbol = Symbol(sym)
    elif type(sym) is Symbol:
      symbol = sym
    else:
      print("type(sym), sym:", type(sym), sym)
      raise CountertermError('sym must be str or Symbol type')

    if renoscheme not in cls.registry:
      raise CountertermError('No ct renormalized in scheme: ' + str(renoscheme))

    if symbol not in cls.registry[renoscheme]:
      raise CountertermError('ct ' + symbol.name + ' not found in scheme: ' +
                             str(renoscheme))

    return cls.registry[renoscheme][symbol]

  @classmethod
  def update_renodict(cls, sym, renodict, renoscheme):
    if type(sym) is str:
      symbol = Symbol(sym)
    elif type(sym) is Symbol:
      symbol = sym

    if renoscheme not in cls.registry:
      cls.registry[renoscheme] = {}
    cls.registry[renoscheme][symbol] = renodict

  @classmethod
  def apply_on_value(cls, sym, func, renoscheme=None):
    """ Applies the function `func` on the value of the parameter sym
    """
    if renoscheme:
      if type(sym) is str:
        symbol = Symbol(sym)
      elif type(sym) is Symbol:
        symbol = sym
      if renoscheme not in cls.registry:
        raise Exception('Renormalization scheme not found: ' + str(renoscheme))

      if symbol not in cls.registry[renoscheme]:
        raise Exception('CT not found for scheme: ' + str(renoscheme))

      ctdict = cls.registry[renoscheme][symbol]
    else:
      ctdict = cls[sym]

    newval = func(ctdict['value'])
    ctdict['value'] = newval

  @classmethod
  def remove_renodict(cls, sym, renoscheme):
    if type(sym) is str:
      symbol = Symbol(sym)
    elif type(sym) is Symbol:
      symbol = sym
    if renoscheme not in cls.registry:
      raise Exception('Renormalization scheme not found: ' + str(renoscheme))

    if symbol not in cls.registry[renoscheme]:
      raise Exception('CT not found for scheme: ' + str(renoscheme))

    del cls.registry[renoscheme][symbol]

  @classmethod
  def remove_renoscheme(cls, renoscheme):
    if renoscheme not in cls.registry:
      raise Exception('Renormalization scheme not found: ' + str(renoscheme))

    del cls.registry[renoscheme]

  @classmethod
  def apply_on_all_values(cls, func, active=True, plotprogress=True):
    """ Applies the function `func` on the value of every registered parameter.
    The parameters can be restricted to the active ones via `active`=True.
    """
    if active:
      syms = list(cls.renosdict().keys())
      if plotprogress:
        from rept1l.pyfort import ProgressBar
        PG = ProgressBar(len(syms))
      for symbol in syms:
        if plotprogress:
          PG.advanceAndPlot(status='simplifying ' + symbol.name)
        cls.apply_on_value(symbol, func)
    else:
      for renoscheme in cls.registry:
        for sym in cls.registry[renoscheme]:
          cls.apply_on_value(sym, func, renoscheme=renoscheme)

  @classmethod
  def renosdict(cls, multi_reno=False, *selection):
    """ Returns the counterterm parameter as {symbol: value} dictionary.
    The user can provide a selection:

    :param selection: counterterm parameters
    :type selection: Symbol or str
    """
    renos = {}
    select = len(selection) > 0
    for renoscheme in cls.registry:
      for reno in cls.registry[renoscheme]:
        renodict = cls.registry[renoscheme][reno]
        if reno in renos and renodict['active'] and not multi_reno:
          raise Exception('Renormalization constant ' + reno.name +
                          ' active in different renormalizations.')
        elif renodict['active']:
          if (not select) or (reno.name in selection or reno in selection):
            if reno not in renos and multi_reno:
              renos[reno] = {}
            if multi_reno:
              renos[reno][renoscheme] = cls.registry[renoscheme][reno]['value']
            else:
              renos[reno] = cls.registry[renoscheme][reno]['value']
    if select:
      true_selection = len(renos) == len(selection)
      if not true_selection:
        missing = ', '.join([str(u) for u in selection
                             if (sympify(u) not in renos)])
        err = ('Unable to give all solution to the selection. Missing: ' +
               missing)
        raise Exception(err)
    return renos

  @classmethod
  def set_active(cls, sym, renoscheme, active=True):
    renodict = cls.return_ct(sym, renoscheme)
    renodict['active'] = active
    cls.update_renodict(sym, renodict, renoscheme)

  @classmethod
  def is_active(cls, sym, renoscheme):
    renodict = cls.return_ct(sym, renoscheme)
    return renodict['active']

  @classmethod
  def build_ms_ct_solutions(cls, msrenos, plugin_ms_values=False,
                            scaledep='muMS', renoscheme='MS'):
    """ Constructs the msbar solutions for ct parameters given their value in
    some other scheme (e.g. onshell).

    :param msrenos: List of counterterm paramter
    :type  msrenos: list of symbols

    :param plugin_ms_values: Inserts the actual values for counterterms which if
                             `sym` depends on them.
    :type  plugin_ms_values: bool
    """

    from rept1l.pyfort import ProgressBar

    reno_sols = {}
    PG = ProgressBar(len(msrenos))
    for ct in msrenos:
      PG.advanceAndPlot(status=ct.name)
      try:
        val = cls.return_ct(ct.name, renoscheme=renoscheme)['value']
        if plugin_ms_values:
          reno_sols[ct] = val
        else:
          reno_sols[ct] = Symbol(ct.name)
      except CountertermError:
        try:
          val = cls.return_ct(ct.name + '_' + renoscheme,
                              renoscheme=renoscheme)['value']
          if plugin_ms_values:
            reno_sols[ct] = val
          else:
            reno_sols[ct] = Symbol(ct.name + '_' + renoscheme)
        except CountertermError:
          cls.extract_ms(ct, plugin_ms_values=plugin_ms_values,
                         scaledep=scaledep, renoscheme=renoscheme)
          if plugin_ms_values:
            val = cls.return_ct(ct.name + '_' + renoscheme,
                                renoscheme=renoscheme)['value']
            reno_sols[ct] = val
          else:
            reno_sols[ct] = Symbol(ct.name + '_' + renoscheme)
    return reno_sols

  @classmethod
  def extract_ms(cls, sym, is_value=False, plugin_ms_values=False,
                 ct_from_model=False, scaledep='muMS', renoscheme='MS'):
    """ Extracts the divergence of a renormalization constant or expression. If
    `sym` is a ct parameter the result is stored as a ct parameter
    with renoscheme `MS`. If `sym` should be treated as a normal expression
    `is_value` has to be set to True.

    :param sym: The CT parameter or expression from which the divergence is
                extracted.
    :type  sym: symbol or expression

    :param is_value: Declares `sym` to be an expression
    :type  is_value: bool

    :param plugin_ms_values: Inserts the actual values for counterterms which if
                             `sym` depends on them.
    :type  plugin_ms_values: bool
    """

    # check if ct param exists and return the MS result if already computed
    if not is_value:
      ctdict = cls[sym]
      try:
        if ctdict['renoscheme'] == renoscheme:
          return ctdict
        else:
          value = ctdict['value']
      # ct param with different renormalization found
      except KeyError:
        if renoscheme not in ctdict:
          # select the first one of the schemes and extract MS
          ctdict = ctdict[list(ctdict.keys())[0]]
          if ctdict['renoscheme'] == renoscheme:
            return ctdict
          else:
            value = ctdict['value']
        else:
          return ctdict[renoscheme]
    else:
      value = sym

    from rept1l.regularize import Regularize
    RS = Regularize()
    from rept1l.particles import ParticleMass
    ParticleMass.load()

    ct_dependence = cls.ctparameter_dependence(value,
                                               ct_from_model=ct_from_model)

    # if dependent on ct parameters -> compute their divergent part
    if len(ct_dependence) > 0:
      reno_repl = cls.build_ms_ct_solutions(ct_dependence,
                                            plugin_ms_values=plugin_ms_values,
                                            scaledep=scaledep,
                                            renoscheme=renoscheme
                                            )
    else:
      reno_repl = {}

    # Substitute real mass expressions by complex ones
    mass_repl = {Symbol(u.name): Symbol('c' + u.name)
                 for u in ParticleMass.masses.values()}

    # compute the divergent part which does not depend on ct parameter
    expr_wo_ct_dep = value.subs({u: 0 for u in reno_repl})
    if expr_wo_ct_dep != 0:
      val_ms = RS.extract_ms(expr_wo_ct_dep, on_tensor=False).subs(mass_repl)
    else:
      val_ms = 0

    # add the divergent parts comping from the ct parameter
    value = value.expand()  # expand is necessary for coeff to work
    for r, r_subs in iteritems(reno_repl):
      val_ms += value.coeff(r) * r_subs

    if not is_value:
      # construct a new ct parameter dict
      new_ctdict = {}
      new_ctdict['value'] = val_ms
      new_ctdict['active'] = True
      new_ctdict['renoscheme'] = renoscheme
      new_ctdict['scaledep'] = scaledep
      new_ctdict['default'] = False

      # Counterterm deleted when calling restore...
      new_ctdict['original'] = False

      if type(sym) is str:
        symbol = Symbol(sym + '_' + renoscheme)
      elif type(sym) is Symbol:
        symbol = Symbol(sym.name + '_' + renoscheme)

      cls[symbol] = new_ctdict
      return new_ctdict

    else:
      return val_ms

  @classmethod
  def ctparameter_dependence(cls, value, ct_from_model=False):
    """ Returns the counterterm parameters associated to the `value`.

    :param value: A valid sympy expression
    :type  value: expression

    :param ct_from_model: The pool of possible counterterm parameters can be
                          used from the modelfile. By default the storage of
                          RenormalizationConstant is used.
    :type  ct_from_model: bool
    """

    # construct the possible counterterm dependence (stored in ct)
    if ct_from_model:
      # load counterterms
      from rept1l.counterterms import Counterterms, get_wf_ct, get_param_ct
      Counterterms.load()
      # cts = Counterterms.cts
      # All counterterms in the theory except for wavefunctions
      c_ct = list(get_param_ct(model.param.all_parameters).values())

      # todo: Counterterms.get_all_ct() ??
      # All wavefunctions
      w_ct = []
      for particle in model.P.all_particles:
        for reno in get_wf_ct(particle):
          if reno not in w_ct:
            w_ct.append(reno)

      ct = c_ct + w_ct
    else:
      ct = []
      for renoscheme in cls.registry:
        for sym in cls.registry[renoscheme]:
          if sym not in ct:
            ct.append(sym)

    # search for occurences in value which are also contained in ct
    try:
      value_syms = value.free_symbols
      ct_found = [cc for cc in ct if cc in value_syms]
    except AttributeError:
      ct_found = []

    return ct_found

  @classmethod
  def renosdict_ordered(cls, *selection, **kwargs):
    """ Returns the list of ct parameters ordered according to their dependence
    on other ct parameter.

    Optionally the user can provide a pool of counterterms which should be
    ordered according to their dependence:

      - RenormalizationConstant.renosdict_ordered(ct1, ct2, ct2, ... )

    :param selection: list of couplings
    :type selection: *list
    """

    if 'multi_reno' in kwargs:
      multi_reno = kwargs['multi_reno']
    else:
      multi_reno = True

    select = len(selection) > 0
    if not select:
      selection = cls.renosdict(multi_reno=multi_reno)

    if 'ordered' in kwargs:
      ordered = kwargs['ordered']
    else:
      ordered = []

    renosdict_dep = {}
    for u in selection:
      renodict = cls[sympify(u)]
      # in case multiple renormalizations we determine the order by the
      # merging the dependence on other counterterms for all values
      if multi_reno and 'renoscheme' not in renodict:
        ctdep = set()
        for renoscheme in renodict.values():
          val = renoscheme['value']
          ctdep = ctdep | set(cls.ctparameter_dependence(val))
        renosdict_dep[sympify(u)] = list(ctdep)

      else:
        val = cls[sympify(u)]['value']
        renosdict_dep[sympify(u)] = cls.ctparameter_dependence(val)

    for u in list(renosdict_dep.keys()):
      dep = renosdict_dep[u]
      if dep == []:
        ordered.append(u)
        del renosdict_dep[u]
      else:
        if all((v in ordered) for v in dep):
          ordered.append(u)
          del renosdict_dep[u]
        else:
          err_lst = [v.name for v in dep
                     if (v not in ordered and v not in selection)]
          if len(err_lst) > 0:
            err_msg = ('The following counterterms must be set active: ' +
                       ', '.join(err_lst))
            raise Exception(err_msg)

    if len(renosdict_dep) > 0:
      new_selection = list(renosdict_dep.keys())
      return cls.renosdict_ordered(ordered=ordered, *new_selection)
    else:
      return ordered

  @classmethod
  def get_original(cls, return_original=True):
    """ Returns the original parameters which are not modified by
    register_ctparameter if `return_original` is True, else returns the
    non-original parameters. """
    ret = {}
    for renoscheme in cls.registry:
      for param_sym in cls.registry[renoscheme]:
        param = cls.registry[renoscheme][param_sym]
        if return_original:
          if ('original' not in param) or param['original'] is True:
            if renoscheme not in ret:
              ret[renoscheme] = []
            ret[renoscheme].append(param_sym)
        else:
          if ('original' in param) and param['original'] is False:
            if renoscheme not in ret:
              ret[renoscheme] = []
            ret[renoscheme].append(param_sym)
    return ret

  @classmethod
  def restore_original_status(cls):
    """ Removes non-original parameters and sets original ones active. """
    originals = cls.get_original()
    non_originals = cls.get_original(return_original=False)
    for renoscheme in non_originals:
      for param_sym in non_originals[renoscheme]:
        cls.remove_renodict(param_sym, renoscheme)

    for renoscheme in originals:
      for param_sym in originals[renoscheme]:
        cls.set_active(param_sym, renoscheme)

  @classmethod
  def get_coupling_dependence(cls, selection=[], multi_reno=True, **kwargs):
    """ Returns a list of couplings on which the currently active
    renormalization parameter depend. """

    couplings = set()
    select = len(selection) > 0
    if not select:
      selection = cls.renosdict(multi_reno=multi_reno)

    for u in selection:
      renodict = cls[sympify(u)]

      if multi_reno and 'renoscheme' not in renodict:
        cdep = set()
        for renoscheme in renodict.values():
          val = renoscheme['value']
          try:
            ccs = set(v for v in val.free_symbols if str(v).startswith('GC'))
            cdep = cdep | ccs
          except AttributeError:
            pass
      else:
        val = renodict['value']
        cdep = set(v for v in val.free_symbols if str(v).startswith('GC'))
      couplings = couplings | cdep
    return couplings

  @classmethod
  def export_mathematica(cls, sym, output, sym_mname=None,
                         export_func=None, **kwargs):
    """ Writes the expression for `sym` as mathematica code.

    :param sym: counter term symbol
    :type  sym: str or sympy expression

    :param output: writes the result to an output file
    :type  output: str

    :param sym_mname: name for mathematica symbol
    :type  sym_mname: str or sympy expression

    :param export_func: function to simplify expression
    :type  export_func: function

    """
    ct = cls.return_ct(sym, **kwargs)

    from sympy import mathematica_code as mcode
    expr = ct['value'].expand()
    td = sorted(u.name for u in expr.free_symbols if u.name.startswith('T'))

    if sym_mname:
      symm = sym_mname
    else:
      symm = str(sym)

    SIlist = []

    from rept1l.pyfort import ProgressBar
    if len(td) > 0:
      PG = ProgressBar(len(td))

    # scalar integral coefficients
    from rept1l.regularize import Regularize as RR
    tw = symm + ' = '
    for t in td:
      PG.advanceAndPlot()
      ts = RR.tens_dict[t]
      tsexpr = (ts[0] + ts[1] + '[' + ','.join(ts[2])) + ']'
      SIlist.append(tsexpr)
      if export_func:
        coeffexp = export_func(expr.coeff(t), plotProgress=False)
      else:
        coeffexp = expr.coeff(t)
      tw += '+1*' + tsexpr + ' * (' + mcode(coeffexp) + ')'

    # rational part and other counterterm dependence
    if export_func:
      tw_r = symm + 'R = ' + mcode(export_func(expr.subs({u: 0 for u in td})))
    else:
      tw_r = symm + 'R = ' + mcode(expr.subs({u: 0 for u in td}))
    SIlist = 'si = {' + ','.join(SIlist) + '}'
    with open(output, 'w+') as f:
      f.write(tw)
      f.write('\n')
      f.write(tw_r)
      f.write('\n')
      f.write(SIlist)

  @staticmethod
  def convert_mathematica(strexpr, assign_tensors=True):
    """ Converts the string expression `strexpr` to sympy.

    :param strexpr: Mathematica expression as string
    :type  strexpr: str

    :param assign_tensors: Using internal representation for scalar integrals
    :type  assign_tensors: bool
    """
    from rept1l.helper_lib import string_subs
    expr = string_subs(strexpr, {'[': '(', ']': ')', '^': '**', 'Sqrt': 'sqrt'})
    if assign_tensors:
      from rept1l.regularize import Regularize as RR
      RR.load()
      r = RR()
      RR.store()
      expr = sympify(r.assign_tensors(expr, RR.tens_dict))
    else:
      expr = sympify(expr)
    return expr
