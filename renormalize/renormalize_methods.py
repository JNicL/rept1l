#==============================================================================#
#                            renormalize_methods.py                            #
#==============================================================================#

#============#
#  Includes  #
#============#

import sys
import os
import re

from sympy import Symbol, I
from rept1l.logging_setup import log
from rept1l.combinatorics import flatten
import rept1l_config

import rept1l.Model as Model
model = Model.model

from .renormalize_utils import cms_correction
from .renormalize_template import RenormalizeTemplate

#===========#
#  Methods  #
#===========#

#==============================================================================#
#                           class RenormalizeTadpole                           #
#==============================================================================#

class RenormalizeTadpole(RenormalizeTemplate):
  # particle types
  scalars = 1
  fermions = 2
  vectors = 3

  # onshell scheme.
  onshell_scheme = {scalars: 'scalar_tadpole', 'kwargs': {'scheme': 'onshell'}}
  # momentum subtraction scheme
  msmom_scheme = {scalars: 'scalar_tadpole', 'kwargs': {'scheme': 'MSMOM'}}
  # MS scheme
  ms_scheme = {scalars: 'scalar_tadpole', 'kwargs': {'scheme': 'MS'}}

  # Label the schemes
  schemes = {'onshell': onshell_scheme,
             'MSMOM': msmom_scheme,
             'MS': ms_scheme}

  optional_args = {'particle_out': None,
                   'renoscheme': 'onshell'}

  def __init__(self, particle, index=1, tabs=1, tadpole_expr='dt', **kwargs):
    """
    :param particle: particle for which the tadpole should be computed.
    :type  particle: string

    :param index: process index
    :type  index: integer
    """
    super(self.__class__, self).__init__(**kwargs)
    self.set_process_defs(particle, **kwargs)
    self.set_regularization_defs()
    self.regularize()

    self.setup_renormalization()
    tadpole = self.renorm_conditions[0]
    if len(self.renorm_conditions) > 1:
      warnmsg = 'Found more than one renormalization condition.'
      log(warnmsg, 'warning')
      msg = 'renorm_conditions:' + str(self.renorm_conditions)
      log(msg, 'debug')
      msg = 'len(renorm_conditions):' + str(len(self.renorm_conditions))
      log(msg, 'debug')

    self.solutions = {Symbol(tadpole_expr): tadpole*I}

    msg = 'Fixed tadpole counterterm.'
    log(msg, 'info')
    self.status = 'success'
    self.ls_dict = self.Reg.ls_dict

#==============================================================================#
#                            RenormalizeSelfenergy                             #
#==============================================================================#

class RenormalizeSelfenergy(RenormalizeTemplate):
  """ Derives the expression for the renormalized selfenergy and solves the
  renormalization condition.
  """

  # check if default is overwritten by rept1l_config
  if(hasattr(rept1l_config, 'complex_mass_scheme') and not
     rept1l_config.complex_mass_scheme):
    include_cms_correction = False
    print ('Disabled complex-mass scheme correction. ' +
           'Note that the system will not take ' +
           'the real part of the counterterm expression which has to be done ' +
           'by hand.')
  else:
    # default
    include_cms_correction = True

  if(hasattr(rept1l_config, 'realmomentum') and not
     rept1l_config.realmomentum):
    realmomentum = False
    print ('Disabled complex-mass scheme correction. ' +
           'Note that the system will not take ' +
           'the real part of the counterterm expression which has to be done ' +
           'by hand.')
  else:
    # default
    realmomentum = True

  optional_args = {'particle_out': None,
                   'renoscheme': '',
                   'solve_eq': True,
                   'cts': None,
                   'simplify_ct_dep': False,
                   'mass_reg': False,
                   'realmomentum': realmomentum,
                   'include_cms_correction': include_cms_correction}

  # particle types
  scalars = 1
  fermions = 2
  vectors = 3

  # different cases for massive and massless renormalization
  massless = True
  massive = False

  # Define the different renormalization schemes. The corresponding
  # regularization methods are specified as strings and must exist in the class
  # Regularization

  # onshell scheme.
  onshell_scheme = {scalars: {massless: 'onshell_massive_scalar',
                              massive: 'onshell_massive_scalar'},
                    fermions: 'onshell_fermion',
                    vectors: {massless: 'onshell_massless_vector',
                              massive: 'onshell_massive_vector'}
                    }

  # momentum subtraction scheme
  msmom_scheme = {vectors: {massless: 'CWZ_massless_vector'},
                  fermions: 'CWZ_fermion', 'kwargs': {'scheme': 'MSMOM'},
                  scalars: 'CWZ_scalar', 'kwargs': {'scheme': 'MSMOM'}}

  # minimal subtraction (MS) scheme
  ms_scheme = {vectors: {massless: 'CWZ_massless_vector',
                         massive: 'ms_massive_vector'},
               fermions: 'CWZ_fermion',
               scalars: 'ms_scalar',
               'kwargs': {'scheme': 'MS'}}

  # Label the schemes
  schemes = {'MSMOM': msmom_scheme,
             'MS': ms_scheme,
             'onshell': onshell_scheme}

  def __init__(self, particle, index=1, tabs=1, *args, **kwargs):
    """
    :param particle: particle for which the tadpole should be computed.
    :type  particle: string

    :param index: process index
    :type  index: integer
    """

    super(self.__class__, self).__init__(**kwargs)
    particle_out = particle if not self.particle_out else self.particle_out
    particles = [particle, particle_out]
    self.set_process_defs(*particles, **kwargs)
    self.set_regularization_defs()
    self.regularize()

    self.determine_particle_ct_dependence()

    self.setup_renormalization()
    if self.solve_eq:
      self.solve_renormalization()
      if self.status == 'success' and self.include_cms_correction and self.unstable:
        self.apply_cms_correction()

    else:
      log('Derived renormalization conditions.', 'info')
      self.status = 'success'
      self.ls_dict = self.Reg.ls_dict

  def determine_particle_ct_dependence(self):
    """ Derives all counterterm parameters associated to the particles in the
    selected vertex. """

    if self.cts is None:
      # guess possible counterterm parameters
      #  -> wavefunctions andmass counterterms of participating particles
      from rept1l.counterterms import get_param_ct, get_wf_ct
      renos = set()
      for particle in self.particles:
        renos = renos.union(set(get_wf_ct(particle)))
      if self.particles[0].mass != model.param.ZERO and not self.force_massless:
        m_cts = list(get_param_ct([particle.mass]).values())
        if len(m_cts) == 1:
          renos.add(m_cts[0])

        elif len(m_cts) > 1:
          warnmsg = 'More than one mass counterterm encountered!'
          log(warnmsg, 'warning')
      msg = ('Potentially qualify for renormalization constants: ' +
             ', '.join(u.name for u in renos))
      log(msg, 'info')
      self.renos = list(set(renos))
    else:
      self.renos = list(set(self.cts))

  def apply_cms_correction(self):
    from rept1l.counterterms import get_param_ct
    m_cts = list(get_param_ct([self.particles[0].mass]).values())
    if len(m_cts) > 0:
      mct = m_cts[0]
      msg = ('Attempt to compute CMS expansion correction for: ' +
             str(mct))
      log(msg, 'info')
      if mct in self.solutions:
        mct_sol = self.solutions[mct]
        ct = cms_correction(mct_sol, self.particles[0].mass.name)
        if ct is not None:
          self.solutions[mct] += ct
      else:
        msg = ('Unable to compute CMS expansion correction. ' +
               'Mass counterterm not in solutions.')
        log(msg, 'warning')

#==============================================================================#
#                           class RenormalizeVertex                            #
#==============================================================================#

class RenormalizeVertex(RenormalizeTemplate):
  optional_args = {'renoscheme': 'MSMOM', 'solve_eq': True,
                   'simplify_expr': None, 'ct': None, 'reno_sols': {},
                   'compute_wavefunctions': True, 'simplify_ct_dep': False}

  # momentum subtraction scheme
  msmom_scheme = ['MS_subtraction', {'MSMOM': True}]

  # minimal subtraction (MS) scheme
  ms_scheme = ['MS_subtraction', {'MSMOM': False}]

  # Label the schemes
  schemes = {'MSMOM': msmom_scheme,
             'MS': ms_scheme}

  def __init__(self, vertex, index=1, tabs=1, *args, **kwargs):
    """
    :param particle: particle for which the tadpole should be computed.
    :type  particle: string

    :param index: process index
    :type  index: integer
    """
    super(self.__class__, self).__init__(**kwargs)
    self.vertex = vertex
    particles = self.vertex.particles
    self.set_process_defs(*particles, **kwargs)
    self.set_regularization_defs()
    self.regularize()

    self.determine_vertex_ct_dependence()

    self.setup_renormalization()
    if len(self.renorm_conditions) > 1:
      log('Restricting renormalization condition to one equation.', 'warning')
      if type(self.renorm_conditions) is list:
        self.renorm_conditions = [self.renorm_conditions[0]]
      else:
        renokey = self.renorm_conditions.keys()[0]
        self.renorm_conditions = {renokey: self.renorm_conditions[renokey]}
    if self.solve_eq:
      self.solve_renormalization()
      if self.status == 'success':
        sol = self.solutions
        self.solutions = {s: sol[s].subs(self.reno_sols) for s in sol}
    else:
      log('Derived renormalization conditions.', 'info')
      self.status = 'success'
      self.ls_dict = self.Reg.ls_dict

  def determine_vertex_ct_dependence(self):
      from rept1l.counterterms import get_wf_ct
      renos = []
      for particle in self.vertex.particles:
        for reno in get_wf_ct(particle):
          if reno not in renos:
            renos.append(reno)

      if self.compute_wavefunctions:
        # get all different external particles, particle and antiparticle are
        # considered as the same type
        wf_to_compute = self.particle_types(self.vertex.particles)
        reno_sols = self.get_wavefunctions(wf_to_compute, **self.reg_kwargs)
      else:
        reno_sols = {u: Symbol(u.name + '_' + self.renoscheme) for u in renos}

      # check if the vertex counterterm is uniquely defined.
      ct = self.unique_vertex_counterterm(self.vertex, ct_provided=self.ct,
                                          sols_provided=self.reno_sols)
      self.renos = [ct]

      for r in reno_sols:
        if r not in self.reno_sols:
          log("Adding " + str(r) + " to CT solutions.", 'info')
          self.reno_sols[r] = reno_sols[r]

  @staticmethod
  def get_vertex_counterterms(vertex):
    """ Returns the counterterm parameter associated to the couplings of vertex.
    """
    from rept1l.coupling import RCoupling as FRC
    # load counterterms
    from rept1l.counterterms import Counterterms, get_param_ct
    Counterterms.load()
    cts = Counterterms.cts
    # Determine which counterterm should be computed

    # Get all possible counterterm parameters in the theory, no field
    # renormalization constants included
    c_ct_ext = list(get_param_ct(model.param.all_parameters).values())
    # Add derived (internal) counterterm parameters
    ct_derv = [Symbol(u) for u in Counterterms.derived_ct_parameters.values()]
    c_ct = c_ct_ext + ct_derv

    # couplings to the vertex which also a counterterm expansion
    vertex_ct = flatten([[u for u in cts[cc.name].free_symbols] for cc in
                         vertex.couplings.values() if cc.name in cts])
    vertex_ct = [v for v in set([u for u in vertex_ct])]

    # include  couplings to the CT vertex
    if type(vertex) is model.object_library.CTVertex:
      ct_vertex = flatten([[u for u in FRC.coupling_values[cc.name].
                          free_symbols] for cc in vertex.couplings.values()])
      ct_vertex = [v for v in set([u for u in ct_vertex])]
    else:
      ct_vertex = []
    # Determine which of the c_ct are also in vertex_ct
    ct_found = set(cc for cc in c_ct if cc in vertex_ct or cc in ct_vertex)

    # replace derived counterterm parameters by their dependence on external
    # ones
    c_ct_derv = [u for u in ct_derv if u in ct_found]
    if len(c_ct_derv) > 0:
      for ctd in c_ct_derv:
        for ct in Counterterms.derived_ct_values[ctd.name].free_symbols:
          if ct in c_ct_ext:
            ct_found.add(ct)
        ct_found.remove(ctd)

    return ct_found

  @staticmethod
  def get_vertex_wavefunctions(vertex, diagonal=True, mixback=True):
    """ Returns the counterterm parameter associated to the couplings of vertex.

    :param vertex: the UFO Vertex
    :type  vertex: Vertex

    :param mixback: If true, wavefunction are added which mix into `vertex`.
    :type  mixback: bool
    """
    from rept1l.counterterms import get_wf_ct
    ct_found = set()
    for particle in vertex.particles:
      for reno in get_wf_ct(particle, diagonal=diagonal):
        ct_found.add(reno)
    if mixback:
      from rept1l.helper_lib import get_mixback_wavefunctions
      ct_found = ct_found | get_mixback_wavefunctions(vertex)
    return ct_found

  def unique_vertex_counterterm(self, vertex, ct_provided=None,
                                sols_provided=[]):
    """ Verifies if the vertex uniquely defines a counterterm parameter or if
    the user has provided an appropriate candidate for a counterterm parameter.
    In case of missing counterterm parameters it is pointed out which
    counterterm parameters the user must provide.
    """

    ct_found = self.get_vertex_counterterms(vertex)

    if ct_provided is None and self.solve_eq:
      # If no counterterm parameter is provided we assume that the counterterm
      # is uniquely defined in the vertex
      if len(ct_found) != 1:
        if len(ct_found) > 1:
          ct_list = ', '.join(u.name for u in ct_found)
          err = ('Multiple counterterm expressions found in vertex. ' +
                 'Either specify the counterterm or provide solutions for ' +
                 'all other counterterm parameter. CT parameters found: ' +
                 ct_list)
        elif len(ct_found) == 0:
          err = ('No counterterm expressions found in vertex. ' +
                 'Verify the vertex expansion')
        raise Exception(err)
      else:
        ct = ct_found.pop()
    else:
      ct = ct_provided
      other_ct = [u for u in ct_found if u != ct]
      other_ct = [v for v in other_ct if v not in sols_provided]
      if len(other_ct) > 0:
        msg = ('User has to plug in the solutions for: ' +
               ', '.join(u.name for u in other_ct))
        log(msg, 'info')
    return ct

  def particle_types(self, particles):
    """ Maps anitparticles to particles and returns the set of different
    particle types.
    """
    from rept1l.parsing import get_antiparticle, is_antiparticle
    p = [u if not is_antiparticle(u.name) else get_antiparticle(u)
         for u in particles]
    wf_to_compute = set(p)

    return wf_to_compute

  def get_wavefunctions(self, particles, **kwargs):
    """ Computes the wavefunction corrections for all particles in `particles`.
    Assumes that there is no particle mixing. Otherwise the user has to provide
    the solutions via `reno_sols`.
    """
    from rept1l.counterterms import get_wf_ct, get_param_ct
    from rept1l.parsing import get_antiparticle
    reno_sols = {}
    reg_kwargs = kwargs.copy()
    for p in particles:
      msg = 'Computing wavefunction renormalizations for ' + p.name
      log(msg, 'info')
      cts = get_wf_ct(p, diagonal=True)
      cts_anti = get_wf_ct(get_antiparticle(p), diagonal=True)
      cts = [u for u in set(cts + cts_anti)]
      if p.mass != model.param.ZERO:
        m_cts = list(get_param_ct([p.mass]).values())
        if len(m_cts) == 1:
          cts += [m_cts[0]]
      reg_kwargs['cts'] = cts

      if len(cts) == 0:
        log('No CT assigned to particle ' + p.name +
            '. Skipping particle renormlaization.', 'error')
        continue

      rs = RenormalizeSelfenergy(p, renoscheme=self.renoscheme,
                                 **reg_kwargs)
      if rs.status == 'success':
        reno_sols.update(rs.solutions)
      else:
        msg = 'failed renormalization: ' + str(p)
        log(msg, 'error')
        sys.exit()
    return reno_sols

if __name__ == "__main__":
  import doctest
  doctest.testmod()
