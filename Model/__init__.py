#==============================================================================#
#                                 __init__.py                                  #
#==============================================================================#
from __future__ import print_function

#============#
#  Includes  #
#============#

import os
import sys
import imp
import rept1l_config
from rept1l.logging_setup import log

# Assume python2 version
if sys.version_info.major > 2:
  log('REPT1L with python3 is currently under development. ' +
      'Major sys version: ' + str(sys.version_info.major), 'warning')

# Environment name for Rept1l(UFO) model file path
if hasattr(rept1l_config, 'rept1l_environ'):
  model_env_name = rept1l_config.rept1l_environ
else:
  model_env_name = 'REPTIL_MODEL_PATH'

# load Rept1l(UFO) model file as python module with a dynamic path
if model_env_name not in os.environ:
  raise ImportError('No Model set active. Export `' + model_env_name +
                    '` to the full path of the model.')

else:
  mpath = os.path.expandvars(os.environ[model_env_name])
  try:
    desc = ('', '', imp.PKG_DIRECTORY)
    model = imp.load_module('model', None, mpath, desc)
    if model.__package__ is None:
      raise ImportError('Tried to import the model from `' + mpath +
                        '`, but failed. Make sure there is a valid Modelfile.')

  except:
    print("Failed to load model file!")
    raise

# Copy UFO model information
if hasattr(model.model, '__author__'):
  __author__ = model.model.__author__
else:
  __author__ = 'Unknown author'
if hasattr(model.model, '__date__'):
  __date__ = model.model.__date__
else:
  __date__ = 'No date'
if hasattr(model.model, '__version__'):
  __version__ = model.model.__version__
else:
  __version__ = 'No version'
