#==============================================================================#
#                                 __init__.py                                  #
#==============================================================================#

from .tensoradd import ATensor as tensor
from .tensoradd import simplify, DeltaTensor, OneTensor
from .tensormul import MTensor, sub_args
from .tensorarg import TensorArg

tensor_arg = TensorArg
diracAlgebra = MTensor.LS

__author__ = "Jean-Nicolas lang"
__date__ = "29. 5. 2016"
__version__ = "0.9"
