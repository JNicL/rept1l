###############################################################################
#                                tensorarg.py                                 #
###############################################################################

#############
#  Imports  #
#############

from sympy import Symbol

###############################################################################
#                                  TensorArg                                  #
###############################################################################

class TensorArg(object):
  """ TensorArg class defines tensor arguments used in other classes.

  :param domain: Range of the index, e.g. [0, 1]
  :type  domain: list
  :param resolve: True if the argument should be expanded on its range,
                  False if the symbolic expression should be kept.
  :type  resolve: bool
  :param metric: Assign a metric to the index, which is used when contracted
                 with another TensorArg.
  :type  metric: Matrix
  """
  optional_args = ['domain', 'resolve', 'metric']
  options = ['reuse']

  def construct_get(get_var):
    def get_func(cls):
      return getattr(cls, '__' + get_var)
    return get_func

  def construct_set(set_var):
    def set_func(cls, set_value):
      if getattr(cls, '__' + set_var) != set_value:
        errmsg = ('Trying to modify tensor argument `' + set_var +
                  '` which is not permitted!')
        raise Exception(errmsg)
        if not cls.changed:
          setattr(cls, '__original_' + set_var, getattr(cls, '__' + set_var))
        cls.changed = True

      if set_var == 'domain':
        try:
          setattr(cls, '__' + set_var, list(set_value))
        except ValueError:
          setattr(cls, '__' + set_var, set_value)
      else:
        setattr(cls, '__' + set_var, set_value)
    return set_func

  # optional args are realized as attributes. When an attribute is changed
  # the argument is tagged with `changed=True` signalling that tensor may need
  # to be reconstructed.
  resolve = property(construct_get('resolve'), construct_set('resolve'))
  domain = property(construct_get('domain'), construct_set('domain'))
  metric = property(construct_get('metric'), construct_set('metric'))

  def __init__(self, symbol, **kwargs):
    self.changed = False
    for optional_arg in TensorArg.optional_args:
      if optional_arg in kwargs:
        if optional_arg == 'domain':
          try:
            setattr(self, '__' + optional_arg, list(kwargs[optional_arg]))
          except TypeError:
            setattr(self, '__' + optional_arg, kwargs[optional_arg])
        else:
          setattr(self, '__' + optional_arg, kwargs[optional_arg])
      else:
        setattr(self, '__' + optional_arg, None)

    if type(symbol) is int:
      self.symbol = symbol
    elif type(symbol) is str:
      try:
        if str(int(symbol)) == symbol:
          self.symbol = int(symbol)
        else:
          raise Exception('Not sure how to handle tensor argument: '
                          + str(symbol))
      except ValueError:
        self.symbol = symbol
    else:
      raise Exception('Not sure how to handle tensor argument: '
                      + str(symbol))

  def __str__(self):
    if self.symbol is not None:
      if type(self.symbol) is Symbol:
        return self.symbol.name
      elif type(self.symbol) is str:
        return self.symbol
      elif type(self.symbol) is int:
        return str(self.symbol)
      else:
        return 'Unknown tensor arg type'
    else:
      return 'Unnamed tensor arg'

  def __repr__(self):
    if self.symbol is not None:
      return str(self)
    else:
      return 'Unnamed tensor arg'

  def __eq__(self, other):
    """ Comparison is done for the symbol. If all attributes agree True is
    returned. If the symbols agree, then True and the list of attributes which
    do not agree is returned. False is returned if the symbols do not agree.

    >>> ta1 = TensorArg('i', domain=range(2))
    >>> ta2 = TensorArg('i', domain=range(2))
    >>> ta3 = TensorArg('i', domain=range(4))
    >>> ta4 = TensorArg('j', domain=range(4))
    >>> ta1 == ta2
    True
    >>> ta1 == ta3
    (True, ['domain'])
    >>> ta1 == ta4
    False
    """
    ret = self.symbol == other.symbol
    opt_mismatch = []
    for opt in TensorArg.optional_args:
      if hasattr(self, opt) and hasattr(other, opt):
        if getattr(self, opt) != getattr(other, opt):
          opt_mismatch.append(opt)
      elif (not hasattr(self, opt)) and (not hasattr(other, opt)):
        pass
      else:
        return False
    if len(opt_mismatch) > 0 and ret is True:
      return ret, opt_mismatch
    else:
      return ret

  def __hash__(self):
    return hash(self.symbol)

  def __ne__(self, other):
    """ Returns True if TensorArg symbols agree, else False is returned """
    ret = self.__eq__(other)
    if type(ret) is tuple or ret is False:
      return True
    else:
      return False

  def copy(self, **kwargs):
    """ Build a copy from the instance. Overwrite attributes of self with the
    ones listed in `kwargs`.
    >>> ta = TensorArg('i', domain=range(2), resolve=True)
    >>> tac = ta.copy()
    >>> tac.resolve
    True
    >>> tac.domain
    [0, 1]
    >>> tac == ta
    True
    >>> tac = TensorArg('i', domain=range(2), resolve=False)
    >>> tac == ta
    (True, ['resolve'])
    """
    opts = {opt: kwargs[opt] if opt in kwargs else getattr(self, '__' + opt)
            for opt in self.optional_args}
    new_dict = {k: kwargs[k] if k in kwargs else self.__dict__[k]
                for k in self.__dict__}
    new_dict.update(opts)
    return TensorArg(**new_dict)

  def copy_original(self):
    """ Build a copy from the instance making sure that the original attributes
    are copied
    #>>> ta = TensorArg('i', domain=range(2), resolve=True)
    #>>> ta.resolve = False
    #>>> tac = ta.copy_original()
    #>>> tac == ta
    #(True, ['resolve'])
    """
    opts = {opt: getattr(self, '__' + opt) for opt in self.optional_args}
    opts_orig = {opt: getattr(self, '__original_' + opt)
                 for opt in self.optional_args
                 if hasattr(self, '__original_' + opt)}
    new_dict = self.__dict__
    new_dict.update(opts)
    new_dict.update(opts_orig)
    return TensorArg(**new_dict)

if __name__ == "__main__":
  import doctest
  doctest.testmod()
