###############################################################################
#                           tensormultiplication.py                           #
###############################################################################

import re
from sympy import Symbol, Matrix, sympify, symbols, Dummy

from rept1l.combinatorics import fold, pair_arguments
from rept1l.tensor.dirac import diracAlgebra
from rept1l.tensor.tensorarg import TensorArg

###############################################################################
#                                 DomainError                                 #
###############################################################################

class DomainError(Exception):
  """
  DomainError exception is raised if the user specified a wrong domain, e.g.
  ranges of args and metrics, wrong tensor multiplication/addition etc.
  """
  def __init__(self, value):
    self.value = value

  def __str__(self):
    return repr(self.value)

###############################################################################
#                                  sub_args                                   #
###############################################################################

def sub_args(expression, repl_dict):
  keys = repl_dict.keys()
  new_dict = dict()
  for key in keys:
    key_to_arg = ['\,\s*' + key + '\s*\)', '\(\s*' + key + '\s*\)',
                  '\(\s*' + key + '\s*\,', '\,\s*' + key + '\s*\,']
    for arg in key_to_arg:
      pattern = re.compile(arg)
      match = pattern.search(expression)
      if match is not None:
        expr = match.group(0)
        new_res = re.sub(key, repl_dict[key], expr)
        expression = re.sub(arg, new_res, expression)

  return expression

###############################################################################
#                          find_tensor_valued_symbol                          #
###############################################################################

def find_tensor_valued_symbol(expr, search_rules):
  ret = []
  args = expr.args
  if (len(args) > 0):
    for term in expr.args:
      ret.extend(find_tensor_valued_symbol(term, search_rules))
  else:
    for key in search_rules.keys():
      rule = search_rules[key]
      match = rule.search(str(expr))
      if match:
        tmp = (expr, key)
        ret.append(tmp)
  return ret


###############################################################################
#                               get_dummies_set                               #
###############################################################################

def get_dummies_set(expression):
  dummies_list = set()
  expression_args = sympify(expression).args
  if len(expression_args) > 0:
    for expr in expression_args:
      ret = get_dummies_set(expr)
      if ret is not None:
        dummies_list = dummies_list.union(ret)
    return dummies_list
  else:
    if isinstance(expression, Dummy):
      dummies_list.update([expression])
      return dummies_list
    else:
      return []


###############################################################################
#                                   MTensor                                   #
###############################################################################

class MTensor(object):
  """ A tensor class allowing to define indexed objects.

  Tensor objects can be multiplied, having same or different arguments. The
  result of multiplication is a new tensor with a possibly new arguments domain.

  Pairs of negative arguments  are assumed to be summed.  If specified, the
  tensor object may be linked to numerical values. If desired, it is possible
  that the tensor expression is expanded to numerical values. To this end it is
  necessary that the corresponding arguments are `resolved`. It is possible to
  to a symbolic expansion which requires that the tensor has been assigned a
  symbol. It is possible to assign numerical and symbol at the same time. The
  system will expand numerically if all arguments are necessary are resolved,
  otherwise the symbolic expansion is performed.
  It is possible to change arguments for a composed tensor and to reconstruct
  the the tensor with the new argument properties.
  """
  LS = diracAlgebra()

  #def get_expten(cls):
    #return cls._expanded_tensor

  #def set_expten(cls, expten):
    ##if self.lazy and self._expanded_tensor == []:
    #cls.expand()
    #cls._expanded_tensor = expten
  #expanded_tensor = property(get_expten, set_expten)

  @property
  def expanded_tensor(self):
    if self._expanded_tensor is None:
      ret = self.reconstruct_tensor(lazy=False)
      self._expanded_tensor = ret._expanded_tensor
    return self._expanded_tensor

  @expanded_tensor.setter
  def expanded_tensor(self, expt):
    self._expanded_tensor = expt

  def __init__(self, args, numerical=None, symbol=None,
               expand=True, is_primitive=True, delta=False,
               zero_expand=False, block_reconstruction=False,
               lazy=True):
    self.symbol = symbol
    self.numerical = numerical
    self.changed = False
    self.delta = delta

    self._expanded_tensor = None
    self.args_expanded = []

    self.paired_arguments = []
    self.is_primitive = is_primitive
    self.mstrucs = []

    self.primitive_substructures = []
    self.derived_tensor = None
    self.block_reconstruction = block_reconstruction

    self.lazy = lazy

    self.args = []
    #args_expanded = []
    for arg in args:
      self.args.append(arg)
      #if arg.resolve:
        #args_expanded.append(arg.domain)
      #else:
        #args_expanded.append([arg.symbol])
    #if len(self.args) > 0:
      #self.paired_arguments = pair_arguments(args_expanded)
    self.derive_paired_arguments()

    self.set_all_args_resolved()

    if expand and not lazy:
      self.expand(zero_expand=zero_expand)

  def derive_paired_arguments(self):
    args_expanded = []
    if len(self.args) > 0:
      for arg in self.args:
        if arg.resolve:
          args_expanded.append(arg.domain)
        else:
          args_expanded.append([arg.symbol])
        self.paired_arguments = pair_arguments(args_expanded)

  def set_all_args_resolved(self):
    """ Set the flag of whether all args are resolved or none of the args is
    resolved """
    self.all_args_resolved = True
    self.no_arg_resolved = True
    for arg in self.args:
      self.all_args_resolved = self.all_args_resolved and arg.resolve
      self.no_arg_resolved = self.no_arg_resolved and not arg.resolve

  def copy(self, copy_mstrucs=True, lazy=True):
    """ Builds a copy of the MTensor including a copy of all arguments. """
    #  Take the dict as a basis of the new MTensor
    new_args = [u.copy() for u in self.__dict__['args']]
    ret = MTensor(new_args, numerical=self.numerical, symbol=self.symbol,
                  expand=False, is_primitive=self.is_primitive, delta=self.delta,
                  block_reconstruction=self.block_reconstruction,
                  lazy=lazy)
    if self.block_reconstruction:
      ret.expanded_tensor = (self.expanded_tensor)[:]
      ret.paired_arguments = self.paired_arguments[:]

    # only copy top-level mstrucs which carries all information on performed
    # multiplications
    if copy_mstrucs:
      mstrucs_copy = [u.copy(copy_mstrucs=copy_mstrucs, lazy=lazy)
                      for u in self.mstrucs]
      ret.mstrucs = mstrucs_copy

    return ret.reconstruct_tensor(lazy=lazy)

  def __str__(self):
    return self.expanded_tensor

  """
  # TODO: (nick 2015-05-06) Implement slicing according to
  http://stackoverflow.com/questions/2936863/
    python-implementing-slicing-in-getitem

  def __getitem__( self, key ) :
    if isinstance( key, slice ) :
        #Get the start, stop, and step from the slice
        return [self[ii] for ii in xrange(*key.indices(len(self)))]
    elif isinstance( key, int ) :
        if key < 0 : #Handle negative indices
            key += len( self )
        if key >= len( self ) :
            raise IndexError, "The index (%d) is out of range."%key
        return self.getData(key) #Get the data from elsewhere
    else:
        raise TypeError, "Invalid argument type.
  """

  def __getitem__(self, args):
    if len(args) != len(self.args):
      raise DomainError('Number of args do not match! IN: ' + str(len(args))
                        + ' TENSOR: ' + str(len(self.args)))
    if args in self.paired_arguments:
      arg_id = self.paired_arguments.index(args)
      return self.expanded_tensor[arg_id]

    # scalar case
    elif self.paired_arguments == [] and args == ():
      return self.expanded_tensor[0]

    else:
      raise DomainError('Argument not in Tensor: IN: ' + repr(args) +
                        ' TENSOR: ' + repr(self.paired_arguments))

  def get_args(self):
    return [v.symbol for v in self.args]

  def get_all_args(self):
    if self.is_primitive:
      return self.args
    else:
      ret = []
      for tens in self.mstrucs:
        for arg in tens.args:
          if arg not in ret:
            ret.append(arg)
      return ret

  def __repr__(self):
    if type(self.symbol) is not Symbol:
      return str(self.symbol)
    sym = self.symbol.name if self.symbol else 'Unnamed'
    identifier = self.__class__.__name__ + '(' + sym + ')'

    max_height = 5
    repr_matrix = []
    broken = False
    for pos, arg_comb in enumerate(self.paired_arguments):
      repr_matrix.append(str(arg_comb) + ': ' + str(self.expanded_tensor[pos]))
      if pos >= max_height:
        broken = True
        break

    if broken:
      repr_matrix.append('...')

    repr_matrix = [identifier] + repr_matrix
    return '\n'.join(repr_matrix)

  def reconstruct_tensor(self, lazy=True):
    """ Re-computes a tensor according to its definition. A primitive tensor is
    reexpanded and non-primitive tensors are computed by multiplying
    their primitive substructures which are recusively reconstructed aswell.

    >>> arg1 = TensorArg('i', domain=range(4), resolve=True)
    >>> arg2 = TensorArg('j', domain=range(4), resolve=True)
    >>>
    >>> tensor1 = MTensor([arg1], symbol=Symbol('t'))
    >>> tensor2 = MTensor([arg2], symbol=Symbol('t'))
    >>> tensor1 is tensor1.reconstruct_tensor()
    True
    >>> tensor2 is tensor2.reconstruct_tensor()
    True

    >>> tensor = tensor1*tensor2
    >>> tensor_rc = tensor.reconstruct_tensor()
    >>> tensor_rc.args == tensor.args
    True
    """
    if self.block_reconstruction is True:
      return self
    elif len(self.mstrucs) > 0:
      b = [u.reconstruct_tensor(lazy=lazy) for u in self.mstrucs[1:]]
      a = self.mstrucs[0].reconstruct_tensor()
      ret = fold(lambda x, y: x.__mul__(y, lazy=lazy), b, a)
      return ret
    else:
      assert(self.is_primitive)

      # HACK: Allowing the same argument twice in a primitive structure. This
      # won't work if the structure is expanded numerically.
      if(len(self.args) == 2 and
         self.is_contraction_argument(self.args[0]) is False):
        if self.args[0] == self.args[1]:
          self.args = [self.args[0]]

      self._expanded_tensor = None
      if not lazy:
        self.expand()
      return self

  def substitute_arg(self, arg_old, arg_new, subs_substructure=True):
    """ Substitutes `arg_old` with `arg_new`. If the tensor is non-primitive
    arguments of primitive substructures are replaced by default.

    :param arg_old: argument to-be replaced
    :type  arg_old: TensorArg

    :param arg_new: argument to-be replaced with
    :type  arg_new: TensorArg

    :param subs_substructure: Enable disable recursive substitution in
                              substructures.
    :type  subs_substructure: bool

    >>> arg1 = TensorArg('i', domain=range(2), resolve=True)
    >>> arg2 = TensorArg('j', domain=range(2), resolve=True)
    >>> arg3 = TensorArg('j', domain=range(2), resolve=False)
    >>> arg4 = TensorArg('i', domain=range(2), resolve=False)

    >>> tensor1 = MTensor([arg1], symbol=Symbol('t'))
    >>> tensor2 = MTensor([arg2], symbol=Symbol('t'))

    >>> tensor = tensor1*tensor2
    >>> tensor.paired_arguments
    [(0, 0), (0, 1), (1, 0), (1, 1)]
    >>> tensor.expanded_tensor
    [t(0)**2, t(0)*t(1), t(0)*t(1), t(1)**2]
    >>> tensor.substitute_arg(arg2, arg3)
    >>> tensor = tensor.reconstruct_tensor()
    >>> tensor.paired_arguments
    [(0, 'j'), (1, 'j')]
    >>> tensor.expanded_tensor
    [t(0)*t(j), t(1)*t(j)]
    >>> tensor.substitute_arg(arg1, arg4)
    >>> tensor = tensor.reconstruct_tensor()
    >>> tensor.paired_arguments
    [('i', 'j')]
    >>> tensor.expanded_tensor
    [t(i)*t(j)]
    """
    for pos, arg in enumerate(self.args):
      if (arg == arg_old) is True:
        success = True
        self.args[pos] = arg_new

    if subs_substructure:
      for mstruc in self.mstrucs:
        mstruc.substitute_arg(arg_old, arg_new)

    self.set_all_args_resolved()

    self.derive_paired_arguments()

  def arg_resolve_state(self, arg_sym, resolve, subs_substructure=True,
                        original=False):
    """ Changes the resolve state of `arg_sym` to `resolve` in the tensor and
    all subtensors. Use with care. It is safer to substitute for arguments via
    `substitute_arg` """
    modified_args = False
    for arg in self.args:
      if arg.symbol == arg_sym:
        modified_args = True
        if original:
          arg.__resolve = resolve
        else:
          arg.resolve = resolve

    modified_args = True
    if subs_substructure:
      for mstruc in self.mstrucs:
        t = mstruc.arg_resolve_state(arg_sym, resolve,
                                     subs_substructure=subs_substructure,
                                     original=original)

        modified_args = modified_args or t
    return modified_args

  def make_primitive(self, block_reconstruction=True, symbol=False):
    """
    Transforms a MTensor into a primitive MTensor without substructure.
    :block_reconstruction: forbids to reconstruct the tensor
    """
    # trigger expansion if lazy
    self.expanded_tensor

    self.block_reconstruction = block_reconstruction
    self.is_primitive = True
    self.mstrucs = []
    if symbol:
      self.symbol = symbol

#===================#
#  _start_contract  #
#===================#

  def _start_contract(self, tensors, lazy=True, symbol=None):
    """ Prepares tensors for multiplication:

    - multiplication via recursion
    - derive contracted and non-contracted arguments
    """
    is_symbolic = False
    numerical = False

    possibly_contracted = []
    contracted_args = []
    non_contracted_args = []
    primitive_substructures = []

    # multiplicaton perfmored recursively
    if len(tensors) > 2:
      tensors = [tensors[0], self._start_contract(tensors[1:])]

    # collect all primitive substructures and look for possibly contracted
    # arguments
    for tens in tensors:
      args = tens.args

      # gathering substructures, but yet not used anywhere
      if tens.is_primitive:
        if tens.symbol not in self.primitive_substructures:
          primitive_substructures.append(tens.symbol)
      else:
        for prim_struc in tens.primitive_substructures:
          primitive_substructures.append(prim_struc)

      # determine whether the result is symbolic. If at least one struc is
      # symbolic, the contracted tensor is symbolic too.
      is_symbolic = is_symbolic or tens.is_symbolic()

      for arg in args:
        if tens.is_contraction_argument(arg):
          possibly_contracted.append(arg)

    possibly_contracted_symbols = [u.symbol for u in possibly_contracted]

    for args in (tens.args for tens in tensors):
      for arg in args:
        if arg in possibly_contracted:
          count = possibly_contracted_symbols.count(arg.symbol)
          if count > 1:
            if arg.symbol not in [u.symbol for u in contracted_args]:
              contracted_args.append(arg)
          else:
            if arg not in non_contracted_args:
              non_contracted_args.append(arg)
        else:
          if arg not in non_contracted_args:
            non_contracted_args.append(arg)

    if is_symbolic:
      numerical = None
    else:
      numerical = True

    # Construct new MTensor instance
    res = MTensor(args=non_contracted_args, numerical=numerical,
                  symbol=symbol, expand=False, is_primitive=False)
    res._prepare_contraction(contracted_args, non_contracted_args, tensors, lazy=lazy)
    return res

  def _delta_contr(self, delta_contraction, delta_pos,
                   tensors_gathered, non_contracted_tensors, carg):
    # Delta self contraction
    if len(tensors_gathered) == 1:
      tens = MTensor([], symbol=Symbol('dim'))
      if len(non_contracted_tensors) > 0:
        tens *= non_contracted_tensors[0]

      self.args = tens.args
      self.symbol = tens.symbol
      self.numerical = tens.numerical
      self.mstrucs = tens.mstrucs
      self._expanded_tensor = tens.expanded_tensor
      self.derived_tensor = tens.derived_tensor
      self.paired_arguments = tens.paired_arguments
      return

    assert(len(non_contracted_tensors) == 0)
    assert(len(tensors_gathered) == 2)
    delta_struc = delta_contraction[0]
    if delta_contraction[1] is not None:
      other_tensors = (delta_contraction[1] *
                       tensors_gathered[(delta_pos + 1) % 2])
    else:
      other_tensors = tensors_gathered[(delta_pos + 1) % 2]

    just_multiplied = []
    kronecker_contracted_struc = None
    delta_arg_nc = [u for u in delta_struc.args if u.symbol != carg.symbol][0]

    # perform d_{i j} * T_{a b .. j  .. m n) =  T_{a b .. i  .. m n)
    if other_tensors.is_primitive:
      # copy is crucial here, otherwise primitive tensor arguments are
      # overwritten
      args = other_tensors.args[:]
      c_index = [u.symbol for u in args].index(carg.symbol)

      # if original argument was resolved, the new index needs to be resolved
      if other_tensors.numerical:
        new_arg = TensorArg(delta_arg_nc.symbol, domain=delta_arg_nc.domain,
                            resolve=args[c_index].resolve,
                            metric=delta_arg_nc.metric)
      else:
        new_arg = TensorArg(delta_arg_nc.symbol, domain=delta_arg_nc.domain,
                            resolve=delta_arg_nc.resolve,
                            metric=delta_arg_nc.metric)

      args[c_index] = new_arg

      # since the tensor is primitive we can initialize it properly
      self.__init__(args=args, symbol=other_tensors.symbol,
                    numerical=other_tensors.numerical, delta=other_tensors.delta)

    else:
      # Strategy is as follows: The routine must prevent any `real` contraction
      # before all kronecker deltas are eliminated. At this point we try to find
      # another tensor in other tensors with an argument in common with the
      # kronecker delta.

      # check for other delta contractions
      if any(delta_arg_nc.symbol in [u.symbol for u in struc.args]
             for struc in other_tensors.mstrucs):
        arg = delta_arg_nc
        for struc in other_tensors.mstrucs:
          if arg.symbol in [u.symbol for u in struc.args]:
            kronecker_contracted_struc = struc
          else:
            just_multiplied.append(struc)
      # No other delta contractions found:
      # - find the structure with which the kronecker delta is contracted
      # - peform the contraction between the two tensors and multiply the rest
      else:
        arg = carg
        for struc in other_tensors.mstrucs:
          if arg.symbol in [u.symbol for u in struc.args]:
            kronecker_contracted_struc = struc
          else:
            just_multiplied.append(struc)

      # This must be true since we started with a valid contraction, i.e. there
      # must be another structure contracted to the kronecker delta
      assert(kronecker_contracted_struc is not None)
      tmp = kronecker_contracted_struc * delta_struc

      for struc in just_multiplied:
        tmp = tmp * struc

      self.args = tmp.args
      self.mstrucs = tmp.mstrucs
      self.derived_tensor = tmp.derived_tensor
      self.paired_arguments = tmp.paired_arguments
      self.primitive_substructures = tmp.primitive_substructures

#========================#
#  _perform_contraction  #
#========================#

  def _check_args_changed(self, change_args=True):
    """ Changing the argument may change the argument in multiple tensors.  """
    args_changed = (u.changed for u in self.args)
    if any(args_changed):
      if change_args:
        def cll_reset_arg(arg):
          def reset_arg():
            arg_cpy = arg.copy_original()
            return arg_cpy
          return reset_arg
        arg_repl = {arg: cll_reset_arg(arg)() for arg in self.args
                    if arg.changed}
        for aold, anew in arg_repl.iteritems():
          self.substitute_arg(aold, anew)
      else:
        return self.reconstruct_tensor()

    return self

  def _check_resolve_contraction(self, contracted_args, tensors_gathered):
    contracted_args_sym = [u.symbol for u in contracted_args]
    tens_need_reconstruction = []
    for tpos, tens in enumerate(tensors_gathered):
      args_in_tensor = [u.symbol for u in tens.args]
      # this shit only causes problems, never touch resolve state of an
      # argument! Always substitute for a new argument.
      #tensors_gathered[tpos] = tens._check_args_changed()

      args_subs = []
      for t_arg in tens.args:
        # .symbol is crucial here because we can have two different arguments,
        # but the same contraction argument (one resolved the other one not)
        if t_arg.symbol in contracted_args_sym and not t_arg.resolve:
          #if not t_arg.symbol in cargs_resolved:
          #new_arg = t_arg.copy()
          new_arg = TensorArg(t_arg.symbol, domain=t_arg.domain,
                              resolve=True, metric=t_arg.metric)
          new_arg.resolve = True
          tens.substitute_arg(t_arg, new_arg)
          if tpos not in tens_need_reconstruction:
            tens_need_reconstruction.append(tpos)

    for tpos in tens_need_reconstruction:
      tens = tensors_gathered[tpos]
      tensors_gathered[tpos] = tens.reconstruct_tensor()
    return tensors_gathered

#===========================#
#  _perform_multiplication  #
#===========================#

  def _perform_multiplication(self, args_domain, tensors, ncargs, cargs,
                              oarg_pos, carg_pos, lazy=True):
    self.paired_arguments = args_domain
    if lazy:
      return
    else:
      self._expanded_tensor = []
    ncargs_sym = [v.symbol for v in ncargs]
    for arg in args_domain:
      mult_result = 1
      for s_index, tens in enumerate(tensors):
        struc_arg = len(tens.args)*[None]
        for sym in (u.symbol for u in tens.args):
          if sym in ncargs_sym:
            id_open_arg = ncargs_sym.index(sym)
            struc_arg[oarg_pos[s_index][sym]] = arg[id_open_arg]
          else:
            id_contr_arg = [v.symbol for v in cargs].index(sym)
            struc_arg[carg_pos[s_index][sym]] = cargs[id_contr_arg]

        if len(struc_arg) == 0:
          mult_result *= tens.expanded_tensor[0]
        else:
          try:
            component_index = (tens.paired_arguments.
                               index(tuple(struc_arg)))
            mult_result *= tens.expanded_tensor[component_index]
          except ValueError:
            raise
      self._expanded_tensor.append(mult_result)

    # If the one element is multiplied with another tensor, the numerical
    # structure should be taken over which is done here.
    # In case of more complicated multiplications the numerical structure has
    # no more meaning and is dropped. The numerical structure is resolved in
    # expanded_tensor if the tesor args are ''resolved''.
    if 1 in [tens.symbol for tens in tensors]:
      numericals = [tens.numerical for tens in tensors
                    if tens.numerical is not None]
      tenss = [tens for tens in tensors if tens.symbol != 1]
      assert(len(numericals) <= 1)
      if len(tenss) > 0:
        self.symbol = tenss[0].symbol
        self.is_primitive = tenss[0].is_primitive
        self.delta = tenss[0].delta

      if len(numericals) == 1:
        self.numerical = numericals[0]

  def _perform_contraction(self, contracted_args, non_contracted_args,
                           oarg_positions, carg_positions,
                           args_expanded, contracted_args_domain,
                           tensors_gathered,
                           lazy=True):

    args_expanded = [arg.domain if arg.resolve else [arg.symbol]
                     for arg in non_contracted_args]

    self.paired_arguments = pair_arguments(args_expanded)
    if lazy:
      return
    else:
      self._expanded_tensor = []
    signature = None
    for arg_pos, contracted_arg in enumerate(contracted_args):
      if contracted_arg.metric is not None:
        if signature is None:
          signature = [arg_pos]
        else:
          signature.append(arg_pos)

    if signature is not None:
      signatures = []
      for contracted_arg in contracted_args:
        metric = contracted_arg.metric
        domain_len = len(contracted_arg.domain)
        try:
          assert(isinstance(metric, Matrix))
          metric = metric.tolist()
          metric_len = len(metric)
          if metric_len != domain_len:
            raise DomainError('args domain does not match metric ' +
                              'dimension! Metric length: ' +
                              str(metric_len) + ' domain length: ' +
                              str(domain_len))

          signatures.append([metric[i][i] for i in range(domain_len)])
        except DomainError:
          raise
        except:
          signatures.append([1 for i in range(domain_len)])

      signatures_paired = pair_arguments(signatures)
      signatures_domain = [fold(lambda x, y: x * y, u, 1) for u in
                           signatures_paired]
    non_contracted_args_sym = [v.symbol for v in non_contracted_args]
    for arg in pair_arguments(args_expanded):
      contracted_result = 0
      for contr_args in contracted_args_domain:
        if signature:
          arg_index = contracted_args_domain.index(contr_args)
          mult_result = signatures_domain[arg_index]
        else:
          mult_result = 1

        for s_index, tens in enumerate(tensors_gathered):
          struc_arg = len(tens.args)*[None]
          for sym in (u.symbol for u in tens.args):
            if sym in non_contracted_args_sym:
              index_open_arg = non_contracted_args_sym.index(sym)
              struc_arg[oarg_positions[s_index][sym]] = arg[index_open_arg]
            else:
              i_contr_arg = [v.symbol for v in
                             contracted_args].index(sym)
              struc_arg[carg_positions[s_index][sym]] = contr_args[i_contr_arg]
          struc_arg = [u for u in struc_arg if u is not None]
          component_index = (tens.paired_arguments.
                             index(tuple(struc_arg)))
          mult_result *= tens.expanded_tensor[component_index]

        contracted_result += mult_result

      self._expanded_tensor.append(contracted_result)


#==========================#
#  get_argument_positions  #
#==========================#

  @staticmethod
  def get_argument_positions(tensors, args):
    """" Returns the positions of arguments in the tensors.

    :return: List of dictionaries where the position of the dicitonary
              corresponds to the position in `tensors`.
    >>> arg1 = TensorArg('i', domain=range(2), resolve=True)
    >>> arg2 = TensorArg('j', domain=range(2), resolve=True)
    >>> arg3 = TensorArg('j', domain=range(2), resolve=False)
    >>> arg4 = TensorArg('i', domain=range(2), resolve=False)

    >>> t1 = MTensor([arg1, arg2], symbol=Symbol('t1'))
    >>> t2 = MTensor([arg3, arg4], symbol=Symbol('t2'))
    >>> pos = MTensor.get_argument_positions([t1, t2], [arg1])
    >>> pos[0]
    {'i': 0}
    >>> pos[1]
    {'i': 1}
    """
    oarg_positions = []
    # contracted argument positions
    carg_positions = []
    #contraction_dicts = []
    for tens in tensors:
      TensorArg_symbols = [u.symbol for u in tens.args]

      index_dict = {sym: TensorArg_symbols.index(sym)
                    for sym in [v.symbol for v in args]
                    if sym in TensorArg_symbols}

      oarg_positions.append(index_dict)

      #cindex_dict = {sym: TensorArg_symbols.index(sym)
                     #for sym in (v.symbol for v in cargs)
                     #if sym in TensorArg_symbols}
      #carg_positions.append(cindex_dict)

    return oarg_positions

#------------------------------------------------------------------------------#

  def _prepare_contraction(self, contracted_args, non_contracted_args, tensors,
                           lazy=True):
    """ Prepares for the contraction and multiplication of two tensors """

    for tens in tensors:
      if tens.is_primitive and not hasattr(tens, 'one'):
        self.mstrucs.append(tens)
      else:
        self.mstrucs.extend(tens.mstrucs)

    # Prepare contraction if contraction arguments are present
    if len(contracted_args) > 0:
      # Contraction is performed recursively. Starting with the first
      # contraction argument
      c_arg = contracted_args[0]
      #tensors_gathered = []
      contracted_tensors = []
      non_contracted_tensors = []

      # Filter for those tensors which participate in the contraction and fro
      # those which do not
      for tens in tensors:
        args_in_tensor = [u.symbol for u in tens.args]
        if c_arg.symbol in args_in_tensor:
          #tensors_gathered.append(tens)
          contracted_tensors.append(tens)
        else:
          # non contracted tensors only found if there is a selfcontraction of a
          # tensor due to kronecker deltas
          non_contracted_tensors.append(tens)

      #oarg_pos, carg_pos = self.get_argument_positions(contracted_tensors,
                                                       #non_contracted_args,
                                                       #contracted_args)
      oarg_pos = self.get_argument_positions(contracted_tensors,
                                             non_contracted_args)
      carg_pos = self.get_argument_positions(contracted_tensors,
                                             contracted_args)

      # check if the contraction is a multiplication with a kronecker delta
      # which is handlet differently
      delta_contr = None
      delta_pos = None
      for t_id, tens in enumerate(contracted_tensors):
        # find a Delta_{ij} tensor
        delta_contr = self._find_delta_tensor(tens, c_arg)
        if delta_contr is not None:
          delta_pos = t_id
          break

      # Found a kronecker delta
      if delta_contr is not None:
        self._delta_contr(delta_contr, delta_pos, contracted_tensors,
                          non_contracted_tensors, c_arg)

      # Standard contraction
      else:
        # at this stage all tensors participate in the contraction
        assert(len(non_contracted_tensors) == 0)
        args_expanded = [arg.domain if arg.resolve else [arg.symbol]
                         for arg in non_contracted_args]
        contracted_args_domain = pair_arguments([u.domain for u in
                                                 contracted_args])

        contracted_tensors = self._check_resolve_contraction(contracted_args,
                                                             contracted_tensors)
        self._perform_contraction(contracted_args, non_contracted_args,
                                  oarg_pos, carg_pos, args_expanded,
                                  contracted_args_domain, contracted_tensors,
                                  lazy=lazy)

    else:
      # Prepare multiplication
      args_expanded = [arg.domain if arg.resolve else [arg.symbol]
                       for arg in non_contracted_args]
      #print ('New session')
      #for arg in non_contracted_args:
        #print("arg:", arg)
        #print("arg.resolve:", arg.resolve)
      #for tens in tensors:
        #print("tens.paired_arguments:", tens.paired_arguments)
        #print("tens.changed:", tens.changed)
        #for arg in tens.args:
          #print("arg:", arg)
          #print("arg.resolve:", arg.resolve)
      #tensors = self._check_resolve_contraction(non_contracted_args, tensors)
      # todo  check domain match all arguments in tensors
      args_domain = pair_arguments(args_expanded)
      oarg_positions = self.get_argument_positions(tensors, non_contracted_args)
      carg_positions = self.get_argument_positions(tensors, contracted_args)
      self._perform_multiplication(args_domain, tensors, non_contracted_args,
                                   contracted_args, oarg_positions,
                                   carg_positions,
                                   lazy=lazy)

#------------------------------------------------------------------------------#

  def _find_delta_tensor(self, tensor, arg):
    """ Looks for a delta tensor substructure in `tensor` with  argument `arg`.

    :param tensor: tensor instance
    :type  tensor: MTensor

    :param arg: The argument which must be included in `tensor`
    :type  arg: TensorArg

    On success the function returns
    :return:  (found tensor, substructure)

    If the discovered tensor is primitive substructure is None
    :return:  (found tensor, None)

    If no delta with the given argument is found None is returned.

    :return: None

    >>> rank_one = Symbol('R1')
    >>> contraction_arg = TensorArg('-1', domain=[0, 1, 2, 3], resolve=True)
    >>> R1_tensor = MTensor([contraction_arg], symbol=rank_one)

    >>> j_arg = TensorArg('j', domain=[0, 1, 2, 3], resolve=True)
    >>> delta_tensor = MTensor([contraction_arg, j_arg], numerical=MTensor.LS.one)

    >>> prod = delta_tensor*R1_tensor
    >>> prod.expanded_tensor
    [R1(0), R1(1), R1(2), R1(3)]
    >>> prod.args
    [j]
    """
    if tensor.is_primitive:
      if hasattr(tensor, 'delta') and tensor.delta is True:
        TensorArgs = [u.symbol for u in tensor.args]
        if arg.symbol in TensorArgs:
          arg_index = TensorArgs.index(arg.symbol)
          other_index = (arg_index + 1) % 2
          delta_arg = tensor.args[arg_index]
          return (tensor, None)

      return None

    else:
      subtensors = tensor.mstrucs
      numericals = [u.numerical for u in subtensors]
      candidates = [u for u in subtensors
                    if hasattr(u, 'delta') and u.delta is True]
      if len(candidates) > 0:
        for cindex, cargs in enumerate(u.args for u in candidates):
          if arg.symbol in [u.symbol for u in cargs]:
            cindex = subtensors.index(candidates[cindex])
            found = subtensors.pop(cindex)
            subtensor = fold(lambda x, y: x*y, subtensors,
                             MTensor([], symbol=1))
            return (found, subtensor)

      return None

  def find_primitive_tensor_symbol(self, symbol, repl=False, debug=False):
    """
    Searches a tensors substructure for tensor symbols which equal symbol.
    Optionally, matching tensors can be replaced by a new tensor defined
    by repl.
    """
    subs = False
    if self.is_primitive:
      if self.symbol == symbol:
        if not repl:
          return self
        else:
          return repl.tensors[0]
      else:
        return False
    else:
      for pos, tens in enumerate(self.mstrucs):
        res = tens.find_primitive_tensor_symbol(symbol, repl=repl)
        if res:
          if not repl:
            return res
          else:
            subs = True
            self.mstrucs[pos] = res
    if repl and subs:
      return self
    else:
      return False

  def __mul__(self, other, lazy=True):
    """ Multiplication rule for MTensors.
    >>> i_arg = TensorArg('-1', domain=[0, 1], resolve=True)
    >>> mytensor = MTensor([i_arg], numerical=[1, 1], symbol=Symbol('t'))
    >>> product = mytensor*mytensor
    >>> product.expanded_tensor
    [2]
    >>> product.get_args()
    []
    >>> product.expanded_tensor
    [2]
    >>> new_product = MTensor([], symbol=1)*product
    >>> new_product.expanded_tensor
    [2]
    """
    if isinstance(other, MTensor):
      return self._start_contract([self, other], lazy=lazy)
    else:
      return MTensor([], symbol=other)*self

  def __rmul__(self, other):
    self.expanded_tensor = [u * other for u in self.expanded_tensor]
    return self

  def string_subs(self, string, dict_repl):
    if len(dict_repl) > 0:
      p = (re.compile('|'.join(re.escape(key)
           for key in dict_repl.keys()), re.S))
      return p.sub(lambda x: dict_repl[x.group()], string)
    else:
      return string

  def zero(self, other):
    if not (isinstance(other, MTensor)):
      raise Exception("argument is not a tensor")

    self.paired_arguments = other.paired_arguments[:]
    new_args = [TensorArg(u.symbol, domain=u.domain,
                          resolve=u.resolve, metric=u.metric)
                for u in other.args]
    self.args = new_args
    self.expanded_tensor = [0] * len(other.expanded_tensor)
    return self

  def replace_args(self, repl_dict, res=None):
    pattern = re.compile('|'.join(repl_dict.keys()))
    if res is None:
      ret = []
      for term in self.expanded_tensor:
        ret.append(sub_args(str(term), repl_dict))
      return ret
    else:
      ret = []
      for term in res:
        ret.append(sub_args(str(term), repl_dict))
      return ret

  def is_contraction_argument(self, arg):
    try:
      if arg.symbol < 0:
        return True
    except:
      arg_sympified = sympify(arg.symbol)
      if arg_sympified.is_integer:
        if arg_sympified < 0:
          return True

    return False

  def is_symbolic(self):
    return self.symbol is not None

  def expand(self, zero_expand=False):
    """ Def expand the tensor in the domain defined by the arguments. """

    if self.block_reconstruction is True:
      return

    self._expanded_tensor = []

    # build up the domain. If not resolved only the symbol is kept.
    args_expanded = []
    for arg in self.args:
      if arg.resolve:
        args_expanded.append(arg.domain)
      else:
        args_expanded.append([arg.symbol])

    # Scalar case: tensor has no argument
    if len(args_expanded) == 0:
      self.paired_arguments = []
      self._expanded_tensor = [self.symbol]
    else:

      # compute product base of arguments domain
      self.paired_arguments = pair_arguments(args_expanded)
      self.set_all_args_resolved()

      # Numerical expansion only if all arguments resolved and valid numerical
      # expression
      if(self.numerical is not None and self.all_args_resolved and
         type(self.numerical) is not bool):

        for arg in self.paired_arguments:
          value = self.numerical
          for i in arg:
            if isinstance(value, Matrix):
              value = value.tolist()
            value = value[i]
          self._expanded_tensor.append(value)

      elif self.symbol is not None:
        # sympify takes care of numbers such that .free_symbols is well-defined
        base_symbol = self.symbol
        base_symbol_args = sympify(base_symbol).args
        prefactor = 1
        if len(base_symbol_args) > 0:
          prefactor = base_symbol_args[0]
          base_symbol = base_symbol_args[1]

        args_expanded = []
        for arg in self.args:
          if arg.resolve:
            args_expanded.append(arg.domain)
          else:
            args_expanded.append([arg.symbol])
        for i in range(len(self.paired_arguments)):
          arg = self.paired_arguments[i]
          arg_str = '(' + fold(lambda x, y: x + "," + str(y), arg[1:],
                               str(arg[0])) + ')'

          # intentional strange behavior. We can contract a tensor with a number
          # while adding a new argument and the number is of course not bound to
          # that argument. This is, for instance, necessary when we have the
          # case that a loop momentum is the one from the outgoing particle ,
          # thus the lorentz index of the ougoint wavefunction mathes to the
          # lorentz index for the tensor integral
          if not sympify(base_symbol).is_number:
            newsymbol = prefactor*Symbol(str(base_symbol) + arg_str[:])
          else:
            newsymbol = self.symbol
          self._expanded_tensor.append(newsymbol)
      elif zero_expand:
        self._expanded_tensor = [0]*len(self.paired_arguments)
      else:
        # No valid symbol or numerical given -> initialized as zero tensor
        #TODO: nick symbol is not set in this case So 07 Sep 2014 10:04:14 CEST
        print("self.symbol:", self.symbol)
        print("self.numerical:", self.numerical)
        print("self.args:", self.args)
        print("self.is_primitive:", self.is_primitive)
        raise Exception('Cannot expand tensor! Args need to be resolved or ' +
                        'symolic expression needed.')

  def remove_dummies(self):
    for i in range(len(self.expanded_tensor)):
      term = self.expanded_tensor[i]
      dummies = get_dummies_set(term)
      repl_dict = dict()
      for dummy in dummies:
        # maybe add the case where leading underscores are removes from
        # dummy.name
        repl_dict[dummy] = Symbol(dummy.name)
      if len(repl_dict.keys()) > 0:
        self.expanded_tensor[i] = self.expanded_tensor[i].subs(repl_dict)

    return self

  def compute_tensor_index_shift(self, repl_dict):
    index_rules = dict()
    tensor_indices = repl_dict.keys()
    for index in tensor_indices:
      index_rules[index] = re.compile("\(" + index + "\)")

    carries_tensor_index = []
    repl_dict_new = dict()
    index_rules_new = dict()
    exprs_new = []
    for expr in self.expanded_tensor:
      for key in index_rules.keys():
        rule = index_rules[key]
        match = rule.search(str(expr))
        if match:
          repl_dict_new[key] = repl_dict[key]
          index_rules_new[key] = index_rules[key]

      repl_dict_symbols = dict()
      tensor_valued = find_tensor_valued_symbol(expr, index_rules_new)
      for value in tensor_valued:
        repl_dict_symbols[value[0]] = symbols(re.sub(value[1],
                                              repl_dict_new[value[1]],
                                              str(value[0])))

      expr_new = expr.subs(repl_dict_symbols, simultaneous=True)
      exprs_new.append(expr_new)

    self.expanded_tensor = exprs_new
    # according to http://stackoverflow.com/questions/2400504/
    # easiest-way-to-replace-a-string-using-a-dictionary-of-replacements
    # this substituion is simultaneous.
    pattern = re.compile('|'.join(repl_dict.keys()))
    self_args_symbol = [u.symbol for u in self.args]
    for i in range(len(self_args_symbol)):
      self.args[i] = TensorArg(pattern.sub(lambda x:
                               repl_dict[x.group()], self_args_symbol[i]),
                               domain=self.args[i].domain,
                               resolve=self.args[i].resolve)
    args_expanded = []
    for arg in self.args:
      if arg.resolve:
        args_expanded.append(arg.domain)
      else:
        args_expanded.append([arg.symbol])

    if len(args_expanded) == 0:
      self.paired_arguments = []
      self.expanded_tensor = [self.symbol]
    else:
      self.paired_arguments = pair_arguments(args_expanded)
    return self

if __name__ == "__main__":
  #import doctest
  #doctest.testmod()

  #r1 = Symbol('R1')
  #r2 = Symbol('R2')
  #r3 = Symbol('R3')
  #mu = TensorArg('mu', domain=range(4), resolve=False)
  #nu = TensorArg('nu', domain=range(4), resolve=False)
  #carg = TensorArg('-1', domain=range(4), resolve=True)
  #carg = TensorArg('-1', domain=range(4), resolve=True)
  #R1 = MTensor([mu], symbol=r1)
  #R2 = MTensor([nu, carg], symbol=r2)
  #R3 = MTensor([carg], symbol=r3)
  #print("R1.paired_arguments:", R1.paired_arguments)
  #R1.reconstruct_tensor(lazy=False)
  #print("R1.expanded_tensor:", R1.expanded_tensor)
  #print("R2.expanded_tensor:", R2.expanded_tensor)
  #R = R1*R2
  #R = R*R3
  #print("R.args:", R.args)
  #print("R._expanded_tensor:", R._expanded_tensor)
  ##print("R.mstrucs:", R.mstrucs)
  #print("R.expanded_tensor:", R.expanded_tensor)
  #print("R._expanded_tensor:", R._expanded_tensor)

  #print("R1.expanded_tensor:", R1.expanded_tensor)
  #print("R2.expanded_tensor:", R2.expanded_tensor)

  #j_arg = TensorArg('j', domain=[0, 1, 2, 3], resolve=False)
  #delta_tensor = MTensor([contraction_arg, j_arg], numerical=MTensor.LS.g)
  #delta_tensor.delta = True
  #print("delta_tensor.expanded_tensor:", delta_tensor.expanded_tensor)

  #prod = delta_tensor*R1_tensor
  #print("prod.expanded_tensor:", prod.expanded_tensor)

  arg1 = TensorArg('i', domain=range(2), resolve=True)
  arg2 = TensorArg('j', domain=range(2), resolve=True)
  arg3 = TensorArg('j', domain=range(2), resolve=False)
  arg4 = TensorArg('i', domain=range(2), resolve=False)

  tensor1 = MTensor([arg1], symbol=Symbol('t'))
  tensor2 = MTensor([arg2], symbol=Symbol('t'))

  tensor = tensor1*tensor2
  tensor.paired_arguments
  tensor.expanded_tensor
  tensor.substitute_arg(arg2, arg3)
  #tensor = tensor.reconstruct_tensor()
  tensor.paired_arguments
  #tensor.expanded_tensor
  tensor.substitute_arg(arg1, arg4)
  tensor = tensor.reconstruct_tensor()
  print (tensor.paired_arguments)
  print("tensor.args:", tensor.args)
  print (tensor.expanded_tensor)
