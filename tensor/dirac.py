#==============================================================================#
#                                   dirac.py                                   #
#==============================================================================#
from sympy import symbols, Matrix, I

def arePermsEqualParity(perm0, perm1):
    """Check if 2 permutations are of equal parity.

    Assume that both permutation lists are of equal length
    and have the same elements. No need to check for these
    conditions.

    from: http://stackoverflow.com/questions/1503072/
          how-to-check-if-permutations-have-equal-parity
    """
    perm1 = perm1[:]  # copy this list so we don't mutate the original

    transCount = 0
    for loc in range(len(perm0) - 1):  # Do (len - 1) transpositions
      p0 = perm0[loc]
      p1 = perm1[loc]
      if p0 != p1:
        sloc = perm1[loc:].index(p0) + loc  # Find position in perm1
        perm1[loc], perm1[sloc] = p0, p1  # Swap in perm1
        transCount += 1

    # Even number of transpositions means equal parity
    if (transCount % 2) == 0:
        return True
    else:
        return False

class diracAlgebra(object):
  def __init__(self):

    # momenta
    p0, p1, p2, p3 = symbols('p0 p1 p2 p3')
    #cima = symbols('cima')
    self.momenta = [p0, p1, p2, p3]

    # suited superosition of momenta
    pl1, pl2, pl3, pl4 = symbols('pl(0) pl(1) pl(2) pl(3)')

    # outgoing wavefunction
    w0, w1, w2, w3 = symbols('w0 w1 w2 w3')

    # metric tensor
    self.g = Matrix([[1, 0, 0, 0], [0, -1, 0, 0], [0, 0, -1, 0], [0, 0, 0, -1]])

    # Gamma matrices in Weyls representation
    #Weyl1
    g0 = Matrix([[0, 0, 1, 0], [0, 0, 0, 1], [1, 0, 0, 0], [0, 1, 0, 0]])
    g1 = Matrix([[0, 0, 0, 1], [0, 0, 1, 0], [0, -1, 0, 0], [-1, 0, 0, 0]])
    g2 = Matrix([[0, 0, 0, -I], [0, 0, I, 0], [0, I, 0, 0], [-I, 0, 0, 0]])
    #g2 c.c.
    #g2 = Matrix([[0,0,0,I],[0,0,-I,0],[0,-I,0,0],[I,0,0,0]])
    g3 = Matrix([[0, 0, 1, 0], [0, 0, 0, -1], [-1, 0, 0, 0], [0, 1, 0, 0]])

    self.g5 = Matrix([[-1, 0, 0, 0], [0, -1, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]])

    #Weyl2
    g0 = Matrix([[0, 0, -1, 0], [0, 0, 0, -1], [-1, 0, 0, 0], [0, -1, 0, 0]])
    self.g5 = Matrix([[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, -1, 0], [0, 0, 0, -1]])

    expr = p0*g0 - p1*g1 - p2*g2 - p3*g3
    self.pslash = (expr.subs(p0+p3, pl1).subs(p0-p3, pl2).
                   subs(-p1-I*p2, -pl3).subs(-p1+I*p2, -pl4))

    # recola convention
    #g2 = g2.subs(I, cima)

    self.gamma = [g0, g1, g2, g3]
    self.one = Matrix([[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]])
    self.ProjP = (self.one+self.g5)/2
    self.ProjM = (self.one-self.g5)/2

    self.gammaP = [u*self.ProjP for u in self.gamma]
    self.gammaM = [u*self.ProjM for u in self.gamma]

    self.w = Matrix([w0, w1, w2, w3])

    # construct levi civita tensor in 4 dimensions
    eps = []
    dim = 4
    for i in range(dim):
      for j in range(dim):
        for k in range(dim):
          for l in range(dim):
            if len(eps) <= i:
              eps.append([])
            if len(eps[i]) <= j:
              eps[i].append([])
            if len(eps[i][j]) <= k:
              eps[i][j].append([])

            is_perm = all(u in [i, j, k, l] for u in range(dim))
            if not is_perm:
              eps[i][j][k].append(0)
            elif arePermsEqualParity([0, 1, 2, 3], [i, j, k, l]):
              eps[i][j][k].append(1)
            else:
              eps[i][j][k].append(-1)
    self.epsilon = eps

    sigma = []
    # construct \sigma^{\mu, \nu} = i/2 [ga^\mu, ga^\nu]
    for mu in range(dim):
      for nu in range(dim):
        if len(sigma) <= mu:
          sigma.append([])
        sigma[mu].append(Matrix(self.gamma[mu])*Matrix(self.gamma[nu]))
    self.sigma = sigma
