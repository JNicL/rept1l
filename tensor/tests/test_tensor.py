#==============================================================================#
#                                test_tensor.py                                #
#==============================================================================#

#============#
#  Includes  #
#============#

from test_tools import *
from rept1l.tensor.tensoradd import *
from sympy import Symbol

class Test_Tensor(object):

  def test_one_tensor(self):
    one_tensor = MTensor([], symbol=1)
    one_tensor *= 2
    eq_(one_tensor.expanded_tensor[0], 2)

  def test_delta_tensor(self):
    rank_one = Symbol('R1')
    contraction_arg = TensorArg('-1', domain=[0, 1, 2, 3], resolve=True)
    R1_tensor = MTensor([contraction_arg], symbol=rank_one)

    j_arg = TensorArg('j', domain=[0, 1, 2, 3], resolve=False)
    delta_tensor = MTensor([contraction_arg, j_arg], numerical=MTensor.LS.one,
                           symbol=Symbol('d'))
    delta_tensor.delta = True

    prod = delta_tensor*R1_tensor
    R1_repl_tensor = MTensor([j_arg], symbol=rank_one)

    eq_(prod.expanded_tensor, R1_repl_tensor.expanded_tensor)

  def test_delta_on_Atensor(self):
    rank_one = Symbol('R1')
    contraction_arg = CArg('-1')
    R1_tensor = ATensor([contraction_arg], symbol=rank_one)

    j_arg = CArg('j')
    delta_tensor = DeltaTensor(contraction_arg, j_arg)

    prod = delta_tensor*R1_tensor
    R1_repl_tensor = MTensor([j_arg], symbol=rank_one)

    eq_(prod.expanded_tensor, R1_repl_tensor.expanded_tensor)

  def test_make_primitive(self):
    w = Symbol('w')
    arg = TensorArg('-1', resolve=True, domain=range(4))
    newarg = TensorArg('i', resolve=True, domain=range(4))
    new_wavefunction = MTensor([arg], symbol=w)
    prj = {'+': MTensor.LS.ProjP, '-': MTensor.LS.ProjM}
    ret = MTensor([newarg, arg], numerical=prj['+'])
    ret *= new_wavefunction

    ret.make_primitive(symbol=Symbol('Prj_w'), block_reconstruction=True)
    retnew = ATensor(ret)
    # block reconstruction property should survive reconstruction and copy
    retnew = retnew.reconstruct_tensor()
    eq_(retnew.expanded_tensor, ret.expanded_tensor)
    retnew = retnew.copy()
    eq_(retnew.expanded_tensor, ret.expanded_tensor)

  def test_delta_on_Atensor_nested(self):
    R1 = Symbol('R1')
    R2 = Symbol('R2')
    R1_t = ATensor([CArg('-1')], symbol=R1)
    R2_t = ATensor([CArg('nu')], symbol=R2)
    delta_t = DeltaTensor(CArg('-1'), CArg('nu'))
    t = -1*R1_t*R2_t + delta_t
    delta2_t = DeltaTensor(CArg('mu'), CArg('-1'))
    prod = t*delta2_t

    prod2 = ATensor([CArg('mu')], symbol=R1) * ATensor([CArg('nu')], symbol=R2)
    prod2 = DeltaTensor(CArg('mu'), CArg('nu')) + -1*prod2

    eq_(prod.expanded_tensor, prod2.expanded_tensor)

  def test_rank2_delta_tensor(self):
    R11 = Symbol('R11')
    R12 = Symbol('R12')
    delta = Symbol('Delta')

    i_arg = TensorArg('i', domain=[0, 1, 2, 3], resolve=False)
    j_arg = TensorArg('j', domain=[0, 1, 2, 3], resolve=False)
    contraction_arg = TensorArg('-1', domain=[0, 1, 2, 3], resolve=False)

    one_tensor_1 = MTensor([], symbol=1)
    one_tensor_2 = MTensor([], symbol=1)
    one_tensor_3 = MTensor([], symbol=1)
    R11_tensor = MTensor([contraction_arg], symbol=R11)
    R12_tensor = MTensor([j_arg], symbol=R12)
    delta_tensor = MTensor([contraction_arg, i_arg], symbol=delta,
                           numerical=MTensor.LS.g)
    delta_tensor.delta = True

    R11_2_tensor = MTensor([i_arg], symbol=R11)
    R12_2_tensor = MTensor([j_arg], symbol=R12)

    prod1 = one_tensor_1 * delta_tensor * R11_tensor * R12_tensor
    prod2 = one_tensor_2 * R11_2_tensor * R12_2_tensor
    prod3 = one_tensor_3 * R11_tensor * delta_tensor * R12_tensor
    prod3 = one_tensor_3 * R12_tensor * delta_tensor * R11_tensor
    eq_(prod1.expanded_tensor, prod2.expanded_tensor)
    eq_(prod1.expanded_tensor, prod3.expanded_tensor)

  def test_multi_contraction(self):

    R11 = Symbol('R11')
    R12 = Symbol('R12')
    delta = Symbol('Delta')

    i_arg = TensorArg('-1', domain=[0, 1], resolve=True)
    j_arg = TensorArg('-2', domain=[0, 1], resolve=True)
    k_arg = TensorArg('k', domain=[0, 1], resolve=False)
    l_arg = TensorArg('-3', domain=[0, 1], resolve=True)

    one_tensor1 = MTensor([], symbol=1)
    one_tensor2 = MTensor([], symbol=1)
    R11_tensor = MTensor([i_arg, l_arg], symbol=R11)
    R12_tensor = MTensor([j_arg, i_arg, k_arg], symbol=R12)
    delta_tensor = MTensor([j_arg, l_arg], symbol='Delta',
                           numerical=MTensor.LS.one)

    prod1 = one_tensor1 * R11_tensor * delta_tensor * R12_tensor
    prod2 = one_tensor2 * R12_tensor * R11_tensor * delta_tensor

    eq_(prod1.expanded_tensor, prod2.expanded_tensor)

  def test_color_relation(self):
    c1_arg = CArg('-1')
    c2_arg = CArg('-2')
    c3_arg = CArg('-3')
    c4_arg = CArg('-4')

    NM1 = 1/Symbol('N')
    N = ATensor([], symbol=NM1)
    M1 = ATensor([], symbol=-1/Symbol('dim'))

    d11 = DeltaTensor(c1_arg, c2_arg)
    d12 = DeltaTensor(c3_arg, c4_arg)

    d21 = DeltaTensor(c1_arg, c4_arg)
    d22 = DeltaTensor(c3_arg, c2_arg)

    d3 = DeltaTensor(c3_arg, c2_arg)
    prod = (d11 * d12 + M1 * d21 * d22) * d3
    eq_(prod.expanded_tensor[0], 0)

  def test_pslash_pslash_contraction(self):
    dom = [0, 1, 2, 3]
    g = MTensor.LS.g
    gamma = MTensor.LS.gamma
    P = Symbol('P')

    cp1 = TensorArg('-1', domain=dom, resolve=True, metric=g)
    cp2 = TensorArg('-2', domain=dom, resolve=True, metric=g)
    ps1_open = TensorArg('i', domain=dom, resolve=True)
    ps_closed = TensorArg('-3', domain=dom, resolve=True)
    ps2_open = TensorArg('j', domain=dom, resolve=True)

    gamma1 = ATensor([cp1, ps1_open, ps_closed],
                     numerical=gamma, symbol=Symbol('Gamma'))

    p1 = ATensor([cp1], symbol=P)
    p1slash = p1*gamma1

    gamma2 = ATensor([cp2, ps_closed, ps2_open],
                     numerical=gamma, symbol=Symbol('Gamma'))
    p2 = ATensor([cp2], symbol=P)
    p2slash = p2*gamma2

    ps22_var1 = p1slash*p2slash
    ps22_var2 = p1*p2*gamma1*gamma2
    ps22_var4 = gamma1*p2*p1*gamma2
    from rept1l.tensor import simplify
    expr_var4 = simplify(ps22_var1)
    from sympy import simplify, I
    expr_var1 = [simplify(u.subs({Symbol('cima'): I}))
                 for u in ps22_var1.expanded_tensor]
    expr_var2 = [simplify(u.subs({Symbol('cima'): I}))
                 for u in ps22_var2.expanded_tensor]
    expr_var3 = [simplify(u.subs({Symbol('cima'): I}))
                 for u in ps22_var2.expanded_tensor]
    eq_(expr_var4.expanded_tensor, expr_var1)
#
    p_2 = (p1*p1).expanded_tensor[0]
    for index, arg in enumerate(ps22_var1.paired_arguments):
      # diagonal elements must be p^2
      if arg[0] == arg[1]:
        eq_(expr_var1[index], p_2)
        eq_(expr_var2[index], p_2)
        eq_(expr_var3[index], p_2)
      # offdiagonal elements must vanish
      else:
        eq_(expr_var1[index], 0)
        eq_(expr_var2[index], 0)
        eq_(expr_var3[index], 0)

  def test_gamma_gamma_contraction(self):
    dom = [0, 1, 2, 3]
    g = MTensor.LS.g
    gamma = MTensor.LS.gamma
    cp = TensorArg('-1', domain=dom, resolve=True, metric=g)
    ps1_open = TensorArg('i', domain=dom, resolve=True)
    ps2_open = TensorArg('j', domain=dom, resolve=True)
    ps3_open = TensorArg('k', domain=dom, resolve=True)
    ps4_open = TensorArg('l', domain=dom, resolve=True)

    gamma1 = ATensor([cp, ps1_open, ps2_open],
                     numerical=gamma, symbol=Symbol('Gamma'))

    gamma2 = ATensor([cp, ps3_open, ps4_open],
                     numerical=gamma, symbol=Symbol('Gamma'))

    gamma_gamma = gamma1*gamma2

  def test_symmtrization(self):
    dom = range(4)
    ps1_open = TensorArg('i', domain=dom, resolve=False)
    #ps2_open = TensorArg('j', domain=dom, resolve=False)
    ps3_open = TensorArg('k', domain=dom, resolve=False)
    #ps4_open = TensorArg('l', domain=dom, resolve=False)
    ps5_open = TensorArg('m', domain=dom, resolve=False)
    #ps6_open = TensorArg('n', domain=dom, resolve=False)

    R1 = ATensor([ps1_open], symbol=Symbol('R1'))
    R2 = ATensor([ps3_open], symbol=Symbol('R2'))
    R3 = ATensor([ps5_open], symbol=Symbol('R3'))

    R123 = R1*R2*R3
    eq_(str(R123.args), '[i, k, m]')
    eq_(str(R123.expanded_tensor),  '[R1(i)*R2(k)*R3(m)]')

    R132 = R123.copy()

    def sym2(T, pos1, pos2):
      args = T.get_args()
      arg1 = args[pos1]
      arg2 = args[pos2]
      repl = {arg1: arg2, arg2: arg1}
      T = T.tensor_arg_substitution(repl)
      return T

    R132 = sym2(R132, 1, 2)

    from sympy import Rational
    R1_23 = R123 + R132

    R213 = R123.copy()
    R213 = sym2(R213, 0, 1)

    eq_(str(R213.args), '[k, i, m]')
    eq_(str(R213.expanded_tensor),  '[R1(k)*R2(i)*R3(m)]')

    R321 = R123.copy()
    R321 = sym2(R321, 0, 2)

    eq_(str(R321.args), '[m, k, i]')
    eq_(str(R321.expanded_tensor),  '[R1(m)*R2(k)*R3(i)]')

    # Full symmetrization
    R23_1 = R1_23.copy()
    R23_1 = sym2(R23_1, 0, 1)
    R1_23_23_1 = R1_23 + R23_1

    R3_2_1 = R23_1.copy()
    R1_2_3 = (R1_23_23_1 + sym2(R3_2_1, 0, 2))*Rational(1, 2*3)

    #from rept1l.tensor import simplify
    from sympy import simplify, sympify
    #R1_2_3 = simplify(R1_2_3)
    expc = ('(R1(i)*R2(k)*R3(m) + R1(i)*R2(m)*R3(k) + R1(k)*R2(i)*R3(m) + ' +
            'R1(k)*R2(m)*R3(i) + R1(m)*R2(i)*R3(k) + R1(m)*R2(k)*R3(i))/6')
    eq_(simplify(sympify(str(R1_2_3.expanded_tensor[0]))-sympify(expc)), 0)
