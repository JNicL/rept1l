###############################################################################
#                                tensoradd.py                                 #
###############################################################################

#============#
#  Includes  #
#============#

import sys
import os
import re

from sympy import (Matrix, symbols, I, numbered_symbols, sympify, Dummy,
                   Symbol, Rational, Poly)
from rept1l.combinatorics import fold
from rept1l.tensor.dirac import diracAlgebra
from rept1l.tensor.tensorarg import TensorArg
from rept1l.tensor.tensormul import MTensor, DomainError, get_dummies_set
from rept1l.tensor.tensormul import sub_args

#===========#
#  Methods  #
#===========#

#==============================================================================#
#                            StoreMeta & StoreClass                            #
#==============================================================================#

class StoreMeta(type):
  """
  StoreMeta controls the 'reuse' option which, when set to true, does not
  initiate class instances if all !optional args are the same
  """
  stored_tensor_args = {}
  #def __new__(cls, name, bases, dct):
    #print "Allocating memory for class", name
    #return type.__new__(cls, name, bases, dct)

  def _compare_classes(cls, *args, **kwargs):
    equal = True
    other_cls = StoreMeta.stored_tensor_args[args]
    assert(hasattr(cls, 'optional_args'))
    for attr in cls.optional_args:
      if attr in kwargs:
        if kwargs[attr] != getattr(other_cls, attr):
          equal = False
          break
    return equal

  def __call__(cls, *args, **kwargs):
    if 'reuse' in kwargs:
      if not kwargs['reuse']:
        return type.__call__(cls, *args, **kwargs)

      if args in StoreMeta.stored_tensor_args:
        equal = cls._compare_classes(*args, **kwargs)
        if equal:
          return StoreMeta.stored_tensor_args[args]

      else:
        self = cls.__new__(cls, *args, **kwargs)
        cls.__init__(self, *args, **kwargs)
        StoreMeta.stored_tensor_args[args] = self
        return self
    else:
      return type.__call__(cls, *args, **kwargs)

class StoreClass(object):
  __metaclass__ = StoreMeta

  def __init__(self):
    super(StoreClass, self).__init__()

#==============================================================================#
#                              ArgResolveMismatch                              #
#==============================================================================#

class ArgResolveMismatch(Exception):
  """
  ArgResolveMismatch is raised when adding tensors sharing the arguments which
  differ in their resolve status.
  """
  def __init__(self, args_repl):
    self.args_repl = args_repl

  def __str__(self):
    return repr(self.value)

#==============================================================================#
#                                NonZeroTensor                                 #
#==============================================================================#

class NonZeroTensorException(Exception):
  pass

#==============================================================================#
#                                 ScalarTensor                                 #
#==============================================================================#

class ScalarTensorException(Exception):
  pass

#==============================================================================#
#                                   ATensor                                    #
#==============================================================================#

class ATensor(object):
  """ Tensor class based on MTensor allowing to add tensors. """
  @property
  def expanded_tensor(self):
    if self._expanded_tensor is None:
      ret = self.reconstruct_tensor(lazy=False)
      self._expanded_tensor = ret._expanded_tensor
    return self._expanded_tensor

  @expanded_tensor.setter
  def expanded_tensor(self, expt):
    self._expanded_tensor = expt

  def __init__(self, arguments, lazy=True, **kwargs):
    if type(arguments) is MTensor:
      self.tensors = [arguments]
    else:
      self.tensors = [MTensor(arguments, zero_expand=True, lazy=lazy, **kwargs)]

    self.set_all_args_resolved()
    self.paired_arguments = self.tensors[0].paired_arguments
    self.lazy = lazy
    if not lazy:
      self._expanded_tensor = self.tensors[0].expanded_tensor
    else:
      self._expanded_tensor = None
    self.args = self.tensors[0].args
    self.symbol = self.tensors[0].symbol

  def set_all_args_resolved(self):
    """ Extends the methods on MTensors to ATensors. """
    # sufficient to copy the state from the first tensor
    self.all_args_resolved = self.tensors[0].all_args_resolved
    self.no_arg_resolved = self.tensors[0].no_arg_resolved

  def copy(self, lazy=True):
    """ Builds a copy of the ATensor including a copy of MTensors and
    arguments. """
    # Take the MTensor dependencies as a basis
    new_tensors = [u.copy(lazy=lazy) for u in self.tensors]
    ret = ATensor(new_tensors[0], lazy=self.lazy)
    ret.tensors = new_tensors
    for arg in self.args:
      ret.arg_resolve_state(arg.symbol, arg.resolve)
    ret = ret.reconstruct_tensor(lazy=lazy)
    return ret

  def __repr__(self):
    """
    Defined analogously to MTensor.
    """
    self.__class__ = MTensor
    ret = MTensor.__repr__(self)
    self.__class__ = ATensor
    return ret

  def __getitem__(self, args):
    """ Defined analogously to MTensor.  """
    # trigger expansion if lazy
    self.expanded_tensor

    self.__class__ = MTensor
    ret = MTensor.__getitem__(self, args)
    self.__class__ = ATensor
    return ret

  def __mul__(self, other, lazy=True):
    """ Performs the multiplication of MTensors applied to ATensor instances.
    For safety, original tensor are copied to prevent any any side effects.
    >>> i_arg = TensorArg('-1', domain=[0, 1], resolve=True)
    >>> mytensor = ATensor([i_arg], numerical=[1, 1], symbol=Symbol('t'))
    >>> product = mytensor*mytensor
    >>> product.expanded_tensor
    [2]
    >>> product.get_args()
    []
    """
    # stores the sum of all products of tensors between self and other
    # e.g. self = a + b, other = c + d -> tensors = [a*c, b*c, a*d, b*d]
    tensors = []
    for self_tens in self.tensors:
      if hasattr(other, 'tensors'):
        for other_tens in other.tensors:
          other_copy = other_tens.copy(lazy=lazy)
          tensors.append(MTensor([], symbol=1) * self_tens.copy(lazy=lazy) *
                         other_copy)
      else:
        self_tens = self_tens.copy(lazy=lazy) * sympify(other)
        tensors.append(self_tens)

    # TODO: (nick 2015-03-27) Not sure if this case triggers at all.
    # Actually it should only trigger if a tensor of self or other is delta, but
    # not sure if it makes sense
    if tensors[0].symbol is None:
      ret_symbol = None
    else:
      ret_symbol = tensors[0].symbol

    ret = ATensor(tensors[0].args, symbol=ret_symbol,
                  numerical=tensors[0].numerical)
    ret.paired_arguments = tensors[0].paired_arguments

    if not (lazy and self.lazy):
      ret.args = ret.tensors[0].args
      ret._expanded_tensor = [0]*len(ret.paired_arguments)
      if len(tensors[0].args) > 0:
        for arg in ret.paired_arguments:
          index_arg = ret.paired_arguments.index(arg)
          ret._expanded_tensor[index_arg] = 0
          for tens in tensors:
            ret._expanded_tensor[index_arg] += tens.expanded_tensor[index_arg]

      # Catching scalar case.
      else:
        ret._expanded_tensor[0] = 0
        for tens in tensors:
          ret._expanded_tensor[0] += tens.expanded_tensor[0]
    else:
      ret._expanded_tensor = None

    ret.tensors = tensors
    return ret

  def reconstruct_tensor(self, lazy=True):
    """ Extends the method reconstruct_tensor defined on MTensors for ATensors.

    >>> arg1 = TensorArg('i', domain=range(4), resolve=True)
    >>> arg2 = TensorArg('j', domain=range(4), resolve=True)
    >>>
    >>> tensor1 = ATensor([arg1], symbol=Symbol('t'))
    >>> tensor2 = ATensor([arg2], symbol=Symbol('t'))
    >>> tensor1 is tensor1.reconstruct_tensor()
    True

    >>> tensor2 is tensor2.reconstruct_tensor()
    True

    >>> tensor = tensor1*tensor2
    >>> tensor_rc = tensor.reconstruct_tensor()
    >>> tensor_rc.args == tensor.args
    True
    """
    if len(self.tensors) > 1:
      # We do not force the reconstruction (lazy=lazy) for MTensors which is
      # triggered automaticall when adding up the results.
      b = [ATensor(u.reconstruct_tensor(lazy=True)) for u in self.tensors[1:]]
      a = ATensor(self.tensors[0].reconstruct_tensor(lazy=True))
      ret = fold(lambda x, y: x.__add__(y, lazy=lazy), b, a)
      return ret
    elif len(self.tensors) == 1:
      if self.tensors[0].is_primitive:
        self.tensors[0].expand()
        self._expanded_tensor = self.tensors[0].expanded_tensor
        self.paired_arguments = self.tensors[0].paired_arguments
        self.all_args_resolved = self.tensors[0].all_args_resolved
        self.no_arg_resolved = self.tensors[0].no_arg_resolved
        self.args = self.tensors[0].args
        self.symbol = self.tensors[0].symbol
        return self
      else:
        # For one tensor we have to force lazy=lazy for MTensor as well
        mtens = self.tensors[0].reconstruct_tensor(lazy=lazy)
        return ATensor(mtens, lazy=lazy)

  def substitute_arg(self, arg_old, arg_new, subs_substructure=True):
    """ Extends the method substitute_arg defined on MTensors for ATensors.

    :param arg_old: argument to-be replaced
    :type  arg_old: TensorArg

    :param arg_new: argument to-be replaced with
    :type  arg_new: TensorArg

    :param subs_substructure: Enable disable recursive substitution in
                              substructures.
    :type  subs_substructure: bool

    >>> arg1 = TensorArg('i', domain=range(2), resolve=True)
    >>> arg2 = TensorArg('j', domain=range(2), resolve=True)
    >>> arg3 = TensorArg('j', domain=range(2), resolve=False)
    >>> arg4 = TensorArg('i', domain=range(2), resolve=False)

    >>> tensor1 = ATensor([arg1], symbol=Symbol('t'))
    >>> tensor2 = ATensor([arg2], symbol=Symbol('t'))

    >>> tensor = tensor1*tensor2
    >>> tensor.paired_arguments
    [(0, 0), (0, 1), (1, 0), (1, 1)]
    >>> tensor.expanded_tensor
    [t(0)**2, t(0)*t(1), t(0)*t(1), t(1)**2]

    >>> tensor.substitute_arg(arg2, arg3)
    >>> tensor = tensor.reconstruct_tensor()
    >>> tensor.paired_arguments
    [(0, 'j'), (1, 'j')]
    >>> tensor.expanded_tensor
    [t(0)*t(j), t(1)*t(j)]

    >>> tensor.substitute_arg(arg1, arg4)
    >>> tensor = tensor.reconstruct_tensor()
    >>> tensor.paired_arguments
    [('i', 'j')]
    >>> tensor.expanded_tensor
    [t(i)*t(j)]
    """
    self.args = self.args[:]
    success = False
    for pos, arg in enumerate(self.args):
      if (arg == arg_old) is True:
        self.args[pos] = arg_new
        success = True
        break
    if not success:
      raise Exception('Arg substition in ATensor failed')
    if subs_substructure:
      for tens in self.tensors:
        tens.substitute_arg(arg_old, arg_new, subs_substructure)

  def arg_resolve_state(self, arg_sym, resolve, subs_substructure=True,
                        original=False):
    """ Changes the resolve state of `arg_sym` to `resolve` in the tensor and
    all subtensors """
    for arg in self.args:
      if arg.symbol == arg_sym:
        if original:
          arg.__resolve = resolve
        else:
          arg.resolve = resolve

    if subs_substructure:
      for tens in self.tensors:
        tens.arg_resolve_state(arg_sym, resolve,
                               subs_substructure=subs_substructure)

  def __rmul__(self, other):
    return self.__mul__(other)

  def zero_add_case(self, other):
    """ If self is the zero tensor, the method returns a copy of other.  """
    if not self.tensors[0].is_primitive:
      raise NonZeroTensorException
    self_args_symbol = [v.symbol for v in self.tensors[0].args]
    if len(self_args_symbol) == 0 and self.expanded_tensor[0] == 0:
      ret = ATensor(other.tensors[0].args, symbol=other.tensors[0].symbol,
                    numerical=other.tensors[0].numerical)
      ret.tensors = other.tensors[:]
      return ret
    else:
      raise NonZeroTensorException

  def scalar_add(self, other):
    """
    If both tensors are scalars their value is added and returned. The case of
    scalars is treated on different footing because scalars have no arguments.
    """
    other_args_symbol = [u.symbol for u in other.tensors[0].args]
    self_args_symbol = [v.symbol for v in self.tensors[0].args]
    if len(self_args_symbol) == 0 and len(other_args_symbol) == 0:
      ret = ATensor(other.tensors[0].args, symbol=other.tensors[0].symbol,
                    numerical=other.tensors[0].numerical)
      # necessary to execute `expanded_tensor` here?
      ret.expanded_tensor = [other.expanded_tensor[0] + self.expanded_tensor[0]]
      ret.tensors = self.tensors[:] + other.tensors[:]
      return ret
    else:
      raise ScalarTensorException

  def tensor_arg_substitution(self, repl_dict):
    """ Replaces arguments by new arguments and reconstructs the tensor.

    :param repl_dict: dict of strings defining the replacment rules
    :type  repl_dict: dict
    """
    if len(set(repl_dict.values())) < len(repl_dict):
      raise Exception('Mapping different argument on the same argument which ' +
                      'is not allowed.')

    args = {str(u.symbol): u for u in self.args}

    # in order to prevent collisions the substitution is done in two steps
    subs_pass1 = []
    subs_pass2 = []
    for key in repl_dict:
      if key in args:
        arg = args[key]
        arg_new_dummy = arg.copy(symbol='dummy' + repl_dict[key])
        arg_new = arg.copy(symbol=repl_dict[key])
        subs_pass1.append((arg, arg_new_dummy))
        subs_pass2.append((arg_new_dummy, arg_new))
      else:
       raise Exception('Tensor arg ' + key + ' not found in tensor. ' +
                       'Cannot proceed with compute_tensor_index_shift.')
    # Perform dummy substitutions
    for arg_old, arg_new_dummy in subs_pass1:
      self.substitute_arg(arg_old, arg_new_dummy)
    # Perform final substitutions
    for arg_new_dummy, arg_new in subs_pass2:
      self.substitute_arg(arg_new_dummy, arg_new)

    return self.reconstruct_tensor()

  def check_arguments(self, other):
    """ Checks if the arguments of self and other permit addition """
    self_args = self.tensors[0].args
    other_args = other.tensors[0].args

    test1 = [[arg == v for v in self.tensors[0].args if arg == v][0]
             for arg in other_args]

    test2 = [[arg == v for v in other.tensors[0].args if arg == v][0]
             for arg in self_args]

    if not (all(test1) and all(test2)):
      raise DomainError('tensors args do not match')

    mismatch = {}
    for pos, match_value in enumerate(test1):
      if type(match_value) is tuple:
        mismatch[pos] = match_value[1]

    # everthing is fine and the tensors can be summed
    if len(mismatch) == 0:
      return True
    # the arguments are the same except for resolve status the
    # old_arg(resolve=False) are stored in args_repl and should be replaced with
    # new_arg(resolve=True) in the corresponding tensor.
    elif all(u == ['resolve'] for u in mismatch.values()):
      args_repl = {'self': [], 'other': []}
      for pos in mismatch:
        arg = other_args[pos]
        #old_arg = arg.copy()
        #new_arg = arg.copy()
        if arg.resolve is False:
          #new_arg.resolve = True
          old_arg = arg.copy()
          new_arg = TensorArg(arg.symbol, domain=arg.domain,
                              resolve=True, metric=arg.metric)
          args_repl['other'].append((old_arg, new_arg))
        else:
          #old_arg.resolve = False
          new_arg = arg.copy()
          old_arg = TensorArg(arg.symbol, domain=arg.domain,
                              resolve=False, metric=arg.metric)
          args_repl['self'].append((old_arg, new_arg))

      raise ArgResolveMismatch(args_repl)
    else:
      raise DomainError('Arg mismatch cannot be resolved: ' + str(mismatch))

  def __add__(self, other, lazy=True):
    if not (isinstance(other, ATensor)):
      raise DomainError('Addition of non-ATensor structures')

    addition_allowed = True
    other_args_symbol = [u.symbol for u in other.tensors[0].args]

    self_args_symbol = [v.symbol for v in self.tensors[0].args]

    # hack to be able to add zeros and thus, able to iterate over sums of
    # tensors
    try:
      return self.zero_add_case(other)
    except NonZeroTensorException:
      pass

    try:
      return self.scalar_add(other)
    except ScalarTensorException:
      pass

    if len(other_args_symbol) != len(self_args_symbol):
      raise DomainError('tensors args do not match')

    for sym in self_args_symbol:
      addition_allowed = addition_allowed and (sym in other_args_symbol)

    try:
      self.check_arguments(other)

    except ArgResolveMismatch as e:
      for old_arg, new_arg in e.args_repl['self']:
        self.substitute_arg(old_arg, new_arg)

      if len(e.args_repl['self']) > 0:
        self = self.reconstruct_tensor(lazy=lazy)

      for old_arg, new_arg in e.args_repl['other']:
        other.substitute_arg(old_arg, new_arg)

      if len(e.args_repl['other']) > 0:
        other = other.reconstruct_tensor(lazy=lazy)

    if not addition_allowed:
      raise DomainError("Trying to add different tensor structures")

    index_dict = dict()
    for sym in self_args_symbol:
      index_dict[sym] = other_args_symbol.index(sym)

    ret = ATensor(self.tensors[0].args, symbol=self.tensors[0].symbol,
                  numerical=self.tensors[0].numerical)

    if not (lazy and self.lazy):
      ret._expanded_tensor = [0]*len(self.paired_arguments)
      for arg in self.paired_arguments:
        arg_for_other = [None]*len(arg)

        for i in range(len(self_args_symbol)):
          sym = self_args_symbol[i]
          arg_for_other[index_dict[sym]] = arg[i]

        arg_for_other = tuple(arg_for_other)
        index_self = self.paired_arguments.index(arg)
        index_other = other.paired_arguments.index(arg_for_other)
        ret._expanded_tensor[index_self] = 0
        ret._expanded_tensor[index_self] = (self.expanded_tensor[index_self] +
                                            other.expanded_tensor
                                            [index_other])
    # copy is crucial here
    ret.tensors = self.tensors[:]
    ret.tensors.extend(other.tensors)
    return ret

  def remove_dummies(self):
    for i in range(len(self.expanded_tensor)):
      term = self.expanded_tensor[i]
      dummies = get_dummies_set(term)
      repl_dict = dict()
      for dummy in dummies:
        # maybe add the case where leading underscores are removes from
        # dummy.name
        repl_dict[dummy] = Symbol(dummy.name)
      if len(repl_dict.keys()) > 0:
        self.expanded_tensor[i] = self.expanded_tensor[i].subs(repl_dict)

    return self

  def get_args(self):
    return self.tensors[0].get_args()

  def get_all_args(self):
    ret = set(self.tensors[0].get_all_args())
    for u in self.tensors[1:]:
      ret.update(set(u.get_all_args()))
    return list(ret)

  def string_subs(self, string, dict_repl):
    if len(dict_repl) > 0:
      p = (re.compile('|'.join(re.escape(key)
           for key in dict_repl.keys()), re.S))
      return p.sub(lambda x: dict_repl[x.group()], string)
    else:
      return string

  def replace_args(self, repl_dict, res=None):
    pattern = re.compile('|'.join(repl_dict.keys()))
    if res is None:
      ret = []
      for term in self.expanded_tensor:
        ret.append(sub_args(str(term), repl_dict))
      return ret
    else:
      ret = []
      for term in res:
        ret.append(sub_args(str(term), repl_dict))
      return ret

  def find_primitive_tensor_symbol(self, symbol, repl=False, debug=False):
    """
    Applies find_primitive_tensor_symbol to all tensors in self.tensor.

    :repl: In contrast to the function defined on MTensors, repl is dictionary,
           where the keys are tensor numbers. This allows for different
           substitution rules between addends.
    """
    ret = {i: [] for i in range(len(self.tensors))}
    for pos, mtens in enumerate(self.tensors):
      if repl:
        repl_mtensor = repl[pos]
      else:
        repl_mtensor = False
      res = mtens.find_primitive_tensor_symbol(symbol, repl=repl_mtensor,
                                               debug=debug)
      if repl and res:
        self.tensors[pos] = res
        ret[pos].append(True)
      else:
        ret[pos].append(res)
    if repl:
      return self
    else:
      return ret

  def contraction_indices(self):
    used_contractions = []
    for t in self.tensors:
      for ms in t.mstrucs:
        for arg in ms.get_all_args():
          try:
            if int(arg.symbol) < 0:
              used_contractions.append(int(arg.symbol))
          except:
            pass
    return used_contractions

def simplify(a, subs={}):
  """ Wrapper for sympy's simplify method """
  from sympy import simplify as sympy_simplify
  if (type(a) is MTensor) or (type(a) is ATensor):
    a._expanded_tensor = [sympy_simplify(u).subs(subs)
                          for u in a.expanded_tensor]
    return a
  else:
    return sympy_simplify(a)

def conjugate(a):
  """ Wrapper for sympy's simplify method """
  from sympy import conjugate as sympy_conjugate
  if (type(a) is MTensor) or (type(a) is ATensor):
    a.expanded_tensor = [sympy_conjugate(u)
                         for u in a.expanded_tensor]
    return a
  else:
    return sympy_conjugate(a)

#=============#
#  Shortcuts  #
#=============#

class CArg(TensorArg):
  def __init__(self, *args):
    super(CArg, self).__init__(*args, domain=range(4),
                               resolve=False, reuse=True)

class OneTensor(ATensor):
  def __init__(self, *args):
    symbol = 1
    self.one = True
    super(OneTensor, self).__init__([], symbol=symbol)

class DeltaTensor(ATensor):
  delta = Symbol('Delta')

  def __init__(self, *args):
    super(DeltaTensor, self).__init__(args, symbol=DeltaTensor.delta,
                                      numerical=MTensor.LS.g, delta=True)

if __name__ == "__main__":
  #import doctest
  #doctest.testmod()
  dom = range(4)
  g = MTensor.LS.g
  gamma = MTensor.LS.gamma
  P = Symbol('P')

  cp1 = TensorArg('-1', domain=dom, resolve=True, metric=g)
  cp2 = TensorArg('-2', domain=dom, resolve=True, metric=g)
  ps_closed = TensorArg('-3', domain=dom, resolve=True)
  ps1_open = TensorArg('i', domain=dom, resolve=True)
  ps2_open = TensorArg('j', domain=dom, resolve=True)
  ps3_open = TensorArg('k', domain=dom, resolve=True)
  ps4_open = TensorArg('l', domain=dom, resolve=True)

  gamma1 = ATensor([cp1, ps1_open, ps2_open],
                   numerical=gamma, symbol=Symbol('Gamma'))
  gamma2 = ATensor([cp2, ps3_open, ps4_open],
                   numerical=gamma, symbol=Symbol('Gamma'))
  gamma3 = ATensor([cp1, ps1_open, ps4_open],
                   numerical=gamma, symbol=Symbol('Gamma'))
  gamma4 = ATensor([cp2, ps3_open, ps2_open],
                   numerical=gamma, symbol=Symbol('Gamma'))

  gg1 = gamma1*gamma2 + gamma3*gamma4
