#==============================================================================#
#                                   model.py                                   #
#==============================================================================#

#============#
#  Includes  #
#============#

import os
from rept1l.autoct.modelfile import model, CTParameter
from rept1l.autoct.modelfile import TreeInducedCTVertex, CTVertex
from rept1l.autoct.auto_parameters_ct import auto_assign_ct_particle
from rept1l.autoct.auto_parameters_ct import set_light_particle
from rept1l.autoct.auto_parameters_ct import assign_counterterm
from rept1l.autoct.auto_parameters_ct import add_counterterm
from rept1l.autoct.auto_tadpole_ct import auto_tadpole_vertices
from rept1l.autoct.auto_tadpole_ct import assign_FJTadpole_field_shift

#===========#
#  Globals  #
#===========#

# coupling constructor
Coupling = model.object_library.Coupling

# modelname = 'Some model name'
# modelgauge = "Some gauge"

# features = {'fermionloop_opt' : False,
#             'qcd_rescaling' : True,
#            }


# access to instances
param = model.parameters
P = model.particles
L = model.lorentz

# LCT allows to access the 2-point lorentz structures defined in lorentz_ct.py
import rept1l.autoct.lorentz_ct as LCT

#=============================#
#  External Parameter Orders  #
#=============================#

# In UFO, parameters have no order assigned in fundamental
# couplings (e.g. power of QED, QCD). REPT1L requires that information
# for a CT expansion which can be passed by filling `parameter_orders`:

# Example:
#parameter_orders = {param.aEW: {'QED': 2},
                    #param.aS: {'QCD': 2}}


#===============================#
#  Fermion mass regularization  #
#===============================#

#for p in [P.e__minus__, P.mu__minus__, P.ta__minus__, P.u, P.d, P.c, P.s, P.b]:
  #set_light_particle(p)

###################################
#  SM Optimizations for Fermions  #
###################################

#doublets = [(P.nu_e, P.e__minus__), (P.nu_mu, P.mu__minus__),
#            (P.nu_tau, P.tau__minus__), (P.u, P.d), (P.c, P.s), (P.t, P.b)]

#generations = [(P.e__minus__, P.mu__minus__, P.tau__minus__),
#               (P.d, P.s, P.b)]

#==============================================================================#
#                           coupling renormalization                           #
#==============================================================================#


# One can assign similar counterterms to non-independent parameters in order to
# avoid ambigous representation due to elimination in favor of external
# parameters. However, one must make sure that the counterterms are compatible
# with each other.

# Then assign the ct parameter to couplings which are then used as the ct
# expansion
#assign_counterterm(param.ee, 'dZee', 'ee*dZee')
#assign_counterterm(param.gs, 'dZgs', 'gs*dZgs')

#==============================#
#  Declare tadpole parameters  #
#==============================#

#dt = add_counterterm('dt', 'dt')

# the tadpole parameter are useful to declare parameter which are zero at
# Tree-Level, but have a one-loop expansion. Can be used not only for tadpoles
# Interally, the alogrithm then knows that it is safe to set the values to zero
# after renormalization, which optimizes ct expressions.

#tadpole_parameter = [param.t]

#==============================================================================#
#                                  particles                                   #
#==============================================================================#

# Automatic assignment of mass-counterterms and wavefunction. Possible mixing
# have to be passed to the method.
#mixings = {P.a: [P.Z], P.Z: [P.a]}
#auto_assign_ct_particle(mixings)

# TreeInducedCTVertex/CTVertex instances are defined similar to normal vertices
# in FeynRules. You can use all couplings/lorentz from your model and additional
# couplings from couplings_ct and lorentz_ct.

#########################################
#  Renormalization of the Gauge-fixing  #
#########################################

# Example how to induce counterterms from TreeInducedCTVertex. Note that the
# the vertex itself is not written to the modelfile, only the Counterterms
# expansion of it.

# Example of a commonly used (non-)renormalization of the Gauge-Fixing function
# in the SM

#GC_MZ_GF = Coupling(name='GC_MZ_GF',
                    #value='MZ',
                    #order={'QED': 0})

#GC_MW_GF = Coupling(name='GC_MW_GF',
                    #value='I*MW',
                    #order={'QED': 0})

#GC_MMW_GF = Coupling(name='GC_MMW_GF',
                     #value='-I*MW',
                     #order={'QED': 0})

#V_WPGM = TreeInducedCTVertex(name='V_WPGM',
                             #particles=[P.W__plus__, P.G__minus__],
                             #color=['1'],
                             #type='treeinducedct',
                             #loop_particles='N',
                             #lorentz=[LCT.VS1],
                             #couplings={(0, 0): GC_MW_GF})

#V_WMGP = TreeInducedCTVertex(name='V_WMGP',
                             #particles=[P.W__minus__, P.G__plus__],
                             #color=['1'],
                             #type='treeinducedct',
                             #loop_particles='N',
                             #lorentz=[LCT.VS1],
                             #couplings={(0, 0): GC_MMW_GF})

#V_ZG0 = TreeInducedCTVertex(name='V_ZG0',
                            #particles=[P.Z, P.G0],
                            #color=['1'],
                            #type='treeinducedct',
                            #loop_particles='N',
                            #lorentz=[LCT.VS1],
                            #couplings={(0, 0): GC_MZ_GF})

#===========================#
#  Tadpole n-Vertices, n=2  #
#===========================#

# The way how counterterms can be added by hand at the example of tadpole
# counterterms in the SM in the Denner scheme

#GC_GGTadpole = Coupling(name='GC_GG',
                        #value='I*dt/vev',
                        #order={'QED': 2})


#V_G0G0_T = CTVertex(name='V_G0G0_T',
                    #particles=[P.G0, P.G0],
                    #color=['1'],
                    #type='massct',
                    #loop_particles='N',
                    #lorentz=[LCT.SS1],
                    #couplings={(0, 0): GC_GGTadpole})

#V_GPGM_T = CTVertex(name='V_GPGM_T',
                    #particles=[P.G__plus__, P.G__minus__],
                    #color=['1'],
                    #type='massct',
                    #loop_particles='N',
                    #lorentz=[LCT.SS1],
                    #couplings={(0, 0): GC_GGTadpole})
