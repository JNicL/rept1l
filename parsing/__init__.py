#==============================================================================#
#                                 __init__.py                                  #
#==============================================================================#

from .regular import FormParse, string_subs, ParseColor
from .formprocess import suppress_stdout_stderr, FormProcess, derive_tensor_expr
from .formprocess import get_antiparticle, is_antiparticle, ProcessNotExist
from .vertexparticles import ParseVertexParticles
from .parseamplitude import ParseAmplitude

#========#
#  Info  #
#========#

__author__ = "Jean-Nicolas lang"
__date__ = "31. 3. 2016"
__version__ = "0.9"
