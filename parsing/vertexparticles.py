#==============================================================================#
#                              vertexparticles.py                              #
#==============================================================================#

from __future__ import print_function

import re
from rept1l.parsing import FormProcess, FormParse
from rept1l.helper_lib import get_anti_particle
from rept1l.helper_lib import get_particle_mixings, get_vertex_mixing

class VertexParticles(FormProcess):

  def __init__(self, vertexmode=True, tree=False, **process_definitions):
    level_process = {'tree': tree, 'bare': True, 'ct': False, 'r2': False}
    if vertexmode:
      form_amplitude = 5
    else:
      form_amplitude = 6
    process_definitions['level_process'] = level_process
    process_definitions['form_amplitude'] = form_amplitude
    process_definitions['all_couplings_active_frm'] = False
    super(VertexParticles, self).__init__(**process_definitions)

class ParseVertexParticles(FormParse):

  particle_pattern = r'c\(((?:\d+,)*\d+)\)'
  #particle_pattern = r'c\(45,\d+\)'

  def __init__(self, filein):
    """ Custom parsing routine, not entirely using FormParse. Amplitudes are
    stored in the attribute `amplitudes`. """
    super(ParseVertexParticles, self).__init__()
    self.vertex_particles = []

    with open(filein, 'r') as fin:
      text = fin.read()

      # find all ampltiude blocks corresponding to different cut particles and
      # branches (Tree Loop R2 CT)
      matches = re.findall(self.global_pattern, text)

      amps_reg = []
      exprs = []

      # Sums up all amplitudes with the same colorflow and perturbative order
      for match_id, match in enumerate(matches):
        for comb in re.findall(self.particle_pattern, match[1]):
          c = tuple(int(u) for u in comb.split(','))
          if c not in self.vertex_particles:
            self.vertex_particles.append(c)

      import rept1l.Model as Model
      model = Model.model
      ap = model.model_objects.all_particles
      self.vertex_particles = [[ap[v-1] for v in u]
                               for u in self.vertex_particles]


#==============================================================================#
#                             get_vertex_particles                             #
#==============================================================================#

def get_vertex_particles(particles, mixings=True, include_antiprocess=True,
                         **kwargs):
  mp = get_particle_mixings(particles)
  get_vertex_mixing(particles, mp)
  VP = VertexParticles(particles=particles, **kwargs)
  PVP = ParseVertexParticles(VP.amplitude_file)
  vp = {}
  for pc in PVP.vertex_particles:
    p = tuple(sorted(u.name for u in pc))
    np = len(p)
    if np not in vp:
      vp[np] = set()
    vp[np].add(p)
    if include_antiprocess:
      import rept1l.Model as Model
      model = Model.model
      pca = [get_anti_particle(u, model.model_objects.all_particles)
             for u in pc]
      pa = tuple(sorted(u.name for u in pca))
      vp[np].add(pa)

    if mixings:
      if include_antiprocess:
        pcc = [pc, pca]
      else:
        pcc
      pcc = [pc, pca] if include_antiprocess else [pc]
      for pp in pcc:
        mp = get_particle_mixings(pp)
        mp = get_vertex_mixing(pp, mp)
        for m in mp:
          np = len(m)
          vp[np].add(m)
  return vp

if __name__ == '__main__':
  import rept1l.Model as Model
  model = Model.model
  P = model.P

  # H4f
  #particles = [P.h1, P.e__plus__, P.ve__tilde__, P.mu__plus__, P.vm]

  # # Higgs Strahlung to nu nu
  #in1 = [P.u__tilde__, P.u]
  #in2 = [P.d__tilde__, P.d]
  #in3 = [P.c__tilde__, P.c]
  #in4 = [P.s__tilde__, P.s]
  #out = [P.ve__tilde__, P.ve, P.H]

  #particles = in1 + out
  #ctexp1 = get_vertex_particles(particles, mixings=False)

  #particles = in2 + out
  #ctexp2 = get_vertex_particles(particles, mixings=False)

  #particles = in3 + out
  #ctexp3 = get_vertex_particles(particles, mixings=False)

  #particles = in4 + out
  #ctexp4 = get_vertex_particles(particles, mixings=False)

  #ctexp = {}
  #for n in range(2, 6):
    #ctexp[n] = ctexp1[n] | ctexp2[n] | ctexp3[n] | ctexp4[n]

  #for np, pc in ctexp.iteritems():
    #print "np:", np
    #for p in pc:
      #print ' '.join(p)

  # Higgs decay into gamma gamma
  # particles = [P.H, P.A, P.A]
  # particles = [P.g, P.g, P.H, P.g]

  # alpha renormalization
  #particles = [P.h1, P.ta__plus__, P.ta__minus__]

  # DY
  #in1 = [P.e__minus__, P.e__plus__]
  #in2 = [P.e__minus__, P.ve__tilde__]

  # ctexp = get_vertex_particles(particles, vertexmode=False, tree=True, mixings=False)
  #ctexp2 = get_vertex_particles(in2 + in2, mixings=True)

  # order_NLO = (('QCD', 4), ('QED', 1), ('YKT', 0), ('HGT', 1))
  # order_NLO = (('QCD', 2), ('GC1', 2), ('GC1', 4), ('GC2', 2))
  # particles = [P.W__minus__, P.W__plus__, P.u, P.u__tilde__]
  # ctexp = get_vertex_particles(particles, mixings=False)


  order_NLO = (('QCD', 4), ('QED', 2))
  particles = [P.A, P.A, P.u, P.u__tilde__, P.d, P.d__tilde__]
  ctexp = get_vertex_particles(particles, mixings=False, select_power_NLO=order_NLO)

  # order_NLO = (('QCD', 5), ('QED', 1), ('YKT', 0), ('HGT', 1))
  # particles = [P.g, P.g, P.H, P.g]
  # ctexp2 = get_vertex_particles(particles, mixings=False)
  #
  # # order_NLO = (('QCD', 6), ('QED', 1), ('YKT', 0), ('HGT', 1))
  # # particles = [P.g, P.g, P.H, P.g, P.g]
  # # ctexp3 = get_vertex_particles(particles, mixings=False)
  # ctexp3 = {}
  #
  # order_NLO = (('QCD', 4), ('QED', 2), ('YKT', 0), ('HGT', 2))
  # particles = [P.g, P.g, P.H, P.H]
  # ctexp4 = get_vertex_particles(particles, mixings=False)
  #
  # order_NLO = (('QCD', 5), ('QED', 2), ('YKT', 0), ('HGT', 2))
  # particles = [P.g, P.g, P.H, P.H, P.g]
  # ctexp5 = get_vertex_particles(particles, mixings=False)

  # order_NLO = (('QCD', 6), ('QED', 2), ('YKT', 0), ('HGT', 1))
  # particles = [P.g, P.g, P.H, P.H, P.g, P.g]
  # ctexp6 = get_vertex_particles(particles, mixings=False)
  # ctexp6 = {}

  # ctexp = {}
  # for n in range(2, 6):
    # ctexp[n] = ctexp1[n] | ctexp2[n] | ctexp3[n] | ctexp4[n] | ctexp5[n] | ctexp6[n]



  #ctexp = {}
  #for n in range(2, 5):
    #ctexp[n] = ctexp1[n] | ctexp2[n]

  for np, pc in ctexp.iteritems():
    print("np:", np)
    for p in pc:
      print(' '.join(p))
