#==============================================================================#
#                                formprocess.py                                #
#==============================================================================#
from __future__ import print_function, division

#============#
#  Includes  #
#============#

import os
import subprocess
import re

from sympy import Symbol, sympify

import rept1l_config
from rept1l.logging_setup import log
from rept1l.combinatorics import P
from rept1l.baseutils import CurrentBase
import rept1l.Model as Model

model = Model.model
#===========#
#  Globals  #
#===========#

#---------------     symbolic expressions for certain bases     ---------------#
lorentz_base_symbol_dict = {}

lorentz_base_symbol_dict[2] = {}
lorentz_base_symbol_dict[2][0] = sympify(1)

lorentz_base_symbol_dict[2*P*P] = {}
base_id = CurrentBase.get_arg_id(2*P*P, [[-1, 1], [-1, 1]])
lorentz_base_symbol_dict[2*P*P][base_id] = Symbol('p12')

def unique_symbol_generator(i=-1):
  """ Generator for unique symbols.  """
  while True:
    i += 1
    yield Symbol("U" + str(i))

unique_symbol = unique_symbol_generator()
#------------------------------------------------------------------------------#

#===========#
#  Methods  #
#===========#

class ProcessNotExist(Exception):
  """ ProcessNotExist exception is raised if the process does not exist after
  recolas generation call.
  """
  def __init__(self):
    pass


#==============================================================================#
#                               get_antiparticle                               #
#==============================================================================#

def get_antiparticle(particle):
  if (particle.selfconjugate):
    return particle
  else:
    for particle_tmp in model.model_objects.all_particles:
      if (particle_tmp.name == particle.antiname):
        return particle_tmp

#==============================================================================#
#                               is_antiparticle                                #
#==============================================================================#

def is_antiparticle(pname):
  pattern1 = r'\+'
  pattern2 = r'\~'
  if (re.search(pattern1, pname) or re.search(pattern2, pname)):
    return True
  else:
    return False

#==============================================================================#
#                            suppress_stdout_stderr                            #
#==============================================================================#

# Define a context manager to suppress stdout and stderr.
class suppress_stdout_stderr(object):
  """
  A context manager for doing a "deep suppression" of stdout and stderr in
  Python, i.e. will suppress all print, even if the print originates in a
  compiled C/Fortran sub-function.
     This will not suppress raised exceptions, since exceptions are printed
  to stderr just before a script exits, and after the context manager has
  exited.
  """
  def __init__(self):
      # Open a pair of null files
      self.null_fds = [os.open(os.devnull, os.O_RDWR) for x in range(2)]
      # Save the actual stdout (1) and stderr (2) file descriptors.
      # os.dup makes a copy -> must be closed too
      self.save_fds = (os.dup(1), os.dup(2))

  def __enter__(self):
      # Assign the null pointers to stdout and stderr.
      os.dup2(self.null_fds[0], 1)
      os.dup2(self.null_fds[1], 2)

  def __exit__(self, *_):
      # Re-assign the real stdout/stderr back to (1) and (2)
      os.dup2(self.save_fds[0], 1)
      os.dup2(self.save_fds[1], 2)

      # Close the null files
      os.close(self.save_fds[0])
      os.close(self.save_fds[1])
      os.close(self.null_fds[0])
      os.close(self.null_fds[1])

class FormProcess(object):
  """
  Generates one-particle irreduzible amplitudes in FORM format. The amplitudes
  are constructed with RECOLA  and the result is stored to `process_(index).log`
  """

  args = ['vertex', 'particles']

  # processing with form
  if hasattr(rept1l_config, 'formcmd'):
    cmd = rept1l_config.formcmd
  else:
    cmd = 'form'

  def __init__(self, index=1, gen_process=True, suppress_gen_stdout=False,
               verbose=True, **process_definition):
    """
    :index: The process index defined in recola, leave it to 1 if not multiple
            processes are defined simultaneously.

    :param gen_process: Generate the process. If false the form amplitude is not
                        generated, but computed from process_{index}.frm.
    :type  gen_process: bool

    :param suppress_gen_stdout: Suppress the stdout when calling
                                generate_processes_rcl. Disabling the stdout is
                                not recommended.
    :type  suppress_gen_stdout: bool

    :process definition:
      Mandatory:
       The process can be specified by
       -particles: Particles list as UFO particle instances or strings.
       -vertex: UFO Vertex instance

       Optional:
       -set_compute_tadpole_rcl: Allow tadpoles in loop amplitudes.
       -color_optimization: Optimize colorflow.  0 for no optimization which can
                            be used if external U1 Gluons are needed, e.g. for
                            deriving rational terms.
                            1 optimized colorflow - introduces mother and
                            daughter branches.
                            2 compromises one and neglects external U1 Gluons.
       -level_process: Define which branches are computed:
                       {bare: bool, loop: bool, r2: bool, ct: bool} where bool
                       is either True or False.
       -vertex_functions: If true only one-particle irreducible loop graphs are
                          generated.
       -form_amplitude: define how the form amplitude should be generated.
                        Allowed values 0 to 4.

       -draw_amplitude: Generate tex code. Allowed values 0 1 or 2.
    """

    from pyrecola import (define_process_rcl,
                          generate_processes_rcl,
                          set_lp_rcl,
                          set_draw_level_branches_rcl,
                          set_cutparticle_rcl,
                          unset_cutparticle_rcl,
                          set_form_rcl,
                          set_compute_selfenergy_rcl,
                          set_compute_tadpole_rcl,
                          set_complex_mass_form_rcl,
                          set_masscut_form_rcl,
                          set_masscut_rcl,
                          set_init_collier_rcl,
                          set_ifail_rcl,
                          set_all_couplings_active_rcl,
                          set_all_ct_couplings_active_rcl,
                          set_colour_optimization_rcl,
                          set_vertex_functions_rcl,
                          set_particle_ordering_rcl,
                          set_print_recola_logo_rcl,
                          set_fermionloop_optimization_rcl,
                          reset_recola_rcl,
                          process_exists_rcl,
                          select_power_BornAmpl_rcl,
                          unselect_power_BornAmpl_rcl,
                          unselect_all_powers_BornAmpl_rcl,
                          select_power_LoopAmpl_rcl,
                          unselect_power_LoopAmpl_rcl,
                          unselect_all_powers_LoopAmpl_rcl)
    # define the process
    if not [u in process_definition for u in self.args].count(True) == 1:
      raise TypeError('Process must be defined either by vertex or particles')

    for u in process_definition:
      setattr(self, u, process_definition[u])

    # define the proces via particles
    if hasattr(self, 'particles'):
      if len(self.particles) < 2:
        raise ValueError('The number of particles must be higher equal 2.')

      if all(type(u) is str for u in self.particles):
        particles = [u for u in self.particles]
      elif all(hasattr(u, 'name') for u in self.particles):
        particles = [u.name for u in self.particles]
      else:
        raise Exception('Particles must be given as strings or must have a ' +
                        'name attribute.')

      if len(self.particles) <= 3:
        particle_in = particles[0]
        particles_out = ' '.join(particles[1:])
      else:
        particle_in = ' '.join([particles[0], particles[1]])
        particles_out = ' '.join(particles[2:])

      process = particle_in + ' -> ' + particles_out

      # Check
      if 'compute_tadpole' in process_definition:
        set_compute_tadpole_rcl_b = process_definition['compute_tadpole']
      else:
        set_compute_tadpole_rcl_b = False

      if not set_compute_tadpole_rcl_b:
        set_compute_tadpole_rcl(False)
        if len(self.particles) == 2:
          set_compute_selfenergy_rcl(True)
        else:
          set_compute_selfenergy_rcl(False)
      else:
        set_compute_selfenergy_rcl(False)
        set_compute_tadpole_rcl(True)

    # define the proces via vertex
    elif hasattr(self, 'vertex'):
      vertex = process_definition['vertex']
      nparticles = len(vertex.particles)
      try:
        assert(nparticles > 2)
      except:
        raise ValueError('Vertex must be a >2 Vertex')
      if nparticles == 3:
        particles_in = [get_antiparticle(vertex.particles[0]).name]
        particles_out = [u.name for u in vertex.particles[1:]]
      else:
        particles_in = [get_antiparticle(vertex.particles[0]).name,
                        get_antiparticle(vertex.particles[1]).name]
        particles_out = [u.name for u in vertex.particles[2:]]

      particles_in = ' '.join(particles_in)
      particles_out = ' '.join(particles_out)
      process = particles_in + ' -> ' + particles_out
      set_compute_selfenergy_rcl(False)

    order = 'NLO'
    exits = False
    if gen_process:

      set_particle_ordering_rcl(False)
      set_fermionloop_optimization_rcl(False)
      set_init_collier_rcl(False)
      set_print_recola_logo_rcl(False)
      if hasattr(self, 'vertex_functions'):
        set_vertex_functions_rcl(self.vertex_functions)
      if hasattr(self, 'ifail'):
        set_ifail_rcl(self.ifail)

      if hasattr(self, 'level_process'):
        lp_val = {'tree': 0, 'bare': 1, 'ct': 2, 'r2': 3}
        for lp in self.level_process:
          set_lp_rcl(lp_val[lp], self.level_process[lp])
      else:
        set_lp_rcl(0, False)  # disable tree
        set_lp_rcl(1, True)   # enable loop
        set_lp_rcl(2, True)   # enable ct
        set_lp_rcl(3, False)  # disable r2

      if hasattr(self, 'cutparticle'):
        set_cutparticle_rcl(self.cutparticle)
      else:
        unset_cutparticle_rcl()

      if hasattr(self, 'form_amplitude'):
        set_form_rcl(self.form_amplitude)
      else:
        set_form_rcl(4)  # complete amplitude without external spinors

      if hasattr(self, 'draw_amplitude'):
        set_draw_level_branches_rcl(self.draw_amplitude)
      else:
        set_draw_level_branches_rcl(0)  # complete amplitude without external spinors

      set_complex_mass_form_rcl(False)

      if 'masscut_frm' in process_definition:
        set_masscut_form_rcl(process_definition['masscut_frm'])
      else:
        set_masscut_form_rcl(False)

      if 'masscut' in process_definition:
        if not hasattr(self.__class__, 'set_masscut_done'):
          set_masscut_rcl(process_definition['masscut'])
          self.__class__.set_masscut_done = True

      # Tree-level couplings which are zero are not taken along
      if 'all_couplings_active_frm' in process_definition:
        set_all_couplings_active_rcl(process_definition['all_couplings_active_frm'])
      else:
        set_all_couplings_active_rcl(True)
      # enable counterterms by default -> can be initialized with zero,
      # renormalization still works
      set_all_ct_couplings_active_rcl(True)

      if 'color_optimization' in process_definition:
        set_colour_optimization_rcl(process_definition['color_optimization'])
      else:
        set_colour_optimization_rcl(0)

      define_process_rcl(index, process, order)

      if verbose:
        for pd in process_definition:
          log(pd + ':' + str(process_definition[pd]), 'info')

      if 'select_power_LO' in process_definition:
        unselect_all_powers_BornAmpl_rcl(index)
        for order, power in process_definition['select_power_LO']:
          select_power_BornAmpl_rcl(index, order, power)

      if 'select_power_NLO' in process_definition:
        unselect_all_powers_LoopAmpl_rcl(index)
        for order, power in process_definition['select_power_NLO']:
          select_power_LoopAmpl_rcl(index, order, power)

      if 'unselect_power_LO' in process_definition:
        for order, power in process_definition['unselect_power_LO']:
          unselect_power_BornAmpl_rcl(index, order, power)

      if 'unselect_power_NLO' in process_definition:
        for order, power in process_definition['unselect_power_NLO']:
          unselect_power_LoopAmpl_rcl(index, order, power)

      if suppress_gen_stdout:
        with suppress_stdout_stderr():
          generate_processes_rcl()
      else:
        generate_processes_rcl()

      exits = process_exists_rcl(index)

      reset_recola_rcl()

    if not exits:
      raise ProcessNotExist()

    flag = '-l'
    self.process_file = 'process' + str(index) + '.frm'
    bash_cmd = [self.cmd, flag, self.process_file]
    if gen_process:
      subprocess.check_output(bash_cmd)
      path = os.getcwd()
      for file in os.listdir(path):
        if file.endswith('.str') and 'xformx' in file:
          try:
            os.remove(file)
          except Exception as e:
            pass

    self.processed_file = 'process' + str(index) + '.log'
    self.amplitude_file = 'process' + str(index) + '.log'
    self.colorflow_file = 'colorstructures' + str(index) + '.txt'

  def __enter__(self):
    return self.processed_file

  def delete_process_files(self):
    os.remove(self.process_file)
    os.remove(self.amplitude_file)
    os.remove(self.colorflow_file)

  def __exit__(self, *args):
    try:
      self.delete_process_files()
    except OSError as e:
      print("e:", e)

#==============================================================================#
#                               get_collier_args                               #
#==============================================================================#

def get_tensor_args(tensor_args, complex_masses=False, real_momentum=False,
                    known_masses=[]):
  """ Constructs the scalar integral arguments suited for an evaluation with
  collier_cll wrapper functions.

  :param tensor_args: momenta and masses
  :type  tensor_id:   list of strings

  :param known_masses: list of mass names and squared mass names as strings
  :type  known_masses: list

  :return: Collier argument for collier_call subroutine
  """

  # Transform argument to squared argument if known, i.e. M -> M2.
  # 0 is set to cnul, other parameters are just squared
  new_args = ['cnul' if arg == '0' else arg + '2' if arg in known_masses else
              arg + '**2d0' if arg != 'M0' else 'cnul'
              for arg in tensor_args]

  # prepare the arguments for complex-valued masses
  if complex_masses:
    if real_momentum:
      mom = ('complex(' + new_args[0] + ', 0)' if new_args[0] in known_masses
             else new_args[0])
      masses = ['cnul' if arg == '0'else
                'c' + arg if arg in known_masses and arg[0] != 'r'
                else arg for arg in new_args[1:]]
      new_args = [mom] + masses

    else:
      new_args = ['cnul' if arg == '0'else
                  'c' + arg + '_reg' if arg in known_masses and arg[0] != 'r'
                  else arg
                  for arg in new_args]

  # arguments are expected to be real-valued
  else:
    new_args = ['complex(' + arg + ', 0)'
                if arg in known_masses else arg
                for arg in new_args]

  return tuple(new_args)


def get_collier_args(scalar_tensor, tensor_id, tensor_args,
                     complex_masses=False):
  """
  Given a scalar_tensor, its id and arguments, get_collier_args returns
  the collier argument for the main_cll function and appropriate arguments.

  :param scalar_tensor: n-point scalar tensor
  :type  scalar_tensor: character, e.g. B, C, D

  :param tensor_id: C00 -> 00 = tenso_id
  :type  tensor_id: string

  :param tensor_args: momenta and masses
  :type  tensor_id:   list of strings

  :return: Collier argument for main_cll subroutine
  """
  max_p = {'A': 0,
           'B': 1,
           'DB': 1,
           'C': 2,
           'D': 3}
  # rank is given by the number of digits, e.g. C00 -> rank=2
  rank = len(tensor_id)

  # two zeros equal one g_{mu nu}
  metrics2 = tensor_id.count('0')
  assert((metrics2 % 2 == 0) or (metrics2 == 1))
  metrics = metrics2/2

  # momenta   C12 -> p_1^\mu*p_2^\nu coefficient
  ps = {}
  assert(scalar_tensor in max_p)
  for i in range(1, max_p[scalar_tensor]+1):
    ps[i] = tensor_id.count(str(i))
  # arguments
  new_args = ['cnul' if arg == '0' else
              arg + '2' if arg != 'M0' else
              'cnul'
              for arg in tensor_args]
  if complex_masses:
    new_args = ['c' + arg if arg[0] == 'M' else arg for arg in new_args]
  else:
    new_args = ['complex(' + arg + ', 0)'if arg[0] == 'M' else
                arg for arg in new_args]

  ret = [metrics] + ps.values()
  return tuple(ret), tuple(new_args)

#==============================================================================#
#                              derive_tensor_expr                              #
#==============================================================================#

def derive_tensor_expr(tensors, tabs=0, debug=False):
  """ Translates tensor expressions to fortran code.

  :param tensors:  Tensor dictionary. An entry has the following structure.
                  'T0': ('B', ['0', ['p1', 'M0', 'M0']])
                  - T0 is the tensor symbol(string).
                  - B is the scalar tensor.
                  - 0 is the tensor id
                  - ['p1', 'M0', 'M0'] are the tensor arguments
  :type  tensors: dict
  >>> tensors = {'T8': ('DB', '0', ('MT', 'M0', 'MT'))}
  >>> derive_tensor_expr(tensors).printStatment()
    T8 = DB0(cMT2_reg, cnul, cMT2_reg)
  >>> tensors = {'T8': ('A', '0', ('MT',))}
  >>> derive_tensor_expr(tensors).printStatment()
    T8 = A0(cMT2_reg)
  >>> tensors = {'T9': ('HT', '', ('MT','MB'))}
  >>> derive_tensor_expr(tensors).printStatment()
    T9 = HT(cMT2_reg, cMB2_reg)
  """
  n_point = {'A': 0,
             'B': 1,
             'DB': 1,
             'C': 2,
             'D': 3,
             'HT': -1}   # HT special function used to handle scaleless cases

  from rept1l.pyfort import spaceBlock
  from rept1l.particles import ParticleMass
  masses = [u.name for u in ParticleMass.masses.values()]
  rmasses = ['r' + u for u in masses]
  masses2 = [u + '2' for u in masses]
  rmasses2 = ['r' + u + '2' for u in masses]
  masses = masses + masses2 + rmasses + rmasses2
  tensor_call = spaceBlock(tabs=tabs)
  for tensor in tensors:
    if tensors[tensor][0] in n_point:
      args = get_tensor_args(tensors[tensor][2], True, known_masses=masses)
      tens_sym = tensors[tensor][0]
    elif tensors[tensor][0][:-1] in n_point:
      args = get_tensor_args(tensors[tensor][2], True, real_momentum=True,
                             known_masses=masses)
      tens_sym = tensors[tensor][0][:-1]
    else:
      raise ValueError('Tensor: ' + tensors[tensor][0] + ' not supported.')

    args_joined = ', '.join(args)
    scalar = tens_sym + tensors[tensor][1]
    tensor_call + (tensor + ' = ' + scalar + '(' + args_joined + ')')
    if debug:
      st = 'write(*,*) "' + tensor + '", ' + tensor
      tensor_call + st
  return tensor_call

#==============================================================================#
#                              recola_selfenergy                               #
#==============================================================================#

def parse_selfenergy(particle, particle_out=None, index=1):
  """
  Derives the expression for the selfenergy. The final result can be processed
  as it is returned as sympy expressions.

  :param particle: particle for which the selfenergy should be computed
  :type  particle: string, e.g. 'g' = Gluon, 'A' = photon, ...

  :param index: process index
  :type  index: integer
  """

  from pyrecola import (define_process_rcl, generate_processes_rcl,
                        set_lp_rcl, set_form_rcl, set_compute_selfenergy_rcl,
                        set_compute_tadpole_rcl, set_selfenergy_projection,
                        set_complex_mass_form_rcl, reset_recola_rcl)
  print('Computing selfenergy for ' + particle.name)
  # define the process
  if not particle_out:
    process = particle.name + ' -> ' + particle.name
  else:
    process = particle.name + ' -> ' + particle_out.name
  order = 'NLO'
  with suppress_stdout_stderr():
    define_process_rcl(index, process, order)
    set_lp_rcl(0, False)  # disable tree
    set_lp_rcl(1, True)   # enable loop
    set_lp_rcl(2, True)   # diable ct
    set_lp_rcl(3, False)  # disable r2
    set_form_rcl(4)       # complete amplitude without external spinors
    if not particle_out:
      set_compute_selfenergy_rcl(True)

    set_complex_mass_form_rcl(False)
    generate_processes_rcl()
    reset_recola_rcl()

  # processing with form
  if hasattr(rept1l_config, 'formcmd'):
    cmd = rept1l_config.formcmd
  else:
    cmd = 'form'

  flag = '-l'
  process_file = 'process' + str(index) + '.frm'
  bash_cmd = [cmd, flag, process_file]
  subprocess.check_output(bash_cmd)
  print("processing finished")

  # load vertices. Needed to assign the couplings.
  from rept1l.vertices_rc import Vertex
  Vertex.load()

  from pyrecola import get_driver_timestamp_rcl, get_modelname_rcl
  if Vertex.timestamp != get_driver_timestamp_rcl():
    log('Timestamp of vertex class and model do not match!', 'error')
    log('Timestamp Vertex: ' + Vertex.timestamp, 'debug')
    log('Timestamp model: ' + get_driver_timestamp_rcl(), 'debug')
    log('Rept1l model path: ' + Model.mpath, 'debug')
    if hasattr(Model.model, 'modelname'):
      modelname = Model.model.modelname
    else:
      modelname = 'NOTSET'
    log('Recola model name: ' + get_modelname_rcl(), 'debug')
    log('Rept1l model name: ' + modelname, 'debug')
    raise Exception('Timestamp of Vertex and Model do not match!')

  # parse the amplitudes
  processed_file = 'process' + str(index) + '.log'
  from .parseamplitude import ParseAmplitude
  sigma = ParseAmplitude(Vertex.treevertices, Vertex.ctvertices)
  sigma.start(processed_file)
  sigma.replace_tensor_symbols()
  return sigma

if __name__ == "__main__":
  import doctest
  doctest.testmod()
  for i in range(10):
    with suppress_stdout_stderr():
      print("i:", i)
