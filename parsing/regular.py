#==============================================================================#
#                                  regular.py                                  #
#==============================================================================#

#============#
#  Includes  #
#============#

import sys
import os
import re
import types
from sympy import Symbol, Poly, I, IndexedBase, sympify
from collections import OrderedDict
from six import iteritems

from rept1l.coupling import RCoupling
from rept1l.formutils import FormPype
import rept1l.combinatorics as cb
from rept1l.helper_lib import parse_UFO, string_subs

#=====================#
#  Globals & Methods  #
#=====================#

def couplings(i=-1):
  while True:
    i += 1
    yield Symbol("C" + str(i))

def lorentz(i=-1):
  while True:
    i += 1
    yield "L" + str(i)

def tensor(i=-1):
  while True:
    i += 1
    yield "T" + str(i)

def propagator(i=-1):
  while True:
    i += 1
    yield "D" + str(i)

global callback_results
callback_results = OrderedDict()

def callback(match):
  sym = next(callback.v)
  callback_results[sym] = match.group()
  return sym

#==============================================================================#
#                                  ParseColor                                  #
#==============================================================================#

class ParseColor(object):
  r""" Parses amplitudes and in particular the colour information
  # TODO: (nick 2016-05-05) rename class

  >>> c = 'legs=3\nparticles=2,2,2\nfermionsign=1'
  >>> c = c + '\ncs=1:(1,2)(2,3)\ncs=2:(1,3)(2,2)'

  >>> from rept1l.helper_lib.fakefile import fakefile
  >>> PC = None
  >>> with fakefile(c) as tmp:
  ...   PC = ParseColor(tmp)
  ...   PC.colors
  ...   PC.particles
  {1: ({1: 2, 2: 3}, 1), 2: ({1: 3, 2: 2}, 1)}
  [2, 2, 2]

  >>> PC.compute_color_base_relations({})
  {(((1, 3), (2, 2)), 1): 1, (((1, 2), (2, 3)), 1): 0}
  """

  legs_pattern = r'legs=(\d)'
  par_pattern = r'particles=((?:\d+,)*\d+)'
  color_pattern = r'cs=(\d+?):((\(\d*?,\d*?\))+)'
  fermionsign_pattern = r'fermionsign=([-\d]+)'

  legs_pattern = re.compile(legs_pattern)
  par_pattern = re.compile(par_pattern)
  color_pattern = re.compile(color_pattern)
  fermionsign_pattern = re.compile(fermionsign_pattern)

  def __init__(self, filename):
    self.colors = {}
    self.new_colors = {}
    self.cs_id = {}
    with open(filename, 'r') as f:
      text = f.read()
      matches = re.findall(self.legs_pattern, text)
      self.n_In = int(matches[0]) - 2

      matches = re.findall(self.fermionsign_pattern, text)
      if len(matches) == 1:
        self.fermionsign = int(matches[0])

      matches = re.findall(self.par_pattern, text)
      self.particles = [int(u) for u in matches[0].split(',')]
      matches = re.findall(self.color_pattern, text)
      for match in matches:
        self.colors[int(match[0])] = {}
        if match[1]:
          for cd in string_subs(match[1], {')(': ';'})[1:-1].split(';'):
            cd = cd.split(',')
            self.colors[int(match[0])].update({int(cd[0]): int(cd[1])})

          self.colors[int(match[0])] = (self.colors[int(match[0])], self.n_In)

      # not sure if still used?
      self._gen_colordeltas()

  def _gen_colordeltas(self):
    up_index = [Symbol('i' + str(i)) for i in range(6)]
    down_index = [Symbol('j' + str(i)) for i in range(6)]
    Delta = IndexedBase('Delta')

    self.colordeltas = {}
    for cs in self.colors:
      tmp = 1
      for up_i in self.colors[cs][0]:
        tmp *= Delta[up_index[up_i], down_index[self.colors[cs][0][up_i]]]

      self.colordeltas[cs] = (tmp, self.n_In)

  def compute_color_base_relations(self, colors_dict):
    """ Relating RECOLA and REPT1L colorflow IDs.

    Internally, REPT1L uses colorflow IDs to label colorflows. Similar,
    RECOLA introduces colorflow IDs (cs_id) at runtime to label every
    colorflow.

    In total there are 3 different internal colorflow labellings. The first
    one (denoted as cs_id) is the number assigned to a specific colorflow which
    is done while generating the model file. This id is model (and model
    generation setup) dependent.  The second one (denoted as cs) is the number
    associated to a colorflow and a given process which is defined at runtime in
    recola.  The last one is the explicit colorflow either as color_dict or
    color_key.  The rational terms are derived only with the knowledge of cs and
    cs must be related to cs_id which is done by identifying the colorflow
    representation in both cases.

    :colors_dict: color_key -> c_id
    :colors_dict_cc: c_id -> color_dict
    :self.cs_id: cs -> c_id
    :self.new_colorflows: c_id -> color_dict
    :colors_dict: colorflow id representation in the UFO wrapper
    """
    colors_dict_cc = {colors_dict[key]: ({u[0]: u[1] for u in key[0]}, key[1])
                      for key in colors_dict}

    for cs in self.colors:
      if self.colors[cs] not in colors_dict_cc.values():
        new_id = len(colors_dict)
        self.new_colors[new_id] = self.colors[cs]
        color_key = (tuple([(color, self.colors[cs][0][color])
                            for color in self.colors[cs][0]]),
                     self.colors[cs][1])
        colors_dict.update({color_key: new_id})
        colors_dict_cc.update({new_id: self.colors[cs]})
        self.cs_id[cs] = new_id
      else:
        # TODO: nick bug fixed not checked So 05 Okt 2014 16:52:14 CEST
        self.cs_id[cs] = list(colors_dict_cc.keys())[list(colors_dict_cc.values()).
                                                     index(self.colors[cs])]
    return colors_dict

def parse_g(struc, args, **kwargs):
  """ Parses Metric arguments. Metric in FORM has the structure of a kronecker
  delta d_(mu1,mu3)

  >>> parse_g('d_', 'mu1,mu3')
  ([Metric], [[(1, 'mu1'), (3, 'mu3')]])
  """
  # by convention all spacetime indices have 2 characters, the argument index
  # starts from the third digit
  args = [(int(u[2:]), u) for u in (v.strip() for v in args.split(','))]
  strucs = [cb.g]
  order = cb.lorentz_base.order[strucs[0]]
  return strucs, [args]

def parse_Si(struc, args, coffset=0, **kwargs):
  """ Parses Sigma^{\mu,\nu} arguments. Sigma in FORM has the structure
  si(i1,i2,mu,nu) where i1, i2 are the anti-fermion, fermion arguments.

  >>> parse_Si('si', 'i3,i4,ro2,ro3')
  ([Sigma], [[(2, 'ro2'), (3, 'ro3'), (3, 'i3'), (4, 'i4')]])
  >>> parse_Si('si','i3,i4,p3,nu35')
  ([P, Sigma], [[-1, 3], [-1, (35, 'nu35'), (3, 'i3'), (4, 'i4')]])
  >>> parse_Si('si','i3,i4,p3,p4')
  ([P, P, Sigma], [[-1, 3], [-2, 4], [-1, -2, (3, 'i3'), (4, 'i4')]])
  >>> parse_Si('si','i3,i4,p7,ro1')
  ([P, Sigma], [[-1, 7], [-1, (1, 'ro1'), (3, 'i3'), (4, 'i4')]])
  """
  strucs = [cb.Sigma]
  args = args.split(',')
  ferm_args = args[:2]
  ferm_args = [(int(ferm_arg[1:]), ferm_arg) for ferm_arg in ferm_args]

  def is_spacetime_arg(arg):
    lsi = ['mu', 'nu', 'ro', 'al', 'be']
    return any(u in arg for u in lsi)

  open_pos = [is_spacetime_arg(v) for v in args[2:]]
  # open args present
  o_args = any(open_pos)
  if o_args:
    open_args = {pos: args[2:][pos] for pos in
                 [open_pos.index(v) for v in open_pos if v]}
    open_args = {pos: args[2:][pos] for pos, v in enumerate(open_pos) if v}

  mom_pos = ['p' in v for v in args[2:]]
  # momenta present
  m_args = any(mom_pos)
  if m_args:
    mom_args = {pos: args[2:][pos]for pos in
                [pos for pos, v in enumerate(mom_pos) if v]}

  if m_args and o_args:
    common_args = dict(list(mom_args.items()) + list(open_args.items()))
  elif m_args:
    common_args = mom_args
  elif o_args:
    common_args = open_args
  else:
    common_args = {}

  if m_args:
    strucs = [cb.P]*len(mom_args) + strucs

  p_args = []
  sigma_args = []

  contr_offset = coffset-1
  p_offset = -1
  for g_pos, i in enumerate(sorted(common_args.keys())):
    if 'p' in common_args[i]:
      p_offset += 1
      p_arg = int(common_args[i][1:])
      p_args.append([-p_offset+contr_offset, p_arg])
      sigma_arg = -p_offset+contr_offset
      sigma_args.append(sigma_arg)
    else:
      mu_arg = int(common_args[i][2:])
      sigma_arg = (mu_arg, common_args[i])
      sigma_args.append(sigma_arg)
  sigma_args += ferm_args

  #vb_args = [(int(args[2][2:]), args[2]), (int(args[3][2:]), args[3])]
  #return strucs, [vb_args + ferm_args]
  return strucs, p_args + [sigma_args]


def parse_PP(struc, coffset=0):
  """ Parses Pi^\mu Pj_\mu arguments. In FORM it has the structure: pi.pj
  The argument `args` is split into [id, args] where args are the open
  arguments.

  >>> parse_PP('p1.p3')
  ([P, P], [[-1, 1], [-1, 3]])

  >>> parse_PP('p1.p3', coffset=-3)
  ([P, P], [[-4, 1], [-4, 3]])
  """
  args = [[coffset-1, int(u[1:])] for u in struc.split('.')]
  strucs = [cb.P]*2
  return strucs, args

def parse_P(struc, args, **kwargs):
  """ Parses P^mu_i arguments. P in FORM has the structure: pi(mu)
  The argument `args` is split into [id, args] where args are the open
  arguments.

  >>> parse_P('p', ['1', 'mu2'])
  ([P], [[(2, 'mu2'), 1]])
  """
  new_args = [[(int(args[1][2:]), args[1]), int(args[0])]]
  strucs = [cb.P]
  return strucs, new_args

def parse_EPS(struc, args, coffset=0):
  """ Parses Parsing epsilon^(\mu, \nu, p_i, \al) arguments. Epsilon in FORM has
  the structure:  e_(mu1,nu2,p1,p2)

  >>> parse_EPS('e_', 'p1,nu2,p1,p2')
  ([P, P, P, Epsilon], [[-1, 1], [-2, 1], [-3, 2], [-1, (2, 'nu2'), -2, -3]])
  >>> parse_EPS('e_', 'mu1,mu2,mu3,mu4')
  ([Epsilon], [[(1, 'mu1'), (2, 'mu2'), (3, 'mu3'), (4, 'mu4')]])
  """
  def contraction_index(i=0):
    while True:
      i -= 1
      yield i

  def is_spacetime_arg(arg):
    lsi = ['mu', 'nu', 'ro', 'al', 'be']
    return any(u in arg for u in lsi)

  def contracted_momentum(momentum, momenta, generator):
    ci = next(generator)
    momenta.append((momentum, ci))
    return ci

  momenta = []
  args_split = (u.strip() for u in args.split(','))
  ci = contraction_index(coffset)
  args_new = [(int(u[2:]), u) if is_spacetime_arg(u)
              else contracted_momentum(u, momenta, ci)
              for u in args_split]
  p_strucs = []
  p_args = []
  for momentum in momenta:
    p_strucs.append(cb.P)
    if len(momentum[0]) > 1:
      p_args.append([momentum[1], int(momentum[0][1:])])
    else:
    # this case happens when the integration momentum did not drop out in
    # the R2 computation -> should be fixed in form. For now we set the
    # particle index to zero
      p_args.append([momentum[1], 0])

  if len(p_args) > 0:
    args = p_args + [args_new]
    strucs = p_strucs + [cb.Epsilon]
  else:
    args = [args_new]
    strucs = [cb.Epsilon]

  return strucs, args


def parse_Ga(struc, args, coffset=0):
  """ Parsing Gamma traces. In FORM they have the form:
    ga(i,j,POL,mu,p,..nu,..,)

  >>> parse_Ga('ga', 'i3,i4,[+]')
  ([ProjP], [[3, 4]])

  >>> parse_Ga('ga', 'i1,i2,mu3')
  ([Gamma], [[(3, 'mu3'), (1, 'i1'), (2, 'i2')]])

  >>> parse_Ga('ga', 'i3,i4,[+],ro2,ro3')
  ([GammaM, GammaP], [[(2, 'ro2'), (3, 'i3'), -1], [(3, 'ro3'), -1, (4, 'i4')]])

  >>> parse_Ga('ga', 'i1,i2,p3')
  ([P, Gamma], [[-1, 3], [-1, (1, 'i1'), (2, 'i2')]])

  >>> parse_Ga('ga', 'i1,i2,p3,p4')
  ([P, P, Gamma, Gamma], [[-2, 3], [-3, 4], [-2, (1, 'i1'), -1], \
[-3, -1, (2, 'i2')]])

  >>> parse_Ga('ga', 'i1,i2,[+],mu1,mu2')
  ([GammaM, GammaP], [[(1, 'mu1'), (1, 'i1'), -1], [(2, 'mu2'), -1, (2, 'i2')]])
  """
  args = args.split(',')
  ferm_args = args[:2]
  pols = ['[-]', '[+]']
  pols_pos = [[u in v for u in ['[-]', '[+]']] for v in args[2:]]
  polarized = any(any(u) for u in pols_pos)
  if polarized:
    # pol is either [-] or [+]
    pol = pols[pols_pos[[any(u) for u in pols_pos].index(True)].index(True)]

  def is_spacetime_arg(arg):
    lsi = ['mu', 'nu', 'ro', 'al', 'be']
    return any(u in arg for u in lsi)

  open_pos = [is_spacetime_arg(v) for v in args[2:]]
  # open args present
  o_args = any(open_pos)
  if o_args:
    open_args = {pos: args[2:][pos] for pos in
                 [open_pos.index(v) for v in open_pos if v]}
    open_args = {pos: args[2:][pos] for pos, v in enumerate(open_pos) if v}

  mom_pos = ['p' in v for v in args[2:]]
  # momenta present
  m_args = any(mom_pos)
  if m_args:
    mom_args = {pos: args[2:][pos]for pos in
                [pos for pos, v in enumerate(mom_pos) if v]}

  if m_args and o_args:
    common_args = dict(list(mom_args.items()) + list(open_args.items()))
  elif m_args:
    common_args = mom_args
  elif o_args:
    common_args = open_args
  else:
    common_args = {}

  if not (o_args or m_args):
    if not polarized:
      struc = cb.Gamma
    elif pol == '[-]':
      struc = cb.ProjM
    else:
      struc = cb.ProjP
    return [struc], [[int(u[1:]) for u in ferm_args]]
  else:
    further_gammas = len(common_args) - 1
    if polarized:
      pol = '[-]' if pol == '[+]' else '[+]'
      if pol == '[-]':
        strucs = [cb.GammaM]
      else:
        strucs = [cb.GammaP]
    else:
      strucs = [cb.Gamma]

    if polarized:
      if pol == '[-]':
        gp = cb.GammaM
        gap = cb.GammaP
      else:
        gp = cb.GammaP
        gap = cb.GammaM
      for i in range(1, further_gammas+1):
        if i % 2 == 0:
          strucs += [gp]
        else:
          strucs += [gap]
    else:
      strucs += [cb.Gamma] * further_gammas
    if m_args:
      strucs = [cb.P]*len(mom_args) + strucs

  p_args = []
  gamma_args = []

  contr_offset = coffset-1
  gamma_outer_indices = [(int(ferm_args[0][1:]), ferm_args[0])]
  for i in range(further_gammas):
    gamma_outer_indices += [-i + contr_offset] + [-i + contr_offset]

  gamma_outer_indices += [(int(ferm_args[1][1:]), ferm_args[1])]
  gamma_outer_indices = list(zip(gamma_outer_indices[::2],
                                 gamma_outer_indices[1::2]))
  contr_offset -= further_gammas
  p_offset = -1
  for g_pos, i in enumerate(sorted(common_args.keys())):
    if 'p' in common_args[i]:
      p_offset += 1
      p_arg = int(common_args[i][1:])
      p_args.append([-p_offset+contr_offset, p_arg])
      gamma_arg = [-p_offset+contr_offset,
                   gamma_outer_indices[g_pos][0],
                   gamma_outer_indices[g_pos][1]]
      gamma_args.append(gamma_arg)
    else:
      mu_arg = int(common_args[i][2:])
      gamma_arg = [(mu_arg, common_args[i]),
                   gamma_outer_indices[g_pos][0],
                   gamma_outer_indices[g_pos][1]]
      gamma_args.append(gamma_arg)

  return strucs, p_args + gamma_args

#==============================================================================#
#                                  R2Pattern                                   #
#==============================================================================#

class R2Pattern(object):

  global_pattern = r'(\[Amp.*?]\s=)(.*?)(;)'
  global_flags = re.S | re.M

  amp_pattern = r'(Amp[\d\w]+?)[,\]]'
  color_pattern = r'cs=(.*?)[,\]]'
  order_patterns = []
  for order in RCoupling.orders:
    order_patterns.append(r'(' + order + ')=(.*?)[,\]]')

  proj_pattern = r'pj=(\w+?)[,\]]'

  has_arg = True
  has_id = True

  # Parsing Gamma, Epsilon G_\mu\nu
  def get_sympy_struc_G_Eps_Ga(struc, args):
    lsi = ['mu', 'nu']
    if struc == 'd_':
      args = [(int(u[2:]), u) for u in args.split(',')]
      strucs = [cb.g]
      order = cb.lorentz_base.order[strucs[0]]
      return strucs, [args], order

    if any(i in args for i in lsi) and struc == 'e_':
      def contraction_index(i=0):
        while True:
          i -= 1
          yield i

      def contracted_momentum(momentum, momenta, generator):
        ci = next(generator)
        momenta.append((momentum, ci))
        return ci

      momenta = []
      args_split = (u.strip() for u in args.split(','))
      ci = contraction_index()
      args_new = [int(u[2:]) if 'mu' in u
                  else contracted_momentum(u, momenta, ci)
                  for u in args_split]
      p_strucs = []
      p_args = []
      for momentum in momenta:
        p_strucs.append(cb.P)
        print("momentum[0]:", momentum[0])
        print("momentum:", momentum)
        if len(momentum[0]) > 1:
          p_args.append([momentum[1], int(momentum[0][1:])])
        else:
        # this case happens when the integration momentum did not drop out in
        # the R2 computation -> should be fixed in form. For now we set the
        # particle index to zero
          p_args.append([momentum[1], 0])

      if len(p_args) > 0:
        args = p_args + [args_new]
        strucs = p_strucs + [cb.Epsilon]
      else:
        args = [args_new]
        strucs = [cb.Epsilon]

      # TODO: nick order should be a list of orders! Di 11 Nov 2014 14:41:59 CET
      # In this case we would have:
      # orders = [order[cb.P], order[cb.Epsilon]]

      order = cb.lorentz_base.order[cb.Epsilon]
      return strucs, args, order

    if struc == 'ga':
      args = args.split(',')
      ferm_args = args[:2]
      pols = ['[-]', '[+]']
      pols_pos = [[u in v for u in ['[-]', '[+]']] for v in args[2:]]
      polarized = any(any(u) for u in pols_pos)
      if polarized:
        # pol is either [-] or [+]
        pol = pols[pols_pos[[any(u) for u in pols_pos].index(True)].index(True)]

      open_pos = [any(i in v for i in lsi) for v in args[2:]]
      # open args present
      o_args = any(open_pos)
      if o_args:
        open_args = {pos: args[2:][pos] for pos in
                     [open_pos.index(v) for v in open_pos if v]}

      mom_pos = ['p' in v for v in args[2:]]
      # momenta present
      m_args = any(mom_pos)
      if m_args:
        mom_args = {pos: args[2:][pos]for pos in
                    [mom_pos.index(v) for v in mom_pos if v]}

      if m_args and o_args:
        common_args = dict(list(mom_args.items()) + list(open_args.items()))
      elif m_args:
        common_args = mom_args
      elif o_args:
        common_args = open_args
      else:
        common_args = {}

      if not (o_args or m_args):
        if not polarized:
          struc = cb.Gamma
        elif pol == '[-]':
          struc = cb.ProjM
        else:
          struc = cb.ProjP
        order = cb.lorentz_base.order[struc]
        return [struc], [[int(u[1:]) for u in ferm_args]], order
      else:
        further_gammas = len(common_args) - 1
        if polarized:
          pol = '[-]' if pol == '[+]' else '[+]'
          if pol == '[-]':
            strucs = [cb.GammaM]
          else:
            strucs = [cb.GammaP]
        else:
          strucs = [cb.Gamma]

        strucs += [cb.Gamma] * further_gammas
        if m_args:
          strucs = [cb.P]*len(mom_args) + strucs

      p_args = []
      gamma_args = []
      if m_args:
        contr_offset = len(mom_args)
      else:
        contr_offset = 0
      gamma_outer_indices = [(int(ferm_args[0][1:]), ferm_args[0])]
      for i in range(further_gammas):
        gamma_outer_indices += [-i - contr_offset] + [-i - contr_offset]

      gamma_outer_indices += [(int(ferm_args[1][1:]), ferm_args[1])]
      gamma_outer_indices = zip(gamma_outer_indices[::2],
                                gamma_outer_indices[1::2])
      for g_pos, i in enumerate(sorted(common_args.keys())):
        if 'p' in common_args[i]:
          p_arg = int(common_args[i][1:])
          p_args.append([-i, p_arg])
          gamma_arg = [-i,
                       gamma_outer_indices[g_pos][0],
                       gamma_outer_indices[g_pos][1]]
          gamma_args.append(gamma_arg)
        else:
          #if common_args[i][:2] == 'mu':
          mu_arg = int(common_args[i][2:])
          #else:
            #mu_arg = -int(common_args[i][2:])
          gamma_arg = [(mu_arg, common_args[i]),
                       gamma_outer_indices[g_pos][0],
                       gamma_outer_indices[g_pos][1]]
          gamma_args.append(gamma_arg)

      return strucs, p_args + gamma_args, cb.lorentz_base.order[cb.Gamma]

  def get_sympy_struc_A(struc, args):
    # args[0] is the scalar tensor index, e.g. B00 -> args[0] = '00'
    new_args = [args[0], args[1].split(',')]
    struc = 'A'
    return struc, new_args

  def get_sympy_struc_B(struc, args):
    # args[0] is the scalar tensor index, e.g. B00 -> args[0] = '00'
    new_args = [args[0], args[1].split(',')]
    struc = 'B'
    return struc, new_args

  def get_sympy_struc_DB(struc, args):
    # args[0] is the scalar tensor index, e.g. B00 -> args[0] = '00'
    new_args = [args[0], args[1].split(',')]
    struc = 'DB'
    return struc, new_args

  def get_sympy_struc_C(struc, args):
    # args[0] is the scalar tensor index, e.g. B00 -> args[0] = '00'
    new_args = [args[0], args[1].split(',')]
    struc = 'C'
    return struc, new_args

  def get_sympy_struc_HT(struc, args):
    # HT is a special scalar function which is zero if all arguments are
    # exaclty zero. Used to handle scaleless cases.
    new_args = ['', args.split(',')]
    struc = 'HT'
    return struc, new_args

  tensor_structures = {
      "A": (r'A',  has_arg, has_id, get_sympy_struc_A),
      "B": (r'B',  has_arg, has_id, get_sympy_struc_B),
      "DB": (r'DB',  has_arg, has_id, get_sympy_struc_DB),
      "C": (r'C',  has_arg, has_id, get_sympy_struc_C),
      "HT": (r'HT',  has_arg, not has_id, get_sympy_struc_HT),
      }

  lorentz_structures = {
      "G": (r'd_', has_arg, not has_id, parse_g),
      "P": (r'p', has_arg, has_id,  parse_P),
      "Gamma": (r'ga', has_arg, not has_id, parse_Ga),
      "Sigma": (r'si', has_arg, not has_id, parse_Si),
      "Epsilon": (r'e_', has_arg, not has_id, parse_EPS),
      "PP": (r'(p\d\.p\d)', not has_arg, not has_id, parse_PP)
      }

  math_syms = r'[\*\+-/]'
  whitespaces = r'\s*?'
  arguments = r'\(([a-z\d,\+-\[\]\s]*?)\)'
  id_arg = r'(\d+)'

  coupl_pows = ','.join(len(RCoupling.orders)*['\d{1,2}'])
  coupling_pattern = (r'c(?:\d{2})+\(\d{1,2},' + coupl_pows +
                      ',\d{1,2},\d{1,2}\s*\)')

  lorentz_patterns = {}
  for key in lorentz_structures:
    struc, arg, has_id,  _ = lorentz_structures[key]
    if arg and not has_id:
      lorentz_patterns[key] = re.compile(r'(' + struc + arguments + ')')
    elif arg and has_id:
      lorentz_patterns[key] = re.compile(r'(' + struc + id_arg +
                                         arguments + r')')
    else:
      lorentz_patterns[key] = re.compile(r'(' + struc + ')')

  tensor_patterns = []
  for key in tensor_structures:
    struc, arg, has_id,  _ = tensor_structures[key]
    if arg and not has_id:
      tensor_patterns.append(struc + arguments)
    elif arg and has_id:
      tensor_patterns.append(struc + id_arg + arguments)
    else:
      tensor_patterns.append(struc)

  global_pattern = re.compile(global_pattern, global_flags)
  amp_pattern = re.compile(amp_pattern)
  color_pattern = re.compile(color_pattern)
  order_patterns = [re.compile(op) for op in order_patterns]
  coupling_pattern = re.compile(coupling_pattern)
  proj_pattern = re.compile(proj_pattern)
  tensor_patterns = [re.compile(p) for p in tensor_patterns]


#==============================================================================#
#                               class FormParse                                #
#==============================================================================#

class FormParse(R2Pattern):
  """ Provides methods for parsing the output of form amplitudes.  """

  original_coupling_pattern = re.compile(r'c(?:\d{2})+\((?:\d{1,2},)*?\d{1,2}\s*\)')

  propagator_pattern = re.compile(r'((?i)den\(-?(\w+),(\w+)\))')

  def get_sympy_struc_A(struc, args):
    # args[0] is the scalar tensor index, e.g. A00 -> args[0] = '00'
    new_args = args.split(',')
    struc = 'A'
    return struc, new_args

  def get_sympy_struc_B(struc, args):
    # args[0] is the scalar tensor index, e.g. B00 -> args[0] = '00'
    new_args = args.split(',')
    struc = 'B'
    return struc, new_args

  def get_sympy_struc_C(struc, args):
    # args[0] is the scalar tensor index, e.g. C00 -> args[0] = '00'
    new_args = args.split(',')
    struc = 'C'
    return struc, new_args

  def get_sympy_struc_D(struc, args):
    # args[0] is the scalar tensor index, e.g. D00 -> args[0] = '00'
    new_args = args.split(',')
    struc = 'D'
    return struc, new_args

  def get_sympy_struc_E(struc, args):
    # args[0] is the scalar tensor index, e.g. E00 -> args[0] = '00'
    new_args = args.split(',')
    struc = 'E'
    return struc, new_args

  has_arg = True
  has_id = True
  tensor_structures2 = {'A': (r'A', has_arg, not has_id, get_sympy_struc_A),
                        'B': (r'B', has_arg, not has_id, get_sympy_struc_B),
                        'C': (r'C', has_arg, not has_id, get_sympy_struc_C),
                        'D': (r'D', has_arg, not has_id, get_sympy_struc_D),
                        'E': (r'E', has_arg, not has_id, get_sympy_struc_E)}

  math_syms = r'[\*\+-/]'
  whitespaces = r'\s*?'
  arguments = r'\(([a-z\d,\+-\[\]\s]*?)\)'
  id_arg = r'(\d+)'

  tensor_patterns2 = []
  for key in tensor_structures2:
    struc, _, _, _ = tensor_structures2[key]
    tensor_patterns2.append(re.compile(struc + arguments))


  digits_pattern = re.compile(r'([a-z]+\d+)')

  def __init__(self, coupl_dict={}, ls_dict={}, prop_dict={}, debug=False):
    self.coupl_dict = coupl_dict
    self.ls_dict = ls_dict
    self.prop_dict = prop_dict
    self.debug = debug

  @staticmethod
  def parse_UFO(expr):
    return parse_UFO(expr)

  def assign_propagator(self, amp):
    """ Assigning generic propagator expressions D_i in `amp` for FORM
    propagator. The propagator information is stored in prop_dict.

    >>> FP = FormParse(debug=True)
    >>> FP.assign_propagator('3/4*pi*den(0,MT)')
    '3/4*pi*D0'
    >>> FP.prop_dict
    {'D0': ('0', 'MT')}
    >>> FP.assign_propagator('3*pi*den(0,MB)')
    '3*pi*D1'
    >>> FP.assign_propagator('3*pi*(den(0,MT)+den(0,MB))')
    '3*pi*(D0+D1)'
    """
    matches = self.propagator_pattern.findall(amp)

    prop_repl = {}
    d = propagator(i=len(self.prop_dict)-1)
    for match_id, match in enumerate(matches):

      prop_expr = (match[1], match[2])
      if prop_expr not in self.prop_dict.values():
        prop_sym = next(d)
        self.prop_dict[prop_sym] = prop_expr
        prop_repl[match[0]] = prop_sym
      else:
        prop_sym = list(self.prop_dict.keys())[list(self.prop_dict.values()).
                                               index(prop_expr)]

        prop_repl[match[0]] = prop_sym

    return string_subs(amp, prop_repl)

  @classmethod
  def get_particle_keys(cls, vertices):
    """ Builds the vertices dictonary with particle combinations as keys. """
    if not hasattr(cls, 'particle_keys'):
      cls.particle_keys = {}
    for branch in vertices:
      if branch not in cls.particle_keys:
        cls.particle_keys[branch] = {tuple([p.name for p in vert.particles]):
                                     vert for vert in vertices[branch]}
    return cls.particle_keys

  def assign_couplings(self, amp, vertices=None, coupl_dict=None,
                       original_match=False):
    """ Assigning generic coupling expressions C_i in `amp` for FORM
    couplings. The couplings C_i are related to the couplings of the theory
    (GC_) via the vertices information `vertices`. The coupling dictionary
    `coupl_dict` {C_i: GC_, } can be updated, which can be used to avoid
    assigning the same coupling with different coupling expressions C_j.

    :param amp: FORM amplitude
    :type  amp: str

    :param vertices: list of vertex instance, ordered as dict according to
                     branch (Tree=1, Loop=1, CT=2, R2=3)
    :type  vertices: dict

    :param coupl_dict: A dictionary translating between generic coupling
                       expressions and the couplings of the theory or FORM
                       couplings
    :type coupl_dict: dict

    :param original_match: If True couplings are related to FORM couplings
                           instead of the couplings of the theory (via
                           `vertices`).
    :type  original_match: bool
    """
    if vertices is None and not original_match:
      raise Exception('Cannot assign couplings if no vertices are specified ' +
                      'and original match disabled')
    if coupl_dict is None:
      coupl_dict = self.coupl_dict

    if original_match:
      coupl = set(self.original_coupling_pattern.findall(amp))
    else:
      coupl = set(re.findall(self.coupling_pattern, amp))

    new_coupl = {}

    if not original_match:
      p_keys = self.get_particle_keys(vertices)

    # the len of the particle indices. Set to two digits
    p_len = 2
    for c in coupl:
      end = c.index('(')
      end2 = c.index(')')
      p_indices = c[1:end]
      assert(len(c[1:end]) % p_len == 0)

      if not original_match:
        import rept1l.Model as Model
        model = Model.model
        p_indices = [int(p_indices[i:i+p_len]) - 1
                     for i in range(0, len(p_indices), p_len)]
        p_key = tuple([model.model_objects.all_particles[i].name
                       for i in p_indices])

        # integers values extracted from FORM coupling
        coupl_props = [int(u) for u in c[end+1:end2].split(',')]

        # cN: coupling number associated to different lorentz structures
        cN = coupl_props[0]

        # coupl_pws: powers in the fundamental couplings. Not needed ...
        coupl_pows = coupl_props[1:len(RCoupling.orders)+1]
        coupl_pows = tuple([(RCoupling.orders[i], u)
                            for i, u in enumerate(coupl_pows)])
        # br: skeleton branch, (Tree/Loop=1, CT=2, R2=3)
        # brN: branch number which resolves the colorflow/coupling
        # order/propagator extension
        br, brN = coupl_props[len(RCoupling.orders)+1:]

        # lookup the coupling given information of particles, orders, branch.
        if p_key in p_keys[br]:
          #vertex = vertices[br][p_keys[br].index(p_key)]
          vertex = p_keys[br][p_key]
          coupling = vertex.couplings_brN[brN-1]['couplings'][cN-1]

          if coupling not in new_coupl:
            new_coupl[coupling] = [c]
          else:
            new_coupl[coupling].append(c)

        else:
          raise Exception('Unknown coupling: ' + str(c))
      else:
        new_coupl[c] = [c]

    coupl_repl = {}
    for i, c in enumerate(new_coupl.keys()):
      if c in coupl_dict:
        coupl_sym = coupl_dict[c]
      else:
        coupl_sym = 'C' + str(len(coupl_dict))
        coupl_dict[c] = coupl_sym
      for cc in new_coupl[c]:
        if c == 0:
          coupl_repl[cc] = '0'
        else:
          coupl_repl[cc] = coupl_dict[c]

    self.coupl_dict = coupl_dict
    return string_subs(amp, coupl_repl)

  def register_amplitude(self, amp_id_str, amps_reg, initexprs, pjs=None):
    """ Parses form amplitude string ids and returns values which further
    specify the amplitude such as color, order, cut particle in the loop etc.

    :amp_id_str: typical form amplitude id. See form codes.
    :amps_reg: List of tuples of color and coupling orders which specify what
               kind of amplitude have been registered.
    :initexprs: initial calls of new form amplitudes

    :return: amp_kind and amp_id.


    :amp_kind: String composed of the prefix `amp` plus
               - cut particle number if loop amplitude
               - `CT` if CT amplitude
    :pjs: Projections assigned

    >>> amp_id_str = '[Amp14,cs=1,QCD=2,QED=0] ='
    >>> amps_reg = []
    >>> initexprs = []
    >>> FP = FormParse(debug=True)
    >>> FP.register_amplitude(amp_id_str, amps_reg, initexprs)
    ('Amp14', '[amp(1,2,0)]')
    >>> amps_reg
    [('1', (('QCD', '2'), ('QED', '0')))]
    >>> initexprs
    ['l [amp(1,2,0)] = 0;']
    """
    amp_kind = re.search(self.amp_pattern, amp_id_str).group(1).strip()
    color = re.search(self.color_pattern, amp_id_str)
    orders = []
    for order_pattern in self.order_patterns:
      orders.append(re.search(order_pattern, amp_id_str))

    amp_order = tuple([(order.group(1), order.group(2))
                       for order in orders])
    # only take powers for identifying the amplitude
    order = ','.join([u[1] for u in amp_order])

    if pjs:
      amp_ids = {}
      for pj in pjs:
        amp_id = '[amp(' + color.group(1) + ',' + order + ',pj=' + pj + ')]'
        amp_ids[pj] = amp_id

        # initialize amplitude if not yet done
        ampinit = 'l ' + amp_id + ' = 0;'
        if ampinit not in initexprs:
          initexprs.append(ampinit)

    else:
      amp_id = '[amp(' + color.group(1) + ',' + order + ')]'
      amp_ids = amp_id

      # initialize amplitude if not yet done
      ampinit = 'l ' + amp_id + ' = 0;'
      if ampinit not in initexprs:
        initexprs.append(ampinit)

    if (color.group(1), amp_order) not in amps_reg:
      amps_reg.append((color.group(1), amp_order))

    return amp_kind, amp_ids

  def amplitude_split_tensors(self, amp_string):
    """ Splits an amplitude in terms of tensor integrals.

    >>> myamp = ('+i_*pi^-2*(-1/32*B([],p1,MT,MT)*c140515(1,1,1,1)' +
    ...          '*p1(mu1)*p1(mu2)*Nc + 1/32*B([],p1,M0,M0)*c140515(2,1,1,1)*'+
    ...          'p1(mu1)*p1(mu2)*Nc)')
    >>> FP = FormParse(debug=True)
    >>> res = FP.amplitude_split_tensors(myamp)
    >>> res['B([],p1,MT,MT)']
    '-B([],p1,MT,MT)*Nc*c140515(1,1,1,1)*i_*p1(mu1)*p1(mu2)/(32*pi^2)'
    >>> res['B([],p1,M0,M0)']
    'B([],p1,M0,M0)*Nc*c140515(2,1,1,1)*i_*p1(mu1)*p1(mu2)/(32*pi^2)'
    """
    # store current state of dicts
    ls_dict = self.ls_dict.copy()
    coupl_dict = self.coupl_dict.copy()

    tens_dict = {}
    expr = self.assign_tensors(amp_string, tens_dict=tens_dict,
                               only_scalar_integrals=False)
    expr = self.assign_couplings(expr, original_match=True, coupl_dict={})
    expr = self.assign_lorentz(expr, ls_dict={}, original_match=True)
    expr = parse_UFO(expr)

    # tens base only from expression and not storage !
    tens_base = [Symbol(u) for u in tens_dict]
    split_expr = {}
    expr_dct = Poly(expr, tens_base).as_dict()
    tens_subs = {}
    for tensor in tens_dict:
      tn = tens_dict[tensor]
      tens_subs[tensor] = tn[0] + '(' + ','.join(tn[1]) + ')'

    coupl_dict_inv = {self.coupl_dict[key]: key for key in self.coupl_dict}

    for key in expr_dct:
      rest = expr_dct[key]
      tensor = [tens_base[pos]**power for pos, power in enumerate(key)]
      tensor = cb.fold(lambda x, y: x*y, tensor, 1)
      sympy_subs = {Symbol(u): Symbol(v) for u, v in iteritems(self.ls_dict)}
      sympy_subs.update({Symbol(u): Symbol(v)
                         for u, v in iteritems(tens_subs)})
      sympy_subs.update(coupl_dict_inv)
      sympy_subs.update({I: Symbol('i_')})
      repl_dict = {'**': '^'}
      res = string_subs(str((rest*tensor).subs(sympy_subs)), repl_dict)
      split_expr[string_subs(str(tensor), tens_subs)] = res

    # restore state of dicts
    self.ls_dict = ls_dict
    self.coupl_dict = coupl_dict

    return split_expr

  def form_zeromom_propagator(self, amp):
    """ Replaces propagators with zero momentum by the corresponding mass, i.e.
    den(0,m) -> 1/(0 - m^2).

    :param amp: amplitude in form syntax
    :type  amp: str
    """
    FP = FormPype()
    exprs = ['l amp = (' + amp + ');',
             'id den(0, m?) = -1/(m*m);',
             '.sort']
    FP.eval_exprs(exprs)
    return ''.join(FP.return_expr('amp', stop=True).split())

  def form_add(self, addend1, addend2):
    """ Adds the expression addend1 and addend2 with FORM.

    :param addend1: The first addend
    :type  addend1: String

    :param addend2: The second addend
    :type  addend2: String
    """
    FP = FormPype()
    exprs = ['l a1 = (' + addend1 + ');',
             'l a2 = (' + addend2 + ');',
             'l res = a1 + a2;',
             '.sort']
    FP.eval_exprs(exprs)
    return ''.join(FP.return_expr('res', stop=True).split())

  def simplify_dirac(self, amp):
    """ Applies the FORM procedure `lorentzdirac.frm` to the amplitdue `amp` """
    FP = FormPype()
    exprs = []
    exprs.extend(['l amp = (' + amp + ');'])
    exprs.extend(['#call lorentzdirac'])
    FP.eval_exprs(exprs)
    return ''.join(FP.return_expr('amp', stop=True).split())

  def compute_reduction(self, amp, PV=True, R1=True, R2=True,
                        PVderivative=False, A0_to_B0=False,
                        zero_mass_theta=None,
                        ddim=False):
    """ Reduces the n-dimensional amplitude to strict 4-dimensions taking into
    account rational terms. Tensor integrals are expressed by scalar integrals
    via tensor decomposition.  """
    FP = FormPype()
    exprs = []
    if R2:
      # TODO: (nick 2016-01-14) DANGEROUS. Need to check for all symbols in the
      # expression. If unknown expressions are in amp -> the method returns
      # parts twice due to amp = amp + ampr2
      #exprs.extend(['l ampr2 = (' + amp + ');', '#call r2', '.sort'])
      exprs.extend(['l ampr2 = (' + amp + ');', '#call MStens',
                    'id n = 4 - ep;', '.sort',
                    'id ep = 0;', 'id 1/ep = 0;', 'id M0 = 0;', '.sort'])
    exprs.extend(['l amp = (' + amp + ');'])
    if R1:
      exprs.extend(['#call LC'])
    else:
      exprs.extend(['#call LC2'])
      # Should be
      #exprs.extend(['#call LC', '#call PV_no_R1])

    if not ddim:
      exprs.extend(['id n = 4;', '.sort'])

    if PV:
      exprs.append('#call PV')
    elif PVderivative:
      exprs.append('#call PVderivative')

    # Symmetries not implemented for all scalar integrals -> better to be called
    # after PV (in case PV is done, the result is fully symmetric)
    if not hasattr(self, 'sintegral_ids'):
      self.build_sintegral_identities()
    exprs.extend(self.sintegral_ids)
    exprs.extend(['.sort'])

    if A0_to_B0:
      exprs.extend(['id A0(m?) = m**2*(B0(M0, m, m) + 1);', '.sort'])

    if R2:
      if zero_mass_theta:
        th = 'HT(' + ','.join(zero_mass_theta) + ')*'
        exprs.extend(['l amp = amp + ' + th + 'ampr2;',
                      '.sort'])
      else:
        exprs.extend(['l amp = amp + ampr2;',
                      '.sort'])

    FP.eval_exprs(exprs)
    return ''.join(FP.return_expr('amp', stop=True).split())

  def compute_MS(self, amp):
    """ Computes the divergent part of the amplitude. """
    FP = FormPype()
    exprs = []
    exprs.extend(['l amp = (' + amp + ');'])
    exprs.append('#call MStens')
    exprs.append('#call lorentzdirac')
    # exprs.append('#call lorentzbasis')
    FP.eval_exprs(exprs)
    ret = ''.join(FP.return_expr('amp', stop=True).split())
    return ret

  @classmethod
  def build_sintegral_identities(cls, mom='p1'):
    """ Builds argument simplifications for 2-point scalar integrals in FORM.
    The code makes use of the symmetries when interchanging the masses and
    orders the arguments according to a predefined order.
    The identities used are:

      - B0(p1,m0,m1) = B0(p1,m1,m0)
      - B00(p1,m0,m1) = B00(p1,m1,m0)
      - B1(p,m0,m1) = B0(p,m1,m0) - B1(p,m1,m0)
      - B1(p,m1,m1) = -B0(p,m1,m1)/2
      - B11(p,m0,m1) = B11(p,m1,m0) + 2*B1(p,m1,m0) + B0(p,m1,m0)
    """
    from rept1l.particles import ParticleMass
    ParticleMass.fill_masses()
    massorder = [u for u in sorted(v.name for v in ParticleMass.masses.values())]
    massorder = ['M0'] + massorder
    from rept1l.combinatorics import pair_arguments
    sortm = lambda x: massorder.index(x)
    identities = []
    for m1_m2 in pair_arguments([massorder, massorder]):
      m1_m2_sort = tuple(sorted(m1_m2[:], key=sortm))
      if m1_m2 != m1_m2_sort:
        # B0, B00 identities
        for sym in ['B0', 'B00']:
          sym_id = ('id ' +
                    sym + '(' + mom + ',' + ','.join(m1_m2) + ') = ' +
                    sym + '(' + mom + ',' + ','.join(m1_m2_sort) + ');')
          identities.append(sym_id)
        # B1 identity
        sym_id = ('id B1(' + mom + ',' + ','.join(m1_m2) + ') = -' +
                  'B1(' + mom + ',' + ','.join(m1_m2_sort) + ') -' +
                  'B0(' + mom + ',' + ','.join(m1_m2_sort) + ');')
        identities.append(sym_id)
        # B11 identity
        sym_id = ('id B11(' + mom + ',' + ','.join(m1_m2) + ') = ' +
                  'B11(' + mom + ',' + ','.join(m1_m2_sort) + ') +' +
                  '2*B1(' + mom + ',' + ','.join(m1_m2_sort) + ') +' +
                  'B0(' + mom + ',' + ','.join(m1_m2_sort) + ');')
        identities.append(sym_id)

      else:  # m1_m2 == m1_m2_sort
        # B1 identity
        sym_id = ('id B1(' + mom + ',' + ','.join(m1_m2) + ') = ' +
                  '-B0(' + mom + ',' + ','.join(m1_m2) + ')/2;')

    identities.append('.sort')
    cls.sintegral_ids = identities

  @classmethod
  def apply_sintegral_ids(cls, amp_string):
    if not hasattr(cls, 'sintegral_ids'):
      cls.build_sintegral_identities()

    FP = FormPype()
    exprs = ['l amp = ' + '(' + amp_string + ');'] + cls.sintegral_ids
    FP.eval_exprs(exprs)
    res = FP.return_expr('amp', stop=True)
    return ''.join(res.split())

  def assign_tensors(self, amp_string, tens_dict=None, only_scalar_integrals=True):
    """ Parses string and looks for tensor structures. Tensor structures are
    replaced by the symbols T0, T1, ... and so on.  The information of arguments
    in stored in tens_dict. For multiple amplitudes tens_dict should be passed
    iteratively.  `assign_tensors` updates the dict, guaranteeing unambigous
    assignment.

    :only_scalar_integrals: By default it is assumed that the tensor have been
                       reduced via lorentz invariance. If not set
                       only_scalar_integrals=False.

    >>> FP = FormParse(debug=True)
    >>> FP.assign_tensors('3/4*pi*B0(0,MT,MT)')
    '3/4*pi*T0'
    >>> FP.assign_tensors('3*pi*B1(0,MT,MT)')
    '3*pi*T0'
    >>> FP.assign_tensors('3*pi*(B0(0,MT,MT)+B1( 0 , MT,MT  ))')
    '3*pi*(T0+T1)'
    >>> FP.assign_tensors('3*pi*B0000(0,MT,MT)')
    '3*pi*T0'
    >>> FP.assign_tensors('1/32*C(mu,[],p1, M0 ,  M0 ) + C(mu,[],p1, M0 ,M0)', only_scalar_integrals=False)
    '1/32*T0 + T0'

    """
    if tens_dict is None:
      tens_dict = {}

    tens_repl = {}
    t = tensor(i=len(tens_dict)-1)
    if only_scalar_integrals:
      tensor_patterns = self.tensor_patterns
      tensor_structures = self.tensor_structures
    else:
      tensor_patterns = self.tensor_patterns2
      tensor_structures = self.tensor_structures2

    for i, tens_pattern in enumerate(tensor_patterns):
      tens_matches = tens_pattern.findall(amp_string)
      for m in tens_matches:
        key = list(tensor_structures.keys())[i]
        tn_struc, arg, has_id, func = tensor_structures[key]

        if has_id:
          tens_match = key + m[0] + '(' + m[1] + ')'
        else:
          tens_match = key + '(' + m + ')'

        if arg and not has_id:
          tn = func(tn_struc, m)
        elif arg and has_id:
          tn = func(tn_struc, m)
        else:
          tn = func(m)

        if only_scalar_integrals:
          tn_expr = (tn[0], tn[1][0], tuple(u.replace(" ","") for u in tn[1][1]))
        else:
          tn_expr = [tn[0], [u.replace(" ","") for u in tn[1]]]

        if tn_expr not in tens_dict.values():
          tn_sym = next(t)
          tens_dict[tn_sym] = tn_expr
        else:
          tn_sym = list(tens_dict.keys())[list(tens_dict.values()).index(tn_expr)]

        tens_repl[tens_match] = tn_sym

    return string_subs(amp_string, tens_repl)

  @staticmethod
  def get_contracted_args(amp_string, args):
    """
    >>> t = 'p1(mu1)*p2(mu1)*p3(mu2)+p1(mu3)*p2(mu3)*p1(mu2)'
    >>> FormParse.get_contracted_args(t, ['mu1', 'mu2', 'mu3'])
    ['mu1', 'mu3']
    """
    amps = re.split("(?!\[)[\-\+](?!\])", amp_string)
    cargs = []
    for amp in amps:
      indices = FormParse.digits_pattern.findall(amp)
      for arg in args:
        c = indices.count(arg)
        if c == 2:
          if arg not in cargs:
            cargs.append(arg)
        elif c > 2:
          print('WARNING IN REGULAR')
          print("amp_string:", amp_string)
          #print("amps:", amps)
          print("args:", args)
          raise Exception('Amplitude has multiple contractions with the ' +
                          'same symbol.')
    return cargs

  @staticmethod
  def add_contraction(cargs, struc, contr):
    """ Gathers structures with the same contraction arguments. If a contruction
    is complete a new contraction slot is taken and the contraction dict `contr`
    is updated.

    :param cargs: arguments which are contracted (not verified)
    :type  cargs: tuple

    :param struc: string representation of the structure
    :type  struc: str

    :param contr: dicitonary holding all information of contractions
    :type  contr: dict

    :return contr: updated `contr`

    >>> cargs = ('nu1', 'i4')
    >>> struc = 'ga(i2,i4,[-],nu1)'
    >>> contr = FormParse.add_contraction(cargs, struc, {})
    >>> contr
    {('nu1', 'i4'): {1: ['ga(i2,i4,[-],nu1)']}}
    >>> struc = 'ga(i1,i4,[-],nu1)'
    >>> contr = FormParse.add_contraction(cargs, struc, contr)
    >>> contr
    {('nu1', 'i4'): {1: ['ga(i2,i4,[-],nu1)', 'ga(i1,i4,[-],nu1)']}}
    """
    # contraction argument not registered yet -> add new entry
    if cargs not in contr:
      contr[cargs] = {1: [struc]}
    else:
      ncontr = len(contr[cargs])
      strucs = contr[cargs][ncontr]
      new_contr = False
      carg_contr = 0
      # check if the arguments of cargs are already contracted (every argument
      # appears twice)
      for carg in cargs:
        count = 0
        for struc_tmp in strucs:
          if carg in struc_tmp:
            count += 1
        if count == 2:
          carg_contr += 1

      # if all args are contracted `struc` is a new structure
      new_contr = carg_contr == len(cargs)
      if new_contr:
        contr[cargs][ncontr+1] = [struc]
      else:
        contr[cargs][ncontr].append(struc)
    return contr

  def assign_lorentz(self, amp_string, ls_dict=None, original_match=False):
    """ Parses string and looks for lorentz structures. Lorentz structures are
    replaced by the symbols L0, L1, ... and so on.  The information of arguments
    in stored in ls_dict. For multiple amplitudes ls_dict should be passed
    iteratively. It is possible to reset the internal ls_dict via ls_dict={}
    when calling assign_lorentz.  `assign_lorentz` updates the dict,
    guaranteeing unambigous assignment.

    :original_match: keeps the original expression of lorentz expressions in
                     FORM instead of expressing them in terms of lorentz base
                     structures

    >>> FP = FormParse(debug=True); FP.ls_dict = {}
    >>> sympify(FP.assign_lorentz('d_(mu1,mu2)*pi'))
    pi*L0
    >>> FP.ls_dict
    {'L0': ([Metric], [[1, 2]])}

    >>> ls_str1 = FP.assign_lorentz('p1(mu2)*p2(mu1)', ls_dict={})
    >>> ls_str2 = FP.assign_lorentz('p1(mu2)*p3(mu3)')
    >>> FP.ls_dict
    {'L2': ([P], [[3, 3]]), 'L0': ([P], [[2, 1]]), 'L1': ([P], [[1, 2]])}
    >>> ls_str2
    'L0*L2'

    >>> ls_str = FP.assign_lorentz('ga(i1,i2,nu1)*ga(i3,i4,[-],nu1)', {})
    >>> FP.ls_dict['L0']
    ([GammaP, Gamma], [[-1, 3, 4], [-1, 1, 2]])

    >>> ls_str = FP.assign_lorentz('ga(i1,i2,mu1)*ga(i3,i4,[-],mu1)', {})
    >>> FP.ls_dict['L0']
    ([GammaP, Gamma], [[-1, 3, 4], [-1, 1, 2]])

    >>> ls_str = FP.assign_lorentz('e_(mu1,nu2,p1,p2)*d_(mu1,mu3)', {})
    >>> FP.ls_dict['L0']
    ([P, P, Metric, Epsilon], [[-1, 1], [-2, 2], [-3, 3], [-3, 2, -1, -2]])

    >>> ls_str = FP.assign_lorentz('ga(i1,i2,mu1,mu2,p1)', {})
    >>> FP.ls_dict['L0']
    ([P, Gamma, Gamma, Gamma], [[-3, 1], [1, 1, -1], [2, -1, -2], [-3, -2, 2]])

    >>> ls1 = 'ga(i2,i4,[-],nu1)*ga(i3,i1,[-],nu1)'
    >>> ls2 = '+ga(i2,i4,[-],nu1)*ga(i3,i1,[+],nu1)'
    >>> ls = ls1 + ls2
    >>> ls_str = FP.assign_lorentz(ls, {})
    >>> ls_str
    'L0 + L1'
    >>> FP.ls_dict['L0']
    ([GammaP, GammaP], [[-1, 2, 4], [-1, 3, 1]])
    >>> FP.ls_dict['L1']
    ([GammaP, GammaM], [[-1, 2, 4], [-1, 3, 1]])

    >>> ls = 'ga(i3,i4,ro61,ro2)'
    >>> ls_str = FP.assign_lorentz(ls, {})
    >>> FP.ls_dict[ls_str]
    ([Gamma, Gamma], [[61, 3, -1], [2, -1, 4]])

    >>> ls = 'ga(i3,i4,[+],ro2,ro3)*ga(i2,i1,[-],ro3,ro2)'
    >>> ls_str = FP.assign_lorentz(ls, {})
    >>> FP.ls_dict[ls_str]
    ([GammaP, GammaP, GammaM, GammaM], [[-1, -2, 4], [-1, 2, -3], [-4, -3, 1], [-4, 3, -2]])

    >>> ls = 'si(i3,i4,ro2,ro3)*si(i1,i2,ro2,ro3)'
    >>> ls_str = FP.assign_lorentz(ls, {})
    >>> FP.ls_dict[ls_str]
    ([Sigma, Sigma], [[-1, -2, 1, 2], [-1, -2, 3, 4]])

    >>> ls = 'ga(i3,i4,[+],ro2)'
    >>> ls_str = FP.assign_lorentz(ls, {}, original_match=True)
    >>> FP.ls_dict[ls_str]
    'ga(i3,i4,[+],ro2)'


    >>> ls = '+1/4*ga(i2,i1,nu6,nu5)*ga(i4,i3,nu5,nu6)-1/4*ga(i2,i1,nu6,nu5)*ga(i4,i3,nu6,nu5)'
    >>> ls_str = FP.assign_lorentz(ls, {})
    >>> FP.ls_dict['L0']
    ([Gamma, Gamma, Gamma, Gamma], [[-1, -2, 1], [-1, -3, 3], [-4, 2, -2], [-4, 4, -3]])
    >>> FP.ls_dict['L1']
    ([Gamma, Gamma, Gamma, Gamma], [[-1, -2, 1], [-3, -4, 3], [-3, 2, -2], [-1, 4, -4]])
    """
    if ls_dict is None:
      ls_dict = self.ls_dict

    amp_string_sep = self.sep_lorentz(amp_string)
    if len(amp_string_sep) > 1:
      ret = 0
      for u in amp_string_sep:
        r = self.assign_lorentz(u, ls_dict=ls_dict,
                 original_match=original_match)
        ret += sympify(r)
      return str(ret)
    else:
      amp_string = amp_string_sep[0]

    cargs_assigned = {}
    contr_args = {}
    ls_repl = {}
    ls_matched = {}
    ls_base = [u.symbol for u in cb.lorentz_base.bases]
    for ls_key in self.lorentz_patterns:
      ls_pattern = self.lorentz_patterns[ls_key]
      ls_matches = ls_pattern.findall(amp_string)
      for m in ls_matches:
        ls_struc, arg, has_id, func = self.lorentz_structures[ls_key]

        if len(cargs_assigned) > 0:
          coffset = min(cargs_assigned.values())
        else:
          coffset = 0
        if arg and not has_id:
          ls = func(ls_struc, m[1], coffset=coffset)
        elif arg and has_id:
          ls = func(ls_struc, [m[1], m[2]], coffset=coffset-1)
        else:
          ls = func(m[0])

        arg_types = []
        for arg in ls[1]:
          arg_types.append([u[1] for u in arg if type(u) is tuple])

        arg_types = [u for u in cb.flatten(arg_types)]

        if original_match:
          cargs = ()
        else:
          cargs = self.get_contracted_args(amp_string, arg_types)
          cargs = tuple(sorted(cargs))

        args = []
        # add negative args to assigned contraction avoiding reassigning
        cargs_assigned.update({u: u for u in cb.flatten(ls[1])
                               if type(u) is int and u < 0})
        for arg in ls[1]:
          new_arg = []
          for u in arg:
            if type(u) is tuple:
              if u[1] in cargs_assigned:
                new_arg.append(cargs_assigned[u[1]])
              elif u[1] in cargs:
                if -u[0] not in cargs_assigned.values():
                  new_arg.append(-u[0])
                  cargs_assigned[u[1]] = -u[0]
                else:
                  i = -1
                  while i in cargs_assigned.values():
                    i -= 1
                  new_arg.append(i)
                  cargs_assigned[u[1]] = i
              else:
                new_arg.append(u[0])
            else:
              new_arg.append(u)
          args.append(new_arg)

        ls = (ls[0], args)
        if len(cargs) > 0:
          contr_args = self.add_contraction(cargs, m[0], contr_args)

        if ls not in ls_dict.values():
          ls_matched[m[0]] = ls
        else:
          index = list(ls_dict.values()).index(ls)
          ls_repl[m[0]] = list(ls_dict.keys())[index]

    ls_del = set()
    # Build up contractions
    for contr in contr_args:
      for ncontr in contr_args[contr]:
        contr_strucs = contr_args[contr][ncontr]
        if len(contr_args[contr][ncontr]) == 1:
          # no need to merge when the contraction happens within one structure
          continue

        # found structures which are contracted with each other
        # build string representative for the structures
        if len(contr_strucs) != 2:
          raise Exception('Invalid contraction structures.')
        struc_combined = '*'.join(contr_strucs)

        # group the lorentz keys and args together
        ls_keys = [ls_matched[u][0] for u in contr_strucs]
        args = [ls_matched[u][1] for u in contr_strucs]

        # the new key is simply the product of the lorentz symbols
        new_ls_key = []
        for k in ls_keys:
          new_ls_key += k

        # For the argument is must we must make sure to follow the conventions
        # for the order of arguments. get_representative takes care of that.
        new_args = []
        for k in args:
          new_args += k

        new_args = cb.get_representative(new_ls_key, new_args)

        # register the new structure and remove the old ones
        new_ls_key = cb.order_strucs(new_ls_key)
        ls_matched[struc_combined] = (new_ls_key, new_args)
        for ls in contr_strucs:
          # the contraction might already been deleted
          ls_del.add(ls)

    # remove single structures contained in composed ones
    for ls in ls_del:
      if ls in ls_matched:
        del ls_matched[ls]

    l = lorentz(i=len(ls_dict)-1)
    for match in ls_matched:
      ls = ls_matched[match]
      if match not in ls_repl:
        sym = None
        for ls_sym in ls_dict:
          if ls_dict[ls_sym] == ls:
            sym = ls_sym
        if sym is None:
          sym = next(l)
          if not original_match:
            ls_dict[sym] = ls
          else:
            ls_dict[sym] = match
        ls_repl[match] = sym

    self.ls_dict = ls_dict
    ret = string_subs(amp_string, ls_repl)
    return ret

  def sep_lorentz(self, amp_string):
    """ Separates a string of lorentz structures into addends """
    l = lorentz(i=0)
    ls_repl = {}
    ls_repl_inv = {}
    for ls_key in self.lorentz_patterns:
      ls_pattern = self.lorentz_patterns[ls_key]
      ls_matches = ls_pattern.findall(amp_string)
      for match in ls_matches:
        ls = match[0]
        if ls not in ls_repl:
          sym = next(l)
          ls_repl[ls] = sym
          ls_repl_inv[Symbol(sym)] = Symbol(ls)

    ret = sympify(string_subs(amp_string, ls_repl)).expand()
    if ret.is_Add:
      ret = [str(u.xreplace(ls_repl_inv)) for u in ret.args]
    else:
      ret = [str(ret.xreplace(ls_repl_inv))]
    return ret

if __name__ == "__main__":
  import doctest
  doctest.testmod()

  # expr = '-C2**2*C204*Nc*ga(i3,i2,[-],ro2,ro260)*ga(i4,i1,[-],ro2,ro260)*sq2**2/(32*pi**2)'
  # FP = FormParse()
  # FP.assign_lorentz(expr)
