#==============================================================================#
#                              parseamplitude.py                               #
#==============================================================================#

#============#
#  Includes  #
#============#

import re
from sympy import Symbol
from .regular import FormParse
from rept1l.formutils import FormPype
from rept1l.logging_setup import log
from rept1l.pyfort import ProgressBar

#==============================================================================#
#                                ParseAmplitude                                #
#==============================================================================#

class ParseAmplitude(FormParse):
  """ Template for parsing FORM ampltides. Standard processing routines are
  given. By default, the amplitude is reduced to strict 4 dimension.

  For bare-loop the following options are provided

  :param compute_r2: Derives the rational terms of type R2
  :type  compute_r2: bool

  :param PV: Perform Passarino-Veltman reduction.
  :type  PV: bool

  :param A0_to_B0: Substitute all A0 in favor of B0
  :type  A0_to_B0: bool


  :param zeromom_propagator: Substitute all propagator with zero momentum by the
                             mass expression.
  :type  zeromom_propagator: bool
  """
  optional = {'compute_r2': True, 'PassarinoVeltman': True,
              'compute_r1': True, 'A0_to_B0': False,
              'zeromom_propagator': False}

  def __init__(self, **kwargs):

    super(ParseAmplitude, self).__init__()

    from rept1l.vertices_rc import Vertex
    Vertex.load()
    # from pyrecola import get_driver_timestamp_rcl, get_modelname_rcl
    # if Vertex.timestamp != get_driver_timestamp_rcl():
    #
    #   log('Timestamp of vertex class and model do not match!', 'error')
    #   log('Timestamp Vertex: ' + Vertex.timestamp, 'debug')
    #   log('Timestamp model: ' + get_driver_timestamp_rcl(), 'debug')
    #   from rept1l import Model as Model
    #   log('Rept1l model path: ' + Model.mpath, 'debug')
    #   if hasattr(Model.model, 'modelname'):
    #     modelname = Model.model.modelname
    #   else:
    #     modelname = 'NOTSET'
    #   log('Recola model name: ' + get_modelname_rcl(), 'debug')
    #   log('Rept1l model name: ' + modelname, 'debug')
    #   raise Exception('Timestamp of Vertex and Model do not match!')

    self.treevertices = Vertex.treevertices
    self.ct_vertices = Vertex.ctvertices
    self.r2_vertices = Vertex.r2vertices
    self.amplitudes = {}

    self.optional = {}
    for arg in ParseAmplitude.optional:
      if arg in kwargs:
        self.optional[arg] = kwargs[arg]
      else:
        self.optional[arg] = ParseAmplitude.optional[arg]

  def process_loopamp(self, amp):
    R2 = self.optional['compute_r2']
    PV = self.optional['PassarinoVeltman']
    R1 = self.optional['compute_r1']
    A0_to_B0 = self.optional['A0_to_B0']
    amp = self.compute_reduction(amp, PV=PV, R2=R2, R1=R1,
                                 A0_to_B0=A0_to_B0)  # loop (+ R1) + R2
    if self.optional['zeromom_propagator']:
      amp = self.form_zeromom_propagator(amp)
    return amp

  def process_treeamp(self, amp):
    """ Processing the tree-level FORM amplitudes. """
    return amp

  def process_ctamp(self, amp):
    """ Processing the counterterm FORM amplitudes. """
    return amp

  def process_r2amp(self, amp):
    """ Processing the rational FORM amplitudes. """
    return amp

  def process_sumamp(self, amp):
    """ Processing the bare-loop FORM amplitudes. """
    from rept1l.regularize import Regularize
    amp = self.assign_lorentz(amp)
    amp = self.assign_tensors(amp, Regularize.tens_dict)
    amp = self.parse_UFO(amp)
    return amp

  def parse(self, filein):
    """ Core routine which reads the formamplitude and applies specific
    processing routines to the individual parts of the amplitude. The summed
    results are ordered according to their colorflow and order in fundamental
    couplings."""
    log('Parsing FORM file: ' + str(filein) + '.', 'info')
    with open(filein, 'r') as fin:
      text = fin.read()
      matches = re.findall(self.global_pattern, text)
      # sum up amplitudes with the same colorflow
      amps_reg = []
      exprs = []
      PG = ProgressBar(len(matches))
      for match_id, match in enumerate(matches):
        amp_kind, amp_id = self.register_amplitude(match[0], amps_reg, exprs)

        if match[1].strip() != '0':
          amp = ''.join(match[1].split())
          amp = self.assign_couplings(amp, {1: self.treevertices,
                                            2: self.ct_vertices,
                                            3: self.r2_vertices})
          if amp_kind == 'AmpLO':
            amp = self.process_treeamp(amp)
          elif amp_kind == 'AmpCT':
            amp = self.process_ctamp(amp)
          elif amp_kind == 'AmpR2':
            amp = self.process_r2amp(amp)
          else:
            amp = self.process_loopamp(amp)

          exprs.extend(['l ' + amp_id + ' = ' + amp_id + ' + (' + amp + ');',
                        '.sort'])
        PG.advanceAndPlot()

      log('Building sum of sub-amplitudes.', 'info')
      FP = FormPype()
      FP.eval_exprs(exprs)
      PG = ProgressBar(len(amps_reg))
      for amp_id in amps_reg:
        cs = amp_id[0]
        amp_order = amp_id[1]
        order = ','.join([u[1] for u in amp_order])
        amp_id = '[amp(' + cs + ',' + order + ')]'
        res = ''.join(FP.return_expr(amp_id).split())

        if res != '0':
          res = self.process_sumamp(res)

          if (int(cs), order) not in self.amplitudes:
            self.amplitudes[(int(cs), order)] = 0
          self.amplitudes[(int(cs), order)] += res
        PG.advanceAndPlot()

      coupl_dict_inv = {Symbol(self.coupl_dict[key]): key
                        for key in self.coupl_dict}
      log('Substituting for coupling expressions.', 'info')
      for amp_key in self.amplitudes:
        crepl = {u: coupl_dict_inv[u] for u in self.amplitudes[amp_key].
                 free_symbols if u in coupl_dict_inv}
        self.amplitudes[amp_key] = self.amplitudes[amp_key].xreplace(crepl)
