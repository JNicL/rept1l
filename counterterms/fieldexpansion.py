###############################################################################
#                              fieldexpansion.py                              #
###############################################################################

##############
#  Includes  #
##############

from sympy import sympify
try:
  from types import ListType
  list_type = ListType
except ImportError:
  list_type = list


from rept1l.logging_setup import log
from rept1l.combinatorics import get_lorentz_key_components, order_strucs
from rept1l.combinatorics import lorentz_base_dict, merge_structures

#############
#  Globals  #
#############

allow_nonprimitve_appylct = False

#############
#  Methods  #
#############


def apply_ct_rule(ls_struc, insert_ls_key, insert_arg, index_combination={},
                  form_debug=False):
  """ Takes a lorentz base structure and multiplies/contracts it with a new
  structure.

  :param ls_struc: lorentz base structure.
  :type  ls_struc: list

  :param insert_ls_key: lorentz base key
  :type  insert_ls_key: Symbol

  >>> from sympy import Symbol
  >>> ls_struc = [3*Symbol('Gamma'), (0, 2)]
  >>> insert_ls_key = Symbol('ProjP')
  >>> insert_arg = [[2, 3]]
  >>> apply_ct_rule(ls_struc, insert_ls_key, insert_arg, {3: 2}, True)
  [(3*GammaP, (0, 2))]

  >>> ls_struc = [2*Symbol('Gamma')*Symbol('P'), (0, 2)]
  >>> insert_arg = [[1, 2]]
  >>> apply_ct_rule(ls_struc, insert_ls_key, insert_arg, {2: 1}, True)
  [(2*GammaP*P, (0, 2))]
  >>> insert_ls_key = Symbol('ProjM')
  >>> apply_ct_rule(ls_struc, insert_ls_key, insert_arg, {2: 1}, True)
  [(2*GammaM*P, (0, 2))]

  >>> lorentz = 'Sigma(-1,-2,1,2)*Sigma(-2,-1,3,4)'; perm = range(4)
  >>> from rept1l.baseutils import CurrentBase
  >>> ls_struc = CurrentBase(lorentz).compute(perm)[0]
  >>> insert_arg = [[1, 2]]
  >>> insert_ls_key = Symbol('ProjM')
  >>> apply_ct_rule(ls_struc, insert_ls_key, insert_arg, {2: 1}, True)
  [(4*Epsilon*Sigma**2, (0, 1)), (4*Sigma**2, (0, 1))]
  """

  ls_key, particles = get_lorentz_key_components(ls_struc[0])
  ls_key = order_strucs(ls_key)
  arg = lorentz_base_dict[ls_struc[0]][ls_struc[1][0]]
  prefactor = ls_struc[1][1]
  if not isinstance(insert_ls_key, list_type):
    insert_ls_key, _ = get_lorentz_key_components(insert_ls_key)

  new_ls_key, new_arg = merge_structures(ls_key, arg, insert_ls_key,
                                         insert_arg, index_combination)

  # construct the form expression and simplify by calling lorentzdirac module
  from rept1l.baseutils.base_utils import generate_form_expression
  frm_str = generate_form_expression(new_ls_key, new_arg)
  from rept1l.formutils import FormPype
  with FormPype(debug=form_debug) as FP:
    exprs = ['l expr = ' + frm_str + ';',
             '#call lorentzdirac']
    # exprs = ['l expr = ' + frm_str + ';',
    #          '#call lorentzdirac',
    #          '#call lorentzbasis']

    FP.eval_exprs(exprs)

    res = ''.join(FP.return_expr('expr').split())
  if res != '0':
    # transform the result back to a lorentz base structure
    from rept1l.parsing import FormParse
    FP = FormParse()
    res = FP.assign_lorentz(res)
    res = FP.parse_UFO(res)
    from rept1l.baseutils import CurrentBase
    res = CurrentBase(res).compute2(FP.ls_dict, particles).keys()

    if len(res) > 1 and allow_nonprimitve_appylct:
      errmsg = ('Found structure which after applying `apply_ct_rule` ' +
                'is not a primitive structure which is not tested. ' +
                'See debug.')
      log(errmsg, 'warning')
      log('ls_struc: ' + str(ls_struc), 'debug')
      log('insert_ls_key: ' + str(insert_ls_key), 'debug')
      log('insert_arg: ' + str(insert_arg), 'debug')
      log('index_combination: ' + str(index_combination), 'debug')
      log('Form expression: ' + str(frm_str), 'debug')
      log('Resulting structures: ' + str(res), 'debug')
    res = [(u[0], (u[1][0], u[1][1] * prefactor)) for u in res]
    return res
  else:
    return [None]


def derive_wavefunction_ct(ls_strucs, particles,
                           offdiag_once=False, form_debug=False):
  """ Derives the counterterm vertex structure obtained when performing the
  wavefunction renormalization.

  :param ls_strucs: Lorentz base structures.
  :type  ls_strucs: list

  :param particles: List of UFO particles. Only particles with the counterterm
                    attribute are processed.
  :type  particles: list

  :param offdiag_once: The wavefunction renormalization is performed
                       only once for one particle if the same particle occurs
                       multiple times in the vertex.
  :type  offdiag_once: bool

  Usage:

  >>> from sympy import Symbol
  >>> ls_struc = [[2*Symbol('P')*Symbol('GammaM'), (0, 1)],
  ...             [2*Symbol('P')*Symbol('GammaP'), (0, 1)]]
  >>> Particle = type('Particle', (object,), {})
  >>> b__anti__ = Particle(); b = Particle()
  >>> wave_anti = {b__anti__: {'dZbA/2': (Symbol('ProjP'),
  ...                          ['Particle', 'Contraction'])}}
  >>> wave = {b: {'dZb/2': (Symbol('ProjM'), ['Contraction', 'Particle'])}}
  >>> b__anti__.counterterm = wave_anti
  >>> b.counterterm = wave
  >>> particles = [b__anti__, b]
  >>> derive_wavefunction_ct(ls_struc, particles,
  ... form_debug=True)[(b__anti__, b)]
  {((2*GammaM*P, (0, 1)),): dZb/2 + dZbA/2}

  The option `offdiag_once` is intended to be used in combination with
  computing only one permutation of lorentz structures. Consider the particle
  combination a, b, b, in total there are 3 different permutations:

    - (a, b, b), (b, a, b), (b, b, a).

  When performing offdiagonal wavefunction renormalization from a -> b the
  result should be:

    - 3x (b, b, b)

  Similar, when performing b -> a one obtains:

    - 2x(a, a, b), 2x(a, b, a), 2x(b, a, a)

  The idea is to compute only one permutation, e.g.
  (a, b, b) and perform the wavefunction renormalization with adequate factors
  called `multiplic` in the code below.

  For the transition from a -> b the factor is given by the number of b
  particles in the new vertex.

  >>> ls_struc = [[2*Symbol('P'), (0, 1)]]
  >>> Particle = type('Particle', (object,), {})
  >>> a = Particle(); b = Particle()
  >>> waveb = {b: {'dZbb/2': None}, a: {'dZba/2': None}}
  >>> wavea = {a: {'dZaa/2': None}, b: {'dZab/2': None}}
  >>> b.counterterm = waveb; a.counterterm = wavea
  >>> particles = [a, b, b]
  >>> wfct = derive_wavefunction_ct(ls_struc, particles, offdiag_once=True,
  ...                               form_debug=True)
  >>> wfct[(a, a, b)]
  {((2*P, (0, 1)),): dZba}
  >>> wfct[(b, b, b)]
  {((2*P, (0, 1)),): 3*dZab/2}
  """

  ls = dict()
  if offdiag_once:
    offdiagonal_mix = []

  # iterate over all particles which take part in the vertex and which are
  # wavefunction renormalized
  for particle_id, particle in enumerate(particles):
    if hasattr(particle, 'counterterm'):
      for particle_mixing in particle.counterterm:

        offdiag = particle != particle_mixing
        # if particle already mixed to particle_mixing discard the vertex
        if(offdiag_once and offdiag):
          if (particle, particle_mixing) in offdiagonal_mix:
            continue
          else:
            offdiagonal_mix.append((particle, particle_mixing))
            multiplic = particles.count(particle_mixing) + 1
        else:
          multiplic = 1

        for wf_renorm in particle.counterterm[particle_mixing]:
          ls_strucs_new = []
          try:
            insert_ls_key, arg = (particle.counterterm[particle_mixing]
                                  [wf_renorm])
          except:
            arg = None

          # construct new lorentz base structure
          # arg == None is scalar case -> no non-trivial structure
          if arg is not None:
            for ls_struc in ls_strucs:
              contr_pos = arg.index('Contraction')
              part_pos = arg.index('Particle')
              insert_arg = arg[:]
              insert_arg[contr_pos] = particle_id + 2
              insert_arg[part_pos] = particle_id + 1
              insert_arg = [insert_arg]
              contr_dict = {particle_id + 1: particle_id + 2}

              new_strucs = apply_ct_rule(ls_struc, insert_ls_key, insert_arg,
                                         contr_dict, form_debug=form_debug)
              ls_strucs_new.extend(new_strucs)
          else:
              ls_strucs_new = ls_strucs

          # construct the particle key and replace the current particle with
          # particle_mixing
          p_key = particles[:]
          p_key[particle_id] = particle_mixing

          # make result hashable
          t_p_key = tuple(p_key)
          t_ls_strucs_new = tuple([(u[0], u[1])
                                  for u in ls_strucs_new if u is not None])

          # construct ct coupling
          if t_p_key not in ls:
            ls[t_p_key] = {t_ls_strucs_new: sympify(wf_renorm) * multiplic}
          else:
            tmp = ls[t_p_key]
            if t_ls_strucs_new not in tmp:
              tmp[t_ls_strucs_new] = 0
            tmp[t_ls_strucs_new] += (sympify(wf_renorm) * multiplic)
            ls[t_p_key] = tmp
  return ls


if __name__ == "__main__":
  import doctest
  doctest.testmod()
