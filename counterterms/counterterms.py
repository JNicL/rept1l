#==============================================================================#
#                               counterterms.py                                #
#==============================================================================#

#============#
#  Includes  #
#============#

import os
import sys
import multiprocessing
from six import with_metaclass
from sympy import Symbol, Poly, together, simplify
from rept1l.helper_lib import StorageProperty, parse_UFO, file_exists
from rept1l.helper_lib import hashfile, query_yes_no
from rept1l.logging_setup import log
from rept1l.combinatorics import flatten
from rept1l.counterterms.ctexpansion import expand_coupling, expand_expression
from rept1l.counterterms.ctexpansion import get_first_order
from rept1l.counterterms.particle_counterterms import get_param_ct
from six import iteritems
import rept1l.Model as Model
import rept1l_config

#############
#  Globals  #
#############

model = Model.model
if hasattr(model, 'modelname'):
  modelname = model.modelname
else:
  modelname = None


#===========#
#  Methods  #
#===========#

def new_coupling(expr, FRC, base=[], flag='Tree', simp=together):
  """ Defines a new coupling from the expression `expr`. If base elements are
  provided only the coefficients of the base elements are defined as new
  couplings. """
  if len(base) > 0:
    pexpr = Poly(expr, base).as_dict()
    for k, val in iteritems(pexpr):
      c = FRC(simp(val), flag, subs_cvalues=False,
              check_coupling=False).return_coupling()
      pexpr[k] = c
    return Poly(pexpr, base).as_expr()
  else:
    return FRC(simp(val), flag, subs_cvalues=False,
               check_coupling=False).return_coupling()


#==============================================================================#
#                              class Counterterms                              #
#==============================================================================#

class Counterterms(with_metaclass(StorageProperty)):

  # StorageMeta attributes. Those attributes are stored when `store` is called.
  storage = ['ctdict', 'ctdict_int', 'cts', 'internal_param_ext', 'renos_order',
             'coupling_hashes', 'derived_ct_parameters', 'derived_ct_values']
  # StorageMeta storage path
  #path = os.path.dirname(os.path.realpath(__file__)) + '/ModelCounterterms'
  mpath, _ = model.get_modelpath()
  path = mpath + '/ModelCounterterms'
  # StorageMeta filename
  name = 'counterterm_data.txt'

  @classmethod
  def get_wavefunction_ct(self, particles):
    """ Deprecated?  !!! Todo !!! Compare with get_wf_ct!

    Returns wavefunctions as Sympy expressions for a set of
    particles.

    The wavefunction is a particle property assigned by the user.

    :particles: List of UFO particle instances

    >>> Particle = type('Particle', (object,), {})
    >>> t = Particle();
    >>> b = Particle();
    >>> waveCTT = {t: {'I*dZtt/2': lambda x: x}, b: {'I*dZtb/2': lambda x: x}}
    >>> waveCTB = {b: {'I*dZbb/2': lambda x: x}, t: {'I*dZbt/2': lambda x: x}}
    >>> t.counterterm = waveCTT
    >>> b.counterterm = waveCTB
    >>> sorted([u.name for u in list(Counterterms.get_wavefunction_ct([t, b]))])
    ['dZbb', 'dZbt', 'dZtb', 'dZtt']
    """
    wavefuncrenorms = [[list(particle.counterterm[mixparticle])
                        for mixparticle in particle.counterterm]
                       for particle in particles
                       if hasattr(particle, 'counterterm')]
    wavefuncrenorms = [parse_UFO(u) for u in flatten(wavefuncrenorms)]
    # remove possible factors
    wavefuncrenorms = [v for v in flatten([[u for u in wf.atoms()
                                            if type(u) is Symbol]
                                           for wf in wavefuncrenorms])]
    return set(wavefuncrenorms)

  @classmethod
  def get_all_ct(cls, as_string=False, derived_ct=True):
    if not hasattr(cls, 'all_ct'):
      # external counterterm parameters
      c_ct = get_param_ct(model.param.all_parameters).values()

      # field renormalization constants
      w_ct = Counterterms.get_wavefunction_ct(model.model_objects.all_particles)

      # construct set of all counterterm parameters
      all_ct = w_ct.union(set(c_ct))
      if derived_ct:
        # derived counterterm parameters
        ct_derv = Counterterms.get_derived_ct()
        all_ct = all_ct.union(ct_derv)

      all_ct_str = set(u.name for u in all_ct)
      cls.all_ct = {'symbol': all_ct, 'string': all_ct_str}
    if as_string:
      return cls.all_ct['string']
    else:
      return cls.all_ct['symbol']

  @classmethod
  def get_derived_ct(cls, as_string=False):
    ct_derv = Counterterms.derived_ct_parameters.values()
    if as_string:
      return set(ct_derv)
    else:
      return set(Symbol(u) for u in set(ct_derv))

  @staticmethod
  def get_first_order(expr, ctparams):
    """ Returns the the part of the expression of first order in ctparams. """
    ret = {key: value for key, value in iteritems(Poly(expr, *ctparams).as_dict()) if sum(key) == 1}
    return Poly(ret, *ctparams).as_expr()

  @classmethod
  def expand_couplings(self, FRC, selection=[], parameters_changed=False,
                       multi=True, force=False, signal_handler=False,
                       pure_qcd=False, **kwargs):
    """ Performs the expansion expand_coupling for all couplings of the UFO
    model. The expansion is stored as the dictionary :attr:`cts`. """

    from rept1l.renormalize import Renormalize
    if not hasattr(self, '_coupling_hashes'):
      self._coupling_hashes = {}

    if not hasattr(self, 'plot_progress'):
      self.plot_progress = True

    from hashlib import sha256
    if signal_handler:
      def signal_handler(signal, frame):
        Counterterms.store()
        print("Counterterms.storage:", Counterterms.storage)
        print("Counterterms.coupling_hashes:", Counterterms.coupling_hashes)
        print('Stored data. Exiting!')
        sys.exit(0)
      # Allowing the user to stop the expansion. The intermediate results are
      # stored.
      import signal
      signal.signal(signal.SIGINT, signal_handler)

    if(not hasattr(self, '_ctdict') or
       not hasattr(self, '_expansion_parameters')):
      #raise Exception('Cached parameters _ctdict or ' +
                      #'_expansion_parameters not found')
      storage, name = self.set_cache_storage()
      hashes = self.compute_ct_hashes()

      try:
        # make sure that cached files are loaded even if Counterterms were
        # loaded before
        loaded = self.loaded
        self.loaded = False
        self.load()
        # set loaded flag to previous state
        self.loaded = loaded
      # cache file does not exist -> initialize
      except IOError:
        log('could not load cached files.', 'warning')
        for attr in self.storage:
          print("attr:", attr)
          setattr(self, attr, {})
        self.store()

      # restore storage
      self.storage = storage
      self.name = name

    if not self.loaded:
      # make sure that previously expanded couplings are loaded
      try:
        self.load(load_all=['cts', 'coupling_hashes'])
        # set loaded flag to zero, otherwise the actual storage is not getting
        # loaded
      # cache file does not exist -> initialize
      except IOError:
        for attr in ['cts', 'coupling_hashes']:
          setattr(self, attr, {})
        self.store()

    # check if the counterterm rules are empty. If so, give the user the
    # possiblity to derive them adhoc.
    if len(self._ctdict) == 0 or len(self._expansion_parameters) == 0:
      if not force:
        answer = query_yes_no('Counterterm rules are empty. Do you want ' +
                              'to derive the counterterm rules?')
      else:
        answer = True

      if answer:
        self.derive_ct_expansion(FRC, expand_couplings=False, force=force,
                                 multi=multi, plot_progress=self.plot_progress)
      else:
        raise Exception('Counterterm rules are empty.')

    if self.plot_progress and not multi:
      from rept1l.pyfort import ProgressBar
      PG = ProgressBar(len(model.model_objects.all_couplings))
      log('Expanding couplings.', 'info')

    if multi:
      rs = multiprocessing.Queue()
      jobs = []

    # if no couplings are selected, expand all couplings in the theory
    if selection == []:
      selection = model.model_objects.all_couplings

    if pure_qcd:
      new_selection = []
      for cc in selection:
        qcdpower = FRC.coupling_orders[cc.name]['QCD']
        restpower = sum(FRC.coupling_orders[cc.name].values()) - qcdpower
        if qcdpower >= 0 and restpower == 0:
          new_selection.append(cc)
        else:
          from sympy import sympify
          self._cts[cc.name] = sympify('0')
          chash = sha256(cc.value.encode()).digest()
          self.coupling_hashes[cc.name] = chash
      selection = new_selection

    # collect expansion paramters: independent + derived ones
    expp = self._expansion_parameters + list(self._derived_ct_parameters.values())
    ct_params = tuple([Symbol(u) for u in expp])
    for i, cc in enumerate(selection):
      # compute coupling hash
      chash = sha256(cc.value.encode()).digest()
      # check if coupling needs to be reexpanded
      if self.plot_progress and not multi:
        PG.advanceAndPlot(status=str(cc))
      if((not parameters_changed) and (cc.name in self.coupling_hashes) and
         (chash == self.coupling_hashes[cc.name])):
        continue

      cc_sympy = parse_UFO(cc.value)
      if hasattr(cc, 'ct_shift'):
        ct_shift = parse_UFO(cc.ct_shift.value)
      else:
        ct_shift = 0

      # choose expansion rules according to config
      if(hasattr(rept1l_config, 'derived_ct_expansion') and
         rept1l_config.derived_ct_expansion):
        expansion_dict = self._ctdict_int
      else:
        expansion_dict = self._ctdict

      if multi:
        func = (lambda *args:
                (FRC.complex_mass_couplings(expand_coupling(*args))))

        j = ExpandCouplingJob((cc.name, chash), func, rs, cc_sympy,
                              expansion_dict, expp, ct_shift)
        jobs.append(j)
      else:
        cc_sympy_ct = expand_coupling(cc_sympy, expansion_dict, expp,
                                      ct_shift)

        cc_simplified = new_coupling(cc_sympy_ct, FRC, ct_params)
        cc_simplified = FRC.complex_mass_couplings(cc_simplified)
        self._cts[cc.name] = cc_simplified
        self.coupling_hashes[cc.name] = chash

    if multi:
      max_jobs = 2*multiprocessing.cpu_count()
      n_jobs = len(jobs)
      if len(jobs) > 0 and self.plot_progress:
        from rept1l.pyfort import ProgressBar
        PG = ProgressBar(n_jobs)
      n_active = 0
      finished_jobs = {}
      while n_jobs > 0:
        while n_active < max_jobs and n_active < n_jobs:
          new_job = jobs.pop(0)
          new_job.start()
          finished_jobs[new_job.id] = new_job
          n_active += 1
        res = rs.get()
        cc_name, chash = res[0]
        worker = finished_jobs[(cc_name, chash)]
        worker.join()
        try:
          worker.close()
        except AttributeError:
          pass

        res_simplified = new_coupling(res[1], FRC, ct_params)
        self._cts[cc_name] = res_simplified
        self.coupling_hashes[cc_name] = chash
        n_jobs -= 1
        n_active -= 1
        if self.plot_progress:
          PG.advanceAndPlot(status=cc_name)

    if multi and len(jobs) > 0 and self.plot_progress:
      PG.advanceAndPlot(status='')
      log('Couplings expansion finished.', 'info')

  @classmethod
  def expand_parameters(self):
    """ Generates the the expansion rules for all parameters in the UFO format.
    The result is stored in :attr:`ctdict`. """

    if self.plot_progress:
      from rept1l.pyfort import ProgressBar
      PG = ProgressBar(len(model.model_objects.all_parameters))
      log('Generating parameter expansion.', 'info')

    multi = False
    if multi:
      rs = multiprocessing.Queue()
      jobs = []

    for parameter in model.model_objects.all_parameters:
      if(parameter.name != 'ZERO' and
         parameter.name not in self._counterterms):
        p_sym = parse_UFO(parameter.name)
        if not multi:
          if self.plot_progress:
            PG.advanceAndPlot(status=str(p_sym))

        if p_sym in self._ctdict:
          if multi:
            PG.advanceAndPlot()
          continue

        if hasattr(parameter, 'ct_shift'):
          ct_shift = parse_UFO(parameter.ct_shift.value)
        else:
          ct_shift = 0

        expansion_dict = self._ctdict.copy()
        if multi:
          j = ExpandExpressionJob(p_sym, expansion_dict,
                                  self._expansion_parameters,
                                  self._renorm_rules,
                                  model.model_objects.all_parameters, rs,
                                  ct_shift)
          jobs.append(j)
          j.start()
        else:
          entries = expand_expression(p_sym, expansion_dict,
                                      self._expansion_parameters,
                                      self._renorm_rules,
                                      model.model_objects.
                                      all_parameters,
                                      ct_shift)
          self.ctdict.update(entries)

    if multi:
      for res in [rs.get() for j in jobs]:
        self.ctdict.update(res)
      for j in jobs:
        j.join()
        PG.advanceAndPlot()

    if self.plot_progress:
      log('Simplifying internal parameter expansion.', 'info')
      from rept1l.pyfort import ProgressBar
      PG = ProgressBar(len(self._ctdict))

    if(hasattr(rept1l_config, 'derived_ct_expansion') and
       rept1l_config.derived_ct_expansion):
      for p in self._ctdict:
        if self.plot_progress:
          PG.advanceAndPlot(status=p.name)
        if p in self._renorm_rules:
          self._ctdict_int[p] = self._ctdict[p]
        else:
          new_expp = 'd' + p.name
          dctv = self._ctdict[p]
          ctparams = tuple([u for u in dctv.free_symbols
                            if u.name in self._expansion_parameters])

          if len(ctparams) > 0:
            self._ctdict_int[p] = p + Symbol(new_expp)
            self._derived_ct_parameters[p.name] = new_expp
            try:
              import rept1l.Model as Model
              zero_tadpole = Model.model.zero_tadpole
            except ImportError:
              zero_tadpole = []
            dctv = get_first_order(dctv, ctparams, return_poly=False).xreplace(zero_tadpole)
            if modelname == 'SM':
              from rept1l.advanced_tools.sm_basis import expr_SM
              dctv = expr_SM(dctv, cache=False)
            elif modelname == 'HS':
              from rept1l.advanced_tools.hs_basis import expr_HS
              dctv = expr_HS(dctv, cache=False)
            elif modelname == 'THDM':
              from rept1l.advanced_tools.thdm_basis import expr_THDM
              dctv = expr_THDM(dctv, cache=False)
            elif modelname == 'NTHDM':
              from rept1l.advanced_tools.nthdm_basis import expr_NTHDM
              dctv = expr_NTHDM(dctv, cache=False)
            elif modelname == 'HT':
              from rept1l.advanced_tools.sm_basis import expr_SM
              dctv = expr_SM(dctv, cache=False)
            else:
              from rept1l.coupling import RCoupling as FRC
              dctv = FRC.complex_mass_couplings(dctv)
            self._derived_ct_values[new_expp] = dctv
            log('ct expansion for ' + p.name + ' = ' + str(dctv), 'debug')
          else:
            # self._derived_ct_values[new_expp] = sympify('0')
            log('No ct expansion for ' + p.name + '.', 'debug')

    self.store()
    if self.plot_progress:
      PG.advanceAndPlot(status='')
      log('Parameter expansion finished.', 'info')

  @staticmethod
  def eliminate_internal_parameters(parameter, internal_parameters):
    """ Elminiates the dependence on internal parameters by expressing the
    parameter(-value) `parameter` in terms of external ones.

    :param parameter: Parameter expression or value
    :type  parameter: sympy

    :param internal_parameters: A dicitonary expressing the value of internal
                                parameters (keys) by external parameters.
    :type  internal_parameters: dict
    """
    found_param = {p: internal_parameters[p] for p in parameter.free_symbols
                   if p in internal_parameters}
    if len(found_param) > 0:
      parameter = parameter.xreplace(found_param)
      return Counterterms.eliminate_internal_parameters(parameter,
                                                        internal_parameters)
    else:
      return parameter

  @classmethod
  def set_cache_storage(cls):
    # backup the original storage parameter
    storage = cls.storage[:]
    name = cls.name

    # cached results are stored in cached_model.txt
    cls.name = 'cached_counterterms_params.txt'
    cls.storage = ['_renorm_rules', '_ctdict', '_expansion_parameters',
                   '_counterterms', '_checksum', '_internal_param_ext']
    return storage, name

  @classmethod
  def compute_ct_hashes(cls):
    # cached results for ct parameters rely on the following files
    hash_param_model = ['parameters.py']
    # parameters_ct.py kept for compatibility
    hash_param_ct = ['user_settings.py', 'parameters_ct.py']

    # retrieve the path of the model and counterterm files
    path, modelname = model.get_modelpath()
    modelpath = path + '/' + modelname

    # compute the hash of the files
    from hashlib import sha256
    modelfiles = [modelpath + '/' + u for u in hash_param_model]
    ctfiles = []
    for u in hash_param_ct:
      filepath = os.path.join(path, u)
      if file_exists(filepath):
        ctfiles.append(filepath)
    hashmodel = [hashfile(open(fname, 'rb'), sha256()) for fname in modelfiles]
    hashct = [hashfile(open(fname, 'rb'), sha256()) for fname in ctfiles]
    hashes = hashmodel + hashct

    return hashes

  @classmethod
  def derive_ct_expansion(self, FRC, expand_couplings=True, plot_progress=True,
                          force=False, multi=False, signal_handler=False,
                          selection=[]):
    """ Computes the counterterm for each coupling. The user has to provide
    expansion rules for the parameters.  """

    self.plot_progress = plot_progress

    storage, name = self.set_cache_storage()
    hashes = self.compute_ct_hashes()

    try:
      # make sure that cached files are loaded even if Counterterms were loaded
      # before
      loaded = self.loaded
      self.loaded = False
      self.load()
      # set loaded flag to previous state
      self.loaded = loaded
    # cache file does not exist -> initialize
    except IOError:
      log('Could not load cached files.', 'warning')
      for attr in self.storage:
        setattr(self, attr, {})
      self.store()

    # check if the parameters have been changed
    cache_parameter = True
    if hasattr(self, '_checksum') and self._checksum == hashes:
      cache_parameter = False

    if cache_parameter:
      # Couplings which have an associated counterterm, will get overwritten by
      # the list of counterterms
      counterterms = [param for param in model.model_objects.all_parameters
                      if hasattr(param, 'counterterm')]

      # Derive rules for the perturbative expansion at one-loop
      #print [u.counterterm.value for u in counterterms]
      self._renorm_rules = {Symbol(c.name): Symbol(c.name) +
                            parse_UFO(c.counterterm.value)
                            for c in counterterms}

      # The actual list of counterterms
      self._counterterms = get_param_ct(model.model_objects.all_parameters)

      self._expansion_parameters = []
      self._derived_ct_parameters = {}
      self._derived_ct_values = {}
      # expansion_parameters: unique list of ct parameters as strings
      for param in self._counterterms.values():
        if param.name not in self._expansion_parameters:
          self._expansion_parameters.append(param.name)

      internal_parameters = {}
      external_parameters = {}

      log("Creating parameters set.", 'info')
      for parameter in model.model_objects.all_parameters:
        if(parameter.name != 'ZERO' and parameter.nature == 'internal' and
           parameter not in self._counterterms):
          c_value = parse_UFO(parameter.value)
          c_value_subs = simplify(c_value.subs(internal_parameters))
          internal_parameters[Symbol(parameter.name)] = c_value_subs
        else:
          c_value = Symbol(parameter.name)
          external_parameters[Symbol(parameter.name)] = c_value

      log("Reducing parameters to external parameters.", 'info')
      from rept1l.pyfort import ProgressBar
      PG = ProgressBar(len(internal_parameters))
      for internalp in internal_parameters:
        c_val = FRC.complex_mass_couplings(self.eliminate_internal_parameters
                                           (internalp, internal_parameters))
        self.internal_param_ext[internalp] = c_val
        PG.advanceAndPlot()

      # clear expansion of all parameters before deriving the expansion
      self._ctdict = {}

      log("Expanding derived counterterm parameters.", 'info')
      # derive parameter expansion
      self.expand_parameters()

    if cache_parameter:
      self._checksum = hashes
      self.store()

    # restore storage
    self.storage = storage
    self.name = name

    do_eliminiate_internal_parameters = False
    if do_eliminiate_internal_parameters:
      ctdict_resolved = {}
      for internalp in self.ctdict:
        ctdict_resolved[internalp] = (self.
                                      eliminate_internal_parameters
                                      (Counterterms.ctdict[internalp],
                                       internal_parameters))
      Counterterms.ctdict = ctdict_resolved

    answer = False
    # if the parameters have been modified the system will rederive the ct
    # couplings for all couplings
    if cache_parameter:
      if not force:
        answer = query_yes_no('Parameters were changed. [Y]: The system will ' +
                              'expand couplings and delete previous results,' +
                              ' continue?')
      else:
        answer = True
    if expand_couplings:
      self.expand_couplings(FRC, parameters_changed=answer, multi=multi,
                            signal_handler=signal_handler, selection=selection)
      self.store()
      FRC.store()




class ExpandExpressionJob(multiprocessing.Process):

    def __init__(self, sym, expansion_dict, expansion_parameters,
                 renorm_rules, parameter_list, result_queue, ct_shift):
      self.sym = sym
      self.expansion_dict = expansion_dict
      self.expansion_parameters = expansion_parameters
      self.renorm_rules = renorm_rules
      self.parameter_list = parameter_list
      self.result_queue = result_queue
      self.ct_shift = ct_shift
      super(ExpandExpressionJob, self).__init__()

    def run(self):
      ret = expand_expression(self.sym, self.expansion_dict,
                              self.expansion_parameters,
                              self.renorm_rules, self.parameter_list,
                              ct_shift=self.ct_shift)
      self.result_queue.put(ret)
      return

class ExpandCouplingJob(multiprocessing.Process):

    def __init__(self, id, func, result_queue, *args):
      self.id = id
      self.func = func
      self.args = args
      self.result_queue = result_queue
      super(ExpandCouplingJob, self).__init__()

    def run(self):

      ret = self.func(*self.args).as_expr()
      self.result_queue.put((self.id, ret))
      return

if __name__ == "__main__":
  import doctest
  doctest.testmod()
