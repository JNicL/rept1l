###############################################################################
#                          particle_counterterms.py                           #
###############################################################################

#############
#  Imports  #
#############

import sys
from sympy import Symbol

import rept1l.Model as Model
from rept1l.helper_lib import get_anti_particle, parse_UFO
from rept1l.combinatorics import flatten, fold

##############
#  Gloabals  #
##############

model = Model.model

#############
#  Methods  #
#############

def get_wf_ct(particle, diagonal=False):
  """ Returns a particle's wavefunction as Sympy expressions.

  The wavefunction is a particle property assigned by the user.

  :particle: UFO particle instance

  >>> Particle = type('Particle', (object,), {})
  >>> t = Particle();
  >>> waveCT = {t: {'I*dZ/2': lambda x: x}}
  >>> t.counterterm = waveCT
  >>> get_wf_ct(t)
  [dZ]
  """
  if hasattr(particle, 'counterterm'):
    if diagonal:
      wavefuncrenorms = [list(particle.counterterm[particle].keys())]
    else:
      wavefuncrenorms = [list(particle.counterterm[mixparticle].keys())
                         for mixparticle in particle.counterterm
                         ]
    wavefuncrenorms = [parse_UFO(u) for u in flatten(wavefuncrenorms)]

    # remove possible factors
    wavefuncrenorms = flatten([[u for u in wf.atoms() if type(u) is Symbol]
                               for wf in wavefuncrenorms])
    return [u for u in wavefuncrenorms]
  else:
    return []

###############################################################################

def get_param_ct(parameter_list, ct_attribute=True, pure_ct=True):
  """ Returns a dictionary of counterterm names and corresponding sympy
  expression.  The counterterm parameter is either identified a Parameter which
  is assigned via the counterterm attribute to another parameter or being of
  type CTParameter.

  :param ct_attribute: Return counterterms identified via counterterm attribute
  :type  ct_attribute: bool

  :param pure_ct: Return counterterms identified by type CTParameter.
  :type  pure_ct: bool

  >>> Parameter = type('Parameter', (object,), {})
  >>> CT = Parameter()
  >>> CT.name = 'dZgs'; CT.value = 'I*dZgs/2'; CT.nature = 'external'
  >>> gs = Parameter(); gs.name = 'gs'; gs.counterterm = CT
  >>> parameter_list = [gs]
  >>> get_param_ct(parameter_list)
  {'dZgs': dZgs}
  """
  # Adding parameters which are set via the counterterm attribute
  if ct_attribute:
    ctparams = {param.counterterm.name:
                parse_UFO(param.counterterm.value)
                for param in parameter_list
                if hasattr(param, 'counterterm') and
                param.counterterm.nature == 'external'}
  else:
    ctparams = {}

  # Adding parameters which are instanced via CTParameter
  # hack to import CTParameter due to circular dependence
  CTParameter = model.modelfile.CTParameter
  if pure_ct:
    ctparams.update({param.name: parse_UFO(param.value)
                     for param in parameter_list
                     if isinstance(param, CTParameter)
                     and param.nature == 'external'})
  ret = {}
  for ctname in ctparams:
    ctvalue_atoms = [u.name for u in ctparams[ctname].atoms()
                     if isinstance(u, Symbol)]
    found_ct = [ctname in u for u in ctvalue_atoms]
    ct_well_defined = fold(lambda x, y: x ^ y, found_ct, False)
    try:
      assert(ct_well_defined)
    except:
      if not any(found_ct):
        raise Exception('Parameter name: "' + ctname +
                        '" not found in Parameter value: ' +
                        str(ctparams[ctname]))
      else:
        ambigous = (",".join([ctvalue_atoms[pos]
                    for pos, val in enumerate(found_ct) if val]))
        raise Exception('Ambiguous definition. Parameter name : "' +
                        ctname + '" found in: ' + ambigous)
      sys.exit()
    ret[ctname] = Symbol(ctvalue_atoms[found_ct.index(True)])
  return ret

###############################################################################

def get_particle_ct(particle, wavefunction=True, only_diagonal=False,
                    only_offdiagonal=False, mass=True, tadpole=False):
  """ Returns all counterterm parameters associated to the particle `particle`
  as Sympy symbols. """

  if only_diagonal:
    assert(not only_offdiagonal)

  if wavefunction:

    cts = get_wf_ct(particle, diagonal=only_diagonal)
    aparticle = get_anti_particle(particle, model.model_objects.all_particles)
    cts_anti = get_wf_ct(aparticle, diagonal=only_diagonal)
    if only_offdiagonal:
      cts_diag = get_wf_ct(particle, diagonal=True)
      cts = [u for u in cts if u not in cts_diag]
      aparticle = get_anti_particle(particle, model.model_objects.all_particles)
      cts_a_diag = get_wf_ct(aparticle, diagonal=True)
      cts_anti = [u for u in cts_anti if u not in cts_a_diag]
    cts = [u for u in set(cts + cts_anti)]
  else:
    cts = []
  if mass:
    if particle.mass != model.param.ZERO:
      m_cts = list(get_param_ct([particle.mass]).values())
      if len(m_cts) == 1:
        cts += [m_cts[0]]
  if tadpole:
    if hasattr(particle, 'field_shift'):
      t_cts = list(get_param_ct([particle.field_shift]).values())
      if len(t_cts) == 1:
        cts += [t_cts[0]]

  return cts

if __name__ == "__main__":
  import doctest
  doctest.testmod()
