###############################################################################
#                               ctexpansion.py                                #
###############################################################################

##############
#  Includes  #
##############

from sympy import Symbol, diff, together, factor_terms, Poly
from rept1l.helper_lib import parse_UFO
from six import iteritems

#############
#  Methods  #
#############

def linear_series(expr, vars):
  """ Performs a a linear taylor expansion of expr in vars.
  Sympys series does a strange/unefficient expansion.
  E.g. try: series(sqrt(x + a + b), x).

  >>> from sympy import sympify
  >>> expr = sympify('sqrt(y + a + b)'); y = Symbol('y')
  >>> linear_series(expr, [y])
  y/(2*sqrt(a + b)) + sqrt(a + b)
  """
  # default assumption that the expansion parameter is real
  vars_to_zero = {u: 0 for u in vars}
  ret = expr.subs(vars_to_zero)
  for var in vars:

    # todo  need to find a fix for this!
    #if not (var.is_real or var.is_imaginary):
      #var = Symbol(var.name, real=True)
      #logging.warning('Set var:' + str(var) + ' to real.')
    ret += var * (diff(expr, var).subs(vars_to_zero))
  return ret


###############################################################################

def get_first_order(expr, ctparams, return_poly=False):
  """ Returns the the part of the expression of first order in ctparams. """
  ret = {key: value for key, value in iteritems(Poly(expr, *ctparams).as_dict()) if sum(key) == 1}
  if return_poly:
    return factor_terms(Poly(ret, *ctparams).expand())
  else:
    return Poly(ret, *ctparams).as_expr()

###############################################################################
#                              expand_expression                              #
###############################################################################


def expand_expression(parameter_val, expansion_dict,
                      expansion_parameters, renorm_rules,
                      parameter_list,
                      ct_shift=0):
    """ Derives the counterterm associated to a primitive or composed parameter
    by applying the chain rule for differentiation given expansion rules for
    internal parameters `renorm_rules`.

    :param parameter_val: The new primitive/composed parameter to be expanded.
    :type  parameter_val: Sympy expression

    :param expansion_dict: Expansion of a set of base couplings.Keys are the
                           parameters and the values are the expanded
                           parameters.
    :type  expansion_dict: dict

    :param expansion_parameters: List of parameters as strings in which the
                                 expansion is done
    :type  expansion_parameters: list

    :return: the updated expansion_dict which includes the expansion of
             `parameter_val`

    Example:

    >>> Parameter = type('Parameter', (object,), {})

    The Parameter class is just an example and mimics the one of the UFO format.
    What we need are certain attributes assigned to instances of that class.
    We define two parameter, the parameter which should be expanded, namely aS
    and the counterterm paramter daS
    >>> aS = Parameter(); aS.name = 'aS'
    >>> daS = Parameter(); daS.name = 'daS'; daS.nature = 'external'

    We define the complete parameter list,
    >>> parameter_list = [daS, aS]

    the renormalization rule,
    >>> renorm_rules = {Symbol('aS'): Symbol('aS') + Symbol('daS')}

    and the counterterm attribute
    >>> aS.counterterm = daS

    The expansion is determined for all paramters depending on aS. Let us
    define the internal parameter saS depending on aS. In UFO internal
    parameters depend on other parameters, in contrast to external ones which
    have a value assigned.

    >>> saS = Parameter()
    >>> saS.name = 'saS'; saS.value = 'sqrt(1+aS)'; saS.nature = 'internal'

    The expansion dict keys are Sympy expression of the parameter names.
    >>> saS_sym = Symbol(saS.name)

    The parameter list is updated:
    >>> parameter_list.append(saS);

    We decalre 'daS' as the only one expansion parameter
    >>> expansion_parameters = ['daS']
    >>> ced = expand_expression(saS_sym, {}, expansion_parameters, renorm_rules,
    ...                         parameter_list)

    expand_expression returns the updated expansion dict and we can obtain the
    expansion via
    >>> ced[saS_sym]
    daS/(2*sqrt(aS + 1)) + sqrt(aS + 1)

    ! do not use this unless you know what you are doing. !
    ! Be sure that the returning expansion dictitonary is not used expanding a !
    ! parameter as it is done above. !

    Expansion directly on parameter values
    >>> from rept1l.helper_lib import parse_UFO
    >>> composed_parameter = parse_UFO('sqrt(1+aS)')
    >>> ced = expand_expression(composed_parameter, {}, expansion_parameters,
    ...                         renorm_rules, parameter_list)

    >>> ced[composed_parameter]
    daS/(2*sqrt(aS + 1)) + sqrt(aS + 1)

    # TODO: (nick 2015-02-13) test broken due to conjugate
    >>> composed_parameter = parse_UFO('conjugate(aS)')
    >>> ced = expand_expression(composed_parameter, ced,
    ...                         ['dZgs'], renorm_rules,
    ...                         parameter_list)

    #>>> ced[composed_parameter]
    #conjugate(G)*conjugate(dZgs)/(2*pi) + conjugate(aS)
    """
    symbol_list = [u for u in parameter_val.free_symbols]

    # remove symbols which have no perturbative expansion
    pi = Symbol('pi')
    if pi in symbol_list:
      symbol_list.remove(pi)

    if len(symbol_list) == 1:
      if parameter_val == symbol_list[0]:
        for parameter in parameter_list:
          if parameter_val.name == parameter.name:
            if hasattr(parameter, 'counterterm'):
              return {parameter_val: renorm_rules[parameter_val]}
            elif parameter.nature == 'external':
              return {}
            else:
              sub = parse_UFO(parameter.value)
              if hasattr(parameter, 'ct_shift'):
                parameter_ct_shift = parse_UFO(parameter.ct_shift.value)
              else:
                parameter_ct_shift = 0
              tmp = expand_expression(sub, expansion_dict,
                                      expansion_parameters,
                                      renorm_rules, parameter_list,
                                      ct_shift=parameter_ct_shift)
              if sub in tmp:
                tmp[parameter_val] = tmp[sub]
                del tmp[sub]
              return tmp

        raise Exception('Parameter not found: ' + str(parameter_val))

    for sym in symbol_list:
      if sym not in expansion_dict:
        new_exp = expand_expression(sym, expansion_dict, expansion_parameters,
                                    renorm_rules, parameter_list)
        expansion_dict.update(new_exp)

    # expansion via chain rule
    expr = parameter_val
    ct_params = tuple([Symbol(u) for u in expansion_parameters])

    expr = expr.xreplace(expansion_dict)
    expr = linear_series(expr, ct_params)
    if ct_shift != 0:
      expr += ct_shift

    try:
      import rept1l.Model as Model
      zero_tadpole = Model.model.zero_tadpole
      expr = expr.xreplace(zero_tadpole)
    except ImportError:
      pass

    new_exp = {parameter_val: expr}

    expansion_dict.update(new_exp)
    return expansion_dict

#==============================================================================#
#                               expand_coupling                                #
#==============================================================================#

def expand_coupling(coupling, expansion_dict, expansion_parameters, ct_shift=0,
                    return_poly=True):
  """ Expands a coupling perturbatively.

  :param coupling: product or sum of products of parameters.
  :type  coupling: sympy expression

  :param expansion_dict: A dictionary specifying how to expand the parameters
                         in parameters and counterterm parameters.
  :type  expansion_dict: dictionary where the keys and values are sympy
                         expressions

  :param expansion_parameters: The set of counterterm parameters. The coupling
                               is taylor expanded with respect to this set and
                               the expansion point is zero.
  :type  expansion_parameters: List of strings

  :param ct_shift: A counterterm shift added to the result.
  :type  ct_shift: sympy expression

  Example:

  We define a primitive parameter
  >>> G = Symbol('G')

  Couplings are usually composed of multiple (different) parameters
  >>> coupling =  G*G

  The parameter G is expanded according to G -> G + dG
  >>> expansion_dict = {Symbol('G'): Symbol('G') + Symbol('dG')}

  Declare  dG as the only counterterm parameter
  >>> expansion_parameters = ['dG']

  Expand the coupling
  >>> expand_coupling(coupling, expansion_dict,
  ...                 expansion_parameters)
  Poly(2*G*dG, dG, domain='ZZ[G]')
  """

  # -> choose if the expansion is done in one step or
  # step by step for each ct parameter
  # step_by_step = True is slower
  step_by_step = False

  #  dont use both simplifys -> slower than just simplify on final result
  global_simplify = False  # simplify the final result
  intermediate_simplify = False  # simplify intermediate results

  isy = together if intermediate_simplify else lambda x: x
  gsy = together if global_simplify else lambda x: x

  try:
    import rept1l.Model as Model
    model = Model.model
    zero_tadpole = model.zero_tadpole
  except ImportError:
    zero_tadpole = []

  if step_by_step:
    expanded_coupling = 0
    cc = [sym for sym in coupling.free_symbols if sym in expansion_dict]
    for c in cc:
      # perform the substitution g -> g + dg
      expr = coupling.xreplace({c: expansion_dict[c]})

      # expand in dg
      ctparams = tuple([u for u in expansion_dict[c].free_symbols
                        if u.name in expansion_parameters])
      expr = linear_series(expr, ctparams)

      # keep only the correction terms in first order of ct_params
      foe = get_first_order(expr, ctparams,
                            return_poly=return_poly).xreplace(zero_tadpole)
      expanded_coupling += isy(foe)
    expanded_coupling = gsy(expanded_coupling)
  else:
    # perform the substitution g -> g + dg
    expr = coupling.xreplace(expansion_dict).xreplace(zero_tadpole)
    expanded_coupling = 0

    # expand in dg
    ct_params = tuple([Symbol(u) for u in expansion_parameters])
    expr = linear_series(expr, ct_params)

    # keep only the correction terms in first order of ct_params
    poly_exp = iteritems(Poly(expr, *ct_params).as_dict())
    cts = {key: isy(value) for key, value in poly_exp if sum(key) == 1}

    if return_poly:
      expanded_coupling = gsy(Poly(cts, *ct_params))
    else:
      expanded_coupling = gsy(Poly(cts, *ct_params).as_expr())

  return expanded_coupling + ct_shift


if __name__ == "__main__":
  import doctest
  doctest.testmod()
