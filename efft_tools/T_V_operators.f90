!#####################################################################
  program test

  use recola

  implicit none

  call set_form_val(4)
  call define_process_rcl(1,'chi chi~ -> chi chi~','NLO')
  call set_output_file_rcl('*')
  !call define_process_rcl(1,'chi~ chi -> chi chi~','NLO')
  !call define_process_rcl(1,'chi~ chi -> chi~ chi ','NLO')
  !call define_process_rcl(1,'chi chi~ -> chi~ chi ','NLO')

  call unselect_all_powers_LoopAmpl_rcl(1)

  call select_power_LoopAmpl_rcl(1,'FV4', 1)
  call select_power_LoopAmpl_rcl(1,'FV4', 0)
  call select_power_LoopAmpl_rcl(1,'FT4', 0)
  call select_power_LoopAmpl_rcl(1,'FT4', 1)
  call select_power_LoopAmpl_rcl(1,'YUK', 2)
  call set_lp_val(1, .true.)
  call set_lp_val(2, .false.)
  call set_lp_val(3, .false.)
  call set_lp_val(0, .false.)
  call set_vertex_functions(.true.)
  call generate_processes_rcl

  call deallocate_rcl

  end program test

!#####################################################################
