#==============================================================================#
#                                wilsoncoeff.py                                #
#==============================================================================#
from __future__ import print_function

#============#
#  Includes  #
#============#

# Autoload the the UFO from modeltests.
import os
from subprocess import Popen, PIPE
RP = os.environ['REPT1L_PATH']
RPT = os.path.join(RP, 'tools')
cmd = os.path.join(RPT, 'crm') + ' -a'
p = Popen(cmd, stdout=PIPE, shell=True)
modelpath, err = p.communicate()
base = os.path.basename(modelpath)
UFO_modelpath = os.path.dirname(modelpath)
os.environ['REPTIL_MODEL_PATH'] = UFO_modelpath

from sympy import Symbol, factor
from rept1l.renormalize import export_expression

#=============#
#  Procedure  #
#=============#

from rept1l.parsing import ParseAmplitude
# Custom parsing class which computes MSbar for loop amplitudes
class MSAmplitude(ParseAmplitude):

  def __init__(self, **kwargs):
    super(MSAmplitude, self).__init__()

  def process_loopamp(self, amp):
    amp = self.compute_MS(amp)
    return amp


# The process file to be parsed
processed_file = 'process1.log'

print ('Parsing Amplitudes.')
MS = MSAmplitude()
MS.parse(processed_file)

print ('Exporting Amplitudes.')
# Filter for two orders
# Case FV4=1, FT4=0, YUK=2
order1 = (1, '1,0,2')
# Case FV4=0, FT4=1, YUK=2
order2 = (1, '0,1,2')

amp1 = MS.amplitudes[order1]
amp2 = MS.amplitudes[order2]

# Build the full amplitude and export the expression to external parameters
amp = amp1+amp2
amp = export_expression(amp)

# Search for lorentz structures
ls = [u for u in amp.free_symbols if u.name.startswith('L')]
ls_dict = {u: MS.ls_dict[u.name] for u in ls}
print("Lorentz structures:")
for ls in ls_dict:
  print("ls:", ls)
  print(ls_dict[ls])

Sigma = Symbol('Sigma')
ss = [Sigma, Sigma]
T = [u for u in ls_dict if ls_dict[u][0] == ss]
V = [u for u in ls_dict if u not in T]

print("Tensor four-fermion operators:", T)
print("Vector four-fermion operators:", V)

amp = amp.expand()

# Collect contributions to tensor operators
ampT = 0
for t in T:
  ampT += amp.coeff(t)*t

ampT = factor(ampT)
print("ampT:", ampT)

# Collect contributions to vector operators
ampV = 0
for v in V:
  ampV += amp.coeff(v)*v

ampV = factor(ampV)

print("ampV:", ampV)
