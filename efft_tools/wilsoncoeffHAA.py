#==============================================================================#
#                                wilsoncoeff.py                                #
#==============================================================================#
from __future__ import print_function

#============#
#  Includes  #
#============#

# Autoload the the UFO from modeltests.
import os
from subprocess import Popen, PIPE
#RP = os.environ['REPT1L_PATH']
#RPT = os.path.join(RP, 'tools')
#cmd = os.path.join(RPT, 'crm') + ' -a'
#p = Popen(cmd, stdout=PIPE, shell=True)
#modelpath, err = p.communicate()
#base = os.path.basename(modelpath)
#UFO_modelpath = os.path.dirname(modelpath)
#os.environ['REPTIL_MODEL_PATH'] = UFO_modelpath

from sympy import Symbol, factor
from rept1l.parsing import FormProcess
from rept1l.parsing import ParseAmplitude
from rept1l.renormalize import export_expression
from rept1l.advanced_tools.sm_basis import expr_SM
from rept1l.counterterms import get_param_ct, get_wf_ct
from rept1l.helper_lib import flatten
from rept1l.vertex import Vertex
from rept1l.counterterms import Counterterms
from rept1l.coupling import RCoupling as FRC
from rept1l.renormalize import RenoConst as RC

import rept1l.Model as Model
model = Model.model
P = model.P

#=============#
#  Procedure  #
#=============#

class OperatorMS(FormProcess):

  def __init__(self, **process_definitions):
    level_process = {'tree': False, 'bare': True, 'ct': False, 'r2': False}
    # In mode 4 all momenta are incoming except for the last one!
    form_amplitude = 4
    process_definitions['level_process'] = level_process
    process_definitions['form_amplitude'] = form_amplitude
    process_definitions['all_couplings_active_frm'] = False
    order_NLO = {'QCD': 0, 'QED': 3, 'LAM': 1}
    process_definitions['order_NLO'] = order_NLO
    process_definitions['suppress_gen_stdout'] = False
    super(OperatorMS, self).__init__(**process_definitions)

class OperatorCT(FormProcess):

  def __init__(self, **process_definitions):
    level_process = {'tree': False, 'bare': False, 'ct': True, 'r2': False}
    # In mode 4 all momenta are incoming except for the last one!
    form_amplitude = 4
    process_definitions['level_process'] = level_process
    process_definitions['form_amplitude'] = form_amplitude
    process_definitions['all_couplings_active_frm'] = False
    process_definitions['suppress_gen_stdout'] = False
    super(OperatorCT, self).__init__(**process_definitions)

#------------------------------------------------------------------------------#

class MSAmplitude(ParseAmplitude):

  def __init__(self, **kwargs):
    super(MSAmplitude, self).__init__(suppress_gen_stdout=False)

  def process_loopamp(self, amp):
    amp = self.compute_MS(amp)

    from rept1l.formutils import FormPype

    FP = FormPype()
    exprs = []
    exprs.extend(['l amp = (' + amp + ');'])
    # using momentum conservation for 3-Vertex
    exprs.extend(['id p1 = -p2 +p3;'  # <- get rid of p1 in scalar products
                  'id n = 4;',
                  '.sort',
                  'id p1(mu1) = +p3(mu1) -p2(mu1);',
                  #'id p2(mu2) = +p3(mu2) -p1(mu2);',
                  #'id p3(mu3) = +p1(mu3) +p2(mu3);',
                  '.sort',
                  'id p2(mu2) = 0;',
                  'id p3(mu3) = 0;',
                  #'id p2.p2 = 0;',
                  #'id p3.p3 = 0;',
                  #'id p2.p2 = MZ^2;',
                  #'id p3.p3 = 0;',
                  #'id p2.p3 = -MH^2/2+MZ^2/2;',
                  #'id p2.p2 = MZ^2;',
                  #'id p3.p3 = MZ^2;',
                  #'id p2.p3 = -MH^2/2+MZ^2;',
                  '.sort',
                  'id p2 = -p2;',
                  '.sort'])
    exprs.extend(['id n = 4;',
                  '.sort'])
    FP.eval_exprs(exprs)
    amp = ''.join(FP.return_expr('amp', stop=True).split())
    return amp

  def process_ctamp(self, amp):
    """ Processing the counterterm FORM amplitudes. """
    from rept1l.formutils import FormPype

    FP = FormPype()
    exprs = []
    exprs.extend(['l amp = (' + amp + ');'])
    # using momentum conservation for 3-Vertex
    exprs.extend(['id p1 = -p2 +p3;'  # <- get rid of p1 in scalar products
                  'id n = 4;',
                  '.sort',
                  'id p1(mu1) = +p3(mu1) -p2(mu1);',
                  '.sort',
                  'id p2(mu2) = 0;',
                  'id p3(mu3) = 0;',
                  '.sort',
                  'id p2 = -p2;',
                  '.sort'])
    FP.eval_exprs(exprs)
    amp = ''.join(FP.return_expr('amp', stop=True).split())
    print("amp:", amp)
    return amp

#------------------------------------------------------------------------------#

class OperatorMixing(object):

  # list of wilson coefficients
  wcs = ['aPhiQuad', 'aPhiW', 'aPhiB', 'aPhiWB', 'aPhiD']

  def __init__(self, particles, **kwargs):
    print('Generating loop amplitude')
    OMS = OperatorMS(particles=particles, **kwargs)
    print('Extracting ms part and parsing')
    MSA = MSAmplitude()
    print("OMS.processed_file:", OMS.processed_file)
    MSA.parse(OMS.processed_file)
    self.amplitude = MSA.amplitudes[(1, '0,3,1')]
    self.ls_dict = MSA.ls_dict

    print('Generating ct amplitude')
    OMS = OperatorCT(particles=particles, **kwargs)
    print('parsing ct amplitude')
    MCT = MSAmplitude(ls_dict=self.ls_dict)
    MCT.parse(OMS.processed_file)
    self.CT = sum(MCT.amplitudes.values())

  def reveal_CTs(self):
    """ Reveals the counterterm dependence in the ct amplitude """
    c_set = FRC.get_couplings_from_expr(self.CT)
    all_ct = [u for u in Counterterms.get_all_ct()]
    coupl_repl = {c: FRC.find_expression(all_ct, c) for c in c_set}
    self.CT_reveal = self.CT.subs(coupl_repl)

    check_amps = [self.CT_reveal]
    ct = set(list(flatten([[v for v in u.free_symbols if v in all_ct]
                           for u in check_amps])))

    wcs_ct = [getattr(model.param, wc).counterterm.name for wc in self.wcs
              if hasattr(model.param, wc) and
              hasattr(getattr(model.param, wc), 'counterterm')]
    self.ct_dependence = [u for u in ct if u.name not in wcs_ct]

  def new_coupling(self, expr, base=[], flag=''):
    """ """
    from rept1l.coupling import RCoupling
    from sympy import together
    if len(base) > 0:
      #exprc = collect(expr, base)
      exprc = expr.expand()
      pexpr = 0
      for b in base:
        pexpr += b*RCoupling(together(exprc.coeff(b)), flag, subs_cvalues=False,
                             check_coupling=False).return_coupling()

      pexpr += RCoupling(exprc.subs({b: 0 for b in base}), flag,
                         subs_cvalues=False,
                         check_coupling=False).return_coupling()
      return pexpr
    else:
      return RCoupling(expr, flag, subs_cvalues=False,
                       check_coupling=False).return_coupling()

  def reveal_wilson_coeffs(self, expr):
    wcs_base = [Symbol(u) for u in self.wcs]
    expr = FRC.find_expression(wcs_base, expr)
    return self.new_coupling(expr, base=wcs_base)

  def build_ms_CT_dependence(self):
    reno_sols = RC.build_ms_ct_solutions(self.ct_dependence,
                                         plugin_ms_values=True)
    RC.store()
    from sympy import together
    reno_sols_ordered = {}
    orders_of_interest_ctp = [(('LAM', 0), ('QCD', 0), ('QED', 1)),
                              (('LAM', 0), ('QCD', 0), ('QED', 2)),
                              (('LAM', 1), ('QCD', 0), ('QED', 2))]

    orders_of_interest_amp = [(('LAM', 1), ('QCD', 0), ('QED', 3))]
    for u in reno_sols:
      rso = FRC(reno_sols[u], 'CT', check_coupling=False,
                subs_cvalues=False, forbid_update=False, add_order=True,
                simp_func=together).return_coupling()
      for e in rso.free_symbols:
        o = tuple(sorted((u, FRC.coupling_orders[e.name][u])
                         for u in FRC.coupling_orders[e.name]))
        #print("o:", o)
        if o not in orders_of_interest_ctp:
          rso = rso.subs({e: 0})
      print("u, rso:", u, rso)
      #rso = sum(self.reveal_wilson_coeffs(FRC.coupling_values[u.name]) for u in rso.free_symbols)
      print("rso:", rso)
      reno_sols_ordered[u] = rso

    #print("reno_sols:", reno_sols)
    #from rept1l.advanced_tools.sm_basis import expr_SM
    amp = self.CT_reveal.xreplace(reno_sols_ordered)
    amp = FRC(amp, 'CT', opt_name='amp_CT', check_coupling=False,
              subs_cvalues=False, add_order=True).return_coupling()
    for e in amp.free_symbols:
      o = tuple(sorted((u, FRC.coupling_orders[e.name][u])
                       for u in FRC.coupling_orders[e.name]))
      print("o:", o)
      if o not in orders_of_interest_amp:
        amp = amp.subs({e: 0})
      ##else:
    #amp = amp.subs({e: FRC.coupling_values[e.name] for e in amp.free_symbols})
    #amp = self.reveal_wilson_coeffs(amp)
    print("amp:", amp)

    self.CT_subs = amp


# The process file to be parsed
#processed_file = 'processA.log'
#processed_file = 'processZA.log'
#processed_file = 'processZZ.log'
#processed_file = 'processWW.log'



if __name__ == '__main__':

  print ('Parsing Amplitudes.')
#MS = MSAmplitude()
#MS.parse(processed_file)

#ps = [P.H, P.a, P.a]
  ps = [P.H, P.Z, P.A]
  OM_HAA = OperatorMixing(ps)

  print ('Exporting Amplitude.')
# Filter for two orders
# Case QCD=0, QED=3, LAM=1

  OP = OM_HAA
#ampCT = OP.CT

#print("ampCT:", ampCT)

  OP.reveal_CTs()
  print("OP.ct_dependence:", OP.ct_dependence)
  print("OP.CT_reveal:", OP.CT_reveal)
  OP.build_ms_CT_dependence()


#amp = OP.amplitude + OP.CT_subs
  amp = OP.CT_subs
#from sympy import together, fraction
#amp, den = fraction(together(amp))
  print("amp:", amp)
  import sys
  sys.exit()
  c_set = FRC.get_couplings_from_expr(amp)
#aPhiD = Symbol('aPhiD')
  aPhiQuad = Symbol('aPhiQuad')
  coupl_repl = {c: FRC.find_expression([aPhiQuad], c).expand().coeff(aPhiQuad) for c in c_set}
  print("coupl_repl:", coupl_repl)
  amp_aPhiQuad = amp.xreplace(coupl_repl)
#print("amp_aPhiD:", amp_aPhiD)
  print("amp_aPhiQuad:", amp_aPhiQuad)

  import sys
  sys.exit()


#Vertex.load()
#pkeys = Vertex.lorentzCT_dict.keys()
#pkey = [u for u in pkeys if len(u) == 3 and u[0].name == 'H' and u[1].name == 'Z' and u[2].name == 'a'][0]
#v = Vertex.lorentzCT_dict[pkey]
#print (v)

#c_set = [v for v in flatten([FRC.get_couplings_from_expr(u)
                              #for u in Reg.renorm_conditions.values()])]
#c_set = FRC.get_couplings_from_expr(ampCT)
#all_ct = [u for u in Counterterms.get_all_ct()]
#print("all_ct:", all_ct)
#coupl_repl = {c: FRC.find_expression(all_ct, c) for c in c_set}
#ampCT = ampCT.subs(coupl_repl)

#ct_renorm = set(list(flatten([[v for v in u.free_symbols if v in all_ct]
                                 #for u in [ampCT]])))
#print("coupl_repl:", coupl_repl)
#print("ct_renorm:", ct_renorm)

#import sys
#sys.exit()



#ampct = OP.CT
#amp = export_expression(amp)

# Search for lorentz structures
  ls = [u for u in amp.free_symbols if u.name.startswith('L') and u.name != 'Lm2']
  ls_dict = {u: OP.ls_dict[u.name] for u in ls}
  print("Lorentz structures:")
  for ls in ls_dict:
    print("ls:", ls)
    print(ls_dict[ls])

#Sigma = Symbol('Sigma')
#ss = [Sigma, Sigma]
#V = [u for u in ls_dict if u not in T]

  amp = expr_SM(amp)
  amp = amp.expand()


#ampct = expr_SM(ampct)
#ampct = ampct.expand()


  wcs = [Symbol(u) for u in OperatorMixing.wcs]

  for wc in wcs:
    c = amp.coeff(wc)
    print(str(wc) + ':')
    print(expr_SM(c))


#wcs = ['daPhiQuad', 'daPhiW', 'daPhiB', 'daPhiWB', 'daPhiD']
#wcs = [Symbol(u) for u in wcs]

#for wc in wcs:
    #c = ampct.coeff(wc)
    #print(str(wc) + ':')
    #print(expr_SM(c))
# Collect contributions to vector operators
#ampV = 0
#for ls in ls_dict:
    #print("ls:", ls)
    #print("amp.coeff(ls):", export_expression(amp.coeff(ls)))
    #ampV += amp.coeff(ls)*v

#ampV = factor(ampV)

#print("ampV:", ampV)
