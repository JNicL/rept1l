###############################################################################
#                            fourfermionmix_gen.py                            #
###############################################################################

import os
import rept1l.modeltests.ff4_ns_setup as ff4ns
import rept1l.modeltests.setup_models as sm

def gen_model(perform_expansion=False, vertices_file=None, only_form=True):
  """ Generates the RECOLA modelfile for testing four fermion interactions """

  # Create a temporary folder to store all sources & binaries for testing
  ff4nspath_base = sm.setup_model(ff4ns.driver_ff4ns_path)
  print("ff4nspath_base:", ff4nspath_base)

  # Path for the RECOLA modelfile library
  ff4nspath_run = os.path.join(ff4nspath_base, 'libff4')
  os.makedirs(ff4nspath_run)

  # Copy drivers to temporary folder Run REPT1L to generate the RECOLA modelfile
  sm.generate_recola_model(ff4nspath_base, ff4nspath_run,
                           ff4ns.driver_ff4ns_path,
                           copy_drivers=True,
                           vertices_file=vertices_file,
                           perform_expansion=perform_expansion,
                           only_form=only_form)

  sm.compile_model(ff4nspath_run)
  linked_model = sm.compile_recola()
  linked_model_path = os.path.dirname(linked_model)

  linked_model = sm.compile_pyrecola()
  linked_model_path_pyr = os.path.dirname(linked_model)


if __name__ == "__main__":
  gen_model()
