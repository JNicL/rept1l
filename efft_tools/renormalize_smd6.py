#==============================================================================#
#                             renormalize_smd6.py                              #
#==============================================================================#

from sympy import Symbol, sympify
from rept1l.renormalize import Renormalize
from rept1l.renormalize import RenormalizeTadpole
from rept1l.renormalize import RenoConst
from rept1l.renormalize import get_particle_ct
from rept1l.helper_lib import Rept1lArgParser
import rept1l.Model as Model
model = Model.model

class RenormalizeHiggs(Renormalize):

  RenoConst = RenoConst
  orders = (('QCD', 0), ('QCD', 2),
            ('QED', 0), ('QED', 2),
            ('LAM', 0), ('LAM', 1))

  def __init__(self, particles, *args, **kwargs):
    super(RenormalizeHiggs, self).__init__(particles, RenoConst,
                                           *args, **kwargs)

  @classmethod
  def renormalize_neutral_higgs(cls, simplify=False, **kwargs):
    """ Renormalize the SM higgs onshell. """
    cls.load_storage()
    SM_higgs = [model.P.H]
    cls(SM_higgs, simplify=simplify, select_power_NLO=cls.orders,
        inc_cms_corr=False)

  @classmethod
  def renormalize_tadpole_SM(cls, simplify=False, **kwargs):
    """ Higgs tadpole renormalization. """
    cls.load_storage()
    dt = RenormalizeTadpole(model.P.H, renoscheme='onshell', tadpole_expr='dt')
    cls.update_renos(dt.solutions, renoscheme='onshell', simplify=simplify,
                     **kwargs)

  @classmethod
  def renormalize_gsw(cls, simplify=False, **kwargs):
    """ Renormalizes the GSW theory onshell.  """
    vector_bosons = [[model.P.A, model.P.Z], model.P.W__minus__]
    cls(vector_bosons, simplify=simplify, inc_cms_corr=False)

  @classmethod
  def renormalize_alpha_0(cls, BFM=False, simplify=False, **kwargs):
    """ The electric charge is renormalized in the Thomson-limit via the
    modified electric Ward identity"""

    cls.load_storage()

    # dZZa wavefunction
    Z_ct = get_particle_ct(model.P.Z, only_offdiagonal=True, mass=False)
    assert(len(Z_ct) == 1)
    dZZa = Z_ct[0]

    # dZa wavefunction
    a_ct = get_particle_ct(model.P.A, only_diagonal=True, mass=False)
    assert(len(a_ct) == 1)
    dZa = a_ct[0]

    # dZaZ wavefunction
    a_ct = get_particle_ct(model.P.A, only_offdiagonal=True, mass=False)
    assert(len(a_ct) == 1)
    dZaZ = a_ct[0]

    if BFM:
      dZee_val = -dZa/2
    else:
      dZee_val = -dZa/2 - sympify('sw/cw')*dZZa/2

    cls.update_renos({Symbol('dZee'): dZee_val}, renoscheme='onshell',
                     simplify=simplify)


if __name__ == '__main__':
  RenormalizeHiggs.renormalize_tadpole_SM()
  RenormalizeHiggs.renormalize_neutral_higgs()
  RenormalizeHiggs.renormalize_gsw()
  RenormalizeHiggs.renormalize_alpha_0()
