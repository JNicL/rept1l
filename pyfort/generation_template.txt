                                                      .---.
                                                     /  .  \ 
                                                    |\_/|   |
                                                    |   |  /|
     .----------------------------------------------------' |
    /  .-.   %(FILENAME)-30s                 |
   |  /   \  This code was generated with REPT1L.           |
   | |\_.  | Change this file and you get what you deserve. |
   |\|  | /| Author: %(AUTHOR)-20s                   |      
   | `---' | UFO date: %(UFODATE)-14s                       |
   |       | UFO version: %(UFOVERSION)-14s                    |
   |       | Generation date: %(DATE)-14s             /
   |       |----------------------------------------------'
   \       |
    \     /
     `---'
