#==============================================================================#
#                                    pyfort                                    #
#==============================================================================#
from __future__ import print_function

#============#
#  Includes  #
#============#

import os
import sys
import re
import types
import datetime
from rept1l.logging_setup import log

try:
  from types import ListType
  list_type = ListType
except ImportError:
  list_type = list

#===========#
#  Methods  #
#===========#

#==============================================================================#
#                                  Statement                                   #
#==============================================================================#

#------------------------------------------------------------------------------#
class Statement(object):
  """
  Used for storing source code.
  Features: Automatic line break and indentation.
  """
  def __init__(self, n_tabs):
    self.stmnts = []
    self.n_tabs = n_tabs
    # Tabspace by default equal to two whitespaces
    self.tabsspace = "  "
    # Linebreak at 80
    self.linelength = 80
    # Linebreak in Fortran &
    self.newline = '&'
    # Comments in Fortran &
    self.comment = '!'

#------------------------------------------------------------------------------#
  def mergeParts(self, split, treat_as_eq=False, eq_start=0):
    """
    >>> st = Statement(0)
    >>> st.setStatement('s = 5', treat_as_eq=True)
    >>> st.printStatment()
    s = 5
    """
    tmp = ''
    for i in range(self.n_tabs):
      tmp += self.tabsspace
    if treat_as_eq and eq_start > 0:
      tmp += " " * (eq_start + 2)

    split_cpy = split[:]

    for i in range(len(split)):
      if (len(tmp)+len(split[i]) < self.linelength):
        tmp += split[i]
        split_cpy.remove(split[i])
        if (i == len(split)-1):
          if (len(tmp) < self.linelength):
            if (treat_as_eq and eq_start == 0):
              try:
                eq_start = tmp.index('=') - self.n_tabs*len(self.tabsspace)
              except:
                treat_as_eq = False

            self.stmnts.append(tmp)
          else:
            print("")
            sys.exit("Cannot split statement - " +
                     "identifier longer than line length")

      else:
        fill = self.linelength - len(tmp)
        for j in range(fill-1):
            tmp += " "
        tmp += self.newline
        if (treat_as_eq and eq_start == 0):
          try:
            eq_start = tmp.index('=') - self.n_tabs*len(self.tabsspace)
          except:
            try:
              eq_start = tmp.index('::') + 1 - self.n_tabs*len(self.tabsspace)
            except:
              treat_as_eq = False

        self.stmnts.append(tmp)
        return self.mergeParts(split_cpy, treat_as_eq, eq_start)

  def mergePartsNonRec(self, split, treat_as_eq=False, eq_start=0, comment=False):
    """
    >>> st = Statement(0)
    >>> st.setStatement('Assigning a value to s', comment=True)
    >>> st.setStatement('s = 5', treat_as_eq=True)
    >>> st.printStatment()
    ! Assigning a value to s
    s = 5
    """
    split_cpy = split[:]
    while len(split_cpy) > 0:
      tmp = ''
      for i in range(self.n_tabs):
        tmp += self.tabsspace
      if treat_as_eq and eq_start > 0:
        tmp += " " * (eq_start + 2)

      offset = len(split) - len(split_cpy)
      for i in range(offset, len(split)):
        if (len(tmp)+len(split[i]) < self.linelength):
          tmp += split[i]
          split_cpy.remove(split[i])
          if (i == len(split)-1):
            if (len(tmp) < self.linelength):
              if (treat_as_eq and eq_start == 0):
                try:
                  eq_start = tmp.index('=') - self.n_tabs*len(self.tabsspace)
                except:
                  treat_as_eq = False

              if comment:
                tmp = self.comment + ' ' + tmp
              self.stmnts.append(tmp)
            else:
              print("")
              sys.exit("Cannot split statement - " +
                       "identifier longer than line length")

        else:
          if tmp.strip() == '':
            # -> resulting in endless loop
            log('Failed mergeParts for split:', 'warning')
            log(str(split), 'warning')
            treat_as_eq = False
            break
          fill = self.linelength - len(tmp)
          for j in range(fill - 1):
            tmp += " "
          if not comment:
            tmp += self.newline
          if (treat_as_eq and eq_start == 0):
            try:
              eq_start = tmp.index('=') - self.n_tabs * len(self.tabsspace)
            except Exception:
              try:
                eq_start = tmp.index('::') + 1 - self.n_tabs * len(self.tabsspace)
              except Exception:
                treat_as_eq = False

          if comment:
            tmp = self.comment + ' ' + tmp
          self.stmnts.append(tmp)
          break

  @classmethod
  def fix_boolean_expr(self, split):
    """ Takes care of boolean expressions in Fortran which are not allowed to be
    split.

    >>> split = ['', 'if', ' (', 'f', '(', 'w1', ') .', 'eq', '. ', 'pos_hel',\
' .', 'and', '. ', 'f', '(', 'w2', ') .', 'eq', '. ', 'unpol', ') ', 'then', '']
    >>> Statement.fix_boolean_expr(split)
    ['', 'if', ' (', 'f', '(', 'w1', ') ', '.eq.', ' ', 'pos_hel', ' ', \
'.and.', ' ', 'f', '(', 'w2', ') ', '.eq.', ' ', 'unpol', ') ', 'then', '']
    """
    split_new = []
    if len(split) > 1:
      splitl = split[0]
      for u in range(len(split)-1):
        splitr = split[u+1]
        if len(splitl) > 0 and splitl[-1] == '.':
          split_new.append(splitl[:-1])
          splitl = '.' + splitr
        elif len(splitl) > 1 and splitl[0] == '.' and splitl.count('.') != 2:
          if splitr[0] == '.':
            split_new.append(splitl + '.')
            splitl = split[u+1][1:]
          else:
            split_new.append(splitl)
            splitl = split[u+1]
        else:
          split_new.append(splitl)
          splitl = split[u+1]
      split_new.append(splitl)
      return split_new
    else:
      return split

  @classmethod
  def reconstruct_strings(self, split):
    """ Strings are accidentally split by our split method. Need to reconstruct
    those strings as they lead to wrong fotran code.

    >>> split = ["'func", "tion(x)' + 2*b", "-7*'x'+ 'y", "**2'"]
    >>> Statement.reconstruct_strings(split)
    ["'function(x)' + 2*b", "-7*'x'+ 'y**2'"]
    """
    split = self.fix_boolean_expr(split)
    merge = [pos for pos, u in enumerate(split) if u.count("'") % 2 == 1]
    merges = list(zip(merge[0::2], merge[1::2]))
    final = {i: (i, i) for i in range(len(split))
             if not any(i in range(u[0], u[1]+1) for u in merges)}

    final.update({u[0]: u for u in merges})
    split_new = [''.join(split[final[i][0]:final[i][1]+1])
                 for i in range(len(split)) if i in final]
    return split_new

#------------------------------------------------------------------------------#
  def setStatement(self, st, treat_as_eq=False, comment=False):
    # introduce some preprocessing here to make sure the string has the correct
    # structure such as no trailing leading blanks etc.
    tmp = ''
    for i in range(self.n_tabs):
      tmp += self.tabsspace

    #strip() removes leading and trailing whitespace characters.
    st = st.strip()
    tmp += st
    if len(tmp) > self.linelength:
      # always split between whitespaces
      split = [u for u in re.split('(\w+)', tmp)]
      split = self.reconstruct_strings(split)

      # but restoring whitespaces afterwards for better readability
      if treat_as_eq and '=' in tmp:
        eq_start = tmp.index('=') - self.n_tabs*len(self.tabsspace)
        if eq_start > 0:
          minimal_split = [len(u) + eq_start for u in split[2:]]
          if any(u > self.linelength for u in minimal_split):
            max_length = max(u for u in minimal_split
                             if u > self.linelength)
            log('Split only possible with higher linelength', 'info')
            #self.linelength = max_length + 3
            self.linelength = max_length + self.n_tabs*len(self.tabsspace) + 1
      else:
        split_violation = any(len(u) + 1 > self.linelength for u in split)
        if split_violation:
          max_length = max(len(u) for u in split if len(u) + 1
                           > self.linelength)
          log('Split only possible with higher linelength', 'info')
          self.linelength = max_length + self.n_tabs*len(self.tabsspace) + 1

      if self.n_tabs > 0:
        split.pop(0)
      self.mergePartsNonRec(split, treat_as_eq, comment=comment)
    else:
      if comment:
        tmp = self.comment + ' ' + tmp
      self.stmnts.append(tmp)

#------------------------------------------------------------------------------#
  def printStatment(self, File=None):
    for i in self.stmnts:
      if(File is not None):
        File.write(i+'\n')
      else:
        print(i)
#------------------------------------------------------------------------------#

#==============================================================================#
#                                    PyFort                                    #
#==============================================================================#

#------------------------------------------------------------------------------#
class PyFort(Statement):
  """ Store Fortran module code and methods for export to file.  """
  def __init__(self, header=None, basetype=None, s_tabs=0, arg=''):
    """
    header  : name of module function etc.
    basetype: module, function etc
    arg     : attached to the header at beginning, but not at the end
    s_tabs  : relative indentation
    """
    self.tabs = s_tabs
    self.program_heading = Statement(self.tabs)
    self.declaration = []
    self.statements = []
    self.n_statements = 0
    self.temporary_tab = None
    self.temporary_eq = None

    if (header is None):
      self.program_ending = None
    else:
      if basetype is None:
        if header != "":
          self.program_heading.setStatement(header)
        self.program_ending = None
      else:
        self.program_heading.setStatement(basetype + " " + header + arg)
        self.program_ending = "end " + basetype + " " + header

    Statement.__init__(self, self.tabs)
    self.tabsspace = ''
    for i in range(self.tabs):
      self.tabsspace = self.tabsspace + "  "

#------------------------------------------------------------------------------#

  def addType(self, Type, n_tabs=1):
    """ Add a new type to the module. """
    if (isinstance(Type, list_type)):
      tmp = Statement(n_tabs+self.tabs)
      tmp.setStatement(Type[0][0] + " " + Type[0][1])
      self.declaration.append(tmp)
      for i in range(1, len(Type)):
        tmp = Statement(n_tabs + self.tabs + 1)
        tmp.setStatement(Type[i][0] + " :: " + Type[i][1], True)
        self.declaration.append(tmp)
      tmp = Statement(n_tabs + self.tabs)
      tmp.setStatement("end "+Type[0][0])
      self.declaration.append(tmp)
    else:
      tmp = Statement(n_tabs + self.tabs)
      tmp.setStatement(Type[0] + " :: " + Type[1], True)
      self.declaration.append(tmp)

#------------------------------------------------------------------------------#

  def addPrefix(self, st, n_tabs=1, **kwargs):
    """ Adds instructions before the actual code block. Can be used for
    declaring variables etc. """

    # tabs is the same as n_tabs, kept for compatibility
    if 'tabs' in kwargs:
      tabs = kwargs['tabs']
    else:
      tabs = n_tabs

    if (isinstance(st, list_type)):
      for s in st:
        self.addPrefix(st, tabs=tabs)
    elif isinstance(st, loadBlock):
      for s in st.statements:
        self.declaration.append(s)
    else:
      tmp = Statement(tabs+self.tabs)
      tmp.setStatement(st)
      self.declaration.append(tmp)

  def addLine(self):
    tmp = Statement(0)
    tmp.setStatement('')
    self.statements.append(tmp)

  def addLineD(self):
    """ add line to declaration block """
    tmp = Statement(0)
    tmp.setStatement('')
    self.declaration.append(tmp)


#------------------------------------------------------------------------------#
  def append(self, st):
    """
    Appends a statement instance to pyfort instance to the statements block
    """
    self.statements.append(st)

  def appendD(self, st):
    """ Appends a statement instance to pyfort instance to the declaration block
    """
    self.declaration.append(st)

  def extend(self, sts):
    """ Appends statement instances to pyfort instance to the statements block
    """
    for st in sts:
      self.statements.append(st)

#------------------------------------------------------------------------------#
  def addStatement(self, st, n_tabs=1, treat_as_eq=False, eq=False):
    if self.temporary_tab is None:
      tmp = Statement(n_tabs + self.tabs)
    else:
      tmp = Statement(self.temporary_tab + self.tabs)
      self.temporary_tab = None

    # slowly going over from treat_as_eq to eq
    if self.temporary_eq is None:
      if eq:
        tmp.setStatement(st, eq)
      else:
        tmp.setStatement(st, treat_as_eq)
    else:
      tmp.setStatement(st, self.temporary_eq)
      self.temporary_eq = None

    self.statements.append(tmp)
    self.n_statements += 1

  def addcomment(self, st, tabs=0, prefix=False):
    """ Adds a comment to the statements """
    tmp = Statement(tabs + self.tabs)
    tmp.setStatement(st, comment=True)
    if not prefix:
      self.statements.append(tmp)
      self.n_statements += 1
    else:
      self.declaration.append(tmp)

  def addStatements(self, sts, tabs=1, eq=False):
    # temporary attributes are set to None after applying addStatement
    # the values should apply to all statements.
    stored_temporary_tab = self.temporary_tab
    stored_temporary_eq = self.temporary_eq

    for st in sts:
      self.temporary_tab = self.temporary_tab
      self.temporary_eq = stored_temporary_eq
      self.addStatement(st, n_tabs=tabs, eq=eq)

  def __add__(self, st):
    """
    >>> test = spaceBlock()
    >>> test = (test > 2) + 'hi'
    >>> test = test + ['foo', 'bar']
    >>> st = Statement(0)
    >>> st.setStatement('blub')
    >>> test = test + st
    >>> test.printStatment()
        hi
      foo
      bar
    blub
    """
    if st == '\n':
      self.addLine()
      return self

    cases = {str: self.addStatement,
             list: self.addStatements,
             Statement: self.append,
             PyFort: self.append}

    if type(st) in cases:
      cases[type(st)](st)
    elif issubclass(type(st), PyFort):
      cases[PyFort](st)
    else:
      raise TypeError('Unsupported type ' + str(type(st)) + '. Type must be ' +
                      'string, list of strings or Statement')
    return self

  def __iadd__(self, st):
    """

    >>> test = spaceBlock()
    >>> test = (test >= 0) + ('5 = ' + ' + '.join(['10']*20))
    >>> test.printStatment()
    5 = 10 + 10 + 10 + 10 + 10 + 10 + 10 + 10 + 10 + 10 + 10 + 10 + 10 + 10 + 10 + &
        10 + 10 + 10 + 10 + 10

    >>> test2 = spaceBlock()
    >>> test2 = (test2 > 0)
    >>> test2 += ('5 = ' + ' + '.join(['10']*20))
    >>> test2.printStatment()
    5 = 10 + 10 + 10 + 10 + 10 + 10 + 10 + 10 + 10 + 10 + 10 + 10 + 10 + 10 + 10 + &
        10 + 10 + 10 + 10 + 10

    """
    self.temporary_eq = True
    return self.__add__(st)

  def __gt__(self, tabs):
    assert(type(tabs) is int)
    self.temporary_tab = tabs
    return self

  def __ge__(self, tabs):
    assert(type(tabs) is int)
    self.temporary_tab = tabs
    self.temporary_eq = True
    return self

  def getStatement(self, i):
    if (i < len(self.statements)):
      return self.statements[i]
    else:
      pass

  def getStatements(self):
    tmp = self.n_statements
    return tmp

#------------------------------------------------------------------------------#
  def printDeclaration(self):
    for i in self.declaration:
      for j in i.stmnts:
        print(j)

#------------------------------------------------------------------------------#
  def genOuput(self, File=None):
    if(File is not None):
      if len(self.program_heading.stmnts) > 0:
        self.program_heading.printStatment(File)
    else:
      self.program_heading.printStatment(File)

    # Declarations
    if (len(self.declaration) > 0):
      for i in self.declaration:
        i.printStatment(File)

    # Statements
    if (len(self.statements) > 0):
      for i in self.statements:
        i.printStatment(File)

    # ending
    if(File is not None):
      if self.program_ending is not None:
        File.write(self.tabsspace + self.program_ending + '\n')
    else:
      if self.program_ending is not None:
        print(self.tabsspace + self.program_ending)

  def printStatment(self, File=None):
    self.genOuput(File)

  def genCodeBlock(self, File, Filename, Author, UFOdate, UFOversion):
    """ genCodeBlock builds the commented head of auto-generated files
    with pyfort. It inserts a license and generation information. """
    commentline = self.comment + (self.linelength-2)*'*' + self.comment
    now = datetime.datetime.now()
    now = now.strftime("%Y-%m-%d %H:%M")
    thispath = os.path.dirname(os.path.realpath(__file__))

    # read lincense template, adjust offset and fill slot with filename
    licensepath = os.path.join(thispath, 'license_template.txt')
    with open(licensepath, 'r') as lb:
      licblock = [' '*2 + u[:-1] for u in lb.readlines()]
    licblock[1] = licblock[1] % {'FILENAME': Filename}

    licblockcommented = [self.comment + u + ' ' * (self.linelength - len(u) - 2)
                         + self.comment for u in licblock]

    # read generation template, adjust offset and fill slots with filename and
    # date
    genpath = os.path.join(thispath, 'generation_template.txt')
    with open(genpath, 'r') as gb:
      genblock = [' '*5 + u[:-1] for u in gb.readlines()]

    genblock[5] = genblock[5] % {'FILENAME': Filename}
    genblock[8] = genblock[8] % {'AUTHOR': Author}
    genblock[9] = genblock[9] % {'UFODATE': UFOdate}
    genblock[10] = genblock[10] % {'UFOVERSION': UFOversion}
    genblock[11] = genblock[11] % {'DATE': now}

    genblockcommented = [self.comment + u + ' ' * (self.linelength - len(u) - 2)
                         + self.comment for u in genblock]

    if File is not None:
      File.write(commentline+'\n')
      for i in licblockcommented:
        File.write(i + '\n')
      File.write(commentline+'\n')
      for i in genblockcommented:
        File.write(i + '\n')
      File.write(commentline+'\n')
#------------------------------------------------------------------------------#

#==============================================================================#
#                                   ifBlock                                    #
#==============================================================================#
class ifBlock(PyFort):
  def __init__(self, arg, tabs=0):
    super(ifBlock, self).__init__(header="", basetype="if",
                                  s_tabs=tabs, arg="(" + arg + ") then")

#==============================================================================#
#                                   doBlock                                    #
#==============================================================================#
class doBlock(PyFort):
  def __init__(self, arg, tabs=0):
    super(doBlock, self).__init__(header="", basetype="do",
                                  s_tabs=tabs, arg=arg)

#==============================================================================#
#                                interfaceBlock                                #
#==============================================================================#
class interfaceBlock(PyFort):
  def __init__(self, name, tabs=0):
    super(interfaceBlock, self).__init__(header="", basetype="interface",
                                         s_tabs=tabs, arg=name)

#==============================================================================#
#                                  spaceBlock                                  #
#==============================================================================#
class spaceBlock(PyFort):
  def __init__(self, header="", tabs=0):
    super(spaceBlock, self).__init__(header=header, basetype=None,
                                     s_tabs=tabs, arg='')

#==============================================================================#
#                                 selectBlock                                  #
#==============================================================================#
class selectBlock(PyFort):
  def __init__(self, arg, tabs=0):
    super(selectBlock, self).__init__(header="", basetype="select",
                                      s_tabs=tabs, arg="case(" + arg + ")")

#==============================================================================#
#                                  caseBlock                                   #
#==============================================================================#
class caseBlock(PyFort):
  def __init__(self, arg, tabs=0):
    if arg != 'default':
      super(caseBlock, self).__init__(header="case(" + arg + ")",
                                      basetype=None, s_tabs=tabs, arg=None)
    else:
      super(caseBlock, self).__init__(header="case default",
                                      basetype=None, s_tabs=tabs, arg=None)


#==============================================================================#
#                                   subBlock                                   #
#==============================================================================#
class subBlock(PyFort):

  def __init__(self, name, arg="", tabs=0):
    super(subBlock, self).__init__(header=name, basetype="subroutine",
                                   s_tabs=tabs, arg="(" + arg + ")")

#==============================================================================#
#                                  funcBlock                                   #
#==============================================================================#

class funcBlock(PyFort):

  def __init__(self, name, type=None, result=None, arg="", tabs=0):
    if result is None:
      super(funcBlock, self).__init__(header=name, basetype="function",
                                      s_tabs=tabs, arg="(" + arg + ")")
    else:
      arg_tmp = '(' + arg + ') result(' + result + ')'
      super(funcBlock, self).__init__(header=name, basetype="function",
                                      s_tabs=tabs, arg=arg_tmp)
    if type is not None:
      self.addType((type, name))

#==============================================================================#
#                                   modBlock                                   #
#==============================================================================#
class modBlock(PyFort):
  def __init__(self, name, tabs=0):
    super(modBlock, self).__init__(header=name, basetype="module",
                                   s_tabs=tabs, arg="")

#==============================================================================#
#                                  progBlock                                   #
#==============================================================================#

class progBlock(PyFort):
  def __init__(self, name, tabs=0):
    super(progBlock, self).__init__(header=name, basetype="program",
                                    s_tabs=tabs, arg="")

#==============================================================================#
#                                containsBlock                                 #
#==============================================================================#
class containsBlock(PyFort):
  def __init__(self, tabs=0):
    super(containsBlock, self).__init__(header="contains", basetype=None,
                                        s_tabs=tabs, arg=None)

#==============================================================================#
#                                  loadBlock                                   #
#==============================================================================#
class loadBlock(spaceBlock):
  def __init__(self, filename, tabs=0, prefix=False):
    super(loadBlock, self).__init__(tabs=tabs)
    with open(filename) as f:
      for line in f:
        st = line.lstrip()
        st_tab = (len(line) - len(st))//2
        if not prefix:
          self.addStatement(st, st_tab)
        else:
          self.addPrefix(st, st_tab)

#==============================================================================#
#                                formProcedure                                 #
#==============================================================================#

class formProcedure(spaceBlock):
  def __init__(self, procedure_name, tabs=0):
    super(formProcedure, self).__init__(tabs=tabs)
    self.comment = '*'
    self.addPrefix('#procedure ' + procedure_name, 0)
    self.program_ending = '#endprocedure'

  def addStatement(self, st, tabs=0):
    super(formProcedure, self).addStatement(st, n_tabs=tabs,
                                            treat_as_eq=True)


#==============================================================================#
#                                 ProgressBar                                  #
#==============================================================================#

class ProgressBar():
  """ From:
  http://stackoverflow.com/questions/3173320/text-progress-bar-in-the-console
  """
  DEFAULT_BAR_LENGTH = float(45)
  STATUS_LENGTH = int(10)

  def __init__(self, end, start=0, status=None):
    self.end = end
    self.start = start
    if status is not None:
      if len(status) < ProgressBar.STATUS_LENGTH:
        status += ' ' * (ProgressBar.STATUS_LENGTH - len(status))
      else:
        status = status[:ProgressBar.STATUS_LENGTH]

    self.status = status
    self._barLength = ProgressBar.DEFAULT_BAR_LENGTH

    self.setLevel(self.start)
    self._plotted = False

  def setLevel(self, level):
    """ Set the current progress level. """
    self._level = level
    if level < self.start:
      self._level = self.start
    if level > self.end:
      self._level = self.end

    self._ratio = (float(self._level - self.start) /
                   float(self.end - self.start))
    self._levelChars = int(self._ratio * self._barLength)

  def plotProgress(self):
    """ Plots the current progress and status. """
    status = ''
    if self.status:
      status = ": " + str(self.status)
    sys.stdout.write("\r  %3i%% [%s%s] %s" % (int(self._ratio * 100.0),
                                              '=' * int(self._levelChars),
                     ' ' * int(self._barLength - self._levelChars), status))
    self._plotted = True
    sys.stdout.flush()

  def setAndPlot(self, level):
    """ Set and plot the progress level. """
    oldChars = self._levelChars
    self.setLevel(level)
    if (not self._plotted) or (oldChars != self._levelChars):
        self.plotProgress()

  def advanceAndPlot(self, status=None):
    """ Advance the level by and plot. In addition, the status can be updated.
    """
    new_status = self.status != status
    if new_status:
      if status is not None:
        if len(status) < ProgressBar.STATUS_LENGTH:
          status += (' ' * (ProgressBar.STATUS_LENGTH - len(status)))
        else:
          status = status[:ProgressBar.STATUS_LENGTH]
    self.status = status
    oldChars = self._levelChars
    self.setLevel(self._level+1)
    if (not self._plotted) or (oldChars != self._levelChars) or new_status:
        self.plotProgress()

  def __del__(self):
    sys.stdout.write("\n")

if __name__ == "__main__":
  import doctest
  doctest.testmod()
