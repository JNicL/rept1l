      if (regSoft.eq.1) then
        call setmuir2(muIR**2)
      else
        call setmuir2(lambda**2)
      endif
      call setdeltauv(DeltaUV)
      call setdeltair(DeltaIR,DeltaIR2)

      call clearminf2
      do i = 1,n_particles
        p = get_particle(i)
        if (p%reg_id .eq. 2 .and. p%is_particle)  then
          call addminf2(p%cmass2_reg)
        endif
      enddo
