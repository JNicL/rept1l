  subroutine set_complex_mass_scheme_mdl

    ! This subroutine (re-)enables the complex mass scheme (hep-ph/0505042)

    complex_mass_scheme = .true.

  end subroutine set_complex_mass_scheme_mdl

  subroutine set_on_shell_scheme_mdl

    ! This subroutine disables the complex mass scheme

    complex_mass_scheme = .false.

  end subroutine set_on_shell_scheme_mdl
