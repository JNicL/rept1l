    function requiresColorflow(particles_in, particle_out)                     &
    result(requires_colorflow) 
      integer, dimension(:) :: particles_in
      integer :: particle_out
      logical :: requires_colorflow
      requires_colorflow = .false.
      if (size(particles_in) .eq. 2) then
        requires_colorflow = (particles_in(1).eq.P_g) .and.                    &
                             (particles_in(2).eq.P_g) .and.                    &
                             (particle_out.eq.P_g) 
      else if (size(particles_in) .eq. 3)then
        requires_colorflow = (particles_in(1) .eq. P_g) .and. (particles_in(2) &
                             .eq. P_g) .and. (particles_in(3) .eq. P_g).and.   &
                             (particle_out .eq. P_g) 
      end if
    end function requiresColorflow
