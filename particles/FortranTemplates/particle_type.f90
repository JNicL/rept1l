    function get_particle_type_mdl(particle_in) result(particle_type)
      integer, intent(in) :: particle_in
      character(2) :: particle_type
      select case (get_particle_spin_mdl(particle_in))
      ! ghost
      case(-1)
        select case (is_particle_mdl(particle_in))
        ! ghost
        case (.true.)
          particle_type = 'x'
        ! antighost
        case (.false.)
          particle_type = 'x~'
        end select
      ! scalar
      case (1)
        particle_type = 's'
      ! fermion
      case (2)
        select case (is_particle_mdl(particle_in))
        ! fermion
        case (.true.)
          particle_type = 'f'
        ! antifermion
        case (.false.)
          particle_type = 'f~'
        end select
      ! vector
      case (3)
        particle_type = 'v'
      case default
        particle_type = '0'
      end select
    end function get_particle_type_mdl

    function get_particle_type2_mdl(particle_in) result(particle_type)
      integer, intent(in) :: particle_in
      character(2) :: particle_type
      select case (get_particle_spin_mdl(particle_in))
      ! ghost
      case(-1)
        select case (is_particle_mdl(particle_in))
        ! ghost
        case (.true.)
          select case (get_particle_colour_mdl(particle_in) .ne. 1)
          ! colored ghost
          case (.true.)
            particle_type = 'G'
          ! ghost
          case (.false.)
            particle_type = 'x'
          end select
        ! antighost
        case (.false.)
          select case (get_particle_colour_mdl(particle_in) .ne. 1)
          ! colored antighost
          case (.true.)
            particle_type = 'G~'
          ! antighost
          case (.false.)
            particle_type = 'x~'
          end select
        end select
      ! scalar
      case (1)
        particle_type = 's'
      ! fermion
      case (2)
        select case (is_particle_mdl(particle_in))
        ! fermion
        case (.true.)
          select case (get_particle_colour_mdl(particle_in) .ne. 1)
          ! quark
          case (.true.)
            particle_type = 'q'
          ! fermion
          case (.false.)
            particle_type = 'f'
          end select
        ! antifermion
        case (.false.)
          select case (get_particle_colour_mdl(particle_in) .ne. 1)
          ! antiquark
          case (.true.)
            particle_type = 'q~'
          ! antifermion
          case (.false.)
            particle_type = 'f~'
          end select
        end select
      ! vector
      case (3)
        select case (get_particle_colour_mdl(particle_in) .ne. 1)
        ! gluon
        case (.true.)
          particle_type = 'g'
        ! vector
        case (.false.)
          particle_type = 'v'
        end select
      case default
        particle_type = '0'
      end select
    end function get_particle_type2_mdl

    function get_axodraw_propagator_mdl(particle_type) result(axoprop)
      character(2), intent(in) :: particle_type
      character(20) :: axoprop
      select case (particle_type)
        case ('g')
          axoprop = '\Gluon'
        case ('v')
          axoprop = '\Photon'
        case ('f', 'f~')
          axoprop = '\ArrowLine'
        case ('q', 'q~')
          axoprop = '\ArrowLine'
        case ('x', 'x~')
          axoprop = '\DashArrowLine'
        case ('G', 'G~')
          axoprop = '\DashLine'
        case ('s')
          axoprop = '\DashLine'
        case default
          write(*,*) "particle_type:", particle_type
          write (*, *) 'Case not implemented in axodraw_propagator'
          stop
      end select
    end function get_axodraw_propagator_mdl
