    function sec(a) result(b)
      complex(kind=dp), intent(in) :: a
      complex(kind=dp) :: b
      b = 1/cos(a)
    end function sec 

    function csc(a) result(b)
      complex(kind=dp), intent(in) :: a
      complex(kind=dp) :: b
      b = 1/sin(a)
    end function csc

    function cot(a) result(b)
      complex(kind=dp), intent(in) :: a
      complex(kind=dp) :: b
      b = 1/tan(a)
    end function cot

    function conjugate(a) result(b)
      complex(kind=dp), intent(in) :: a
      complex(kind=dp) :: b
      b = dconjg(a)
    end function conjugate

    function get_modelname_mdl() result(ret_modelname)
      character(len=100) :: ret_modelname
      ret_modelname = modelname
    end function get_modelname_mdl

    function get_modelgauge_mdl() result(ret_modelgauge)
      character(len=100) :: ret_modelgauge
      ret_modelgauge = modelgauge
    end function get_modelgauge_mdl

    function get_driver_timestamp_mdl() result(ret_driver_timestamp)
      character(len=200) :: ret_driver_timestamp
      ret_driver_timestamp = driver_timestamp
    end function get_driver_timestamp_mdl

    function get_cmass2_mdl(m_id) result(ret_cmass2)
      integer, intent(in) :: m_id
      complex(kind=dp) :: ret_cmass2
      ret_cmass2 = cmass2(m_id)
    end function get_cmass2_mdl

    function get_particle_cmass2_mdl(p_id) result(ret_cmass2)
      integer, intent(in) :: p_id
      integer :: m_id
      complex(kind=dp) :: ret_cmass2
      m_id = get_particle_mass_id_mdl(p_id)
      ret_cmass2 = cmass2(m_id)
    end function get_particle_cmass2_mdl

    function get_cmass2_reg_mdl(m_id) result(ret_cmass2_reg)
      integer, intent(in) :: m_id
      complex(kind=dp) :: ret_cmass2_reg
      ret_cmass2_reg = cmass2_reg(m_id)
    end function get_cmass2_reg_mdl

    function get_particle_cmass2_reg_mdl(p_id) result(ret_cmass2_reg)
      integer, intent(in) :: p_id
      integer :: m_id
      complex(kind=dp) :: ret_cmass2_reg
      m_id = get_particle_mass_id_mdl(p_id)
      ret_cmass2_reg = cmass2_reg(m_id)
    end function get_particle_cmass2_reg_mdl

    function get_n_masses_mdl() result(ret_n_masses)
      integer :: ret_n_masses
      ret_n_masses = n_masses
    end function get_n_masses_mdl

    function get_n_particles_mdl() result(ret_n_particles)
      integer :: ret_n_particles
      ret_n_particles = n_particles
    end function get_n_particles_mdl

    function get_order_id_mdl(o_id) result(ret_order)
      integer, intent(in) :: o_id
      character(len=3) :: ret_order
      ret_order = order_ids(o_id)
    end function get_order_id_mdl

    function get_n_orders_mdl() result(ret_n_orders)
      integer :: ret_n_orders
      ret_n_orders = size(order_ids)
    end function get_n_orders_mdl

    function get_max_polarizations_mdl() result(ret_max_polarizations)
      integer :: ret_max_polarizations
      ret_max_polarizations = max_polarizations
    end function get_max_polarizations_mdl

    function get_particle_polarizations_mdl(p) result(npolarizations)
      integer, intent(in) :: p
      integer :: npolarizations
      character(len=3) :: p_str
      select case(get_particle_mass_reg_mdl(p))
      case(1,2)
        select case(get_particle_spin_mdl(p))
        case(-1,1)
          npolarizations = 1
        case(2,3)
          npolarizations = 2
        case default
          write(p_str,'(i3)') p
          call error_mdl("Spin other than -1,1,2,3 not supported. P_id:" // &
                         trim(p_str), where='get_particle_polarizations_mdl')
        end select 
      case(3)
        select case(get_particle_spin_mdl(p))
        case(-1,1)
          npolarizations = 1
        case(2)
          npolarizations = 2
        case(3)
          npolarizations = 3
        case default
          write(p_str,'(i3)') p
          call error_mdl("Spin other than -1,1,2,3 not supported. P_id:" // &
                         trim(p_str), where='get_particle_polarizations_mdl')
          stop
        end select 
      case default
        write(p_str,'(i3)') p
        call error_mdl("Mass reg cannot be other than 1,2 or 3. P_id:" // &
                       trim(p_str), where='get_particle_polarizations_mdl')
        stop
      end select 
    end function get_particle_polarizations_mdl

    function is_particle_model_init_mdl() result(ret_init)
      logical :: ret_init
      ret_init = particle_model_init
    end function is_particle_model_init_mdl

    subroutine set_particle_model_init_mdl(set_init)
      logical, intent(in) :: set_init
      particle_model_init = set_init
    end subroutine set_particle_model_init_mdl

    subroutine clear_particles_mdl
      if (allocated(cmass2_reg)) deallocate(cmass2_reg)
      if (allocated(cmass2_reg)) deallocate(cmass2_reg)
      particle_model_init = .false.
    end subroutine clear_particles_mdl

    subroutine set_masscut_mdl (m)
      ! Sets mass limit below which particles are considered massless.
      real(dp), intent(in) :: m

      masscut = m
      if (is_particle_model_init_mdl()) then
        call warning_mdl('Masscut needs to be set before defining processes.',&
                         where='set_masscut_mdl')
      end if

    end subroutine set_masscut_mdl

    subroutine set_light_particle_mdl(p)
      character(*), intent(in) :: p
      call set_light_particle_value_mdl(p, .true.)
    end subroutine set_light_particle_mdl

    subroutine unset_light_particle_mdl(p)
      character(*), intent(in) :: p
      call set_light_particle_value_mdl(p, .false.)
    end subroutine unset_light_particle_mdl

    subroutine set_resonant_particle_mdl(p)
      character(*), intent(in) :: p
      call set_resonant_particle_value_mdl(p, .true.)
    end subroutine set_resonant_particle_mdl

    subroutine unset_resonant_particle_mdl(p)
      character(*), intent(in) :: p
      call set_resonant_particle_value_mdl(p, .false.)
    end subroutine unset_resonant_particle_mdl
