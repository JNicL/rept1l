#==============================================================================#
#                             resonant_particle.py                             #
#==============================================================================#

#============#
#  Includes  #
#============#

from rept1l.pyfort import subBlock, ifBlock, selectBlock, caseBlock, funcBlock
from rept1l.helper_lib import export_particle_name
from .masses import ParticleMass
import rept1l.Model as Model
model = Model.model

#===========#
#  Methods  #
#===========#

def gen_set_resonant_particle_value(tabs=2):
  """ Generates the Fortran routine to set particles resonant. """
  srp = subBlock('set_resonant_particle_value_mdl', arg='p,val', tabs=tabs)
  srp.addType(('character(*), intent(in)', 'p'))
  srp.addType(('logical, intent(in)', 'val'))

  # Setting particles resonant must be done before generating the processes.
  if_pminit = ifBlock('particle_model_init', tabs=tabs+1)
  sts = ("call warning_mdl('Resonances need to be set before " +
         "defining processes.'" +
         ", where='set_resonant_particle_value_mdl')")
  (if_pminit > 1) + sts

  srp + if_pminit
  sc = selectBlock('p', tabs=tabs+1)
  srp + sc

  resonances = ParticleMass.resonances
  all_particles = model.object_library.all_particles
  # potential resonances are identified by widths (no width no resonance possible)
  for particle in all_particles:
    if hasattr(particle, 'width') and particle.width.name in resonances:
      p_name = particle.name
      res = resonances[particle.width.name]
      case = caseBlock('"' + p_name + '"', tabs=tabs+2)
      case + (res + ' = val')
      sc + case

  cd = caseBlock('default', tabs=3)
  sts = ("call warning_mdl('Trying to set resonance for non-supported " +
         "particle: ' // trim(p), where='set_resonant_particle_value_mdl')")
  cd + sts
  sc + cd
  return srp

#------------------------------------------------------------------------------#

def gen_is_resonant_particle_id(tabs=2):
  """ Generates the Fortran function to get resonance status of a particle. """
  srp = funcBlock('is_resonant_particle_id_mdl', arg='p',
                  result='is_resonant_particle', tabs=tabs)
  srp.addType(('integer, intent(in)', 'p'))
  srp.addType(('logical', 'is_resonant_particle'))

  sc = selectBlock('p', tabs=tabs+1)
  srp + sc

  resonances = ParticleMass.resonances
  all_particles = model.object_library.all_particles

  # resonances are identified by widths (no width no resonance possible)
  for particle in all_particles:
    if hasattr(particle, 'width') and particle.width.name in resonances:
      p_name = 'P_' + export_particle_name(particle.name)
      res = resonances[particle.width.name]
      case = caseBlock(p_name, tabs=tabs+1)
      case + ('is_resonant_particle = ' + res)
      sc + case

  cd = caseBlock('default', tabs=3)
  sts = ("call warning_mdl('Trying to determine resonance status for " +
         "non-support particle id.', where='is_resonant_particle_id_mdl')")
  cd + sts
  cd + ('is_resonant_particle = .false.')
  sc + cd
  return srp
