#==============================================================================#
#                                  masses.py                                   #
#==============================================================================#

#============#
#  Includes  #
#============#

import os
from six import with_metaclass
from rept1l.helper_lib import StorageProperty
from rept1l.pyfort import subBlock, ifBlock, funcBlock, spaceBlock, selectBlock
from rept1l.pyfort import caseBlock
from rept1l.logging_setup import log
from rept1l.helper_lib import export_particle_name, is_anti_particle
from rept1l.helper_lib import export_fortran
from rept1l.counterterms import get_particle_ct
from .defaultparamvalues import default_values
import rept1l.Model as Model
model = Model.model

#===========#
#  Methods  #
#===========#

#==============================================================================#
#                                 complex_mass                                 #
#==============================================================================#

def squared_mass(particle, force_zerowidth=False, on_shell=False):
  # Stable particle
  parameters = model.param
  if particle.width == parameters.ZERO or force_zerowidth:

    particle_mass_zero = (particle.mass == parameters.ZERO or
                          particle.mass.value == 0)
    # massless particle
    if particle_mass_zero:
      cmass = "cnul"
    else:
      cmass = "cmplx(" + particle.mass.name + "**2, 0d0,kind=dp)"
  # Massive & unstable
  else:
    if on_shell:
      cmass = ("(" + particle.mass.name + "**2 - cima*" + particle.width.name +
               "*" + particle.mass.name + ")/(1d0 + " + particle.width.name +
               "**2 /" + particle.mass.name + "**2)")
    # pole mass
    else:
      cmass = (particle.mass.name + "**2 - cima*" + particle.width.name + "*" +
               particle.mass.name)
  return cmass

#==============================================================================#
#                                gen_mass_name                                 #
#==============================================================================#

def gen_mass_name(tabs=2):
  """ Generates the function returning the particle's mass name given the
  particle's mass id.
  """
  function_mass_name = funcBlock('get_mass_name_mdl',
                                 arg='mass_id',
                                 result='mass_name',
                                 tabs=tabs)
  function_mass_name.addType(('integer, intent(in)', 'mass_id'))
  function_mass_name.addType(('character(len=6)', 'mass_name, mass_id_str'))

  all_particles = model.model_objects.all_particles
  parameters = model.param

  if_mass_id = ifBlock('mass_id .le. 2', tabs=tabs+1) + 'mass_name = "M0"'
  masses_regged = []
  for p_index, particle in enumerate(all_particles):
    particle_mass_zero = (particle.mass == parameters.ZERO or
                          particle.mass.value == 0)
    mass_name = particle.mass.name
    if (not particle_mass_zero) and (mass_name not in masses_regged):
      masses_regged.append(particle.mass.name)
      new_if = ('else if(mass_id .eq. get_particle_mass_id_mdl(' +
                str(p_index+1) + ')) then')
      new_case = 'mass_name = "' + mass_name + '"'
      (if_mass_id > 0) + new_if
      if_mass_id += new_case

  # last case particle not found
  case_default = 'else'
  (if_mass_id > 0) + case_default

  if_mass_id + "write(mass_id_str,'(i3)') mass_id"
  sts = ('call error_mdl("Mass id " // trim(mass_id_str) // " not defined.", ' +
         'where="get_mass_name_mdl")')

  if_mass_id + sts

  function_mass_name += if_mass_id
  return function_mass_name

#------------------------------------------------------------------------------#

def gen_get_particle_width():
  """ Generates the function returning the particle's width value given the
  particle's id.
  """
  function_get_particle_width = funcBlock('get_particle_width_mdl', arg='p',
                                          result='pwidth', tabs=2)
  function_get_particle_width.addType(('integer, intent(in)', 'p'))
  function_get_particle_width.addType(('complex(kind=dp)', 'pwidth'))

  all_particles = model.model_objects.all_particles
  select_particle = selectBlock('p', tabs=3)
  function_get_particle_width + select_particle

  masses_regged = {}
  for particle in all_particles:
    particle_width_zero = (particle.width == model.param.ZERO or
                           particle.width.value == 0)
    if not particle_width_zero:
      if particle.mass.name not in masses_regged:
        masses_regged[particle.mass.name] = particle.width.name
      elif masses_regged[particle.mass.name] != particle.width.name:
        raise Exception('Particles with same mass but different ' +
                        'widths detected: ' + particle.name)

  for particle in all_particles:
    if particle.mass.name in masses_regged:
      p_name = 'P_' + export_particle_name(particle.name)
      case = caseBlock(p_name, tabs=3)
      select_particle + case
      case + ('pwidth = ' + masses_regged[particle.mass.name])

  case_default = caseBlock('default', tabs=3)
  select_particle + case_default
  case_default + 'pwidth = cnul'

  return function_get_particle_width

#------------------------------------------------------------------------------#

def gen_get_particle_mass_id():
  """ Generates the function returning the particle_mass_id given the particle
  id.  """
  function_particle_mass_id = funcBlock('get_particle_mass_id_mdl', arg='p',
                                        result='pmassid', tabs=2)
  function_particle_mass_id.addType(('integer, intent(in)', 'p'))
  function_particle_mass_id.addType(('integer', 'pmassid'))

  all_particles = model.model_objects.all_particles
  select_particle = selectBlock('p', tabs=3)

  function_particle_mass_id + select_particle

  for p_index, particle in enumerate(all_particles):

    p_name = 'P_' + export_particle_name(particle.name)
    case = caseBlock(p_name, tabs=3)
    select_particle + case

    particle_mass_zero = (particle.mass == model.param.ZERO or
                          particle.mass.value == 0)
    if not particle_mass_zero:

      # regulator mass id (always 'n' + mass name)
      particle_mass_id_name = 'n' + particle.mass.name

      case + ('pmassid = ' + particle_mass_id_name)

    # massless case
    else:
      case + 'pmassid = 1'

  return function_particle_mass_id

#------------------------------------------------------------------------------#

def gen_get_particle_mass_reg():
  """ Generates the function returning the mass_reg given the particle id.  """
  function_particle_mass_reg = funcBlock('get_particle_mass_reg_mdl', arg='p',
                                         result='pmassreg', tabs=2)
  function_particle_mass_reg.addType(('integer, intent(in)', 'p'))
  function_particle_mass_reg.addType(('integer', 'pmassreg'))

  all_particles = model.model_objects.all_particles
  select_particle = selectBlock('p', tabs=3)
  function_particle_mass_reg + select_particle

  for p_index, particle in enumerate(all_particles):

    p_name = 'P_' + export_particle_name(particle.name)
    case = caseBlock(p_name, tabs=3)
    select_particle + case

    particle_mass_zero = (particle.mass == model.param.ZERO or
                          particle.mass.value == 0)
    if not particle_mass_zero:

      # real mass regulator
      particle_mass_reg_name = particle.mass.name + '_reg'

      case + ('pmassreg = ' + particle_mass_reg_name)

    # massless case
    else:
      case + 'pmassreg = 1'

  return function_particle_mass_reg

#==============================================================================#
#                            fill_particles_masses                             #
#==============================================================================#

def fill_particles_masses(class_particles, overwrite_default_params=True):
  """ Fills the class particles witht the mass & width values.

  :param overwrite_default_params: Overwrite default UFO parameters values with
                                   the ones defined in defaultparamvalues.
  :type overwrite_default_params: bool
  """

  cp = class_particles

  nparticles = len(model.model_objects.all_particles)

  particle_antiparticle_dict = dict()
  pad = particle_antiparticle_dict

  masses = []
  widths = []
  complex_mass_index = 2

  function_init_particles = subBlock("init_particles_mdl", tabs=2)
  fip = function_init_particles
  fip + 'call init_cmasses_mdl'

  function_init_prop_masses = subBlock('init_cmasses_mdl', tabs=2)
  fim = function_init_prop_masses
  init_masses_widths = spaceBlock(tabs=2)
  fim += init_masses_widths

  parameters = model.param
  all_particles = model.model_objects.all_particles
  all_parameters = model.model_objects.all_parameters
  for particle in all_particles:

    # renaming particle due to possible ambiguous definition with parameters
    p_name = 'P_' + export_particle_name(particle.name)
    p_antiname = 'P_' + export_particle_name(particle.antiname)
    pad[p_name] = p_antiname

    particle_mass_zero = (particle.mass == parameters.ZERO or
                          particle.mass.value == 0)
    if particle_mass_zero:
      mass_name = 'ZERO'
    else:
      mass_name = particle.mass.name

    p_index = all_particles.index(particle) + 1

    ptype = p_name + " = " + str(p_index)
    description = ("Particle:" + particle.texname +
                   ", pdg_code:" + str(particle.pdg_code))
    cp.addcomment(description, prefix=True)
    cp.addType(("integer, parameter", ptype))

    is_particle = ".false."
    is_particle = not is_anti_particle(particle.name)
    if is_particle:
      is_particle = ".true."
    else:
      is_particle = ".false."

    mr = ParticleMass(particle, fim)

  masses = list(ParticleMass.masses.values())
  widths = list(ParticleMass.widths.values())
  resonances = list(ParticleMass.resonances.values())
  init_masses_widths.addcomment("Init mass squared parameters")

  # Initialize squared mass values
  for mass in masses:
    # If the mass depends on external parameters it has to be initialized first
    # -> e.g. if a different mass instance is used for goldstone bosons which
    # depends on the gauge bosons
    if mass.nature == 'internal':
      st = mass.name + " = " + export_fortran(str(mass.value))
      init_masses_widths + st
    st2 = mass.name + "2 = " + mass.name + '**2'
    init_masses_widths + st2

  #==================#
  #  mass variables  #
  #==================#

  # The mass variables of real type are only used o set the value of masses. In
  # expressions the real valued masses are replaced by the complex valued.
  masses_names = [u.name for u in masses]

  if(overwrite_default_params and
     hasattr(model, 'modelname') and
     model.modelname in default_values):
    mn = model.modelname
    mass_params = [u.name + ' = ' + default_values[mn][u.name]
                   if u.nature == 'external' and u.name in default_values[mn]
                   else u.name + ' = ' + export_fortran(str(u.value))
                   if u.nature == 'external' else u.name for u in masses]
  else:
    mass_params = [u.name + ' = ' + export_fortran(str(u.value))
                   if u.nature == 'external' else u.name
                   for u in masses]
  mass2_params = ', '.join(u.name + '2' for u in masses)

  mass_id_params = ', '.join("n" + u.name for u in masses)
  mass_reg_params = ', '.join(u.name + '_reg' for u in masses)

  #==========================#
  #  complex mass variables  #
  #==========================#

  # The complex valued couplings are encountered everywhere a normal mass is
  # expected as a consequence of the complex mass scheme. In addition in special
  # situation one can have the real mass (of complex type) in expressions.

  rmasses_names = ['r' + u.name for u in masses]
  rmasses_parameter = ', '.join(rmasses_names)
  rmasses2_names = ['r' + u.name + '2' for u in masses]
  rmasses2_parameter = ', '.join(rmasses2_names)

  cmasses_names = ['c' + u.name for u in masses]
  cmasses_names_reg = [u + '_reg' for u in cmasses_names]
  cmasses_parameter = ', '.join(cmasses_names)
  cmasses_parameter_reg = ', '.join(cmasses_names_reg)

  cmasses2_names = ["c" + u.name + '2' for u in masses]
  cmasses2_names_reg = [u + "_reg" for u in cmasses2_names]
  cmasses2_parameter = ', '.join(cmasses2_names)
  cmasses2_parameter_reg = ', '.join(cmasses2_names_reg)

  init_masses_widths.addLine()
  init_masses_widths.addcomment("Start with mass Ids and complex masses")
  init_masses_widths.addcomment('The mass ids are given by their order in UFO')
  init_masses_widths + 'n_masses = 2'

  fim.addLine()
  fim + 'if (allocated(cmass2)) deallocate(cmass2)'
  fim + 'allocate(cmass2(n_masses))'
  fim + 'if (allocated(cmass2_reg)) deallocate(cmass2_reg)'
  fim + 'allocate(cmass2_reg(n_masses))'
  fim + 'cmass2(1:2) = cnul'
  fim + 'cmass2_reg(1:2) = cnul'

  for particle in ParticleMass.complex_mass_indices:
    cmass = ParticleMass.complex_mass_indices[particle]
    fim + cmass[0]
    fim + cmass[1]

  fim + '\n'
  for cmass in cmasses_names:
    fim + (cmass + ' = ' + 'sqrt(' + cmass + '2)')
    fim + (cmass + '_reg = ' + 'sqrt(' + cmass + '2_reg)')

  fim + '\n'
  for rmass in rmasses_names:
    fim + (rmass + '2 = cmplx(real(c' + rmass[1:] + '2),0d0,kind=dp)')
    fim + (rmass + ' = sqrt(' + rmass + '2)')

  fim + '\n'
  fim + 'call init_parameters_mdl()'

  fim + '\n'

  if any(len(u) > 0 for u in mass_params):
    cp.addLineD()
    cp.addcomment('Masses and widths', prefix=True)
    for mass_param in mass_params:
      cp.addType(("real(kind=dp)", mass_param))

  if(overwrite_default_params and
     hasattr(model, 'modelname') and
     model.modelname in default_values):
    mn = model.modelname
    width_parameters = [u.name + ' = ' + default_values[mn][u.name]
                        if u.name in default_values[mn]
                        else u.name + ' = ' + export_fortran(str(u.value))
                        for u in widths]
  else:
    width_parameters = [u.name + ' = ' + export_fortran(str(u.value))
                        for u in widths]
  for width_parameter in width_parameters:
    cp.addType(("real(kind=dp)", width_parameter))

  if len(mass2_params) > 0:
    cp.addLineD()
    cp.addType(("real(kind=dp)", mass2_params))

  if len(mass_id_params) > 0:
    cp.addcomment("Unique mass ids", prefix=True)
    cp.addType(("integer", mass_id_params))

  if len(mass_reg_params) > 0:
    cp.addcomment("Mass regularization", prefix=True)
    cp.addType(("integer", mass_reg_params))

  cp.addcomment("The number of distinct mass regulators.", prefix=True)
  cp.addType(("integer", "n_masses"))
  if len(cmasses_parameter) > 0:
    cp.addcomment("Complex mass parameters", prefix=True)
    cp.addType(("complex(kind=dp)", cmasses_parameter))
    cp.addType(("complex(kind=dp)", cmasses_parameter_reg))
    cp.addType(("complex(kind=dp)", cmasses2_parameter))
    cp.addType(("complex(kind=dp)", cmasses2_parameter_reg))
    cp.addType(("complex(kind=dp)", rmasses_parameter))
    cp.addType(("complex(kind=dp)", rmasses2_parameter))

  if any(len(u) > 0 for u in resonances):
    cp.addLineD()
    cp.addcomment('Resonances', prefix=True)
    for res in resonances:
      cp.addType(("logical", res + ' = .false.'))

  if len(ParticleMass.massreg_massct) > 0:
    cp.addLineD()
    cp.addcomment('Light particles', prefix=True)
    for mreg in ParticleMass.massreg_massct:
      cp.addType(("logical", mreg + '_light = .false.'))

  max_polarizations = ParticleMass.max_polarizations

  max_polarizations_description = ("Maximal helicity configurations. " +
                                   "Determined by the particle with " +
                                   "highest spin  e.g. massive VB  -> 3")
  cp.addLineD()
  cp.addcomment(max_polarizations_description, prefix=True)
  cp.addType(("integer, parameter",
              "max_polarizations = " + str(max_polarizations)))

  nparticles = len(model.model_objects.all_particles)
  ptype = 'integer, dimension(' + str(nparticles) + ')'

  return (cp, fip, fim, pad, masses, widths)

#==============================================================================#
#                              class ParticleMass                              #
#==============================================================================#

class ParticleMass(with_metaclass(StorageProperty)):
  """ The class derives the number of polarizations, the complex
  masses and the regularization type for each particle.
  The results are stored with help of the Storage class.

  RECOLA currently supports
    -dimensional regularization: nM = 1
    -mass cutoff: nM = 2
  where nM is the mass id. Massive particles have nM > 2, whereas massless
  particles have nM = 1 or 2
  """

  # StorageMeta attributes. Those attributes are stored when `store` is called.
  storage = ['masses', 'widths', 'resonances', 'complex_mass_indices',
             'yukawa', 'zero_parameters', 'massreg_massct']
  # StorageMeta storage path
  mpath, _ = model.get_modelpath()
  path = os.path.join(mpath, 'ModelMasses')
  # StorageMeta filename
  name = 'masses_data.txt'

  max_polarizations = 1

  # particle spins
  Ghost = -1
  Scalar = 1
  Fermion = 2
  Vector = 3

  # spin -> polarization
  spin_polarization = {Ghost: {'massless': 1, 'massive': 1},
                       Scalar: {'massless': 1, 'massive': 1},
                       Vector: {'massless': 2, 'massive': 3},
                       Fermion: {'massless': 2, 'massive': 2}}

  mass_regularization = {Ghost: '1', Scalar: True,
                         Vector: None, Fermion: True}

  parameters = model.param
  all_parameters = model.object_library.all_parameters

  @classmethod
  def fill_masses(cls):
    all_particles = model.model_objects.all_particles
    for particle in all_particles:
      ParticleMass(particle)

  def __init__(self, particle, fip=None, tabs=1):
    cls = self.__class__
    if particle.spin not in cls.spin_polarization:
      raise Exception('Particle spin unknown: ' + str(particle.spin))

    # The particle is declared as massless if the parameter is equal to the ZERO
    # parameter or if the mass is set to 0 in the UFO modelfile
    particle_mass_zero = (particle.mass == self.parameters.ZERO or
                          particle.mass.value == 0)

    # Check if this mass needs to be registered
    if particle.mass not in cls.masses.values() and not particle_mass_zero:

      # determine number of polarizations
      self.polarizations = cls.spin_polarization[particle.spin]['massive']
      if fip:
        fip.addcomment('Mass representative: ' + particle.name)

      # introduce complex masses
      cmass_name = 'c' + particle.mass.name + '2'
      cls.masses[particle.name] = particle.mass
      mass_index = self.all_parameters.index(particle.mass)

      # introduce finite width if a width is assigned to the particle
      if particle.width != self.parameters.ZERO:
        cls.widths[particle.name] = particle.width
        width_name = particle.width.name
      else:
        width_name = 'cnul'

      # Unstable particle can be resonant if it has assigned a non-zero width
      if particle.width != self.parameters.ZERO:
        res_name = export_particle_name(particle.name, cut_suffix=True) + '_res'
        cls.resonances[particle.width.name] = res_name
      else:
        res_name = '.false.'

      # introduce regulator mass id and masses

      # regulator mass id (always 'n' + mass name)
      mass_id_name = 'n' + particle.mass.name
      # real mass regulator
      mass_reg_name = particle.mass.name + '_reg'
      # complex mass regulator
      cmass_name_reg = cmass_name + '_reg'

      # construct the complex mass given the real mass and width and spin
      cmass = squared_mass(particle)

      # Particle has been tagged to be potentially mass/dimensionally
      # regularized.
      if hasattr(particle, 'light_particle') and particle.light_particle:

        # In the following the mass regularization is connected to the masss and
        # wavefunction counterterms. For different values of the mass and
        # masscut different renormalization scheme is used automatically.
        # (See ctparamter.py : set_light_particles)
        mct = get_particle_ct(particle, wavefunction=False, mass=True)
        if len(mct) == 0:
          log('Particle ' + particle.name + ' has no mass counterterm ' +
              'assigned. Tagging as light particle has no effect.', 'warning')
        else:
          assert(len(mct) == 1)
          mct = mct[0]
          cls.massreg_massct[mass_reg_name] = {'mass_ct': mct.name,
                                               'particle_name': particle.name}
          wcts = get_particle_ct(particle, wavefunction=True, mass=False,
                                 only_diagonal=True)
          # In the case of the fermions one can have more than one diagonal
          # wavefunction
          for n, wct in enumerate(wcts):
            cls.massreg_massct[mass_reg_name]['wave_ct' + str(n)] = wct.name

      # Allow for mass regularization for scalars and fermions
      # If scalars/fermions have a mass below the masscut  or tagged as light
      # with a non-vanishing mass they are mass regularized. In case the mass
      # is zero they are dimensionally regularized.

      if cls.mass_regularization[particle.spin]:
        # determine if the particles mass is below the zeromass limit
        mass_name = particle.mass.name
        cond = particle.mass.name + ' .lt. masscut'
        if mass_name + '_reg' in cls.massreg_massct:
          cond += ' .or. ' + mass_name + '_reg_light'
        mass_id = ifBlock(cond, tabs=tabs+2)
        zerom = ifBlock(particle.mass.name + ' .eq. 0d0', tabs=tabs+3)
        mass_id + zerom
        # dimensional regularization -> all masses zero, id = 1
        zerom + (mass_id_name + ' = 1')
        zerom + (mass_reg_name + ' = 1')
        zerom + (cmass_name + ' = cnul')
        zerom + (cmass_name_reg + ' = cnul')
        (zerom > 0) + 'else'
        # mass regularization -> masses zero, but nonzero regulator mass
        # id = 1, increase nonzero mass counter
        zerom + (mass_reg_name + ' = 2')
        zerom + ('n_masses = n_masses + 1')
        zerom + (mass_id_name + ' = n_masses')
        zerom + (cmass_name + " = cnul")
        zerom + (cmass_name_reg + " = " + cmass)
        if particle.width.name in cls.resonances:
          w = particle.width.name
          fwerror = ifBlock('abs(' + w + ') .gt. 0d0', tabs=tabs+4)
          fwerror + ("call error_mdl('Finite width " + w + ">0 not tested in " +
                     "massreg.', where='init_cmasses_mdl')")
          zerom + fwerror
        mass_id.addStatement('else', 0)
        # particle massive -> nonzero masses, regulator = complexmass
        mass_id + (mass_reg_name + ' = 3')
        mass_id.addStatement('n_masses = n_masses + 1', 1)
        mass_id.addStatement(mass_id_name + ' = n_masses', 1)

        # if the particle is tagged as resonant the pole approximation requires
        # to put all widths to zero in couplings and complex masses. The width
        # in the resonant propagators is kept in RECOLA.
        if particle.width.name in cls.resonances:
          r = ifBlock(cls.resonances[particle.width.name] + ' .eqv. .false.',
                      tabs=4)
          r + (cmass_name + " = " + cmass)
          r + (cmass_name_reg + " = " + cmass)
          (r > 0) + 'else'
          rmass = squared_mass(particle, force_zerowidth=True)
          r + (cmass_name + " = " + rmass)
          r + (cmass_name_reg + " = " + rmass)
          mass_id + r
        else:
          mass_id + (cmass_name + " = " + cmass)
          mass_id + (cmass_name_reg + " = " + cmass)

      else:
        mass_id = spaceBlock(tabs=3)
        # particle massive -> nonzero masses, regulator = complexmass
        (mass_id > 0) + (mass_reg_name + ' = 3')
        (mass_id > 0) + ('n_masses = n_masses + 1')
        (mass_id > 0) + (mass_id_name + " = n_masses")

        # tagged as resonant? -> width to zero in couplings and masses
        if particle.width.name in cls.resonances:
          r = ifBlock(cls.resonances[particle.width.name] + ' .eqv. .false.',
                      tabs=3)
          r + (cmass_name + " = " + cmass)
          r + (cmass_name_reg + " = " + cmass)
          (r > 0) + 'else'
          rmass = squared_mass(particle, force_zerowidth=True)
          r + (cmass_name + " = " + rmass)
          r + (cmass_name_reg + " = " + rmass)
          mass_id + r
        else:
          mass_id + (cmass_name + " = " + cmass)
          mass_id + (cmass_name_reg + " = " + cmass)

      # register the complex mass and mass regulator in two different arrays
      cmass2 = 'cmass2(' + mass_id_name + ') = ' + cmass_name
      cmass2_reg = 'cmass2_reg(' + mass_id_name + ') = ' + cmass_name_reg
      cls.complex_mass_indices[particle.name] = (cmass2, cmass2_reg)
      if fip:
        fip + mass_id
        fip + '\n'

    else:
      # Vector bosons are either massive or massless.
      # dynamically changing the mass is implemented
      if particle_mass_zero:
        mass_id_name = '1'
        mass_reg_name = '1'
        cmass_name = 'cnul'
        cmass_name_reg = 'cnul'
        width_name = 'cnul'
        res_name = '.false.'
        self.polarizations = (cls.spin_polarization[particle.spin]['massless'])
        # The particles mass is zero, however, the FeynRules modelfile has been
        # derived with non-vanishing mass
        # add the mass to the zero_parameters array which is used to optimize
        # expression
        if particle.mass != self.parameters.ZERO:
          cls.zero_parameters[particle.mass.name] = 0
      else:
        self.polarizations = (cls.spin_polarization[particle.spin]['massive'])
        mass_id_name = 'n' + particle.mass.name
        mass_reg_name = particle.mass.name + '_reg'
        cmass_name = 'c' + particle.mass.name + '2'
        cmass_name_reg = cmass_name + '_reg'
        if particle.width != self.parameters.ZERO:
          width_name = particle.width.name
          res_name = cls.resonances[particle.width.name]
        else:
          width_name = 'cnul'
          res_name = '.false.'

    self.mass_id_name = mass_id_name
    self.mass_reg_name = mass_reg_name
    self.cmass_name = cmass_name
    self.cmass_name_reg = cmass_name_reg
    self.width_name = width_name
    self.res_name = res_name

    if cls.max_polarizations <= self.polarizations:
      cls.max_polarizations = self.polarizations
