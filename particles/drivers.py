###############################################################################
#                                 drivers.py                                  #
###############################################################################

##############
#  Includes  #
##############

import os
import shutil

#########################
#  Globals and Methods  #
#########################

def copytree(src, dst, symlinks=False, ignore=None):
  if not os.path.exists(dst):
    os.makedirs(dst)
  for item in os.listdir(src):
    s = os.path.join(src, item)
    d = os.path.join(dst, item)
    if os.path.isdir(s):
      copytree(s, d, symlinks, ignore)
    else:
      if((not os.path.exists(d)) or
         os.stat(s).st_mtime - os.stat(d).st_mtime > 1):
        shutil.copy2(s, d)

def copy_drivers(folder):
  """ Method for copying all the drivers to a certain path"""
  libpath = os.path.dirname(os.path.realpath(__file__))
  libpath = os.path.abspath(os.path.join(libpath, os.path.pardir))
  libpath = os.path.join(libpath, 'RecolaFeynrulesLibs')
  copytree(libpath, folder)
