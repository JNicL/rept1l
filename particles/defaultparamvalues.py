################################################################################
#                            defaultparamvalues.py                             #
################################################################################

# Default parameters overwriting the UFO ones
SM_defaults = {'MZ': '91.153480619183d0',
               'WZ': '2.4942663787728d0',
               'MW': '80.357973609878d0',
               'WW': '2.0842989982782d0',
               'MH': '125d0',
               'WH': '0d0',
               'ME': '0d0',
               'MM': '0d0',
               'WM': '0d0',
               'MTA': '0d0',
               'WTA': '0d0',
               'MU': '0d0',
               'MD': '0d0',
               'MC': '0d0',
               'WC': '0d0',
               'MS': '0d0',
               'MT': '173.2d0',
               'WT': '0d0',
               'MB': '0d0',
               'WB': '0d0',
               'aEW': '0.75552541674291d-02',
               'aS': '0.118d0'}

HS_defaults = {'MHL': '125d0',
               'WHL': '0d0',
               'MHH': '500d0',
               'WHH': '0d0',
               'sa': '-0.1d0',
               'l3': '0.2d0'}
HS_defaults.update(SM_defaults)

HT_defaults = {'MH1': '125d0',
               'WH1': '0d0',
               'MHP': '500d0',
               'WHP': '0d0',
               'a2': '0.1d0',
               'b4': '0.2d0'}
HT_defaults.update(SM_defaults)



THDM_defaults = {'MHL': '125d0',
                 'WHL': '0d0',
                 'MHH': '500d0',
                 'WHH': '0d0',
                 'MHA': '420d0',
                 'WHA': '0d0',
                 'MHC': '420d0',
                 'WHC': '0d0',
                 'cb': '0.31622776601684d0',
                 'sa': '-0.31622776601684d0',
                 'l5': '0.1d0',
                 'h1d': '1d0',
                 'h1l': '1d0',
                 'h1u': '0d0',
                 'h2d': '0d0',
                 'h2l': '0d0',
                 'h2u': '1d0'}
THDM_defaults.update(SM_defaults)

NTHDM_defaults = {'MHL': '125d0',
                  'WHL': '0d0',
                  'MHM': '300d0',
                  'WHM': '0d0',
                  'MHH': '500d0',
                  'WHH': '0d0',
                  'MHA': '420d0',
                  'WHA': '0d0',
                  'MHC': '420d0',
                  'WHC': '0d0',
                  'cb': '0.31622776601684d0',
                  'sa1': '-0.31622776601684d0/2.1d0',
                  'sa2': '-0.31622776601684d0/7.2d0',
                  'sa3': '-0.31622776601684d0*1.3d0',
                  'MSB': '129.09944487358186d0',
                  'h1d': '1d0',
                  'h1l': '1d0',
                  'h1u': '0d0',
                  'h2d': '0d0',
                  'h2l': '0d0',
                  'h2u': '1d0'}
NTHDM_defaults.update(SM_defaults)


GM_defaults = {'MHL': '125d0',
               'WHL': '0d0',
               'MHH': '500d0',
               'WHH': '0d0',
               'MHA': '420d0',
               'WHA': '0d0',
               'MHC': '420d0',
               'WHC': '0d0',
               'MH3': '420d0',
               'WH3': '0d0',
               'MH5': '420d0',
               'WH5': '0d0',
               'th': '0.01d0',
               'sa': '-0.31622776601684d0',
               'M1': '1.5d0',
               'M2': '0.4d0'
               }
GM_defaults.update(SM_defaults)


SMWP_defaults = SM_defaults.copy()

SMWZP_defaults = {
    'MZp': '3000d0', 'WZp': '0d0', 'MWp': '2000d0', 'WWp': '0d0',
    'kWL': '1.1d0', 'kWR': '0.3d0', 'kZL': '1.3d0', 'kZR': '0.1d0', 'cabi': 0.,
    'CZdR1x1': '1d0', 'CZdR1x2': '0d0', 'CZdR1x3': '0d0', 'CZdR2x1': '0d0', 'CZdR2x2': '1d0',
    'CZdR2x3': '0d0', 'CZdR3x1': '0d0', 'CZdR3x2': '0d0', 'CZdR3x3': '1d0', 'CZdL1x1': '1d0',
    'CZdL1x2': '0d0', 'CZdL1x3': '0d0', 'CZdL2x1': '0d0', 'CZdL2x2': '1d0', 'CZdL2x3': '0d0',
    'CZdL3x1': '0d0', 'CZdL3x2': '0d0', 'CZdL3x3': '1d0', 'CZuR1x1': '1d0', 'CZuR1x2': '0d0',
    'CZuR1x3': '0d0', 'CZuR2x1': '0d0', 'CZuR2x2': '1d0', 'CZuR2x3': '0d0', 'CZuR3x1': '0d0',
    'CZuR3x2': '0d0', 'CZuR3x3': '1d0', 'CZuL1x1': '1d0', 'CZuL1x2': '0d0', 'CZuL1x3': '0d0',
    'CZuL2x1': '0d0', 'CZuL2x2': '1d0', 'CZuL2x3': '0d0', 'CZuL3x1': '0d0', 'CZuL3x2': '0d0',
    'CZuL3x3': '1d0', 'CZlR1x1': '1d0', 'CZlR1x2': '0d0', 'CZlR1x3': '0d0', 'CZlR2x1': '0d0',
    'CZlR2x2': '1d0', 'CZlR2x3': '0d0', 'CZlR3x1': '0d0', 'CZlR3x2': '0d0', 'CZlR3x3': '1d0',
    'CZlL1x1': '1d0', 'CZlL1x2': '0d0', 'CZlL1x3': '0d0', 'CZlL2x1': '0d0', 'CZlL2x2': '1d0',
    'CZlL2x3': '0d0', 'CZlL3x1': '0d0', 'CZlL3x2': '0d0', 'CZlL3x3': '1d0', 'CWqR1x1': '1d0',
    'CWqR1x2': '0d0', 'CWqR1x3': '0d0', 'CWqR2x1': '0d0', 'CWqR2x2': '1d0', 'CWqR2x3': '0d0',
    'CWqR3x1': '0d0', 'CWqR3x2': '0d0', 'CWqR3x3': '1d0', 'CWqL1x1': '1d0', 'CWqL1x2': '0d0',
    'CWqL1x3': '0d0', 'CWqL2x1': '0d0', 'CWqL2x2': '1d0', 'CWqL2x3': '0d0', 'CWqL3x1': '0d0',
    'CWqL3x2': '0d0', 'CWqL3x3': '1d0', 'CWlR1x1': '1d0', 'CWlR1x2': '0d0', 'CWlR1x3': '0d0',
    'CWlR2x1': '0d0', 'CWlR2x2': '1d0', 'CWlR2x3': '0d0', 'CWlR3x1': '0d0', 'CWlR3x2': '0d0',
    'CWlR3x3': '1d0', 'CWlL1x1': '1d0', 'CWlL1x2': '0d0', 'CWlL1x3': '0d0', 'CWlL2x1': '0d0',
    'CWlL2x2': '1d0', 'CWlL2x3': '0d0', 'CWlL3x1': '0d0', 'CWlL3x2': '0d0', 'CWlL3x3': '1d0'}

SMWZP_defaults.update(SM_defaults)

HEFT_defaults = SM_defaults.copy()

SMATGC_defaults = SM_defaults.copy()

default_values = {'SM': SM_defaults,
                  'SM (QCD,fermloops,YT)': SM_defaults,
                  'HS': HS_defaults,
                  'HT': HT_defaults,
                  'THDM': THDM_defaults,
                  'NTHDM': NTHDM_defaults,
                  'GM': GM_defaults,
                  "SM W'": SMWP_defaults,
                  "SM W'Z'": SMWZP_defaults,
                  'SM (QCD) + HEFT': HEFT_defaults,
                  'SM (QCD) + ATGC': SMATGC_defaults,
                  'SMEFT (QCD)': SM_defaults,
                  }
