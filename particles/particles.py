#==============================================================================#
#                               makeParticles.py                               #
#==============================================================================#

#============#
#  Includes  #
#============#

import sys
import os
import re
from collections import OrderedDict
from six import iteritems

import rept1l.Model as Model
from rept1l.helper_lib import file_exists, export_particle_name
from rept1l.helper_lib import is_anti_particle
from rept1l.pyfort import modBlock, containsBlock, subBlock
from rept1l.pyfort import funcBlock, loadBlock, selectBlock
from rept1l.pyfort import caseBlock, ifBlock

from .masses import gen_mass_name, fill_particles_masses
from .masses import gen_get_particle_mass_id
from .masses import gen_get_particle_mass_reg
from .masses import gen_get_particle_width
from .fermionloop_opt import gen_isospin_opt, get_gen
from .parameter import fill_parameters, gen_set_parameter, gen_get_parameter
from .parameter import gen_print_parameters
from .resonant_particle import gen_set_resonant_particle_value
#from .resonant_particle import gen_unset_resonant_particle
from .resonant_particle import gen_is_resonant_particle_id
from .light_particle import gen_set_light_particle_value
from .light_particle import gen_is_light_particle

#=======================#
#  Globals and Methods  #
#=======================#

model = Model.model

def replace_line(file_name, line_num, text):
  lines = open(file_name, 'r').readlines()
  lines[line_num] = text
  out = open(file_name, 'w')
  out.writelines(lines)
  out.close()

#=============#
#  structure  #
#=============#

#/---------------------------\#
# module class_particles      #
#                             #
#  prefix                     #
#                             #
#  contains                   #
#                             #
#   subroutines initCMasse    #
#                             #
#   subroutine initParameters #
#                             #
#   subroutine initParticles  #
#                             #
#   function requiresColorflow#
#                             #
#   function doubletPartner   #
#                             #
#   function isFirstGen       #
#                             #
#   function secondGen        #
#                             #
#   function thirdGen         #
#                             #
# end module                  #
#\---------------------------/#

#==============================================================================#
#                             gen_get_particle_id                              #
#==============================================================================#

def gen_get_particle_id(tabs=2):
  """ Generates the function returning the particle_id given the particle
  string.  """
  function_particle_id = funcBlock('get_particle_id_mdl',
                                   arg='p_str',
                                   result='particle_id',
                                   tabs=tabs)
  function_particle_id.addType(('character(*), intent(in)', 'p_str'))
  function_particle_id.addType(('integer', 'particle_id'))

  all_particles = model.model_objects.all_particles
  select_particle = selectBlock('p_str', tabs=tabs+1)
  for p_index, particle in enumerate(all_particles):
    case = caseBlock('"' + str(particle.name) + '"', tabs=tabs+1)
    case += 'particle_id = ' + str(p_index+1)
    select_particle += case

  # last case particle not found
  case_default = caseBlock('default', tabs=tabs+1)
  sts = ('call error_mdl("Particle  "// trim(p_str) // " not defined.", ' +
         'where="get_antiparticle_id_mdl")')
  case_default + sts

  function_particle_id += select_particle
  return function_particle_id

#------------------------------------------------------------------------------#

def gen_get_particle_name(tabs=2):
  """ Generates the function returning the particle name given the particle
  id.  """
  function_particle_name = funcBlock('get_particle_name_mdl', arg='p',
                                     result='pname',
                                     tabs=tabs)
  function_particle_name.addType(('integer, intent(in)', 'p'))
  function_particle_name.addType(('character(len=10)', 'pname'))
  function_particle_name.addType(('character(len=3)', 'p_str'))

  all_particles = model.model_objects.all_particles
  select_particle = selectBlock('p', tabs=tabs+1)
  for p_index, particle in enumerate(all_particles):
    p_name = 'P_' + export_particle_name(particle.name)
    case = caseBlock(p_name, tabs=tabs+1)
    case += 'pname = "' + str(particle.name) + '"'
    select_particle += case

  # last case particle not found
  case_default = caseBlock('default', tabs=tabs+1)
  case_default + "write(p_str,'(i3)') p"
  sts = ('call error_mdl("Particle id " // trim(p_str) // " not defined.", ' +
         'where="get_particle_name_mdl")')
  case_default + sts

  select_particle += case_default
  function_particle_name += select_particle
  return function_particle_name

#------------------------------------------------------------------------------#

def gen_get_particle_texname():
  """ Generates the function returning the tex particle name given the particle
  id.  """
  function_particle_texname = funcBlock('get_particle_texname_mdl', arg='p',
                                        result='ptexname', tabs=2)
  function_particle_texname.addType(('integer, intent(in)', 'p'))
  function_particle_texname.addType(('character(len=30)', 'ptexname'))
  function_particle_texname.addType(('character(len=3)', 'p_str'))

  all_particles = model.model_objects.all_particles
  select_particle = selectBlock('p', tabs=3)
  for p_index, particle in enumerate(all_particles):
    p_name = 'P_' + export_particle_name(particle.name)
    case = caseBlock(p_name, tabs=3)
    case += 'ptexname = "' + str(particle.texname) + '"'
    select_particle += case

  # last case particle not found
  case_default = caseBlock('default', tabs=3)
  case_default + "write(p_str,'(i3)') p"
  sts = ('call error_mdl("Particle id " // trim(p_str) // " not defined.", ' +
         'where="get_particle_texname_mdl")')
  case_default + sts
  select_particle += case_default
  function_particle_texname += select_particle
  return function_particle_texname

#------------------------------------------------------------------------------#

def gen_antiparticle_id(tabs=2):
  """ Generates the function returning the antiparticle_id given the particle
  string.  """
  function_antiparticle_id = funcBlock('get_antiparticle_id_mdl',
                                       arg='p_str',
                                       result='particle_id',
                                       tabs=tabs)
  function_antiparticle_id.addType(('character(*), intent(in)', 'p_str'))
  function_antiparticle_id.addType(('integer', 'particle_id'))

  all_particles = model.model_objects.all_particles
  select_particle = selectBlock('p_str', tabs=tabs+1)
  for p_index, particle in enumerate(all_particles):
    case = caseBlock('"' + str(particle.antiname) + '"', tabs=tabs+1)
    case += 'particle_id = ' + str(p_index+1)
    select_particle += case

  # last case particle not found
  case_default = caseBlock('default', tabs=tabs+1)
  sts = ('call error_mdl("Particle  "// trim(p_str) // " not defined.", ' +
         'where="get_antiparticle_id_mdl")')
  case_default + sts

  select_particle += case_default
  function_antiparticle_id += select_particle
  return function_antiparticle_id

#------------------------------------------------------------------------------#

def gen_get_particle_antiparticle(tabs=2):
  """ Generates the function returning the antiparticle_id given the particle
  string.  """
  function_get_particle_antiparticle = funcBlock('get_particle_antiparticle_mdl',
                                                 arg='p', result='ppa',
                                                 tabs=tabs)
  function_get_particle_antiparticle.addType(('integer, intent(in)', 'p'))
  function_get_particle_antiparticle.addType(('integer', 'ppa'))
  function_get_particle_antiparticle.addType(('character(len=3)', 'p_str'))

  all_particles = model.model_objects.all_particles
  select_particle = selectBlock('p', tabs=tabs+1)
  for p_index, particle in enumerate(all_particles):
    p_name = 'P_' + export_particle_name(particle.name)
    case = caseBlock(p_name, tabs=tabs+1)
    ap_name = 'P_' + export_particle_name(particle.antiname)
    case += 'ppa = ' + ap_name
    select_particle += case

  # last case particle not found
  case_default = caseBlock('default', tabs=tabs+1)
  case_default + "write(p_str,'(i3)') p"
  sts = ('call error_mdl("Particle id " // trim(p_str) // " not defined.", ' +
         'where="get_particle_antiparticle_mdl")')
  case_default + sts

  select_particle += case_default
  function_get_particle_antiparticle += select_particle
  return function_get_particle_antiparticle

#------------------------------------------------------------------------------#

def gen_get_particle_spin():
  """ Generates the function returning the spin given the particle id. """
  function_get_particle_spin = funcBlock('get_particle_spin_mdl', arg='p',
                                         result='pspin', tabs=2)
  function_get_particle_spin.addType(('integer, intent(in)', 'p'))
  function_get_particle_spin.addType(('integer', 'pspin'))
  function_get_particle_spin.addType(('character(len=3)', 'p_str'))

  all_particles = model.model_objects.all_particles
  select_particle = selectBlock('p', tabs=3)
  for p_index, particle in enumerate(all_particles):
    p_name = 'P_' + export_particle_name(particle.name)
    case = caseBlock(p_name, tabs=3)
    case += 'pspin = ' + str(particle.spin)
    select_particle += case

  # last case particle not found
  case_default = caseBlock('default', tabs=3)

  case_default + "write(p_str,'(i3)') p"
  sts = ('call error_mdl("Particle id " // trim(p_str) // " not defined.", ' +
         'where="get_particle_spin_mdl")')
  case_default + sts
  select_particle += case_default
  function_get_particle_spin += select_particle
  return function_get_particle_spin

#------------------------------------------------------------------------------#

def gen_get_particle_color():
  """ Generates the function returning the color given the particle id. """
  function_get_particle_color = funcBlock('get_particle_colour_mdl', arg='p',
                                          result='pcolour', tabs=2)
  function_get_particle_color.addType(('integer, intent(in)', 'p'))
  function_get_particle_color.addType(('integer', 'pcolour'))
  function_get_particle_color.addType(('character(len=3)', 'p_str'))

  all_particles = model.model_objects.all_particles
  select_particle = selectBlock('p', tabs=3)
  for p_index, particle in enumerate(all_particles):
    p_name = 'P_' + export_particle_name(particle.name)
    case = caseBlock(p_name, tabs=3)
    case += 'pcolour = ' + str(particle.color)
    select_particle += case

  # last case particle not found
  case_default = caseBlock('default', tabs=3)
  case_default + "write(p_str,'(i3)') p"
  sts = ('call error_mdl("Particle id " // trim(p_str) // " not defined.", ' +
         'where="get_particle_colour_mdl")')
  case_default + sts
  select_particle += case_default
  function_get_particle_color += select_particle
  return function_get_particle_color

#------------------------------------------------------------------------------#

def gen_is_particle():
  """ Generates the function returning whether the particle is the particle or
  anti-particle. """
  function_is_particle = funcBlock('is_particle_mdl', arg='p',
                                   result='is_particle', tabs=2)
  function_is_particle.addType(('integer, intent(in)', 'p'))
  function_is_particle.addType(('logical', 'is_particle'))
  function_is_particle.addType(('character(len=3)', 'p_str'))

  all_particles = model.model_objects.all_particles
  select_particle = selectBlock('p', tabs=3)
  for p_index, particle in enumerate(all_particles):
    p_name = 'P_' + export_particle_name(particle.name)
    case = caseBlock(p_name, tabs=3)

    is_particle = ".false."
    is_particle = not is_anti_particle(particle.name)
    if is_particle:
      is_particle = ".true."
    else:
      is_particle = ".false."
    case += 'is_particle = ' + is_particle
    select_particle += case

  # last case particle not found
  case_default = caseBlock('default', tabs=3)
  case_default + "write(p_str,'(i3)') p"
  sts = ('call error_mdl("Particle id " // trim(p_str) // " not defined.", ' +
         'where="is_particle_mdl")')
  case_default + sts
  select_particle += case_default
  function_is_particle += select_particle
  return function_is_particle

#------------------------------------------------------------------------------#

def gen_is_backgroundfield():
  """ Generates the function returning whether the particle is a backgroundfield
  particle. """
  function_is_backgroundfield = funcBlock('is_backgroundfield_mdl', arg='p',
                                          result='isbf', tabs=2)
  function_is_backgroundfield.addType(('integer, intent(in)', 'p'))
  function_is_backgroundfield.addType(('logical', 'isbf'))
  function_is_backgroundfield.addType(('character(len=3)', 'p_str'))

  all_particles = model.model_objects.all_particles
  select_particle = selectBlock('p', tabs=3)
  for p_index, particle in enumerate(all_particles):
    p_name = 'P_' + export_particle_name(particle.name)
    case = caseBlock(p_name, tabs=3)

    if((not hasattr(particle, 'quantumfield')) and
       (not hasattr(particle, 'backgroundfield'))):
      # particle has not assigned either quantumfield nor bgfield
      # -> enable both (field appears in loops and trees)
      backgroundfield = '.true.'
      quantumfield = '.true.'
    else:
      if hasattr(particle, 'backgroundfield') and particle.backgroundfield:
        backgroundfield = '.true.'
      else:
        backgroundfield = '.false.'

      if hasattr(particle, 'quantumfield') and particle.quantumfield:
        quantumfield = '.true.'
      else:
        quantumfield = '.false.'

    case += 'isbf = ' + backgroundfield
    select_particle += case

  # last case particle not found
  case_default = caseBlock('default', tabs=3)
  case_default + "write(p_str,'(i3)') p"
  sts = ('call error_mdl("Particle id " // trim(p_str) // " not defined.", ' +
         'where="is_backgroundfield_mdl")')
  case_default + sts
  select_particle + case_default
  function_is_backgroundfield += select_particle
  return function_is_backgroundfield

#------------------------------------------------------------------------------#

def gen_is_quantumfield():
  """ Generates the function returning whether the particle is a quantumfield
  particle. """
  function_is_quantumfield = funcBlock('is_quantumfield_mdl', arg='p',
                                       result='isqf', tabs=2)
  function_is_quantumfield.addType(('integer, intent(in)', 'p'))
  function_is_quantumfield.addType(('logical', 'isqf'))
  function_is_quantumfield.addType(('character(len=3)', 'p_str'))

  all_particles = model.model_objects.all_particles
  select_particle = selectBlock('p', tabs=3)
  for p_index, particle in enumerate(all_particles):
    p_name = 'P_' + export_particle_name(particle.name)
    case = caseBlock(p_name, tabs=3)

    if((not hasattr(particle, 'quantumfield')) and
       (not hasattr(particle, 'backgroundfield'))):
      # particle has not assigned either quantumfield nor bgfield
      # -> enable both (field appears in loops and trees)
      backgroundfield = '.true.'
      quantumfield = '.true.'
    else:
      if hasattr(particle, 'backgroundfield') and particle.backgroundfield:
        backgroundfield = '.true.'
      else:
        backgroundfield = '.false.'

      if hasattr(particle, 'quantumfield') and particle.quantumfield:
        quantumfield = '.true.'
      else:
        quantumfield = '.false.'

    case += 'isqf = ' + quantumfield
    select_particle += case

  # last case particle not found
  case_default = caseBlock('default', tabs=3)
  case_default + "write(p_str,'(i3)') p"
  sts = ('call error_mdl("Particle id " // trim(p_str) // " not defined.", ' +
         'where="is_quantumfield_mdl")')
  case_default + sts
  select_particle += case_default
  function_is_quantumfield += select_particle
  return function_is_quantumfield

#------------------------------------------------------------------------------#

def gen_has_feature(features, tabs=2):
  """ Generates the function returning true/false depending on whether a feature
  is supported in the model.  """
  function_hasfeature = funcBlock('has_feature_mdl',
                                   arg='feature',
                                   result='has_feature',
                                   tabs=tabs)
  function_hasfeature.addType(('character(*), intent(in)', 'feature'))
  function_hasfeature.addType(('logical', 'has_feature'))

  select_feature = selectBlock('feature', tabs=tabs+1)
  for f, hf in iteritems(features):
    case = caseBlock('"' + f + '"', tabs=tabs+1)
    if hf:
      case += 'has_feature = .true.'
    else:
      case += 'has_feature = .false.'
    select_feature += case

  case_default = caseBlock('default', tabs=tabs+1)
  case_default += 'has_feature = .false.'
  select_feature += case_default

  function_hasfeature += select_feature
  return function_hasfeature

#==============================================================================#
#                              gen_particle_class                              #
#==============================================================================#

def gen_particle_class(folder, timestamp,
                       modelname='NOTSET',
                       modelgauge='NOTSET',
                       overwrite_default_params=False,
                       features={},
                       write=True):
  """ Generates the module class_particles.f90.  """

  f_path = os.path.dirname(os.path.realpath(__file__)) + '/FortranTemplates/'
  class_particles = modBlock('class_particles')

  contains_particles = containsBlock(1)
  function_auxiliary = loadBlock(f_path + 'functions.f90')

  function_init_parameters = subBlock('init_parameters_mdl', tabs=2)

  # particle definition
  class_particles.addPrefix(loadBlock(f_path + 'class_particles_prefix.f90'))
  nparticles = len(model.model_objects.all_particles)

  class_particles.addType(('character(len=*), parameter',
                           'modelname = "' + modelname + '"'))
  class_particles.addType(('character(len=*), parameter',
                           'modelgauge = "' + modelgauge + '"'))
  class_particles.addType(('character(len=*), parameter',
                           'driver_timestamp = "' + timestamp + '"'))

  class_particles.addcomment('number of particles in the model', prefix=True)
  class_particles.addType(('integer, parameter',
                           'n_particles = ' + str(nparticles)))

  orders = [u.name for u in model.model_objects.all_orders]
  # coupling order definition
  norders = str(len(orders))
  class_particles.addType(('integer, parameter',
                           'n_orders = ' + norders))

  n_name_orders = str(max(len(u) for u in orders))
  order_type = ('character(len=' + n_name_orders + '), ' +
                'parameter, dimension(n_orders)')
  order_names = ["'" + u + "'" for u in orders]
  order_ids = 'order_ids = (/' + ', '.join(order_names) + '/)'
  class_particles.addType((order_type, order_ids))
  class_particles.addLineD()

  odp = overwrite_default_params
  (class_particles,
   function_init_particles,
   function_init_prop_masses,
   particle_antiparticle_dict,
   masses, widths) = fill_particles_masses(class_particles,
                                           overwrite_default_params=odp)

  fill_parameters(masses, widths, class_particles, function_init_parameters,
                  overwrite_default_params=odp)

  # hierarchy
  class_particles += contains_particles
  contains_particles += function_auxiliary
  contains_particles += '\n'
  contains_particles += function_init_prop_masses
  contains_particles += '\n'
  contains_particles += function_init_parameters
  contains_particles += '\n'
  contains_particles += function_init_particles
  contains_particles += '\n'

  if 'doublet_partners' in model.__dict__:
    doublet_partners = model.doublet_partners
  else:
    doublet_partners = {}
  function_doubletPartner = gen_isospin_opt(doublet_partners,
                                            particle_antiparticle_dict)
  contains_particles.statements.append(function_doubletPartner)
  contains_particles.addLine()
  (function_isFirstGen,
   function_secondGen,
   function_thirdGen) = get_gen(particle_antiparticle_dict, doublet_partners)

  contains_particles += function_isFirstGen
  contains_particles += '\n'
  contains_particles += function_secondGen
  contains_particles += '\n'
  contains_particles += function_thirdGen
  contains_particles += '\n'
  contains_particles + gen_get_particle_id()
  contains_particles += '\n'
  contains_particles + gen_get_particle_name()
  contains_particles += '\n'
  contains_particles + gen_get_particle_texname()
  contains_particles += '\n'
  contains_particles + gen_antiparticle_id()
  contains_particles += '\n'
  contains_particles + gen_get_particle_antiparticle()
  contains_particles += '\n'
  contains_particles + gen_get_particle_spin()
  contains_particles += '\n'
  contains_particles + gen_get_particle_color()
  contains_particles += '\n'
  contains_particles + gen_mass_name()
  contains_particles += '\n'
  contains_particles + gen_get_particle_mass_id()
  contains_particles += '\n'
  contains_particles + gen_get_particle_mass_reg()
  contains_particles += '\n'
  contains_particles + gen_get_particle_width()
  contains_particles += '\n'
  contains_particles + gen_set_parameter()
  contains_particles += '\n'
  contains_particles + gen_get_parameter()
  contains_particles += '\n'
  contains_particles + gen_print_parameters()
  contains_particles += '\n'
  contains_particles + gen_set_resonant_particle_value()
  contains_particles += '\n'
  contains_particles + gen_is_resonant_particle_id()
  contains_particles += '\n'
  contains_particles + gen_set_light_particle_value()
  contains_particles += '\n'
  contains_particles + gen_is_light_particle()
  contains_particles += '\n'
  contains_particles + gen_is_particle()
  contains_particles += '\n'
  contains_particles + gen_is_backgroundfield()
  contains_particles += '\n'
  contains_particles + gen_is_quantumfield()
  contains_particles += '\n'
  contains_particles + gen_has_feature(features)
  contains_particles += '\n'
  contains_particles + loadBlock(f_path + 'particle_type.f90')

  if write:
    Author = Model.__author__
    UFOdate = Model.__date__
    UFOversion = Model.__version__

    # save exisiting files in treecurrents (there shouldn't be any files)
    cp_path = os.path.join(folder, 'class_particles.f90')
    if file_exists(cp_path):
      directory = os.path.join(folder, 'Model_' + timestamp)
      if not os.path.exists(directory):
        os.makedirs(directory)
      os.rename(cp_path, os.path.join(directory, 'class_particles.f90'))
    with open(cp_path, 'w') as the_file:
      class_particles.genCodeBlock(the_file, 'class_particles.f90', Author,
                                   UFOdate, UFOversion)
      class_particles.printStatment(the_file)

if __name__ == "__main__":
  import doctest
  doctest.testmod()
