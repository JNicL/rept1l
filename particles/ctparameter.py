#==============================================================================#
#                                ctparameter.py                                #
#==============================================================================#

#============#
#  Includes  #
#============#

import os
from sympy import Symbol
from collections import OrderedDict
from six import iteritems

import rept1l.Model as Model
from rept1l.helper_lib import export_fortran, file_exists
from rept1l.pyfort import modBlock, containsBlock, subBlock, ifBlock, loadBlock
from rept1l.pyfort import selectBlock, caseBlock, spaceBlock, funcBlock
from rept1l.logging_setup import log
from rept1l.counterterms import get_particle_ct, get_param_ct, get_wf_ct
from .masses import ParticleMass

#=====================#
#  Globals & Methods  #
#=====================#

model = Model.model

#==============================================================================#
#                              gen_set_renoscheme                              #
#==============================================================================#

def gen_set_renoscheme(renoschemes, tabs=1):
  """ Generates the subroutine for changing the renormalization scheme for a
  counterterm """

  sr = subBlock('set_renoscheme_mdl', arg='ctparam,renoscheme',
                tabs=tabs)
  sr.addPrefix('use input_mdl, only: warning_mdl')
  sr.addType(('character(*), intent(in)', 'ctparam'))
  sr.addType(('character(*), intent(in)', 'renoscheme'))

  sr + '\n'
  sb = selectBlock('ctparam', tabs=tabs+1)
  sr + sb
  for rs, schemes in iteritems(renoschemes):

    cc = caseBlock("'" + rs + "'", tabs=tabs+1)
    scs = selectBlock('renoscheme', tabs=tabs+2)
    cc + scs
    sb + cc
    for scheme, sid in iteritems(schemes):
      scc = caseBlock("'" + scheme + "'", tabs=tabs+2)
      scs + scc
      scc + (rs + '_scheme = ' + str(sid))

    cd = caseBlock('default', tabs=tabs+2)

    sts = ("call warning_mdl('Invalid renoscheme for '// trim(ctparam) // '.'" +
           ", where='set_renoscheme_mdl')")
    cd + sts
    scs + cd

  cd = caseBlock('default', tabs=tabs+1)
  sts = ("call warning_mdl('No renoscheme available for '// trim(ctparam) // " +
         "'.', where='set_renoscheme_mdl')")
  cd + sts
  sb + cd
  return sr


###############################################################################
#                             gen_get_renoscheme                              #
###############################################################################

def gen_get_renoscheme(renoschemes, tabs=1):
  """ Generates the subroutine which returns the renormalizatin scheme applied
  to the given renormalization constant. """

  sr = funcBlock('get_renoscheme_mdl', arg='ctparam', tabs=tabs)
  sr.addPrefix('use input_mdl, only: warning_mdl')
  sr.addType(('character(*), intent(in)', 'ctparam'))
  sr.addType(('character(len=100)', 'get_renoscheme_mdl'))

  sr + '\n'
  sb = selectBlock('ctparam', tabs=tabs+1)
  sr + sb
  for rs, schemes in iteritems(renoschemes):

    cc = caseBlock("'" + rs + "'", tabs=tabs+1)

    scs = selectBlock(rs + '_scheme', tabs=tabs+2)
    cc + scs
    sb + cc
    for scheme, sid in iteritems(schemes):
      scc = caseBlock(str(sid), tabs=tabs+2)
      scs + scc
      scc + ('get_renoscheme_mdl = "' + scheme + '"')

    cd = caseBlock('default', tabs=tabs+2)

    sts = ("call warning_mdl('Invalid renoscheme for '// trim(ctparam) // " +
           "'.', where='get_renoscheme_mdl')")
    cd + sts
    cd + ('get_renoscheme_mdl = "NONE"')
    scs + cd

  cd = caseBlock('default', tabs=tabs+1)
  sts = ("call warning_mdl('No renoschemes for '// trim(ctparam) // " +
         "'.', where='get_renoscheme_mdl')")
  cd + sts
  cd + ('get_renoscheme_mdl = "NONE"')
  sb + cd
  return sr

#==============================================================================#
#                         gen_print_active_renoscheme                          #
#==============================================================================#

def gen_print_renoschemes(renoschemes, tabs=1):
  """ Generates the subroutine for printing renormalization schemes """

  sr = subBlock('print_renoschemes_mdl', tabs=tabs)
  sr.addPrefix('use input_mdl, only: nx')
  sr.addType(('character(len=100)', 'active_renoscheme'))
  sr.addType(('character(len=14)', "fmt = '(4x,a15,a30)'"))

  if len(renoschemes) > 0:
    sr + '\n'
    sr + "write(nx,*) 'Renormalization scheme choice:'"
    for rs, schemes in iteritems(renoschemes):
      sr.addcomment(rs + ', schemes: ' + ','.join(schemes))
      sr + ('active_renoscheme = get_renoscheme_mdl("' + rs + '")')
      sr + ("write(nx,fmt) adjustl('" + rs + ": '), trim(active_renoscheme)")
    sr + """write(nx,'(1x,75("-"))')"""

  return sr


################################################################################
#                          gen_print_counterterms_mdl                          #
################################################################################

def gen_print_counterterms_mdl(renos, tabs=1):
  """ Generates the subroutine for printing the values of counterterm parameter
  """
  renos_names = [u.name for u in renos]
  all_parameters = model.model_objects.all_parameters

  from rept1l.renormalize import RenoConst

  def get_base_name(reno):
    if 'basename' not in RenoConst[reno]:
      return RenoConst[reno][list(RenoConst[reno].keys())[0]]['basename']
    else:
      return RenoConst[reno]['basename']

  p_ct = {}
  for particle in model.P.all_particles:
    p_ct[particle.name] = {}

    for reno in get_particle_ct(particle, mass=True, wavefunction=False):
      rn = reno.name
      selection = sorted(u for u in renos_names if rn in u and get_base_name(u) == rn)
      for s in selection:
        renos_names.remove(s)
      if selection != []:
        if 'massct' not in p_ct[particle.name]:
          p_ct[particle.name]['massct'] = selection
        else:
          p_ct[particle.name]['massct'].extend(selection)

    for reno in get_particle_ct(particle, mass=False, wavefunction=True):
      rn = reno.name
      selection = sorted(u for u in renos_names if rn in u and get_base_name(u) == rn)
      for s in selection:
        renos_names.remove(s)
      if selection != []:
        if 'wavect' not in p_ct[particle.name]:
          p_ct[particle.name]['wavect'] = selection
        else:
          p_ct[particle.name]['wavect'].extend(selection)

    for reno in get_particle_ct(particle, mass=False, wavefunction=False, tadpole=True):
      rn = reno.name
      selection = sorted(u for u in renos_names if rn in u and get_base_name(u) == rn)
      for s in selection:
        renos_names.remove(s)
      if selection != []:
        if 'tadpole' not in p_ct[particle.name]:
          p_ct[particle.name]['tadpole'] = selection
        else:
          p_ct[particle.name]['tadpole'].extend(selection)

    if p_ct[particle.name] == {}:
      del p_ct[particle.name]

  pa_ct = {}
  for pa in get_param_ct(all_parameters, pure_ct=False).keys():
    selection = sorted(u for u in renos_names if pa in u and get_base_name(u) == pa)
    for s in selection:
      renos_names.remove(s)
    if selection != []:
      pa_ct[pa] = selection

  pct = subBlock('print_counterterms_mdl', tabs=tabs)
  pct.addPrefix('use input_mdl, only: nx')
  pct.addType(('character(len=26)', "fmtc = '(4x,a17,es23.16,x,es23.16)'"))

  pct + """write(nx,'(1x,75("-"))')"""
  pct + "write(nx,*) 'Counterterm parameters'"
  pct + """write(nx,'(1x,75("-"))')"""
  pct + "write(nx,*) 'Particle counterterm parameter:'"

  for p in p_ct:
    pct + ("write(nx,*) 'Particle: " + p + "'")
    if 'massct' in p_ct[p]:
      for mct in p_ct[p]['massct']:
        pct + ("write(nx,fmtc) '" + mct + " =', " + mct)
    if 'wavect' in p_ct[p]:
      for wct in p_ct[p]['wavect']:
        pct + ("write(nx,fmtc) '" + wct + " =', " + wct)
    if 'tadpole' in p_ct[p]:
      for tct in p_ct[p]['tadpole']:
        pct + ("write(nx,fmtc) '" + tct + " =', " + tct)

  pct + """write(nx,'(1x,75("-"))')"""
  pct + "write(nx,*) 'Coupling counterterm parameter:'"

  for pa in pa_ct:
    for pp in pa_ct[pa]:
      pct + ("write(nx,fmtc) '" + pp + " =', " + pp)

  return pct


#==============================================================================#
#                           gen_set_light_particles                            #
#==============================================================================#

def gen_set_light_particles(massreg_renoschemes, tabs=1):
  """ Generates the Fortran routine setting the mass and wavefunction
  renormalization of light particles. """

  ams = subBlock('set_light_particles_mdl', tabs=tabs)
  ams.addPrefix('use class_particles')
  ams.addPrefix('use input_mdl, only: nx, print_regularization_scheme')

  ifprs = ifBlock('print_regularization_scheme', tabs=tabs + 1)
  ams + ifprs
  ifprs + """write(nx,'(1x,75("-"))')"""
  ifprs + "write(nx,*) 'Light fermions renormalization scheme choice:'"
  for massreg, renoschemes in iteritems(massreg_renoschemes):
    ifm = ifBlock(massreg + ' .eq. 2', tabs=tabs + 1)

    sp1 = spaceBlock(tabs=tabs + 1)
    ifm + sp1
    ifm = (ifm > 0) + ('else if (' + massreg + ' .eq. 1) then')
    sp2 = spaceBlock(tabs=tabs + 1)
    ifm + sp2

    # write message that mass counterterm is set to massreg/dimreg scheme (i.e.
    # set to zero.)
    particle = renoschemes[0]['particle']
    assert(all(particle == u['particle'] for u in renoschemes))

    ifprs1 = ifBlock('print_regularization_scheme', tabs=tabs + 2)
    sp1 + ifprs1
    ifprs2 = ifBlock('print_regularization_scheme', tabs=tabs + 2)
    sp2 + ifprs2
    ifprs1 + ('write(nx,*) "' + particle + ' is treated in massreg."')
    ifprs1 + ('write(nx,*) "Disabled  mass counterterm for ' + particle + '."')
    ifprs2 + ('write(nx,*) "' + particle + ' is treated in dimreg."')
    ifprs2 + ('write(nx,*) "Disabled  mass counterterm for ' + particle + '."')

    for renoscheme in renoschemes:
      if renoscheme['massreg'] == 1:
        sp = sp2
      else:
        sp = sp1
      id = renoscheme['id']
      if renoscheme['massreg'] == 2:
        sp + ('call set_renoscheme_mdl("' + id + '", "massreg")')
      else:
        sp + ('call set_renoscheme_mdl("' + id + '", "dimreg")')

    ams + ifm

  ams + """ if (print_regularization_scheme) write(nx,'(1x,75("-"))')"""
  return ams


#==============================================================================#
#                              fill_ctparameters                               #
#==============================================================================#

def fill_ctparameters(class_ctparameter, function_init_CTparameters,
                      contains_ctparameter, tabs=0):
  """ Fills pyfort module class_ctparameter which initializes the
  counterterm parameters. """

  from rept1l.renormalize import Renormalize, RenoConst
  from rept1l.regularize import Regularize
  from rept1l.parsing import derive_tensor_expr
  from rept1l.counterterms import Counterterms

  Renormalize.load()
  RenoConst.load()
  Regularize.load()

  modints = modBlock('modelintegrals')
  modints.addPrefix('use constants_mdl, only: dp')
  modints.addPrefix('implicit none')
  cnts = containsBlock(tabs=tabs + 1)
  modints + cnts
  mints = subBlock('set_modelintegrals', arg='reset_collier', tabs=tabs + 1)
  cnts + mints

  mints.addPrefix('use input_mdl, only: init_si')
  mints.addPrefix('use scalar_functions')
  mints.addType(('logical, intent(in)', 'reset_collier'))
  mints + '\n'

  # initialize coli/collier
  mints + ' if (init_si) call init_scalar_functions(reset_collier)'

  mints.addcomment('Precomputation of scalar integrals')

  tens_repl = {}  # substitute the symbols T_i to Ten_i, in order to avoid
                  # conflict with user defined t_i in the fortran code
  if len(Regularize.tens_dict) > 0:
    tens_keys = Regularize.tens_dict.keys()
    for u in tens_keys:
      tens_repl[Symbol(u)] = Symbol('Ten' + u[1:])
    tens_keys = ['Ten' + u[1:] for u in tens_keys]
    # scalar integral results
    var1 = ', '.join(tens_keys)
    modints.addType(('complex(dp)', var1))
    modints.addLine()

  tens_dict = {'Ten' + u[1:]: Regularize.tens_dict[u]
               for u in Regularize.tens_dict}
  tensors_evaluated = derive_tensor_expr(tens_dict, tabs=tabs + 1)
  mints + tensors_evaluated
  mints + '\n'

  # Write all counterterm parameters and associated values to class_particles.
  # Parameters which are substituted according to Renormalize.reno_repl are
  # not written out except if they are zero.

  def gen_reno_module(id):
    if id == 'derived':
      mod = 'derived_ctparameter'
    else:
      mod = 'ctparameter' + str(id)
    mct = modBlock(mod)
    mct.addPrefix('use input_mdl, only: DeltaUV,muUV,muMS')
    mct.addPrefix('use class_ctparameters')
    mct.addPrefix('use modelintegrals')
    mct.addPrefix('use class_couplings')
    cntct = containsBlock()
    mct + cntct
    if id == 'derived':
      sub = 'set_derived_ctparameter'
    else:
      sub = 'set_ctparameter' + str(id)
    setct = subBlock(sub)
    cntct + setct
    return (mod, sub), mct, setct
  reno_modules = OrderedDict()

  reno_modules[('modelintegrals', 'set_modelintegrals')] = modints

  lightp_cts = ParticleMass.massreg_massct

  mct_lightp = {}  # mass counterterms corresp to light particles
  wct_lightp = {}  # wavefunction counterterms corresp to light particles
  for massreg_id in lightp_cts:
    ctdict = lightp_cts[massreg_id]
    if 'mass_ct' in ctdict:
      mct_lightp[ctdict['mass_ct']] = {'mass_reg': massreg_id,
                                       'particle_name': ctdict['particle_name']}
    wct_candidates = (u for u in ctdict.keys() if 'wave_ct' in u)
    for wct_c in wct_candidates:
      wct_lightp[ctdict[wct_c]] = {'mass_reg': massreg_id,
                                   'particle_name': ctdict['particle_name']}

  renos = RenoConst.renosdict_ordered()
  # fill renoscheme dicts to generate setter and getter
  renoschemes = {}  # all schemes
  coupling_renoschemes = {}  # only schemes affecting couplings
  lightp_renoschemes = {}  # only schemes affecting light particles

  all_parameters = model.model_objects.all_parameters
  param_cts = get_param_ct(all_parameters, pure_ct=False).keys()
  particle_cts = set()
  for particle in model.model_objects.all_particles:
    pcts = get_particle_ct(particle, wavefunction=True, mass=True)
    for pct in pcts:
      particle_cts.add(pct.name)

  reno_id = 0
  for reno in renos:
    class_ctparameter.addType(("complex(dp)", str(reno)))
    renodict = RenoConst[reno]

    # check if there are multiple renoschemes for that counterterm
    if 'renoscheme' in renodict:
      multi_reno = False
      renodicts = {renodict['renoscheme']: renodict}
      renoscheme_id = str(reno)

      # check if ct belongs to a coupling
      renobasename = renodict['basename']
      if(not any(u == renobasename for u in particle_cts) and
         renobasename in param_cts):
        coupling_renoschemes[renoscheme_id] = (renodict['renoscheme'],)

      # default (integer)id is zero
      renoschemes[renoscheme_id] = {renodict['renoscheme']: 0}
      default = renoscheme_id + '_scheme = 0'
      class_ctparameter.addType(('integer', default))
    else:
      multi_reno = True
      renodicts = renodict
      renoscheme_id = str(reno)

      # assign (integer) ids for different schemes
      renoscheme = {scheme: sid for sid, scheme in enumerate(renodicts.keys())}

      unique_default = [u for u in renodicts if renodicts[u]['default']]
      if len(unique_default) > 1:
        log('Multiple schemes tagged as default: ' + ', '.join(unique_default),
            'warning')
        default_scheme = list(renodicts.keys())[0]
      elif len(unique_default) == 1:
        default_scheme = unique_default[0]
      else:
        default_scheme = list(renodicts.keys())[0]
      log('Default scheme for ' + renoscheme_id + ': ' + default_scheme,
          'debug')

      # change the default reno scheme, should not be massreg by default
      if default_scheme == 'massreg' and not len(renodict) == 1:
        default_scheme = list(renodicts.keys())[-1]

      default = renoscheme_id + '_scheme = ' + str(renoscheme[default_scheme])
      class_ctparameter.addType(('integer', default))
      renoschemes[renoscheme_id] = renoscheme

      # check if ct belongs to a coupling
      if not ('massreg' in renodict or 'dimreg' in renodict):
        renobasename = renodicts[default_scheme]['basename']

        if(not any(u == renobasename for u in particle_cts) and
           renobasename in param_cts):
          coupling_renoschemes[renoscheme_id] = list(renoscheme.keys())

      # renoscheme is tagged as massreg and is added to the light
      # particle renoschemes.
      if 'massreg' in renodict:
        if renodict['massreg']['basename'] in mct_lightp:
          massct = renodict['massreg']['basename']
          massreg = mct_lightp[massct]['mass_reg']
          particle_name = mct_lightp[massct]['particle_name']
        elif renodict['massreg']['basename'] in wct_lightp:
          wavect = renodict['massreg']['basename']
          massreg = wct_lightp[wavect]['mass_reg']
          particle_name = wct_lightp[wavect]['particle_name']
        else:
          raise Exception('Dimreg ct ' + renodict['dimreg']['basename'] +
                          ' has to be wavefunction or mass ct')
        lpr = {'id': renoscheme_id, 'particle': particle_name, 'massreg': 2}
        if massreg not in lightp_renoschemes:
          lightp_renoschemes[massreg] = [lpr]
        else:
          lightp_renoschemes[massreg].append(lpr)

      # renoscheme is tagged as dimreg and is added to the light
      # particle renoschemes.
      if 'dimreg' in renodict:
        if renodict['dimreg']['basename'] in wct_lightp:
          wavect = renodict['dimreg']['basename']
          massreg = wct_lightp[wavect]['mass_reg']
          particle_name = wct_lightp[wavect]['particle_name']
        elif renodict['dimreg']['basename'] in mct_lightp:
          massct = renodict['dimreg']['basename']
          massreg = mct_lightp[massct]['mass_reg']
          particle_name = mct_lightp[massct]['particle_name']
        else:
          raise Exception('Dimreg ct ' + renodict['dimreg']['basename'] +
                          ' has to be wavefunction or mass ct')

        lpr = {'id': renoscheme_id, 'particle': particle_name, 'massreg': 1}
        if massreg not in lightp_renoschemes:
          lightp_renoschemes[massreg] = [lpr]
        else:
          lightp_renoschemes[massreg].append(lpr)

    # Generate fortran code for ct evaluation
    for renoscheme, renodict in iteritems(renodicts):
      reno_val = renodict['value']
      is_real = 'real' in renodict and renodict['real']
      has_scaledep = 'scaledep' in renodict and renodict['scaledep'] is not None
      if has_scaledep:
        scaledep = renodict['scaledep']
        scaleshift = 'log(muUV**2/' + scaledep + '**2)'

      coupl_repl = {u: Symbol('coupl(n' + u.name + ')')
                    for u in reno_val.free_symbols
                    if u.name.startswith('GC_')}

      # perform the substitution T_i -> Ten_i and GC_ABC -> coupl(nGC_ABC)
      reno_val = reno_val.xreplace(tens_repl).xreplace(coupl_repl)
      c_val = export_fortran(str(reno_val))
      if multi_reno:
        ifreno = ifBlock(renoscheme_id + '_scheme .eq. ' +
                         str(renoschemes[renoscheme_id][renoscheme]),
                         tabs=tabs+1)

      # TODO: (nick 2016-11-21) would be better to use names rather than
      # numbers for the ct modules
      modsub, mct, setct = gen_reno_module(reno_id)
      if multi_reno:
        ctmod = ifreno
        setct + ifreno
      else:
        ctmod = setct
      reno_modules[modsub] = mct

      # shift DeltaUV, including the scale dependence
      if has_scaledep:
        ctmod += ('DeltaUV = DeltaUV + ' + scaleshift)
      ctmod += (str(reno) + ' = ' + c_val)
      # take the real part if parameter tagged as real
      if is_real:
        ctmod += (str(reno) + ' = cone*real(' + str(reno) + ',kind=dp)')
      else:
        ifncms = ifBlock('.not. complex_mass_scheme', tabs=tabs+2)
        ifncms += (str(reno) + ' = cone*real(' + str(reno) + ',kind=dp)')
        ctmod + ifncms

      # undo the DeltaUV shift
      if has_scaledep:
        ctmod += ('DeltaUV = DeltaUV - ' + scaleshift)

      reno_id += 1

  contains_ctparameter + '\n'
  contains_ctparameter + gen_set_renoscheme(renoschemes)
  contains_ctparameter + '\n'
  contains_ctparameter + gen_get_renoscheme(renoschemes)
  contains_ctparameter + '\n'
  contains_ctparameter + gen_print_renoschemes(coupling_renoschemes)
  contains_ctparameter + '\n'
  contains_ctparameter + gen_print_counterterms_mdl(renos)
  contains_ctparameter + '\n'
  contains_ctparameter + gen_set_light_particles(lightp_renoschemes)
  parameters_ct = Counterterms.get_all_ct(derived_ct=False)

  for c in parameters_ct:
    if c not in renos and c not in Renormalize.reno_repl:
      class_ctparameter.addType(("complex(dp)", str(c)))
      log('CT parameters ' + str(c) + ' has no value assigned.', 'warning')

  # Add derived ct parameters
  parameters_ct_derived = Counterterms.get_derived_ct(as_string=True)
  if len(parameters_ct_derived) > 0:
    class_ctparameter.addcomment("Derived counterterm parameters", prefix=True)
  for ct in parameters_ct_derived:
    if ct in Renormalize.reno_repl:
      cc = Renormalize.reno_repl[ct].free_symbols
    else:
      cc = [ct]
    for c in cc:
      class_ctparameter.addType(("complex(dp)", str(c)))

  # Add ct functions ((un-)setting the complex-mass scheme etc.)
  ccctp = containsBlock(tabs=tabs+1)
  f_path = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                        'FortranTemplates')
  function_auxiliary = loadBlock(os.path.join(f_path, 'functions_ct.f90'),
                                 tabs=tabs)
  ccctp + function_auxiliary
  class_ctparameter + ccctp

  # Add derived (internal) counterterm parameters
  if len(parameters_ct_derived) > 0:
    modsub, mct, setct = gen_reno_module('derived')
    for ct in parameters_ct_derived:
      # registered ct parameters undergo substitution
      if ct in Renormalize.reno_repl:
        cc = (u.name for u in Renormalize.reno_repl[ct].free_symbols)
      else:
        cc = [ct]
        log('CT parameter ' + str(ct) + ' has not been registered.', 'warning')
      # write values to fortran module
      for c in cc:
        val = str(Counterterms.derived_ct_values[c])
        val = export_fortran(val)
        setct += (c + ' = ' + val)

    # add module to modules dict
    reno_modules[modsub] = mct

  return reno_modules


#==============================================================================#
#                            gen_ctparameter_class                             #
#==============================================================================#

def gen_ctparameter_class(folder, time, write=True):
  """ Generates the module class_ctparameters.f90 """

  class_ctparameter = modBlock('class_ctparameters')
  class_ctparameter.addPrefix('use constants_mdl, only: dp')
  class_ctparameter.addPrefix('implicit none')
  class_ctparameter.addType(('logical', 'complex_mass_scheme=.true.'))
  fctp = modBlock('fill_ctparameters')
  fctp.addPrefix('use class_ctparameters')

  contains_ctparameter = containsBlock(1)
  cctp = contains_ctparameter

  function_init_CTparameters = subBlock('init_CTparameters_mdl',
                                        arg='reset_collier', tabs=1)
  fict = function_init_CTparameters
  fict.addType(('logical, intent(in)', 'reset_collier'))
  fict + ('call set_light_particles_mdl')

  # hierarchy
  fctp + cctp
  fctp + '\n'
  fctp + function_init_CTparameters

  reno_modules = fill_ctparameters(class_ctparameter, fict, cctp)

  for m, s in reno_modules:
    fctp.addPrefix('use ' + m + ', only: ' + s)
    if s == 'set_modelintegrals':
      fict + ('call set_modelintegrals(reset_collier)')
    else:
      fict + ('call ' + s)

  if write:
    Author = Model.__author__
    UFOdate = Model.__date__
    UFOversion = Model.__version__
    # save exisiting files in treecurrents (there shouldn't be any files)
    cctp_path = os.path.join(folder, 'class_ctparameters.f90')
    if file_exists(cctp_path):
      directory = os.path.join(folder, 'Model_' + time)
      if not os.path.exists(directory):
        os.makedirs(directory)
      os.rename(cctp_path, os.path.join(directory, 'class_ctparameters.f90'))

    with open(cctp_path, 'w') as the_file:
      class_ctparameter.genCodeBlock(the_file, 'class_ctparameters.f90', Author,
                                     UFOdate, UFOversion)
      class_ctparameter.printStatment(the_file)

    # save exisiting files in treecurrents (there shouldn't be any files)
    fct_path = os.path.join(folder, 'fill_ctparameters.f90')
    if file_exists(fct_path):
      directory = os.path.join(folder, 'Model_' + time)
      if not os.path.exists(directory):
        os.makedirs(directory)
      os.rename(fct_path, os.path.join(directory, 'fill_ctparameters.f90'))

    with open(fct_path, 'w') as the_file:
      fctp.genCodeBlock(the_file, 'fill_ctparameters.f90', Author, UFOdate,
                        UFOversion)
      fctp.printStatment(the_file)

    ctparameters = os.path.join(folder, 'ctparameters')
    # no support for  make a `safe` copy of ctparameters yet

    if not os.path.exists(ctparameters):
      os.makedirs(ctparameters)

    for m, s in reno_modules:
      mname = m + '.f90'
      mod = reno_modules[(m, s)]
      if m == 'modelintegrals':
        mpath = os.path.join(folder, mname)
      else:
        mpath = os.path.join(ctparameters, mname)

      with open(mpath, 'w') as the_file:
        mod.genCodeBlock(the_file, mname, Author, UFOdate, UFOversion)
        mod.printStatment(the_file)
