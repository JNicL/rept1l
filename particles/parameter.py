#==============================================================================#
#                               genparameter.py                                #
#==============================================================================#

#============#
#  Includes  #
#============#

from sympy import Symbol
from rept1l.logging_setup import log
from rept1l.pyfort import subBlock, ifBlock, selectBlock, caseBlock, funcBlock
from rept1l.helper_lib import export_fortran, SympyParsing
from .masses import ParticleMass
from .defaultparamvalues import default_values
import rept1l.Model as Model

#############
#  Globals  #
#############

model = Model.model

#==============================================================================#
#                              gen_set_parameter                               #
#==============================================================================#

def gen_set_parameter(tabs=2):
  from rept1l.counterterms import get_param_ct
  """ Generates the subroutine for setting/modifying model dependent parameters
  in RECOLA. Parameters are addressed by their string representative. """

  sp = subBlock('set_parameter_mdl', arg='param,value', tabs=tabs)
  sp.addType(('character(*), intent(in)', 'param'))
  sp.addType(('complex(dp), intent(in)', 'value'))

  if_pminit = ifBlock('particle_model_init', tabs=tabs+1)
  if_pminit + ("call warning_mdl('Parameters need to be set before the process " +
               " generation.', where='set_parameter_mdl')")

  sp + if_pminit
  sc = selectBlock('param', tabs=tabs+1)
  sp + sc

  # user declaration of counterterms (Instance name)
  parameters_ct_def = [u for u in get_param_ct(model.model_objects.
                                               all_parameters).keys()]
  # acutal symbol of the counterterm
  parameters_ct = [u.name for u in get_param_ct(model.model_objects.
                                                all_parameters).values()]
  external = set([(p.name, p.type) for p in model.model_objects.all_parameters
                  if p.nature == 'external' and
                  p.name not in parameters_ct_def])

  if ('muMS', 'real') not in external:
    external.add(('muMS', 'real'))

  internal = set([(p.name, p.type) for p in model.model_objects.all_parameters
                  if p.nature == 'internal' and
                  p.name not in parameters_ct_def])

  cases = []

  for p in external:
    p_name = p[0]
    p_type = p[1]
    case = caseBlock('"' + p_name + '"', tabs=tabs+2)
    if p_type == 'real':
      case + (p_name + ' = real(value,kind=dp)')
    else:
      case + (p_name + ' = value')
    cases.append(case)

  for case in cases:
    sc + case

  sc + 'case default'
  sc + ("call warning_mdl('Trying to change unknown/internal parameter:' " +
        "// trim(param), where='set_parameter_mdl')")

  return sp

#==============================================================================#
#                              gen_get_parameter                               #
#==============================================================================#

def gen_get_parameter(tabs=2):
  from rept1l.counterterms import get_param_ct
  """ Generates the subroutine for getting model dependent parameters
  in RECOLA. Parameters are addressed by their string representative. """

  sp = funcBlock('get_parameter_mdl', arg='param', tabs=tabs)
  sp.addType(('character(*), intent(in)', 'param'))
  sp.addType(('complex(dp)', 'get_parameter_mdl'))

  sc = selectBlock('param', tabs=tabs+1)
  sp + sc

  # user declaration of counterterms (Instance name)
  parameters_ct_def = [u for u in get_param_ct(model.model_objects.
                                               all_parameters).keys()]
  # acutal symbol of the counterterm
  parameters_ct = [u.name for u in get_param_ct(model.model_objects.
                                                all_parameters).values()]
  external = set([(p.name, p.type) for p in model.model_objects.all_parameters
                  if p.nature == 'external' and
                  p.name not in parameters_ct_def])
  if ('muMS', 'real') not in external:
    external.add(('muMS', 'real'))

  internal = set([(p.name, p.type) for p in model.model_objects.all_parameters
                  if p.nature == 'internal' and
                  p.name not in parameters_ct_def])

  cases = []

  for p in external:
    p_name = p[0]
    p_type = p[1]
    case = caseBlock('"' + p_name + '"', tabs=tabs+2)
    case + ('get_parameter_mdl = ' + p_name)
    cases.append(case)

  for p in internal:
    p_name = p[0]
    p_type = p[1]
    case = caseBlock('"' + p_name + '"', tabs=tabs+2)
    case + ('call warning_mdl("Trying to get an internal variable which might' +
            ' not be computed yet", where="get_parameter_mdl")')
    case + ('get_parameter_mdl = ' + p_name)
    cases.append(case)

  for case in cases:
    sc + case

  # case default
  sc + 'case default'
  (sc > 2) + ("call warning_mdl('Trying to get an unknown parameter: ' // " +
              "trim(param), where='get_parameter_mdl')")
  (sc > 2) + ('get_parameter_mdl = cnul')

  return sp

#------------------------------------------------------------------------------#

def gen_print_parameters(tabs=2):
  """ Generates the subroutine for printing model dependent input parameters
  in RECOLA. """

  sp = subBlock('print_parameters_mdl', arg='level', tabs=tabs)
  sp.addType(('integer, intent(in)', 'level'))

  # build a list of masses and widths associated to a representative particle,
  # avoiding printing the same parameters twice
  pparams = {}
  masses_and_widths = []
  mass_particles = {}
  for particle in model.model_objects.all_particles:
    if particle.mass != model.param.ZERO:
      pmass = particle.mass.name
      masses_and_widths.append(pmass)
      pregged = pmass in mass_particles
      if particle.width != model.param.ZERO:
        pwidth = particle.width.name
        masses_and_widths.append(pwidth)
        # overwrite if a particle with the same mass
        if pregged:
          pregkey = mass_particles[pmass]['pkey']
          del pparams[pregkey]
        pparams[(particle.name, particle.spin)] = (pmass, pwidth)
        mass_particles[pmass] = {'pkey': (particle.name, particle.spin),
                                 'width': pwidth}
      elif not pregged:
        pparams[(particle.name, particle.spin)] = pmass
        mass_particles[pmass] = {'pkey': (particle.name, particle.spin),
                                 'width': None}

  # user declaration of counterterms which are used to filter actual input
  # paramters.
  from rept1l.counterterms import get_param_ct
  parameters_ct_def = [u for u in get_param_ct(model.model_objects.
                                               all_parameters).keys()]

  yukawa_params = ParticleMass._yukawa

  # get parameter names for user-defined scale parameters
  model_scales = []
  if hasattr(model, 'scales'):
    for scale in model.scales:
      try:
        model_scales.append(scale.name)
      except:
        model_scales.append(str(scale))

  # sets scale parameter
  scales = set([(p.name, p.type) for p in model.model_objects.all_parameters
                 if p.name in model_scales])
  scales.add(("muMS", "real"))

  # set of external parameter
  external = set([(p.name, p.type) for p in model.model_objects.all_parameters
                  if p.nature == 'external' and
                  p.name not in parameters_ct_def and
                  p not in yukawa_params and
                  p.name not in masses_and_widths and
                  p.name not in model_scales])

  # set of internal (i.e. derived) parameters
  internal = set([(p.name, p.type) for p in model.model_objects.all_parameters
                  if p.nature == 'internal' and
                  p.name not in parameters_ct_def and
                  p.name not in model_scales])

  yukawa_params = set((p.name, p.type) for p in ParticleMass._yukawa)

  sp.addType(('character(len=15)', "fmts = '(4x,a10,g21.14)'"))
  sp.addType(('character(len=24)', "fmtsc = '(4x,a10,g21.14,x,g21.14)'"))
  sp.addType(('character(len=15)', "fmtm = '(4x,a7,es23.16)'"))
  sp.addType(('character(len=25)', "fmtc = '(4x,a7,es23.16,x,es23.16)'"))
  sp.addType(('character(len=26)', "fmtmw = '(4x,a7,es23.16,a8,es23.16)'"))
  sp.addType(('character(len=18)', "fmtml = '(4x,a7,es23.16,a8)'"))

  sp + """write(nx,'(1x,75("-"))')"""
  sp + "write(nx,*) 'Input: Scale values:'"
  for p in sorted(scales):
    p_name = p[0]
    p_type = p[1]

    if p_type == 'real':
      sp + ("write(nx,fmts) adjustl('" + p_name + " ='),real(" +
            p_name + ")")
    else:
      sp + ('if(dimag(' + p_name + ') .eq. 0) then')
      (sp > 2) + ("write(nx,fmts) adjustl('" + p_name + " ='),real(" + p_name + ")")
      sp + ('else')
      (sp > 2) + ("write(nx,fmtsc) adjustl('" + p_name + " =')," + p_name)
      sp + ('end if')

  sp + """write(nx,'(1x,75("-"))')"""
  sp + "write(nx,*) 'Input: Coupling values:'"
  for p in sorted(external):
    p_name = p[0]
    p_type = p[1]

    if p_type == 'real':
      sp + ("write(nx,fmtm) adjustl('" + p_name + " ='),real(" +
            p_name + ")")
    else:
      sp + ('if(dimag(' + p_name + ') .eq. 0) then')
      (sp > 2) + ("write(nx,fmtm) adjustl('" + p_name + " ='),real(" + p_name + ")")
      sp + ('else')
      (sp > 2) + ("write(nx,fmtc) adjustl('" + p_name + " =')," + p_name)
      sp + ('end if')

  sp + """write(nx,'(1x,75("-"))')"""
  sp + "write(nx,*) 'Input: Pole masses and widths [GeV]:'"

  # sort particles according to spin
  for pkey in sorted(pparams.keys(), key=lambda x: x[1]):
    if type(pparams[pkey]) is tuple:
      m, w = pparams[pkey]
    else:
      m = pparams[pkey]
      w = None

    if m + '_reg' in ParticleMass.massreg_massct:

      light = m + '_reg .eq. 2'
      sp + ('if(' + light + ') then')
      (sp > 2) + ("write(nx,fmtml) adjustl('" + m + " ='), " + m +
                  ", 'light'")
      sp + ('else')
      if w:
        (sp > 2) + ("write(nx,fmtmw) adjustl('" + m + " =')," + m +
                    ", adjustl('" + w + " =')," + w)
      else:
        (sp > 2) + ("write(nx,fmtm) adjustl('" + m + " ='), " + m)
      sp + ('endif')
    else:
      if w:
        sp + ("write(nx,fmtmw) adjustl('" + m + " =')," + m +
              ", adjustl('" + w + " =')," + w)
      else:
        sp + ("write(nx,fmtm) adjustl('" + m + " ='), " + m)

  sp + """write(nx,'(1x,75("-"))')"""
  ifb = ifBlock('level .gt. 0', tabs=tabs+1)
  sp + ifb

  ifb + "write(nx,*) 'Complex masses squared [GeV^2] (internal):'"
  for pkey in sorted(pparams.keys(), key=lambda x: x[1]):
    if type(pparams[pkey]) is tuple:
      m, w = pparams[pkey]
      ifb + ("write(nx,fmtc) adjustl('c" + m + "2 ='), c" + m + "2")
    else:
      m = pparams[pkey]
      ifb + ("write(nx,fmtc) adjustl('c" + m + "2 ='), c" + m + "2")

  # all other internal couplings are treated as complex values and may not be
  # equal to their real part due to the complex-mass scheme
  if len(yukawa_params) > 0:
    ifb + """write(nx,'(1x,75("-"))')"""
    ifb + "write(nx,*) 'Yukawa parameter values (internal):'"
    for p in sorted(yukawa_params):
      p_name = p[0]
      ifb + ("write(nx,fmtc) adjustl('" + p_name + " =')," + p_name)

  ifb + """write(nx,'(1x,75("-"))')"""
  ifb + "write(nx,*) 'Internal parameter values:'"

  for p in sorted(internal):
    p_name = p[0]
    ifb + ("write(nx,fmtc) '" + p_name + " ='," + p_name)
  ifb + """write(nx,'(1x,75("-"))')"""

  return sp

#==============================================================================#
#                               fill_parameters                                #
#==============================================================================#

def fill_parameters(masses, widths, class_particles, function_init_parameters,
                    overwrite_default_params=True):
  """ Exports the FeynRules parameters (parameters.py) to class_particles.f90.


  :param overwrite_default_params: Overwrite default UFO parameters values with
                                   the ones defined in defaultparamvalues.
  :type overwrite_default_params: bool
  """

  from rept1l.counterterms import get_param_ct
  parameters_ct = get_param_ct(model.model_objects.all_parameters)
  # Masses, Widths and Counterterms parameters are excluded. The Masses and
  # Widths are processed allowing for the use of the complex mass scheme which
  # is why they are initialized separately

  # external values need to be initialized first - user defines their value
  parameters = [p for p in model.model_objects.all_parameters
                if p.nature == 'external' and
                p.name not in [v.name for v in masses] and
                p.name not in [v.name for v in widths] and
                p.name not in parameters_ct]

  # check if parameters are related to masses such as Yukawa masses. Those need
  # to be set to the same value as their corresponding(complex) mass.
  masses_vals = [u.value if u.nature == 'external' else None for u in masses]
  yukawa_params = {u: masses[masses_vals.index(u.value)] for u in parameters
                   if u.value in masses_vals and u.value != 0.0}
  ParticleMass._yukawa = yukawa_params

  zero_parameters = {u.name: 0 for u in parameters if u.value == 0.0}
  ParticleMass.zero_parameters.update(zero_parameters)
  care = {u for u in zero_parameters if u != 'ZERO'}
  if len(care) > 0:
    if len(care) > 6:
      log('The parameters `' + ', '.join(list(care)[:6]) + ',...` have assigned zero value. ' +
          'This can be problematic for the computation of CT/R2 when ' +
          'considering situations in which these parameters are non-zero.',
          'warning')
      log('The parameters `' + ', '.join(care) + '` have assigned zero value. ' +
          'This can be problematic for the computation of CT/R2 when ' +
          'considering situations in which these parameters are non-zero.',
          'debug')
    else:
      log('The parameters `' + ', '.join(care) + ',...` have assigned zero value. ' +
          'This can be problematic for the computation of CT/R2 when ' +
          'considering situations in which these parameters are non-zero.',
          'warning')

  class_particles.addLineD()
  class_particles.addcomment("External parameter", prefix=True)
  for param in (p for p in parameters if p not in yukawa_params):
    pname = param.name
    if(overwrite_default_params and hasattr(model, 'modelname') and
       model.modelname in default_values and
       pname in default_values[model.modelname]):
      if param.nature == 'external':
        pvalue = str(default_values[model.modelname][pname])
      else:
        log('Parameter `' + pname +
            '` assigned default value, but not external. ' + 'Ignoring.',
            'warning')
        pvalue = export_fortran(str(param.value))
    else:
      pvalue = export_fortran(str(param.value))
    class_particles.addType(("complex(kind=dp)", pname + ' = ' + pvalue))

  class_particles.addLineD()
  class_particles.addcomment("Internal parameter", prefix=True)
  for yukawa_param in yukawa_params:
    pname = yukawa_param.name
    cmass = 'c' + yukawa_params[yukawa_param].name
    class_particles.addType(("complex(kind=dp)", pname))
    function_init_parameters.addStatement(pname + ' = ' + cmass)

  # internal parameters depend on external ones and they are intialized
  # afterwards.

  # parameters are first parsed to sympy, also correctifying the
  # expression/valid fortran expression. Then substitute real mass expressions
  # by complex ones
  mass_repl = {Symbol(u.name):
               Symbol('c' + u.name) for u in ParticleMass.masses.values()}
  for param in (p for p in model.model_objects.all_parameters
                if p.nature == 'internal' and
                p.name not in parameters_ct and p not in masses):
    pname = param.name
    try:
      pvalue = SympyParsing.parse_to_sympy(param.value).subs(mass_repl)
    except TypeError:
      # the value is most likely a number
      pvalue = param.value

    if(overwrite_default_params and hasattr(model, 'modelname') and
       model.modelname in default_values and
       pname in default_values[model.modelname]):
      if param.nature == 'external':
        pvalue = default_values[model.modelname][pname]
      else:
        log('Parameter `' + pname +
            '` assigned default value, but not external. ' + 'Ignoring.',
            'warning')
        pvalue = export_fortran(str(pvalue))
    else:
      pvalue = export_fortran(str(pvalue))

    class_particles.addType(("complex(kind=dp)", pname))
    function_init_parameters.addStatement(pname + ' = ' + pvalue)

  class_particles.addLineD()
  class_particles.addcomment('cmass2/cmass2_reg relates the mass id to ' +
                             'the actual complex squared mass(_reg) value.',
                             prefix=True)
  class_particles.addType(('complex(kind=dp), allocatable',
                           'cmass2(:), cmass2_reg(:)'))
  class_particles.addLineD()

  return class_particles, function_init_parameters
