###############################################################################
#                              light_particle.py                              #
###############################################################################

##############
#  Includes  #
##############

from rept1l.pyfort import subBlock, ifBlock, selectBlock, caseBlock, funcBlock
from .masses import ParticleMass
import rept1l.Model as Model
model = Model.model

#############
#  Methods  #
#############

def gen_set_light_particle_value(tabs=2):
  """ Generates the Fortran subroutine for tagging particles as light
  """
  srp = subBlock('set_light_particle_value_mdl', arg='p,val', tabs=tabs)
  srp.addPrefix('use input_mdl, only: warning_mdl')
  srp.addType(('character(*), intent(in)', 'p'))
  srp.addType(('logical, intent(in)', 'val'))

  # Tag particles as light must be done before generating the processes.
  if_pminit = ifBlock('particle_model_init', tabs=tabs+1)

  sts = ("call warning_mdl('Light particles need to be set before " +
         "defining processes.'" +
         ", where='set_light_particle_value_mdl')")
  (if_pminit > 1) + sts

  lps = ParticleMass.massreg_massct
  all_particles = model.object_library.all_particles

  srp + if_pminit
  sc = selectBlock('p', tabs=tabs+1)
  srp + sc

  for particle in all_particles:
    mass_reg_name = particle.mass.name + '_reg'
    if mass_reg_name in lps:
      p_name = particle.name

      case = caseBlock('"' + p_name + '"', tabs=tabs+1)
      light_tag = mass_reg_name + '_light'
      case + (light_tag + ' = val')
      sc + case

  cd = caseBlock('default', tabs=3)
  sts = ("call warning_mdl('Trying to set light particle for non-supported " +
         "particle: ' // trim(p), where='set_light_particle_value_mdl')")
  cd + sts
  sc + cd

  return srp

#------------------------------------------------------------------------------#

def gen_is_light_particle(tabs=2):
  """ Generates the Fortran function determining whether a particles is tagged
  as light.
  """
  filp = funcBlock('is_light_mass_mdl', arg='m', tabs=tabs)
  filp.addPrefix('use input_mdl, only: warning_mdl')
  filp.addType(('character(*), intent(in)', 'm'))
  filp.addType(('logical', 'is_light_mass_mdl'))

  lps = ParticleMass.massreg_massct
  all_particles = model.object_library.all_particles

  sc = selectBlock('m', tabs=tabs+1)
  filp + sc

  for m in lps:
    case = caseBlock('"' + m + '"', tabs=tabs+1)
    light_tag = m + '_light'
    case + ('is_light_mass_mdl = ' + light_tag)
    sc + case

  cd = caseBlock('default', tabs=3)

  sts = ("call warning_mdl('Trying to determine light status for non-support " +
         "mass: ' // trim(m), where='is_light_mass_mdl')")
  cd + sts
  cd + ('is_light_mass_mdl = .false.')
  sc + cd

  return filp
