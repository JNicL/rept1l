#==============================================================================#
#                             fermion_loop_opt.py                              #
#==============================================================================#

#============#
#  Includes  #
#============#

from rept1l.pyfort import funcBlock, selectBlock
import rept1l.Model as Model
model = Model.model

#===========#
#  Methods  #
#===========#

#==============================================================================#
#                      doublet partners - SM optimization                      #
#==============================================================================#
def gen_isospin_opt(doublet_partners, particle_antiparticle_dict):
  function_doubletPartner = funcBlock('get_doublet_partner_mdl',
                                      arg='particle_in',
                                      result='particle_out',
                                      tabs=2)
  function_doubletPartner.addType(("integer", "particle_in, particle_out"))
  select_case_doubletPartner = selectBlock("particle_in", tabs=3)
  function_doubletPartner.statements.append(select_case_doubletPartner)

  for key in list(doublet_partners.keys()):
    antikey = particle_antiparticle_dict[key]
    value = doublet_partners[key]
    antivalue = particle_antiparticle_dict[value]
    doublet_partners[value] = key
    doublet_partners[antikey] = antivalue
    doublet_partners[antivalue] = antikey

  for key in doublet_partners.keys():
    value = doublet_partners[key]
    select_case_doubletPartner.addStatement("case(" + key + ")", 0)
    select_case_doubletPartner.addStatement("particle_out = " + value, 1)

  select_case_doubletPartner.addStatement("case default", 0)
  select_case_doubletPartner.addStatement("particle_out = 0", 1)

  return function_doubletPartner

#==============================================================================#
#                  Optimizing fermion loops - SM optimization                  #
#==============================================================================#

def get_gen(particle_antiparticle_dict, doublet_partners):
  function_isFirstGen = funcBlock('is_firstgen_mdl',
                                  arg='particle',
                                  result='isFirstGen',
                                  tabs=2)
  function_isFirstGen.addType(("integer", "particle"))
  function_isFirstGen.addType(("logical", "isFirstGen"))
  function_secondGen = funcBlock('get_secondgen_mdl',
                                 arg='particle',
                                 result='secondGen',
                                 tabs=2)
  function_secondGen.addType(("integer", "particle, secondGen"))
  function_thirdGen = funcBlock('get_thirdgen_mdl',
                                arg='particle',
                                result='thirdGen',
                                tabs=2)
  function_thirdGen.addType(("integer", "particle, thirdGen"))

  #flavours = [["P_eMinus", "P_muMinus", "P_taMinus"],
              #["P_u", "P_c", "P_t"]]
  if 'flavours' in model.__dict__:
    flavours = model.flavours
  else:
    flavours = []
  flavours_complete = []

  for family in flavours:
    antifamily = [particle_antiparticle_dict[u] for u in family]
    doublet_family = [doublet_partners[u] for u in family]
    doublet_antifamily = [particle_antiparticle_dict[u] for u in doublet_family]
    flavours_complete.append(family)
    flavours_complete.append(antifamily)
    flavours_complete.append(doublet_family)
    flavours_complete.append(doublet_antifamily)

  first_generation = [u[0] for u in flavours_complete]
  second_generation = dict((key, value) for (key, value) in [(u[0], u[1])
                           for u in flavours_complete])
  third_generation = dict((key, value) for (key, value) in [(u[0], u[2])
                          for u in flavours_complete])

  function_isFirstGen.addStatement("select case(particle)", 1)
  if len(first_generation) > 0:
    firstgenparticles = ', '.join(first_generation)
    function_isFirstGen.addStatement("case(" + firstgenparticles + ")", 1)
    function_isFirstGen.addStatement("isFirstGen = .true.", 2)
  function_isFirstGen.addStatement("case default", 1)
  function_isFirstGen.addStatement("isFirstGen = .false.", 2)
  function_isFirstGen.addStatement("end select", 1)

  function_secondGen.addStatement("select case(particle)", 1)
  for particle in second_generation.keys():
    function_secondGen.addStatement("case(" + particle + ")", 1)
    function_secondGen.addStatement("secondGen = " +
                                    second_generation[particle], 2)
  function_secondGen.addStatement("end select", 1)
  function_thirdGen.addStatement("select case(particle)", 1)
  for particle in third_generation.keys():
    function_thirdGen.addStatement("case(" + particle + ")", 1)
    function_thirdGen.addStatement("thirdGen = " + third_generation[particle],
                                   2)
  function_thirdGen.addStatement("end select", 1)
  return function_isFirstGen, function_secondGen, function_thirdGen
