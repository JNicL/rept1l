#==============================================================================#
#                                   vertices                                   #
#==============================================================================#

#============#
#  Includes  #
#============#

import sys
import re
import rept1l_config

from sympy import Symbol, I
from future.utils import iteritems
from rept1l.vertex import Vertex, permute_particle_branch, update_branch
from rept1l.vertex import fill_couplings, fill_vertices, fill_currents
from rept1l.pyfort import spaceBlock, ProgressBar
from rept1l.logging_setup import log
from rept1l.baseutils import compute_ls_coupling_grouped
from rept1l.baseutils import extract_couplings, CurrentBase
from rept1l.baseutils.recola_base import gen_recola_base
from rept1l.renormalize import Renormalize
from rept1l.currents import lorentz_lib
from rept1l.currents import write_tree_vertex
from rept1l.currents import write_loop_vertex
from rept1l.currents import write_form_vertex
from rept1l.currents import TreeCurrentPyfort
from rept1l.currents import LoopCurrentPyfort
from rept1l.currents import FormCurrentPyfort
from rept1l.helper_lib import get_anti_particle, flatten
from rept1l.helper_lib import export_particle_name
from rept1l.helper_lib import export_fortran
from rept1l.helper_lib import is_anti_particle
from rept1l.colorflow import ColorflowVertex, create_colorflow_entry
from rept1l.combinatorics import perm_elems, lorentz_base_dict
from rept1l.combinatorics import reconstruct_lorentz
from rept1l.combinatorics import gen_permutations, permute_string
from rept1l.coupling import RCoupling as FRC
from rept1l.counterterms import Counterterms
from rept1l import Model as Model


model = Model.model
model_objects = model.object_library
particle_names = [particle.name for particle in model_objects.all_particles]

if(hasattr(rept1l_config, 'split_loop_currents')):
  split_loop_currents = rept1l_config.split_loop_currents
else:
  split_loop_currents = 2

if(hasattr(rept1l_config, 'split_tree_currents')):
  split_tree_currents = rept1l_config.split_tree_currents
else:
  split_tree_currents = 2

if(hasattr(rept1l_config, 'split_form_currents')):
  split_form_currents = rept1l_config.split_form_currents
else:
  split_form_currents = 2

if(hasattr(rept1l_config, 'optimize_base') and
   rept1l_config.optimize_base is True):
  optimize_base = True
else:
  optimize_base = False

if(hasattr(rept1l_config, 'recola_base') and
   rept1l_config.recola_base is True):
  use_recola_base = True
else:
  use_recola_base = False

if use_recola_base and optimize_base:
  warnmsg = 'Using recola base and merging (optimize_base) at the same time.\n'
  warnmsg += 'This feature is not well tested yet.'
  log(warnmsg, 'warning')

if(hasattr(rept1l_config, 'apply_cse_currents') and
   rept1l_config.apply_cse_currents is True):
  apply_cse_currents = True
else:
  apply_cse_currents = False

if(hasattr(rept1l_config, 'simplify_currents') and
   rept1l_config.simplify_currents is True):
  simplify_currents = True
else:
  simplify_currents = False

# compute the model neglecting generated modelfiles
compute_model = False

#  stores the model in an intermediate format. This abstract format is used for
#  the communication between recola(fortran) and python. It also allows for
#  writing out the modelfile without reading the UFO input.
store_intermediate = True

# compute tree currents
compute_tree = True
# compute ct currents. Expansion rules need to be defined. See counterterms.py.
compute_ct = False
# compute ct currents only for QCD orders.
pure_qcd = False
# compute r2 currents. lorentzR2_dict needs to be filled. See rational.py.
compute_r2 = False

# Substitute solutions for renormalization -> proper order classification.
subs_reno_constants = False

# Write compute_tree currents methods
write_tree = True
# Write compute_loop currents methods
write_loop = True
# Write formCurrent methods
write_form = True
# Can be set to a list of vertex (names) which should be expanded in couplings
# and wavefunctions. compute_ct must be active. 2-point vertices are
# automatically computed and do not need to be included in that list
vertex_expansion = False
# Restrict the vertices taken into account
vertex_list = None
# Ignore a list of vertices
exclude_vertices = None

#Force process. Answers all querys with yes if force=True
force = False

vertices_info = {}  # stores the fortran modules carrying the vertex info
couplings_info = {}  # ...                                ... coupling info
ctcouplings_info = {}  # ...                              ... ctcoupling info
currents_info = {} # ...                              ... currents info


def create_particle_configuration(particles):
  """
  particles: List of integers

  Used to create a dictionary key for a certain particle configuration
  """
  tmp = 0
  for i in range(len(particles)):
    tmp += particles[i] << 8 * i
  return tmp


def get_prime_number(n):
  if n < 4:
    return 3
  else:
    from sympy import sieve
    prime_iter = sieve.primerange(3, n)
    for last in prime_iter:
      pass
    return last


def create_vertex_entry(pkey, v_id, func, simpleout):
  """ Allocates the space for a new vertex/current

  :pkey: list of UFO particles
  :v_id: vertex identifier
  :func: Pyfort instance filled with vertex/current information
  """

  particles_indices = [particle_names.index(u) + 1 for u in pkey]
  renamed_particles = ['P_' + export_particle_name(u) for u in pkey]
  simpleout + ('# ' + ' '.join(u for u in pkey))
  hash_code = ', '.join(renamed_particles + [str(v_id)] + ['check'])
  simpleout + (str(len(particles_indices)) + ' ' + str(v_id))
  simpleout + (' '.join(str(u) for u in particles_indices))
  func + ('call createCurrent(' + hash_code + ')')

  return func


recola_base = {}


def generate_recola_vertices(gendict, generation_timestamp=''):
  """ Derives the RECOLA modelfile from the UFO modelfile.
  """

  global compute_model, compute_r2, compute_ct, pure_qcd
  global subs_reno_constants
  global write_tree, write_loop, store_intermediate
  global vertex_expansion
  global vertex_list, exclude_vertices

  register_ct_on_demand = True

  if use_recola_base:
    recola_base = gen_recola_base()
  else:
    recola_base = {}

  FRC.load()
  if not compute_model:
    ColorflowVertex.load()
    Vertex.load()
  else:
    if compute_r2 or Vertex.any_r2vertices():
      ColorflowVertex.load()
      Vertex.load()
      Vertex.lorentzTree_dict = {}
      Vertex.lorentzCT_dict = {}

    if compute_r2 and not Vertex.any_r2vertices():
      log('Cannot compute rational vertices, no rational vertices found! ' +
          ' Run compute_all_rationals.', 'error')
      sys.exit()

    if compute_ct:
      if subs_reno_constants:
        Renormalize.load()
        if len(Renormalize.reno_repl) == 0:
          # this must not be a problem if the counterterms do not split in
          # different perturbative orders.
          log('No ct parameter substitutions found.', 'info')
        if not register_ct_on_demand:
          FRC.generate_CT(repl=Renormalize.reno_repl)
        Counterterms.load()
        if not len(Counterterms.renos_order) > 0:
          log('The ct parameters order have not been determined.', 'error')
          sys.exit()
      else:
        FRC.load()
        Counterterms.load()
        if len(Counterterms.renos_order) > 0:
          log('Counterterms attribute renos_order is not empty!',
              'warning')
          Counterterms._renos_order = {}
        if len(Counterterms.cts) == 0:
          log('No counterterms couplings found! ' +
              'The system tries to generate them on the fly.',
              'warning')

        if not register_ct_on_demand:
          FRC.generate_CT()

    n_particles = len(model.model_objects.all_particles)

  # By default the max_leg_multiplicity is set to 4, however,  if rept1l
  # encounters higher leg multiplies this value is overwritten
  max_leg_multiplicity = 4

  explicit_permute = False

  max_vertices_block = spaceBlock()
  allocate_dict_block = spaceBlock()
  max_leg_multiplicity_block = spaceBlock()

  # determine max vertices
  n_vertices = 0
  for vertex in model_objects.all_vertices:
    valid_vertex = Vertex.valid_vertex_BFM(vertex.particles)
    if valid_vertex:
      if not explicit_permute:
        n_vertices += 1
      else:
        perms = gen_permutations(vertex.particles)
        for perm in perms:
          perm_particles = perm_elems(vertex.particles, perm)
          valid_vertex = Vertex.valid_vertex_BFM(perm_particles)
          if valid_vertex:
            n_vertices += 1

  n_vertices_max = n_vertices
  PG = ProgressBar(n_vertices_max)
  gendict['function_init_vertices'].append(allocate_dict_block)

  gendict['class_vertices'].appendD(max_vertices_block)
  gendict['class_vertices'].appendD(max_leg_multiplicity_block)

#------------------------------------------------------------------------------#

  if compute_model:
    if vertex_list is None:
      vertex_list = model_objects.all_vertices
    # Do 23 Okt 2014 00:42:24 CEST
    #special_list = ['V_1', 'V_65', 'V_67']
    #vertex_list = [u for u in model_objects.all_vertices
                   #if len(u.particles) == 3]
    if compute_ct:
      Counterterms.load()
      vertex_list.extend(model_objects.all_CTvertices)

    if exclude_vertices is not None:
      from rept1l.helper_lib import find_vertex
      vertex_list = [u for u in vertex_list if u not in exclude_vertices]

  else:
    log('loaded model files', 'info')


  # runs over all vertices defined in model. If compute_model = False
  # vertex_list is empty
  log('Processing ' + str(len(vertex_list)) + ' UFO vertices.', 'info')
  for vertex in vertex_list:
    vertex_color = vertex.color
    n_In = len(vertex.lorentz[0].spins) - 2

    # transform particles from outgoing to incoming
    # Convention in UFO is that all particles are outgoing
    # We use the convention that all particles are incoming.
    # The colorflow is inverted at a later stage in the colorflow.py module
    vertex_particles = [get_anti_particle(u, model_objects.all_particles)
                        for u in vertex.particles]

    # loop over all possible particle permutations.
    permutations = gen_permutations(vertex_particles)

    if not explicit_permute:
      # it can happen that the default vertex is not a valid
      # one according to the BFM. In that case we need to find a valid
      # permutation, otherwise the vertex is not taken into account.
      valid_vertex = Vertex.valid_vertex_BFM(vertex_particles)
      if not valid_vertex:
        found_perm = False
        for permutation in gen_permutations(vertex_particles):
          if Vertex.valid_vertex_BFM(perm_elems(vertex_particles, permutation)):
            permutations = [permutation]
            found_perm = True
            break

        # the vertex does not contribute at all
        if found_perm is False:
          PG.advanceAndPlot()
          continue

      else:
        permutations = [range(len(vertex_particles))]

    for permutation in permutations:
      # update color arguments to permutation
      vcp = [permute_string(c, permutation) if c != '1' else '1'
             for c in vertex_color]

      particles = perm_elems(vertex_particles, permutation)
      valid_vertex = Vertex.valid_vertex_BFM(particles)
      if not valid_vertex:
        PG.advanceAndPlot()
        continue
      else:
        #
        assign_tree = True  # for tree and bare-loop -currents
        # derive ct current only if the current exists at tree-level
        assign_ct = (True if valid_vertex['tree_and_loop']
                     else valid_vertex['tree_only'])

      PG.advanceAndPlot()

      # In UFO couplings of different orders are given in a list which needs to
      # be ``flattened``. In order to do so we make copies of lorentz
      # structures to uniquely assign every coupling to a color and lorentz
      # structure key
      if any(type(u) is list for u in vertex.couplings.values()):
        ls_enlarged = {}  # old lorentz array. keys point to list of couplings
        for k in vertex.couplings:
          if k[1] not in ls_enlarged:
            ls_enlarged[k[1]] = []
          cc = vertex.couplings[k]
          if type(cc) is list:
            ls_enlarged[k[1]].extend(cc)
          else:
            ls_enlarged[k[1]].append(cc)

        offset = 0
        # introduce copies of lorentz structure and assign new indices
        new_ls_map = {}  # old key can be assigned to more new keys
        for k in ls_enlarged:
          new_ls_map[k] = range(k + offset, k + offset + len(ls_enlarged[k]))
          offset += len(new_ls_map[k]) - 1

        # build the new lorentz structure of the vertex
        vertex_lorentz = [v for v in flatten([[u] * len(ls_enlarged[pos])
                          for pos, u in enumerate(vertex.lorentz)])]

        # build the new coupling structure of the vertex
        vertex_couplings = {}
        for k in vertex.couplings:
          for pos, l in enumerate(new_ls_map[k[1]]):
            key = (k[0], l)
            if type(vertex.couplings[k]) is list:
              vertex_couplings[key] = vertex.couplings[k][pos].name
            else:
              vertex_couplings[key] = vertex.couplings[k].name

      else:
        vertex_lorentz = vertex.lorentz
        vertex_couplings = {u: vertex.couplings[u].name
                            for u in vertex.couplings}

      # here, spins is a identifier for fermions and antifermions.
      spins = [-u.spin if (u.spin == 2 and is_anti_particle(u.name)) else
               u.spin for u in vertex.particles]
      spinsp = perm_elems(spins, permutation)
      # Generator for colorflow vertices
      CV = ColorflowVertex.process(vcp, vertex_lorentz,
                                   vertex_couplings, n_In, spinsp)
      for vert in CV:
        log('Processing vertex: ' + ','.join(u.name for u in vertex.particles),
            'debug')
        couplings = [Symbol(u) for u in vertex_couplings.values()]
        lorentz_coupling_ordered = {}
        for coupling in vert['coupling_lorentz']:
          c_index = [u.name for u in couplings].index(coupling)
          lorentz_key = vert['coupling_lorentz'][coupling]
          lorentz = vert['ls_dict'][lorentz_key]
          lbs = CurrentBase(lorentz).compute(permutation, form_simplify=True)
          lorentz_coupling_ordered[couplings[c_index]] = lbs
          log('Coupling ' + str(coupling), 'debug')
          log('Lorentz key ' + str(lorentz_key), 'debug')
          log('Lorentz ' + str(lorentz), 'debug')
          log('Coupling' + str(c_index) + ' multiplied with: ', 'debug')
          log('LBS: ' + str(lbs), 'debug')

        Vertex.assign_vertex(vertex,
                             particles,
                             vert['colorkey'],
                             lorentz_coupling_ordered,
                             assign_tree=assign_tree,
                             assign_ct=assign_ct,
                             explicit_permute=explicit_permute,
                             register_ct_coupling=register_ct_on_demand,
                             subs_reno_constants=subs_reno_constants,
                             vertex_expansion=vertex_expansion,
                             compute_ct=compute_ct,
                             force=force,
                             pure_qcd=pure_qcd)

  if not explicit_permute:
    log('\n', 'info')
    log('Permuting tree branches.', 'info')
    n = len(Vertex.lorentzTree_dict)
    PG = ProgressBar(n)
    n0 = 0
    for p_key, amplitudes in iteritems(Vertex.lorentzTree_dict.copy()):
      PG.advanceAndPlot(status=str(n0) + '/' + str(n))
      n0 += 1
      for perm in gen_permutations(p_key):
        valid_vertex = Vertex.valid_vertex_BFM(perm_elems(p_key, perm))
        if valid_vertex and perm != list(range(len(p_key))):
          p = permute_particle_branch({p_key: amplitudes}, perm)
          Vertex.lorentzTree_dict = update_branch(Vertex.lorentzTree_dict, p,
                                                  type='particle')

    if compute_ct and len(Vertex.lorentzCT_dict) > -1:
      for p_key, amplitudes in iteritems(Vertex.lorentzCT_dict.copy()):
        for perm in gen_permutations(p_key):
          if perm != list(range(len(p_key))):
            p = permute_particle_branch({p_key: amplitudes}, perm)
            Vertex.lorentzCT_dict = update_branch(Vertex.lorentzCT_dict, p,
                                                  type='particle')

  log('\n', 'info')
  log('Register colorflows.', 'info')
  n = len(ColorflowVertex.colors_dict)
  PG = ProgressBar(n)
  n = 0
  for c_key in ColorflowVertex.colors_dict:
    n += 1
    PG.setLevel(n)
    PG.plotProgress()
    cf, n_In = c_key
    c_id = ColorflowVertex.colors_dict[c_key]
    create_colorflow_entry(cf, c_id, n_In, gendict['selectCaseColorflow'])

  n_tree = len(Vertex.lorentzTree_dict)
  if compute_tree and n_tree > 0:
    log('\n', 'info')
    log('Register tree-and loop-currents.', 'info')
    # register Tree & Loop vertices
    PG = ProgressBar(n_tree)
    n = 0
    for p_key in Vertex.lorentzTree_dict:
      if len(p_key) > max_leg_multiplicity:
        max_leg_multiplicity = len(p_key)
      n = n + 1
      PG.setLevel(n)
      PG.plotProgress()
      vertex = Vertex(list(Vertex.lorentzTree_dict[p_key].keys()), p_key)
      for co_key in Vertex.lorentzTree_dict[p_key]:
        vertex.assign_lorentz(co_key, Vertex.lorentzTree_dict[p_key][co_key],
                              flag=Vertex.Tree)

  n_ct = len(Vertex.lorentzCT_dict)
  if compute_ct and n_ct > 0:
    log('\n', 'info')
    log('Register counterterm currents.', 'info')

    # register counter vertices
    PG = ProgressBar(n_ct)
    n = 0
    for p_key in Vertex.lorentzCT_dict:
      if len(p_key) > max_leg_multiplicity:
        max_leg_multiplicity = len(p_key)
      n = n + 1
      if n_ct > 0:
        PG.setLevel(n)
        PG.plotProgress()
      ctvertex = Vertex(Vertex.lorentzCT_dict[p_key].keys(), p_key)
      for co_key in Vertex.lorentzCT_dict[p_key]:
        ctvertex.assign_lorentz(co_key, Vertex.lorentzCT_dict[p_key][co_key],
                                flag=Vertex.CT)

  if compute_r2:
    log('\n', 'info')
    log('Register r2 currents.', 'info')
    # register r2 vertices
    n_r2 = len(Vertex.lorentzR2_dict)
    PG = ProgressBar(n_r2)
    n = 0
    for p_key in Vertex.lorentzR2_dict:
      if len(p_key) > max_leg_multiplicity:
        max_leg_multiplicity = len(p_key)
      n = n + 1
      PG.setLevel(n)
      PG.plotProgress()
      r2vertex = Vertex(Vertex.lorentzR2_dict[p_key].keys(), p_key)
      for co_key in Vertex.lorentzR2_dict[p_key]:
        r2vertex.assign_lorentz(co_key, Vertex.lorentzR2_dict[p_key][co_key],
                                flag=Vertex.R2)

  # generate fortran current entries
  fill_currents(currents_info)

  # fill fortran vertex entries
  def print_v():
    print("Vertex.currentIDs:", Vertex.currentIDs)
    for key in Vertex.current_strucs:
      print("key:", key)
      for v in Vertex.current_strucs[key]:
        print("v:", v)

  log('\n', 'info')
  log('Derive lorentz base.', 'info')
  if optimize_base:
    fill_vertices(None)
    #print_v()
    Vertex.optimize_bases(nomerge=recola_base.keys())
    #print_v()
    Vertex.currentIDs = {}
    log('\n', 'info')
    log('Rederive lorentz base.', 'info')
    fill_vertices(vertices_info, clear_cache=True, forbid_new_bases=True)
  else:
    fill_vertices(vertices_info)

  # TODO: (nick 2015-01-19) check if branch_tag always equal to flag -> if so
  # remove one or the other

  log('\n', 'info')
  log('Fill vertices.', 'info')
  if store_intermediate:
    Vertex.treevertices = [vert for vert in Vertex._registry
                           if vert.branch_tag == '']

    Vertex.r2vertices = [vert for vert in Vertex._registry
                         if vert.branch_tag == 'R2']

    Vertex.ctvertices = [vert for vert in Vertex._registry
                         if vert.branch_tag == 'CT']
    ColorflowVertex.store(generation_timestamp, force=force)
    Vertex.store(generation_timestamp, force=force)
    if compute_ct:
      Counterterms.store(generation_timestamp, force=force)

  currentIDs = Vertex.currentIDs

  keys = currentIDs.keys()
  n = 0
  for key in keys:
    n += len(currentIDs[key])

  log('Compute current structures.', 'info')
  PG = ProgressBar(n)
  n = 0

  current_strucs = Vertex.current_strucs
  ls_bases = [[v[0] for v in u] for u in current_strucs.values()]

  # lorentz structure id dict
  ls_tag_id = {}
  # four-fermion flow id
  ff_id = {}

  # lorentz structure propagator extension dict
  rxi_propagator = hasattr(Vertex, 'rxi_propagator') and Vertex.rxi_propagator
  rxi_parameter = rxi_propagator and Vertex.rxi_parameter

  recola_base_dict = {}
  lspext = {}
  for key in keys:
    for ids_to_calc in currentIDs[key]:
      id_number = currentIDs[key].index(ids_to_calc)
      id_tag = key + str(id_number)
      log("id_tag:" + str(id_tag), 'debug')

      status = id_tag + '(/' + str(len(currentIDs[key])) + ')'
      PG.advanceAndPlot(status=status)
      ls_strucs, arguments, prefactors, ffid = Vertex.get_lorentz_structure(ids_to_calc, key)
      wavefunction_dim = 4
      propagator_type = 0
      if (key[-1] == 'S' or key[-1] == 'U'):
        wavefunction_dim = 1
      elif (key[-1] == 'a'):
        if (key[-2] == 'U'):
          wavefunction_dim = 1
        else:
          propagator_type = 1
      if (key[-1] == 'V'):
        propagator_numerator = -I
        propagator_type = 3
      else:
        propagator_numerator = I
      if (key[-1] == 'F'):
        propagator_type = 2
      results, n_particles = reconstruct_lorentz(ls_strucs, arguments,
                                                 prefactors)

      # If fermions take part in a current helicity optimization is turned on
      fermion_out = (propagator_type == 1 or propagator_type == 2)
      fermions_in = [pos + 1 for pos, u in
                     enumerate(re.findall('[A-Z][^A-Z]*', key)[:-1])
                     if (u == 'F' or u == 'Fa')]
      if len(fermions_in) == 0:
        fermions_in = None

      if rxi_propagator and propagator_type == 3:
        # standard rxi propagator
        if rxi_parameter:
          propagator_extensions = [None, 'RXI']
        # decomposed rxi propagator
        else:
          propagator_extensions = [None, 'RXI1', 'RXI2']
      else:
        propagator_extensions = [None]

      for pext in propagator_extensions:
        n = n + 1
        if pext is None:
          id_tag_ext = id_tag
          if use_recola_base and tuple(ids_to_calc) in recola_base:
            recola_base_dict[n] = recola_base[tuple(ids_to_calc)]
          elif use_recola_base:
            from rept1l.baseutils.base_utils import lbs_to_form_expressions
            for ls_struc in ls_strucs:
              coeffs = Vertex.current_strucs[ls_struc]
              for u in coeffs:
                if u[0] in ids_to_calc:
                  for v in u[1]:
                    lbs = [(ls_struc, (v[0], v[2]))]
                    print(v[1], '* ' + lbs_to_form_expressions(lbs)[0])

            if use_recola_base:
              log("No matching recola base for structure: " + id_tag_ext, 'info')
            else:
              log("No matching recola base for structure: " + id_tag_ext, 'debug')
            # print("recola_base:", recola_base)
            # # print("currentIDs:", currentIDs.keys())
            # for key in currentIDs:
            #   print("key:", key)
            #   print("currentIDs[key]:", currentIDs[key])

            # ids_to_calc = [61, 1]
            # for ids_to_calc in currentIDs[key]:
            # print("ids_to_calc:", ids_to_calc)
            # bases = []
            # ls_strucs = []
            # # id_number = currentIDs[key].index(ids_to_calc)
            # # id_tag = key + str(id_number)
            # #
            # # status = id_tag + '(/' + str(len(currentIDs[key])) + ')'
            # # PG.advanceAndPlot(status=status)
            # ids_to_calc = [61]
            # for id_to_calc in ids_to_calc:
            #   ls_index = 0
            #   base_index = 0
            #   for ls in ls_bases:
            #     if id_to_calc in ls:
            #       ls_index = ls_bases.index(ls)
            #       base_index = ls.index(id_to_calc)
            #
            #   ls_struc = current_strucs.keys()[ls_index]
            #   base = current_strucs[ls_struc][base_index]
            #   bases.append(base)
            #   ls_strucs.append(ls_struc)
            #
            # # update bases and ls_strucs
            # bases, ls_strucs = compute_ls_coupling_grouped(bases, ls_strucs)
            #
            # couplings_base = set([q for q in flatten([[[k for k in v[1].free_symbols]
            #                                          for v in u[1]] for u in bases])])
            # couplings_base = sorted(couplings_base, key=lambda x: x.name)
            # prefactors = [[extract_couplings(v[1] * v[2], couplings_base)[1]
            #                for v in u[1]] for u in bases]
            # couplings = [[extract_couplings(v[1] * v[2], couplings_base)[0]
            #               for v in u[1]] for u in bases]
            # arguments = None
            # if any(len(lorentz_base_dict[ls]) > 0 for ls in ls_strucs):
            #   arguments = [[lorentz_base_dict[ls][u[0]] for u in bases[i][1]]
            #                if len(lorentz_base_dict[ls]) > 0 else [[]]
            #                for i, ls in enumerate(ls_strucs)]
            #
            # print("bases:", bases[0])
            # print("ls_strucs:", ls_strucs)
            # for a in arguments:
            #   print("a:", a)
            # print("lorentz_base_dict[ls][u[0]]:",
            #     lorentz_base_dict[ls_strucs[0]][bases[0][0]])


            # lbs = [(Sympify('4*GammaM**2*GammaP**2', (v[0], v[2]))]
            # print(v[1], '* ' + lbs_to_form_expressions(lbs)[0])
            # import sys
            # sys.exit()

        else:
          # adding suffix to extension
          id_tag_ext = id_tag + '_' + pext
          lspext[n] = propagator_extensions.index(pext)

        if write_form:
          if split_form_currents == 0:
            keyhash = id_tag_ext
          elif split_form_currents == 1:
            keyhash = key
          elif split_form_currents == 2:
            keyhash = ''.join(sorted(key))
          formmodsc = FormCurrentPyfort.get_selectcase(keyhash,
                                                       n_particles,
                                                       id_tag_ext)
          try:
            write_form_vertex(formmodsc, id_tag_ext, ls_strucs, arguments,
                              prefactors, propagator_type, pext=pext)
          except Exception:
            log('Failed to derive form vertex for structure: ' +
                str(id_tag_ext), 'error')
            log('See log for more information.', 'warning')
            log('Computing write_form_vertex has failed for the following ' +
                'arguments:', 'debug')
            log('id_tag_ext: ' + str(id_tag_ext), 'debug')
            log('ls_strucs: ' + str(ls_strucs), 'debug')
            log('arguments: ' + str(arguments), 'debug')
            log('prefactors: ' + str(prefactors), 'debug')
            log('propagator_type: ' + str(propagator_type), 'debug')
            raise

        if write_tree:
          if split_tree_currents == 0:
            keyhash = id_tag_ext
          elif split_tree_currents == 1:
            keyhash = key
          elif split_tree_currents == 2:
            keyhash = ''.join(sorted(key))
          treemodsc = TreeCurrentPyfort.get_selectcase(keyhash,
                                                       n_particles,
                                                       id_tag_ext)
          try:
            hc = gendict['helicity_cases']
            write_tree_vertex(treemodsc, hc, results, n_particles,
                              wavefunction_dim, id_tag_ext,
                              fermions_in=fermions_in,
                              out_is_fermion=fermion_out, pext=pext,
                              simplify=simplify_currents,
                              apply_cse=apply_cse_currents)
          except Exception:
            log('Failed to write tree vertex for structure: ' + str(id_tag_ext),
                'error')
            log('See log for more information.', 'warning')
            log('Calling write_tree_vertex failed for the following arguments:',
                'debug')
            log('hc: ' + str(hc), 'debug')
            log('results: ' + str(results), 'debug')
            log('n_particles: ' + str(n_particles), 'debug')
            log('wavefunction_dim: ' + str(wavefunction_dim), 'debug')
            log('id_tag_ext: ' + str(id_tag_ext), 'debug')
            log('fermions_in: ' + str(fermions_in), 'debug')
            log('out_is_fermion: ' + str(fermion_out), 'debug')
            raise

        gendict['class_vertices'].addType(("integer, parameter",
                                           id_tag_ext + " = " + str(n)))
        ls_tag_id[id_tag_ext] = n
        ff_id[n] = ffid

        # Check if the current ids are needed for loop currents. The usual
        # Feynrules vertices are all needed for loop currents whereas
        # counterterms vertices do not need loop currents at one-loop order.
        if key in Vertex.current_types['Tree']:
          has_loop_current = ids_to_calc in Vertex.current_types['Tree'][key]
        else:
          has_loop_current = False

        if has_loop_current and write_loop:
          if split_loop_currents == 0:
            keyhash = id_tag_ext
          elif split_loop_currents == 1:
            keyhash = key
          elif split_loop_currents == 2:
            keyhash = ''.join(sorted(key))
          loopmodsc = LoopCurrentPyfort.get_selectcase(keyhash,
                                                       n_particles,
                                                       id_tag_ext)
          try:
            sclr = gendict['sc_get_lorentz_rank']
            write_loop_vertex(loopmodsc, sclr, results, n_particles,
                              wavefunction_dim, propagator_numerator,
                              id_tag_ext, n, pext=pext,
                              apply_cse=apply_cse_currents,
                              simplify=simplify_currents)
          except Exception:
            log('Failed to write loop vertex for structure: ' + str(id_tag_ext),
                'error')
            log('See log for more information.', 'warning')
            log('Calling write_loop_vertex failed for the following arguments:',
                'debug')
            log('results: ' + str(results), 'debug')
            log('n_particles: ' + str(n_particles), 'debug')
            log('wavefunction_dim: ' + str(wavefunction_dim), 'debug')
            log('propagator_numerator: ' + str(propagator_numerator), 'debug')
            log('id_tag_ext: ' + str(id_tag_ext), 'debug')
            log('n: ' + str(n), 'debug')
            raise

  # Counterterms parameters depend on (Tree-) couplings which might not be
  # registered
  if subs_reno_constants:
    log('Loading counterterm couplings.', 'info')
    from rept1l.renormalize import RenoConst as RC
    reno_cp = RC.get_coupling_dependence()
    FRC.update_regged_couplings('Tree', *reno_cp)

  # Due to how couplings are are registered, it may happen that couplings depend
  # on other couplings which are not registered because they do not show up
  # explicitly in currents. We need to include those couplings in order to
  # resolve the dependencies correctly.
  log('\n', 'info')
  log('Resolving coupling dependence.', 'info')
  FRC.enlarge_regged_couplings()

  # order the couplings in a way that they can be computed successively
  log('Sorting and exporting couplings.', 'info')
  log('\n', 'info')
  couplings_ordered = FRC.regged_couplings_ordered(plotProgress=True)
  not_used_couplings = [u for u in FRC.coupling_values
                        if u not in couplings_ordered]

  #if len(not_used_couplings) > 0:
    #answer = query_yes_no('There are ' + str(len(not_used_couplings)) + ' ' +
                          #'unused couplings. Do you want to remove them from '+
                          #'storage (In case of doubt [N]). [Y]: The system ' +
                          #'will remove the couplings.')
    #if answer is True:
      #FRC.remove_couplings(not_used_couplings)

  # export couplings to the fortran module
  fill_couplings(couplings_ordered,
                 gendict['class_couplings'],
                 gendict['class_ctcouplings'],
                 couplings_info, ctcouplings_info, compute_ct=compute_ct,
                 compute_r2=compute_r2)
  c_tag_id = {c: c_id for c_id, c in enumerate(couplings_ordered)}

  for vid, vert in iteritems(Vertex.simple_output):
    branches = {'': gendict['currentbare'],
                'ct': gendict['currentct'],
                'r2': gendict['currentr2']}
    for b, test in iteritems(branches):
      vert_restr = {key: val for key, val in iteritems(vert)
                    if val['btag'] == b}
      nbr = str(len(vert_restr))
      test + ('#V ID ' + str(vid))
      test + (str(vid) + ' ' + nbr)
      for br, branch in iteritems(vert):
        # replace couplings by couplings ids
        couplings = branch['couplings']
        couplings = ' '.join(str(c_tag_id[u.name]) if u != 0 else '0'
                             for u in couplings)
        # replace lorentz by lorentz ids
        lid = branch['lid'][branch['order']]
        lid = str(ls_tag_id[lid])

        # replace color coefficient by id
        cid = str(branch['cid'])
        coid = str(Vertex.colorcoeffs[branch['c_coef']])

        order = ' '.join(str(u[1]) for u in branch['order'])
        test + (cid + ' ' + coid + ' ' + lid + ' ' + order)
        test + (couplings)

  colorcoeffs_inv = {val: key for key, val in iteritems(Vertex.colorcoeffs)}
  ncc = len(Vertex.colorcoeffs)
  for i in range(1, ncc + 1):
    rp, ip = colorcoeffs_inv[i]
    rp = export_fortran(str(rp))
    ip = export_fortran(str(ip))
    cval = 'cmplx(' + rp + ', ' + ip + ', kind=dp)'
    gendict['class_vertices'].addType(('complex(dp), parameter',
                                       'c' + str(i) + ' = ' + cval))

    gendict['class_vertices'].addType(("character(len=20), parameter",
                                       'c' + str(i) + 'f = "' +
                                       Vertex.colorcoeffs_form[i - 1] + '"'))

  if any(len(u) > 20 for u in Vertex.colorcoeffs_form):
    log('Encountered color coefficient string which is longer than 20: ' +
        str(Vertex.colorcoeffs_form), 'warning')

  # add colocoefficients as complex variables
  cc = ['c' + str(i)for i in range(1, ncc + 1)]
  cc = '(/' + ', '.join(cc) + '/)'
  gendict['class_vertices'].addType(("complex(dp), dimension(1:" + str(ncc) +
                                     ")", "colorcoeffs = " + cc))

  # add colocoefficients as form expressions
  ccf = ['c' + str(i) + 'f' for i in range(1, ncc + 1)]
  ccf = '(/' + ', '.join(ccf) + '/)'
  gendict['class_vertices'].addType(("character(len=20), dimension(1:" +
                                     str(ncc) +
                                     ")", "colorcoeffs_form = " + ccf))

  if store_intermediate:
    FRC.store(generation_timestamp, force=force)

  # store auxiliary variables
  if lorentz_lib.max_non_vectors_tree > 0:
    adds = map(lambda y: "x" + str(y), range(lorentz_lib.max_non_vectors_tree))
    adds = ', '.join(adds)
    for currkey in TreeCurrentPyfort.registry:
      mod = TreeCurrentPyfort.get_module(*currkey)
      mod.addType(("complex (dp)", adds))

  if lorentz_lib.max_vectors_tree > 0:
    # add vector-valued types to subroutine_compute_tree(subct)
    adds = map(lambda y: "x" + str(y) + "vec",
               range(lorentz_lib.max_vectors_tree))
    adds = ', '.join(adds)
    for currkey in TreeCurrentPyfort.registry:
      mod = TreeCurrentPyfort.get_module(*currkey)
      mod.addType(("complex (dp), dimension(0:3)", adds))

  if lorentz_lib.max_non_vectors_loop > 0:
    # add types to subroutine_compute_loop(subcl)
    adds = map(lambda y: "x" + str(y), range(lorentz_lib.max_non_vectors_loop))
    adds = ', '.join(adds)
    for currkey in LoopCurrentPyfort.registry:
      mod = LoopCurrentPyfort.get_module(*currkey)
      mod.addType(("complex (dp)", adds))

  if lorentz_lib.max_vectors_loop > 0:
    # add vector-valued types to subroutine_compute_loop(subcl)
    adds = map(lambda y: "x" + str(y) + "vec",
               range(lorentz_lib.max_vectors_loop))
    adds = ', '.join(adds)

    for currkey in LoopCurrentPyfort.registry:
      mod = LoopCurrentPyfort.get_module(*currkey)
      mod.addType(("complex (dp), dimension(0:3)", adds))

  # rank increase information of lorentz structures
  lorentz_rank = [str(lorentz_lib.lorentz_rank[i]) if i in
                  lorentz_lib.lorentz_rank else str(-1) for i in range(1, n + 1)]
  lorentz_rank = "(/" + ', '.join(lorentz_rank) + "/)"
  gendict['class_vertices'].addType(("integer, dimension(1:" + str(n) + ")",
                                     "lorentz_rank = " + lorentz_rank))

  # four-fermion ids
  ff_ids = (str(ff_id[i]) for i in range(1, n + 1))
  ff_ids = "(/" + ', '.join(ff_ids) + "/)"
  gendict['class_vertices'].addType(("integer, dimension(1:" + str(n) + ")",
                                     "ff_ids = " + ff_ids))

  # str representations
  for k in recola_base_dict:
    v = recola_base_dict[k]
    if type(v) is dict:
      iid = v['integer_id']
      sid = v['string_id']
      if sid:
        gendict['class_vertices'].addType(('integer, parameter', sid + ' = ' + str(iid)))
        recola_base_dict[k] = sid
      else:
        recola_base_dict[k] = iid
  rb = [str(recola_base_dict[i]) if i in recola_base_dict else str(0) for i in range(1, n + 1)]
  rb = "(/" + ', '.join(rb) + "/)"
  gendict['class_vertices'].addType(("integer, dimension(1:" + str(n) + ")",
                                     "recola_base = " + rb))

  # for i in range(1, n + 1):
  #   if i in recola_base_dict:
  #     print(i, recola_base_dict[i])
  #   else:
  #     print(i, str(0))
  # print("lorentz_lib.__dict__:", lorentz_lib.__dict__)
  # sys.exit()

  # propagator extension information of lorentz structures
  propagator_extension = [str(lspext[i]) if i in lspext else str(0)
                          for i in range(1, n + 1)]
  propagator_extension = "(/" + ', '.join(propagator_extension) + "/)"
  gendict['class_vertices'].addType(("integer, dimension(1:" + str(n) + ")",
                                     "propagator_extension = " +
                                     propagator_extension))

  mlmb = max_leg_multiplicity_block
  mlmb.addPrefix("! Defines the maximum allowed number" +
                 " of legs participating at a vertex")
  mlmb.addType(("integer, parameter", "max_leg_multiplicity = " +
                str(max_leg_multiplicity)))
  mlmb.addLine()

  PG.plotProgress()

  n_colors = len(ColorflowVertex.colors_dict)

  n_vertices_max = len(Vertex.vertex_ids)
  # Sa 26 Jul 2014 16:25:18 CEST
  #n_vertices_max = len(Vertex.lorentzTree_dict.keys())
  #if 'lorentzR2_dict' in Vertex.__dict__:
    #n_vertices_max += len(Vertex.lorentzR2_dict.keys())
  #if 'lorentzCT_dict' in Vertex.__dict__:
    #n_vertices_max += len(Vertex.lorentzCT_dict.keys())

  res_n_vertices = "# number of possible vertices: " + str(n_vertices_max)
  res_n_vertices += " " * (80 - len(res_n_vertices) - 1) + '#'
  dict_size = get_prime_number(2 * n_vertices_max + 2)

  mvb = max_vertices_block
  mvb.addType(("integer, parameter", "n_vertices = " + str(n_vertices_max)))
  mvb.addType(("type(Vertex)", "vertices(0:n_vertices-1)"))
  mvb.addLineD()

  allocate_dict_block.addStatement("call dict_init(" + str(dict_size) + ")", 2)
  allocate_dict_block.addStatement("call createCurrents", 2)
  allocate_dict_block.addLine()

  res_res = "# Results:"
  res_res += " " * (80 - len(res_res) - 1) + '#'

  # log number of vertices
  res_v = "# " + str(n_vertices_max) + " different vertices."
  res_v += " " * (80 - len(res_v) - 1) + '#'

  # log vertex dictionary size
  res_d = "# setting dictionary size to " + str(dict_size)
  res_d += " " * (80 - len(res_d) - 1) + '#'

  # log number of different lorentz structures
  res_l = "# " + str(n) + " different lorentz contractions."
  res_l += " " * (80 - len(res_l) - 1) + '#'

  # log number of different colorflows
  res_c = "# " + str(n_colors) + " different color flows."
  res_c += ' ' * (80 - len(res_c) - 1) + '#'

  log('\n', 'info')
  log("#" * 80, 'info')
  log(res_res, 'info')
  log(res_v, 'info')
  log(res_d, 'info')
  log(res_l, 'info')
  log(res_c, 'info')
  log("#" * 80, 'info')


if __name__ == "__main__":
  import doctest
  doctest.testmod()
