#==============================================================================#
#                                 __init__.py                                  #
#==============================================================================#

from .colorflow import ColorflowVertex, permute_colorid
from .class_colorflow import gen_color_class, create_colorflow_entry

__author__ = "Jean-Nicolas lang"
__date__ = "31. 3. 2016"
__version__ = "0.9"
