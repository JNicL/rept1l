#==============================================================================#
#                              class_colorflow.py                              #
#==============================================================================#

""" Generator for the Fortran module class_colorflow.rcl """

#============#
#  Includes  #
#============#

import os
from rept1l.pyfort import modBlock, containsBlock, loadBlock, spaceBlock
from rept1l.pyfort import selectBlock, caseBlock, ifBlock, subBlock, doBlock

#=============#
#  structure  #
#=============#

#/---------------------------\#
# module class_colorflow      #
#                             #
#  prefix                     #
#                             #
#  contains                   #
#                             #
#   subroutines computeCorr1  #
#                             #
#     prefix                  #
#                             #
#     loop over colorflowids  #
#                             #
#       select case colorflow #
#                             #
#   subroutines computeCorr2  #
#             .               #
#             .               #
#                             #
# end module                  #
#\---------------------------/#


#==============================================================================#
#                               gen_color_class                                #
#==============================================================================#

def gen_color_class(max_npoint=8, tabs=2):
  path = os.path.dirname(os.path.realpath(__file__)) + '/FortranTemplates/'
  class_colorflow = modBlock("class_colourflow")
  contains_colorflow = containsBlock(tabs=tabs - 1)

  args = ["colorflowIds", "csIn", "U1GluonIn", "csOut",
          "U1GluonOut", "csLast", "select_branch", "last", "pr", "lp",
          "legsE", "ncpower"]
  cc_args = ", ".join(args)
  cc_args2 = ", ".join(['nbranches'] + args)

  #=========================================#
  #  selcompcc: ColorCorrelation Interface  #
  #=========================================#
  # Build up the colorcorrelation computation which, depending on n_In selects
  # the appropritate subroutine
  compcc = subBlock('compute_colour_correlation_mdl', cc_args + ', n_In', tabs=tabs)
  compcc.addPrefix(loadBlock(path + 'color_corr_prefix2.f90'))

  compcc.addType(('integer', 'nbranches'))
  compcc += 'nbranches = size(select_branch)'
  selcompcc = selectBlock('n_In', tabs=tabs + 1)
  for np in range(2, max_npoint + 1):
    cb = caseBlock(str(np - 1), tabs=tabs + 1)
    selcompcc + cb
    cb + ('call compute_colour_correlation' + str(np) + '(' +
          cc_args2 + ')')

  cb = caseBlock('default', tabs=tabs + 1)
  cb + ('call error_mdl("No colourflow for `n_In`< 1 | `n_In`> 7.", ' +
        'where="compute_colour_correlation_mdl")')
  selcompcc + cb

  subroutine_computeColorCorrelation = []
  for np in range(2, max_npoint + 1):
    sub = subBlock("compute_colour_correlation" + str(np),
                   arg=cc_args2, tabs=tabs)
    subroutine_computeColorCorrelation.append(sub)

  compcc + selcompcc
  contains_colorflow + compcc
  contains_colorflow + "\n"

  dowhileColorflow = []
  for np in range(2, max_npoint + 1):
    dow = doBlock("while(cf < size(colorflowIds))", tabs=tabs + 1)
    dowhileColorflow.append(dow)

  selectCaseColorflow = []
  selectCaseColorflowBlock = []
  for np in range(2, max_npoint + 1):
    sel = selectBlock("colorflowIds(cf)", tabs=tabs + 2)
    ssel = spaceBlock(tabs=tabs + 2)
    sel + ssel
    cb = caseBlock('default', tabs=tabs + 2)
    cb + ("call error_mdl('Unknown colourflow id.', " +
          "where='compute_colour_correlation" + str(np) + "')")
    sel + cb
    selectCaseColorflow.append(ssel)
    selectCaseColorflowBlock.append(sel)

  container1Colorflow2 = spaceBlock(tabs=tabs + 1)
  container1Colorflow3 = spaceBlock(tabs=tabs + 1)
  container1Colorflow4 = spaceBlock(tabs=tabs + 1)

  container1Colorflow = [container1Colorflow2,
                         container1Colorflow3,
                         container1Colorflow4]

  container1Colorflow = []
  for np in range(2, max_npoint + 1):
    con = spaceBlock(tabs=tabs + 1)
    container1Colorflow.append(con)

  container2Colorflow = spaceBlock(tabs=tabs + 1)

  class_colorflow.addPrefix("use input_mdl, only: error_mdl", 1)
  class_colorflow.addPrefix("use class_vertices", 1)
  class_colorflow.addPrefix("implicit none", 1)

  #=============#
  #  hierarchy  #
  #=============#

  class_colorflow + contains_colorflow
  for i, ccc in enumerate(subroutine_computeColorCorrelation):
    ccc.addPrefix(loadBlock(path + 'color_corr_prefix1.f90'))
    contains_colorflow + ccc
    contains_colorflow + '\n'
    ccc += "bin_color = 1"
    ccc += "cf = 0"
    ccc + dowhileColorflow[i]
    dowhileColorflow[i] + loadBlock(path + 'doloop.f90')
    dowhileColorflow[i] + container1Colorflow[i]
    dowhileColorflow[i] + container2Colorflow
    container1Colorflow[i] + selectCaseColorflowBlock[i]
    ccc + "bin_color = bin_color - 1"
    ccc + last_loop_color(3)
    ccc + loadBlock(path + 'color_corr_st2.f90')

  container2Colorflow += "cf = cf +1"
  container2Colorflow += "bin_color = bin_color + 1"

  return class_colorflow, selectCaseColorflow


def color(particle_index):
  return 'colorP(' + str(particle_index) + ')'


def anticolor(particle_index):
  return 'anticolorP(' + str(particle_index) + ')'


#==============================================================================#
#                            gen_color_correlation                             #
#==============================================================================#

def gen_color_correlation(colorflow, final_particle=None, last=False):
  """
  Generates the fortran code for computing color correlations.  In the recursive
  approach only the colors from incoming currents are known. The routines
  compute all possible outcomes for the outgoing current given the incoming
  currents.

  >>> colorflow = ((1,2),(2,3),(3,1))

  In this example the colorflows are as follows:
  >>> cs = gen_color_correlation(colorflow, 3, False)

  color particle 2 to anticolor particle 3
  >>> cs[0]
  'csTmp(-1,bin_color) = csIn(2,-1)'

  color particle 3 to anticolor particle 1
  >>> cs[1]
  'csTmp(0,bin_color) = csIn(1,0)'

  color particle 1 to anticolor particle 2
  >>> cs[2]
  ['csTmp(csIn(1,-1), bin_color) = csIn(2,0)']
  """

  colorcorrelation = [(u[0], u[1]) for u in colorflow]
  incomig_gluons = []
  for i in range(1, final_particle):
    # pairs of i
    pi = (sum(u.count(i) for u in colorcorrelation))
    if pi == 2:
      incomig_gluons.append(i)
    elif pi > 2:
      raise Exception('Strange colour correlation: ' + str(colorcorrelation))

# removing correlations with final particle
  colorcorrelation_tmp = colorcorrelation[:]
  final_particle_colors = []
  for pos, i in enumerate(colorcorrelation_tmp):
    if (final_particle in i):
      final_particle_colors.append(i)
      colorcorrelation.remove(i)

  set_color = False
  set_anticolor = False

  t1 = 'csTmp(0,bin_color)'
  t2 = 'csTmp(-1,bin_color)'
  t3 = []
  t3f = []

  gluon_neutral = False
  final_particle_colored = False
  final_particle_single_colored = (len(final_particle_colors) == 1)
  final_particle_gluon = (len(final_particle_colors) == 2)
  ifflags = ''

  for i in final_particle_colors:
    if (i[0] == final_particle or i[1] == final_particle):
      final_particle_colored = True
      if (i[0] == final_particle and i[1] == final_particle):
        gluon_neutral = True

  if(not gluon_neutral and final_particle_colored):
    for fpcolor in final_particle_colors:
      if (fpcolor[0] == final_particle):
        t1 += ' = csIn(' + str(fpcolor[1]) + ',0)'
        set_anticolor = True
    if(set_anticolor is False):
      t1 += ' = 0'

    for fpcolor in final_particle_colors:
      if (fpcolor[1] == final_particle):
        t2 += ' = csIn(' + str(fpcolor[0]) + ',-1)'
        set_color = True

    if(set_color is False):
      t2 += ' = 0'

    for colcor in colorcorrelation:
      if(colcor[0] == colcor[1]):
        ifflags = "U1GluonIn(" + str(colcor[0]) + ")"
      t3.append("csTmp(csIn(" + str(colcor[0]) + ",-1), bin_color) = " +
                "csIn(" + str(colcor[1]) + ",0)")

    if final_particle_gluon and len(incomig_gluons) == 1:
      ifflags = 'U1GluonOut = U1GluonIn(' + str(incomig_gluons[0]) + ')'

  elif(not final_particle_colored):
    t1 += ' = 0'
    t2 += ' = 0'
    for colcor in colorcorrelation:
      t3.append("csTmp(csIn(" + str(colcor[0]) + ",-1), bin_color) = " +
                "csIn(" + str(colcor[1]) + ",0)")
  else:
    # Final particle is U(1) gluon
    t1 += ' = 0'
    t2 += ' = 0'
    ifflags = 'gluonneutral'

  if(final_particle_single_colored and (not gluon_neutral)):
    if (final_particle_colors[0][0] == final_particle):
      t3f.append("csTmp(csLast(-1), bin_color) = csTmp(0, bin_color)")
    else:
      t3f.append("csTmp(csTmp(-1, bin_color), bin_color) = csLast(0)")

  elif(final_particle_colored):
    if (gluon_neutral):
      t3f.append("csTmp(csLast(-1), bin_color) = csLast(0)")
      for colcor in colorcorrelation:
        t3f.append("csTmp(csIn(" + str(colcor[0]) + ",-1), bin_color) = " +
                   "csIn(" + str(colcor[1]) + ",0)")

    else:
      t3f.append("csTmp(csLast(-1), bin_color) = csTmp(0, bin_color)")
      t3f.append("csTmp(csTmp(-1, bin_color), bin_color) = csLast(0)")

  return t2, t1, t3, ifflags, t3f


def create_colorflow_entry(colorflow, color_id, n_In, selectCaseColorflow,
                           tabs=4):
  """ Generates a colorflow entry in the Fortran module.

  :colorflow: A colorkey which is a tuple of tuples, e.g. ((1,2), (2,1))
  :color_id: The id associated the the colorflow `colorflow`
  :n_In: Number of incoming particles minus 2: 0 = 2 leg vertex
                                               1 = 3 leg vertex
  """
  cb = caseBlock(str(color_id), tabs=tabs)
  if len(colorflow) > 0:
    colorflow_str = ' '.join('d^' + str(u[0]) + '_' + str(u[1]) for u in colorflow)
    selectCaseColorflow[n_In].addcomment(colorflow_str, tabs=-1)
  selectCaseColorflow[n_In] + cb
  t1, t2, t3, ifflags, t3f = gen_color_correlation(colorflow, n_In + 2)

  set_select_branch = "select_branch(bin_color) = cf + 1"
  # This is normal propagation of the colour flow.
  # The outgoing gluon can become a U1Gluon, even if the colourflow is not U1.
  # E.g. two gluons 1,2 may be connected, via (1,2), (2,1),
  # but if the incoming one becomes a U1, then: (1,2), (2,1) -> (2,2).
  if (len(ifflags) == 0 or 'U1GluonOut = U1GluonIn' in ifflags):
    cb + t1
    cb + t2
    for t3ff in t3:
      cb + t3ff
    ib = ifBlock('last', tabs=tabs + 1)
    cb + ib
    for t3ff in t3f:
      ib + t3ff
    colorEQ0 = "csTmp(-1,bin_color) = 0"
    acolorEQ0 = "csTmp(0,bin_color) = 0"
    ib + colorEQ0
    ib + acolorEQ0
    cb + set_select_branch
    if len(ifflags) > 0:
      cb + ifflags

  elif(not (ifflags == 'gluonneutral')):
    # This is the conditional situation if one of the incoming particles is a U1
    # Gluon.
    ibf = ifBlock(ifflags, tabs=tabs + 1)
    cb + ibf
    ibf + t1
    ibf + t2
    for t3ff in t3:
      ibf + t3ff

    ibl = ifBlock('last', tabs=tabs + 2)
    ibf + ibl
    for t3ff in t3f:
      ibl + t3ff

    colorEQ0 = "csTmp(-1,bin_color) = 0"
    acolorEQ0 = "csTmp(0,bin_color) = 0"
    ibl + colorEQ0
    ibl + acolorEQ0
    ibf + set_select_branch
    (ibf > 0) + 'else'
    ibf + "bin_color = bin_color - 1"

  else:
    # This is the conditional situation if the outgoing particle is a U1 Gluon.
    lg = ifBlock('last', tabs=tabs + 1)
    cb + lg
    for t3ff in t3:
      lg + t3ff
    for t3ff in t3f:
      lg + t3ff
    colorEQ0 = "csTmp(-1,bin_color) = 0"
    acolorEQ0 = "csTmp(0,bin_color) = 0"
    lg + colorEQ0
    lg + acolorEQ0
    lg + set_select_branch
    (lg > 0) + "else"
    lg + "bin_color = bin_color - 1"
    cb + "U1GluonOut = .true."

  return selectCaseColorflow


#####################
#  last_loop_color  #
#####################

def last_loop_color(tabs):
  path = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                      'FortranTemplates')
  last_loop = loadBlock(os.path.join(path, "loopcolor.f90"), tabs=tabs)
  return last_loop


if __name__ == "__main__":
  # import doctest
  # doctest.testmod()

  colorflow = [(1, 1), (2, 3), (3, 2), ]
  scc = [spaceBlock(), spaceBlock()]
  create_colorflow_entry(colorflow, '0', 1, scc)
  scc[1].printStatment()
