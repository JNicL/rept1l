#==============================================================================#
#                                 colorflow.py                                 #
#==============================================================================#

#============#
#  Includes  #
#============#

import sys
import os
import re
try:
  from types import ListType
  list_type = ListType
except ImportError:
  list_type = list
from six import with_metaclass
from six import itervalues

from sympy import Symbol, I, Poly, sympify, sqrt, simplify

from rept1l.helper_lib import Storage, SympyParsing, string_subs
from rept1l.formutils import FormPype
from rept1l.combinatorics import fold, permute_particles, lorentz_base

import rept1l.Model as Model
model = Model.model

#===========#
#  Methods  #
#===========#

def FreeIndices(i=-1):
  while True:
    i += 1
    yield "l" + str(i)

def ColorDelta(i=-1):
  while True:
    i += 1
    yield "C" + str(i)

def Coupling(i=-1):
  while True:
    i += 1
    yield "C" + str(i)

def Lorentz(i=-1):
  while True:
    i += 1
    yield "L" + str(i)

#==============================================================================#
#                                 to_colorkey                                  #
#==============================================================================#

def to_colorkey(colordict):
  """
  Transforms a colordict into a colorkey.

  :param colordict: Colordict where the keys are the colors and values the
                    anticolors (direction of colorflow)
  :type  colordict: dict of integer keys and values

  :return: tuple of pairs of color and anticolor

  >>> to_colorkey({1: 2, 2: 3, 3:1})
  ((1, 2), (2, 3), (3, 1))
  """
  return tuple(sorted((color, colordict[color]) for color in colordict))

#==============================================================================#
#                               permute_colorkey                               #
#==============================================================================#

def permute_colorkey(colorkey, permutation):
  """
  >>> colorkey = ((1, 2), (2, 3), (3, 1))
  >>> permute_colorkey(colorkey, [1, 0, 2])
  ((2, 1), (1, 3), (3, 2))
  """
  c = [[v for v in u] for u in colorkey]
  return tuple(sorted(tuple(u) for u in permute_particles(c, permutation)))

#==============================================================================#
#                               permute_colorid                                #
#==============================================================================#

def permute_colorid(cs_id, permutation):
  """ Returns the permuted color structure id (cs_id).
  The methods retrieves the colorstructure given the cs_id and permutes it.
  Then the cs_id of the resulting colorstructure is retrieved.

  :param cs_id: The color structure id which should be permuted
  :type  cs_id: int

  :param permutation: permutation list
  :type  permutation: list

  >>> colors_dict = {(((1, 2), (2, 3), (3, 1)), 1): 0}
  >>> ColorflowVertex.colors_dict = colors_dict
  >>> permute_colorid(0, [1, 0, 2])
  1
  >>> colors_dict[(((2, 1), (1, 3), (3, 2)), 1)]
  1
  """
  try:
    n_In = len(permutation) - 2
    #ids = [pos for pos, u in enumerate(colors_dict.values()) if u == cs_id]
    #colorkey = [ckey for ckey, n_In_t in colors_dict.keys() if n_In_t == n_In]
    colorkey = [u for u in ColorflowVertex.colors_dict
                if (ColorflowVertex.colors_dict[u] == cs_id and u[1] == n_In)]
    assert(len(colorkey) == 1)
    colorkey = colorkey[0][0]
  except:
    raise

  if colorkey == ():
    return cs_id

  colorkey_perm = permute_colorkey(colorkey, permutation)
  if (colorkey_perm, n_In) not in ColorflowVertex.colors_dict:
    new_id = len(ColorflowVertex.colors_dict)
    ColorflowVertex.colors_dict[(colorkey_perm, n_In)] = new_id
    return new_id
  else:
    return ColorflowVertex.colors_dict[(colorkey_perm, n_In)]

#==============================================================================#
#                                 to_colordict                                 #
#==============================================================================#

def to_colordict(colorkey):
  """
  Transforms a colorkey into a colordict

  :param colorkey: Pairs of color and anticolor
  :type  colorkey: Tuple of pairs(tuple) of integers

  :return: colordict

  >>> to_colordict(((1, 2), (2, 3), (3, 1)))
  {1: 2, 2: 3, 3: 1}
  """
  return {u[0]: u[1] for u in colorkey}

#==============================================================================#
#                                sort_colorflow                                #
#==============================================================================#

def sort_colorflow(colorcorrelations_list):
  """
  Sorts a list of colorcorrelations according to the colorflow. Used for
  computing the lorentz structure of the four gluon vertex.

  :return: dict where keys are colorflows and values are tuples of the position
           of the coloflow in the list and the colorfactor

  :param colorcorrelations_list: List of colorcorrelations
  :type  colorcorrelations_list: List of lists of tuples. First entry of the
                                 tuple is a colordict and the second entry is
                                 the colorfactor

  >>> sort_colorflow([[({1: 2, 2: 3, 3: 1}, 1)], [({1: 2, 2: 3, 3: 1}, -1)]])
  {((1, 2), (2, 3), (3, 1)): [(0, 1), (1, -1)]}
  """
  ret = {}
  for i, colorcorrelations in enumerate(colorcorrelations_list):
    for colorcorrelation in colorcorrelations:
      colordict, colorfactor = colorcorrelation
      colorkey = to_colorkey(colordict)
      if colorkey not in ret:
        ret[colorkey] = [(i, colorfactor)]
      else:
        ret[colorkey].append((i, colorfactor))
  return ret

#==============================================================================#
#                              diagonalize_color                               #
#==============================================================================#

def diagonalize_color(sorted_cf):
  """
  Different colorstructures can have same colorflow components as it is the case
  for the fourgluon vertex.  Diagonalize colors takes a sorted colorflow and
  orders the result according to the colorflow.

  :return: List of pairs. First element is a dict where the keys are the
           colorflow position defined by sort_colorflow and the values are the
           relative factor with which the colorflow needs to be taken. The
           second element is the global factor.

  >>> cf1 = Colorflow('f(-1,1,2)*f(-1,3,4)', [3, 3, 3, 3], debug=True)
  >>> cf2 = Colorflow('f(-1,1,3)*f(-1,2,4)', [3, 3, 3, 3], debug=True)
  >>> cf3 = Colorflow('f(-1,1,4)*f(-1,2,3)', [3, 3, 3, 3], debug=True)
  >>> ccl = [cf1.get_colorcorrelation(), cf2.get_colorcorrelation(),
  ...  cf3.get_colorcorrelation()]
  >>> sorted_cf = sort_colorflow(ccl)
  >>> diagonalize_color(sorted_cf)
  [[{1: 1, 2: 1}, -1/2], [{1: 1, 2: 1}, -1/2], [{0: 1, 1: 1}, 1/2], \
[{0: 1, 2: -1}, -1/2], [{0: 1, 1: 1}, 1/2], [{0: 1, 2: -1}, -1/2]]


  [{0: 1, 2: -1}, -1] means: First colorflow with factor 1, second colorflow
  with factor -1. After superposition multiply with -1.
  """
  ret = []
  for colorkey in sorted_cf:
    ids = [u[0] for u in sorted_cf[colorkey]]
    cfs = [u[1] for u in sorted_cf[colorkey]]
    add_rule = {}
    for id_i, u in enumerate(cfs[:]):
      add_rule[ids[id_i]] = sympify(u/cfs[0]).subs({'n': 3})
    cfs = cfs[0]
    ret.append([add_rule, cfs])
  return ret

#==============================================================================#
#                                 color_sorted                                 #
#==============================================================================#

def color_sorted(color_v, couplings_v, lorentz_v):
  """
  Computes the color sorted expression of a vertex. A vertex is given
  by the rule: color_vector^T * coupling_matrix * lorentz_vector.

  :return: {(color_vector)_i: (coupling_matrix * lorentz_vector)_i}

  >>> color = ['f(-1,1,2)*f(3,4,-1)',
  ...          'f(-1,1,3)*f(2,4,-1)',
  ...          'f(-1,1,4)*f(2,3,-1)']
  >>> lorentz = ['VVVV1', 'VVVV3', 'VVVV4']
  >>> couplings = {(0, 0): 'GC_12', (1, 1): 'GC_12', (2, 2): 'GC_12'}
  >>> cs, cd, ld = color_sorted(color, couplings, lorentz)
  >>> cs['f(-1,1,3)*f(2,4,-1)']
  C0*L1
  >>> cs['f(-1,1,4)*f(2,3,-1)']
  C0*L2
  >>> cs['f(-1,1,2)*f(3,4,-1)']
  C0*L0
  >>> cd
  {'GC_12': 'C0'}
  >>> ld['VVVV4']
  'L2'
  >>> ld['VVVV1']
  'L0'
  >>> ld['VVVV3']
  'L1'
  """
  color_sorted = {}
  lorentz_dict = {}
  coupling_dict = {}
  cc = Coupling()
  ll = Lorentz()
  for c_i, color in enumerate(color_v):
    expr = 0
    for l_i, lorentz in enumerate(lorentz_v):
      if (c_i, l_i) in couplings_v:
        coupl = couplings_v[(c_i, l_i)]
        if coupl not in coupling_dict:
          cc_sym = next(cc)
          coupling_dict[coupl] = cc_sym
        if lorentz not in lorentz_dict:
          ll_sym = next(ll)
          lorentz_dict[lorentz] = ll_sym
        expr += (Symbol(coupling_dict[coupl]) *
                 Symbol(lorentz_dict[lorentz]))
    color_sorted[color] = expr
  return color_sorted, coupling_dict, lorentz_dict

#==============================================================================#
#                               colorflow_vertex                               #
#==============================================================================#

def colorflow_vertex(color_sorted, spins, debug=False):
  """
  Computes the colorflow of a color sorted vertex.

  :return: colorflow dict: the keys are colorkeys and the values are tuples
           where the first entry is a product of couplings and lorentz
           structures and the second entry is the colorfactor.  The symbols used
           in the products are the ones defined in the lorentz and coupling
           dicts.

  >>> C0 = Symbol('C0')
  >>> L0 = Symbol('L0')
  >>> L1 = Symbol('L1')
  >>> L2 = Symbol('L2')
  >>> spins = [3, 3, 3, 3]
  >>> color_sorted = {'f(-1,1,3)*f(2,4,-1)': C0*L1,
  ...                 'f(-1,1,4)*f(2,3,-1)': C0*L2,
  ...                 'f(-1,1,2)*f(3,4,-1)': C0*L0}
  >>> cv = colorflow_vertex(color_sorted, spins, debug=True)
  >>> cv[((1, 3), (2, 4), (3, 2), (4, 1))]
  (C0*L1 + C0*L2, -1/2)
  >>> cv[((1, 4), (2, 3), (3, 1), (4, 2))]
  (C0*L1 + C0*L2, -1/2)
  >>> cv[((1, 2), (2, 4), (3, 1), (4, 3))]
  (C0*L0 + C0*L1, 1/2)
  >>> cv[((1, 4), (2, 1), (3, 2), (4, 3))]
  (C0*L0 - C0*L2, -1/2)
  >>> cv[((1, 3), (2, 1), (3, 4), (4, 2))]
  (C0*L0 + C0*L1, 1/2)
  >>> cv[((1, 2), (2, 3), (3, 4), (4, 1))]
  (C0*L0 - C0*L2, -1/2)


  >>> C1 = Symbol('C1')
  >>> C2 = Symbol('C2')
  >>> C3 = Symbol('C3')
  >>> color_sorted= {'Identity(1,2)*Identity(3,4)': C0*L1,
  ...                'Identity(1,4)*Identity(2,3)': C1*L2,
  ...                'T(-1,2,1)*T(-1,4,3)': C2*L1,
  ...                'T(-1,2,3)*T(-1,4,1)': C3*L2}
  >>> spins = [-2, 2, -2, 2]
  >>> colorflow_vertex(color_sorted, spins, debug=True)
  {((1, 4), (3, 2)): (-6*C1*L2 - 3*C2*L1 + C3*L2, -1/(2*n)), \
((1, 2), (3, 4)): (C0*L1 - C2*L1/6 + C3*L2/2, 1)}
  """
  colorflow = []
  for color in color_sorted:
    tmp = Colorflow(color, spins, debug=debug)
    colorflow.append(tmp.get_colorcorrelation())
  sorted_cf = sort_colorflow(colorflow)
  dc = diagonalize_color(sorted_cf)
  cf_vertex = {}
  color_sorted_values = list(itervalues(color_sorted))
  sorted_cf_keys = list(sorted_cf)
  for i, addrule_cf in enumerate(dc):
    add_rule, cf = addrule_cf
    expr = 0
    for pos in add_rule:
      expr += color_sorted_values[pos]*add_rule[pos]
    cf_vertex[sorted_cf_keys[i]] = (expr, cf)
  return cf_vertex

#==============================================================================#
#                                piecewise_add                                 #
#==============================================================================#

def piecewise_add(list_a, list_b):
  len_a = len(list_a)
  assert(len_a == len(list_b))
  return list(map(lambda i: list_a[i] + list_b[i], range(len_a)))

#==============================================================================#
#                          colorflow_coupling_sorted                           #
#==============================================================================#

def colorflow_coupling_sorted(colorflow_vertex, coupling_dict, lorentz_dict):
  """
  >>> C0 = Symbol('C0')
  >>> L0 = Symbol('L0')
  >>> cd = {'GC': 'C0'}
  >>> ld = {'VVV0': 'L0'}
  >>> cfv = {((1, 2), (2, 3), (3, 1)): (C0*L0, 1),
  ...       ((1, 3), (2, 1), (3, 2)): (C0*L0, -1)}
  >>> ccs, ul = colorflow_coupling_sorted(cfv, cd, ld)
  >>> ccs
  {((1, 2), (2, 3), (3, 1)): ({'GC': (1,)}, 1), \
((1, 3), (2, 1), (3, 2)): ({'GC': (1,)}, -1)}
  >>> ul
  {(1,): L0}
  """
  coupling_dict_inv = {coupling_dict[key]: key for key in coupling_dict}
  cbase = [Symbol(u) for u in itervalues(coupling_dict)]

  lorentz_dict_inv = {lorentz_dict[key]: key for key in lorentz_dict}
  lbase = [Symbol(u) for u in itervalues(lorentz_dict)]

  unique_lorentz = {}

  ret = {}
  for cflow in colorflow_vertex:
    lorentz_coupling, cfactor = colorflow_vertex[cflow]
    lorentz_coupling = Poly(lorentz_coupling, cbase).as_dict()
    lorentz_coupling_ordered = {}
    for coupling in lorentz_coupling:
      c_key = Poly({coupling: 1}, cbase).as_expr()
      c_key = coupling_dict_inv[c_key.name]
      ldict = Poly(lorentz_coupling[coupling], lbase).as_dict()
      laddrule = [0]*len(lbase)
      for lelem in ldict:
        laddrule = piecewise_add(list(map(lambda x: ldict[lelem]*lelem[x],
                                 range(len(lelem)))),
                                 laddrule)

      laddrule = tuple(laddrule)
      lorentz_coupling_ordered[c_key] = laddrule
      if laddrule not in unique_lorentz:
        unique_lorentz[laddrule] = lorentz_coupling[coupling]
    ret[cflow] = (lorentz_coupling_ordered, cfactor)

  return ret, unique_lorentz

#==============================================================================#
#                               ColorflowVertex                                #
#==============================================================================#

class ColorflowVertex(Storage, SympyParsing):
  """ Stores all possible colorflows and provides methods for transforming a
  FeynRules Vertex into a color ordered Vertex suited for further processing.

  We give an example encountered in the Background Field Method.
  The four-gluon Vertex is made up of three different lorentz structures, say
  V1, V2 V3.  The lorentz structures are constructed similar to those in the
  FeynRules modelfile:

  Lorentz structure definition.
  >>> LL2 = type('Lorentz', (object,), {})
  >>> V1 = LL2()
  >>> V1.name = 'V1'
  >>> V1.structure = 'Metric(1,4)*Metric(2,3) - Metric(1,3)*Metric(2,4)'
  >>> V2 = LL2()
  >>> V2.name = 'V1'
  >>> V2.structure = ('Metric(1,4)*Metric(2,3) - xi*Metric(1,3)*Metric(2,4) - '+
  ...                 'Metric(1,2)*Metric(3,4)')

  >>> V3 = LL2
  >>> V3.name = 'V3',
  >>> V3.structure = ('xi*Metric(1,4)*Metric(2,3) - Metric(1,3)*Metric(2,4) + '+
  ...             'Metric(1,2)*Metric(3,4)')

  The colorflow is determined from the structure constants
  >>> color = ['f(-1,1,2)*f(3,4,-1)', 'f(-1,1,3)*f(2,4,-1)',
  ...          'f(-1,1,4)*f(2,3,-1)']
  >>> lorentz = [V1, V2, V3]

  The couplings are generically denoted as GC_1, GC_2
  >>> couplings = {(2, 2): 'GC_1', (1, 1): 'GC_2', (0, 0): 'GC_2'}

  The complete Vertex is computed via
  Sum_{i,j} lorentz[i]*couplings[(i,j)]*color[j]

  The number of particles is 4
  >>> n_particles = 4

  All particles are vector-bosons
  >>> spins = [3, 3, 3, 3]

  n_In is the number of particles minus 2. (Smallest n_In is 0 corresponding to
  a 2-Vertex).
  >>> offset = 2
  >>> n_In = n_particles-offset

  Vertices are generated by invoking `process`:
  >>> vertices = [vert for vert in
  ...     ColorflowVertex.process(color, lorentz, couplings, n_In, spins, debug=True)]

  We check the result for one specific colorflow. First we obtain the color id
  >>> cs_id = ColorflowVertex.colors_dict[(((1, 3), (2, 4), (3, 2), (4, 1)),
  ...                                        n_In)]

  >>> selected = [u for u in vertices if u['colorkey'][0] == cs_id][0]

  The vertex for this colorflow can be computed following the result
  >>> selected['coupling_lorentz']['GC_2']
  (1, 0)
  >>> selected['coupling_lorentz']['GC_1']
  (0, 1)

  The keys are the (possibly new) lorentz structures keys and the lorentz
  structures can be accessed via the ls_dict

  >>> l_key = selected['coupling_lorentz']['GC_1']

  >>> selected['ls_dict'][l_key]
  'Metric(1,2)*Metric(3,4) - Metric(1,3)*Metric(2,4) + \
Metric(1,4)*Metric(2,3)*xi'

  >>> l_key = selected['coupling_lorentz']['GC_2']
  >>> selected['ls_dict'][l_key]
  '-Metric(1,3)*Metric(2,4) + Metric(1,4)*Metric(2,3)'

  """
  # StorageMeta attributes. Those attributes are stored when `store` is called.
  storage = ['colors_dict']
  # StorageMeta storage path
  #path = os.path.dirname(os.path.realpath(__file__)) + '/ModelColorflows'
  mpath, _ = model.get_modelpath()
  path = mpath + '/ModelColorflows'
  # StorageMeta filename
  name = 'colorflow_data.txt'

  @classmethod
  def process(self, color, lorentz, couplings_name, n_In, spins, debug=False):
    lorentz_name = [u.name for u in lorentz]
    c_sorted, coupling_dict, lorentz_dict = color_sorted(color,
                                                         couplings_name,
                                                         lorentz_name)
    cf_vertex = colorflow_vertex(c_sorted, spins, debug=debug)
    ccs, ul = colorflow_coupling_sorted(cf_vertex, coupling_dict, lorentz_dict)

    lorentz_strings = []
    for l in lorentz_dict.keys():
      lpos = lorentz_name.index(l)
      lorentz_strings.append(lorentz[lpos].structure)

    ls_dict = {}
    for addrule in ul:
      al = AddLorentz(lorentz_strings)
      ls_dict[addrule] = al.compute(addrule)

    for co in ccs:
      colorflow = (co, n_In)
      if colorflow not in self.colors_dict:
        n_colors = len(self.colors_dict)
        self.colors_dict[colorflow] = n_colors

      lco, cf = ccs[co]
      co_key = (self.colors_dict[colorflow], cf)
      yield {'coupling_lorentz': lco, 'colorfactor': cf, 'colorkey': co_key,
             'ls_dict': ls_dict}


#==============================================================================#
#                       class FeynRulesColorflowPattern                        #
#==============================================================================#

class FeynRulesColorflowPattern(type):

  gen_pat = r'(T\((-*\d+?,-*\d+?,-*\d+?)\))'
  id_pat = r'(Identity\((-*\d+?,-*\d+?)\))'
  adjid_pat = r'(Adjointid\((-*\d+?,-*\d+?)\))'
  struc_pat = r'(f\((-*\d+?,-*\d+?,-*\d+?)\))'

  colorflow_pat = r'(d_\((\w\d+?,\w\d+?)\))'

  def __init__(cls, name, bases, dct):

    super(FeynRulesColorflowPattern, cls).__init__(name, bases, dct)
    cls.gen_pat = re.compile(FeynRulesColorflowPattern.gen_pat)
    cls.id_pat = re.compile(FeynRulesColorflowPattern.id_pat)
    cls.struc_pat = re.compile(FeynRulesColorflowPattern.struc_pat)

    cls.colorflow_rules = {}
    cls.colorflow_rules['gen'] = {'pattern': cls.gen_pat,
                                  'gluon_index': [0],
                                  'symbol': 'T'}
    cls.colorflow_rules['id'] = {'pattern': cls.id_pat,
                                 'gluon_index': [],
                                 'symbol': 'd_'}
    cls.colorflow_rules['adjid'] = {'pattern': cls.adjid_pat,
                                    'gluon_index': [0, 1],
                                    'symbol': 'd_'}
    cls.colorflow_rules['struc'] = {'pattern': cls.struc_pat,
                                    'gluon_index': [0, 1, 2],
                                    'symbol': 'f'}

    cls.colorflow_pat = re.compile(FeynRulesColorflowPattern.colorflow_pat)

  def __iter__(cls):
    return cls.classiter()

#==============================================================================#
#                               class Colorflow                                #
#==============================================================================#

class Colorflow(with_metaclass(FeynRulesColorflowPattern)):

  # store results computed in the current session
  results = {}

  def __init__(self, colorflow, spins, debug=False):
    self.debug = debug
    self.colorflow = colorflow
    self.spins = spins

  def get_colorcorrelation(self, conjugate=True, normalization=True):
    """ Derives the colorflow correlation.

    :param conjugate: see :py:meth:`Colorflow.compute`
    :type  conjugate: bool

    :param normalization: see :py:meth:`Colorflow.compute`
    :type  normalization: bool

    :return: Returns the colorflow correlations as a list of tuples. The first
             element represents the colorflow in form of a dictonary. The keys
             are the color and the values of the keys are the associated
             anticolor. The color flows from color to anticolor. The second
             element in the tuple is the colorfactor.

    >>> cf = Colorflow('f(1,2,3)', [3, 3, 3], debug=True)
    >>> sq2 = Symbol('sq2')
    >>> cf.get_colorcorrelation()
    [({3: 2, 2: 1, 1: 3}, -I*sq2/2), ({1: 2, 2: 3, 3: 1}, I*sq2/2)]
    >>> cf = Colorflow('T(1,2,3)', [3, 2, -2], debug=True)
    >>> cf.get_colorcorrelation(False)
    [({2: 1, 1: 3}, sq2/2), ({2: 3, 1: 1}, -sq2/(2*n))]
    >>> cf = Colorflow('T(1,2,3)', [3, 2, -2], debug=True)

    Removing previous result from the history, otherwise colorflow is not
    computed again.
    >>> del Colorflow.results[('T(1,2,3)', (3, 2, -2))]
    >>> cf.get_colorcorrelation()
    [({3: 1, 1: 2}, sq2/2), ({3: 2, 1: 1}, -sq2/(2*n))]

    >>> Colorflow('Adjointid(1,2)', [3, 3]).get_colorcorrelation()
    [({1: 2, 2: 1}, 1), ({1: 1, 2: 2}, -1/n)]
    """
    cache_key = (self.colorflow, tuple(self.spins))
    if cache_key in self.results:
      return self.results[cache_key]

    res = self.compute(conjugate=conjugate, normalization=normalization)
    if res == {}:
      return [({}, 1)]
    matches = re.findall(self.colorflow_pat, res)
    colordict = {}
    repldict = {}
    cd = ColorDelta()
    for match in matches:
      # sorting convention color then anticolor
      sortconv = {'i': 1, 'j': 2}
      args = match[1].split(',')
      args = sorted(args, key=lambda x: sortconv[x[0]])

      # reconstruct the colorflow delta with correcly sorted arguments
      struc_sorted = 'd_(' + ','.join(args) + ')'
      if struc_sorted not in colordict:
        colorstruc_sym = next(cd)
        repldict[struc_sorted] = colorstruc_sym
        # add wrongly sorted struc to repldict
        if struc_sorted is not match[0]:
          repldict[match[0]] = colorstruc_sym

        args_dict = {int(args[0][1:]): int(args[1][1:])}
        colordict[colorstruc_sym] = args_dict

    # the FORM expr is now substituted with repl_dict expression and parsed to
    # sympy in order to separate colorflow from colorfactors
    res = string_subs(res, repldict)
    res = self.parse_to_sympy(res)

    # the base is given by symbols `C0`, `C1`, .. which represent individual
    # kronecker deltas
    base = [Symbol(u) for u in colordict.keys()]

    # The expression is expressed as a base of kronecker deltas where the keys
    # are products of deltas and the keys are the colorfactors
    res = Poly(res, base).as_dict()
    ret = []
    sq2R = Symbol('sq2', real=True)
    sq2 = Symbol('sq2')
    for cf in res:
      base_elem = Poly({cf: 1}, base).as_expr()
      colorcorrelation = {}
      for elem in base_elem.atoms():
        colorcorrelation.update(colordict[elem.name])
      colorfactor = simplify(res[cf])
      if normalization:
        sq2_subs1 = {sq2: sqrt(2), sq2R: sqrt(2)}
        colorfactor = simplify(colorfactor.subs(sq2_subs1))
        sq2_subs2 = {sqrt(2): sq2R}
        colorfactor = simplify(colorfactor.subs(sq2_subs2))
      ret.append((colorcorrelation, colorfactor))

    # store results
    self.results[cache_key] = ret

    return ret

  def parse_to_sympy(self, expr):
    """ Parses a string or a list of strings to sympy instances. The sympy
    internal parse_expr is used.

    :param expr: Expression to be parsed
    :type  expr: String or list of strings

    :return: Sympy expression of expr
    """
    if isinstance(expr, str):
      expr = string_subs(expr, {'i_': 'I', '^': '**'})
      ret = sympify(expr)
      return ret

    elif isinstance(expr, list_type):
      return [self.parse_to_sympy(u) for u in expr]
    else:
     raise TypeError('parse_to_sympy requires string or list of strings. Given:'
                     + str(type(expr)))

  def compute(self, conjugate=True, normalization=True):
    """ Computes the colorflow by contracting all open indices corresponding to
    the adjoint representation with generators and by expressing structure
    constants via traces of products of generators. The actual color object for
    which the colorflow should be computed is passed via the Colorflow
    constructor.

    :param conjugate: If true interchanges color and anticolor. It is set by
                      default because Feynrules considers all particles as
                      outgoing whereas in Recola all particles are incomming.
                      Thus, not only the particles must be conjugated, but also
                      the colors.
    :type conjugate: bool

    :param normalization: Uses a proper normalization of the structure
                          constants and generators.
                          No need to substitute g-> g/sqrt(2)
    :type  normalization: bool

    :return: String of the colorflow expression. The colorflow consists of
             kronecker deltas `d_ ` and colorfactors i_, n

    Usage:

    >>> cf = Colorflow('f(3,1,2)', [3, 3, 3], debug=True)
    >>> cf.compute(False)
    'd_(i3,j1)*d_(j3,i2)*d_(i1,j2)*i_*sq2^-1-\
d_(i3,j2)*d_(j3,i1)*d_(j1,i2)*i_*sq2^-1'


    >>> cf = Colorflow('T(3,2,1)', [-2, 2, 1], debug=True)
    >>> cf.compute(False)
    '-d_(i2,j1)*d_(i3,j3)*sq2^-1*n^-1+d_(i2,j3)*d_(j1,i3)*sq2^-1'
    >>> cf = Colorflow('T(1,-1,-2)*T(2,-2,-1)', [3, 3], debug=True)
    >>> cf.compute(False)
    '-d_(i1,j1)*d_(i2,j2)*sq2^-2*n^-1+d_(i1,j2)*d_(j1,i2)*sq2^-2'
    >>> cf = Colorflow('T(-1,3,-3)*T(1,-3,-2)*T(-1,-2,2)', [3, -2, 2], debug=True)
    >>> cf.compute(False)
    'd_(i3,j2)*d_(i1,j1)*sq2^-3*n^-2-d_(i3,j1)*d_(j2,i1)*sq2^-3*n^-1'

    other example, four gluon vertex
    cf = Colorflow('f(-1,1,2)*f(-1,3,4)')
    """
    if self.colorflow == '1':
      return {}

    parsed = []
    repldict = {}
    for pattern_key in self.colorflow_rules:
      pattern = self.colorflow_rules[pattern_key]['pattern']
      matches = re.findall(pattern, self.colorflow)
      for match in matches:
        repldict[match[0]] = '1'
        parsed.append((pattern_key, match[0],
                       [int(u) for u in match[1].split(',')]))
        # interchange fermionic/anti-fermionic indices
        if pattern_key == 'gen' and conjugate:
          tmp = parsed[-1]
          tmp = (tmp[0], tmp[1], [tmp[2][0], tmp[2][2], tmp[2][1]])
          parsed[-1] = tmp
        # interchange fermionic/anti-fermionic indices
        elif pattern_key == 'id':
          tmp = parsed[-1]
          args = tmp[2]
          # new_args = [None, None]
          if sum(abs(self.spins[u-1]) for u in args) == 4: # fermions
            # fermionic arguments must take first index in d_
            if conjugate:
              if self.spins[args[0]-1] < 0:
                new_args = args
              else:
                new_args = [args[1], args[0]]
            # fermionic arguments must take second index in d_
            else:
              if self.spins[args[0]-1] < 0:
                new_args = [args[1], args[0]]
              else:
                new_args = args

            parsed[-1] = (tmp[0], tmp[1], new_args)

          # replace identity by adjoint identity
          else:
            parsed[-1] = ('adjid', string_subs(tmp[1], {'Identity': 'Adjointid'}),
                          [tmp[2][1], tmp[2][0]])

    # determine the overall factor by setting all structures to '1'. Evaluating
    # the resulting string yields the result and should succeed if no structure
    # has been missed
    overallfactor = sympify(string_subs(self.colorflow, repldict))
    # estimation of indices which should not be used as conraction indices
    reserved_indices = 0

    # external gluons are contracted with a generator
    ext_gen = []
    for res in parsed:
      pattern_key = res[0]
      args = res[2]
      ext_gluons = self.get_external_gluon(pattern_key, args)
      for ext_gluon in ext_gluons:
        ext_gen.append('T(k' + str(ext_gluon) +
                       ',i' + str(ext_gluon) +
                       ',j' + str(ext_gluon) + ')')
      ext_quarks = self.get_external_quark(pattern_key, args)
      reserved_indices += (2*len(ext_gluons) +
                           len(ext_quarks))
    # generates new indices
    fi = FreeIndices(i=reserved_indices)

    # the structure f_, T_ need to be rescaled by 1/sqrt(2) to match the
    # normalization used for derving the colorflow.
    if normalization:
      normalization = 1
      sq2 = Symbol('sq2', real=True)
    # FeynRules conventions are replaced by other arguments for use in FORM
    transformed = []
    for res in parsed:
      pattern_key = res[0]
      args = self.transform_args(pattern_key, res[2], reserved_indices)
      if pattern_key == 'struc':
        if normalization:
          normalization /= sq2
        transformed.append(self.express_via_gen(args, fi))
      else:
        if pattern_key == 'gen' and normalization:
          normalization /= sq2
        new_args = ','.join(args)
        transformed.append(self.colorflow_rules[res[0]]['symbol'] + '(' +
                           new_args + ')')

    transformed = ['(' + '*'.join(transformed) + ')']
    transformed.extend(ext_gen)
    if normalization:
      overallfactor *= normalization
    cf = 'l cf = ' + '*'.join(transformed) + '*' + str(overallfactor) + ';'
    exprs = [cf,
             'repeat;',
             'id T(mu?,i?,j?)*T(mu?,k?,l?) = ' +
             '(d_(i,l)*d_(j,k)-d_(i,j)*d_(k,l)/n);',
             'endrepeat;',
             '.sort']
    FP = FormPype(basefile=os.path.dirname(os.path.realpath(__file__)) +
                  '/colorflow.frm', debug=self.debug)
    FP.eval_exprs(exprs)
    res = FP.return_expr('cf', stop=True)
    res = ''.join(res.split())
    return res

  def transform_args(self, pattern_key, args, offset):
    """
    Contraction indices are by convention negative indices in FeynRules. All
    internal indices are labeled as k which implies that all adjoint indices are
    labeled as k. The (anti-)fundamental  are labeled as (j)i unless contracted.

    :return: List of indices represented as strings. The (negative) contraction
             indices are replaced by new arguments for further processing in
             FORM.

    :param args: arguments of the structure constant
    :type  args: list of strings
    :param free_indices: free_indices are used as contraction indices for
                         performing the trace
    :type  free_indices: generator of strings of the form 'l1', 'l2', ...
    :param offset: starting index for contraction indices
    :type  offset: integer

    >>> cf = Colorflow('', [])
    >>> cf.transform_args('struc', [-1, 1, 2], 3)
    ['k4', 'k1', 'k2']
    >>> cf.transform_args('gen', [1, -1, 2], 3)
    ['k1', 'k4', 'j2']
    """
    if pattern_key == 'struc':
      arg_indices = ['k', 'k', 'k']
    elif pattern_key == 'gen':
      arg_indices = []
      for pos, arg in enumerate(args):
        # gluon index always contracted -> k
        if pos == 0:
          arg_indices.append('k')
        else:
          # color index is contracted if negative
          if int(arg) < 0:
            arg_indices.append('k')
          # color index is i, anticolor index is j
          else:
            arg_indices.append(['i', 'j'][pos-1])
    elif pattern_key == 'id':
      arg_indices = ['i', 'j']
    elif pattern_key == 'adjid':
      arg_indices = ['k', 'k']
    else:
      raise Exception('Unknown pattern: ' + str(pattern_key))

    return ([arg_indices[args.index(arg)] + str(arg) if arg > 0
             else arg_indices[args.index(arg)] + str(-arg + offset)
             for arg in args])

  def express_via_gen(self, args, free_indices, conjugate=True):
    """
    :return: Expresses the structure constants f^{abc} via generators according
             to f^{abc} = -i Tr[T^a[T^b,T^c]]

    :param args: arguments of the structure constant
    :type  args: list of strings
    :param free_indices: free_indices are used as contraction indices for
                         performing the trace
    :type  free_indices: generator of strings of the form 'l1', 'l2', ...

    >>> cf = Colorflow('', [])
    >>> args = ['k3', 'k1', 'k2']
    >>> fi = FreeIndices()
    >>> cf.express_via_gen(args, fi, conjugate=False)
    '(-i_)*T(k3,l0,l1)*(T(k1,l1,l2)*T(k2,l2,l0)-T(k2,l1,l2)*T(k1,l2,l0))'
    """
    arg1 = next(free_indices)
    arg2 = next(free_indices)
    arg3 = next(free_indices)
    if conjugate:
      t1 = [args[0], arg2, arg1]
      t21 = [args[1], arg3, arg2]
      t31 = [args[2], arg1, arg3]
      t32 = [args[2], arg3, arg2]
      t22 = [args[1], arg1, arg3]
    else:
      t1 = [args[0], arg1, arg2]
      t21 = [args[1], arg2, arg3]
      t31 = [args[2], arg3, arg1]
      t32 = [args[2], arg2, arg3]
      t22 = [args[1], arg3, arg1]

    T1 = 'T(' + ','.join(t1) + ')'
    T21 = 'T(' + ','.join(t21) + ')'
    T31 = 'T(' + ','.join(t31) + ')'
    T32 = 'T(' + ','.join(t32) + ')'
    T22 = 'T(' + ','.join(t22) + ')'
    commutator = '-'.join(['*'.join([T21, T31]), '*'.join([T32, T22])])
    res = '(-i_)*' + '*'.join([T1, '(' + commutator + ')'])
    return res

  def get_external_gluon(self, pattern_key, args):
    """
    :return: Returns the values of adjoint indices.

    :param pattern_key: specifying the color structure and needs to be a key of
                        colorflow_rules
    :type  pattern_key: string
    :param  args: arguments of the considered structure
    :type   args: list of integers

    >>> cf = Colorflow('',[])
    >>> cf.get_external_gluon('struc', [1,2,3])
    [1, 2, 3]
    >>> cf.get_external_gluon('struc', [-1,2,-3])
    [2]
    >>> cf.get_external_gluon('gen', [3,1,2])
    [3]
    """
    gluon_indices = self.colorflow_rules[pattern_key]['gluon_index']
    external = []
    for pos in gluon_indices:
      if args[pos] > 0:
        external.append(args[pos])

    return external

  def get_external_quark(self, pattern_key, args):
    """
    :return: Returns the values of fundamental indices.

    :param pattern_key: specifying the color structure and needs to be a key of
                        colorflow_rules
    :type  pattern_key: string
    :param  args: arguments of the considered structure
    :type   args: list of integers

    >>> cf = Colorflow('', [])
    >>> cf.get_external_quark('struc', [1,2,3])
    []
    >>> cf.get_external_quark('gen', [3,1,2])
    [1, 2]
    >>> cf.get_external_quark('gen', [3,-1,2])
    [2]
    """
    gluon_indices = self.colorflow_rules[pattern_key]['gluon_index']
    external = []
    for pos in [i for i in range(len(args)) if i not in gluon_indices]:
      if args[pos] > 0:
        external.append(args[pos])
    return external


class AddLorentz(object):
  """ Parses and computes linear combinations of lorentz strings.  """

  # Import all structures from the lorentz base
  strucs = [u.symbol.name for u in lorentz_base.bases]
  lorentz_pats = [r'(' + u + '\((?:-*\d+?,*)+?\))' for u in strucs]
  lorentz_pats = [re.compile(u) for u in lorentz_pats]

  def __init__(self, lorentz_strings):
    l = Lorentz()
    repldict = {}
    new_lorentz_strings = []
    for lorentz_string in lorentz_strings:
      for pat in self.lorentz_pats:
        matches = re.findall(pat, lorentz_string)
        for match in matches:
          if match not in repldict:
            l_sym = next(l)
            repldict[match] = l_sym
      new_lorentz_strings.append(string_subs(lorentz_string, repldict))
    new_lorentz_strings = self.parse_to_sympy(new_lorentz_strings)
    self.nls = new_lorentz_strings
    self.repldict = repldict

  def compute(self, add_rule):
    """
    Computes the linear combination of lorentz structures.

    :add_rule: List of prefactors. Length must be equal to the length of
               lorentz_strings.

    >>> strings = ['Gamma(3,2,-1)*ProjM(-1,1)',
    ...            'Gamma(3,2,-1)*ProjM(-1,1)']
    >>> al = AddLorentz(strings)
    >>> al.compute([2, -3])
    '-Gamma(3,2,-1)*ProjM(-1,1)'
    """
    if len(add_rule) != len(self.nls):
      raise TypeError('add_rule must have the same length as lorentz_strings')
    res = 0
    for i, ls in enumerate(self.nls):
      res += add_rule[i]*ls

    repldict_inv = {self.repldict[u]: u for u in self.repldict}
    return str(res.subs({Symbol(u):
                         Symbol(repldict_inv[u]) for u in repldict_inv}))

  def parse_to_sympy(self, expr):
    """
    Parses a string or a list of strings to sympy instances. The sympy internal
    parse_expr is used.

    :param expr: Expression to be parsed
    :type  expr: String or list of strings

    :return: Sympy expression of expr
    """
    if isinstance(expr, str):
      expr = string_subs(expr, {'^': '**'})
      ret = sympify(expr)
      return ret

    elif isinstance(expr, list_type):
      return [self.parse_to_sympy(u) for u in expr]
    else:
     raise TypeError('parse_to_sympy requires string or list of strings. Given:'
                     + str(type(expr)))

if __name__ == "__main__":
  import doctest
  doctest.testmod()
