if (last .and. (lp .eq.1)) then
  do ib = 1, bin_color
    if (csTmp(legsE - 1, ib).eq.legsE) then
      ncpower(ib) = ncpower(ib) + 1
    else
      do j = 1, legsE
        if (csTmp(j, ib).eq.legsE) then
          csTmp(j, ib) = csTmp(legsE - 1, ib)
        end if
      end do
    end if
    csTmp(legsE - 1, ib) = 0
    if (csTmp(legsE, ib) .eq. legsE - 1) then
      ncpower(ib) = ncpower(ib) + 1
    else
      do j = 1, legsE
        if (csTmp(j, ib).eq.legsE - 1) then
          csTmp(j, ib) = csTmp(legsE, ib)
        end if
      end do
    end if
    csTmp(legsE, ib) = 0
  end do
end if 
