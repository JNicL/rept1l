      ! Outgoing colorflow is adjusted to the actual number of valid branches
      allocate(csOut(-1:legsE, bin_color))
      do ib = 1, bin_color
        csOut(-1:legsE, ib) = csTmp(-1:legsE, ib)
      end do
      ! select_branch has been updated and valid branches are filled gapless.
      ! non-valid branches are set to zero.
      if (bin_color .lt. size(select_branch)) then
        select_branch(bin_color+1:size(select_branch)) = 0
      end if
