        ! select_branch is zero if it has been filtered before, and the
        ! corresponding colorflow is not computed
        if (select_branch(cf+1) == 0) then
          cf = cf+1
          cycle
        end if       
        csTmp(-1:legsE, bin_color) = 0 ! Init as colorless
        do i = 1, legsE
          do j = 1, size(csIn,1)
            ! Copying color deltas
            csTmp(i, bin_color) = csTmp(i, bin_color) + csIn(j,i)
          end do
        end do
