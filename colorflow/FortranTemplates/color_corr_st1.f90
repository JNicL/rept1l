      if (size(select_branch) .gt. branch3Max) then
        write (*, *) 'The current branch number exceeds the maximal value. '
        write (*, *) 'Adjust corresponding branchNMax in internal_variables.f90'
        stop
      end if     
      bin_color = 1
      cf = 0
