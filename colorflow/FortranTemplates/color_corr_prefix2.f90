      integer, dimension(:),     intent(inout) :: select_branch
      integer, dimension(:,-1:), intent(in)    :: csIn
      integer, allocatable,      intent(inout) :: csOut(:,:)
      logical, dimension(:),     intent(in)    :: U1GluonIn
      logical,                   intent(inout) :: U1GluonOut
      integer, dimension(-1:0),  intent(in)    :: csLast
      logical,                   intent(in)    :: last
      integer,                   intent(in)    :: pr,legsE,lp,n_In
      integer, dimension(0:),    intent(in)    :: colorflowIds
      integer, dimension(:),     intent(inout) :: ncpower
