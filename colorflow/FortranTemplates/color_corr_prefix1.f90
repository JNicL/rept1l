      integer,                       intent(in)    :: nbranches
      integer, dimension(nbranches), intent(inout) :: select_branch
      integer, dimension(:,-1:),     intent(in)    :: csIn
      integer, allocatable,          intent(inout) :: csOut(:,:)
      logical, dimension(:),         intent(in)    :: U1GluonIn
      logical,                       intent(inout) :: U1GluonOut
      integer, dimension(-1:0),      intent(in)    :: csLast
      logical,                       intent(in)    :: last
      integer,                       intent(in)    :: pr, legsE, lp
      integer, dimension(0:),        intent(in)    :: colorflowIds
      integer, dimension(nbranches), intent(inout) :: ncpower

      integer, dimension(-1:legsE,nbranches) :: csTmp
      integer :: bin_color  ! Number of valid colorflows
      integer :: cf
      integer :: i, j, ib
