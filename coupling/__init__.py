###############################################################################
#                                 __init__.py                                 #
###############################################################################

from . import coupling as c
from . import class_couplings as cc

RCoupling = c.RCoupling
gen_couplings_class = cc.gen_couplings_class
gen_ctcouplings_class = cc.gen_ctcouplings_class

__author__ = "Jean-Nicolas lang"
__date__ = "31. 7. 2016"
__version__ = "0.9"
