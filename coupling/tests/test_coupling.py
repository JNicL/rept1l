#==============================================================================#
#                                test_tensor.py                                #
#==============================================================================#

#============#
#  Includes  #
#============#

from test_tools import *
from coupling import *
from sympy import Symbol


ee = Symbol('ee')
G = Symbol('G')
QED = Symbol('QED', positive=True)
QCD = Symbol('QCD', positive=True)

class Test_Coupling(object):

  def setUp(self):

    RCoupling.loaded = True
    RCoupling._order_value_coupling = {}
    RCoupling._all_couplings = []
    RCoupling._coupling_values = {}
    RCoupling._orders = ['QCD', 'QED']
    RCoupling.param_orders = {G.name: {'QED': 0, 'QCD': 1},
                              ee.name: {'QED': 1, 'QCD': 0}}

  def test_insert_order_base(self):
    cc1 = RCoupling(16*G**2*ee**2).return_coupling()
    result = RCoupling.insert_order_base(2*ee**2*cc1)
    eq_(result, QED**4*QCD**2)

    cc2 = RCoupling(6*G*ee).return_coupling()
    result = RCoupling.insert_order_base(2*ee**2*cc1/cc2)
    eq_(result, QED**3*QCD)

    from sympy import sqrt
    cc3 = RCoupling(sqrt(cc1)).return_coupling()
    result = RCoupling.insert_order_base(cc3)
    eq_(result, QED*QCD)
    result = RCoupling.insert_order_base(sqrt(cc1))
    eq_(result, QED*QCD)

  def test_make_order_dict(self):
    r = QED**2*QCD
    order = RCoupling.make_order_dict(r)
    expected = {'QED': 2, 'QCD': 1}
    eq_(order, expected)
