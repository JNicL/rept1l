#==============================================================================#
#                                parameters.py                                 #
#==============================================================================#

#============#
#  Includes  #
#============#

import os
import re
import cmath
from rept1l.pyfort import modBlock, containsBlock, subBlock, loadBlock
from rept1l.combinatorics import fold

#=============#
#  structure  #
#=============#

#/-----------------------------\#
# module class_couplings        #
#                               #
#  prefix                       #
#                               #
#  contains                     #
#                               #
#   subroutines initCouplings   #
#                               #
#   subroutines initCTCouplings #
#                               #
#   subroutines initR2Couplings #
#                               #
# end module                    #
#\-----------------------------/#

#==============================================================================#
#                             gen_couplings_class                              #
#==============================================================================#

def gen_couplings_class():

  import rept1l.Model as Model
  model = Model.model
  model_objects = model.model_objects

  #===================#
  #  class_couplings  #
  #===================#

  class_couplings = modBlock("class_couplings")
  contains_couplings = containsBlock(tabs=1)

  path = os.path.dirname(os.path.realpath(__file__))
  path = os.path.join(path, 'FortranTemplates')
  coupling_path = os.path.join(path, 'get_coupling_value.f90')
  get_coupling = loadBlock(coupling_path)
  contains_couplings + get_coupling
  function_init_couplings = subBlock("fill_couplings_mdl", tabs=2)
  class_couplings.addPrefix("use constants_mdl", 1)
  class_couplings.addPrefix("use class_particles", 1)
  class_couplings.addPrefix("implicit none", 1)
  class_couplings.addPrefix("save", 1)
  n_couplings_tmp = len(model_objects.all_couplings)
  class_couplings.addType(("integer, parameter", "n = " + str(n_couplings_tmp)))
  class_couplings.addLineD()
  class_couplings.addType(("logical", "couplings_init = .false."))
  class_couplings.addType(("logical", "ctcouplings_init = .false."))

  # hierarchy
  class_couplings + contains_couplings

  function_init_couplings.addPrefix("implicit none", 1)
  function_init_couplings + "couplings_init = .true."

  return class_couplings, function_init_couplings

#==============================================================================#
#                            gen_ctcouplings_class                             #
#==============================================================================#

def gen_ctcouplings_class():

  import rept1l.Model as Model
  model = Model.model
  model_objects = model.model_objects

  #=====================#
  #  class_ctcouplings  #
  #=====================#

  # class ctcouplings is junk and not exported to fortran
  class_ctcouplings = modBlock("class_ctcouplings")
  contains_ctcouplings = containsBlock(tabs=1)
  function_init_CTcouplings = subBlock("fill_CTcouplings_mdl", tabs=2)

  class_ctcouplings.addPrefix("use constants_mdl", 1)
  class_ctcouplings.addPrefix("use class_particles", 1)
  class_ctcouplings.addPrefix("use class_couplings", 1)
  class_ctcouplings.addPrefix("use class_ctparameters", 1)
  class_ctcouplings.addPrefix("implicit none", 1)
  class_ctcouplings.addPrefix("save", 1)

  class_ctcouplings.addType(("logical", "ctcouplings_init = .false."))

  # hierarchy
  class_ctcouplings + contains_ctcouplings

  function_init_CTcouplings.addPrefix("use constants_mdl, only: Nc", 1)
  function_init_CTcouplings.addPrefix("implicit none", 1)
  function_init_CTcouplings + "ctcouplings_init = .true."
  contains_ctcouplings + "\n"

  return class_ctcouplings, function_init_CTcouplings

