#==============================================================================#
#                                 coupling.py                                  #
#==============================================================================#
from __future__ import print_function

#============#
#  Includes  #
#============#

import sys
import os
import re
try:
  from types import ListType
  list_type = ListType
except ImportError:
  list_type = list
from past.builtins import xrange
from six import with_metaclass
from six import iteritems

from sympy import Symbol, sympify, together, Poly, I, Pow, Mul
from sympy import fraction, factor
from sympy import PolynomialError
from sympy import count_ops
import rept1l.Model as Model
from rept1l.combinatorics import fold
from rept1l.helper_lib import StorageProperty, hashfile, file_exists, parse_UFO
from rept1l.logging_setup import log

#############
#  Globals  #
#############

model = Model.model


class AmbigousOrder(Exception):
  """
  AmbigousOrder is raised if during computation a non unique order in
  fundamental couplings is ecountered, but is required.
  """
  def __init__(self, args_repl):
    self.args_repl = args_repl

  def __str__(self):
    return repr(self.value)

#==============================================================================#
#                            RCouplingMeta                             #
#==============================================================================#

class RCouplingMeta(StorageProperty):
  """ Generates lists of couplings allowing for direct access of values in form
  of sympy expressions and order in coupling constants.

  The evaluation is done only once and shared among all RCoupling
  instances.

  For the first run the couplings, orders etc. are cached. The cached results
  are loaded and used for each run as long as the modelfiles are untouched.
  """

  def set_cache_storage(cls):
    # backup the original storage parameter
    storage = cls.storage[:]
    name = cls.name

    # cached results are stored in cached_model.txt
    cls.name = 'cached_model.txt'
    cls.storage = ['_order_value_couplings', '_coupling_values',
                   '_coupling_orders', '_coupling_dict', '_checksum',
                   'param_orders', '_original']
    return storage, name

  def compute_coupling_hashes(cls):
    # cached results rely on the following files
    hash_base_model = ['parameters.py', 'coupling_orders.py', 'couplings.py']
    # couplings_ct.py kept for compatibility
    hash_base_ct = ['user_settings.py', 'couplings_ct.py']

    # retrieve the path of the model and counterterm files
    path, mname = model.get_modelpath()
    modelpath = path + '/' + mname

    # compute the hash of the files
    from hashlib import sha256
    modelfiles = [os.path.join(modelpath, u) for u in hash_base_model]
    ctfiles = []
    for u in hash_base_ct:
      filepath = os.path.join(path, u)
      if file_exists(filepath):
        ctfiles.append(filepath)

    hashmodel = [hashfile(open(fname, 'rb'), sha256()) for fname in modelfiles]
    hashct = [hashfile(open(fname, 'rb'), sha256()) for fname in ctfiles]
    hashes = hashmodel + hashct

    return hashes

  def __init__(cls, name, bases, dct):
    super(RCouplingMeta, cls).__init__(name, bases, dct)

    storage, name = cls.set_cache_storage()

    hashes = cls.compute_coupling_hashes()

    try:
      cls.load()
      # set loaded flag to zero, otherwise the actual storage is not getting
      # loaded
      cls.loaded = False
    # cache file does not exist -> initialize
    except IOError:
      for attr in cls.storage:
        setattr(cls, attr, {})
      cls.store()

    # check if the model has been changed
    cache_model = True
    if hasattr(cls, '_checksum') and cls._checksum == hashes:
      cache_model = False

    # simpyfied version of param_orders
    cls._orders = [u.name for u in model.object_library.all_orders]
    param_orders = model.parameter_orders

    from rept1l.counterterms import Counterterms, get_param_ct
    cls._cts = get_param_ct(model.object_library.all_parameters)
    if cache_model:
      # load user defined param orders
      cls.param_orders = {u.name: param_orders[u] for u in param_orders}
      # compute powers of all other parameters except for counterterm parameters
      for param in (u for u in model.object_library.all_parameters
                    if u.name not in cls.cts):
        if param.name not in cls.param_orders:
          try:
            cls.parameter_order(param)
          except Exception as e:
            log('The system was not able to derive the order in the ' +
                'fundamental couplings for parameter: ' + str(param) + '.',
                'error')
            log("Errormessage: " + str(e), 'debug')
            log('You can define the order yourself by setting the order in' +
                'the paramter_orders in model_extended_parameters.py.', 'debug')
            raise

    # load counterterm parameter orders assigned by the user
    Counterterms.load()
    cls.param_orders.update({u: Counterterms.renos_order[u]
                             for u in Counterterms.renos_order})

    if cache_model:
      # replace couplings by products of R parameters (as sympy expr)
      cls._coupling_values = {}
      # storing the order associated to a coupling
      cls._coupling_orders = {}
      # compare values for couplings by parameters
      cls._order_value_coupling = {}
      # get coupling instance from sympy expr
      cls._coupling_dict = {}
      # original UFO coupling ?
      cls._original = {}

      from rept1l.pyfort import ProgressBar
      log('Caching couplings.', 'info')
      PG = ProgressBar(len(model.object_library.all_couplings))
      for coupl in model.object_library.all_couplings:
        order = {u: (coupl.order[u] if u in coupl.order else 0)
                 for u in cls.orders}
        # make coupl.order hashable
        coupl_order = tuple([(order_type, coupl.order[order_type])
                             if order_type in coupl.order
                             else
                             (order_type, 0) for order_type in cls.orders])
        if coupl_order not in cls._order_value_coupling:
          cls._order_value_coupling[coupl_order] = {}
        c_name = coupl.name
        c_value = cls.parse_to_sympy(coupl.value)
        cls.check_parameter_dep(c_name, c_value)
        c_value = cls.complex_mass_couplings(c_value)
        cls._order_value_coupling[coupl_order][c_value] = coupl
        cls._coupling_values[c_name] = c_value
        cls._coupling_orders[c_name] = order
        cls._coupling_dict[c_name] = coupl
        cls._original[c_name] = True
        PG.advanceAndPlot()

    if cache_model:
      cls._checksum = hashes
      cls.store()
      cls.loaded = False

    # restore storage
    cls.storage = storage
    cls.name = name

    cls._all_couplings = model.object_library.all_couplings

    # store newly created couplings which are not contained in the R
    # modelfile
    cls.new_couplings = []

    # class coupling
    cls.Coupling = model.object_library.Coupling

    # store actually used couplings (which were entered at the point of
    # generating the modelfile for RECOLA). The keys are sympy expressions of
    # the couplings and the values are lists of tags which indicate for which
    # kind of current the coupling is used (e.g. Tree, Loop, CT, R2)
    cls.couplings_regged = {}

  def string_subs(self, string, dict_repl):
    if len(dict_repl) > 0:
      p = (re.compile('|'.join(re.escape(key)
           for key in dict_repl.keys()), re.S))
      return p.sub(lambda x: dict_repl[x.group()], string)
    else:
      return string

  def check_parameter_dep(self, cname, cvalue):
    from rept1l.counterterms import Counterterms
    all_ct = Counterterms.get_all_ct(as_string=True)
    unknown_params = [v for v in (u.name for u in cvalue.free_symbols)
                      if v not in self.param_orders and
                      not v.startswith('GC_') and
                      v not in all_ct]
    if len(unknown_params) > 0:
      log('Encountered unknown parameters for coupling ' + cname + '.',
          'warning')
      log('Unknown parameters: ' + ','.join(unknown_params),
          'warning')

  def parse_to_sympy(self, expr):
    """ Parses a string or a list of strings to sympy instances. The sympy
    internal parse_expr is used.

    :param expr: Expression to be parsed
    :type  expr: String or list of strings

    :return: Sympy expression of expr
    """
    return parse_UFO(expr)

  def parameter_order(cls, parameter):
    """ Determines the order in the coupling constants of a parameter
    expression. """
    # counterterm parameters are initialized with a default order. After
    # renormalization the order is updated. See in `__init__`  the call
    # 'cls.param_orders.update(Counterterms.renos_order)'
    if parameter.name in cls.cts:
      final_order = {u: 0 for u in cls.orders}
      cls.param_orders[parameter.name] = final_order
      return

    try:
      expr = cls.parse_to_sympy(parameter.value)
    except TypeError:
      # Parameter is a number -> if not set by the user, the order is assigned
      # to be zero in all couplings
      final_order = {u: 0 for u in cls.orders}
      cls.param_orders[parameter.name] = final_order
      return
    except:
      raise

    if expr.is_number:
      final_order = {u: 0 for u in cls.orders}
      cls.param_orders[parameter.name] = final_order
      return

    # The parameter seems to be an ordinary expression and the order is
    # determined

    num, den = cls.insert_order_base_fraction(expr)
    den = Poly(together(den), cls.get_order_base(cls.orders)).as_dict()
    assert(len(den) == 1)
    den = list(den)[0]

    num = Poly(together(num), cls.get_order_base(cls.orders)).as_dict()
    assert(len(num) == 1)
    num = list(num)[0]
    final_order = {u: num[i] - den[i] for i, u in enumerate(cls.orders)}
    cls.param_orders[parameter.name] = final_order

  def insert_order_base(cls, expr):
    """ Replaces expressions of parameters and couplings with order symbols
    associated to fundamental couplings (e.q.  QED, QCD) in  `expr`.

    :param expr: symbolic expression
    :type  expr: sympy
    """
    if expr.is_number:
      return 1

    if expr.is_Symbol:
      if expr.name in cls.param_orders:
        return cls.form_order_coupling(cls.param_orders[expr.name])
      elif expr.name in cls._coupling_orders:
        return cls.form_order_coupling(cls.coupling_orders[expr.name])
      elif expr in cls.cts.values():
        final_order = {u: 0 for u in cls.orders}
        index = list(cls.cts.values()).index(expr)
        ct_param = list(cls.cts.keys())[index]
        cls.param_orders[ct_param] = final_order
        return 1
      else:
        return 1

    if expr.is_Add:
      orders = sum([cls.insert_order_base(u) for u in expr.args])
      return orders

    # special symbols like imaginary unit
    if not (expr.is_Mul or expr.is_Pow):
      return 1

    # determine all orders of parameters in the expression
    mul_atoms = [u for u in expr.free_symbols]
    for param in mul_atoms[:]:
      if((param.name not in cls.param_orders) and
         (param.name not in cls.coupling_orders)):
        if param.name.startswith('GC_'):
          raise Exception('Coupling ' + param.name +
                          ' not found in coupling_orders.')
        parameter = cls.get_parameter(param.name)
        if parameter is not None:
          cls.parameter_order(parameter)
        else:
          mul_atoms.remove(param)

    mul_atoms_param = (u for u in mul_atoms if u.name in cls.param_orders)
    mul_atoms_coupl = (u for u in mul_atoms if u.name in cls.coupling_orders)
    subs_dict_param = {u: cls.form_order_coupling(cls.param_orders[u.name])
                       for u in mul_atoms_param
                       if cls.form_order_coupling(
                       cls.param_orders[u.name]) != 1}

    subs_dict_coupl = {u: cls.form_order_coupling(cls.coupling_orders[u.name])
                       for u in mul_atoms_coupl
                       if cls.form_order_coupling(
                       cls.coupling_orders[u.name]) != 1}

    # replace param/coupl by order expression
    ret = expr.xreplace(subs_dict_param).xreplace(subs_dict_coupl)
    b = cls.get_order_base(cls.get_order_base(cls.orders))

    if ret.is_Add:
      raise Exception('ret is add. Case not handled')

    # make sure is properly factorized
    ret = together(ret)
    num, den = fraction(ret)
    # set prefactors to 1 via building polynomial key expression
    pdn = Poly(num, b).as_dict()
    pdn = {k: 1 for k in pdn}
    pdd = Poly(den, b).as_dict()
    pdd = {k: 1 for k in pdd}
    ret = Poly(pdn, b).as_expr() / Poly(pdd, b).as_expr()
    return ret

  def insert_order_base_fraction(cls, coupling, debug=False):
    """ Applies `insert_order_base` individually on numerator and denominator
    of `coupling`.

    :param expr: symbolic expression
    :type  expr: sympy
    """
    num, den = fraction(coupling)
    num = cls.insert_order_base(num)
    den = cls.insert_order_base(den)
    ret = fraction(together(num / den))
    return ret

  def make_order_dict(cls, mul_expr):
    """ Builds the order dictionary to `mul_expr`. """
    try:
      mul_dict = Poly(together(mul_expr),
                      cls.get_order_base(cls.orders)).as_dict()
    except PolynomialError:
      # try to simplify the expression if not too large (otherwise it would take
      # too long) and hope the nonpolynomical generator cancels out.
      if count_ops(mul_expr) < 500:
        mul_expr_simp = together(mul_expr)
        try:
          mul_dict = Poly(mul_expr_simp,
                          cls.get_order_base(cls.orders)).as_dict()
        # last
        except PolynomialError:
          mul_dict = Poly(1 / mul_expr_simp,
                          cls.get_order_base(cls.orders)).as_dict()
          mul_dict = {tuple(-u for u in key): val for key, val in iteritems(mul_dict)}
      else:
        raise
    if len(mul_dict) == 1:
      mul_dict = list(mul_dict)[0]
      order = {u: mul_dict[i] for i, u in enumerate(cls.orders)}
      return order
    else:
      raise AmbigousOrder('Expression has multiple orders: ' + str(mul_expr))

  @classmethod
  def get_order_base(cls, orders):
    if not hasattr(cls, 'order_base'):
      cls.order_base = [Symbol(u, positive=True) for u in orders]
    return cls.order_base

  @staticmethod
  def form_order_coupling(order_dict):
    ret = 1
    for order_type in order_dict:
      power = order_dict[order_type]
      order_sym = Symbol(order_type, positive=True)
      ret *= Pow(order_sym, power)
    return ret

  @staticmethod
  def get_parameter(param_name, debug=False):
    if debug:
      print("param_name:", param_name)
    for u in model.object_library.all_parameters:
      if u.name == param_name:
        if type(u) is not model.CTParameter:
          return u
        else:
          return None

#==============================================================================#
#                              RCoupling                               #
#==============================================================================#

class RCoupling(with_metaclass(RCouplingMeta)):
  """ Methods for registering and storing couplings. The coupling is instanced
  via RCoupling and the result is returned with return_coupling. The the
  couplings are instanced as R couplings which is done automatically.
  Model couplings are stored in (see RCouplingMeta)

  Examples:

  The setup is done with the modelfile and should not be edited by the user
  >>> RCoupling.loaded = True
  >>> G = Symbol('G'); ee = Symbol('ee')
  >>> RCoupling._order_value_coupling = {}
  >>> RCoupling._all_couplings = []
  >>> RCoupling._coupling_values = {}
  >>> RCoupling._orders = ['QCD', 'QED']
  >>> RCoupling.param_orders = {'G': {'QED': 0, 'QCD': 1},\
                                'ee': {'QED': 1, 'QCD': 0}}

  Register a new coupling by passing a coupling value and returning the
  associated R coupling.
  >>> RCoupling(16*G**2*ee, 'Tree').return_coupling()
  GC_0
  >>> RCoupling(17*G**2).return_coupling()
  GC_1

  Custom name
  >>> RCoupling(18*G**2, 'Loop', opt_name = 'dZg').return_coupling()
  dZg

  Extend the branch domain (see couplings_regged below)
  >>> RCoupling(16*ee*G**2, 'Loop').return_coupling()
  GC_0

  A new coupling can be defined by a previous one
  >>> RCoupling(Symbol('GC_0')*ee, 'Loop').return_coupling()
  GC_3

  A new coupling can be registered without explicitly checking that the (value)
  of the coupling already exists.
  >>> RCoupling(16*ee*G**2, 'Loop', check_coupling=False).\
  return_coupling()
  GC_4

  A coupling can be processed without registering which means that the coupling
  is in the database of values etc., but not in couplings_regged.
  >>> gc = RCoupling(32*G**2, 'Loop', register=False).return_coupling()

  This new coupling can be used to define a new one. The computation of orders
  is different and the algorithm reuses the results for gc.
  >>> RCoupling(gc*G, 'Loop').return_coupling()
  GC_6

  Ambiguous name definition raises exception
  >>> RCoupling(19*G**2, 'Loop', opt_name='dZg').return_coupling()
  Traceback (most recent call last):
    ...
  Exception: The coupling symbol dZg is already listed.

  In new_couplings are stored all couplings instanced after the R
  modelfile.
  >>> RCoupling.new_couplings
  [GC_0, GC_1, dZg, GC_3, GC_4, GC_5, GC_6]

  As the couplings are R coupling instances you can get, for instance,
  the order via
  >>> RCoupling.new_couplings[0].order == {'QED': 1, 'QCD': 2}
  True

  >>> RCoupling.new_couplings[3].order == {'QED': 2, 'QCD': 2}
  True


  All coulpings which are `registered` after the R modelfile are stored
  in couplings_regged
  >>> list(sorted(RCoupling.couplings_regged.keys()))
  ['GC_0', 'GC_1', 'GC_3', 'GC_4', 'GC_6', 'dZg']

  You can get the associated branch(es) via
  >>> RCoupling.couplings_regged['GC_0']
  ['Tree', 'Loop']

  A new coupling can be mixture of orders, then a sum of couplings of different
  order is returned.
  >>> RCoupling(Symbol('GC_0')*ee + G, 'Loop').return_coupling()
  GC_3 + GC_7

  Disable computing the order of the coupling
  >>> gc2 = RCoupling(gc*gc, 'Loop',\
                              compute_order=False).return_coupling()
  >>> RCoupling._coupling_orders[gc2.name] == {'QED': 0, 'QCD': 0}
  True


  Bugfix test:
  >>> c = sympify('I*G**2*(-Nc**2 + 1)/(16*pi**2*Nc)')
  >>> cret = RCoupling(c).return_coupling()
  >>> RCoupling.coupling_values[cret.name]
  -I*G**2*(Nc - 1)*(Nc + 1)/(16*pi**2*Nc)

  Bugfix test:
  >>> alpha = Symbol('alpha')
  >>> RCoupling.param_orders['alpha'] = {'QED': 2, 'QCD': 0}
  >>> c = sympify('24*sqrt(alpha)*sqrt(pi)')
  >>> cret = RCoupling(c).return_coupling()
  >>> RCoupling.coupling_values[cret.name]
  24*sqrt(pi)*sqrt(alpha)
  >>> RCoupling.coupling_orders[cret.name] == {'QED': 1, 'QCD': 0}
  True
  """
  # StorageMeta attributes. Those attributes are stored when `store` is called.
  storage = ['orders', 'coupling_values', 'coupling_orders',
             'order_value_coupling', 'coupling_dict', 'all_couplings',
             'original', 'coupling_values_ext']

  # StorageMeta storage path
  #path = os.path.dirname(os.path.realpath(__file__)) + '/ModelCouplings'
  mpath, _ = model.get_modelpath()
  path = mpath + '/ModelCouplings'

  # StorageMeta filename
  name = 'coupling_data.txt'

  couplings_in = {}

  def __init__(self, coupling, branch_tag='', opt_name=None,
               check_coupling=True, register=True, compute_order=True,
               simp_func=factor, subs_cvalues=True, forbid_update=True,
               debug=False, add_order=False, original=False):
    """ Generates a new R coupling given a parameter representation of
    the coupling. Multiple couplings are generated only if orders are
    (additively) mixed.

    :coupling: Sympy expression of the coupling
    :branch_tag: Assign a tag to the coupling. A coupling can have multiple
                 tags.
    :opt_name: optional name for the coupling, not following the GC_??
               convention.
    :check_coupling: verifies if coupling has already been registered, which
                     avoids defining a known coupling again.
    :register: If false the coupling is processed as other couplings except
               that it is not put in `couplings_regged`. See examples on how to
               use as for optimizing.

    :simp_func: simplification function applied to expressions
    """
    self.debug = debug
    self.simp_func = simp_func
    self.couplings = []
    cls = self.__class__

    # check if same coupling expression already registered
    if check_coupling and coupling in RCoupling.couplings_in:
      self.couplings = RCoupling.couplings_in[coupling]
      for c in self.couplings:
        if register:
          try:
            if branch_tag not in self.couplings_regged[c.name]:
              self.couplings_regged[c.name].append(branch_tag)
          except Exception:
            print("self.couplings:", self.couplings)
            print("coupling:", coupling)
            raise
      return
    if check_coupling and isinstance(coupling, Symbol):
      if coupling.name in cls.coupling_values:
        self.couplings.append(coupling)
        if register:
          if coupling.name not in self.couplings_regged:
            self.couplings_regged[coupling.name] = []
          if branch_tag not in self.couplings_regged[coupling.name]:
            self.couplings_regged[coupling.name].append(branch_tag)
        return

    # do not register imaginary unit as coupling
    if coupling == I or coupling == -I:
      self.couplings = [coupling]
      return

    #  Sympys Polynomial methods work only with positive powers which is why we
    #  divide the expr in numerator and denominator which should in principle
    #  work because the simplify expression casts the expression usually in a
    #  numerator/denominator with only positive powers. <- This assumption might

    # TODO: (nick 2015-03-13)
    # The routines can be made safe by expanding any expression and applying the
    # following for the addends.
    #  expr = expr.expand().args -> list of sympy.core.mul.Mul or Symbol objects

    if compute_order:
      try:
        ord = self.compute_order_general(coupling)
      except Exception:
        log('Failed compute_order_general for: ' + str(coupling), 'error')
        raise
    else:
      default_order = tuple((u, 0) for u in cls.orders)
      ord = {default_order: coupling}

    # couplings have been assigned an order
    for order_key in ord:
      c_value = self.simp_func(ord[order_key])

      # Verify if the coupling is a new one
      order = {u[0]: u[1] for u in order_key}
      if check_coupling:
        cc = self._check_coupling(c_value, order, subs_cvalues)
      else:
        cc = None

      if cc is None:
        # register a new coupling
        if not opt_name:
          c_id = len(cls.all_couplings)
          c_name = 'GC_' + str(c_id)

          # collision -> need to find another unused coupling
          # in between 0 and c_id
          if c_name in self._coupling_values:
            c_name = None
            ff = ((('GC_' + str(i)) in self._coupling_values, i)
                  for i in xrange(c_id))
            for f in ff:
              if f[0]:
                c_name = 'GC_' + str(f[1])
                break
          # still no valid coupling name found -> increase id untill there is a
          # free coupling
          if c_name is None:
            while True:
              c_id = c_id + 1
              c_name = 'GC_' + str(c_id)
              if c_name not in self._coupling_values:
                break

        else:
          if len(ord) == 1 and not add_order:
            # coupling does not mix different orders
            c_name = opt_name
          else:
            # if the coupling mixes different perturbative orders, the coupling
            # id is extended by an order id
            order_id = '_'.join([u + str(order[u]) if order[u] > 0 else
                                 u + 'M' + str(-order[u])
                                 for u in (v for v in order if order[v] != 0)])
            c_name = opt_name + '_' + order_id

          # check if the symbol is already present and raise exception if not
          # allowed to update
          if forbid_update and c_name in cls.coupling_values:
            raise Exception('The coupling symbol ' + c_name +
                            ' is already listed.')

        #if simplify:
        if subs_cvalues:
          cdep = self.get_couplings_from_expr(c_value)
          cdict = {u: cls.coupling_values[u.name] for u in cdep}
          c_value = self.simp_func(c_value.subs(cdict))
        else:
          c_value = self.simp_func(c_value)

        # new R Coupling instance
        nc = self.Coupling(c_name, str(c_value), order)

        # global arrays
        self.new_couplings.append(nc)
        self._all_couplings.append(nc)

        # local array (not shared between RCoupling instances)
        self.couplings.append(nc)

        # hashable order ((order_type, value), (..,..), ... )
        coupl_order = tuple([(order_type, order[order_type])
                            for order_type in self._orders])
        if coupl_order not in self._order_value_coupling:
          self._order_value_coupling[coupl_order] = {}
        self._order_value_coupling[coupl_order][c_value] = nc
        if register:
          self.couplings_regged[c_name] = [branch_tag]
        self._coupling_orders[c_name] = order
        self._coupling_values[c_name] = c_value
        self._coupling_dict[c_name] = nc
        self._original[c_name] = original
      else:
        self.couplings.append(cc)
        if register:
          if cc.name not in self.couplings_regged:
            self.couplings_regged[cc.name] = []
          if branch_tag not in self.couplings_regged[cc.name]:
            self.couplings_regged[cc.name].append(branch_tag)

    if register:
      self.couplings_in[coupling] = self.couplings

  @classmethod
  def update_coupling(cls, coupling_name, value, order):
    if type(coupling_name) is not str:
      c = coupling_name.name
    else:
      c = coupling_name
    if c not in cls.coupling_values:
      raise Exception('Cannot update coupling ' + str(coupling_name) +
                      '. Not found in coupling values.')

    cls._coupling_orders[c] = order
    cls._coupling_values[c] = value

  @classmethod
  def compute_order_general(cls, expr):
    """ Computes the order of a generic expression expressed by couplings,
    parameters and counterterm paramters. """

    # do not try to determine the order directly by using
    # `insert_order_base`... which can lead to mistakes due to addition, e.g.
    # GC_XY - GC_YZ  -> QCD^2 - QCD^2=0 if both couplings are of order QCD^2.
    # Happens in SM_DC_BFM for the vertex: P_WPlus, P_uAnti, P_d

    # Try to extract a prefactor
    if expr.is_Mul:
      m = expr.args
    elif expr.is_Pow:
      nexpr, exp = expr.args
      assert(exp.is_number)

      def exp_order(order, exp):
        return tuple((u[0], u[1] * exp) for u in order)

      nexpr_ord = cls.compute_order_general(nexpr)
      if len(nexpr_ord) != 1:
        log('Unhandled case in compute_order.', 'error')
        log('expr:' + str(expr), 'debug')
        sys.exit()
      ret = {exp_order(ord, exp): expr for ord in nexpr_ord}
      return ret
    else:
      c = together(expr)
      if c.is_Mul:
        m = c.args
      else:
        m = [c]

    m_ord = []
    for pos, mul in enumerate(m):
      mul_exp = mul.expand()
      # need to expand `mul` in order to assure that no more +- is involved in
      # the expression `mul`
      if mul_exp.is_Mul or mul_exp.is_Pow or mul_exp.is_Symbol:
        m_ord.append(cls.compute_order(mul_exp))
      elif mul_exp.is_number:
        order = {u: 0 for u in cls.orders}
        order = tuple([(u, order[u]) for u in cls.orders])
        m_ord.append({order: mul_exp})
      elif mul_exp.is_Function:
        log('Warning, encountered function. ' +
            ' Assuming no order in fundamental couplings!', 'warning')
        order = {u: 0 for u in cls.orders}
        order = tuple([(u, order[u]) for u in cls.orders])
        m_ord.append({order: mul_exp})
      else:
        #  The expression is more compliacted and consists of sums and
        #  multiplication

        # Try again to extract a prefactor
        mul_tog = together(mul)
        if mul_tog.is_Add:
          # No common prefactor, divide the expression into addends
          mul_tog_ord = {}
          for u in mul_tog.args:
            # The addends can be simple, i.e. only a Symbol or a product of
            # symbol
            if u.is_number:
              zero_order = tuple([(v, 0) for v in cls.orders])
              u_ord = {zero_order: u}
            elif u.is_Symbol:
              if u.name in cls.coupling_orders:
                order = cls.coupling_orders[u.name]
                order = tuple([(v, order[v]) for v in cls.orders])
                u_ord = {order: u}
              else:
                u_ord = cls.compute_order(u)
            elif (-u).is_Symbol and (-u).name in cls.coupling_orders:
              order = cls.coupling_orders[(-u).name]
              order = tuple([(v, order[v]) for v in cls.orders])
              u_ord = {order: u}
            else:
              ue = u.expand()
              ut = together(u)
              if ue.is_Mul or ue.is_Pow:
                u_ord = cls.compute_order(u)
              elif ut.is_Mul:
                u_ord = cls.compute_order_general(ut)
              elif ut.is_Pow:
                u_ord = cls.compute_order_general(ut)

              # addends are more complicated
              else:
                u_ord = {}
                for uea in ue.args:
                  # expressions are fully expanded and we do not gain anything
                  # by calling compute_order_general -> end of recursion.
                  for ord, val in iteritems(cls.compute_order(uea)):
                    if ord not in u_ord:
                      u_ord[ord] = val
                    else:
                      u_ord[ord] += val

            # reconstruct the order of mul_tog
            for ord, val in iteritems(u_ord):
              if ord not in mul_tog_ord:
                mul_tog_ord[ord] = val
              else:
                mul_tog_ord[ord] += val


          m_ord.append(mul_tog_ord)
        else:
          # sum could be represented as multiplication
          m_ord.append(cls.compute_order_general(mul_tog))

    # build up the full order dependence
    if len(m_ord) > 1:
      joined_ord = fold(lambda x, y: cls.multiply_ordered(x, y),
                        m_ord[1:], m_ord[0])
    else:
      joined_ord = m_ord[0]

    for key in joined_ord:
      assert([u[0] for u in key] == cls.orders)
    return joined_ord

  @classmethod
  def multiply_ordered(cls, ord_dicta, ord_dictb):
    """ Computes the product of two ordered expression given in the form
     of {order1: expression1, order_key2: expression2, ...}

     :param ord_dicta: Ordered expression
     :type  ord_dicta: dict

     :param ord_dictb: Ordered expression
     :type  ord_dictb: dict

     >>> ord1 = {(('QED', 4), ('QCD', 5)): Symbol('a')}
     >>> ord2 = {(('QED', 2), ('QCD', 1)): Symbol('b'), \
(('QED', 1), ('QCD', 1)): Symbol('c')}
     >>> ordr = RCoupling.multiply_ordered(ord1, ord2)

     >>> key1 = {'QED': 5, 'QCD': 6}
     >>> key1 = tuple([(u, key1[u]) for u in RCoupling.orders])
     >>> ordr[key1]
     a*c
     >>> key2 = {'QED': 6, 'QCD': 6}
     >>> key2 = tuple([(u, key2[u]) for u in RCoupling.orders])
     >>> ordr[key2]
     a*b
     """
    ret = {}
    for orda in ord_dicta:
      for ordb in ord_dictb:
        orda_d = {u[0]: u[1] for u in orda}
        ordb_d = {u[0]: u[1] for u in ordb}

        keys = set(list(orda_d) + list(ordb_d))
        new_ord = {}
        for key in keys:
          new_ord[key] = 0
          if key in orda_d:
            new_ord[key] += orda_d[key]
          if key in ordb_d:
            new_ord[key] += ordb_d[key]

        new_ord = tuple((u, new_ord[u]) for u in cls.orders if u in cls.orders)
        if new_ord in ret:
          ret[new_ord] += ord_dicta[orda] * ord_dictb[ordb]
        else:
          ret[new_ord] = ord_dicta[orda] * ord_dictb[ordb]
    return ret

  @classmethod
  def compute_order(cls, expr):
    """ Computes the order of a multplicatively connected expression

    :param return: order dictionary: {order1:, value1, order2: value2, ...}
    :type  return: dict
    """

    # a number has no order
    if expr.is_number:
      order = {u: 0 for u in cls.orders}
      order = tuple([(u, order[u]) for u in cls.orders])
      return {order: expr}

    # symbol is a special case of a multiplicatively connected expression
    elif expr.is_Symbol:
      order = cls.insert_order_base(expr)
      order = cls.make_order_dict(order)

      order = tuple([(u, order[u]) for u in cls.orders])
      return {order: expr}

    # at this point the expression has to be of type Mul or Pow
    if not (expr.is_Mul or expr.is_Pow):
      log('Unhandled case in compute_order.', 'error')
      log('expr:' + str(expr), 'debug')
      sys.exit()

    # Some expressions have no polynomial order even though their power
    # expression does, e.g. 1/vev is of order QED, but vev is QED^-1
    try:
      if len(expr.free_symbols) == 1 and expr.is_Pow:
        nexpr, exp = expr.args
        assert(exp.is_number)

        def exp_order(order, exp):
          return tuple((u[0], u[1] * exp) for u in order)

        nexpr_ord = cls.compute_order_general(nexpr)
        if len(nexpr_ord) != 1:
          log('Unhandled case in compute_order.', 'error')
          log('expr:' + str(expr), 'debug')
          sys.exit()
        ret = {exp_order(ord, exp): expr for ord in nexpr_ord}
        return ret
    except PolynomialError:
      # failed, but should still work for the whole expression
      pass

    # The strategy is to compute the order separately for numerator and
    # denominator, otherwise we cannot use the POLY method.
    num, den = fraction(expr)

    # the expressions are then expressed as polynomials of parameters (not
    # couplings).
    order_base = [Symbol(u) for u in cls.param_orders
                  if any(cls.param_orders[u].values())]

    # No known parameter found in expr -> No order at all.
    if len(order_base) == 0:
      zero_order = tuple([(v, 0) for v in cls.orders])
      # Printing a warning since this case never occured before
      log('Assigning zero order to: ' + str(expr), 'warning')
      return {zero_order: expr}

    # Poly numerator
    try:
      num_poly = Poly(together(num), order_base).as_dict()
    except Exception:
      log('Failed to make a polynomial of expression.', 'error')
      log('expr: ' + str(expr), 'debug')
      log('num: ' + str(num), 'debug')
      log('order_base: ' + str(order_base), 'debug')
      raise

    num_poly_order = {}
    for c_new in num_poly:
      order = {u: 0 for u in cls.orders}
      for pos, power in enumerate(c_new):
        cc = cls.param_orders[order_base[pos].name]
        for order_type in cc:
          coupling_power = cc[order_type]
          # the new power of the coupling is computed by its own power times
          # the power the coupling has in the coupling constant
          order[order_type] += coupling_power * power
      num_poly_order[c_new] = order

    # Poly denominator
    den_poly = Poly(together(den), order_base).as_dict()
    den_poly_order = {}
    for den_new in den_poly:
      order = {u: 0 for u in cls.orders}
      for pos, power in enumerate(den_new):
        cc = cls.param_orders[order_base[pos].name]
        for order_type in cc:
          coupling_power = cc[order_type]
          # the new power of the coupling is computed by its own power times
          # the power the coupling has in the coupling constant
          order[order_type] += coupling_power * power
      den_poly_order[den_new] = order

    # Next we calculate the full order dependence by inverting the dicts and
    # computing the order of the poly coefficients.
    if len(den_poly_order) == 0:  # simple optimization for denominator
      den_key = None
      expr_den = 1
      order_den = {u: 0 for u in cls.orders}
    else:  # general case
      expr_den = 0
      prefactor_base_ord = None
      for den_key in den_poly_order.keys():
        base = fold(lambda x, y: x * y,
                    [order_base[pos]**power for pos, power in
                     enumerate(den_key)], 1)
        prefactor = den_poly[den_key]

        try:
          prefac_ord = cls.insert_order_base(prefactor)
        except Exception:
          log('Failed computing order for prefactor:' + str(expr), 'error')
          log('Extracted prefactor:' + str(prefactor), 'error')
          log('Make sure the expression has a unique order.', 'error')
          raise
        try:
          base_ord = cls.insert_order_base(base)
          # prefac_ord = cls.make_order_dict(prefac_ord)
        except Exception:
          log('Failed computing order for base:' + str(expr), 'error')
          log('Extracted base:' + str(base), 'error')
          log('Make sure the expression has a unique order.', 'error')
          raise
        prefacbase_ord = cls.make_order_dict(prefac_ord * base_ord)

        # denominator must have a unique order
        if prefactor_base_ord is not None:
          try:
            assert(prefactor_base_ord == prefacbase_ord)
          except Exception:
            print("den:", den)
            print("order_base:", order_base)
            print("den_poly:", den_poly)
            print("den_poly_order:", den_poly_order)
            raise
        else:
          prefactor_base_ord = prefacbase_ord
        expr_den += base * prefactor

      order_den = prefactor_base_ord

    # compute order dependence of num / den
    expr_ord = {}
    for num_key in num_poly:
      # retrieve the order of the parameters and multiply divide by the den
      # order
      order = num_poly_order[num_key]
      order = cls.divide_orders(order, order_den)

      # compute the order of the prefactor
      expr_num = 0
      base = fold(lambda x, y: x * y,
                  [order_base[pos]**power for pos, power in
                   enumerate(num_key)], 1)

      prefactor = num_poly[num_key]
      prefactor_order = cls.insert_order_base(prefactor)
      prefactor_order = cls.make_order_dict(prefactor_order)
      # full order of the expression
      order = cls.multiply_orders(order, prefactor_order)

      expr_num += base * prefactor
      c_expr = expr_num / expr_den

      # hashable order key
      order = tuple([(u, order[u]) for u in cls.orders])
      if order not in expr_ord:
        expr_ord[order] = 0

      expr_ord[order] += c_expr
    return expr_ord

  @staticmethod
  def divide_orders(num_ord, den_ord):
    """ Multiplication rule for order dictionaries.
    >>> num_ord = {'QED': 4, 'QCD': 5}; den_ord = {'QED': 2, 'QCD': 1}
    >>> ordr = RCoupling.divide_orders(num_ord, den_ord)
    >>> ordr['QED']
    2
    >>> ordr['QCD']
    4
    """
    if den_ord is not None:
      return {u: num_ord[u] - den_ord[u] for u in num_ord}
    else:
      return num_ord

  @staticmethod
  def multiply_orders(ord1, ord2):
    """ Multiplication rule for order dictionaries.
    >>> ord1 = {'QED': 4, 'QCD': 5}; ord2 = {'QED': 2, 'QCD': 1}
    >>> ordr = RCoupling.multiply_orders(ord1, ord2)
    >>> ordr['QED']
    6
    >>> ordr['QCD']
    6
    """
    if ord1 == 1:
      return ord2
    elif ord2 == 1:
      return ord1
    else:
      keys = set(list(ord1) + list(ord2))
      # not sure if needed to initialize non-existent orders
      ord1_new = {u: ord1[u] if u in ord1 else 0 for u in keys}
      ord2_new = {u: ord2[u] if u in ord2 else 0 for u in keys}
      return {u: ord1_new[u] + ord2_new[u] for u in keys}

  @classmethod
  def remove_couplings(cls, couplings):
    """ Removes the coupling from the Storage attributes. """
    if isinstance(couplings, list_type):
      rem = [u.name for u in couplings]
    else:
      rem = [couplings.name]

    for c in cls._all_couplings:
      if c.name in rem:
        cls._all_couplings.remove(c)

    for order in cls._order_value_coupling.copy():
      for c_val in cls._order_value_coupling[order].copy():
        c = cls._order_value_coupling[order][c_val]
        if c.name in rem:
          del cls._order_value_coupling[order][c_val]

    for c in rem:
      del cls._coupling_orders[c]
      del cls._coupling_values[c]
      del cls.couplings_regged[c]
      del cls._coupling_dict[c]
      del cls._original[c]

    for c in list(cls.couplings_in.keys()):
      if any(u in [v.name for v in cls.couplings_in[c]] for u in rem):
        del cls.couplings_in[c]

  def pow_to_mul(self, expr):
    """ Convert integer powers in an expression to Muls, e.g. a**2 => a*a. """
    pows = list(expr.atoms(Pow))
    if any(not e.is_Integer for b, e in (i.as_base_exp() for i in pows)):
        raise ValueError("A power contains a non-integer exponent")
    repl = zip(pows, (Mul(*[b] * e, evaluate=False)
                      for b, e in (i.as_base_exp() for i in pows)))
    return expr.subs(repl)

  def _check_coupling(self, coupling, order, subs_cvalues=True):
    """ Checks if 'coupling' is in `order_value_coupling` and returns the
    coupling in case of success """
    cls = self.__class__
    if coupling.is_Symbol:
      if coupling.name in cls.coupling_orders:
        return coupling
    coupl_order = tuple([(order_type, order[order_type])
                         for order_type in cls.orders])
    if coupl_order not in cls.order_value_coupling:
      # new coupl_order => coupling does not exist yet
      return None
    else:
      # new coupling=> coupling does not exist yet
      if subs_cvalues:
        cdep = self.get_couplings_from_expr(coupling)
        cdict = {u: cls.coupling_values[u.name] for u in cdep}
        c_value = self.simp_func(coupling.subs(cdict))
      else:
        c_value = coupling

      if c_value not in cls.order_value_coupling[coupl_order]:
        return None
      else:
        # coupling exists return the coupling
        ret_c = cls.order_value_coupling[coupl_order][c_value]

        # I had a bug once that the coupling stored in `order_value_coupling`
        # did not match the value stored in `coupling_values`. If this happens
        # the modelfile is corrupted and should not be trusted. The following
        # catches the bug and stops the program.
        try:
          diff = cls.coupling_values[ret_c.name] - c_value
          assert(together(diff) == 0)
        except:
          log('ERROR IN _check_coupling', 'error')
          log('Coupling:' + str(coupling), 'debug')
          log('Value:' + str(c_value), 'debug')
          log('ret_c:' + str(ret_c), 'debug')
          ret_c_value = cls.coupling_values[ret_c.name]
          log('ret_c_value:' + str(ret_c_value), 'debug')
          raise
        return ret_c

  def _check_coupling_value(self, coupling_value):
    """ Checks if 'coupling_value' is in `coupling_values` and returns the
    coupling in case of success """
    cls = self.__class__
    if coupling_value in cls.coupling_values.values():
      index = cls.coupling_values.values().index(coupling_value)
      coupling = list(cls.coupling_values.keys())[index]
      return coupling
    else:
      return None

  def return_coupling(self):
    self.last_coupling_return = self.couplings
    ret = 0
    for c in self.couplings:
      ret += sympify(c)
    return ret

  @classmethod
  def generate_CT(cls, repl=None):
    """ Registers all counterterm coupling. """
    from rept1l.counterterms import Counterterms
    Counterterms.load()

    if not hasattr(cls, 'ct_coupling_reg'):
      cls.ct_coupling_reg = {}

    from rept1l.pyfort import ProgressBar
    log('Register counterterm couplings', 'info')
    PG = ProgressBar(len(Counterterms.cts))
    for coupling in Counterterms.cts:
      PG.advanceAndPlot(status=str(coupling))
      if coupling in cls.ct_coupling_reg:
        continue
      ct_expr = Counterterms.cts[coupling]
      if repl:
        ct_expr = ct_expr.subs(repl)

      compute_order = False if repl is None else True
      couplingCT = RCoupling(ct_expr, 'CT', check_coupling=False,
                             register=False,
                             compute_order=compute_order,
                             subs_cvalues=False).return_coupling()
      cls.ct_coupling_reg[coupling] = couplingCT

  @classmethod
  def register_ct_coupling(cls, coupling, ct_expr, repl=None, ctexpansion=True):
    """ Registers the counterterm coupling associated to `coupling`. Registering
    the ct coupling assigns a unique coupling symbol, computes a perturbative
    order if repl is not None and possibly splits the coupling into different
    couplings if different orders are observed.

    :param coupling: R coupling as sympy expression
    :type  coupling: sympy/str


    :param repl: A replacement dictionary. Used to replace expressions for ct
                 parameter in the expansion of `coupling`.
    :type  repl: dict
    """

    if not hasattr(cls, 'ct_coupling_reg'):
      cls.ct_coupling_reg = {}

    if type(coupling) is Symbol:
      coupl_name = coupling.name
    else:
      assert(type(coupling) is str)
      coupl_name = coupling

    # coupling already registered in this run, no need to register again.
    if coupl_name in cls.ct_coupling_reg:
      return

    if repl:
      ct_expr = ct_expr.subs(repl)

    # the compute_order flag is triggered only if `repl` is not zero.
    # The reason is that in the first run the the perturbative order of
    # counterterms cannot be determined properly. In the second run,
    # after renormalization, parameters are split into their
    # perturbative parts (`repl` is now a replacement dictionary) and the
    # perturbative order of the counterterms are fixed.
    compute_order = False if repl is None else True

    couplingCT = cls(ct_expr, 'CT', check_coupling=False, register=False,
                     compute_order=compute_order,
                     subs_cvalues=False).return_coupling()
    if ctexpansion:
      cls.ct_coupling_reg[coupl_name] = couplingCT
    else:
      cls.update_coupling(Symbol(coupl_name),
                          cls.coupling_values[couplingCT.name],
                          cls.coupling_orders[couplingCT.name])
      cls.ct_coupling_reg[coupl_name] = None

  @classmethod
  def reduce_to_parameter(cls, coupling, external=False, force=False,
                          replace_yukawa=True, complex_mass=False,
                          zero_tadpole=True,
                          is_expr=False):
    """ Reduces a standard coupling (usually (not necessarily) starting with
    GC_) to parameters. The return value is the value of the coupling in terms
    of parameters only.

    :param coupling: Coupling
    :type  coupling: symbol(or expression)

    Optional

    :param external: If True the coupling is reduced to external parameters.
    :type  external: bool

    :param force: If True previous results are ignored and the coupling is
                  reduced again
    :type  force: bool

    :param replace_yukawa: Uses identities between Masses and Yukawa paraemter.
    :type  replace_yukawa: bool

    :param complex_mass: substitutes real mass expressions with complex ones
    :type  complex_mass: bool

    :param zero_tadpole: sets tadpole expressions to zero
    :type  zero_tadpole: bool

    :param is_expr: allows to use reduce_to_parameter for expressions
    :type  is_expr: bool
    """

    if not is_expr and coupling.name not in cls.coupling_values:
      raise Exception('coupling ' + str(coupling) + ' not in coupling values')

    # return the couplings value if it has already been reduced to parameters
    if not force and not is_expr and coupling.name in cls.coupling_values_ext:
      return cls.coupling_values_ext[coupling.name]

    if is_expr:
      value = coupling
    else:
      value = cls.coupling_values[coupling.name]

    couplings_left = cls.get_couplings_from_expr(value)

    # construct substition maps
    def chain(expr, *funcs):
      if len(funcs) > 0:
        return fold(lambda x, f: f(x), funcs, expr)
      else:
        return expr

    # construct substitution function out of a dictionary
    def subf(repl):
      def eval_sub(expr):
        return expr.subs(repl)
      return eval_sub

    substitutions = []

    if external:
      substitutions.append(subf(cls.get_internal_parameter_dict()))

    if replace_yukawa:
      substitutions.append(cls.replace_yukawa)

    if complex_mass:
      substitutions.append(cls.complex_mass_couplings)

    if zero_tadpole:
      substitutions.append(cls.set_tadpole_zero)

    substitutions.append(factor)

    # still need to substitute couplings
    if len(couplings_left) > 0:
      repl = {}
      for c in couplings_left:
        if c.name not in cls.coupling_values:
          raise Exception('coupling ' + str(c) + ' not in coupling values')
        c_red = cls.reduce_to_parameter(c, external=external, force=force,
                                        replace_yukawa=replace_yukawa,
                                        complex_mass=complex_mass,
                                        zero_tadpole=zero_tadpole,
                                        is_expr=False)
        repl[c] = factor(c_red)

      # coupling substitutions need to be performed first (added on the left)
      substitutions = [subf(repl)] + substitutions

    ret = chain(value, *substitutions)
    if not is_expr:
      cls._coupling_values_ext[coupling.name] = ret
    return ret

  @classmethod
  def find_couplings(cls, parameters, c_set=None, deep_search=False):
    """ Finds and returns couplings containing the parameter specified in
    `parameters`.

    :param parameters: parameters of the theory
    :type  parameters: Symbol or list of symbols

    :param c_set: restrict the search to the set of parameters `c_set`
    :type  c_set: list of couplings

    :param deep_search: Perform a deep search by resolving internal parameters
    :type  deep_search: bool
    """
    params = parameters if type(parameters) is list else [parameters]
    ret = []
    if c_set:
      clst = c_set
    else:
      clst = cls.coupling_values

    for c in clst:
      val = cls.reduce_to_parameter(c, external=deep_search, force=True)
      # the `in` method does not work in this case for generator expression
      # must be some conflict with sympy
      # TODO: (nick 2015-11-26) understand !
      val_sym = [u for u in val.free_symbols]  # free_symbols returns set
      if any(param in val_sym for param in params):
        ret.append(c)
    return ret

  @classmethod
  def find_expression(cls, expressions, coupling, stop_on_error=False):
    """ Unreveals the parameter dependence on couplings values if a coupling
    depends on an expression in `expressions`. No deep search is
    performed (parameters are not replaced by external ones) and the search
    stops after successfully finding one parameter.
    """
    params = expressions if type(expressions) is list else [expressions]

    if coupling.is_Symbol:
      #if coupling.name.startswith('GC_') :
        #c = cls.coupling_values[coupling.name]
      if coupling.name in cls.coupling_values:
        c = cls.coupling_values[coupling.name]
      else:
        c = coupling
    else:
      c = coupling

    # the coupling is brought to standard form: sum over products of couplings
    ce = c.expand()

    # res corresponds to the same sum, but where coupling expressions have
    # been replaced. res is constructed recursively
    res = 0

    # 3 cases:
    # If c is additive we separate c into addends and call find_expression for
    # each of these addends
    if ce.is_Add:
      cea = ce.args
    elif ce.is_Mul:
      cea = [ce]
    elif ce.is_Symbol:
      # prevent recursion loop -> Symbol is not a coupling
      if ce == coupling:
        return ce
      else:
        return cls.find_expression(expressions, ce)
      # TODO: (nick 2016-02-19) description
    elif ce.is_number:
      return ce

    # if any sub expression in coupling has been substituted the new expression
    # is returned, otherwise the original expression is returned unaltered
    any_subs = False
    for ca in cea:
      ca_sym = [u for u in ca.free_symbols]
      # found the parameters in the coupling value
      if any(param in ca_sym for param in params):
        any_subs = True
        res += ca
      else:
        # ca can be defined through other couplings. Find their expressions and
        # add the results together
        #ca_sym_c = [u for u in ca_sym if u.name.startswith('GC_')]
        ca_sym_c = [u for u in ca_sym if u.name in cls.coupling_values]
        if len(ca_sym_c) > 0:
          repl = {}
          for u in ca_sym_c:
            expr = cls.find_expression(expressions, u)
            # only accept real substitutions
            if expr != u:
              repl[u] = expr
          if len(repl) > 0:
            any_subs = True
            res += ca.subs(repl)
          else:
            if stop_on_error:
              p = ', '.join(u.name for u in params)
              raise Exception('Coupling ' + ce.name + ' does not have one ' +
                              'of the parameters ' + p + '.')
            else:
              res += ca
        else:
          if stop_on_error:
            p = ', '.join(u.name for u in params)
            raise Exception('Coupling ' + ce.name + ' does not have one ' +
                            'of the parameters ' + p + '.')
          else:
            res += ca
    if any_subs:
      return res
    else:
      return coupling

  @staticmethod
  def get_couplings_from_expr(expr):
    return [u for u in expr.free_symbols if (u.name).startswith('GC_')]

  @classmethod
  def update_regged_couplings(cls, branch, *selection):
    """ Appends the list of couplings (`selection`) to the registered couplings
    and sets the branch to `branch` """
    for c in selection:
      if type(c) is str:
        cc = c
      else:
        cc = c.name
      if cc not in cls.couplings_regged:
        cls.couplings_regged[cc] = [branch]
      elif branch not in cls.couplings_regged[cc]:
        cls.couplings_regged[cc].append(branch)

  @classmethod
  def enlarge_regged_couplings(cls, selection=[]):
    """ Enlarges the list of registered couplings. The method runs through all
    the couplings in the selection and checks on the dependency on other
    couplings which are yet not registered. Dependencies are resolved by
    appending the necessary couplings to the list of registered couplings.

    selection: If no selection is provided the default selection is
               cls.couplings_regged
    """
    select = len(selection) > 0
    if not select:
      selection = list(cls.couplings_regged.keys())

    # trigger if new couplings have been need to be checked for dependency
    new = False

    for c in selection[:]:
      val = cls.coupling_values[c]
      val_syms = cls.get_couplings_from_expr(val)
      for sym in val_syms:
        if sym.name not in selection:
          selection.append(sym.name)
          # if the coupling has the same order we assume the same branches
          if cls.coupling_orders[sym.name] == cls.coupling_orders[c]:
            cls.couplings_regged[sym.name] = cls.couplings_regged[c]
          # for unresolved couplings we assume the branch is a tree branch!
          else:
            cls.couplings_regged[sym.name] = ['Tree']
          new = True

    if new:
      cls.enlarge_regged_couplings(selection=selection)

  @classmethod
  def parameter_ordered(cls, ordered=[], selection=[]):
    """ Returns the list of internal parameters in such a way that they
    can be evaluated successively.
    """
    select = len(selection) > 0
    if not select:
      selection = cls.get_internal_parameters()
      cls.external_params_name = [p.name for p in cls.get_external_parameters()]

    parameter_dep = {}
    for c in selection:
      val_syms = cls.parse_to_sympy(c.value).free_symbols
      val_syms = [p for p in val_syms if p.name not in cls.external_params_name]
      parameter_dep[Symbol(c.name)] = val_syms

    for u in list(parameter_dep.keys()):
      dep = parameter_dep[u]
      if u in dep:
        raise Exception('Parameter ' + u.name + 'depends on its own value.')

      if dep == []:
        ordered.append(u)
        del parameter_dep[u]
      else:
        if all((v in ordered) for v in dep):
          ordered.append(u)
          del parameter_dep[u]

    if len(parameter_dep) > 0:
      new_selection = [u for u in selection if Symbol(u.name) in parameter_dep]
      return cls.parameter_ordered(ordered=ordered,
                                   selection=new_selection)
    else:
      return ordered

  @classmethod
  def regged_couplings_ordered(cls, ordered=[], selection=[],
                               plotProgress=False, PG=None):
    """ Returns the list of registered couplings ordered in such a way that they
    can be evaluated successively.
    >>> G = Symbol('G')
    >>> RCoupling._order_value_coupling = {}
    >>> RCoupling._all_couplings = []
    >>> RCoupling.couplings_regged = {}
    >>> RCoupling._coupling_values = {}

    >>> RCoupling(G**2).return_coupling()
    GC_0
    >>> RCoupling(-Symbol('GC_0'), subs_cvalues=False).return_coupling()
    GC_1
    >>> RCoupling(-Symbol('GC_1'), subs_cvalues=False).return_coupling()
    GC_2

    >>> sorted(RCoupling.regged_couplings_ordered())
    ['GC_0', 'GC_1', 'GC_2']
    """
    select = len(selection) > 0
    if not select:
      selection = cls.couplings_regged.keys()

    # generate the dictionary of coupling dependence
    couplings_dep = {}  # -> {coupling: [ coupling1, coupling2, ..], ...}
    for c in selection:
      val = cls.coupling_values[c]
      val_syms = cls.get_couplings_from_expr(val)
      couplings_dep[c] = [u.name for u in val_syms]

    if plotProgress and PG is None:
      from rept1l.pyfort import ProgressBar
      PG = ProgressBar(len(couplings_dep))
      PG.plotProgress()

    for u in list(couplings_dep):
      dep = couplings_dep[u]
      if u in dep:
        raise Exception('Coupling ' + u + ' depends on its own value.')

      # if the coupling does not depend on any other coupling we can compute it
      if dep == []:
        ordered.append(u)
        del couplings_dep[u]

      # otherwise we check if the dependence is already computed
      else:
        if all((v in ordered) for v in dep):
          ordered.append(u)
          del couplings_dep[u]

    if plotProgress and PG is not None:
      PG.setLevel(PG.end - len(couplings_dep))
      PG.plotProgress()

    if len(couplings_dep) > 0:
      new_selection = couplings_dep.keys()
      return cls.regged_couplings_ordered(ordered=ordered,
                                          selection=new_selection,
                                          plotProgress=plotProgress,
                                          PG=PG)
    else:
      return ordered

  @classmethod
  def get_internal_parameter_dict(cls, treat_external=[]):
    """ Returns the dicitionary expressing internal parameters by external ones.

    :treat_external: A list of parameter names which are not substituted.
    """
    if hasattr(cls, 'param_int_dict'):
      return cls.param_int_dict
    internal_param = {Symbol(u.name): u for u in cls.get_internal_parameters()
                      if u.name not in treat_external}

    internal_sym = [u for u in cls.parameter_ordered()
                    if u.name not in treat_external]
    ret = {}

    # iterate over all internal values in the order of evaluation
    for p in internal_sym:
      p_val = cls.parse_to_sympy(internal_param[p].value)
      # if the current parameter has internal parameter dependence, this
      # dependence is resolved by substituting with ret.
      for u in p_val.free_symbols:
        if u in internal_sym:
          p_val = p_val.xreplace(ret)
          break

      ret[p] = p_val

    cls.param_int_dict = ret
    return ret

  @classmethod
  def get_internal_parameters(cls):
    """ Returns internal parameters except for counterterm parameters """
    from rept1l.counterterms import get_param_ct
    counterterms = get_param_ct(model.model_objects.all_parameters)
    # check that there are no `internal` counterterms which is not supported
    internalct = [p.name for p in model.model_objects.all_parameters
                  if (p.nature == 'internal' and p.name in counterterms)]
    if len(internalct) > 0:
      log('The system found internal counterterm parameter which is not ' +
          'supported. All couterterm parameter have to be defined as ' +
          'external ones: ' + ', '.join(internalct) + '.',
          'error')
    return [p for p in model.model_objects.all_parameters
            if (p.nature == 'internal' and p.name not in counterterms)]

  @classmethod
  def get_external_parameters(cls):
    """ Returns external parameters except for counterterm parameters """
    from rept1l.counterterms import get_param_ct
    counterterms = get_param_ct(model.model_objects.all_parameters)
    return [p for p in model.model_objects.all_parameters
            if (p.nature == 'external' and p.name not in counterterms)]

  @classmethod
  def get_parameters(cls):
    """ Returns parameters except for counterterm parameters """
    from rept1l.counterterms import get_param_ct
    counterterms = get_param_ct(model.model_objects.all_parameters)
    return [p for p in model.model_objects.all_parameters
            if p.name not in counterterms]

  @classmethod
  def parameter_value_dict(cls):
    """ Solves the dependence of parameters on other parameters and computes
    their values.
    """
    if hasattr(cls, 'param_val_dict'):
      return cls.param_val_dict
    ext = cls.get_external_parameters()
    it = cls.get_internal_parameters()
    ext_val = {Symbol(p.name): sympify(p.value).evalf() for p in ext}

    param_vals = {Symbol(p.name): cls.parse_to_sympy(p.value).xreplace(ext_val).
                  evalf() for p in cls.get_parameters()}
    ipo = cls.parameter_ordered()
    for p in ipo:
      p_val = [u.value for u in it if u.name == p.name][0]
      p_val = cls.parse_to_sympy(p_val)
      param_vals[p] = p_val.xreplace(param_vals).evalf()

    cls.param_val_dict = param_vals
    return param_vals

  @classmethod
  def get_complex_mass_dict(cls):
    """ Generates a substitution dictionary  from real to complex masses. """
    if not hasattr(cls, 'mass_repl'):
      # replacing complex masses with real masses.
      from rept1l.particles import ParticleMass
      ParticleMass.load()

      # check if masses have been initialized
      if len(ParticleMass.masses) == 0:
        from rept1l.particles import gen_particle_class
        gen_particle_class('', '0', write=False)
        ParticleMass.store()

      cls.mass_repl = {Symbol(u.name): Symbol('c' + u.name)
                       for u in ParticleMass.masses.values()}
      yuk = {u: Symbol(ParticleMass.yukawa[u].name).
             xreplace(cls.mass_repl) for u in ParticleMass.yukawa}
      cls.mass_repl.update(yuk)

    return cls.mass_repl

  @classmethod
  def get_real_mass_dict(cls):
    """ Generates a substitution dictionary from complex to real masses. """
    if not hasattr(cls, 'cmass_repl'):
      # replacing complex masses with real masses.
      from rept1l.particles import ParticleMass
      ParticleMass.load()
      cls.cmass_repl = {Symbol('c' + u.name): Symbol(u.name)
                        for u in ParticleMass.masses.values()}
    return cls.cmass_repl

  @classmethod
  def get_coupling_value_num(cls, c):
    """ Returns the numerical value for the coupling `c` """

    # coupling values are cached for fast repetetive evaluation
    if not hasattr(cls, 'coupling_value_num'):
      cls.coupling_value_num = {}

    if c not in cls.coupling_value_num:
      pvd = cls.parameter_value_dict()
      mass_repl = cls.get_real_mass_dict()
      cret = cls.reduce_to_parameter(c).subs(mass_repl).subs(pvd).evalf()
      cls.coupling_value_num[c] = cret

    return cls.coupling_value_num[c]

  @classmethod
  def expression_value(cls, expr, psubs={}):
    """ Computes the numerical value of `expr` which can be composed of
    couplings and parameters.

    :param expr: expression to be evaluted numerically
    :type  expr: sympy

    :param psubs: substitutions performed before numerical evaluation
    :type  psubs: dict
    """
    coupls = [u for u in expr.free_symbols if (u.name).startswith('GC_')]
    param = [u for u in expr.free_symbols if not (u.name).startswith('GC_')]

    pvd = cls.parameter_value_dict()
    mass_repl = cls.get_real_mass_dict()
    mass_repl = {u: mass_repl[u] for u in mass_repl if u in param}
    c_vals = {c: cls.get_coupling_value_num(c) for c in coupls}
    expr = expr.xreplace(mass_repl)
    param = [u for u in expr.free_symbols if not (u.name).startswith('GC_')]
    pvd = {u: pvd[u] for u in pvd if u in param}
    return expr.xreplace(pvd).xreplace(c_vals).xreplace(psubs).evalf()

  @staticmethod
  def set_tadpole_zero(expr):
    """ Inserts the tadpole conditions """
    return expr.subs(model.zero_tadpole)

  @classmethod
  def replace_yukawa(cls, expr):
    """ Replaces all yukawaw terms with the associated mass terms"""
    from rept1l.particles import ParticleMass
    ParticleMass.load()

    if not hasattr(cls, 'yukawa_dict'):
      yukawa_dict = {Symbol(u.name): Symbol(v.name)
                     for u, v in iteritems(ParticleMass.yukawa)}
      cls.yukawa_dict = yukawa_dict

    ret = expr.subs(cls.yukawa_dict)
    return ret

  @classmethod
  def complex_mass_couplings(cls, expr, zero_parameter=False):
    """ Replaces any mass expression in `expr` with corresponding complex one.

    :param expr: expression on which substitutions are performed
    :type  expr: sympy

    :param zero_parameter: substitute for `zero` parameters, i.e. parameters
                           which can be safely set to zero
    :type  zero_parameter: bool
    """
    mass_repl = cls.get_complex_mass_dict()
    try:
      expr = expr.subs(mass_repl)
    except AttributeError:
      expr = sympify(expr).subs(mass_repl)
    if zero_parameter:
      from rept1l.particles import ParticleMass
      ParticleMass.load()
      zero_repl = {Symbol(u): 0 for u in ParticleMass.zero_parameters}
      expr = factor(expr.subs(zero_repl))
    return expr

  @classmethod
  def substitute_original(cls, expr, verbose=False):
    """ Method to replace all coupling expression with original (UFO) coupling
    expressions. """
    if not hasattr(cls, 'original_repl'):
      cls.original_repl = {}
    not_original_yet = [u for u in cls.get_couplings_from_expr(expr) if
                        u.name not in cls.original_repl and
                        not cls.original[u.name]]

    if verbose:
      print('Non original coupligns: ' + str(not_original_yet))
    for noy in not_original_yet:
      noy_val = cls.coupling_values[noy.name]
      cls.original_repl[noy.name] = cls.substitute_original(noy_val)
    return expr.subs(cls.original_repl)


if __name__ == "__main__":
  import doctest
  doctest.testmod()
