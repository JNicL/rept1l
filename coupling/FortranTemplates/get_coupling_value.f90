  function get_coupling_value_mdl(c_id) result(ret_c)
    integer, intent(in) :: c_id
    complex(dp) :: ret_c
    if (c_id .eq. 0) then
      ret_c = cnul
    else
      ret_c = coupl(c_id)
    end if
  end function get_coupling_value_mdl
