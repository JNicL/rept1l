################################################################################
#                                recola_base.py                                #
################################################################################

""" Mapping to the recola current base. """

from future.utils import iteritems
from rept1l.baseutils import CurrentBase, insert_ls
from rept1l.vertex import Vertex
from rept1l.combinatorics import gen_permutations
from collections import OrderedDict as OD


def register_permutation(structures, permutation, vid, recola_base,
                         vid_str=None, defaultp=False):
  """ Register a given structure as one of the basis structures. The code will
  try to use this structure whenever possible.

  :structures: list of lorentz structures
  :permutation: permutation as integer list
  :vid: structure id
  :recola_base: dictionary of all structure ids and associated structures.
  :vid_str: string representation of vid (optional)
  :defaultp: declare permutation as default permutation (optional)
  """
  ids = []
  lco = []

  # from rept1l.baseutils.base_utils import lbs_to_form_expressions
  for ls in structures:
    if defaultp:
      lbs = CurrentBase(ls).compute(permutation, is_defaultp=True)
    else:
      lbs = CurrentBase(ls).compute(permutation)
    lbs = CurrentBase.form_simplify_lbs(lbs)
    lco += [[tuple(u) for u in lbs]]
    # ssum = lbs_to_form_expressions(lbs)
    # print("ssum:", ssum)

  # check for canonical order of structures
  check = [u[0] for u in lco]
  check_p1 = [u[0] for u in check]
  check_p2 = CurrentBase.order_base_structure([u[0] for u in lco])
  check_p2 = [u[0] for u in check_p2]
  if check_p1 != check_p2:
    print("canonical order violated.")
    print("check_p1:", check_p1)
    print("check_p2:", check_p2)
    print("permutation:", permutation)
    print("structures:", structures)
    print("vid:", vid)
    import sys
    sys.exit()

  (Vertex.current_strucs, vbase, _,
   Vertex.n_lorentz) = insert_ls(Vertex.current_strucs,
                                 lco, Vertex.n_lorentz,
                                 simple_structures=True)
  ids = tuple(u[1] for u in vbase)
  recola_base[ids] = {'integer_id': vid, 'string_id': vid_str}
  return recola_base


def gamma_3_base(recola_base):
  """ gamma_mu(l,k)*( cop(1)*omega_+(k,i) + cop(2)*omega_-(k,i) )"""
  GP = 'GammaP(3,2,1)'
  GM = 'GammaM(3,2,1)'
  perms = {(0, 1, 2): {'+-': '15', '-': '17', '+': '19'},
           (0, 2, 1): {'+-': '31', '-': '35', '+': '37'},
           (1, 0, 2): {'+-': '16', '-': '18', '+': '20'},
           (2, 0, 1): {'+-': '51', '-': '57', '+': '55'},
           (1, 2, 0): {'+-': '33', '-': '36', '+': '38'},
           (2, 1, 0): {'+-': '53', '-': '58', '+': '56'}
           }

  for permutation, elem in iteritems(perms):
    vid = elem['+']
    recola_base = register_permutation([GP], permutation, vid, recola_base)
    vid = elem['-']
    recola_base = register_permutation([GM], permutation, vid, recola_base)
    vid = elem['+-']
    recola_base = register_permutation([GP, GM], permutation, vid, recola_base)

  return recola_base


def w_3_base(recola_base):
  """ ( cop(1)*omega_+(l,i) + cop(2)*omega_-(l,i) )"""
  PP = 'ProjP(2,1)'
  PM = 'ProjM(2,1)'
  perms = {(0, 1, 2): {'+-': '5', '+': '9', '-': '7'},    # Proj(2,1), checked
           (1, 0, 2): {'+-': '6', '+': '10', '-': '8'},   # Proj(1,2), checked
           (0, 2, 1): {'+-': '21', '+': '27', '-': '25'}, # Proj(3,1), checked
           (1, 2, 0): {'+-': '23', '+': '28', '-': '26'}, # Proj(3,2), checked
           (2, 0, 1): {'+-': '41', '+': '47', '-': '45'}, # Proj(1,3), checked
           (2, 1, 0): {'+-': '43', '+': '48', '-': '46'}, # Proj(2,3), checked
           }
  for permutation, elem in iteritems(perms):
    vid = elem['+']
    recola_base = register_permutation([PP], permutation, vid, recola_base)
    vid = elem['-']
    recola_base = register_permutation([PM], permutation, vid, recola_base)
    vid = elem['+-']
    recola_base = register_permutation([PP, PM], permutation, vid, recola_base)

  return recola_base


def g_g_4_base(recola_base):
  """
  + cop(1)*g_{mu,al}*g_{nu,ro}
  + cop(2)*g_{mu,ro}*g_{nu,al}
  + cop(3)*g_{mu,nu}*g_{ro,al}
  """
  lorentz = 'Metric(1,4)*Metric(2,3)'
  lbs = CurrentBase(lorentz).compute(range(4), is_defaultp=True)
  lorentz = 'Metric(1,3)*Metric(2,4)'
  lbs += CurrentBase(lorentz).compute(range(4), is_defaultp=True)
  lorentz = 'Metric(1,2)*Metric(3,4)'
  lbs += CurrentBase(lorentz).compute(range(4), is_defaultp=True)

  lco = [[tuple(u)] for u in lbs]
  (Vertex.current_strucs, t, a,
   Vertex.n_lorentz) = insert_ls(Vertex.current_strucs, lco, Vertex.n_lorentz,
                                 simple_structures=True)
  nlorentz = Vertex.n_lorentz
  recola_base[(nlorentz,)] = '-11'

  # BFM additional
  lorentz = 'Metric(1,4)*Metric(2,3)'
  lbs = CurrentBase(lorentz).compute(range(4))

  lco = [[tuple(u)] for u in lbs]
  (Vertex.current_strucs, t, a,
   Vertex.n_lorentz) = insert_ls(Vertex.current_strucs, lco, Vertex.n_lorentz,
                                 simple_structures=True)
  nlorentz = Vertex.n_lorentz
  recola_base[(nlorentz,)] = '-111'

  return recola_base


def g_3_base(recola_base):
  """ cop(1)*g_{mu,nu} """
  lorentz = 'Metric(1,2)'
  lbs = CurrentBase(lorentz).compute(range(3))
  lco = [[tuple(u)] for u in lbs]
  (Vertex.current_strucs, _, _,
   Vertex.n_lorentz) = insert_ls(Vertex.current_strucs, lco, Vertex.n_lorentz,
                                 simple_structures=True)
  nlorentz = Vertex.n_lorentz
  recola_base[(nlorentz,)] = '2'

  lorentz = 'Metric(1,3)'
  lbs = CurrentBase(lorentz).compute(range(3))
  lco = [[tuple(u)] for u in lbs]
  (Vertex.current_strucs, _, _,
   Vertex.n_lorentz) = insert_ls(Vertex.current_strucs, lco, Vertex.n_lorentz,
                                 simple_structures=True)
  nlorentz = Vertex.n_lorentz
  recola_base[(nlorentz,)] = '14'

  lorentz = 'Metric(2,3)'
  lbs = CurrentBase(lorentz).compute(range(3))
  lco = [[tuple(u)] for u in lbs]
  (Vertex.current_strucs, _, _,
   Vertex.n_lorentz) = insert_ls(Vertex.current_strucs, lco, Vertex.n_lorentz,
                                 simple_structures=True)
  nlorentz = Vertex.n_lorentz
  recola_base[(nlorentz,)] = '13'
  return recola_base


def g_4_base(recola_base):
  """ cop(1)*g_{mu,nu} """
  lorentz = 'Metric(1,2)'
  lbs = CurrentBase(lorentz).compute(range(4))
  lco = [[tuple(u)] for u in lbs]
  (Vertex.current_strucs, _, _,
   Vertex.n_lorentz) = insert_ls(Vertex.current_strucs, lco, Vertex.n_lorentz,
                                 simple_structures=True)
  nlorentz = Vertex.n_lorentz
  recola_base[(nlorentz,)] = '-4'

  lorentz = 'Metric(1,3)'
  lbs = CurrentBase(lorentz).compute(range(4))
  lco = [[tuple(u)] for u in lbs]
  (Vertex.current_strucs, _, _,
   Vertex.n_lorentz) = insert_ls(Vertex.current_strucs, lco, Vertex.n_lorentz,
                                 simple_structures=True)
  nlorentz = Vertex.n_lorentz
  recola_base[(nlorentz,)] = '-3'

  lorentz = 'Metric(1,4)'
  lbs = CurrentBase(lorentz).compute(range(4))
  lco = [[tuple(u)] for u in lbs]
  (Vertex.current_strucs, _, _,
   Vertex.n_lorentz) = insert_ls(Vertex.current_strucs, lco, Vertex.n_lorentz,
                                 simple_structures=True)
  nlorentz = Vertex.n_lorentz
  recola_base[(nlorentz,)] = '-13'

  lorentz = 'Metric(2,3)'
  lbs = CurrentBase(lorentz).compute(range(4))
  lco = [[tuple(u)] for u in lbs]
  (Vertex.current_strucs, _, _,
   Vertex.n_lorentz) = insert_ls(Vertex.current_strucs, lco, Vertex.n_lorentz,
                                 simple_structures=True)
  nlorentz = Vertex.n_lorentz
  recola_base[(nlorentz,)] = '-2'

  lorentz = 'Metric(2,4)'
  lbs = CurrentBase(lorentz).compute(range(4))
  lco = [[tuple(u)] for u in lbs]
  (Vertex.current_strucs, _, _,
   Vertex.n_lorentz) = insert_ls(Vertex.current_strucs, lco, Vertex.n_lorentz,
                                 simple_structures=True)
  nlorentz = Vertex.n_lorentz
  recola_base[(nlorentz,)] = '-14'

  lorentz = 'Metric(3,4)'
  lbs = CurrentBase(lorentz).compute(range(4))
  lco = [[tuple(u)] for u in lbs]
  (Vertex.current_strucs, _, _,
   Vertex.n_lorentz) = insert_ls(Vertex.current_strucs, lco, Vertex.n_lorentz,
                                 simple_structures=True)
  nlorentz = Vertex.n_lorentz
  recola_base[(nlorentz,)] = '-12'

  return recola_base


def g_p_base_eft(recola_base):
  lorentz1 = 'Metric(1,2)'
  lorentz2 = 'P(1,2)*P(2,1)'
  lorentz3 = 'P(-1,1)*P(-1,2)*Metric(1,2)'

  #  Add to recola EFT basis:  <22-02-18, Jean-Nicolas Lang> #
  permutations = {(0, 1, 2): '177',
                  (0, 2, 1): '178',
                  (1, 2, 0): '179',
                  (1, 3, 0, 2): '2000',
                  (2, 3, 0, 1): '2001',
                  (1, 2, 0, 3): '2002',
                  (0, 1, 2, 3): '2003',
                  (0, 3, 1, 2): '2004',
                  (0, 2, 1, 3): '2005'
                  }

  for permutation, vid in iteritems(permutations):
    lco = []
    for ls in [lorentz1, lorentz2, lorentz3]:
      lbs = CurrentBase(ls).compute(permutation)
      lco += [[tuple(u) for u in lbs]]
    (Vertex.current_strucs, vbase, _,
     Vertex.n_lorentz) = insert_ls(Vertex.current_strucs,
                                   lco, Vertex.n_lorentz,
                                   simple_structures=True)
    ids = tuple(u[1] for u in vbase)
    recola_base[ids] = vid

  permutations = {(0, 1, 2): '174',
                  (0, 2, 1): '175',
                  (1, 2, 0): '176',
                  (1, 3, 0, 2): '2005',
                  (2, 3, 0, 1): '2006',
                  (1, 2, 0, 3): '2007',
                  (0, 1, 2, 3): '2008',
                  (0, 3, 1, 2): '2009',
                  (0, 2, 1, 3): '2010'
                  }

  for permutation, vid in iteritems(permutations):
    lco = []
    for ls in [lorentz2, lorentz3]:
      lbs = CurrentBase(ls).compute(permutation)
      lco += [[tuple(u) for u in lbs]]
    (Vertex.current_strucs, vbase, _,
     Vertex.n_lorentz) = insert_ls(Vertex.current_strucs,
                                   lco, Vertex.n_lorentz,
                                   simple_structures=True)
    ids = tuple(u[1] for u in vbase)
    recola_base[ids] = vid

  return recola_base


def p_g_3_base(recola_base):
  """
  cop(1)*(
  + g_{mu,nu}*(p2-p1)_si
  + g_{nu,si}*(p3-p2)_mu
  + g_{si,mu}*(p1-p3)_nu
  )
  """
  lorentz = ('P(3,1)*Metric(1,2) - P(3,2)*Metric(1,2) - P(2,1)*Metric(1,3) + ' +
             'P(2,3)*Metric(1,3) + P(1,2)*Metric(2,3) - P(1,3)*Metric(2,3)')
  lbs = CurrentBase(lorentz).compute(range(3))
  lco = [[tuple(u) for u in lbs]]
  (Vertex.current_strucs, _, _,
   Vertex.n_lorentz) = insert_ls(Vertex.current_strucs, lco, Vertex.n_lorentz,
                                 simple_structures=True)
  nlorentz = Vertex.n_lorentz
  recola_base[(nlorentz,)] = '12'

  return recola_base


def p_g_3_base_heft(recola_base):

  lorentz = ('P(3,1)*Metric(1,2) - P(3,2)*Metric(1,2) - P(2,1)*Metric(1,3) + ' +
             'P(2,3)*Metric(1,3) + P(1,2)*Metric(2,3) - P(1,3)*Metric(2,3)')

  p4_perms = OD([((0, 1, 2, 3), (140, 'VVVS_heft', True)),    # VVVS
                 ((0, 1, 3, 2), (141, 'VVSV_heft', True)),    # VVSV
                 ((0, 2, 3, 1), (142, 'VSVV_heft', False)),   # VSVV
                 ((1, 2, 3, 0), (143, 'SVVV_heft', True)),    # SVVV
                 ]
                )

  p5_perms = OD([((0, 1, 2, 3, 4), (150, 'VVVSS_heft', True)),   # VVVSS
                 ((0, 1, 3, 2, 4), (151, 'VVSVS_heft', False)),  # VVSVS
                 ((0, 2, 3, 1, 4), (152, 'VSVVS_heft', False)),  # VSVVS
                 ((0, 1, 4, 2, 3), (153, 'VVSSV_heft', True)),   # VVSSV
                 ((0, 2, 4, 1, 3), (154, 'VSVSV_heft', False)),  # VSVSV
                 ((0, 3, 4, 1, 2), (155, 'VSSVV_heft', False)),  # VSSVV
                 # (0, 2, 3, 1, 4) => pos 4 -> 2, pos 2 -> 3, pos 3 -> 4 ...
                 ((1, 2, 3, 0, 4), (156, 'SVVVS_heft', True)),   # SVVVS
                 ((1, 2, 4, 0, 3), (157, 'SVVSV_heft', True)),   # SVVSV
                 ((1, 3, 4, 0, 2), (158, 'SVSVV_heft', False)),  # SVSVV
                 ((2, 3, 4, 0, 1), (159, 'SSVVV_heft', False)),  # SSVVV
                 ]
                )

  for permutation, elem in iteritems(p4_perms):
    vid, vid_str, defaultp = elem
    recola_base = register_permutation([lorentz], permutation, vid,
                                       recola_base, vid_str=vid_str,
                                       defaultp=defaultp)

  for permutation, elem in iteritems(p5_perms):
    vid, vid_str, defaultp = elem
    recola_base = register_permutation([lorentz], permutation, vid,
                                       recola_base, vid_str=vid_str,
                                       defaultp=defaultp)

  return recola_base


def vvvv_base_heft(recola_base):
  """ Extends the SM basis VVVV
  + cop(1)*g_{mu,al}*g_{nu,ro}
  + cop(2)*g_{mu,ro}*g_{nu,al}
  + cop(3)*g_{mu,nu}*g_{ro,al}

  to additional scalar fields.
  """
  lorentz1 = 'Metric(1,4)*Metric(2,3)'
  lorentz2 = 'Metric(1,3)*Metric(2,4)'
  lorentz3 = 'Metric(1,2)*Metric(3,4)'

  lorentz = [lorentz1, lorentz2, lorentz3]

  p5_perms = OD([((0, 1, 2, 3, 4), (110, 'VVVVS_heft', True)),
                 ((0, 1, 2, 4, 3), (111, 'VVVSV_heft', True)),
                 ((0, 1, 3, 4, 2), (112, 'VVSVV_heft', False)),
                 ((0, 2, 3, 4, 1), (113, 'VSVVV_heft', False)),
                 ((1, 2, 3, 4, 0), (114, 'SVVVV_heft', True)),
                 ]
                )

  p6_perms = OD([((0, 1, 2, 3, 4, 5), (120, 'VVVVSS_heft', True)),
                 ((0, 1, 2, 4, 3, 5), (121, 'VVVSVS_heft', False)),
                 ((0, 1, 2, 5, 3, 4), (122, 'VVVSSV_heft', True)),
                 ((0, 1, 3, 4, 2, 5), (123, 'VVSVVS_heft', False)),
                 ((0, 1, 3, 5, 2, 4), (124, 'VVSVSV_heft', False)),
                 ((0, 1, 4, 5, 2, 3), (125, 'VVSSVV_heft', False)),
                 ((0, 2, 3, 4, 1, 5), (126, 'VSVVVS_heft', False)),
                 ((0, 2, 3, 5, 1, 4), (127, 'VSVVSV_heft', False)),
                 ((0, 2, 4, 5, 1, 3), (128, 'VSVSVV_heft', False)),
                 ((0, 3, 4, 5, 1, 2), (129, 'VSSVVV_heft', False)),
                 ((1, 2, 3, 4, 0, 5), (130, 'SVVVVS_heft', True)),
                 ((1, 2, 3, 5, 0, 4), (131, 'SVVVSV_heft', True)),
                 ((1, 2, 4, 5, 0, 3), (132, 'SVVSVV_heft', False)),
                 ((1, 3, 4, 5, 0, 2), (133, 'SVSVVV_heft', False)),
                 ((2, 3, 4, 5, 0, 1), (134, 'SSVVVV_heft', False)),
                 ]
                )

  for permutation, elem in iteritems(p5_perms):
    vid, vid_str, defaultp = elem
    recola_base = register_permutation(lorentz, permutation, vid, recola_base,
                                       vid_str=vid_str, defaultp=defaultp)
  for permutation, elem in iteritems(p6_perms):
    vid, vid_str, defaultp = elem
    recola_base = register_permutation(lorentz, permutation, vid, recola_base,
                                       vid_str=vid_str, defaultp=defaultp)

  return recola_base


def vvs_base_heft(recola_base):

  lorentz1 = 'P(1,2)*P(2,1)'
  lorentz2 = 'P(-1,1)*P(-1,2)*Metric(1,2)'

  lorentz = [lorentz1, lorentz2]

  p3_perms = OD([((0, 1, 2), (97, 'VVS_heft', True)),
                 ((0, 2, 1), (98, 'VSV_heft', True)),
                 ((1, 2, 0), (99, 'SVV_heft', True)),
                 ]
                )

  for permutation, elem in iteritems(p3_perms):
    vid, vid_str, defaultp = elem
    recola_base = register_permutation(lorentz, permutation, vid, recola_base,
                                       vid_str=vid_str, defaultp=defaultp)

  return recola_base


def vvss_base_heft(recola_base):

  lorentz1 = 'P(1,2)*P(2,1)'
  lorentz2 = 'P(-1,1)*P(-1,2)*Metric(1,2)'

  lorentz = [lorentz1, lorentz2]
  p4_perms = OD([((0, 1, 2, 3), (100, 'VVSS_heft', True)),
                 ((0, 2, 1, 3), (101, 'VSVS_heft', False)),
                 ((0, 3, 1, 2), (102, 'VSSV_heft', True)),
                 ((1, 2, 0, 3), (103, 'SVVS_heft', True)),
                 ((1, 3, 0, 2), (104, 'SVSV_heft', True)),
                 ((2, 3, 0, 1), (105, 'SSVV_heft', False)),
                 ]
                )

  for permutation, elem in iteritems(p4_perms):
    vid, vid_str, defaultp = elem
    recola_base = register_permutation(lorentz, permutation, vid, recola_base,
                                       vid_str=vid_str, defaultp=defaultp)

  return recola_base


def p_g_3_bfm_base(recola_base):
  """
  cop(1)*(
    + g_{mu,nu}*(p2-p1+p3)_si
    + g_{nu,si}*(p3-p2)_mu
    + g_{si,mu}*(p1-p3-p2)_nu
    )
  """
  lorentz = ('P(3,1)*Metric(1,2) - P(3,2)*Metric(1,2) + P(3,3)*Metric(1,2) - ' +
             'P(2,1)*Metric(1,3) + P(2,3)*Metric(1,3) - P(1,1)*Metric(2,3) + ' +
             'P(1,2)*Metric(2,3) - P(1,3)*Metric(2,3)')

  permutation = (0, 1, 2)
  vid = 160
  vid_str = 'VVV_bfm'
  defaultp = True
  recola_base = register_permutation([lorentz], permutation, vid, recola_base,
                                     vid_str=vid_str, defaultp=defaultp)

  return recola_base


def VVV_base_eft(recola_base):
  """
  cop(1)*(
  + g_{mu,nu}*(p2-p1)_si
  )
  +
  cop(2)*(
  + g_{nu,si}*(p3-p2)_mu
  )
  +
  cop(3)*(
  + g_{si,mu}*(p1-p3)_nu
  )
  """
  lco = []
  lorentz1 = 'P(3,1)*Metric(1,2) - P(2,1)*Metric(1,3)'
  lorentz2 = 'P(2,3)*Metric(1,3) - P(1,3)*Metric(2,3)'
  lorentz3 = 'P(1,2)*Metric(2,3) - P(3,2)*Metric(1,2)'

  lorentz4 = '-P(1,2)*P(2,3)*P(3,1) + P(1,3)*P(2,1)*P(3,2)'
  lorentz5 = ('+ P(-1,2)*P(-1,3)*P(3,1)*Metric(1,2)' +
             '- P(-1,1)*P(-1,3)*P(3,2)*Metric(1,2)' +
             '- P(-1,2)*P(-1,3)*P(2,1)*Metric(1,3)' +
             '+ P(-1,1)*P(-1,2)*P(2,3)*Metric(1,3)' +
             '+ P(-1,1)*P(-1,3)*P(1,2)*Metric(2,3)' +
             '- P(-1,1)*P(-1,2)*P(1,3)*Metric(2,3)')

  # CBtWL4
  lorentz6 = ('P(-1,1)*P(-2,2)*P(2,3)*Epsilon(-1,-2,1,3)'
              ' + P(-1,1)*P(-2,3)*P(3,2)*Epsilon(-1,-2,1,2)')
  lorentz7 = ('P(-1,1)*P(-2,3)*P(1,2)*Epsilon(-1,-2,2,3)'
              ' + P(-1,2)*P(-2,3)*P(2,1)*Epsilon(-1,-2,1,3)')
  lorentz8 = ('P(-1,1)*P(-2,2)*P(1,3)*Epsilon(-1,-2,2,3)'
              ' + P(-1,2)*P(-2,3)*P(3,1)*Epsilon(-1,-2,1,2)')
  # CPWWWL2
  lorentz9 = ('P(-1,1)*P(-2,2)*P(-3,3)*Metric(1,2)*Epsilon(-1,-2,-3,3)' +
              '+ P(-1,1)*P(-2,2)*P(-3,3)*Metric(1,3)*Epsilon(-1,-2,-3,2)' +
              '+ P(-1,1)*P(-2,2)*P(-3,3)*Metric(2,3)*Epsilon(-1,-2,-3,1)')

  lorentz10 = ('-P(-1,1)*P(-2,2)*P(2,3)*Epsilon(-1,-2,1,3)' +
               '+P(-1,1)*P(-2,3)*P(3,2)*Epsilon(-1,-2,1,2)' +
               '-P(-1,1)*P(-2,3)*P(1,2)*Epsilon(-1,-2,2,3)' +
               '+P(-1,2)*P(-2,3)*P(2,1)*Epsilon(-1,-2,1,3)' +
               '-P(-1,1)*P(-2,2)*P(1,3)*Epsilon(-1,-2,2,3)' +
               '+P(-1,2)*P(-2,3)*P(3,1)*Epsilon(-1,-2,1,2)' +
               '-P(-1,1)*P(-1,2)*P(-2,3)*Epsilon(-2,1,2,3)' +
               '-P(-1,1)*P(-2,2)*P(-2,3)*Epsilon(-1,1,2,3)' +
               '-P(-1,1)*P(-2,2)*P(-1,3)*Epsilon(-2,1,2,3)')
  # lorentz6 = 'Epsilon(1,2,3,-1)*P(-1,1)'

  # d

  # representatives = {(0, 1, 2, 3): '771',
  #                    (3, 0, 1, 2): '772'}

  # permutations = [u for u in gen_permutations(range(5))]
  # permutations = [[3, 0, 1, 2]]
  # permutations = {(0, 1, 2, 3): '771',
  #                 (0, 1, 3, 2): '772',
  #                 (0, 2, 3, 1): '773',
  #                 (1, 2, 3, 0): '774'
  #                 }
  #  TODO:  <23-02-18, Jean-Nicolas Lang> #
  permutations = {
   (0, 1, 2): 'VVV_012',
   (0, 2, 1): 'VVV_012',
   (0, 1, 2, 3): 'VVVV_0123',
   (0, 1, 3, 2): 'VVVV_0132',
   (0, 2, 3, 1): 'VVVV_0231',
   (1, 2, 3, 0): 'VVVV_1230',
   (0, 1, 3, 2, 4): '?',
   (0, 1, 2, 3, 4): '?',
   (0, 1, 4, 2, 3): '?',
   (0, 2, 3, 1, 4): '?',
   (0, 2, 4, 1, 3): '?',
   (0, 3, 4, 1, 2): '?',
   (1, 2, 3, 0, 4): '?',
   (1, 2, 4, 0, 3): '?',
   (1, 3, 4, 0, 2): '?',
   (2, 3, 4, 0, 1): '?'
   }
  for permutation, vid in iteritems(permutations):
    ids = []
    lco = []
    if len(permutation) == 3:
      consider_ls = [lorentz1, lorentz2, lorentz3, lorentz4, lorentz5,
                     lorentz6, lorentz7, lorentz8, lorentz9, lorentz10]
      # consider_ls = [lorentz9, lorentz10]
      consider_ls = [lorentz6, lorentz7, lorentz8, lorentz9, lorentz10]
    else:
      consider_ls = [lorentz1, lorentz2, lorentz3]
    for ls in consider_ls:
      lbs = CurrentBase(ls).compute(permutation)
      lbs = CurrentBase.form_simplify_lbs(lbs)
      lco += [[tuple(u) for u in lbs]]
    (Vertex.current_strucs, vbase, _,
     Vertex.n_lorentz) = insert_ls(Vertex.current_strucs,
                                   lco, Vertex.n_lorentz,
                                   simple_structures=True)
    ids = tuple(u[1] for u in vbase)

    print("ids:", ids)
    recola_base[ids] = vid
  return recola_base

def ZZZ_d8_eft(recola_base):
  LP3 = ['P(1,3)*P(2,1)*P(3,2) + P(1,2)*P(2,3)*P(3,1)']

  # CBtWL4
  LP3E = ['+ P(-1,1)*P(-2,2)*P(2,3)*Epsilon(-1,-2,1,3)' +
          '+ P(-1,1)*P(-2,3)*P(3,2)*Epsilon(-1,-2,1,2)' +
          '+ P(-1,1)*P(-2,3)*P(1,2)*Epsilon(-1,-2,2,3)' +
          '+ P(-1,2)*P(-2,3)*P(2,1)*Epsilon(-1,-2,1,3)' +
          '- P(-1,1)*P(-2,2)*P(1,3)*Epsilon(-1,-2,2,3)' +
          '- P(-1,2)*P(-2,3)*P(3,1)*Epsilon(-1,-2,1,2)']

  LP3M = [('+ P(-1,2)*P(-1,3)*P(3,1)*Metric(1,2)' +
           '+ P(-1,1)*P(-1,3)*P(3,2)*Metric(1,2)' +
           '+ P(-1,2)*P(-1,3)*P(2,1)*Metric(1,3)' +
           '+ P(-1,1)*P(-1,2)*P(2,3)*Metric(1,3)' +
           '+ P(-1,1)*P(-1,3)*P(1,2)*Metric(2,3)' +
           '+ P(-1,1)*P(-1,2)*P(1,3)*Metric(2,3)')]



  # LP3E2 = [('P(-1,1)*P(-1,2)*P(-2,3)*Epsilon(-2,1,2,3)' +
  #           ' - P(-1,1)*P(-2,2)*P(-2,3)*Epsilon(-1,1,2,3)' +
  #           ' - P(-1,1)*P(-2,2)*P(-1,3)*Epsilon(-2,1,2,3)')]

  permutations = {
   (0, 1, 2): 'ZZZ',
   }
  for permutation, vid in iteritems(permutations):
    ids = []
    lco = []
    consider_ls = LP3 + LP3M + LP3E

    for ls in consider_ls:
      lbs = CurrentBase(ls).compute(permutation)
      lbs = CurrentBase.form_simplify_lbs(lbs)
      lco += [[tuple(u) for u in lbs]]
    (Vertex.current_strucs, vbase, _,
     Vertex.n_lorentz) = insert_ls(Vertex.current_strucs,
                                   lco, Vertex.n_lorentz,
                                   simple_structures=True)
    ids = tuple(u[1] for u in vbase)

    print("ids:", ids)
    recola_base[ids] = vid + '_CBtWL4'

  return recola_base


def vvv_d6_eft(recola_base):

  PM_SM = ['P(3,1)*Metric(1,2) - P(2,1)*Metric(1,3)',
           'P(2,3)*Metric(1,3) - P(1,3)*Metric(2,3)',
           'P(1,2)*Metric(2,3) - P(3,2)*Metric(1,2)']

  # P^3 g Eps must come first due to higher priority  (see order_base_structures)
  P3ME = ['P(-1,1)*P(-2,2)*P(-3,3)*Metric(1,2)*Epsilon(-1,-2,-3,3)' +
          '+ P(-1,1)*P(-2,2)*P(-3,3)*Metric(1,3)*Epsilon(-1,-2,-3,2)' +
          '+ P(-1,1)*P(-2,2)*P(-3,3)*Metric(2,3)*Epsilon(-1,-2,-3,1)']

  P2E2 = ['+ P(-1,1)*P(-2,2)*P(2,3)*Epsilon(-1,-2,1,3)',
          '+ P(-1,1)*P(-2,3)*P(1,2)*Epsilon(-1,-2,2,3)',
          '+ P(-1,2)*P(-2,3)*P(3,1)*Epsilon(-1,-2,1,2)',
          '+ P(-1,1)*P(-2,3)*P(3,2)*Epsilon(-1,-2,1,2)',
          '+ P(-1,2)*P(-2,3)*P(2,1)*Epsilon(-1,-2,1,3)',
          '+ P(-1,1)*P(-2,2)*P(1,3)*Epsilon(-1,-2,2,3)'
          ]

  # P2E2 = ['+ P(-1,1)*P(-2,2)*P(2,3)*Epsilon(-1,-2,1,3)' +
  #         '+ P(-1,1)*P(-2,3)*P(1,2)*Epsilon(-1,-2,2,3)' +
  #         '- P(-1,2)*P(-2,3)*P(3,1)*Epsilon(-1,-2,1,2)',
  #         '- P(-1,1)*P(-2,3)*P(3,2)*Epsilon(-1,-2,1,2)' +
  #         '- P(-1,2)*P(-2,3)*P(2,1)*Epsilon(-1,-2,1,3)' +
  #         '+ P(-1,1)*P(-2,2)*P(1,3)*Epsilon(-1,-2,2,3)'
  #         ]

  # doesn't work ...
  # P2E2_2 = [' - P(-1,1)*P(-2,2)*P(1,3)*Epsilon(-1,-2,2,3)' +
  #         ' + P(-1,1)*P(-2,3)*P(1,2)*Epsilon(-1,-2,2,3)',
  #         ' - P(-1,2)*P(-2,3)*P(3,1)*Epsilon(-1,-2,1,2)' +
  #         ' + P(-1,1)*P(-2,3)*P(3,2)*Epsilon(-1,-2,1,2)',
  #         ' + P(-1,2)*P(-2,3)*P(2,1)*Epsilon(-1,-2,1,3)' +
  #         ' + P(-1,1)*P(-2,2)*P(2,3)*Epsilon(-1,-2,1,3)']

  P1E1_1 = ['Epsilon(1,2,3,-1)*P(-1,1)', ]
  P1E1_2 = ['Epsilon(1,2,3,-1)*P(-1,2)', ]
  P1E1_3 = ['Epsilon(1,2,3,-1)*P(-1,3)', ]


  # LP3M = [('+ P(-1,2)*P(-1,3)*P(3,1)*Metric(1,2)' +
  #          '+ P(-1,1)*P(-1,3)*P(3,2)*Metric(1,2)' +
  #          '+ P(-1,2)*P(-1,3)*P(2,1)*Metric(1,3)' +
  #          '+ P(-1,1)*P(-1,2)*P(2,3)*Metric(1,3)' +
  #          '+ P(-1,1)*P(-1,3)*P(1,2)*Metric(2,3)' +
  #          '+ P(-1,1)*P(-1,2)*P(1,3)*Metric(2,3)')]
  LP3M_P = ['+ P(-1,2)*P(-1,3)*P(3,1)*Metric(1,2)' +
            '+ P(-1,1)*P(-1,2)*P(1,3)*Metric(2,3)',
            '+ P(-1,1)*P(-1,3)*P(3,2)*Metric(1,2)' +
            '+ P(-1,1)*P(-1,2)*P(2,3)*Metric(1,3)',
            '+ P(-1,2)*P(-1,3)*P(2,1)*Metric(1,3)' +
            '+ P(-1,1)*P(-1,3)*P(1,2)*Metric(2,3)'
            ]
  LP3M_M = ['+ P(-1,2)*P(-1,3)*P(3,1)*Metric(1,2)' +
            '- P(-1,1)*P(-1,2)*P(1,3)*Metric(2,3)',
            '+ P(-1,1)*P(-1,3)*P(3,2)*Metric(1,2)' +
            '- P(-1,1)*P(-1,2)*P(2,3)*Metric(1,3)',
            '+ P(-1,2)*P(-1,3)*P(2,1)*Metric(1,3)' +
            '- P(-1,1)*P(-1,3)*P(1,2)*Metric(2,3)'
            ]
  LP3M = ['+ P(-1,2)*P(-1,3)*P(3,1)*Metric(1,2)',
          '+ P(-1,1)*P(-1,2)*P(1,3)*Metric(2,3)',
          '+ P(-1,1)*P(-1,3)*P(3,2)*Metric(1,2)',
          '+ P(-1,1)*P(-1,2)*P(2,3)*Metric(1,3)',
          '+ P(-1,2)*P(-1,3)*P(2,1)*Metric(1,3)',
          '+ P(-1,1)*P(-1,3)*P(1,2)*Metric(2,3)'
          ]
  # LP3M_P = LP3M
  # LP3M_M = LP3M






  # CBtWL4 = [L1, L2, L3, L4, L5, L6]

  # CPWWWL2

  P3E1 = ['P(-1,1)*P(-1,2)*P(-2,3)*Epsilon(-2,1,2,3)' +
          '+ P(-1,1)*P(-2,2)*P(-2,3)*Epsilon(-1,1,2,3)' +
          '+ P(-1,1)*P(-2,2)*P(-1,3)*Epsilon(-2,1,2,3)']

  P3 = ['P(1,3)*P(2,1)*P(3,2)', 'P(1,2)*P(2,3)*P(3,1)']
  # CPWWWL2 = [lorentz9, lorentz10]

  # d

  # representatives = {(0, 1, 2, 3): '771',
  #                    (3, 0, 1, 2): '772'}

  # permutations = [u for u in gen_permutations(range(5))]
  # permutations = [[3, 0, 1, 2]]
  # permutations = {(0, 1, 2, 3): '771',
  #                 (0, 1, 3, 2): '772',
  #                 (1, 2, 3, 0): '774'
  #                 (0, 2, 3, 1): '773',
  #                 }
  #  TODO:  <23-02-18, Jean-Nicolas Lang> #
  permutations = {
   (0, 1, 2): 'VVV_EFT_012',
   (0, 2, 1): 'VVV_EFT_021',
   }
  for permutation, vid in iteritems(permutations):
    # pure EPSILON strucs
    consider_ls = PM_SM
    vid_mod = vid + '_PM_SM'
    recola_base = register_permutation(consider_ls, permutation, vid_mod, recola_base)
    consider_ls = P2E2 + P3E1
    vid_mod = vid + '_P2P3'
    recola_base = register_permutation(consider_ls, permutation, vid_mod, recola_base)
    # pure EPSILON strucs + P3 + LP3M
    consider_ls = P3 + LP3M + P2E2 + P3E1
    vid_mod = vid + '_P2P3_P3_LP3M'
    recola_base = register_permutation(consider_ls, permutation, vid_mod, recola_base)
    # consider_ls = P3 + LP3M_M + P2E2 + P3E1
    # vid_mod = vid + '_P2P3_P3_LP3M_M'
    # recola_base = register_permutation(consider_ls, permutation, vid_mod, recola_base)
    for pos, P1E1 in enumerate([P1E1_1, P1E1_2, P1E1_3]):
      consider_ls = P1E1
      recola_base = register_permutation(consider_ls, permutation, vid +
          '_P1E1_only_' + str(pos), recola_base)
      # pure EPSILON strucs + P1E1
      consider_ls = P3ME + P2E2 + P3E1 + P1E1
      recola_base = register_permutation(consider_ls, permutation, vid +
          '_P1E1_' + str(pos), recola_base)
      consider_ls = P3 + LP3M + P3ME + P2E2 + P3E1 + P1E1
      vid_mod = vid + '_P3_LP3M_P3ME_P2E2_P3E1_P1E1_' + str(pos)
      recola_base = register_permutation(consider_ls, permutation, vid_mod, recola_base)
      # consider_ls = P3 + LP3M_M + P3ME + P2E2 + P3E1 + P1E1
      # vid_mod = vid + '_P3_LP3M_M_P3ME_P2E2_P3E1_P1E1_' + str(pos)
      # recola_base = register_permutation(consider_ls, permutation, vid_mod, recola_base)
    # for pos, P1E1 in enumerate([P1E1_1, P1E1_2, P1E1_2]):
    #   # pure EPSILON strucs + P1E1
    #   v1_modjj
    #   consider_ls = P2E2 + P3E1 + P1E1
    #   recola_base = register_permutation(consider_ls, permutation, vid +
    #       '_P1E1_' + str(pos), recola_base)
      # EPSILON + ?  strucs
      # consider_ls = LP3M + P3ME + P2E2 + P3E1 + P1E1
      # recola_base = register_permutation(consider_ls, permutation, vid +
      #     '_P1E1_' + str(pos) + '_LP3M', recola_base)
  return recola_base

def fourferm_base_eft(recola_base):
  """
  """
  # nu~ l- q~ q' (nu_e~ mu- b~ t)
  all_ls = [
  'GammaP(-1,1,-2)*GammaP(-1,3,-3)*GammaM(-4,-2,2)*GammaM(-4,-3,4)',
  'GammaP(-1,1,-2)*GammaP(-3,3,-4)*GammaM(-3,-2,2)*GammaM(-1,-4,4)',
  'GammaM(-1,1,2)*GammaM(-1,3,4)',
  'ProjP(1,2)*ProjM(3,4)',
  'ProjM(1,2)*ProjM(3,4)'
  ]
  permutations = {
  (0, 1, 2, 3): (600, 'F4_NLQQ_0', True),
  (0, 1, 3, 2): (601, 'F4_NLQQ_1', True),
  (0, 2, 1, 3): (602, 'F4_NLQQ_2', False),
  (0, 3, 1, 2): (603, 'F4_NLQQ_3', True),
  (0, 2, 3, 1): (604, 'F4_NLQQ_4', True),
  (0, 3, 2, 1): (605, 'F4_NLQQ_5', False),
  (1, 0, 2, 3): (606, 'F4_NLQQ_6', True),
  (1, 0, 3, 2): (607, 'F4_NLQQ_7', True),
  (2, 0, 1, 3): (608, 'F4_NLQQ_8', True),
  (3, 0, 1, 2): (609, 'F4_NLQQ_9', True),
  (2, 0, 3, 1): (610, 'F4_NLQQ_10', False),
  (3, 0, 2, 1): (611, 'F4_NLQQ_11', False),
  (1, 2, 0, 3): (612, 'F4_NLQQ_12', True),
  (1, 3, 0, 2): (613, 'F4_NLQQ_13', True),
  (2, 1, 0, 3): (614, 'F4_NLQQ_14', False),
  (3, 1, 0, 2): (615, 'F4_NLQQ_15', True),
  (2, 3, 0, 1): (616, 'F4_NLQQ_16', True),
  (3, 2, 0, 1): (617, 'F4_NLQQ_17', True),
  (1, 2, 3, 0): (618, 'F4_NLQQ_18', True),
  (1, 3, 2, 0): (619, 'F4_NLQQ_19', True),
  (2, 1, 3, 0): (620, 'F4_NLQQ_20', False),
  (3, 1, 2, 0): (621, 'F4_NLQQ_21', True),
  (2, 3, 1, 0): (622, 'F4_NLQQ_22', True),
  (3, 2, 1, 0): (623, 'F4_NLQQ_23', True),
  }
  for permutation, elem in permutations.iteritems():
    vid, vid_str, defaultp = elem
    recola_base = register_permutation(all_ls, permutation, vid,
                                       recola_base, vid_str=vid_str,
                                       defaultp=defaultp)

  # l+ l- q~ q (tau+ tau- t~ t)
  all_ls = [
   'GammaP(-1,1,2)*GammaP(-1,3,4)',
   'GammaP(-1,1,-2)*GammaP(-1,3,-3)*GammaM(-4,-2,2)*GammaM(-4,-3,4)',
   'GammaP(-1,1,-2)*GammaP(-3,3,-4)*GammaM(-3,-2,2)*GammaM(-1,-4,4)',
   'GammaP(-1,-2,2)*GammaP(-1,-3,4)*GammaM(-4,1,-2)*GammaM(-4,3,-3)',
   'GammaP(-1,-2,2)*GammaP(-3,-4,4)*GammaM(-3,1,-2)*GammaM(-1,3,-4)',
   'GammaP(-1,1,2)*GammaM(-1,3,4)',
   'GammaP(-1,3,4)*GammaM(-1,1,2)',
   'GammaM(-1,1,2)*GammaM(-1,3,4)',
   'ProjP(1,2)*ProjP(3,4)',
   'ProjM(1,2)*ProjM(3,4)'
   ]
  permutations = OD([
  ((0, 1, 2, 3), (700, 'F4_LLQQ_0', True)),
  ((0, 1, 3, 2), (701, 'F4_LLQQ_1', True)),
  ((0, 2, 1, 3), (702, 'F4_LLQQ_2', False)),
  ((0, 3, 1, 2), (703, 'F4_LLQQ_3', True)),
  ((0, 2, 3, 1), (704, 'F4_LLQQ_4', True)),
  ((0, 3, 2, 1), (705, 'F4_LLQQ_5', False)),
  ((1, 0, 2, 3), (706, 'F4_LLQQ_6', True)),
  ((1, 0, 3, 2), (707, 'F4_LLQQ_7', True)),
  ((2, 0, 1, 3), (708, 'F4_LLQQ_8', True)),
  ((3, 0, 1, 2), (709, 'F4_LLQQ_9', True)),
  ((2, 0, 3, 1), (710, 'F4_LLQQ_10', False)),
  ((3, 0, 2, 1), (711, 'F4_LLQQ_11', False)),
  ])
  for permutation, elem in permutations.iteritems():
    vid, vid_str, defaultp = elem
    recola_base = register_permutation(all_ls, permutation, vid,
                                       recola_base, vid_str=vid_str,
                                       defaultp=defaultp)


  # c~ b b~ t
  # cannot merge
  # 'GammaM(-1,1,4)*GammaM(-1,3,2)', 'GammaM(-1,1,2)*GammaM(-1,3,4)' together
  # because it will always match single one from previous bases rather than
  # introducing a zero coupling -> optimize inside recola
  # QQ'Q'Q
  all_ls = [
   'GammaP(-1,1,4)*GammaP(-1,3,2)',
   'GammaP(-1,1,4)*GammaM(-1,3,2)',
   'GammaP(-1,3,2)*GammaM(-1,1,4)',
   'GammaM(-1,1,4)*GammaM(-1,3,2)',
   'ProjP(1,2)*ProjP(3,4)' ,
   'ProjP(1,4)*ProjP(3,2)' ,
   'ProjM(1,4)*ProjM(3,2)' ,
   'ProjM(1,2)*ProjM(3,4)' ,
   ]
  permutations = OD([
  ((0, 1, 2, 3), (712,'F4_QQQQ_1_0', True)),
  ((0, 1, 3, 2), (713,'F4_QQQQ_1_1', True)),
  ((0, 2, 1, 3), (714,'F4_QQQQ_1_2', False)),
  ((0, 3, 1, 2), (715,'F4_QQQQ_1_3', True)),
  ((0, 2, 3, 1), (716,'F4_QQQQ_1_4', True)),
  ((0, 3, 2, 1), (717,'F4_QQQQ_1_5', True)),
  ((1, 0, 2, 3), (718,'F4_QQQQ_1_6', True)),
  ((1, 0, 3, 2), (719,'F4_QQQQ_1_7', True)),
  ((2, 0, 1, 3), (720,'F4_QQQQ_1_8', True)),
  ((3, 0, 1, 2), (721,'F4_QQQQ_1_9', True)),
  ((2, 0, 3, 1), (722,'F4_QQQQ_1_10', False)),
  ((3, 0, 2, 1), (723,'F4_QQQQ_1_11', False))]
  )
  for permutation, elem in permutations.iteritems():
    vid, vid_str, defaultp = elem
    recola_base = register_permutation(all_ls, permutation, vid,
                                       recola_base, vid_str=vid_str,
                                       defaultp=defaultp)
  all_ls = [
   'GammaP(-1,1,4)*GammaP(-1,3,2)',
   'GammaP(-1,1,4)*GammaM(-1,3,2)',
   'GammaP(-1,3,2)*GammaM(-1,1,4)',
   'GammaM(-1,1,2)*GammaM(-1,3,4)',
   'ProjP(1,2)*ProjP(3,4)',
   'ProjP(1,4)*ProjP(3,2)',
   'ProjM(1,4)*ProjM(3,2)',
   'ProjM(1,2)*ProjM(3,4)',
   ]
  permutations = OD([
    ((0, 1, 2, 3), (724, 'F4_QQQQ_2_0', True)),
    ((0, 1, 3, 2), (725, 'F4_QQQQ_2_1', True)),
    ((0, 2, 1, 3), (726, 'F4_QQQQ_2_2', False)),
    ((0, 3, 1, 2), (727, 'F4_QQQQ_2_3', True)),
    ((0, 2, 3, 1), (728, 'F4_QQQQ_2_4', True)),
    ((0, 3, 2, 1), (729, 'F4_QQQQ_2_5', True)),
    ((1, 0, 2, 3), (730, 'F4_QQQQ_2_6', True)),
    ((1, 0, 3, 2), (731, 'F4_QQQQ_2_7', True)),
    ((2, 0, 1, 3), (732, 'F4_QQQQ_2_8', True)),
    ((3, 0, 1, 2), (733, 'F4_QQQQ_2_9', True)),
    ((2, 0, 3, 1), (734, 'F4_QQQQ_2_10', False)),
    ((3, 0, 2, 1), (735, 'F4_QQQQ_2_11', False))
    ])
  for permutation, elem in permutations.iteritems():
    vid, vid_str, defaultp = elem
    recola_base = register_permutation(all_ls, permutation, vid,
                                       recola_base, vid_str=vid_str,
                                       defaultp=defaultp)

  # idential 4 quarks
  all_ls = [
   'GammaP(-1,1,4)*GammaP(-1,3,2)',
   'GammaP(-1,3,4)*GammaM(-1,1,2) + GammaP(-1,1,2)*GammaM(-1,3,4)',
   'GammaP(-1,3,2)*GammaM(-1,1,4) + GammaP(-1,1,4)*GammaM(-1,3,2)',
   'GammaM(-1,1,4)*GammaM(-1,3,2)'
   ]
  permutations = OD([
    ((0, 1, 2, 3), (736, 'F4_QQQQ_3_0', True)),
    ((0, 1, 3, 2), (737, 'F4_QQQQ_3_1', True)),
    ((0, 2, 1, 3), (738, 'F4_QQQQ_3_2', False)),
    ((0, 3, 1, 2), (739, 'F4_QQQQ_3_3', True)),
    ((0, 2, 3, 1), (740, 'F4_QQQQ_3_4', True)),
    ((0, 3, 2, 1), (741, 'F4_QQQQ_3_5', True)),
    ((1, 0, 2, 3), (742, 'F4_QQQQ_3_6', True)),
    ((1, 0, 3, 2), (743, 'F4_QQQQ_3_7', True)),
    ((2, 0, 1, 3), (744, 'F4_QQQQ_3_8', True)),
    ((3, 0, 1, 2), (745, 'F4_QQQQ_3_9', True)),
    ((2, 0, 3, 1), (746, 'F4_QQQQ_3_10', False)),
    ((3, 0, 2, 1), (747, 'F4_QQQQ_3_11', False))
    ])
  for permutation, elem in permutations.iteritems():
    vid, vid_str, defaultp = elem
    recola_base = register_permutation(all_ls, permutation, vid,
                                       recola_base, vid_str=vid_str,
                                       defaultp=defaultp)
  # d~ d d~ d
  all_ls = [
   'GammaP(-1,1,2)*GammaP(-1,3,4)',
   'GammaP(-1,3,4)*GammaM(-1,1,2) + GammaP(-1,1,2)*GammaM(-1,3,4)',
   'GammaP(-1,3,2)*GammaM(-1,1,4) + GammaP(-1,1,4)*GammaM(-1,3,2)',
   'GammaM(-1,1,4)*GammaM(-1,3,2)'
   ]
  permutations = OD([
    ((0, 1, 2, 3), (748, 'F4_QQQQ_4_0', True)),
    ((0, 1, 3, 2), (749, 'F4_QQQQ_4_1', True)),
    ((0, 2, 1, 3), (750, 'F4_QQQQ_4_2', False)),
    ((0, 3, 1, 2), (751, 'F4_QQQQ_4_3', True)),
    ((0, 2, 3, 1), (752, 'F4_QQQQ_4_4', True)),
    ((0, 3, 2, 1), (753, 'F4_QQQQ_4_5', True)),
    ((1, 0, 2, 3), (754, 'F4_QQQQ_4_6', True)),
    ((1, 0, 3, 2), (755, 'F4_QQQQ_4_7', True)),
    ((2, 0, 1, 3), (756, 'F4_QQQQ_4_8', True)),
    ((3, 0, 1, 2), (757, 'F4_QQQQ_4_9', True)),
    ((2, 0, 3, 1), (758, 'F4_QQQQ_4_10', False)),
    ((3, 0, 2, 1), (759, 'F4_QQQQ_4_11', False))
    ])
  for permutation, elem in permutations.iteritems():
    vid, vid_str, defaultp = elem
    recola_base = register_permutation(all_ls, permutation, vid,
                                       recola_base, vid_str=vid_str,
                                       defaultp=defaultp)

  # d~ d nu_e~ nu_e
  all_ls = [
   'GammaP(-1,1,2)*GammaM(-1,3,4)',
   'GammaM(-1,1,2)*GammaM(-1,3,4)'
   ]
  permutations = OD([
    ((0, 1, 2, 3), (760, 'F4_QQQQ_5_0', True)),
    ((0, 1, 3, 2), (761, 'F4_QQQQ_5_1', True)),
    ((0, 2, 1, 3), (762, 'F4_QQQQ_5_2', False)),
    ((0, 3, 1, 2), (763, 'F4_QQQQ_5_3', True)),
    ((0, 2, 3, 1), (764, 'F4_QQQQ_5_4', False)),
    ((0, 3, 2, 1), (765, 'F4_QQQQ_5_5', False)),
    ((1, 0, 2, 3), (766, 'F4_QQQQ_5_6', True)),
    ((1, 0, 3, 2), (767, 'F4_QQQQ_5_7', True)),
    ((2, 0, 1, 3), (768, 'F4_QQQQ_5_8', True)),
    ((3, 0, 1, 2), (769, 'F4_QQQQ_5_9', True)),
    ((2, 0, 3, 1), (770, 'F4_QQQQ_5_10', False)),
    ((3, 0, 2, 1), (771, 'F4_QQQQ_5_11', False)),
    ((1, 2, 0, 3), (772, 'F4_QQQQ_5_12', True)),
    ((1, 3, 0, 2), (773, 'F4_QQQQ_5_13', True)),
    ((2, 1, 0, 3), (774, 'F4_QQQQ_5_14', False)),
    ((3, 1, 0, 2), (775, 'F4_QQQQ_5_15', True)),
    ((2, 3, 0, 1), (776, 'F4_QQQQ_5_16', True)),
    ((3, 2, 0, 1), (777, 'F4_QQQQ_5_17', True)),
    ((1, 2, 3, 0), (778, 'F4_QQQQ_5_18', True)),
    ((1, 3, 2, 0), (779, 'F4_QQQQ_5_19', True)),
    ((2, 1, 3, 0), (780, 'F4_QQQQ_5_20', False)),
    ((3, 1, 2, 0), (781, 'F4_QQQQ_5_21', True)),
    ((2, 3, 1, 0), (782, 'F4_QQQQ_5_22', True)),
    ((3, 2, 1, 0), (783, 'F4_QQQQ_5_23', True))
  ])
  for permutation, elem in permutations.iteritems():
    vid, vid_str, defaultp = elem
    recola_base = register_permutation(all_ls, permutation, vid,
                                       recola_base, vid_str=vid_str,
                                       defaultp=defaultp)

  # nu_mu~ nu_e nu_e~ nu_mu
  all_ls = [
   'GammaM(-1,1,4)*GammaM(-1,3,2)',
   'GammaM(-1,1,2)*GammaM(-1,3,4)'
   ]
  permutations = OD([
    ((0, 1, 2, 3), (784, 'F4_QQQQ_6_0', True)),
    ((0, 1, 3, 2), (785, 'F4_QQQQ_6_1', True)),
    ((0, 2, 1, 3), (786, 'F4_QQQQ_6_2', False)),
    ((1, 0, 2, 3), (787, 'F4_QQQQ_6_3', True)),
    ((1, 0, 3, 2), (788, 'F4_QQQQ_6_4', True)),
    ((2, 0, 3, 1), (789, 'F4_QQQQ_6_5', False))
  ])
  for permutation, elem in permutations.iteritems():
    vid, vid_str, defaultp = elem
    recola_base = register_permutation(all_ls, permutation, vid,
                                       recola_base, vid_str=vid_str,
                                       defaultp=defaultp)

  return recola_base

def vvvv_base_eft(recola_base):
  """
  + cop(1)*g_{mu,al}*g_{nu,ro}
  + cop(2)*g_{mu,ro}*g_{nu,al}
  + cop(3)*g_{mu,nu}*g_{ro,al}
  """
  lorentz1 = 'Metric(1,4)*Metric(2,3)'
  lorentz2 = 'Metric(1,3)*Metric(2,4)'
  lorentz3 = 'Metric(1,2)*Metric(3,4)'

  lorentz4 = '- P(1,2)*P(4,1)*Metric(2,3) + P(3,2)*P(4,1)*Metric(1,2) - P(2,1)*P(3,2)*Metric(1,4) + P(-1,1)*P(-1,2)*Metric(1,4)*Metric(2,3)'

  # lorentz5 = '(P(3,4)*P(4,1) + P(3,4)*P(4,2) + P(3,1)*P(4,3) + P(3,2)*P(4,3))*Metric(1,2)'
  # lorentz6 = '(P(2,3)*P(4,1) - P(2,4)*P(4,1) - P(2,3)*P(4,2) - P(2,1)*P(4,3))*Metric(1,3)'
  # lorentz7 = '(- P(2,3)*P(3,1) + P(2,4)*P(3,1) - P(2,4)*P(3,2) - P(2,1)*P(3,4))*Metric(1,4)'
  # lorentz8 = '(- P(1,3)*P(4,1) + P(1,3)*P(4,2) - P(1,4)*P(4,2) - P(1,2)*P(4,3))*Metric(2,3)'
  # lorentz9 = '(- P(1,4)*P(3,1) - P(1,3)*P(3,2) + P(1,4)*P(3,2) - P(1,2)*P(3,4))*Metric(2,4)'
  # lorentz10 = '(P(1,3)*P(2,1) + P(1,4)*P(2,1) + P(1,2)*P(2,3) + P(1,2)*P(2,4))*Metric(3,4)'
  # lorentz11 = '-(P(-1,1)*P(-1,3) + P(-1,2)*P(-1,3) + P(-1,1)*P(-1,4) + P(-1,2)*P(-1,4))*Metric(1,2)*Metric(3,4)'
  # lorentz12 = '(P(-1,1)*P(-1,3) + P(-1,2)*P(-1,4))*Metric(1,4)*Metric(2,3)'
  # lorentz13 = '(P(-1,2)*P(-1,3) + P(-1,1)*P(-1,4))*Metric(1,3)*Metric(2,4)'

  permutations = [u for u in gen_permutations(range(4))]
  # permutations = {
  #  (0, 1, 2, 3): '?',
  #  # (0, 1, 2, 3): '771',
  #  # (0, 1, 3, 2): '772',
  #  # (0, 2, 3, 1): '773',
  #  # (1, 2, 3, 0): '774',
  #  # (0, 1, 3, 2, 4): '777',
  #  # (0, 1, 2, 3, 4): '778',
  #  # (0, 1, 4, 2, 3): '779',
  #  # (0, 2, 3, 1, 4): '780',
  #  # (0, 2, 4, 1, 3): '781',
  #  # (0, 3, 4, 1, 2): '782',
  #  # (1, 2, 3, 0, 4): '783',
  #  # (1, 2, 4, 0, 3): '784',
  #  # (1, 3, 4, 0, 2): '785',
  #  # (2, 3, 4, 0, 1): '786'
  #  }
# permutations =

  permutations = {
      (0, 1, 2, 3): '?',
      # (0, 1, 2, 3, 4): '?',
      # (0, 1, 2, 4, 3): '?',
      # (0, 1, 3, 4, 2): '?',
      # (0, 2, 3, 4, 1): '?',
      # (1, 2, 3, 4, 0): '?',
      # (0, 1, 2, 3, 4, 5): '?',
      # (0, 1, 2, 4, 3, 5): '?',
      # (0, 1, 2, 5, 3, 4): '?',
      # (0, 1, 3, 4, 2, 5): '?',
      # (0, 1, 3, 5, 2, 4): '?',
      # (0, 1, 4, 5, 2, 3): '?',
      # (0, 2, 3, 4, 1, 5): '?',
      # (0, 2, 3, 5, 1, 4): '?',
      # (0, 2, 4, 5, 1, 3): '?',
      # (0, 3, 4, 5, 1, 2): '?',
      # (1, 2, 3, 4, 0, 5): '?',
      # (1, 2, 3, 5, 0, 4): '?',
      # (1, 2, 4, 5, 0, 3): '?',
      # (1, 3, 4, 5, 0, 2): '?',
      # (2, 3, 4, 5, 0, 1): '?',
      }
  dealt_with = []
  perms = []
  # for permutation, vid in permutations.iteritems():
  for ppos, permutation in enumerate(permutations):
    ids = []
    lco = []
    for ls in [lorentz1, lorentz2, lorentz3]:
      lbs = CurrentBase(ls).compute(permutation)
      # print("permutation:", permutation)
      # from rept1l.combinatorics import lorentz_base_dict, lorentz_base
      # arg1id = lbs[0][1][0]
      # arg2id = lbs[1][1][0]
      # lbb = lorentz_base_dict[4*Symbol('P')*Symbol('Metric')]
      # print("arg1id:", arg1id)
      # print("arg2id:", arg2id)
      # print("1:", 'P' + str(lbb[arg1id][0]) + '*Metric' +  str(lbb[arg1id][1]))
      # print("2:", 'P' + str(lbb[arg2id][0]) + '*Metric' +  str(lbb[arg2id][1]))
      # print("lbb[arg2id]:", lbb[arg2id])
      # print("lorentz_base.order:", lorentz_base.order)
      # import sys
      # sys.exit()
      # if lbs not in dealt_with:
      #   dealt_with.append(lbs)
      #   perms.append(permutation)
      # else:
      #   continue

      lco += [[tuple(u) for u in lbs]]


    dw = []
    for pp in gen_permutations(range(4)):
      lbs = CurrentBase(lorentz4).compute(pp)
      if lbs not in dw:
        lco += [[tuple(u) for u in lbs]]
        dw.append(lbs)

    # if len(permutation) == 4:
    #   for ls in [lorentz4, lorentz5, lorentz6,
    #              lorentz7, lorentz8, lorentz9,
    #              lorentz10, lorentz11, lorentz12,
    #              lorentz13]:
    #   lbs = CurrentBase(ls).compute(permutation)
    (Vertex.current_strucs, vbase, _,
     Vertex.n_lorentz) = insert_ls(Vertex.current_strucs,
                                   lco, Vertex.n_lorentz,
                                   simple_structures=True)
    # assert(len(vbase) == 1)
    # ids.append(vbase[0][1])
    # ids = (vbase[0][1],)
    ids = tuple(u[1] for u in vbase)
    # recola_base[ids] = vid
    recola_base[ids] = str(77 + ppos)
    if ids not in dealt_with:
      dealt_with.append(ids)
      perms.append(permutation)
  #   print("vbase[0][1]:", vbase[0][1])
  # print("perms:", perms)
  # print("len(perms):", len(perms))
  # print("len(permutations):", len(permutations))

  # import sys
  # sys.exit()
  return recola_base






def p_3_base(recola_base):
  # note: momenta in UFO are outgoing -> will be reversed
  lorentz = 'P(1,2) - P(1,3)'
  # assume 2,3 are indistinguishable
  permutations = [[0, 1, 2], [1, 2, 0], [2, 0, 1]]
  for permutation in permutations:
    lbs = CurrentBase(lorentz).compute(permutation)
    lco = [[tuple(u) for u in lbs]]
    (Vertex.current_strucs, _, _,
     Vertex.n_lorentz) = insert_ls(Vertex.current_strucs, lco, Vertex.n_lorentz,
                                   simple_structures=True)
    nlorentz = Vertex.n_lorentz
    # lorentz = 'P(1,2) - P(1,3)': v + s -> s
    if permutation == [0, 1, 2]:
      recola_base[(nlorentz,)] = '4'
    # lorentz = 'P(2,3) - P(2,1)': s + v -> s
    elif permutation == [1, 2, 0]:
      recola_base[(nlorentz,)] = '3'
    # lorentz = 'P(3,1) - P(3,2)': s + s -> v
    elif permutation == [2, 0, 1]:
      recola_base[(nlorentz,)] = '11'

  # additional rules for BFM
  # v + s -> s
  # lorentz = 'P(1,2)'
  lorentz = '-P(1,3)-P(1,1)'
  lbs = CurrentBase(lorentz).compute(range(3))
  lco = [[tuple(u) for u in lbs]]
  (Vertex.current_strucs, _, _,
   Vertex.n_lorentz) = insert_ls(Vertex.current_strucs, lco, Vertex.n_lorentz,
                                 simple_structures=True)
  nlorentz = Vertex.n_lorentz
  recola_base[(nlorentz,)] = '162'

  # s + s -> v
  # lorentz = 'P(3,2)'
  lorentz = '-P(3,1)-P(3,3)'
  lbs = CurrentBase(lorentz).compute(range(3))
  lco = [[tuple(u) for u in lbs]]
  (Vertex.current_strucs, _, _,
   Vertex.n_lorentz) = insert_ls(Vertex.current_strucs, lco, Vertex.n_lorentz,
                                 simple_structures=True)
  nlorentz = Vertex.n_lorentz
  recola_base[(nlorentz,)] = '161'

  # v + s -> s
  # lorentz = 'P(1,3)'
  lorentz = '-P(1,1)-P(1,2)'
  lbs = CurrentBase(lorentz).compute(range(3))
  lco = [[tuple(u) for u in lbs]]
  (Vertex.current_strucs, _, _,
   Vertex.n_lorentz) = insert_ls(Vertex.current_strucs, lco, Vertex.n_lorentz,
                                 simple_structures=True)
  nlorentz = Vertex.n_lorentz
  recola_base[(nlorentz,)] = '163'

  # s + v -> s
  # lorentz = 'P(2,3)'
  lorentz = '-P(2,1) - P(2,2)'
  lbs = CurrentBase(lorentz).compute(range(3))
  lco = [[tuple(u) for u in lbs]]
  (Vertex.current_strucs, vbase, _,
   Vertex.n_lorentz) = insert_ls(Vertex.current_strucs, lco, Vertex.n_lorentz,
                                 simple_structures=True)
  nlorentz = Vertex.n_lorentz
  recola_base[(nlorentz,)] = '165'

  # s + v -> s
  # lorentz = 'P(2,1)'
  lorentz = '-P(2,2)-P(2,3)'
  lbs = CurrentBase(lorentz).compute(range(3))
  lco = [[tuple(u) for u in lbs]]
  (Vertex.current_strucs, _, _,
   Vertex.n_lorentz) = insert_ls(Vertex.current_strucs, lco, Vertex.n_lorentz,
                                 simple_structures=True)
  nlorentz = Vertex.n_lorentz
  recola_base[(nlorentz,)] = '166'

  # s + s -> v
  # lorentz = 'P(3,1)'
  lorentz = '-P(3,2)-P(3,3)'
  lbs = CurrentBase(lorentz).compute(range(3))
  lco = [[tuple(u) for u in lbs]]
  (Vertex.current_strucs, _, _,
   Vertex.n_lorentz) = insert_ls(Vertex.current_strucs, lco, Vertex.n_lorentz,
                                 simple_structures=True)
  nlorentz = Vertex.n_lorentz
  recola_base[(nlorentz,)] = '164'

  return recola_base


def scalar_3_4_base(recola_base):
  # s + s -> s
  lorentz = '1'
  lbs = CurrentBase(lorentz).compute(range(3))

  lco = [[tuple(u)] for u in lbs]
  (Vertex.current_strucs, t, a,
   Vertex.n_lorentz) = insert_ls(Vertex.current_strucs, lco, Vertex.n_lorentz,
                                 simple_structures=True)
  nlorentz = Vertex.n_lorentz
  recola_base[(nlorentz,)] = '1'

  # s + s + s -> s
  lorentz = '1'
  lbs = CurrentBase(lorentz).compute(range(4))

  lco = [[tuple(u)] for u in lbs]
  (Vertex.current_strucs, t, a,
   Vertex.n_lorentz) = insert_ls(Vertex.current_strucs, lco, Vertex.n_lorentz,
                                 simple_structures=True)
  nlorentz = Vertex.n_lorentz
  recola_base[(nlorentz,)] = '-1'
  return recola_base


def p_base_eft(recola_base):

  lorentz = 'P(3,2) + P(3,3)'

  dealt_with = []
  perms = []


  # permutations = [u for u in gen_permutations(range(3))]

  permutations = {
      (0, 1, 2): '221',
      (0, 2, 1): '222',
      (1, 0, 2): '223',
      (2, 0, 1): '224',
      (1, 2, 0): '225',
      (2, 1, 0): '226'
      }

  for permutation, vid in iteritems(permutations):
    lbs = CurrentBase(lorentz).compute(permutation)
    lco = [[tuple(u) for u in lbs]]

    (Vertex.current_strucs, vbase, _,
     Vertex.n_lorentz) = insert_ls(Vertex.current_strucs,
                                   lco, Vertex.n_lorentz,
                                   simple_structures=True)
    assert(len(vbase) == 1)
    ids = (vbase[0][1],)
    recola_base[ids] = vid
  return recola_base


# def VVV_base_eft(recola_base):
#
#   lorentz1 = '-P(1,2)*P(2,3)*P(3,1) + P(1,3)*P(2,1)*P(3,2)'
#   lorentz2 = ('+ P(-1,2)*P(-1,3)*P(3,1)*Metric(1,2)' +
#              '- P(-1,1)*P(-1,3)*P(3,2)*Metric(1,2)' +
#              '- P(-1,2)*P(-1,3)*P(2,1)*Metric(1,3)' +
#              '+ P(-1,1)*P(-1,2)*P(2,3)*Metric(1,3)' +
#              '+ P(-1,1)*P(-1,3)*P(1,2)*Metric(2,3)' +
#              '- P(-1,1)*P(-1,2)*P(1,3)*Metric(2,3)')
#   lorentz3 = 'Epsilon(1,2,3,-1)*P(-1,1)'
#
#   dealt_with = []
#   perms = []
#
#
#   # permutations = [u for u in gen_permutations(range(3))]
#
#   # permutations = {
#   #     (0, 1, 2): '221',
#   #     (0, 2, 1): '222',
#   #     (1, 0, 2): '223',
#   #     (2, 0, 1): '224',
#   #     (1, 2, 0): '225',
#   #     (2, 1, 0): '226'
#   #     }
#
#   permutations = {
#       (0, 1, 2): 'VVV_EFT_012',
#       (0, 2, 1): 'VVV_EFT_021',
#       }
#
#   for permutation, vid in iteritems(permutations):
#   # for ppos, permutation in enumerate(permutations):
#
#     lco = []
#     for ls in [lorentz1, lorentz2, lorentz3]:
#       lbs = CurrentBase(ls).compute(permutation)
#       lco += [[tuple(u) for u in lbs]]
#     # lbs = CurrentBase(lorentz).compute(permutation)
#     # lco = [[tuple(u) for u in lbs]]
#
#     (Vertex.current_strucs, vbase, _,
#      Vertex.n_lorentz) = insert_ls(Vertex.current_strucs,
#                                    lco, Vertex.n_lorentz,
#                                    simple_structures=True)
#     # print("vbase:", vbase)
#     # assert(len(vbase) == 2)
#     ids = tuple(u[1] for u in vbase)
#     recola_base[ids] = vid
#     ids = tuple(u[1] for u in vbase[:2])
#     recola_base[ids] = vid + '_2'
#     # recola_base[ids] = str(ppos + 10000)
#     if lbs not in dealt_with:
#       dealt_with.append(lbs)
#       perms.append(permutation)
#       # else:
#         # continue
#   # print("dealt_with:", dealt_with)
#   # print("perms:", perms)
#   # print("len(perms):", len(perms))
#   # print("len(permutations):", len(permutations))
#   # import sys
#   # sys.exit()
#   return recola_base

def VSS_base_eft(recola_base):
  lorentz0 = 'P(1,2) - P(1,3)'
  lorentz1 = '-(P(-1,1)*P(-1,3)*P(1,2)) + P(-1,1)*P(-1,2)*P(1,3)'
  lorentz2 = 'Epsilon(1,-1,-2,-3)*P(-3,1)*P(-2,2)*P(-1,3)'
  permutations = {
      (0, 1, 2): 'VSS_EFT_012',
      (0, 2, 1): 'VSS_EFT_021',
      (1, 0, 2): 'VSS_EFT_102',
      (2, 0, 1): 'VSS_EFT_201',
      (1, 2, 0): 'VSS_EFT_120',
      (2, 1, 0): 'VSS_EFT_210'
      }

  for permutation, vid in iteritems(permutations):
    lco = []
    for ls in [lorentz0, lorentz1, lorentz2]:
      lbs = CurrentBase(ls).compute(permutation)
      lco += [[tuple(u) for u in lbs]]

    (Vertex.current_strucs, vbase, _,
     Vertex.n_lorentz) = insert_ls(Vertex.current_strucs,
                                   lco, Vertex.n_lorentz,
                                   simple_structures=True)
    ids = tuple(u[1] for u in vbase)
    recola_base[ids] = vid
    # drop last structure to make an independent one
    ids = tuple(u[1] for u in vbase[:2])
    recola_base[ids] = (vid + '_2')
  return recola_base


def UV_complete_base(init={}):
  recola_base = gamma_3_base(init)
  recola_base = g_4_base(recola_base)
  recola_base = g_g_4_base(recola_base)
  recola_base = g_3_base(recola_base)

  # don't use this anymore, general structure in VVV_EFT
  recola_base = p_g_3_base(recola_base)
  recola_base = p_g_3_bfm_base(recola_base)
  recola_base = p_3_base(recola_base)
  recola_base = w_3_base(recola_base)
  recola_base = scalar_3_4_base(recola_base)

  return recola_base


def D6D8_TGC(init={}):
  recola_base = gamma_3_base(init)
  recola_base = ZZZ_d8_eft(recola_base)
  recola_base = vvv_d6_eft(recola_base)


def HEFT_base(init={}):
  recola_base = UV_complete_base(init=init)
  recola_base = p_g_3_base_heft(recola_base)
  recola_base = vvvv_base_heft(recola_base)
  recola_base = vvs_base_heft(recola_base)
  recola_base = vvss_base_heft(recola_base)

  return recola_base


def gen_recola_base():
  recola_base = HEFT_base()
  # recola_base = fourferm_base_eft({})
  # recola_base = gamma_3_base({})
  # recola_base = g_4_base(recola_base)
  # recola_base = g_g_4_base(recola_base)
  # recola_base = g_3_base(recola_base)

  # don't use this anymore, general structure in VVV_EFT
  # recola_base = p_g_3_base(recola_base)

  # recola_base = p_g_3_bfm_base(recola_base)
  # recola_base = p_3_base(recola_base)
  # recola_base = w_3_base(recola_base)
  # recola_base = scalar_3_4_base(recola_base)
  #
  # recola_base = p_g_base_eft({})

  # recola_base = p_g_3_base_eft(recola_base)
  # recola_base = vvv_base_eft(recola_base)
  # recola_base = g_p_base_eft(recola_base)
  # recola_base = vvvv_base_eft(recola_base)
  # recola_base = p_base_eft(recola_base)
  # recola_base = VVV_base_eft(recola_base)
  # recola_base = VSS_base_eft(recola_base)

  # recola_base = ZZZ_d8_eft(recola_base)
  # recola_base = vvv_d6_eft(recola_base)

  # recola_base = vvv_d6_eft({})
  # print("recola_base:", recola_base)
  # import sys
  # sys.exit()

  return recola_base
