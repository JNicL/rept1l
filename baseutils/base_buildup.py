#==============================================================================#
#                               base_buildup.py                                #
#==============================================================================#
from __future__ import print_function

#============#
#  Includes  #
#============#
from future.utils import implements_iterator

from sympy import sympify, Symbol, cancel
from sympy import factor as sympy_factor

from rept1l.combinatorics import order_expression, order_strucs, lorentz_base
from rept1l.combinatorics import lorentz_base_dict, get_lorentz_key_components
from rept1l.combinatorics import compute_permutation_from_elements
from rept1l.combinatorics import permute_spins
from rept1l.helper_lib import flatten
from rept1l.logging_setup import log

#===========#
#  Methods  #
#===========#


class BaseMismatch(Exception):
  """ BaseMismatch is raised when a base could not be matched to an existing
  one.  """

  def __init__(self, ls_only_base, global_ls_only_base, global_ls,
               ls_global_key):
    self.ls_only_base = ls_only_base
    self.global_ls_only_base = global_ls_only_base
    self.global_ls = global_ls
    self.ls_global_key = ls_global_key

  def __str__(self):
    return repr(self.value)


def couplingcoeffs(expr, base):
  """ Translates a sum of couplings into a list of coefficients (integers) over
  the basis `base`

  :param expr: sum of couplings
  :type  expr: sympy expressions

  :param expr: list of coupling symbols
  :type  expr: list of sympy expressions

  >>> c1 = Symbol('c1'); c2 = Symbol('c2')
  >>> couplingcoeffs(c1+2*c2, [c1, c2])
  [1, 2]
  """
  expre = expr.expand()
  return [expre.coeff(c) for c in base]


def gen_csym(i=-1):
  """ Coupling symbol generator """
  while True:
    i += 1
    yield Symbol("d" + str(i))


def simple_coupling_poly_fail(ccs):
  """ Simple gauss algorithm. The first linear independent couplings serve as
  basis. All other couplings are expressed in that basis.
  >>> c1 = Symbol('c1'); c2 = Symbol('c2')
  >>> expr = [c1+2*c2, 2*c1+4*c2, c1-c2,-2*c1-4*c2, 4*c1+4*c2]
  >>> simple_coupling_poly(expr)
  ([d0, 2*d0, d1, -2*d0, 8*d0/3 + 4*d1/3], 2)


  ALGORITHM FAILS FOR

  expr = [c1, c1, c1 + c2, c1 - c2, c1 - c2, c1 + c2, c0 + c1 + c2,
          c0 + c1 + c2, c0 + c1 + c2, c0 + c1 + c2, c0 + c1 + c3, c0 + c1 + c3]

  [c0, c0, c0]
  """
  cbase = []
  for c in ccs:
    for cs in c.free_symbols:
      if cs not in cbase:
        cbase.append(cs)
  base = []  # set of new base couplings
  mapped = {}  # couplings which are successfully mapped onto base
  base_mapped = {}  # shift of base couplings in terms of other base couplings
  for cpos, c in enumerate(ccs):
    lc = {}
    cc = [sympify(u) for u in couplingcoeffs(c, cbase)]

    # check if coupling can be mapped onto base
    for bpos, b in enumerate(base):
      ccb = cc[bpos]
      bcb = b[bpos]
      if ccb != 0 and bcb != 0:
        cc = [x - ccb / b[bpos] * y for x, y in zip(cc, b)]
        lc[bpos] = ccb / b[bpos]
      else:
        lc[bpos] = 0

    # not possible -> introduce new (shifted) base vector
    if any(u != 0 for u in cc):
      # note that cc is shifted and not the original coupling in ccs
      base.append(cc)
      base_mapped[len(base) - 1] = lc
    else:
      # undo shift in cc base to obtain the correct mapping
      for b in lc:
        bm = base_mapped[b]
        for c in bm:
          lc[c] -= lc[b] * bm[c]
      mapped[cpos] = lc

  # build new coupling array
  cgen = gen_csym()
  ret = [0] * len(ccs)
  for c in mapped:
    m = 0
    for cindex in mapped[c]:
      coeff = mapped[c][cindex]
      m += coeff * Symbol('d' + str(cindex))
    ret[c] = m
  for bpos, b in enumerate(ret):
    if b == 0:
      ret[bpos] = next(cgen)
  return ret, len(base)


def simple_coupling_poly(ccs):
  """ Simple gauss algorithm. The first linear independent couplings serve as
  basis. All other couplings are expressed in that basis.
  >>> c1 = Symbol('c1'); c2 = Symbol('c2')
  >>> c0 = Symbol('c0'); c3 = Symbol('c3')

  >>> expr = [c1+2*c2, 2*c1+4*c2, c1-c2,-2*c1-4*c2, 4*c1+4*c2]
  >>> simple_coupling_poly(expr)
  ([d0, 2*d0, d1, -2*d0, 8*d0/3 + 4*d1/3], 2)

  >>> expr  = [c0 + c2, c0 + c2, c2 + c3, c2 + c3, c2 + c3, c2 + c3]
  >>> simple_coupling_poly(expr)
  ([d0, d0, d1, d1, d1, d1], 2)
  """
  cbase = []
  for c in ccs:
    for cs in c.free_symbols:
      if cs not in cbase:
        cbase.append(cs)

  base_mapping = {}
  cgen = gen_csym()
  ccs_new = []
  for elem in ccs:
    elem_subs = elem.subs(base_mapping)
    old_symbols_found = [u for u in elem_subs.free_symbols if u in cbase]
    if len(old_symbols_found) > 0:
      cnew = next(cgen)
      ccs_new.append(cnew)
      solvefor = old_symbols_found[0]
      coeff = elem_subs.coeff(solvefor)
      base_mapping[solvefor] = ((-elem_subs.xreplace({solvefor: 0}) + cnew) / coeff).expand()
      # eliminate variable `solvefor` in exisiting substituions to prevent
      # conflicts when substituting all expressions (it would then depend on
      # the order of substitution)
      for s in base_mapping:
        if solvefor in base_mapping[s].free_symbols:
          base_mapping[s] = base_mapping[s].subs({solvefor: base_mapping[solvefor]})
    else:
      ccs_new.append(elem_subs)
  return ccs_new, len(base_mapping)


def coeff_as_dict(expr):
  """ Extracts the prefactor from the coupling and returns the result as a dict

  >>> c1 = Symbol('c1'); c2 = Symbol('c2')
  >>> expr = c1*2 - 5*c2
  >>> coeff_as_dict(expr) == {1: 2, 2: -5}
  True
  """
  base = [u for u in expr.free_symbols]
  zero_couplings = {u: 0 for u in base if 'ZERO' in u.name}
  expr_subs = expr.subs(zero_couplings)
  if expr_subs.is_Mul:
    expr_subs = cancel(expr_subs)

  ret = {int(u.name[1:]): expr_subs.coeff(u) for u in expr_subs.free_symbols}

  return ret


def extract_couplings(expr, *args, **kwargs):
  """ Separate a global prefactor from the coupling expression. The prefactor
  is chosen as the smallest prefactor of a coupling.

  >>> c0 = Symbol('c0'); c1 = Symbol('c1')
  >>> extract_couplings(2*c0-7*c1)
  (c0 - 7*c1/2, 2)
  >>> extract_couplings(2*c0-c1/7)
  (-14*c0 + c1, -1/7)
  """
  if expr.is_Symbol:
    return (expr, 1)
  elif expr.is_Mul:
    sym = [u for u in expr.free_symbols]
    if len(sym) != 1:
      print("expr:", expr)
      print("*args:", args)
      print("**kwargs:", kwargs)
      raise Exception('Extract coupling failed for: ' + str(expr))

    sym = sym[0]
    ret = (sym, expr.coeff(sym))
    return ret
  elif expr.is_Add:
    syms = sort_couplings(expr.free_symbols)

    # reference coeff is the smallest coefficient -> avoid fracs
    c = sorted((expr.coeff(sym) for sym in syms), key=lambda x: abs(x))[0]
    ret = (sum(expr.coeff(sym) * sym / c for sym in syms), c)

    return ret
  else:
    raise Exception('Unhandled case in extract_couplings for type expr: ' +
                    str(expr))


def compute_ls_coupling_grouped(bases, ls_strucs, simple_structures=True):
  """ Applies group_base to each base in `bases`.

  E.g.
  >>> P = Symbol('P')
  >>> c0 = Symbol('c0'); c1 = Symbol('c1')

  >>> bases = [[1, [[0, c0, 1], [1, c0, 1]]], [2, [[0, c0, 1]]]]
  >>> compute_ls_coupling_grouped(bases, [3*P, 3*P])
  ([[1, [[0, c0, 1], [1, c0, 1]]], [2, [[0, c0, 1]]]], [3*P, 3*P])

  >>> bases = [[1, [[0, c0, 1], [0, c1, 1]]], [2, [[0, c0, 1]]]]
  >>> compute_ls_coupling_grouped(bases, [3*P, 3*P])
  ([[1, [[0, c0, 1]]], [1, [[0, c1, 1]]], [2, [[0, c0, 1]]]], [3*P, 3*P, 3*P])
  """
  bases_grouped = []
  ls_strucs_grouped = []
  for base_id, base in enumerate(bases):
    b, s = group_base(base, ls_strucs[base_id],
                      simple_structures=simple_structures)
    bases_grouped.extend(b)
    ls_strucs_grouped.extend(s)

  return bases_grouped, ls_strucs_grouped


def group_base(ls_base, ls_strucs, simple_structures=True):
  """ This method separates a base `base` in smaller parts which have the same
  coupling.

  Lets say we have a structure 3*P, i.e. 3 Particles, Momentum, e.g. Vector-
  Scalar-Scalar interaction and two different couplings c0, c1
  >>> P = Symbol('P')
  >>> c0 = Symbol('c0'); c1 = Symbol('c1')
  >>> ls_struc = 3*P

  One base entry with id 1 in the lorentz dict associated to P might be
               ._ base id
              /          argument id
              |         /    .coupling
              |         |   /  .prefactor
  >>> base = [1, [     [0, c0, 1],        [1, c0, -1]]]

  Which means that there is a structure with id=1, defining the following
  linear combination (note that the base does not know about the precise
  structure, here) :

  P[args from base 0]*c0*1 + P[args from base 1]*c0*(-1)

  This structure should be computed in the following way
  (P[... 0] - P[... 1])*c0*1

  The algorithm confirms a factorization if the structure is kept together in
  the following sense:
  >>> group_base(base, ls_struc)
  ([[1, [[0, c0, 1], [1, c0, -1]]]], [3*P])

  If we change the second coupling the structures are split
  >>> base = [1, [[0, c0, 1], [1, c1, -1]]]
  >>> group_base(base, ls_struc)
  ([[1, [[0, c0, 1]]], [1, [[1, c1, -1]]]], [3*P, 3*P])

  """
  ls_strucs_grouped = []
  base_sorted = []

  # b = coupling base
  # collects all appearing couplig symbols c0, c1, ..., and generates the list
  # with unique couplings [c0, c1, ..]
  bb = [u for u in flatten([[v for v in u[1].free_symbols] for u in ls_base[1]])]
  bb = [u for u in set(bb)]
  bb = sorted(bb, key=lambda x: int(x.name[1:]))

  tmp = sorted(ls_base[1], key=lambda x:
               order_expression(couplingcoeffs(x[1], base=bb), len(bb),
               weight=-1))
  try:
    b, blen = simple_coupling_poly([u[1] for u in tmp])
  except Exception:
    log("failed simple coupling new for:", 'warning')
    log('bb: ' + str(bb), 'warning')
    log(str([u[1] for u in tmp]), 'warning')
    raise

  base_group = [0] * blen  # couplings are initialized with 0
  base_sorted = [ls_base[0], tmp]

  # set() does reorder the items. Not sure if allowed
  b_unique = []
  base_group = {}
  for bpos, c in enumerate(b):
    if c not in b_unique:
      b_unique.append(c)
      base_group[c] = []
    base_group[c].append(bpos)

  # n counts the number of different couplings for the given base
  n = len(base_group)
  ls_strucs_grouped = [ls_strucs] * n

  base_grouped = []
  # group the base accoring to same couplings.
  # grouping is not done between different base_ids
  #for base_id, base in enumerate(base_sorted):
  # -> each base_group[base_id] is a different coupling
  for b in b_unique:
    group = [base_sorted[1][bpos] for bpos in base_group[b]]
    base_grouped.append([base_sorted[0], group])

  return base_grouped, ls_strucs_grouped


def join_base_couplings_and_signs(bases):
  """ Applies the method join_couplings_and_signs to each base in `bases`.
  """
  return (join_couplings_and_signs(base) for base in bases)


def join_couplings_and_signs(base):
  """ Takes a base `base` and returns a list of elements split into base
  elements and couplings x factors.
  """
  return [[b[0], b[1] * b[2]] for b in base[1]]


def sort_and_join_base(bases, n_base_elements):
  """ Applies join_couplings_and_signs and sort_joined_bases consecutively.
  """
  return sort_joined_bases(join_base_couplings_and_signs(bases),
                           n_base_elements)


def sort_joined_bases(joined_bases, n_base_elements):
  """ Takes a joined bases `joined_bases` and the total number of different
  base elements and sorts the expression according in the order of the base
  element ids.

  e.g.
  joined = [[2, c1], [10, -c1], [4, c2]]
  sorted_joined = [[2, c1], [4, c2], [10, -c1]]
  """
  return sorted(joined_bases, key=lambda x: x[0])


def sort_couplings(cl):
  """ Sort a list of couplings in the order of the coupling index.
  >>> c0 = Symbol('c0'); c2 = Symbol('c2')
  >>> ZERO1 = Symbol('ZERO1'); ZERO3 = Symbol('ZERO3')
  >>> sort_couplings([c2, c0, ZERO3, ZERO1])
  [c0, ZERO1, c2, ZERO3]
  """
  return sorted(cl, key=lambda x: int(x.name[4:]) if 'ZERO' in x.name else int(x.name[1:]))


def map_global(ls, ls_base, global_ls, global_ls_base, enlarge=False):
  """ Tries to map a lorentz structure to exisiting ones
  (global lorentz structure). If successful, the possibly modified structure is
  returned.

  Note: ls is passed even though it can be derived from ls_base because in
  insert_ls it is already available and should not be recomputed. The same is
  true for global_ls and global_ls_base.

  :param ls: The lorentz structure represented as a list of grouped tuples of
             ids and couplings x factors, see examples below.
  :type  ls: list

  :param ls_base: The lorentz structure base represented as a list of base id
                  and grouped tuples of ids and couplings x factors, see
                  examples below or see group_base method.
  :type  ls: list

  :param global_ls: Same as ls
  :type  global_ls: list

  :param global_ls_base: Same as ls_base
  :type  global_ls_base: list

  Define some coupling symbols
  >>> from sympy import symbols
  >>> c0,c1,c2 = symbols('c0 c1 c2')

  One composed lorentz structure id 6 and 12 multiplied each with the couplings
  c0 is represented as
              . argument id
             /          .argument id
             |  .coupl /    .coupling
             |  |      |    |
  >>> l = [[[6, c0], [12, c0]], ]

  >>> b = [[0, [[6, c0, 1], [12, c0, 1]]]]
  >>> gl = [[[6, -c1]], [[12, -c0]]]
  >>> gb = [[25, [[6, c1, -1]]], [25, [[12, c0, -1]]]]
  >>> nl, nb = map_global(l,b,gl,gb)
  >>> nl
  [[[6, c0]], [[12, c0]]]
  >>> nb
  [[0, [[6, c0, 1]]], [0, [[12, c0, 1]]]]


  If enlarge is disabled, mapping to a larger base is prohibited
  >>> gl = [[[6, -c1]], [[12, -c0]], [[18, c2]]]
  >>> gb = [[25, [[6, c1, -1], [12, c0, -1], [18, c2, 1]]]]
  >>> gb = [[25, [[6, c1, -1]]], [25, [[12, c0, -1]]], [25, [[18, c2, 1]]]]
  >>> mapped = map_global(l,b,gl,gb)
  >>> mapped == None
  True

  If enlarge is enabled, and a suited larger base is found, the mapping works
  and introduces zero couplings
  >>> nl, nb = map_global(l,b,gl,gb,enlarge=True)
  >>> nl
  [[[6, c0]], [[12, c0]], [[18, ZERO2]]]
  >>> nb
  [[0, [[6, c0, 1]]], [0, [[12, c0, 1]]], [0, [[18, ZERO2, 1]]]]

  """
  lob = [[v[0] for v in u] for u in ls]
  gob = [[v[0] for v in u] for u in global_ls]
  lobb = [u for u in flatten(lob)]
  gobb = [u for u in flatten(gob)]
  if not all(u in gobb for u in lobb):
    return

  cset = set()
  for l in ls:
    for p in l:
      cset = cset.union(p[1].free_symbols)

  gcset = set()
  for l in global_ls:
    for p in l:
      gcset = gcset.union(p[1].free_symbols)

  subs1 = {u: Symbol('ZERO' + u.name[1:]) for u in gcset}

  def make_key(v):
    return tuple(k[0] for k in v)

  ls_d = {make_key(v): v for v in ls}
  gls_d = {make_key(v): v for v in global_ls}
  global_ls_base_d = {make_key(v[1]): v for v in global_ls_base}

  new_ls = []
  base_add = []
  for g in gob:
    if tuple(g) in ls_d:
      new_ls.append(ls_d[tuple(g)])
    else:
      if all(u in lobb for u in g):
        for ls_k in ls_d:
          if all(u in ls_k for u in g):
            new_ls.append([v for v in ls_d[ls_k] if v[0] in g])
      elif enlarge and all(u not in lobb for u in g):
        gentry = gls_d[tuple(g)]
        gentry = [[u[0], u[1].xreplace(subs1)] for u in gentry]
        new_ls.append(gentry)
        _, gb_v = global_ls_base_d[tuple(g)]

        gb_v = [[v[0], v[1].xreplace(subs1), v[2]] for v in gb_v]

        base_add.append([0, gb_v])

  new_ls = sorted(new_ls, key=lambda x: x[0][0])

  # cancel if base ids do not match and enlarging is not allowed
  nlob = [[v[0] for v in u] for u in new_ls]
  if nlob != gob and not enlarge:
    return

  new_only_ids = sorted(v for v in flatten(nlob))
  old_only_ids = sorted(lobb)
  if any(u not in new_only_ids for u in old_only_ids):
    return

  # check that the structure can truely be mapped to global_ls taking into
  # account relative factors
  scp = simple_coupling_poly
  if enlarge and any(scp([u[1] for u in bnl]) !=
                     scp([u[1] for u in global_ls[bpos]])
                     for bpos, bnl in enumerate(new_ls)):
    return

  new_bases = []
  for base in ls_base:
    base_ids = [k[0] for k in base[1]]
    # no split required
    if base_ids in gob:
      new_bases.append(base)
    else:
      while len(base_ids) > 0:
        first_elem = base_ids[0]
        for g in gob:
          if first_elem in g:
            base_split = [base[0], [u for u in base[1]
                          if u[0] in g]]
            new_bases.append(base_split)
            for u in g:
              try:
                base_ids.remove(u)
              except Exception:
                print('args in:')
                print(ls)
                print(ls_base)
                print(global_ls)
                print(global_ls_base)
                raise

  new_bases.extend(base_add)
  return new_ls, new_bases


def join_structures(lco, simple_structures=False):
  """ Combining same lorentz structures together and forming new couplings.

  :lco: List of lorentz structures, each list entry is associated to another
        coupling, e.g. the 1.(2. 3. ..) element is multiplied with c0(c1, c2 ..)

  >>> GammaM = Symbol('GammaM'); GammaP = Symbol('GammaP')
  >>> lco = [[(3*GammaM, (0, 2))], [(3*GammaP, (0, -2)), (3*GammaM, (0, 4))]]


  Mode 1, making the couplings simple
  >>> join_structures(lco)
  {3*GammaM: [[0, c0 + 2*c1, 2]], 3*GammaP: [[0, c1, -2]]}

  Mode 2, making the structures simple (defined by prefactor=1)
  >>> join_structures(lco, simple_structures=True)
  {3*GammaM: [[0, 2*c0 + 4*c1, 1]], 3*GammaP: [[0, -2*c1, 1]]}
  """
  def coupling(i):
    return Symbol('c' + str(i))

  struc_ordered = {}

  for c_index, l_base in enumerate(lco):
    for struc in l_base:
      # structure alrdy registered -> have to add new couplings to that struc
      if struc[0] in struc_ordered:
        # argument id
        vals = [u[0] for u in struc_ordered[struc[0]]]
        # the same argument for the same lorentz structure
        if struc[1][0] in vals:
          index = vals.index(struc[1][0])
          # Retrieve prefactor by multiplying coupling with the intrinsic
          # prefactor of stored ls_struc

          # tmp is the coupling*prefactor of the structure already stored in
          # struc_ordered
          tmp = (struc_ordered[struc[0]][index][1] *  # coupling
                 struc_ordered[struc[0]][index][2])   # prefactor

          # by adding the new structure, the coupling*prefactor is given by
          # c_times_p
          c_times_p = tmp + coupling(c_index) * struc[1][1]
          if simple_structures:
            # assign couplings
            struc_ordered[struc[0]][index][1] = c_times_p
            # assign factor
            struc_ordered[struc[0]][index][2] = 1
          else:
            # try to separate couplings from a global prefactor
            couplings, factor = extract_couplings(tmp + coupling(c_index) *
                                                  struc[1][1])

            # assign couplings
            struc_ordered[struc[0]][index][1] = couplings
            # assign factor
            struc_ordered[struc[0]][index][2] = factor

        else:
          if simple_structures and struc[1][1] != 1:
            struc_ordered[struc[0]].append([struc[1][0],
                                            coupling(c_index) * struc[1][1],
                                            1])
          else:
            struc_ordered[struc[0]].append([struc[1][0], coupling(c_index),
                                            struc[1][1]])

      else:
        if simple_structures and struc[1][1] != 1:
          struc_ordered[struc[0]] = [[struc[1][0],
                                      struc[1][1] * coupling(c_index), 1]]
        else:
          struc_ordered[struc[0]] = [[struc[1][0], coupling(c_index),
                                      struc[1][1]]]

  return struc_ordered

#==============================================================================#
#                                  insert_ls                                   #
#==============================================================================#


cache_test = {}
bases_global = {}
ls_global = {}


def insert_ls(lorentz_dict, lorentz_coupling_ordered, n_lorentz,
              simple_structures=True, clear_cache=False,
              forbid_new_bases=False, **kwargs):
  """ The function inserts a new lorentz structure into the `lorentz_dict`.  It
  is checked whether the structure is new or if it is related to another known
  lorentz structure.

  :lorentz_dict: carrying lists of base elements (lorentz_base_dict) accessible
                 via lorentz keys (see combinatorics.py)

  :lorentz_coupling_ordered: The to-be inserted into the lorentz_dict coupling
                             ordered lorentz structure


  :c: couplings associated to the base elements in lorentz_coupling_ordered

  n_lorentz: the index of the latest NEW lorentz structure. If the next lorentz
             structure is determined to be new ( i.e. not related to alrdy
             included ones) then n_lorentz is increased by one.

  :simple_structures: Makes the structures simple and prefactors are multiplied
                      with the couplings instead of structures.




  :return:  lorentz_dict, lorentz_id, couplings_and_signs, n_lorentz,

  lorentz_dict is the possibly updated,
  lorentz_id is the unique id of the lorentz_structure,
  couplings_and_signs are the couplings and signs needed to compute the current,
  n_lorentz is the possibly updated n_lorentz

  >>> GammaM = Symbol('GammaM'); GammaP = Symbol('GammaP')
  >>> lco = [[(3*GammaM, (0, 1))], [(3*GammaP, (0, -2)), (3*GammaM, (0, 1))]]

  >>> c0 = Symbol('c0'); c1 = Symbol('c1')
  >>> c = [c0, c1]; nlorentz = 0
  >>> ls, lid, couplings_and_signs, nlorentz = insert_ls({}, lco, nlorentz,\
                                                         simple_structures=True)

  The updated lorentz dict is given by ls
  >>> ls == {3*GammaM: [[2, [[0, c1, 1]]]], 3*GammaP: [[1, [[0, c0, 1]]]]}
  True

  The routine tells us that the total number of different lorentz bases is
  >>> nlorentz
  2

  The current can be computed via the basis
  >>> lid
  [[3*GammaP, 1], [3*GammaM, 2]]

  and the couplings have to be multiplied by
  >>> {1: -2} in couplings_and_signs
  True
  >>> {0: 1, 1: 1} in couplings_and_signs
  True

  We can have a similar structure which is mapped on the same basis by the
  system
  >>> lco = [[(3*GammaM, (0, 1))], [(3*GammaP, (0, -2))]]
  >>> ls, lid, couplings_and_signs, nlorentz = insert_ls(ls, lco, nlorentz)
  >>> lid
  [[3*GammaP, 1], [3*GammaM, 2]]

  and only the couplings change
  >>> couplings_and_signs
  [{1: -2}, {0: 1}]

  the numbder of different lornetz structures stays the same
  >>> nlorentz
  2

  >>> g = Symbol('Metric')
  >>> lco = [[(4*g**2, (2, -1)), (4*g**2, (1, 2)), (4*g**2, (0, -1))]]
  >>> ls, lid, couplings_and_signs, nlorentz = insert_ls(ls, lco, nlorentz)
  >>> lid
  [[4*Metric**2, 3]]


  >>> lco = [[(4*g**2, (2, -1)), (4*g**2, (1, 2)), (4*g**2, (0, -1))], \
             [(4*g**2, (2, -1)), (4*g**2, (1, 2)), (4*g**2, (0, -1))]]
  >>> ls, lid, couplings_and_signs, nlorentz = insert_ls(ls, lco, nlorentz)
  >>> lid
  [[4*Metric**2, 3]]
  """
  # The lorentz_base_ordered then has the following structure: The keys are just
  # the products of lorentz structures times the number of participating
  # particles, e.g. 3*P*g, where P is a momentum vector and g is the metric
  # tensor.  The value assigned to such a key is an array. This array contains
  # other arrays describing how to construct the lorentz structure defined by
  # certain base elements. E.g. [1, c0, 2], [2, c1, -3] means to compute the
  # first (1) base element times 2, with the coupling c0 and add the second (2)
  # base element times -3 and times the c1 coupling. The separation of coupling
  # and multiplicative factor is done to be able to optimize easier. Given two
  # lorentz structures one can check whether the first and third entries match.
  # If so, the lorentz structures must not be computed twice, but only, in case,
  # the couplings have to be adapted.

  if clear_cache:
    global cache_test
    cache_test = {}
    global bases_global
    bases_global = {}
    global ls_global
    ls_global = {}
    # reconstruct bases_global & ls_globals from scratch for caching
    for key in lorentz_dict:
      global_ls = []
      n = len(lorentz_dict[key])
      bases_global_tmp, _ = compute_ls_coupling_grouped(lorentz_dict[key], [key] * n)
      bases_global[key] = bases_global_tmp
      vertex_ids = [u[0] for u in lorentz_dict[key]]
      for vertex_id in vertex_ids:
        offset = vertex_ids.index(vertex_id)
        global_ls.append([])
        for base in bases_global_tmp:
          if (base[0] == vertex_id):
            global_ls[offset].append(join_couplings_and_signs(base))

      # base elements are ordered according to the base element id b[0]
      for k in range(len(global_ls)):
        global_ls[k] = sort_joined_bases(global_ls[k],
                                         len(lorentz_base_dict[key]))
      ls_global[key] = global_ls

  lorentz_id = []
  couplings_and_signs = []

  lcokey = tuple(tuple(u) for u in lorentz_coupling_ordered)
  if lcokey in cache_test:
    lorentz_id, couplings_and_signs = cache_test[lcokey]
    return lorentz_dict, lorentz_id, couplings_and_signs, n_lorentz

  struc_joined = join_structures(lorentz_coupling_ordered)

  # sorting the base elements according to the lorentz base
  # element index
  ls_keys = struc_joined.keys()
  for key in ls_keys:
    struc_joined[key] = sorted(struc_joined[key], key=lambda x: x[0])

  # if there is more than 1 different base structure, then the order is defined
  # by a canonical ordering (See also `order_base_structure`)
  if len(ls_keys) > 1:
    keys = {}
    for lb in ls_keys:
      key, _ = get_lorentz_key_components(lb)
      key = tuple(order_strucs(key))
      weight = tuple(lorentz_base.order[u] for u in key)
      keys[lb] = weight
    ls_keys = sorted((u for u in keys), key=lambda x: keys[x])

  # compare result from lorentz_base_ordered with already computed
  # results stored in lorentz_dict
  #global_ls_keys = lorentz_dict.keys()
  for l_i, key in enumerate(ls_keys):
    # Yet, none of this class of lorentz structures computed
    if key not in lorentz_dict:
      # lorentz id increased by 1
      n_lorentz += 1
      # retrieve lorentz structure
      # ls = [ ls_struc_1, ls_struc_2, ...]
      # where ls_struc_i = [id_i, coupling_i, prefactor_i]

      ls = struc_joined[key]

      # perform simple structure optimization

      # TODO: (nick 2015-05-22) simple structures only defined on for single
      # structure, but not sums of structures -> not implemented yet
      simple_fac = 1
      if simple_structures and len(ls) == 1:
        # check if the structure is already simple
        # for a single structures this means
        # ls = [[base_id, coupling, prefactor]] where coupling must be simple,
        # i.e. c0 or c1 .., but not 2*c0 or c1/4 ... and prefactor = 1
        is_simple = (ls[0][1], ls[0][2]) == (Symbol('c' + str(l_i)), 1)
        if not is_simple:
          simple_fac = ls[0][1] * ls[0][2] / Symbol('c' + str(l_i))
          ls = [[ls[0][0], Symbol('c' + str(l_i)), 1]]

      # register in dictionary
      lorentz_dict[key] = [[n_lorentz, ls]]
      lorentz_id.append([key, n_lorentz])

      new_base_group, _ = group_base([n_lorentz, ls], ls_keys)
      if key not in bases_global:
        bases_global[key] = []
      bases_global[key].extend(new_base_group)

      offset = 0
      ls_global[key] = [[]]
      slsg = sort_and_join_base(new_base_group, len(lorentz_base_dict[key]))
      ls_global[key][offset] = slsg

      # The following routine groups the strucs if they have the same
      # the coupling*prefactor.
      bases, _ = group_base([0, ls], ls_keys)
      # and the couplings are extracted
      couplings = [[b[1] for b in u] for u in join_base_couplings_and_signs(bases)]

      recola_couplings = []
      for k, c in enumerate(couplings):
        # Given a couplings separated ls_struc (k index) we choose the first
        # coupling ([0]). When simplifying the couplings we get rid of global
        # factors and signs via extract_couplings ()[0]
        # we translate it into the array coupling.
        # e.g. c1 - c2 = [ 0, 1, -1, 0, 0, .... ]
        if simple_structures:
          recola_couplings.append(coeff_as_dict(
                                  simple_fac * extract_couplings(c[0])[0]))
        else:
          recola_couplings.append(coeff_as_dict(
                                  extract_couplings(c[0])[0]))
      couplings_and_signs.extend(recola_couplings)

    else:
      # goal: we need to separate different vertices and to collect base
      # elements corresponding to the same coupling. The separation of the
      # coupling is achieved via compute_ls_coupling_grouped, but the
      # function does not separate different vertex ids. This is done in the
      # following
      ls = struc_joined[key]
      bases, _ = group_base([0, ls], ls_keys)
      vertex_ids = [u[0] for u in lorentz_dict[key]]

      # base elements are ordered according to the base element id b[0], for
      # different couplings
      # e.g. [[[2, c1], [10, -c1]], [[3, c0], [7, -c0]], [[4, c2], [8, -c2]],..]

      # Now as we add the couplings up, we need to reorder again
      # [2, c1], [10, -c1], [4, c2] -> [2, c1], [4, c2], [10, -c1]

      ls = sort_and_join_base(bases, len(lorentz_base_dict[key]))

      # n number of vertices for key
      n = len(lorentz_dict[key])
      if key in bases_global:
        bases_global_tmp = bases_global[key]
      else:
        bases_global_tmp = []

      # check if ls is already in global_ls
      global_ls = ls_global[key]

      #first attempt: exactly the same structure calculated?
      if ls not in global_ls:
        # try to check if only the base elements match, without
        # coupling/prefactors
        ls_only_base = sorted([[b[0] for b in u] for u in ls],
                              key=lambda x: x[0])
        global_ls_only_base = []
        for vertex_id in vertex_ids:
          global_ls_only_base.append([])
          offset = vertex_ids.index(vertex_id)
          global_ls_only_base[offset] = [[v[0] for v in u] for u in
                                         global_ls[offset]]

        # If not exactly the same structure is initialized, there
        # is still the chance that the basis elements are the same
        # i.e. they would only differ by a multiplicative factor
        if ls_only_base not in global_ls_only_base:
          mapped_to_global = False
          for gls_pos, gls in enumerate(global_ls):
            vertex_id = vertex_ids[gls_pos]
            bases_global_v = [u for u in bases_global[key]
                              if u[0] == vertex_id]
            split = map_global(ls, bases, gls, bases_global_v,
                               enlarge=forbid_new_bases)
            if split:
              ls, bases = split
              ls_only_base = [[v[0] for v in u] for u in ls]
              mapped_to_global = True
              break
            else:
              continue
        else:
          mapped_to_global = True

        if mapped_to_global:
          g_ids = [i for i in range(len(global_ls_only_base))
                   if global_ls_only_base[i] == ls_only_base]
          for g_id in g_ids:
            # we compute the quotient of the first elements,
            # then we compute how ls_only_base changes with this
            # relative factor -> if ls_only_base matches one of the global
            # ones we have found a solution with a relative factor
            fac = []
            for k in range(len(global_ls[g_id])):
              ls_fac = sympify(ls[k][0][1])
              gls_fac = global_ls[g_id][k][0][1]
              fac.append(gls_fac / ls_fac)

            ls_new = sort_and_join_base(bases, len(lorentz_base_dict[key]))

            # apply the relative factor to ls
            # k loop runs over different couplings
            for k in range(len(ls_new)):
              # m loop runs over same couplings but different base elements
              for m in range(len(ls_new[k])):
                # [0] is the base id and [1] is the associated coupling
                ls_new[k][m][1] = sympy_factor(ls_new[k][m][1] * fac[k])

            # found a match and retrieve the id
            if ls_new in global_ls:
              # get the index of the specific vertex
              index = global_ls.index(ls_new)

              global_lbs = lorentz_dict[key][index][1]
              global_lbs_grouped, _ = group_base([0, global_lbs], ls_keys)

              # the structure is then simplified
              global_lbs = list(join_base_couplings_and_signs(global_lbs_grouped))
              permutation = compute_permutation_from_elements(global_ls[index], global_lbs)

              # and the couplings are extracted
              couplings = [[b[1] for b in u] for u in global_lbs]
              fac = permute_spins(fac, permutation)

              recola_couplings = []
              for k in range(len(couplings)):
                new_coupling = extract_couplings(couplings[k][0])[0] / sympify(fac[k])
                recola_couplings.append(coeff_as_dict(new_coupling))

              couplings_and_signs.extend(recola_couplings)

              # retrieve ls id
              lorentz_id.append([key, lorentz_dict[key][index][0]])

              break  # breaking the g_ids loop

            # no match found and we have to init a new ls
            # structure
            elif g_id == g_ids[-1]:
              if forbid_new_bases:
                print("g_id:", g_id)
                print("g_ids:", g_ids)
                print('No match not found in globals:')
                print("ls:", ls)
                print("ls_new:", ls_new)
                print("global_ls:", global_ls)
                print("lorentz_coupling_ordered:", lorentz_coupling_ordered)
                raise BaseMismatch(ls_only_base, global_ls_only_base,
                                   global_ls, ls_global[key])
              n_lorentz += 1

              # update lorentz_dict
              ls = struc_joined[key]
              simple_fac = 1
              if simple_structures and len(ls) == 1:
                is_simple = (ls[0][1], ls[0][2]) == (Symbol('c' + str(l_i)), 1)
                if not is_simple:
                  simple_fac = ls[0][1] * ls[0][2] / Symbol('c' + str(l_i))
                  ls = [[ls[0][0], Symbol('c' + str(l_i)), 1]]

              lorentz_dict[key].append([n_lorentz, ls])
              new_base_group, _ = group_base([n_lorentz, ls], ls_keys)
              if key not in bases_global:
                bases_global[key] = []
              bases_global[key].extend(new_base_group)

              offset = len(lorentz_dict[key]) - 1
              assert(len(ls_global[key]) == offset)
              slsg = sort_and_join_base(new_base_group,
                                        len(lorentz_base_dict[key]))
              ls_global[key].append(slsg)

              # group couplings
              bases, _ = group_base([0, ls], ls_keys)

              # the structure is then simplified
              ls = join_base_couplings_and_signs(bases)

              # and the couplings are extracted
              couplings = [[b[1] for b in u] for u in ls]

              recola_couplings = []
              for k in range(len(couplings)):
                new_coupling = simple_fac * extract_couplings(couplings[k][0])[0]
                recola_couplings.append(coeff_as_dict(new_coupling))

              couplings_and_signs.extend(recola_couplings)

              # register new ls id
              lorentz_id.append([key, n_lorentz])

        # no other ls with the same basis elements
        # -> must be different
        else:
          if forbid_new_bases:
            raise BaseMismatch(ls_only_base, global_ls_only_base,
                               global_ls, ls_global[key])
          n_lorentz += 1

          # update lorentz_dict
          ls = struc_joined[key]
          simple_fac = 1
          if simple_structures and len(ls) == 1:
            is_simple = (ls[0][1], ls[0][2]) == (Symbol('c' + str(l_i)), 1)
            if not is_simple:
              simple_fac = ls[0][1] * ls[0][2] / Symbol('c' + str(l_i))
              ls = [[ls[0][0], Symbol('c' + str(l_i)), 1]]

          lorentz_dict[key].append([n_lorentz, ls])
          new_base_group, _ = group_base([n_lorentz, ls], ls_keys)
          if key not in bases_global:
            bases_global[key] = []
          bases_global[key].extend(new_base_group)

          offset = len(lorentz_dict[key]) - 1
          assert(len(ls_global[key]) == offset)
          slsg = sort_and_join_base(new_base_group,
                                    len(lorentz_base_dict[key]))
          ls_global[key].append(slsg)

          # group couplings
          bases, _ = group_base([0, ls], ls_keys)

          # the structure is then simplified
          ls = join_base_couplings_and_signs(bases)

          # and the couplings are extracted
          couplings = [[b[1] for b in u] for u in ls]

          recola_couplings = []
          for k in range(len(couplings)):
            new_coupling = simple_fac * extract_couplings(couplings[k][0])[0]
            recola_couplings.append(coeff_as_dict(new_coupling))

          couplings_and_signs.extend(recola_couplings)

          # register new ls id
          lorentz_id.append([key, n_lorentz])

      # ls structure is included and we retrieve the id
      else:
        index = global_ls.index(ls)
        # previous ls has been manipulated for comparison with lorentz_dict.
        # Simplest way to continue is to simply compute ls again
        ls = lorentz_dict[key][index][1]

        # group couplings
        bases, _ = group_base([0, ls], ls_keys)

        # the structure is then simplified
        ls = join_base_couplings_and_signs(bases)
        # and the couplings are extracted
        couplings = [[b[1] for b in u] for u in ls]

        recola_couplings = []
        for k in range(len(couplings)):
          new_coupling = extract_couplings(couplings[k][0])[0]
          recola_couplings.append(coeff_as_dict(new_coupling))

        couplings_and_signs.extend(recola_couplings)

        lorentz_id.append([key, lorentz_dict[key][index][0]])

  cache_test[lcokey] = (lorentz_id, couplings_and_signs)
  return lorentz_dict, lorentz_id, couplings_and_signs, n_lorentz


if __name__ == "__main__":
  import doctest
  doctest.testmod()
