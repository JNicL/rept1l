################################################################################
#                              auto_build_base.py                              #
################################################################################
from __future__ import print_function

from rept1l.baseutils import CurrentBase, insert_ls
from rept1l.baseutils.base_utils import lbs_to_form_expressions
from rept1l.vertex import Vertex
from rept1l.combinatorics import gen_permutations
from collections import OrderedDict as OD


def get_minimal_perms(legs, lorentz):
  """ Returns irreducible permutations of the lorentz structure given number of
  legs (length of permutation).

  >>> lorentz = ['-Metric(1,4)*Metric(2,3)+2*Metric(1,3)*Metric(2,4) - Metric(1,2)*Metric(3,4)']
  >>> get_minimal_perms(4, lorentz)
  [[0, 1, 2, 3], [0, 1, 3, 2], [0, 2, 1, 3]]
  """

  permutations = gen_permutations(range(legs))
  perms = []
  dealt_with = []
  for permutation in permutations:
    lco = []
    for ls in lorentz:
      lbs = CurrentBase(ls).compute(permutation)
      lco += [[tuple(u) for u in lbs]]

    (Vertex.current_strucs, vbase, _,
     Vertex.n_lorentz) = insert_ls(Vertex.current_strucs,
                                   lco, Vertex.n_lorentz,
                                   simple_structures=True)
    ids = tuple(u[1] for u in vbase)
    if ids not in dealt_with:
      dealt_with.append(ids)
      perms.append(permutation)
    else:
      continue
  return perms


def auto_default_base(legs, lorentz, idpattern='', verbose=False):
  """ Derives the minimal permutations for the lorentz structure and derives
  default permutations.

  >>> lorentz = ['Metric(1,4)*Metric(2,3)', 'Metric(1,3)*Metric(2,4)', \
                 'Metric(1,2)*Metric(3,4)']
  >>> res = auto_default_base(6, lorentz)
  >>> [u[1] for u in res.values()].count(True)
  4
  >>> [u[1] for u in res.values()].count(False)
  11
  """
  permutations = get_minimal_perms(legs, lorentz)
  Vertex.n_lorentz = 0
  Vertex.current_strucs = {}

  res = OD()
  for pos, permutation in enumerate(permutations):
    lco_cmp = []
    for ls in lorentz:
      lbs = CurrentBase(ls).compute(permutation)
      lco_cmp += [lbs]
    ssum = []
    for lbs in lco_cmp:
      ssum += lbs_to_form_expressions(lbs)
    if verbose:
      print("lco:", ' , '.join(ssum))

    tag = idpattern + str(pos)
    try:
      CurrentBase.get_default_permutation(lco_cmp, stop_on_failure=True)
      res.update(OD([(tuple(permutation), (tag, False))]))
    except Exception:
      for ls in lorentz:
        del CurrentBase.cache_base_structure[ls]
        CurrentBase(ls).compute(permutation, is_defaultp=True)
      res.update(OD([(tuple(permutation), (tag, True))]))

  return res


if __name__ == "__main__":
  import doctest
  doctest.testmod()

  # lorentz = ['Metric(1,4)*Metric(2,3)', 'Metric(1,3)*Metric(2,4)',
  #            'Metric(1,2)*Metric(3,4)']
  # lorentz1 = 'P(1,2)*P(2,1)'
  # lorentz2 = 'P(-1,1)*P(-1,2)*Metric(1,2)'
  # lorentz = [lorentz1, lorentz2]
  # res = auto_default_base(3, lorentz, idpattern='VVS3_heft_')
  #
  # for k, v in res.iteritems():
  #   print('(' + str(k) + ', ' + str(v) + '),')

  # from sympy import Symbol
  # Metric = Symbol('Metric')
  # lco = [((6 * Metric**2, (24, -1)), (6 * Metric**2, (25, -1)), (6 * Metric**2, (26, 2)))]
  # ssum = []
  # for lbs in lco:
  #   ssum += lbs_to_form_expressions(lbs)
  # print("lco:", ' , '.join(ssum))
  #
  # match = CurrentBase.get_default_permutation(lco)
  # print("match:", match)
