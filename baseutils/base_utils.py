#==============================================================================#
#                                base_utils.py                                 #
#==============================================================================#
from __future__ import print_function

#============#
#  Includes  #
#============#

from six import iteritems
import sys
import re
import rept1l_config
try:
  from types import ListType
  list_type = ListType
except ImportError:
  list_type = list
from sympy import Symbol, Poly, nsimplify, sympify
from functools import reduce
from rept1l.combinatorics import lorentz_base_dict, fold, get_representative
from rept1l.combinatorics import LSBaseCache
from rept1l.combinatorics import get_lorentz_key_components, order_strucs
from rept1l.combinatorics import lorentz_base, permute_particles
from rept1l.logging_setup import log
from rept1l.combinatorics import flatten
from rept1l.combinatorics import P, g, Sigma, Gamma, GammaP, GammaM, Gamma5
from rept1l.combinatorics import ProjP, ProjM, Identity, Epsilon

#############
#  Globals  #
#############

warning_new_ls = True

#===========#
#  Methods  #
#===========#

def generate_form_expression(ls_key, arg):
  def p_frm(args, max_arg=0):
    sym = 'p'
    assert(len(args) == 2)
    lorentz_arg = args[0]
    if lorentz_arg < 0:
      lorentz_arg = -lorentz_arg + max_arg
      lorentz_sym = 'nu'
    else:
      lorentz_sym = 'mu'
    lorentz_arg = lorentz_sym + str(lorentz_arg)

    particle_arg = str(args[1])
    assert(int(particle_arg) > 0)

    return sym + particle_arg + '(' + lorentz_arg + ')'

  def g_frm(args, max_arg=0):
    sym = 'd_'
    assert(len(args) == 2)

    lorentz_arg1 = args[0]
    if lorentz_arg1 < 0:
      lorentz_arg1 = -lorentz_arg1 + max_arg
      lorentz_sym = 'nu'
    else:
      lorentz_sym = 'mu'
    lorentz_arg1 = lorentz_sym + str(lorentz_arg1)

    lorentz_arg2 = args[1]
    if lorentz_arg2 < 0:
      lorentz_arg2 = -lorentz_arg2 + max_arg
      lorentz_sym = 'nu'
    else:
      lorentz_sym = 'mu'
    lorentz_arg2 = lorentz_sym + str(lorentz_arg2)

    lorentz_args = ','.join([lorentz_arg1, lorentz_arg2])
    return sym + '(' + lorentz_args + ')'

  def eps_frm(args, max_arg=0):
    sym = 'e_'
    assert(len(args) == 4)

    lorentz_arg1 = args[0]
    if lorentz_arg1 < 0:
      lorentz_arg1 = -lorentz_arg1 + max_arg
      lorentz_sym = 'nu'
    else:
      lorentz_sym = 'mu'
    lorentz_arg1 = lorentz_sym + str(lorentz_arg1)

    lorentz_arg2 = args[1]
    if lorentz_arg2 < 0:
      lorentz_arg2 = -lorentz_arg2 + max_arg
      lorentz_sym = 'nu'
    else:
      lorentz_sym = 'mu'
    lorentz_arg2 = lorentz_sym + str(lorentz_arg2)

    lorentz_arg3 = args[2]
    if lorentz_arg3 < 0:
      lorentz_arg3 = -lorentz_arg3 + max_arg
      lorentz_sym = 'nu'
    else:
      lorentz_sym = 'mu'
    lorentz_arg3 = lorentz_sym + str(lorentz_arg3)

    lorentz_arg4 = args[3]
    if lorentz_arg4 < 0:
      lorentz_arg4 = -lorentz_arg4 + max_arg
      lorentz_sym = 'nu'
    else:
      lorentz_sym = 'mu'
    lorentz_arg4 = lorentz_sym + str(lorentz_arg4)

    lorentz_args = ','.join([lorentz_arg1, lorentz_arg2,
                             lorentz_arg3, lorentz_arg4])
    return sym + '(' + lorentz_args + ')'

  def ga_frm(args, max_arg=0, vb=True, proj=None):
    sym = 'ga'
    if vb:
      assert(len(args) == 3)
    else:
      assert(len(args) == 2)

    offset = 0
    if vb:
      lorentz_arg = args[0]
      if lorentz_arg < 0:
        lorentz_arg = -lorentz_arg + max_arg
        lorentz_sym = 'nu'
      else:
        lorentz_sym = 'mu'
      lorentz_arg = lorentz_sym + str(lorentz_arg)
    else:
      offset = 1

    fermion_arg = args[2-offset]
    if fermion_arg < 0:
      fermion_arg = -fermion_arg + max_arg
      fermion_sym = 'j'
    else:
      fermion_sym = 'i'
    fermion_arg = fermion_sym + str(fermion_arg)

    afermion_arg = args[1-offset]
    if afermion_arg < 0:
      afermion_arg = -afermion_arg + max_arg
      fermion_sym = 'j'
    else:
      fermion_sym = 'i'
    afermion_arg = fermion_sym + str(afermion_arg)

    if vb:
      lorentz_args = ','.join([afermion_arg, fermion_arg, lorentz_arg])
    else:
      lorentz_args = ','.join([afermion_arg, fermion_arg])
    if proj == '[+]' or proj == '[-]':
      return sym + '(' + lorentz_args + ',' + proj + ')'
    elif vb:
      return sym + '(' + lorentz_args + ')'
    elif proj == '5':
      return ('(' + sym + '(' + lorentz_args + ',[+]) - ' +
                    sym + '(' + lorentz_args + ',[-])' + ')')
    else:
      return ('(' + sym + '(' + lorentz_args + ',[+]) + ' +
              sym + '(' + lorentz_args + ',[-])' + ')')

  def sigma_frm(args, max_arg=0):
    sym = 'ga'
    assert(len(args) == 4)

    l1_arg = args[0]
    if l1_arg < 0:
      l1_arg = -l1_arg + max_arg
      lorentz_sym = 'nu'
    else:
      lorentz_sym = 'mu'
    l1_arg = lorentz_sym + str(l1_arg)

    l2_arg = args[1]
    if l2_arg < 0:
      l2_arg = -l2_arg + max_arg
      lorentz_sym = 'nu'
    else:
      lorentz_sym = 'mu'
    l2_arg = lorentz_sym + str(l2_arg)

    f_arg = args[3]
    if f_arg < 0:
      f_arg = -f_arg + max_arg
      fermion_sym = 'j'
    else:
      fermion_sym = 'i'
    f_arg = fermion_sym + str(f_arg)

    af_arg = args[2]
    if af_arg < 0:
      af_arg = -af_arg + max_arg
      fermion_sym = 'j'
    else:
      fermion_sym = 'i'
    af_arg = fermion_sym + str(af_arg)

    args1 = ','.join([af_arg, f_arg, l1_arg, l2_arg])

    sym = 'si'
    return (sym + '(' + args1 + ')')

  form_dict = {P: p_frm,
               g: g_frm,
               Epsilon: eps_frm,
               Sigma: sigma_frm,
               Gamma: ga_frm,
               GammaP: lambda *args: ga_frm(*args, proj='[+]'),
               GammaM: lambda *args: ga_frm(*args, proj='[-]'),
               ProjP: lambda *args: ga_frm(*args, vb=False, proj='[+]'),
               ProjM: lambda *args: ga_frm(*args, vb=False, proj='[-]'),
               Identity: lambda *args: ga_frm(*args, vb=False),
               Gamma5: lambda *args: ga_frm(*args, vb=False, proj='5'),
               }
  if len(arg) > 0:
    max_arg = max([u for u in flatten(arg)])
    frm_str = '*'.join([form_dict[ls](arg[i], max_arg)
                       for i, ls in enumerate(ls_key)])
  else:
    frm_str = '1'
  return frm_str


def lbs_to_form_expressions(lbs):
  s_sum = []
  for lb in lbs:
    ls_key, _ = get_lorentz_key_components(lb[0])
    ls_key = order_strucs(ls_key)
    args = lorentz_base_dict[lb[0]]
    # must be scalar
    if args == []:
      s_frm = '1'
    else:
      arg = lorentz_base_dict[lb[0]][lb[1][0]]
      s_frm = generate_form_expression(ls_key, arg)
    prefac = lb[1][1]
    if prefac != 1:
      if prefac > 1:
        s_sum.append(str(prefac) + '*' + s_frm)
      else:
        s_sum.append('(' + str(prefac) + ')*' + s_frm)
    else:
      s_sum.append(s_frm)
  return s_sum

#==============================================================================#
#                                shift_indices                                 #
#==============================================================================#

def shift_indices(arg, permutation):
  """ Transforms an integer list according to a permutation prescription.

  :param arg: Integer list of the form [a1, a2, ...] or[[a1, a2 ...],[ ... ],..]
  :type  arg: list (of list) of ints

  For a given permutation, the prescription reads:
    ai -> permutation[a_i -1] + 1,
  where the -1/+1 is are offsets, because the lowest element in
  permutations is 0 (by convention), and for arguments it is 1.

  Example:

  Neutral element
  >>> shift_indices([3, 2, 1], [0, 1, 2])
  [3, 2, 1]

  Exchange 1 <-> 2
  >>> shift_indices([3, 2, 1], [1, 0, 2])
  [3, 1, 2]
  """
  tmp = arg
  for i in range(len(arg)):
    if (isinstance(arg[i], list_type)):
      for j in range(len(arg[i])):
        if(arg[i][j] > -1):
          tmp[i][j] = permutation[arg[i][j]-1]+1
        else:
          tmp[i][j] = arg[i][j]
    else:
      if arg[i] is None:
        tmp[i] = None
      elif arg[i] > -1:
        tmp[i] = permutation[arg[i]-1]+1
      else:
        tmp[i] = arg[i]
  return tmp

#==============================================================================#
#                              separate_sum_struc                              #
#==============================================================================#

def separate_sum_struc(struc, arg, con):
  """ separate_sum_struc takes the result from get_structure and separates the
  multiplicatively connected structures from the additively connected
  substructures.
  The resulting arrays have the following structures

  get_structure: a*b*c+d*e ->  [a,b,c,d,e] ,[*,*,+,*]

  spearate_sum_struc: [a,b,c,d,e], [*,*,+,*] -> [[a,b],[c.d]], [[*,*],[*]], [+]
  """
  strucs = []
  args = []
  cons = []
  adds = []
  tmp_struc = []
  tmp_arg = []
  tmp_con = []
  for i in range(len(con)):
    tmp_struc.append(struc[i])
    tmp_arg.append(arg[i])
    if(con[i] != '+' and con[i] != '-'):
      tmp_con.append(con[i])

    else:
      strucs.append(tmp_struc)
      args.append(tmp_arg)
      cons.append(tmp_con)
      adds.append(con[i])
      tmp_struc = []
      tmp_arg = []
      tmp_con = []

  tmp_struc.append(struc[len(struc)-1])
  tmp_arg.append(arg[len(arg)-1])
  strucs.append(tmp_struc)
  args.append(tmp_arg)
  cons.append(tmp_con)

  return strucs, args, cons, adds

#------------------------------------------------------------------------------#

def string_subs(string, dict_repl):
  if len(dict_repl) > 0:
    p = (re.compile('|'.join(re.escape(key)
         for key in dict_repl.keys()), re.S))
    return p.sub(lambda x: dict_repl[x.group()], string)
  else:
    return string

def Lorentz(i=-1):
  while True:
    i += 1
    yield "L" + str(i)

#------------------------------------------------------------------------------#

class LorentzPattern(object):

  # Import all structures from the lorentz base
  strucs = [u.symbol.name for u in lorentz_base.bases]
  lorentz_pat = r'|'.join(r'((' + u + ')\(((?:-?\d+?,?)+?)\))' for u in strucs)
  lorentz_pat = re.compile(lorentz_pat)

class CurrentBase(LorentzPattern):
  """ Class method for deriving base structures given

  - lorentz string in UFO convention

  - lorentz sympy expression

  The method :py:meth:`lorentz_rc.CurrentBase.compute` parses the
  lorentz string and turns into a lorentz base structure which is composed of a
  list of lorentz symbols, structure ids and prefactors.  Alternatively
  :py:meth:`lorentz_rc.CurrentBase.compute2` does basically the same
  given a lorentz sympy expression and lorentz_dict, but returns a lorentz base
  structure dict.

  Usage:

  Method one - lorentz string.

  >>> lorentz = 'Gamma(3,2,1)'
  >>> permutation = [0, 1, 2]
  >>> dbs = CurrentBase(lorentz)
  >>> dbs.compute(permutation)
  ((3*GammaP, (5, 1)), (3*GammaM, (5, 1)))

  >>> lorentz = 'P(-1,1)**2*Metric(1,2) + Metric(1,2)'
  >>> permutation = [0, 1]
  >>> dbs = CurrentBase(lorentz)
  >>> dbs.compute(permutation)
  ((2*Metric*P**2, (0, 1)), (2*Metric, (0, 1)))

  Method two - lorentz sympy expression.

  >>> c = [Symbol('C0')]
  >>> l = [Symbol('L0'), Symbol('L1')]
  >>> lorentz =  3*c[0]**2*l[0]*l[1] - c[0]**2*l[1]
  >>> P = Symbol('P')
  >>> g = Symbol('Metric')
  >>> lsd =  {'L0': ([P, P], [[-1, 1], [-1, 1]]), 'L1': ([g], [[1, 2]])}
  >>> dbs = CurrentBase(lorentz)
  >>> particles = 2
  >>> dbs.compute2(lsd, particles)
  {(2*Metric*P**2, (0, 1)): 3*C0**2, (2*Metric, (0, 1)): -C0**2}

  """
  new_base = dict()
  Ga = Symbol('Gamma')
  GaP = Symbol('GammaP')
  GaM = Symbol('GammaM')
  Id = Symbol('Identity')
  Ga5 = Symbol('Gamma5')
  PP = Symbol('ProjP')
  PM = Symbol('ProjM')
  new_base[3*Ga] = [(3*GaM, 1), (3*GaP, 1)]
  #new_base[4*Ga*Ga] = [(4*GaM*GaM, 1), (4*GaM*GaP, 1), (4*GaP*GaP, 1)]

  base_transf = {}
  # Gamma is written as Gamma^+ + Gamma^-
  base_transf[(Ga,)] = [([GaM], 1), ([GaP], 1)]

  # Identity is written as P^+ + P^-
  base_transf[(Id,)] = [([PP], 1), ([PM], 1)]
  # Ga5 is written as P^+ - P^-
  base_transf[(Ga5,)] = [([PP], 1), ([PM], -1)]

  # Gamma^2 is written as (Gamma^+ + Gamma^-)^2
  base_transf[(Ga, Ga)] = [([GaM, GaM], 1), ([GaM, GaP], 1),
                           ([GaP, GaM], 1), ([GaP, GaP], 1)]
  base_transf[(Ga, GaP)] = [([GaM, GaP], 1), ([GaP, GaP], 1)]
  base_transf[(Ga, GaM)] = [([GaM, GaM], 1), ([GaP, GaM], 1)]
  base_transf[(GaM, Ga)] = [([GaM, GaM], 1), ([GaM, GaP], 1)]
  base_transf[(GaP, Ga)] = [([GaP, GaM], 1), ([GaP, GaP], 1)]

  # Identity is written as (P^+ + P^-)^2
  base_transf[(Id, Id)] = [([PM, PM], 1), ([PM, PP], 1),
                           ([PP, PM], 1), ([PP, PP], 1)]

  # Identity is written as (P^+ + P^-)^2
  base_transf[(Ga5, Ga5)] = [([PM, PM], 1), ([PM, PP], -1),
                             ([PP, PM], -1), ([PP, PP], 1)]

  cache_form_transform = {}
  cache_base_structure = {}

  # default permutation for a base structure. Other permuations which leave the
  # type of the (first and) final field unchanged.
  cache_has_defaultp = {}
  cache_defaultp = {}

  def __init__(self, lorentz, debug=False):
    self.lorentz = lorentz
    self.debug = debug

  def compute(self, permutation, form_simplify=True, is_defaultp=False,
              check_defaultp=True):

    # check for cached result
    if self.lorentz in self.cache_base_structure:
      if self.debug:
        print('found lorentz in cache_base_structure')
      if tuple(permutation) in self.cache_base_structure[self.lorentz]:
        return self.cache_base_structure[self.lorentz][tuple(permutation)]

    matches = re.findall(self.lorentz_pat, self.lorentz)
    lorentz_dict = {}
    repl_dict = {}
    lgen = Lorentz()
    for match in matches:
      match = [u for u in match if u != '']
      assert(len(match) == 3)
      lstr, lsym, larg = match
      larg = shift_indices([int(u) for u in larg.split(',')], permutation)
      lorentz_dict[lstr] = (lsym, larg)
      repl_dict[lstr] = next(lgen)
    particles = len(permutation)

    lbase = [Symbol(u) for u in repl_dict.values()]

    # turns the lorentz string into a fully algebraic expression
    lorentz = self.parse_to_sympy(string_subs(self.lorentz, repl_dict))
    # simplify the expression and replace floats with rational numbers
    lorentz = nsimplify(lorentz, rational=True)

    lbase.append(sympify(1))
    # separate additively connection structures
    ls_poly = Poly(lorentz, lbase).as_dict()
    repl_dict_inv = {repl_dict[key]: key for key in repl_dict}
    base_structure = []

    # Each ls_key represents a product of lorentz structures
    # For instance, given g(1,2)*g(3,4) + g(1,3)*g(2,3)
    # we have two ls_keys, one for g(1,2)*g(3,4) and one for g(1,3)*g(2,3)
    for ls_key in ls_poly:
      prefactor = ls_poly[ls_key]
      i_key = [i for i in range(len(ls_key)) if ls_key[i] > 0]
      ls_prod = []
      for u in i_key:
        ls_prod.extend([repl_dict_inv[lbase[u].name]] * ls_key[u])

      # lorentz base structure: list of symbols
      lbs = [Symbol(lorentz_dict[v][0]) for v in ls_prod]

      # list of ordered arguments. Arguments are connection to the base in lbs
      # with respect to their postion in the array, e.g. the third arg
      # grouped_args[2] corresponds to the third structure lbs[2]
      grouped_args = [lorentz_dict[v][1] for v in ls_prod]
      lbs, grouped_args = self.gamma_projector(lbs, grouped_args)

      l_key = particles
      for ls in lbs:
        l_key *= ls

      if len(lbs) > 0:
        arg_repr = get_representative(lbs, grouped_args)
        arg_id = self.get_arg_id(l_key, arg_repr)
      else:
        # Purely scalar case there are no arguments
        arg_id = 0
      base_structure.append([l_key, (arg_id, prefactor)])

    if len(lbs) > 0:
      base_structure = self.transform_lbs(base_structure)
      if form_simplify:
        base_structure = self.form_simplify_lbs(base_structure)
      base_structure = self.order_base_structure(base_structure)

    # hashable
    base_structure = tuple(tuple(u) for u in base_structure)

    # cache base structure
    if self.lorentz not in self.cache_base_structure:
      self.cache_base_structure[self.lorentz] = {}

    if tuple(permutation) not in self.cache_base_structure[self.lorentz]:
      # copy of base_structure essential
      self.cache_base_structure[self.lorentz][tuple(permutation)] = [u for u in base_structure]

    if self.debug:
      ssum = lbs_to_form_expressions(base_structure)
    if check_defaultp:
      self.register_default_permutation(permutation, base_structure, is_defaultp)
    return base_structure

  def default_permutation_matching_condition(self, bst, permutation):
    """ Check the condition for matching to default permutation. The
    condition reads:
    1. First field not permuted relative to default permutation
    2. Last field not permuted relative to default permutation
    (3. Permuted lorentz structure  must reproduce the defaults one)
    """
    nparticles = len(permutation)
    args_ref = [u for u in flatten(lorentz_base_dict[bst[0]][bst[1]])]

    invp = [0] * len(permutation)
    for upos, u in enumerate(permutation):
      invp[u] = upos

    for t in self.cache_has_defaultp[bst[0]]:
      bst_ref = t[1]
      perm_ref = t[0]
      args_cmp = [u for u in flatten(lorentz_base_dict[bst_ref[0]][bst_ref[1]])]

      # if nparticles not in args_ref and nparticles not in args_cmp:
      #   cond1 = True
      # elif nparticles in args_ref and nparticles in args_cmp:
      #   cond1 = args_ref.index(nparticles) == args_cmp.index(nparticles)
      # else:
      #   cond1 = False
      #
      # if 1 not in args_ref and 1 not in args_cmp:
      #   cond2 = True
      # elif 1 in args_ref and 1 in args_cmp:
      #   cond2 = args_ref.index(1) == args_cmp.index(1)
      # else:
      #   cond2 = False

      rel_perm = permute_particles(invp, perm_ref, shift=0)

      #  TODO:  <21-06-18, J.-N. Lang> #
      # I have some doubts about this condition
      cond1 = rel_perm[0] == 0
      cond2 = rel_perm[-1] == nparticles - 1

      cond3 = permute_particles(args_ref, rel_perm) == args_cmp

      if cond1 and cond2 and cond3:
        return perm_ref, bst_ref, rel_perm

  def register_default_permutation(self, permutation, base_structure, is_defaultp):
    """ Register default permutations for lorentz base structures, and link
    other permutations to default ones.
    """

    for bst in (tuple((u[0], u[1][0])) for u in base_structure):
      if bst[0] not in self.cache_has_defaultp:
        self.cache_has_defaultp[bst[0]] = []

      if is_defaultp:
        self.cache_has_defaultp[bst[0]].append((tuple(permutation), bst))

      elif len(self.cache_has_defaultp[bst[0]]) > 0:

        # Is default, no caching needed
        if any(u[1] == bst for u in self.cache_has_defaultp[bst[0]]):
          continue

        # retrieve matching permutation
        ref = self.default_permutation_matching_condition(bst, permutation)

        if ref:
          mp = ref[0]
          mbst = ref[1]
          rel_perm = ref[2]
          a = range(len(mp))
          a_orig = permute_particles(a, mp, shift=0)
          a_tmp = permute_particles(a, permutation, shift=0)
          a_tmp = permute_particles(a_tmp, rel_perm, shift=0)
          assert(a_orig == a_tmp)

          if bst[0] not in self.cache_defaultp:
            self.cache_defaultp[bst[0]] = {}
          self.cache_defaultp[bst[0]][bst] = (tuple(permutation), mp,
                                              rel_perm, mbst)

  @classmethod
  def get_default_permutation(cls, lco, stop_on_failure=False):
    """ Identies the equivalent lco for a default permutation.

    Assume we have the SM TGC structure, but extended by 2 scalars (VVVSS
    signature)
    >>> lorentz = ('P(3,1)*Metric(1,2) - P(3,2)*Metric(1,2) - P(2,1)*Metric(1,3) + ' +\
                   'P(2,3)*Metric(1,3) + P(1,2)*Metric(2,3) - P(1,3)*Metric(2,3)')

    We define the following permutation as a default one, corresponding to
    VVSSV:
    >>> permutation = (0, 1, 4, 2, 3)
    >>> lbs = CurrentBase(lorentz).compute(permutation, is_defaultp=True)

    The following two permutations can be mapped to the default one, since the
    first and last field are not permuted which would otherwise break the freedom of the
    1-loop Berends-Giele recursion.
    VSVSV:
    >>> permutation = (0, 2, 4, 1, 3)
    >>> lbs1 = CurrentBase(lorentz).compute(permutation)

    VSSVV:
    >>> permutation = (0, 3, 4, 1, 2)
    >>> lbs2 = CurrentBase(lorentz).compute(permutation)

    Now we check that indeed we get the correct default permutation
    >>> lco = [lbs1]
    >>> match = CurrentBase.get_default_permutation(lco)
    >>> lco_n, ini_p, ref_p, rel_p = match
    >>> assert(len(lco_n) == 1)
    >>> assert(lbs in lco_n)
    >>> rel_p
    [0, 2, 1, 3, 4]

    >>> lco = [lbs2]
    >>> match = CurrentBase.get_default_permutation(lco)
    >>> lco_n, ini_p, ref_p, rel_p = match
    >>> assert(len(lco_n) == 1)
    >>> assert(lbs in lco_n)
    >>> rel_p
    [0, 2, 3, 1, 4]
    """

    # matching lco, equivalent lco
    lco_n = []
    # relative permutation, must match for all elements in lco_n
    rel_p = None

    check = True
    for lbs in lco:
      lbs_n = []
      for rst in lbs:
        bst = (rst[0], rst[1][0])
        if rst[0] in cls.cache_defaultp and (bst in cls.cache_defaultp[bst[0]]):
          if rel_p:
            # relative permutation already assigned, check if consistent
            if rel_p != cls.cache_defaultp[bst[0]][bst][2]:
              log('Mismatch in relative permutation for: ', 'debug')
              log('bst:' + str(bst), 'debug')
              check = False
              break
          else:
            # retrieve relative permutation
            ini_p = cls.cache_defaultp[bst[0]][bst][0]
            ref_p = cls.cache_defaultp[bst[0]][bst][1]
            rel_p = cls.cache_defaultp[bst[0]][bst][2]

          bst_ref = cls.cache_defaultp[bst[0]][bst][3]
          rst_ref = (bst_ref[0], (bst_ref[1], rst[1][1]))
          lbs_n.append(rst_ref)
        else:
          check = False
          break
      lco_n.append(tuple(lbs_n))

    if check:
      return tuple(lco_n), ini_p, ref_p, rel_p
    else:
      # check failed, last chance the structure is default permutation
      # use this only to debug.
      if stop_on_failure:
        is_default = True
        for lbs in lco:
          for rst in lbs:
            if rst[0] in cls.cache_has_defaultp:
              bst = (rst[0], rst[1][0])
              if not any(u[1] == bst for u in cls.cache_has_defaultp[bst[0]]):
                is_default = False
                break

        if not is_default:

          ssum = []
          for lbs in lco:
            ssum += lbs_to_form_expressions(lbs)
          log('Default base not found.', 'debug')
          log('Structure: ' + ', '.join(ssum), 'debug')
          raise Exception("Default base not found.")

  @staticmethod
  def get_arg_id(l_key, arg_repr, forbid_newls=False):
    if l_key not in lorentz_base_dict:
      if forbid_newls:
        log('New lorentz structure: `' + str(l_key) + '` encountered.',
            'error')
        sys.exit()
      if warning_new_ls:
        log('New lorentz structure: `' + str(l_key) + '` registered.',
            'warning')
      lorentz_base_dict[l_key] = []
    if arg_repr not in lorentz_base_dict[l_key]:
      if forbid_newls:
        log('New element for lorentz structure: `' + str(l_key) +
            '` encountered. Args: `' + str(arg_repr) + '`', 'error')
        sys.exit()
      if warning_new_ls:
        log('New element for lorentz structure: `' + str(l_key) +
            '` registered. Args: `' + str(arg_repr) + '`', 'warning')
      lorentz_base_dict[l_key].append(arg_repr)
      LSBaseCache.add_item(l_key, arg_repr)
      LSBaseCache.store()

    return lorentz_base_dict[l_key].index(arg_repr)

  def compute2(self, ls_dict, particles, forbid_newls=False):

    try:
      # purely scalar case
      if len(ls_dict) == 0:
        return {(particles, (0, 1)): self.lorentz}
      lbase = [Symbol(u) for u in ls_dict.keys()]

      # turns the lorentz string into a fully algebraic expression
      lorentz = self.lorentz
      lbase = [u for u in lorentz.free_symbols if u in lbase]

      if lbase == []:
        return {(particles, (0, 1)): lorentz}

      # separate additively connection structures
      ls_poly = Poly(lorentz, lbase).as_dict()
      base_structure = {}

      # Each ls_key represents a product of lorentz structures
      # For instance, given g(1,2)*g(3,4) + g(1,3)*g(2,3)
      # we have two ls_keys, one for g(1,2)*g(3,4) and one for g(1,3)*g(2,3)
      for ls_key in ls_poly:
        prefactor = ls_poly[ls_key]
        i_key = [i for i in range(len(ls_key)) if ls_key[i] == 1]

        # lorentz base structure: list of symbols
        lbs = fold(lambda x, y: x + y,
                   [ls_dict[lbase[u].name][0] for u in i_key], [])

        # list of ordered arguments. Arguments are connection to the base in lbs
        # with respect to their postion in the array, e.g. the third arg
        # grouped_args[2] corresponds to the third structure lbs[2]
        grouped_args = fold(lambda x, y: x + y,
                            [ls_dict[lbase[u].name][1] for u in i_key], [])

        lbs, grouped_args = self.gamma_projector(lbs, grouped_args)

        l_key = particles
        for ls in lbs:
          l_key *= ls

        arg_repr = get_representative(lbs, grouped_args)

        # catch scalar case
        if arg_repr != []:
          arg_id = self.get_arg_id(l_key, arg_repr, forbid_newls=forbid_newls)
        else:
          arg_id = 0
        base_structure[(l_key, (arg_id, 1))] = prefactor

      return base_structure
    except Exception:
      print('Failed compute2 for:')
      print("ls_dict:", ls_dict)
      print("self.lorentz:", self.lorentz)
      print("particles:", particles)
      raise

  def gamma_projector(self, lbs, grouped_args):
    """ Replaces gamma*proj+- with gamma+-.

    >>> lbs = [Symbol('Gamma'), Symbol('ProjM')]
    >>> grouped_args = [[1, 3, -1], [-1, 2]]
    >>> dbs = CurrentBase('')
    >>> dbs.gamma_projector(lbs, grouped_args)
    ([GammaM], [[1, 3, 2]])

    >>> lbs = [Symbol('ProjM'), Symbol('ProjP'), Symbol('Gamma'), Symbol('Gamma')]
    >>> grouped_args = [[-2, 1], [-3, 3], [-1, 2, -3], [-1, 4, -2]]
    >>> dbs = CurrentBase('')
    >>> dbs.gamma_projector(lbs, grouped_args)
    ([GammaP, GammaM], [[-1, 2, 3], [-1, 4, 1]])

    >>> lbs = [Symbol('Gamma'), Symbol('Gamma'), Symbol('Gamma'), Symbol('Gamma'), Symbol('ProjP'), Symbol('ProjP')]
    >>> grouped_args = [[-2,-4,-3],[-2,2,-6],[-1,-6,-5],[-1,4,-4],[-5,1],[-3,3]]
    >>> dbs = CurrentBase('')
    >>> dbs.gamma_projector(lbs, grouped_args)
    ([GammaP, Gamma, GammaP, Gamma], [[-2, -4, 3], [-2, 2, -6], [-1, -6, 1], [-1, 4, -4]])
    """
    gammas = [(ls, grouped_args[pos], pos) for pos, ls in enumerate(lbs)
              if ls == Symbol('Gamma') and any(u<0 for u in grouped_args[pos][1:])]

    if not len(gammas) > 0:
      return lbs, grouped_args

    ferm_contr_indices = flatten([[v for v in u[1][1:] if v < 0] for u in gammas])
    ferm_contr_indices_gamma = set(ferm_contr_indices)

    projPs = [(ls, grouped_args[pos], pos) for pos, ls in enumerate(lbs)
              if ls == Symbol('ProjP') and any(u in ferm_contr_indices_gamma for u in
                grouped_args[pos])]


    projMs = [(ls, grouped_args[pos], pos) for pos, ls in enumerate(lbs)
              if ls == Symbol('ProjM') and any(u in ferm_contr_indices_gamma for u in
                grouped_args[pos])]


    if not len(projPs) + len(projMs) > 0:
      return lbs, grouped_args

    ferm_contr_indices = flatten([[v for v in u[1] if v < 0] for u in projPs])
    ferm_contr_indices_projp = set(ferm_contr_indices).intersection(ferm_contr_indices_gamma)
    ferm_contr_indices = flatten([[v for v in u[1] if v < 0] for u in projMs])
    ferm_contr_indices_projm = set(ferm_contr_indices).intersection(ferm_contr_indices_gamma)

    gamma_pos = None
    gamma_index = -1
    projP_pos = None
    projP_index = -1
    projM_pos = None
    projM_index = -1
    for i, gamma in enumerate(gammas):
      gamma_contr = [u for u in gamma[1][1:] if u < 0]
      if any(v in ferm_contr_indices_projp for v in gamma_contr):
        gamma_index = i
        gamma_pos = gamma[2]
        for i, projP in enumerate(projPs):
          if any(v in gamma_contr for u in gamma for v in projP[1]):
            projP_index = i
            projP_pos = projP[2]
            break
        if projP_index != -1:
          break
      elif any(v in ferm_contr_indices_projm for v in gamma_contr):
        gamma_index = i
        gamma_pos = gamma[2]
        for i, projM in enumerate(projMs):
          # if any(v in ferm_contr_indices_projm for v in projM[1]):
          if any(v in gamma_contr for u in gamma for v in projM[1]):
            projM_index = i
            projM_pos = projM[2]
            break
        if projM_index != -1:
          break

    if projM_index + projP_index < -1:
      return lbs, grouped_args

    if projM_index > -1:
        proj_arg = projMs[projM_index][1]
        proj = '-'
    else:
      proj_arg = projPs[projP_index][1]
      proj = '+'
    gamma_arg = gammas[gamma_index][1][:]

    found_valid = False
    if (proj_arg[0] < 0 and gamma_arg[2] < 0):
      gamma_arg[2] = proj_arg[1]
      found_valid = 1
    elif (proj_arg[1] < 0 and gamma_arg[1] < 0):
      gamma_arg[1] = proj_arg[0]
      found_valid = 2

    if found_valid == 1:
      new_struc = 'GammaP' if proj == '+' else 'GammaM'

    elif found_valid == 2:
      new_struc = 'GammaM' if proj == '+' else 'GammaP'

    if found_valid:
      lbs[gamma_pos] = Symbol(new_struc)
      grouped_args[gamma_pos] = gamma_arg

      if proj == '+':
        del lbs[projP_pos]
        del grouped_args[projP_pos]
      else:
        del lbs[projM_pos]
        del grouped_args[projM_pos]

    if len(ferm_contr_indices_projp) + len(ferm_contr_indices_projm) > 1:
      return self.gamma_projector(lbs, grouped_args)
    else:
      return lbs, grouped_args

  @classmethod
  def transform_lbs(cls, lbs):
    """ Transform every `lb` in the  lorentz base sum (`lbs`)  according to
    `transform_lb` method. The final result is combined to uniqe `lb`.

    >>> Ga = Symbol('Gamma'); GaM = Symbol('GammaM'); GaP = Symbol('GammaP')
    >>> lbs = [[3*Ga, (1,2)], [3*GaP, (1,1)]]
    >>> dbs = CurrentBase('')
    >>> dbs.transform_lbs(lbs)
    [[3*GammaM, (1, 2)], [3*GammaP, (1, 3)]]

    >>> lorentz = 'Gamma(3,2,-1)*Gamma(1,4,-1)'
    >>> dbs = CurrentBase(lorentz)
    >>> _ = dbs.compute(range(4))
    >>> lorentz = 'Gamma(3,2,-1)*GammaM(1,4,-1)'
    >>> dbs = CurrentBase(lorentz)
    >>> _ = dbs.compute(range(4))

    # Build up the lbs
    >>> lb1 = [4*Ga*Ga, (0,2)]; lb2 = [4*Ga*GaM, (0,2)]
    >>> lbs = [lb1, lb2]
    >>> dbs.transform_lbs(lbs)
    [[4*GammaP**2, (0, 2)], [4*GammaM*GammaP, (0, 4)], \
[4*GammaM**2, (0, 4)], [4*GammaM*GammaP, (1, 2)]]
    """
    # we collect lb_keys defined by (ls_ley, arg_id): Prefactor
    same_lbs = {}
    for lb in lbs:
      if type(lb[0]) is int:
        new_lbs = [lb]
      else:
        new_lbs = cls.transform_lb(lb)
      # for every lb we check if the lb_key is in same_lbs, if not a new entry
      # is created, otherwise the prefactors are summed.
      for new_lb in new_lbs:
        ls_key = new_lb[0]
        arg_id, prefac = new_lb[1]
        lb_key = (ls_key, arg_id)
        if lb_key not in same_lbs:
          same_lbs[lb_key] = prefac
        else:
          same_lbs[lb_key] += prefac
    # from same_lbs the lbs is contructed.
    ret_lbs = []
    for lb_key in same_lbs:
      # lb_key[0] is the ls_key, .e.g 3*g*g, lb_key[0] is the argument id
      # same_lbs[lb_key] is the prefactor
      new_lb = [lb_key[0], (lb_key[1], same_lbs[lb_key])]
      ret_lbs.append(new_lb)
    return ret_lbs

  @classmethod
  def transform_lb(cls, lb):
    """ Transforms a lorentz base (`lb`) according to the some transformation
    rules defined by the class attribute `base_transf`.

    :param lb: Standard lorentz base, e.g.  [3*Ga, (1,2)]
    :type  lb: list

    Examples:
    ---------

    >>> Ga = Symbol('Gamma'); GaM = Symbol('GammaM'); GaP = Symbol('GammaP')
    >>> lb = [3*Ga, (1,2)]
    >>> dbs = CurrentBase('')
    >>> dbs.transform_lb(lb)
    [[3*GammaM, (1, 2)], [3*GammaP, (1, 2)]]

    # the Gamma^2 structures are not in the lorentz_base by default, so we add
    # one of them
    >>> lorentz = 'Gamma(3,2,-1)*Gamma(1,4,-1)'
    >>> dbs = CurrentBase(lorentz)
    >>> _ = dbs.compute(range(4))

    # Build up the lb
    >>> lb = [4*Ga*Ga, (0,2)]
    >>> dbs.transform_lb(lb)
    [[4*GammaM**2, (0, 2)], [4*GammaM*GammaP, (0, 2)], \
[4*GammaM*GammaP, (1, 2)], [4*GammaP**2, (0, 2)]]
    """
    # get the lorentz key and arguments and
    ls_key = lb[0]
    arg_id, pref = lb[1]
    args = lorentz_base_dict[ls_key][arg_id]

    # disassemble the lorentz key and bring it into the canconcial order
    # corresponding to the order of arguments
    key, np = get_lorentz_key_components(ls_key)
    key = tuple(order_strucs(key))

    # Check if the key should be transformed according to the base_transf.
    if key in cls.base_transf:
      new_base = cls.base_transf[key]
      lbs = []
      # Found a new base. We build up the lbs (sum of different lb)
      for nk, prefn in new_base:
        # make sure that the arguments are in the correct order defined by the
        # order of the new basis
        new_args = get_representative(nk, args)
        new_ls_key = np*reduce(lambda x, y: x*y, nk)

        # retrieve the arg id and construct the lb
        new_arg_id = cls.get_arg_id(new_ls_key, new_args)
        # the new prefactor is given by the old prefactor times the prefactor of
        # the basis
        lbs.append([new_ls_key, (new_arg_id, pref*prefn)])
      return lbs
    else:
      return [lb]

  @classmethod
  def form_simplify_lbs(cls, lbs, **kwargs):
    """
    """
    #  fix SORTING :  <07-05-18, J.-N. Lang> #
# ('base_structure before:', [[3*Epsilon*Metric*P**3, (0, -1/2)], [3*Epsilon*Metric*P**3, (1, -1/2)], [3*Epsilon*Metric*P**3, (2, 1/2)], [3*Epsilon*Metric*P**3, (3, 1/2)], [3*Epsilon*Metric*P**3, (4, -1/2)], [3*Epsilon*P**3, (2, 1)], [3*Epsilon*Metric*P**3, (5, -1/2)], [3*Epsilon*P**3, (3, 1)], [3*Epsilon*Metric*P**3, (6, 1/2)], [3*Epsilon*P**3, (4, 1)], [3*Epsilon*Metric*P**3, (7, -1/2)], [3*Epsilon*P**3, (5, -1)], [3*Epsilon*P**3, (6, 1)], [3*Epsilon*P**3, (7, -2)], [3*Epsilon*P**3, (8, -1)], [3*Epsilon*P**3, (9, -1)], [3*Epsilon*Metric*P**3, (8, 1/2)], [3*Epsilon*P**3, (10, 1)], [3*Epsilon*P**3, (11, -1)], [3*Epsilon*Metric*P**3, (9, -1/2)], [3*Epsilon*Metric*P**3, (10, 1/2)], [3*Epsilon*P**3, (12, 1)], [3*Epsilon*Metric*P**3, (11, 1/2)], [3*Epsilon*P**3, (13, -2)], [3*Epsilon*P**3, (14, -2)], [3*Epsilon*P**3, (15, -1)], [3*Epsilon*P**3, (16, -1)]])
# ('base_structure after:', [[3*Epsilon*P**3, (56, 2)], [3*Epsilon*P**3, (59, -2)], [3*Epsilon*P**3, (58, -2)], [3*Epsilon*P**3, (64, 2)], [3*Epsilon*Metric*P**3, (13, 2)], [3*Epsilon*Metric*P**3, (12, 2)], [3*Epsilon*P**3, (60, 2)], [3*Epsilon*P**3, (63, 2)], [3*Epsilon*Metric*P**3, (14, 2)], [3*Epsilon*P**3, (62, -2)], [3*Epsilon*P**3, (61, 2)], [3*Epsilon*P**3, (57, 2)]])
    # we collect lb_keys defined by (ls_ley, arg_id): Prefactor
    same_lbs = {}
    for lb in lbs:

      if type(lb[0]) is int or lb[0].is_number:
        new_lbs = [lb]
      else:
        new_lbs = cls.form_simplify_lb(lb, **kwargs)
      # for every lb we check if the lb_key is in same_lbs, if not a new entry
      # is created, otherwise the prefactors are summed.
      for new_lb in new_lbs:
        if new_lb is not None:
          ls_key = new_lb[0]
          arg_id, prefac = new_lb[1]
          lb_key = (ls_key, arg_id)
          if lb_key not in same_lbs:
            same_lbs[lb_key] = prefac
          else:
            same_lbs[lb_key] += prefac
            if same_lbs[lb_key] == 0:
              del same_lbs[lb_key]

    # from same_lbs the lbs is contructed.
    ret_lbs = []
    for lb_key in same_lbs:
      # lb_key[0] is the ls_key, .e.g 3*g*g, lb_key[0] is the argument id
      # same_lbs[lb_key] is the prefactor
      new_lb = [lb_key[0], (lb_key[1], same_lbs[lb_key])]
      ret_lbs.append(new_lb)

    return ret_lbs

  @classmethod
  def order_base_structure(cls, lbs):
    """ Brings the lbs to canonical order.  """
    keys = {}
    for lb in lbs:
      key, _ = get_lorentz_key_components(lb[0])
      key = tuple(order_strucs(key))
      if key not in keys:
        weight = tuple(lorentz_base.order[u] for u in key)
        keys[key] = (weight, [lb])
      else:
        keys[key][1].append(lb)

    if len(keys) == 1:
      return sorted((u for u in lbs), key=lambda x: x[1])
    else:
      keys_sorted = sorted((u for u in keys), key=lambda x: keys[x][0])
      ret = []
      for key in keys_sorted:
        ret += sorted(keys[key][1], key=lambda x: x[1][0])
      return ret

  @classmethod
  def form_simplify_lb(cls, lb, use_momentum_conservation=False,
                       reduce_dalgebra=False, **kwargs):
    """
    >>> from sympy import Symbol
    >>> lb = [4*Symbol('Epsilon'), (0, 2)]
    >>> CurrentBase.form_simplify_lb(lb)
    [[4*Epsilon, (0, 2)]]

    # >>> lb = [3*Symbol('P'), (4, 2)]
    # >>> CurrentBase.form_simplify_lb(lb)
    # [[3*P, (0, -2)], [3*P, (2, -2)]]
    """
    lb_key = (lb[0], lb[1][0])
    prefac = lb[1][1]
    if lb_key in cls.cache_form_transform:
      lbs_ret = cls.cache_form_transform[lb_key]
      if lbs_ret is None:
        return [None]
      else:
        return [[u[0], (u[1][0], u[1][1] * prefac)] for u in lbs_ret]
    ls_key, particles = get_lorentz_key_components(lb[0])
    ls_key = order_strucs(ls_key)
    arg = lorentz_base_dict[lb[0]][lb[1][0]]
    frm_str = generate_form_expression(ls_key, arg)

    exprs = ['l expr = ' + frm_str + ';']
    if use_momentum_conservation:
      if particles == 3:
        # simple improvement that the 3-Vertex looks simpler
        exprs.extend(['id p3 = -p1 -p2;'  # <- get rid of p3 in scalar products
                      '.sort',
                      'id p1(mu1) = -p3(mu1) -p2(mu1);',
                      'id p2(mu2) = -p3(mu2) -p1(mu2);',
                      'id p3(mu3) = -p1(mu3) -p2(mu3);'])
      else:
        mom_cons = ('id p' + str(particles) + ' = ' +
                    ''.join(map(lambda x: '-p' + str(x), range(1, particles)))
                    + ';')
        exprs.append(mom_cons)
        mom_cons = ('id p' + str(particles) + '(mu?) = ' +
                    ''.join(map(lambda x: '-p' + str(x) + '(mu)',
                            range(1, particles)))
                    + ';')
        exprs.append(mom_cons)

    exprs.append('#call lorentzdirac'),

    # choose expansion rules according to config
    if(hasattr(rept1l_config, 'reduce_dalgebra') and
       rept1l_config.reduce_dalgebra and reduce_dalgebra):
      exprs.append('#call lorentzbasis')

    from rept1l.formutils import FormPype
    with FormPype() as FP:
      FP.eval_exprs(exprs)
      res = ''.join(FP.return_expr('expr').split())

    if res != '0':
      # transform the result back to a lorentz base structure
      from rept1l.parsing import FormParse
      FP = FormParse()
      res = FP.parse_UFO(FP.assign_lorentz(res))
      from rept1l.baseutils import CurrentBase
      res = CurrentBase(res).compute2(FP.ls_dict, particles)
      s = []
      for lb_tmp, prefac_tmp in iteritems(res):
        lb_mod = [lb_tmp[0], (lb_tmp[1][0], lb_tmp[1][1] * prefac_tmp)]
        s.append(lb_mod)

      s = sorted(s, key=lambda x: x[1][0])
      cls.cache_form_transform[lb_key] = s
      lb_ret = [[u[0], (u[1][0], u[1][1] * prefac)] for u in s]
      return lb_ret
    else:
      cls.cache_form_transform[lb_key] = None
      return [None]

  def parse_to_sympy(self, expr):
    """
    Parses a string or a list of strings to sympy instances. The sympy internal
    parse_expr is used.

    :param expr: Expression to be parsed
    :type  expr: String or list of strings

    :return: Sympy expression of expr
    """
    if isinstance(expr, str):
      ret = sympify(expr)
      return ret

    elif isinstance(expr, list_type):
      return [self.parse_to_sympy(u) for u in expr]
    else:
     raise TypeError('parse_to_sympy requires string or list of strings. Given:' +
                      str(type(expr)))


if __name__ == "__main__":
  import doctest
  doctest.testmod()


  # lorentz = 'Gamma5(1,2)'
  # permutation = [0, 1]
  # dbs = CurrentBase(lorentz)
  # print(dbs.compute(permutation))

  #from sympy import Symbol
  #lb = [4*Symbol('Epsilon'), (0, 2)]
  #CurrentBase.form_simplify_lb(lb)
  #[[4*Epsilon, (0, 2)]]
  #lb = [3*Symbol('P'), (4, 2)]
  #CurrentBase.form_simplify_lb(lb)
  #[[3*P, (0, -2)], [3*P, (2, -2)]]
  #lsd = {'L0': ([P, P], [[-1, 1], [-1, 1]]), 'L1': ([g], [[1, 2]])}
  #particles = 3
  #lorentz = sympify('3*I*C21*C59/(16*pi**2) + 3*C59*C60**2/(32*pi**2) - 3*C60**2*C86/(32*pi**2) + 3*C61*C62*C63/(16*pi**2) + 3*I*C61*C7/(8*pi**2) - 3*C62*C63*C86/(16*pi**2) + 3*C64**3*ME/(4*pi**2) + 3*C65**3*MM/(4*pi**2) + 3*C66**3*MTA/(4*pi**2) + 3*C67**3*MU*Nc/(4*pi**2) + 3*C68**3*MC*Nc/(4*pi**2) + 3*C69**3*MT*Nc/(4*pi**2) + 3*C70**3*MD*Nc/(4*pi**2) + 3*C71**3*MS*Nc/(4*pi**2) + 3*C72**3*MB*Nc/(4*pi**2)')
  #dbs = CurrentBase(lorentz)
  #particles = 2
  #print dbs.compute2(lsd, particles)
  #{(2*Metric*P**2, (0, 1)): 3*C0**2, (2*Metric, (0, 1)): -C0**2}
