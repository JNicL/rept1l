.. REPT1L documentation master file, created by
   sphinx-quickstart on Fri Sep 18 15:27:00 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to REPT1L!
==================================
.. image:: _static/reptil.png

**REPT1L** is a *Python* (2.7) library which stands for

  **R** ecola's r **E** normalization **P** rocedure **T** ool at **1** **L** oop,
  
and which can be used to derive tree-level as well as one-loop renormalized
model files for **RECOLA2** from bare **UFO** model files.
REPT1L has the following features:

* Derive RECOLA2 model files from UFO format.

* Perform **counterterm expansion** of vertices.

* Set-up and solve **renormalization conditions**.

* Compute rational terms of type **R2** in a fully automated way.

* **Validate results** using the :math:`R_\xi`-gauge or background-field method.

REPT1L is not a self-contained program and makes extensive use of `FORM <http://www.nikhef.nl/~form/>`_, `RECOLA
<http://recola.hepforge.org/>`_ and `Sympy <http://www.sympy.org/>`_.

The documentation provides an introduction to the usage of REPT1L, accompanied
by several (code) examples. Please follow the links on the sidebar to get started.

.. toctree::
   :maxdepth: 2
   :hidden:

   Installation <installation/installation.rst>
   Quickstart <quickstart/quickstart.rst>
   Renormalization <renormalization/renormalization.rst>
   Tools <tools/tools.rst>
   process_generation.rst

==================

* :ref:`genindex`
* :ref:`modindex`
