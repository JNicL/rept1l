.. _two-point_renormalization:

Renormalization of 2-point-functions
====================================


.. module:: Renormalize.renormalize_methods

The renormalization of 2-point functions determines wavefunction and mass
counterterm parameters.  *REP1L* provides classes for imposing MS-bar and
onshell conditions and for solving them automatically in terms of the
counterterm parameters.

The function :py:func:`spam` does a similar thing.

The decisive class is :py:class:`RenormalizeSelfenergy`
which is defined as follows

.. autofunction:: Renormalize.renormalize_methods.renormalize_tadpole



.. autoclass:: Renormalize.renormalize_methods.RenormalizeSelfenergy()
    :members:

    .. automethod:: __init__(particle, **kwargs)


.. py:function:: spam(eggs)
                 ham(eggs)

   Spam or ham the foo.

As a simple example
consider the function :py:func:`renormalize_SM_higgs` which
renormalizaties the Higgs onshell:

.. literalinclude:: ../../recola_feynrules_wrapper/Renormalize/renormalize.py
  :linenos:
  :emphasize-lines: 3-6
  :pyobject: Renormalize.renormalize_SM_higgs

First, the active model is imported by calling `import Model`. This gives access to all
instances of vertices, parameters, particles, etc. as provided by the UFO format.
The renormalization is performed by applying the class
:py:class:`RenormalizeSelfenergy`.

.. literalinclude:: ../../recola_feynrules_wrapper/Renormalize/renormalize_methods.py
  :linenos:
  :emphasize-lines: 3-6
  :pyobject: RenormalizeSelfenergy.__init__

.. toctree::
   :hidden:

   Regularization/regu_methods.rst
