.. _counterterm_expansion:

Designing user_settings.py
==========================

This section explains how a typical ``user_settings.py`` should look like. See
:ref:`Quickstart <quickstart>` for how to set-up a REPT1L model file.



Quickguide
----------

todo

****

Defining counterterm parameters
-------------------------------

Counterterm parameters are defined as usual Parameter in UFO (See
``parameters.py`` in a UFO model file). For instance, the counterterm to the
electric charge can be defined as follows::

  dZee = Parameter(name='dZee',
                   nature='external',
                   type='real',
                   value='ee*dZee',
                   texname='\delta Z_e',
                   lhablock='CTs',
                   lhacode=[1])

wich is equivalent to the shortcut::

  # import the method add_counterterm which is not imported by default
  # in user_settings.py
  from rept1l.autoct.auto_parameters_ct import add_counterterm
  dZee = add_counterterm('dZee', 'ee*dZee')

As for now the counterterm parameter is just a parameter in the sense
that it is a ``Parameter`` instance. What makes the ``dZee`` a counterterm for
REPT1L is when it is assigned to another parameter via the
``counterterm`` attribute::

  # access to the electric charge parameter in the model
  ee = param.ee 
  # assign counterterm
  ee.counterterm = dZee


It is recommended to use the method ``assign_counterterm`` which combines adding
and assigning counterterm parameter:

.. code-block:: python
   :name: ctrules
   :caption: ctrules

   assign_counterterm(param.ee, 'dZee', 'ee*dZee')

The precise definition of the method is given by

.. autofunction:: rept1l.autoct.auto_parameters_ct.assign_counterterm 

The ``counterterm`` attribute of a ``Parameter`` is what triggers a counterterm
expansion. In this way REPT1L is able to derive counterterms to all vertex
function with legs :math:`\ge 3` given expansion rules such as :ref:`ctrules`.
In order to trigger the counterterm expansion
use the optional argument ``-cct`` in the :ref:`run_model <run_model>` tool.

****

Defining counterterm vertices
=============================

todo

****

.. toctree::
    :hidden:

    ../quickstart/quickstart.rst
    ../tools/run_model.rst
