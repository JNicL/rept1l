.. _regu_methods:

Regularization methods
======================

.. autoclass:: parsing.Regularize
    :members: onshell_fermion, onshell_massless_vector, onshell_massive_scalar, onshell_massless_scalar

