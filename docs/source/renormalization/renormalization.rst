.. _renormalizaton:

***************
Renormalization  
***************

Introduction 
------------

The following sections introduce the renormalization procedure in
REPT1L and are intended to help the user to get familiar with
the necessary steps and to indicate where intervention is required.

What REPT1L can do
------------------

Renormalization is a procedure to render a canonically defined theory
well-defined at every order in the perturbative expansion.  REPT1L follows the
standard procedure by reexpressing bare parameters of the theory in terms of
renormalized ones and solving renormalization conditions. The full procedure is
schematically displayed in Figure :num:`renproc`.  

..  _renproc:

.. figure:: pics/rept1ltoolchain.svg

    Renormalization procedure in REPT1L.

The UFO model file is considered as bare model and in a first step a bare
RECOLA2 model file is generated with a suited counterterm expansion. This allows
RECOLA2 to generate a set of vertex functions which are then required to be UV
finite, e.g. by applying on-shell renormalization to two-point functions. All
expressions are derived analytically in :math:`d` dimensions also allowing to
extract the rational terms of type :math:`R_2`. After the renormalization and
computation of rational terms, the model file is regenerated, including the
solutions for counterterms oand the rational terms as new Feynman rules.  The
model file is then complete and ready for computation of one-loop amplitudes.

Counterm expansion
------------------

The user has to define certain counterterm expansion rules such that REPT1L can
generate counterterm vertices. Since the renormalization is usually performed
in the mass eigenbasis, which is `assummed` for every model, assigning
counterterm parameters can be automated. In fact, the user does not have to
provide any rules for mass-or wavefunction renormalization which can be done
automatically and only rules for fundamental couplings need to be provided.
For instance, consider the Lagrangian of a scalar theory:

.. math:: \mathcal{L} = \frac{1}{2}\left(\partial_\mu \Phi_0 \partial_\mu \Phi_0 - m_0^2 \Phi_0^2\right) +
  \frac{\lambda_0}{3!} \Phi^3_0 = \\ \frac{Z_\Phi}{2} \left(\partial_\mu \Phi \partial_\mu \Phi - Z_m m^2 \Phi^2 \right)
  + \sqrt{Z_\Phi}^3 Z_\lambda \frac{\lambda}{3!} \Phi^3, 
  :label: lagrange


with 

.. math:: \Phi_0=\sqrt{Z_\Phi} \Phi, \quad m_0=Z_m m .
  :label: wfmass

.. math:: \lambda_0=Z_\lambda \lambda .
  :label: coupling


REPT1L expects the user to provide certain rules such as
:eq:`coupling`. Automatically assigning fules for :eq:`wfmass` is provided in
the ``autoct`` module.
For example, the Standard Model only requires the expansion
rule for the gauge couplings :math:`e, g_s`, i.e. the electric charge, and strong
couplings.

The counterterm expansion is controlled throught the ``user_settings.py`` file
in the corresponding REPT1L model file.
For a complete discussion of the counterterm expansion read

* :ref:`Setting up user_settings.py <counterterm_expansion>`

Further topics with RECOLA
---------------------------
The third step discusses the renormalization of particle masses and
wave-functions, or in general the renormalization of 2-point functions.

* :ref:`Renormalization of 2-point functions <two-point_renormalization>`

in the last step, we explain how couplings can be renormalized, by either using
a standard renormalization procedure or by making use of identities.

* :ref:`Renormalization of vertex functions <vertex_renormalization>`

* :ref:`A list of regularization methods <regu_methods>`

.. toctree::
   :hidden:

   renormalization_preparation.rst
   counterterm_expansion.rst
   two-point_renormalization.rst
   vertex_renormalization.rst
   Regularization/regu_methods.rst
