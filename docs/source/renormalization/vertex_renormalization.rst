.. _vertex_renormalization:

^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Renormalization of Vertex-Functions
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. module:: Renormalize.renormalize_methods



The renormalization of vertex functions determines counterterm parameters
corresponding to couplings. *REP1L* provides the class
:py:class:`RenormalizeVertex` to deal with vertices. The generation of vertex
functions is restricted to the capabilities of *RECOLA* (see :ref:`Process generation with REP1L <process_generation>`) which should be kept in
mind.

The class is defined as follows

.. autoclass:: Renormalize.renormalize_methods.RenormalizeVertex()
    :members:

    .. automethod:: __init__(vertex, vertex, renoscheme, **kwargs)

Usage
^^^^^

In order to renormalize methods access to the modelfile is required. The
class :py:class:`RenormalizeVertex` requires a UFO vertex to identify the vertex
function. There are basically two ways to obtain a vertex.

The simpler one is to use the find_vertex method from ????:

.. code-block:: python

   from Rept1l.helper_lib import find_vertex
   particles = ['g', 'g', 'g']
   myvertex = find_vertex(particles)
   # check particles
   print myvertex.particles


Alternatively it is possible to import the Model and to import the vertex id
direclty which needs to be looked up in the UFO modelfile. E.g.

.. code-block:: python

   import Rept1l.Model as model
   myvertex = model.V.V_1
   # check particles
   print myvertex.particles

The renormalization is tarted by invoking

.. code-block:: python

   from Rept1l.Renormalize import RenormalizeVertex
   result = RenormalizeVertex(myvertex)

Hacking
^^^^^^^

Rept1l is designed to be modular in the sense that it is possible to use
exisiting code blocks and extend the functionality to some extend. For example,
it might be desired to renormalize a vertex at zero-momentum transfer. Thus,
:py:class:`RenormalizeVertex` needs to be modified and the call where the
ampltiude is regularized needs to be adapted.  In order to achieve this, I
propose to modify the method :py:func:`RenormalizeVertex.regularize_vertex`

.. literalinclude:: ../../recola_feynrules_wrapper/Renormalize/renormalize_methods.py
  :linenos:
  :emphasize-lines: 5-6
  :pyobject: RenormalizeVertex.regularize_vertex


A custom RenormalizeVertex class could be created as follows:

.. code-block:: python

   from Rept1l.Renormalize import RenormalizeVertex
   
   class RenormalizeVertexZM(RenormalizeVertex):
     # use the exisiting init function of RenormalizeVertex
     def __init__(self, *args, **kwargs):
       super(RenormalizeVertexZM,  self).__init__(*args, **kwargs)

     # overwrite renormalize_vertex with a custom regularization function
     def regularize_vertex(self):
       from Rept1l.Regularize import Regularize
       Reg = Regularize(self.treevertices, self.ctvertices)

       # need to define the subtraction method my_custom_subtraction
       Reg.my_custom_subtraction(self.process_file)
       return Reg
