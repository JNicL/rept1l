.. _pyrecola:


^^^^^^^^
PYRECOLA
^^^^^^^^

.. module:: pyrecola


*PYRECOLA* is imported as every other python module by invoking 

.. code-block:: python

  import pyrecola

in a python shell.


*PYRECOLA* comes with the following methods which are divided in process
generation, process compitation and deallocation methods:

Process generation
^^^^^^^^^^^^^^^^^^

.. function:: define_process_rcl(process_id, process, order)

    Defines a process in RECOLA.
    
    :param process_id: The process id in order to distinguish between different
                       processes.
    :type  process_id: int

    :param process: The process string in the form 'p1 p2 -> p3 p4 ...' where
                    :math:`p_i` are particles of the model. See RECOLA's manual
                    if unclear.
    :type  process: str

.. function:: generate_processes_rcl()

    Generates the processes which are defined in the previous step of RECOLA's
    workflow.

.. function:: compute_selfenergy(value)

    By default RECOLA discards selfenergy topologies on external legs which
    can be disabled by calling this function. This
    should be used only in combination with a :math:`1 \to 1` process.

    :param value: Enable selfenergies on external legs by setting value to True.
    :type  value: bool

.. function:: compute_tadpole(value)

    By default RECOLA discards tadpole topologies which can be disabled by
    calling this function. This should be used only in combination with a
    :math:`1 \to 1` process.

    :param value: Enable tadpoles by setting value to True.
    :type  value: bool


Process computation
^^^^^^^^^^^^^^^^^^^

.. function:: compute_process(process_id, momenta)

    Computes the process with process ID `process_id` at the phase-space point
    specified by `momenta`

    :param process_id: The process ID defined when calling
                       :py:func:`define_process_rcl`.
    :type  process_id: int

    :param momenta: Matrix p of the form p(legs, 0:3). The first index
                    represents the particle in the order of the process
                    definition and the second index is the space-time component
                    of the four-momentum.
    :type  momenta: :math:`N\times4` real matrix



Deallocation
^^^^^^^^^^^^

.. function:: deallocate()

  Deallocates *RECOLA* and allows for new calls of :py:func:`define_process_rcl`
  and :py:func:`generate_processes_rcl`.

