.. _quickstart:

***************
Getting started
***************

This tutorial discusses the generation of bare-loop RECOLA2 model files
starting from a valid_ UFO format.  The general procedure is the same for
deriving renormalized models, but requires preparatory work, e.g. the
renormalization itself which is discussed in other sections.  Finally, it is
shown how compilation and linkage to RECOLA2 can be achieved.

Setting up REPT1L model files
-----------------------------

In the following it is assumed that a UFO model file has been derived, e.g.
by means of using FeynRules or SARAH, referred to as the *UFO_MODEL_FILE* and
that it is present as a folder containing the following files:

* *UFO_MODEL_FILE*

  * ``__init__.py``

  * ``object_library.py``

  * ``particles.py``

  * ``couplings.py``

  * ``vertices.py``

  * ``lorentz.py``

  * ``coupling_orders.py``

.. note:: FeynRules generates further files which are not needed by REPT1L.

REPT1L works with *UFO_MODEL_FILE*, augmented by drivers allowing for an
NLO expansion. A valid REPT1L model file, referred to as
*REPT1L_MODEL_FILE*, has the following structure:

* REPT1L_MODEL_FILE

  * ``__init__.py``

  * ``user_settings.py``

  * UFO_MODEL_FILE

i.e. the *UFO_MODEL_FILE* is just placed into the *REPT1L_MODEL_FILE*. The additional files ``__init__.py`` and ``user_settings.py`` are generic (template) files which should be copied from ::

    $REPT1L_PATH/modeldriver


****

Activating REPT1L model files
------------------------------

The possibility for switching between different model files is useful and
REPT1L has a dynamic way for selecting a specific model. A model file is
selected by exporting the environment variable *REPTIL_MODEL_PATH* with a
value pointing to the precise model file path::

    export REPTIL_MODEL_PATH=/PATH/TO/REPT1L_MODEL_FILE/

.. note:: The path variable name *REPTIL_MODEL_PATH* can be altered by changing the corresponding entry in the rept1l_config.

****

Testing model imports
---------------------

In order to test if the model file is loaded correctly, run in a python shell,
e.g.  **IPython**::

  ipython
  >> import rept1l.Model as Model
  >> model = Model.model
  >> print(model.object_library.all_particles)

If IPython is not available, a demo-file can be run located in
`advanced_tools`::

  cd $REPT1L_PATH/advanced_tools
  python importmodel.py

****

Running REPT1L
--------------

The RECOLA2 model file generation is done with the tool ``run_model`` located in *tools*.
Before execution, one should make sure there is an empty folder, referred to as
*RECOLA_MODEL_FILE*, which can be populated with
RECOLA2 model files. Then execute::

    cd $REPT1L_PATH/tools
    ./run_model /PATH/TO/RECOLA_MODEL_FILE -f

The optional argument ``-f`` forces the generation and answering any queries
with the default response. Consult the help via ``./run_model -h`` for different
optional arguments.

****

Simplified compilation
----------------------

REPT1L  comes with the tool ``crm`` (compile recola model) which handles the
compilation of model files and the RECOLA2 library. To register a new model
exectute::

  cd $REPT1L_PATH/tools
  ./crm -c MODELNAME

where you have to provide the path to the previously derived RECOLA2 model
*RECOLA_MODEL_FILE*.

Then compile the model::

  ./crm MODELNAME

****

Manual compilation
------------------

The manual compilation requires to compile the model file first::

    cd /PATH/TO/RECOLA/MODEL/FILE
    cmake . && make

Then compile RECOLA2::

    export MODELFILE_PATH=/PATH/TO/RECOLA/MODEL/FILE
    cmake . && make

If REPT1L has been installed manually, it is necessary to provide the paths
to the RECOLA2 sources, as well as the path to the COLLIER library.


.. _valid:

Valid UFO model files
---------------------

.. todo::

  List restrictions when deriving UFO model files
   
* real valued parameter

* no special functions in couplings except for *sqrt* and trigonometric functions
  
* no form factors

* Case insensitive definition of parameters, otherwise conflict in Fortran
