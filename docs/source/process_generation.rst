.. _process_generation:

^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Process generation with REP1L
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

REPT1L comes with two process generation tools which use *RECOLA* and *FORM*
internally. The multi-purpose library is *PYRECOLA* which is a python wrapper to
*RECOLA* and can be used to generate and compute arbitrary processes limited to
the capabilities of *RECOLA*.


A. :ref:`PYREOLA <pyrecola>`


.. toctree::
   :hidden:

   pyrecola.rst
