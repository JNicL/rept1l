.. _installation:


************
Installation 
************

****

Automatic installation
======================

REPT1L can be configured and installed with an automated install script
available from::

    git clone git@bitbucket.org:JNicL/rept1nstall.git


The script requires the following packages:

* `CMake <https://cmake.org/>`_ ``2.8.7`` or better. Needs to be compiled with ssl support.

* `Python <https://www.python.org/>`_
  ``2.7`` exact.

* `pip <https://pypi.python.org/pypi/pip/>`_
  ``7`` or better.


It is recommended to install pip systemwide by invoking (as root)::

    apt-get install python-pip 

Users without root rights can install pip locally via::

  wget https://bootstrap.pypa.io/get-pip.py
  python get-pip.py --user

.. note:: Additional dependencies are (automatically) installed locally and are only available to the current user.

The installation begins by invoking::

    cd rept1nstall
    python install.py

After selecting the automatic installation (1) the core program and third-party
tools are automatically downloaded and compiled. See dependencies_ for further
details.

.. note:: The install script performs the installation REPT1L, RECOLA2, FORM and COLLIER to the current working directory, i.e. to the *rept1nstall* folder. The installation path can be modified by moving *that* folder or its content 

After installation the running shell should be restarted such that the PATHS get
set/updated (or by sourcing the shell rc). It is recommended to run the tests in
order to see that the installation succeeded.

.. note:: The script will set-up config files usually in `REPT1L_CONFIG=~/config/rept1l/`.


****

Testing
=======

REPT1L comes with unittests, doctests and a testsuite tool. The unit-and
doctests can be run by invoking::

    cd $REPT1L_PATH
    ./run_unittests.sh

All tests should succeed in order to guarantee correct functionality of
REPT1L. For extensive tests consider running the *testsuite*::

    cd $REPT1L_PATH/tools
    ./testsuite

**** 

Manual installation
===================

**REPT1L** is obtained from the repository ::

    git clone git@bitbucket.org:JNicL/rept1l.git

**RECOLA2** is obtained from the repository ::

    git clone git@bitbucket.org:JNicL/recola2.0.git


**COLLIER** is hosted on `hepforge <http://www.hepforge.org/>`_  and has been
tested with version ``1.0``::

    wget http://www.hepforge.org/archive/collier/collier-1.0.tar.gz

Follow the `collier installation instructions <http://collier.hepforge.org/installation.html/>`_

**FORM** is hosted on `github <https://github.com/vermaseren/form>`_ and has
been tested with version ``4.1``::

    wget https://github.com/vermaseren/form/archive/v4.1-20131025.tar.gz

which completed the third-party tools.

Finally, the **Python**  dependencies should be installed with **pip** via::

    cd rept1l
    install pip -r requirements.txt --user

where requirements is a file listing all python dependencies and is located at
the top level of the REPT1L source folder.

Next, the environment paths_ need to be set by hand, or by using the
guided installation script::
  
    python install.py

The installation script then requires to specify the path to the RECOLA
repository directory. The repository should have a recola and pyrecola directory
containing all the sources. 

****

.. _paths:

Runtime-paths 
=============

In order to run REPT1L certain environment variables need to be set to library
and source file paths, and need to be available in every REPT1L session. 
It is recommended to export them to the shell config.

RECOLA paths
------------

* Path to the (compiled) RECOLA library::
  
    export RECOLA_PATH=/Folder/Including/librecola.so/

* Path to RECOLA sources (pointing to CMakeLists.txt)::
  
    export RECOLA_SOURCE_PATH=/PATH/TO/SOURCES/

****

COLLIER paths
-------------

* Path where the COLLIER library ``libcollier.so`` is located::

    export COLLIER_LIBRARY_DIR=/Folder/Including/librecola.so/

* Path where the COLLIER modules ``*.mod`` are located::

    export COLLIER_INCLUDE_DIR=/PATH/TO/MODULES/

****

PYRECOLA paths
--------------

* Path where RECOLA python library ``pyrecola.so`` is located::
  
    export PYRECOLA_PATH=/Folder/Including/pyrecola.so/

* Path to PYRECOLA sources::

    export PYRECOLA_SOURCE_PATH=/PATH/TO/SOURCES/

****

REPT1L paths
------------

* Path to the REPT1L config file::

    export REPT1L_CONFIG="$HOME/.config/rept1l"

* Path to the REPT1L repository::

    export REPT1L_PATH=/PATH/TO/REPT1L/

It is useful to export REPT1L's path to the *PYTHONPATH* for unrestricted
access::

    export PYTHONPATH=$PYTHONPATH:"$(dirname "$REPT1L_PATH")"
    export PYTHONPATH=$PYTHONPATH:$REPT1L_CONFIG

****

FORM paths
----------

In order for FORM to find procedures the *FORMPATH* needs to be extended

* Path to where FORM procedures, usually `$REPT1L_PATH/formutils`::

    export FORMPATH=/PATH/TO/FORM/PROCEDURES

If a local version of FORM is kept it is uselful to export the path containing the binary to PATH (not mandatory)

* Path where FORM binary ``form`` is located::

    export PATH=$PATH:/PATH/TO/FORM

****

REPT1L config
-------------

At runtime rept1l needs access to the config file rept1l_config.py which
should be added to the *PYTHONPATH*. Currently, the config can have the
following settings:


.. rst:role:: check_massless_renormalization(bool)

  Check if particle renormalization is compatible without a mass counterterm

    True by default


.. rst:role:: complex_mass_scheme(int)

  Enable complex-mass scheme for unstable particles.

    True by default


.. rst:role:: realmomentum

  Perform on-shell renormalization for real-momentum. Should be used in
  combination with the complex-mass scheme.

    True by default

.. rst:role:: couplings_per_mod(int)

  Control the number of couplings which are written to one module file

    200 by default


.. rst:role:: ctcouplings_per_mod(int)

  Control the number of counterterm couplings which are written to one module file

    50 by default

.. rst:role:: vertices_per_mod(int)

  Control the number of vertices which are written to one module file

    800 by default

.. rst:role:: symmetrize_currents(bool)

  Decide whether off-shell current rules are symmetrized. Only affects rank
  increase higher than 1.

    False by default


.. rst:role:: formcmd(str)

  Path to the FORM executable used by REPT1L.

    by default form is searched in PATH

****

.. _dependencies:

Dependencies
============

Third-party tools
-----------------

REPT1L makes use of the following packages:

* `COLLIER <http://collier.hepforge.org/>`_
  ``1.0.0``.

* `FORM <https://github.com/vermaseren/form/>`_
  ``4.1.0``.

Python dependencies
-------------------

REPT1L requires several python packages:

* `cloudpickle <https://github.com/cloudpipe/cloudpickle>`_
  ``2.8.5`` or better.

* `sympy <http://www.sympy.org/en/index.html/>`_
  ``0.7.5``, ``0.7.6``, ``1.0.0`` or better.

  .. note:: This package will be replaced by the most recent version ``1.0.0`` which is not compatible with previous versions.

* `psutil <https://github.com/giampaolo/psutil>`_
  ``2.2.1`` or better.

* `nose <https://pypi.python.org/pypi/nose>`_
  ``1.3.3`` or better.

* `nose-exclude <https://pypi.python.org/pypi/nose-exclude>`_
  ``0.4.1`` or better.

* `nose-cov <https://pypi.python.org/pypi/nose-cov>`_
  ``1.6`` or better.

* `mock <https://pypi.python.org/pypi/mock>`_
  ``2.0`` or better.

* `python-form <https://pypi.python.org/pypi/python-form>`_
  ``0.1.0`` or better.
