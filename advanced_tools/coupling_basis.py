################################################################################
#                              coupling_basis.py                               #
################################################################################

from __future__ import print_function

""" Module provides generic simplify and caching methods. """


import os
from sympy import sympify, simplify, powdenest
from subprocess import PIPE, Popen
from rept1l.helper_lib import StorageProperty
from rept1l.helper_lib import string_subs
from rept1l.coupling import RCoupling as FRC
from rept1l.pyfort import ProgressBar
from rept1l.renormalize import export_expression
from six import with_metaclass

# prevent substituting cw,sw,... which will be undone anyways
FRC.get_internal_parameter_dict(treat_external=['cw', 'sw',
                                                'sa', 'ca',
                                                'ee',
                                                'sb', 'cb',
                                                'sh', 'ch',
                                                'sa1', 'ca1',
                                                'sa2', 'ca2',
                                                'sa3', 'ca3',
                                                ])


def mathematica_simplify(expr):
  """ Mathemtatica FullSimplify Wrapper. """
  powerrepl = {'**': '^'}
  x = string_subs(str(expr), powerrepl)
  tmpfile = 'simplifythis.m'
  with open(tmpfile, 'w') as f:
    f.write('out=(' + str(x) + ') // FullSimplify\n')
    f.write('Print[out]')

  cmd = ['MathematicaScript', '-script', tmpfile]
  p = Popen(cmd, stdin=PIPE, stdout=PIPE, stderr=PIPE)
  output, err = p.communicate()
  # rc = p.returncode
  try:
    os.remove(tmpfile)
  except Exception:
    pass
  output = output.split('\n')
  if len(output) == 2:
    output = output[0]
  elif len(output) == 3:
    output = output[1]
  else:
    print("x:", x)
    print("len(output):", len(output))
    print("output:", output)
    raise Exception('Failed to simplify expression in mathematica.')
  try:
    return sympify(output)
  except Exception:
    print("expr:", expr)
    print("output:", output)
    raise Exception('Failed to sympify expression')


##########################
#  make_expression_func  #
##########################

def make_expression_func(simpfunc):
  """ Defines a function which takes an expression `expr` and returns it
  expressed in terms of external parameters with additional simplification. """

  def expr_func(expr, cache=True, **kwargs):
    """ Expresses `expr` in terms of a predefined basis defined by `simpfunc` and
    simplifies the result. Retrieves cached results if not disabled. """
    if cache:
      return derive_expr(expr, None, simpfunc, **kwargs)
    else:
      return derive_expr(expr, CouplingCache, simpfunc, **kwargs)

  return expr_func


#######################
#  CouplingCacheMeta  #
#######################

class CouplingCacheMeta(StorageProperty):
  """ Meta storage for HS_cache providing get and set methods on the class
  itself. """
  def __contains__(cls, expr):
    if not hasattr(cls, 'cache_path'):
      from pyrecola import get_modelname_rcl, get_driver_timestamp_rcl
      cls.cache_path = get_driver_timestamp_rcl()
      print('Model: ' + get_modelname_rcl())
      print('Coupling cache path (gen timestamp): ' + cls.cache_path)

    if cls.cache_path not in cls.registry:
      cls.registry[cls.cache_path] = {}
    return expr in cls.registry[cls.cache_path]

  def __getitem__(cls, expr):
    if expr in cls:
      return cls.registry[cls.cache_path][expr]

  def __setitem__(cls, expr, value):
    # trigger that cls.cache_path is set
    if expr in cls:
      pass
    cls.registry[cls.cache_path][expr] = value


class CouplingCache(with_metaclass(CouplingCacheMeta)):
  """ Class which caches simplified results for couplings.

  The results are accessed via `CouplingCache[coupling]`. The system stores the
  cached results in the current working directory and makes sure that there is
  no collision if used with different models.
  """
  storage = ['registry']
  path = os.path.join(os.getcwd(), 'ModelCachedCouplings')
  name = 'cached_couplings.txt'

  def __init__(self, *args, **kwargs):
    super(CouplingCache, self).__init__(*args, **kwargs)


def derive_expr(expr, CacheClass, simpfunc, plotProgress=True,
                return_cdict=False, **kwargs):
  """ Translate a generic expression in terms of couplings and parameters to a
  simple expression in terms of parameters.

  :param expr: Symbolic expression in terms of UFO couplings
  :type  expr: Sympy

  """
  couplings = FRC.get_couplings_from_expr(expr)
  cdict = {}
  plotProgress = plotProgress and len(couplings)
  if CacheClass:
    CacheClass.load()
  if plotProgress:
    PG = ProgressBar(len(couplings))
  for c in couplings:
    if CacheClass and c in CacheClass:
      cdict[c] = CacheClass[c]
    else:
      cval = FRC.coupling_values[c.name]
      cval = simpfunc(cval, verbose=False)
      cdict[c] = cval
      if CacheClass:
        CacheClass[c] = cval
    if plotProgress:
      PG.advanceAndPlot(status=c.name)
  if return_cdict:
    return cdict
  ret = simpfunc(FRC.complex_mass_couplings(expr.subs(cdict)))
  if CacheClass:
    CacheClass.store()
  return ret


class SimplifyModelExpr(object):

  rules = {'default': []}

  def __init__(self, *rules, **kwargs):
    if 'setup' in kwargs:
      setup = kwargs['setup']
    else:
      setup = 'default'
    if setup not in self.rules:
      self.rules[setup] = []
    self.rules[setup].extend(list(rules))

  def __call__(self, *args, **kwargs):
    return self.simplify(*args, **kwargs)

  def update(self, *rules, **kwargs):
    """ Update the substitution rules. """
    if 'setup' in kwargs:
      setup = kwargs['setup']
    else:
      setup = 'default'
    for rpos, rule in enumerate(rules):
      if setup not in self.rules:
        self.rules[setup] = [u.copy() for u in self.rules['default']]
      assert(len(rules) == len(self.rules[setup]))
      self.rules[setup][rpos].update(rule)

  def simplify(self, expr, verbose=False, use_powdenest=True,
               use_mathematica=False, setup='default', **kwargs):
    """ Employs strategies to simplify the expressions of composite couplings.
    The strategy consists in substituting for parameters in the way
    and order defined by the substitution rules.  rulesew1, rulesew2, rulesew3.

    :param expr: Symbolic expression in terms of UFO couplings
    :type  expr: Sympy

    :param verbose: print stage of the simplification
    :type  verbose: bool
    """
    if verbose:
      print('Exporting expression')

    expr = simplify(export_expression(expr, plotProgress=verbose))
    if use_powdenest:
      powdenest_func = powdenest
    else:
      powdenest_func = lambda x, **kwargs: x

    for i in range(len(self.rules[setup]) - 1):
      exprs = expr.subs(self.rules[setup][i])
      expr = simplify(powdenest_func(exprs, force=True))

    exprs = expr.subs(self.rules[setup][-1])
    if use_mathematica:
      expr = mathematica_simplify(powdenest_func(exprs, force=True))
    else:
      expr = simplify(powdenest_func(exprs, force=True))
    return expr
