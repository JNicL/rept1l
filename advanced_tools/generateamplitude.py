#==============================================================================#
#                             generateamplitude.py                             #
#==============================================================================#

""" This source code demonstrates how to generate analytic expressions for
amplitudes with pyrecola and how the results can be further processed with
rept1l. """


# PYRECOLA is a python interface to RECOLA. The generation of processes is done
# in the same way as in Fortran, i.e. first the processes are declared and then
# generated. In the following it is demonstrated how to derive the expression
# for a selfenergy topology including tadpoles. The necessary methods are
# imported as follows

from pyrecola import (define_process_rcl,
                      generate_processes_rcl,
                      set_lp_rcl,
                      set_form_rcl,
                      set_draw_level_branches_rcl,
                      set_compute_selfenergy_rcl,
                      set_compute_tadpole_rcl,
                      set_masscut_rcl,
                      reset_recola_rcl)

# As in RECOLA the process is defined with the same syntax.
process = 'Hl -> Hl'

define_process_rcl(1, process, 'NLO')
set_masscut_rcl(0.)

# We are only interested in the bare loop part of the amplitude and we disable
# tree (which is zero anyway), counterterm and R2 rational terms. The rational
# terms can be generated afterwards since the amplitude is derived in d
# dimensions
set_lp_rcl(0, True)  # Tree
set_lp_rcl(1, True)  # Bare-Loop
set_lp_rcl(2, False)  # CT
set_lp_rcl(3, False)  # R2

set_draw_level_branches_rcl(1)

# By default RECOLA filters for selfenergies on external legs and we need to
# enable the selfenergy in this case.
# compute_selfenergy(True)

# Similar, by default tadpoles are filtered and we need to enable them
set_compute_tadpole_rcl(True)
set_compute_selfenergy_rcl(False)

# We choose the form output in which only the bare amplitude is written without
# polarization and without performing any other computation
set_form_rcl(4)

# Finally, the process is generated
generate_processes_rcl()

# and recola is deallocated
reset_recola_rcl()

# and a FORM process file is written to the working directory named process1.log
# The index is the same as the process index defined in `define_process_rcl`

# In rept1l pyrecola is considered as low-level library and high-level classes
# are available to achieve what is done before in a simple way. One class which
# is used throughout rept1l is FormProcess. This class is used to generate the
# one-particle irreducible amplitudes for the computation of counterterms and
# rational terms in a fully automated way. In addition to what is done before,
# FormProcess also calls FORM to evaluate the FORM process file directly. Note
# that FormProcess supresses all the standard ouput from RECOLA.
# FormProcess is imported follows

from rept1l.parsing import FormProcess

# The process is defined defined either by particles or by vertices(from the UFO
# modelfile). In case the user provides particles the user has to stick to the
# process definition conventions: If the number of particles is 2 or 3, the
# first particle is considered in the initial state and the second (and possibly
# third) particle is considered in the final state. For more than three
# particles the first two are considered in the initial state and all other
# particles are considered in the final state.

# The particles
particles = ['W+', 'W+']
# defines the process W+ -> W+. Note that the generated amplitude has
# different conventions from the conventions how the process is defined. In the
# FORM amplitudes all particles are considered as incoming!

# We can reproduce the result from before with same setup as follows
level_process = {'tree': False, 'bare': True, 'ct': False, 'r2': False}
process_defs = {'compute_tadpole': True, 'compute_selfenergy': True,
                'level_process': level_process}

# and the process is generated and evaluated with
with FormProcess(particles=particles, **process_defs) as processed_file:
  print("processed_file:", processed_file)

# the `with` environment is useful if one wishes to remove any files produced by
# FORM afterwards.
