#==============================================================================#
#                             exportexpression.py                              #
#==============================================================================#
""" This source code demonstrates how internal expressions in rept1l can be
exported and used outside of rept1l. """

# If using the high-level functions of rept1l importing the explicitly Model is
# not need and done automatically, however, it is necessary to set the
# `REPTIL_MODEL_PATH` as described in `importmodel.py`.


# Different objects can be exported, starting from parameter (values/properties)
# to counterterm expressions or whole amplitudes.


# We start with couplings of the model. Couplings in rept1l are stored as in the
# RCoupling class. Internall this class takes care of registering the
# couplings and computing the corresponding order in the fundamental couplings
# if non is given. It also allows to define new couplings with a recursive
# dependence on other couplings and paramters.

# The class is imported as follows (abbreviated as FRC)
from rept1l.coupling import RCoupling as FRC

# The runtime dependent variables are stored with the help of the Storage
# methods in helper_lib. The corresponding attributes can be seen by

print("FRC.storage:", FRC.storage)

# which should return a list of variables (of type dictonary):
# _orders, _coupling_values, _coupling_orders _order_value_coupling,
# _coupling_dict, _all_couplings, _coupling_values_ext

# The leading underscore indicates that the variables are private. The variables
# are correclty accessed without the underscore  as follows

cv = FRC.coupling_values
co = FRC.coupling_orders

# which guarantees that the cached coupling values are loaded from the disk.

# The RCoupling Storage attributes are all dictionaries, i.e.
# information is retrieved by providing a suited key and the corresponding value
# is returned. Most keys have to be of type Symbol

# The symbol for the coupling GC_1 (which should be present in any model) is
# defined with sympy's Symbol class
from sympy import Symbol
GC_1 = Symbol('GC_1')

try:
# the value for the coupling can then be accessed via
  print("cv[GC_1]:", cv[GC_1])

# the value order can be accessed via
  print("co[GC_1]:", co[GC_1])

except KeyError as e:
  print("The coupling GC_1 is not present in your model.")
except Exception as e:
  print("Unexpected error:", e)


# In a similar way counterterm parameter can be accessed. The corresponding
# class is RenoConst (abbreviated as RC)

from renormalize import RenoConst as RC

# And RenoConst is also equipped with a Storage.

print("RC.storage:", RC.storage)

# which returns `_registry`. The registry is a nested dictionary of the form
# RC.registry = {'onshell': renos1, 'MS': renos2, ...}
# where the keys are renormaliztion schemes of type string as declared by the
# user when defining the renormalization condition. The keys renos1, renos2 and
# so on are again dictionaries of the form
# renos1 = {dMW2: {'active': True, 'basename': 'dMW2', 'renoscheme': 'onshell',
#                  'value': ...},
#            ...
# The registry is accessed as follows
print("RC.registry:", RC.registry)

# The keys for the actual renormalization constants are again of type Symbol.
# There are multiple ways to obtain the value for dMW2. The direct, but lengthy
# way is to go access it via the registry

dMW2 = Symbol('dMW2')
try:
  dMW2_dict = RC.registry['onshell'][dMW2]
  dMW2_value = dMW2_dict['value']

  print("dMW2_value:", dMW2_value)

except KeyError as e:
  print("The counterterm dMW2 has not been derived.")
except Exception as e:
  print("Unexpected error:", e)


# Usually expression depend on couplings introduced by rept1l and are therefore
# not useful outside of rept1l. It is possible to export the expression where
# all couplings are replaced by external parameters. The method which can do
# this is part of the Renormalize class (abbreviated as R)
from rept1l.renormalize import export_expression

# For instance we may export the coupling GC_1
try:
# the value for the coupling can then be accessed via
  GC_1_val = cv[GC_1]
  GC_1_exp = export_expression(GC_1_val)
  print("GC_1_exp:", GC_1_exp)
except KeyError as e:
  print("The coupling GC_1 is not present in your model.")
except Exception as e:
  print("Unexpected error:", e)


# We can also export the counterterm dMW2
try:
  dMW2_value = dMW2_dict['value']
  dMW2_exp = export_expression(dMW2_value)
  print("dMW2_exp:", dMW2_exp)
except KeyError as e:
  print("The counterterm dMW2 has not been derived.")
except Exception as e:
  print("Unexpected error:", e)


# If the counterterm depends on scalar integrals additional information is
# needed from rept1l. Scalar integrals are represented by symbols T1, T2, and
# so on. The actual integral is accessed from the the  Regularize class
# (abbreviated as RR)
from rept1l.regularize import Regularize as RR

print("RR.tens_dict:", RR.tens_dict)

# this can be used to print all the types and arguments of the (tensor)scalar
# integrals appearing in dMW2_value

# We can also export the counterterm dMW2
try:
  dMW2_value = dMW2_dict['value']
  sis = [u.name for u in dMW2_value.free_symbols if u.name.startswith('T')]
  for si in sis:
    si_np, si_id, si_args = RR.tens_dict[si]
    print("scalar integral npoint:", si_np)
    print("scalar integral id:", si_id)
    print("scalar integral arguments:", si_args)

except KeyError as e:
  print("The counterterm dMW2 has not been derived.")
except Exception as e:
  print("Unexpected error:", e)
