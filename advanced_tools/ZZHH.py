################################################################################
#                                   ZZHH.py                                    #
################################################################################

""" Check the S-matrix element Z Z -> H H for UV finiteness """

from __future__ import print_function

#============#
#  Includes  #
#============#

import os
from sympy import Symbol, factor, Poly, simplify
from rept1l.parsing import FormProcess
from rept1l.parsing import ParseAmplitude
from rept1l.renormalize import export_expression
from rept1l.helper_lib import flatten
from rept1l.counterterms import Counterterms
from rept1l.coupling import RCoupling as FRC
from rept1l.renormalize import RenoConst as RC

import rept1l.Model as Model
model = Model.model
P = model.P

#=============#
#  Procedure  #
#=============#

class OperatorMS(FormProcess):

  def __init__(self, **process_definitions):
    level_process = {'tree': False, 'bare': True, 'ct': False, 'r2': False}
    # In mode 4 all momenta are incoming except for the last one!
    form_amplitude = 4
    process_definitions['level_process'] = level_process
    process_definitions['form_amplitude'] = form_amplitude
    process_definitions['all_couplings_active_frm'] = False
    order_NLO = {'QED': 4}
    process_definitions['order_NLO'] = order_NLO
    process_definitions['suppress_gen_stdout'] = False
    super(OperatorMS, self).__init__(**process_definitions)

class OperatorCT(FormProcess):

  def __init__(self, **process_definitions):
    level_process = {'tree': False, 'bare': False, 'ct': True, 'r2': False}
    # In mode 4 all momenta are incoming except for the last one!
    form_amplitude = 4
    process_definitions['level_process'] = level_process
    process_definitions['form_amplitude'] = form_amplitude
    process_definitions['all_couplings_active_frm'] = False
    process_definitions['suppress_gen_stdout'] = False
    super(OperatorCT, self).__init__(**process_definitions)

#------------------------------------------------------------------------------#

class MSAmplitude(ParseAmplitude):

  def __init__(self, particles, **kwargs):
    super(MSAmplitude, self).__init__(suppress_gen_stdout=False)
    self.particles = particles

  def process_loopamp(self, amp):
    amp = self.compute_MS(amp)

    from rept1l.formutils import FormPype

    FP = FormPype()
    exprs = []
    exprs.extend(['l amp = 16*pi^2*(' + amp + ');'])
    exprs.extend(['id p5 = p1+p4;',
                  'id p3 = p1+p2;',
                  'id p6 = p2+p4;',
                  'id p7 = p1+p2+p4;',
                  '.sort',
                  'id p1(mu1) = 0;',
                  'id p2(mu2) = 0;',
                  'id p2.p1 = p1.p2;',
                  'id p1.p1 = cMZ^2;',
                  'id p2.p2 = cMZ^2;',
                  'id p4.p4 = cMH^2;',
                  '.sort'
                  ])
    exprs.extend(['id n = 4;',
                  '.sort'])
    FP.eval_exprs(exprs)
    amp = ''.join(FP.return_expr('amp', stop=True).split())
    return amp

  def process_ctamp(self, amp):
    """ Processing the counterterm FORM amplitudes. """
    from rept1l.formutils import FormPype

    FP = FormPype()
    exprs = []
    exprs.extend(['l amp = 16*pi^2*(' + amp + ');'])
    exprs.extend(['id p5 = p1+p4;',
                  'id p3 = p1+p2;',
                  'id p6 = p2+p4;',
                  'id p7 = p1+p2+p4;',
                  '.sort',
                  'id p1(mu1) = 0;',
                  'id p2(mu2) = 0;',
                  'id p2.p1 = p1.p2;',
                  'id p1.p1 = cMZ^2;',
                  'id p2.p2 = cMZ^2;',
                  'id p4.p4 = cMH^2;',
                  '.sort',
                  ])

    FP.eval_exprs(exprs)
    amp = ''.join(FP.return_expr('amp', stop=True).split())
    return amp

  def process_sumamp(self, amp):
    """ Processing the bare-loop FORM amplitudes. """
    from rept1l.regularize import Regularize
    amp = self.assign_lorentz(amp)
    amp = self.assign_tensors(amp, Regularize.tens_dict)
    amp = self.assign_propagator(amp)
    amp = self.parse_UFO(amp)
    return amp


class SMatrix(object):

  def __init__(self, particles, **kwargs):
    print('Generating loop amplitude')
    OMS = OperatorMS(particles=particles, **kwargs)
    print('Extracting ms part and parsing')
    MSA = MSAmplitude(particles)
    print("OMS.processed_file:", OMS.processed_file)
    MSA.parse(OMS.processed_file)
    self.amplitude = MSA.amplitudes[(1, '4')]
    self.ls_dict = MSA.ls_dict

    print('Generating ct amplitude')
    OMS = OperatorCT(particles=particles, **kwargs)
    print('parsing ct amplitude')
    MCT = MSAmplitude(particles, ls_dict=self.ls_dict)
    MCT.parse(OMS.processed_file)
    self.CT = sum(MCT.amplitudes.values())
    self.prop_dict = MSA.prop_dict

  def reveal_CTs(self):
    """ Reveals the counterterm dependence in the ct amplitude """
    c_set = FRC.get_couplings_from_expr(self.CT)
    all_ct = [u for u in Counterterms.get_all_ct()]
    coupl_repl = {c: FRC.find_expression(all_ct, c) for c in c_set}
    self.CT_reveal = self.CT.subs(coupl_repl)

    check_amps = [self.CT_reveal]
    ct = set(list(flatten([[v for v in u.free_symbols if v in all_ct]
                           for u in check_amps])))
    self.ct_dependence = ct


if __name__ == "__main__":

  ps = [P.Z, P.Z, P.H, P.H]
  OP = SMatrix(ps)
  OP.reveal_CTs()

  # construction of ct -> UV pole dictionary
  cts = OP.ct_dependence
  s = RC.build_ms_ct_solutions(cts)
  Dsubs = {Symbol('DeltaUV'): 2/Symbol('ep')}
  cts = cts.union(set(s.values()))
  for ct in cts:
    if ct not in s:
      s[ct] = RC[ct]['value'].subs(Dsubs)
    elif ct == s[ct]:
      s[ct] = RC[ct]['value'].subs(Dsubs)

  def eliminate_cts_dep(ctval, cts, subsdict):
    for u in ctval.free_symbols:
      if u in cts:
        ctval = ctval.subs(subsdict)
        return eliminate_cts_dep(ctval, cts, subsdict)
    return ctval
  s = {k: export_expression(eliminate_cts_dep(v, cts, s)) for k, v in s.iteritems()}


  # check for UV finiteness
  amp = OP.amplitude + OP.CT
  prop_base = [Symbol(u) for u in OP.prop_dict.keys()]
  lorentz_base = [Symbol(u) for u in OP.ls_dict.keys()]
  ampp = Poly(amp, prop_base).as_dict()
  for k in ampp:
    ampl = Poly(ampp[k], lorentz_base).as_dict()
    denprefac = ' '.join(['den(' + ','.join(OP.prop_dict[prop_base[posu].name]) +
                            ')^' + str(u) for posu, u in enumerate(k) if u > 0])
    print(denprefac, " x ", simplify(export_expression(ampp[k]).subs(s)))

  print("OP.prop_dict:", OP.prop_dict)
  for u in sorted(OP.ls_dict.keys()):
    print( u + ":", OP.ls_dict[u])
  print("If the goldstone boson G0 is not renormalized, " +
        "further simplification is required!")
  print("Use identities like `Den(p6,MZ)^-1 = (p_6^2 -MZ^2) = MH^2 + 2 P2.P4.`")
