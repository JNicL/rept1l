################################################################################
#                                nthdm_basis.py                                #
################################################################################

from __future__ import print_function

""" Simpliciation function for the NTHDM. """

#===========#
#  Imports  #
#===========#

from rept1l.renormalize import export_expression
from rept1l.advanced_tools.coupling_basis import make_expression_func
from rept1l.advanced_tools.coupling_basis import mathematica_simplify
from sympy import symbols, sqrt, simplify
from sympy import powdenest, pi

#=====================#
#  Globals & Methods  #
#=====================#

# SM parameters
cMZ, cMW, sw, cw, alpha, e, gw = symbols('cMZ cMW sw cw aEW e gw')
alphaS, gs = symbols('aS gs')
# NTHDM parameters
sa1, ca1, sa2, ca2, sa3, ca3, sb, cb = symbols('sa1 ca1 sa2 ca2 sa3 ca3 sb cb')

# In a first step alpha, cMZ and cw are replaced by the external parameters gw,
# sw, and cMW. The procedure prefers `sw` over `cw`.
rulesew1 = {alpha: (gw * sw)**2 / (4 * pi),
            alphaS: (gs)**2 / (4 * pi),
            cMZ: cMW / sqrt(-sw**2 + 1),
            cw: sqrt(-sw**2 + 1),
            sb: sqrt(-cb**2 + 1),
            ca1: sqrt(-sa1**2 + 1),
            ca2: sqrt(-sa2**2 + 1),
            ca3: sqrt(-sa3**2 + 1),
            }

# in the next step square roots of sw are removed
rulesew2 = {sqrt(-sw**2 + 1): cw,
            sqrt(-cb**2 + 1): sb,
            sqrt(-sa1**2 + 1): ca1,
            sqrt(-sa2**2 + 1): ca2,
            sqrt(-sa3**2 + 1): ca3,
            }

# Further obvious simplifications are performed
rulesew3 = {-sw**2 + 1:  cw**2,
             sw**2 - 1: -cw**2,
            -cb**2 + 1:  sb**2,
             cb**2 - 1: -sb**2,
            -sa1**2 + 1: ca1**2,
            sa1**2 - 1: -ca1**2,
            -sa2**2 + 1: ca2**2,
            sa2**2 - 1: -ca2**2,
            -sa3**2 + 1: ca3**2,
            sa3**2 - 1: -ca3**2,
            }


###############
#  simpnthdm  #
###############

def simpnthdm(expr, verbose=False, use_powdenest=True, use_mathematica=False):
  """ Employs a strategie to simplify the trigonometric structures sw^+cw^2=1
  in the NTHDM. The strategy consists in susbstiting for parameters in the way
  and order defined by the substitution dicts rulesew1, rulesew2, rulesew3.

  :param expr: Symbolic expression in terms of UFO couplings
  :type  expr: Sympy

  :param verbose: print stage of the simplification
  :type  verbose: bool
  """
  if verbose:
    print('Exporting expression')

  if use_mathematica:
    simpfunc = mathematica_simplify
  else:
    simpfunc = simplify

  expr = simpfunc(export_expression(expr, plotProgress=verbose))
  if use_powdenest:
    powdenest_func = powdenest
  else:
    powdenest_func = lambda x, **kwargs: x

  expr = simpfunc(powdenest_func(expr.subs(rulesew1), force=True))
  expr = simpfunc(powdenest_func(expr.subs(rulesew2), force=True))
  expr = simpfunc(powdenest_func(expr.subs(rulesew3), force=True))
  return expr


################
#  expr_NTHDM  #
################

expr_NTHDM = make_expression_func(simpnthdm)


def main():
  from sympy import sympify
  t = sympify('MW/MZ')
  t = simpnthdm(t, verbose=False)
  print("t:", t)


if __name__ == '__main__':
  main()
