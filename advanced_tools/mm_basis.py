###############################################################################
#                                 hs_basis.py                                 #
###############################################################################

from __future__ import print_function

""" Translates any MM model expression given in terms of couplings and
parameters to a simple basis with external parameters:
  cMZ,
  ge ,
  sa ,
  ca ( = sqrt(1-ca^2) ),
  tb


Simplified expressions for couplings are cached (individually for different
rept1l model paths) and retrieved upon new occurence.
"""

#===========#
#  Imports  #
#===========#

import os
from rept1l.renormalize import export_expression
from rept1l.advanced_tools.coupling_basis import CouplingCache, derive_expr
from rept1l.advanced_tools.coupling_basis import mathematica_simplify
from sympy import symbols, sqrt, simplify, Symbol
from sympy import powdenest, pi, together, factor

#=====================#
#  Globals & Methods  #
#=====================#

# SM parameters
cMZ, ge = symbols('cMZ ge')
# HS parameters
sa, ca, sb, cb, tb = symbols('sa ca sb cb tb')

# In a first step alpha, cMZ and cw are replaced by the external parameters gw,
# sw, and cMW. The procedure prefers `sw` over `cw`.
rulesew1 = {ca: sqrt(-sa**2+1),
            sb: sqrt(-cb**2+1)
            }

# in the next step square roots of sw are removed
rulesew2 = {sqrt(-sa**2+1): ca,
            sqrt(-cb**2+1): sb,
           }

# Further obvious simplifications are performed
rulesew3 = {}

#############
#  expr_MM  #
#############

def expr_MM(expr, **kwargs):
  """ Expresses expr in terms of a predefined basis and simplifies the result. """
  return derive_expr(expr, CouplingCache, simpmm, **kwargs)

############
#  simpmm  #
############

def simpmm(expr, verbose=False, use_powdenest=True, use_mathematica=False):
  """ Employs a strategie to simplify the trigonometric structures sa^+ca^2=1
  in the MM.

  :param expr: Symbolic expression in terms of UFO couplings
  :type  expr: Sympy

  :param verbose: print stage of the simplification
  :type  verbose: bool
  """
  if verbose:
    print('Exporting expression')

  expr = simplify(export_expression(expr, plotProgress=verbose))
  if use_powdenest:
    powdenest_func = powdenest
  else:
    powdenest_func = lambda x, **kwargs: x
  expr = simplify(powdenest_func(expr.subs(rulesew1), force=True))
  expr = simplify(powdenest_func(expr.subs(rulesew2), force=True))
  if use_mathematica:
    expr = mathematica_simplify(powdenest_func(expr.subs(rulesew3), force=True))
  else:
    expr = simplify(powdenest_func(expr.subs(rulesew3), force=True))
  return expr
