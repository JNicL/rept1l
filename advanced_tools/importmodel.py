#==============================================================================#
#                                importmodel.py                                #
#==============================================================================#
""" This source code demonstrates how UFO modelfiles are dynamically loaded by
rept1l and how the model specific information are accessed and further
processed. """


# The UFO modelfile is loaded by import rept1l.Model.
import rept1l.Model as Model
model = Model.model

# this requires the environment variable `REPTIL_MODEL_PATH` to be set to a UFO
# modelfile path. In case the env variable is not set rept1l will raise the
# exception

"Exception: No Model set active. Set REPTIL_MODEL_PATH to the full path of the model. "

# It is important that `REPTIL_MODEL_PATH` points to a folder where the
# topfolder of the UFO modelfile is located. The folder containing the UFO
# modelfile is populated with additional libraries

# In case `REPTIL_MODEL_PATH` is not set correctly rept1l will raise the
# exception

"Exception: Tried to import the model from `SOME PATH`, but failed. Make sure there is a valid Modelfile."

# Model specific objects can be accessed by the object_library attribute

# all particles of the models can be access via
print(model.object_library.all_particles)

# all parameters of the models can be access via
print(model.object_library.all_parameters)

# all couplings of the models can be access via
print(model.object_library.all_couplings)

# all vertices of the models can be access via
print(model.object_library.all_vertices)

# all lorentz of the models can be access via
print(model.object_library.all_lorentz)

# See the global variables of object_library for more attributes
