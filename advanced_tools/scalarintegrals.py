###############################################################################
#                             scalarintegrals.py                              #
###############################################################################
""" This source code demonstrates how the internal form expression were derived
allowing to compute rational terms in effective theories due to higher rank.
"""

from rept1l.rational.divintegral import DivTensorCoeff


# Building the divergent part of the 3-point integral up to rank 4
N = 3
for rank in range(N+2):
  print(''.join(DivTensorCoeff.build_form_id(N, rank)))

print ('\n')
print ('\n')

# Building the tensor decomposition for rank 6 with n =3 (3 g^\mu\nu), e.g.
# C_000000
print (' + '.join(str(u) for u in DivTensorCoeff.
       build_form_coeff(6, (0, 0, 0, 0, 0))))

print ('\n')
print ('\n')

# Building the tensor decomposition for rank 6 with n = 1 and two different
# momenta of equal power 2 (g^\mu\nu p_1^al p_1^be p_2^\tau p p_2^\omega), e.g.
# C_001122
print (' + '.join(str(u) for u in DivTensorCoeff.
       build_form_coeff(6, (2, 2, 0, 0, 0))))
