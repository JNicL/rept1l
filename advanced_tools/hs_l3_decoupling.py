################################################################################
#                             hs_l3_decoupling.py                              #
################################################################################

"""  """
#============#
#  Includes  #
#============#

import os
from sympy import Symbol, Poly, limit, oo, symbols, sympify, pi, I, simplify
from subprocess import PIPE, Popen
from rept1l.parsing import ParseAmplitude
from rept1l.parsing import FormProcess
from rept1l.regularize import Regularize as RR
from rept1l.vertices_rc import Vertex
from rept1l.renormalize import export_expression

import rept1l.Model as Model
model = Model.model
P = model.P

class DeriveBareSelfenergy(FormProcess):
  """ Derives a bare selfenergy for a given particle combination at order QED^2.
  """
  def __init__(self, **process_definitions):
    level_process = {'tree': False, 'bare': True, 'ct': False, 'r2': False}
    form_amplitude = 4
    process_definitions['level_process'] = level_process
    process_definitions['form_amplitude'] = form_amplitude
    process_definitions['all_couplings_active_frm'] = False
    order_NLO = {'QCD': 0, 'QED': 2}
    process_definitions['order_NLO'] = order_NLO
    super(DeriveBareSelfenergy, self).__init__(**process_definitions)

class ParseBareSelfenergy(ParseAmplitude):
  """ Parses a form expression, reducing to 4 dimensions, performing PV and
  replacing all A0 function with zero momentum B0 functions.
  """
  optional = ParseAmplitude.optional
  optional['PassarinoVeltman'] = True
  optional['compute_r2'] = True
  optional['A0_to_B0'] = False
  optional['zeromom_propagator'] = True

  def __init__(self, **kwargs):
    super(ParseBareSelfenergy, self).__init__()

class BareSelfenergy(object):
  """ Derives the bare selfenergy for a given particle combination at order
  QED^2 and parses the expression to SymPy. """
  def __init__(self, particles, **kwargs):
    DBS = DeriveBareSelfenergy(particles=particles, **kwargs)
    PBS = ParseBareSelfenergy()
    PBS.parse(DBS.processed_file)
    self.amplitude = PBS.amplitudes[(1, '0,2')]
    self.ls_dict = PBS.ls_dict
    self.tens_dict = RR.tens_dict


def derive_poly_energy(particles, include_tadpole=False):
  fac = 16*pi**2/I
  BSE = BareSelfenergy(particles, compute_tadpole=False)
  amp = BSE.amplitude
  #amp = simplify(fac*amp)
  tens = [u.name for u in amp.free_symbols if u.name.startswith('T')]
  if include_tadpole:
    BSET = BareSelfenergy(particles, compute_tadpole=True)
    ampT = BSET.amplitude
    #ampT = simplify(fac*ampT)
    tensT = [u.name for u in ampT.free_symbols if u.name.startswith('T')]
    tensd = {u: BSE.tens_dict[u] for u in BSE.tens_dict
             if u in tens and u in tensT}
    assert(all(u in tensd for u in tensT))
    assert(all(u in tensd for u in tens))
  else:
    tensd = {u: BSE.tens_dict[u] for u in BSE.tens_dict if u in tens}
    assert(all(u in tensd for u in tens))

  tens_base = [Symbol(u) for u in tensd]

  ampP = Poly(amp*fac, tens_base)
  if include_tadpole:
    ampTP = Poly(ampT*fac, tens_base)
    amp = ampP + ampTP
  else:
    amp = ampP
  return amp, tensd


def find_B0(mass1, mass2, tens_dict):
  for u in tens_dict:
    if tens_dict[u][0] == 'B':
      _, m1, m2 = tens_dict[u][2]
      if m1 == mass1 and m2 == mass2:
        return Symbol(u)


hhs, tenshhs = derive_poly_energy([P.H, P.HS], include_tadpole=False)
print("tenshhs:", tenshhs)
hh, tenshh = derive_poly_energy([P.H, P.H], include_tadpole=False)
print("tenshh:", tenshh)
hshs, tenshshs = derive_poly_energy([P.HS, P.HS], include_tadpole=False)
print("tenshshs:", tenshshs)



l1, l2, l3 = symbols('l1 l2 l3')

degscen = {l2: l1, l3: 0}
B0MZMZ = find_B0('MZ', 'MZ', tenshhs)
chhs = hhs.coeff_monomial(B0MZMZ)
B0MZMZ = find_B0('MZ', 'MZ', tenshh)
chh = hh.coeff_monomial(B0MZMZ)
B0MZMZ = find_B0('MZ', 'MZ', tenshshs)
chshs = hh.coeff_monomial(B0MZMZ)

print (simplify(export_expression(chhs, external=False).subs(degscen)))
print (simplify(export_expression(chh, external=False).subs(degscen)))
print (simplify(export_expressi