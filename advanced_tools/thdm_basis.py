################################################################################
#                                thdm_basis.py                                 #
################################################################################

from __future__ import print_function

""" Simpliciation function for the THDM. """

#===========#
#  Imports  #
#===========#

from rept1l.advanced_tools.sm_basis import expr_SM
from sympy import symbols, sqrt

#=====================#
#  Globals & Methods  #
#=====================#

# 2HDM parameters
sa, sb, ca, cb = symbols('sa sb ca cb')
# Alignment parametrization
cab, tb, sab = symbols('cab tb sab')

rulesew1 = {sb: sqrt(-cb**2 + 1),
            ca: sqrt(-sa**2 + 1),
            }
rulesew2 = {sqrt(-cb**2 + 1): sb,
            sqrt(-sa**2 + 1): ca,
            }

rulesew3 = {-cb**2 + 1: sb**2,
            cb**2 - 1: -sb**2,
            -sa**2 + 1: ca**2,
            sa**2 - 1: -ca**2,
            }


# apply basis transformation
# We assume sab < 0, therefore sab -> - sqrt(1-cab**2)
rulesa1 = {cb: 1 / sqrt(1 + tb**2),
           sb: tb / sqrt(1 + tb**2),
           ca: cab / sqrt(1 + tb**2) - sab * tb / sqrt(1 + tb**2),
           sa: sab / sqrt(1 + tb**2) + cab * tb / sqrt(1 + tb**2),
           }

rulesa2 = {-sab**2 + 1: cab**2,
            sab**2 - 1: -cab**2,
           -cab**2 + 1: sab**2,
            cab**2 - 1: -sab**2,
           }

###############
#  expr_THDM  #
###############

expr_THDM = expr_SM
expr_THDM.update(rulesew1, rulesew2, rulesew3)
expr_THDM.update(rulesa1, rulesa2, {}, setup='alignment')


def main():
  from sympy import sympify
  t = sympify('ca/sb')
  t = expr_THDM(t, verbose=False, setup='alignment')
  print("t:", t)


if __name__ == '__main__':
  main()
