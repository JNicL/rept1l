################################################################################
#                              vertex_tadpoles.py                              #
################################################################################


from __future__ import print_function

""" Derives the full tadpole dependence of a self-energy or vertex function. """

from sympy import Symbol
from rept1l.parsing import ParseAmplitude
from rept1l.parsing import FormProcess
from rept1l.regularize import Regularize as RR
from rept1l.renormalize import RenoConst as RC
from rept1l.renormalize import Renormalize as RE
from rept1l.renormalize import export_expression
from rept1l.coupling import RCoupling as FRC
from rept1l.counterterms import Counterterms
from rept1l.helper_lib import flatten
import rept1l.Model as Model

model = Model.model
P = model.P
m = model.modelname
if m == 'SM':
  from rept1l.advanced_tools.sm_basis import expr_SM
  export_func = expr_SM
elif m == 'HS':
  from rept1l.advanced_tools.hs_basis import expr_HS
  export_func = expr_HS
elif m == 'THDM':
  from rept1l.advanced_tools.thdm_basis import expr_THDM
  export_func = expr_THDM
else:
  export_func = export_expression


#############
#  Methods  #
#############

class DeriveCTVertex(FormProcess):
  """ Derives FORM currents.  """
  def __init__(self, **process_definitions):
    # r2 is disabled since the expression is derived in d dimension.
    level_process = {'tree': False, 'bare': False, 'ct': True, 'r2': False}
    form_amplitude = 4
    process_definitions['level_process'] = level_process
    process_definitions['form_amplitude'] = form_amplitude
    # keep all couplings + full mass dependence
    process_definitions['all_couplings_active_frm'] = True
    process_definitions['masscut'] = 0.
    super(DeriveCTVertex, self).__init__(**process_definitions)


class ParseCTVertex(ParseAmplitude):
  """ Compute the FORM amplitude from the currents.  """

  optional = ParseAmplitude.optional

  def __init__(self, **kwargs):
    super(ParseCTVertex, self).__init__()

  def process_ctamp(self, amp):
    from rept1l.formutils import FormPype
    amp = ParseAmplitude.process_ctamp(self, amp)
    FP = FormPype()
    exprs = []
    exprs = ['l amp = (' + amp + ');', '.sort']
    FP.eval_exprs(exprs)
    amp = ''.join(FP.return_expr('amp', stop=True).split())
    return amp


class CTVertex(object):
  """ Parses the FORM amplitude to SymPy. """

  def __init__(self, particles, **kwargs):
    DBS = DeriveCTVertex(particles=particles, **kwargs)
    PBS = ParseCTVertex(**kwargs)
    PBS.parse(DBS.processed_file)
    print("PBS.amplitudes:", PBS.amplitudes)
    self.amplitudeCT = sum(PBS.amplitudes.values())
    self.ls_dict = PBS.ls_dict
    self.tens_dict = RR.tens_dict

  def reveal_CTs(self):
    """ Reveals the counterterm dependence in the ct amplitude """
    c_set = FRC.get_couplings_from_expr(self.amplitudeCT)
    all_ct = [u for u in Counterterms.get_all_ct()]
    coupl_repl = {c: FRC.find_expression(all_ct, c) for c in c_set}
    self.CT_reveal = self.amplitudeCT.subs(coupl_repl)

    check_amps = [self.CT_reveal]
    ct = set(list(flatten([[v for v in u.free_symbols if v in all_ct]
                           for u in check_amps])))
    self.ct_dependence = ct


if __name__ == "__main__":
  if m == 'HS' or m == 'THDM':
    tadpoles = ['dtHl', 'dtHh']
  elif m == 'SM':
    tadpoles = ['dt']
  else:
    tadpoles = ['?']
    raise Exception('Set tadpole expression here.')

  V = [P.Hh, P.Hl, P.Hl]
  VCT = CTVertex(V)

  VCT.reveal_CTs()
  cts = VCT.ct_dependence

  amp_ct = VCT.CT_reveal
  print("amp_ct:", amp_ct)

  subs_dt = {}

  # set some ct to zero
  # subs_dt['dtb'] = 0
  # subs_dt['da'] = 0
  # subs_dt['dZHlHh'] = 0
  # subs_dt['dZHhHl'] = 0
  subs_dt['dZee'] = 0

  cts_ext = [u for u in cts if u.name not in subs_dt]
  print("cts_ext:", cts_ext)

  for ct in cts_ext:
    if ct.name in tadpoles:
      continue
    val = RC[ct]['value'].expand()
    subs_dt[ct] = 0
    for t in tadpoles:
      dt_s = Symbol(t)
      val = RE.simpct2(val.coeff(dt_s))
      subs_dt[ct] += val * dt_s

  print("subs_dt:", subs_dt)
  print("cts_ext:", cts_ext)
  ctamp = RE.simpct2(amp_ct.subs(subs_dt), ctbase=[Symbol(u) for u in tadpoles])
  print('result: ', expr_HS(ctamp))

  # prefac = (export_func(amp_ct.coeff('dtb')))
  # print("prefac of dtb:", prefac)
