###############################################################################
#                                 sm_basis.py                                 #
###############################################################################

from __future__ import print_function

""" Translates any SM expression given in terms of couplings and parameters to
a simple basis with external parameters:
  cMW,
  cw ( = cMW/cMZ ),
  sw ( = sqrt(1-cw^2) )
  gw ( = e/sw).

Simplified expressions for couplings are cached (individually for different
rept1l model paths) and retrieved upon new occurence.
"""

#===========#
#  Imports  #
#===========#

from rept1l.advanced_tools.coupling_basis import SimplifyModelExpr
from sympy import symbols, sqrt, Symbol
from sympy import pi

#=====================#
#  Globals & Methods  #
#=====================#

cMZ, cMW, sw, cw, alpha, ee, gw = symbols('cMZ cMW sw cw aEW ee gw')
alphaS, gs = symbols('aS gs')
sq2 = Symbol('sq2')
rulesew1 = {alpha: (gw)**2 * sw**2 / (4 * pi),
            alphaS: (gs)**2 / (4 * pi),
            ee: (gw) * sqrt(1 - cw**2),
            sw: sqrt(1 - cw**2),
            cMZ: cMW / cw,
            sq2: sqrt(2),
            }

rulesew2 = {cw: sqrt(1 - sw**2),
            }

rulesew3 = {-cw**2 + 1: sw**2,
            cw**2 - 1: -sw**2,
            -sw**2 + 1: cw**2,
            sw**2 - 1: -cw**2
            }

#############
#  expr_SM  #
#############

expr_SM = SimplifyModelExpr(rulesew1, rulesew2, rulesew3)


def main():
  from sympy import sympify
  t = sympify('MW/MZ')
  t=sympify('+ sqrt(2)*dZee*ee*yms/(2*MW*sqrt(-MW**2/MZ**2 + 1))')
  t = expr_SM(t, verbose=False)
  print("t:", t)


if __name__ == '__main__':
  main()


