###############################################################################
#                              feynman_rules.py                               #
###############################################################################

from __future__ import print_function

""" Module for printing Feynman rules in human readable way. The information is
derived from the UFO Feynman rules. """

#############
#  Imports  #
#############

from sympy import Symbol
from rept1l.renormalize import export_expression
from rept1l.helper_lib import find_vertex
from rept1l.helper_lib import string_subs

import rept1l.Model as Model
m = Model.model.modelname
if m == 'HS':
  from rept1l.advanced_tools.hs_basis import expr_HS
  export_func = expr_HS
elif m == 'THDM':
  from rept1l.advanced_tools.thdm_basis import expr_THDM
  # export_func = expr_THDM
  export_func = lambda x, **kwargs: expr_THDM(x, setup='alignment', **kwargs)
elif m == 'GM':
  from rept1l.advanced_tools.gm_basis import expr_GM
  export_func = expr_GM
elif m == 'SM':
  from rept1l.advanced_tools.sm_basis import expr_SM
  export_func = expr_SM
else:
  raise Exception('Incompatible model')


#############
#  Methods  #
#############


def print_legend():
  """ Prints the legend for lorentz structures """
  print('All particles/momenta are outgoing!')
  print('d(i,j): \delta_{i,j}')
  print('g(i,j): \g_{{mu_i},{\mu_j}}')
  print('P(i,j): P^{\mu_i}_{j}')
  print('w^-(i,j): [(1-\gamma_5)/2]_(i,j)')
  print('w^+(i,j): [(1+\gamma_5)/2]_(i,j)')


def print_vertex(v, **kwargs):
  vexp = export_vertex(v, **kwargs)
  p = vexp['particles']
  print(len(p) * '-')
  print(p)
  print(len(p) * '-')
  for cstruc in vexp['cstructure']:
    print("    color:", cstruc['color'])
    print("structure:", cstruc['lstructure'])
    for ck, (_, cv) in cstruc['couplings'].iteritems():
      print('       ' + ck + ':', cv)


def export_vertex(v, **kwargs):
  plen = 14
  parr = ' '.join(p.name + '(' + str(pos + 1) + ')'
                  for pos, p in enumerate(v.particles))
  parr = parr + (plen - len(parr)) * ' ' + ':'
  color_sorted_vertex = []
  repl = {'Metric': 'g', 'Identity': 'd', 'ProjM': 'w^-', 'ProjP': 'w^+'}
  crepl = {'**': '^', 'cMW': 'MW', 'cMZ': 'MZ', 'cMH': 'MH', 'cMHS': 'MHS',
           'cMHC': 'MHC', 'cMHA': 'MHA', 'cMT': 'MT'}
  for cpos, color in enumerate(v.color):
    couplings = {}
    for lpos, lorentz in enumerate(v.lorentz):
      if (cpos, lpos) in v.couplings:
        coupling = v.couplings[(cpos, lpos)]
        cexp = str(export_coupling(coupling, **kwargs))
        couplings['C' + str(lpos + 1)] = (lpos, string_subs(cexp, crepl))
    expr = 0
    for c, (lpos, cval) in couplings.iteritems():
      ls = string_subs(v.lorentz[lpos].structure, repl)
      expr += Symbol('(' + ls + ')') * Symbol(c)
    cc = {'color': string_subs(color, repl), 'lstructure': str(expr),
          'couplings': couplings}
    color_sorted_vertex.append(cc)
  return {'particles': parr, 'cstructure': color_sorted_vertex}


def export_coupling(couplings, model='SM', verbose=False):
  from rept1l.coupling import RCoupling as FRC
  if type(couplings) is not list:
    cc = [couplings]
  else:
    cc = couplings
  valexps = []
  for coupling in cc:
    ccs = coupling.name
    val = FRC.coupling_values[ccs]
    valexp = export_func(val)

    if verbose:
      print("Result for " + ccs + ':')
      print("value:", valexp)
    valexps.append(valexp)
  return valexps


def fr_from_file(filepath, **kwargs):
  """ Prints all the Feynman rules for the vertices listed in the file
  `filepath` """
  print_legend()
  print()
  print()
  with open(filepath) as f:
    for line in sorted(f.readlines()):
      vs = find_vertex(line.split())
      for v in vs:
        print()
        print_vertex(v, **kwargs)


def fr_from_list(plist, **kwargs):
  """ Prints all the Feynman rules for the vertices in `plist` """
  print_legend()
  print()
  print()
  for line in plist:
    vs = find_vertex(line.split())
    for v in vs:
      print()
      print_vertex(v, **kwargs)


def filter_4p_3v_no_ferm_or_ghost(v):
  """ Filter for 4-point vertices with 3 vectors, excluding fermions or ghosts
  """
  p = v.particles
  if(len(p) == 4 and [u.spin == 1 for u in p].count(True) == 3 and
     not any(u.spin == 2 or u.spin == -1 for u in p)):
     return True
  else:
    False


def filter_3(v):
  """ Filter for 3-point vertices """
  p = v.particles
  if len(p) == 3:
    return True
  else:
    return False


def filter_4(v):
  """ Filter for 4-point vertices """
  p = v.particles
  if len(p) == 4:
    return True
  else:
    return False


def filter_3_scalars_3(v):
  """ Filter for 3-point pure scalar vertices """
  p = v.particles
  if len(p) == 3 and [u.spin == 0 for u in p].count(True) == 3:
    return True
  else:
    return False


def filter_3_scalars_1(v):
  """ Filter for 3-point with 1 scalar vertices """
  p = v.particles
  if len(p) == 3 and [u.spin == 0 for u in p].count(True) == 1:
    return True
  else:
    return False


if __name__ == "__main__":
  # fr_from_file('HSAA_HS.txt', model='HS')
  #c = ['Ha Z Z']
  import rept1l.Model as Model
  model = Model.model

  vs = model.model_objects.all_vertices

  # l = filter(filter_3_scalars_3, vs)
  # l = [' '.join(u.name for u in v.particles) for v in l]
  # l = sorted(l)
  # fr_from_list(l, model='HS')

  l = filter(filter_4, vs)
  l = [' '.join(u.name for u in v.particles) for v in l]
  l = sorted(l)
  fr_from_list(l, model='THDM')
