################################################################################
#                             se_bfm_difference.py                             #
################################################################################


""" Simple script to compute the difference of the mixing energy in the
conventional and BFM formulation.
"""
#============#
#  Includes  #
#============#

import os
from sympy import Symbol, sqrt, Poly, simplify
from pyrecola import get_modelname_rcl, get_driver_timestamp_rcl
from rept1l.parsing import ParseAmplitude
from rept1l.parsing import FormProcess
from rept1l.regularize import Regularize as RR
from rept1l.helper_lib import StorageProperty
from rept1l.coupling import RCoupling as FRC
from rept1l.formutils import FormPype

import rept1l.Model as Model
model = Model.model
P = model.P

# prevent substituting cw,sw,... which will be undone anyways
FRC.get_internal_parameter_dict(treat_external=['cw', 'sw', 'sa', 'ca',
                                                'ee', 'sb', 'cb',
                                                'sa1', 'ca1', 'sa2', 'ca2',
                                                'sa3', 'ca3',
                                                ])

#############
#  Methods  #
#############

class DeriveBareSelfenergy(FormProcess):
  """ Derives a bare selfenergy for a given particle combination at order QED^2.
  """
  def __init__(self, **process_definitions):
    level_process = {'tree': False, 'bare': True, 'ct': False, 'r2': False}
    form_amplitude = 4
    process_definitions['level_process'] = level_process
    process_definitions['form_amplitude'] = form_amplitude
    process_definitions['all_couplings_active_frm'] = True
    process_definitions['masscut'] = 0.
    order_NLO = {'QCD': 0, 'QED': 2}
    process_definitions['order_NLO'] = order_NLO
    super(DeriveBareSelfenergy, self).__init__(**process_definitions)


class ParseBareSelfenergy(ParseAmplitude):
  """ Parses a form expression, reducing to 4 dimensions, performing PV and
  replacing all A0 function with zero momentum B0 functions.
  """
  optional = ParseAmplitude.optional
  optional['PassarinoVeltman'] = True
  optional['compute_r2'] = True
  optional['A0_to_B0'] = True
  optional['zeromom_propagator'] = True

  def __init__(self, **kwargs):
    super(ParseBareSelfenergy, self).__init__()

  def process_loopamp(self, amp):
    amp = ParseAmplitude.process_loopamp(self, amp)
    FP = FormPype()
    exprs = []
    exprs.extend(['l amp = (' + amp + ')*16*pi**2;'])

    FP.eval_exprs(exprs)
    amp = ''.join(FP.return_expr('amp', stop=True).split())
    return amp


class BareSelfenergy(object):
  """ Derives the bare selfenergy for a given particle combination at order
  QED^2 and parses the expression to SymPy. """
  def __init__(self, particles, **kwargs):
    DBS = DeriveBareSelfenergy(particles=particles, **kwargs)
    PBS = ParseBareSelfenergy()
    PBS.parse(DBS.processed_file)
    self.amplitude = PBS.amplitudes[(1, '0,2')]
    self.ls_dict = PBS.ls_dict
    self.tens_dict = RR.tens_dict


class CacheResults(object):
  """ Class to store the results of a run. """
  __metaclass__ = StorageProperty
  storage = ['registry']
  path = os.path.join(os.getcwd(), 'CachedHSSE')
  name = 'cached_se.txt'

  def __init__(self, *args, **kwargs):
    super(CacheResults, self).__init__(*args, **kwargs)


import rept1l.Model as Model
m = Model.model.modelname
if m == 'HS':
  from rept1l.advanced_tools.hs_basis import expr_HS
  export_func = expr_HS
elif m == 'THDM':
  from rept1l.advanced_tools.thdm_basis import expr_THDM
  # export_func = expr_THDM
  export_func = lambda x, **kwargs: expr_THDM(x, setup='alignment', **kwargs)
elif m == 'GM':
  from rept1l.advanced_tools.gm_basis import expr_GM
  export_func = expr_GM
elif m == 'SM':
  from rept1l.advanced_tools.sm_basis import expr_SM
  export_func = expr_SM
else:
  raise Exception('Incompatible model')


active = get_driver_timestamp_rcl()

# cache results depending on the active model
if len(CacheResults.registry) == 0:
  print('Registry empty.')
  print('This is the first run.')
  CacheResults.registry[active] = {}
  compare = False
else:
  print('Registry non-empty.')
  print('This is the second run.')
  compare = True


psubs = {Symbol('Nc'): 3., Symbol('sq2'): sqrt(2.)}

if __name__ == "__main__":
  HlHh = [P.W__plus__, P.W__plus__]
  # HlHh = [P.Hh, P.Hh]
  # HlHh = [P.Hl, P.Hh]
  # HlHh = [P.G0, P.G0]
  # HlHh = [P.G__plus__, P.G__plus__]
  # HlHh = [P.H__plus__, P.G__plus__]
  # HlHh = [P.Ha, P.Ha]
  # HlHh = [P.H__plus__, P.H__plus__]
# HlHh = [P.H3__plus__, P.H3__plus__]
# HlHh = [P.H3, P.H3]
  BSE = BareSelfenergy(HlHh, compute_tadpole=False)
  amp = BSE.amplitude
  print("amp:", amp)

# Build a polyonmial in scalar integrals
  tens = [u for u in amp.free_symbols if u.name.startswith('T')]
  ampp = Poly(amp, tens)

# simplify coefficients of ampp and store them
  monoms = ampp.monoms()
  for m in monoms:
    c = ampp.coeff_monomial(m)
    if sum(m) == 1:
      t = tens[list(m).index(1)]
      did, dco, args = BSE.tens_dict[t.name]
      ts = (did + dco + '(' + ','.join(args) + ')')
    else:
      ts = 'Rational'
      continue
    print(ts + ':')
    csimp = export_func(c)
    if not compare:
      CacheResults.registry[ts] = csimp
    # check if direct comparison is possible
    if compare:
      # if so compute the difference

      # ts not in cached results -> permute mass arguments
      if ts not in CacheResults.registry:
        assert(did == 'B')
        did, dco, args = BSE.tens_dict[t.name]
        ts = (did + dco + '(' + ','.join([args[0], args[2], args[1]]) + ')')

      diff = csimp - CacheResults.registry[ts]
      c_val = FRC.expression_value(diff, psubs=psubs)
      print("c_val:", c_val)
      if c_val != 0:
        print('difference:', simplify(diff))
    else:
      print(csimp)

  if not compare:
    # store results
    CacheResults.store()
