# -*- coding: utf-8 -*-
################################################################################
#                         zz_zg_gg_ward_identities.py                          #
################################################################################

from __future__ import print_function

"""  """
#============#
#  Includes  #
#============#

from sympy import Symbol, sympify, I, pi, simplify
from rept1l.parsing import ParseAmplitude
from rept1l.parsing import FormProcess
from rept1l.regularize import Regularize as RR
from rept1l.renormalize import RenoConst as RC
from rept1l.renormalize import export_expression
from rept1l.renormalize import solve_renormalization
from rept1l.coupling import RCoupling as FRC
from rept1l.counterterms import Counterterms
from rept1l.renormalize.renoconst import CountertermError
from rept1l.helper_lib import flatten

import rept1l.Model as Model
model = Model.model
P = model.P

m = Model.model.modelname
if m == 'SM':
  from rept1l.advanced_tools.sm_basis import expr_SM
  export_func = expr_SM
elif m == 'HS':
  from rept1l.advanced_tools.hs_basis import expr_HS
  export_func = expr_HS
elif m == 'THDM':
  from rept1l.advanced_tools.thdm_basis import expr_THDM
  export_func = expr_THDM
elif m == 'GM':
  from rept1l.advanced_tools.gm_basis import expr_GM
  export_func = expr_GM
else:
  export_func = export_expression

#############
#  Methods  #
#############


class DeriveBareSelfenergy(FormProcess):
  """ Derives a bare selfenergy.  """
  def __init__(self, **process_definitions):
    # r2 is disabled since the expression is derived in d dimension.
    level_process = {'tree': False, 'bare': True, 'ct': True, 'r2': False}
    form_amplitude = 4
    process_definitions['level_process'] = level_process
    process_definitions['form_amplitude'] = form_amplitude
    process_definitions['all_couplings_active_frm'] = True
    process_definitions['masscut'] = 0.
    super(DeriveBareSelfenergy, self).__init__(**process_definitions)


class ParseBareSelfenergy(ParseAmplitude):
  """ Parses a form expression, reducing to 4 dimensions, performing PV and
  replacing all A0 function with zero momentum B0 functions.
  """
  optional = ParseAmplitude.optional
  optional['PassarinoVeltman'] = True
  optional['compute_r2'] = True
  optional['A0_to_B0'] = False
  optional['zeromom_propagator'] = True

  def __init__(self, **kwargs):
    super(ParseBareSelfenergy, self).__init__()
    if 'contract_mom12' in kwargs:
      self.contract_mom12 = kwargs['contract_mom12']
    if 'contract_mom1' in kwargs:
      self.contract_mom1 = kwargs['contract_mom1']
    if 'contract_mom2' in kwargs:
      self.contract_mom2 = kwargs['contract_mom2']
    if 'set_onshell' in kwargs and kwargs['set_onshell']:
      self.set_onshell = kwargs['set_onshell']
    if 'onshell_mass' in kwargs:
      self.onshell_mass = kwargs['onshell_mass']

  def process_loopamp(self, amp):
    from rept1l.formutils import FormPype
    amp = ParseAmplitude.process_loopamp(self, amp)
    FP = FormPype()
    exprs = []
    if hasattr(self, 'contract_mom12'):
      exprs.extend(['l amp = p1(mu1)*p1(mu2)*(' + amp + ')*16*pi**2;'])
    elif hasattr(self, 'contract_mom1'):
      exprs.extend(['l amp = p1(mu1)*(' + amp + ')*16*pi**2;'])
    elif hasattr(self, 'contract_mom2'):
      exprs.extend(['l amp = p1(mu2)*(' + amp + ')*16*pi**2;'])
    else:
      exprs.extend(['l amp = (' + amp + ')*16*pi**2;'])

    if hasattr(self, 'set_onshell') and self.set_onshell:
      exprs.extend(['id p1.p1 = ' + self.onshell_mass + '^2;',
                    'argument B0,B1,B00,B11;',
                    ' id p1 = ' + self.onshell_mass + ';',
                    'endargument;',
                    'id M0=0;',
                    ])

    exprs.append('.sort')
    FP.eval_exprs(exprs)
    amp = ''.join(FP.return_expr('amp', stop=True).split())
    return amp

  def process_ctamp(self, amp):
    from rept1l.formutils import FormPype
    amp = ParseAmplitude.process_ctamp(self, amp)
    FP = FormPype()
    exprs = []
    if hasattr(self, 'contract_mom12'):
      exprs.extend(['l amp = p1(mu1)*p1(mu2)*(' + amp + ')*16*pi**2;'])
    elif hasattr(self, 'contract_mom1'):
      exprs.extend(['l amp = p1(mu1)*(' + amp + ')*16*pi**2;'])
    elif hasattr(self, 'contract_mom2'):
      exprs.extend(['l amp = p1(mu2)*(' + amp + ')*16*pi**2;'])
    else:
      exprs.extend(['l amp = (' + amp + ')*16*pi**2;'])

    if hasattr(self, 'set_onshell') and self.set_onshell:
      exprs.extend(['id p1.p1 = ' + self.onshell_mass + '^2;'])

    exprs.append('.sort')
    FP.eval_exprs(exprs)
    amp = ''.join(FP.return_expr('amp', stop=True).split())
    return amp


class BareSelfenergy(object):
  """ Derives the bare selfenergy for a given particle combination at order
  QUD^2 and parses the expression to SymPy. """

  def __init__(self, particles, **kwargs):
    DBS = DeriveBareSelfenergy(particles=particles, **kwargs)
    PBS = ParseBareSelfenergy(**kwargs)
    PBS.parse(DBS.processed_file)
    amporders = PBS.amplitudes.keys()
    print("PBS.amplitudes:", PBS.amplitudes)

    print("amporders:", amporders)
    if (1, '0,0') in amporders:
      order0 = '0,0'
    else:
      order0 = '0'

    if (1, '0,2') in amporders:
      order2 = '0,2'
    else:
      order2 = '2'

    self.amplitude = PBS.amplitudes[(1, order2)]
    if (1, order0) in PBS.amplitudes:
      self.amplitudeCT = PBS.amplitudes[(1, order0)]
    else:
      self.amplitudeCT = 0
    self.ls_dict = PBS.ls_dict
    self.tens_dict = RR.tens_dict

  def reveal_CTs(self):
    """ Reveals the counterterm dependence in the ct amplitude """
    c_set = FRC.get_couplings_from_expr(self.amplitudeCT)
    all_ct = [u for u in Counterterms.get_all_ct()]
    coupl_repl = {c: FRC.find_expression(all_ct, c) for c in c_set}
    self.CT_reveal = self.amplitudeCT.subs(coupl_repl)

    check_amps = [self.CT_reveal]
    ct = set(list(flatten([[v for v in u.free_symbols if v in all_ct]
                           for u in check_amps])))
    self.ct_dependence = ct


def success(coloured=True):
  if coloured:
    return u"\033[0;32m✓\033[1;m".encode('utf-8')
  else:
    return u"✓".encode('utf-8')


def warning(coloured=True):
  if coloured:
    return u"\033[0;33m?\033[1;m".encode('utf-8')
  else:
    return "?".encode('utf-8')


def fail(coloured=True):
  if coloured:
    return u"\033[0;31m✗\033[1;m".encode('utf-8')
  else:
    return u"✗".encode('utf-8')


def check_amp_zero(amp, tens_dict):
  ret = amp
  tens = [u.name for u in ret.free_symbols if u.name.startswith('T')]
  ret_exp = ret.expand()
  for t in tens:
    did, dco, args = tens_dict[t]
    c = export_func(ret_exp.coeff(t))
    print(did + dco + '(' + ','.join(args) + '): ', c, success() if c == 0 else fail())

  c = export_func(ret_exp.subs({u: 0 for u in tens_dict}))
  print('Rationals: ', c, success() if c == 0 else fail())


def eliminate_cts_dep(ctval, cts, subsdict):
  for u in ctval.free_symbols:
    if u in cts:
      ctval = ctval.subs(subsdict)
      return eliminate_cts_dep(ctval, cts, subsdict)
  return ctval


def check_ct_pole_part(sol, BS1, BS2):
  # BFM UV check
  # construction of ct -> UV pole dictionary
  BS1.reveal_CTs()
  print("BS1.amplitudeCT:", BS1.CT_reveal)
  cts = BS1.ct_dependence
  BS2.reveal_CTs()
  print("BS2.amplitudeCT:", BS2.CT_reveal)
  cts = cts.union(BS2.ct_dependence)

  try:
    s = RC.build_ms_ct_solutions(cts)
  except CountertermError:
    print('Checking ct WI pole part requires counterterm expressions.')
    return

  Dsubs = {Symbol('DeltaUV'): 2 / Symbol('ep')}
  cts = cts.union(set(s.values()))
  for ct in cts:
    if ct not in s:
      s[ct] = RC[ct]['value'].subs(Dsubs)
    elif ct == s[ct]:
      s[ct] = RC[ct]['value'].subs(Dsubs)

  s = {k: export_func(eliminate_cts_dep(v, cts, s)) for k, v in s.iteritems()}
  dZZUVWI = simplify(sol.subs(s))
  dZZUVC = export_func(RC['dZZ_MS']['value']).subs(Dsubs)

  print(" dZZ UV from WI:", dZZUVWI)
  print('dZZ UV computed:', dZZUVC,
        success() if dZZUVWI == dZZUVC else fail())


def ZZZG_WI():
  """ Verifies the bare-loop WI
    `k_\mu \Gamma^{\mu \\nu}_{Z Z} - \i MZ \Gamma^{\\nu}_{G0 Z} == 0`
  and computes the constraint for dZ
  """

  ZZ = [P.Z, P.Z]
  BS1 = BareSelfenergy(ZZ, contract_mom1=True, compute_tadpole=False)
  amp1 = BS1.amplitude
  amp1CT = BS1.amplitudeCT
  td = BS1.tens_dict

  GZ = [P.G0, P.Z]
  BS2 = BareSelfenergy(GZ, compute_tadpole=False)
  amp2 = BS2.amplitude
  amp2CT = BS2.amplitudeCT

  td.update(BS2.tens_dict)
  amp = amp1 - I * Symbol('cMZ') * amp2
  ampCT = amp1CT - I * Symbol('cMZ') * amp2CT

  print()
  print('Checking bare WI:')
  print('k_\mu \Gamma^{\mu \\nu}_{Z Z} - \i MZ \Gamma^{\\nu}_{G0 Z} == 0')
  check_amp_zero(amp, td)
  print()
  print()

  # BFM on-shell relation
  dZZ = Symbol('dZZ')
  if ampCT == 0:
    print("CT amplitude is zero.")
    return
  ampCT = export_func(ampCT)
  sol = solve_renormalization([ampCT], [dZZ])
  sol = simplify(sol[dZZ])
  print("Constraint: dZZ ==", sol)

  print()
  print('Deriving WF constraint:')

  check_ct_pole_part(sol, BS1, BS2)


def ZGGG_WI():
  """ Verifies the bare-loop WI
    `k_\mu \Gamma^{\mu}_{Z G0} - \i MZ \Gamma_{G0 G0} == 0`
  and computes the constraint for dZ
  """
  ZG = [P.Z, P.G0]
  BS1 = BareSelfenergy(ZG, contract_mom1=True, compute_tadpole=False)
  amp1 = BS1.amplitude
  amp1CT = BS1.amplitudeCT
  td = BS1.tens_dict

  GG = [P.G0, P.G0]
  BS2 = BareSelfenergy(GG, compute_tadpole=False)
  amp2 = BS2.amplitude
  amp2CT = BS2.amplitudeCT
  td.update(BS2.tens_dict)

  try:
    print()
    print('Checking bare WI:')
    print('k_\mu \Gamma^{\mu}_{Z G0} - \i MZ \Gamma_{G0 G0} == 0')

    amp = amp1 - I * Symbol('cMZ') * amp2
    if m == 'SM':
      dte = export_func(16 * pi**2 * RC['dt']['value'])
      amp += dte * sympify('ee/(2*cw*sw)')
    elif m == 'MS':
      dte = export_func(16 * pi**2 * RC['dt']['value'])
      amp += - dte * Symbol('ge')
    elif m == 'HS':
      dtHl = export_func(16 * pi**2 * RC['dtHl']['value'])
      dtHh = export_func(16 * pi**2 * RC['dtHh']['value'])
      ca = Symbol('ca')
      sa = Symbol('sa')
      vev = Symbol('vev')
      cMZ = Symbol('cMZ')
      amp += cMZ * (ca * dtHh - sa * dtHl) / vev
    elif m == 'THDM':
      dtHl = export_func(16 * pi**2 * RC['dtHl']['value'])
      dtHlcoeff = sympify('(cb*sa+sb*ca)')
      dtHh = export_func(16 * pi**2 * RC['dtHh']['value'])
      dtHhcoeff = sympify('(cb*ca-sb*sa)')

      vev = Symbol('vev')
      cMZ = Symbol('cMZ')
      amp += cMZ / vev * (dtHl * dtHlcoeff + dtHh * dtHhcoeff)
    elif m == 'GM':
      dtHl = export_func(16 * pi**2 * RC['dtHl']['value'])
      dtHlcoeff = sympify('(ch*sa+2*sqrt(2/3)*ca*sh)')
      dtHh = export_func(16 * pi**2 * RC['dtHh']['value'])
      dtHhcoeff = sympify('(ch*ca-2*sqrt(2/3)*sa*sh)')
      dtH5 = export_func(16 * pi**2 * RC['dtH5']['value'])
      dtH5coeff = sympify('-2*sh/sqrt(3)')

      vev = Symbol('vev')
      cMZ = Symbol('cMZ')
      amp += cMZ / vev * (dtHl * dtHlcoeff + dtHh * dtHhcoeff + dtH5 * dtH5coeff)

    check_amp_zero(amp, td)
    print()
    print()
  except CountertermError:
    print('Checking bare-loop WI requires the tadpole expression.')

  ampCT = amp1CT - I * Symbol('cMZ') * amp2CT
  if m == 'SM':
    ampCT += 16 * pi**2 * Symbol('gw') * Symbol('dt')
  elif m == 'MS':
    ampCT += 16 * pi**2 * Symbol('ge') * Symbol('dt')
  elif m == 'HS':
    ampCT += -16 * pi**2 * sympify('cMZ*(ca*dtHh - sa*dtHl)/vev')

  if ampCT == 0:
    print("CT amplitude is zero.")
    return

  # BFM on-shell relation
  dZZ = Symbol('dZZ')
  ampCT = export_func(ampCT)
  sol = solve_renormalization([ampCT], [dZZ])
  sol = simplify(sol[dZZ])
  print("Constraint: dZZ ==", sol)

  print()
  print('Deriving WF constraint:')

  check_ct_pole_part(sol, BS1, BS2)


def ZH3GH3_WI():
  """ Verifies the bare-loop WI
    `k_\mu \Gamma^{\mu}_{Z H3} - MZ \Gamma_{G0 H3} == 0`
  and computes the constraint for dZ
  """
  HA = P.Ha
  ZG = [P.Z, HA]
  BS1 = BareSelfenergy(ZG, contract_mom1=True, compute_tadpole=False)
  amp1 = BS1.amplitude
  amp1CT = BS1.amplitudeCT
  td = BS1.tens_dict

  GG = [P.G0, HA]
  BS2 = BareSelfenergy(GG, compute_tadpole=False)
  amp2 = BS2.amplitude
  amp2CT = BS2.amplitudeCT
  td.update(BS2.tens_dict)

  try:
    print()
    print('Checking bare WI:')
    print('k_\mu \Gamma^{\mu}_{Z HA} - \i MZ \Gamma_{G0 HA} == 0')

    amp = amp1 - I * Symbol('cMZ') * amp2
    if m == 'SM':
      dte = export_func(16 * pi**2 * RC['dt']['value'])
      amp += - dte * Symbol('ge')
    elif m == 'MS':
      dte = export_func(16 * pi**2 * RC['dt']['value'])
      amp += - dte * Symbol('ge')
    elif m == 'HS':
      dtHl = export_func(16 * pi**2 * RC['dtHl']['value'])
      dtHh = export_func(16 * pi**2 * RC['dtHh']['value'])
      ca = Symbol('ca')
      sa = Symbol('sa')
      vev = Symbol('vev')
      cMZ = Symbol('cMZ')
      amp += cMZ * (ca * dtHh - sa * dtHl) / vev
    elif m == 'THDM':
      dtHl = export_func(16 * pi**2 * RC['dtHl']['value'])
      dtHlcoeff = sympify('(cb*sa+sb*ca)')
      dtHh = export_func(16 * pi**2 * RC['dtHh']['value'])
      dtHhcoeff = sympify('(cb*ca-sb*sa)')

      vev = Symbol('vev')
      cMZ = Symbol('cMZ')
      amp += cMZ / vev * (dtHl * dtHlcoeff + dtHh * dtHhcoeff)

    check_amp_zero(amp, td)
    print()
    print()
  except CountertermError:
    print('Checking bare-loop WI requires the tadpole expression.')

  ampCT = amp1CT - I * Symbol('cMZ') * amp2CT
  if m == 'SM':
    ampCT += 16 * pi**2 * Symbol('gw') * Symbol('dt')
  elif m == 'MS':
    ampCT += 16 * pi**2 * Symbol('ge') * Symbol('dt')
  elif m == 'HS':
    ampCT += -16 * pi**2 * sympify('cMZ*(ca*dtHh - sa*dtHl)/vev')

  if ampCT == 0:
    print("CT amplitude is zero.")
    return

  # BFM on-shell relation
  dZZ = Symbol('dZZ')
  ampCT = export_func(ampCT)
  sol = solve_renormalization([ampCT], [dZZ])
  sol = simplify(sol[dZZ])
  print("Constraint: dZZ ==", sol)

  print()
  print('Deriving WF constraint:')

  check_ct_pole_part(sol, BS1, BS2)


def ZA_WI():
  """ Verifies the bare-loop WI
    `k_\mu \Gamma^{\mu\nu}_{Z A}== 0`
  """
  ZA = [P.Z, P.A]
  BS1 = BareSelfenergy(ZA, contract_mom1=True, compute_tadpole=False)

  amp1 = BS1.amplitude
  td = BS1.tens_dict

  print()
  print('Checking bare WI:')
  print('k_\mu \Gamma^{\mu \nu}_{Z A} == 0')

  amp = amp1
  check_amp_zero(amp, td)


def HAG0_WI():
  """ Verifies the bare-loop WI. !! Wrong, only valid in Landau gauge, need to
  include tadpoles for validity in BFM.
  """
  HAG0 = [P.Ha, P.G0]
  BS1 = BareSelfenergy(HAG0, compute_tadpole=False, set_onshell=True,
                       onshell_mass='M0')

  amp1 = BS1.amplitude
  td = BS1.tens_dict

  print()
  print('Checking bare WI:')
  print('\Gamma_{HA G0}(0) == 0')

  amp = amp1
  check_amp_zero(amp, td)


if __name__ == "__main__":
  # ZA_WI()
  # ZH3GH3_WI()
  # ZZZG_WI()
  # ZGGG_WI()
  HAG0_WI()
