################################################################################
#                               write_r2_list.py                               #
################################################################################
""" Writes the list of particle tuples with non-zero rational term which have
been computed with rept1ls tool run_r2"""

from rept1l.vertex import Vertex


def gen_npoint_r2(np, r2dict):
  return sorted(set(tuple(sorted(v.name for v in u)) for u in rd if len(u) == np))


npoints = [2, 3, 4]
Vertex.load()
rd = Vertex.lorentzR2_dict

# r2_2 = sorted(set(tuple(sorted(v.name for v in u)) for u in rd if len(u) == 2))
# r2_3 = sorted(set(tuple(sorted(v.name for v in u)) for u in rd if len(u) == 3))
# r2_4 = sorted(set(tuple(sorted(v.name for v in u)) for u in rd if len(u) == 4))

r2s = (gen_npoint_r2(np, rd) for np in npoints)

# r2s = [r2_2, r2_3, r2_4]

with open('r2list.txt', 'w') as f:
  for r2 in r2s:
    for u in r2:
      f.write(' '.join(u) + '\n')
