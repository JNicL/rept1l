###############################################################################
#                                 hs_basis.py                                 #
###############################################################################

from __future__ import print_function

""" Translates any HSESM expression given in terms of couplings and parameters
to a simple basis with external parameters:
  cMW,
  cw ( = cMW/cMZ ),
  sw ( = sqrt(1-cw^2) ),
  gw ( = e/sw),
  sa ,
  ca ( = sqrt(1-ca^2) )

Simplified expressions for couplings are cached (individually for different
rept1l model paths) and retrieved upon new occurence.
"""

#===========#
#  Imports  #
#===========#

from sympy import symbols, sqrt
from rept1l.advanced_tools.sm_basis import expr_SM

#=====================#
#  Globals & Methods  #
#=====================#

# HS parameters
sa, ca, tb = symbols('sa ca tb')

rulesew1 = {ca: sqrt(-sa**2 + 1), }
rulesew2 = {sqrt(-sa**2 + 1): ca, }
rulesew3 = {-ca**2 + 1: sa**2,
            ca**2 - 1: -sa**2,
            -sa**2 + 1: ca**2,
            sa**2 - 1: -ca**2,
            }

expr_HS = expr_SM
expr_HS.update(rulesew1, rulesew2, rulesew3)


# Dittmaier basis, vev2 = v = 2 MW sw / ee
SA, CA, vevs, vev = symbols('SA CA vevs vev')
rulesdd = {tb: vevs / vev
           }

expr_HS.update({}, {}, rulesdd, setup='dd')


def main():
  from sympy import sympify
  t = sympify('MW/MZ')
  t = expr_HS(t, verbose=False)
  print("t:", t)


if __name__ == '__main__':
  main()
