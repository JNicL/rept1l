#==============================================================================#
#                               vertices_H4F.py                                #
#==============================================================================#

""" This source code demonstrated how to derive all vertex functions for a class
of processes using the module vertexparticles """

import rept1l.Model as Model
model = Model.model
P = model.P

# list of H4F channels H -> 4F
channels = [('d', 'd~', 'd', 'd~'), ('c', 'c~', 'u', 'u~'),
            ('u', 'u~', 'e-', 'e+'), ('d', 'd~', 'e-', 'e+'),
            ('u', 'd~', 's', 'c~'), ('ve', 've~', 'd', 'd~'),
            ('u', 'u~', 's', 's~'), ('ve', 'e+', 'e-', 've~'),
            ('e-', 'e+', 'e-', 'e+'), ('u', 'u~', 'u', 'u~'),
            ('vm', 'mu+', 'e-', 've~'), ('d', 'd~', 's', 's~'),
            ('mu-', 'mu+', 'e-', 'e+'), ('ve', 'e+', 'd', 'u~'),
            ('u', 'd~', 'd', 'u~'), ('ve', 've~', 'mu-', 'mu+'),
            ('ve', 've~', 'u', 'u~')]

vert = {}

from rept1l.helper_lib import get_particle, get_anti_particle
from rept1l.parsing.vertexparticles import get_vertex_particles

for channel in channels:
  # for every channel we build the process H + q -> 3q
  # because the interface does not support the decay currently
  ps = [get_particle(u, model.model_objects.all_particles) for u in channel]
  ps_in = [P.h1, get_anti_particle(ps[0], model.model_objects.all_particles)]
  ps_out = ps[1:]

  print('Computing channel: ' + ', '.join(channel))
  vert_tmp = get_vertex_particles(ps_in + ps_out, mixings=False)

  # merge new vertices `vert_tmp` to `vert`
  for n in range(2, 6):
    if n not in vert:
      vert[n] = set()
    vert[n] = vert[n] | vert_tmp[n]

for np, pc in vert.iteritems():
  print "Npoint:", np
  for p in pc:
    print ' '.join(p)
