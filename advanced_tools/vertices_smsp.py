################################################################################
#                               vertices_smsp.py                               #
################################################################################


""" Derives all vertex functions for a class of processes using the module vertexparticles """

import rept1l.Model as Model
model = Model.model
P = model.P

channels = [('u', 'u~', 'W1', 'W1'),
            ('u', 'u~', 'W1', 'W2'),
            ('u', 'u~', 'W2', 'W2'),
            ('u', 'u~', 'W3', 'W3'),
            ('u', 'u~', 'B0', 'B0'),
            ('d', 'd~', 'W1', 'W1'),
            ('d', 'd~', 'W1', 'W2'),
            ('d', 'd~', 'W2', 'W2'),
            ('d', 'd~', 'W3', 'W3'),
            ('d', 'd~', 'B0', 'B0')]

vert = {}

from rept1l.helper_lib import get_particle, get_anti_particle
from rept1l.parsing.vertexparticles import get_vertex_particles

for channel in channels:
  ps = [get_particle(u, model.model_objects.all_particles) for u in channel]
  ps_in = [ps[0], ps[1]]
  ps_out = ps[2:]

  print('Computing channel: ' + ', '.join(channel))
  vert_tmp = get_vertex_particles(ps_in + ps_out, mixings=False)

  for n in range(2, 5):
    if n not in vert:
      vert[n] = set()
    vert[n] = vert[n] | vert_tmp[n]

for np, pc in vert.iteritems():
  print "Npoint:", np
  for p in pc:
    print ' '.join(p)
