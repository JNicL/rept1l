#==============================================================================#
#                                  logger.py                                   #
#==============================================================================#
from __future__ import print_function

#============#
#  Includes  #
#============#

import sys
import os
import logging
#from optparse import OptionParser

#==============================================================================#
#                              class LoggingSetup                              #
#==============================================================================#

class LoggingSetup(object):

  root = None

  @classmethod
  def logger_setup(self):
    #usage = "usage: %prog [options]"
    #parser = OptionParser(usage)

    # parse options
    #parser.add_option("-s", "--script", dest="filename",
                      #help="run eZchat with script", metavar="FILE")

    #parser.add_option("-q", "--quiet",
                      #action="store_false", dest="verbose", default=True,
                      #help="don't print status messages")

    #formatter = logging.Formatter('%(levelname)s:%(message)s')

    fmt_file = '%(asctime)s - %(levelname)s - %(message)s'
    fmt_stream = '%(message)s'
    formatter_file = logging.Formatter(fmt=fmt_file)
    formatter_stream = logging.Formatter(fmt=fmt_stream)

    self.root = logging.getLogger('root')
    self.root.setLevel(logging.DEBUG)

    # extract name of the file which is executed as main and remove file ending
    mainsrc = os.path.splitext(sys.argv[0])[0]
    logfile = mainsrc + '.log'

    # create file handler which logs even debug messages
    try:
      file_handler = logging.FileHandler(logfile)
      file_handler.setLevel(logging.DEBUG)
      file_handler.setFormatter(formatter_file)
      file_handler.propagate = False
      self.root.addHandler(file_handler)
    except IOError as e:
        print('Cannot open log file: ', e)

    # warning and higher level are printed to stdout in addition
    stdout_handler = logging.StreamHandler()
    stdout_handler.setFormatter(formatter_stream)

    #parser.add_option("-l", "--log",
                      #action="store", dest="loglevel", default=False,
                      #help="Set the loglevel. Accepted values are: " +
                           #"DEBUG, INFO, WARNING")

    #(options, args) = parser.parse_args()
    #loglevel = options.loglevel

    #if loglevel:
      #numeric_level = getattr(logging, loglevel.upper(), None)
      #if not isinstance(numeric_level, int):
          #raise ValueError('Invalid log level: %s' % loglevel)

      #stdout_handler.setLevel(numeric_level)
    #else:
    stdout_handler.setLevel(logging.INFO)

    stdout_handler.propagate = False
    self.root.addHandler(stdout_handler)


def log(msg, mode):
  if mode not in ['debug', 'info', 'warning', 'error', 'critical']:
    raise ValueError('Logging failed for unknown mode: ' + str(mode))

  if LoggingSetup.root is None:
    LoggingSetup.logger_setup()

  logger = LoggingSetup.root
  try:
    logger.__class__.__dict__[mode](logger, msg)
  except:
    getattr(logger, mode)(msg)


