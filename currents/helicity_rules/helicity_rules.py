#==============================================================================#
#                              helicity_rules.py                               #
#==============================================================================#

#============#
#  Includes  #
#============#

from sympy import Symbol

from rept1l.combinatorics import pair_arguments
from rept1l.tensor import diracAlgebra, tensor, tensor_arg
from rept1l.pyfort import ifBlock, caseBlock

#===========#
#  Methods  #
#===========#

def test_fermion_helicity(current, contr_index):
  """ Tests if `current` is invariant under helicity projections. The current,
  labeled as `w(i)`, is replaced by the rule

    w(i) -> \sum_j P_{ij}*w(j),

  where P is either a P^+ or P^- helicity projector

  :return: {'+': projected current on '+' == current,
            '-': projected current on '-' == current}
  """
  if len(current.args) != 1:
    raise ValueError('Fermionic current should have only one argument')

  # construct contraction arg and replace the wavefunction arg with it
  out_arg = current.args[0]
  argc = tensor_arg(contr_index, resolve=True, domain=range(4))
  current.substitute_arg(out_arg, argc)

  # construct the two polarization projection P^{+/-}
  polplus = tensor([out_arg, argc], numerical=diracAlgebra.ProjP)
  polminus = tensor([out_arg, argc], numerical=diracAlgebra.ProjM)

  # contract the current with the projections
  check1 = current * polplus
  check2 = current * polminus

  # restore the state of `current` <- import if `current` is further processed
  current.substitute_arg(argc, out_arg)

  # check if the polarized currents match `current`
  resplus = check1.expanded_tensor == current.expanded_tensor
  resminus = check2.expanded_tensor == current.expanded_tensor

  # check if the result is zero
  if check1.expanded_tensor == [0] * len(check1.expanded_tensor):
    resplus = 'Zero'
  if check2.expanded_tensor == [0] * len(check2.expanded_tensor):
    resminus = 'Zero'

  # if one the final polarization is zero, we enforce a correlation (which might
  # have nothing to do with the initial state polarization)
  if resplus == 'Zero' and resminus is False:
    resminus = True
  if resminus == 'Zero' and resplus is False:
    resplus = True

  return {'+': resminus, '-': resplus}

#------------------------------------------------------------------------------#

def polarized_fermion(wavefunction, pol, contr_index):
  """ Construct a polarized fermionic wavefunction out of `wavefunction`.

  :param wavefunction: fermionic wavefunction
  :type  wavefunction: ATensor

  :param pol: the polarization, either '+' or '-'
  :type  pol: str

  :param contr_index: A free contraction index.
  :type  contr_index: int

  Constructing a wavefunction of dimension 4
  >>> from rept1l.tensor import tensor_arg, tensor
  >>> farg = tensor_arg('i', resolve=True, domain=range(4))
  >>> ftens = tensor([farg], symbol=Symbol('w'))
  >>> ftens.expanded_tensor
  [w(0), w(1), w(2), w(3)]

  Applying a polarization projection with `polarized_fermion`
  >>> pol_ftens = polarized_fermion(ftens, '+', -1)
  >>> pol_ftens.expanded_tensor
  [w(0), w(1), 0, 0]

  The projected fermion is invariant under further `+` projections and zero
  under `-` projection
  >>> test_fermion_helicity(pol_ftens, -2)
  {'+': 'Zero', '-': True}
  """

  # MTensors are used on purpose here
  from rept1l.tensor import MTensor
  arg = tensor_arg(contr_index, resolve=True, domain=range(4))
  new_wavefunction = MTensor([arg], symbol=wavefunction.symbol)
  prj = {'+': diracAlgebra.ProjP, '-': diracAlgebra.ProjM}
  ret = MTensor([wavefunction.args[0], arg], numerical=prj[pol])
  ret *= new_wavefunction

  # safe contruction to make ATensor primitive
  ret.make_primitive(symbol=Symbol('Prj_' + wavefunction.symbol.name),
                     block_reconstruction=True)
  ret = tensor(ret)
  return ret

#------------------------------------------------------------------------------#

def derive_helicity_correlation(current, fermions_in, out_is_fermion=True):
  """ Derives a helicity correlation of the external fermion in terms of the
  ingoing fermion helicity states.

  :param current: offshell current
  :type  current: tensor
  :param fermions_in: indices of external fermions
  :type  fermions_in: list of integers

  :return: dictionary where the keys specify a correlation and the value allows
           to decide whether there is a correlation.

  >>> lorentz_strings = [['1*GammaM(1,3,2)'], ['2*GammaP(1,3,2)']]
  >>> from rept1l.currents.current import Current
  >>> tens = Current(lorentz_strings, 3, last=True,\
                     optimized=False, dim=4, loop_current=False).current
  >>> hc = derive_helicity_correlation(tens, [2])

  As we only have one ingoing fermion all possible correlations are given by the
  state of the ingoing fermion, namely, either polarized +/- or unpolarized 0.
  >>> hc.keys()
  [((2, '+'),), ((2, '-'),), ((2, '0'),)]

  A Gamma matrix flips the helicity states, thus we expect
  >>> hc[((2, '+'),)]['-']
  'Zero'
  >>> hc[((2, '+'),)]['+']
  True
  >>> hc[((2, '-'),)]['+']
  'Zero'
  >>> hc[((2, '-'),)]['-']
  True
  >>> hc[((2, '0'),)]['-']
  False
  >>> hc[((2, '0'),)]['+']
  False
  """
  fermion_wavefunctions = {}
  for f_id in fermions_in:
    found = current.find_primitive_tensor_symbol(Symbol('w' + str(f_id)))
    if not found:
      raise ValueError('Fermion wavefunction ' + str(f_id) +
                       ' not found in current')
    fermion_wavefunctions[f_id] = found

  states = pair_arguments([((key, '0'), (key, '+'), (key, '-'))
                           for key in fermion_wavefunctions])
  contr_offset = min(u.symbol for u in current.get_all_args()
                     if type(u.symbol) is int)
  hel_corr = {}
  for state_tmp in states:
    state = {u[0]: u[1] for u in state_tmp}
    wave_repl = {}  # substitution rule for wavefunction->polarized wavefunction
    wave_repl_inv = {}  # inverse rule
    contr = contr_offset - 1
    for f_id in state:
      if state[f_id] != '0':
        found = current.find_primitive_tensor_symbol(Symbol('w' + str(f_id)))
        # pos: index of current.tensor
        for pos in found:
          # generate the polarized wavefunction
          pol_f = polarized_fermion(found[pos][0], state[f_id], contr)

          # the substitution are defined for each current.tensor because the
          # `same` wavefunction can have a different contraction index in
          # different current.tensor
          if found[pos][0].symbol not in wave_repl:
            wave_repl[found[pos][0].symbol] = {}
          if pol_f.symbol not in wave_repl_inv:
            wave_repl_inv[pol_f.symbol] = {}

          wave_repl[found[pos][0].symbol][pos] = pol_f
          wave_repl_inv[pol_f.symbol][pos] = tensor(found[pos][0])

    # generate the polarized current
    for w_old in wave_repl:
      current = current.find_primitive_tensor_symbol(w_old,
                                                     repl=wave_repl[w_old])
    # compute the polarized current
    current = current.reconstruct_tensor()

    # if the outgoing particle is a fermion, a correlation between incoming and
    # outgoing helicities is computed
    if out_is_fermion:
      hel_corr[state_tmp] = test_fermion_helicity(current, contr)

    # for a  non-fermionic current it is only checked if the current is zero
    # given the incoming helicities
    else:
      zero_curr = current.expanded_tensor == [0] * len(current.expanded_tensor)
      if zero_curr:
        hel_corr[state_tmp] = 'Zero'
      else:
        hel_corr[state_tmp] = 'Unpolarized'

    # restore the current state
    for w_old in wave_repl_inv:
      current.find_primitive_tensor_symbol(w_old, repl=wave_repl_inv[w_old])

    current = current.reconstruct_tensor()
  return hel_corr

def simplify_helicity_correlation(hc_in):
  """ Removes helicity states which are not correlated with the final particle
  helicity state. (Not well tested yet)

  >>> k1 = ((1, '+'), (2, '0'), (3, '0'))
  >>> k2 = ((1, '+'), (2, '0'), (3, '+'))
  >>> k3 = ((1, '+'), (2, '0'), (3, '-'))
  >>> corrstate = {'+': 'Zero', '-': True}
  >>> corrs = {k1: corrstate, k2: corrstate, k3: corrstate}
  >>> simplify_helicity_correlation(corrs)
  {((1, '+'), (2, '0')): {'+': 'Zero', '-': True}}
  """
  hc = hc_in.copy()
  hc_counter = {}
  hc_corrs = {}
  for u in hc:
    # do not simplify zero/unpolarized states
    if hc[u] == 'Zero' or hc[u] == 'Unpolarized':
      continue
    hck = (hc[u]['+'], hc[u]['-'])

    if hck not in hc_counter:
      hc_counter[hck] = 1
      hc_corrs[hck] = [u]
    else:
      hc_counter[hck] += 1
      hc_corrs[hck].append(u)

  for u in hc:
    if hc[u] == 'Zero' or hc[u] == 'Unpolarized':
      continue
    hck = (hc[u]['+'], hc[u]['-'])
    if hc_counter[hck] > 2:
      ckeys = find_corr_keys(hc_corrs[hck])
      if ckeys:
        assert(len(ckeys) == 3)
        if len(ckeys[0])==3:
          if ckeys[0][0] != ckeys[1][0]:
            combcorr = (ckeys[0][1], ckeys[0][2])
          elif ckeys[0][1] != ckeys[1][1]:
            combcorr = (ckeys[0][0], ckeys[0][2])
          elif ckeys[0][2] != ckeys[1][2]:
            combcorr = (ckeys[0][0], ckeys[0][1])
        elif len(ckeys[0])==2:
          if ckeys[0][0] != ckeys[1][0]:
            combcorr = (ckeys[0][1], )
          elif ckeys[0][1] != ckeys[1][1]:
            combcorr = (ckeys[0][0], )
        assert(combcorr not in hc)
        hc[combcorr] = hc[ckeys[0]]
        del hc[ckeys[0]]
        del hc[ckeys[1]]
        del hc[ckeys[2]]
        return simplify_helicity_correlation(hc)
  return hc

def corr_dist(corr1, corr2):
  if len(corr1) != len(corr2):
    return -1
  return [corr1[u] == corr2[u] for u in range(len(corr1))].count(False)

def find_corr_keys(corrs):
  checks = {}
  if len(corrs) > 2:
    for l1, cr1 in enumerate(corrs):
      d1c = [cc for cc in corrs[l1+1:] if corr_dist(cr1, cc) == 1]

      if len(d1c) > 1:
        for l2, cr2 in enumerate(d1c):
          d2c = [cc for cc in d1c[l2+1:] if corr_dist(cr2, cc) == 1 and corr_dist(cr1, cc) == 1]

          assert(len(d2c) < 2)
          if len(d2c) == 1:
            return cr1, cr2, d2c[0]

#------------------------------------------------------------------------------#

def write_helicity_case(l_id, current, fermions_in, out_is_fermion, tabs=1):
  """Exports the helicity conservation rule for the current `current`.

  :l_id: current id which goes into the Fortran case `case(l_id)`

  :param current: An arbitrary offshell current
  :type  current: current

  :param fermions_in: Arguments which are fermionic
  :type  fermions_in: List of int

  :param out_is_fermion: True if current is fermionic
  :type  out_is_fermion: bool
  """
  hc = derive_helicity_correlation(current, fermions_in,
                                   out_is_fermion=out_is_fermion)

  # hc = simplify_helicity_correlation(hc)
  cb = caseBlock(l_id, tabs=tabs)

  state_trans = {'+': 'pos_hel', '-': 'neg_hel', '0': 'unpol'}
  cases = None
  # We filter only for non-trivial results, the case where the outgoing
  # wavefunction is independent of the incoming polarization is dealt in the
  # `else` case
  if not out_is_fermion:
    # For non fermionic currents, the current can be unpolarized or zero
    hc_pol = {u: hc[u] for u in hc if hc[u] != 'Unpolarized'}
  else:
    # For fermionic currents, the current can have specific polarizations which
    # depend on the initial particle polarizations
    hc_pol = {u: hc[u] for u in hc if any(hc[u].values())}

  for n_case, hel_in in enumerate(hc_pol):
    corr = hc[hel_in]

    # generate if cases for selecting incoming helicity configuration
    ifs = []
    for particle_index, particle_state in hel_in:
      ifc = ('fheT(w(' + str(particle_index) + ')) .eq. ' +
             state_trans[particle_state])
      ifs.append(ifc)

    if type(corr) is dict:
      assert(out_is_fermion is True)
      corr_true = [u for u in corr if corr[u] is True]
      corr_Zero = [corr[u] == 'Zero' for u in corr]
    else:
      assert(out_is_fermion is False)
      corr_true = []
      corr_Zero = [corr == 'Zero']
    if len(corr_true) > 0:
      if len(corr_true) > 1:
        raise Exception('Case in write_helicity_correlation ' +
                        'does not make sense. More than one true ' +
                        'correlation.')
      out = 'fheT(wOut) = ' + state_trans[corr_true[0]]
    elif all(corr_Zero):
      out = 'zero_current'
    elif not any(corr_Zero):
      out = 'fheT(wOut) = unpol'
    elif any(corr_Zero):
      assert(len(corr_true == 1))
    else:
      raise Exception('Unhandled case in write_helicity_correlation')

    # rules derived for massless outgoing fermions
    # a current being 0 is  independent of ml
    if out_is_fermion and out != 'zero_current':
      ifs = ['ml'] + ifs
    if_case = ' .and. '.join(ifs)

    # first case
    if n_case == 0:
      cases = ifBlock(if_case, tabs=tabs + 1)
    else:
      cases = (cases > 0) + ('else if(' + if_case + ') then')

    if out == 'zero_current':
      cases += 'fhfilter(j) = .false.'
      cases += 'if (fheT(wOut) .eq. not_set) fheT(wOut) = zero_current'
    else:
      cases += out
      if out_is_fermion:
        ifll = ifBlock('last_tl', tabs=tabs + 2)
        ifll + 'fheT(wOut) = - fheT(wOut)'
        cases + ifll

  if len(hc_pol) == 0:
    cases = 'fheT(wOut) = unpol'
  else:
    cases = (cases > 0) + ('else')
    cases += 'fheT(wOut) = unpol'
  cb + cases

  return cb


#------------------------------------------------------------------------------#

if __name__ == "__main__":
  import doctest
  doctest.testmod()
