      if (present(last_leg)) then ! last current
        if (fheT(wOut)*fheT(last_leg) .ne. unpol .and. &
            fheT(wOut) .ne. fheT(last_leg)) then
          fhfilter(j) = .false.
          if (fheT(wOut) .eq. not_set) fheT(wOut) = zero_current
        end if
      end if
