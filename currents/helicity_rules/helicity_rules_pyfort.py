#==============================================================================#
#                           helicity_rules_pyfort.py                           #
#==============================================================================#

#============#
#  Includes  #
#============#

from rept1l.pyfort import subBlock, selectBlock, spaceBlock

#===========#
#  Methods  #
#===========#

def gen_helicity_subroutine(tabs=1):
  """ Generates the fortran subroutine helicity_conservation as a PyFort
  instance.

  The layout is as follows:

      +-------------------+
      |    select case    |
      `====================\
        |      cases        |
        |(filled at runtime)|
        +-------------------|
        |    default        |
      +--------------------/
      | last leg case     |
      +-------------------+

  :return: returns the subroutine and the select block for the cases as a tuple
  """
  hc = subBlock('helicity_conservation_mdl',
                arg='fheT, typeT, fhfilter, j, w, wOut, ml, last_tl',
                tabs=tabs)

  hc.addPrefix("use class_vertices")
  hc.addType(('integer, dimension(0:), intent(inout)', 'fheT'))
  hc.addType(('logical, dimension(:), intent(inout)', 'fhfilter'))
  hc.addType(('integer, intent(in), dimension(:)', 'w'))
  hc.addType(('integer, intent(in)', 'wOut, typeT, j'))
  hc.addType(('logical, intent(in)', 'ml, last_tl'))
  hc.addType(('integer, parameter', 'pos_hel = 1'))
  hc.addType(('integer, parameter', 'neg_hel = -1'))
  hc.addType(('integer, parameter', 'unpol = 0'))
  hc.addType(('integer, parameter', 'zero_current = 3'))
  hc.addType(('integer, parameter', 'not_set = 111'))

  sb = selectBlock('typeT', tabs=tabs + 1)  # select case block

  # cases stores the helicity correlation for different lorentz structures
  cases = spaceBlock(tabs=tabs)   # general case filled at runtime in rept1l
  default = spaceBlock(tabs=tabs + 1)  # default case if there is no correlation
  sb + cases
  sb + default

  # construct the default case
  (default > 0) + 'case default'
  # the default is unpolarized which means that all polarization have to be
  # computed independent  of the incoming ones
  default + 'fheT(wOut) = unpol'

  hc.addLine()
  hc + sb
  return hc, cases
