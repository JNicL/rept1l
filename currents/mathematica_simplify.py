###############################################################################
#                           mathematica_simplify.py                           #
###############################################################################

from __future__ import print_function

""" Wrapper for mathematicas FullSimplify function. Translating expressions
to symbols which can be used in mathematica. """
from sympy import sympify, Symbol

current_repl = {}
ncouplings = 100
for i in range(ncouplings):
  current_repl[Symbol('co(' + str(i) + ')')] = Symbol('c' + str(i))

nparticles = 8
for i in range(1, nparticles):
  for mu in range(4):
    p = 'P' + str(i) + '(' + str(mu) + ')'
    current_repl[Symbol(p)] = Symbol('P' + str(i) + str(mu))
    w = 'w' + str(i) + '(' + str(mu) + ')'
    current_repl[Symbol(w)] = Symbol('w' + str(i) + str(mu))

  p = 'P' + str(i) + '(i)'
  current_repl[Symbol(p)] = Symbol('P' + str(i) + 'i')
  p = 'P' + str(i) + '(mu)'
  current_repl[Symbol(p)] = Symbol('P' + str(i) + 'm')
  p = 'P' + str(i) + '(nu)'
  current_repl[Symbol(p)] = Symbol('P' + str(i) + 'n')
  p = 'P' + str(i) + '(ro)'
  current_repl[Symbol(p)] = Symbol('P' + str(i) + 'r')
  w = 'w' + str(i) + '(i)'
  current_repl[Symbol(w)] = Symbol('w' + str(i) + 'i')
  w = 'w' + str(i) + '(mu)'
  current_repl[Symbol(w)] = Symbol('w' + str(i) + 'm')
  w = 'w' + str(i) + '(nu)'
  current_repl[Symbol(w)] = Symbol('w' + str(i) + 'n')
  w = 'w' + str(i) + '(ro)'
  current_repl[Symbol(w)] = Symbol('w' + str(i) + 'r')

for mu in range(8):
  w = 'wl(' + str(mu) + ',rank)'
  current_repl[Symbol(w)] = Symbol('wl' + str(mu))
  p = 'pl' + '(' + str(mu) + ')'
  current_repl[Symbol(p)] = Symbol('pl' + str(mu))
  ggimu = Symbol('gg(' + str(mu) + ',mu)')
  current_repl[ggimu] = Symbol('gg' + str(mu) + 'm')
  gginu = Symbol('gg(' + str(mu) + ',nu)')
  current_repl[gginu] = Symbol('gg' + str(mu) + 'n')
  ggiro = Symbol('gg(' + str(mu) + ',nu)')
  current_repl[ggiro] = Symbol('gg' + str(mu) + 'n')
  ggmui = Symbol('gg(mu,' + str(mu) + ')')
  current_repl[ggmui] = Symbol('gg' + 'm' + str(mu))
  ggnui = Symbol('gg(nu,' + str(mu) + ')')
  current_repl[ggnui] = Symbol('gg' + 'n' + str(mu))
  ggroi = Symbol('gg(ro,' + str(mu) + ')')
  current_repl[ggroi] = Symbol('gg' + 'r' + str(mu))

w = 'wl(i,rank)'
current_repl[Symbol(w)] = Symbol('wli')
w = 'wl(mu,rank)'
current_repl[Symbol(w)] = Symbol('wlm')
w = 'wl(nu,rank)'
current_repl[Symbol(w)] = Symbol('wln')
w = 'wl(ro,rank)'
current_repl[Symbol(w)] = Symbol('wlr')
p = 'pl' + '(mu)'
current_repl[Symbol(p)] = Symbol('plm')
p = 'pl' + '(nu)'
current_repl[Symbol(p)] = Symbol('pln')
p = 'pl' + '(ro)'
current_repl[Symbol(p)] = Symbol('plr')
p = 'pl' + '(i)'
current_repl[Symbol(p)] = Symbol('pli')
p = 'p' + '(0)'
current_repl[Symbol(p)] = Symbol('p0')
p = 'p' + '(1)'
current_repl[Symbol(p)] = Symbol('p1')
p = 'p' + '(2)'
current_repl[Symbol(p)] = Symbol('p2')
p = 'p' + '(3)'
current_repl[Symbol(p)] = Symbol('p3')
p = 'p' + '(i)'
current_repl[Symbol(p)] = Symbol('pi')
p = 'p' + '(mu)'
current_repl[Symbol(p)] = Symbol('pm')
p = 'p' + '(nu)'
current_repl[Symbol(p)] = Symbol('pn')
current_repl[Symbol('gg(mu)')] = Symbol('ggm')
current_repl[Symbol('gg(i,mu)')] = Symbol('ggim')
current_repl[Symbol('gg(mu,i)')] = Symbol('ggmi')
current_repl[Symbol('gg(i,nu)')] = Symbol('ggin')
current_repl[Symbol('gg(nu,i)')] = Symbol('ggni')
current_repl[Symbol('gg(mu,nu)')] = Symbol('ggmn')
current_repl[Symbol('gg(nu,mu)')] = Symbol('ggnm')
current_repl[Symbol('gg(ro,nu)')] = Symbol('ggrn')
current_repl[Symbol('gg(nu,ro)')] = Symbol('ggnr')
current_repl[Symbol('gg(ro,mu)')] = Symbol('ggrm')
current_repl[Symbol('gg(mu,ro)')] = Symbol('ggmr')

current_repl_inv = {current_repl[k]: k for k in current_repl}

mcr = current_repl
mcri = current_repl_inv


def mathematica_simplify(expr):
  """ Mathemtatica FullSimplify Wrapper. """
  x = expr.xreplace(current_repl)

  def is_ordinary_symbol(expr):
    if '(' in expr:
      return False
    elif ')' in expr:
      return False
    else:
      return True

  subst_check = {u: u in current_repl_inv for u in x.free_symbols
                 if not u.name.startswith('x') and
                 not is_ordinary_symbol(u.name)}

  if not all(subst_check.values()):
    print('Missing substitutions:')
    for s in subst_check:
      if subst_check[s] is False:
        print("s:", s)
    print("current_repl:", current_repl)
    raise Exception('Failed to simplify expression in mathematica.')
  tmpfile = 'simplifythis.m'
  with open(tmpfile, 'w') as f:
    f.write('out=(' + str(x) + ') // FullSimplify\n')
    f.write('Print[out]')

  from subprocess import PIPE, Popen
  cmd = ['math', '-script', tmpfile]
  p = Popen(cmd, stdin=PIPE, stdout=PIPE, stderr=PIPE)
  output, err = p.communicate()
  rc = p.returncode
  import os
  try:
    os.remove(tmpfile)
  except Exception as e:
    pass
  output = output.decode('utf-8').split('\n')
  if len(output) == 2:
    output = output[0]
  elif len(output) == 3:
    output = output[1]
  else:
    print("x:", x)
    print("len(output):", len(output))
    print("output:", output)
    raise Exception('Failed to simplify expression in mathematica.')
  return sympify(output).xreplace(current_repl_inv)
