#==============================================================================#
#                                 __init__.py                                  #
#==============================================================================#

#============#
#  Includes  #
#============#

from .lorentz_lib import get_structure
from .tree_vertex import write_tree_vertex
from .loop_vertex import write_loop_vertex
from .form_vertex import write_form_vertex

from .pyfort_classes import class_tree as ct
from .pyfort_classes import class_loop as cl
from .pyfort_classes import class_form as cf
from . import form_vertex as fv
from . import tree_vertex as tv
from . import loop_vertex as lv
from . import helicity_rules as hr

#===========#
#  Globals  #
#===========#

gen_form_class = cf.gen_form_class
gen_loop_class = cl.gen_loop_class
gen_tree_class = ct.gen_tree_class
TreeCurrentPyfort = ct.TreeCurrentPyfort
LoopCurrentPyfort = cl.LoopCurrentPyfort
FormCurrentPyfort = cf.FormCurrentPyfort
gen_helicity_subroutine = hr.gen_helicity_subroutine

#========#
#  Info  #
#========#

__author__ = "Jean-Nicolas lang"
__date__ = "28. 7. 2016"
__version__ = "0.1"
