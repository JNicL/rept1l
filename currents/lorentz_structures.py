#==============================================================================#
#                              lorentz_structures                              #
#==============================================================================#

#============#
#  Includes  #
#============#

import sys
import os

from rept1l.currents.lorentz_lib import lorentz_func_dict_tree
from rept1l.currents.lorentz_lib import lorentz_func_dict_loop

#===========#
#  Globals  #
#===========#
def id(final_particle, dirac, vectorized, last_flag,
       additional, msyms, args_i_j, last_contraction=False):
  return msyms, additional, vectorized, last_flag

def id_loop(final_particle, dirac, vectorized, last_flag,
            additional, msyms, args_i_j, max_rank=0):
  return msyms, additional, vectorized, max_rank


lorentz_func_dict_tree['1'] = id
lorentz_func_dict_loop['1'] = id_loop

from rept1l.currents.currentblocks.metrictree import MetricTree
lorentz_func_dict_tree['Metric'] = MetricTree
from rept1l.currents.currentblocks.metricloop import MetricLoop
lorentz_func_dict_loop['Metric'] = MetricLoop

from rept1l.currents.currentblocks.momentumtree import MomentumTree
lorentz_func_dict_tree['P'] = MomentumTree
from rept1l.currents.currentblocks.momentumloop import MomentumLoop
lorentz_func_dict_loop['P'] = MomentumLoop

from rept1l.currents.currentblocks.gammatree import GammaTree
lorentz_func_dict_tree['Gamma'] = (lambda *args, **kwargs:
                                   GammaTree(*args, null_plus_minus='0',
                                             **kwargs))
lorentz_func_dict_tree['GammaP'] = (lambda *args, **kwargs:
                                    GammaTree(*args, null_plus_minus='+',
                                              **kwargs))
lorentz_func_dict_tree['GammaM'] = (lambda *args, **kwargs:
                                    GammaTree(*args, null_plus_minus='-',
                                              **kwargs))

from rept1l.currents.currentblocks.gammaloop import GammaLoop
lorentz_func_dict_loop['Gamma'] = (lambda *args, **kwargs:
                                   GammaLoop(*args, null_plus_minus='0',
                                             **kwargs))

lorentz_func_dict_loop['GammaP'] = (lambda *args, **kwargs:
                                    GammaLoop(*args, null_plus_minus='+',
                                              **kwargs))

lorentz_func_dict_loop['GammaM'] = (lambda *args, **kwargs:
                                    GammaLoop(*args, null_plus_minus='-',
                                              **kwargs))

from rept1l.currents.currentblocks.sigmatree import SigmaTree
lorentz_func_dict_tree['Sigma'] = SigmaTree
from rept1l.currents.currentblocks.sigmaloop import SigmaLoop
lorentz_func_dict_loop['Sigma'] = SigmaLoop

from rept1l.currents.currentblocks.projtree import ProjTree
lorentz_func_dict_tree['Identity'] = (lambda *args, **kwargs:
                                      ProjTree(*args, null_plus_minus='0',
                                               **kwargs))
lorentz_func_dict_tree['ProjP'] = (lambda *args, **kwargs:
                                   ProjTree(*args, null_plus_minus='+',
                                            **kwargs))
lorentz_func_dict_tree['ProjM'] = (lambda *args, **kwargs:
                                   ProjTree(*args, null_plus_minus='-',
                                            **kwargs))

from rept1l.currents.currentblocks.projloop import ProjLoop
lorentz_func_dict_loop['ProjP'] = (lambda *args, **kwargs:
                                   ProjLoop(*args, null_plus_minus='+',
                                            **kwargs))
lorentz_func_dict_loop['ProjM'] = (lambda *args, **kwargs:
                                   ProjLoop(*args, null_plus_minus='-',
                                            **kwargs))

from rept1l.currents.currentblocks.epsilontree import EpsilonTree
lorentz_func_dict_tree['Epsilon'] = EpsilonTree
from rept1l.currents.currentblocks.epsilonloop import EpsilonLoop
lorentz_func_dict_loop['Epsilon'] = EpsilonLoop
