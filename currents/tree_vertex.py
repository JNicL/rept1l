#==============================================================================#
#                                 tree vertex                                  #
#==============================================================================#
from __future__ import print_function

#============#
#  Includes  #
#============#

import os
from sympy import I, Symbol
from rept1l.tensor import sub_args
from rept1l.pyfort import ifBlock, spaceBlock
from rept1l.currents.helicity_rules import write_helicity_case
from rept1l.helper_lib import StorageProperty
from rept1l.logging_setup import log
from rept1l.currents.currentsimplify import simplify_current

#===========#
#  Methods  #
#===========#

class TreeCurrentCache(object):
  """ Class which retrives currents which are cached or computes and chaches
  them. """
  __metaclass__ = StorageProperty

  # StorageMeta attributes. Those attributes are stored when `store` is called.
  storage = ['registry']
  # StorageMeta storage path
  rpath = os.environ['REPT1L_CONFIG']
  path = os.path.join(rpath, 'ModelCurrents')
  # StorageMeta filename
  name = 'treecurrents.txt'

  @classmethod
  def is_current_cached(cls, lorentz_strings, nparticles):
    lst = (tuple(tuple(u) for u in lorentz_strings), nparticles)
    return lst in cls.registry

  @classmethod
  def get_current_rule(cls, lorentz_strings, nparticles, dim, apply_cse=True,
                       pext=None):
    lst = (tuple(tuple(u) for u in lorentz_strings), nparticles)
    # if cached no computation is needed
    if lst in cls.registry:
      return cls.registry[lst]

    # computation is done
    reg_entry = {}

    from rept1l.currents.current import Current
    OTC = Current(lorentz_strings, nparticles, dim=dim, pext=pext, last=False)

    # do helicity correlation here

    curr = OTC.current
    additionals = OTC.additionals

    from sympy import I, Symbol
    isubs = {I: Symbol('cima')}
    curr.expanded_tensor = [u.subs(isubs) for u in curr.expanded_tensor]
    last_flag = OTC.last_flag
    if apply_cse:
      Current.cse_current(curr, additionals, loop=False)

    reg_entry['current'] = curr
    reg_entry['additionals'] = additionals
    reg_entry['last_flag'] = last_flag

    # check if the current requires an additional current in case of the last
    # current
    if (last_flag):
      # compute the truncated offshell current

      OTC = Current(lorentz_strings, nparticles, dim=dim, last=True)
      curr_last = OTC.current
      additionals_last = OTC.additionals

      if apply_cse:
        Current.cse_current(curr_last, additionals_last, loop=False)

      reg_entry['current_last'] = curr_last
      reg_entry['additionals_last'] = additionals_last

    cls.registry[lst] = reg_entry
    return reg_entry

  @classmethod
  def reg_current_rule(cls, ls, wouts, adds, vectorized, last_flag):
    lst = tuple(tuple(u) for u in ls)
    if lst in cls.registry:
      log('Overwriting current rule for ' + str(lst), 'warning')
    d = {'wavefunctions': wouts, 'additionals': adds, 'vectorized': vectorized,
         'last_flag': last_flag}
    cls.registry[lst] = d

def write_tree_vertex(sct, hc_cases, lorentz_strings, n_particles, dim,
                      lorentz_id, apply_cse=True, simplify=True,
                      fermions_in=None, out_is_fermion=False, pext=None,
                      tabs=2):
  """ Derives the current rules for the lorentz structure `lorentz_strings` and
  exports the  result as offshell currents to Fortran.

  :param lorentz_strings: Doubly nested list of strings of the form
                          [ [a, b, ...  ], [c, d, ... ],  ]
                          where a,b,c, are lorentz strings
                          the outer parenthesis separates structures multiplied
                          with different couplings. The inner paraenthesis
                          groups different structure, which are multiplied with
                          the same coupling.
  :type  lorentz_strings: list

  :param n_particles: number of particles
  :type  n_particles: int

  :param dim: dimension of the outgoing wavefunction
  :type  dim: int

  :param lorentz_id: id associated to the off shell current
  :type  lorentz_id: str

  Example:
    lorentz_strings = [['GammaP(1,2,3)', 'GammaP(1,2,3)'], ['Gamma(1,2,3)']]
    write_tree_vertex(lorentz_strings, 3, 4, '0')

    select_case_tree.printStatment()
  """
  from rept1l.currents.current import Current
  OTC = Current(lorentz_strings, n_particles, dim=dim, pext=pext)
  # for c in OTC.feynman_rule:
  #   print(c)
  #   # sct.addcomment(str(c))
  # import sys
  # sys.exit()

  wout = OTC.current
  additionals = OTC.additionals
  last_flag = OTC.last_flag

  if simplify:
    res = simplify_current(wout, vectorized=OTC.vectorized)
    wout.expanded_tensor = res
  isubs = {I: Symbol('cima')}
  wout.expanded_tensor = [u.subs(isubs) for u in wout.expanded_tensor]

  (sct > 0) + ("case(" + lorentz_id + ")")
  for c in OTC.feynman_rule:
    sct.addcomment(str(c))

  runs = 1
  if (last_flag):
    # compute the truncated offshell current

    OTC = Current(lorentz_strings, n_particles, dim=dim, last=True)
    wout_last = OTC.current
    additionals_last = OTC.additionals
    if simplify:
      res = simplify_current(wout_last, vectorized=OTC.vectorized)
      wout_last.expanded_tensor = res

    wout_last.expanded_tensor = [u.subs(isubs)
                                 for u in wout_last.expanded_tensor]

    if apply_cse:
      Current.cse_current(wout_last, additionals_last, loop=False)
      Current.cse_current(wout, additionals, loop=False)

    select_overall = ifBlock("last .eqv. .false.", tabs=tabs)
    select_not_last = spaceBlock(tabs=tabs)
    select_last = spaceBlock("else", tabs=tabs)
    select_overall.extend([select_not_last, select_last])

    runs = 2
    sct + select_overall
    select_pyfort = [select_not_last, select_last]
    w_out = [wout, wout_last]
    adds = [additionals, additionals_last]

  else:
    if apply_cse:
      Current.cse_current(wout, additionals, loop=False)
    select_pyfort = [sct]
    w_out = [wout]
    adds = [additionals]

  if fermions_in == []:
    fermions_in = None

  if fermions_in is not None:
    if out_is_fermion:
      assert(last_flag)
      ww = wout_last
    else:
      ww = wout
    case = write_helicity_case(lorentz_id, ww, fermions_in,
                               out_is_fermion=out_is_fermion,
                               tabs=tabs)
    hc_cases + case

  for run in range(runs):
    # The results have been vectorized if the outgoing dimension is 1 and if the
    # ougoing particle is not a scalar
    vectorized = False
    if ((len(w_out[run].expanded_tensor) == 1) and (dim != 1)):
      vectorized = True
      vec_dict = {'i': ':'}

    for additional in adds[run]:
      #if isinstance(additional, types.TupleType):
      if type(additional) == tuple:
        lhs = str(additional[0])
        rhs = str(additional[1])
        if lhs.endswith('vec') and vectorized:
          rhs = sub_args(rhs, vec_dict)

        additional = lhs + ' = ' + rhs
      select_pyfort[run].addStatement(additional, 1)

    w_id = "wout"
    if(vectorized):
      sum_dim = 1
      # 'i' is the convention as vectorized parameter for the outgoing current.
      # Consider lorentz_structures.py to see how it shows up.
      w_out[run].expanded_tensor = w_out[run].replace_args(vec_dict)
    else:
      sum_dim = dim
    for j in range(sum_dim):
      dim_id = ''
      if (not vectorized):
        dim_id = "(" + str(j) + ")"
      res = str(w_out[run].expanded_tensor[j])
      select_pyfort[run].addStatement(w_id + dim_id + " = " + res, 1, True)

    if (dim == 1):
      select_pyfort[run] + "wout(1:3) = cnul"

#------------------------------------------------------------------------------#
if __name__ == "__main__":
  import doctest
  doctest.testmod()
  #lorentz_strings = [['P(-1,1)*GammaM(-1,2,1)']]
  #lorentz_strings = [['1*Gamma(1,3,2)'], ['1*GammaP(1,3,2)']]
  #lorentz_strings = [['1']]
  #lorentz_strings = [['-1*P(1,1)*P(2,1)'], ['1*P(-1,1)*P(-1,1)*Metric(1,2)']]
  #lorentz_strings = [['1'], ['1*P(-1,1)*P(-1,1)']]

  #write_tree_vertex(lorentz_strings, 2, 4, '0')
  #lorentz_strings = [['P(-1,2)*GammaM(-1,1,2)']]
  #write_tree_vertex(lorentz_strings, 2, 4, '0', optimized=False)

  #lorentz_strings = [['1*P(1,2)*Metric(2,3)'], ['1*P(3,1)*Metric(1,2)'],
                     #['1*P(3,2)*Metric(1,2)'], ['1*P(1,3)*Metric(2,3)'],
                     #['1*P(2,3)*Metric(1,3)'], ['1*P(2,1)*Metric(1,3)'],
                     #['1*P(-1,2)*Epsilon(-1,1,2,3)'],
                     #['1*P(-1,1)*Epsilon(-1,1,2,3)']]

  #lorentz_strings = [['1*P(2,3)*Metric(1,3)'],
                     #['1*P(-1,2)*Epsilon(-1,1,2,3)']]
  #lorentz_strings = [['1*GammaM(3,1,2)']]
  #ls = [['1*GammaP(-1,2)*P(-1,1,2)'], ['1*ProjP(1,2)'], ['1*ProjM(1,2)']]
  #lorentz_strings = [['1*P(1,3)'], ['1*P(1,2)']]
  #ls = [['1*P(-1,2)*GammaP(-1,1,2)'], ['1*ProjP(1,2)']]
  #ls = [['Metric(1,2)']]

  #tens, add, last = compute_tree(ls, 2, last=False, optimized=True, dim=4)
  #print "tens:", tens.expanded_tensor
  #print "tens.expanded_tensor:", tens.expanded_tensor
  #print "tens.get_all_args():", tens.get_all_args()
  #print min(u.symbol for u in tens.get_all_args() if type(u.symbol) is int)
  #found = tens.find_primitive_tensor_symbol(Symbol('w2'))
  #new_tens = OneTensor()*tens
  #from tensor import tensor, tensor_arg
  ##new_ctens = new_ctens.reconstruct_tensor()
  #write_tree_vertex(ls, 2, 4, '0', fermions_in=None, out_is_fermion=False)
  #arg = tensor_arg(-10, resolve=True, domain=range(4))
  #new_tens = tensor([arg], symbol=Symbol('t'))
  #poltens = tensor([found[0].args[0], arg], numerical=diracAlgebra.ProjM)
  #poltens *= new_tens
  #new_ctens = tens.find_primitive_tensor_symbol(Symbol('w2'), repl=poltens)
  #arg = new_ctens.args[0]
  #arg2 = tensor_arg(-11, resolve=True, domain=range(4))
  #new_ctens.substitute_arg(arg, arg2)
  #new_ctens = new_ctens.reconstruct_tensor()
  #polplus = tensor([arg, arg2], numerical=diracAlgebra.ProjP)
  #polminus = tensor([arg, arg2], numerical=diracAlgebra.ProjM)
  #check1 = new_ctens*polplus
  #check2 = new_ctens*polminus
  #print check1.expanded_tensor == new_ctens.expanded_tensor
  #print check2.expanded_tensor == new_ctens.expanded_tensor
  #print "tens:", tens
  #case = write_helicity_correlation('test', tens, [1, 2], out_is_fermion=False,
                                    #tabs=2)
  #hc_cases + case
  #sub_hc.printStatment()

  #found = new_ctens.find_primitive_tensor_symbol(Symbol('w1'))

  #check in RXi:
  #ls = [['P(1,2)', '-1*P(1,3)']]
  #ls = [['1*P(3,1)', '1*P(3,3)/2']]
  #ls = [['1*P(1,2)*Metric(2,3)'], ['1*P(3,1)*Metric(1,2)'],
                     #['1*P(3,2)*Metric(1,2)'], ['1*P(1,3)*Metric(2,3)'],
                     #['1*P(2,3)*Metric(1,3)'], ['1*P(2,1)*Metric(1,3)'],
                     #['1*P(-1,2)*Epsilon(-1,1,2,3)'],
                     #['1*P(-1,1)*Epsilon(-1,1,2,3)']]
  #ls = [['ProjP(2,1)'], ['ProjM(2,1)']]
  #ls = [['1*GammaM(1,2,3)'], ['1*GammaP(1,2,3)']]

  ##ls = [['ProjM(1,2)'], ['ProjP(1,2)']]
  ##ls = [['Sigma(1,2,3,4)']]
  #ls = [['Sigma(-1,-2,1,2)*Sigma(-2,-1,3,4)'], ['Gamma(-1,1,2)*Gamma(-1,3,4)']]

  from rept1l.currents import gen_tree_class, gen_helicity_subroutine
  ls = [['Gamma(1,3,2)']]
  #ls = [['GammaM(3,1,2)']]
  ls = [['Metric(1,2)*P(3,1)', '-1*Metric(1,3)*P(2,1)',
         'Metric(1,3)*P(2,3)', '-1*Metric(2,3)*P(1,3)'],
        ['-1*Metric(1,2)*P(3,2)', 'Metric(2,3)*P(1,2)']]


  ls = [['P(3,1)*Metric(1,2) - P(3,2)*Metric(1,2) + P(3,3)*Metric(1,2) - ' +
         'P(2,1)*Metric(1,3) + P(2,3)*Metric(1,3) - P(1,1)*Metric(2,3) + ' +
         'P(1,2)*Metric(2,3) - P(1,3)*Metric(2,3)']]
  ls = [['P(3,1)*Metric(1,2) - P(3,2)*Metric(1,2) - ' +
         'P(2,1)*Metric(1,3) + P(2,3)*Metric(1,3) + ' +
         'P(1,2)*Metric(2,3) - P(1,3)*Metric(2,3)']]

  ls = [[
       'P(-1,3)*GammaP(-1,-2,1)*GammaM(3,2,-2)',
       '- P(-1,3)*GammaP(3,-2,1)*GammaM(-1,2,-2)'],
        ['P(-1,3)*GammaP(3,2,-2)*GammaM(-1,-2,1)',
       '- P(-1,3)*GammaP(-1,2,-2)*GammaM(3,-2,1)']
        ]

  ls = [['Metric(1,4)*Metric(2,3)', '-2*Metric(1,3)*Metric(2,4)', 'Metric(1,2)*Metric(3,4)']]
  # ls = [['P(-1,3)*GammaP(-1,-2,1)*GammaM(3,2,-2)',]]

  class_tree, subct, sct = gen_tree_class()
  sub_hc, hc_cases = gen_helicity_subroutine()

  #tens, add, last = compute_tree(ls, 4, last=False, optimized=True, dim=4)
  #print("tens.get_all_args():", tens.get_all_args())
  #print("tens.expanded_tensor:", tens.expanded_tensor)
  # write_tree_vertex(sct[1], hc_cases, ls, 3, 4, '0', fermions_in=[],
                    # out_is_fermion=True, apply_cse=True)
  # write_tree_vertex(sct[1], hc_cases, ls, 4, 0, '0', fermions_in=[1, 2],
                    # out_is_fermion=False, apply_cse=False, simplify=True)
  #write_tree_vertex(sct, sub_hc, ls, 3, 4, '0', fermions_in=None,
                    #out_is_fermion=False, pext='RXI2')
  write_tree_vertex(sct[1], hc_cases, ls, 7, 0, '0', fermions_in=[],
                    out_is_fermion=False, apply_cse=False, simplify=True)

  sct[1].printStatment()
  #sub_hc.printStatment()

  #from rept1l.tensor import tensor_arg, tensor
  #farg = tensor_arg('i', resolve=True, domain=range(4))
  #ftens = tensor([farg], symbol=Symbol('w'))
  #print("ftens.expanded_tensor:", ftens.expanded_tensor)

  #pol_ftens = polarized_fermion(ftens, '+', -1)
  #pol_ftens.expanded_tensor
  #print("pol_ftens.expanded_tensor:", pol_ftens.expanded_tensor)

  #r = test_fermion_helicity(pol_ftens, -2)
  #print("r:", r)
