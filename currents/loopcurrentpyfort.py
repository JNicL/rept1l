#==============================================================================#
#                             loopcurrentpyfort.py                             #
#==============================================================================#

from __future__ import print_function
from past.builtins import xrange
from six import iteritems

#============#
#  Includes  #
#============#

try:
  from types import ListType
  list_type = ListType
except ImportError:
  list_type = list

from sympy import Symbol, sympify, symbols, simplify, I

import rept1l.helper_lib as hl
import rept1l.currents.lorentz_lib as lb
from rept1l.logging_setup import log
from rept1l.combinatorics import (construct_tensor_keys,
                                  construct_tensor_pyfort, fold,
                                  rank_sorted_additionals,
                                  free_indices,
                                  get_tensor_summations)
from rept1l.currents.loopcurrentsymbols import LoopCurrentSymbols
from rept1l.tensor import tensor, tensor_arg
from rept1l.tensor import simplify as simplify_tensor
from rept1l.pyfort import doBlock, spaceBlock, ifBlock
from rept1l.currents.currentsimplify import simplify_current
import rept1l_config

#==============================================================================#
#                            TensorSymmetrizeError                             #
#==============================================================================#

class TensorSymmetrizeError(Exception):
  """ TensorSymmetrizeError exception is raised if the symmetrization of a
  tensor fails.  """
  def __init__(self, value):
    self.value = value

  def __str__(self):
    return repr(self.value)

#==============================================================================#
#                               ZerocheckTensor                                #
#==============================================================================#

class ZerocheckTensor(Exception):
  """ ZerocheckTensor exception is raised if the checking for zero is not
  supported or fails for a specific tensor. """
  def __init__(self, value):
    self.value = value

  def __str__(self):
    return repr(self.value)

#==============================================================================#
#                              LoopCurrentPyFort                               #
#==============================================================================#

class LoopCurrentPyFort(object):
  """ Generates the PyFort Fortran code for offshell (Loop) currents.  """

  # By default current of rank higher equal 2 are not symmetrized
  if(hasattr(rept1l_config, 'symmetrize_currents') and
     rept1l_config.symmetrize_currents is True):
    symmetrize_currents = rept1l_config.symmetrize_currents
  else:
    symmetrize_currents = False

  def __init__(self, tensors, additionals, dim, symmetrize=None, tabs=4,
               apply_cse=True, simplify=True):
    """
    :tensors: the tensor from which the offshell current is computed.
    :additionals: values on which the offshell current depends.
    :dim: dimensionality of the current
    :symmetrize: Symmetrize the rank increase. Only affects currents
                 increasing the rank by two or higher.
    """
    self.max_rank = 0
    self.dim = dim
    self.tabs = tabs
    self.simplify = simplify
    self.additionals = additionals
    if symmetrize is None:
      self.symmetrize = self.symmetrize_currents
    else:
      self.symmetrize = symmetrize

    try:
      self.tensors_ranked, self.max_rank = self.rank_tensors(tensors)
    except TensorSymmetrizeError as e:
      print ('Warning: ' + str(e))
      print ('Disabled symmetrization for this tensor.')
      self.symmetrize = False
      self.tensors_ranked, self.max_rank = self.rank_tensors(tensors)

    self.are_tensors_vectorized()
    self.derived_tensor_structure()
    self.substitute_fortran_symbols()
    if apply_cse:
      self.cse_currents()

    self.rank_additionals()
    self.build_rank_blocks()

    add_keys = list(sorted(self.additionals_ranked.keys()))
    if 0 in add_keys:
      add_keys.pop(0)
      self.fill_additionals_rank0()

    # construct the wavefunction symbols (left-hand side of the equation)
    wps = LoopCurrentSymbols(self.max_rank)
    wps.build_symbols(self.tensors_ranked, self.rank_sum is not None, self.dim,
                      symmetrize=self.symmetrize)

    self.fill_wavefunction_pyfort(wps)

    for key in add_keys:
      for u in self.additionals_ranked[key]:
        self.rankN_new[key]['additional'] + (str(u[0]) + " = " +
                                             str(self.additionals_dict[u[0]]))

  def substitute_fortran_symbols(self):
    """ Substitutes for certain symbols which are predefined in Fortran """
    isubs = {I: Symbol('cima')}

    for rank in self.tensors_ranked:
      et = self.tensors_ranked[rank].expanded_tensor
      self.tensors_ranked[rank].expanded_tensor = [u.subs(isubs) for u in et]

    self.additionals = [(u[0], u[1].subs(isubs)) for u in self.additionals]

  @staticmethod
  def symmetrize_tensor(tensor, rank):
    """  Symmetrizes a tensor.
    Symmetrization for rank 2:
      T^12 = 1/2 (T^12 + T^21)
    """
    from sympy import Rational

    def sym2(T, arg1, arg2):
      repl = {arg1.symbol: 'dummy' + arg1.symbol}
      T = T.tensor_arg_substitution(repl)
      repl = {arg2.symbol: arg1.symbol}
      T = T.tensor_arg_substitution(repl)
      repl = {'dummy' + arg1.symbol: arg2.symbol}
      T = T.tensor_arg_substitution(repl)
      return T

    if rank == 2:
      args = {str(u.symbol): u for u in tensor.args
              if str(u.symbol) in ['mu', 'nu']}
      if len(args) != 2:
        raise Exception('Did not find mu, nu args in tensor of rank 2.')

      if args['mu'].resolve or args['nu'].resolve:
        new_arg = args['mu'].copy()
        new_arg.resolve = True
        new_arg.changed = False
        tensor.substitute_arg(args['mu'], new_arg)
        new_arg = args['nu'].copy()
        new_arg.resolve = True
        new_arg.changed = False
        tensor.substitute_arg(args['nu'], new_arg)
        tensor = tensor.reconstruct_tensor()

      repl = {'mu': 'dummy'}
      tensor2 = tensor.copy()
      tensor2 = tensor2.tensor_arg_substitution(repl)
      repl = {'nu': 'mu'}
      tensor2 = tensor2.tensor_arg_substitution(repl)
      repl = {'dummy': 'nu'}
      tensor2 = tensor2.tensor_arg_substitution(repl)

      ret = (tensor2 + tensor)
      ret = ret * Rational(1, 2)
      return ret
    elif rank == 3:
      args = {str(u.symbol): u for u in tensor.args
              if str(u.symbol) in ['mu', 'nu', 'ro']}
      if len(args) != 3:
        raise Exception('Did not find mu, nu, ro args in tensor of rank 3.')

      arg_symbols = ['mu', 'nu', 'ro']
      if any(args[u].resolve for u in arg_symbols):
      #if args['mu'].resolve or args['nu'].resolve or args['ro'].resolve:
        #new_arg = args['mu'].copy()
        #new_arg.resolve = True
        #new_arg.changed = False
        #tensor.substitute_arg(args['mu'], new_arg)
        for u in arg_symbols:
          new_arg = args[arg_symbols].copy()
          new_arg.resolve = True
          new_arg.changed = False
          tensor.substitute_arg(args[u], new_arg)
        tensor = tensor.reconstruct_tensor()

        #new_arg = args['nu'].copy()
        #new_arg.resolve = True
        #new_arg.changed = False
        #tensor.substitute_arg(args['nu'], new_arg)
        #tensor = tensor.reconstruct_tensor()
        #new_arg = args['ro'].copy()
        #new_arg.resolve = True
        #new_arg.changed = False
        #tensor.substitute_arg(args['ro'], new_arg)
        #tensor = tensor.reconstruct_tensor()

      # symmetrized 23
      R132 = tensor.copy()
      R132 = sym2(R132, args['nu'], args['ro'])
      R1_23 = tensor + R132

      # symmetrized 12
      R23_1 = R1_23.copy()
      R23_1 = sym2(R23_1, args['mu'], args['nu'])
      R1_23_23_1 = R1_23 + R23_1

      # symmetrized 13
      R3_2_1 = R23_1.copy()
      R3_2_1 = sym2(R3_2_1, args['mu'], args['ro'])
      R1_2_3 = (R1_23_23_1 + R3_2_1)*Rational(1, 2*3)
      return R1_2_3
    else:
      raise Exception('Rank ' + str(rank) +
                      ' not supported in symmetrize_tensor')

  def rank_tensors(self, tensors):
    """ Replaces tensor arguments `mu`, `munu` by the rank `1`, `2`

    tensors = {'mu': 'T1', '0': 'T2', 'munu': 'T3'}
    tensors_ranked, max_rank = LoopCurrentPyFort.rank_tensors(tensors)
    max_rank
    tensors_ranked
    {0: 'T2', 1: 'T1', 2: 'T3'}
    """
    # determine the highest rank among all tensors
    max_rank = len(tensors)

    keys = []
    for rank in range(max_rank):
      keys.append(construct_tensor_keys(max_rank, rank)[0])

    res_dict = {}
    for key in tensors:
      res_dict[keys.index(key)] = tensors[key]

    if self.symmetrize:
      if max_rank > 4:
        errmsg = ('Rank increase by ' + str(max_rank-1) +
                  ' is not yet supported with symmetrization.')
        raise TensorSymmetrizeError(errmsg)
      if 2 in res_dict:
        # case nu < mu-1
        symfac = 2
        t = res_dict[2].copy()
        #t = t.reconstruct_tensor()
        t = symfac*self.symmetrize_tensor(t, 2)
        # case mu = nu
        symfac = 1
        t2 = res_dict[2].copy()
        # if one argument is resolved, the other one needs to be resolved too,
        # otherwise cannot set mu = nu
        args = {str(u.symbol): u for u in t2.args
                if str(u.symbol) in ['mu', 'nu']}
        if args['mu'].resolve or args['nu'].resolve:
          new_arg = args['mu'].copy()
          new_arg.resolve = True
          new_arg.changed = False
          t2.substitute_arg(args['mu'], new_arg)
          new_arg = args['nu'].copy()
          new_arg.resolve = True
          new_arg.changed = False
          t2.substitute_arg(args['nu'], new_arg)
          t2 = t2.reconstruct_tensor()

        repl = {'nu': 'mu'}
        t2 = t2.tensor_arg_substitution(repl)
        res_dict[2] = t
        res_dict[(1, 1)] = t2
      if 3 in res_dict:
        # case ro < nu < mu
        symfac = 6
        t = res_dict[3].copy()
        #t = t.reconstruct_tensor()
        ts = self.symmetrize_tensor(t, 3)
        res_dict[3] = ts*symfac

        # cases mu = nu, mu = ro, nu = ro
        symfac = 2
        # if one argument is resolved, the other one needs to be resolved too,
        # otherwise cannot set mu = nu
        #arg_symbols = ['mu', 'nu', 'ro']
        #args = {str(u.symbol): u for u in t2.args
                #if str(u.symbol) in arg_symbols}
        #if any(args[u].resolve for u in arg_symbols):
          #for u in arg_symbols:
            #new_arg = args[u].copy()
            #new_arg.resolve = True
            #new_arg.changed = False
            #t2.substitute_arg(args[u], new_arg)
          #t2 = t2.reconstruct_tensor()

        t12 = ts.copy()
        repl = {'nu': 'mu'}
        t12 = t12.tensor_arg_substitution(repl)
        repl = {'ro': 'nu'}
        t12 = t12.tensor_arg_substitution(repl)
        res_dict[(1, 1, 2)] = t12*symfac

        t13 = ts.copy()
        repl = {'ro': 'mu'}
        t13 = t13.tensor_arg_substitution(repl)
        res_dict[(1, 2, 1)] = t13*symfac

        t23 = ts.copy()
        repl = {'ro': 'nu'}
        t23 = t23.tensor_arg_substitution(repl)
        res_dict[(2, 1, 1)] = t23*symfac

        # cases mu = nu = ro
        #symfac = 1
        t123 = ts.copy()
        repl = {'ro': 'mu', 'nu': 'mu'}
        t123 = t123.tensor_arg_substitution(repl)
        res_dict[(1, 1, 1)] = t123
    return res_dict, max_rank

  def fill_additionals_rank0(self):
    """ Fills the rank0 addtionals pyfort block. """
    for u in self.additionals_ranked[0]:
      if self.rank_sum is None:
        self.rank0.addStatement(str(u[0]) + " = " +
                                self.additionals_dict[u[0]],
                                n_tabs=0, treat_as_eq=True)
      else:
        if u[1]:
          repl_dict = {'rank': 'j'}
          res = hl.string_subs(self.additionals_dict[u[0]], repl_dict)
          self.rank0.addStatement(str(u[0]) + " = " + res, n_tabs=0,
                                  treat_as_eq=True)
        else:
          self.pre_rank_sum.addStatement(str(u[0]) + " = " +
                                         self.additionals_dict[u[0]],
                                         n_tabs=0, treat_as_eq=True)

  def fill_rank0_derived(self, tens, repl_dict, rank):
    """ Fills the rank0 derived tensor structures pyfort block. """
    derived_tensor = tens.derived_tensor
    res = derived_tensor.replace_args(repl_dict)
    derived_tensor_args_expanded = [tensor_arg(u.symbol,
                                    domain=u.domain, resolve=True)
                                    for u in derived_tensor.args]
    derived_tensor_expanded = tensor(derived_tensor_args_expanded,
                                     symbol=Symbol(derived_tensor.symbol.
                                                   name))
    repl_dict = {'rank': 'j'}

    if self.simplify:
      tens_expanded_tenor = simplify_current(tens)

    else:
      tens_expanded_tenor = tens.expanded_tensor

    rhs = tens.replace_args(repl_dict, res=tens_expanded_tenor)

    if (not self.symmetrize) or rank == 1 or rank == (1, 1):
      for q in range(len(derived_tensor_expanded.expanded_tensor)):
        wave = str(derived_tensor_expanded.expanded_tensor[q])
        self.rank0.addStatement(wave + " = " + rhs[q], n_tabs=0,
                                treat_as_eq=True)
    elif rank == 2:
      nu_pos = derived_tensor_expanded.get_args().index('nu')
      mu_pos = derived_tensor_expanded.get_args().index('mu')
      for pos, arg in enumerate(derived_tensor_expanded.paired_arguments):
        nu_arg = arg[nu_pos]
        mu_arg = arg[mu_pos]
        if nu_arg < mu_arg:
          wave = str(derived_tensor_expanded[arg])
          self.rank0.addStatement(wave + " = " + rhs[pos], n_tabs=0,
                                  treat_as_eq=True)

    return res

  def are_tensors_vectorized(self):
    self.vectorized = {}
    for rank in self.tensors_ranked:
      tens = self.tensors_ranked[rank]
      if rank not in self.vectorized:
        self.vectorized[rank] = True
      if hasattr(tens, 'derived_tensor') and tens.derived_tensor is not None:
        self.vectorized[rank] = (self.vectorized[rank] and
                                 tens.derived_tensor.
                                 no_arg_resolved)
      else:
        self.vectorized[rank] = (self.vectorized[rank] and
                                 tens.no_arg_resolved)

  def nonzero_rank(self, rank):
    """Verifies if the `rank` contribution is zero. """
    t = self.tensors_ranked[rank]
    if hasattr(t, 'derived_tensor') and t.derived_tensor is not None:
      t = t.derived_tensor
    if rank == 1 or rank == (1, 1) or (rank == 2 and not self.symmetrize):
      return any(u != 0 for u in t.expanded_tensor)
    elif (rank == 2 or rank == (1, 1, 2) or rank == (1, 2, 1)
          or rank == (2, 1, 1)):
      nu_pos = t.get_args().index('nu')
      mu_pos = t.get_args().index('mu')
      for arg in t.paired_arguments:
        nu_arg = arg[nu_pos]
        mu_arg = arg[mu_pos]
        if type(mu_arg) is int and type(nu_arg) is int:
          if nu_arg < mu_arg:
            if t[arg] != 0:
              return True
        else:
          # in symmetrized form, the metrics are zero in rank 2 because
          zerotest = t[arg].subs({Symbol('gg(mu,nu)'): 0,
                                  Symbol('gg(nu,mu)'): 0})
          if zerotest != 0:
            return True
      return False
    elif rank == 3:
      ro_pos = t.get_args().index('ro')
      nu_pos = t.get_args().index('nu')
      mu_pos = t.get_args().index('mu')
      for arg in t.paired_arguments:
        ro_arg = arg[ro_pos]
        nu_arg = arg[nu_pos]
        mu_arg = arg[mu_pos]
        if type(mu_arg) is int and type(nu_arg) is int:
          if nu_arg < mu_arg:
            if t[arg] != 0:
              return True
        elif type(mu_arg) is int and type(ro_arg) is int:
          if ro_arg < mu_arg:
            if t[arg] != 0:
              return True
        elif type(nu_arg) is int and type(ro_arg) is int:
          if ro_arg < nu_arg:
            if t[arg] != 0:
              return True
        else:
          # in symmetrized form, the metrics are zero in rank 2 because
          zerotest = t[arg].subs({Symbol('gg(mu,nu)'): 0,
                                  Symbol('gg(nu,mu)'): 0,
                                  Symbol('gg(mu,ro)'): 0,
                                  Symbol('gg(ro,mu)'): 0,
                                  Symbol('gg(nu,ro)'): 0,
                                  Symbol('gg(ro,nu)'): 0,
                                  })
          if zerotest != 0:
            return True
      return False
    else:
      errmsg = ('Check for nonzero_rank not implemented for rank: ' +
                str(rank) + '.')
      raise ZerocheckTensor(errmsg)

  def build_rankN_block(self, tabs=0):
    """ Builds the Fortran loops over all lorentz indices. """
    max_rank = self.max_rank - 1
    if max_rank < 1:
      return None

    if max_rank > 3 and self.symmetrize:
      raise Exception('Rank increase: ' + str(max_rank) +
                      ' not implemented in PyFort for symmetrized currents.')

    loops_ranked = {}
    prev_ri = 'j'
    loop_indices = ['3'] + free_indices[:max_rank]
    if self.symmetrize:
      indices = [loop_indices[0]] + [u + '-1' for u in loop_indices[1:]]
    else:
      indices = ['3']*(max_rank+1)

    # Determine the number of nested do mu = 0, 3 loops
    nloops = max_rank
    if self.symmetrize:
      for rank in xrange(max_rank-1, -1, -1):
        try:
          nonzero = self.nonzero_rank(rank+1)
        except ZerocheckTensor as e:
          nonzero = True
          print ('Warning: ' + str(e))

        if not nonzero:
          print('rank ' + str(rank+1) + ' is zero')
          nloops = rank
        else:
          break

    for k in xrange(nloops):
      loops_ranked[k+1] = {}
      doblock = doBlock(loop_indices[k + 1] + ' = 0, ' + indices[k],
                        tabs=tabs+k)
      addblock = spaceBlock(tabs=tabs+k)
      spaceblock = spaceBlock(tabs=tabs+k)
      doblock + addblock
      doblock + spaceblock
      if k > 0:
        loops_ranked[k]['do_loop'] + doblock

      prev_ri = 'incRI(' + loop_indices[k+1] + ', ' + prev_ri + ')'

      spaceblock + ('riOut = ' + prev_ri)
      loops_ranked[k+1]['do_loop'] = doblock
      loops_ranked[k+1]['space'] = spaceblock
      loops_ranked[k+1]['additional'] = addblock

    if self.symmetrize:
      for k in xrange(1, max_rank):
        # rank two case
        if k == 1:
          # rank two increase
          prev_ri = 'j'
          prev_ri = 'incRI(' + loop_indices[k] + ', ' + prev_ri + ')'
          #firstRI = 'firstRI(' + loop_indices[k] + ',' + prev_ri + ')'
          prev_ri = 'incRI(' + loop_indices[k] + ', ' + prev_ri + ')'
          spaceblock_trace = spaceBlock(tabs=tabs+k-1)
          spaceblock_trace + ('riOut = ' + prev_ri)
          loops_ranked[k]['do_loop'] + spaceblock_trace
          loops_ranked[(1, 1)] = {'space': spaceblock_trace}
          if max_rank == 3:
            # rank three increase
            prev_ri = 'j'
            prev_ri = 'incRI(' + loop_indices[k] + ', ' + prev_ri + ')'
            prev_ri = 'incRI(' + loop_indices[k] + ', ' + prev_ri + ')'
            prev_ri = 'incRI(' + loop_indices[k] + ', ' + prev_ri + ')'
            spaceblock_trace = spaceBlock(tabs=tabs+k-1)
            spaceblock_trace + ('riOut = ' + prev_ri)
            loops_ranked[k]['do_loop'] + spaceblock_trace
            loops_ranked[(1, 1, 1)] = {'space': spaceblock_trace}
        elif k == 2:
          # rank three increase case (1, 2, 1) (mu = ro != nu)
          prev_ri = 'j'
          prev_ri = 'incRI(' + loop_indices[k-1] + ', ' + prev_ri + ')'
          prev_ri = 'incRI(' + loop_indices[k] + ', ' + prev_ri + ')'
          prev_ri = 'incRI(' + loop_indices[k-1] + ', ' + prev_ri + ')'
          spaceblock_trace = spaceBlock(tabs=tabs+k-1)
          spaceblock_trace + ('riOut = ' + prev_ri)
          loops_ranked[k]['do_loop'] + spaceblock_trace
          loops_ranked[(1, 2, 1)] = {'space': spaceblock_trace}
          # rank three increase case (2, 1, 1) (mu != ro = nu)
          prev_ri = 'j'
          prev_ri = 'incRI(' + loop_indices[k-1] + ', ' + prev_ri + ')'
          prev_ri = 'incRI(' + loop_indices[k] + ', ' + prev_ri + ')'
          prev_ri = 'incRI(' + loop_indices[k] + ', ' + prev_ri + ')'
          spaceblock_trace = spaceBlock(tabs=tabs+k-1)
          spaceblock_trace + ('riOut = ' + prev_ri)
          loops_ranked[k]['do_loop'] + spaceblock_trace
          loops_ranked[(2, 1, 1)] = {'space': spaceblock_trace}
          # rank three increase case (1, 1, 2) (mu != ro = nu)
          prev_ri = 'j'
          prev_ri = 'incRI(' + loop_indices[k-1] + ', ' + prev_ri + ')'
          prev_ri = 'incRI(' + loop_indices[k] + ', ' + prev_ri + ')'
          prev_ri = 'incRI(' + loop_indices[k] + ', ' + prev_ri + ')'
          spaceblock_trace = spaceBlock(tabs=tabs+k-1)
          spaceblock_trace + ('riOut = ' + prev_ri)
          loops_ranked[k]['do_loop'] + spaceblock_trace
          loops_ranked[(1, 1, 2)] = {'space': spaceblock_trace}

    return loops_ranked

  def build_rank_blocks(self):
    """ Build up the rank structure rank0 and rankN. The rank_sum/pre_rank_sum
    over the incoming rank is needed if

    - the rank increase is > 0
    - additionals dependent on the incoming rank
    - dimension of the wavefunction > 1
    """
    tab_indentation = 0

    rank0_additional_rankdep = False
    if 0 in self.additionals_ranked:
      for add in self.additionals_ranked[0]:
        rank0_additional_rankdep = rank0_additional_rankdep or add[1]

    # try to vectorize the j index (rank incoming)
    # This is possible if the rank is not increased and the wavefunction is
    # scalar. (In case of non-scalars the wavefunction index is preferably
    # vectorized)
    if self.max_rank > 1 or rank0_additional_rankdep or self.dim > 1:
      self.pre_rank_sum = spaceBlock(tabs=self.tabs)
      self.rank_sum = doBlock('j = riMaxIn, 0, -1', tabs=self.tabs)
      tab_indentation = 1
    else:
      self.rank_sum = None

    self.rank0 = spaceBlock(tabs=self.tabs + tab_indentation)
    self.rankN, self.factors = construct_tensor_pyfort(self.max_rank,
                                                       self.tabs +
                                                       tab_indentation)

    self.rankN_new = self.build_rankN_block(tabs=self.tabs + tab_indentation)
    if self.rank_sum is not None:
      self.rank_sum.append(self.rank0)
      if self.rankN_new is not None and 'do_loop' in self.rankN_new[1]:
        self.rank_sum.append(self.rankN_new[1]['do_loop'])

  def fill_rank0_pyfort(self, wps, tensor, k=0):
    """ Fills the rank0 pyfort block. """
    rank = 0
    repl_dict = wps.repl_dict[rank]
    tensor = tensor[rank]

    if self.simplify:
      tensor_expanded_tensor = simplify_current(tensor,
                                                vectorized=self.vectorized[0])
    else:
      tensor_expanded_tensor = tensor.expanded_tensor

    res = tensor.replace_args(repl_dict, res=tensor_expanded_tensor)

    for j in range(len(res)):
      if isinstance(wps.wp[rank], list_type):
        wave = wps.wp[rank][j]
      else:
        wave = wps.wp[rank]
      rhs = str(res[j])
      self.rank0.addStatement(wave + " = " + rhs, n_tabs=0, treat_as_eq=True)

  def fill_rank_pyfort(self, wps, tensor, rank, k=0):
    """ Fills the rankN pyfort block. The blocks are build up recusively and for
    every rank `rank` the structure is:

    ------------
      beforeRI        -> Computes additionals of rank `rank`
    ------------

    do loop over one lorentz index, e.g. mu or nu or ...

      if firstRI:
      ------------
        firstRI           -> initialize wavefunction
      ------------
      endif
      ---------------
      not_first_RI        -> Computes the outgoing wavefunction, the outgoing
      ---------------        rank index is determined dynamically

    """

    r = rank
    if_string = "(riOut.gt.riMaxIn) .and. isFirstRI"

    indent = {1: 2, 2: 3, (1, 1): 2, (2, 1, 1): 3, (1, 2, 1): 3,
              (1, 1, 2): 3, 3: 4, (1, 1, 1): 2}[r]

    if isinstance(wps.wp[r], list_type):
      wave = wps.wp[r][k]
    else:
      wave = wps.wp[r]

    if r not in self.beforeRI.keys():
      self.beforeRI[r] = spaceBlock(tabs=self.tabs)

    if r not in self.not_firstRI.keys():
      self.not_firstRI[r] = spaceBlock(tabs=self.tabs + indent - 1)

    for tens in tensor:
      wave = str(wps.wp[r])

      # check for leading sign
      if tens[0] == '-':
        add = ' '
        tens = tens[0] + ' ' + tens[1:]
      else:
        add = " + "
      self.not_firstRI[r] += (wave + " = " + wave + add + tens)

  def fill_wavefunction_pyfort(self, wps):
    """ Fills the left-and right-hand side of the wavefunction. The results are
    stored as pyfort code in rank0 and rankN attributes.
    """
    self.beforeRI = {}
    self.not_firstRI = {}

    res_keys = list(sorted(self.tensors_ranked.keys()))
    if 0 in res_keys:
      res_keys.pop(0)
      self.fill_rank0_pyfort(wps, self.tensors_ranked)

    # print("self.rankN_new:", self.rankN_new)
    res_keys = list(sorted(u for u in res_keys if u in self.rankN_new))
    for rank in res_keys:
      repl_dict = wps.repl_dict[rank]

      # only compute highter rank if necessary
      if rank == 1:
        iflto = 'if(rankInc .lt. 1) cycle'
        (self.rank0 > 0) + iflto
      elif rank == (1, 1):
        #iflto = 'if(rankInc .lt. 2) continue'
        #self.not_firstRI[1] + iflto
        pass
      elif rank > 1:
        iflto = 'if(rankInc .lt. ' + str(rank) + ') cycle'
        self.not_firstRI[rank-1] + iflto

      # no space means that the tensor has been found to be zero
      if 'space' in self.rankN_new[rank]:
        t = self.tensors_ranked[rank]

        if hasattr(t, 'derived_tensor') and t.derived_tensor is not None:
          res = self.fill_rank0_derived(t, repl_dict, rank)
        else:
          res = self.tensors_ranked[rank]
          if self.simplify:
            res_tens = simplify_current(res, vectorized=self.vectorized[rank])
          else:
            res_tens = res.expanded_tensor

          res = res.replace_args(repl_dict, res=res_tens)

        self.fill_rank_pyfort(wps, res, rank)

        # Add block with additionals of rank `rank`
        self.rankN_new[rank]['space'] + self.beforeRI[rank]

        # Add block with wavefunctions of rank `rank`
        self.rankN_new[rank]['space'] + self.not_firstRI[rank]

  def cse_currents(self):
    """ Wrapper for cse_current """
    from .current import Current
    simplify_func = None
    if self.simplify:
      for rank, res in iteritems(self.tensors_ranked):
        res_tens = simplify_current(res, vectorized=self.vectorized[rank])
        self.tensors_ranked[rank].expanded_tensor = res_tens
    t, add = Current.cse_current(self.tensors_ranked, self.additionals,
                                 loop=True, simplify=simplify_func)
    self.tensors_ranked = t
    return add

  def derived_tensor_symbol(self, tens):
    """ Replaces the tensor by a new tensor `symbol` with non-resolved
    arguments. A computation instruction for the new tensor is derived. """

    # build new args: the wavefunction index i and lorentz index mu which are
    # not resolved
    args = tens.args
    args_derived = (tensor_arg(arg.symbol, domain=arg.domain)
                    for arg in args if str(arg.symbol) != 'rank')

    # ignore args of dimension 0
    args_derived = [u for u in args_derived if len(u.domain) > 1]
    symbol = Symbol('sp' + str(len(args_derived)))

    # since the tensor is not vectorized there are arguments which are
    # unresolved. Need to reconstruct the tensor with resolved arguments
    args_non_derived = {arg: tensor_arg(arg.symbol, domain=arg.domain,
                        resolve=True)
                        for arg in args if str(arg.symbol) != 'rank'}

    tensor_derived = tensor(args_derived, symbol=symbol)
    for old_arg, new_arg in iteritems(args_non_derived):
      tens.substitute_arg(old_arg, new_arg)

    tens = tens.reconstruct_tensor()
    tens.derived_tensor = tensor_derived
    return tens

  def derived_tensor_structure(self):
    """ Tensors of rank higher equal 1 which cannot be written in a
    vectorized form need to be expressed by a new tensor symbol. """
    if self.max_rank > 1:
      req_derived = [u for u in self.vectorized
                     if (not self.vectorized[u] and u > 0)]
      for rank in req_derived:
        dt = self.derived_tensor_symbol(self.tensors_ranked[rank])
        self.tensors_ranked[rank] = dt

  def rank_additionals(self):
    self.additionals_ranked = rank_sorted_additionals(self.additionals)

    # keys: additional symbol, value: additional expression
    self.additionals_dict = {}
    # replace the wavefunction index i by ':'
    # replace rank by the index j (the rank index is j)
    repl_dict = {'i': ':', 'rank': 'j'}
    for add in self.additionals:
      self.additionals_dict[add[0]] = hl.subs_arguments(str(add[1]), repl_dict)

  def gen_output(self, select_case_lorentz=None):
    if select_case_lorentz is None:
      if self.rank_sum is None:
        self.rank0.printStatment()
        if self.rankN is not None:
          self.rankN[0][0].printStatment()
      else:
        self.pre_rank_sum.printStatment()
        self.rank_sum.printStatment()
    else:
      if self.rank_sum is None:
        select_case_lorentz.append(self.rank0)
        if self.rankN is not None:
          select_case_lorentz.append(self.rankN[0][0])

      else:
        select_case_lorentz.append(self.pre_rank_sum)
        select_case_lorentz.append(self.rank_sum)

      if self.init_to_zero is not None:
          select_case_lorentz.addStatement(self.init_to_zero,
                                           n_tabs=self.tabs + 1)
      return select_case_lorentz

if __name__ == "__main__":
  #import doctest
  #doctest.testmod()
 t = LoopCurrentPyFort.build_rankN_block(3)
 print("t:", t)
 t = t[1]['do_loop']
 t.printStatment()
