#=============================================================================#
#                                class_form.py                                #
#=============================================================================#

#============#
#  Includes  #
#============#
import os
from rept1l.pyfort import selectBlock, loadBlock
from rept1l.pyfort import caseBlock, subBlock, modBlock, containsBlock
from rept1l.pyfort import spaceBlock

#############
#  Methods  #
#############


class FormCurrentPyfort(object):
  """ Handles all modules storing form current informaton. """
  registry = {}
  tabs = 1

  @classmethod
  def get_selectcase(cls, currentlabel, legs, new_case):
    """ Returns the select case pyfort block associated to the key
    (currentlabel, str(legs)). If it does not exist it is generated.
    """
    currkey = (currentlabel, legs)
    if currkey not in cls.registry:
      currmod, currcase = gen_form_current_case(currentlabel, legs,
                                                tabs=cls.tabs)
      cls.registry[currkey] = {'mod': currmod,
                               'selectcase': currcase,
                               'cases': [new_case]}
    if new_case not in cls.registry[currkey]['cases']:
      cls.registry[currkey]['cases'].append(new_case)

    return cls.registry[currkey]['selectcase']

  @classmethod
  def get_module(cls, currentlabel, legs):
    """ Returns the select case pyfort block associated to the key
    (currentlabel, str(legs))."""
    currkey = (currentlabel, legs)
    if currkey not in cls.registry:
      raise Exception('Could not return module for: ' +
                      currentlabel + ' ' + str(legs) +
                      '. No such module.')
    return cls.registry[currkey]['mod']

  @classmethod
  def write_cases(cls, mod, sc, tabs=2):
    """ Writes the cases to the class_form.f90

    :param mod: The module which contains the general routines
    :type  mod: PyFort

    :param sc: The select case block.
    :type  sc: PyFort
    """
    for currkey in cls.registry:
      cases = ', '.join(cls.registry[currkey]['cases'])
      cb = caseBlock(cases, tabs=tabs)
      case_label = currkey[0]
      sc + cb

      # build use `module`
      module_label = 'fc' + case_label
      routine_label = 'form' + case_label
      usemodule = 'use ' + module_label + ', only: ' + routine_label
      assert(hasattr(mod, 'useblock'))
      mod.useblock + usemodule

      # building the arguments of the call for `legs`
      parameters = ['ty', 'legs', 'nDef', 'ind', 'e', 'cou', 'den',
                    'denGSB', 'rxip', 'last', 'cgs', 'cw', 'cj', 'C', 'm', 'p']
      args = '(' + ', '.join(parameters) + ')'
      # routine convenvtion is `compute` + case_label, see gen_current_case

      callcompcurr = 'call form' + case_label + args
      cb + callcompcurr

    # default case
    cb = caseBlock('default', tabs=tabs)
    cb + "write(int_str,'(i3)') ty"
    cb + ("call error_mdl('Unhandled case: ' // int_str, " +
          "where='form_current_mdl')")
    sc + cb


def gen_form_rule(legs, subroutine_label, last=True, tabs=1):
  """ Generates a Fortran subroutine which can be filled with an off-shell
  form current rule """
  assert(legs > 1 and legs < 9)
  parameters = ['ty', 'legs', 'nDef', 'ind', 'e', 'cou', 'den',
                'denGSB', 'rxip', 'last', 'cgs', 'cw', 'cj', 'C', 'm', 'p']

  args = ', '.join(parameters)

  sub = subBlock(subroutine_label, args, tabs=tabs + 1)
  sub.addType(("integer, intent(in)", "ty, legs, nDef, ind, e(:)"))

  sub.addType(("logical, intent(in)", "cou(:), last"))
  sub.addType(("character(len=*), intent(in)", "den, denGSB, cgs, cw(:), cj, C"))
  sub.addType(("character(len=5), intent(in)", "m(1:nDef)"))
  sub.addType(("character(len=7), intent(in)", "rxip"))
  sub.addType(("character(len=10), intent(in)", "p(1:nDef)"))
  sub.addType(("character(len=80)", "indent=''"))

  return sub


def gen_form_current_case(case_label, legs, tabs=1):
  """ Generates a new module which contains all form currents of specific type
  """
  module_label = 'fc' + case_label
  currmod = modBlock(module_label)

  #======================#
  #  contains selection  #
  #======================#

  contains = containsBlock(tabs=tabs)

  subroutine_label = 'form' + case_label
  sub = gen_form_rule(legs, subroutine_label, last=False, tabs=tabs - 1)

  #====================#
  #  select_case_tree  #
  #====================#

  select_case = selectBlock('ty', tabs=tabs + 1)

  # hierarchy
  currmod + '\n'
  currmod + contains
  contains + '\n'
  sub + select_case
  contains + sub

  currmod.addPrefix('use class_vertices', tabs=tabs)
  currmod.addPrefix('implicit none', tabs=tabs)

  return currmod, select_case


def gen_form_class(tabs=1):
  ffpath = os.path.join(os.environ['REPT1L_PATH'], 'formutils')
  ffpath = os.path.join(ffpath, 'FortranTemplates')

  class_form = modBlock("class_form")
  class_form.addPrefix("use class_vertices")
  class_form.addPrefix('implicit none', tabs=tabs)

  contains_form = containsBlock(tabs=tabs)

  fC_args1 = ('lp, t, legs, j, nMax, e, f, w, cs, absolute_order, ty,' +
              ' order_increase, sign')
  fC_args2 = "cou, colT_imag, colT_symbol, Nc_power, branch_type, branch"
  subroutine_formCurrent = subBlock("form_current_mdl", tabs=tabs,
                                    arg=fC_args1 + ", " + fC_args2)

  subroutine_formCurrent.addPrefix('use input_mdl,' +
                                   ' only: complex_mass_frm, masscut_frm, error_mdl')
  subroutine_formCurrent.addPrefix("use class_vertices, only: get_propagator_extension_mdl")
  subroutine_formCurrent.addPrefix("use class_particles, only: order_ids, &")
  subroutine_formCurrent.addPrefix("get_particle_id_mdl, &")
  subroutine_formCurrent.addPrefix("get_particle_mass_id_mdl, &")
  subroutine_formCurrent.addPrefix("get_mass_name_mdl, &")
  subroutine_formCurrent.addPrefix("get_particle_spin_mdl, &")
  subroutine_formCurrent.addPrefix("get_particle_mass_reg_mdl")
  # hacking the possibility to add further `use` statments by accessing
  # useblock
  useblock = spaceBlock(tabs=tabs)
  subroutine_formCurrent.appendD(useblock)
  class_form.useblock = useblock

  scf = selectBlock("ty", tabs=tabs + 1)

  # abbreviation
  rf = subroutine_formCurrent

  fC_args1 = ("lp, t, legs, j, nMax, e(:), f(:), w(:), " +
              "cs, ty, sign, Nc_power, " +
              "branch_type, branch")
  fC_args2 = "cou(:), colT_imag"
  fC_args3 = "order_increase, absolute_order"
  rf.addType(("integer, intent(in)", fC_args1))
  rf.addType(("character(len=20), intent(in)", 'colT_symbol'))
  rf.addType(("logical, intent(in)", fC_args2))
  rf.addType(("integer, dimension(:), intent(in)", fC_args3))
  rf.addPrefix(loadBlock(os.path.join(ffpath, 'class_form_prefix.f90')))
  rf.addLineD()

  rf.append(loadBlock(os.path.join(ffpath, 'amp.f90'), tabs=rf.tabs + 1))
  rf.addLine()

  # hierarchy
  class_form + contains_form
  contains_form + subroutine_formCurrent
  subroutine_formCurrent + scf
  return class_form, scf
