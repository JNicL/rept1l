#==============================================================================#
#                                class_tree.py                                 #
#==============================================================================#

#============#
#  Includes  #
#============#

from rept1l.combinatorics import fold
from rept1l.pyfort import modBlock, containsBlock, subBlock, interfaceBlock
from rept1l.pyfort import selectBlock, ifBlock, caseBlock, spaceBlock


###############################################################################
#                              TreeCurrentPyfort                              #
###############################################################################

class TreeCurrentPyfort(object):
  """ Handles all modules storing tree current informaton. """
  registry = {}
  tabs = 1

  @classmethod
  def get_selectcase(cls, currentlabel, legs, new_case):
    """ Returns the select case pyfort block associated to the key
    (currentlabel, str(legs)). If it does not exist it is generated.
    """
    currkey = (currentlabel, legs)
    if currkey not in cls.registry:
      currmod, currcase = gen_current_case(currentlabel, legs, tabs=cls.tabs)
      cls.registry[currkey] = {'mod': currmod,
                               'selectcase': currcase,
                               'cases': [new_case]}
    if new_case not in cls.registry[currkey]['cases']:
      cls.registry[currkey]['cases'].append(new_case)

    return cls.registry[currkey]['selectcase']

  @classmethod
  def get_module(cls, currentlabel, legs):
    """ Returns the select case pyfort block associated to the key
    (currentlabel, str(legs))."""
    currkey = (currentlabel, legs)
    if currkey not in cls.registry:
      raise Exception('Could not return module for: ' +
                      currentlabel + ' ' + str(legs) +
                      '. No such module.')
    return cls.registry[currkey]['mod']

  @classmethod
  def write_cases(cls, mod, sc, tabs=2):
    """ Writes the cases to the class_tree.f90

    :param mod: The module which contains the general routines
    :type  mod: PyFort

    :param sc: The select case block.
    :type  sc: PyFort
    """
    for currkey in cls.registry:
      cases = ', '.join(cls.registry[currkey]['cases'])
      cb = caseBlock(cases, tabs=tabs)
      case_label = currkey[0]
      legs = currkey[1]
      sc[legs - 2] + cb

      # build use `module`
      module_label = 'tc' + case_label
      routine_label = 'tree' + case_label
      usemodule = 'use ' + module_label + ', only: ' + routine_label
      assert(hasattr(mod, 'useblock'))
      mod.useblock + usemodule

      # building the arguments of the call for `legs`
      parameters = ['ty', 'co']
      p_parameters = ['P', 'PL']
      for j in range(1, legs):
        wavefunction = ['w' + str(j)]
        parameter = list(map(lambda x: x + str(j), p_parameters))
        parameters += (wavefunction + parameter)

      parameters += ['den', 'm', 'wout', 'last', 'VB_last']
      args = '(' + ', '.join(parameters) + ')'
      # routine convenvtion is `compute` + case_label, see gen_current_case
      callcompcurr = 'call tree' + case_label + args
      cb + callcompcurr

    # default cases
    for i in range(8):
      cb = caseBlock('default', tabs=tabs)
      cb + "write(ty_str,'(i3)') ty"
      cb + ("call error_mdl('Unhandled case: ' // ty_str, " +
            "where='compute_tree_mdl')")
      sc[i] + cb


#==============================================================================#
#                               gen_compute_rule                               #
#==============================================================================#

def gen_compute_rule(legs, subroutine_label, last=True, tabs=1):
  """ Generates a Fortran subroutine which can be filled with an off-shell
  current rule """
  assert(legs > 1 and legs < 9)
  parameters = ['ty', 'co']
  p_parameters = ['P', 'PL']
  for j in range(1, legs):
    wavefunction = ['w' + str(j)]
    parameter = list(map(lambda x: x + str(j), p_parameters))
    parameters.extend(wavefunction + parameter)

  # in case the currents are split, last argument is not required,
  # but VB_last and passed (back) instead.
  if(not last):
    parameters += ['den', 'm', 'wout', 'last', 'VB_last']
  else:
    parameters += ['den', 'm', 'wout', 'last']

  args = fold(lambda x, y: x + ', ' + y, parameters[1:], parameters[0])

  sub = subBlock(subroutine_label, args, tabs=tabs + 1)
  sub.addPrefix("use class_vertices")
  sub.addType(("integer, intent(in)", "ty"))

  if(not last):
    sub.addType(("logical, intent(inout)", "VB_last"))
  else:
    sub.addType(("logical", "VB_last"))
  sub.addType(("logical, intent(in)", "last"))
  sub.addType(("complex(kind=dp), dimension(0:), intent(in)", "co"))

  # incoming momenta and wavefunctions
  for j in range(1, legs):
    sub.addType(('complex(kind=dp), intent(in), dimension(0:3)',
                 'P' + str(j) + ', PL' + str(j)))
    wave = 'w' + str(j)
    sub.addType(('complex(kind=dp), intent(in), dimension(0:3)', wave))

  # mass of the outgoing particle
  sub.addType(("complex(kind=dp), intent(in)", "m"))
  # propagator denominator
  sub.addType(("complex(kind=dp), intent(in)", "den"))
  # outgoing wavefunctions
  sub.addType(("complex(kind=dp), dimension(0:3), intent(out)", "wout"))

  sub.addType(("character(len=3)", "ty_str"))
  sub.addType(("complex(kind=dp), dimension(0:3)", "pl, p"))

  return sub


#==============================================================================#
#                               gen_current_case                               #
#==============================================================================#

def gen_current_case(case_label, legs, tabs=1):
  """ Generates a new module which contains all currents of specific type """
  module_label = 'tc' + case_label
  currmod = modBlock(module_label)

  #======================#
  #  contains selection  #
  #======================#

  contains = containsBlock(tabs=tabs)

  #==========================#
  #  subroutine computeTree  #
  #==========================#

  subroutine_label = 'tree' + case_label
  sub = gen_compute_rule(legs, subroutine_label, last=False, tabs=tabs)

  #====================#
  #  select_case_tree  #
  #====================#

  select_case = selectBlock('ty', tabs=tabs + 2)

  # hierarchy
  currmod + contains
  sub + select_case
  contains + sub

  currmod.addPrefix('use class_vertices', tabs=tabs)
  currmod.addPrefix('use constants_mdl', tabs=tabs)
  currmod.addPrefix('implicit none', tabs=tabs)

  return currmod, select_case


#==============================================================================#
#                                gen_tree_class                                #
#==============================================================================#

def gen_tree_class(tabs=1):
  class_tree = modBlock('class_tree')

  #======================#
  #  contains selection  #
  #======================#

  contains_tree = containsBlock(tabs=tabs)

  max_legs = 8

  #==========================#
  #  subroutine computeTree  #
  #==========================#

  subroutine_compute_tree = []
  for i in range(2, max_legs + 1):
    sub = gen_compute_rule(i, 'computeTree' + str(i), last=True, tabs=tabs - 1)
    subroutine_compute_tree.append(sub)

  #=========================#
  #  interface_computeTree  #
  #=========================#

  interface_computeTree = interfaceBlock("compute_tree_mdl", tabs=tabs)
  routines = ['computeTree' + str(i) for i in range(2, max_legs + 1)]
  routines = fold(lambda x, y: x + ", " + y, routines[1:], routines[0])
  interface_computeTree.addPrefix("module procedure " + routines, 1)

  #====================#
  #  select_case_tree  #
  #====================#

  select_case_tree = [selectBlock('ty', tabs=tabs + 1)
                      for i in range(1, max_legs + 1)]

  # hierarchy
  class_tree.addLine()
  class_tree + interface_computeTree
  class_tree.addLine()
  class_tree + contains_tree
  contains_tree + '\n'

  for i, sub in enumerate(subroutine_compute_tree):
    contains_tree + sub
    contains_tree.addLine()

    sub + '\n'
    sub += 'wout(:) = 0.0d0 '
    sub += 'VB_last = .false.'
    sub + '\n'
    sub + select_case_tree[i]

    lastcase = ifBlock('last .eqv. .false.', tabs=tabs + 1)
    lastcase += 'wout(:) = cima * wout(:)/den'
    lastcase = (lastcase > 0) + 'else'
    lastcase += 'if (VB_last) wout(0) = - wout(0)'

    sub + lastcase

  class_tree.addPrefix('use constants_mdl', 1)
  class_tree.addPrefix('use input_mdl, only: error_mdl', tabs=tabs)

  # hacking the possibility to add further `use` statments by accessing
  # useblock
  useblock = spaceBlock(tabs=tabs - 1)
  class_tree.appendD(useblock)
  class_tree.useblock = useblock

  class_tree.addPrefix('implicit none', 1)

  return class_tree, subroutine_compute_tree, select_case_tree
