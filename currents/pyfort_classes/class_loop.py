###############################################################################
#                                class_loop.py                                #
###############################################################################

##############
#  Includes  #
##############

import rept1l_config
from rept1l.pyfort import modBlock, containsBlock, subBlock, spaceBlock
from rept1l.pyfort import interfaceBlock, selectBlock, caseBlock
from rept1l.combinatorics import fold

###############################################################################
#                              LoopCurrentPyFort                              #
###############################################################################

class LoopCurrentPyfort(object):
  """ Handles all modules storing loop current informaton. """
  registry = {}
  tabs = 1

  @classmethod
  def get_selectcase(cls, currentlabel, legs, new_case):
    """ Returns the select case pyfort block associated to the key
    (currentlabel, str(legs)). If it does not exist it is generated.
    """
    currkey = (currentlabel, legs)
    if currkey not in cls.registry:
      currmod, currcase = gen_loop_current_case(currentlabel, legs,
                                                tabs=cls.tabs)
      cls.registry[currkey] = {'mod': currmod,
                               'selectcase': currcase,
                               'cases': [new_case]}
    if new_case not in cls.registry[currkey]['cases']:
      cls.registry[currkey]['cases'].append(new_case)

    return cls.registry[currkey]['selectcase']

  @classmethod
  def get_module(cls, currentlabel, legs):
    """ Returns the select case pyfort block associated to the key
    (currentlabel, str(legs))."""
    currkey = (currentlabel, legs)
    if currkey not in cls.registry:
      raise Exception('Could not return module for: '
                      + currentlabel + ' ' + str(legs) +
                      '. No such module.')
    return cls.registry[currkey]['mod']

  @classmethod
  def write_cases(cls, mod, sc, tabs=2):
    """ Writes the cases to the class_loop.f90

    :param mod: The module which contains the general routines
    :type  mod: PyFort

    :param sc: The select case block.
    :type  sc: PyFort
    """
    for currkey in cls.registry:
      cases = ', '.join(cls.registry[currkey]['cases'])
      cb = caseBlock(cases, tabs=tabs)
      case_label = currkey[0]
      legs = currkey[1]
      sc[legs-2] + cb

      # build use `module`
      module_label = 'lc' + case_label
      routine_label = 'loop' + case_label
      usemodule = 'use ' + module_label + ', only: ' + routine_label
      assert(hasattr(mod, 'useblock'))
      mod.useblock + usemodule

      # building the arguments of the call for `legs`
      parameters = ['ty', 'co']
      p_parameters = ['P', 'PL']
      for j in range(1, legs):
        wavefunction = ['wl'] if j == 1 else ['w' + str(j)]
        parameter = list(map(lambda x: x + str(j), p_parameters))
        parameters += (wavefunction + parameter)

      parameters += ['m', 'wp', 'riMaxIn', 'riMaxOut', 'rankInc']
      args = '(' + ', '.join(parameters) + ')'
      # routine convenvtion is `compute` + case_label, see gen_current_case
      callcompcurr = 'call loop' + case_label + args
      cb + callcompcurr

    # default cases
    for i in range(8):
      cb = caseBlock('default', tabs=tabs)
      cb + "write(ty_str,'(i3)') ty"
      cb + ("call error_mdl('Unhandled case: ' // ty_str, " +
            "where='compute_loop_mdl')")
      sc[i] + cb

###############################################################################
#                            gen_loop_compute_rule                            #
###############################################################################

def gen_loop_compute_rule(legs, subroutine_label, tabs=1):
  """ Generates a Fortran subroutine which can be filled with an off-shell
  loop current rule """
  assert(legs > 1 and legs < 9)
  parameters = ['ty', 'co']
  p_parameters = ['P', 'PL']
  for j in range(1, legs):
    wavefunction = ['wl'] if j == 1 else ['w' + str(j)]
    parameter = list(map(lambda x: x + str(j), p_parameters))
    parameters += (wavefunction + parameter)

  parameters += ['m', 'wp', 'riMaxIn', 'riMaxOut', 'rankInc']

  args = ', '.join(parameters)

  sub = subBlock(subroutine_label, args, tabs=tabs+1)
  sub.addPrefix("use class_vertices")
  sub.addType(("integer, intent(in)", "riMaxIn, riMaxOut, rankInc"))
  # Mass, Momentum and Wavefunctions
  for j in range(1, legs):
    sub.addType(('complex (kind = dp), intent(in), dimension(0:3)',
                 'P' + str(j) + ', PL' + str(j)))

    wave = 'w' + str(j) if j != 1 else 'wl'
    dim = 'dimension(0:3)' if j != 1 else 'dimension(0:3, 0:riMaxIn)'
    sub.addType(('complex (kind = dp), intent(in), ' + dim, wave))

  sub.addType(("complex (dp), intent(out), dimension(0:3,0:riMaxOut)", "wp"))
  sub.addType(("complex (kind = dp), intent(in)", "m"))
  sub.addType(("complex (kind = dp), dimension(0:3)", "pl, p"))
  sub.addType(("integer, intent(in)", "ty"))
  sub.addType(("complex (kind = dp), dimension(0:), intent(in)", "co"))
  sub.addType(("complex (kind = dp), dimension(0:3)", "sp1"))
  sub.addType(("complex (kind = dp), dimension(0:3,0:3)", "sp2"))
  sub.addType(("complex (kind = dp), dimension(0:3,0:3,0:3)", "sp3"))
  sub.addType(("complex (kind = dp), dimension(0:3,0:3,0:3,0:3)", "sp4"))
  sub.addType(("character(len=3)", "ty_str"))
  sub.addType(("integer", "riOut, j"))
  sub.addType(("logical", "isFirstRI"))
  sub.addType(("integer", "mu, nu, ro, ta, te, tu"))

  return sub

###############################################################################
#                            gen_loop_current_case                            #
###############################################################################

def gen_loop_current_case(case_label, legs, tabs=1):
  """ Generates a new module which contains all currents of specific type """
  module_label = 'lc' + case_label
  currmod = modBlock(module_label)

  #======================#
  #  contains selection  #
  #======================#

  contains = containsBlock(tabs=tabs)

  ######################
  #  subroutine loopX  #
  ######################

  subroutine_label = 'loop' + case_label
  sub = gen_loop_compute_rule(legs, subroutine_label, tabs=tabs)

  #====================#
  #  select_case_loop  #
  #====================#

  select_case = selectBlock('ty', tabs=tabs+2)

  # hierarchy
  currmod + contains
  sub + select_case
  contains + sub

  currmod.addPrefix('use class_vertices', tabs=tabs)
  currmod.addPrefix('use constants_mdl', tabs=tabs)

  currmod.addPrefix('implicit none', tabs=tabs)

  return currmod, select_case

#=====================#
#  PyFort class_loop  #
#=====================#

def gen_loop_class(tabs=1):
  class_loop = modBlock("class_loop")
  contains_loop = containsBlock(tabs=tabs)

  max_legs = 8

  subroutine_compute_loop = []
  for i in range(2, max_legs + 1):
    sub = gen_loop_compute_rule(i, 'computeLoop' + str(i), tabs=tabs-1)
    subroutine_compute_loop.append(sub)

  interface_computeLoop = interfaceBlock("compute_loop_mdl", tabs=tabs)
  routines = ['computeLoop' + str(i) for i in range(2, max_legs + 1)]
  routines = ', '.join(routines)
  interface_computeLoop.addPrefix("module procedure " + routines, 1)

  select_case_loop = [selectBlock('ty', tabs=tabs+1)
                      for i in range(1, max_legs + 1)]

  class_loop + '\n'
  class_loop + interface_computeLoop
  class_loop + '\n'
  class_loop + contains_loop
  contains_loop + '\n'

  for i, sub in enumerate(subroutine_compute_loop):
    contains_loop + sub
    contains_loop + '\n'
    sub + select_case_loop[i]

  class_loop.addPrefix("use constants_mdl", 1)
  class_loop.addPrefix('use input_mdl, only: error_mdl', tabs=tabs)

  # hacking the possibility to add further `use` statments by accessing
  # useblock
  useblock = spaceBlock(tabs=tabs-1)
  class_loop.appendD(useblock)
  class_loop.useblock = useblock

  class_loop.addPrefix("implicit none", 1)

  return class_loop, subroutine_compute_loop, select_case_loop
