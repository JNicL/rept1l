################################################################################
#                              currentsimplify.py                              #
################################################################################

from sympy import Symbol, Poly, simplify
from sympy.core.exprtools import factor_terms
import rept1l_config

# check whether mathematica simplify is enabled
if(hasattr(rept1l_config, 'mathematica_simplify') and
   rept1l_config.mathematica_simplify is True):
  enable_mathematica_simplify = True
  from rept1l.currents.mathematica_simplify import mathematica_simplify
else:
  enable_mathematica_simplify = False

# build a basis of known vector expressions
nparticles = 6
vectorbase = []
wfbase = []
for i in range(1, nparticles):
  vectorbase.append(Symbol('P' + str(i) + '(i)'))
  if i == 1:
    vectorbase.append(Symbol('w' + str(i) + '(i,j)'))
  else:
    vectorbase.append(Symbol('w' + str(i) + '(i)'))
  for mu in range(4):
    w = 'w' + str(i) + '(' + str(mu) + ')'
    wfbase.append(Symbol(w))
  w = 'w' + str(i) + '(i)'
  wfbase.append(Symbol(w))
  w = 'w' + str(i) + '(mu)'
  wfbase.append(Symbol(w))
  w = 'w' + str(i) + '(nu)'
  wfbase.append(Symbol(w))
  w = 'w' + str(i) + '(ro)'
  wfbase.append(Symbol(w))

for mu in range(4):
  w = 'wl(' + str(mu) + ',rank)'
  wfbase.append(Symbol(w))

w = 'wl(i,rank)'
wfbase.append(Symbol(w))
w = 'wl(mu,rank)'
wfbase.append(Symbol(w))
w = 'wl(nu,rank)'
wfbase.append(Symbol(w))
w = 'wl(ro,rank)'
wfbase.append(Symbol(w))

vectorbase.append(Symbol('gg(i,mu)'))
vectorbase.append(Symbol('gg(mu,i)'))
vectorbase.append(Symbol('gg(i,nu)'))
vectorbase.append(Symbol('gg(nu,i)'))


def simplify_current(tensor, vectorized=True, cp=False, wfp=False):
  ret = []
  for tpos, t in enumerate(tensor.expanded_tensor):
    ret.append(simplify_wrapper(t, vp=vectorized, cp=cp, wfp=wfp))

  return ret

ncouplings = 100
coupling_base = ['co(' + str(i) + ')' for i in range(ncouplings)]

def vectorize_poly_simplify(expr, vp=True, **kwargs):
  ret = []
  vb = [u for u in expr.free_symbols if 'vec' in u.name] + vectorbase
  tp = Poly(expr, vb).as_dict()
  for key in tp:
    tp[key] = simplify_wrapper(tp[key], vp=False, **kwargs)
  return Poly(tp, vb).as_expr()

def wf_poly_simplify(expr, wfp=True, **kwargs):
  cb = [u for u in expr.free_symbols if u in wfbase]
  if len(cb) > 0:
    cbe = Poly(expr, cb).as_dict()
    for key in cbe:
      cbe[key] = simplify_wrapper(cbe[key], wfp=False, **kwargs)
    return Poly(cbe, cb).as_expr()
  else:
    return simplify_wrapper(expr, wfp=False, **kwargs)

def coupling_poly_simplify(expr, cp=True, **kwargs):
  cb = [u for u in expr.free_symbols if u.name in coupling_base]
  if len(cb) > 0:
    cbe = Poly(expr, cb).as_dict()
    for key in cbe:
      cbe[key] = simplify_wrapper(cbe[key], cp=False, **kwargs)
    return Poly(cbe, cb).as_expr()
  else:
    return simplify_wrapper(expr, cp=False, **kwargs)

def simplify_wrapper(expr, **kwargs):
  if enable_mathematica_simplify:
    try:
      return mathematica_simplify(expr)
    except Exception as e:
      return factor_terms(expr)
  else:
    return factor_terms(expr)
