###############################################################################
#                              test_currents.py                               #
###############################################################################

#============#
#  Includes  #
#============#

from test_tools import *
from rept1l.currents.current import Current
from sympy import simplify

class TestCurrents(object):

  def test_pp(self):
    ls = [['P(1,1)*P(2,1)']]
    OLC = Current(ls, 2, optimized=False, dim=4, loop_current=False)
    current = OLC.current
    print("current.expanded_tensor:", current.expanded_tensor)
    comp0 = simplify(current.expanded_tensor[0])
    expc = ('P1(i)*co(0)*' +
            '(-P1(0)*w1(0) + P1(1)*w1(1) + P1(2)*w1(2) + P1(3)*w1(3))')
    eq_(str(comp0), expc)

  def test_proj(self):
    ls = [['ProjM(2,1)', '-ProjP(2,1)']]
    OLC = Current(ls, 3, optimized=False, dim=1, loop_current=False)
    current = OLC.current
    comp0_expec = ('co(0)*' +
                   '(-w1(0)*w2(0) - w1(1)*w2(1) + w1(2)*w2(2) + w1(3)*w2(3))')

    comp0 = simplify(current.expanded_tensor[0])
    eq_(str(comp0), comp0_expec)

  def test_gamma(self):
    ls = [['1*GammaM(1,3,2)'], ['1*GammaP(1,3,2)']]
    OLC = Current(ls, 3, optimized=False, dim=4, loop_current=False)
    current = OLC.current
    comp0_expec = ('-co(0)*m*' +
                   '(w2(2)*(w1(0) + w1(3)) + w2(3)*(w1(1) - I*w1(2)))' +
                   ' - co(1)*(w2(0)*'
                   '(-pl(0)*w1(0) + pl(0)*w1(3) + pl(3)*w1(1) + I*pl(3)*' +
                   'w1(2)) - w2(1)*(-pl(0)*w1(1) + I*pl(0)*w1(2) + ' +
                   'pl(3)*w1(0) + pl(3)*w1(3)))')

    comp1_expec = ('-co(0)*m*(w2(2)*' +
                   '(w1(1) + I*w1(2)) + w2(3)*(w1(0) - w1(3))) - co(1)*' +
                   '(w2(0)*(pl(1)*w1(1) + I*pl(1)*w1(2) - ' +
                   'pl(2)*w1(0) + pl(2)*w1(3)) - w2(1)*(pl(1)*w1(0) + ' +
                   'pl(1)*w1(3) - pl(2)*w1(1) + I*pl(2)*w1(2)))')

    comp0 = current[(0,)]
    from sympy import sympify, simplify
    # expressions are parsed again because f() is parsed as a function instead
    # of a symbol.
    eq_(simplify(sympify(str(comp0))-sympify(comp0_expec)), 0)

    comp1 = current[(1,)]
    eq_(simplify(sympify(str(comp1))-sympify(comp1_expec)), 0)

  def test_sigmasigma(self):
    # Test broken, but it was never checked for correctness
    ls = [['Sigma(-1,-2,1,2)*Sigma(-2,-1,3,4)/4']]
    OLC = Current(ls, 4, optimized=False, dim=4, loop_current=False)
    current = OLC.current

    from sympy import sympify, simplify
    comp0 = current[(0,)]
    comp0_expec = ('co(0)*(m*(2*w1(0)*(w2(0)*w3(0) + w2(1)*w3(1)) + ' +
                   'w1(2)*w2(2)*w3(0) + w1(3)*w2(3)*w3(0)) + 2*pl(1)*w1(2)*' +
                   '(w2(2)*w3(2) + w2(3)*w3(3)) - 2*pl(2)*w1(3)*(w2(2)*w3(2) ' +
                   '+ w2(3)*w3(3)) - w1(0)*w2(0)*(-pl(1)*w3(2) + pl(2)*w3(3))' +
                   ' + w1(1)*w2(1)*(pl(1)*w3(2) - pl(2)*w3(3)))')
    eq_(simplify(sympify(str(comp0))-sympify(comp0_expec)), 0)

    comp3 = current[(3,)]
    comp3_expec = ('co(0)*(m*(w1(0)*w2(0)*w3(3) + w1(1)*w2(1)*w3(3) + ' +
                   '2*w1(3)*(w2(2)*w3(2) + w2(3)*w3(3))) + 2*pl(1)*w1(1)*(' +
                   'w2(0)*w3(0) + w2(1)*w3(1)) + 2*pl(3)*w1(0)*(w2(0)*w3(0)' +
                   ' + w2(1)*w3(1)) + w1(2)*w2(2)*(pl(1)*w3(1) + pl(3)*w3(0))' +
                   ' + w1(3)*w2(3)*(pl(1)*w3(1) + pl(3)*w3(0)))')
    eq_(simplify(sympify(str(comp3))-sympify(comp3_expec)), 0)

  def test_epsilonpp(self):
    ls = [['-Epsilon(1,3,-1,-2)*P(-1,1)*P(-2,3)',
           'Epsilon(1,3,-2,-1)*P(-1,1)*P(-2,3)']]
    OLC = Current(ls, 3, optimized=False, dim=4, loop_current=False)
    current = OLC.current
    from sympy import sympify, simplify
    comp0 = current[(0,)]
    comp0_expec = ('2*co(0)*w2(0)*(P1(1)*(-P1(2)*w1(3) + P1(3)*w1(2)) + '
                   'P1(1)*(-P2(2)*w1(3) + P2(3)*w1(2)) + P1(2)*(P1(1)*w1(3) ' +
                   '- P1(3)*w1(1)) + P1(2)*(P2(1)*w1(3) - P2(3)*w1(1)) + ' +
                   'P1(3)*(-P1(1)*w1(2) + P1(2)*w1(1)) + ' +
                   'P1(3)*(-P2(1)*w1(2) + P2(2)*w1(1)))')
    eq_(simplify(sympify(str(comp0))-sympify(comp0_expec)), 0)
