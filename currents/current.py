#==============================================================================#
#                                  current.py                                  #
#==============================================================================#

#============#
#  Includes  #
#============#

from six import iteritems
from sympy import Symbol, I, cse, sympify, simplify

import rept1l.helper_lib as hl
from rept1l.tensor import diracAlgebra
from rept1l.tensor import tensor, tensor_arg
from rept1l.baseutils import CurrentBase
from rept1l.combinatorics import lorentz_base, construct_tensor_keys
from rept1l.combinatorics import shift_tensor_keys_dict, fold
from rept1l.combinatorics import get_lorentz_key_components, lorentz_base_dict
from rept1l.combinatorics import order_strucs, lorentz_sympy_dict
import rept1l.currents.lorentz_lib as lb
import rept1l.currents.lorentz_structures

#==============================================================================#
#                                 TreeCurrent                                  #
#==============================================================================#

class Current(object):
  """ Generates tree or loop (offshell-)current rules from Feynman Rules. The
  rules are stored in the attributes `current`, `additionals` and
  `feynman_rule`.  `current` corresponds to the offshell current and
  `additionals` are structures which need to be computed before the current.
  `feynman_rule` is a symbolic expression for the offshell current.
  """

  def __init__(self, feynman_rules, final, last=False, dim=4, prop_num=1,
               loop_current=False, simplify=True, **kwargs):
    """
    The Feynman rules are given in FeynRules syntax as follows:

    [['P(3,1)*Metric(1,2)', '-1*P(3,2)*Metric(1,2)', '-1*P(2,1)*Metric(1,3)',
      'P(2,3)*Metric(1,3)', 'P(1,2)*Metric(2,3)', '-1*P(1,3)*Metric(2,3)']]
    -> 1 coupling multiplied on 6 different structures

    [['1*GammaM(3,2,1)'], ['2*GammaP(3,2,1)']]
    -> 2 couplings multiplied on 2 different structures

    The outer parenthesis represents structures for different couplings and the
    inner parenthesis represents substructures multiplied with the same
    coupling.

    :param feynman_rules: List of Feynman rules as strings.
    :type  feynman_rules: list

    :param final: Argument index of the outgoing particle.
    :type  final: int

    :param last: If last is true the current is truncated by the propagator, or
                 aquivalently no propagator is multiplied for building the
                 current.
    :type  last: bool

    :param dim: dimension of the wavefunction
    :type  dim: int

    :param prop_num: Propagator numerator. Multiplied in addition to couplings.
    :type  prop_num: int or sympy

    :param rank_rep: If rank_rep is true the current depends on loop momenta.
                     The algorithm computes the loop current rule rather than
                     tree current.
    :type  rank_rep: bool

    """

    # set some attributes used/modified by class methods
    self.dirac = diracAlgebra  # numerical values for lorentz structures
    self.vectorized = False    # current can be written in vectorized way
    self.last_flag = False     # current differs for the last current
    self.final = final         # index of final particle
    self.dim = dim             # wavefunction dimension
    self.loop_current = loop_current   # tree or loop current
    if loop_current:
      self.lorentz_func = lb.lorentz_func_dict_loop
    else:
      self.lorentz_func = lb.lorentz_func_dict_tree

    # stores all additionals which need to be computed before computing the
    # current. current depends on additonals.
    additionals = []

    # list of tensors which are mulitplied with a different coupling each
    results = []

    # simple symbolic expression for the feynmanrules
    result_sym = []

    # computes the expression for each Feynman rule and collects additionals

    # outer loop runs over structures with different couplings
    for k, substruc_diff in enumerate(feynman_rules):
      additional = []
      tensors_list = []
      tensor_sym = 0

      def expand_base(s):
        bs = CurrentBase(s)
        ret = bs.compute(range(final))
        return ret

      # inner loop runs over structures with different couplings
      for substruc_same in substruc_diff:

        substruc_same = expand_base(substruc_same)
        for substruc_add in substruc_same:
          # if the structure is a loop current intermediate results are split
          # according to the rank increase
          if self.loop_current:
            self.max_rank = 0
            msyms = {}
            msyms[str(self.max_rank)] = tensor([], symbol=1)
          else:
            msyms = tensor([], symbol=1)
          tensor_sym += self.build_symbolic(substruc_add)
          outcurr = self.build_current(substruc_add, msyms, additional,
                                       last=last, **kwargs)
          msyms, additional, last_flag = outcurr

          if self.loop_current:
            msyms = self.shift_tensor_args(msyms)
          tensors_list.append(msyms)
          if len(additional) > 0:
            for add in additional:
              if add not in additionals:
                additionals.append(add)

      result_sym.append(tensor_sym)
      results.append(self.merge_tensors(tensors_list))

    # merge the result to one tensor
    results = self.multiply_couplings(results, prop_num)
    result_sym = [Symbol('c' + str(pos)) * u
                  for pos, u in enumerate(result_sym)]

    # store results
    self.current = results
    self.additionals = additionals
    self.feynman_rule = result_sym
    if not self.loop_current:
      self.last_flag = last_flag

  def get_scalars(self, nparticles, args):
    """ Determines whether the structure is missing scalar wavefunction.

    The problem is that they are not immeadiately apparent in the Feynman Rule
    as they do not have an open argument"""
    scalar_waves = [v for v in range(1, nparticles)  # sum not over the last
                    if v not in (u for u in hl.flatten(args) if u > 0)]

    scalars = 1
    for i in scalar_waves:
      # For loop currents the first incoming wavefunction depends on the rank
      if i == 1 and self.loop_current:
        # TODO: (nick 2016-01-04) adding the scalar argument fails exporting
        # derived tensors. test on ls =  [['1*ProjM(3,2)']]
        scalar_arg = tensor_arg("0", domain=[0], resolve=False)
        rank_arg = tensor_arg("rank", domain=None, resolve=False)
        waveloop_symbol = Symbol('wl')
        scalars *= tensor([scalar_arg, rank_arg], symbol=waveloop_symbol)
      else:
        scalars *= Symbol('w' + str(i) + '(0)')
    return scalars

  @classmethod
  def build_feynman_rule(cls, lco, final):
    ret = []

    def expand_base(s):
      bs = CurrentBase(s)
      ret = bs.compute(range(final))
      return ret

    def combine_substruc(substrucs):
      tensor_sym = 0
      for substruc_add in expand_base(substrucs):
        tensor_sym += cls.build_symbolic(substruc_add)
      return tensor_sym

    # outer loop runs over structures with different couplings
    for pos, substruc_diff in enumerate(lco):
      # inner loop runs over structures with different couplings
      tensor_sym = sum(combine_substruc(substruc_same)
                       for substruc_same in substruc_diff)
      print("tensor_sym:", tensor_sym)
      ret.append(Symbol('c' + str(pos)) * tensor_sym)
    return ret


  @staticmethod
  def build_symbolic(ls):
    """ Constructs a sympy expression from ls.
    >>> P = Symbol('P'); g = Symbol('g')
    >>> c = Current([['P(1,3)*Metric(2,3)','-1*P(1,2)*Metric(2,3)']], 3)
    >>> c.feynman_rule[0]
    c0*(-Metric(2,3)*P(1,2) + Metric(2,3)*P(1,3))
    """
    # lorentz structure identifier
    ls_key = ls[0]

    arg_id, prefactor = ls[1]

    # derive the arguments from the the lorentz id and argument id
    lbs, nparticles = get_lorentz_key_components(ls_key)
    lbs = order_strucs(lbs)

    # pure scalar case
    if lbs == []:
      args = None
    else:  # all other cases are stores in lorentz_base_dict
      args = lorentz_base_dict[ls_key][arg_id]

    expr = prefactor
    for pos, lb in enumerate(lbs):
      if args is not None:
        arg = ','.join(str(u) for u in args[pos])
        expr *= Symbol(lb.name + '(' + arg + ')')
    return expr

  def build_current(self, ls, msyms, add, last=False, **kwargs):
    """ Extends the current msysm by the structure `ls` and updates the
    requrired additionals `add`"""

    # lorentz structure identifier
    ls_key = ls[0]

    arg_id, prefactor = ls[1]

    # derive the arguments from the the lorentz id and argument id
    lbs, nparticles = get_lorentz_key_components(ls_key)
    lbs = order_strucs(lbs)
    # pure scalar case
    if lbs == []:
      args = None
    # all other cases are stores in lorentz_base_dict
    else:
      args = lorentz_base_dict[ls_key][arg_id]

    # build outgoing current(tensor) by multiplying the Feynman rules with
    # incoming wavefunctions while keeping outgoing arguments open
    extra = None
    for pos, lbs_sym in enumerate(lbs):
      lbs_name = None
      for ls_str, ls_sym in iteritems(lorentz_sympy_dict):
        if ls_sym.name == lbs_sym.name:
          lbs_name = ls_str
          break
      if lbs_name is None:
        raise Exception('Structure ' + lbs.name +
                        ' not found in lorentz_sympy_dict.')

      lbs_args = args[pos]
      if self.loop_current:
        extra_arg = {'max_rank': self.max_rank}
      else:
        extra_arg = {'last_contraction': last}
      # Add the propagator extension if present in kwargs
      if 'pext' in kwargs:
        extra_arg['pext'] = kwargs['pext']

      msyms, add, vect, extra = (self.lorentz_func[lbs_name](
                                  self.final, self.dirac, self.vectorized,
                                  self.last_flag, add, msyms, lbs_args,
                                  **extra_arg))
      self.vectorized = vect
      self.last_flag = extra
      if self.loop_current:
        self.max_rank = extra

    open_args = [lorentz_base.get_open_args(l, args[pos])
                 for pos, l in enumerate(lbs)]
    scalars = self.get_scalars(nparticles, open_args)

    if self.loop_current:
      for rank in msyms:
        msyms[rank] = msyms[rank] * scalars * prefactor
    else:
      msyms = msyms * scalars * prefactor

    return msyms, add, extra

  def merge_tensors(self, tensors_list):
    """ Performs the sum over tensors in `tensors_list` """
    if self.loop_current:
      # collect all occuring ranks
      key_set = set()
      for res_sub in tensors_list:
        key_set.update(res_sub.keys())

      tensors_merged = {}
      for key in key_set:
        tmp = tensor([], symbol=0)
        for res_sub in tensors_list:
          if key in res_sub:
            tmp = tmp + res_sub[key]
        tensors_merged[key] = tmp
      return tensors_merged
    else:
      if len(tensors_list) > 1:
        return fold(lambda x, y: x + y, tensors_list, tensor([], symbol=0))
      else:
        return tensors_list[0]

  def multiply_couplings(self, results, prop_num):
    """ Multiplies the tensors with couplings and adds up the result.
    """

    # couplings_ranked tracks the couplings which appear in higher rank
    # computations. This information is used to make computations faster when
    # couplings are zero.
    if self.loop_current:
      rankdict = {'0': 0, 'mu': 1, 'munu': 2, 'munuro': 3}
      self.couplings_ranked = {}

    for res_pos, res in enumerate(results):
      c = Symbol("co(" + str(res_pos) + ")")
      if self.loop_current:
        for key, res_r in iteritems(res):
          # register coupling with id `res_pos` appearing at rank `rank`
          rank = rankdict[key]
          if rank not in self.couplings_ranked:
            self.couplings_ranked[rank] = [res_pos]
          else:
            self.couplings_ranked[rank].append(res_pos)

          t = (prop_num * tensor([], symbol=c) * res_r)
          results[res_pos][key] = t
      else:
        t = prop_num * tensor([], symbol=c) * res
        results[res_pos] = t

    if len(results) > 1:
      if self.loop_current:
        keys_all = []
        for result in results:
          for key in result:
            if key not in keys_all:
              keys_all.append(key)
        merged_results = {}
        for key in keys_all:
          merged_results[key] = fold(lambda x, y: x + y,
                                     [u[key] for u in results if key in u],
                                     tensor([], symbol=0))

        results[0] = merged_results
        for i in range(1, len(results)):
          results[i] = {}
      else:
        merged_results = {}
        # only the first slot of the list is nonzero. returning of this type is
        # kept for compatibility with older version
        results[0] = fold(lambda x, y: x + y, results, tensor([], symbol=0))
        for i in range(1, len(results)):
          results[i] = {}
    return results[0]

  def shift_tensor_args(self, msyms):
    """ Makes the tensor symmetric in the tensor arguments if necessary.

    When the rank is increased by 2 the
    indices mu and nu occur simulataneously at rank 1. This happens for
    instance, if there is a structure p^mu p^nu. If the momentum p has a
    loop (q) and tree momentum (k) part, i.e. p = q + k, then we have in general
    p^mu p^nu = k^mu k^nu + k^mu q^nu + q^mu k^nu + q^mu q^nu.


    # TODO: (nick 2015-12-29) check if this is actually allowed
    """
    msyms_new = {}
    msyms_new['0'] = msyms['0']
    for rank in range(1, self.max_rank + 1):
      keys = construct_tensor_keys(self.max_rank, rank)
      valid_keys = [key for key in keys if key in msyms.keys()]

      if len(valid_keys) > 1:
        tmp = msyms[valid_keys[0]]
        for key in valid_keys[1:]:
          d = shift_tensor_keys_dict(self.max_rank, rank)[key]
          msyms[key] = msyms[key].tensor_arg_substitution(d)
          tmp += msyms[key]

        msyms_new[valid_keys[0]] = tmp
      else:
        msyms_new[valid_keys[0]] = msyms[valid_keys[0]]
    return msyms_new

  @staticmethod
  def cse_current(currents, additionals, loop=True, simplify=None):
    """ Apply common subexpression to the current.

    :param currents: ranked tensor or tensor if loop=False
    :type  currents: dict/tensor

    :param additionals: list of subexpressions to-be updated
    :type  additionals: list

    :param loop: Toggle for tree current.
    :type  loop: bool

    """
    # for tree currents the the current is mapped to rank 0
    if not loop:
      currents_mod = {'0': currents}
    else:
      currents_mod = currents

    # order the ranks which is crucial for later reconstruction
    rank_keys = list(currents_mod.keys())

    # in order to apply the cse all tensor coefficient must be in the form of a
    # single list
    currents_flattened = [currents_mod[rank].expanded_tensor
                          for rank in rank_keys]
    currents_flattened = hl.flatten(currents_flattened)

    isubs = {I: Symbol('cima')}
    (precomp, currents_flattened) = cse([v.subs(isubs)
                                        for v in currents_flattened],
                                        optimizations='basic',
                                        order='none')

    # check if some of the additionals are vector-valued
    repl_dict, n_non_vectors, n_vectors = (hl.check_vector_valued_expressions(
                                           precomp))
    if loop:
      if n_non_vectors > lb.max_non_vectors_loop:
        lb.max_non_vectors_loop = n_non_vectors
      if n_vectors > lb.max_vectors_loop:
        lb.max_vectors_loop = n_vectors
    else:
      if n_non_vectors > lb.max_non_vectors_tree:
        lb.max_non_vectors_tree = n_non_vectors
      if n_vectors > lb.max_vectors_tree:
        lb.max_vectors_tree = n_vectors

    if len(repl_dict.keys()) > 0:
      for k in range(len(precomp)):
        tmp0 = precomp[k][0].subs(repl_dict, simultaneous=True)
        tmp1 = precomp[k][1].subs(repl_dict, simultaneous=True)
        precomp[k] = (tmp0, tmp1)

      for k in range(len(currents_flattened)):
        currents_flattened[k] = currents_flattened[k].subs(repl_dict,
                                                           simultaneous=True)

    for add in precomp:
      if add not in additionals:
        additionals.append(add)

    # simplify cse
    if simplify:
      currents_tmp = [simplify(u) for u in currents_flattened]
    else:
      currents_tmp = currents_flattened

    # start reconstructing the currents
    currents_sub_tensor = {}
    for pos, rank in enumerate(rank_keys):
      currents_sub_tensor[rank] = []
      # The length_offset is the space previous currents have taken in the
      offset_start = sum(len(currents_mod[p].expanded_tensor)
                         for p in rank_keys[:pos])
      length_offset = len(currents_mod[rank].expanded_tensor)
      currents_sub_tensor[rank] = currents_tmp[offset_start:
                                               offset_start + length_offset]
    for key in currents_sub_tensor.keys():
      currents_mod[key].expanded_tensor = currents_sub_tensor[key]

    if loop:
      return currents_mod, additionals
    else:
      return currents_mod['0'], additionals

if __name__ == "__main__":
  #import doctest
  #doctest.testmod()
  ls = [['P(1,2)', '-1*P(1,3)']]
  #ls = [['1*P(3,1)', '1*P(3,3)/2']]
  #ls = [['1*P(-1,2)*Epsilon(-1,1,2,3)'], ['1*P(-1,1)*Epsilon(-1,1,2,3)']]
  #ls = [['1*P(-1,2)*Epsilon(-1,-2,-3,3)*Epsilon(5,-2,-3,4)']]
  #ls = [['Epsilon(-1,-2,-3,-4)*Epsilon(-1,-2,-3,-4)']]
  #ls = [['P(1,3)*Metric(2,3)',
         #'-1*P(1,2)*Metric(2,3)',
         #'P(1,1)*Metric(2,3)',
         #'P(3,2)*Metric(1,2)',
         #'-1*P(3,3)*Metric(1,2)',
         #'-1*P(3,1)*Metric(1,2)',
         #'-1*P(2,3)*Metric(1,3)',
         #'P(2,1)*Metric(1,3)'
         #]]
  #ls = [['1*GammaM(1,3,2)'], ['1*GammaP(1,3,2)']]
  #ls = [['Sigma(-1,-2,1,2)*Sigma(-2,-1,3,4)/4']]

  ls = [['Sigma(-1,-2,1,2)*Sigma(-2,-1,3,4)/4'],
        ['-1*Sigma(-1,-2,1,4)*Sigma(-2,-1,3,2)/4'],
        ['GammaM(-1,1,2)*GammaP(-1,3,4)'],
        ['-GammaM(-1,1,4)*GammaP(-1,3,2)'],
        ['GammaP(-1,1,2)*GammaM(-1,3,4)'],
        ['-GammaP(-1,1,4)*GammaM(-1,3,2)']]
  #ls = [['1*ProjM(2,1)'], ['1*ProjP(2,1)']]

  #ls = [['Metric(1,2)']]
  #ls = [['P(2,2)']]
  #ls = [['Gamma(3,2,1)']]
  ls = [['P(3,1)', '-1*P(3,2)']]
  ls = [['-P(1,2)*P(2,3)*P(3,1)'],
        ['-Metric(2,3)*P(-1,1)*P(-1,2)*P(1,3)']]

  ls = [['-Metric(1,2)*P(3,1)', 'Metric(1,2)*P(3,2)', 'Metric(1,3)*P(2,1)',
         '-Metric(2,3)*P(1,2)'],
        ['-Metric(1,3)*P(2,3)', 'Metric(2,3)*P(1,3)'],
        ['-P(1,2)*P(2,3)*P(3,1)', 'P(1,3)*P(2,1)*P(3,2)'],
        ['-2*Epsilon(-1,-2,1,2)*P(-1,1)*P(-2,2)*P(3,1)',
         '2*Epsilon(-1,-2,1,2)*P(-1,1)*P(-2,2)*P(3,2)',
         '- 2*Epsilon(-1,-2,1,3)*P(-1,1)*P(-2,2)*P(2,1)',
         '+ 2*Epsilon(-1,-2,1,3)*P(-1,1)*P(-2,2)*P(2,3)',
         '- 2*Epsilon(-1,-2,2,3)*P(-1,1)*P(-2,2)*P(1,2)',
         '+ 2*Epsilon(-1,-2,2,3)*P(-1,1)*P(-2,2)*P(1,3)',
         '- 2*Epsilon(-1,1,2,3)*P(-1,1)*P(-2,2)*P(-2,2)',
         '- 2*Epsilon(-2,1,2,3)*P(-1,1)*P(-1,1)*P(-2,2)',
         '-4*Epsilon(-2,1,2,3)*P(-1,1)*P(-1,2)*P(-2,1)',
         '- 4*Epsilon(-2,1,2,3)*P(-1,1)*P(-1,2)*P(-2,2)'],
        ['Metric(1,2)*P(-1,1)*P(-1,1)*P(3,2)',
         '- Metric(1,2)*P(-1,1)*P(-1,2)*P(3,1)',
         '+ Metric(1,2)*P(-1,1)*P(-1,2)*P(3,2)',
         '- Metric(1,2)*P(-1,2)*P(-1,2)*P(3,1)',
         'Metric(1,3)*P(-1,1)*P(-1,2)*P(2,1)',
         'Metric(1,3)*P(-1,1)*P(-1,2)*P(2,3)',
         'Metric(1,3)*P(-1,2)*P(-1,2)*P(2,1)',
         '- Metric(2,3)*P(-1,1)*P(-1,1)*P(1,2)',
         '-Metric(2,3)*P(-1,1)*P(-1,2)*P(1,2)',
         '-Metric(2,3)*P(-1,1)*P(-1,2)*P(1,3)'],
        ['Epsilon(-1,1,2,3)*P(-1,1)',
         'Epsilon(-1,1,2,3)*P(-1,2)']]
  #ls = [['Epsilon(-1,1,2,3)*P(-1,1)',
         #'Epsilon(-1,1,2,3)*P(-1,2)']]

  ls = [['- P(1,2)*Metric(2,3)', 'P(3,2)*Metric(1,2)'], ['P(1,3)*Metric(2,3)', '- P(2,3)*Metric(1,3)'], ['P(2,1)*Metric(1,3)', '- P(3,1)*Metric(1,2)'], ['P(1,3)*P(2,1)*P(3,2)'], ['P(1,2)*P(2,3)*P(3,1)'], ['P(-1,1)*P(-1,2)*P(-2,2)*Epsilon(-2,1,2,3)'], ['2*P(-1,1)*P(-2,2)*P(-2,2)*Epsilon(-1,1,2,3)', '2*P(-1,1)*P(-1,1)*P(-2,2)*Epsilon(-2,1,2,3)'], ['P(-1,1)*P(-2,2)*P(1,3)*Epsilon(-1,-2,2,3)'], ['P(-1,1)*P(-2,2)*P(1,2)*Epsilon(-1,-2,2,3)'], ['P(-1,1)*P(-2,2)*P(2,3)*Epsilon(-1,-2,1,3)'], ['P(-1,1)*P(-2,2)*P(3,2)*Epsilon(-1,-2,1,2)'], ['4*P(-1,1)*P(-2,1)*P(-1,2)*Epsilon(-2,1,2,3)', '2*P(-1,1)*P(-1,1)*P(-2,1)*Epsilon(-2,1,2,3)', '-2*P(-1,1)*P(-2,1)*P(-2,1)*Epsilon(-1,1,2,3)'], ['P(-1,1)*P(-2,2)*P(2,1)*Epsilon(-1,-2,1,3)'], ['P(-1,1)*P(-2,2)*P(3,1)*Epsilon(-1,-2,1,2)'], ['-4*P(-1,1)*P(-2,1)*P(-2,2)*Epsilon(-1,1,2,3)', '-2*P(-1,1)*P(-2,2)*P(-1,2)*Epsilon(-2,1,2,3)', '2*P(-1,1)*P(-2,2)*P(3,1)*Epsilon(-2,-1,1,2)'], ['P(-1,1)*P(-2,2)*P(2,1)*Epsilon(-2,-1,1,3)'], ['- P(-1,1)*P(-1,2)*P(2,1)*Metric(1,3)', '- P(-1,2)*P(-1,2)*P(2,1)*Metric(1,3)'], ['P(-1,1)*P(-1,1)*P(1,2)*Metric(2,3)', 'P(-1,1)*P(-1,2)*P(1,2)*Metric(2,3)'], ['P(-1,1)*P(-1,2)*P(3,1)*Metric(1,2)', 'P(-1,2)*P(-1,2)*P(3,1)*Metric(1,2)'], ['P(-1,1)*P(-1,1)*P(3,2)*Metric(1,2)', 'P(-1,1)*P(-1,2)*P(3,2)*Metric(1,2)'], ['P(-1,1)*P(-1,2)*P(2,3)*Metric(1,3)'], ['P(-1,1)*P(-1,2)*P(1,3)*Metric(2,3)'], ['- P(-1,2)*Epsilon(-1,1,2,3)'], ['- P(-1,1)*Epsilon(-1,1,2,3)']]

  # fill lorentz_func dicts
  #import rept1l.lorentz_structures.lorentz_structures as LL
  # OLC = Current(ls, 3, dim=4, loop_current=True, pext=None)
  #
  # current = OLC.current
  # #print("current:", current)
  # additionals_loop = OLC.additionals
  # #print("additional:", additionals_loop)
  # #from rept1l.tensor import simplify
  # #current0 = current['0']
  # #print("current0.expanded_tensor:", current0.args)
  # #print("current0.expanded_tensor:", current0.expanded_tensor)
  # #current1 = current['mu']
  # #print("current1.expanded_tensor:", current1.args)
  # #print("current1.expanded_tensor:", current1.expanded_tensor)
  # #current1 = current['munuro']
  # #print("current1.expanded_tensor:", current1.args)
  # #print("current1.expanded_tensor:", current1.expanded_tensor)
  # #current1 = current['nu']
  # #print("current1.expanded_tensor:", current1.expanded_tensor)
  # #current1 = current['munu']
  # #print("current1.args:", current1.args)
  # #print("current1.expanded_tensor:", len(current1.expanded_tensor))
  # #r = current0
  # #r = simplify(current)
  # #print("r.expanded_tensor:", r.expanded_tensor)
  # #print("r.args:", r.args)
  # #print("results_loop:", results_loop[0]['mu'].expanded_tensor)
  # #res, add = Current.cse_current(results_loop, [], loop=True)
  # #print("res.expanded_tensor:", res.expanded_tensor)
  # #print("res.expanded_tensor:", res['munu'].expanded_tensor)
  # #print("res.expanded_tensor:", res['mu'].expanded_tensor)
  # #print("res.expanded_tensor:", res['0'].expanded_tensor)
  # #print("add:", add)
  # from rept1l.currents.loopcurrentpyfort import LoopCurrentPyFort
  # t = LoopCurrentPyFort(current, additionals_loop, 4, symmetrize=False,
  #                       apply_cse=True, simplify=False)
  # t.gen_output()
  #current = curr.current
  ##print "current:", current[0]['mu'].expanded_tensor
  #print "current:", current[0]['0'].expanded_tensor
  ##print "current:", current[0].expanded_tensor
  #print "curr.vectorized:", curr.vectorized

  lco = [['- P(-1,1)*P(-1,3)*P(1,2)', 'P(-1,1)*P(-1,2)*P(1,3)'], ['P(-1,1)*P(-2,2)*P(-3,3)*Epsilon(-1,-2,-3,1)'], ['P(1,2)', '- P(1,3)']]
  final = 3
  Current.build_feynman_rule(lco, final)
