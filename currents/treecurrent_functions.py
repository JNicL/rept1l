#==============================================================================#
#                           treecurrent_functions.py                           #
#==============================================================================#

#============#
#  Includes  #
#============#

from sympy import Symbol
from rept1l.tensor import tensor_arg, tensor, diracAlgebra
d = diracAlgebra

#===========#
#  Methods  #
#===========#

def index_gen(i=-20, not_allowed=[]):
  while True:
    i -= 1
    while i in not_allowed:
      i -= 1
    yield i

#------------------------------------------------------------------------------#

def feynman_propagator(free_ci, pext=None, domain=range(4), resolve=False,
                       **kwargs):
  if pext is None:
    return -1, 'i', free_ci

  ci = next(free_ci)
  varg1 = tensor_arg(ci, domain=domain, resolve=resolve, metric=d.g)
  varg2 = tensor_arg('i', domain=domain, resolve=resolve, metric=d.g)

  p1 = tensor([varg1], symbol=Symbol('p'))
  p2 = tensor([varg2], symbol=Symbol('p'))
  m = Symbol("m")
  if pext == 'RXI1':
    return p1*p2*(1/m**2), ci, free_ci
  elif pext == 'RXI2':
    return -1*p1*p2*(1/m**2), ci, free_ci
  else:
    raise Exception('Propagator extension ' + str(pext) + ' not supported.')

def fermion_propagator(free_ci, fermion_out=True, domain=range(4),
                       resolve=True):
  ci = next(free_ci)
  if fermion_out:
    pslash_arg1 = tensor_arg(ci, domain=domain, resolve=resolve)
    pslash_arg2 = tensor_arg('i', domain=domain, resolve=resolve)
  else:
    pslash_arg1 = tensor_arg('i', domain=domain, resolve=resolve)
    pslash_arg2 = tensor_arg(ci, domain=domain, resolve=resolve)
  #free_ci.remove(ci)

  pslash_tensor = tensor([pslash_arg1, pslash_arg2],
                         numerical=d.pslash, symbol=Symbol('PS'))
  m = Symbol("m")
  mass_tensor = m*tensor([pslash_arg1, pslash_arg2], numerical=d.one)

  # p_sign = -1: the momentum direction is opposed to the fermion line direction
  p_sign = -1 if fermion_out else 1
  return (p_sign * pslash_tensor + mass_tensor), ci, free_ci

#------------------------------------------------------------------------------#

def fermion_additionals(additional, final_particle, last_contraction):
  """ Appends necessary variables for a fermionic current. """
  if not last_contraction:
    pl = Symbol("pl(:)")
    pl_sum = 0
    for k in range(final_particle-1):
      pl_sum += Symbol("PL" + str(k+1) + "(:)")

    if not (pl, pl_sum) in additional:
      additional.append((pl, pl_sum))

  return additional

#------------------------------------------------------------------------------#

def vector_additionals(additional, final_particle, last_contraction):
  """ Appends necessary variables for a vector current. """
  if not last_contraction:
    p = Symbol("p(:)")
    p_sum = 0
    for k in range(final_particle-1):
      p_sum += Symbol("P" + str(k+1) + "(:)")

    if not (p, p_sum) in additional:
      additional.append((p, p_sum))

  return additional

#------------------------------------------------------------------------------#

def is_final_arg(arg, final_particle):
  return arg == final_particle or arg == 'i'

#------------------------------------------------------------------------------#

def construct_arg(arg, free_ci, final_particle, domain=range(4),
                  resolve=True, metric=None):
  """ Constructs the wavefunction(tensor) if arg > 0, else sets the wavefunction
  to None and takes a free contraction index and contructs a tensor argument.

  :return: wavefunction, tensor argument, left free contraction indices
  """
  try:
    posint = int(arg) >= 0
  except ValueError:
    posint = False
  if posint and not is_final_arg(arg, final_particle):
    ci = next(free_ci)
    wave_symbol = Symbol('w' + str(arg))
    wave_arg = tensor_arg(ci, domain=domain, resolve=resolve, metric=metric)
    wave_args = [wave_arg]
    wave_tensor = tensor(wave_args, symbol=wave_symbol)
    t_arg = tensor_arg(ci, domain=domain, resolve=resolve, metric=metric)
  else:
    wave_tensor = 1
    t_arg = tensor_arg(arg, domain=domain, resolve=resolve, metric=metric)
  return wave_tensor, t_arg, free_ci

