#==============================================================================#
#                                 lorentz_lib                                  #
#==============================================================================#

#============#
#  Includes  #
#============#

import sys
import os
try:
  from types import ListType
  list_type = ListType
except ImportError:
  list_type = list

import rept1l.helper_lib as hl
from rept1l.helper_lib import is_plus_minus, isdigit

#===========#
#  Globals  #
#===========#

lorentz_func_dict_tree = dict()
lorentz_func_dict_loop = dict()

lorentz_rank = dict()

max_vectors_tree  = 0
max_vectors_loop  = 0
max_non_vectors_tree = 0
max_non_vectors_loop = 0



#==============================================================================#
#                                get_structure                                 #
#==============================================================================#

#------------------------------------------------------------------------------#
def get_structure(expr):
  """
  expr: parsed lorentz structure in primitve structures and arguments
  separated by connections (+-*/)

  returns: a structure array containing factors and lorentzfunctions such as
  g_{\mu \nu} or gamma matrices. The correspoding arguments are stored in
  'arguments'. The 'connections' list stores the way structures are connected to
  each other, e.g. +-*/
  """
  structures  = []
  arguments  = []
  connections = []
  for i in expr:
    if (not is_plus_minus(i) and i != '*' and i != '**' and i != '/'):
      if(not isinstance(i, list_type)):
        structures.append(i)
        if ( isdigit(i)):
          arguments.append(None)

      else:
        # if structure is a list -> array of arguments. Must be parsed again.
        # arguments must be parsed again
        if ( len(i) == 1):
          tmp1 = hl.argument_pp.parseString('('+ i[0] + ')')
          tmp2 = filter( lambda a: a!=',',tmp1.asList()[0])
          tmp3 = [int(elem) for elem in tmp2]
          arguments.append(tmp3)
        else:
          # If it is not an argument, the expression is not fully expanded and
          # has nested expressions. In that case we compute the sympy expression
          # of the string and we expand the result algebraically. Then the
          # structure is computed again.
          new_expr = (str(get_sympy_expression(expr).simplify().expand())
                        .replace(";", ","))
          res = hl.nested_pp.parseString('(' + new_expr + ')')
          q = res.asList()[0]
          return get_structure(q)
          #sys.exit("deeper nested structure")

    else:
      connections.append(i)

  return structures, arguments, connections

#------------------------------------------------------------------------------#

#==============================================================================#
#                            reconstruct_struct_arg                            #
#==============================================================================#

#------------------------------------------------------------------------------#
def reconstruct_struct_arg(structure, arguments):
  if (arguments == None):
    return structure
  else:
    res = structure
    arg_tmp = '('
    for k in arguments:
      arg_tmp += str(k) + ';'
    arg_tmp = arg_tmp[:-1] + ')'
    return symbols(res + arg_tmp)
#------------------------------------------------------------------------------#

#==============================================================================#
#                             get_sympy_expression                             #
#==============================================================================#

#------------------------------------------------------------------------------#
def get_sympy_expression(expr):
  """
  expr: parsed lorentz structure in primitve structures and arguments
  separated by connections (+-*/)

  returns: a structure array containing factors and lorentzfunctions such as
  g_{\mu \nu} or gamma matrices. The correspoding arguments are stored in
  'arguments'. The 'connections' list stores the way structures are connected to
  each other, e.g. +-*/
  """
  structures  = []
  arguments   = []
  connections = []
  for i in range(len(expr)):
    if (not is_plus_minus(expr[i]) and expr[i] != '*' and expr[i] != '/'):
      if(not isinstance(expr[i], list_type)):
        structures.append(expr[i])
        if ( expr[i].replace("-", "", 1).isdigit()):
          arguments.append(None)

      else:
        # if structure is a list -> array of arguments. Must be parsed again.
        # arguments must be parsed again
        if ( len(expr[i]) == 1):
          tmp1 = hl.argument_pp.parseString('('+ expr[i][0] + ')')
          tmp2 = filter( lambda a: a!=',',tmp1.asList()[0])
          tmp3 = [int(elem) for elem in tmp2]
          arguments.append(tmp3)
        else:
          # If it is not an argument, it is a nested subexpression which was in
          # parenthesis.
          structures.append(get_sympy_expression(expr[i]))
          arguments.append(None)

    else:
      # A leading sign is interpreted as (struc, arg, con) = (-1, None, *) in
      # order to keep the constraint len(struc) = len(con) + 1
      if (i == 0):
        structures.append(-1)
        arguments.append(None)
        connections.append('*')
      else:
        connections.append(expr[i])

  if (arguments[0] == 0):
    res = structures[0]
  else:
    res = reconstruct_struct_arg(structures[0], arguments[0])
  for j in range(len(connections)):
    if (connections[j] == '*'):
      res *= reconstruct_struct_arg(structures[j+1], arguments[j+1])
    elif (connections[j] == '+'):
      res += reconstruct_struct_arg(structures[j+1], arguments[j+1])
    elif (connections[j] == '-'):
      res -= reconstruct_struct_arg(structures[j+1], arguments[j+1])

  return res
#------------------------------------------------------------------------------#

#==============================================================================#
#                              separate_sum_struc                              #
#==============================================================================#

#------------------------------------------------------------------------------#
def separate_sum_struc(struc, arg, con):
  """
  separate_sum_struc takes the result from get_structure and separates the
  multiplicatively connected structures from the additively connected
  substructures.
  The resulting arrays have the following structures

  get_structure: a*b*c+d*e ->  [a,b,c,d,e] ,[*,*,+,*]

  spearate_sum_struc: [a,b,c,d,e], [*,*,+,*] -> [[a,b],[c.d]], [[*,*],[*]], [+]
  """
  strucs = []
  args = []
  cons = []
  adds = []
  tmp_struc = []
  tmp_arg   = []
  tmp_con   = []
  difference_con_arg = 0
  for i in range(len(con)):
    tmp_struc.append(struc[i])

    if (isdigit(struc[i])):
      if ( i > 0):
        # if we divide by an integer there is no fake argument slot in contrast
        # to the multiplication with an integer 4 is used as fake argument =>
        # should be set to None
        if ( con[i-1] == '/'):
          difference_con_arg += 1
        else:
          tmp_arg.append(arg[i - difference_con_arg])

      else:
        tmp_arg.append(arg[i - difference_con_arg])
    else:
      tmp_arg.append(arg[i - difference_con_arg])

    if( con[i] != '+' and con[i] != '-'):
      tmp_con.append(con[i])

    else:
      strucs.append(tmp_struc)
      args.append(tmp_arg)
      cons.append(tmp_con)
      adds.append(con[i])
      tmp_struc = []
      tmp_arg = []
      tmp_con = []

  tmp_struc.append(struc[len(struc)-1])
  tmp_arg.append(arg[len(arg)-1])
  strucs.append(tmp_struc)
  args.append(tmp_arg)
  cons.append(tmp_con)

  return strucs, args, cons, adds
#------------------------------------------------------------------------------#
