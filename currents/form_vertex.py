#==============================================================================#
#                                 form_vertex                                  #
#==============================================================================#

#============#
#  Includes  #
#============#

import os
import rept1l.combinatorics as cb
from rept1l.helper_lib import flatten
from rept1l.pyfort import ifBlock, loadBlock, caseBlock

#===========#
#  Globals  #
#===========#

ffpath = os.path.join(os.path.join(os.path.dirname(os.path.realpath(__file__)),
                                   os.pardir), 'formutils/FortranTemplates')

#===========#
#  Methods  #
#===========#

def ext_arg(arg, final, base1, base2, end):
  """ Returns external arguments of wavefunctions. See get_arg. """
  ret = ("trim(" + base1 + "(" + str(arg) + "))" if arg != final else
         "trim(" + base2 + end + ")")
  return ret

def int_arg(*args):
  """ Returns internal contraction argument specified by recola """
  return "trim(cj)"

def get_arg(arg, final, propagator=False, symbol="cw"):
  """ Transforms the argument into an internal argument used to write FORM
  expressions.

  :param arg: argument to be transformed
  :type  arg: int

  :param final: argument of the final particle
  :type  final: int
  """
  if propagator:
    return(ext_arg(arg, final, base1=symbol, base2="cj", end="")
           if arg > 0 else int_arg())
  else:
    return(ext_arg(arg, final, base1=symbol, base2=symbol, end="(nDef)")
           if arg > 0 else int_arg())


def Epsilon_form(arguments, fi, ti, final):
  """ Constructs the form expression for the lorentz structure g^\mu^\nu

  :param arguments: Momentum arguments. First argument is the lorentz index,
                    second is the particle index.
  :type  arguments: list

  :param free_indices: A list of symbols allowed to use in the expression.
  :type  free_indices: list

  :param final: The index of the final particle.
  :type  final: int

  >>> args = [1, 2, 3, 4]
  >>> fi = {'fermionic': ['i', 'j', 'k', 'l', 'i'], 'bosonic': ['nu']}; f = 4
  >>> Epsilon_form(args, fi, {}, f)
  " 'e_(mu' // trim(cw(1)) // ',mu' // trim(cw(2)) // ',mu' // trim(cw(3)) //\
 ',nu' // trim(cj)// ')'"
  """
  vargs = []
  for arg in arguments:
    varg, fi, ti = assign_index(arg, fi, ti, final, 'bosonic')
    vargs.append(varg)

  arg = arguments[:]
  arg = [get_arg(u, final, propagator=True) for u in arg]
  #f = lambda x: 'mu' if x > 0 else cb.free_indices[x-1]
  contr_syms = " // ',".join(w + "' // " + v for v, w in zip(arg, vargs))
  return " 'e_(" + contr_syms + "// ')'"

def p_form(arguments, fi, ti, final):
  """ Constructs the form expression for the momentum structure P^\mu_i

  :param arguments: Momentum arguments. First argument is the lorentz index,
                    second is the particle index.
  :type  arguments: list

  :param free_indices: A list of symbols allowed to use in the expression.
  :type  free_indices: list

  :param final: The index of the final particle.
  :type  final: int

  >>> args = [1, 2]; f = 3
  >>> fi = {'fermionic': ['i', 'j', 'k', 'l', 'i'], 'bosonic': ['nu']};
  >>> p_form(args, fi, {}, f)
  "trim(p(2)) // '(mu' //trim(cw(1)) // ')'"

  >>> args = [-2, 3]
  >>> fi = {'fermionic': ['i', 'j', 'k', 'l', 'i'], 'bosonic': ['nu']};
  >>> p_form(args, fi, {}, f)
  "trim(p(nDef)) // '(nu' //trim(cj) // ')'"

  >>> args = [3, 3]
  >>> fi = {'fermionic': ['i', 'j', 'k', 'l', 'i'], 'bosonic': ['nu']};
  >>> p_form(args, fi, {}, f)
  "trim(p(nDef)) // '(nu' //trim(cj) // ')'"
  """
  arg = arguments[:]
  sym = 'p'
  arg[0] = get_arg(arg[0], final, propagator=True)
  arg[1] = get_arg(arg[1], final, symbol=sym)

  varg, fi, ti = assign_index(arguments[0], fi, ti, final, 'bosonic')
  return (str(arg[1]) + " // '(" + varg + "' //" + arg[0] + " // ')'")

def g_form(arguments, fi, ti, final):
  """ Constructs the form expression for the lorentz structure g^\mu^\nu

  :param arguments: Momentum arguments. First argument is the lorentz index,
                    second is the particle index.
  :type  arguments: list

  :param free_indices: A list of symbols allowed to use in the expression.
  :type  free_indices: list

  :param final: The index of the final particle.
  :type  final: int

  >>> fi = {'fermionic': ['i', 'j', 'k', 'l', 'i'], 'bosonic': ['mu']};
  >>> args = [1, 2]; f = 3
  >>> g_form(args, fi, {}, f)
  " 'd_(mu' // trim(cw(1)) // ',mu' // trim(cw(2)) // ')'"
  """
  varg1, fi, ti = assign_index(arguments[0], fi, ti, final, 'bosonic')
  varg2, fi, ti = assign_index(arguments[1], fi, ti, final, 'bosonic')

  arg = arguments[:]

  arg = list(map(lambda x: get_arg(x, final, propagator=True), arg))
  return (" 'd_(" + varg1 + "' // " + arg[0] +
          " // '," + varg2 + "' // " + arg[1] + " // ')'")

def PPlMi_form(arguments, fi, ti, final, plus_minus='+'):
  # op/om (i1, i2)
  i1, fi, ti = assign_index(arguments[0], fi, ti, final, 'fermionic')
  i2, fi, ti = assign_index(arguments[1], fi, ti, final, 'fermionic')
  arg = list(map(lambda x: get_arg(x, final, propagator=True), arguments))

  if plus_minus == '+':
    return (" 'op(" + i1 + "' // " + arg[0] +
            " // '," + i2 + "' // " + arg[1] + " // ')'")
  elif plus_minus == '-':
    return (" 'om(" + i1 + "' // " + arg[0] +
            " // '," + i2 + "' // " + arg[1] + " // ')'")
  else:
    return (" 'ga(" + i1 + "' // " + arg[0] +
            " // '," + i2 + "' // " + arg[1] + " // ')'")

def assign_index(i, f_indices, t_indices, final, type):
  assert(type == 'fermionic' or type == 'bosonic')
  #if (i > 0 and i != final) or (type == 'bosonic' and i == final):
  if (i > 0 and i != final):
    if type == 'fermionic':
      return 'i', f_indices, t_indices
    else:
      return 'mu', f_indices, t_indices
  else:
    if i in t_indices:
      return t_indices[i], f_indices, t_indices
    else:
      fi = f_indices[type].pop(0)
      t_indices[i] = fi
      return fi, f_indices, t_indices

def Ga_form(arguments, fi, ti, final):
  """
  # ga(i1, i2, mu)
  >>> fi = {'fermionic': ['i', 'j', 'k', 'l', 'i'], 'bosonic': ['mu']};
  >>> args = [1, 2, 3]; f = 3
  >>> Ga_form(args, fi, {}, f)
  " 'ga(i' // trim(cw(2)) // ',i' // trim(cj) // ',' //\
 'mu'//trim(cw(1)) // ')'"
  >>> args = [-1, -1, 2]; f = 3
  >>> Ga_form(args, fi, {}, f)
  " 'ga(j' // trim(cj) // ',i' // trim(cw(2)) // ',' // 'j'//trim(cj) // ')'"
  """
  i1, fi, ti = assign_index(arguments[1], fi, ti, final, 'fermionic')
  i2, fi, ti = assign_index(arguments[2], fi, ti, final, 'fermionic')
  varg, fi, ti = assign_index(arguments[0], fi, ti, final, 'bosonic')

  arg = arguments[:]
  arg[0] = get_arg(arg[0], final, propagator=True)
  arg[1], arg[2] = list(map(lambda x: get_arg(x, final, propagator=True),
                            [arg[1], arg[2]]))

  aferm_arg = " 'ga(" + i1 + "' // " + arg[1] + " // ',"
  ferm_arg = i2 + "' // " + arg[2] + " // ','"
  vbosn_arg = " // '" + varg + "'//" + arg[0] + " // ')'"

  ret = aferm_arg + ferm_arg + vbosn_arg
  return ret

def Si_form(arguments, fi, ti, final):
  """ Sigma(mu1, mu2, i1, i2) """
  v1arg, fi, ti = assign_index(arguments[0], fi, ti, final, 'bosonic')
  v2arg, fi, ti = assign_index(arguments[1], fi, ti, final, 'bosonic')
  i1, fi, ti = assign_index(arguments[2], fi, ti, final, 'fermionic')
  i2, fi, ti = assign_index(arguments[3], fi, ti, final, 'fermionic')

  arg = arguments[:]
  arg[0] = get_arg(arg[0], final, propagator=True)
  arg[1] = get_arg(arg[1], final, propagator=True)
  arg[2], arg[3] = list(map(lambda x: get_arg(x, final, propagator=True),
                            [arg[2], arg[3]]))

# Fermion args are always the same -> ga(i1,i2, ...)
  aferm1_arg = " 'ga(" + i1 + "' // " + arg[2] + " // ',"
  ferm1_arg = i2 + "' // " + arg[3] + " // ','"

  # First combination -> ga(... mu1, mu2)
  vbosn11_arg = " // '" + v1arg + "'//" + arg[0] + " // ','"
  vbosn12_arg = " // '" + v2arg + "'//" + arg[1] + " // ')'"

  # Second combination -> ga(... mu1, mu2)
  vbosn21_arg = " // '" + v2arg + "'//" + arg[0] + " // ','"
  vbosn22_arg = " // '" + v1arg + "'//" + arg[1] + " // ')'"

  g1 = aferm1_arg + ferm1_arg + vbosn11_arg + vbosn12_arg
  g2 = aferm1_arg + ferm1_arg + vbosn21_arg + vbosn22_arg

  ret = "'i_/2*(' // " + g1 + " // '-' // " + g2 + " // ')'"
  return ret

def GaPlMi_form(arguments, fi, ti, final, plus_minus="+"):
  """ Constructs the form expression for the dirac structure
  Gamma^\mu_{ij}*(1\pm Gamma_5)

  :param arguments: Momentum arguments. First argument is the lorentz index,
                    second is the particle index.
  :type  arguments: list

  :param free_indices: A list of symbols allowed to use in the expression.
  :type  free_indices: list

  :param final: The index of the final particle.
  :type  final: int

  :param plus_mins: + or - projector.
  :type  plus_mins: str

  >>> args = [1, 2, 3];  f = 3
  >>> fi = {'fermionic': ['j', 'k', 'l', 'i'], 'bosonic': ['mu']};
  >>> GaPlMi_form(args, fi, {}, f)
  " 'ga(i' // trim(cw(2)) // ',k' // trim(cj) // ',' // 'mu'//trim(cw(1)) // ')\
 * '// 'op(k' // trim(cj) // ',j' // trim(cj) //')'"
  >>> args = [-2, 1, 2]
  >>> GaPlMi_form(args, fi, {}, f)
  " 'ga(i' // trim(cw(1)) // ',l' // trim(cj) // ',' // 'mu'//trim(cj) // ') *\
 '// 'op(l' // trim(cj) // ',i' // trim(cw(2)) //')'"

  """
  # ga(i1, i2, mu) * op/om (i2, i3)
  #i1 = free_indices[0] if arguments[2] != final else free_indices[-1]
  #i3 = free_indices[-1] if arguments[2] != final else free_indices[0]
  #free_indices.pop(0)
  #free_indices.pop(1)

  i1, fi, ti = assign_index(arguments[1], fi, ti, final, 'fermionic')
  i3, fi, ti = assign_index(arguments[2], fi, ti, final, 'fermionic')
  varg, fi, ti = assign_index(arguments[0], fi, ti, final, 'bosonic')

  l = fi['fermionic']
  i2 = l.pop(0)
  fi['fermionic'] = l
  #TODO: nick this is not general enough Do 21 Aug 2014 17:36:04 CEST
  # the final particle gets a contracted arg cj whereas the incomming particles
  # have external args.
  #arg = [ str(u) if u != len(arg) else "nDef" for u in arg ]
  # TODO: nick done - validation needed Mo 25 Aug 2014 23:29:16 CEST
  # for the vectorboson the we have the feynman propagator and internally we do
  # not multiply the result with g^{\mu \nu} but instead set the outgoint index
  # straight knowing that it is the same result
  # In contrast to fermions. There we explicitly set the outgoing fermion index
  # to cj meaning that it will be further contracted with the fermion propagator

  # In recola cj is equal to the branch. In general theories we need more
  # than one contraction index -> need a more robost method
  # The contraction indices are constructed as follows:
  # - take free index, e.g. i
  # - label index according to branch, i.e. branch = 5 -> i5

  # right now the system does not support further contraction within the same
  # current, but will be realised in the future with negative arguments

  #varg, fi, ti = assign_index(arguments[0], fi, ti, final, 'bosonic')

  arg = arguments[:]
  arg[0] = get_arg(arg[0], final, propagator=True)
  arg[1], arg[2] = list(map(lambda x: get_arg(x, final, propagator=True),
                            [arg[1], arg[2]]))
  aferm_arg = " 'ga(" + i1 + "' // " + arg[1] + " // ',"
  cntr1_arg = i2 + "' // trim(cj) // ','"

  vbosn_arg = " // '" + varg + "'//" + arg[0] + " // ') * '// "

  if plus_minus == '+':
    cntr2_arg = "'op(" + i2 + "' // trim(cj) // ',"
  else:
    cntr2_arg = "'om(" + i2 + "' // trim(cj) // ',"

  ferm_arg = i3 + "' // " + arg[2] + " //')'"

  ret = (aferm_arg + cntr1_arg + vbosn_arg + cntr2_arg + ferm_arg)
  return ret


def coupling_form(arg, *args):
  """ Builds the Fortran expression which writes the coupling multiplication in
  FORM """
  return "'+ '// C//'(" + str(arg) + ",'//trim(cgs)//')'//' * ('"

def reg_coupling(i, statements, tabs=3):
  if_coupling = ifBlock("cou(" + str(i) + ")", tabs=tabs)
  st = ("write(999,'(a)') indent(1:ind)//" +
        cb.fold(lambda x, y: x + y, statements, ""))
  if_coupling.addStatement(st)

  return if_coupling

def scalar_propagator(tabs=3):
  if_propagator = loadBlock(os.path.join(ffpath, 'scalar.f90'), tabs=tabs)
  return if_propagator

def vectorboson_propagator(fi, ti, final, pext=None, tabs=3):

  if_propagator = ifBlock("last .and. e(nDef) .lt. 2**legs", tabs=tabs)
  v_c, fi, ti = assign_index(final, fi, ti, final, 'bosonic')
  v_f = 'mu'
  arg_f = " // trim(cw(ndef)) // "
  arg_c = " // trim(cj) // "
  st = ("write(999,'(a)') indent(1:ind) //" +
        "'d_(mu" + "'" + arg_f + "'," + v_c + "'" + arg_c + "') * ('")
  if_propagator.addStatement(st)
  if_propagator.addStatement("else", n_tabs=0)

  if pext is None:
    # t'Hooft Feynman gauge: g^{mu nu}
    st = ("write(999,'(a)') indent(1:ind) // ' ( '// " +
          "'d_(mu'" + arg_f + "'," + v_c + "'" + arg_c + "') ' // " +
          "') * (-i_) *' //trim(den)// ' * ('")
  elif pext == 'RXI1':
    # additional contributions to R_\xi gauge of the form p^mu p^nu
    st = ("write(999,'(a)') indent(1:ind) // ' ( '// " +
          "trim(p(nDef)) // '(mu'" + arg_f + "') * ' // " +
          "trim(p(nDef)) // '(" + v_c + "'" + arg_c + "') ' // " +
          "') / ' // trim(m(nDef)) // '^2' // ' * (i_) *' //trim(den)// ' * ('")
  elif pext == 'RXI2':
    st = ("write(999,'(a)') indent(1:ind) // ' ( '// " +
          "trim(p(nDef)) // '(mu'" + arg_f + "') * ' // " +
          "trim(p(nDef)) // '(" + v_c + "'" + arg_c + "') ' // " +
          "') / ' // trim(m(nDef)) // '^2' //" +
          " ' * (-i_) *' //trim(denGSB)// ' * ('")

  elif pext == 'RXI':
    # standard rxi propagator
    st = ("write(999,'(a)') indent(1:ind) // ' ( '// " +
          "trim(p(nDef)) // '(mu'" + arg_f + "') * ' // " +
          "trim(p(nDef)) // '(" + v_c + "'" + arg_c + "') ' // " +
          "') * (i_) * ' //trim(rxip)// ' * '//trim(den)// " +
          "' * ' //trim(denGSB)// ' *('")
  else:
    raise Exception('Propagator extension ' + str(pext) + ' not supported.')

  if_propagator.addStatement(st)

  return if_propagator

def fermion_propagator(momentumsign, fi, ti, final, tabs=3):
  """ Builds the Fortran routine writing the FORM expression for the fermion
  propagator, both fermion and anti-fermion """
  ms = momentumsign
  if ms == '+':
    i2, fi, ti = assign_index(final, fi, ti, final, 'fermionic')
    i1 = 'i'
  else:
    i1, fi, ti = assign_index(final, fi, ti, final, 'fermionic')
    i2 = 'i'

  arg_f = (" // trim(cw(ndef)) // " if momentumsign == '+'
           else " // trim(cj) // ")

  arg_af = (" // trim(cj) // " if momentumsign == '+'
            else " // trim(cw(ndef)) // ")

  if_propagator = ifBlock("last .and. e(nDef) .lt. 2**legs", tabs=tabs)
  st = ("write(999,'(a)') indent(1:ind) //" +
        "'d_(" + i1 + "'" + arg_f + "'," + i2 + "'" + arg_af + "') * ('")
  if_propagator.addStatement(st)
  if_propagator.addStatement("else", n_tabs=0)
  st = ("write(999,'(a)') indent(1:ind) // 'i_ * ( '// " +
        "'" + ms + " ga(" + i1 + "'" + arg_f + "'," + i2 +
        "'" + arg_af + "',' //" +
        "trim(p(nDef)) // ') ' // '+ d_(" + i1 + "'" + arg_f +
        "'," + i2 + "'" + arg_af + "')*'//trim(m(nDef))//' '// " +
        "') * '//trim(den)//' * ('")
  if_propagator.addStatement(st)
  return if_propagator


form_dict = {cb.P: p_form, cb.g: g_form,
             cb.Epsilon: Epsilon_form,
             cb.ProjM: lambda *args: PPlMi_form(*args, plus_minus='-'),
             cb.ProjP: lambda *args: PPlMi_form(*args, plus_minus='+'),
             cb.Identity: lambda *args: PPlMi_form(*args, plus_minus='0'),
             cb.GammaM: lambda *args: GaPlMi_form(*args, plus_minus='-'),
             cb.GammaP: lambda *args: GaPlMi_form(*args, plus_minus='+'),
             cb.Sigma: Si_form,
             cb.Gamma: Ga_form}

def get_free_indices(taken):
  indices = {'fermionic': ['j', 'k', 'l', 'o', 'e', 'a', 'y', 'w'],
             'bosonic': ['nu', 'ro', 'al', 'ta', 'te', 'tu', 'tt']}
  ret = {}
  for type in indices:
    ret[type] = [i for i in indices[type] if i not in taken]
  return ret


def write_form_vertex(scf, lid, strucs, args, prefacs, propagator_type,
                      pext=None, tabs=2):
  """ Constructs Fortran routines to write the FORM expressions for off-shell
  currents.

  :param scf: writes the form procedure to scf
  :type  scf: PyFort

  :param lid: the structure id
  :type  lid: str

  :param strucs: structures in the current
  :type  strucs: list

  :param args: arguments applied to the structures
  :type  args: list

  :param prefacs: prefactors applied to the structures
  :type  prefacs: list

  :param pext: Propagator extension
  :type  pext: str


  Examples:
  ---------

  >>> lid = 'FaF0'; propagator_type = 2
  >>> from sympy import Symbol
  >>> strucs = [2*Symbol('GammaP')*Symbol('P'),
  ...           2*Symbol('GammaM')*Symbol('P'),
  ...           2*Symbol('ProjP'),
  ...           2*Symbol('ProjM')]

  >>> prefacs = [[1], [1], [1], [1]]
  >>> args = [[[[-1, 2], [-1, 1, 2]]],
  ...         [[[-1, 2], [-1, 1, 2]]],
  ...         [[[1, 2]]],
  ...         [[[1, 2]]]]

  >>> sc = write_form_vertex(None,lid,strucs,args,prefacs,propagator_type,0)

  >>> lid = 'SSS0'; propagator_type = 0
  >>> from sympy import Symbol
  >>> strucs = [3*Symbol('P')*Symbol('P'), 3]

  >>> prefacs = [[1, 1], [1]]
  >>> args = [[[[-1, 1], [-1, 2]], [[-1, 1], [-1, 3]]], []]

  >>> sc = write_form_vertex(None,lid,strucs,args,prefacs,propagator_type,0)

  These results can be viewed with sc.printStatment()

  """
  # In the first step the lorentz base struc is separated both, multiplicatively
  # as well as additively.
  select_case = caseBlock(lid, tabs=tabs)
  # abbr
  sc = select_case

  from rept1l.combinatorics import reconstruct_lorentz
  lco, final = reconstruct_lorentz(strucs, args, prefacs)
  for ci, fr in enumerate(lco):
    sc.addcomment('c' + str(ci) + ' x (', tabs=-1)
    for f in fr:
      f_str = str(f)
      if f_str[0] != '-':
        sc.addcomment('+ ' + f_str, tabs=-1)
      else:
        sc.addcomment(f_str, tabs=-1)
    sc.addcomment(')', tabs=-1)

  if scf is not None:
    scf.statements.append(sc)

  free_indices = {'fermionic': ['j', 'k', 'l', 'm', 'n', 'o', 'p'],
                  'bosonic': ['nu', 'ro', 'ta', 'si', 'om']}
  taken_indices = {}

  _, nparticles = cb.get_lorentz_key_components(strucs[0])
  if propagator_type == 0:
    sc + scalar_propagator(tabs=tabs)
  elif propagator_type == 1:
    sc + fermion_propagator('+', free_indices, taken_indices, nparticles,
                            tabs=tabs)
  elif propagator_type == 2:
    sc + fermion_propagator('-', free_indices, taken_indices, nparticles,
                            tabs=tabs)
  elif propagator_type == 3:
    sc + vectorboson_propagator(free_indices, taken_indices, nparticles,
                                pext=pext, tabs=tabs)
  else:
    raise Exception('Propagator type ' + propagator_type + 'unknown.')
  last_coupling = len(strucs) - 1

  # The j loop runs over structures associated to different couplings
  for j, l_key in enumerate(strucs):
    # copying free_indices does not work because of shared memory!
    struc, nparticles = cb.get_lorentz_key_components(l_key)
    struc = cb.order_strucs(struc)

    # build up the chain of expression for the structure j. The structures
    # always start with the coupling, then times the lorentz structures
    st = [coupling_form(j + 1)]

    # check if the lorentz structure has non-trivial structure (not a scalar)
    has_args = True
    if args is None:
      has_args = False
    else:
      if len([u for u in flatten(args[j])]) == 0:
        has_args = False

    if has_args:
      # The i loop runs over additively connected base structures (composed or
      # non-composed, P*g is composed e.g. VVV vertex, g is not composed e.g.
      # VVS vertex)
      for i, arg in enumerate(args[j]):
        f_indices = get_free_indices(taken_indices.values())
        t_indices = taken_indices.copy()

        l_prefac = prefacs[j][i]
        if l_prefac < 0:
          tmp = " // '-" + str(abs(l_prefac)) + "*' //"
        elif l_prefac == 1:
          tmp = "// '+' //"
        else:
          tmp = " // '+" + str(l_prefac) + "*' //"

        # the k loop runs over the multiplicatively connected structures. If the
        # structure is non_composed the iteration is trivial -> k = 0
        for k, ls_struc in enumerate(struc):
          if k > 0:
            tmp += " // "
          # TODO: nick Make Feynrules independent Mi 17 Sep 2014 17:58:04 CEST
          # i.e. make general option for momentum convention
          # the following code assumes the convention momenta are outgoing
          if ls_struc == cb.P:
            if arg[k][1] != nparticles:
              tmp += " '(-1)*' // "
          tmp += (form_dict[ls_struc](arg[k], f_indices, t_indices,
                                      nparticles) + " // '*'")
        st.append(tmp)
        # TODO: JNL Not very elegant
        # cut away the multiplication ` // '*' for the last structure
        st[-1] = st[-1][:-6]
    else:
      # no arguments -> purely scalar structure
      assert(len(prefacs[j]) == 1)
      assert(prefacs[j][0] == 1)
      st[-1] += "// '" + str(prefacs[j][0]) + "' "

    st[-1] = st[-1] + "// ')'"
    sc + (reg_coupling(j + 1, st, tabs=tabs))
    if j == last_coupling:
      st = "write(999,'(a)') indent(1:ind)// ') ;'"
      sc.addStatement(st, n_tabs=0)

  if scf is not None:
    (scf > 0) + "write(999,'(a)')"
    (scf > 0) + "write(999,'(a)') '.sort'"
    (scf > 0) + "write(999,'(a)')"
    #  scf.addStatement("write(999,'(a)')", n_tabs=0)
    #  scf.addStatement("write(999,'(a)') '.sort'", n_tabs=0)
    #  scf.addStatement("write(999,'(a)')", n_tabs=0)
  else:
    return sc

if __name__ == "__main__":
  #import doctest
  #doctest.testmod()

  #print 'Example run'
  from sympy import Symbol
  #l_id = 'SS1'
  #l_strucs = [2, 2*Symbol('P')**2]

  ##form_vertex(l_id, l_strucs, l_args, l_prefacs, propagator_type)

  #l_id = 'FaF0'
  #propagator_type = 2
  #l_strucs = [2*Symbol('GammaP')*Symbol('P'),
              #2*Symbol('GammaM')*Symbol('P'),
              #2*Symbol('ProjP'), 2*Symbol('ProjM')]

  #l_prefacs = [[1], [1], [1], [1]]
  #l_args = [[[[-1, 2], [-1, 1, 2]]], [[[-1, 2], [-1, 1, 2]]], [[[1, 2]]],
            #[[[1, 2]]]]

  ##form_vertex(l_id, l_strucs, l_args, l_prefacs, propagator_type)

  #l_id = 'VVV'
  #l_prefacs = [[1]]
  #P = Symbol('P')
  #g = Symbol('g')
  #Epsilon = Symbol('Epsilon')
  #l_strucs = [3*Epsilon*P]
  #propagator_type = 3
  #l_args = [[[[-1, 2], [-1, 1, 2, 3]]]]

  ##form_vertex(l_id, l_strucs, l_args, l_prefacs, propagator_type)

  ##scf.printStatment()

  ##from lorentz_rc import derive_base_structure
  ##ls = 'P(1,1)*Metric(2,3)-P(3,3)*Metric(1,2)'
  ##lb = derive_base_structure(ls).compute(range(3))
  ##print "lb:", lb
  #l_strucs = [3*Symbol('g')*Symbol('P')]
  ##l_prefacs = [[1, -1]]
  ##l_prefacs = [[1,-1,1,-1,-1,1]]
  ##l_args = [[[[1, 1], [2, 3]],[[3, 3], [1, 2]]]]

  # original QF F -> QF
  #l_prefacs = [[1, -1, 1, 1, -1, -1, -1, 1]]
  #l_args = [[[[1, 3], [2, 3]], [[1, 2], [2, 3]],
            #[[1, 1], [2, 3]],
            #[[3, 2], [1, 2]],
            #[[3, 3], [1, 2]],
            #[[3, 1], [1, 2]],
            #[[2, 3], [1, 3]], [[2, 1], [1, 3]]]]

  # original - SM  QF F -> QF
  #l_prefacs = [[1, -1]]
  #l_args = [[[[1, 1], [2, 3]],
             #[[3, 3], [1, 2]]]]

  from class_form import gen_form_class
  _, scf = gen_form_class()
  ##form_vertex(scf, l_id, l_strucs, l_args, l_prefacs, propagator_type)

  ##l_id = 'VV'
  ##l_prefacs = [[1]]
  ##P = Symbol('P')
  ##g = Symbol('g')
  ##Epsilon = Symbol('Epsilon')
  ##l_strucs = [3*Epsilon*P*P]
  ##propagator_type = 3
  ##l_args = [[[[-1, 2], [-2, 1], [-1, -2, 1, 2]]]]
  ##form_vertex(scf, l_id, l_strucs, l_args, l_prefacs, propagator_type)

  #l_id = 'FFFF'
  #l_prefacs = [[1]]
  #Ga = Symbol('Gamma')
  #l_strucs = [4*Ga*Ga]

  #propagator_type = 2
  ##ls = [['1*Gamma(-1,4,2)*Gamma(-1,1,3)']]
  #l_args = [[[[-1, 4, 2], [-1, 1, 3]]]]
  ##form_vertex(scf, l_id, l_strucs, l_args, l_prefacs, propagator_type)
  ##scf.printStatment()

  ##args = [-1, 2, 3]; freei = ['j', 'k', 'l']; f = 4
  ##print Ga_form(args, freei, {}, f)

  ##args = [1, 2, 3, 4]; freei = ['i', 'j', 'k', 'l', 'i']; f = 4
  ##print Epsilon_form(args, freei, {}, f )
##

  Gamma = Symbol('Gamma')
  Sigma = Symbol('Sigma')
  GammaM = Symbol('GammaM')
  GammaP = Symbol('GammaP')
  Epsilon = Symbol('Epsilon')
  Metric = Symbol('Metric')

  prefactors = [[2, 2], [2], [2], [2], [2], [2], [2], [2], [2]]
  ls_strucs = [4*Gamma**2, 4*Gamma*GammaM, 4*Gamma*GammaM, 4*Gamma*GammaM, 4*Gamma*GammaM, 4*Gamma*GammaP, 4*Gamma*GammaP, 4*Gamma*GammaP, 4*Gamma*GammaP]
  n_particles = 4
  propagator_type = 1
  arguments = [[[[-1, 1, 3], [-1, 4, 2]], [[-1, 1, 2], [-1, 4, 3]]], [[[-1, 1, 2], [-1, 4, 3]]], [[[-1, 1, 3], [-1, 4, 2]]], [[[-1, 4, 2], [-1, 1, 3]]], [[[-1, 4, 3], [-1, 1, 2]]], [[[-1, 4, 3], [-1, 1, 2]]], [[[-1, 1, 3], [-1, 4, 2]]], [[[-1, 4, 2], [-1, 1, 3]]], [[[-1, 1, 2], [-1, 4, 3]]]]
  l_id = 'FaFFFa1'

  #prefactors = [[2, 2], [2], [2]]
  #ls_strucs = [4*Gamma**2, 4*Gamma*GammaM, 4*Gamma*GammaM]
  #n_particles = 4
  #propagator_type = 1
  #arguments = [[[[-1, 1, 3], [-1, 4, 2]], [[-1, 1, 2], [-1, 4, 3]]], [[[-1, 1, 2], [-1, 4, 3]]], [[[-1, 1, 3], [-1, 4, 2]]]]
  #l_id = 'FaFFFa1'

  l_id = 'FaFFFa0'
  ls_strucs = [4*GammaM*GammaP, 4*GammaM**2, 4*GammaP**2]
  arguments = [[[[-1, 4, 3], [-1, 1, 2]], [[-1, 1, 2], [-1, 4, 3]], [[-1, 4, 2], [-1, 1, 3]], [[-1, 1, 3], [-1, 4, 2]]], [[[-1, 1, 2], [-1, 4, 3]], [[-1, 1, 3], [-1, 4, 2]]], [[[-1, 1, 3], [-1, 4, 2]], [[-1, 1, 2], [-1, 4, 3]]]]
  prefactors = [[2, 2, 2, 2], [2, 2], [2, 2]]

  ls_strucs = [4*Sigma**2*Epsilon]
  arguments = [[[[-1, -2, 1, 3], [-3, -4, 2, 4], [-4, -3, -2, -1]]]]
  prefactors = [[1]]

  #ls_strucs = [4*Sigma**2]
  #arguments = [[[[-1, -2, 2, 3], [-1, -2, 4, 1]]], ]
  #prefactors = [[1]]
  #propagator_type = 1

  ls_strucs = [4*Metric]
  arguments = [[[[3, 4]]]]
  prefactors = [[1]]

  propagator_type = 3

  write_form_vertex(scf, l_id, ls_strucs, arguments, prefactors, propagator_type)

  scf.printStatment()

  #l_id = 'SSS0'; propagator_type = 0
  #from sympy import Symbol
  #l_strucs = [3*Symbol('P')*Symbol('P'), 3]

  #l_prefacs = [[1, 1], [1]]
  #l_args = [[[[-1, 1], [-1, 2]], [[-1, 1], [-1, 3]]], []]

  #sc = form_vertex(None,l_id,l_strucs,l_args,l_prefacs,propagator_type,0)
  #sc.printStatment()
