#==============================================================================#
#                            loopcurrentsymbols.py                             #
#==============================================================================#

class LoopCurrentSymbols(object):

  """ Builds the loopcurrent wavefunction symbols.
  # TODO: (nick 2016-01-06) wpm is deprecated. """

  def __init__(self, max_rank):
    self.max_rank = max_rank

    self.wp = {}

    self.wpm = {}
    for rank in range(self.max_rank):
      self.wpm[rank] = {}

    # rules for substituting indices
    self.repl_dict = {}

  @staticmethod
  def is_derived(rank, tensors_ranked):
    derived_tensor = True
    #for tens in tensors_ranked:
    if rank in tensors_ranked:
      if hasattr(tensors_ranked[rank], 'derived_tensor'):
        derived_tensor = (derived_tensor and
                          tensors_ranked[rank].derived_tensor is not None)
      else:
        derived_tensor = False

    return derived_tensor

  @staticmethod
  def is_vectorized(rank, is_derived, tensors_ranked):
    if is_derived:
      vectorized = (len(tensors_ranked[rank].
                    derived_tensor.expanded_tensor) == 1)
    else:
      vectorized = len(tensors_ranked[rank].expanded_tensor) == 1
    return vectorized

  def build_symbols(self, tensors_ranked, rank_sum, dim, symmetrize=False):
    for rank in range(self.max_rank):
      if rank == 0:
        rank_str = 'j'
      else:
        rank_str = 'riOut'

      # scalar case: - no wavefunction index dependence
      #              - wavefunction may (not) depend on the incoming rank j
      if dim == 1:
        # wavefunction explicitly depends on j
        if rank_sum:
          wp_sym = "wp(0," + rank_str + ")"
          repl_dict = {'rank': 'j'}
        # wavefunction does not depend on j
        else:
          wp_sym = "wp(0,:)"
          repl_dict = {'rank': ':'}

        self.wp[rank] = wp_sym
        self.repl_dict[rank] = repl_dict
        if rank == 2 and symmetrize:
          self.wp[(1, 1)] = wp_sym
          self.repl_dict[(1, 1)] = repl_dict
        elif rank == 3 and symmetrize:
          self.wp[(1, 1, 1)] = wp_sym
          self.repl_dict[(1, 1, 1)] = repl_dict
          self.wp[(2, 1, 1)] = wp_sym
          self.repl_dict[(2, 1, 1)] = repl_dict
          self.wp[(1, 2, 1)] = wp_sym
          self.repl_dict[(1, 2, 1)] = repl_dict
          self.wp[(1, 1, 2)] = wp_sym
          self.repl_dict[(1, 1, 2)] = repl_dict

      # wavefunction is a vector
      else:
        is_derived = self.is_derived(rank, tensors_ranked)
        is_vectorized = self.is_vectorized(rank, is_derived, tensors_ranked)

        if rank_sum:
          if is_vectorized:
            wp_sym = "wp(:," + rank_str + ")"
            repl_dict = {'rank': 'j', 'i': ':'}
          else:
            wp_sym = list(map(lambda x: "wp(" + str(x) + "," + rank_str + ")",
                              range(dim)))
            repl_dict = {'rank': 'j'}

          self.wp[rank] = wp_sym
          self.repl_dict[rank] = repl_dict
          if rank == 2 and symmetrize:
            self.wp[(1, 1)] = wp_sym
            self.repl_dict[(1, 1)] = repl_dict
          elif rank == 3 and symmetrize:
            self.wp[(1, 1, 1)] = wp_sym
            self.repl_dict[(1, 1, 1)] = repl_dict
            self.wp[(2, 1, 1)] = wp_sym
            self.repl_dict[(2, 1, 1)] = repl_dict
            self.wp[(1, 2, 1)] = wp_sym
            self.repl_dict[(1, 2, 1)] = repl_dict
            self.wp[(1, 1, 2)] = wp_sym
            self.repl_dict[(1, 1, 2)] = repl_dict
        else:
          if is_vectorized:
            # disabled for the moment because of rank mismatch in fortrancode
            wp_sym = "wp(:, " + rank_str + ")"
            repl_dict = {'rank': 'j', 'i': ':'}
          else:
            wp_sym = list(map(lambda x: "wp(" + str(x) + ",:)", range(dim)))
            repl_dict = {'rank': ':'}
          self.wp[rank] = wp_sym
          self.repl_dict[rank] = repl_dict
          if rank == 2 and symmetrize:
            self.wp[(1, 1)] = wp_sym
            self.repl_dict[(1, 1)] = repl_dict
          elif rank == 3 and symmetrize:
            self.wp[(1, 1, 1)] = wp_sym
            self.repl_dict[(1, 1, 1)] = repl_dict
            self.wp[(2, 1, 1)] = wp_sym
            self.repl_dict[(2, 1, 1)] = repl_dict
            self.wp[(1, 2, 1)] = wp_sym
            self.repl_dict[(1, 2, 1)] = repl_dict
            self.wp[(1, 1, 2)] = wp_sym
            self.repl_dict[(1, 1, 2)] = repl_dict
