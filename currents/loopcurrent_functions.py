#==============================================================================#
#                           loopcurrent_functions.py                           #
#==============================================================================#

#===========#
#  Imports  #
#===========#

from sympy import Symbol

from rept1l.combinatorics import free_indices
from rept1l.tensor import tensor_arg, tensor, diracAlgebra
d = diracAlgebra

#===========#
#  Methods  #
#===========#

def index_gen(i=-20, not_allowed=[]):
  while True:
    i -= 1
    while i in not_allowed:
      i -= 1
    yield i

#------------------------------------------------------------------------------#

def feynman_propagator(free_ci, varg=None, domain=range(4), resolve=False,
                       pext=None, rank=0, rank_inc=0, **kwargs):
  # for loop currents the propagator numerator (factor) is taken into account
  # in write_loop_vertex
  if pext is None:
    return 1, 'i', free_ci

  if pext != 'RXI1' and pext != 'RXI2':
    raise Exception('Propagator extension ' + str(pext) + ' not supported.')

  ci = next(free_ci)
  varg_in = tensor_arg(ci, domain=domain, resolve=resolve, metric=d.g)
  varg_out = tensor_arg('i', domain=domain, resolve=resolve, metric=d.g)

  pext_sign = 1 if pext == 'RXI2' else -1
  m = Symbol("m")
  if rank_inc == 0:
    p1 = tensor([varg_in], symbol=Symbol('p'))
    p2 = tensor([varg_out], symbol=Symbol('p'))
    return p1*p2*(pext_sign/m**2), ci, free_ci
  elif rank_inc == 1:
    mu = free_indices[rank-1]
    muarg = tensor_arg(mu, domain=domain, resolve=resolve, metric=d.g)

    #q^out p^in
    g1 = tensor([muarg, varg_out], numerical=d.g,
                symbol=Symbol('gg'), delta=True)
    p1 = tensor([varg_in], symbol=Symbol('p'))
    t1 = g1*p1*(pext_sign/m**2)

    #p^out q^in
    g2 = tensor([muarg, varg_in], numerical=d.g,
                symbol=Symbol('gg'), delta=True)
    p2 = tensor([varg_out], symbol=Symbol('p'))
    t2 = g2*p2*(pext_sign/m**2)

    t = t1+t2
    return t, ci, free_ci
  elif rank_inc == 2:
    mu1 = free_indices[rank-1]
    muarg1 = tensor_arg(mu1, domain=domain, resolve=resolve, metric=d.g)
    mu2 = free_indices[rank]
    muarg2 = tensor_arg(mu2, domain=domain, resolve=resolve, metric=d.g)

    #q^out q^in
    g3 = tensor([muarg1, varg_out], numerical=d.g,
                symbol=Symbol('gg'), delta=True)
    g4 = tensor([muarg2, varg_in], numerical=d.g,
                symbol=Symbol('gg'), delta=True)
    t = g3*g4*(pext_sign/m**2)
    return t, ci, free_ci


#------------------------------------------------------------------------------#

def fermion_propagator(free_ci, lorentz_id, fermion_out=True, domain=range(4),
                       resolve=True):
  ci = next(free_ci)
  if fermion_out:
    pslash_arg1 = tensor_arg(ci, domain=domain, resolve=resolve)
    pslash_arg2 = tensor_arg('i', domain=domain, resolve=resolve)
  else:
    pslash_arg1 = tensor_arg('i', domain=domain, resolve=resolve)
    pslash_arg2 = tensor_arg(ci, domain=domain, resolve=resolve)

  # \slash p + m
  pslash_tensor = tensor([pslash_arg1, pslash_arg2],
                         numerical=d.pslash, symbol=Symbol('PS'))
  m = Symbol("m")
  mass_tensor = m*tensor([pslash_arg1, pslash_arg2], numerical=d.one)

  # \slash q
  pslash_arg0 = tensor_arg(lorentz_id, domain=domain, resolve=resolve,
                           metric=d.g)
  qmu_tensor = tensor([pslash_arg0, pslash_arg1, pslash_arg2],
                      numerical=d.gamma, symbol=Symbol('Gamma'))

  # p_sign = -1: the momentum direction is opposed to the fermion line direction
  p_sign = -1 if fermion_out else 1
  return ((p_sign * pslash_tensor + mass_tensor),
          (p_sign * qmu_tensor), ci, free_ci)

#------------------------------------------------------------------------------#

def fermion_prop(free_ci, lorentz_id, fermion_out=True, domain=range(4),
                 resolve=True, loop=False):
  ci = next(free_ci)
  if fermion_out:
    pslash_arg1 = tensor_arg(ci, domain=domain, resolve=resolve)
    pslash_arg2 = tensor_arg('i', domain=domain, resolve=resolve)
  else:
    pslash_arg1 = tensor_arg('i', domain=domain, resolve=resolve)
    pslash_arg2 = tensor_arg(ci, domain=domain, resolve=resolve)

  # \slash p + m
  pslash_tensor = tensor([pslash_arg1, pslash_arg2],
                         numerical=d.pslash, symbol=Symbol('PS'))
  if not loop:
    m = Symbol("m")
    mass_tensor = m*tensor([pslash_arg1, pslash_arg2], numerical=d.one)

  # \slash q
  pslash_arg0 = tensor_arg(lorentz_id, domain=domain, resolve=resolve,
                           metric=d.g)
  qmu_tensor = tensor([pslash_arg0, pslash_arg1, pslash_arg2],
                      numerical=d.gamma, symbol=Symbol('Gamma'))

  # p_sign = -1: the momentum direction is opposed to the fermion line direction
  p_sign = -1 if fermion_out else 1
  if not loop:
    return (p_sign * pslash_tensor + mass_tensor), ci, free_ci
  else:
    return p_sign * qmu_tensor, ci, free_ci

#------------------------------------------------------------------------------#

def fermion_additionals(additional, final_particle):
  pl = Symbol("pl(:)")
  pl_sum = 0
  for k in range(final_particle-1):
    pl_sum += Symbol("PL" + str(k+1) + "(:)")
  if (pl, pl_sum) not in additional:
    additional.append((pl, pl_sum))

#------------------------------------------------------------------------------#

def vector_additionals(additional, final_particle, last_contraction):
  """ Appends necessary variables for a vector current. """
  if not last_contraction:
    p = Symbol("p(:)")
    p_sum = 0
    for k in range(final_particle-1):
      p_sum += Symbol("P" + str(k+1) + "(:)")

    if not (p, p_sum) in additional:
      additional.append((p, p_sum))

  return additional

#------------------------------------------------------------------------------#

def is_final_arg(arg, final_particle):
  return arg == final_particle or arg == 'i'

#------------------------------------------------------------------------------#

def construct_arg(arg, free_ci, final_particle, domain=range(4),
                  resolve=True, metric=None):
  """ Constructs the wavefunction(tensor) if arg > 0, else sets the wavefunction
  to None and takes a free contraction index and contructs a tensor argument.

  :return: wavefunction, tensor argument, left free contraction indices
  """
  try:
    posint = int(arg) >= 0
  except ValueError:
    posint = False
  if posint and not is_final_arg(arg, final_particle):
    ci = next(free_ci)
    wave_arg = tensor_arg(ci, domain=domain, resolve=resolve, metric=metric)
    if arg == 1:  # loop current
      wave_symbol = Symbol('wl')
      wave_arg2 = tensor_arg('rank', domain=None, resolve=False)
      wave_args = [wave_arg, wave_arg2]
    else:
      wave_args = [wave_arg]
      wave_symbol = Symbol('w' + str(arg))
    wave_tensor = tensor(wave_args, symbol=wave_symbol)
    t_arg = tensor_arg(ci, domain=domain, resolve=resolve, metric=metric)
  else:
    wave_tensor = 1
    t_arg = tensor_arg(arg, domain=domain, resolve=resolve, metric=metric)
  return wave_tensor, t_arg, free_ci
