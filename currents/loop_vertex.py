#==============================================================================#
#                                 loop vertex                                  #
#==============================================================================#

from __future__ import print_function

#============#
#  Includes  #
#============#

from rept1l.pyfort import selectBlock, caseBlock, ifBlock
from rept1l.combinatorics import fold

#############
#  Methods  #
#############


def add_list_tuples(l):
  return fold(lambda x, y: x + y, l, ())


def gen_rank_case(id_n, rankdict, tabs=3):
  """ Generates the a if/else block checking whether zero-couplings lead to a
  smaller rank increase.
  """
  # no rank increase in structure
  if len(rankdict) == 0:
    return
  else:
    lrs = caseBlock(str(id_n), tabs=tabs)
    ff = None
    rank, coupl = rankdict[0]
    case_str = ' .or. ' .join('cc(' + str(u) + ')' for u in coupl)
    ff = ifBlock(case_str, tabs=tabs + 1)
    ff + ('mr = ' + str(rank))
    lrs + ff

    for rank, coupl in rankdict[1:]:
      case_str = ' .or. ' .join('cc(' + str(u) + ')' for u in coupl)
      (ff > 0) + ('else if(' + case_str + ') then')
      ff + ('mr = ' + str(rank))
    (ff > 0) + 'else'
    ff + ('mr = 0')
  return lrs


def write_loop_vertex(scl, sclr, results, n_particles, wave_dim, prop_num,
                      id_tag, id_n, apply_cse=True, simplify=True,
                      pext=None):

  from rept1l.currents.current import Current
  OLC = Current(results, n_particles,
                dim=wave_dim, prop_num=prop_num, loop_current=True,
                pext=pext)

  results_loop = OLC.current
  additionals_loop = OLC.additionals

  from rept1l.currents.loopcurrentpyfort import LoopCurrentPyFort
  t = LoopCurrentPyFort(results_loop, additionals_loop, wave_dim,
                        apply_cse=apply_cse, simplify=simplify)

  select = caseBlock(id_tag, tabs=3)
  for c in OLC.feynman_rule:
    select.addcomment(str(c))

  # max_rank in LoopCurrentPyFort is max_rank + 1 in reality
  max_rank = t.max_rank - 1

  coupl_rank = []
  dealt_with = set()
  for r in range(max_rank, 0, -1):
    if r in OLC.couplings_ranked:
      cr = [c for c in OLC.couplings_ranked[r] if c not in dealt_with]
      if len(cr) > 0:
        dealt_with.update(set(OLC.couplings_ranked[r]))
        coupl_rank.append((r, tuple(cr)))

  # build fortran code and add it to select lorentz rank if non-trivial
  rr_pyfort = gen_rank_case(id_n, coupl_rank)
  if rr_pyfort:
    sclr + rr_pyfort

  # old method kept for compatibility
  import rept1l.currents.lorentz_lib as lb
  lb.lorentz_rank[id_n] = max_rank

  if t.rank_sum is None:
    select.append(t.rank0)
    if t.rankN is not None:
      select.append(t.rankN[0][0])
  else:
    select.append(t.pre_rank_sum)
    select.append(t.rank_sum)

  if wave_dim == 1:
    select.addStatement("wp(1:3,:) = cnul ", n_tabs=1)

  scl.append(select)

if __name__ == "__main__":
  import doctest
  doctest.testmod()
  from sympy import I
  propagator_numerator = -I
  ls = [['P(3,1)*Metric(1,2)', '-1*P(3,2)*Metric(1,2)', '-1*P(2,1)*Metric(1,3)',
        'P(2,3)*Metric(1,3)', 'P(1,2)*Metric(2,3)', '-1*P(1,3)*Metric(2,3)']]
  ls = [['1*GammaM(3,2,1)'], ['2*GammaP(3,2,1)']]
  ls = [['1*P(2,1)*Metric(1,3)', '-1*P(3,1)*Metric(1,2)', '-1*P(1,2)*Metric(2,3)', '1*P(3,2)*Metric(1,2)', '1*P(1,3)*Metric(2,3)', '-1*P(2,3)*Metric(1,3)']]
  ls = [['1*P(3,1)', '1*P(3,3)/2']]
  ls = [['P(3,1)*Metric(1,2)', '-1*P(3,2)*Metric(1,2)', ' P(3,3)*Metric(1,2)',
         '-1*P(2,1)*Metric(1,3)', 'P(2,3)*Metric(1,3)', '-1*P(1,1)*Metric(2,3)',
         'P(1,2)*Metric(2,3)', '-1*P(1,3)*Metric(2,3)']]

  ls = [['P(1,3)*Metric(2,3)',
         '-1*P(1,2)*Metric(2,3)',
         'P(1,1)*Metric(2,3)',
         'P(3,2)*Metric(1,2)',
         '-1*P(3,3)*Metric(1,2)',
         '-1*P(3,1)*Metric(1,2)',
         '-1*P(2,3)*Metric(1,3)',
         'P(2,1)*Metric(1,3)'
         ]]

  #ls = [['P(1,3)*Metric(2,3)',
         #'-1*P(1,2)*Metric(2,3)',
         #'P(1,1)*Metric(2,3)',
         #'P(3,2)*Metric(1,2)']]
  #ls =[[ 'Metric(2,3)' ]]
  #lorentz_strings = [['1*GammaM(1,3,2)'], ['1*GammaP(1,3,2)']]
  #ls = [['1*Gamma(-1,4,2)*Gamma(-1,1,3)']]
  #ls = [['1*P(3,1)', '1*P(3,2)']]
  #ls =  [['1*ProjM(3,2)'], ['1*ProjP(3,2)']]
  #ls = [['1*GammaM(1,2,3)'], ['1*GammaP(1,2,3)']]
  ls = [['Sigma(1,2,3,4)']]

  ls = [['1*GammaP(-1,4,3)*GammaM(-1,1,2)', '1*GammaP(-1,1,2)*GammaM(-1,4,3)'],
        ['1*GammaP(-1,4,2)*GammaM(-1,1,3)', '1*GammaP(-1,1,3)*GammaM(-1,4,2)'],
        ['1*Sigma(-1,-2,1,2)*Sigma(-1,-2,4,3)'],
        ['1*Sigma(-1,-2,1,3)*Sigma(-1,-2,4,2)'],
        ['1*GammaM(-1,1,2)*GammaM(-1,4,3)'], ['1*GammaM(-1,1,3)*GammaM(-1,4,2)'],
        ['1*GammaP(-1,1,2)*GammaP(-1,4,3)'], ['1*GammaP(-1,1,3)*GammaP(-1,4,2)']]

  #ls = [['P(3,1)*P(-1,1)*Gamma(-1,2,1)']]
  ls = [['-2*Metric(1,2)*Metric(3,4)', '1*Metric(1,3)*Metric(2,4)', '1*Metric(1,4)*Metric(2,3)']]
  ls = [['-1*P(2,1)*Metric(1,3)', '1*P(3,1)*Metric(1,2)', '1*P(1,2)*Metric(2,3)',
         '-1*P(3,2)*Metric(1,2)', '-1*P(1,3)*Metric(2,3)', '1*P(2,3)*Metric(1,3)']]

  ls = [['Metric(2,3)']]

  #ls = [['P(2,3)*P(3,2)'],
  ls = [['P(2,1)*P(3,2)', 'P(2,3)*P(3,1)'],
        ['-Epsilon(2,3,-1,-2)*P(-1,2)*P(-2,3)'],
        ['Metric(2,3)'],
        ['-Metric(2,3)*P(-1,2)*P(-1,3)'],
        ['-Metric(2,3)*P(-1,1)*P(-1,2)', '-Metric(2,3)*P(-1,1)*P(-1,3)']]

  ls = [['P(1,2)*P(2,1)'],
        ['P(1,2)*P(2,3)'],
        ['Epsilon(2,1,-1,-2)*P(-1,1)*P(-2,2)',
         '- Epsilon(2,1,-2,-1)*P(-1,1)*P(-2,2)'],
        ['-Metric(1,2)*P(-1,1)*P(-1,2)'],
        ['-Metric(1,2)*P(-1,2)*P(-1,3)']]

  ls = [['P(1,2)*P(2,1)*P(-1,1)*P(-1,1)']]

  ls = [['Metric(1,3)'], ['P(1,1)*P(3,3)'], ['P(3,1)*P(1,3)'], ['P(-1,1)*P(-1,3)*Metric(1,3)']]
  ls = [['P(1,1)*P(3,3)'], ['P(3,1)*P(1,3)']]
  #ls = [['Metric(1,3)'], ['P(-1,1)*P(-1,3)*Metric(1,3)']]
  #ls = [['Metric(1,2)'], ['P(-1,1)*P(-1,2)*Metric(1,2)']]
  ls = [['- P(1,2)*Metric(3,4)', 'P(3,2)*Metric(1,4)', 'P(4,3)*Metric(1,3)', '-2*P(1,3)*Metric(3,4)', '- P(4,1)*Metric(1,3)', '2*P(3,1)*Metric(1,4)', '- P(1,1)*Metric(3,4)', 'P(3,3)*Metric(1,4)']]
  ls = [['Gamma(1,2,3)']]

  ls = [['+ P(1,2)*Metric(2,3)', '- P(3,2)*Metric(1,2)', '- P(1,3)*Metric(2,3)',
         '+ P(2,3)*Metric(1,3)', '- P(2,1)*Metric(1,3)', '+ P(3,1)*Metric(1,2)']]

  ls = [['+ P(4,2)*Metric(2,3)', '- P(4,3)*Metric(2,3)', '- P(3,2)*Metric(2,4)',
    '+ P(2,3)*Metric(3,4)', '+ P(3,4)*Metric(2,4)', '- P(2,4)*Metric(3,4)']]

  ls = [['+ P(2,3)*Metric(3,4)', '- P(4,3)*Metric(2,3)', '- P(3,2)*Metric(2,4)',
    '+ P(4,2)*Metric(2,3)', '- P(2,4)*Metric(3,4)', '+ P(3,4)*Metric(2,4)']]

  ls = [['+ Metric(2,5)*Metric(3,4)'], ['+ Metric(2,4)*Metric(3,5)'],
        ['+ Metric(2,3)*Metric(4,5)']]

  ls = [['+ Metric(1,4)*Metric(2,3)'], ['+ Metric(1,3)*Metric(2,4)'],
        ['+ Metric(1,2)*Metric(3,4)']]

  ls = [['+ Metric(2,5)*Metric(3,4)'], ['+ Metric(2,4)*Metric(3,5)'],
        ['+ Metric(2,3)*Metric(4,5)']]

  ls = [['+ P(1,2)*P(2,1)'], ['+ P(-1,1)*P(-1,2)*Metric(1,2)']]

  ls = [['+ P(1,3)*P(3,1)'], ['+ P(-1,1)*P(-1,3)*Metric(1,3)']]
  # ls = [['+ P(-1,1)*P(-1,3)*Metric(1,3)']]

  # ls = [['+ P(2,3)*P(3,2)'], ['+ P(-1,2)*P(-1,3)*Metric(2,3)']]

  from rept1l.currents.pyfort_classes.class_tree import gen_tree_class

  class_tree, subct, sct = gen_tree_class()
  #sub_hc, helicity_cases = gen_helicity_subroutine()

  from rept1l.pyfort import spaceBlock
  sb = spaceBlock()
  import rept1l.currents.lorentz_lib as lb
  write_loop_vertex(sct[2], sb, ls, 3, 4, propagator_numerator, '0', 0,
                    pext=None, apply_cse=False)
  sct[2].printStatment()
  print("lb.lorentz_rank[0] :", lb.lorentz_rank[0])
  sb.printStatment()
