#==============================================================================#
#                                 gammaloop.py                                 #
#==============================================================================#

#============#
#  Includes  #
#============#

from sympy import Symbol

from rept1l.combinatorics import construct_tensor_keys, free_indices, fold
from rept1l.tensor import tensor_arg, tensor, diracAlgebra
from rept1l.currents.loopcurrent_functions import index_gen, feynman_propagator
from rept1l.currents.loopcurrent_functions import fermion_prop
from rept1l.currents.loopcurrent_functions import construct_arg
from rept1l.currents.loopcurrent_functions import fermion_additionals
from rept1l.currents.loopcurrent_functions import vector_additionals
d = diracAlgebra

#===========#
#  Methods  #
#===========#

def build_gamma_tensor(vb_arg, af_arg, f_arg, null_plus_minus, prop,
                       final_particle, free_ci):
  """ Builds \gamma^{\mu_vb_arg}_{af_arg, f_arg} * wave functions
  * propagator.  Where propagator and/or wave functions are eventually 1. """
  # vbt: Vectorboson wavefunction (tensor)
  # vbt_arg: VB argument which is used for the gamma structure
  vb_t, vb_t_arg, free_ci = construct_arg(vb_arg, free_ci, final_particle,
                                          metric=d.g)

  # ft: Fermion wavefunction (tensor)
  # ft_arg: Fermionic argument which is used in the gamma structure
  f_t, f_t_arg, free_ci = construct_arg(f_arg, free_ci, final_particle)

  # aft: AntiFermion wavefunction (tensor)
  # ft_arg: AntiFermionic argument which is used in the gamma structure
  af_t, af_t_arg, free_ci = construct_arg(af_arg, free_ci, final_particle)
  # Build up the structure
  g = {'0': d.gamma, '+': d.gammaP, '-': d.gammaM}
  gamma_t = tensor([vb_t_arg, af_t_arg, f_t_arg],
                   numerical=g[null_plus_minus], symbol=Symbol('Gamma'))

  gamma_t = gamma_t * vb_t * f_t * af_t
  gamma_t = prop * gamma_t

  return gamma_t

def GammaLoop(final_particle, dirac, vectorized, last_flag, additional, msyms,
              args_i_j, max_rank=0, null_plus_minus='', pext=None):
  """ Gamma(i,j,k) = \gamma^(\mu_i)_{jk} """

  # assign arguments
  vb_arg = args_i_j[0]
  af_arg = args_i_j[1]
  f_arg = args_i_j[2]

  # make sure that none of the above indices is used as contraction indices
  reserverd_ci = fold(lambda x,y: x+y, [msyms[key].contraction_indices() for key in msyms], [])
  free_ci = index_gen(not_allowed=[vb_arg, f_arg, af_arg] + reserverd_ci)

  # generate the propagator (in presence of external indices)
  if vb_arg == final_particle:
    prop, vb_arg_s, free_ci = feynman_propagator(free_ci, rank=0, pext=pext)
    inc_rank_prop = False if pext is None else True
    af_arg_s = af_arg
    f_arg_s = f_arg
    if inc_rank_prop:
      vector_additionals(additional, final_particle, last_flag)
  elif af_arg == final_particle:
    fermion_additionals(additional, final_particle)
    prop, af_arg_s, free_ci = fermion_prop(free_ci, free_indices[max_rank],
                                           fermion_out=False)
    inc_rank_prop = True
    f_arg_s = f_arg
    vb_arg_s = vb_arg
  elif f_arg == final_particle:
    fermion_additionals(additional, final_particle)
    prop, f_arg_s, free_ci = fermion_prop(free_ci, free_indices[max_rank],
                                          fermion_out=True)
    inc_rank_prop = True
    af_arg_s = af_arg
    vb_arg_s = vb_arg
  else:
    prop = 1
    inc_rank_prop = False
    vb_arg_s = vb_arg
    af_arg_s = af_arg
    f_arg_s = f_arg

  # construct the wavefunctions (in presence of external indices)
  gamma_t = build_gamma_tensor(vb_arg_s, af_arg_s, f_arg_s, null_plus_minus,
                               prop, final_particle, free_ci)

  # case without rank increase by propagator
  if inc_rank_prop is False:
    for rank in range(max_rank + 1):
      keys = construct_tensor_keys(max_rank, rank)
      for key in keys:
        msyms[key] = gamma_t * msyms[key]
  else:
    lower = {}
    for rank in range(max_rank + 1):
      keys = construct_tensor_keys(max_rank, rank)
      for key in keys:
        lower[key] = gamma_t * msyms[key]

    upper = {}
    for rank in range(max_rank + 1):
      new_keys = construct_tensor_keys(max_rank, rank)
      for key in new_keys:
        if key not in msyms.keys():
          msyms[key] = []

    max_rank += 1
    if af_arg == final_particle:
      propr1, af_arg_s, free_ci = fermion_prop(free_ci,
                                               free_indices[max_rank-1],
                                               fermion_out=False, loop=True)
    elif f_arg == final_particle:
      propr1, f_arg_s, free_ci = fermion_prop(free_ci,
                                              free_indices[max_rank-1],
                                              fermion_out=True, loop=True)
    else:
      propr1, vb_arg_s, free_ci = feynman_propagator(free_ci, rank=max_rank,
                                                     rank_inc=1, pext=pext)
      propr2, vb_arg_s2, free_ci = feynman_propagator(free_ci, rank=max_rank,
                                                      rank_inc=2, pext=pext)

    # first rank increase (e.g. mu)
    gamma_tr1 = build_gamma_tensor(vb_arg_s, af_arg_s, f_arg_s, null_plus_minus,
                                   propr1, final_particle, free_ci)

    for rank in range(max_rank):
      old_keys = construct_tensor_keys(max_rank - 1, rank)
      for key in old_keys:
        if (key == '0'):
          new_key = free_indices[max_rank - 1]
        else:
          new_key = key + free_indices[max_rank - 1]

        if msyms[key] == []:
          upper[new_key] = gamma_tr1
        else:
          upper[new_key] = gamma_tr1 * msyms[key]

    if vb_arg == final_particle:
      max_rank += 1
      # second rank increase (e.g. nu) + rank 2 increase (e.g. munu)
      gamma_tr2 = build_gamma_tensor(vb_arg_s2, af_arg_s, f_arg_s,
                                     null_plus_minus, propr2, final_particle,
                                     free_ci)
      for rank in range(max_rank):
        old_keys = construct_tensor_keys(max_rank-2, rank)
        for key in old_keys:
          if (key == '0'):
            new_key = ''.join(free_indices[max_rank - 2:max_rank])
          else:
            new_key = key + ''.join(free_indices[max_rank - 2:max_rank])

          if msyms[key] == []:
            upper[new_key] = gamma_tr2
          else:
            upper[new_key] = msyms[key] * gamma_tr2

    # merging the results
    for key in lower.keys():
      msyms[key] = lower[key]

    for key in upper.keys():
      msyms[key] = upper[key]

  return msyms, additional, vectorized, max_rank
