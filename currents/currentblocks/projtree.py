#==============================================================================#
#                                 projtree.py                                  #
#==============================================================================#

from sympy import Symbol
from rept1l.tensor import tensor_arg, tensor, diracAlgebra
from rept1l.currents.treecurrent_functions import index_gen
from rept1l.currents.treecurrent_functions import fermion_propagator
from rept1l.currents.treecurrent_functions import construct_arg
from rept1l.currents.treecurrent_functions import fermion_additionals
d = diracAlgebra

def ProjTree(final_particle, dirac, vectorized, last_flag,
             additional, msyms, args_i_j, last_contraction=False,
             null_plus_minus='', **kwargs):

  fermion_arg = args_i_j[1]
  afermion_arg = args_i_j[0]

  if fermion_arg == final_particle or afermion_arg == final_particle:
    fermion_additionals(additional, final_particle, last_contraction)
    last_flag = True

  free_ci = index_gen(not_allowed=[fermion_arg, afermion_arg] +
                                  msyms.contraction_indices())

  prop = {fermion_arg: lambda x: fermion_propagator(x, fermion_out=True),
          afermion_arg: lambda x: fermion_propagator(x, fermion_out=False)}

  if final_particle in prop and not last_contraction:
    prop, final_i, free_ci = prop[final_particle](free_ci)
    if fermion_arg == final_particle:
      fermion_arg = final_i
    elif afermion_arg == final_particle:
      afermion_arg = final_i
    else:
      vector_arg = final_i
  else:
    prop = 1

  f_t, f_t_arg, free_ci = construct_arg(fermion_arg, free_ci, final_particle)
  af_t, af_t_arg, free_ci = construct_arg(afermion_arg, free_ci, final_particle)

  pj = {'0': d.one, '+': d.ProjP, '-': d.ProjM}

  pj_tensor = tensor([af_t_arg, f_t_arg],
                     numerical=pj[null_plus_minus], symbol=Symbol('PJ'))

  pj_tensor = pj_tensor * prop * f_t * af_t
  msyms = pj_tensor * msyms

  return msyms, additional, vectorized, last_flag

