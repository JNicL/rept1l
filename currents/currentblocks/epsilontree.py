###############################################################################
#                               epsilontree.py                                #
###############################################################################

#============#
#  Includes  #
#============#

from sympy import Symbol
from rept1l.tensor import tensor_arg, tensor, diracAlgebra, DeltaTensor
from rept1l.currents.treecurrent_functions import index_gen, feynman_propagator
from rept1l.currents.treecurrent_functions import construct_arg
d = diracAlgebra

#############
#  Methods  #
#############

def EpsilonTree(final_particle, dirac, vectorized, last_flag, additional, msyms,
                args, last_contraction=False, pext=None, **kwargs):

  vb1_arg = args[0]
  vb2_arg = args[1]
  vb3_arg = args[2]
  vb4_arg = args[3]

  if any(u == final_particle for u in args):
    last_case = "if (last) VB_last = .true."
    additional.append(last_case)

  free_ci = index_gen(not_allowed=args + msyms.contraction_indices())

  prop = {vb1_arg: lambda x: feynman_propagator(x, pext=pext),
          vb2_arg: lambda x: feynman_propagator(x, pext=pext),
          vb3_arg: lambda x: feynman_propagator(x, pext=pext),
          vb4_arg: lambda x: feynman_propagator(x, pext=pext)}

  if final_particle in prop and not last_contraction:
    prop, final_i, free_ci = prop[final_particle](free_ci)
    if vb1_arg == final_particle:
      vb1_arg = final_i
    elif vb2_arg == final_particle:
      vb2_arg = final_i
    elif vb3_arg == final_particle:
      vb3_arg = final_i
    else:
      vb4_arg = final_i
  else:
    prop = 1

  vb1_t, vb1_t_arg, free_ci = construct_arg(vb1_arg, free_ci, final_particle,
                                            metric=d.g)
  vb2_t, vb2_t_arg, free_ci = construct_arg(vb2_arg, free_ci, final_particle,
                                            metric=d.g)
  vb3_t, vb3_t_arg, free_ci = construct_arg(vb3_arg, free_ci, final_particle,
                                            metric=d.g)
  vb4_t, vb4_t_arg, free_ci = construct_arg(vb4_arg, free_ci, final_particle,
                                            metric=d.g)

  epsilon_tensor = tensor([vb1_t_arg, vb2_t_arg, vb3_t_arg, vb4_t_arg],
                          numerical=d.epsilon, symbol=Symbol('Epsilon'))

  epsilon_tensor = epsilon_tensor * prop * vb1_t * vb2_t * vb3_t * vb4_t
  msyms = epsilon_tensor * msyms
  return msyms, additional, vectorized, last_flag
