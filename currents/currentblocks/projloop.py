#==============================================================================#
#                                 projloop.py                                  #
#==============================================================================#

#============#
#  Includes  #
#============#

from sympy import Symbol
from rept1l.combinatorics import construct_tensor_keys, free_indices, fold
from rept1l.tensor import tensor_arg, tensor, diracAlgebra
from rept1l.currents.loopcurrent_functions import index_gen, feynman_propagator
from rept1l.currents.loopcurrent_functions import fermion_propagator
from rept1l.currents.loopcurrent_functions import construct_arg
from rept1l.currents.loopcurrent_functions import fermion_additionals
d = diracAlgebra

#===========#
#  Methods  #
#===========#

def ProjLoop(final_particle, dirac, vectorized, last_flag,
             additional, msyms, args_i_j, max_rank=0,
             null_plus_minus='', **kwargs):

  # assign arguments
  f_arg = args_i_j[1]  # fermion argument (defined as the rightmost index of
                       # gamma)
  af_arg = args_i_j[0]  # antifermion argument

  # make sure that none of the above indices is used as contraction indices
  reserverd_ci = fold(lambda x,y: x+y, [msyms[key].contraction_indices() for key in msyms], [])
  free_ci = index_gen(not_allowed=[f_arg, af_arg] + reserverd_ci)

  # generate the propagator if the one of the indices corresponds to the final
  # particle. Also generates the part of the propagator which increases the rank
  # (\slash q where q is the loop momentum)
  if f_arg == final_particle:
    fermion_additionals(additional, final_particle)
    max_rank += 1
    prop, propr, f_arg, free_ci = fermion_propagator(free_ci,
                                                     free_indices[max_rank-1],
                                                     fermion_out=True)
  elif af_arg == final_particle:
    fermion_additionals(additional, final_particle)
    max_rank += 1
    prop, propr, af_arg, free_ci = fermion_propagator(free_ci,
                                                      free_indices[max_rank-1],
                                                      fermion_out=False)
  else:
    prop = 1
    propr = None

  # construct the wavefunctions (in present of external indices)
  ft, ft_arg, free_ci = construct_arg(f_arg, free_ci, final_particle)
  aft, aft_arg, free_ci = construct_arg(af_arg, free_ci, final_particle)

  # Build up the structure
  pj = {'0': d.one, '+': d.ProjP, '-': d.ProjM}
  projt = tensor([aft_arg, ft_arg],
                 numerical=pj[null_plus_minus], symbol=Symbol('PJ'))

  # multiply with external wavefunctions
  projt = projt * ft * aft
  # multiply with the propagator
  projt = prop * projt

  # in case the rank is not increased the structure is simply given by the
  # structure msyms times the projector tensor
  if propr is None:
    for rank in range(max_rank + 1):
      keys = construct_tensor_keys(max_rank, rank)
      for key in keys:
        msyms[key] = projt * msyms[key]
  else:
    lower = dict()
    for rank in range(max_rank):
      keys = construct_tensor_keys(max_rank-1, rank)
      for key in keys:
        lower[key] = projt * msyms[key]

    upper = dict()
    for rank in range(max_rank + 1):
      new_keys = construct_tensor_keys(max_rank, rank)
      for key in new_keys:
        if key not in msyms.keys():
          msyms[key] = []

    # The rank is increased by a \slash q in the numerator
    # Need to reconstruct the wavefunctions (in present of external indices)!
    ftr, ftr_arg, free_ci = construct_arg(f_arg, free_ci, final_particle)
    aftr, aftr_arg, free_ci = construct_arg(af_arg, free_ci, final_particle)

    # Need to rebuild up the structure!
    projtr = tensor([aftr_arg, ftr_arg],
                    numerical=pj[null_plus_minus], symbol=Symbol('PJ'))
    projtr = projtr * propr * ftr * aftr
    for rank in range(max_rank):
      old_keys = construct_tensor_keys(max_rank - 1, rank)
      for key in old_keys:
        if (key == '0'):
          new_key = free_indices[max_rank - 1]
        else:
          new_key = key + free_indices[max_rank - 1]
        new_arg = tensor_arg(free_indices[max_rank - 1], domain=range(4),
                             resolve=False)
        upper[new_key] = projtr * msyms[key]

    # merge the results
    for key in lower.keys():
      msyms[key] = lower[key]

    for key in upper.keys():
      msyms[key] = upper[key]

  return msyms, additional, vectorized, max_rank
