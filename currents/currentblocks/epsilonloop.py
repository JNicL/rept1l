###############################################################################
#                                epsilonloo.py                                #
###############################################################################

#############
#  Include  #
#############

from sympy import Symbol
from rept1l.combinatorics import construct_tensor_keys, free_indices, fold
from rept1l.tensor import tensor_arg, tensor, diracAlgebra
from rept1l.currents.loopcurrent_functions import index_gen, feynman_propagator
from rept1l.currents.loopcurrent_functions import construct_arg
d = diracAlgebra

#############
#  Methods  #
#############

def build_epsilon_tensor(vb1_arg, vb2_arg, vb3_arg, vb4_arg, prop,
                         final_particle, free_ci):
  """ Builds \epsilon^{\mu_vb1_arg, \mu_vb2_arg, \mu_vb3_arg, \mu_vb4_arg} *
  wave functions * propagator.  Where propagator and/or wave functions are
  eventually 1. """
  # vbt: Vectorboson wavefunction (tensor)
  # vbt_arg: VB argument which is used for the epsilon structure
  vb1_t, vb1_t_arg, free_ci = construct_arg(vb1_arg, free_ci, final_particle,
                                            metric=d.g)
  vb2_t, vb2_t_arg, free_ci = construct_arg(vb2_arg, free_ci, final_particle,
                                            metric=d.g)
  vb3_t, vb3_t_arg, free_ci = construct_arg(vb3_arg, free_ci, final_particle,
                                            metric=d.g)
  vb4_t, vb4_t_arg, free_ci = construct_arg(vb4_arg, free_ci, final_particle,
                                            metric=d.g)

  epsilon_tensor = tensor([vb1_t_arg, vb2_t_arg, vb3_t_arg, vb4_t_arg],
                          numerical=d.epsilon, symbol=Symbol('Epsilon'))

  epsilon_tensor = epsilon_tensor * prop * vb1_t * vb2_t * vb3_t * vb4_t

  return epsilon_tensor

def EpsilonLoop(final_particle, dirac, vectorized, last_flag,
                additional, msyms, args, max_rank=0, pext=None, **kwargs):
  """ Build the ranked ordered current for \sigma^{\mu_vb1_arg,
  \mu_vb2_arg}_{af_arg, f_arg} """
  vb1_arg = args[0]
  vb2_arg = args[1]
  vb3_arg = args[2]
  vb4_arg = args[3]

  reserverd_ci = fold(lambda x,y: x+y, [msyms[key].contraction_indices() for key in msyms], [])
  free_ci = index_gen(not_allowed=args + reserverd_ci)

  inc_rank_prop = False
  if vb1_arg == final_particle:
    prop, vb1_arg, free_ci = feynman_propagator(free_ci)
  elif vb2_arg == final_particle:
    prop, vb2_arg, free_ci = feynman_propagator(free_ci)
  elif vb3_arg == final_particle:
    prop, vb3_arg, free_ci = feynman_propagator(free_ci)
  elif vb4_arg == final_particle:
    prop, vb4_arg, free_ci = feynman_propagator(free_ci)
  else:
    prop = 1

  # construct the wavefunctions (in presence of external indices)
  epsilon_tensor = build_epsilon_tensor(vb1_arg, vb2_arg, vb3_arg, vb4_arg,
                                        prop, final_particle, free_ci)

  if inc_rank_prop is False:
    for rank in range(max_rank + 1):
      keys = construct_tensor_keys(max_rank, rank)
      for key in keys:
        msyms[key] = epsilon_tensor * msyms[key]
  else:
    raise Exception('TODO')
    # merging the results

    #for key in lower.keys():
      #msyms[key] = lower[key]

    #for key in upper.keys():
      #msyms[key] = upper[key]

  return msyms, additional, vectorized, max_rank


