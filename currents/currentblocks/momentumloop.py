#==============================================================================#
#                                 gammaloop.py                                 #
#==============================================================================#

#============#
#  Includes  #
#============#

from sympy import Symbol
from rept1l.combinatorics import construct_tensor_keys, free_indices, fold
from rept1l.tensor import tensor_arg, tensor, diracAlgebra
from rept1l.currents.loopcurrent_functions import index_gen, feynman_propagator
from rept1l.currents.loopcurrent_functions import fermion_propagator
from rept1l.currents.loopcurrent_functions import construct_arg
from rept1l.currents.loopcurrent_functions import fermion_additionals
from rept1l.currents.loopcurrent_functions import vector_additionals
d = diracAlgebra

def build_p_tensor(l_arg, p_arg, prop, final_particle, free_ci):
  """ Builds ... """

  l_t, l_t_arg, free_ci = construct_arg(l_arg, free_ci, final_particle,
                                        metric=d.g, resolve=False)
  # build momentum
  if p_arg == final_particle:
    P_tensor = tensor([], symbol=0)
    for k in range(final_particle-1):
      P_symbol = Symbol('P' + str(k+1))
      P_tensor += tensor([l_t_arg], symbol=P_symbol)
  else:
    P_symbol = Symbol('P' + str(p_arg))
    P_tensor = tensor([l_t_arg], symbol=-1*P_symbol)

  PgW_tensor = P_tensor * l_t * prop

  return PgW_tensor

def build_q_tensor(l_arg, p_arg, prop, final_particle, free_ci, max_rank):
  """ Builds ... """

  v_arg = tensor_arg(free_indices[max_rank-1], domain=range(4),
                     resolve=False, metric=d.g)
  l_t, l_t_arg, free_ci = construct_arg(l_arg, free_ci, final_particle,
                                        metric=d.g, resolve=False)
  sign = 1 if p_arg == final_particle else -1
  g_tensor = sign * tensor([l_t_arg, v_arg], numerical=d.g,
                           symbol=Symbol('gg'), delta=True)
  g_tensor = g_tensor * l_t * prop
  return g_tensor

def MomentumLoop(final_particle, dirac, vectorized, last_flag,
                 additional, msyms, args_i_j, max_rank=0, pext=None, **kwargs):
  """@todo: Docstring for P.

  P(i,j) = P^(\mu_i)_j

  """
  current_rank = max_rank
  lorentz_arg = args_i_j[0]
  particle_arg = args_i_j[1]

  reserverd_ci = fold(lambda x,y: x+y, [msyms[key].contraction_indices() for key in msyms], [])
  free_ci = index_gen(not_allowed=[lorentz_arg, particle_arg] + reserverd_ci)

  if lorentz_arg == final_particle:
    prop, varg, free_ci = feynman_propagator(free_ci, pext=pext)
  else:
    prop = 1
    varg = lorentz_arg

  lower = {}
  for rank in range(max_rank + 1):
    keys = construct_tensor_keys(max_rank, rank)
    for key in keys:
      p_tensor = build_p_tensor(varg, particle_arg, prop, final_particle,
                                free_ci)
      lower[key] = p_tensor * msyms[key]

  # The upper dict is the consequence of a rank increase by 1
  upper = {}

  # The rank is increased either by the lorentz structures or by the propagator

  # The lorentz structures increase the rank if the momentum carries a loop
  # momentum component which is the case for the first and last wavefunction
  LS_rank_inc = particle_arg == final_particle or particle_arg == 1

  # The propagator of
  rxi_propagator = lorentz_arg == final_particle
  prop_rank_inc = pext is not None and rxi_propagator
  if LS_rank_inc or prop_rank_inc:
    max_rank += 1
    #max_rank = 4
    for rank in range(max_rank):
      new_keys = construct_tensor_keys(max_rank, rank)
      for key in new_keys:
        if key not in msyms.keys():
          msyms[key] = []

    if prop_rank_inc:

      vector_additionals(additional, final_particle, last_flag)
      propr1, vargrxi1, free_ci = feynman_propagator(free_ci, rank=max_rank,
                                                     rank_inc=1, pext=pext)
      propr2, vargrxi2, free_ci = feynman_propagator(free_ci, rank=max_rank,
                                                     rank_inc=2, pext=pext)

    # rank increase by one
    for rank in range(max_rank):
      old_keys = construct_tensor_keys(max_rank-1, rank)
      for key in old_keys:
        if (key == '0'):
          new_key = free_indices[max_rank - 1]
        else:
          new_key = key + free_indices[max_rank - 1]

        if LS_rank_inc:
          g_tensor = build_q_tensor(varg, particle_arg, prop,
                                    final_particle, free_ci, max_rank)

          # rank increase by momentum in propagator structure
          if prop_rank_inc:
            g_tensor += build_p_tensor(vargrxi1, particle_arg, propr1,
                                       final_particle, free_ci)
        else:
          g_tensor = build_p_tensor(vargrxi1, particle_arg, propr1,
                                    final_particle, free_ci)

        if msyms[key] == []:
          upper[new_key] = g_tensor
        else:
          upper[new_key] = msyms[key] * g_tensor

    if prop_rank_inc:
      # rank increase by two
      max_rank += 1
      for rank in range(max_rank):
        old_keys = construct_tensor_keys(max_rank-2, rank)
        for key in old_keys:
          if (key == '0'):
            new_key = ''.join(free_indices[max_rank - 2:max_rank])

          else:
            new_key = key + ''.join(free_indices[max_rank - 2:max_rank])

          g_tensor = build_p_tensor(vargrxi2, particle_arg, propr2,
                                    final_particle, free_ci)
          if LS_rank_inc:
            g_tensor += build_q_tensor(vargrxi1, particle_arg, propr1,
                                       final_particle, free_ci, max_rank)

          if msyms[key] == []:
            upper[new_key] = g_tensor
          else:
            upper[new_key] = msyms[key] * g_tensor

      # rank increase by three
      if LS_rank_inc:
        max_rank += 1
        for rank in range(max_rank):
          old_keys = construct_tensor_keys(max_rank-3, rank)
          for key in old_keys:
            if (key == '0'):
              new_key = ''.join(free_indices[max_rank - 3:max_rank])

            else:
              new_key = key + ''.join(free_indices[max_rank - 3:max_rank])
            g_tensor = build_q_tensor(vargrxi2, particle_arg, propr2,
                                      final_particle, free_ci, max_rank)

            if msyms[key] == []:
              upper[new_key] = g_tensor
            else:
              upper[new_key] = msyms[key] * g_tensor

  # merging the results
  for key in lower.keys():
    msyms[key] = lower[key]

  for key in upper.keys():
    msyms[key] = upper[key]

  return msyms, additional, vectorized, max_rank
