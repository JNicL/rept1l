#==============================================================================#
#                                 sigmatree.py                                 #
#==============================================================================#

#============#
#  Includes  #
#============#

from sympy import Symbol
from rept1l.tensor import tensor_arg, tensor, diracAlgebra, DeltaTensor
from rept1l.currents.treecurrent_functions import index_gen, feynman_propagator
from rept1l.currents.treecurrent_functions import fermion_propagator
from rept1l.currents.treecurrent_functions import construct_arg
from rept1l.currents.treecurrent_functions import fermion_additionals
d = diracAlgebra

#===========#
#  Methods  #
#===========#

def SigmaTree(final_particle, dirac, vectorized, last_flag, additional, msyms,
              args, last_contraction=False, pext=None, **kwargs):

  vb1_arg = args[0]
  vb2_arg = args[1]
  af_arg = args[2]
  f_arg = args[3]

  if vb1_arg == final_particle or vb2_arg == final_particle:
    last_case = "if (last) VB_last = .true."
    additional.append(last_case)

  if f_arg == final_particle or af_arg == final_particle:
    fermion_additionals(additional, final_particle, last_contraction)
    last_flag = True

  free_ci = index_gen(not_allowed=args + msyms.contraction_indices())

  prop = {vb1_arg: lambda x: feynman_propagator(x, pext=pext),
          vb2_arg: lambda x: feynman_propagator(x, pext=pext),
          f_arg: lambda x: fermion_propagator(x, fermion_out=True),
          af_arg: lambda x: fermion_propagator(x, fermion_out=False)}

  if final_particle in prop and not last_contraction:
    prop, final_i, free_ci = prop[final_particle](free_ci)
    if f_arg == final_particle:
      f_arg = final_i
    elif af_arg == final_particle:
      af_arg = final_i
    elif vb1_arg == final_particle:
      vb1_arg = final_i
    else:
      vb2_arg = final_i
  else:
    prop = 1

  vb1_t, vb1_t_arg, free_ci = construct_arg(vb1_arg, free_ci, final_particle,
                                            metric=d.g)
  vb2_t, vb2_t_arg, free_ci = construct_arg(vb2_arg, free_ci, final_particle,
                                            metric=d.g)
  f_t, f_t_arg, free_ci = construct_arg(f_arg, free_ci, final_particle)
  af_t, af_t_arg, free_ci = construct_arg(af_arg, free_ci, final_particle)

  sigma_tensor = tensor([vb1_t_arg, vb2_t_arg, af_t_arg, f_t_arg],
                        numerical=d.sigma, symbol=Symbol('Sigma'))

  sigma_tensor = sigma_tensor * prop * vb1_t * vb2_t * f_t * af_t
  msyms = sigma_tensor * msyms
  return msyms, additional, vectorized, last_flag
