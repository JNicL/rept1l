#==============================================================================#
#                                metricloop.py                                 #
#==============================================================================#

#===========#
#  Include  #
#===========#
from sympy import Symbol
from rept1l.combinatorics import construct_tensor_keys, free_indices, fold
from rept1l.tensor import tensor_arg, tensor, diracAlgebra
from rept1l.currents.loopcurrent_functions import index_gen, feynman_propagator
from rept1l.currents.loopcurrent_functions import fermion_propagator
from rept1l.currents.loopcurrent_functions import construct_arg
from rept1l.currents.loopcurrent_functions import fermion_additionals
from rept1l.currents.loopcurrent_functions import vector_additionals
d = diracAlgebra

#===========#
#  Methods  #
#===========#

def build_WgW_tensor(varg1, varg2, prop, final_particle, free_ci):
  vb1_t, vb1_t_arg, free_ci = construct_arg(varg1, free_ci, final_particle,
                                            metric=d.g, resolve=False)
  vb2_t, vb2_t_arg, free_ci = construct_arg(varg2, free_ci, final_particle,
                                            metric=d.g, resolve=False)

  metric_tensor = tensor([vb1_t_arg, vb2_t_arg], numerical=d.g,
                         symbol=Symbol('gg'), delta=True)

  WgW = metric_tensor * vb1_t * vb2_t * prop
  return WgW

def MetricLoop(final_particle, dirac, vectorized, last_flag,
               additional, msyms, args_i_j, max_rank=0, pext=None, **kwargs):

  arg1 = args_i_j[0]
  arg2 = args_i_j[1]

  reserverd_ci = fold(lambda x,y: x+y, [msyms[key].contraction_indices() for key in msyms], [])
  free_ci = index_gen(not_allowed=[arg1, arg2] + reserverd_ci)
  if arg1 == final_particle:
    prop, varg1, free_ci = feynman_propagator(free_ci, rank=0, pext=pext)
    varg2 = arg2
  elif arg2 == final_particle:
    prop, varg2, free_ci = feynman_propagator(free_ci, rank=0, pext=pext)
    varg1 = arg1
  else:
    prop = 1
    varg1 = arg1
    varg2 = arg2

  WgW = build_WgW_tensor(varg1, varg2, prop, final_particle, free_ci)

  propagator_req = (arg1 == final_particle) or (arg2 == final_particle)
  if pext is None or not propagator_req:
    for rank in range(max_rank + 1):
      keys = construct_tensor_keys(max_rank, rank)
      for key in keys:
        if key in msyms:
          msyms[key] = WgW * msyms[key]
  else:
    vector_additionals(additional, final_particle, last_flag)
    lower = dict()
    for rank in range(max_rank + 1):
      keys = construct_tensor_keys(max_rank, rank)
      for key in keys:
        lower[key] = WgW * msyms[key]

    # RXI propagator increases rank by 1
    upper = {}

    max_rank += 1
    if arg1 == final_particle:
      propr1, varg1rxi1, free_ci = feynman_propagator(free_ci, rank=max_rank,
                                                      rank_inc=1, pext=pext)
      varg2rxi1 = arg2
      propr2, varg1rxi2, free_ci = feynman_propagator(free_ci, rank=max_rank,
                                                      rank_inc=2, pext=pext)
      varg2rxi2 = arg2
    else:
      propr1, varg2rxi1, free_ci = feynman_propagator(free_ci, rank=max_rank,
                                                      rank_inc=1, pext=pext)
      varg1rxi1 = arg1
      propr2, varg2rxi2, free_ci = feynman_propagator(free_ci, rank=max_rank,
                                                      rank_inc=2, pext=pext)
      varg1rxi2 = arg1

    # first rank increase (e.g. mu)
    for rank in range(max_rank):
      new_keys = construct_tensor_keys(max_rank, rank)
      for key in new_keys:
        if key not in msyms.keys():
          msyms[key] = []

      old_keys = construct_tensor_keys(max_rank-1, rank)
      for key in old_keys:
        if (key == '0'):
          new_key = free_indices[max_rank - 1]
        else:
          new_key = key + free_indices[max_rank - 1]

        WgW = build_WgW_tensor(varg1rxi1, varg2rxi1, propr1,
                               final_particle, free_ci)
        if msyms[key] == []:
          upper[new_key] = WgW
        else:
          upper[new_key] = msyms[key] * WgW

    max_rank += 1
    # second rank increase (e.g. nu) + rank 2 increase (e.g. munu)
    for rank in range(max_rank):
      old_keys = construct_tensor_keys(max_rank-2, rank)
      for key in old_keys:
        if (key == '0'):
          new_key = ''.join(free_indices[max_rank - 2:max_rank])
        else:
          new_key = key + ''.join(free_indices[max_rank - 2:max_rank])

        WgW = build_WgW_tensor(varg1rxi2, varg2rxi2, propr2,
                               final_particle, free_ci)
        if msyms[key] == []:
          upper[new_key] = WgW
        else:
          upper[new_key] = msyms[key] * WgW

    # merging the results
    for key in lower.keys():
      msyms[key] = lower[key]

    for key in upper.keys():
      msyms[key] = upper[key]
  return msyms, additional, vectorized, max_rank
