#==============================================================================#
#                                metrictree.py                                 #
#==============================================================================#

#============#
#  Includes  #
#============#

from sympy import Symbol
from rept1l.tensor import tensor_arg, tensor, diracAlgebra, DeltaTensor
from rept1l.currents.treecurrent_functions import index_gen, feynman_propagator
from rept1l.currents.treecurrent_functions import fermion_propagator
from rept1l.currents.treecurrent_functions import construct_arg
from rept1l.currents.treecurrent_functions import fermion_additionals
from rept1l.currents.treecurrent_functions import vector_additionals
d = diracAlgebra

#===========#
#  Methods  #
#===========#

def MetricTree(final_particle, dirac, vectorized, last_flag,
               additional, msyms, args_i_j, last_contraction=False,
               pext=None, **kwargs):

  arg1 = args_i_j[0]
  arg2 = args_i_j[1]

  free_ci = index_gen(not_allowed=[arg1, arg2] + msyms.contraction_indices())

  prop = {arg1: feynman_propagator, arg2: feynman_propagator}

  if final_particle in prop and not last_contraction:
    last_case = "if (last) VB_last = .true."
    additional.append(last_case)
    if pext is not None:
      vector_additionals(additional, final_particle, last_contraction)
    prop, final_i, free_ci = prop[final_particle](free_ci, pext=pext)
    if arg1 == final_particle:
      arg1 = final_i
    else:
      arg2 = final_i
  else:
    prop = 1

  vb1_t, vb1_t_arg, free_ci = construct_arg(arg1, free_ci, final_particle,
                                            metric=d.g, resolve=False)
  vb2_t, vb2_t_arg, free_ci = construct_arg(arg2, free_ci, final_particle,
                                            metric=d.g, resolve=False)

  metric_tensor = DeltaTensor(vb1_t_arg, vb2_t_arg)
  metric_tensor = metric_tensor * prop * vb1_t * vb2_t
  msyms = metric_tensor * msyms

  return msyms, additional, vectorized, last_flag
