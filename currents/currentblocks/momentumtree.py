#==============================================================================#
#                               momentumtree.py                                #
#==============================================================================#

#============#
#  Includes  #
#============#

from sympy import Symbol

from rept1l.tensor import tensor_arg, tensor, diracAlgebra
from rept1l.currents.treecurrent_functions import index_gen, feynman_propagator
from rept1l.currents.treecurrent_functions import fermion_propagator
from rept1l.currents.treecurrent_functions import construct_arg
from rept1l.currents.treecurrent_functions import fermion_additionals
from rept1l.currents.treecurrent_functions import vector_additionals
d = diracAlgebra

#===========#
#  Methods  #
#===========#

def MomentumTree(final_particle, dirac, vectorized, last_flag,
                 additional, msyms, args_i_j, last_contraction=False,
                 pext=None, **kwargs):
  """ P(i,j) = P^(\mu_i)_j """

  lorentz_arg = args_i_j[0]
  particle_arg = args_i_j[1]

  if lorentz_arg == final_particle:
    last_case = "if (last) VB_last = .true."
    additional.append(last_case)
    if pext is not None:
      vector_additionals(additional, final_particle, last_contraction)

  free_ci = index_gen(not_allowed=[lorentz_arg, particle_arg] + msyms.contraction_indices())
  prop = {lorentz_arg: lambda x: feynman_propagator(x, pext=pext)}

  if final_particle in prop and not last_contraction:
    prop, final_i, free_ci = prop[final_particle](free_ci)
    lorentz_arg = final_i
  else:
    prop = 1

  l_t, l_t_arg, free_ci = construct_arg(lorentz_arg, free_ci, final_particle,
                                        metric=d.g, resolve=False)

  # build momentum
  if particle_arg == final_particle:
    P_tensor = tensor([], symbol=0)
    for k in range(final_particle-1):
      P_symbol = Symbol('P' + str(k+1))
      P_tensor += tensor([l_t_arg], symbol=P_symbol)
  else:
    P_symbol = Symbol('P' + str(particle_arg))
    P_tensor = -1*tensor([l_t_arg], symbol=P_symbol)

  PgW_tensor = P_tensor * l_t * prop
  msyms = PgW_tensor * msyms

  return msyms, additional, vectorized, last_flag
