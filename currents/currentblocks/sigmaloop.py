#===========#
#  Include  #
#===========#
from sympy import Symbol
from rept1l.combinatorics import construct_tensor_keys, free_indices, fold
from rept1l.tensor import tensor_arg, tensor, diracAlgebra
from rept1l.currents.loopcurrent_functions import index_gen, feynman_propagator
from rept1l.currents.loopcurrent_functions import fermion_prop, construct_arg
from rept1l.currents.loopcurrent_functions import fermion_additionals
d = diracAlgebra

#===========#
#  Methods  #
#===========#

def build_sigma_tensor(vb1_arg, vb2_arg, af_arg, f_arg, prop, final_particle,
                       free_ci):
  """ Builds \sigma^{\mu_vb1_arg, \mu_vb2_arg}_{af_arg, f_arg} * wave functions
  * propagator.  Where propagator and/or wave functions are eventually 1. """
  # vbt: Vectorboson wavefunction (tensor)
  # vbt_arg: VB argument which is used for the gamma structure
  vb1_t, vb1_t_arg, free_ci = construct_arg(vb1_arg, free_ci, final_particle,
                                            metric=d.g)
  vb2_t, vb2_t_arg, free_ci = construct_arg(vb2_arg, free_ci, final_particle,
                                            metric=d.g)

  # ft: Fermion wavefunction (tensor)
  # ft_arg: Fermionic argument which is used in the gamma structure
  f_t, f_t_arg, free_ci = construct_arg(f_arg, free_ci, final_particle)

  # aft: AntiFermion wavefunction (tensor)
  # ft_arg: AntiFermionic argument which is used in the gamma structure
  af_t, af_t_arg, free_ci = construct_arg(af_arg, free_ci, final_particle)
  sigma_tensor = tensor([vb1_t_arg, vb2_t_arg, af_t_arg, f_t_arg],
                        numerical=d.sigma, symbol=Symbol('Sigma'))

  sigma_tensor = sigma_tensor * prop * vb1_t * vb2_t * f_t * af_t

  return sigma_tensor

def SigmaLoop(final_particle, dirac, vectorized, last_flag,
              additional, msyms, args, max_rank=0, pext=None, **kwargs):
  """ Build the ranked ordered current for \sigma^{\mu_vb1_arg,
  \mu_vb2_arg}_{af_arg, f_arg} """
  vb1_arg = args[0]
  vb2_arg = args[1]
  af_arg = args[2]
  f_arg = args[3]

  reserverd_ci = fold(lambda x,y: x+y, [msyms[key].contraction_indices() for key in msyms], [])
  free_ci = index_gen(not_allowed=[vb1_arg, vb2_arg, af_arg, f_arg] + reserverd_ci)

  if vb1_arg == final_particle:
    prop, vb1_arg, free_ci = feynman_propagator(free_ci)
    inc_rank_prop = False
    af_arg_s = af_arg
    f_arg_s = f_arg
  elif vb2_arg == final_particle:
    prop, vb2_arg, free_ci = feynman_propagator(free_ci)
    inc_rank_prop = False
    af_arg_s = af_arg
    f_arg_s = f_arg
  elif af_arg == final_particle:
    fermion_additionals(additional, final_particle)
    max_rank += 1
    prop, af_arg_s, free_ci = fermion_prop(free_ci, free_indices[max_rank-1],
                                           fermion_out=False)
    inc_rank_prop = True
    f_arg_s = f_arg
  elif f_arg == final_particle:
    fermion_additionals(additional, final_particle)
    max_rank += 1
    prop, f_arg_s, free_ci = fermion_prop(free_ci, free_indices[max_rank-1],
                                          fermion_out=True)
    inc_rank_prop = True
    af_arg_s = af_arg
  else:
    prop = 1
    inc_rank_prop = False
    af_arg_s = af_arg
    f_arg_s = f_arg

  # construct the wavefunctions (in presence of external indices)

  # vbt: Vectorboson wavefunction (tensor)
  # vbt_arg: VB argument which is used for the gamma structure
  #vb1_t, vb1_t_arg, free_ci = construct_arg(vb1_arg, free_ci, final_particle,
                                            #metric=d.g)
  #vb2_t, vb2_t_arg, free_ci = construct_arg(vb2_arg, free_ci, final_particle,
                                            #metric=d.g)

  # ft: Fermion wavefunction (tensor)
  # ft_arg: Fermionic argument which is used in the gamma structure
  #f_t, f_t_arg, free_ci = construct_arg(f_arg_s, free_ci, final_particle)

  # aft: AntiFermion wavefunction (tensor)
  # ft_arg: AntiFermionic argument which is used in the gamma structure
  #af_t, af_t_arg, free_ci = construct_arg(af_arg_s, free_ci, final_particle)

  sigma_tensor = build_sigma_tensor(vb1_arg, vb2_arg, af_arg_s, f_arg_s,
                                    prop, final_particle, free_ci)

  if inc_rank_prop is False:
    for rank in range(max_rank + 1):
      keys = construct_tensor_keys(max_rank, rank)
      for key in keys:
        msyms[key] = sigma_tensor * msyms[key]
  else:
    lower = dict()
    for rank in range(max_rank):
      keys = construct_tensor_keys(max_rank-1, rank)
      for key in keys:
        lower[key] = sigma_tensor * msyms[key]

    upper = dict()
    for rank in range(max_rank + 1):
      new_keys = construct_tensor_keys(max_rank, rank)
      for key in new_keys:
        if key not in msyms.keys():
          msyms[key] = []

    if af_arg == final_particle:
      propr, af_arg_s, free_ci = fermion_prop(free_ci, free_indices[max_rank-1],
                                              fermion_out=True, loop=True)
    elif f_arg == final_particle:

      # get a fresh loop kpropagator \slash q
      propr, f_arg_s, free_ci = fermion_prop(free_ci, free_indices[max_rank-1],
                                             fermion_out=True, loop=True)

    sigma_tr = build_sigma_tensor(vb1_arg, vb2_arg, af_arg_s, f_arg_s,
                                  propr, final_particle, free_ci)

    for rank in range(max_rank):
      old_keys = construct_tensor_keys(max_rank - 1, rank)
      for key in old_keys:
        if (key == '0'):
          new_key = free_indices[max_rank - 1]
        else:
          new_key = key + free_indices[max_rank - 1]
        upper[new_key] = sigma_tr * msyms[key]

    # merging the results

    for key in lower.keys():
      msyms[key] = lower[key]

    for key in upper.keys():
      msyms[key] = upper[key]

  return msyms, additional, vectorized, max_rank
