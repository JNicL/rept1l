#==============================================================================#
#                                 gammatree.py                                 #
#==============================================================================#

#============#
#  Includes  #
#============#

from sympy import Symbol
from rept1l.tensor import tensor_arg, tensor, diracAlgebra
from rept1l.currents.treecurrent_functions import index_gen, feynman_propagator
from rept1l.currents.treecurrent_functions import fermion_propagator
from rept1l.currents.treecurrent_functions import construct_arg
from rept1l.currents.treecurrent_functions import fermion_additionals
from rept1l.currents.treecurrent_functions import vector_additionals
d = diracAlgebra

#===========#
#  Methods  #
#===========#

def GammaTree(final_particle, dirac, vectorized, last_flag,
              additional, msyms, args_i_j, last_contraction=False,
              null_plus_minus='', pext=None, **kwargs):

  vector_arg = args_i_j[0]
  fermion_arg = args_i_j[2]
  afermion_arg = args_i_j[1]

  if vector_arg == final_particle:
    last_case = "if (last) VB_last = .true."
    additional.append(last_case)
    if pext is not None:
      vector_additionals(additional, final_particle, last_contraction)

  if fermion_arg == final_particle or afermion_arg == final_particle:
    fermion_additionals(additional, final_particle, last_contraction)
    last_flag = True

  free_ci = index_gen(not_allowed=[vector_arg, fermion_arg, afermion_arg] +
                                  msyms.contraction_indices())

  prop = {vector_arg: lambda x: feynman_propagator(x, pext=pext),
          fermion_arg: lambda x: fermion_propagator(x, fermion_out=True),
          afermion_arg: lambda x: fermion_propagator(x, fermion_out=False)}

  if final_particle in prop and not last_contraction:
    prop, final_i, free_ci = prop[final_particle](free_ci)
    if fermion_arg == final_particle:
      fermion_arg = final_i
    elif afermion_arg == final_particle:
      afermion_arg = final_i
    else:
      vector_arg = final_i
  else:
    prop = 1

  vb_t, vb_t_arg, free_ci = construct_arg(vector_arg, free_ci, final_particle,
                                          metric=d.g)
  f_t, f_t_arg, free_ci = construct_arg(fermion_arg, free_ci, final_particle)
  af_t, af_t_arg, free_ci = construct_arg(afermion_arg, free_ci, final_particle)

  g = {'0': d.gamma, '+': d.gammaP, '-': d.gammaM}
  gamma_tensor = tensor([vb_t_arg, af_t_arg, f_t_arg],
                        numerical=g[null_plus_minus], symbol=Symbol('Gamma'))

  gamma_tensor = gamma_tensor * prop * vb_t * f_t * af_t

  msyms = gamma_tensor * msyms

  return msyms, additional, vectorized, last_flag
