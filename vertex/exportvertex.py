#==============================================================================#
#                               exportvertex.py                                #
#==============================================================================#

#============#
#  Includes  #
#============#

import sys
import os
from sympy import Symbol, I

from rept1l.logging_setup import log
from rept1l.coupling import RCoupling as RC
from rept1l.helper_lib import export_fortran
from rept1l.pyfort import modBlock, subBlock, containsBlock, ProgressBar

from .vertex import Vertex

import rept1l_config

#==============================================================================#
#                                fill_couplings                                #
#==============================================================================#

def fill_couplings(couplings, class_couplings, class_ctcouplings,
                   couplings_info, ctcouplings_info, compute_ct=False,
                   compute_r2=False):
  """ Generates for fortran code for initializing the coupling values """

  # helper function
  def gen_couplings_mod(id, ct=False):
    """ Generates a new fortran module for initializing a certain number of
    couplings """
    if ct:
      mod = modBlock('fill_ctcouplings' + str(id))
    else:
      mod = modBlock('fill_couplings' + str(id))

    mod.addPrefix('use constants_mdl')
    mod.addPrefix('use class_couplings')
    mod.addPrefix('use class_ctparameters')
    mod.addPrefix('implicit none')

    contains_mod = containsBlock(tabs=1)
    if ct:
      sub = subBlock('fill_CTcouplings_mdl' + str(id), tabs=1)
    else:
      sub = subBlock('fill_couplings_mdl' + str(id), tabs=1)
    mod + contains_mod
    contains_mod + sub
    return mod, sub

  # helper function
  def gen_init_couplings_mod():
    """ Generates a fortran module with the subroutine which, when called, calls
    all other subroutines, thus, initializing all couplings. """
    mod = modBlock('fill_couplings')
    mod.addPrefix('implicit none')
    contains_mod = containsBlock(tabs=1)
    sub = subBlock('fill_couplings_mdl', tabs=1)
    sub.addPrefix("use class_couplings, only: couplings_init")
    subct = subBlock('fill_CTcouplings_mdl', tabs=1)
    subct.addPrefix("use class_couplings, only: ctcouplings_init")
    sub + 'couplings_init = .true.'
    subct + 'ctcouplings_init = .true.'
    mod + contains_mod
    contains_mod + sub
    contains_mod + subct
    return mod, sub, subct

  # by default 200 couplings per fortran module are exported
  if(hasattr(rept1l_config, 'couplings_per_mod') and
     rept1l_config.couplings_per_mod > 0):
    max_couplings_per_mod = rept1l_config.couplings_per_mod
  else:
    max_couplings_per_mod = 200

  # by default 50 ct couplings per fortran module are exported
  if(hasattr(rept1l_config, 'ctcouplings_per_mod') and
     rept1l_config.ctcouplings_per_mod > 0):
    max_ctcouplings_per_mod = rept1l_config.ctcouplings_per_mod
  else:
    max_ctcouplings_per_mod = 50

  n_c = 0
  n_ct = 0
  class_couplings.addType(('complex(dp)',
                           'coupl(' + str(len(couplings)) + ')'))
  for c_id, c in enumerate(couplings):
    # register new id to the coupling c
    c_id_name = 'n' + c

    flags = RC.couplings_regged[c]
    c_val = RC.coupling_values[c].subs({I: Symbol('cima')})

    # transform coupling expressions to coupl(coupling position)
    coupl_repl = {u: Symbol('coupl(n' + u.name + ')')
                  for u in RC.get_couplings_from_expr(c_val)}
    c_val = c_val.subs(coupl_repl)

    # Tree couplings (and `bare loop` couplings ..)
    if 'Tree' in flags:
      n_c += 1
      # Declaration in class_couplings.f90.
      class_couplings.addType(("integer, parameter",
                               c_id_name + ' = ' + str(c_id+1)))
      # Select the module
      nmod = (n_c // max_couplings_per_mod) + 1
      # If non-exisitent generate a new module
      if nmod not in couplings_info:
        mod, sub = gen_couplings_mod(nmod, ct=False)
        name = 'fill_couplings' + str(nmod)
        moddict = {'mod': mod, 'sub': sub, 'name': name}
        couplings_info[nmod] = moddict
        if nmod == 1 and 0 not in couplings_info:
          mod, sub, subct = gen_init_couplings_mod()
          moddict = {'mod': mod, 'sub': sub, 'subct': subct,
                     'name': 'fill_couplings'}
          couplings_info[0] = moddict

        funcmod = ('use fill_couplings' + str(nmod) + ', only:' +
                   ' fill_couplings_mdl' + str(nmod))
        couplings_info[0]['sub'].addPrefix(funcmod)
        couplings_info[0]['sub'] + ('call fill_couplings_mdl' + str(nmod))

      sub_init_couplings = couplings_info[nmod]['sub']
      # Value initialized in init_couplings?.f90.
      c_array = 'coupl(' + c_id_name + ')'
      sub_init_couplings.addStatement(c_array + ' = ' +
                                      export_fortran(str(c_val)),
                                      treat_as_eq=True)

    # Counterterm couplings
    elif ('CT' in flags and compute_ct) or ('R2' in flags and compute_r2):
      n_ct += 1
      # Declaration in class_couplings.f90.
      class_couplings.addType(("integer, parameter",
                               c_id_name + ' = ' + str(c_id+1)))
      # Select the module
      nmod = (n_ct // max_ctcouplings_per_mod) + 1
      # If non-exisitent generate a new module
      if nmod not in ctcouplings_info:
        mod, sub = gen_couplings_mod(nmod, ct=True)
        name = 'fill_ctcouplings' + str(nmod)
        moddict = {'mod': mod, 'sub': sub, 'name': name}
        ctcouplings_info[nmod] = moddict
        if nmod == 1 and 0 not in couplings_info:
          mod, sub, subct = gen_init_couplings_mod()
          moddict = {'mod': mod, 'sub': sub, 'subct': subct,
                     'name': 'fill_couplings'}
          couplings_info[0] = moddict
        couplings_info[0]['subct'].addPrefix('use fill_ctcouplings' + str(nmod))
        couplings_info[0]['subct'] + ('call fill_CTcouplings_mdl' + str(nmod))

      # Value initialized in init_ctcouplings?.f90.
      sub_init_ctcouplings = ctcouplings_info[nmod]['sub']
      c_array = 'coupl(' + c_id_name + ')'
      sub_init_ctcouplings.addStatement(c_array + ' = ' +
                                        export_fortran(str(c_val)),
                                        treat_as_eq=True)
    else:
      log('Coupling ' + c.name + ' not associated to any branch:' + str(flags),
          'error')
      sys.exit()

#==============================================================================#
#                                fill_vertices                                 #
#==============================================================================#

def fill_vertices(vertices_info, clear_cache=False, **kwargs):
  """ Generates the fortran code which declares and fills all vertices. """
  ntot = len(Vertex._registry)
  PG = ProgressBar(ntot)

  if vertices_info is None:
    generate_fortran_code = False
  else:
    generate_fortran_code = True

  # by default 800 vertices per fortran module are exported
  if(hasattr(rept1l_config, 'vertices_per_mod') and
     rept1l_config.vertices_per_mod > 0):
    max_vertices_per_module = rept1l_config.vertices_per_mod
  else:
    max_vertices_per_module = 800

  # helper function
  def gen_vertex_mod(id):
    """ Module for filling vertex information """
    from rept1l.pyfort import modBlock, subBlock, containsBlock
    mod = modBlock('vertices_info' + str(id))
    mod.addPrefix('implicit none')

    contains_mod = containsBlock(tabs=1)
    sub = subBlock('fill_vertices_mdl' + str(id), tabs=1)
    sub.addPrefix('use class_vertices')
    sub.addPrefix('use class_couplings')
    mod + contains_mod
    contains_mod + sub
    return mod, sub

  # helper function
  def gen_fvertices_mod():
    """ Module declaring vertices """
    from rept1l.pyfort import modBlock, subBlock, containsBlock, loadBlock
    mod = modBlock('fill_vertices')
    mod.addPrefix('implicit none')
    contains_mod = containsBlock(tabs=1)
    mod + contains_mod

    # fillVertices subroutine
    sub = subBlock('fill_vertices_mdl', tabs=1)
    contains_mod + sub

    # build rept1l path
    r_path = os.path.abspath(os.path.join(os.path.
                                          dirname(os.path.realpath(__file__)),
                                          os.pardir))
    f_path = os.path.join(r_path, 'vertex/FortranTemplates')

    # clearVertices subroutine
    clearVertices = loadBlock(os.path.join(f_path, 'clear_vertices.f90'))
    mod + clearVertices

    # resetVertices subroutine
    resetVertices = loadBlock(os.path.join(f_path, 'reset_vertices.f90'))
    mod + resetVertices

    return mod, sub

  for n, vert in enumerate(Vertex):
    PG.advanceAndPlot(status=str(n+1) + '/' + str(ntot))
    if generate_fortran_code:
      nmod = (n // max_vertices_per_module) + 1
      if nmod not in vertices_info:
        mod, sub = gen_vertex_mod(nmod)
        name = 'vertices_info' + str(nmod)
        moddict = {'mod': mod, 'sub': sub, 'name': name}
        vertices_info[nmod] = moddict
        if nmod == 1:
          mod, sub = gen_fvertices_mod()
          moddict = {'mod': mod, 'sub': sub, 'name': 'fill_vertices'}
          vertices_info[0] = moddict
          sub.addPrefix('use class_vertices, only: vertices_filled')

        vertmod = ('use vertices_info' + str(nmod) + ', only:' +
                   ' fill_vertices_mdl' + str(nmod))
        vertices_info[0]['sub'].addPrefix(vertmod)
        vertices_info[0]['sub'] + ('call fill_vertices_mdl' + str(nmod))

      sub_fillVertices = vertices_info[nmod]['sub']
    else:
      sub_fillVertices = None
    vert.fill_vertex(sub_fillVertices, clear_cache=clear_cache, **kwargs)
    clear_cache = False

  if generate_fortran_code:
    vertices_info[0]['sub'] + 'vertices_filled = .true.'

#==============================================================================#
#                                fill_currents                                 #
#==============================================================================#

def fill_currents(currents_info, clear_cache=False, **kwargs):
  """ Generates the fortran code which declares and fills all vertices. """
  ntot = len(Vertex.vertex_ids)
  PG = ProgressBar(ntot)

  from rept1l import Model as Model
  model_objects = Model.model.object_library
  particle_names = [particle.name for particle in model_objects.all_particles]

  from rept1l.helper_lib import export_particle_name

  # by default 2000 currents per fortran module are exported
  if(hasattr(rept1l_config, 'currents_per_mod') and
     rept1l_config.currents_per_mod > 0):
    max_currents_per_module = rept1l_config.currents_per_mod
  else:
    max_currents_per_module = 2000

  # helper function
  def gen_current_mod(id):
    """ Module for filling current information """
    from rept1l.pyfort import modBlock, subBlock, containsBlock
    mod = modBlock('currents_info' + str(id))
    mod.addPrefix('implicit none')

    contains_mod = containsBlock(tabs=1)
    sub = subBlock('fill_current_' + str(id), arg='check', tabs=1)
    sub.addPrefix('use class_particles')
    sub.addPrefix('use current_keys, only: createCurrent')
    sub.addType(('logical, intent(in)', 'check'))
    mod + contains_mod
    contains_mod + sub
    return mod, sub

  # helper function
  def gen_fcurrents_mod():
    """ Module declaring vertices """
    from rept1l.pyfort import modBlock, subBlock, containsBlock, loadBlock
    mod = modBlock('fill_currents')
    contains_mod = containsBlock(tabs=1)
    mod + contains_mod

    sub = subBlock('fill_currents_mdl', arg='check', tabs=1)
    contains_mod + sub

    return mod, sub


  def create_current_entry(pkey, v_id, func):
    particles_indices = [particle_names.index(u) + 1 for u in pkey]
    renamed_particles = ['P_' + export_particle_name(u) for u in pkey]
    hash_code = ', '.join(renamed_particles + [str(v_id)] + ['check'])
    func + ('call createCurrent(' + hash_code + ')')

    return func

  for n, pkey in enumerate(Vertex.vertex_ids):
    PG.advanceAndPlot(status=str(n+1) + '/' + str(ntot))

    nmod = (n // max_currents_per_module) + 1
    if nmod not in currents_info:
      mod, sub = gen_current_mod(nmod)
      name = 'currents_info' + str(nmod)
      moddict = {'mod': mod, 'sub': sub, 'name': name}
      currents_info[nmod] = moddict
      if nmod == 1:
        mod, sub = gen_fcurrents_mod()
        moddict = {'mod': mod, 'sub': sub, 'name': 'fill_currents'}
        mod.addPrefix('implicit none')
        currents_info[0] = moddict

      currmod = ('use currents_info' + str(nmod) + ', only:' +
                 ' fill_current_' + str(nmod))
      currents_info[0]['sub'].addPrefix(currmod)
      currents_info[0]['sub'] + ('call fill_current_' + str(nmod) + '(check)')

      sub_fillCurrent = currents_info[nmod]['sub']


    vertex_id = Vertex.vertex_ids[pkey]
    create_current_entry(pkey, vertex_id, sub_fillCurrent)
  currents_info[0]['sub'].addType(('logical, intent(in)', 'check'))
