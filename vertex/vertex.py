#==============================================================================#
#                                  vertex.py                                   #
#==============================================================================#

from __future__ import print_function

#============#
#  Includes  #
#============#

import sys
from sympy import re as real
from sympy import im as imag
from sympy import Symbol, Poly, I, sympify

from rept1l.helper_lib import Storage, is_anti_particle, spin_to_lorentz
from rept1l.renormalize import Renormalize
from rept1l.coupling import RCoupling as RC
from rept1l.baseutils import insert_ls, BaseMismatch
from rept1l.pyfort import ProgressBar

from rept1l.counterterms import derive_wavefunction_ct
from rept1l.logging_setup import log
from rept1l.counterterms import Counterterms

import rept1l.Model as Model
model = Model.model
model_objects = model.object_library


class Vertex(Storage):

  # StorageMeta attributes. Those attributes are stored when `store` is called.
  storage = ['lorentzTree_dict', 'lorentzR2_dict', 'lorentzCT_dict',
             'treevertices', 'r2vertices', 'ctvertices']
  # StorageMeta storage path
  mpath, _ = model.get_modelpath()
  path = mpath + '/ModelVertices'
  # StorageMeta filename
  name = 'vertices_data.txt'
  _registry = []

  currentIDs = {}

  # current_types is a dictionay with the keys 'Tree', 'CT' or 'R2'. The
  # system does not distinguish between Tree and Loop currents in the sense that
  # if there is a Tree current, the corresponding Loop current is computed as
  # well. current_types[...] is a list containing current ids which should be
  # computed.
  current_types = {}

  # current_strucs is a dictonary with
  # TODO
  current_strucs = {}

  n_lorentz = 0
  vertex_ids = {}

  # simple_output is the test to export the current rules via txt data, not yet
  # functional
  simple_output = {}

  # collect different colorcoefficients and assign an id
  colorcoeffs = {}
  # colorcoefficients as form expressions (as string)
  colorcoeffs_form = []

  N = 0
  Tree = 'Tree'
  CT = 'CT'
  R2 = 'R2'

  branch_tags = {N: '', Tree: '', CT: 'CT', R2: 'R2'}
  coupling_orders = {}
  orders = [u.name for u in model_objects.all_orders]
  for coupling in model_objects.all_couplings:
    order = []
    for o in orders:
      if o in coupling.order:
        order.append(coupling.order[o])
      else:
        order.append(0)
    coupling_orders[Symbol(coupling.name)] = tuple(order)

  def __init__(self, branch_ids, particles):
    super(Vertex, self).__init__()
    # append the new instance to the registry
    self._registry.append(self)
    self.branches = {}

    # determine if a vertex with the same particle configuration has already
    # been inserted. The vertex Id is recovered if so.
    self.particles = tuple(particles)
    pkey = tuple([u.name for u in self.particles])
    if pkey in Vertex.vertex_ids:
      self.vertex_id = Vertex.vertex_ids[pkey]
    else:
      self.vertex_id = len(Vertex.vertex_ids)
      Vertex.vertex_ids[pkey] = self.vertex_id

    self.couplings_lorentz_ordered = {}
    for branch_id in branch_ids:
      self.branches[branch_id] = {}

  @staticmethod
  def field_origin_BFM(particle):
    if((not hasattr(particle, 'backgroundfield')) and
       (not hasattr(particle, 'quantumfield'))):
      bg = True
      q = True
    else:
      if hasattr(particle, 'backgroundfield'):
        bg = particle.backgroundfield
      else:
        bg = False

      if hasattr(particle, 'quantumfield'):
        q = particle.quantumfield
      else:
        q = False

    return bg, q

  @classmethod
  def valid_vertex_BFM(cls, vertex_particles):
    """ Checks if a vertex has to be taken along with respect to background and
    quantum fields. The assumptions are made in view of one-loop amplitudes and
    the RECOLA algorithm. """

    # the fields 2 to N-1 are never in the loop, thus they must be
    # backgroundfields, and cannot be quantumfields only.
    tree_particles = all(cls.field_origin_BFM(u)[0]
                         for u in vertex_particles[1:-1])
    if not tree_particles:
      return False

    # The first and the last (N) field can be in the loop
    first_bg, first_q = cls.field_origin_BFM(vertex_particles[0])
    last_bg, last_q = cls.field_origin_BFM(vertex_particles[-1])

    tree_and_loop = False
    loop_only = False
    tree_only = False

    # First field is a backgroundfield only
    if first_bg and not first_q:
      if not last_bg:  # last field must be a backgroundfield
        return False
      else:
        tree_only = True
    # First field is a quantumfield only
    elif not first_bg and first_q:
      if not last_q:  # last field must be a quantumfield
        return False
      else:
        loop_only = True
    else:
      # the first field is both, quantum and background field
      assert(first_bg and first_q)
      # last field is both, quantum and background field
      if last_q and last_bg:
        tree_and_loop = True
      elif last_q:
        loop_only = True
      elif last_bg:
        tree_only = True
      else:
        assert(False)

    return {'tree_and_loop': tree_and_loop, 'tree_only': tree_only,
            'loop_only': loop_only}

  @classmethod
  def any_r2vertices(cls):
    """ checks if the storage contains any rational terms. """
    if cls.loaded:
      return len(cls.lorentzR2_dict) > 0
    else:
      cls.load()
      cls.loaded = False
      ret = len(cls.lorentzR2_dict) > 0
      cls.init_storage_dict()
      return ret

  @classmethod
  def subs_reno_constants(self):
    """ Substitutes the renormalization constants for their solution in the
    lorentzCT_dict. The renormalization constants are stored in
    Renormalize.renos.

    >>> g = Symbol('g'); gs = Symbol('gs'); cs = (1, 1); dZ = Symbol('dZ')
    >>> DeltaUV = Symbol('DeltaUV')
    >>> ls = (Symbol('g')*Symbol('P'), (1, 1))
    >>> Vertex.lorentzCT_dict = {(g, g, g): {cs: {ls: gs*dZ}}}
    >>> Renormalize.renos = {dZ: gs**2 * DeltaUV }
    >>> Vertex.subs_reno_constants()
    >>> Vertex.lorentzCT_dict
    {(g, g, g): {(1, 1): {(P*g, (1, 1)): DeltaUV(0)*gs**3}}}
    """
    for p_key in self.lorentzCT_dict:
      for co_key in self.lorentzCT_dict[p_key]:
        for ls_key in self.lorentzCT_dict[p_key][co_key]:
          subs = (self.lorentzCT_dict[p_key][co_key][ls_key].
                  subs(Renormalize.renos))
          self.lorentzCT_dict[p_key][co_key][ls_key] = subs

    Symbol('DeltaUV').name = 'DeltaUV(0)'

  def assign_lorentz(self, branch_id, lorentz_couplings_dict, flag=0):
    """ Assigns lorentz and couplings to a given branch_id and removes
    structures which have a vanishing coupling.

    >>> p_key = [model.P.W__minus__, model.P.W__plus__, model.P.A]
    >>> branch_ids = [(1,1)]
    >>> new_vertex = Vertex(branch_ids, p_key)
    >>> lcd = {((4*Symbol('g')*Symbol('g'), (1, 1)),): Symbol('GC')}
    >>> new_vertex.assign_lorentz((1, 1), lcd)
    >>> new_vertex.branches
    {(1, 1): {((4*g**2, (1, 1)),): GC}}
    """
    assert(branch_id in self.branches)
    self.flag = flag
    assert(flag in Vertex.branch_tags)
    self.branch_tag = Vertex.branch_tags[flag]

    for ls in lorentz_couplings_dict:
      # only add a new structure if non-zero
      if ls != ():
        coupling = lorentz_couplings_dict[ls]
        if coupling != 0:
          self.branches[branch_id][ls] = coupling

    # remove the vertex if all structures are zero
    if self.branches[branch_id] == {}:
      del self.branches[branch_id]

  def get_coupling_order(self, coupling):
    """
    :param coupling: Sympified coupling. Must be in all_couplings.
    :type  coupling: Sympy Symbol

    return: {coupling1: order1, coupling2: order2, ...}
            such that coupling1 + coupling 2 + ... = coupling
            order = [i, j, k, ...] where i,j,k integers
            In the SM [ QCD order, QED order] see UFO coupling_orders.py

    >>> p_key = [model.P.W__minus__, model.P.W__plus__, model.P.A]
    >>> branch_ids = [(1,1)]
    >>> new_vertex = Vertex(branch_ids, p_key)
    >>> G = Symbol('G'); ee = Symbol('ee')
    >>> RC.loaded = True
    >>> RC._coupling_values = {}
    >>> RC._all_couplings = []
    >>> RC.couplings_regged = {}
    >>> RC.couplings_in = {}
    >>> c1 = 16*G**2*ee; c2 = 8*G*ee**2; c3 = 8*G**2*ee
    >>> RC(c1).return_coupling()
    GC_0
    >>> RC(c2).return_coupling()
    GC_1
    >>> RC(c3).return_coupling()
    GC_2
    >>> new_vertex.get_coupling_order(Symbol('GC_0') + Symbol('GC_1') +
    ...                               Symbol('GC_2'))
    {GC_1: {'QED': 2, 'QCD': 1}, GC_0 + GC_2: {'QED': 1, 'QCD': 2}}
    """
    if coupling.is_Symbol and coupling.name in RC.coupling_orders:
      return {coupling: RC.coupling_orders[coupling.name]}

    orders = {}
    # the coupling is represented as polynomial allowing to separate additively
    # connected couplings.
    cdep = RC.get_couplings_from_expr(coupling)
    poly = Poly(coupling, cdep).as_dict()
    for key in poly:
      prefactor = poly[key]
      # for every multiplicatively connected coupling we compute the order in
      # the coupling constants
      order = {u: 0 for u in RC.orders}
      for pos, c in enumerate(key):
        if c != 0:
          c_order = RC.coupling_orders[cdep[pos].name]
          c_order = {u: c_order[u] * c for u in c_order}
          order = {u: order[u] + c_order[u] for u in order if u in c_order}
      # Couplings with the same order are merged meaning that they are added at
      # the end. If the order does not match they are separated
      order_t = tuple((u, order[u]) for u in order)
      if order_t in orders:
        orders[order_t].update({key: prefactor})
      else:
        orders[order_t] = {key: prefactor}
    # The expression for the couplings are reconstructed. Due to the separation
    # in orders, the returning dict might contain more than one coupling.
    return {Poly(orders[order_t], cdep).
            as_expr(): {u[0]: u[1] for u in order_t} for order_t in orders}

  def build_lorentz_tag(self, particles):
    pIsAntiParticle = [is_anti_particle(u.name) for u in particles]
    spins = [u.spin for u in particles]
    lb = spin_to_lorentz(spins, True)
    ret = ''
    for i in range(len(lb)):
      if ((pIsAntiParticle[i] is True) and (spins[i] == 2 or spins[i] == -1)):
        ret += lb[i] + 'a'
      else:
        ret += lb[i]
    return ret

  def derive_lorentz_base(self, branch_ids, **kwargs):
    """ Derives the lorentz structure base by comparing the lorentz structures
    (ls) of all the vertices. Similar structures, i.e. the same up to couplings,
    are defined as a base element.

    """
    self.lorentz_ids = dict()
    self.field_permutation = {}
    branches = branch_ids if branch_ids is not None else self.branches
    for co_key in branches:
      lc_ordered = {}
      for ls in branches[co_key]:
        couplings = branches[co_key][ls]
        if couplings.is_Add:
          assert(sum(couplings.args) == couplings)
          couplings = couplings.args
        else:
          couplings = [couplings]

        for c in couplings:
          r = RC(c, register=False, check_coupling=False,
                 compute_order=True, subs_cvalues=False).return_coupling()
          if r.is_Add:
            for cp in r.free_symbols:
              order = RC.coupling_orders[cp.name]
              order_h = tuple([(order_type, order[order_type])
                               if order_type in order else
                               (order_type, 0) for order_type in RC.orders])
              if order_h not in lc_ordered:
                lc_ordered[order_h] = {}
              if ls not in lc_ordered[order_h]:
                lc_ordered[order_h][ls] = 0
              lc_ordered[order_h][ls] += RC.coupling_values[cp.name]
          else:
            order = RC.coupling_orders[r.name]
            order_h = tuple([(order_type, order[order_type])
                             if order_type in order else
                             (order_type, 0) for order_type in RC.orders])
            if order_h not in lc_ordered:
              lc_ordered[order_h] = {}
            if ls not in lc_ordered[order_h]:
              lc_ordered[order_h][ls] = 0
            lc_ordered[order_h][ls] += RC.coupling_values[r.name]

      for order in lc_ordered:
        lco = lc_ordered[order].keys()
        # making sure each ls has one unique coupling
        for ls in lco:
          r = RC(lc_ordered[order][ls], self.flag,
                 register=True, check_coupling=True,
                 compute_order=True, subs_cvalues=False).return_coupling()
          lc_ordered[order][ls] = r
        lorentz_couplings = list(lc_ordered[order].values())

        # check if lco is equivalent up to a (default) permutation
        from rept1l.baseutils import CurrentBase
        dp = CurrentBase.get_default_permutation(lco)
        if not dp:
          lco_n = lco
          rel_p = None
          ini_p = None
          ref_p = None
        else:
          log('Found default permutation.', 'debug')
          lco_n, ini_p, ref_p, rel_p = dp
          log('Replaced: ' + str(lco), 'debug')
          log('With: ' + str(lco_n), 'debug')
          log('rel_p: ' + str(rel_p), 'debug')

          # from rept1l.baseutils.base_utils import lbs_to_form_expressions
          # for pos, lbs in enumerate(lco):
          #   ssum = lbs_to_form_expressions(lbs)
          #   print("pos:", pos)
          #   print("ssum:", ssum)
          # for pos, lbs in enumerate(lco_n):
          #   ssum = lbs_to_form_expressions(lbs)
          #   print("pos:", pos)
          #   print("ssum:", ssum)

        try:
          (Vertex.current_strucs, lorentz_id,
           couplings_and_signs, Vertex.n_lorentz) = (insert_ls(
                                                     Vertex.current_strucs,
                                                     lco_n,
                                                     Vertex.n_lorentz,
                                                     **kwargs))

        except BaseMismatch as e:
          print('Failed insert_ls:')
          from rept1l.baseutils.base_utils import lbs_to_form_expressions
          for pos, lbs in enumerate(lco_n):
            ssum = lbs_to_form_expressions(lbs)
            print("pos:", pos)
            print("ssum:", ssum)
          print("lorentz_coupling_ordered:", lco_n)
          print("ls_only_base:", e.ls_only_base)
          print("global_ls_only_base:", e.global_ls_only_base)
          print("global_ls:", e.global_ls)
          print("ls_global_key:", e.ls_global_key)
          raise

        if couplings_and_signs == []:
          print("co_key:", co_key)
          print("lorentz_coupling_ordered:", lco_n)
          print("self.branches[co_key]:", self.branches[co_key])
          print("couplings_and_signs:", couplings_and_signs)
          import sys
          sys.exit()

        # if relative permutation -> need to change ls label
        if rel_p:
          from rept1l.combinatorics import permute_elements
          new_rel_p = [0] * len(rel_p)
          for i in range(len(rel_p)):
            new_rel_p[ref_p.index(i)] = ini_p.index(i)

          pparticles = permute_elements(self.particles, rel_p)
          lbfr = self.build_lorentz_tag(pparticles)
        else:
          lbfr = self.build_lorentz_tag(self.particles)
          pparticles = None
          new_rel_p = None

        if (lbfr not in Vertex.currentIDs.keys()):
          Vertex.currentIDs[lbfr] = []

        if self.flag not in Vertex.current_types:
          Vertex.current_types[self.flag] = {}

        if lbfr not in Vertex.current_types[self.flag]:
          Vertex.current_types[self.flag][lbfr] = []

        ls_id = [u[1] for u in lorentz_id]
        if ls_id not in Vertex.current_types[self.flag][lbfr]:
          Vertex.current_types[self.flag][lbfr].append(ls_id)

        # check if structure was known before, if not then needs to be computed
        if (not ([u[1] for u in lorentz_id] in Vertex.currentIDs[lbfr])):
          tmp = Vertex.currentIDs[lbfr]
          tmp.append([u[1] for u in lorentz_id])
          Vertex.currentIDs[lbfr] = tmp

        # construct the full lorentz id
        l_id = (lbfr + str(Vertex.currentIDs[lbfr].
                index([u[1] for u in lorentz_id])))

        couplings_tmp = []
        for cc in couplings_and_signs:
          tmp = 0
          for i in cc:
            if cc[i] != 0:
              tmp += cc[i] * lorentz_couplings[i]
          # do not insert the values for couplings in the case of counterterms
          # -> too large expressions

          if tmp != 0:
            if self.flag != '':
              tmpr = RC(tmp, self.flag, subs_cvalues=False).return_coupling()
            else:
              tmpr = RC(tmp, self.flag).return_coupling()
          else:
            tmpr = sympify(tmp)

          couplings_tmp.append(tmpr)

        if co_key not in self.couplings_lorentz_ordered:
          self.couplings_lorentz_ordered[co_key] = {}
        if order not in self.couplings_lorentz_ordered[co_key]:
          self.couplings_lorentz_ordered[co_key][order] = {}
        self.couplings_lorentz_ordered[co_key][order] = couplings_tmp

        if co_key not in self.lorentz_ids:
          self.lorentz_ids[co_key] = {}
        if order not in self.lorentz_ids[co_key]:
          self.lorentz_ids[co_key][order] = {}
        self.lorentz_ids[co_key][order] = l_id

        if rel_p:
          self.field_permutation[order] = self.get_permutation_index(rel_p)
        else:
          self.field_permutation[order] = 0

  def get_permutation_index(self, perm):
    if perm == [0, 1, 2, 3]:
      return 0
    elif perm == [0, 2, 1, 3]:
      return 1

    elif perm == [0, 1, 2, 3, 4]:
      return 0
    elif perm == [0, 2, 1, 3, 4]:
      return 1
    elif perm == [0, 1, 3, 2, 4]:
      return 2
    elif perm == [0, 3, 2, 1, 4]:
      return 3
    elif perm == [0, 3, 1, 2, 4]:
      return 4
    elif perm == [0, 2, 3, 1, 4]:
      return 5

    elif perm == [0, 1, 2, 3, 4, 5]:
      return 0
    elif perm == [0, 2, 1, 3, 4, 5]:
      return 1
    elif perm == [0, 1, 3, 2, 4, 5]:
      return 2
    elif perm == [0, 3, 2, 1, 4, 5]:
      return 3
    elif perm == [0, 3, 1, 2, 4, 5]:
      return 4
    elif perm == [0, 2, 3, 1, 4, 5]:
      return 5

    elif perm == [0, 1, 2, 4, 3, 5]:
      return 6
    elif perm == [0, 1, 3, 4, 2, 5]:
      return 7
    elif perm == [0, 2, 3, 4, 1, 5]:
      return 8
    elif perm == [0, 3, 1, 4, 2, 5]:
      return 9

    elif perm == [0, 1, 4, 2, 3, 5]:
      return 10
    elif perm == [0, 3, 4, 1, 2, 5]:
      return 11

    elif perm == [0, 4, 1, 2, 3, 5]:
      return 12
    else:
      log("No perm assigned for " + str(perm), 'error')
      log("self.flag:", 'error')
      import sys
      sys.exit()

  def fill_vertex(self, pyfort_vertex, **kwargs):
    """ Writing the vertex information to a pyfort instance.

    :param pyfort_vertex: The fortran subroutine for initializing the vertices.
    :type  pyfort_vertex: PyFort instance
    """
    if 'branch_ids' in kwargs:
      branch_ids = kwargs['branch_ids']
    else:
      branch_ids = None

    self.derive_lorentz_base(branch_ids, **kwargs)

    if pyfort_vertex is None:
      return

    branch_tag = self.branch_tag
    v_id = "vertices(" + str(self.vertex_id) + ")"

    branches = branch_ids if branch_ids is not None else self.branches
    couplings = {}

    # couplings ordered by the branch number brN
    self.couplings_brN = {}
    colour_orders = {}

    # compute the number of branches necessary to resolve different colorflows
    # and coupling orders
    branch_keys = sorted(branches.keys())
    for j, co_key in enumerate(branch_keys):
      couplings_grouped = {order: {'lorentz': self.lorentz_ids[co_key][order],
                                   'couplings': self.couplings_lorentz_ordered[co_key][order]}
                           for order in self.couplings_lorentz_ordered[co_key]}

      # the branch number brN is given by all foregone branches.
      # Branches differ in either different colorflows or coupling orders.
      offset = sum(len(u) for u in couplings.values())
      for order_index, order in enumerate(couplings_grouped):
        brN = offset + order_index
        self.couplings_brN[brN] = couplings_grouped[order]
      couplings[co_key] = couplings_grouped
      colour_orders[co_key] = len(couplings_grouped)

    nbranches = sum(colour_orders.values())
    cbrN_tmp = self.couplings_brN.copy()
    if(hasattr(self.particles[-1], 'rxi_propagator') and
       self.particles[-1].rxi_propagator):
      # activating RXI gauge propagators
      Vertex.rxi_propagator = True

      if hasattr(self.particles[-1], 'rxi_parameter'):
        propagator_multiplier = 2
        Vertex.rxi_parameter = True

      # For massive vectors the explicit rxi-paramter is not needed to compute
      # the rxi propagator because it can be decomposed into single poles where
      # the pole positions are given by the gauge boson mass and the goldstone
      # boson mass. This requires two additional propagator.
      else:
        propagator_multiplier = 3
        Vertex.rxi_parameter = False

      # Extend the branches by the propagator multiplier
      for i in range(1, propagator_multiplier):
        for brN in cbrN_tmp:
          nbrN = brN + i * nbranches
          self.couplings_brN[nbrN] = cbrN_tmp[brN]
    else:
      propagator_multiplier = 1

    nbranchesp = nbranches * propagator_multiplier
    pyfort_vertex + ("allocate(" + v_id + "%branch" + branch_tag +
                     "(0:" + str(nbranchesp - 1) + "))")
    tp = {}
    for nprop in range(propagator_multiplier):
      branch_mp = 0
      for j, co_key in enumerate(branch_keys):
        if nprop > 0:
          # decomposed rxi propagator
          if propagator_multiplier == 3:
            l_id = self.lorentz_ids[co_key] + '_RXI' + str(nprop)
          # standard rxi propagator
          elif propagator_multiplier == 2:
            l_id = self.lorentz_ids[co_key] + '_RXI'
        else:
          l_id = self.lorentz_ids[co_key]
        co_id, co_coef = co_key
        # note when applying simpify the `real` property of a symbol gets lost
        Nc = Symbol('Nc', real=True)
        sq2 = Symbol('sq2', real=True)
        co_coef = sympify(co_coef).subs({Symbol('n'): Nc,
                                         Symbol('sq2'): sq2})
        co_coef_t = (real(co_coef), imag(co_coef))
        if co_coef_t not in self.colorcoeffs:
          co_coef_id = len(self.colorcoeffs) + 1
          self.colorcoeffs[co_coef_t] = co_coef_id
          co_coef_frm = str(co_coef.subs({I: Symbol('i_')}))
          self.colorcoeffs_form.append(co_coef_frm)
        cof_id = self.colorcoeffs[co_coef_t]

        branch_offset = nprop * nbranches + branch_mp - j
        branch_mp = branch_mp + colour_orders[co_key]
        for k, order in enumerate(couplings[co_key]):
          tp[j + k + branch_offset] = {'lid': l_id,
                                       'perm': self.field_permutation,
                                       'cid': co_id,
                                       'c_coef': co_coef_t,
                                       'btag': branch_tag,
                                       'order': order,
                                       'couplings': []}
          order_rule = [str(u[1]) for u in order]
          order_rule = '[' + ', '.join(order_rule) + ']'

          # cop: list of couplings (nGC_xx, nGC_xy, ...)
          cop = []
          for c in self.couplings_lorentz_ordered[co_key][order]:
            tp[j + k + branch_offset]['couplings'].append(c)
            if c != 0:
              cop.append('n' + str(c))
            else:
              cop.append('0')

          coupling_rule = '[' + ', '.join(cop) + ']'
          avst = 'call av' + branch_tag + '('
          avst += ', '.join([str(self.vertex_id), str(j + k + branch_offset),
                             str(co_id), str(cof_id), str(nprop), l_id[order],
                             str(self.field_permutation[order]),
                             order_rule, coupling_rule]) + ')'
          pyfort_vertex + avst
          pyfort_vertex.addLine()

    self.simple_output[self.vertex_id] = tp

  @classmethod
  def init_keys(self, p_key, co_key, branch):
    """ Initialize the the nested lorentz{branch}_dict.  """
    if branch == 'Tree':
      if p_key not in self.lorentzTree_dict:
        self.lorentzTree_dict[p_key] = {}
      if co_key not in self.lorentzTree_dict[p_key]:
        self.lorentzTree_dict[p_key][co_key] = {}
    elif branch == 'CT':
      if p_key not in self.lorentzCT_dict:
        self.lorentzCT_dict[p_key] = {}
      if co_key not in self.lorentzCT_dict[p_key]:
        self.lorentzCT_dict[p_key][co_key] = {}
    else:
      raise ValueError('Branch must be `Tree` or `CT`')

  @classmethod
  def clean_entry(self, p_key, co_key, branch):
    """ Remove empty strucs in lorentz{branch}_dict.  """
    if branch == 'Tree':
      if p_key in self.lorentzTree_dict:
        if co_key in self.lorentzTree_dict[p_key]:
          if () in self.lorentzTree_dict[p_key][co_key]:
            del self.lorentzTree_dict[p_key][co_key][()]
    elif branch == 'CT':
      if p_key in self.lorentzCT_dict:
        if co_key in self.lorentzCT_dict[p_key]:
          if () in self.lorentzCT_dict[p_key][co_key]:
            del self.lorentzCT_dict[p_key][co_key][()]
    else:
      raise ValueError('Branch must be `Tree` or `CT`')

  @classmethod
  def assign_coupling(self, p_key, co_key, ls_key, branch, coupling):
    """ Adds the coupling to the entry in
    lorentz{branch}_dict[p_key][co_key][ls_key].

    :p_key: Tuple of UFO particles.

    :co_key: Colorkey = (color_id, colorfactor)

    :param ls_key: Lorentzkey (Nparticles * product of lorentz structures,
                               (base_index, prefactor))
    :type ls_key: (Sympy.Mul, (int, int))

    :branch: 'Tree' or 'CT'

    :coupling: Sympy coupling expression
    """
    if coupling != 0:
      self.init_keys(p_key, co_key, branch)

      if branch == 'Tree':
        if ls_key not in self.lorentzTree_dict[p_key][co_key]:
          self.lorentzTree_dict[p_key][co_key][ls_key] = coupling
        else:
          self.lorentzTree_dict[p_key][co_key][ls_key] += coupling
      elif branch == 'CT':
        if ls_key not in self.lorentzCT_dict[p_key][co_key]:
          self.lorentzCT_dict[p_key][co_key][ls_key] = coupling
        else:
          self.lorentzCT_dict[p_key][co_key][ls_key] += coupling

  @classmethod
  def assign_couplings(self, p_key, co_key, ls_c_ord, branch, altcoupls=None):
    """ Adds the couplings defined in the lorentz ordered coupling list
    `ls_c_ord` to lorentz{branch}_dict[p_key][co_key][ls_key].

    :param altcoupl: Alternative list of couplings used instead of the couplings
                     in `ls_c_ord`.
    :type  altcoupls: list

    This is used to replace the couplings by counterterm couplings which is
    possible as long as the couplings are connected to the same lorentz
    structures.

    See assign_coupling for more details.
    """
    for c_i, cc in enumerate(ls_c_ord):
      lorentz = ls_c_ord[cc]
      ls_key = tuple([tuple([v for v in u]) for u in lorentz])
      if altcoupls is not None:
        coupling = altcoupls[c_i]
      else:
        coupling = cc

      # make sure the coupling is registered
      try:
        if coupling.is_Symbol:
          assert(coupling.name in RC.coupling_values)
          RC(coupling, branch)
      except AttributeError:
        if coupling != 0:
          log('Cannot assign coupling:`' + str(coupling) + '`', 'error')
          raise

      self.assign_coupling(p_key, co_key, ls_key, branch, coupling)

  @classmethod
  def assign_wavefunction(self, particles, co_key, ls_c_ord,
                          offdiag_once=False, subs_reno_constants=False):
    """ Derives wavefunction counterterms associated to the particles
    configuration `particles`, computes the resulting couplings and assigns them
    to the lorentzCT_dict.

    :particles: List of UFO particles.

    :co_key: Colorkey = (color_id, colorfactor)
    """
    wave_reno = []
    unique_couplings = list(ls_c_ord.keys())
    for cc in ls_c_ord:
      ls_key = ls_c_ord[cc]
      wave_reno.append(derive_wavefunction_ct(ls_key, particles,
                       offdiag_once=offdiag_once))
    for c_index in range(len(wave_reno)):
      # wavefunction renormalization
      for key in wave_reno[c_index]:
        for ls_key in wave_reno[c_index][key]:
          wave_reno_tmp = wave_reno[c_index][key][ls_key]
          if subs_reno_constants:
            wave_reno_tmp = wave_reno_tmp.subs(Renormalize.reno_repl)

          # make sure the coupling is registered
          coupling = unique_couplings[c_index]
          if coupling.is_Symbol:
            assert(coupling.name in RC.coupling_values)
            RC(coupling, 'Tree')

          cc = coupling * wave_reno_tmp
          if cc != 0:
            self.assign_coupling(key, co_key, ls_key, 'CT', cc)

  @classmethod
  def check_ctcouplings(cls, couplings, register_ct_coupling,
                        subs_reno_constants, ctexpansion=True, force=False, **kwargs):
    """ Checks if all couplings in `couplings` have been expanded, meaning that
    their expanded result should be present in Counterterms.cts (counterterm
    expansion dict.  In addition the counterterms are registered and their order
    in the coupling constants is computed.

    :coupling: List of symbols
    :param register_ct_coupling: Substitute for solutions of renormalization
                                 constants. This will make sure that the order
                                 of the countererm is computed.

    :type  register_ct_coupling: bool
    :subs_reno_constants: bool

    :param ctexpansion: True if the countererm coupling is a result of an
                        expansion.
    :type  ctexpansion: bool
    """
    if register_ct_coupling:
      if subs_reno_constants:
        repl = Renormalize.reno_repl
      else:
        repl = None
      for u in couplings:

        if ctexpansion:
          if u.name not in Counterterms.cts:
            uc = RC.coupling_dict[u.name]
            Counterterms.plot_progress = False
            Counterterms.expand_couplings(RC, selection=[uc],
                                          force=force, **kwargs)
          RC.register_ct_coupling(u, Counterterms.cts[u.name], repl=repl,
                                  ctexpansion=ctexpansion)
        else:
          if u.name not in RC.coupling_values:
            raise Exception('Coupling: ' + u.name +
                            ' not found in coupling values.')
          RC.register_ct_coupling(u, RC.coupling_values[u.name], repl=repl,
                                  ctexpansion=ctexpansion)
    else:
      if not (hasattr(RC, 'ct_coupling_reg')):
        raise Exception('Run generate_CT first!')

  @classmethod
  def assign_vertex(self, vertex, particles, co_key,
                    lorentz_coupling_ordered,
                    assign_tree=True,
                    assign_ct=True,
                    explicit_permute=True,
                    register_ct_coupling=False,
                    subs_reno_constants=False,
                    vertex_expansion=False,
                    compute_ct=False,
                    force=False,
                    **kwargs):
    """ Disassembles the vertex in particle (p_key), colour(co_key) and lorentz
    (ls_key) components and stores the result in the attributes lorentzTree_dict
    or lorentzCT_dict which are dictionaries hold all information on
    the Feynman rules for tree-and counterterms.

    A vertex can yield several tree as well as ct Feynman rules depending on the
    kind of the vertex.

    - Vertex: It is possible, in addition to the normal vertex to derive the
      associated counterterm vertices.

    - CTVertex: Only reads the CT vertex information. No expansion performed.

    - TreeInducedCTVertex: Performs the expansion and reads the derived CT
                           vertex information. The tree vertex information is
                           not stored.

    :param vertex: UFO Vertex, CTVertex or TreeInducedCTVertex instance
    :type  vertex: Vertex,CTVertex,TreeInducedCTVertex

    :param particles: List of particles which must agree with the particles
                      associated to the vertex up to a permutation.
    :type  particles: list of UFO particles

    :param assign_tree: Enable/Disable this vertex for Tree and Bare Loop
    :type  assign_tree: bool

    :param assign_ct: Enable/Disable this vertex for CT
    :type  assign_ct: bool

    """

    p_key = tuple(particles)
    if type(vertex) is model_objects.Vertex and assign_tree:

      self.assign_couplings(p_key, co_key, lorentz_coupling_ordered, 'Tree')

      specific_vertex = bool(vertex_expansion) and (len(p_key) != 2)
      if specific_vertex:
        vertex_allowed = vertex.name in vertex_expansion

        expand_vertex = compute_ct and vertex_allowed
      else:
        expand_vertex = compute_ct

      if expand_vertex and assign_ct:
        self.check_ctcouplings(lorentz_coupling_ordered,
                               register_ct_coupling, subs_reno_constants,
                               ctexpansion=True, force=force, **kwargs)

        ct_expr = [RC.ct_coupling_reg[u.name]
                   if Counterterms.cts[u.name] != 0 else
                   sympify(0) for u in lorentz_coupling_ordered]

        ct_couplings = ct_expr

        self.assign_wavefunction(particles, co_key, lorentz_coupling_ordered,
                                 offdiag_once=not explicit_permute,
                                 subs_reno_constants=subs_reno_constants)

        self.assign_couplings(p_key, co_key, lorentz_coupling_ordered, 'CT',
                              ct_couplings)
        self.clean_entry(p_key, co_key, 'CT')

    # Counterterm vertices only
    elif type(vertex) == model_objects.CTVertex and assign_ct:
      specific_vertex = (compute_ct and bool(vertex_expansion) and
                         (len(p_key) != 2))
      if specific_vertex:
        vertex_allowed = vertex.name in vertex_expansion

        expand_vertex = compute_ct and vertex_allowed
      else:
        expand_vertex = compute_ct

      if expand_vertex:
        self.init_keys(p_key, co_key, 'CT')

        self.check_ctcouplings(lorentz_coupling_ordered,
                               register_ct_coupling, subs_reno_constants,
                               ctexpansion=False, force=force, **kwargs)

        self.assign_couplings(p_key, co_key, lorentz_coupling_ordered, 'CT')

        self.clean_entry(p_key, co_key, 'CT')
      else:
        # Case shouldn't happen. Only if the user adds a CTVertex to the usual
        # Vertex array.
        log('Compute_ct disabled, vertex not taken into account: ' +
            vertex.name, 'warning')
        log('Possibly a CTVertex resides in the all_vertices list. ' +
            'CTVertex should be in all_CTvertices', 'debug')

    # TreeInducedCTVertex are treevertices from which the counterterms are
    # deduced without taking along the treevertex.
    elif type(vertex) is model.modelfile.TreeInducedCTVertex and assign_ct:
      if compute_ct:
        #if vertex.expand_couplings:
        if (not hasattr(vertex, 'expand_couplings')) or vertex.expand_couplings:
          self.check_ctcouplings(lorentz_coupling_ordered,
                                 register_ct_coupling, subs_reno_constants,
                                 ctexpansion=True, force=force, **kwargs)
          ct_expansion_done = {u: u.name in Counterterms.cts
                               for u in lorentz_coupling_ordered}
          if not all(ct_expansion_done.values()):
            log('Couplings have not properly been expanded. See log.', 'error')
            log('Rerun derive_ct_expansion.', 'error')
            ct_expansion_done = [u.name for u in ct_expansion_done
                                 if ct_expansion_done[u] is False]
            log('Missing counterterms: ' + ', '.join(ct_expansion_done),
                'debug')
            sys.exit()

          ct_expr = [RC.ct_coupling_reg[u.name]
                     if Counterterms.cts[u.name] != 0 else
                     sympify(0) for u in lorentz_coupling_ordered]
          ct_couplings = ct_expr

          self.assign_couplings(p_key, co_key, lorentz_coupling_ordered, 'CT',
                                ct_couplings)

        if((not hasattr(vertex, 'expand_wavefunctions')) or
           vertex.expand_wavefunctions):
          self.assign_wavefunction(particles, co_key, lorentz_coupling_ordered,
                                   offdiag_once=not explicit_permute,
                                   subs_reno_constants=subs_reno_constants)

        self.clean_entry(p_key, co_key, 'CT')
      else:
        log('Compute_ct disabled, vertex not taken into account: ' +
            vertex.name, 'warning')
        log('Possibly a TreeInducedCTVertex resides in the all_vertices list.' +
            ' TreeInducedCTVertex should be in all_CTvertices', 'debug')

  @classmethod
  def optimize_bases(cls, plot_progress=True, nomerge=[]):
    log('\n', 'info')
    log('Merging bases.', 'info')
    if plot_progress:
      PG = ProgressBar(len(cls.current_strucs))
    for key in cls.current_strucs:
      if len(cls.current_strucs[key]) > 1:
        cls.optimize_base(key, nomerge=nomerge)
      if plot_progress:
        PG.advanceAndPlot()

  @staticmethod
  def print_base(b):
    from rept1l.baseutils.base_buildup import simple_coupling_poly
    bb = sorted(b[1], key=lambda x: x[0])
    couplings = [u[1] for u in bb]
    facs = [u[2] for u in b[1]]
    couplings, _ = simple_coupling_poly(couplings)
    couplings = [c * facs[cpos] for cpos, c in enumerate(couplings)]
    print(couplings)

  @classmethod
  def optimize_base(cls, key, nomerge=[]):
    cs = {}
    from rept1l.vertex.vertex_utils import merge_bases
    for s in cls.current_strucs[key]:
      sids = tuple(sorted(u[0] for u in s[1]))
      if sids not in cs:
        cs[sids] = [s]
      else:
        cs[sids].append(s)

    for sids in cs:
      cs0 = cs[sids][0]
      if len(cs[sids]) > 1:
        for csc in cs[sids][1:]:
          cs0_base = merge_bases(cs0, csc)
          cs0 = [cs0[0], cs0_base]

      cs[sids] = cs0

    # remove bases which are not supposed to be merged
    css_nomerge = []
    for sids in list(cs):
      if sids in nomerge:
        css_nomerge.append(cs[sids])
        del cs[sids]

    def merge_recursive(css, offset=0, verbose=False):
      for c1pos, c1 in enumerate(css[offset:-1]):
        for c2pos, c2 in enumerate(css[c1pos + offset + 1:]):
          if verbose:
            print('\n')
            print('\n')
            print('Merging for c1 c2')
            print("c1:", c1)
            print("c2:", c2)
            print('\n')
          cs_base = merge_bases(c1, c2, exact_base_match=False)
          if verbose:
            print("cs_base:", cs_base)
            print('\n')
            print('\n')
          if cs_base:
            new_base_ids = [u[0] for u in cs_base]
            old_base_ids = [u[0] for u in c2[1]]
            assert(all(u in new_base_ids for u in old_base_ids))
            del css[c1pos + offset + 1 + c2pos]
            css[c1pos + offset] = [c1[0], cs_base]
            return merge_recursive(css, offset=c1pos)
      return css

    css = merge_recursive(cs.values())
    cls.current_strucs[key] = css + css_nomerge

  @classmethod
  def get_lorentz_structure(cls, ids_to_calc, key):
    """ Return lorentz base structures, corresponding arguments and prefactors
    defined by the base key and argument identifier.
    """
    from rept1l.baseutils import compute_ls_coupling_grouped
    from rept1l.baseutils import extract_couplings
    from rept1l.helper_lib import flatten
    from rept1l.combinatorics import lorentz_base_dict

    if not hasattr(cls, 'ls_bases'):
      cls.ls_bases = [[v[0] for v in u] for u in cls.current_strucs.values()]

    ls_strucs = []
    bases = []
    for id_to_calc in ids_to_calc:
      ls_index = 0
      base_index = 0
      for ls in cls.ls_bases:
        if id_to_calc in ls:
          ls_index = cls.ls_bases.index(ls)
          base_index = ls.index(id_to_calc)

      ls_struc = list(cls.current_strucs.keys())[ls_index]
      base = cls.current_strucs[ls_struc][base_index]
      bases.append(base)
      ls_strucs.append(ls_struc)

    # update bases and ls_strucs
    bases, ls_strucs = compute_ls_coupling_grouped(bases, ls_strucs)

    couplings_base = set([q for q in flatten([[[k for k in v[1].free_symbols]
                                             for v in u[1]] for u in bases])])
    couplings_base = sorted(couplings_base, key=lambda x: x.name)
    prefactors = [[extract_couplings(v[1] * v[2], couplings_base)[1]
                   for v in u[1]] for u in bases]
    couplings = [[extract_couplings(v[1] * v[2], couplings_base)[0]
                  for v in u[1]] for u in bases]
    arguments = None
    if any(len(lorentz_base_dict[ls]) > 0 for ls in ls_strucs):
      arguments = [[lorentz_base_dict[ls][u[0]] for u in bases[i][1]]
                   if len(lorentz_base_dict[ls]) > 0 else [[]]
                   for i, ls in enumerate(ls_strucs)]

    ffid = 0
    if key.count('F') == 4:
      log("Four-fermion operator", 'debug')
      prefactors, ffid = cls.get_fourfermionflow_id(ls_strucs, arguments, prefactors)
      log("Assigned fermionflow id: " + str(ffid), 'debug')

    log("bases:" + str(bases), 'debug')
    log("lorentz_base_dict[ls_strucs[0]]:" +
        str(lorentz_base_dict[ls_strucs[0]]), 'debug')
    log("ls_strucs:" + str(ls_strucs), 'debug')
    log("prefactors:" + str(prefactors), 'debug')
    log("couplings:" + str(couplings), 'debug')
    log("arguments:" + str(arguments), 'debug')

    return ls_strucs, arguments, prefactors, ffid

  @staticmethod
  def get_fourfermionflow_id(ls_strucs, args, prefactors):
    """ Returns the fermion flow for a given four-fermion structure.

    >>> GammaP = Symbol('GammaP')
    >>> GammaM = Symbol('GammaM')
    >>> lss = [4*GammaP**2, 4*GammaM*GammaP, 4*GammaM*GammaP, 4*GammaM**2]
    >>> args = [[[[-1, 2, 4], [-1, 3, 1]]],\
 [[[-1, 2, 1], [-1, 3, 4]], [[-1, 3, 4], [-1, 2, 1]]],\
 [[[-1, 3, 1], [-1, 2, 4]], [[-1, 2, 4], [-1, 3, 1]]],\
 [[[-1, 2, 4], [-1, 3, 1]]]]
    >>> prefactors = [[1], [-3, -3], [1, 1], [1]]
    >>> pf, ffid = Vertex.get_fourfermionflow_id(lss,args,prefactors)
    >>> ffid
    2
    >>> pf
    [[1], [3, 3], [1, 1], [1]]
    """
    from rept1l.combinatorics import get_lorentz_key_components, order_strucs, lorentz_base
    fffs = []
    for lpos, ls in enumerate(ls_strucs):
      ls_key, _ = get_lorentz_key_components(ls)
      ls_key = order_strucs(ls_key)
      aarg = args[0]
      ffs = []
      # loop over additively connected structures
      for ppos, parg in enumerate(args[lpos]):
        # a map which connects anti-fermion arguments to fermion arguments
        connection_map = {}
        contractions = False
        # loop over multiplicatively connected structures
        for mpos, marg in enumerate(parg):
          lb = lorentz_base.get_base(ls_key[mpos])
          if 'antispinor' in lb.arg_types:
            asi = lb.arg_types.index('antispinor')
            si = lb.arg_types.index('spinor')
            casi = marg[asi]
            csi = marg[si]
            if casi < 0 or csi < 0:
              contractions = True
            assert(casi not in connection_map)
            connection_map[casi] = csi

        # follow fermion chains to eliminate contractions
        if contractions is True:
          cargs = [u for u in connection_map.keys() if u < 0]
          while len(cargs) > 0:
            cc = connection_map[cargs[0]]
            for u in connection_map:
              if connection_map[u] == cargs[0]:
                connection_map[u] = cc
                del connection_map[cargs[0]]
                del cargs[0]
                break
        # sorted fermion flow (we don't care about the order of fermion/anti-fermion indices)
        # 3 possible orderings, only 2 realized in each case, relative -1 between
        # ((1,2), (3,4)):  1
        # ((1,3), (2,4)):  2
        # ((1,4), (2,3)):  3
        ff = tuple(sorted(tuple(sorted((k,connection_map[k]))) for k in connection_map))
        ffs.append(ff)
      fffs.append(ffs)

    # adapt prefactors including relative fermion signs
    defaultff = fffs[0][0]
    for lpos in range(len(ls_strucs)):
      aarg = args[0]
      for ppos, parg in enumerate(args[lpos]):
        if fffs[lpos][ppos] != defaultff:
          prefactors[lpos][ppos] = -prefactors[lpos][ppos]

    ffid = {((1,2), (3,4)): 1, ((1,3), (2,4)): 2, ((1,4), (2,3)): 3}[defaultff]
    return prefactors, ffid

if __name__ == "__main__":
  import doctest
  doctest.testmod()
