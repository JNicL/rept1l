#==============================================================================#
#                               vertex_utils.py                                #
#==============================================================================#

##############
#  Includes  #
##############

from sympy import Symbol, Rational
from rept1l.baseutils.base_buildup import simple_coupling_poly

import rept1l_config
if(hasattr(rept1l_config, 'max_base_length')):
  max_base_length = rept1l_config.max_base_length
else:
  max_base_length = 8

#############
#  Methods  #
#############

def prod(vec1, vec2):
  return sum(x*y for x, y in zip(vec1, vec2))

def smul(fac, vec):
  return tuple(fac*u for u in vec)

def diff(vec1, vec2):
  return tuple(x-y for x, y in zip(vec1, vec2))

def screen_vector(vec1, vec2):
  vec2_l = list(vec2)
  return tuple(u if vec2_l[upos] == 0 else 0
               for upos, u in enumerate(vec1))

def screen_vector_set(vec1, vecset):
  vec1_tmp = vec1
  for vec2 in vecset:
    vec2_l = list(vec2)
    vec1_tmp = tuple(u if vec2_l[upos] == 0 else 0
                     for upos, u in enumerate(vec1_tmp))
  return vec1_tmp

def count_nonzero(v):
  return sum(1 if u != 0 else 0 for u in v)

def diffpos(v1, v2, pos):
  v1l = [u for u in v1]
  v2l = [u for u in v2]
  if v2l[pos] == 0 or v1l[pos] == 0:
    return

  fac = Rational(v1l[pos], v2l[pos])
  return diff(v1, smul(fac, v2l))

def is_zero_vector(v):
  return sum(abs(u) for u in v) == 0

def compute_prj(v1, v2):
  """ Compute projection of v1 onto v2 """
  v1_v2 = prod(v1, v2)
  v2_v2 = prod(v2, v2)
  f = Rational(v1_v2, v2_v2)
  return smul(f, v2)

def is_linear_dependent(v, vset):
  """ Returns true if vector v is linear dependent on vectors in vset.
  >>> vset = [(1, 0, 0), (0, 0, 1)]
  >>> is_linear_dependent((1, -1, 0), vset)
  False
  >>> is_linear_dependent((1, 0, -1), vset)
  True
  """
  vtmp = v
  for vc in vset:
    prj = compute_prj(vtmp, vc)
    vtmp = diff(vtmp, prj)
  return is_zero_vector(vtmp)

def get_minimal_diff(v1, v2, checkpos=[], dist={}):
  """ Determines the position indices i for which the difference
  v1 - v1[i]/v2[i] v2 is minimal in the sense that the number of zeros is
  maximal. The result is returned as a dictionary {distance: position index i}.

  >>> v1 = [2, -1, 1, 1, -1, 3, -2, 2, -1, 1, -1, 1, -3, -1, 1, -2, 1, -1]
  >>> v2 = [2, -1, -2, -2, -2, -2, 2, 2, 2, 2, -2, 2, -2, 2, -2, 2, -1, -2]
  >>> diffdict = get_minimal_diff(v1, v2)

  >>> diffdict
  {17: 5, 13: 2, 15: 0}
  >>> pos=diffdict[15]; fac=v1[pos]/v2[pos]; v2m=smul(fac, v2)
  >>> vd = diffpos(v1, v2, pos)
  >>> vd
  (0, 0, 3, 3, 1, 5, -4, 0, -3, -1, 1, -1, -1, -3, 3, -4, 2, 1)
  >>> count_nonzero(vd)
  15
  >>> screen_vector(v1, vd) == screen_vector(v2m, vd)
  True

  >>> pos=diffdict[13]
  >>> vd = diffpos(v1, v2, pos)
  >>> vd
  (3, -3/2, 0, 0, -2, 2, -1, 3, 0, 2, -2, 2, -4, 0, 0, -1, 1/2, -2)
  >>> count_nonzero(vd)
  13
  """
  overlap = any(u != 0 and v != 0 for u, v in zip(v1, v2))
  if not overlap:
    return
  if checkpos == []:
    checkpos = range(len(v1))
  for p in checkpos:
    dp = diffpos(v1, v2, p)
    if dp:
      distance = count_nonzero(dp)
      if distance not in dist:
        dist[distance] = p

      left = [u for u in checkpos if dp[u] != 0]
      if len(left) > 0:
        return get_minimal_diff(v1, v2, checkpos=left, dist=dist)
  return dist

def select_span_once(vecs1, vecs2, include_one_elem=True, **kwargs):
  """ Find a set of `simple` vectors which span the VS=vecs1, vecs2.

  >>> c1_vecs = set([(1, 1, 0, 0, 0), (0, 0, 1, 0, 0), (0, 0, 0, 1, -2)])
  >>> c2_vecs = set([(-1, -1, 1, 0, 0), (0, 0, 0, 2, -4)])
  >>> select_span(c1_vecs, c2_vecs)
  set([(0, 0, 0, 1, -2), (1, 1, 0, 0, 0), (0, 0, 1, 0, 0)])

  >>> c1_vecs = set([(1, 1, 1)])
  >>> c2_vecs = set([(-1, -1, -1)])
  >>> select_span(c1_vecs, c2_vecs)
  set([(1, 1, 1)])

  >>> c1_vecs = set([(1, 1, 1)])
  >>> c2_vecs = set([(1, -1, -1)])
  >>> select_span(c1_vecs, c2_vecs)
  set([(1, 0, 0), (0, 1, 1)])
  """
  # checks - no overlapp within same base
  for v1 in vecs1:
    for v2 in vecs1:
      if v1 != v2:
        assert(prod(v1, v2) == 0)

  for v1 in vecs2:
    for v2 in vecs2:
      if v1 != v2:
        assert(prod(v1, v2) == 0)

  span = set()
  for v1 in vecs1:
    for v2 in vecs2:
      span_add = set()
      dist = get_minimal_diff(v1, v2, dist={})
      if dist:
        for d in sorted(dist.keys()):
          opt = dist[d]
          dp = diffpos(v1, v2, opt)
          t1 = screen_vector(v1, dp)
          test = True
          if test:
            fac = Rational([u for u in v1][opt], [u for u in v2][opt])
            t2 = smul(fac, screen_vector(v2, dp))
            assert(t1 == t2)
          if len(span_add) > 0:
            t1 = screen_vector_set(t1, span_add)
          if not is_zero_vector(t1):
            is_one_element = [u != 0 for u in t1]
            if is_one_element.count(True) == 1:
              if include_one_elem:
                one_elem = tuple(1 if pos == is_one_element.index(True) else 0
                                 for pos in range(len(t1)))
                span_add.add(one_elem)
            else:
              span_add.add(t1)
      span = span.union(span_add)

  return span

def select_span(vecs1, vecs2, span=set(), **kwargs):
  """ Applies recursively the method select_span_once to all vectors in `vecs1`
  and `vecs2`.

  >>> c1_vecs = set([(2, -1, 1, 1, -1, 3, -2, 2, -1, 1, -1, 1, -3, -1, 1, -2, 1, -1)])
  >>> c2_vecs = set([(1, -2, -1, -1, -1, -1, 1, 1, 1, 1, -1, 1, -1, 1, -1, 1, -2, -1)])
  >>> span = select_span(c1_vecs, c2_vecs)
  >>> len(span)
  8
  >>> all(is_linear_dependent(u, span) for u in c1_vecs)
  True
  >>> all(is_linear_dependent(u, span) for u in c2_vecs)
  True

  The method is optimal in the sense that if we leave out all the one-element
  vectors (standard base vectors in Z^n), we are left with
  >>> span = select_span(c1_vecs, c2_vecs, include_one_elem=False)
  >>> screen_vector_set([v for v in c1_vecs][0], span)
  (0, -1, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, -3, 0, 0, 0, 1, 0)
  >>> screen_vector_set([v for v in c2_vecs][0], span)
  (0, -2, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, -2, 0)

  Clearly, the two vectors cannot be spanned by simpler vectros than
  one-element vectors.
  """
  span_tmp = select_span_once(vecs1, vecs2, **kwargs)
  if span_tmp == set():
    return span
  else:
    span = span.union(span_tmp)
    vecs1 = set(screen_vector_set(u, span) for u in vecs1)
    vecs1 = set(u for u in vecs1 if not is_zero_vector(u))
    vecs2 = set(screen_vector_set(u, span) for u in vecs2)
    vecs2 = set(u for u in vecs2 if not is_zero_vector(u))
    return select_span(vecs1, vecs2, span=span, **kwargs)

def get_base_ids(base):
  return [u[0] for u in base[1]]

def get_cvecs(couplings, facs):
  cvecs = set()
  for cc in set(couplings):
    cvec = tuple(facs[upos] if u == cc else 0
                 for upos, u in enumerate(couplings))
    cvecs.add(cvec)
  return cvecs

def merge_bases(v1, v2, exact_base_match=True):
  """ Finds a common and minimal base for v1 and v2.

  :param exact_base_match: Set true if bases should be merged if they are not
                           the same but have a non-zero overlap.
  :type  exact_base_match: bool
  >>> from sympy import symbols
  >>> c0, c1, c2, c3 = symbols('c0 c1 c2 c3')
  >>> v1 = [81, [[9, c1, -1], [10, c1, 1], [11, c1, 1]]]
  >>> v2 = [85, [[0, c0, -1], [10, c1, -1], [9, c1, 1]]]

  Base ids differ and no merging is performed if `exact_base_match` is not set
  to true
  >>> merge_bases(v1, v2)


  >>> merge_bases(v1, v2, exact_base_match=False)
  [[0, c2, -1], [9, c0, -1], [10, c0, 1], [11, c1, 1]]
  """
  base_id_v1 = get_base_ids(v1)
  base_id_v2 = get_base_ids(v2)
  overlapp = [u for u in base_id_v1 if u in base_id_v2]

  # bases not allowed to be merged if they have no base ids in common
  if len(overlapp) == 0:
    return

  # v1 fit into v2?
  v1_rest = [u for u in base_id_v1 if u not in overlapp]
  # and vice versa
  v2_rest = [u for u in base_id_v2 if u not in overlapp]

  if exact_base_match and (len(v1_rest) > 0 or len(v2_rest) > 0):
    return

  couplings_v1 = [u[1] for u in v1[1] if u[0] in overlapp]
  couplings_v1, _ = simple_coupling_poly(couplings_v1)
  facs_v1 = [u[2] for u in v1[1] if u[0] in overlapp]

  c1_vecs = get_cvecs(couplings_v1, facs_v1)

  couplings_v2 = [u[1] for u in v2[1] if u[0] in overlapp]
  couplings_v2, _ = simple_coupling_poly(couplings_v2)
  facs_v2 = [u[2] for u in v2[1] if u[0] in overlapp]
  c2_vecs = get_cvecs(couplings_v2, facs_v2)

  new_base, clen = get_base_vecs(c1_vecs, c2_vecs, overlapp)

  # prevent merging if length exceeds the max_base_length.
  if max_base_length and clen > max_base_length:
    # perform the merging anyway if both structures have equal base
    if (len(v1_rest) > 0 or len(v2_rest) > 0):
      return
  offset = clen

  if len(v1_rest) > 0:
    couplings_v1 = [u[1] for u in v1[1] if u[0] not in overlapp]
    couplings_v1, _ = simple_coupling_poly(couplings_v1)
    facs_v1 = [u[2] for u in v1[1] if u[0] not in overlapp]
    c1_vecs = get_cvecs(couplings_v1, facs_v1)
    new_c1_base = build_base_from_vecs(c1_vecs, v1_rest, offset=offset)
    offset += len(c1_vecs)
    new_base.extend(new_c1_base)

  if len(v2_rest) > 0:
    couplings_v2 = [u[1] for u in v2[1] if u[0] not in overlapp]
    couplings_v2, _ = simple_coupling_poly(couplings_v2)
    facs_v2 = [u[2] for u in v2[1] if u[0] not in overlapp]
    c2_vecs = get_cvecs(couplings_v2, facs_v2)
    new_c2_base = build_base_from_vecs(c2_vecs, v2_rest, offset=offset)
    offset += len(c2_vecs)
    new_base.extend(new_c2_base)

  return sorted(new_base, key=lambda x: x[0])

def build_base_from_vecs(new_vecs, base_ids, offset=0):
  """ Takes a set of (integer) vectors (assuming no overlapp) and a list of
  base_ids which length should match the dimension of the vectors and builds a
  base.

  >>> nv = set([(-1, 1, 0), (0, 0, 1)])
  >>> ni = [9, 10, 11]
  >>> build_base_from_vecs(nv, ni)
  [[9, c0, -1], [10, c0, 1], [11, c1, 1]]
  """
  new_base = []
  for cpos, new_vec in enumerate(sorted(new_vecs)):
    assert(len(new_vec) == len(base_ids))
    for upos, u in enumerate(new_vec):
      if u != 0:
        cc = Symbol('c' + str(cpos+offset))
        new_base.append([base_ids[upos], cc, u])
  return new_base

def get_base_vecs(vecs1, vecs2, base_ids, perform_checks=True):
  """ Merges two vector basses defined by vecs1 and vecs2

  Trivial merge, bases are identical
  >>> vecs1 = set([(-1, 1)])
  >>> vecs2 = set([(-1, 1)])
  >>> bids = [1, 2]
  >>> bv, nc = get_base_vecs(vecs1, vecs2, bids)

  Base vectors
  >>> bv
  [[1, c0, -1], [2, c0, 1]]

  Number of different couplings
  >>> nc
  1

  Less trivial, first two dimension can be merged, but need separate vector for
  last index.
  >>> vecs1 = set([(-1, 1, 1)])
  >>> vecs2 = set([(-1, 1, 2)])
  >>> bids = [1, 2, 3]
  >>> bv, nc = get_base_vecs(vecs1, vecs2, bids)

  Base vectors
  >>> bv
  [[1, c0, -1], [2, c0, 1], [3, c1, 1]]

  Number of different couplings
  >>> nc
  2

  Even less trivial, first two dimension can be merged, but need separate
  vector for the two index which themselves can be merged.
  >>> vecs1 = set([(-1, 1, 1, 2)])
  >>> vecs2 = set([(-1, 1, 2, 4)])
  >>> bids = [1, 2, 3, 4]
  >>> bv, nc = get_base_vecs(vecs1, vecs2, bids)

  Base vectors
  >>> bv
  [[1, c0, -1], [2, c0, 1], [3, c1, 1], [4, c1, 2]]

  Number of different couplings
  >>> nc
  2
  """
  if all(is_linear_dependent(u, vecs1) for u in vecs2):
    new_base = build_base_from_vecs(vecs1, base_ids)
    return new_base, len(vecs1)

  # search for vectors which have non-zero overlap and split them
  new_vecs = select_span(vecs1, vecs2)
  if perform_checks:
    for v in vecs1:
      assert(is_linear_dependent(v, new_vecs))
    for v in vecs2:
      assert(is_linear_dependent(v, new_vecs))

  new_base = build_base_from_vecs(new_vecs, base_ids)
  return new_base, len(new_vecs)

if __name__ == '__main__':
  import doctest
  doctest.testmod()
