    function get_propagator_extension_mdl(l_id) result(ret_pext)
      integer, intent(in) :: l_id
      integer :: ret_pext
      ret_pext = propagator_extension(l_id)
    end function get_propagator_extension_mdl
