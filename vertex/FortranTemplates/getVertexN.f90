    subroutine get_vertexN_mdl(particles, particleOut, vertex_ret)
      integer, intent(out)              :: vertex_ret
      integer, dimension(:), intent(in) :: particles
      integer, intent(in)               :: particleOut
      integer(kind=8)                   :: config
      integer                           :: address
      logical                           :: found_vertex

      call createParticleConfig(particles, particleOut, config)
      call dict(config, 1, address, found_vertex, 0)
      if(found_vertex) then
        vertex_ret = dict_get_value(address)
      else
        vertex_ret = -1
      end if

    end subroutine get_vertexN_mdl
