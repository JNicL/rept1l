  function get_recola_base_mdl(l_id) result(ret_rb)
      integer, intent(in) :: l_id
      integer :: ret_rb
      ret_rb = recola_base(l_id)
    end function get_recola_base_mdl

  subroutine get_vertex_branch_status_mdl(vx,xlp,branch_exists)
    integer, intent(in)  :: vx,xlp
    logical, intent(out) ::branch_exists
    select case (xlp)
    case(TreeBranch:LoopBranch)
      branch_exists = allocated(vertices(vx)%branch)
    case(CTBranch)
      branch_exists = allocated(vertices(vx)%branchCT)
    case(R2Branch)
      branch_exists = allocated(vertices(vx)%branchR2)
    end select
  end subroutine get_vertex_branch_status_mdl

  subroutine dealloc_vertex_mdl(vx,xlp)
    integer, intent(in)  :: vx,xlp
    logical ::branch_exists
    if (vx .ge. 0 .and.  vx .lt. n_vertices) then
      select case (xlp)
      case(TreeBranch:LoopBranch)
        if (allocated(vertices(vx)%branch)) deallocate(vertices(vx)%branch)
      case(CTBranch)
        if (allocated(vertices(vx)%branchCT)) deallocate(vertices(vx)%branchCT)
      case(R2Branch)
        if (allocated(vertices(vx)%branchR2)) deallocate(vertices(vx)%branchR2)
      case default
        if (allocated(vertices(vx)%branch)) deallocate(vertices(vx)%branch)
        if (allocated(vertices(vx)%branchCT)) deallocate(vertices(vx)%branchCT)
        if (allocated(vertices(vx)%branchR2)) deallocate(vertices(vx)%branchR2)
      end select
    end if
  end subroutine dealloc_vertex_mdl

  subroutine get_vertex_colour_branches_mdl(vx,xlp,cfids,nbranches)
    integer, allocatable, intent(out) :: cfids(:)
    integer, intent(out) :: nbranches
    integer, intent(in)  :: vx,xlp
    select case (xlp)
    case (TreeBranch:LoopBranch)
      allocate(cfids(size(vertices(vx)%branch)))
      cfids(:) = vertices(vx)%branch(0:)%color_id
      nbranches = size(vertices(vx)%branch)
    case(CTBranch)
      allocate(cfids(size(vertices(vx)%branchCT)))
      cfids(:) = vertices(vx)%branchCT(0:)%color_id
      nbranches = size(vertices(vx)%branchCT)
    case(R2Branch)
      allocate(cfids(size(vertices(vx)%branchR2)))
      cfids(:) = vertices(vx)%branchR2(0:)%color_id
      nbranches = size(vertices(vx)%branchR2)
    end select
  end subroutine get_vertex_colour_branches_mdl

  subroutine get_vertex_pext_mdl(vx,xlp,br,pext)
    integer, intent(in) :: vx,xlp,br
    integer, intent(out) :: pext
    select case (xlp)
    case (TreeBranch:LoopBranch)
      pext = vertices(vx)%branch(br)%pext
    case(CTBranch)
      pext = vertices(vx)%branchCT(br)%pext
    case(R2Branch)
      pext = vertices(vx)%branchR2(br)%pext
    end select
  end subroutine get_vertex_pext_mdl

  subroutine get_vertex_order_increase_mdl(vx,xlp,br,orderInc)
    integer, intent(in) :: vx,xlp,br
    integer, dimension(:), intent(out) :: orderInc
    select case (xlp)
    case (TreeBranch:LoopBranch)
      orderInc = vertices(vx)%branch(br)%coupling_order
    case(CTBranch)
      orderInc = vertices(vx)%branchCT(br)%coupling_order
    case(R2Branch)
      orderInc = vertices(vx)%branchR2(br)%coupling_order
    end select
  end subroutine get_vertex_order_increase_mdl

  subroutine get_vertex_lorentz_type_mdl(vx,xlp,br,lid)
    integer, intent(in)  :: vx,xlp,br
    integer, intent(out) :: lid
    select case (xlp)
    case (TreeBranch:LoopBranch)
      lid = vertices(vx)%branch(br)%lorentz_id
    case(CTBranch)
      lid = vertices(vx)%branchCT(br)%lorentz_id
    case(R2Branch)
      lid = vertices(vx)%branchR2(br)%lorentz_id
    end select
  end subroutine get_vertex_lorentz_type_mdl

  subroutine get_vertex_perm_mdl(vx,xlp,br,perm)
    integer, intent(in)  :: vx,xlp,br
    integer, intent(out) :: perm
    select case (xlp)
    case (TreeBranch:LoopBranch)
      perm = vertices(vx)%branch(br)%perm
    case(CTBranch)
      perm = vertices(vx)%branchCT(br)%perm
    case(R2Branch)
      perm = vertices(vx)%branchR2(br)%perm
    end select
  end subroutine get_vertex_perm_mdl

  subroutine get_vertex_colour_id_mdl(vx,xlp,br,cid)
    integer, intent(in)  :: vx,xlp,br
    integer, intent(out) :: cid
    select case (xlp)
    case (TreeBranch:LoopBranch)
      cid = vertices(vx)%branch(br)%colorfac_id
    case(CTBranch)
      cid = vertices(vx)%branchCT(br)%colorfac_id
    case(R2Branch)
      cid = vertices(vx)%branchR2(br)%colorfac_id
    end select
  end subroutine get_vertex_colour_id_mdl

  function is_zero_coupling_vertex_mdl(vx,xlp,br) result(zero)
    use class_couplings, only: get_coupling_value_mdl
    use input_mdl, only: zerocut
    integer, intent(in)  :: vx,xlp,br
    logical              :: zero
    real(dp)             :: maxvalue
    integer              :: i

    zero = .false.
    select case (xlp)
    case (TreeBranch:LoopBranch)
      maxvalue = 0d0
      do i = 1,size(vertices(vx)%branch(br)%couplings)
        maxvalue = maxvalue + &
                 abs(get_coupling_value_mdl(vertices(vx)%branch(br)%couplings(i-1)))
      end do
      if (maxvalue/zerocut .lt. 100d0) then
        zero = .true.
      end if
    case(CTBranch)
      maxvalue = 0d0
      do i = 1,size(vertices(vx)%branchCT(br)%couplings)
        maxvalue = maxvalue + &
                 abs(get_coupling_value_mdl(vertices(vx)%branchCT(br)%couplings(i-1)))
      end do
      if (maxvalue/zerocut .lt. 100d0) then
        zero = .true.
      end if
    case(R2Branch)
      maxvalue = 0d0
      do i = 1,size(vertices(vx)%branchR2(br)%couplings)
        maxvalue = maxvalue + &
                 abs(get_coupling_value_mdl(vertices(vx)%branchR2(br)%couplings(i-1)))
      end do
      if (maxvalue/zerocut .lt. 100d0) then
        zero = .true.
      end if
    end select
  end function is_zero_coupling_vertex_mdl

  subroutine get_vertex_ncouplings_mdl(vx,xlp,br,ncouplings)
    integer, intent(in)  :: vx,xlp,br
    integer, intent(out) :: ncouplings
    select case(xlp)
    case(TreeBranch, LoopBranch)
      ncouplings = size(vertices(vx)%branch(br)%couplings)
    case (CTBranch)
      ncouplings = size(vertices(vx)%branchCT(br)%couplings)
    case (R2Branch)
      ncouplings = size(vertices(vx)%branchR2(br)%couplings)
    end select
  end subroutine get_vertex_ncouplings_mdl

  subroutine get_vertex_coupling_ids_mdl(vx,xlp,br,cids)
    integer, intent(in)  :: vx,xlp,br
    integer, intent(out) :: cids(:)
    select case(xlp)
    case(TreeBranch, LoopBranch)
      cids(:) = vertices(vx)%branch(br)%couplings(:)
    case (CTBranch)
      cids(:) = vertices(vx)%branchCT(br)%couplings(:)
    case (R2Branch)
      cids(:) = vertices(vx)%branchR2(br)%couplings(:)
    end select
  end subroutine get_vertex_coupling_ids_mdl
