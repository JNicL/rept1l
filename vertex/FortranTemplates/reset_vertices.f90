    subroutine reset_vertices_mdl
      use class_particles, only: clear_particles_mdl,init_particles_mdl
      use fill_couplings, only: fill_couplings_mdl,fill_CTcouplings_mdl
      use fill_ctparameters, only: init_CTparameters_mdl
      implicit none
      call clear_particles_mdl
      call init_particles_mdl
      call clear_vertices_mdl
      call fill_couplings_mdl
      call init_CTparameters_mdl(.false.)
      call fill_CTcouplings_mdl
      call fill_vertices_mdl
    end subroutine reset_vertices_mdl
