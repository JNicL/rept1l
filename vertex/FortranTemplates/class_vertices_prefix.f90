  use constants_mdl
  use dictionary, only: dict_init
  implicit none
  save

  type Branch
    integer :: lorentz_id
    integer :: perm
    integer, allocatable :: couplings(:)
    integer, dimension(n_orders) :: coupling_order
    integer :: color_id
    integer :: colorfac_id
    ! Propagator extension (only supported for massive vectors) 
    ! 0: Usual propagator
    ! 1: Rxi-propagator part with pole @ M_V^2
    ! 2: Rxi-propagator part with pole @ \xi*M_V^2
    integer :: pext
  end type

  type Vertex
    type(Branch), allocatable :: branch(:)
    type(Branch), allocatable :: branchCT(:)
    type(Branch), allocatable :: branchR2(:)
  end type

  logical :: vertices_init = .false.
  logical :: vertices_filled = .false.
