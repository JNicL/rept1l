    subroutine get_vertex3_mdl(p1, p2, p3, vertex_ret)
      integer, intent(out) :: vertex_ret
      integer, intent(in)  :: p1, p2, p3
      integer(kind=8)      :: config
      integer              :: address
      logical              :: found_vertex

      call createParticleConfig(p1, p2, p3, config)
      call dict(config, 1, address, found_vertex, 0)
      if(found_vertex) then
        vertex_ret = dict_get_value(address)
      else
        vertex_ret = -1
      end if

    end subroutine get_vertex3_mdl
