    function get_colourfac_mdl(cfid)
      integer, intent(in) :: cfid
      complex(dp) :: get_colourfac_mdl
      get_colourfac_mdl = colorcoeffs(cfid)
    end function get_colourfac_mdl
