    function get_lorentz_rank_mdl(l_id) result(ret_lorentz_rank)
      integer, intent(in) :: l_id
      integer :: ret_lorentz_rank
      ret_lorentz_rank = lorentz_rank(l_id)
    end function get_lorentz_rank_mdl
