    use input_mdl, only: nx, error_mdl
    use class_particles,only: n_orders,driver_timestamp
    use fill_ctparameters, only: init_CTparameters_mdl
    use fill_couplings,only: fill_couplings_mdl,fill_CTcouplings_mdl

    ! check if modelfile is compatible to drivers
    if (model_timestamp .ne. driver_timestamp) then
      call error_mdl("Model corrupted. Driver timestamp mismatch.", &
                     where='init_vertices')
    end if

    vertices_init = .true.
    call fill_couplings_mdl
