    function get_fourfermion_id_mdl(l_id) result(ret_ffid)
      integer, intent(in) :: l_id
      integer :: ret_ffid
      ret_ffid = ff_ids(l_id)
    end function get_fourfermion_id_mdl
