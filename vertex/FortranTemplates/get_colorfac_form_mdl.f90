    function get_colourfac_form_mdl(cfid)
      integer, intent(in) :: cfid
      character(len=20) :: get_colourfac_form_mdl
      get_colourfac_form_mdl = colorcoeffs_form(cfid)
    end function get_colourfac_form_mdl
