    subroutine av(vid,bid,cid,cfid,pext,lid,perm,co,cop)
      integer, intent(in) :: vid,bid,cid,cfid,pext,lid,perm
      integer, dimension(0:), intent(in) :: co, cop
      integer scop,i
      vertices(vid)%branch(bid)%color_id = cid
      vertices(vid)%branch(bid)%colorfac_id = cfid
      vertices(vid)%branch(bid)%pext = pext
      vertices(vid)%branch(bid)%lorentz_id = lid
      vertices(vid)%branch(bid)%perm = perm
      vertices(vid)%branch(bid)%coupling_order = co
      scop = size(cop)
      allocate(vertices(vid)%branch(bid)%couplings(0:scop-1))
      do i = 0, scop-1
        vertices(vid)%branch(bid)%couplings(i) = cop(i)
      end do
    end subroutine av

    subroutine avCT(vid,bid,cid,cfid,pext,lid,perm,co,cop)
      integer, intent(in) :: vid,bid,cid,cfid,pext,lid,perm
      integer, dimension(0:), intent(in) :: co, cop
      integer scop,i
      vertices(vid)%branchCT(bid)%color_id = cid
      vertices(vid)%branchCT(bid)%colorfac_id = cfid
      vertices(vid)%branchCT(bid)%pext = pext
      vertices(vid)%branchCT(bid)%lorentz_id = lid
      vertices(vid)%branchCT(bid)%perm = perm
      vertices(vid)%branchCT(bid)%coupling_order = co
      scop = size(cop)
      allocate(vertices(vid)%branchCT(bid)%couplings(0:scop-1))
      do i = 0, scop-1
        vertices(vid)%branchCT(bid)%couplings(i) = cop(i)
      end do
    end subroutine avCT

    subroutine avR2(vid,bid,cid,cfid,pext,lid,perm,co,cop)
      integer, intent(in) :: vid,bid,cid,cfid,pext,lid,perm
      integer, dimension(0:), intent(in) :: co, cop
      integer scop,i
      vertices(vid)%branchR2(bid)%color_id = cid
      vertices(vid)%branchR2(bid)%colorfac_id = cfid
      vertices(vid)%branchR2(bid)%pext = pext
      vertices(vid)%branchR2(bid)%lorentz_id = lid
      vertices(vid)%branchR2(bid)%perm = perm
      vertices(vid)%branchR2(bid)%coupling_order = co
      scop = size(cop)
      allocate(vertices(vid)%branchR2(bid)%couplings(0:scop-1))
      do i = 0, scop-1
        vertices(vid)%branchR2(bid)%couplings(i) = cop(i)
      end do
    end subroutine avR2
