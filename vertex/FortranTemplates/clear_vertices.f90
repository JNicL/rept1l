  subroutine clear_vertices_mdl
    use class_vertices, only:n_vertices,vertices,vertices_filled
    implicit none
    integer :: i
    do i = 0, n_vertices-1
      if (allocated(vertices(i)%branch)) then
        deallocate(vertices(i)%branch)
      end if
      if (allocated(vertices(i)%branchCT)) then
        deallocate(vertices(i)%branchCT)
      end if
      if (allocated(vertices(i)%branchR2)) then
        deallocate(vertices(i)%branchR2)
      end if
    end do
    vertices_filled = .false.
  end subroutine clear_vertices_mdl
