#==============================================================================#
#                              class_vertices.py                               #
#==============================================================================#

""" Generator for the Fortran module class_vertices.py """

#============#
#  Includes  #
#============#

import os
import sys

from rept1l.pyfort import subBlock, modBlock, loadBlock, spaceBlock, ifBlock
from rept1l.pyfort import containsBlock, selectBlock, caseBlock, interfaceBlock

#=============#
#  structure  #
#=============#

#/-------------------------\#
# module class_vertices     #
#                           #
#  prefix                   #
#                           #
#  contains                 #
#                           #
#   subroutines getVertex   #
#                           #
#   subroutine initVertices #
#                           #
#   subroutine checkVertices#
#                           #
# end module                #
#\-------------------------/#

def generate_current(multiplicity, tabs=2):
  """ Generates the createCurrent subroutines for a given particle
  `multiplicity`.
  """
  if multiplicity <= 1:
    raise Exception('Multiplicity in generateCurrent must ' +
                    'be greater or equal 2.')

  sub_name = 'createCurrent'
  subs_names = list(map(lambda x: sub_name + str(x), range(2, multiplicity+1)))

  ib = interfaceBlock(sub_name, tabs=tabs)
  mods = ', '.join(subs_names)
  ib + ('module procedure ' + mods)

  subs = []
  for i, sub in enumerate(subs_names):
    nparticles = i + 2
    particle_names = map(lambda x: 'P' + str(x), range(1, nparticles+1))
    p_joined = ', '.join(particle_names)
    args = p_joined + ', id'
    sb = subBlock(sub, args + ', check', tabs=tabs)
    sb.addType(('integer, intent(in)', args))
    sb.addType(('logical, intent(in)', 'check'))
    sb.addType(('integer(kind=8)', 'key'))
    sb.addType(('integer', 'res,address'))
    sb.addType(('logical', 'found'))
    sb + ('call createParticleConfig(' + p_joined + ', key)')
    ifncheck = ifBlock('.not. check', tabs=tabs+1)
    sb + ifncheck
    ifncheck + 'call dict(key, 2, address, found, id)'
    (ifncheck > 0) + 'else'
    ifncheck + 'call dict(key, 1, address, found, 0)'
    ifncheck + 'res = dict_get_value(address)'
    ifncheck + 'write (*,*) "out=", res, found, address, "shouldbe = ", id'
    iff = ifBlock('res /= id', tabs=tabs+2)
    ifncheck + iff
    iff + 'stop 9'

    subs.append(sb)

  return ib, subs


def gen_get_vertex_lorentz_rank(tabs=1):
  """ Generates the get_lorentz_rank_mdl subroutine which returns the maximal
  rank increase (rm) for a given lorentz structure (ty) and a list of logicals
  (cc) indicating whether couplings are zero or not.
  """
  glr = subBlock('get_vertex_lorentz_rank_mdl', 'ty,cc,mr', tabs=tabs+1)
  glr.addType(('integer, intent(in)', 'ty'))
  glr.addType(('logical, intent(in)', 'cc(0:)'))
  glr.addType(('integer, intent(out)', 'mr'))
  sb = selectBlock('ty', tabs=tabs+2)
  spb = spaceBlock()
  sb + spb
  cbd = caseBlock('default', tabs=tabs+2)
  sb + cbd
  cbd + 'mr = 0'
  glr + sb

  return glr, spb

#==============================================================================#
#                               gen_vertex_class                               #
#==============================================================================#

def gen_vertex_class(CT=False, R2=False, tabs=1):
  path = os.path.join(os.environ['REPT1L_PATH'], 'vertex')
  path = os.path.join(path, 'FortranTemplates')
  class_vertices = modBlock('class_vertices', tabs=0)
  contains_vertices = containsBlock(tabs=tabs)

  function_init_vertices = subBlock('init_vertices_mdl', tabs=tabs)
  functions = ['get_timestamp', 'get_lorentz_rank', 'get_propagator_extension',
               'get_max_leg_multiplicitiy', 'get_fourfermion_id',
               'is_model_init', 'is_model_filled',
               'create_currents', 'check_vertices',
               'assign_vertex_functions', 'get_colorfac_mdl',
               'get_colorfac_form_mdl', 'vertex_methods']

  for func in functions:
    funcpyfort = loadBlock(os.path.join(path, func + '.f90'))
    contains_vertices + funcpyfort
    contains_vertices + '\n'

  # hierarchy
  class_vertices + '\n'
  class_vertices + contains_vertices

  sub_get_lorentz_rank, sc_get_lorentz_rank = gen_get_vertex_lorentz_rank(tabs=tabs)

  contains_vertices + function_init_vertices
  contains_vertices + '\n'
  contains_vertices + sub_get_lorentz_rank

  class_vertices.addPrefix('use class_particles, only: n_orders', tabs=tabs)

  #  TODO:  <21-08-19, J.-N. Lang> #
  # is this needed?
  # if CT or R2:
  #   class_vertices.addPrefix('use class_ctparameters', tabs=tabs)

  cv_prefix = loadBlock(os.path.join(path, 'class_vertices_prefix.f90'))
  class_vertices.addPrefix(cv_prefix)
  vertex_modules = spaceBlock()
  class_vertices + vertex_modules
  initvpath = os.path.join(path, 'init_vertices_prefix.f90')
  function_init_vertices.addPrefix(loadBlock(initvpath))
  if CT or R2:
    function_init_vertices + 'call init_CTparameters_mdl(.true.)'
    function_init_vertices + 'call fill_CTcouplings_mdl'

  return {'class_vertices': class_vertices, 'vertex_modules': vertex_modules,
          'function_init_vertices': function_init_vertices,
          'sub_get_lorentz_rank': sub_get_lorentz_rank,
          'sc_get_lorentz_rank': sc_get_lorentz_rank}
