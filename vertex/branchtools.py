#==============================================================================#
#                                  branch.py                                   #
#==============================================================================#

from six import iteritems

#============#
#  Includes  #
#============#

from rept1l.combinatorics import permute_lorentz_base, permute_spins
from rept1l.colorflow import permute_colorid

#==============================================================================#
#                                update_branch                                 #
#==============================================================================#

def update_branch(branch_dict, branch_entry, type='particle'):
  """ Enters(merges) a new `branch_entry` in the global `branch_dict`.

  Branches are nested dictionaries with the structure:

    - particle_branch = {particle_key: color_branch, ...}
    - color_branch    = {color_key: lorentz_branch, ...}
    - lorentz_branch  = {lorentz_key: sum of couplings, ...}

  Merging particle branches also requires to merge color_branches, merging color
  branches also requires to merge lorentz_branches and merging lorentz_branches
  is simple, since only the couplings need to be summed for a specific lorentz
  key.

  :param branch_dict: The branch dictionary to-be updated
  :type  branch_dict: dict

  :param branch_entry: A new branch to be merged with `branch_dict`
  :type  branch_entry: dict

  :param type: The branch type. Must be `particle`, `color` or `lorentz`
  :type  type: str
  """

  assert(type in ['particle', 'color', 'lorentz'])

  update = {'particle': lambda x, y: update_branch(x, y, type='color'),
            'color': lambda x, y: update_branch(x, y, type='lorentz'),
            'lorentz': lambda x, y: x+y}

  bd = branch_dict.copy()
  for key, val in iteritems(branch_entry):
    # key already exists -> merging required
    if key in bd:
      bd[key] = update[type](bd[key], val)
    # no merging required
    else:
      bd[key] = val
  return bd

#==============================================================================#
#                            permute_lorentz_branch                            #
#==============================================================================#

cached_lorentz_permutations = {}

def permute_lorentz_branch(lorentz_branch, permutation, form_simplify=True):
  """ Permutes the particles in `lorentz_branch` according to `permutation`.

  :param lorentz_branch: A lorentz branch is a dict with keys specifying a sum
                         lorentz structure and the values are couplings.
  :type  lorentz_branch: dict

  :param permutation: A permutation of particles starting with 0.
                      E.g. [1, 0, 2, 3]
  :type  permutation: list


  Usage:

  Fist we derive the lorentz branch `lb` given a lorentz structure `ls` and
  coupling `C`
  >>> from sympy import Symbol; C = Symbol('C')
  >>> ls = 'Gamma(3,2,1)'
  >>> from rept1l.baseutils import CurrentBase
  >>> lb = tuple(tuple(u) for u in CurrentBase(ls).compute(range(3)))

  The result for the lorentz base key is dependent on the numbering of base
  elements.
  >>> GammaM=Symbol('GammaM');GammaP=Symbol('GammaP')

  >>> r1 = ((3*GammaM, (5, 1)), (3*GammaP, (5, 1)))
  >>> r2 = ((3*GammaP, (5, 1)), (3*GammaM, (5, 1)))
  >>> lb == r1 or lb == r2
  True
  >>> ls_key = lb

  The complete lorentz branch:
  >>> lbb = {ls_key: C}
  >>> plbb = permute_lorentz_branch(lbb, [0,2,1])
  >>> plbb == {((3*GammaM, (4, 1)), (3*GammaP, (4, 1))): C}\
or  plbb == {((3*GammaP, (4, 1)), (3*GammaM, (4, 1))): C}
  True
  """
  ret = {}
  from rept1l.baseutils import CurrentBase
  for ls_key in lorentz_branch:
    compute_perm = True
    if ls_key in cached_lorentz_permutations:
      if tuple(permutation) in cached_lorentz_permutations[ls_key]:
        compute_perm = False
        new_ls_key = cached_lorentz_permutations[ls_key][tuple(permutation)]

    if compute_perm:
      new_ls_key = []
      perm_lbs = [permute_lorentz_base(lb, permutation) for lb in ls_key]
      if form_simplify:
        perm_lbs = CurrentBase.form_simplify_lbs(perm_lbs, reduce_dalgebra=False)
      new_ls_key = tuple(sorted((tuple(u) for u in perm_lbs), key=lambda x: x[1][0]))
      if ls_key not in cached_lorentz_permutations:
        cached_lorentz_permutations[ls_key] = {}
        cached_lorentz_permutations[ls_key][tuple(permutation)] = new_ls_key

    if new_ls_key not in ret:
      ret[tuple(new_ls_key)] = lorentz_branch[ls_key]
    else:
      ret[tuple(new_ls_key)] += lorentz_branch[ls_key]
  return ret

#==============================================================================#
#                             permute_color_branch                             #
#==============================================================================#

def permute_color_branch(color_branch, permutation):
  """ Permutes the particles in `color_branch` according to `permutation`.

  :param color_branch: A color branch is a dict with color ids as keys which
                       specify the colorflow and the values are lorentz
                       branches.
  :type  color_branch: dict

  :param permutation: A permutation of particles starting with 0.
                      E.g. [1, 0, 2, 3]
  :type  permutation: list
  """
  ret = {}

  for color in color_branch:
    cs_id, colorfactor = color
    new_cs_id = permute_colorid(cs_id, permutation)
    new_color = (new_cs_id, colorfactor)
    lorentz_branch = color_branch[color]
    new_lorentz_branch = permute_lorentz_branch(lorentz_branch, permutation)
    ret[new_color] = new_lorentz_branch
  return ret

#==============================================================================#
#                           permute_particle_branch                            #
#==============================================================================#

def permute_particle_branch(particle_branch, perm):
  """ Permutes the particles in `particle_branch` according to `permutation`.

  :param particle_branch: A particle branch is a dict with particle tuples as
                          keys and the values are color branches.
  :type  particle_branch: dict

  :param perm: A permutation of particles starting with 0.
                      E.g. [1, 0, 2, 3]
  :type  perm: list

  Usage:

  See permute_lorentz_branch to see how to construct a lorentz branch
  >>> from sympy import Symbol; C = Symbol('C')
  >>> ls = 'Gamma(3,2,1)'
  >>> from rept1l.baseutils import CurrentBase
  >>> lb = tuple(tuple(u) for u in CurrentBase(ls).compute(range(3)))
  >>> ls_key = lb
  >>> lbb = {ls_key: C}

  The lorentz branch is dependent on the lorentz basis, since the order of the
  base elements is not fixed, the result can be either of the following two
  results

  >>> GammaM=Symbol('GammaM');GammaP=Symbol('GammaP')
  >>> r1 = ((3*GammaM, (5, 1)), (3*GammaP, (5, 1)))
  >>> r2 = ((3*GammaP, (5, 1)), (3*GammaM, (5, 1)))
  >>> lbb == {r1: C} or lbb == {r2: C}
  True

  Construct the color branch. First we create an entry in
  the colorflow dictionary of the form: (colorflow, colorfactor): color id
  () represents no colorflow
  >>> from rept1l.colorflow import ColorflowVertex
  >>> ColorflowVertex.colors_dict = {((), 1): 0}


  The color branch is a dictionary with keys (colorflow, colorfactor) and
  lorentz branches as values
  >>> cbb = {(0, 1): lbb}

  Next we define some particles
  >>> p1, p2, p3 = Symbol('p1'), Symbol('p2'), Symbol('p3')

  The particle branch is a dictionary with particle tuples as keys and color
  branches as values
  >>> pbb = {(p1, p2, p3): cbb}

  The lorentz branch is dependent on the lorentz basis.
  >>> ppbb = permute_particle_branch(pbb, [0,2,1])
  >>> r1 = {((3*GammaM, (4, 1)), (3*GammaP, (4, 1))): C}
  >>> r2 = {((3*GammaP, (4, 1)), (3*GammaM, (4, 1))): C}
  >>> p1key = {(p1, p3, p2): {(0, 1): r1}}
  >>> p2key = {(p1, p3, p2): {(0, 1): r2}}
  >>> ppbb == p1key or ppbb == p2key
  True
  """
  ret = {}
  for pkey in particle_branch:
    new_pkey = tuple(permute_spins([u for u in pkey], perm))
    color_branch = particle_branch[pkey]
    new_color_branch = permute_color_branch(color_branch, perm)
    ret[new_pkey] = new_color_branch
  return ret

if __name__ == "__main__":
  import doctest
  doctest.testmod()
