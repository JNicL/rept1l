#==============================================================================#
#                                 modelfile.py                                 #
#==============================================================================#
""" Searches and imports the FeynRules modelfile. """

#============#
#  Includes  #
#============#

import sys
import os


#===========#
#  Globals  #
#===========#

def get_modelpath():

  import rept1l_config
  if hasattr(rept1l_config, 'rept1l_environ'):
    model_env_name = rept1l_config.rept1l_environ
  else:
    model_env_name = 'REPTIL_MODEL_PATH'

  directory = os.path.expandvars(os.environ[model_env_name])
  found_UFO = False
  for folder in os.listdir(directory):
    if os.path.isdir(directory + '/' + folder):
      m = 'object_library.py' in os.listdir(os.path.join(directory, folder))
      if m and found_UFO:
        raise Exception('Unable to decide which UFO model to load. ' +
                  'Found `object_library.py` in folder ' +
                  found_UFO + ' and ' + folder + '.')
      elif m:
        found_UFO = folder
  if found_UFO:
    return directory, found_UFO
  else:
    raise Exception('Unable to locate ' +
                    '`object_library.py` in any folder in directory ' +
                    os.environ[model_env_name])


def import_model():
  path, modelname = get_modelpath()
  sys.path.append(path)
  return __import__(modelname)


model = import_model()
object_library = model.object_library

Vertex = object_library.Vertex
try:
  CTVertex = object_library.CTVertex
except AttributeError:
  CTVertex = None
  object_library.all_CTvertices = []

  class CTVertex(object_library.UFOBaseClass):

    require_args = ['name', 'particles', 'color', 'lorentz', 'couplings', 'type', 'loop_particles']

    def __init__(self, name, particles, color, lorentz, couplings, type,
                 loop_particles, **kwargs):
        args = (name, particles, color, lorentz, couplings, type, loop_particles)
        super(CTVertex, self).__init__(self, *args, **kwargs)
        args = (particles, color, lorentz, couplings, type, loop_particles)
        object_library.all_CTvertices.append(self)

Parameter = object_library.Parameter


class CTParameter(Parameter):
  def __init__(self, *args, **kwargs):
    super(CTParameter, self).__init__(*args, **kwargs)


class TreeInducedCTVertex(CTVertex):
  def __init__(self, expand_couplings=True, expand_wavefunctions=True,
               *args, **kwargs):
    if 'type' not in kwargs:
      kwargs['type'] = 'treeinducedct'
    if 'loop_particles' not in kwargs:
      kwargs['loop_particles'] = 'N'
    super(TreeInducedCTVertex, self).__init__(*args, **kwargs)
    self.expand_couplings = expand_couplings
    self.expand_wavefunctions = expand_wavefunctions

