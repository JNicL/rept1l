###############################################################################
#                             auto_tadpole_ct.py                              #
###############################################################################

##############
#  Includes  #
##############

from sympy import Symbol, Rational
from functools import reduce
from rept1l.baseutils import CurrentBase
from rept1l.combinatorics import get_lorentz_key_components, order_strucs
from rept1l.combinatorics import lorentz_base_dict
from rept1l.combinatorics import reconstruct_lorentz
from rept1l.combinatorics import permute_lorentz_base
from rept1l.combinatorics import pair_arguments
from rept1l.helper_lib import substitute_integer_string
from rept1l.helper_lib import spin_to_lorentz
from rept1l.helper_lib import parse_UFO
from rept1l.helper_lib import export_particle_name
from rept1l.logging_setup import log
from rept1l.pyfort import ProgressBar
from .modelfile import model, CTVertex, CTParameter

#############
#  Globals  #
#############

model_objects = model.object_library
Coupling = model_objects.Coupling
Lorentz = model_objects.Lorentz

# list of couplings which have been instanced. Preventing to generate other
# instances.
couplings_regged = {}

#############
#  Methods  #
#############

def assign_FJTadpole_field_shift(particle, dt_name):
  if type(particle) != model_objects.Particle:
    raise Exception('Particle has to be of type `Particle`')
  m = particle.mass.name
  ct = CTParameter(name=dt_name,
                   nature='external',
                   type='real',
                   value='-' + dt_name + '/' + m + '**2',
                   texname=dt_name,
                   lhablock='CTs',
                   lhacode=[2])
  particle.field_shift = ct
  log('Assigned field shift ' + ct.value + ' to particle ' + particle.name,
      'debug')

#==============================================================================#

def auto_tadpole_twopoint_vertices(io_v=None, io_c=None, io_l=None):
  """ Generates the tadpole counterterms to two-point function in the
  FJ-tadpole scheme """

  check = any(is_field_shifted(u) for u in model_objects.all_particles)
  if not check:
    log('Building tadpole counterterms has no effect. ' +
        'No particle has a field shift assigned', 'warning')
    return

  log('Building two-point tadpole counterterms.', 'info')
  PG = ProgressBar(len(model_objects.all_vertices))
  for v in sorted(model_objects.all_vertices, key=lambda x: x.name):
    build_tadpole_vertex(v, only_2point=True, io_v=io_v, io_c=io_c, io_l=io_l)
    PG.advanceAndPlot(v.name)

#==============================================================================#

def auto_tadpole_vertices(io_v=None, io_c=None, io_l=None):
  """ Generates all tadpole counterterms in the FJ-tadpole scheme """

  check = any(is_field_shifted(u) for u in model_objects.all_particles)
  if not check:
    log('Building tadpole counterterms has no effect. ' +
        'No particle has a field shift assigned', 'warning')
    return

  log('Building all tadpole counterterms.', 'info')
  PG = ProgressBar(len(model_objects.all_vertices))
  for v in sorted(model_objects.all_vertices, key=lambda x: x.name):
    build_tadpole_vertex(v, io_v=io_v, io_c=io_c, io_l=io_l)
    PG.advanceAndPlot(v.name)

#==============================================================================#

def is_field_shifted(particle):
  return hasattr(particle, 'field_shift')

#==============================================================================#

def factorial(n):
  return reduce(lambda x, y: x*y, [1] + list(range(1, n+1)))

#==============================================================================#

def gen_ctvertex_by_substitution(vertex, particles, lorentz, couplings,
                                 color=None, name_suffix='', io_v=None):
  """ Generates a counterterm vertex starting from the template vertex and
  substituting the elements by new ones.  """

  # define redundant stuff for UFO
  type = 'ctvertex'
  loop_particles = 'N'
  induced_name = 'V_TS_' + vertex.name + name_suffix
  r = CTVertex(name=induced_name,
               particles=particles,
               color=color,
               type=type,
               loop_particles=loop_particles,
               lorentz=lorentz,
               couplings=couplings)
  if io_v:
    head = induced_name + ' = CTVertex('
    lines = [head + 'name="' + induced_name + '",\n',
             len(head)*' ' + 'particles=[' + ', '.join('P.' + u.__repr__()
                                                       for u in particles) +
             '],\n',
             len(head)*' ' + 'color=' + str(color) + ',\n',
             len(head)*' ' + 'type="' + type + '",\n',
             len(head)*' ' + 'loop_particles="' + loop_particles + '",\n',
             len(head)*' ' + 'lorentz=[' + ', '.join('LCT.' + u.name
                                                     for u in lorentz) +
             '],\n',
             len(head)*' ' + 'couplings=' + str(couplings) + ')\n', '\n']
    io_v.writelines(lines)
  return r

#==============================================================================#

def set_momentum_zero(structure, i, nparticles):
  """ For a given structure, set_momentum_zero returns the structure where
  the momentum associated to particle i is set to zero. """

  lbs = CurrentBase(structure).compute(range(nparticles), form_simplify=False)
  new_lbs = []
  if i != nparticles:
    perm = list(range(nparticles))
    perm[i-1] = nparticles-1
    perm[nparticles-1] = i-1
    i = nparticles
  else:
    perm = range(nparticles)
  for lb in lbs[:]:
    lb = permute_lorentz_base(lb, perm)
    strucs, _ = get_lorentz_key_components(lb[0])
    strucs = order_strucs(strucs)
    P = Symbol('P')
    is_zero = False
    for spos, struc in enumerate(strucs):
      if struc == P and lorentz_base_dict[lb[0]][lb[1][0]][spos][1] == i:
        is_zero = True
        break
    if not is_zero:
      new_lbs.append(lb)

  ls = [u[0] for u in new_lbs]
  args = [[lorentz_base_dict[u[0]][u[1][0]]]
          if lorentz_base_dict[u[0]] != [] else [[]] for u in new_lbs]
  pref = [[u[1][1]] for u in new_lbs]

  ret, _ = reconstruct_lorentz(ls, args, pref)
  r = ''
  for upos, u in enumerate(ret):
    if upos == 0:
      r = u[0]
    elif u[0][0] == '-':
      r += ' ' + u[0]
    else:
      r += ' + ' + u[0]
  return r

#############
#  find_ls  #
#############

def find_ls(ls_str, spins):
  """ Returns the lorentz structure instance which string and spins matches
  exaclty `ls_str` and `spins`, respectively. """
  for ls in model_objects.all_lorentz:
    if ls.structure == ls_str and ls.spins == spins:
      return ls

################
#  replace_ls  #
################

def replace_ls(ls_str, spins, io_l=None):
  """ Returns a lorentz structure instance assoicated to `ls_str` and
  `spins`. If no suited match is found a new one is created. """
  ls = find_ls(ls_str, spins)
  if ls is None:
    ls_name_base = spin_to_lorentz(spins)
    similar_strucs = [u.name for u in model_objects.all_lorentz
                      if ls_name_base in u.name]
    ls_name_index = len(similar_strucs) + 1
    ls_name = ls_name_base + str(ls_name_index)

    # make sure the name is not taken yet
    while ls_name in similar_strucs:
      ls_name_index += 1
      ls_name = ls_name_base + str(ls_name_index)

    ls = Lorentz(name=ls_name, spins=spins, structure=ls_str)
    # write the lorentz instance if IO given
    if io_l:
      head = ls_name + ' = Lorentz('
      lines = [head + 'name="' + ls_name + '",\n',
               len(head)*' ' + 'spins=' + str(spins) + ',\n',
               len(head)*' ' + 'structure="' + ls_str + '")\n', '\n']
      io_l.writelines(lines)
  return ls

##########################
#  build_tadpole_vertex  #
##########################

def build_tadpole_vertex(vertex, only_2point=False, io_v=None, io_c=None,
                         io_l=None):
  """ Generates a tadpole counterterm vertex from `vertex` by replacing Higgs
  fields with predefined constant field shifts H -> \Delta v.

  The field shift should be given as a `Particle` attribute of type
  `CTParameter`. The field shifts \Delta v_i can be related to the
  tadpole counterterms t_i which is precisely the FJ Tadpole scheme.

  :param vertex: UFO vertex instance
  :type  vertex: Vertex

  Optionally, the additional UFO instances can be written to txt files

  :io_v: an IO file object for writing UFO CT vertices as txt files
  :io_c: an IO file object for writing UFO couplings as txt files
  :io_l: an IO file object for writing UFO lorentz as txt files
  """
  # count how many particles have a field shift assigned
  nparticles_vev = len([u for u in vertex.particles if is_field_shifted(u)])

  # compute symmetry factor due to multiple occurence of the same field-shifted
  # particle
  symfac = reduce(lambda x, y: x*y, [factorial(vertex.particles.count(u))
                                     for u in set(vertex.particles)])

  def is_background_field(p):
    if not hasattr(p, 'quantumfield'):
      return True
    else:
      return not p.quantumfield

  # no vev -> no tadpole contribution
  if nparticles_vev == 0:
    return

  # no ct with ghosts at one-loop no tadpole contribution
  elif any(u < 0 for u in vertex.lorentz[0].spins):
    return

  # ct only with background fields necessary. FJ shift only done for background
  # fields
  elif not all(is_background_field(u) for u in vertex.particles):
    return

  # only 3-point vertices contribute to the 2-point  tadpole contribution at
  # one-loop order
  elif only_2point and len(vertex.particles) != 3:
    return

  def count_particles(p):
    return len([u for u in p if type(u) is not tuple])

  def count_vevs(p):
    return len([u for u in p if type(u) is tuple])

  # shift all particles which have a field shift assigned and collect all
  # resulting vertices.
  paired_particles = pair_arguments([(u, (u.field_shift, pos+1))
                                     if is_field_shifted(u) else (u,)
                                     for pos, u in enumerate(vertex.particles)],
                                    as_list=False)

  # At one-loop we only need to consider one field-shift at a time
  if only_2point:
    paired_particles = (u for u in paired_particles if count_particles(u) == 2
                        and count_vevs(u) == 1)
  else:
    paired_particles = (u for u in paired_particles if count_vevs(u) == 1)

  # Collect all resulting structures. Structures which have the same particle
  # content are merged, e.g.  given the Vertex  V V H H, shifting the Higgs
  # fields results in  V V dv H + V V H dv = 2 * dv * V V H. The factor 2 drops
  # out for the Feynman rule.

  tadpole_vertices = {}
  for pp in paired_particles:
    p = tuple(u for u in pp if type(u) is not tuple)
    p_elim = [u[1] for u in pp if type(u) is tuple][0]
    new_spins = vertex.lorentz[0].spins[:]
    new_spins.pop(p_elim-1)

    nparticles = len(vertex.particles)
    nsubs = {u: u-1 for u in range(nparticles, p_elim, -1)}

    if hasattr(vertex, 'color'):
      new_color = tuple([substitute_integer_string(u, nsubs)
                         if u != '1' else u for u in vertex.color])
    else:
      new_color = None

    # eliminates the particle in the lorentz structure which has been
    # substituted by a vev. For instance, the momentum of the particle has to
    # be set to zero.
    def subs_ls(ls, p):
      if ls == '1':
        return ls
      else:
        return set_momentum_zero(ls, p, len(vertex.particles))
    new_lorentz_structure = tuple([subs_ls(l.structure, p_elim)
                                   for l in vertex.lorentz])

    new_symfac = reduce(lambda x, y: x*y,
                        [factorial(p.count(u)) for u in set(p)])
    new_symfac = Rational(new_symfac, symfac)

    # extract field shift value from parameter
    vevct = parse_UFO([u[0] for u in pp
                      if type(u) is tuple][0].value)

    vkey = (p, new_color)
    if vkey not in tadpole_vertices:
      tadpole_vertices[vkey] = {new_lorentz_structure: vevct*new_symfac,
                                'origin': vertex.particles[p_elim-1]}
    else:
      # the structures originate from the same particle -> vevshift transf.
      assert(tadpole_vertices[vkey]['origin'] == vertex.particles[p_elim-1])

      # try to merge them, as they have the same particle and color key
      matched = False
      for prev_lorentz_structure in tadpole_vertices[vkey]:
        if prev_lorentz_structure == 'origin':
          continue
        for cpos in range(len(new_color)):
          l1 = []
          for lspos, ls in enumerate(new_lorentz_structure):
            if ls != '' and (cpos, lspos) in vertex.couplings:
              cs = vertex.couplings[(cpos, lspos)]
              if type(cs) is not list:
                l1.append(cs.name)
              else:
                l1.append([u.name for u in cs])

          l2 = []
          for lspos, ls in enumerate(prev_lorentz_structure):
            if ls != '' and (cpos, lspos) in vertex.couplings:
              cs = vertex.couplings[(cpos, lspos)]
              if type(cs) is not list:
                l2.append(cs.name)
              else:
                l2.append([u.name for u in cs])

          if l1 == l2:
            tadpole_vertices[vkey][prev_lorentz_structure] += vevct*new_symfac
            matched = True
            break
          else:
            l1 = sorted(l1, key=lambda x: x[1])
            l2 = sorted(l2, key=lambda x: x[1])
            if l1 == l2:
              tadpole_vertices[vkey][prev_lorentz_structure] += vevct*new_symfac
              matched = True
              break

      # Matching has to work due to consistency. Since Feynman rules are
      # permutation invariant, the derivation of tadpole counterterms can always
      # be derived by only shifting one Higgs field if the same Higgs field
      # appears multiple times.
      if not matched:
        print ('Not matched!')
        print("l1, l2:", l1, l2)
        print("l1 == l2:", l1 == l2)
        print("new_color:", new_color)
        print("vertex.color:", vertex.color)
        print("vertex.name:", vertex.name)
        import sys
        sys.exit()

  # construct the UFO vertices
  induced_CTVertices = []
  for tv in tadpole_vertices:
    p, co = tv
    # there has to be only one lorentz structure, otherwise merging did not work
    len_TV = list(tadpole_vertices[tv].keys())
    len_TV.remove('origin')
    len_TV = len(len_TV)
    assert(len_TV == 1)
    origin = export_particle_name(tadpole_vertices[tv]['origin'].name)

    for ls in tadpole_vertices[tv]:
      if ls == 'origin':
        continue
      vevctfac = tadpole_vertices[tv][ls]
      # derived couplings
      new_c = {}
      # lorentz structure offset due to vanishing structures when setting
      # momentum to zero
      ls_offset = 0
      # coupling keys sorted in the order of the lorentz structures
      vertex_coupling_keys = sorted(vertex.couplings.keys(), key=lambda x: x[1])
      for cpos, ckey in enumerate(vertex_coupling_keys):
        # check for vanishing structure
        if new_lorentz_structure[cpos] == '':
          ls_offset += 1
          continue

        # build derived coupling by multplying vertex coupling with field shift
        vc = vertex.couplings[ckey]
        vevct = vevctfac.free_symbols.pop()
        if type(vc) is not list:
          induced_values = ['' + vc.name + '*(' + str(vevctfac) + ')']
          vcs = [vc]
        else:
          induced_values = ['' + u.name + '*(' + str(vevctfac) + ')'
                            for u in vc]
          vcs = vc

        for vpos, induced_value in enumerate(induced_values):
          # check for cached coupling, unnecessary to instance the same coupling
          # twice
          if induced_value in couplings_regged:
            GC = couplings_regged[induced_value]
          else:
            # by default the order of the coupling is zero and automatically
            # determined by REPT1L
            new_order = {u.name: 0 for u in model_objects.all_orders}
            # construct unique name for coupling - composed of the particle
            # name which has been replaced by the vevshift and the original
            # coupling name
            induced_name = 'GC_TS_' + vcs[vpos].name + '_' + origin
            # this coupling is not allowed to be present in the regged
            # couplings or something went wrong since we checked for
            # induced_value
            assert(induced_name not in
                   [u.name for u in couplings_regged.values()])

            GC = Coupling(name=induced_name,
                          value=induced_value,
                          order=new_order)
            # write the coupling instance if IO given
            if io_c is not None:
              head = induced_name + ' = Coupling('
              lines = [head + 'name="' + induced_name + '",\n',
                       len(head)*' ' + 'value=' + '"' + induced_value + '",\n',
                       len(head)*' ' + 'order=' + str(new_order) + ')\n', '\n']
              io_c.writelines(lines)

            couplings_regged[induced_value] = GC

        # new coupling key which related coupling, color and lorentz structure
        ckey_new = (ckey[0], ckey[1] - ls_offset)
        new_c[ckey_new] = [couplings_regged[induced_value]
                           for induced_value in induced_values]
        if len(new_c[ckey_new]) == 1:
          new_c[ckey_new] = new_c[ckey_new][0]

      # find/generate the needed lorentz structures
      new_ls = [replace_ls(u, new_spins, io_l=io_l) for u in ls if u != '']
      if co is not None:
        co = [u for u in co]

      name_suffix = '_' + origin

      # write the UFO vertex instance
      ctv = gen_ctvertex_by_substitution(vertex, list(p), new_ls, new_c,
                                         color=co, name_suffix=name_suffix,
                                         io_v=io_v)
      induced_CTVertices.append(ctv)
  return induced_CTVertices
