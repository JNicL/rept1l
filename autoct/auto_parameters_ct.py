#==============================================================================#
#                            auto_parameters_ct.py                             #
#==============================================================================#

#============#
#  Includes  #
#============#

from six import iteritems
from rept1l.combinatorics import ProjM, ProjP
from rept1l.helper_lib import get_anti_particle, is_anti_particle
from rept1l.helper_lib import export_particle_name
from rept1l.logging_setup import log

from .modelfile import model, TreeInducedCTVertex, CTParameter
from . import couplings_ct as C
from . import lorentz_ct as LCT

#===========#
#  Globals  #
#===========#

parameters = model.parameters
object_library = model.object_library
model_objects = model.object_library
Parameter = object_library.Parameter
Coupling = object_library.Coupling

#############
#  Methods  #
#############

def add_counterterm(ct_name, ct_value):
  """ Initialize a new counterterm parameter as UFO CTParameter with name
  `ct_name` and value `ct_value`.
  """
  ct = CTParameter(name=ct_name,
                   nature='external',
                   type='real',
                   value=ct_value,
                   texname=ct_name,
                   lhablock='CTs',
                   lhacode=[1])
  return ct

#------------------------------------------------------------------------------#

def assign_counterterm(parameter, ct_name, ct_value):
  """ Assigns a counterterm parameter with name `ct_name` and value `ct_value`
  to the UFO parameter `parameter` """
  if type(parameter) != Parameter:
    raise Exception('parameter has to be of type `Parameter`')

  ct = add_counterterm(ct_name, ct_value)
  if hasattr(parameter, 'counterterm'):
    log('parameter ' + parameter.name + ' already has a counterterm assigned',
        'error')
    log('counterterm: ' + parameter.counterterm.name,
        'error')
    raise Exception('parameter ' + parameter.name +
                    ' already has a counterterm assigned')

  parameter.counterterm = ct
  log('Assigned counterterm ' + ct.name + ' with value ' + ct.value +
      ' to parameter ' + parameter.name, 'debug')

#==============================================================================#

def set_light_particle(particle):
  """ Tags a particle as a light particle. """
  particle.light_particle = True
  aparticle = get_anti_particle(particle, model_objects.all_particles)
  if aparticle is not particle:
    aparticle.light_particle = True

#==============================================================================#

def is_goldstone_boson(particle):
  """ Returns `True` if `particle` is a goldstone boson else `False`. """
  if particle.spin == 1:
    if hasattr(particle, 'goldstone') and particle.goldstone is True:
      ret = True
    else:
      ret = False

    # check whether the antiparticle is a goldstone boson as FeynRules object
    # library does not copy the goldstone attribute to the generation of
    # antiparticles.
    if ret is False:
      aparticle = get_anti_particle(particle, model_objects.all_particles)
      if aparticle is not particle:
        if hasattr(aparticle, 'goldstone') and aparticle.goldstone is True:
          ret = True
        else:
          ret = False
  else:
    ret = False

  return ret

#==============================================================================#

def get_yukawa_parameter(all_parameters, all_masses):
  # external values need to be initialized first - user defines their value
  parameters = (p for p in all_parameters
                if p.nature == 'external' and
                p.name not in [v.name for v in all_masses])

  masses_vals = [u.value for u in all_masses]
  yukawa_params = {u: all_masses[masses_vals.index(u.value)] for u in parameters
                   if u.value in masses_vals and u.value != 0.0}
  return yukawa_params

#==============================================================================#

def sort_particle_antiparticle(particle):
  """ Returns the particle and antiparticle of `particle`"""
  if is_anti_particle(particle.name):
    particle_tmp = get_anti_particle(particle, model_objects.all_particles)
    particle_tmp_anti = particle
  else:
    particle_tmp = particle
    particle_tmp_anti = get_anti_particle(particle,
                                          model_objects.all_particles)
  return particle_tmp, particle_tmp_anti

#==============================================================================#

def gen_mass_ctparameter(particle):
  """ Generatres the mass ct parameter associated to the mass of the particle
  `particle`. By convention, we define for spin 0 and 1 the massct as quadratic
  expression, i.e. we substitute `dm` with `dm2/(2*m)` where m is the mass.

  :param return: Mass CT
  :type  return: FeynRules Parameter
  """

  # define redundant stuff for FeynRules
  nature = 'external'
  type = 'real'
  lhablock = 'CTs'
  lhacode = [2]

  mass_name = particle.mass.name
  # Fermions
  if particle.spin == 2:
    mass_ct_name = 'd' + mass_name
    mass_ct_value = mass_ct_name
  # Bosons
  else:
    # define mass ct as quadratics
    mass_ct_name = 'd' + mass_name + '2'
    mass_ct_value = mass_ct_name + '/(2 * ' + mass_name + ')'

  mass_ct_texname = mass_ct_name

  mct = Parameter(name=mass_ct_name,
                  nature=nature,
                  type=type,
                  value=mass_ct_value,
                  texname=mass_ct_texname,
                  lhablock=lhablock,
                  lhacode=lhacode)
  return mct

#==============================================================================#

def gen_mass_ctcoupling(particle, mass_name):
  """ Generates the mass ct coupling of the particle.  Scalars and vectors have
  quadratic mass terms whereas for fermions the mass terms are linear. """

  # since the order of the counterterms are determined by renormalization
  # condition, the vertex is intitialized with 0 order in the coupling
  # constants.
  orders_zero = {u.name: 0 for u in model_objects.all_orders}
  induced_mass_name = 'GC_' + mass_name
  if particle.spin == 1:
    indeced_mass_value = '-I*' + mass_name + '*' + mass_name
  if particle.spin == 2:
    indeced_mass_value = '-I*' + mass_name
  elif particle.spin == 3:
    indeced_mass_value = 'I*' + mass_name + '*' + mass_name
  GC = Coupling(name=induced_mass_name,
                value=indeced_mass_value,
                order=orders_zero)
  return GC

#==============================================================================#

def gen_vertex_structure(particle, massct=None, gauge_fixing_ct=False):
  """ Generates the vertex structure for the ct induced two-point vertex.

  :param particle: UFO particle instance
  :type  particle: Particle

  :param massct: UFO coupling instance representing the couplings mass.
                        `None` if particle has no mass.
  :type  massct: Coupling

  :param gauge_fixing_ct: Includes the counterterm (del_mu V^mu)^2 from the
                          gauge-fixing.
  :type  gauge_fixing_ct: bool

  The method generates the following structures, depending on the spin of the
  particle:

    - For scalars the ct vertex reads
      I dZ (p^2 - M^2) - i dM
      This form is induced by
      I (p^2 - M^2) = I * SS2 + (-I*M^2)*SS1

    - For fermions the ct vertex is induced by
      -I*(\slash p^- + \slash p^+) - I*M*(w^- + w^+)

    - For Vectorbosons the ct vertex depends on the gauge-fixing
      renormalization. If counterterms from the gauge-fixing are included, we
      assume the form (in the Feynman t'Hooft gauge):
      -g^\mu\nu [I dZ (p^2 - M^2) - I dM]

      This form can be induced by
      -I g^\mu\nu (p^2 - M^2) = (-I)*VV2 + (I*M^2)*VV1

      if the gauge-fixing is taken as renormalized the ct vertex is induced by
      (-I)*VV4 + (I*M^2)*VV1

      Note that in the latter case additional mixing terms such as
      \del Z G_0 should be included by hand.

    """
  if particle.spin == 1:
    lorentz = [LCT.SS2]
    couplings = {(0, 0): C.GC_I}
    if massct:
      lorentz.append(LCT.SS1)
      couplings[(0, len(lorentz)-1)] = massct

  elif particle.spin == 2:
    if massct is not None:
      lorentz = [LCT.FF1, LCT.FF2, LCT.FF3, LCT.FF4]
      couplings = {(0, 0): C.GC_MI, (0, 1): C.GC_MI,
                   (0, 2): massct, (0, 3): massct}
    else:
      lorentz = [LCT.FF1, LCT.FF2]
      couplings = {(0, 0): C.GC_MI, (0, 1): C.GC_MI}
  elif particle.spin == 3:
    VV = LCT.VV2 if gauge_fixing_ct else LCT.VV4
    if massct is not None:
      lorentz = [VV, LCT.VV1]
      couplings = {(0, 0): C.GC_MI, (0, 1): massct}
    else:
      lorentz = [VV]
      couplings = {(0, 0): C.GC_MI}
  return lorentz, couplings

#==============================================================================#

def gen_wavefunction(particle, mixing_particles=[]):
  """ Generates the wavefunction expression and assigns it to the particle. """
  if particle.spin == 2:
    fermion_minus_rule = (ProjM, ['Contraction', 'Particle'])
    fermion_plus_rule = (ProjP, ['Contraction', 'Particle'])
    antifermion_minus_rule = (ProjP, ['Particle', 'Contraction'])
    antifermion_plus_rule = (ProjM, ['Particle', 'Contraction'])
    particle_tmp, particle_tmp_anti = sort_particle_antiparticle(particle)
    pname = export_particle_name(particle_tmp.name).strip()
    wname = 'dZ' + pname

    dZ = {particle_tmp: {wname + 'L/2': fermion_minus_rule,
                         wname + 'R/2': fermion_plus_rule}}
    for mp in mixing_particles:
      mp_tmp, mp_tmp_anti = sort_particle_antiparticle(mp)
      mpname = export_particle_name(mp_tmp.name).strip()
      dZ[mp_tmp] = {wname + mpname + 'L/2': fermion_minus_rule,
                    wname + mpname + 'R/2': fermion_plus_rule}
    particle_tmp.counterterm = dZ

    if particle_tmp != particle_tmp_anti:
      dZc = {particle_tmp_anti: {wname + 'L/2': antifermion_minus_rule,
                                 wname + 'R/2': antifermion_plus_rule}}
      for mp in mixing_particles:
        mp_tmp, mp_tmp_anti = sort_particle_antiparticle(mp)
        mpname = export_particle_name(mp_tmp.name).strip()
        dZc[mp_tmp_anti] = {wname + mpname + 'L/2': antifermion_minus_rule,
                            wname + mpname + 'R/2': antifermion_plus_rule}
      particle_tmp_anti.counterterm = dZc
  else:
    identity_rule = None
    particle_tmp, particle_tmp_anti = sort_particle_antiparticle(particle)
    pname = export_particle_name(particle_tmp.name).strip()
    wname = 'dZ' + pname
    dZ = {particle_tmp: {wname + '/2': identity_rule}}
    for mp in mixing_particles:
      mp_tmp, mp_tmp_anti = sort_particle_antiparticle(mp)
      mpname = export_particle_name(mp_tmp.name).strip()
      dZ[mp_tmp] = {wname + mpname + '/2': identity_rule}
    particle_tmp.counterterm = dZ

    if particle_tmp != particle_tmp_anti:
      dZc = {particle_tmp_anti: {wname + '/2': identity_rule}}
      for mp in mixing_particles:
        mp_tmp, mp_tmp_anti = sort_particle_antiparticle(mp)
        mpname = export_particle_name(mp_tmp.name).strip()
        dZc[mp_tmp_anti] = {wname + mpname + '/2': identity_rule}
      particle_tmp_anti.counterterm = dZc

#==============================================================================#

def get_twopoint_color(color, normalization=True):
  """ Returns the colorstructure for the twopoint function P -> P if particle P
  is in the representation `color

  :param color: color representation: 1, 2, 3
  :type  color: int

  :param normalization: Compensate for non-normalized generators.
  :type  normalization: bool
  """
  if color == 1:
    return ['1']
  elif color == 3:
    return ['Identity(1,2)']
  elif color == 8:
    if normalization:
      return ['2*T(1,-1,-2)*T(2,-2,-1)']
    else:
      return ['T(1,-1,-2)*T(2,-2,-1)']
  else:
    raise ValueError('Passed color in `get_twopoint_color` can have ' +
                     'values 1, 3, 8 only.color')

#==============================================================================#

def gen_twopoint_ctvertex(particle, massct=None, gauge_fixing_ct=False):
  """ Generates the two-point counterterm vertex for the particle `particle`."""

  particle_tmp, particle_tmp_anti = sort_particle_antiparticle(particle)
  # twopoint vertex colorstructure
  color = get_twopoint_color(particle.color)
  # twopoint vertex lorentz structure and couplings
  lorentz, couplings = gen_vertex_structure(particle, massct=massct,
                                            gauge_fixing_ct=gauge_fixing_ct)
  pname = export_particle_name(particle_tmp.name).strip()
  TreeInducedCTVertex(name='V_' + pname + pname,
                      particles=[particle_tmp_anti, particle_tmp],
                      color=color,
                      lorentz=lorentz,
                      couplings=couplings)

#==============================================================================#

def assign_ct_particle(particle, assign_massct=True,
                       assign_wavefunction=True, mixing_particles=[],
                       gauge_fixing_ct=False):
  """ Generates the counterterm structures for particle `particle`
  :param particle: Particle for which the two-point counterterms should be
                   generated
  :type  particle: FeynRules Particle
  j
  :param assign_massct: Derive a mass counterterm. Only possible if the
                         particle is massive.
  :type  assign_massct: bool

  """

  mass = particle.mass
  if mass != parameters.ZERO and assign_massct:
    # for yukawa couplings we have to assign the same counterterm as the mass
    # coupling
    all_masses = list(set(p.mass for p in object_library.all_particles))
    yukawa = get_yukawa_parameter(list(object_library.all_parameters),
                                  all_masses)
    mct = gen_mass_ctparameter(particle)
    massct = gen_mass_ctcoupling(particle, particle.mass.name)
    mass.counterterm = mct
    if mass.name in [u.name for u in yukawa.values()]:
      yuk = [u for u, val in iteritems(yukawa)
             if val.name == mass.name][0]
      yuk.counterterm = mct
  else:
    massct = None

  gen_twopoint_ctvertex(particle, massct, gauge_fixing_ct=gauge_fixing_ct)

  if assign_wavefunction:
    gen_wavefunction(particle, mixing_particles=mixing_particles)

#==============================================================================#

def auto_assign_ct_particle(mixings={},
                            selection=None,
                            exclude_fermions=False,
                            exclude_scalars=False,
                            exclude_vectors=False,
                            assign_goldstone_wf=False,
                            gauge_fixing_ct=False):
  """ Automatically assigns mass and wavefunction counterterms to physical
  particles in the theory.

  :param selection: Only assign counterterms associated to the particle
                    `selection`. Anti-particles are added to the selection if
                    not included in the selection. Note the other filters will
                    apply on the selection.
  :type  selection: None or iterable

  :param exclude_fermions: Exlude counterterms to fermions.
  :type  exclude_fermions: bool

  :param exclude_scalars: Exlude counterterms to scalars.
  :type  exclude_scalars: bool

  :param exclude_vectors: Exlude counterterms to vectors.
  :type  exclude_vectors: bool

  :param gauge_fixing_ct: Set True if counterterms to the gauge-fixing should be
                          included. Affects the renormalization of vectors and
                          goldstone bosons.
  :type  gauge_fixing_ct: bool
  """
  if selection is None:
    particles = object_library.all_particles
  else:
    particles = selection
    for p in selection[:]:
      ap = get_anti_particle(p, object_library.all_particles)
      if ap != p:
        particles.append(ap)

  def is_background_field(p):
    if not hasattr(p, 'quantumfield'):
      return True
    else:
      return not p.quantumfield

  if not exclude_fermions:
    fermions = (u for u in particles if u.spin == 2 and
                not is_anti_particle(u.name) and is_background_field(u))
  else:
    fermions = []

  if not exclude_scalars:
    scalars = (u for u in particles if u.spin == 1 and
               not is_anti_particle(u.name)and is_background_field(u))
  else:
    scalars = []

  if not exclude_vectors:
    vbosons = (u for u in particles if u.spin == 3 and
               not is_anti_particle(u.name) and is_background_field(u))
  else:
    vbosons = []
  for v in scalars:
    if v in mixings:
      # For goldstone bosons there is no mass counterterm if the gauge fixing is
      # taken as renormalized
      if is_goldstone_boson(v) and not gauge_fixing_ct:
        assign_massct = False
      else:
        assign_massct = True

      assign_ct_particle(v, mixing_particles=mixings[v],
                         assign_massct=assign_massct,
                         gauge_fixing_ct=gauge_fixing_ct)
    elif (not is_goldstone_boson(v)) or gauge_fixing_ct:
      assign_ct_particle(v, gauge_fixing_ct=gauge_fixing_ct)
    elif is_goldstone_boson(v) and assign_goldstone_wf:
      assign_ct_particle(v, assign_massct=False, gauge_fixing_ct=gauge_fixing_ct)

  for v in fermions:
    assign_ct_particle(v, gauge_fixing_ct=gauge_fixing_ct)
    if v in mixings:
      raise Exception('Mixing for fermions not tested!')
      # assign_ct_particle(v, mixing_particles=mixings[v],
      #                    gauge_fixing_ct=gauge_fixing_ct)

  for v in vbosons:
    if v in mixings:
      assign_ct_particle(v, mixing_particles=mixings[v],
                         gauge_fixing_ct=gauge_fixing_ct)
    else:
      assign_ct_particle(v, gauge_fixing_ct=gauge_fixing_ct)

#==============================================================================#

def list_active_counterterms():
  from rept1l.counterterms import Counterterms
  print('Active counterterm parameter:')
  print(', '.join(Counterterms.
                  get_param_ct(object_library.all_parameters).keys()))
  print('Active wavefunctions:')
  print(', '.join(u.name for u in Counterterms.
                  get_wavefunction_ct(object_library.all_particles)))
