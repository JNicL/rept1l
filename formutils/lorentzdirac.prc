#procedure lorentzdirac
*==============================================================================*
*                      Procedure to simplify Dirac Gammas                      *
*==============================================================================*

* Do not use gamma_5 directly! 
* Reexpress via ga(i1,i2,5) = (ga(i1,i2,[+]) - ga(i1,i2,[-]))

repeat;
 id si(i?,j?,mu1?,mu2?) = i_/2*(ga(i,j,mu1,mu2)-ga(i,j,mu2,mu1));
endrepeat;

* Join gammas which are in the same trace. All structures are mapped to the ga
* structure
repeat;
 id om(i1?,i2?)*om(i2?,i3?) = om(i1,i3);
 id op(i1?,i2?)*op(i2?,i3?) = op(i1,i3);
 id om(i1?,i2?)*op(i2?,i3?) = 0;
 id op(i1?,i2?)*om(i2?,i3?) = 0;
 id om(i1?,i2?)*ga(i2?,i3?,?m) = ga(i1,i3,[-],?m);
 id op(i1?,i2?)*ga(i2?,i3?,?m) = ga(i1,i3,[+],?m);
 id ga(i1?,i2?,?m)*om(i2?,i3?) = ga(i1,i3,?m,[-]);
 id ga(i1?,i2?,?m)*op(i2?,i3?) = ga(i1,i3,?m,[+]);
 id ga(i1?,i2?,?m1)*ga(i2?,i3?,?m2) = ga(i1,i3,?m1,?m2);
endrepeat;

id om(i1?,i2?) = ga(i1,i2,[-]);
id op(i1?,i2?) = ga(i1,i2,[+]);

.sort

* Gamma_5 needs a careful treatment in d dimensions with closed fermion loops.
* Define a canonical order for gamma traces where all indices in closed gamma
* traces are permuted to the same starting point
argument ga;
 id q = Q;
 id [+] = PP;
 id [-] = PM;
endargument;
#do i= 1,15
  argument ga;
   id p'i' = P'i';
  endargument;
#enddo 

* Map closed gamma traces to cyclic tensors and sort
id ga(i?, i?, ?m) = Y(?m);
.sort

* Undo the preceeding manipulations
id Y(?m) = ga(1, 1, ?m);
#do i= 1,15
  argument ga;
   id P'i' = p'i';
  endargument;
#enddo 
argument ga;
 id Q = q;
 id PP = [+];
 id PM = [-];
endargument;
.sort

* push helicity projectors to the left.
repeat;
 id ga(i1?,i2?,?m1,mu?,[-],?m2) = ga(i1,i2,?m1,[+],mu,?m2);
 id ga(i1?,i2?,?m1,mu?,[+],?m2) = ga(i1,i2,?m1,[-],mu,?m2);
 id ga(?m1,[-],[-],?m2) = ga(?m1,[-],?m2);
 id ga(?m1,[+],[+],?m2) = ga(?m1,[+],?m2);
 id ga(?m1,[+],[-],?m2) = 0;
 id ga(?m1,[-],[+],?m2) = 0;
endrepeat;

.sort

* DISABLED: push polarization to the right.
*repeat;
* id ga(i1?,i2?,?m1,[-],mu?,?m2) = ga(i1,i2,?m1,mu,[+],?m2);
* id ga(i1?,i2?,?m1,[+],mu?,?m2) = ga(i1,i2,?m1,mu,[-],?m2);
*endrepeat;
*.sort

* Use anti-commutation for Gamma when two indices are the same in a gamma trace
* (not necessarily closed)
repeat;
 id ga(i1?,j1?,?m1,mu?,?m,nu?,mu?,?m2) = 
    2*ga(i1,j1,?m1,nu,?m,?m2) - ga(i1,j1,?m1,mu,?m,mu,nu,?m2);
 id ga(i1?,j1?,?m1,mu?,mu?,?m2) = n*ga(i1,j1,?m1,?m2);
endrepeat;
.sort

* Use anti-commutation for Gamma when two momenta are the same in a gamma trace
* (not necessarily closed)
repeat;
 id ga(i1?,j1?,?m1,p?,?m,nu?,p?,?m2) = 
    2*p(nu)*ga(i1,j1,?m1,p,?m,?m2) - ga(i1,j1,?m1,p,?m,p,nu,?m2);
 id ga(i1?,j1?,?m1,p?,p?,?m2) = p.p*ga(i1,j1,?m1,?m2);
endrepeat;
.sort


* Introduce gamme_5
* [-] = (1-ga5)/2
* [+] = (1+ga5)/2
id ga(i?,i?,[-],?m) = 1/2*ga(i,i,?m) - 1/2*ga(i,i,5,?m);
id ga(i?,i?,[+],?m) = 1/2*ga(i,i,?m) + 1/2*ga(i,i,5,?m);

#define ntr "1"
#do repeat= 1,1
  id once ga(i?,i?,?m) = ga(`ntr',?m);
  #redefine ntr "{'ntr'+1}"
  if ( match(ga(i?,i?,?m)) ) redefine repeat "0";
  .sort
#enddo


b ga;
.sort
keep brackets;

* Apply closed trace rules.

#do j = 1,'ntr'

 repeat;
  id ga(`j',5) = 0;
  id ga(`j',5,mu1?) = 0;
  id ga(`j',5,mu1?,mu2?) = 0;
  id ga(`j',5,mu1?,mu2?,mu3?,mu4?) = 4*e_(mu1,mu2,mu3,mu4);
  id ga(`j',5,mu1?,mu2?,mu3?,?a) = 
     + ga(`j',be`j',?a) *e_(mu1,mu2,mu3,be`j')
     + ga(`j',5,mu3,?a) *d_(mu1,mu2)
     - ga(`j',5,mu2,?a) *d_(mu1,mu3)
     + ga(`j',5,mu1,?a) *d_(mu2,mu3);
 endrepeat;

 .sort

 #do i= 1,15,2
  id ga(`j',<mu1?!{,5}>,...,<mu'i'?!{,5}>)= 0;
 #enddo

 id ga(`j',<mu1?!{,5}>,...,<mu10?!{,5}>)= 
    + ga(`j',mu1,mu2,mu3,mu4,mu5,mu6,mu7,mu8)*d_(mu9,mu10)
    - ga(`j',mu1,mu2,mu3,mu4,mu5,mu6,mu7,mu9)*d_(mu8,mu10)
    + ga(`j',mu1,mu2,mu3,mu4,mu5,mu6,mu8,mu9)*d_(mu7,mu10)
    - ga(`j',mu1,mu2,mu3,mu4,mu5,mu7,mu8,mu9)*d_(mu6,mu10)
    + ga(`j',mu1,mu2,mu3,mu4,mu6,mu7,mu8,mu9)*d_(mu5,mu10)
    - ga(`j',mu1,mu2,mu3,mu5,mu6,mu7,mu8,mu9)*d_(mu4,mu10)
    + ga(`j',mu1,mu2,mu4,mu5,mu6,mu7,mu8,mu9)*d_(mu3,mu10)
    - ga(`j',mu1,mu3,mu4,mu5,mu6,mu7,mu8,mu9)*d_(mu2,mu10)
    + ga(`j',mu2,mu3,mu4,mu5,mu6,mu7,mu8,mu9)*d_(mu1,mu10);

 id ga(`j',<mu1?!{,5}>,...,<mu8?!{,5}>)= 
    + ga(`j',mu1,mu2,mu3,mu4,mu5,mu6)*d_(mu7,mu8)
    - ga(`j',mu1,mu2,mu3,mu4,mu5,mu7)*d_(mu6,mu8)
    + ga(`j',mu1,mu2,mu3,mu4,mu6,mu7)*d_(mu5,mu8)
    - ga(`j',mu1,mu2,mu3,mu5,mu6,mu7)*d_(mu4,mu8)
    + ga(`j',mu1,mu2,mu4,mu5,mu6,mu7)*d_(mu3,mu8)
    - ga(`j',mu1,mu3,mu4,mu5,mu6,mu7)*d_(mu2,mu8)
    + ga(`j',mu2,mu3,mu4,mu5,mu6,mu7)*d_(mu1,mu8);

 id ga(`j',<mu1?!{,5}>,...,<mu6?!{,5}>)= 
    + ga(`j',mu1,mu2,mu3,mu4)*d_(mu5,mu6)
    - ga(`j',mu1,mu2,mu3,mu5)*d_(mu4,mu6)
    + ga(`j',mu1,mu2,mu4,mu5)*d_(mu3,mu6)
    - ga(`j',mu1,mu3,mu4,mu5)*d_(mu2,mu6)
    + ga(`j',mu2,mu3,mu4,mu5)*d_(mu1,mu6);

 id ga(`j',<mu1?!{,5}>,...,<mu4?!{,5}>)= 
    + ga(`j',mu1,mu2)*d_(mu3,mu4)
    - ga(`j',mu1,mu3)*d_(mu2,mu4)
    + ga(`j',mu2,mu3)*d_(mu1,mu4);

 id ga(`j',mu1?!{,5},mu2?!{,5})= 4*d_(mu1,mu2);

 id ga(`j')= 4;

#enddo

.sort

#endprocedure
