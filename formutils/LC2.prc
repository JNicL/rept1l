#procedure LC2

*------------------------------------------------------------------------------*
id A([],?m)    = A0(?m);
id A(mu?, [],?m) = 0;
id A(mu?,nu?, [],?m) = d_(mu,nu) * A00(?m);

id B([],?m)    = B0(?m);
id B([],p?,?m) = B0(p,?m);
id B(mu?,[],p?,?m) = p(mu) * B1(p,?m);
id B(mu?,nu?,[],p?,m1?,m2?) = 
  d_(mu,nu) * B00(p,m1,m2) + p(mu)*p(nu)*B11(p,m1,m2);

* Do not use this reduction for msbar because of finte part of rational terms
*repeat;
*  id B1(p?, m1?, m2?)  = 1/(2*p.p)*(A0(m1)-A0(m2) +
*                                  (m2**2 - m1**2 - p.p)*B0(p,m1,m2));
*  id B00(p?, m1?, m2?) = 1/6*(A0(m2) + 2*m1**2*B0(p,m1,m2)+ 
*                                  (p.p - m2**2 + m1**2)*B1(p,m1,m2)+
*                                   m1**2 + m2**2 - p.p/3);
*  id B11(p?, m1?, m2?) = 1/(6*p.p)*(2*A0(m2) - 2*m1**2*B0(p,m1,m2)- 
*                          4*(p.p - m2**2 + m1**2)*B1(p,m1,m2)
*                          -m1**2 - m2**2 + p.p/3);
*endrepeat;

repeat;
  id B1(p?, m1?, m2?)  = 1/(2*p.p)*(A0(m1)-A0(m2) +
                                  (m2**2 - m1**2 - p.p)*B0(p,m1,m2));
  id B00(p?, m1?, m2?) = 1/6*(A0(m2) + 2*m1**2*B0(p,m1,m2)+ 
                                  (p.p - m2**2 + m1**2)*B1(p,m1,m2));
  id B11(p?, m1?, m2?) = 1/(6*p.p)*(2*A0(m2) - 2*m1**2*B0(p,m1,m2)- 
                          4*(p.p - m2**2 + m1**2)*B1(p,m1,m2));
endrepeat;

id A00(m1?)  = m1**2*A0(m1)/4;
id A0000(m1?) = m1**4*A0(m1)/24;
id A000000(m1?) = m1**6*A0(m1)/192;


id C([],p1?,p2?,?m) = C0(p1,p2,?m);
id C(mu?,[],p1?,p2?,?m) = p1(mu)*C1(p1,p2,?m) + p2(mu)*C2(p1,p2,?m);
id C(mu1?,mu2?,[],p1?,p2?,?m) = d_(mu1,mu2)*C00(p1,p2,?m) +
                                p1(mu1)*p1(mu2)*C11(p1,p2,?m) + 
                                p2(mu2)*p2(mu1)*C22(p1,p2,?m) + 
                                (p1(mu1)*p2(mu2) +
                                p1(mu1)*p2(mu2))*C12(p1,p2,?m);
id C(mu1?,mu2?,mu3?,[],p1?,p2?,?m) = (d_(mu1, mu2)*p1(mu3) + 
                                      d_(mu2, mu3)*p1(mu1) + 
                                      d_(mu3, mu1)*p1(mu2) ) * C001(p1,p2,?m)  +
                                    ( d_(mu1, mu2)*p2(mu3) + 
                                      d_(mu2, mu3)*p2(mu1) + 
                                      d_(mu3, mu1)*p2(mu2) ) * C002(p1,p2,?m)  +
                                      p1(mu1)*p1(mu2)*p1(mu3)*C111(p1,p2,?m)   +
                                      p2(mu1)*p2(mu2)*p2(mu3)*C222(p1,p2,?m)   +
                                    ( p1(mu1)*p1(mu2)*p2(mu3) + 
                                      p1(mu1)*p2(mu2)*p1(mu3) + 
                                      p2(mu1)*p1(mu2)*p1(mu3) )*C112(p1,p2,?m) +
                                    ( p2(mu1)*p2(mu2)*p1(mu3) + 
                                      p2(mu1)*p1(mu2)*p2(mu3) + 
                                      p1(mu1)*p2(mu2)*p2(mu3) )*C112(p1,p2,?m);
                                  
*
id n = 4;

id M0 = 0;
id A0(M0) = 0;
*id p1(mu1) = -p3(mu1) -p2(mu1);
*id p2(mu2) = -p3(mu2) -p1(mu2);
*id p3(mu3) = -p1(mu3) -p2(mu3);
*

*id A0(m?) = -2*m**2;
*id B0(p?,m1?,m2?) = -2;
*id B1(p?,m1?,m2?) = 1;
*id B00(p?,m1?,m2?) = (p.p - 3*m1**2 - 3*m2**2)/6;
*id B11(p?,m1?,m2?) = -2/3;
*id C00(p1?,p2?, ?m) = -1/2;
*id C001(p1?,p2?, ?m) = 1/6;
*id C002(p1?,p2?, ?m) = 1/6;
**
*id C0(p1?,p2?, ?m) = 0;
*id C1(p1?,p2?, ?m) = 0;
*id C2(p1?,p2?, ?m) = 0;
*
*id C11(p1?,p2?, ?m) = 0;
*id C12(p1?,p2?, ?m) = 0;
*id C111(p1?,p2?, ?m) = 0;
*id C22(p1?,p2?, ?m) = 0;
*id C222(p1?,p2?, ?m) = 0;
*
*id C112(p1?,p2?, ?m) = 0;
*
**id cMT**2 = cMT2;
**
.sort

*------------------------------------------------------------------------------*

#endprocedure
