#-
* chiral stats, ..
s [-],[+],[],pi,ep,Nc;
cf Eps,Psi,bPsi,ga,om,op,Ga,si,Si,Den,Delta,DeltaP;
auto cf A,B,C,D,E,F,G,H,L;
f den;
t Y(cyclic);
cf Sis(cyclic);

s n; dimension n;
s sq2;
i mu1,mu2,mu3,mu4;
auto i mu,nu,ro,al,be;

auto s m,M,r,xi;
auto c c;

v p1 p2 p3 p4;
v q1 q2 q3 q4;
auto v p,q;

dimension 4;
i PP, PM;
auto i P,Q;

dimension 4;
auto i i,j,k,l,o,e,a,y,w;

auto cf B, DB;
cf g, k12;
s x, n;
s DeltaUV;
f f, dx, [1];
