      ! The couplings are specified by 3 + perturbative couplings(QED, QCD,..)
      ! arguments. 
      ! The first argument indentifies the lorentz structure associated to a
      ! branch id.
      ! The following arguments are stored in cgs, the previous argument is
      ! determined after the lorentz structure selection (select(ty))
      ! The following indices are powers in perturbative couplings defined in
      ! class_particle.f90 order_ids.
      ! The next argument is the branch_type either tree(0), loop(1),
      ! ct(2) or r2(3)
      ! And the last index is the branch id associated to a particle combination


      ! powers in couplings
      write(cgs,'(i1)') order_increase%val(1)
      amporder = trim(cgs)
      do i = 2, size(order_increase%val)
        write(cgs,'(i1)') order_increase%val(i)
        amporder = trim(amporder)//','//trim(cgs)
      end do

      ! branch_type
      write(int_str,'(i1)') branch_type
      tag = trim(int_str)//','

      ! modelfile branch
      write(int_str,'(i1)') branch
      tag = trim(tag)//trim(int_str)
      cgs = trim(amporder)//','//trim(tag)

