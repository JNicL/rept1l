if (lp.eq.1) then; legsE = legs + 2
else;              legsE = legs
endif
last = e(nDef).eq.2**(legsE-1)-1
write(clegs,'(i8)') legs; clegs = adjustl(clegs)
! changed j to j+2 which fixed a bug for 1 -> 1 tree-level diagrams
write(cj,'(i8)') j+2; cj = adjustl(cj)

do i = 1, nDef
  write(cw(i),'(i8)') w(i); cw(i) = adjustl(cw(i))
  cwp(i) = 'wp'//cw(i)
  write(ce(i),'(i8)') e(i); ce(i) = adjustl(ce(i))
  p(i) = 'p'//ce(i)
  write(cf(i),'(i2)') f(i)
  if (f(i).lt.10) cf(i) = '0' // trim(adjustl(cf(i)))
  myparticle = particles(f(i))
  if (myparticle%mass_name .ne. 'ZERO') then
    if (complex_mass_frm .eqv. .true.) then
      m(i) = 'c'//myparticle%mass_name
    else
      m(i) = myparticle%mass_name
    end if 
  else
    m(i) = 'M0'
  end if
end do 
