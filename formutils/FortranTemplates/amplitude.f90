if (lp .eq. 0) then
  amp = 'AmpLO'
else if (lp .eq. 1) then
  write(ct,'(i2)') getParticle(t)
  if (getParticle(t).lt.10) ct = '0'//trim(adjustl(ct))
  amp = 'Amp'//trim(ct)
else if (lp .eq. 2) then
  amp = 'AmpCT'
else if (lp .eq. 3) then
  amp = 'AmpR2'
end if
