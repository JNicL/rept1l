    integer :: legsE, i, ind, pext, mass_id, reg_id
    logical :: last = .false.
    character :: C2*5,C3*7,C4*9
    character(len=5) :: amp
    integer, parameter :: nDef = max_leg_multiplicity
    character(len=8) :: clegs, cj, cw(1:nDef), clast, ce(1:nDef)
    character(len=10) :: p(1:nDef)
    character(len=2) :: ct, cf(1:nDef)
    character(len=5) :: m(1:nDef)
    character(len=10) :: tag
    character(len=5) :: mgsb  ! Goldstone boson mass
    character(len=7) :: rxip  ! 1 - xi:  one minus rxi gauge parameter
    character(len=80) :: cgs
    character(len=80) :: amporder
    character(len=3) :: colorflow
    character(len=3) :: int_str
    character(len=80) :: cwp(1:nDef), den, den0, denGSB, cwIn, indent=''
    character(len=2*nMax+1) :: C
