if (lp.eq.1) then; legsE = legs + 2
else;              legsE = legs
endif
last = e(nDef).eq.2**(legsE-1)-1
write(clegs,'(i8)') legs; clegs = adjustl(clegs)
! changed j to j+2 which fixed a bug for 1 -> 1 tree-level diagrams
write(cj,'(i8)') j+2; cj = adjustl(cj)

do i = 1, nDef
  write(cw(i),'(i8)') w(i); cw(i) = adjustl(cw(i))
  cwp(i) = 'wp'//cw(i)
  write(ce(i),'(i8)') e(i); ce(i) = adjustl(ce(i))
  p(i) = 'p'//ce(i)
  write(cf(i),'(i2)') f(i)
  if (f(i).lt.10) cf(i) = '0' // trim(adjustl(cf(i)))
  ! f(i) may be zero, meaning that there is no particle at
  ! position i.
  if (f(i) .gt. 0) then
    mass_id = get_particle_mass_id_mdl(f(i))
    reg_id  = get_particle_mass_reg_mdl(f(i))
    if ((mass_id .eq. 1) .or. (reg_id .eq. 1) .or. &
        (reg_id .eq. 2 .and. masscut_frm)) then
      m(i) = 'M0'
    else
      if (complex_mass_frm .eqv. .true.) then
        m(i) = 'c'// get_mass_name_mdl(mass_id)
      else
        m(i) = get_mass_name_mdl(mass_id)
      end if
    end if
  end if
end do

! Rxi propagator extension. Setting the goldstone boson 
! propagator mass and gauge parameter

pext = get_propagator_extension_mdl(ty)
if (pext .gt. 0) then
  ! Z boson field
  if (f(nDef) .eq. get_particle_id_mdl('Z')) then
    mgsb = 'MG0'
    rxip = '(1-xiz)'
  ! Z quantum field
  else if (f(nDef) .eq. get_particle_id_mdl('ZQ')) then
    mgsb = 'MG0'
    rxip = '(1-xiQ)'
  ! W boson field
  else if (f(nDef) .eq. get_particle_id_mdl('W+') .or. &
           f(nDef) .eq. get_particle_id_mdl('W-')) then
    mgsb = 'MGP'
    rxip = '(1-xiw)'
  ! W quantum field
  else if (f(nDef) .eq. get_particle_id_mdl('WQ+') .or. &
           f(nDef) .eq. get_particle_id_mdl('WQ-')) then
    mgsb = 'MGP'
    rxip = '(1-xiQ)'
  ! Photon field
  else if (f(nDef) .eq. get_particle_id_mdl('a')) then
    mgsb = 'M0'
    rxip = '(1-xia)'
  ! Photon quantum field
  else if (f(nDef) .eq. get_particle_id_mdl('aQ')) then
    mgsb = 'M0'
    rxip = '(1-xiQ)'
  else
    write (*, *) 'Propagator extension for particle id ', &
                 f(nDef), ' not defined.'
    stop
  end if
else
  mgsb = ''
  rxip = ''
end if

! The couplings are specified by 3 + perturbative couplings(QED, QCD,..)
! arguments. 
! The first argument indentifies the lorentz structure associated to a
! branch id.
! The following arguments are stored in cgs, the previous argument is
! determined after the lorentz structure selection (select(ty))
! The following indices are powers in perturbative couplings defined in
! class_particle.f90 order_ids.
! The next argument is the branch_type either tree(0), loop(1),
! ct(2) or r2(3)
! And the last index is the branch id associated to a particle combination

! powers in couplings
write(cgs,'(i1)') order_increase(1)
amporder = trim(cgs)
do i = 2, size(order_increase)
  write(cgs,'(i1)') order_increase(i)
  amporder = trim(amporder)//','//trim(cgs)
end do

! branch_type
write(int_str,'(i1)') branch_type
tag = trim(int_str)//','

! modelfile branch
if (branch .lt. 10) then
  write(int_str,'(i1)') branch
else if (branch .gt. 9 .and. branch .lt. 100) then
  write(int_str,'(i2)') branch
else
  call error_mdl("More than 99 branches not supported.", &
                 where="form_current_mdl")
end if
tag = trim(tag)//trim(int_str)
cgs = trim(amporder)//','//trim(tag)

if (last) then
if (lp .eq. 0) then
  amp = 'AmpLO'
else if (lp .eq. 1) then
  write(ct,'(i2)') t
  if (t .lt. 10) ct = '0'//trim(adjustl(ct))
  amp = 'Amp'//trim(ct)
else if (lp .eq. 2) then
  amp = 'AmpCT'
else if (lp .eq. 3) then
  amp = 'AmpR2'
end if
write(colorflow,'(i3)') cs; colorflow = adjustl(colorflow)

write(int_str,'(i1)') absolute_order(1)
amporder = trim(order_ids(1))//'='//trim(int_str)
do i = 2, size(order_increase)
  write(int_str,'(i1)') absolute_order(i)
  amporder = trim(amporder)//','//trim(order_ids(i))//'='//trim(int_str)
end do
cwp(nDef) = '['//amp//'(cs='//trim(colorflow)//','//trim(amporder)//')]'
end if

den  = 'den('//trim(p(nDef))//','//trim(m(nDef))//')'
den0 = 'den('//trim(p(nDef))//',M0)'
denGSB = 'den('//trim(p(nDef))//','//trim(mgsb)//')'
ind = len(trim(cwp(nDef))) + 5
write(999,'(a)')
write(999,'(3a)') 'l ',trim(cwp(nDef)),' = '
C = 'c'
cwIn = '+'

do i = 1, nMax -1
  C = trim(C)//cf(i)
  if(i .eq. 2) then; C3 = trim(C)//cf(nDef)
  else if (i.eq.3) then; C4 = trim(C)//cf(nDef)
  end if
  cwIn = trim(cwIn)//' '//trim(cwp(i))//' *'
end do 

C = trim(C)//cf(nDef)
write(999,'(a)') '  '//trim(cwp(nDef))//' '//trim(cwIn)

if (last) then
  write(clast,'(i8)') legsE; clast = adjustl(clast)
  select case(get_particle_spin_mdl(f(nDef)))
  case(3)
    write(999,'(a)') indent(1:ind) // 'd_(mu' // trim(clast) // ',mu' // &
    trim(cw(nDef)) // ') * ' 
  case(2)
    write(999,'(a)') indent(1:ind) // 'd_(i' // trim(clast) // ',i' //   &
    trim(cw(nDef)) // ') * ' 
  end select 
end if 

write(999,'(a,i2,a)') indent(1:ind) // '(',sign,') * '

if (Nc_power .ne. 0) then
  write(999,'(a, i3, a)') indent(1:ind) // 'Nc ** (',Nc_power,') * '
end if 
write(999,'(a)') indent(1:ind) // trim(adjustl(colT_symbol)) // ' * '
