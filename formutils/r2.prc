#procedure r2
*=========================================================================
* Divergent part of 1-loop functions
* n = 4 - ep
*=========================================================================

id A([],m?) = 2 * m**2 * ep^-1;
id A(mu?,nu?,[],m?) = d_(mu,nu)*2* m**2 * ep^-1;
id B([],?m) = 2 * ep^-1;
id B(mu?,[],p?,?m) = - p(mu) * ep^-1;
id B(mu?,nu?,[],p?,m1?,m2?) = 
   - 1/3 * ( 
     + 1/2 * ( p.p - 3*m1^2 - 3*m2^2) * d_(mu,nu)
     - 2 * p(mu) * p(nu)
     ) * ep^-1;

id C([],?m) = 0;
id C(mu?,[],?m) = 0;
id C(mu?,nu?,[],?m) = 1/2 * d_(mu,nu) * ep^-1;
id C(mu?,nu?,ro?,[],p1?,p12?,?m) = 
   - 1/6 * (
     + d_(mu,nu) * ( p1(ro) + p12(ro) )
     + d_(nu,ro) * ( p1(mu) + p12(mu) )
     + d_(ro,mu) * ( p1(nu) + p12(nu) )
     ) * ep^-1;

id D([],?m) = 0;
id D(mu?,[],?m) = 0;
id D(mu?,nu?,[],?m) = 0;
id D(mu?,nu?,ro?,[],?m) = 0;
id D(mu1?,mu2?,mu3?,mu4?,[],?m) = 
   1/12 * (
   + d_(mu1,mu2) * d_(mu3,mu4)
   + d_(mu1,mu3) * d_(mu2,mu4)
   + d_(mu1,mu4) * d_(mu2,mu3)
   ) * ep^-1;

id n = 4 - ep;

.sort

id ep = 0;
id 1/ep = 0;
id M0 = 0;

.sort

*=========================================================================

#endprocedure


