#procedure PVderivative
* Passarino Veltman Reduction
*------------------------------------------------------------------------------*
repeat;
  id B1(p?,m1?,m2?)  = 1/(2*p.p)*(A0(m1)-A0(m2) +
                                  (m2**2 - m1**2 - p.p)*B0(p,m1,m2));
  id DB1(p?,m1?,m2?) = -1/p.p*B1(p,m1,m2) + 1/(2*p.p)*(-B0(p,m1,m2) + 
                        (m2**2 - m1**2 - p.p)*DB0(p,m1,m2));
  id B00(p?,m1?,m2?) = 1/6*(A0(m2) + 2*m1**2*B0(p,m1,m2) + 
                            (p.p - m2**2 + m1**2)*B1(p,m1,m2) +
                            m1**2 + m2**2 - p.p/3);
  id DB00(p?,m1?,m2?) = 1/6*(2*m1**2*DB0(p,m1,m2) + 
                             (p.p - m2**2 + m1**2)*DB1(p,m1,m2) + 
                             B1(p,m1,m2) - 1/3);
  id B11(p?,m1?,m2?) = 1/(6*p.p)*(2*A0(m2) - 2*m1**2*B0(p,m1,m2) -
                          4*(p.p - m2**2 + m1**2)*B1(p,m1,m2) -
                          m1**2 - m2**2 + p.p/3);
  id DB11(p?,m1?,m2?) = -1/p.p*B11(p,m1,m2) + 
                        1/(6*p.p)*(- 2*m1**2*DB0(p,m1,m2) - 
                          4*(p.p - m2**2 + m1**2)*DB1(p,m1,m2) -
                          4*B1(p,m1,m2) + 1/3);
* id A0(m?) = m**2*(B0(M0, m, m) + 1);
endrepeat;
*------------------------------------------------------------------------------*
*id B0(p1, m?, m?)/p1.p1 = B0(0,m, m)/p1.p1 + 1/MT**2/2;
* Implemented the derivative of B at p^2 = 0
*id B0(0, m?, m?) = B0(p1,m, m) - 1/m**2/6*p1.p1;
*.sort
id M0 = 0;
*id n = 4;
.sort
#endprocedure
