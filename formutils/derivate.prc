#procedure derivate
*------------------------------------------------------------------------------*

id p1.p1 = mom;
id p1.p1^-1 = mom1;
id p1.p1^-2 = -mmom2;
id g?commuting?noncommuting(?x) = g(?x);
id g?commuting?noncommuting = g;
Multiply left dx;
repeat;
  id dx*g?noncommuting[n](?x) = derivative[n](?x) + g(?x)*dx;
endrepeat;
id dx = 0;
id [1] = 1;
id f?noncommuting?commuting(?x) = f(?x);
id mom = p1.p1;
id mom1 = p1.p1^-1;
id mmom2 = -p1.p1^-2;
id mom3 = 2*p1.p1^-3;
.sort
*------------------------------------------------------------------------------*
#endprocedure
