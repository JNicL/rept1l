#==============================================================================#
#                               formpype_new.py                                #
#==============================================================================#

import os
import form
import rept1l_config

class FormPype(object):
  """ FormPype uses python-form to establish a direct communication between FORM
  and Python.

  >>> FP = FormPype(debug=True)
  >>> FP.close()
  >>> FP = FormPype(debug=True)
  >>> exprs = ['l expr = 5;']
  >>> FP.eval_exprs(exprs)
  >>> FP.return_expr('expr')
  '(5)'
  >>> exprs = ['l expr1 = 5;',
  ...          '.sort']
  >>> FP.eval_exprs(exprs)
  >>> FP.return_expr('expr1')
  '5'
  >>> exprs = ['l expr2 = expr*2;',
  ...          '.sort']
  >>> FP.eval_exprs(exprs)
  >>> FP.return_expr('expr2', stop=True)
  '10'
  >>> FP.return_expr('expr2', stop=True)
  Traceback (most recent call last):
    ...
  IOError: tried to read from closed connection
  """
  def __init__(self, basefile=os.path.dirname(os.path.realpath(__file__)) +
               '/rept1lbase.frm', debug=False, pipemode=True, filemode=False):

    if hasattr(rept1l_config, 'formcmd'):
      self.formrun = form.open(rept1l_config.formcmd)
    else:
      self.formrun = form.open()

    with open(basefile) as f:
      for line in f.readlines():
        self.formrun.write(line)

  def __enter__(self):
    return self

  def close(self):
    self.formrun.close()

  def __exit__(self, *_):
    self.formrun.close()

  def eval_exprs(self, exprs, stop=False):
    for expr in exprs:
      self.formrun.write(expr)

  def return_expr(self, expr,  stop=False):
    ret = self.formrun.read(expr)
    if stop:
      self.formrun.close()
    return ret

  @classmethod
  def return_path(self):
    return os.path.dirname(os.path.realpath(__file__))


if __name__ == "__main__":
  import doctest
  doctest.testmod()
