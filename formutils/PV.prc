#procedure PV
* Passarino Veltman Reduction
*------------------------------------------------------------------------------*
repeat;
*-------------*
*-  1-point  -*
*-------------*
  id A00(m1?)  = m1**2*(A0(m1)+m1**2/2)/4;
  id A0000(m1?) = m1**4*(A0(m1)+m1**2*(1/2+1/3))/24;
  id A000000(m1?) = m1**6*(A0(m1)+m1**2*(1/2+1/3+1/4))/192;

*-------------*
*-  2-point  -*
*-------------*

*------------*
*-  rank 1  -*
*------------*

  id B1(p?, m1?, m2?)  = 1/(2*p.p)*(A0(m1)-A0(m2) +
                                  (m2**2 - m1**2 - p.p)*B0(p,m1,m2));

*------------*
*-  rank 2  -*
*------------*

  id B00(p?, m1?, m2?) = 1/6*(A0(m2) + 2*m1**2*B0(p,m1,m2)+ 
                                  (p.p - m2**2 + m1**2)*B1(p,m1,m2)+
                                   m1**2 + m2**2 - p.p/3);

  id B11(p?, m1?, m2?) = 1/(6*p.p)*(2*A0(m2) - 2*m1**2*B0(p,m1,m2)- 
                          4*(p.p - m2**2 + m1**2)*B1(p,m1,m2)
                          -m1**2 - m2**2 + p.p/3);

*------------*
*-  rank 3  -*
*------------*

  id B001(p?, m1?, m2?) =
  (2*m1**2*B1(p,m1,m2)-A0(m2)+(p.p-m2**2+m1**2)*B11(p,m1,m2))/8
  -(2*m1**2+4*m2**2-p.p)/48;

  id B111(p?, m1?, m2?) = 1/(2*p.p)*(-3*(p.p-m2**2+m1**2)*B11(p,m1,m2)/2
                                     - A0(m2)/2
                                     -m1**2*B1(p,m1,m2)+(2*m1**2+4*m2**2-p.p)/12);

*------------*
*-  rank 4  -*
*------------*

  id B0000(p?, m1?, m2?) =
  1/10*(A00(m2)+2*m1**2*B00(p,m1,m2)+(p.p-m2**2+m1**2)*B001(p,m1,m2)+
        ((p.p)**2-5*p.p*(m1**2+m2**2)+10*(m1**4+m1**2*m2**2+m2**4))/60);

  id B0011(p?, m1?, m2?) =
  1/10*(A0(m2)+2*m1**2*B11(p,m1,m2)+(p.p-m2**2+m1**2)*B111(p,m1,m2)-
        (3*p.p-5*m1**2-15*m2**2)/30);

  id B1111(p?,m1?,m2?) =
  ((p.p+m1**2+m2**2)*A0(m2)+m1**4*B0(p,m1,m2)-24*B0000(p,m1,m2)-12*p.p*B0011(p,m1,m2)
  + 5*(m1**4+m1**2*m2**2+m2**4)/6
  - p.p*(3*m1**2-m2**2)/12
  - (p.p)**2/60
  )/(p.p)**2;

*------------*
*-  rank 5  -*
*------------*

* incomplete, B11111 not expressed in terms of lower rank

  id B00001(p?, m1?, m2?) =-(A00(m2)+(p.p-m2**2+m1**2)*B0011(p,m1,m2) +
                             2*p.p*B00111(p,m1,m2))/4;
  id B00111(p?, m1?, m2?) =-(A(m2)+(p.p-m2**2+m1**2)*B1111(p,m1,m2) +
                             2*p.p*B11111(p,m1,m2))/8;
*------------*
*-  rank 6  -*
*------------*

* incomplete, B111111 not expressed in terms of lower rank

  id B000000(p?, m1?, m2?) =-(-A0000(m2)+(p.p-m2**2+m1**2)*B00001(p,m1,m2) +
                              2*p.p*B000011(p,m1,m2))/2;
  id B000011(p?, m1?, m2?) =-(-A00(m2)+(p.p-m2**2+m1**2)*B00111(p,m1,m2) +
                              2*p.p*B001111(p,m1,m2))/6;
  id B001111(p?, m1?, m2?) =-(-A0(m2)+(p.p-m2**2+m1**2)*B11111(p,m1,m2) +
                              2*p.p*B111111(p,m1,m2))/10;

endrepeat;
*------------------------------------------------------------------------------*
id M0 = 0;
.sort
#endprocedure
