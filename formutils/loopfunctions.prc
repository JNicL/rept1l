#procedure loopfunctions

*=========================================================================
* 
*=========================================================================

id den(p?,m?) = Den(p,m);
repeat;
 id den(q,p?,m?)*den(q,0,m0?) = den(q,0,m0)*den(q,p,m);
endrepeat;

id q(mu?) = Delta(q,mu);

#define i "1"
#do repeat= 1,1
  id once q.p? = Delta(q,ro'i')*Delta(p,ro'i');
  #redefine i "{'i'+1}"
  if ( match(q.p?) ) redefine repeat "0";
  .sort
#enddo
#do repeat= 1,1
  id once ga(?m1,q,?m2) = ga(?m1,ro'i',?m2)*Delta(q,ro'i');
  #redefine i "{'i'+1}"
  if ( match(ga(?m1,q,?m2)) ) redefine repeat "0";
  .sort
#enddo
#do repeat= 1,1
* pattern matching for arbitrary number of args does not work with e_
* id once e_(q,?m) = e_(ro'i',?m)*Delta(q,ro'i');
 id once Eps(q,?m) = Eps(ro'i',?m)*Delta(q,ro'i');
 id once e_(q,mu1?,mu2?,mu3?) = e_(ro'i',mu1,mu2,mu3)*Delta(q,ro'i');
  #redefine i "{'i'+1}"
 if ( match(e_(q,mu1?,mu2?,mu3?)) ) redefine repeat "0";
  .sort
#enddo

id den(q,0,m0?)*den(q,p1?,m1?)*den(q,p2?,m2?)*
   den(q,p3?,m3?)*den(q,p4?,m4?)*den(q,p5?,m5?) = 
   i_*F(p1,p2,p3,p4,p5,m0,m1,m2,m3,m4,m5)/(16*pi**2);
id den(q,0,m0?)*den(q,p1?,m1?)*den(q,p2?,m2?)*
   den(q,p3?,m3?)*den(q,p4?,m4?) = 
   i_*E(p1,p2,p3,p4,m0,m1,m2,m3,m4)/(16*pi**2);
id den(q,0,m0?)*den(q,p1?,m1?)*den(q,p2?,m2?)*den(q,p3?,m3?) = 
   i_*D(p1,p2,p3,m0,m1,m2,m3)/(16*pi**2);
id den(q,0,m0?)*den(q,p1?,m1?)*den(q,p2?,m2?) = 
   i_*C(p1,p2,m0,m1,m2)/(16*pi**2);
id den(q,0,m0?)*den(q,p1?,m1?) = 
   i_*B(p1,m0,m1)/(2*16*pi**2);
id den(q,0,m0?) = i_*A(m0)/(2*16*pi**2);
*id den(0, m?) = -1/(m*m); 

* massless bubbles set to zero in dimensional reg
id A(0) = 0;

#do f = {A,B,C,D,E,F}
 id 'f'(?m) = 'f'([],?m);
 repeat;
  id Delta(q,mu?)*'f'(?m) = 'f'(mu,?m);
 endrepeat;
#enddo

id Delta(p?,mu?)= d_(p,mu);
*id Delta(ro?,mu?)= d_(ro,mu);

*=========================================================================

#endprocedure


