#-
Off statistics;
* chiral stats, ..
s [-],[+],[],pi,ep,Nc;
cf Eps,Psi,bPsi,ga,om,op,Ga,Den,Delta,DeltaP;
auto cf A,B,C,D,E,F,G,H;
f den;
t Y(cyclic);

s n; dimension n;
s sq2;
i mu1,mu2,mu3,mu4;
auto i mu,nu,ro,al,be;

auto s m,M,r,xi;
auto c c;
auto v p,q;

dimension 4;
i PP, PM;
auto i P,Q;

dimension 4;
auto i i,j,k;

auto cf B, DB;
cf g, mom,mom1,mmom2,mom3;
s x, n;
auto v p;
f f, dx, [1];

f [B0], [B1], [B00], [B11], [B001], [B111], [B0000], [B0011], [B1111], [mom], [mom1],[mmom2];
f [DB0], [DB1], [DB00], [DB11], [DB001], [DB111], [DB0000], [DB0011], [DB1111], [1], [mmom2],[mom3];
Set commuting:    B0, B1, B00, B11, B001, B111, B0000, B0011, B1111, mom, mom1, mmom2, DB0, DB1, DB00, DB11, DB001, DB111, DB0000, DB0011, DB1111;
Set noncommuting: [B0], [B1], [B00], [B11], [B001], [B111], [B0000], [B0011], [B1111], [mom], [mom1],[mmom2], [DB0], [DB1], [DB00], [DB11], [DB001], [DB111], [DB0000], [DB0011], [DB1111];
Set derivative:   [DB0], [DB1], [DB00], [DB11], [DB001], [DB111], [DB0000], [DB0011], [DB1111], [1], [mmom2],[mom3];
