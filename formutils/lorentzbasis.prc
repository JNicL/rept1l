#procedure lorentzbasis
#define i "1"
repeat;
  id,once, ga(i?,j?,[+],mu1?,mu2?,mu3?,?a) = +i_*ga(i,j,[+],be'i',?a)*e_(mu1,mu2,mu3,be'i')
                                                 + ga(i,j,[+],mu3,?a)*d_(mu1,mu2)
                                                 - ga(i,j,[+],mu2,?a)*d_(mu1,mu3)
                                                 + ga(i,j,[+],mu1,?a)*d_(mu2,mu3);

  sum be'i';
  #redefine i "{'i'+1}"
endrepeat;

repeat;
  id,once, ga(i?,j?,[-],mu1?,mu2?,mu3?,?a) = -i_*ga(i,j,[-],be'i',?a)*e_(mu1,mu2,mu3,be'i')
                                                 + ga(i,j,[-],mu3,?a)*d_(mu1,mu2)
                                                 - ga(i,j,[-],mu2,?a)*d_(mu1,mu3)
                                                 + ga(i,j,[-],mu1,?a)*d_(mu2,mu3);
  sum be'i';
  #redefine i "{'i'+1}"
endrepeat;

repeat;
  id,once, ga(i?,j?,mu1?,mu2?,mu3?,?a) = (ga(i,j,[+],be'i',?a)-ga(i,j,[-],be'i',?a)) *
                                         i_*e_(mu1,mu2,mu3,be'i')
                                         + ga(i,j,mu3,?a) *d_(mu1,mu2)
                                         - ga(i,j,mu2,?a) *d_(mu1,mu3)
                                         + ga(i,j,mu1,?a) *d_(mu2,mu3);
  sum be'i';
  #redefine i "{'i'+1}"
endrepeat;
.sort

id ga(i?,j?,[-],mu1?,mu2?,?m) = 1/2*ga(i,j,mu1,mu2,?m) - 1/2*ga(i,j,5,mu1,mu2,?m);
id ga(i?,j?,[+],mu1?,mu2?,?m) = 1/2*ga(i,j,mu1,mu2,?m) + 1/2*ga(i,j,5,mu1,mu2,?m);

#define i "1"
repeat;
  #define j "{'i'+1}"
 id,once ga(i?,j?,5,mu1?,mu2?) = ga(i,j,5)*d_(mu1,mu2)-i_/2*e_(mu1,mu2,be'i',be'j')*si(i,j,be'i',be'j');
  sum be'i',be'j';
  #redefine i "{'i'+2}"
endrepeat;

#define i "1"
repeat;
  #define j "{'i'+1}"
  id,once ga(i?,j?,5)*si(j?,k?,mu1?,mu2?) = -i_/2*e_(mu1,mu2,be'i',be'j')*si(i,k,be'i',be'j');
  sum be'i',be'j';
  #redefine i "{'i'+2}"
endrepeat;


.sort

id ga(i?,j?,mu1?,mu2?) = ga(i,j)*d_(mu1,mu2)-i_*si(i,j,mu1,mu2);
.sort

id  e_(mu1?,mu2?,mu3?,mu4?)*e_(nu1?,nu2?,nu3?,nu4?)=
    - d_(mu1,nu1)*d_(mu2,nu2)*d_(mu3,nu3)*d_(mu4,nu4)
    + d_(mu1,nu1)*d_(mu2,nu2)*d_(mu3,nu4)*d_(mu4,nu3)
    + d_(mu1,nu1)*d_(mu2,nu3)*d_(mu3,nu2)*d_(mu4,nu4)
    - d_(mu1,nu1)*d_(mu2,nu3)*d_(mu3,nu4)*d_(mu4,nu2)
    - d_(mu1,nu1)*d_(mu2,nu4)*d_(mu3,nu2)*d_(mu4,nu3)
    + d_(mu1,nu1)*d_(mu2,nu4)*d_(mu3,nu3)*d_(mu4,nu2)
    + d_(mu1,nu2)*d_(mu2,nu1)*d_(mu3,nu3)*d_(mu4,nu4)
    - d_(mu1,nu2)*d_(mu2,nu1)*d_(mu3,nu4)*d_(mu4,nu3)
    - d_(mu1,nu2)*d_(mu2,nu3)*d_(mu3,nu1)*d_(mu4,nu4)
    + d_(mu1,nu2)*d_(mu2,nu3)*d_(mu3,nu4)*d_(mu4,nu1)
    + d_(mu1,nu2)*d_(mu2,nu4)*d_(mu3,nu1)*d_(mu4,nu3)
    - d_(mu1,nu2)*d_(mu2,nu4)*d_(mu3,nu3)*d_(mu4,nu1)
    - d_(mu1,nu3)*d_(mu2,nu1)*d_(mu3,nu2)*d_(mu4,nu4)
    + d_(mu1,nu3)*d_(mu2,nu1)*d_(mu3,nu4)*d_(mu4,nu2)
    + d_(mu1,nu3)*d_(mu2,nu2)*d_(mu3,nu1)*d_(mu4,nu4)
    - d_(mu1,nu3)*d_(mu2,nu2)*d_(mu3,nu4)*d_(mu4,nu1)
    - d_(mu1,nu3)*d_(mu2,nu4)*d_(mu3,nu1)*d_(mu4,nu2)
    + d_(mu1,nu3)*d_(mu2,nu4)*d_(mu3,nu2)*d_(mu4,nu1)
    + d_(mu1,nu4)*d_(mu2,nu1)*d_(mu3,nu2)*d_(mu4,nu3)
    - d_(mu1,nu4)*d_(mu2,nu1)*d_(mu3,nu3)*d_(mu4,nu2)
    - d_(mu1,nu4)*d_(mu2,nu2)*d_(mu3,nu1)*d_(mu4,nu3)
    + d_(mu1,nu4)*d_(mu2,nu2)*d_(mu3,nu3)*d_(mu4,nu1)
    + d_(mu1,nu4)*d_(mu2,nu3)*d_(mu3,nu1)*d_(mu4,nu2)
    - d_(mu1,nu4)*d_(mu2,nu3)*d_(mu3,nu2)*d_(mu4,nu1);
.sort

** todo  Need more robust substitutions
#define i "1"
#define j "{'i'+1}"
#do repeat= 1,1
  id once ga(i1?,j1?,?m1,mu?,?m2)  *ga(i2?,j2?,?m3,mu?,?m4) =
          Ga(i1 ,j1 ,?m1,nu'i',?m2)*Ga(i2 ,j2 ,?m3,nu'i',?m4);
  #define j "{'i'+1}"
  id once si(i1?,j1?,mu?,nu?)*si(i2?,j2?,mu?,nu?) =
          Si(i1,j1,be'i',be'j')*Si(i2,j2,be'i',be'j');
  id once si(i1?,j1?,mu?,nu?)*si(i2?,j2?,nu?,mu?) =
          -Si(i1,j1,be'i',be'j')*Si(i2,j2,be'i',be'j');
#enddo
.sort

#do repeat= 1,1
  #redefine i "{'i'+2}"
  #define j "{'i'+1}"
  id once si(i1?,j1?,mu?,?m2)*si(i2?,j2?,mu?,?m4) =
          Si(i1,j1,be'i',?m2)*Si(i2,j2,be'i',?m4);
#enddo
.sort

#do repeat= 1,1
  #redefine i "{'i'+2}"
  #define j "{'i'+1}"
  id once si(i1?,j1?,mu?,nu?)*e_(mu1?,mu2?,mu?,nu?) =
          Si(i1,j1,be'i',be'j')*e_(mu1,mu2,be'i',be'j');
#enddo
.sort

#do repeat= 1,1
  #redefine i "{'i'+2}"
  #define j "{'i'+1}"
  id once si(i1?,j1?,mu?,nu?)*e_(mu1?,mu2?,nu?,mu?) =
          -Si(i1,j1,be'i',be'j')*e_(mu1,mu2,be'i',be'j');
#enddo
.sort

#do repeat= 1,1
  #redefine i "{'i'+2}"
  #define j "{'i'+1}"
  id once si(i1?,j1?,mu?,?m2)*e_(mu1?,mu2?,mu?,?m4) =
          Si(i1,j1,be'i',?m2)*e_(mu1,mu2,be'i',?m4);
#enddo
.sort

#do repeat= 1,1
  #redefine i "{'i'+2}"
  #define j "{'i'+1}"
  id once si(i1?,j1?,mu4?,mu?)*e_(mu1?,mu2?,mu5?,mu?) =
          Si(i1,j1,mu4,be'i')*e_(mu1,mu2,mu5,be'i');
#enddo
.sort
#do repeat= 1,1
  #redefine i "{'i'+2}"
  #define j "{'i'+1}"
  id once si(i1?,j1?,mu?,mu4?)*e_(mu1?,mu2?,mu4?,mu?) =
          -Si(i1,j1,mu4,be'i')*e_(mu1,mu2,mu5,be'i');
#enddo
.sort

id Ga(?m) = ga(?m);
id Si(?m) = si(?m);
.sort


id ga(i?,j?,5,?m) = ga(i,j,[+],?m)-ga(i,j,[-],?m);
id ga(i?,j?,?m) = ga(i,j,[+],?m)+ga(i,j,[-],?m);
.sort

repeat;
 id ga(i1?,i2?,?m1,mu?,[-],?m2) = ga(i1,i2,?m1,[+],mu,?m2);
 id ga(i1?,i2?,?m1,mu?,[+],?m2) = ga(i1,i2,?m1,[-],mu,?m2);
 id ga(?m1,[-],[-],?m2) = ga(?m1,[-],?m2);
 id ga(?m1,[+],[+],?m2) = ga(?m1,[+],?m2);
 id ga(?m1,[+],[-],?m2) = 0;
 id ga(?m1,[-],[+],?m2) = 0;
endrepeat;

id Si(i1?,j1?,mu1?,mu2?)=Si(i1,j1,Sis(mu1,mu2));
id Si(i1?,j1?,Sis(mu1?,mu2?))=Si(i1,j1,mu1,mu2);
id Si(?a)=si(?a);
id n = 4;

.sort

#endprocedure
