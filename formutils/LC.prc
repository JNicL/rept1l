#procedure LC

*------------------------------------------------------------------------------*
*-------------*
*-  1-point  -*
*-------------*

id A([],?m) = A0(?m);
id A(mu?, [],?m) = 0;
id A(mu?,nu?, [],?m) = d_(mu,nu) * A00(?m);
id A(mu1?,mu2?,mu3?,[],?m) = 0;
id A(mu1?,mu2?,mu3?,mu4?,[],?m) = 
(d_(mu1,mu4)*d_(mu2,mu3) + d_(mu1,mu3)*d_(mu2,mu4) +
d_(mu1,mu2)*d_(mu3,mu4))*A0000(?m);
id A(mu1?,mu2?,mu3?,mu4?,mu5?,[],?m) = 0;
id A(mu1?,mu2?,mu3?,mu4?,mu5?,mu6?,[],?m) = 
(d_(mu1,mu6)*d_(mu2,mu5)*d_(mu3,mu4) + d_(mu1,mu5)*d_(mu2,mu6)*d_(mu3,mu4) +
 d_(mu1,mu2)*d_(mu3,mu5)*d_(mu4,mu6) + d_(mu1,mu4)*d_(mu2,mu5)*d_(mu3,mu6) +
 d_(mu1,mu3)*d_(mu2,mu5)*d_(mu4,mu6) + d_(mu1,mu4)*d_(mu2,mu3)*d_(mu5,mu6) +
 d_(mu1,mu5)*d_(mu2,mu3)*d_(mu4,mu6) + d_(mu1,mu5)*d_(mu2,mu4)*d_(mu3,mu6) +
 d_(mu1,mu3)*d_(mu2,mu6)*d_(mu4,mu5) + d_(mu1,mu6)*d_(mu2,mu3)*d_(mu4,mu5) +
 d_(mu1,mu2)*d_(mu3,mu6)*d_(mu4,mu5) + d_(mu1,mu3)*d_(mu2,mu4)*d_(mu5,mu6) +
 d_(mu1,mu4)*d_(mu2,mu6)*d_(mu3,mu5) + d_(mu1,mu6)*d_(mu2,mu4)*d_(mu3,mu5) +
 d_(mu1,mu2)*d_(mu3,mu4)*d_(mu5,mu6))*A000000(?m);

*-------------*
*-  2-point  -*
*-------------*

id B([],p?,?m) = B0(p,?m);
id B(mu?,[],p?,?m) = p(mu) * B1(p,?m);
id B(mu?,nu?,[],p?,m1?,m2?) = 
  d_(mu,nu) * B00(p,m1,m2) + p(mu)*p(nu)*B11(p,m1,m2);

id B(mu?,nu?,ro?,[],p?,m1?,m2?) = 
  (d_(mu,nu)*p(ro) + d_(nu,ro)*p(mu) + d_(mu,ro)*p(nu))*B001(p,m1,m2) +
  p(mu)*p(nu)*p(ro)*B111(p,m1,m2);

*id B(mu?,mu?,nu?,nu?,[],p?,m1?,m1?) = 
*(p.p + 2*m1**2)*A0(m1) + m**4*B0(p,m1,m1);

id B(mu?,nu?,al?,be?,[],p?,m1?,m2?) = 
  (d_(mu,nu)*d_(al,be)+d_(mu,al)*d_(nu,be)+d_(mu,be)*d_(nu,al))*B0000(p,m1,m2)+
  (d_(mu,nu)*p(al)*p(be)+d_(mu,al)*p(nu)*p(be)+d_(nu,al)*p(mu)*p(be)+
   d_(be,mu)*p(nu)*p(al)+d_(be,nu)*p(mu)*p(al)+d_(be,al)*p(mu)*p(nu))*B0011(p,m1,m2)
   +p(mu)*p(nu)*p(al)*p(be)*B1111(p,m1,m2);

id B(mu1?,mu2?,mu3?,mu4?,mu5?,[],p?,m1?,m2?) = 
p(mu1)*p(mu2)*p(mu3)*p(mu4)*p(mu5)*B11111(p,m1,m2)+
(d_(mu1,mu2)*p(mu3)*p(mu4)*p(mu5)+d_(mu1,mu3)*p(mu2)*p(mu4)*p(mu5) +
 d_(mu1,mu4)*p(mu2)*p(mu3)*p(mu5)+d_(mu1,mu5)*p(mu2)*p(mu3)*p(mu4) +
 d_(mu2,mu3)*p(mu1)*p(mu4)*p(mu5)+d_(mu2,mu4)*p(mu1)*p(mu3)*p(mu5) +
 d_(mu2,mu5)*p(mu1)*p(mu3)*p(mu4)+d_(mu3,mu4)*p(mu1)*p(mu2)*p(mu5) +
 d_(mu3,mu5)*p(mu1)*p(mu2)*p(mu4)+d_(mu4,mu5)*p(mu1)*p(mu2)*p(mu3))*
B00111(p,m1,m2) +
(d_(mu1,mu4)*d_(mu2,mu3)*p(mu5) + d_(mu1,mu3)*d_(mu2,mu4)*p(mu5) +
 d_(mu1,mu2)*d_(mu3,mu4)*p(mu5) + d_(mu1,mu3)*d_(mu2,mu5)*p(mu4) +
 d_(mu1,mu5)*d_(mu2,mu3)*p(mu4) + d_(mu1,mu2)*d_(mu3,mu5)*p(mu4) +
 d_(mu1,mu5)*d_(mu2,mu4)*p(mu3) + d_(mu1,mu4)*d_(mu2,mu5)*p(mu3) +
 d_(mu1,mu2)*d_(mu4,mu5)*p(mu3) + d_(mu1,mu3)*d_(mu4,mu5)*p(mu2) +
 d_(mu1,mu4)*d_(mu3,mu5)*p(mu2) + d_(mu1,mu5)*d_(mu3,mu4)*p(mu2) +
 d_(mu2,mu4)*d_(mu3,mu5)*p(mu1) + d_(mu2,mu5)*d_(mu3,mu4)*p(mu1) +
 d_(mu2,mu3)*d_(mu4,mu5)*p(mu1))*B00001(p,m1,m2);

id B(mu1?,mu2?,mu3?,mu4?,mu5?,mu6?,[],p?,m1?,m2?) = 
p(mu1)*p(mu2)*p(mu3)*p(mu4)*p(mu5)*p(mu6) * B111111(p,m1,m2) +
(d_(mu1,mu2)*p(mu3)*p(mu4)*p(mu5)*p(mu6) +
 d_(mu1,mu3)*p(mu2)*p(mu4)*p(mu5)*p(mu6) +
 d_(mu1,mu4)*p(mu2)*p(mu3)*p(mu5)*p(mu6) +
 d_(mu1,mu5)*p(mu2)*p(mu3)*p(mu4)*p(mu6) +
 d_(mu1,mu6)*p(mu2)*p(mu3)*p(mu4)*p(mu5) +
 d_(mu2,mu3)*p(mu1)*p(mu4)*p(mu5)*p(mu6) +
 d_(mu2,mu4)*p(mu1)*p(mu3)*p(mu5)*p(mu6) +
 d_(mu2,mu5)*p(mu1)*p(mu3)*p(mu4)*p(mu6) +
 d_(mu2,mu6)*p(mu1)*p(mu3)*p(mu4)*p(mu5) +
 d_(mu3,mu4)*p(mu1)*p(mu2)*p(mu5)*p(mu6) +
 d_(mu3,mu5)*p(mu1)*p(mu2)*p(mu4)*p(mu6) +
 d_(mu3,mu6)*p(mu1)*p(mu2)*p(mu4)*p(mu5) +
 d_(mu4,mu5)*p(mu1)*p(mu2)*p(mu3)*p(mu6) +
 d_(mu4,mu6)*p(mu1)*p(mu2)*p(mu3)*p(mu5) +
 d_(mu5,mu6)*p(mu1)*p(mu2)*p(mu3)*p(mu4)) * B001111(p,m1,m2) +
(d_(mu1,mu4)*d_(mu2,mu3)*p(mu5)*p(mu6) +
 d_(mu1,mu3)*d_(mu2,mu4)*p(mu5)*p(mu6) +
 d_(mu1,mu2)*d_(mu3,mu4)*p(mu5)*p(mu6) +
 d_(mu1,mu3)*d_(mu2,mu5)*p(mu4)*p(mu6) +
 d_(mu1,mu5)*d_(mu2,mu3)*p(mu4)*p(mu6) +
 d_(mu1,mu2)*d_(mu3,mu5)*p(mu4)*p(mu6) +
 d_(mu1,mu2)*d_(mu3,mu6)*p(mu4)*p(mu5) +
 d_(mu1,mu6)*d_(mu2,mu3)*p(mu4)*p(mu5) +
 d_(mu1,mu3)*d_(mu2,mu6)*p(mu4)*p(mu5) +
 d_(mu1,mu5)*d_(mu2,mu4)*p(mu3)*p(mu6) +
 d_(mu1,mu4)*d_(mu2,mu5)*p(mu3)*p(mu6) +
 d_(mu1,mu2)*d_(mu4,mu5)*p(mu3)*p(mu6) +
 d_(mu1,mu4)*d_(mu2,mu6)*p(mu3)*p(mu5) +
 d_(mu1,mu6)*d_(mu2,mu4)*p(mu3)*p(mu5) +
 d_(mu1,mu2)*d_(mu4,mu6)*p(mu3)*p(mu5) +
 d_(mu1,mu6)*d_(mu2,mu5)*p(mu3)*p(mu4) +
 d_(mu1,mu2)*d_(mu5,mu6)*p(mu3)*p(mu4) +
 d_(mu1,mu5)*d_(mu2,mu6)*p(mu3)*p(mu4) +
 d_(mu1,mu3)*d_(mu4,mu5)*p(mu2)*p(mu6) +
 d_(mu1,mu4)*d_(mu3,mu5)*p(mu2)*p(mu6) +
 d_(mu1,mu5)*d_(mu3,mu4)*p(mu2)*p(mu6) +
 d_(mu1,mu6)*d_(mu3,mu4)*p(mu2)*p(mu5) +
 d_(mu1,mu4)*d_(mu3,mu6)*p(mu2)*p(mu5) +
 d_(mu1,mu3)*d_(mu4,mu6)*p(mu2)*p(mu5) +
 d_(mu1,mu3)*d_(mu5,mu6)*p(mu2)*p(mu4) +
 d_(mu1,mu6)*d_(mu3,mu5)*p(mu2)*p(mu4) +
 d_(mu1,mu5)*d_(mu3,mu6)*p(mu2)*p(mu4) +
 d_(mu1,mu5)*d_(mu4,mu6)*p(mu2)*p(mu3) +
 d_(mu1,mu4)*d_(mu5,mu6)*p(mu2)*p(mu3) +
 d_(mu1,mu6)*d_(mu4,mu5)*p(mu2)*p(mu3) +
 d_(mu2,mu4)*d_(mu3,mu5)*p(mu1)*p(mu6) +
 d_(mu2,mu5)*d_(mu3,mu4)*p(mu1)*p(mu6) +
 d_(mu2,mu3)*d_(mu4,mu5)*p(mu1)*p(mu6) +
 d_(mu2,mu3)*d_(mu4,mu6)*p(mu1)*p(mu5) +
 d_(mu2,mu4)*d_(mu3,mu6)*p(mu1)*p(mu5) +
 d_(mu2,mu6)*d_(mu3,mu4)*p(mu1)*p(mu5) +
 d_(mu2,mu6)*d_(mu3,mu5)*p(mu1)*p(mu4) +
 d_(mu2,mu5)*d_(mu3,mu6)*p(mu1)*p(mu4) +
 d_(mu2,mu3)*d_(mu5,mu6)*p(mu1)*p(mu4) +
 d_(mu2,mu6)*d_(mu4,mu5)*p(mu1)*p(mu3) +
 d_(mu2,mu5)*d_(mu4,mu6)*p(mu1)*p(mu3) +
 d_(mu2,mu4)*d_(mu5,mu6)*p(mu1)*p(mu3) +
 d_(mu3,mu5)*d_(mu4,mu6)*p(mu1)*p(mu2) +
 d_(mu3,mu6)*d_(mu4,mu5)*p(mu1)*p(mu2) +
 d_(mu3,mu4)*d_(mu5,mu6)*p(mu1)*p(mu2)) * B000011(p,m1,m2) +
(d_(mu1,mu6)*d_(mu2,mu5)*d_(mu3,mu4) + d_(mu1,mu5)*d_(mu2,mu6)*d_(mu3,mu4) +
 d_(mu1,mu2)*d_(mu3,mu5)*d_(mu4,mu6) + d_(mu1,mu4)*d_(mu2,mu5)*d_(mu3,mu6) +
 d_(mu1,mu3)*d_(mu2,mu5)*d_(mu4,mu6) + d_(mu1,mu4)*d_(mu2,mu3)*d_(mu5,mu6) +
 d_(mu1,mu5)*d_(mu2,mu3)*d_(mu4,mu6) + d_(mu1,mu5)*d_(mu2,mu4)*d_(mu3,mu6) +
 d_(mu1,mu3)*d_(mu2,mu6)*d_(mu4,mu5) + d_(mu1,mu6)*d_(mu2,mu3)*d_(mu4,mu5) +
 d_(mu1,mu2)*d_(mu3,mu6)*d_(mu4,mu5) + d_(mu1,mu3)*d_(mu2,mu4)*d_(mu5,mu6) +
 d_(mu1,mu4)*d_(mu2,mu6)*d_(mu3,mu5) + d_(mu1,mu6)*d_(mu2,mu4)*d_(mu3,mu5) +
 d_(mu1,mu2)*d_(mu3,mu4)*d_(mu5,mu6)) * B000000(p,m1,m2);

*-------------*
*-  3-point  -*
*-------------*

id C([],p1?,p2?,?m) = C0(p1,p2,?m);
id C(mu?,[],p1?,p2?,?m) = p1(mu)*C1(p1,p2,?m) + p2(mu)*C2(p1,p2,?m);
id C(mu1?,mu2?,[],p1?,p2?,?m) = d_(mu1,mu2)*C00(p1,p2,?m) +
                                p1(mu1)*p1(mu2)*C11(p1,p2,?m) + 
                                p2(mu2)*p2(mu1)*C22(p1,p2,?m) + 
                                (p1(mu1)*p2(mu2) +
                                 p1(mu2)*p2(mu1))*C12(p1,p2,?m);
id C(mu1?,mu2?,mu3?,[],p1?,p2?,?m) = (d_(mu1, mu2)*p1(mu3) + 
                                      d_(mu2, mu3)*p1(mu1) + 
                                      d_(mu3, mu1)*p1(mu2) ) * C001(p1,p2,?m)  +
                                    ( d_(mu1, mu2)*p2(mu3) + 
                                      d_(mu2, mu3)*p2(mu1) + 
                                      d_(mu3, mu1)*p2(mu2) ) * C002(p1,p2,?m)  +
                                      p1(mu1)*p1(mu2)*p1(mu3)*C111(p1,p2,?m)   +
                                      p2(mu1)*p2(mu2)*p2(mu3)*C222(p1,p2,?m)   +
                                    ( p1(mu1)*p1(mu2)*p2(mu3) + 
                                      p1(mu1)*p2(mu2)*p1(mu3) + 
                                      p2(mu1)*p1(mu2)*p1(mu3) )*C112(p1,p2,?m) +
                                    ( p2(mu1)*p2(mu2)*p1(mu3) + 
                                      p2(mu1)*p1(mu2)*p2(mu3) + 
                                      p1(mu1)*p2(mu2)*p2(mu3) )*C122(p1,p2,?m);


id C(mu1?,mu2?,mu3?,mu4?,[],p1?,p2?,?m) = 
+(d_(mu1,mu2)*d_(mu3,mu4) + d_(mu1,mu3)*d_(mu2,mu4) +
  d_(mu1,mu4)*d_(mu2,mu3))*C0000(p1,p2,?m)
+(d_(mu1,mu2)*p1(mu3)*p1(mu4) + d_(mu1,mu3)*p1(mu2)*p1(mu4) +
  d_(mu1,mu4)*p1(mu2)*p1(mu3) + d_(mu2,mu3)*p1(mu1)*p1(mu4) +
  d_(mu2,mu4)*p1(mu1)*p1(mu3) + d_(mu3,mu4)*p1(mu1)*p1(mu2))*C0011(p1,p2,?m)
+(d_(mu1,mu2)*p2(mu3)*p2(mu4) + d_(mu1,mu3)*p2(mu2)*p2(mu4) +
  d_(mu1,mu4)*p2(mu2)*p2(mu3) + d_(mu2,mu3)*p2(mu1)*p2(mu4) +
  d_(mu2,mu4)*p2(mu1)*p2(mu3) + d_(mu3,mu4)*p2(mu1)*p2(mu2))*C0022(p1,p2,?m)
+(d_(mu1,mu2)*(p1(mu3)*p2(mu4) + p1(mu4)*p2(mu3)) +
  d_(mu1,mu3)*(p1(mu2)*p2(mu4) + p1(mu4)*p2(mu2)) +
  d_(mu1,mu4)*(p1(mu2)*p2(mu3) + p1(mu3)*p2(mu2)) +
  d_(mu2,mu3)*(p1(mu1)*p2(mu4) + p1(mu4)*p2(mu1)) +
  d_(mu2,mu4)*(p1(mu1)*p2(mu3) + p1(mu3)*p2(mu1)) +
  d_(mu3,mu4)*(p1(mu1)*p2(mu2) + p1(mu2)*p2(mu1)))*C0012(p1,p2,?m)
+(p1(mu1)*p1(mu2)*p1(mu3)*p1(mu4))*C1111(p1,p2,?m)
+(p2(mu1)*p2(mu2)*p2(mu3)*p2(mu4))*C2222(p1,p2,?m)
+(p1(mu1)*p1(mu2)*p2(mu3)*p2(mu4) + p1(mu1)*p1(mu3)*p2(mu2)*p2(mu4) +
  p1(mu1)*p1(mu4)*p2(mu2)*p2(mu3) + p1(mu2)*p1(mu3)*p2(mu1)*p2(mu4) +
  p1(mu2)*p1(mu4)*p2(mu1)*p2(mu3) +
  p1(mu3)*p1(mu4)*p2(mu1)*p2(mu2))*C1122(p1,p2,?m);
                                  
.sort

*------------------------------------------------------------------------------*

#endprocedure
