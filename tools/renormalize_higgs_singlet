#!/usr/bin/env python
###############################################################################
#                          renormalize_higgs_singlet                          #
###############################################################################

from __future__ import print_function

__doc__ = """ Renormalizes the Higgs sector of the Higgs singlet extension of
the SM.  Particles are renormalized onshell and Higgs singlet parameters are
renormalized msbar.  """

from past.builtins import xrange
from six import iteritems
from collections import OrderedDict as OD
from rept1l.logging_setup import log
from sympy import Symbol, I, sympify, Poly
from sympy import sympify as sympy_simplify
from rept1l.renormalize import Renormalize
from rept1l.renormalize import RenormalizeTadpole
from rept1l.renormalize import RenormalizeVertex
from rept1l.renormalize import RenormalizeSelfenergy
from rept1l.renormalize import get_particle_ct
from rept1l.renormalize import renormalize_msbar, substitute_msbar_parameter
from rept1l.counterterms import Counterterms
from rept1l.renormalize.renormalize_utils import solve_renormalization
from rept1l.coupling import RCoupling
from rept1l.renormalize import RenoConst
from rept1l.helper_lib import Rept1lArgParser
from rept1l.helper_lib import find_vertex
from rept1l.regularize import Regularize

import rept1l.Model as Model
model = Model.model

PHl = model.P.Hl
MHL = PHl.mass.name
dtHl = 'dtHl'
PHh = model.P.Hh
MHH = PHh.mass.name
dtHh = 'dtHh'

Pt = model.P.t
Ptt = model.P.t__tilde__

PZ = model.P.Z
MZ = PZ.mass.name

PW = model.P.W__minus__
MW = PW.mass.name

# PSI = model.P.SI
# MSI = PSI.mass.name

class RenormalizeHS(Renormalize):

  RenoConst = RenoConst

  def __init__(self, particles, *args, **kwargs):
    super(RenormalizeHS, self).__init__(particles, RenoConst, *args, **kwargs)

  @classmethod
  def renormalize_neutral_higgs(cls, simplify=True, **kwargs):
    """ Renormalize the neutral higgs onshell and prohibit particle mixing. """
    # neutral_higgs = [[PHl, PHh], model.P.SI]
    neutral_higgs = [[PHl, PHh]]
    cls(neutral_higgs, simplify=simplify, **kwargs)

  @classmethod
  def renormalize_tadpoles(cls, simplify=True, **kwargs):
    """ Higgs tadpole renormalization in the Higgs Singlet Extension of the SM.
    """

    cls.load_storage()
    dtH = RenormalizeTadpole(PHl, renoscheme='onshell', tadpole_expr=dtHl,
                             **kwargs)
    dtHS = RenormalizeTadpole(PHh, renoscheme='onshell', tadpole_expr=dtHh,
                              **kwargs)

    sols = dtH.solutions
    sols.update(dtHS.solutions)
    cls.update_renos(sols, renoscheme='onshell', simplify=simplify)


  # @classmethod
  # def renormalize_y4(cls, simplify=True, **kwargs):
  #   cls.load_storage()
  #   dy4 = Symbol('dy4')
  #   renormalize_msbar(cls, [PSI.name, PSI.name, PHl.name], dy4.name,
  #                     simplify=simplify)

  @classmethod
  def renormalize_yukaux(cls, **kwargs):
    cts = set()

    # particles = [PSI.name, PSI.name, PSI.name, PSI.name]
    particles = [PSI.name, PSI.name, PHl.name, PHl.name]
    vertices = find_vertex(particles)
    vertex = vertices[0]
    ct1 = RenormalizeVertex(vertex, renoscheme='MS',
                            solve_eq=False, 
                            compute_wavefunctions=False)
    assert(len(ct1.renorm_conditions) == 1)
    reno1 = ct1.renorm_conditions[0]

    cts = cts.union(ct1.ct_renorm)

    # particles = [PHa.name, PHa.name, PHh.name]
    # particles = [PZ.name, PZ.name, PHh.name]
    # particles = [PSI.name, PSI.name, PHh.name]
    particles = [PSI.name, PSI.name, PHh.name, PHh.name]
    vertices = find_vertex(particles)
    vertex = vertices[0]
    ct2 = RenormalizeVertex(vertex, renoscheme='MS',
                            solve_eq=False, 
                            compute_wavefunctions=False)
    assert(len(ct2.renorm_conditions) == 1)
    reno2 = ct2.renorm_conditions[0]

    cts = cts.union(ct2.ct_renorm)
    particles = [PHl.name, PHl.name, PHh.name]
    vertices = find_vertex(particles)
    vertex = vertices[0]
    ct3 = RenormalizeVertex(vertex, renoscheme='MS',
                            solve_eq=False,
                            compute_wavefunctions=False)
    assert(len(ct3.renorm_conditions) == 1)
    reno3 = ct3.renorm_conditions[0]

    cts = cts.union(ct3.ct_renorm)

    to_solve = [Symbol(u) for u in ['dy4', 'dy5', 'da']]
    for u in to_solve:
      cts.remove(u)
    reno_sols = RenoConst.build_ms_ct_solutions(cts,
                                                plugin_ms_values=False)

    renorm_conditions = [reno1, reno2, reno3]
    solutions = solve_renormalization(renorm_conditions, to_solve)
    nreno = str(len(renorm_conditions))
    msg = ('Attempt to solve ' + nreno + ' renormalization conditions for: '
           + ','.join(str(u) for u in to_solve))
    log(msg, 'info')
    if len(solutions) == len(to_solve):
      msg = 'Found solution.'
      log(msg, 'info')
    else:
      log('Solution not found.', 'warning')
      msg = 'renorm_conditions:' + str(renorm_conditions)
      log(msg, 'debug')
      msg = 'len(renorm_conditions):' + str(len(renorm_conditions))
      log(msg, 'debug')
      log('Renos:' + str(to_solve), 'debug')
      raise Exception('Renormalization failed.')

    # substitute for msbar sols
    solutions = {u: v.subs(reno_sols) for u, v in iteritems(solutions)}
    cls.update_renos(solutions, scaledep='muMS', renoscheme='MS', simplify=simplify)

  @classmethod
  def renormalize_alpha(cls, method='FJTS', simplify=True, **kwargs):
    """ Sin(alpha) is renormalized msbar for the vertex H t t~.  """
    cls.load_storage()
    da = Symbol('da')

    if method == 'MS':
      renormalize_msbar(cls, [PHl.name, Pt.name, Ptt.name], da.name,
                        simplify=simplify, renoscheme='MS_BSM',
                        scaledep='muMS_BSM')

    elif method == 'FJTS':
      # dZHHS wavefunction
      Hl_ct = get_particle_ct(PHl, only_offdiagonal=True, mass=False)
      assert(len(Hl_ct) == 1)
      dZHlHh = Hl_ct[0]

      # dZHSH wavefunction
      Hh_ct = get_particle_ct(PHh, only_offdiagonal=True, mass=False)
      assert(len(Hh_ct) == 1)
      dZHhHl = Hh_ct[0]
      msrenos = [dZHlHh, dZHhHl]
      reno_sols = RenoConst.build_ms_ct_solutions(msrenos,
                                                  plugin_ms_values=False,
                                                  scaledep='muMS_BSM',
                                                  renoscheme='MS_BSM')

      da_val = (dZHhHl - dZHlHh) / 4

      da_MS_sym = Symbol(da.name + '_MS')
      cls.update_renos({da_MS_sym: da_val.subs(reno_sols)}, renoscheme='MS_BSM',
                       simplify=simplify, scaledep='muMS_BSM')
      cls.update_renos({da: da_MS_sym}, renoscheme=method,
                       simplify=simplify)

    elif method == 'l3_MS':
      # construct counterterm dl3 = da_coeff*da + ...
      l3 = Symbol('l3')
      ctbase = [u for u in Counterterms.get_all_ct()]
      dl3 = Poly(Counterterms.ctdict[l3], ctbase)
      dl3 = dl3.as_dict()
      da_pos = ctbase.index(da)
      da_key = tuple(0 if i != da_pos else 1 for i in xrange(len(ctbase)))
      zerokey = tuple([0] * len(ctbase))
      del dl3[zerokey]
      dl3 = {key: sympy_simplify(val) for key, val in iteritems(dl3)}
      da_coeff = dl3[da_key]
      del dl3[da_key]
      cts = [ctbase[u.index(1)] for u in dl3.keys()]

      # compute MSbar ct of all ct in dl3
      reno_sols = RenoConst.build_ms_ct_solutions(cts,
                                                  plugin_ms_values=False,
                                                  scaledep='muMS_BSM',
                                                  renoscheme='MS_BSM')
      # construct substitution for finite parts:
      # delta_x -> delta_x -delta_x^MSbar
      reno_sols = {key: key - val for key, val in iteritems(reno_sols)}
      dl3 = -sum(ctbase[key.index(1)].subs(reno_sols) * val
                 for key, val in iteritems(dl3)) / da_coeff

      dafin = Symbol('da_l3_fin')
      cls.update_renos({dafin: dl3}, renoscheme=method, simplify=simplify)
      da_ms_sol = RenoConst.build_ms_ct_solutions([da], plugin_ms_values=False)
      cls.update_renos({da: dafin + (da.subs(da_ms_sol))}, renoscheme=method,
                       simplify=simplify)

    # renormalize alpha in MSbar in tadpole counterterm scheme II
    elif method == 'PRTS':
      tb = Symbol('tb')
      ca = Symbol('ca')
      sa = Symbol('sa')
      cMHL2 = Symbol('c' + MHL)**2
      cMHH2 = Symbol('c' + MHH)**2
      cMHL4 = Symbol('c' + MHL)**4
      cMHH4 = Symbol('c' + MHH)**4
      vevs = Symbol('vevs')
      vev = Symbol('vev')
      dtHl_s = Symbol(dtHl)
      dtHh_s = Symbol(dtHh)

      tHhHl_fj = ca*sa*(dtHh_s*(sa/vev-ca/vevs)*(2+cMHL2/cMHH2) +
                        dtHl_s*(ca/vev+sa/vevs)*(2+cMHH2/cMHL2))

      cts = [dtHh_s, dtHl_s]
      reno_sols = RenoConst.build_ms_ct_solutions(cts,
                                                  plugin_ms_values=False,
                                                  scaledep='muMS_BSM',
                                                  renoscheme='MS_BSM')
      reno_sols = {key: key - val for key, val in iteritems(reno_sols)}
      tHhHl_fin_val = tHhHl_fj.xreplace(reno_sols)

      tHhHl_fin = Symbol('dtHhHl_PRTS_fin')
      cls.update_renos({tHhHl_fin: tHhHl_fin_val}, renoscheme=method, simplify=simplify)

      da_ms_sol = RenoConst.build_ms_ct_solutions([da], plugin_ms_values=False,
                                                  scaledep='muMS_BSM',
                                                  renoscheme='MS_BSM')
      cls.update_renos({da: tHhHl_fin / (cMHH2 - cMHL2) + (da.subs(da_ms_sol))},
                       renoscheme=method, simplify=simplify)

    elif method == 'ps_bfm':
      p1 = PHl
      m1 = Symbol(MHL)
      p2 = PHh
      m2 = Symbol(MHH)

      massscale = 'MPS'
      cls.BFM_ps_template(da, p1, m1, p2, m2, massscale, method,
                          simplify=simplify, default=True, **kwargs)

    # onshell BFM: (or Pinched see e.g. 1605.04853 for the THDM)
    # only valid in Feynman Gauge (conventional or BFM formulation)
    elif method == 'BFM':
      p1 = PHl
      m1 = Symbol(MHL)
      p2 = PHh
      m2 = Symbol(MHH)

      P1M = p1.mass.name
      P2M = p2.mass.name

      arg11 = ','.join(['r' + P2M, MZ, MZ])
      arg12 = ','.join(['r' + P2M, MW, MW])
      ints1 = 'B0(' + arg11 + ')+2*cw**2*B0(' + arg12 + ')'

      arg21 = ','.join(['r' + P1M, MZ, MZ])
      arg22 = ','.join(['r' + P1M, MW, MW])
      ints2 = 'B0(' + arg21 + ')+2*cw**2*B0(' + arg22 + ')'

      fac = '-ee**2/(32*pi**2*cw**2*sw**2)*ca*sa'
      cls.BFM_template(da, p1, m1, p2, m2, fac, ints1, ints2, method,
                       simplify=simplify, **kwargs)

    # onshell renormalization via mixing selfenergy (gauge-dependent)
    elif method == 'onshell':
      HlHh_ct = get_particle_ct(model.P.Hl, only_offdiagonal=True, mass=False)
      assert(len(HlHh_ct) == 1)
      dZHlHh = HlHh_ct[0]
      HhHl_ct = get_particle_ct(model.P.Hh, only_offdiagonal=True, mass=False)
      assert(len(HhHl_ct) == 1)
      dZHhHl = HhHl_ct[0]

      da_val = (dZHhHl - dZHlHh) / 4
      cls.update_renos({da: da_val}, renoscheme=method,
                       simplify=simplify)

    elif method == 'OS':
      HlHh_ct = get_particle_ct(model.P.Hl, only_offdiagonal=True, mass=False)
      assert(len(HlHh_ct) == 1)
      dZHlHh = HlHh_ct[0]
      Hl_ct = get_particle_ct(PHl, only_diagonal=True, mass=False)
      assert(len(Hl_ct) == 1)
      dZHl = Hl_ct[0]

      HhHl_ct = get_particle_ct(model.P.Hh, only_offdiagonal=True, mass=False)
      assert(len(HhHl_ct) == 1)
      dZHhHl = HhHl_ct[0]
      Hh_ct = get_particle_ct(PHh, only_diagonal=True, mass=False)
      assert(len(Hh_ct) == 1)
      dZHh = Hh_ct[0]

      sa = Symbol('sa')
      ca = Symbol('ca')

      da_val = (ca*sa*(dZHh-dZHl) + ca**2*dZHhHl - sa**2*dZHlHh)/2
      cls.update_renos({da: da_val}, default=True, renoscheme=method, simplify=simplify)

  @classmethod
  def renormalize_l3(cls, method='FJTS', simplify=True, **kwargs):
    cls.load_storage()
    dl3 = Symbol('dl3')
    if method == 'MS':
      renormalize_msbar(cls, [PHh.name, PHl.name, PHl.name], dl3.name,
                        renoscheme=method, splitvar=True, simplify=simplify,
                        scale='muMS_BSM')

    elif method == 'FJTS':
      duv = Symbol('DeltaUV')
      l1 = Symbol('l1')
      l2 = Symbol('l2')
      l3 = Symbol('l3')
      pi = Symbol('pi')

      dl3val = duv*l3*((4*l3 + 3*l2 + 3*l1)/(32*pi**2) 
                       - sympify('3*aEW*(1+2*cw**2)/(16*pi*cw**2*sw**2)'))

      mlep = ['ME','MM','MTA']
      mupq = ['MU','MC','MT']
      mdoq = ['MD','MS','MB']
      smlep = sum(Symbol('c' + m)**2 for m in mlep)
      smupq = sum(Symbol('c' + m)**2 for m in mupq)
      smdoq = sum(Symbol('c' + m)**2 for m in mdoq)
      dl3val = dl3val + duv*l3*(smlep + 3*(smupq + smdoq))*sympify("aEW/(8*pi*cMW**2*sw**2)")
        
      cls.update_renos({dl3: dl3val}, renoscheme=method,
                       scaledep='muMS_BSM', simplify=simplify)

    elif method == 'BFM':
      # dZ_\eta1 - dZ_\eta2 = (dZ11-dZ22)/(ca2-sa2) 
      # dZ_\eta2 - dZ_\eta1 = (dZ12+dZ21)/(2*ca*sa) 
      # =>
      # dZ_\eta2 - dZ_\eta1 = (1-x) * (1) + x * (2)
      # x = (2*ca*sa)**2
      # 1-x = (ca**2+sa**2)**2-4*ca**2*sa**2
      #     = (ca**2-sa**2)**2
      # =>
      # dZ_\eta1 - dZ_\eta2 = (dZ11-dZ22)*(ca2-sa2) + (dZ12 + dZ21)*2*ca*sa

      sa = Symbol('sa')
      ca = Symbol('ca')
      ca2 = ca**2
      sa2 = sa**2

      p1 = PHl
      m1 = Symbol(MHL)
      p2 = PHh
      m2 = Symbol(MHH)

      h1_ct = get_particle_ct(PHh, only_diagonal=True, mass=False)
      assert(len(h1_ct) == 1)
      dZh1 = h1_ct[0]
      h2_ct = get_particle_ct(PHl, only_diagonal=True, mass=False)
      assert(len(h2_ct) == 1)
      dZh2 = h2_ct[0]

      h1_ct = get_particle_ct(PHh, only_offdiagonal=True, mass=False)
      assert(len(h1_ct) == 1)
      dZh1h2 = h1_ct[0]
      h2_ct = get_particle_ct(PHl, only_offdiagonal=True, mass=False)
      assert(len(h2_ct) == 1)
      dZh2h1 = h2_ct[0]

      assert(RenoConst.is_active(dZh2h1.name, 'onshell'))
      assert(RenoConst.is_active(dZh1h2.name, 'onshell'))
      assert(RenoConst.is_active(dtHl, 'onshell'))
      assert(RenoConst.is_active(dtHh, 'onshell'))

      dvs = '-' + dtHh + '*ca/c' + MHH + '**2+' + dtHl + '*sa/c' + MHL + '**2'
      dv = '-' + dtHh + '*sa/c' + MHH + '**2-' + dtHl + '*ca/c' + MHL + '**2'
      dvsubs = {Symbol('dvs'): sympify(dvs), Symbol('dv'): sympify(dv)}
      tadpoles = '(dv/vev-dvs/vevs)'
      tadpoles = sympify(tadpoles).subs(dvsubs)
      tb = Symbol('tb')
      fac_od = - ca * sa
      fac_d = -(sa2 - ca2) / 2

      # dZvevs - dZvev
      dZDV_val = fac_d * (dZh1 - dZh2) + fac_od * (dZh2h1 + dZh1h2) + tadpoles

      if not('BFM' in kwargs and kwargs['BFM']):
        P1M = p1.mass.name
        P2M = p2.mass.name
        arg11 = ','.join(['r' + P2M, MZ, MZ])
        arg12 = ','.join(['r' + P2M, MW, MW])
        ints1 = 'B0(' + arg11 + ')+2*cw**2*B0(' + arg12 + ')'

        arg21 = ','.join(['r' + P1M, MZ, MZ])
        arg22 = ','.join(['r' + P1M, MW, MW])
        ints2 = 'B0(' + arg21 + ')+2*cw**2*B0(' + arg22 + ')'

        FP = Regularize()
        ints1 = sympify(FP.assign_tensors(ints1, Regularize.tens_dict))
        ints2 = sympify(FP.assign_tensors(ints2, Regularize.tens_dict))

        OD12_BFM = Symbol('OD12_BFM')
        OD12_BFM_val = sympify('ee**2/(32*pi**2*cw**2*sw**2)*ca*sa')*(ints1 + ints2)
        cls.update_renos({OD12_BFM: OD12_BFM_val}, renoscheme='BFM_tadpoles',
                         simplify=False)
        dZDV_val += fac_od * OD12_BFM

        D12_BFM = Symbol('D12_BFM')
        D12_BFM_val = sympify('ee**2/(32*pi**2*cw**2*sw**2)')*(-ca2 * ints2 + sa2 * ints1)
        cls.update_renos({D12_BFM: D12_BFM_val}, renoscheme='BFM_tadpoles',
                         simplify=False)
        dZDV_val += fac_d * D12_BFM

      cls.update_renos({Symbol('dZDV'): dZDV_val}, renoscheme=method,
                       simplify=simplify)

      # construct counterterm dl3 = dvevs * X + ...
      vevs = Symbol('vevs')
      vev = Symbol('vev')
      ctbase = [u for u in Counterterms.get_all_ct()]
      dvevs = Poly(Counterterms.ctdict[vevs], ctbase)
      dvevs = dvevs.as_dict()
      dl3_pos = ctbase.index(dl3)
      dl3_key = tuple(0 if i != dl3_pos else 1 for i in xrange(len(ctbase)))
      zerokey = tuple([0] * len(ctbase))
      del dvevs[zerokey]
      dl3_coeff = dvevs[dl3_key]
      from rept1l.advanced_tools.hs_basis import expr_HS
      dvevs = {key: expr_HS(val/dl3_coeff) for key, val in iteritems(dvevs)}
      del dvevs[dl3_key]

      dvev = Poly(Counterterms.ctdict[vev], ctbase).as_dict()
      del dvev[zerokey]
      dvev = sum(ctbase[u.index(1)]*expr_HS(dvev[u]) for u in dvev)

      dvevs_val = vevs*(sympify('dZDV') + dvev/vev)
      dl3_val = sum([-ctbase[u.index(1)]*dvevs[u] for u in dvevs.keys()] +
                    [dvevs_val/expr_HS(dl3_coeff)])

      cls.update_renos({dl3: dl3_val}, renoscheme=method,
                       simplify=simplify)

    elif method == 'l1_MS':
      from rept1l.advanced_tools.hs_basis import expr_HS
      substitute_msbar_parameter(cls, 'l3', 'l1', simplify_func=expr_HS,
                                scaledep_ms='muMS_BSM', renoscheme_ms='MS_BSM',
                                renoscheme=method, simplify=simplify)
      # construct counterterm dl1 = dl3_coeff*dtb + ...
      # l1 = Symbol('l1')
      # ctbase = [u for u in Counterterms.get_all_ct()]
      # dl1 = Poly(Counterterms.ctdict[l1], ctbase)
      # dl1 = dl1.as_dict()
      # dl3_pos = ctbase.index(dl3)
      # dl3_key = tuple(0 if i != dl3_pos else 1 for i in xrange(len(ctbase)))
      # zerokey = tuple([0] * len(ctbase))
      # del dl1[zerokey]
      # dl1 = {key: sympy_simplify(val) for key, val in dl1.iteritems()}
      # dl3_coeff = dl1[dl3_key]
      # del dl1[dl3_key]
      # cts = [ctbase[u.index(1)] for u in dl1.keys()]
      #
      # # compute MSbar ct of all ct in dl1
      # reno_sols = RenoConst.build_ms_ct_solutions(cts,
      #                                             plugin_ms_values=False,
      #                                             scaledep='muMS_BSM',
      #                                             renoscheme='MS_BSM')
      # # construct substitution for finite parts:
      # # delta_x -> delta_x -delta_x^MSbar
      # reno_sols = {key: key - val for key, val in reno_sols.iteritems()}
      # dl1 = -sum(ctbase[key.index(1)].subs(reno_sols) * val
      #            for key, val in dl1.iteritems()) / dl3_coeff
      #
      # dl3fin = Symbol('dl3_l1_fin')
      # cls.update_renos({dl3fin: dl1}, renoscheme=method, simplify=simplify)
      #
      # dl3_ms_sol = RenoConst.build_ms_ct_solutions([dl3],
      #                                              plugin_ms_values=False,
      #                                              scaledep='muMS_BSM',
      #                                              renoscheme='MS_BSM')
      # cls.update_renos({dl3: dl3fin + (dl3.subs(dl3_ms_sol))}, renoscheme=method,
      #                  default=True, simplify=simplify)





  @classmethod
  def renormalize_tb(cls, method='FJTS', simplify=True, **kwargs):
    """ Tan(beta) is renormalized msbar for the vertex Hh Hl Hl. """

    cls.load_storage()
    dtb = Symbol('dtb')
    if method == 'MS':
      renormalize_msbar(cls, [PHh.name, PHl.name, PHl.name], dtb.name,
                        renoscheme=method, splitvar=True, simplify=simplify)

    elif method == 'FJTS':
      h1_ct = get_particle_ct(PHl, only_diagonal=True, mass=False)
      assert(len(h1_ct) == 1)
      dZh1 = h1_ct[0]
      h2_ct = get_particle_ct(PHh, only_diagonal=True, mass=False)
      assert(len(h2_ct) == 1)
      dZh2 = h2_ct[0]

      assert(RenoConst.is_active(dZh1.name, 'onshell'))
      assert(RenoConst.is_active(dZh2.name, 'onshell'))
      assert(RenoConst.is_active(dtHl, 'onshell'))
      assert(RenoConst.is_active(dtHh, 'onshell'))

      dv1 = '-' + dtHh + '*ca/c' + MHH + '**2+' + dtHl + '*sa/c' + MHL + '**2'
      dv2 = '-' + dtHh + '*sa/c' + MHH + '**2-' + dtHl + '*ca/c' + MHL + '**2'
      dvsubs = {Symbol('dv1'): sympify(dv1), Symbol('dv2'): sympify(dv2)}
      tadpoles = 'tb*(dv1-dv2/tb)'
      tadpoles = sympify(tadpoles).subs(dvsubs)

      fac_d = sympify('tb/(2*(sa**2-ca**2))')
      fac_t = 1 / Symbol('vev')

      msrenos = [dZh2, dZh1, Symbol(dtHl), Symbol(dtHh)]
      reno_sols = RenoConst.build_ms_ct_solutions(msrenos,
                                                  plugin_ms_values=False)

      dtb_val = fac_d * (dZh2 - dZh1).subs(reno_sols) + fac_t * tadpoles.subs(reno_sols)

      if not('BFM' in kwargs and kwargs['BFM']):
        # rigid invariance broken in the conventional formulation
        add = sympify('-ee**2/(64*pi**2*cw**2*sw**2)*ca*sa*(1+2*cw**2)*DeltaUV')
        fac3 = sympify('tb/(sa*ca)')
        dtb_val += fac3 * add

        # explicit scale dependence due to DeltaUV
        cls.update_renos({dtb: dtb_val}, renoscheme=method,
                         scaledep='muMS', simplify=simplify)
      else:
        cls.update_renos({dtb: dtb_val}, renoscheme=method,
                         simplify=simplify)

    # Tadpole subtracted MSbar scheme (= usual DRbar scheme in MSSM)
    elif method == 'PRTS':
      h1_ct = get_particle_ct(PHl, only_diagonal=True, mass=False)
      assert(len(h1_ct) == 1)
      dZh1 = h1_ct[0]
      h2_ct = get_particle_ct(PHh, only_diagonal=True, mass=False)
      assert(len(h2_ct) == 1)
      dZh2 = h2_ct[0]

      assert(RenoConst.is_active(dZh1.name, 'onshell'))
      assert(RenoConst.is_active(dZh2.name, 'onshell'))
      assert(RenoConst.is_active(dtHl, 'onshell'))
      assert(RenoConst.is_active(dtHh, 'onshell'))

      dv1 = '-' + dtHh + '*ca/c' + MHH + '**2+' + dtHl + '*sa/c' + MHL + '**2'
      dv2 = '-' + dtHh + '*sa/c' + MHH + '**2-' + dtHl + '*ca/c' + MHL + '**2'
      dvsubs = {Symbol('dv1'): sympify(dv1), Symbol('dv2'): sympify(dv2)}
      tadpoles = 'tb*(dv1-dv2/tb)'
      tadpoles = sympify(tadpoles).subs(dvsubs)
      fac1 = sympify('tb/(2*(sa**2-ca**2))')
      fac2 = sympify('1/vev')

      msrenos = [dZh2, dZh1]
      reno_sols = RenoConst.build_ms_ct_solutions(msrenos,
                                                  plugin_ms_values=False)

      dtb_val = (dZh2 - dZh1).subs(reno_sols) * fac1 + (tadpoles) * fac2

      if not('BFM' in kwargs and kwargs['BFM']):
        # rigid invariance broken in the conventional formulation
        add = sympify('-ee**2/(64*pi**2*cw**2*sw**2)*ca*sa*(1+2*cw**2)*DeltaUV')
        fac3 = sympify('tb/(sa*ca)')
        dtb_val += fac3 * add

        # explicit scale dependence due to DeltaUV
        cls.update_renos({dtb: dtb_val}, renoscheme=method, scaledep='muMS',
                         simplify=simplify)
      else:
        cls.update_renos({dtb: dtb_val}, renoscheme=method,
                          simplify=simplify)

    elif method == 'BFM_D':
      if not('BFM' in kwargs and kwargs['BFM']):
        raise Exception('BFM_D not derived yet for `t Hooft Feynman gauge.')
      h1_ct = get_particle_ct(PHl, only_diagonal=True, mass=False)
      assert(len(h1_ct) == 1)
      dZh1 = h1_ct[0]
      h2_ct = get_particle_ct(PHh, only_diagonal=True, mass=False)
      assert(len(h2_ct) == 1)
      dZh2 = h2_ct[0]

      assert(RenoConst.is_active(dZh1.name, 'onshell'))
      assert(RenoConst.is_active(dZh2.name, 'onshell'))
      assert(RenoConst.is_active(dtHl, 'onshell'))
      assert(RenoConst.is_active(dtHh, 'onshell'))

      dv1 = '-' + dtHh + '*ca/c' + MHH + '**2+' + dtHl + '*sa/c' + MHL + '**2'
      dv2 = '-' + dtHh + '*sa/c' + MHH + '**2-' + dtHl + '*ca/c' + MHL + '**2'
      dvsubs = {Symbol('dv1'): sympify(dv1), Symbol('dv2'): sympify(dv2)}
      tadpoles = 'tb*(dv1-dv2/tb)'
      tadpoles = sympify(tadpoles).subs(dvsubs)
      fac1 = sympify('tb/(2*(sa**2-ca**2))')
      fac2 = sympify('1/vev')

      dtb_val = (dZh2 - dZh1) * fac1 + (tadpoles) * fac2

      cls.update_renos({dtb: dtb_val}, renoscheme=method,
                       simplify=simplify)

    elif method == 'BFM_OD':
      p1 = PHl
      m1 = Symbol(MHL)
      p2 = PHh
      m2 = Symbol(MHH)

      h1_ct = get_particle_ct(PHl, only_offdiagonal=True, mass=False)
      assert(len(h1_ct) == 1)
      dZh1h2 = h1_ct[0]
      h2_ct = get_particle_ct(PHh, only_offdiagonal=True, mass=False)
      assert(len(h2_ct) == 1)
      dZh2h1 = h2_ct[0]

      assert(RenoConst.is_active(dZh2h1.name, 'onshell'))
      assert(RenoConst.is_active(dZh1h2.name, 'onshell'))
      assert(RenoConst.is_active(dtHl, 'onshell'))
      assert(RenoConst.is_active(dtHh, 'onshell'))

      dv1 = '-' + dtHh + '*ca/c' + MHH + '**2+' + dtHl + '*sa/c' + MHL + '**2'
      dv2 = '-' + dtHh + '*sa/c' + MHH + '**2-' + dtHl + '*ca/c' + MHL + '**2'
      dvsubs = {Symbol('dv1'): sympify(dv1), Symbol('dv2'): sympify(dv2)}
      tadpoles = 'tb*(dv1-dv2/tb)'
      tadpoles = sympify(tadpoles).subs(dvsubs)
      fac_od = sympify('tb/(4*ca*sa)')
      fac_t = 1 / Symbol('vev')

      dtb_val = fac_od * (dZh2h1 + dZh1h2) + fac_t * tadpoles


      if not('BFM' in kwargs and kwargs['BFM']):
        P1M = p1.mass.name
        P2M = p2.mass.name
        arg11 = ','.join(['r' + P2M, MZ, MZ])
        arg12 = ','.join(['r' + P2M, MW, MW])
        ints1 = 'B0(' + arg11 + ')+2*cw**2*B0(' + arg12 + ')'

        arg21 = ','.join(['r' + P1M, MZ, MZ])
        arg22 = ','.join(['r' + P1M, MW, MW])
        ints2 = 'B0(' + arg21 + ')+2*cw**2*B0(' + arg22 + ')'

        FP = Regularize()
        ints1 = sympify(FP.assign_tensors(ints1, Regularize.tens_dict))
        ints2 = sympify(FP.assign_tensors(ints2, Regularize.tens_dict))
        OD12_BFM = Symbol('OD12_BFM')
        OD12_BFM_val = sympify('-ee**2/(32*pi**2*cw**2*sw**2)*ca*sa')*(ints1 + ints2)
        cls.update_renos({OD12_BFM: OD12_BFM_val}, renoscheme='BFM_tadpoles',
                         simplify=False)

        dtb_val += fac_od * OD12_BFM

        # dZh2h1  = 2/(MH22-MH12) Sigma(MH12)
        # (dZh2h1 + dZh2h1)_BFM = (dZh2h1 + dZh2h1) +
        #                         2/(MH22-MH12) (\Delta Sigma(MH12) - \Delta Sigma(MH22))
        #
        # \Delta Sigma(MH12) = - ca sa alpha/ (16 pi cw^2 sw^2) (MH22 - MH12) * ints1
        # \Delta Sigma(MH22) = - ca sa alpha/ (16 pi cw^2 sw^2) (MH12 - MH22) * ints2
        # =>
        # \Delta Sigma(MH12) - \Delta Sigma(MH22) = - ca sa alpha/ (16 pi cw^2 sw^2) (MH22 - MH12) * (ints1 + ints2)

      cls.update_renos({dtb: dtb_val}, renoscheme=method,
                       simplify=simplify)

    elif method == 'BFM':
      # dZ_\eta1 - dZ_\eta2 = (dZ11-dZ22)/(ca2-sa2) 
      # dZ_\eta2 - dZ_\eta1 = (dZ12+dZ21)/(2*ca*sa) 
      # =>
      # dZ_\eta2 - dZ_\eta1 = (1-x) * (1) + x * (2)
      # x = (2*ca*sa)**2
      # 1-x = (ca**2+sa**2)**2-4*ca**2*sa**2
      #     = (ca**2-sa**2)**2
      # =>
      # dZ_\eta1 - dZ_\eta2 = (dZ11-dZ22)*(ca2-sa2) + (dZ12 + dZ21)*2*ca*sa

      sa = Symbol('sa')
      ca = Symbol('ca')
      tb = Symbol('tb')
      ca2 = ca**2
      sa2 = sa**2

      p1 = PHl
      m1 = Symbol(MHL)
      p2 = PHh
      m2 = Symbol(MHH)

      h1_ct = get_particle_ct(PHl, only_diagonal=True, mass=False)
      assert(len(h1_ct) == 1)
      dZh1 = h1_ct[0]
      h2_ct = get_particle_ct(PHh, only_diagonal=True, mass=False)
      assert(len(h2_ct) == 1)
      dZh2 = h2_ct[0]

      h1_ct = get_particle_ct(PHl, only_offdiagonal=True, mass=False)
      assert(len(h1_ct) == 1)
      dZh1h2 = h1_ct[0]
      h2_ct = get_particle_ct(PHh, only_offdiagonal=True, mass=False)
      assert(len(h2_ct) == 1)
      dZh2h1 = h2_ct[0]

      assert(RenoConst.is_active(dZh2h1.name, 'onshell'))
      assert(RenoConst.is_active(dZh1h2.name, 'onshell'))
      assert(RenoConst.is_active(dtHl, 'onshell'))
      assert(RenoConst.is_active(dtHh, 'onshell'))

      dv1 = '-' + dtHh + '*ca/c' + MHH + '**2+' + dtHl + '*sa/c' + MHL + '**2'
      dv2 = '-' + dtHh + '*sa/c' + MHH + '**2-' + dtHl + '*ca/c' + MHL + '**2'
      dvsubs = {Symbol('dv1'): sympify(dv1), Symbol('dv2'): sympify(dv2)}
      tadpoles = 'tb*(dv1-dv2/tb)'
      tadpoles = sympify(tadpoles).subs(dvsubs)
      fac_od = tb * ca * sa
      fac_d = tb * (ca2 - sa2) / 2
      fac_t = 1 / Symbol('vev')

      dtb_val = fac_d * (dZh1 - dZh2) + fac_od * (dZh2h1 + dZh1h2) + fac_t * tadpoles


      if not('BFM' in kwargs and kwargs['BFM']):
        P1M = p1.mass.name
        P2M = p2.mass.name
        arg11 = ','.join(['r' + P2M, MZ, MZ])
        arg12 = ','.join(['r' + P2M, MW, MW])
        ints1 = 'B0(' + arg11 + ')+2*cw**2*B0(' + arg12 + ')'

        arg21 = ','.join(['r' + P1M, MZ, MZ])
        arg22 = ','.join(['r' + P1M, MW, MW])
        ints2 = 'B0(' + arg21 + ')+2*cw**2*B0(' + arg22 + ')'

        FP = Regularize()
        ints1 = sympify(FP.assign_tensors(ints1, Regularize.tens_dict))
        ints2 = sympify(FP.assign_tensors(ints2, Regularize.tens_dict))

        OD12_BFM = Symbol('OD12_BFM')
        OD12_BFM_val = sympify('-ee**2/(32*pi**2*cw**2*sw**2)*ca*sa')*(ints1 + ints2)
        cls.update_renos({OD12_BFM: OD12_BFM_val}, renoscheme='BFM_tadpoles',
                         simplify=False)
        dtb_val += fac_od * OD12_BFM

        D12_BFM = Symbol('D12_BFM')
        D12_BFM_val = sympify('ee**2/(32*pi**2*cw**2*sw**2)')*(sa2 * ints2 - ca2 * ints1)
        cls.update_renos({D12_BFM: D12_BFM_val}, renoscheme='BFM_tadpoles',
                         simplify=False)
        dtb_val += fac_d * D12_BFM

      cls.update_renos({dtb: dtb_val}, renoscheme=method,
                       simplify=simplify)


    elif method == 'l2_MS':
      # construct counterterm dl2 = dtb_coeff*dtb + ...
      l2 = Symbol('l2')
      ctbase = [u for u in Counterterms.get_all_ct()]
      dl2 = Poly(Counterterms.ctdict[l2], ctbase)
      dl2 = dl2.as_dict()
      dtb_pos = ctbase.index(dtb)
      dtb_key = tuple(0 if i != dtb_pos else 1 for i in xrange(len(ctbase)))
      zerokey = tuple([0] * len(ctbase))
      del dl2[zerokey]
      dl2 = {key: sympy_simplify(val) for key, val in iteritems(dl2)}
      dtb_coeff = dl2[dtb_key]
      del dl2[dtb_key]
      cts = [ctbase[u.index(1)] for u in dl2.keys()]

      # compute MSbar ct of all ct in dl2
      reno_sols = RenoConst.build_ms_ct_solutions(cts,
                                                  plugin_ms_values=False)
      # construct substitution for finite parts:
      # delta_x -> delta_x -delta_x^MSbar
      reno_sols = {key: key - val for key, val in iteritems(reno_sols)}
      dl2 = -sum(ctbase[key.index(1)].subs(reno_sols) * val
                 for key, val in iteritems(dl2)) / dtb_coeff

      dtbfin = Symbol('dtb_l2_fin')
      cls.update_renos({dtbfin: dl2}, renoscheme=method, simplify=simplify)
      dtb_ms_sol = RenoConst.build_ms_ct_solutions([dtb], plugin_ms_values=False)
      cls.update_renos({dtb: dtbfin + (dtb.subs(dtb_ms_sol))}, renoscheme=method,
                       default=True, simplify=simplify)



  @classmethod
  def renormalize_alpha_alternatives(cls, **kwargs):
    # cls.renormalize_alpha(method='ps_bfm', **kwargs)
    cls.renormalize_alpha(method='BFM', **kwargs)
    # cls.renormalize_alpha(method='l3_MS', **kwargs)
    cls.renormalize_alpha(method='PRTS', **kwargs)
    # cls.renormalize_alpha(method='MTS', **kwargs)
    cls.renormalize_alpha(method='OS', **kwargs)

  @classmethod
  def renormalize_l3_alternatives(cls, **kwargs):
    cls.renormalize_l3(method='BFM', **kwargs)
    cls.renormalize_l3(method='l1_MS', **kwargs)

  @classmethod
  def renormalize_tb_alternatives(cls, **kwargs):
    cls.renormalize_tb(method='PRTS', **kwargs)
    # cls.renormalize_tb(method='BFM_D', **kwargs)
    cls.renormalize_tb(method='BFM_OD', **kwargs)
    cls.renormalize_tb(method='BFM', **kwargs)
    cls.renormalize_tb(method='l2_MS', **kwargs)

  @classmethod
  def BFM_ps_template(cls, ct, p1, m1, p2, m2, massscale, renoscheme,
                      simplify=False, default=False, **kwargs):
    # The p^* onshell scheme is given by the HlHh mixing energy evaluated at
    # at p=p^*=massscale.

    h_ct = get_particle_ct(p1, only_offdiagonal=True, mass=False)
    assert(len(h_ct) == 1)
    dZ12 = h_ct[0]
    g_ct = get_particle_ct(p2, only_offdiagonal=True, mass=False)
    assert(len(g_ct) == 1)
    dZ21 = g_ct[0]
    GH = RenormalizeSelfenergy(p1, particle_out=p2,
                               renoscheme='onshell',
                               solve_eq=False,
                               mass=massscale,
                               realmomentum=False,
                               residue=False,
                               **kwargs)
    assert(len(GH.renorm_conditions) == 1)
    sigma_GH = -I * GH.renorm_conditions[0].subs({dZ12: 0, dZ21: 0})
    ct_pinch_val = sigma_GH / (m2**2 - m1**2)
    cls.update_renos({ct: ct_pinch_val}, renoscheme=renoscheme,
                     simplify=simplify, default=default)

  @classmethod
  def BFM_template(cls, ct, p1, m1, p2, m2, fac, ints1, ints2, renoscheme,
                   simplify=False, BFM=False, compute_mixing_energy=False,
                   default=False, **kwargs):
    # The onshell BFM scheme is decomposed into an onshell part in Feynman gauge
    # and an additional part.

    # Assuming 't Hooft Feynman gauge
    if not BFM:
      FP = Regularize()
      ints1 = sympify(FP.assign_tensors(ints1, Regularize.tens_dict))
      ints2 = sympify(FP.assign_tensors(ints2, Regularize.tens_dict))

      fac1 = sympify(fac)
      fac2 = (m2**2 - m1**2) / 2

      sigma_add = fac1 * fac2 * (ints1 - ints2)
    else:
      sigma_add = 0

    if compute_mixing_energy:
      # Construction of divergent part da_div
      p1p2_ct = get_particle_ct(p1, only_offdiagonal=True, mass=False)
      assert(len(p1p2_ct) == 1)
      dZ12 = p1p2_ct[0]
      p2p1_ct = get_particle_ct(p2, only_offdiagonal=True, mass=False)
      assert(len(p2p1_ct) == 1)
      dZ21 = p2p1_ct[0]

      cts_zero = {dZ21: 0, dZ12: 0}
      m1os = 'M0' if m1 == 0 else m1.name
      p1p2 = RenormalizeSelfenergy(p1, particle_out=p2,
                                   renoscheme='onshell',
                                   solve_eq=False,
                                   realmomentum=False,
                                   mass=m1os,
                                   residue=False,
                                   **kwargs)
      assert(len(p1p2.renorm_conditions) == 1)
      sigma_p1p2 = -I * p1p2.renorm_conditions[0].subs(cts_zero)

      m2os = 'M0' if m2 == 0 else m2.name
      p2p1 = RenormalizeSelfenergy(p1, particle_out=p2,
                                   renoscheme='onshell',
                                   solve_eq=False,
                                   realmomentum=False,
                                   mass=m2os,
                                   residue=False,
                                   **kwargs)
      assert(len(p2p1.renorm_conditions) == 1)
      sigma_p2p1 = -I * p2p1.renorm_conditions[0].subs(cts_zero)
      sigma_tad = sigma_p1p2 + sigma_p2p1
      ct_val = (sigma_tad + sigma_add) / (2 * (m2**2 - m1**2))

    else:
      p1p2_ct = get_particle_ct(p1, only_offdiagonal=True,
                                mass=False)
      assert(len(p1p2_ct) == 1)
      dZp1p2 = p1p2_ct[0]
      p2p1_ct = get_particle_ct(p2, only_offdiagonal=True,
                                mass=False)
      assert(len(p2p1_ct) == 1)
      dZp2p1 = p2p1_ct[0]

      ct_val = (dZp2p1 - dZp1p2) / 4 + sigma_add / (2 * (m2**2 - m1**2))

    # 1605.04853 Eq.(3.66) Feynman gauge part
    cls.update_renos({ct: ct_val}, renoscheme=renoscheme,
                     simplify=simplify, default=default)


parser = Rept1lArgParser(__doc__)

selection = OD([('tadpoles', 'renormalize_tadpoles'),
                ('neutral', 'renormalize_neutral_higgs'),
                ('alpha', 'renormalize_alpha'),
                # ('yukaux', 'renormalize_yukaux'),
                ('alphaALT', 'renormalize_alpha_alternatives'),
                ('l3', 'renormalize_l3'),
                ('l3ALT', 'renormalize_l3_alternatives'),
                # ('tb', 'renormalize_tb'),
                # ('tbALT', 'renormalize_tb_alternatives'),
                ])

parser.add_argument("-o",
                    help='Sets specific renormalizations. ' +
                         'By default the complete renormalization is ' +
                         'performed. Accepted values are: ' +
                         ', '.join(selection.keys()),
                    nargs='+',
                    type=str)

parser.add_argument("-bfm",
                    help='Enables renormalization for BFM.',
                    action='store_true')

parser.add_argument("-simp",
                    help='Simplify counterterm expression after derivation.',
                    action='store_true')

# For every order in the model an optional argument is created allowing to
# select specific powers in that order.
for order in RCoupling.orders:
  parser.add_argument("-" + order.lower(),
                      help=('Select the power associated to the order ' +
                            order + '.'),
                      nargs='+',
                      type=int)

mt = '172.5d0'
wt = '0d0'
mb = '4.3d0'
mta = '1.77684d0'

mw = '80.385d0'
ww = '0d0'
mz = '91.1876d0'
wz = '0d0'
mh = '125.7d0'
wh = '0d0'
massreg = '1d-3'
m = 'massreg'

MHS = '300d0'
WHS = '0d0'
tb = '1d0'
sa = '-0.44721359549995804d0'  # alignment limit
aEW = '7.5623392289096781d-003'
aS = '0.1d0/2d0'
muUV = '10d0'
test_params = {'muUV': mw, 'muIR': mw, 'lambda': mw, 'MW': mw,
               'WW': ww, 'MZ': mz, 'WZ': wz, 'MH': mh, 'WH': wh,
               'MT': mt, 'WT': wt, 'MB': m, 'MC': m, 'MS': m, 'MU': m,
               'MD': m, 'ME': m, 'MM': m,
               'MTA': mta, 'massreg': massreg,
               'aEW': aEW, 'aS': aS, 'tb': tb, 'sa': sa, 'MHS': MHS,
               'WHS': WHS}

test_params = {'muUV': muUV, 'muMS': muUV, 'muIR': muUV,
               'MW': mw,
               'WW': ww, 'MZ': mz, 'WZ': wz, 'MHL': mh, 'WHL': wh,
               'MT': mt, 'WT': wt,
               'MB': m, 'WB': '0d0',
               'MC': m, 'WC': '0d0',
               'MS': m,
               'MU': m,
               'MD': m,
               'ME': m,
               'MM': m, 'WM': '0d0',
               'MTA': m, 'WTA': '0d0',
               'massreg': massreg,
               'aEW': aEW, 'aS': aS,
               'tb': tb, 'sa': sa,
               'MHH': MHS, 'WHH': WHS,
               }


if __name__ == '__main__':
  args = parser.parse_args()

  if args.simp:
    simplify = args.simp
  else:
    simplify = False

  if args.bfm:
    bfm = args.bfm
  else:
    bfm = False

  if args.o:
    new_selection = OD([(u, selection[u]) for u in selection if u in args.o])
  else:
    log('No renormalization selected. Nothing will be done.', 'info')
    new_selection = {}

  # If specific powers of orders are selected the argument select_power_NLO is
  # filled and passed to the renormalizaiton procedure
  order_selection = []
  for order in RCoupling.orders:
    if hasattr(args, order.lower()):
      s = getattr(args, order.lower())
      if s:
        for v in s:
          order_selection.append((order, v))
  if len(order_selection) > 0:
    kwargs = {'select_power_NLO': order_selection, 'select_power_LO': order_selection}
  else:
    kwargs = {}

  for sel, meth in iteritems(new_selection):
    getattr(RenormalizeHS, meth)(simplify=simplify, BFM=bfm, **kwargs)

  # from rept1l.renormalize import gen_ct_test_file
  # gen_ct_test_file('/home/nick/repos/rept1l/recola_models/HS_RT/test_CT.f90', test_params)
