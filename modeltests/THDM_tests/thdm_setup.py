import os
from subprocess import call, Popen, PIPE
driver_2hdm_ALL_path = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                                    '2HDM_ALL_drivers')
driver_2hdm_ALL_DS_path = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                                      '2HDM_ALL_DC_drivers')
driver_2hdm_ALL_BH_path = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                                      '2HDM_ALL_BH_drivers')
driver_2hdm_bfm_ALL_path = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                                       '2HDM_BFM_ALL_drivers')

params = {'muUV': '80.370881514409731d0',
          'muIR': '80.370881514409731d0',
          'lambda': '80.370881514409731d0',
          'MW': '80.370881514409731d0',
          'WW': '0d0',
          'MZ': '91.153480619182758d0',
          'WZ': '0d0',
          'mh1': '125d0',
          'Wh1': '0d0',
          'MT': '100d0',
          'WT': '0d0',
          'MB': 'massreg',
          'MC': 'massreg',
          'MS': 'massreg',
          'MU': 'massreg',
          'MD': 'massreg',
          'Me': 'massreg',
          'ME': 'massreg',
          'MM': 'massreg',
          'MTA': 'massreg',
          'ymtau': '0d0',
          'aEW': '7.5492131512809261d-003',
          'aS': '0.10530431206662641d0',
          'cb': '0.6d0',
          'sa': '-0.3d0',
          'm12': '1000d0',
          'ma': '800d0',
          'Wa': '0d0',
          'mhc': '100d0',
          'whc': '0d0',
          'mh2': '300d0',
          'Wh2': '0d0'}


mt = '173.21d0'
wt = '0d0'
mw = '80.385d0'
ww = '0d0'
mz = '91.1876d0'
wz = '0d0'
mh = '125.7d0'
wh = '0d0'
massreg = '1d-3'
m = 'massreg'

mh2 = '300d0'
Wh2 = '0d0'
ma = '130d0'
Wa = '0d0'
mhc = '140d0'
whc = '0d0'
cb = '0.44721359549995804d0'  # tanb = 2
sa = '-0.44721359549995804d0'  # alignment limit
msb2 = '0d0'
msb = '0d0'
aEW = '7.5623392289096781d-003'
aS = '0.1d0'
#laura_comp_params = {'muUV': mw, 'muIR': mw, 'lambda': mw, 'MW': mw,
                     #'WW': ww, 'MZ': mz, 'WZ': wz, 'mh1': mh, 'Wh1': wh,
                     #'MT': mt, 'WT': wt, 'MB': m, 'MC': m, 'MS': m, 'MU': m,
                     #'MD': m, 'ME': m, 'MM': m,
                     #'MTA': m, 'massreg': massreg,
                     #'aEW': aEW, 'aS': aS, 'cb': cb, 'sa': sa, 'msb2': msb2,
                     #'ma': ma, 'Wa': Wa, 'mhc': mhc, 'whc': whc, 'mh2': mh2,
                     #'Wh2': Wh2}
laura_comp_params = {'muUV': mw, 'muIR': mw, 'lambda': mw,
                     'MW': mw, 'WW': ww, 'MZ': mz, 'WZ': wz,
                     'MHL': mh, 'WHL': wh,
                     'MT': mt, 'WT': wt,
                     'MB': m, 'WB': '0d0',
                     'MC': m, 'WC': '0d0',
                     'MS': m, 'MU': m, 'MD': m,
                     'ME': m, 'MM': m, 'WM': '0d0',
                     'MTA': m, 'WTA': '0d0',
                     'massreg': massreg,
                     'aEW': aEW, 'aS': aS,
                     'cb': cb, 'sa': sa, 'MSB': msb,
                     'MHA': ma, 'WHA': Wa,
                     'MHC': mhc, 'WHC': whc,
                     'MHH': mh2, 'WHH': Wh2}

mt = 173.21
wt = 0.
mw = 80.385
ww = 0.
mz = 91.1876
wz = 0.
mh = 125.7
wh = 0.
massreg = 1e-3
m = massreg

mh2 = 300
Wh2 = 0.
ma = 400
Wa = 0.
mhc = 500
whc = 0.
cb = 0.44721359549995804  # tanb = 2
sa = -0.44721359549995804  # alignment limit
msb2 = 0.
msb = 0.
aEW = 7.5623392289096781E-003
aS = 0.1
#laura_comp_params2 = {'muUV': mw, 'muIR': mw, 'muMS': mw, 'MW': mw,
                      #'WW': ww, 'MZ': mz, 'WZ': wz, 'mh1': mh, 'Wh1': wh,
                      #'MT': mt, 'WT': wt, 'MB': m, 'MC': m, 'MS': m, 'MU': m,
                      #'MD': m, 'ME': m, 'MM': m,
                      #'MTA': m,
                      #'aEW': aEW, 'aS': aS, 'cb': cb, 'sa': sa, 'msb2': msb2,
                      #'ma': ma, 'Wa': Wa, 'mhc': mhc, 'whc': whc, 'mh2': mh2,
                      #'Wh2': Wh2}
laura_comp_params2 = {'muUV': mw, 'muIR': mw,
                     'MW': mw, 'WW': ww, 'MZ': mz, 'WZ': wz,
                     'MHL': mh, 'WHL': wh,
                     'MT': mt, 'WT': wt,
                     'MB': m, 'WB': 0.,
                     'MC': m, 'WC': 0.,
                     'MS': m, 'MU': m, 'MD': m,
                     'ME': m, 'MM': m, 'WM': 0.,
                     'MTA': m, 'WTA': 0.,
                     'aEW': aEW, 'aS': aS,
                     'cb': cb, 'sa': sa, 'MSB': msb,
                     'MHA': ma, 'WHA': Wa,
                     'MHC': mhc, 'WHC': whc,
                     'MHH': mh2, 'WHH': Wh2}


params_H4F = {'MW': 80.39800,
              'WW': 2.0907035513551233,
              'MZ': 91.18760,
              'WZ': 2.4982398858455146,
              'MH': 125.,
              'WH': 0.0,
              'MT': 172.5,
              'WT': 0.0,
              'MB': 1e-3,
              'MC': 1e-3,
              'MS': 1e-3,
              'MU': 1e-3,
              'MD': 1e-3,
              'MM': 1e-3,
              'MTA': 1e-3,
              'aEW': 7.5562543385512175e-003,
              'aS': 0.10530431206662641,
              'muUV': 80.39800,
              'muIR': 80.39800,
              'muMS': 80.39800}

def compute_ct(modelpath, parametervalues, *renofuncs):
  # perform the renormalization
  for func in renofuncs:
    func()
  os.chdir(modelpath)
  path = os.path.join(modelpath, 'test_CTcouplings.f90')
  from rept1l.renormalize import gen_ct_test_file
  gen_ct_test_file(path, parametervalues=parametervalues)
  cmd = "echo 'add_executable(run test_CTcouplings.f90)' >> CMakeLists.txt"
  call(cmd, shell=True)
  cmd = "echo 'target_link_libraries(run modelfile)' >> CMakeLists.txt"
  call(cmd, shell=True)
