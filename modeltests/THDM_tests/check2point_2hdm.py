#==============================================================================#
#                             check2point_2hdm.py                              #
#==============================================================================#

from __future__ import print_function

#===========#
#  Imports  #
#===========#

import sys
import rept1l.modeltests.modeltest_utils.setup_models as sm
import rept1l.modeltests.THDM_tests.thdm_setup as thdm
from rept1l.modeltests.modeltest_utils.model_tests import test_offshell_selfenergy
from rept1l.modeltests.modeltest_utils.model_tests import fail, success, warning

#===========#
#  Methods  #
#===========#

def check_2point():

  import pyrecola
  params = thdm.laura_comp_params2.copy()
  scales = {'muUV': pyrecola.set_mu_uv_rcl,
            'muIR': pyrecola.set_mu_ir_rcl,
            'muMS': pyrecola.set_mu_ms_rcl}
  for scale, clb in scales.iteritems():
    if scale in params:
      clb(params[scale])
      del params[scale]

  for p, val in params.iteritems():
    print("p, val:", p, val)
    pyrecola.set_parameter_rcl(p, complex(val, 0.))

  pyrecola.set_print_level_amplitude_rcl(1)
  pyrecola.set_compute_selfenergy_offshell_rcl(True)
  pyrecola.define_process_rcl(1, 'Hl -> Hl', 'NLO')
  pyrecola.define_process_rcl(2, 'Hh -> Hh', 'NLO')
  pyrecola.define_process_rcl(3, 'Hl -> Hh', 'NLO')
  pyrecola.define_process_rcl(4, 'H+ -> H+', 'NLO')
  pyrecola.define_process_rcl(5, 'Ha -> Ha', 'NLO')
  pyrecola.generate_processes_rcl()

  total_tests = 0
  tests_succeeded = 0

  # tests in the alignment limit
  expec = {thdm.laura_comp_params2['MHL']*2.: (338.24, 215.135),
           thdm.laura_comp_params2['MHL']*0.5: (0., 0.44702)}
  res = test_offshell_selfenergy(1, expec, err_digits=3, warning_digits=4)
  total_tests += len(res)
  tests_succeeded += res.values().count(True)

  expec = {thdm.laura_comp_params2['MHH']*2.: (-12896, 7204),
           thdm.laura_comp_params2['MHH']*0.5: (0., 603.30)}
  test_offshell_selfenergy(2, expec, err_digits=3, warning_digits=4)
  total_tests += len(res)
  tests_succeeded += res.values().count(True)

  expec = {thdm.laura_comp_params2['MHL']*0.5: (0., -31.1461504),
           thdm.laura_comp_params2['MHL']*2.: (0., 50.89272614)}
  test_offshell_selfenergy(3, expec, err_digits=3, warning_digits=4)
  total_tests += len(res)
  tests_succeeded += res.values().count(True)

  expec = {thdm.laura_comp_params2['MHC']*0.5: (1371.8, -986.9)}
  test_offshell_selfenergy(4, expec, err_digits=3, warning_digits=4)
  total_tests += len(res)
  tests_succeeded += res.values().count(True)

  expec = {thdm.laura_comp_params2['MHA']*0.5: (-1266.9, -301.7),
           thdm.laura_comp_params2['MHA']*2.: (-23109., -24438.)}
  test_offshell_selfenergy(5, expec, err_digits=3, warning_digits=4)
  total_tests += len(res)
  tests_succeeded += res.values().count(True)

  # tests in non-alignment
  # resetting the couplings is not sufficient as some couplings are zero in the
  # alignment limit and the corresponding currents were not taken along
  pyrecola.reset_recola_rcl()
  for p, val in params.iteritems():
    pyrecola.set_parameter_rcl(p, complex(val, 0.))
  pyrecola.set_parameter_rcl('sa', complex(0., 0.))
  pyrecola.reset_couplings_rcl()
  pyrecola.set_compute_selfenergy_offshell_rcl(True)
  pyrecola.define_process_rcl(1, 'Hl -> Hl', 'NLO')
  pyrecola.define_process_rcl(2, 'Hh -> Hh', 'NLO')
  pyrecola.define_process_rcl(3, 'Hl -> Hh', 'NLO')
  pyrecola.define_process_rcl(4, 'H+ -> H+', 'NLO')
  pyrecola.define_process_rcl(5, 'Ha -> Ha', 'NLO')
  pyrecola.generate_processes_rcl()

  expec = {thdm.laura_comp_params2['MHL']*2.: (270.6, 318.91),
           thdm.laura_comp_params2['MHL']*0.5: (0., 5.4334)}
  test_offshell_selfenergy(1, expec, err_digits=3, warning_digits=4)
  total_tests += len(res)
  tests_succeeded += res.values().count(True)

  expec = {thdm.laura_comp_params2['MHH']*2.: (-847.2, 25967),
           thdm.laura_comp_params2['MHH']*0.5: (571.0, 320.61)}
  test_offshell_selfenergy(2, expec, err_digits=3, warning_digits=4)
  total_tests += len(res)
  tests_succeeded += res.values().count(True)

  expec = {thdm.laura_comp_params2['MHL']*0.5: (13.308, -42.671),
           thdm.laura_comp_params2['MHL']*2.: (-89.207, -13.532)}
  test_offshell_selfenergy(3, expec, err_digits=3, warning_digits=4)
  total_tests += len(res)
  tests_succeeded += res.values().count(True)

  expec = {thdm.laura_comp_params2['MHC']*0.5: (1953.1, 498.7),
           thdm.laura_comp_params2['MHC']*2.: (-58938., -48050.)}
  test_offshell_selfenergy(4, expec, err_digits=3, warning_digits=4)
  total_tests += len(res)
  tests_succeeded += res.values().count(True)

  expec = {thdm.laura_comp_params2['MHA']*0.5: (-281.76, -335.75),
           thdm.laura_comp_params2['MHA']*2.: (-28204., -14900.)}
  test_offshell_selfenergy(5, expec, err_digits=3, warning_digits=4)
  total_tests += len(res)
  tests_succeeded += res.values().count(True)

  if total_tests == tests_succeeded:
    print('All tests(' + str(total_tests) + ') succeeded ' + success())
  elif tests_succeeded == 0:
    print('All tests(' + str(total_tests) + ') failed ' + fail())
  else:
    print('Some tests(' + str(total_tests) + ') failed ' + warning())

if __name__ == '__main__':
  check_2point()
