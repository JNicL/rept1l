# This file was automatically created by FeynRules 2.3.23
# Mathematica version: 9.0 for Linux x86 (64-bit) (November 20, 2012)
# Date: Sat 12 Aug 2017 17:51:32


from .object_library import all_couplings, Coupling

from .function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



GC_1 = Coupling(name = 'GC_1',
                value = '(ee*complex(0,1))/3.',
                order = {'QED':1})

GC_2 = Coupling(name = 'GC_2',
                value = '(-2*ee*complex(0,1))/3.',
                order = {'QED':1})

GC_3 = Coupling(name = 'GC_3',
                value = '-(ee*complex(0,1))',
                order = {'QED':1})

GC_4 = Coupling(name = 'GC_4',
                value = 'ee*complex(0,1)',
                order = {'QED':1})

GC_5 = Coupling(name = 'GC_5',
                value = 'ee**2*complex(0,1)',
                order = {'QED':2})

GC_6 = Coupling(name = 'GC_6',
                value = '-gs',
                order = {'QCD':1})

GC_7 = Coupling(name = 'GC_7',
                value = 'complex(0,1)*gs',
                order = {'QCD':1})

GC_8 = Coupling(name = 'GC_8',
                value = 'complex(0,1)*gs**2',
                order = {'QCD':2})

GC_9 = Coupling(name = 'GC_9',
                value = '-3*ca**3*complex(0,1)*l2*sa + 3*ca**3*complex(0,1)*l3*sa + 3*ca**3*complex(0,1)*l4*sa + 3*ca**3*complex(0,1)*l5*sa + 3*ca*complex(0,1)*l1*sa**3 - 3*ca*complex(0,1)*l3*sa**3 - 3*ca*complex(0,1)*l4*sa**3 - 3*ca*complex(0,1)*l5*sa**3',
                order = {'QED':2})

GC_10 = Coupling(name = 'GC_10',
                 value = '3*ca**3*complex(0,1)*l1*sa - 3*ca**3*complex(0,1)*l3*sa - 3*ca**3*complex(0,1)*l4*sa - 3*ca**3*complex(0,1)*l5*sa - 3*ca*complex(0,1)*l2*sa**3 + 3*ca*complex(0,1)*l3*sa**3 + 3*ca*complex(0,1)*l4*sa**3 + 3*ca*complex(0,1)*l5*sa**3',
                 order = {'QED':2})

GC_11 = Coupling(name = 'GC_11',
                 value = '-3*ca**4*complex(0,1)*l2 - 6*ca**2*complex(0,1)*l3*sa**2 - 6*ca**2*complex(0,1)*l4*sa**2 - 6*ca**2*complex(0,1)*l5*sa**2 - 3*complex(0,1)*l1*sa**4',
                 order = {'QED':2})

GC_12 = Coupling(name = 'GC_12',
                 value = '-3*ca**4*complex(0,1)*l1 - 6*ca**2*complex(0,1)*l3*sa**2 - 6*ca**2*complex(0,1)*l4*sa**2 - 6*ca**2*complex(0,1)*l5*sa**2 - 3*complex(0,1)*l2*sa**4',
                 order = {'QED':2})

GC_13 = Coupling(name = 'GC_13',
                 value = '-(ca**4*complex(0,1)*l3) - ca**4*complex(0,1)*l4 - ca**4*complex(0,1)*l5 - 3*ca**2*complex(0,1)*l1*sa**2 - 3*ca**2*complex(0,1)*l2*sa**2 + 4*ca**2*complex(0,1)*l3*sa**2 + 4*ca**2*complex(0,1)*l4*sa**2 + 4*ca**2*complex(0,1)*l5*sa**2 - complex(0,1)*l3*sa**4 - complex(0,1)*l4*sa**4 - complex(0,1)*l5*sa**4',
                 order = {'QED':2})

GC_14 = Coupling(name = 'GC_14',
                 value = '(cb*ee**2*complex(0,1)*sa)/(2.*cw) - (ca*ee**2*complex(0,1)*sb)/(2.*cw)',
                 order = {'QED':2})

GC_15 = Coupling(name = 'GC_15',
                 value = '-(cb*ee**2*complex(0,1)*sa)/(2.*cw) + (ca*ee**2*complex(0,1)*sb)/(2.*cw)',
                 order = {'QED':2})

GC_16 = Coupling(name = 'GC_16',
                 value = '-(ca*cb*ee**2*complex(0,1))/(2.*cw) - (ee**2*complex(0,1)*sa*sb)/(2.*cw)',
                 order = {'QED':2})

GC_17 = Coupling(name = 'GC_17',
                 value = 'cb**2*ee*complex(0,1) + ee*complex(0,1)*sb**2',
                 order = {'QED':1})

GC_18 = Coupling(name = 'GC_18',
                 value = '2*cb**2*ee**2*complex(0,1) + 2*ee**2*complex(0,1)*sb**2',
                 order = {'QED':2})

GC_19 = Coupling(name = 'GC_19',
                 value = '-(cb**2*ee**2)/(2.*cw) - (ee**2*sb**2)/(2.*cw)',
                 order = {'QED':2})

GC_20 = Coupling(name = 'GC_20',
                 value = '(cb**2*ee**2)/(2.*cw) + (ee**2*sb**2)/(2.*cw)',
                 order = {'QED':2})

GC_21 = Coupling(name = 'GC_21',
                 value = 'cb**2*ee*complex(0,1)*MW + ee*complex(0,1)*MW*sb**2',
                 order = {'QED':1})

GC_22 = Coupling(name = 'GC_22',
                 value = '-(ca*cb**2*complex(0,1)*l2*sa) + ca*cb**2*complex(0,1)*l3*sa + ca**2*cb*complex(0,1)*l4*sb + ca**2*cb*complex(0,1)*l5*sb - cb*complex(0,1)*l4*sa**2*sb - cb*complex(0,1)*l5*sa**2*sb + ca*complex(0,1)*l1*sa*sb**2 - ca*complex(0,1)*l3*sa*sb**2',
                 order = {'QED':2})

GC_23 = Coupling(name = 'GC_23',
                 value = 'ca*cb**2*complex(0,1)*l1*sa - ca*cb**2*complex(0,1)*l3*sa - ca**2*cb*complex(0,1)*l4*sb - ca**2*cb*complex(0,1)*l5*sb + cb*complex(0,1)*l4*sa**2*sb + cb*complex(0,1)*l5*sa**2*sb - ca*complex(0,1)*l2*sa*sb**2 + ca*complex(0,1)*l3*sa*sb**2',
                 order = {'QED':2})

GC_24 = Coupling(name = 'GC_24',
                 value = 'ca*cb**2*complex(0,1)*l4*sa + ca*cb**2*complex(0,1)*l5*sa - ca**2*cb*complex(0,1)*l2*sb + ca**2*cb*complex(0,1)*l3*sb + cb*complex(0,1)*l1*sa**2*sb - cb*complex(0,1)*l3*sa**2*sb - ca*complex(0,1)*l4*sa*sb**2 - ca*complex(0,1)*l5*sa*sb**2',
                 order = {'QED':2})

GC_25 = Coupling(name = 'GC_25',
                 value = 'ca*cb**2*complex(0,1)*l1*sa - ca*cb**2*complex(0,1)*l3*sa - ca*cb**2*complex(0,1)*l4*sa + ca*cb**2*complex(0,1)*l5*sa - 2*ca**2*cb*complex(0,1)*l5*sb + 2*cb*complex(0,1)*l5*sa**2*sb - ca*complex(0,1)*l2*sa*sb**2 + ca*complex(0,1)*l3*sa*sb**2 + ca*complex(0,1)*l4*sa*sb**2 - ca*complex(0,1)*l5*sa*sb**2',
                 order = {'QED':2})

GC_26 = Coupling(name = 'GC_26',
                 value = '-(ca*cb**2*complex(0,1)*l2*sa) + ca*cb**2*complex(0,1)*l3*sa + ca*cb**2*complex(0,1)*l4*sa - ca*cb**2*complex(0,1)*l5*sa + 2*ca**2*cb*complex(0,1)*l5*sb - 2*cb*complex(0,1)*l5*sa**2*sb + ca*complex(0,1)*l1*sa*sb**2 - ca*complex(0,1)*l3*sa*sb**2 - ca*complex(0,1)*l4*sa*sb**2 + ca*complex(0,1)*l5*sa*sb**2',
                 order = {'QED':2})

GC_27 = Coupling(name = 'GC_27',
                 value = '-(ca*cb**2*complex(0,1)*l4*sa) - ca*cb**2*complex(0,1)*l5*sa + ca**2*cb*complex(0,1)*l1*sb - ca**2*cb*complex(0,1)*l3*sb - cb*complex(0,1)*l2*sa**2*sb + cb*complex(0,1)*l3*sa**2*sb + ca*complex(0,1)*l4*sa*sb**2 + ca*complex(0,1)*l5*sa*sb**2',
                 order = {'QED':2})

GC_28 = Coupling(name = 'GC_28',
                 value = '2*ca*cb**2*complex(0,1)*l5*sa - ca**2*cb*complex(0,1)*l2*sb + ca**2*cb*complex(0,1)*l3*sb + ca**2*cb*complex(0,1)*l4*sb - ca**2*cb*complex(0,1)*l5*sb + cb*complex(0,1)*l1*sa**2*sb - cb*complex(0,1)*l3*sa**2*sb - cb*complex(0,1)*l4*sa**2*sb + cb*complex(0,1)*l5*sa**2*sb - 2*ca*complex(0,1)*l5*sa*sb**2',
                 order = {'QED':2})

GC_29 = Coupling(name = 'GC_29',
                 value = '-2*ca*cb**2*complex(0,1)*l5*sa + ca**2*cb*complex(0,1)*l1*sb - ca**2*cb*complex(0,1)*l3*sb - ca**2*cb*complex(0,1)*l4*sb + ca**2*cb*complex(0,1)*l5*sb - cb*complex(0,1)*l2*sa**2*sb + cb*complex(0,1)*l3*sa**2*sb + cb*complex(0,1)*l4*sa**2*sb - cb*complex(0,1)*l5*sa**2*sb + 2*ca*complex(0,1)*l5*sa*sb**2',
                 order = {'QED':2})

GC_30 = Coupling(name = 'GC_30',
                 value = '-(ca**2*cb**2*complex(0,1)*l2) - cb**2*complex(0,1)*l3*sa**2 - 2*ca*cb*complex(0,1)*l4*sa*sb - 2*ca*cb*complex(0,1)*l5*sa*sb - ca**2*complex(0,1)*l3*sb**2 - complex(0,1)*l1*sa**2*sb**2',
                 order = {'QED':2})

GC_31 = Coupling(name = 'GC_31',
                 value = '-(ca**2*cb**2*complex(0,1)*l2) - cb**2*complex(0,1)*l3*sa**2 - cb**2*complex(0,1)*l4*sa**2 + cb**2*complex(0,1)*l5*sa**2 - 4*ca*cb*complex(0,1)*l5*sa*sb - ca**2*complex(0,1)*l3*sb**2 - ca**2*complex(0,1)*l4*sb**2 + ca**2*complex(0,1)*l5*sb**2 - complex(0,1)*l1*sa**2*sb**2',
                 order = {'QED':2})

GC_32 = Coupling(name = 'GC_32',
                 value = '-(ca**2*cb**2*complex(0,1)*l1) - cb**2*complex(0,1)*l3*sa**2 - 2*ca*cb*complex(0,1)*l4*sa*sb - 2*ca*cb*complex(0,1)*l5*sa*sb - ca**2*complex(0,1)*l3*sb**2 - complex(0,1)*l2*sa**2*sb**2',
                 order = {'QED':2})

GC_33 = Coupling(name = 'GC_33',
                 value = '-(ca**2*cb**2*complex(0,1)*l1) - cb**2*complex(0,1)*l3*sa**2 - cb**2*complex(0,1)*l4*sa**2 + cb**2*complex(0,1)*l5*sa**2 - 4*ca*cb*complex(0,1)*l5*sa*sb - ca**2*complex(0,1)*l3*sb**2 - ca**2*complex(0,1)*l4*sb**2 + ca**2*complex(0,1)*l5*sb**2 - complex(0,1)*l2*sa**2*sb**2',
                 order = {'QED':2})

GC_34 = Coupling(name = 'GC_34',
                 value = '-(ca**2*cb**2*complex(0,1)*l3) - cb**2*complex(0,1)*l2*sa**2 + 2*ca*cb*complex(0,1)*l4*sa*sb + 2*ca*cb*complex(0,1)*l5*sa*sb - ca**2*complex(0,1)*l1*sb**2 - complex(0,1)*l3*sa**2*sb**2',
                 order = {'QED':2})

GC_35 = Coupling(name = 'GC_35',
                 value = '-(ca**2*cb**2*complex(0,1)*l3) - cb**2*complex(0,1)*l1*sa**2 + 2*ca*cb*complex(0,1)*l4*sa*sb + 2*ca*cb*complex(0,1)*l5*sa*sb - ca**2*complex(0,1)*l2*sb**2 - complex(0,1)*l3*sa**2*sb**2',
                 order = {'QED':2})

GC_36 = Coupling(name = 'GC_36',
                 value = '-(ca**2*cb**2*complex(0,1)*l4)/2. - (ca**2*cb**2*complex(0,1)*l5)/2. + (cb**2*complex(0,1)*l4*sa**2)/2. + (cb**2*complex(0,1)*l5*sa**2)/2. - ca*cb*complex(0,1)*l1*sa*sb - ca*cb*complex(0,1)*l2*sa*sb + 2*ca*cb*complex(0,1)*l3*sa*sb + (ca**2*complex(0,1)*l4*sb**2)/2. + (ca**2*complex(0,1)*l5*sb**2)/2. - (complex(0,1)*l4*sa**2*sb**2)/2. - (complex(0,1)*l5*sa**2*sb**2)/2.',
                 order = {'QED':2})

GC_37 = Coupling(name = 'GC_37',
                 value = '-(ca**2*cb**2*complex(0,1)*l5) + cb**2*complex(0,1)*l5*sa**2 - ca*cb*complex(0,1)*l1*sa*sb - ca*cb*complex(0,1)*l2*sa*sb + 2*ca*cb*complex(0,1)*l3*sa*sb + 2*ca*cb*complex(0,1)*l4*sa*sb - 2*ca*cb*complex(0,1)*l5*sa*sb + ca**2*complex(0,1)*l5*sb**2 - complex(0,1)*l5*sa**2*sb**2',
                 order = {'QED':2})

GC_38 = Coupling(name = 'GC_38',
                 value = '-(ca**2*cb**2*complex(0,1)*l3) - ca**2*cb**2*complex(0,1)*l4 + ca**2*cb**2*complex(0,1)*l5 - cb**2*complex(0,1)*l2*sa**2 + 4*ca*cb*complex(0,1)*l5*sa*sb - ca**2*complex(0,1)*l1*sb**2 - complex(0,1)*l3*sa**2*sb**2 - complex(0,1)*l4*sa**2*sb**2 + complex(0,1)*l5*sa**2*sb**2',
                 order = {'QED':2})

GC_39 = Coupling(name = 'GC_39',
                 value = '-(ca**2*cb**2*complex(0,1)*l3) - ca**2*cb**2*complex(0,1)*l4 + ca**2*cb**2*complex(0,1)*l5 - cb**2*complex(0,1)*l1*sa**2 + 4*ca*cb*complex(0,1)*l5*sa*sb - ca**2*complex(0,1)*l2*sb**2 - complex(0,1)*l3*sa**2*sb**2 - complex(0,1)*l4*sa**2*sb**2 + complex(0,1)*l5*sa**2*sb**2',
                 order = {'QED':2})

GC_40 = Coupling(name = 'GC_40',
                 value = '-(cb**3*l4*sa)/2. + (cb**3*l5*sa)/2. + (ca*cb**2*l4*sb)/2. - (ca*cb**2*l5*sb)/2. - (cb*l4*sa*sb**2)/2. + (cb*l5*sa*sb**2)/2. + (ca*l4*sb**3)/2. - (ca*l5*sb**3)/2.',
                 order = {'QED':2})

GC_41 = Coupling(name = 'GC_41',
                 value = '(cb**3*l4*sa)/2. - (cb**3*l5*sa)/2. - (ca*cb**2*l4*sb)/2. + (ca*cb**2*l5*sb)/2. + (cb*l4*sa*sb**2)/2. - (cb*l5*sa*sb**2)/2. - (ca*l4*sb**3)/2. + (ca*l5*sb**3)/2.',
                 order = {'QED':2})

GC_42 = Coupling(name = 'GC_42',
                 value = '-(cb**3*complex(0,1)*l2*sb) + cb**3*complex(0,1)*l3*sb + cb**3*complex(0,1)*l4*sb + cb**3*complex(0,1)*l5*sb + cb*complex(0,1)*l1*sb**3 - cb*complex(0,1)*l3*sb**3 - cb*complex(0,1)*l4*sb**3 - cb*complex(0,1)*l5*sb**3',
                 order = {'QED':2})

GC_43 = Coupling(name = 'GC_43',
                 value = 'cb**3*complex(0,1)*l1*sb - cb**3*complex(0,1)*l3*sb - cb**3*complex(0,1)*l4*sb - cb**3*complex(0,1)*l5*sb - cb*complex(0,1)*l2*sb**3 + cb*complex(0,1)*l3*sb**3 + cb*complex(0,1)*l4*sb**3 + cb*complex(0,1)*l5*sb**3',
                 order = {'QED':2})

GC_44 = Coupling(name = 'GC_44',
                 value = '-2*cb**3*complex(0,1)*l2*sb + 2*cb**3*complex(0,1)*l3*sb + 2*cb**3*complex(0,1)*l4*sb + 2*cb**3*complex(0,1)*l5*sb + 2*cb*complex(0,1)*l1*sb**3 - 2*cb*complex(0,1)*l3*sb**3 - 2*cb*complex(0,1)*l4*sb**3 - 2*cb*complex(0,1)*l5*sb**3',
                 order = {'QED':2})

GC_45 = Coupling(name = 'GC_45',
                 value = '2*cb**3*complex(0,1)*l1*sb - 2*cb**3*complex(0,1)*l3*sb - 2*cb**3*complex(0,1)*l4*sb - 2*cb**3*complex(0,1)*l5*sb - 2*cb*complex(0,1)*l2*sb**3 + 2*cb*complex(0,1)*l3*sb**3 + 2*cb*complex(0,1)*l4*sb**3 + 2*cb*complex(0,1)*l5*sb**3',
                 order = {'QED':2})

GC_46 = Coupling(name = 'GC_46',
                 value = '-3*cb**3*complex(0,1)*l2*sb + 3*cb**3*complex(0,1)*l3*sb + 3*cb**3*complex(0,1)*l4*sb + 3*cb**3*complex(0,1)*l5*sb + 3*cb*complex(0,1)*l1*sb**3 - 3*cb*complex(0,1)*l3*sb**3 - 3*cb*complex(0,1)*l4*sb**3 - 3*cb*complex(0,1)*l5*sb**3',
                 order = {'QED':2})

GC_47 = Coupling(name = 'GC_47',
                 value = '3*cb**3*complex(0,1)*l1*sb - 3*cb**3*complex(0,1)*l3*sb - 3*cb**3*complex(0,1)*l4*sb - 3*cb**3*complex(0,1)*l5*sb - 3*cb*complex(0,1)*l2*sb**3 + 3*cb*complex(0,1)*l3*sb**3 + 3*cb*complex(0,1)*l4*sb**3 + 3*cb*complex(0,1)*l5*sb**3',
                 order = {'QED':2})

GC_48 = Coupling(name = 'GC_48',
                 value = '(ca*cb**3*l4)/2. - (ca*cb**3*l5)/2. + (cb**2*l4*sa*sb)/2. - (cb**2*l5*sa*sb)/2. + (ca*cb*l4*sb**2)/2. - (ca*cb*l5*sb**2)/2. + (l4*sa*sb**3)/2. - (l5*sa*sb**3)/2.',
                 order = {'QED':2})

GC_49 = Coupling(name = 'GC_49',
                 value = '-(ca*cb**3*l4)/2. + (ca*cb**3*l5)/2. - (cb**2*l4*sa*sb)/2. + (cb**2*l5*sa*sb)/2. - (ca*cb*l4*sb**2)/2. + (ca*cb*l5*sb**2)/2. - (l4*sa*sb**3)/2. + (l5*sa*sb**3)/2.',
                 order = {'QED':2})

GC_50 = Coupling(name = 'GC_50',
                 value = '-(cb**4*complex(0,1)*l2) - 2*cb**2*complex(0,1)*l3*sb**2 - 2*cb**2*complex(0,1)*l4*sb**2 - 2*cb**2*complex(0,1)*l5*sb**2 - complex(0,1)*l1*sb**4',
                 order = {'QED':2})

GC_51 = Coupling(name = 'GC_51',
                 value = '-2*cb**4*complex(0,1)*l2 - 4*cb**2*complex(0,1)*l3*sb**2 - 4*cb**2*complex(0,1)*l4*sb**2 - 4*cb**2*complex(0,1)*l5*sb**2 - 2*complex(0,1)*l1*sb**4',
                 order = {'QED':2})

GC_52 = Coupling(name = 'GC_52',
                 value = '-3*cb**4*complex(0,1)*l2 - 6*cb**2*complex(0,1)*l3*sb**2 - 6*cb**2*complex(0,1)*l4*sb**2 - 6*cb**2*complex(0,1)*l5*sb**2 - 3*complex(0,1)*l1*sb**4',
                 order = {'QED':2})

GC_53 = Coupling(name = 'GC_53',
                 value = '-(cb**4*complex(0,1)*l1) - 2*cb**2*complex(0,1)*l3*sb**2 - 2*cb**2*complex(0,1)*l4*sb**2 - 2*cb**2*complex(0,1)*l5*sb**2 - complex(0,1)*l2*sb**4',
                 order = {'QED':2})

GC_54 = Coupling(name = 'GC_54',
                 value = '-2*cb**4*complex(0,1)*l1 - 4*cb**2*complex(0,1)*l3*sb**2 - 4*cb**2*complex(0,1)*l4*sb**2 - 4*cb**2*complex(0,1)*l5*sb**2 - 2*complex(0,1)*l2*sb**4',
                 order = {'QED':2})

GC_55 = Coupling(name = 'GC_55',
                 value = '-3*cb**4*complex(0,1)*l1 - 6*cb**2*complex(0,1)*l3*sb**2 - 6*cb**2*complex(0,1)*l4*sb**2 - 6*cb**2*complex(0,1)*l5*sb**2 - 3*complex(0,1)*l2*sb**4',
                 order = {'QED':2})

GC_56 = Coupling(name = 'GC_56',
                 value = '-(cb**4*complex(0,1)*l3) - cb**2*complex(0,1)*l1*sb**2 - cb**2*complex(0,1)*l2*sb**2 + 2*cb**2*complex(0,1)*l4*sb**2 + 2*cb**2*complex(0,1)*l5*sb**2 - complex(0,1)*l3*sb**4',
                 order = {'QED':2})

GC_57 = Coupling(name = 'GC_57',
                 value = '-(cb**4*complex(0,1)*l3) - cb**4*complex(0,1)*l4 - 2*cb**2*complex(0,1)*l1*sb**2 - 2*cb**2*complex(0,1)*l2*sb**2 + 2*cb**2*complex(0,1)*l3*sb**2 + 2*cb**2*complex(0,1)*l4*sb**2 + 4*cb**2*complex(0,1)*l5*sb**2 - complex(0,1)*l3*sb**4 - complex(0,1)*l4*sb**4',
                 order = {'QED':2})

GC_58 = Coupling(name = 'GC_58',
                 value = '-(cb**4*complex(0,1)*l4)/2. - (cb**4*complex(0,1)*l5)/2. - cb**2*complex(0,1)*l1*sb**2 - cb**2*complex(0,1)*l2*sb**2 + 2*cb**2*complex(0,1)*l3*sb**2 + cb**2*complex(0,1)*l4*sb**2 + cb**2*complex(0,1)*l5*sb**2 - (complex(0,1)*l4*sb**4)/2. - (complex(0,1)*l5*sb**4)/2.',
                 order = {'QED':2})

GC_59 = Coupling(name = 'GC_59',
                 value = '-(cb**4*complex(0,1)*l3) - cb**4*complex(0,1)*l4 - cb**4*complex(0,1)*l5 - 3*cb**2*complex(0,1)*l1*sb**2 - 3*cb**2*complex(0,1)*l2*sb**2 + 4*cb**2*complex(0,1)*l3*sb**2 + 4*cb**2*complex(0,1)*l4*sb**2 + 4*cb**2*complex(0,1)*l5*sb**2 - complex(0,1)*l3*sb**4 - complex(0,1)*l4*sb**4 - complex(0,1)*l5*sb**4',
                 order = {'QED':2})

GC_60 = Coupling(name = 'GC_60',
                 value = '-2*cb**4*complex(0,1)*l5 - 2*cb**2*complex(0,1)*l1*sb**2 - 2*cb**2*complex(0,1)*l2*sb**2 + 4*cb**2*complex(0,1)*l3*sb**2 + 4*cb**2*complex(0,1)*l4*sb**2 - 2*complex(0,1)*l5*sb**4',
                 order = {'QED':2})

GC_61 = Coupling(name = 'GC_61',
                 value = '(ca**2*ee**2*complex(0,1))/(2.*sw**2) + (ee**2*complex(0,1)*sa**2)/(2.*sw**2)',
                 order = {'QED':2})

GC_62 = Coupling(name = 'GC_62',
                 value = '(cb**2*ee**2*complex(0,1))/(2.*sw**2) + (ee**2*complex(0,1)*sb**2)/(2.*sw**2)',
                 order = {'QED':2})

GC_63 = Coupling(name = 'GC_63',
                 value = '(cb*ee*complex(0,1)*sa)/(2.*sw) - (ca*ee*complex(0,1)*sb)/(2.*sw)',
                 order = {'QED':1})

GC_64 = Coupling(name = 'GC_64',
                 value = '-(cb*ee*complex(0,1)*sa)/(2.*sw) + (ca*ee*complex(0,1)*sb)/(2.*sw)',
                 order = {'QED':1})

GC_65 = Coupling(name = 'GC_65',
                 value = '(cb*ee**2*complex(0,1)*sa)/(2.*sw) - (ca*ee**2*complex(0,1)*sb)/(2.*sw)',
                 order = {'QED':2})

GC_66 = Coupling(name = 'GC_66',
                 value = '-(cb*ee**2*complex(0,1)*sa)/(2.*sw) + (ca*ee**2*complex(0,1)*sb)/(2.*sw)',
                 order = {'QED':2})

GC_67 = Coupling(name = 'GC_67',
                 value = '(cb*ee*complex(0,1)*MW*sa)/(2.*sw) - (ca*ee*complex(0,1)*MW*sb)/(2.*sw)',
                 order = {'QED':1})

GC_68 = Coupling(name = 'GC_68',
                 value = '-(ca*cb*ee*complex(0,1))/(2.*sw) - (ee*complex(0,1)*sa*sb)/(2.*sw)',
                 order = {'QED':1})

GC_69 = Coupling(name = 'GC_69',
                 value = '(ca*cb*ee*complex(0,1))/(2.*sw) + (ee*complex(0,1)*sa*sb)/(2.*sw)',
                 order = {'QED':1})

GC_70 = Coupling(name = 'GC_70',
                 value = '-(ca*cb*ee**2*complex(0,1))/(2.*sw) - (ee**2*complex(0,1)*sa*sb)/(2.*sw)',
                 order = {'QED':2})

GC_71 = Coupling(name = 'GC_71',
                 value = '-(ca*cb*ee*complex(0,1)*MW)/(2.*sw) - (ee*complex(0,1)*MW*sa*sb)/(2.*sw)',
                 order = {'QED':1})

GC_72 = Coupling(name = 'GC_72',
                 value = '(cb**2*ee)/(2.*sw) + (ee*sb**2)/(2.*sw)',
                 order = {'QED':1})

GC_73 = Coupling(name = 'GC_73',
                 value = '-(cb**2*ee**2)/(2.*sw) - (ee**2*sb**2)/(2.*sw)',
                 order = {'QED':2})

GC_74 = Coupling(name = 'GC_74',
                 value = '(cb**2*ee**2)/(2.*sw) + (ee**2*sb**2)/(2.*sw)',
                 order = {'QED':2})

GC_75 = Coupling(name = 'GC_75',
                 value = '-(cb**2*ee*MW)/(2.*sw) - (ee*MW*sb**2)/(2.*sw)',
                 order = {'QED':1})

GC_76 = Coupling(name = 'GC_76',
                 value = '(cb**2*ee*MW)/(2.*sw) + (ee*MW*sb**2)/(2.*sw)',
                 order = {'QED':1})

GC_77 = Coupling(name = 'GC_77',
                 value = '(cb**2*ee*complex(0,1)*MZ)/(2.*sw) + (ee*complex(0,1)*MZ*sb**2)/(2.*sw)',
                 order = {'QED':1})

GC_78 = Coupling(name = 'GC_78',
                 value = '-((ee**2*complex(0,1))/sw**2)',
                 order = {'QED':2})

GC_79 = Coupling(name = 'GC_79',
                 value = '(cw**2*ee**2*complex(0,1))/sw**2',
                 order = {'QED':2})

GC_80 = Coupling(name = 'GC_80',
                 value = '(ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_81 = Coupling(name = 'GC_81',
                 value = '-((cw*ee*complex(0,1))/sw)',
                 order = {'QED':1})

GC_82 = Coupling(name = 'GC_82',
                 value = '(cw*ee*complex(0,1))/sw',
                 order = {'QED':1})

GC_83 = Coupling(name = 'GC_83',
                 value = '(2*cw*ee**2*complex(0,1))/sw',
                 order = {'QED':2})

GC_84 = Coupling(name = 'GC_84',
                 value = '(ee*complex(0,1)*sw)/(3.*cw)',
                 order = {'QED':1})

GC_85 = Coupling(name = 'GC_85',
                 value = '(-2*ee*complex(0,1)*sw)/(3.*cw)',
                 order = {'QED':1})

GC_86 = Coupling(name = 'GC_86',
                 value = '(ee*complex(0,1)*sw)/cw',
                 order = {'QED':1})

GC_87 = Coupling(name = 'GC_87',
                 value = '-(cw*ee*complex(0,1))/(2.*sw) - (ee*complex(0,1)*sw)/(6.*cw)',
                 order = {'QED':1})

GC_88 = Coupling(name = 'GC_88',
                 value = '(cw*ee*complex(0,1))/(2.*sw) - (ee*complex(0,1)*sw)/(6.*cw)',
                 order = {'QED':1})

GC_89 = Coupling(name = 'GC_89',
                 value = '-(cw*ee*complex(0,1))/(2.*sw) + (ee*complex(0,1)*sw)/(2.*cw)',
                 order = {'QED':1})

GC_90 = Coupling(name = 'GC_90',
                 value = '(cw*ee*complex(0,1))/(2.*sw) + (ee*complex(0,1)*sw)/(2.*cw)',
                 order = {'QED':1})

GC_91 = Coupling(name = 'GC_91',
                 value = '(cb*cw*ee*sa)/(2.*sw) - (ca*cw*ee*sb)/(2.*sw) + (cb*ee*sa*sw)/(2.*cw) - (ca*ee*sb*sw)/(2.*cw)',
                 order = {'QED':1})

GC_92 = Coupling(name = 'GC_92',
                 value = '-(cb*cw*ee*sa)/(2.*sw) + (ca*cw*ee*sb)/(2.*sw) - (cb*ee*sa*sw)/(2.*cw) + (ca*ee*sb*sw)/(2.*cw)',
                 order = {'QED':1})

GC_93 = Coupling(name = 'GC_93',
                 value = '(cb*cw*ee*complex(0,1)*MZ*sa)/(2.*sw) - (ca*cw*ee*complex(0,1)*MZ*sb)/(2.*sw) + (cb*ee*complex(0,1)*MZ*sa*sw)/(2.*cw) - (ca*ee*complex(0,1)*MZ*sb*sw)/(2.*cw)',
                 order = {'QED':1})

GC_94 = Coupling(name = 'GC_94',
                 value = '-(ca*cb*cw*ee)/(2.*sw) - (cw*ee*sa*sb)/(2.*sw) - (ca*cb*ee*sw)/(2.*cw) - (ee*sa*sb*sw)/(2.*cw)',
                 order = {'QED':1})

GC_95 = Coupling(name = 'GC_95',
                 value = '-(ca*cb*cw*ee*complex(0,1)*MZ)/(2.*sw) - (cw*ee*complex(0,1)*MZ*sa*sb)/(2.*sw) - (ca*cb*ee*complex(0,1)*MZ*sw)/(2.*cw) - (ee*complex(0,1)*MZ*sa*sb*sw)/(2.*cw)',
                 order = {'QED':1})

GC_96 = Coupling(name = 'GC_96',
                 value = '-(cb**2*cw*ee*complex(0,1))/(2.*sw) - (cw*ee*complex(0,1)*sb**2)/(2.*sw) + (cb**2*ee*complex(0,1)*sw)/(2.*cw) + (ee*complex(0,1)*sb**2*sw)/(2.*cw)',
                 order = {'QED':1})

GC_97 = Coupling(name = 'GC_97',
                 value = '-((cb**2*cw*ee**2*complex(0,1))/sw) - (cw*ee**2*complex(0,1)*sb**2)/sw + (cb**2*ee**2*complex(0,1)*sw)/cw + (ee**2*complex(0,1)*sb**2*sw)/cw',
                 order = {'QED':2})

GC_98 = Coupling(name = 'GC_98',
                 value = '-(cb**2*cw*ee*complex(0,1)*MW)/(2.*sw) - (cw*ee*complex(0,1)*MW*sb**2)/(2.*sw) + (cb**2*ee*complex(0,1)*MW*sw)/(2.*cw) + (ee*complex(0,1)*MW*sb**2*sw)/(2.*cw)',
                 order = {'QED':1})

GC_99 = Coupling(name = 'GC_99',
                 value = 'ca**2*ee**2*complex(0,1) + ee**2*complex(0,1)*sa**2 + (ca**2*cw**2*ee**2*complex(0,1))/(2.*sw**2) + (cw**2*ee**2*complex(0,1)*sa**2)/(2.*sw**2) + (ca**2*ee**2*complex(0,1)*sw**2)/(2.*cw**2) + (ee**2*complex(0,1)*sa**2*sw**2)/(2.*cw**2)',
                 order = {'QED':2})

GC_100 = Coupling(name = 'GC_100',
                  value = '-(cb**2*ee**2*complex(0,1)) - ee**2*complex(0,1)*sb**2 + (cb**2*cw**2*ee**2*complex(0,1))/(2.*sw**2) + (cw**2*ee**2*complex(0,1)*sb**2)/(2.*sw**2) + (cb**2*ee**2*complex(0,1)*sw**2)/(2.*cw**2) + (ee**2*complex(0,1)*sb**2*sw**2)/(2.*cw**2)',
                  order = {'QED':2})

GC_101 = Coupling(name = 'GC_101',
                  value = 'cb**2*ee**2*complex(0,1) + ee**2*complex(0,1)*sb**2 + (cb**2*cw**2*ee**2*complex(0,1))/(2.*sw**2) + (cw**2*ee**2*complex(0,1)*sb**2)/(2.*sw**2) + (cb**2*ee**2*complex(0,1)*sw**2)/(2.*cw**2) + (ee**2*complex(0,1)*sb**2*sw**2)/(2.*cw**2)',
                  order = {'QED':2})

GC_102 = Coupling(name = 'GC_102',
                  value = '(ee**2*complex(0,1)*sb*vev1)/(2.*cw) - (cb*ee**2*complex(0,1)*vev2)/(2.*cw)',
                  order = {'QED':1})

GC_103 = Coupling(name = 'GC_103',
                  value = '3*ca**2*complex(0,1)*l1*sa*vev1 - 2*ca**2*complex(0,1)*l3*sa*vev1 - 2*ca**2*complex(0,1)*l4*sa*vev1 - 2*ca**2*complex(0,1)*l5*sa*vev1 + complex(0,1)*l3*sa**3*vev1 + complex(0,1)*l4*sa**3*vev1 + complex(0,1)*l5*sa**3*vev1 - ca**3*complex(0,1)*l3*vev2 - ca**3*complex(0,1)*l4*vev2 - ca**3*complex(0,1)*l5*vev2 - 3*ca*complex(0,1)*l2*sa**2*vev2 + 2*ca*complex(0,1)*l3*sa**2*vev2 + 2*ca*complex(0,1)*l4*sa**2*vev2 + 2*ca*complex(0,1)*l5*sa**2*vev2',
                  order = {'QED':1})

GC_104 = Coupling(name = 'GC_104',
                  value = '3*ca**2*complex(0,1)*l3*sa*vev1 + 3*ca**2*complex(0,1)*l4*sa*vev1 + 3*ca**2*complex(0,1)*l5*sa*vev1 + 3*complex(0,1)*l1*sa**3*vev1 - 3*ca**3*complex(0,1)*l2*vev2 - 3*ca*complex(0,1)*l3*sa**2*vev2 - 3*ca*complex(0,1)*l4*sa**2*vev2 - 3*ca*complex(0,1)*l5*sa**2*vev2',
                  order = {'QED':1})

GC_105 = Coupling(name = 'GC_105',
                  value = '-3*ca**3*complex(0,1)*l1*vev1 - 3*ca*complex(0,1)*l3*sa**2*vev1 - 3*ca*complex(0,1)*l4*sa**2*vev1 - 3*ca*complex(0,1)*l5*sa**2*vev1 - 3*ca**2*complex(0,1)*l3*sa*vev2 - 3*ca**2*complex(0,1)*l4*sa*vev2 - 3*ca**2*complex(0,1)*l5*sa*vev2 - 3*complex(0,1)*l2*sa**3*vev2',
                  order = {'QED':1})

GC_106 = Coupling(name = 'GC_106',
                  value = '-(ca**3*complex(0,1)*l3*vev1) - ca**3*complex(0,1)*l4*vev1 - ca**3*complex(0,1)*l5*vev1 - 3*ca*complex(0,1)*l1*sa**2*vev1 + 2*ca*complex(0,1)*l3*sa**2*vev1 + 2*ca*complex(0,1)*l4*sa**2*vev1 + 2*ca*complex(0,1)*l5*sa**2*vev1 - 3*ca**2*complex(0,1)*l2*sa*vev2 + 2*ca**2*complex(0,1)*l3*sa*vev2 + 2*ca**2*complex(0,1)*l4*sa*vev2 + 2*ca**2*complex(0,1)*l5*sa*vev2 - complex(0,1)*l3*sa**3*vev2 - complex(0,1)*l4*sa**3*vev2 - complex(0,1)*l5*sa**3*vev2',
                  order = {'QED':1})

GC_107 = Coupling(name = 'GC_107',
                  value = '-(cb*ee**2*complex(0,1)*vev1)/(2.*cw) - (ee**2*complex(0,1)*sb*vev2)/(2.*cw)',
                  order = {'QED':1})

GC_108 = Coupling(name = 'GC_108',
                  value = 'cb**2*complex(0,1)*l1*sa*vev1 - ca*cb*complex(0,1)*l4*sb*vev1 - ca*cb*complex(0,1)*l5*sb*vev1 + complex(0,1)*l3*sa*sb**2*vev1 - ca*cb**2*complex(0,1)*l3*vev2 + cb*complex(0,1)*l4*sa*sb*vev2 + cb*complex(0,1)*l5*sa*sb*vev2 - ca*complex(0,1)*l2*sb**2*vev2',
                  order = {'QED':1})

GC_109 = Coupling(name = 'GC_109',
                  value = 'cb**2*complex(0,1)*l1*sa*vev1 - 2*ca*cb*complex(0,1)*l5*sb*vev1 + complex(0,1)*l3*sa*sb**2*vev1 + complex(0,1)*l4*sa*sb**2*vev1 - complex(0,1)*l5*sa*sb**2*vev1 - ca*cb**2*complex(0,1)*l3*vev2 - ca*cb**2*complex(0,1)*l4*vev2 + ca*cb**2*complex(0,1)*l5*vev2 + 2*cb*complex(0,1)*l5*sa*sb*vev2 - ca*complex(0,1)*l2*sb**2*vev2',
                  order = {'QED':1})

GC_110 = Coupling(name = 'GC_110',
                  value = 'cb**2*complex(0,1)*l3*sa*vev1 + ca*cb*complex(0,1)*l4*sb*vev1 + ca*cb*complex(0,1)*l5*sb*vev1 + complex(0,1)*l1*sa*sb**2*vev1 - ca*cb**2*complex(0,1)*l2*vev2 - cb*complex(0,1)*l4*sa*sb*vev2 - cb*complex(0,1)*l5*sa*sb*vev2 - ca*complex(0,1)*l3*sb**2*vev2',
                  order = {'QED':1})

GC_111 = Coupling(name = 'GC_111',
                  value = '-(cb**2*complex(0,1)*l4*sa*vev1)/2. - (cb**2*complex(0,1)*l5*sa*vev1)/2. + ca*cb*complex(0,1)*l1*sb*vev1 - ca*cb*complex(0,1)*l3*sb*vev1 + (complex(0,1)*l4*sa*sb**2*vev1)/2. + (complex(0,1)*l5*sa*sb**2*vev1)/2. - (ca*cb**2*complex(0,1)*l4*vev2)/2. - (ca*cb**2*complex(0,1)*l5*vev2)/2. - cb*complex(0,1)*l2*sa*sb*vev2 + cb*complex(0,1)*l3*sa*sb*vev2 + (ca*complex(0,1)*l4*sb**2*vev2)/2. + (ca*complex(0,1)*l5*sb**2*vev2)/2.',
                  order = {'QED':1})

GC_112 = Coupling(name = 'GC_112',
                  value = '-(cb**2*complex(0,1)*l5*sa*vev1) + ca*cb*complex(0,1)*l1*sb*vev1 - ca*cb*complex(0,1)*l3*sb*vev1 - ca*cb*complex(0,1)*l4*sb*vev1 + ca*cb*complex(0,1)*l5*sb*vev1 + complex(0,1)*l5*sa*sb**2*vev1 - ca*cb**2*complex(0,1)*l5*vev2 - cb*complex(0,1)*l2*sa*sb*vev2 + cb*complex(0,1)*l3*sa*sb*vev2 + cb*complex(0,1)*l4*sa*sb*vev2 - cb*complex(0,1)*l5*sa*sb*vev2 + ca*complex(0,1)*l5*sb**2*vev2',
                  order = {'QED':1})

GC_113 = Coupling(name = 'GC_113',
                  value = 'cb**2*complex(0,1)*l3*sa*vev1 + cb**2*complex(0,1)*l4*sa*vev1 - cb**2*complex(0,1)*l5*sa*vev1 + 2*ca*cb*complex(0,1)*l5*sb*vev1 + complex(0,1)*l1*sa*sb**2*vev1 - ca*cb**2*complex(0,1)*l2*vev2 - 2*cb*complex(0,1)*l5*sa*sb*vev2 - ca*complex(0,1)*l3*sb**2*vev2 - ca*complex(0,1)*l4*sb**2*vev2 + ca*complex(0,1)*l5*sb**2*vev2',
                  order = {'QED':1})

GC_114 = Coupling(name = 'GC_114',
                  value = '-(cb**2*l4*sb*vev1)/2. + (cb**2*l5*sb*vev1)/2. - (l4*sb**3*vev1)/2. + (l5*sb**3*vev1)/2. + (cb**3*l4*vev2)/2. - (cb**3*l5*vev2)/2. + (cb*l4*sb**2*vev2)/2. - (cb*l5*sb**2*vev2)/2.',
                  order = {'QED':1})

GC_115 = Coupling(name = 'GC_115',
                  value = '(cb**2*l4*sb*vev1)/2. - (cb**2*l5*sb*vev1)/2. + (l4*sb**3*vev1)/2. - (l5*sb**3*vev1)/2. - (cb**3*l4*vev2)/2. + (cb**3*l5*vev2)/2. - (cb*l4*sb**2*vev2)/2. + (cb*l5*sb**2*vev2)/2.',
                  order = {'QED':1})

GC_116 = Coupling(name = 'GC_116',
                  value = '-(ca*cb**2*complex(0,1)*l1*vev1) - cb*complex(0,1)*l4*sa*sb*vev1 - cb*complex(0,1)*l5*sa*sb*vev1 - ca*complex(0,1)*l3*sb**2*vev1 - cb**2*complex(0,1)*l3*sa*vev2 - ca*cb*complex(0,1)*l4*sb*vev2 - ca*cb*complex(0,1)*l5*sb*vev2 - complex(0,1)*l2*sa*sb**2*vev2',
                  order = {'QED':1})

GC_117 = Coupling(name = 'GC_117',
                  value = '-(ca*cb**2*complex(0,1)*l1*vev1) - 2*cb*complex(0,1)*l5*sa*sb*vev1 - ca*complex(0,1)*l3*sb**2*vev1 - ca*complex(0,1)*l4*sb**2*vev1 + ca*complex(0,1)*l5*sb**2*vev1 - cb**2*complex(0,1)*l3*sa*vev2 - cb**2*complex(0,1)*l4*sa*vev2 + cb**2*complex(0,1)*l5*sa*vev2 - 2*ca*cb*complex(0,1)*l5*sb*vev2 - complex(0,1)*l2*sa*sb**2*vev2',
                  order = {'QED':1})

GC_118 = Coupling(name = 'GC_118',
                  value = '-(ca*cb**2*complex(0,1)*l3*vev1) + cb*complex(0,1)*l4*sa*sb*vev1 + cb*complex(0,1)*l5*sa*sb*vev1 - ca*complex(0,1)*l1*sb**2*vev1 - cb**2*complex(0,1)*l2*sa*vev2 + ca*cb*complex(0,1)*l4*sb*vev2 + ca*cb*complex(0,1)*l5*sb*vev2 - complex(0,1)*l3*sa*sb**2*vev2',
                  order = {'QED':1})

GC_119 = Coupling(name = 'GC_119',
                  value = '-(ca*cb**2*complex(0,1)*l4*vev1)/2. - (ca*cb**2*complex(0,1)*l5*vev1)/2. - cb*complex(0,1)*l1*sa*sb*vev1 + cb*complex(0,1)*l3*sa*sb*vev1 + (ca*complex(0,1)*l4*sb**2*vev1)/2. + (ca*complex(0,1)*l5*sb**2*vev1)/2. + (cb**2*complex(0,1)*l4*sa*vev2)/2. + (cb**2*complex(0,1)*l5*sa*vev2)/2. - ca*cb*complex(0,1)*l2*sb*vev2 + ca*cb*complex(0,1)*l3*sb*vev2 - (complex(0,1)*l4*sa*sb**2*vev2)/2. - (complex(0,1)*l5*sa*sb**2*vev2)/2.',
                  order = {'QED':1})

GC_120 = Coupling(name = 'GC_120',
                  value = '-(ca*cb**2*complex(0,1)*l5*vev1) - cb*complex(0,1)*l1*sa*sb*vev1 + cb*complex(0,1)*l3*sa*sb*vev1 + cb*complex(0,1)*l4*sa*sb*vev1 - cb*complex(0,1)*l5*sa*sb*vev1 + ca*complex(0,1)*l5*sb**2*vev1 + cb**2*complex(0,1)*l5*sa*vev2 - ca*cb*complex(0,1)*l2*sb*vev2 + ca*cb*complex(0,1)*l3*sb*vev2 + ca*cb*complex(0,1)*l4*sb*vev2 - ca*cb*complex(0,1)*l5*sb*vev2 - complex(0,1)*l5*sa*sb**2*vev2',
                  order = {'QED':1})

GC_121 = Coupling(name = 'GC_121',
                  value = '-(ca*cb**2*complex(0,1)*l3*vev1) - ca*cb**2*complex(0,1)*l4*vev1 + ca*cb**2*complex(0,1)*l5*vev1 + 2*cb*complex(0,1)*l5*sa*sb*vev1 - ca*complex(0,1)*l1*sb**2*vev1 - cb**2*complex(0,1)*l2*sa*vev2 + 2*ca*cb*complex(0,1)*l5*sb*vev2 - complex(0,1)*l3*sa*sb**2*vev2 - complex(0,1)*l4*sa*sb**2*vev2 + complex(0,1)*l5*sa*sb**2*vev2',
                  order = {'QED':1})

GC_122 = Coupling(name = 'GC_122',
                  value = '(cb**3*l4*vev1)/2. - (cb**3*l5*vev1)/2. + (cb*l4*sb**2*vev1)/2. - (cb*l5*sb**2*vev1)/2. + (cb**2*l4*sb*vev2)/2. - (cb**2*l5*sb*vev2)/2. + (l4*sb**3*vev2)/2. - (l5*sb**3*vev2)/2.',
                  order = {'QED':1})

GC_123 = Coupling(name = 'GC_123',
                  value = '-(cb**3*l4*vev1)/2. + (cb**3*l5*vev1)/2. - (cb*l4*sb**2*vev1)/2. + (cb*l5*sb**2*vev1)/2. - (cb**2*l4*sb*vev2)/2. + (cb**2*l5*sb*vev2)/2. - (l4*sb**3*vev2)/2. + (l5*sb**3*vev2)/2.',
                  order = {'QED':1})

GC_124 = Coupling(name = 'GC_124',
                  value = '-(ee**2*complex(0,1)*sa*vev1)/(2.*sw**2) + (ca*ee**2*complex(0,1)*vev2)/(2.*sw**2)',
                  order = {'QED':1})

GC_125 = Coupling(name = 'GC_125',
                  value = '(ca*ee**2*complex(0,1)*vev1)/(2.*sw**2) + (ee**2*complex(0,1)*sa*vev2)/(2.*sw**2)',
                  order = {'QED':1})

GC_126 = Coupling(name = 'GC_126',
                  value = '(ee**2*complex(0,1)*sb*vev1)/(2.*sw) - (cb*ee**2*complex(0,1)*vev2)/(2.*sw)',
                  order = {'QED':1})

GC_127 = Coupling(name = 'GC_127',
                  value = '-(cb*ee**2*complex(0,1)*vev1)/(2.*sw) - (ee**2*complex(0,1)*sb*vev2)/(2.*sw)',
                  order = {'QED':1})

GC_128 = Coupling(name = 'GC_128',
                  value = '-(ee**2*complex(0,1)*sa*vev1) - (cw**2*ee**2*complex(0,1)*sa*vev1)/(2.*sw**2) - (ee**2*complex(0,1)*sa*sw**2*vev1)/(2.*cw**2) + ca*ee**2*complex(0,1)*vev2 + (ca*cw**2*ee**2*complex(0,1)*vev2)/(2.*sw**2) + (ca*ee**2*complex(0,1)*sw**2*vev2)/(2.*cw**2)',
                  order = {'QED':1})

GC_129 = Coupling(name = 'GC_129',
                  value = 'ca*ee**2*complex(0,1)*vev1 + (ca*cw**2*ee**2*complex(0,1)*vev1)/(2.*sw**2) + (ca*ee**2*complex(0,1)*sw**2*vev1)/(2.*cw**2) + ee**2*complex(0,1)*sa*vev2 + (cw**2*ee**2*complex(0,1)*sa*vev2)/(2.*sw**2) + (ee**2*complex(0,1)*sa*sw**2*vev2)/(2.*cw**2)',
                  order = {'QED':1})

GC_130 = Coupling(name = 'GC_130',
                  value = '(complex(0,1)*sa*yc1)/cmath.sqrt(2) - (ca*complex(0,1)*yc2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_131 = Coupling(name = 'GC_131',
                  value = '-(complex(0,1)*sb*yc1) + cb*complex(0,1)*yc2',
                  order = {'QED':1})

GC_132 = Coupling(name = 'GC_132',
                  value = '-((sb*yc1)/cmath.sqrt(2)) + (cb*yc2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_133 = Coupling(name = 'GC_133',
                  value = '-((ca*complex(0,1)*yc1)/cmath.sqrt(2)) - (complex(0,1)*sa*yc2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_134 = Coupling(name = 'GC_134',
                  value = 'cb*complex(0,1)*yc1 + complex(0,1)*sb*yc2',
                  order = {'QED':1})

GC_135 = Coupling(name = 'GC_135',
                  value = '-((cb*yc1)/cmath.sqrt(2)) - (sb*yc2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_136 = Coupling(name = 'GC_136',
                  value = '(cb*yc1)/cmath.sqrt(2) + (sb*yc2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_137 = Coupling(name = 'GC_137',
                  value = '-((complex(0,1)*sa*yb2)/cmath.sqrt(2)) - (ca*complex(0,1)*yd13x3)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_138 = Coupling(name = 'GC_138',
                  value = '-(complex(0,1)*sb*yb2) - cb*complex(0,1)*yd13x3',
                  order = {'QED':1})

GC_139 = Coupling(name = 'GC_139',
                  value = '-((sb*yb2)/cmath.sqrt(2)) - (cb*yd13x3)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_140 = Coupling(name = 'GC_140',
                  value = '(sb*yb2)/cmath.sqrt(2) + (cb*yd13x3)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_141 = Coupling(name = 'GC_141',
                  value = '-((ca*complex(0,1)*yb2)/cmath.sqrt(2)) + (complex(0,1)*sa*yd13x3)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_142 = Coupling(name = 'GC_142',
                  value = '-(cb*complex(0,1)*yb2) + complex(0,1)*sb*yd13x3',
                  order = {'QED':1})

GC_143 = Coupling(name = 'GC_143',
                  value = '(cb*yb2)/cmath.sqrt(2) - (sb*yd13x3)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_144 = Coupling(name = 'GC_144',
                  value = '-((cb*yb2)/cmath.sqrt(2)) + (sb*yd13x3)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_145 = Coupling(name = 'GC_145',
                  value = '(complex(0,1)*sa*ydo1)/cmath.sqrt(2) - (ca*complex(0,1)*ydo2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_146 = Coupling(name = 'GC_146',
                  value = 'complex(0,1)*sb*ydo1 - cb*complex(0,1)*ydo2',
                  order = {'QED':1})

GC_147 = Coupling(name = 'GC_147',
                  value = '(sb*ydo1)/cmath.sqrt(2) - (cb*ydo2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_148 = Coupling(name = 'GC_148',
                  value = '-((sb*ydo1)/cmath.sqrt(2)) + (cb*ydo2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_149 = Coupling(name = 'GC_149',
                  value = '-((ca*complex(0,1)*ydo1)/cmath.sqrt(2)) - (complex(0,1)*sa*ydo2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_150 = Coupling(name = 'GC_150',
                  value = '-(cb*complex(0,1)*ydo1) - complex(0,1)*sb*ydo2',
                  order = {'QED':1})

GC_151 = Coupling(name = 'GC_151',
                  value = '-((cb*ydo1)/cmath.sqrt(2)) - (sb*ydo2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_152 = Coupling(name = 'GC_152',
                  value = '(cb*ydo1)/cmath.sqrt(2) + (sb*ydo2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_153 = Coupling(name = 'GC_153',
                  value = '(complex(0,1)*sa*ye1)/cmath.sqrt(2) - (ca*complex(0,1)*ye2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_154 = Coupling(name = 'GC_154',
                  value = 'complex(0,1)*sb*ye1 - cb*complex(0,1)*ye2',
                  order = {'QED':1})

GC_155 = Coupling(name = 'GC_155',
                  value = '(sb*ye1)/cmath.sqrt(2) - (cb*ye2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_156 = Coupling(name = 'GC_156',
                  value = '-((sb*ye1)/cmath.sqrt(2)) + (cb*ye2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_157 = Coupling(name = 'GC_157',
                  value = '-((ca*complex(0,1)*ye1)/cmath.sqrt(2)) - (complex(0,1)*sa*ye2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_158 = Coupling(name = 'GC_158',
                  value = '-(cb*complex(0,1)*ye1) - complex(0,1)*sb*ye2',
                  order = {'QED':1})

GC_159 = Coupling(name = 'GC_159',
                  value = '-((cb*ye1)/cmath.sqrt(2)) - (sb*ye2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_160 = Coupling(name = 'GC_160',
                  value = '(cb*ye1)/cmath.sqrt(2) + (sb*ye2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_161 = Coupling(name = 'GC_161',
                  value = '(complex(0,1)*sa*ym1)/cmath.sqrt(2) - (ca*complex(0,1)*ym2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_162 = Coupling(name = 'GC_162',
                  value = 'complex(0,1)*sb*ym1 - cb*complex(0,1)*ym2',
                  order = {'QED':1})

GC_163 = Coupling(name = 'GC_163',
                  value = '(sb*ym1)/cmath.sqrt(2) - (cb*ym2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_164 = Coupling(name = 'GC_164',
                  value = '-((sb*ym1)/cmath.sqrt(2)) + (cb*ym2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_165 = Coupling(name = 'GC_165',
                  value = '-((ca*complex(0,1)*ym1)/cmath.sqrt(2)) - (complex(0,1)*sa*ym2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_166 = Coupling(name = 'GC_166',
                  value = '-(cb*complex(0,1)*ym1) - complex(0,1)*sb*ym2',
                  order = {'QED':1})

GC_167 = Coupling(name = 'GC_167',
                  value = '-((cb*ym1)/cmath.sqrt(2)) - (sb*ym2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_168 = Coupling(name = 'GC_168',
                  value = '(cb*ym1)/cmath.sqrt(2) + (sb*ym2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_169 = Coupling(name = 'GC_169',
                  value = '(complex(0,1)*sa*yd12x2)/cmath.sqrt(2) - (ca*complex(0,1)*ys2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_170 = Coupling(name = 'GC_170',
                  value = 'complex(0,1)*sb*yd12x2 - cb*complex(0,1)*ys2',
                  order = {'QED':1})

GC_171 = Coupling(name = 'GC_171',
                  value = '(sb*yd12x2)/cmath.sqrt(2) - (cb*ys2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_172 = Coupling(name = 'GC_172',
                  value = '-((sb*yd12x2)/cmath.sqrt(2)) + (cb*ys2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_173 = Coupling(name = 'GC_173',
                  value = '-((ca*complex(0,1)*yd12x2)/cmath.sqrt(2)) - (complex(0,1)*sa*ys2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_174 = Coupling(name = 'GC_174',
                  value = '-(cb*complex(0,1)*yd12x2) - complex(0,1)*sb*ys2',
                  order = {'QED':1})

GC_175 = Coupling(name = 'GC_175',
                  value = '-((cb*yd12x2)/cmath.sqrt(2)) - (sb*ys2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_176 = Coupling(name = 'GC_176',
                  value = '(cb*yd12x2)/cmath.sqrt(2) + (sb*ys2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_177 = Coupling(name = 'GC_177',
                  value = '(complex(0,1)*sa*yt1)/cmath.sqrt(2) - (ca*complex(0,1)*yt2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_178 = Coupling(name = 'GC_178',
                  value = '-(complex(0,1)*sb*yt1) + cb*complex(0,1)*yt2',
                  order = {'QED':1})

GC_179 = Coupling(name = 'GC_179',
                  value = '-((sb*yt1)/cmath.sqrt(2)) + (cb*yt2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_180 = Coupling(name = 'GC_180',
                  value = '-((ca*complex(0,1)*yt1)/cmath.sqrt(2)) - (complex(0,1)*sa*yt2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_181 = Coupling(name = 'GC_181',
                  value = 'cb*complex(0,1)*yt1 + complex(0,1)*sb*yt2',
                  order = {'QED':1})

GC_182 = Coupling(name = 'GC_182',
                  value = '-((cb*yt1)/cmath.sqrt(2)) - (sb*yt2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_183 = Coupling(name = 'GC_183',
                  value = '(cb*yt1)/cmath.sqrt(2) + (sb*yt2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_184 = Coupling(name = 'GC_184',
                  value = '(complex(0,1)*sa*ytau1)/cmath.sqrt(2) - (ca*complex(0,1)*ytau2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_185 = Coupling(name = 'GC_185',
                  value = 'complex(0,1)*sb*ytau1 - cb*complex(0,1)*ytau2',
                  order = {'QED':1})

GC_186 = Coupling(name = 'GC_186',
                  value = '(sb*ytau1)/cmath.sqrt(2) - (cb*ytau2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_187 = Coupling(name = 'GC_187',
                  value = '-((sb*ytau1)/cmath.sqrt(2)) + (cb*ytau2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_188 = Coupling(name = 'GC_188',
                  value = '-((ca*complex(0,1)*ytau1)/cmath.sqrt(2)) - (complex(0,1)*sa*ytau2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_189 = Coupling(name = 'GC_189',
                  value = '-(cb*complex(0,1)*ytau1) - complex(0,1)*sb*ytau2',
                  order = {'QED':1})

GC_190 = Coupling(name = 'GC_190',
                  value = '-((cb*ytau1)/cmath.sqrt(2)) - (sb*ytau2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_191 = Coupling(name = 'GC_191',
                  value = '(cb*ytau1)/cmath.sqrt(2) + (sb*ytau2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_192 = Coupling(name = 'GC_192',
                  value = '(complex(0,1)*sa*yup1)/cmath.sqrt(2) - (ca*complex(0,1)*yup2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_193 = Coupling(name = 'GC_193',
                  value = '-(complex(0,1)*sb*yup1) + cb*complex(0,1)*yup2',
                  order = {'QED':1})

GC_194 = Coupling(name = 'GC_194',
                  value = '-((sb*yup1)/cmath.sqrt(2)) + (cb*yup2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_195 = Coupling(name = 'GC_195',
                  value = '-((ca*complex(0,1)*yup1)/cmath.sqrt(2)) - (complex(0,1)*sa*yup2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_196 = Coupling(name = 'GC_196',
                  value = 'cb*complex(0,1)*yup1 + complex(0,1)*sb*yup2',
                  order = {'QED':1})

GC_197 = Coupling(name = 'GC_197',
                  value = '-((cb*yup1)/cmath.sqrt(2)) - (sb*yup2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_198 = Coupling(name = 'GC_198',
                  value = '(cb*yup1)/cmath.sqrt(2) + (sb*yup2)/cmath.sqrt(2)',
                  order = {'QED':1})

