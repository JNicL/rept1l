#==============================================================================#
#                                   model.py                                   #
#==============================================================================#

#============#
#  Includes  #
#============#

import os
from rept1l.autoct.modelfile import model, CTParameter
from rept1l.autoct.modelfile import TreeInducedCTVertex, CTVertex
from rept1l.autoct.auto_parameters_ct import auto_assign_ct_particle
from rept1l.autoct.auto_parameters_ct import assign_counterterm
from rept1l.autoct.auto_parameters_ct import set_light_particle
from rept1l.autoct.auto_tadpole_ct import assign_FJTadpole_field_shift
from rept1l.autoct.auto_tadpole_ct import auto_tadpole_vertices

#===========#
#  Globals  #
#===========#

# coupling constructor
Coupling = model.object_library.Coupling

# access to instances
param = model.parameters
P = model.particles
L = model.lorentz

modelname = 'THDM'
modelgauge = "'t Hooft-Feynman/tadpole DS"

features = {'fermionloop_opt' : True,
            'qcd_rescaling' : True,
            'sm_generation_opt': True,
           }


scales = [param.muMS_BSM]

# LCT allows to access the 2-point lorentz structures defined in lorentz_ct.py
import rept1l.autoct.lorentz_ct as LCT

#=============================#
#  External Parameter Orders  #
#=============================#

parameter_orders = {param.aEW: {'QED': 2},
                    param.aS: {'QCD': 2},
                    param.t1: {'QED': -1},
                    param.t2: {'QED': -1}}

#===============================#
#  Fermion mass regularization  #
#===============================#

for p in [P.e__minus__, P.mu__minus__, P.tau__minus__, P.u, P.d, P.c, P.s, P.b]:
  set_light_particle(p)

###################################
#  SM Optimizations for Fermions  #
###################################

doublets = [(P.nu_e, P.e__minus__), (P.nu_mu, P.mu__minus__),
            (P.nu_tau, P.tau__minus__), (P.u, P.d), (P.c, P.s), (P.t, P.b)]

generations = [(P.e__minus__, P.mu__minus__, P.tau__minus__),
               (P.d, P.s, P.b)]

##############################
#  QCD & EW Renormalization  #
##############################

assign_counterterm(param.ee, 'dZee', 'ee*dZee')
assign_counterterm(param.gs, 'dZgs', 'gs*dZgs')

##########################
#  2HDM Renormalization  #
##########################

assign_counterterm(param.cb, 'db', '-sb*db')
assign_counterterm(param.sb, 'db', 'cb*db')
assign_counterterm(param.sa, 'da', 'ca*da')
assign_counterterm(param.ca, 'da', '-sa*da')
assign_counterterm(param.MSB, 'dMSB2', 'dMSB2/(2*MSB)')

#######################
#  DS Tadpole scheme  #
#######################

tadpole_parameter = [param.t1, param.t2, param.k1, param.k2]
assign_counterterm(param.t1, 'dtHl', 'dtHl')
assign_counterterm(param.t2, 'dtHh', 'dtHh')

##############################
#  Particle Renormalization  #
##############################

mixings = {P.A: [P.Z], P.Z: [P.A],
           P.Ha: [P.G0], P.G0: [P.Ha],
           P.Hl: [P.Hh], P.Hh: [P.Hl],
           P.H__plus__: [P.G__plus__], P.G__plus__: [P.H__plus__],
           P.H__minus__: [P.G__minus__], P.G__minus__: [P.H__minus__]}

auto_assign_ct_particle(mixings)

#########################################
#  Renormalization of the Gauge-fixing  #
#########################################

GC_MZ_GF = Coupling(name='GC_MZ_GF',
                    value='MZ',
                    order={'QED': 0})

GC_MW_GF = Coupling(name='GC_MW_GF',
                    value='I*MW',
                    order={'QED': 0})

GC_MMW_GF = Coupling(name='GC_MMW_GF',
                     value='-I*MW',
                     order={'QED': 0})

V_WPGM = TreeInducedCTVertex(name='V_WPGM',
                             particles=[P.W__plus__, P.G__minus__],
                             color=['1'],
                             type='treeinducedct',
                             loop_particles='N',
                             lorentz=[LCT.VS1],
                             couplings={(0, 0): GC_MW_GF})

V_WMGP = TreeInducedCTVertex(name='V_WMGP',
                             particles=[P.W__minus__, P.G__plus__],
                             color=['1'],
                             type='treeinducedct',
                             loop_particles='N',
                             lorentz=[LCT.VS1],
                             couplings={(0, 0): GC_MMW_GF})

V_ZG0 = TreeInducedCTVertex(name='V_ZG0',
                            particles=[P.Z, P.G0],
                            color=['1'],
                            type='treeinducedct',
                            loop_particles='N',
                            lorentz=[LCT.VS1],
                            couplings={(0, 0): GC_MZ_GF})

#================#
#  Tadpoles n=2  #
#================#

GC_GGTadpole = Coupling(name='GC_GG',
                        value='-I*(cb*k1 + sb*k2)/(2*vev)',
                        order={'QED': 2})

GC_HAG0Tadpole = Coupling(name='GC_HAG0',
                          value='-I*(cb*k2 - sb*k1)/(2*vev)',
                          order={'QED': 2})

V_G0G0_T = TreeInducedCTVertex(name='V_G0G0_T',
                               particles=[P.G0, P.G0],
                               color=['1'],
                               type='massct',
                               loop_particles='N',
                               lorentz=[LCT.SS1],
                               couplings={(0, 0): GC_GGTadpole},
                               expand_wavefunctions=False)


V_GPGM_T = TreeInducedCTVertex(name='V_GPGM_T',
                               particles=[P.G__plus__, P.G__minus__],
                               color=['1'],
                               type='massct',
                               loop_particles='N',
                               lorentz=[LCT.SS1],
                               couplings={(0, 0): GC_GGTadpole},
                               expand_wavefunctions=False)

V_HAG0_T = TreeInducedCTVertex(name='V_HAG0_T',
                               particles=[P.Ha, P.G0],
                               color=['1'],
                               type='massct',
                               loop_particles='N',
                               lorentz=[LCT.SS1],
                               couplings={(0, 0): GC_HAG0Tadpole},
                               expand_wavefunctions=False)

V_HPGM_T = TreeInducedCTVertex(name='V_HPGM_T',
                               particles=[P.H__plus__, P.G__minus__],
                               color=['1'],
                               type='massct',
                               loop_particles='N',
                               lorentz=[LCT.SS1],
                               couplings={(0, 0): GC_HAG0Tadpole},
                               expand_wavefunctions=False)

V_HMGP_T = TreeInducedCTVertex(name='V_HMGP_T',
                               particles=[P.H__minus__, P.G__plus__],
                               color=['1'],
                               type='massct',
                               loop_particles='N',
                               lorentz=[LCT.SS1],
                               couplings={(0, 0): GC_HAG0Tadpole},
                               expand_wavefunctions=False)
