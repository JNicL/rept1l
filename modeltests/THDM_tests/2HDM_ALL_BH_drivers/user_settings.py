#==============================================================================#
#                                   model.py                                   #
#==============================================================================#

#============#
#  Includes  #
#============#

from rept1l.autoct.modelfile import model
from rept1l.autoct.modelfile import TreeInducedCTVertex, CTVertex
from rept1l.autoct.auto_parameters_ct import auto_assign_ct_particle
from rept1l.autoct.auto_parameters_ct import assign_counterterm
from rept1l.autoct.auto_parameters_ct import set_light_particle
from rept1l.autoct.auto_parameters_ct import add_counterterm
import rept1l.autoct.lorentz_ct as LCT

#===========#
#  Globals  #
#===========#

# coupling constructor
Coupling = model.object_library.Coupling

# access to instances
param = model.parameters
P = model.particles
L = model.lorentz

modelname = 'THDM'
modelgauge = "'t Hooft-Feynman/tadpole BH"

features = {'fermionloop_opt' : True,
            'qcd_rescaling' : True,
            'sm_generation_opt': True,
           }


scales = [param.muMS_BSM]

# LCT allows to access the 2-point lorentz structures defined in lorentz_ct.py

#=============================#
#  External Parameter Orders  #
#=============================#

parameter_orders = {param.aEW: {'QED': 2},
                    param.aS: {'QCD': 2},
                    }

#===============================#
#  Fermion mass regularization  #
#===============================#

for p in [P.e__minus__, P.mu__minus__, P.tau__minus__, P.u, P.d, P.c, P.s, P.b]:
  set_light_particle(p)

###################################
#  SM Optimizations for Fermions  #
###################################

doublets = [(P.nu_e, P.e__minus__), (P.nu_mu, P.mu__minus__),
            (P.nu_tau, P.tau__minus__), (P.u, P.d), (P.c, P.s), (P.t, P.b)]

generations = [(P.e__minus__, P.mu__minus__, P.tau__minus__),
               (P.d, P.s, P.b)]

##############################
#  QCD & EW Renormalization  #
##############################

assign_counterterm(param.ee, 'dZee', 'ee*dZee')
assign_counterterm(param.gs, 'dZgs', 'gs*dZgs')

##########################
#  2HDM Renormalization  #
##########################

assign_counterterm(param.cb, 'db', '-sb*db')
assign_counterterm(param.sb, 'db', 'cb*db')
assign_counterterm(param.sa, 'da', 'ca*da')
assign_counterterm(param.ca, 'da', '-sa*da')
assign_counterterm(param.MSB, 'dMSB2', 'dMSB2/(2*MSB)')

#######################
#  DS Tadpole scheme  #
#######################

add_counterterm('dtHl', 'dtHl')
add_counterterm('dtHh', 'dtHh')

##############################
#  Particle Renormalization  #
##############################

mixings = {P.A: [P.Z], P.Z: [P.A],
           P.Ha: [P.G0], P.G0: [P.Ha],
           P.Hl: [P.Hh], P.Hh: [P.Hl],
           P.H__plus__: [P.G__plus__], P.G__plus__: [P.H__plus__],
           P.H__minus__: [P.G__minus__], P.G__minus__: [P.H__minus__]}

auto_assign_ct_particle(mixings)

#########################################
#  Renormalization of the Gauge-fixing  #
#########################################

GC_MZ_GF = Coupling(name='GC_MZ_GF',
                    value='MZ',
                    order={'QED': 0})

GC_MW_GF = Coupling(name='GC_MW_GF',
                    value='I*MW',
                    order={'QED': 0})

GC_MMW_GF = Coupling(name='GC_MMW_GF',
                     value='-I*MW',
                     order={'QED': 0})

V_WPGM = TreeInducedCTVertex(name='V_WPGM',
                             particles=[P.W__plus__, P.G__minus__],
                             color=['1'],
                             type='treeinducedct',
                             loop_particles='N',
                             lorentz=[LCT.VS1],
                             couplings={(0, 0): GC_MW_GF})

V_WMGP = TreeInducedCTVertex(name='V_WMGP',
                             particles=[P.W__minus__, P.G__plus__],
                             color=['1'],
                             type='treeinducedct',
                             loop_particles='N',
                             lorentz=[LCT.VS1],
                             couplings={(0, 0): GC_MMW_GF})

V_ZG0 = TreeInducedCTVertex(name='V_ZG0',
                            particles=[P.Z, P.G0],
                            color=['1'],
                            type='treeinducedct',
                            loop_particles='N',
                            lorentz=[LCT.VS1],
                            couplings={(0, 0): GC_MZ_GF})

#================#
#  Tadpoles n=2  #
#================#

GC_HlHl = Coupling(name='GC_HlHl',
                   value='I*(ca*sa*(ca*cb + sa*sb)*dtHh + (ca**3*cb - sa**3*sb)*dtHl)/(cb*sb*vev)',
                   order={'QED': 2})

GC_HlHh = Coupling(name='GC_HlHh',
                   value='I*(ca*sa*(cb*sa*dtHh - ca*sb*dtHh + ca*cb*dtHl + sa*sb*dtHl))/(cb*sb*vev)',
                   order={'QED': 2})

GC_HhHh = Coupling(name='GC_HhHh',
                   value='I*(cb*sa**2*(sa*dtHh + ca*dtHl) + ca**2*sb*(ca*dtHh - sa*dtHl))/(cb*sb*vev)',
                   order={'QED': 2})

GC_HaHa = Coupling(name='GC_HaHa',
                   value='I*(cb**3*(sa*dtHh + ca*dtHl) + sb**3*(ca*dtHh - sa*dtHl))/(cb*sb*vev)',
                   order={'QED': 2})

GC_HaG0 = Coupling(name='GC_HaG0',
                   value='I*(cb*sa*dtHh - ca*sb*dtHh + ca*cb*dtHl + sa*sb*dtHl)/vev',
                   order={'QED': 2})

GC_GG = Coupling(name='GC_GG',
                 value='I*(ca*dtHh-sa*dtHl)/vev',
                 order={'QED': 2})

V_HlHl_T = CTVertex(name='V_HlHl_T',
                    particles=[P.Hl, P.Hl],
                    color=['1'],
                    type='massct',
                    loop_particles='N',
                    lorentz=[LCT.SS1],
                    couplings={(0, 0): GC_HlHl})

V_HlHh_T = CTVertex(name='V_HlHh_T',
                    particles=[P.Hl, P.Hh],
                    color=['1'],
                    type='massct',
                    loop_particles='N',
                    lorentz=[LCT.SS1],
                    couplings={(0, 0): GC_HlHh})

V_HhHh_T = CTVertex(name='V_HhHh_T',
                    particles=[P.Hh, P.Hh],
                    color=['1'],
                    type='massct',
                    loop_particles='N',
                    lorentz=[LCT.SS1],
                    couplings={(0, 0): GC_HhHh})

V_HaHa_T = CTVertex(name='V_HaHa_T',
                    particles=[P.Ha, P.Ha],
                    color=['1'],
                    type='massct',
                    loop_particles='N',
                    lorentz=[LCT.SS1],
                    couplings={(0, 0): GC_HaHa})

V_G0G0_T = CTVertex(name='V_G0G0_T',
                    particles=[P.G0, P.G0],
                    color=['1'],
                    type='massct',
                    loop_particles='N',
                    lorentz=[LCT.SS1],
                    couplings={(0, 0): GC_GG})

V_HaG0_T = CTVertex(name='V_HaG0_T',
                    particles=[P.Ha, P.G0],
                    color=['1'],
                    type='massct',
                    loop_particles='N',
                    lorentz=[LCT.SS1],
                    couplings={(0, 0): GC_HaG0})

V_HPHM_T = CTVertex(name='V_HPHM_T',
                    particles=[P.H__plus__, P.H__minus__],
                    color=['1'],
                    type='massct',
                    loop_particles='N',
                    lorentz=[LCT.SS1],
                    couplings={(0, 0): GC_HaHa})

V_GPGM_T = CTVertex(name='V_GPGM_T',
                    particles=[P.G__plus__, P.G__minus__],
                    color=['1'],
                    type='massct',
                    loop_particles='N',
                    lorentz=[LCT.SS1],
                    couplings={(0, 0): GC_GG})

V_HPGM_T = CTVertex(name='V_HPGM_T',
                    particles=[P.H__plus__, P.G__minus__],
                    color=['1'],
                    type='massct',
                    loop_particles='N',
                    lorentz=[LCT.SS1],
                    couplings={(0, 0): GC_HaG0})

V_HMGP_T = CTVertex(name='V_HMGP_T',
                    particles=[P.H__minus__, P.G__plus__],
                    color=['1'],
                    type='massct',
                    loop_particles='N',
                    lorentz=[LCT.SS1],
                    couplings={(0, 0): GC_HaG0})
