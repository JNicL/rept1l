# This file was automatically created by FeynRules 2.3.23
# Mathematica version: 9.0 for Linux x86 (64-bit) (November 20, 2012)
# Date: Tue 22 Aug 2017 21:01:33


from .object_library import all_couplings, Coupling

from .function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



GC_1 = Coupling(name = 'GC_1',
                value = '(ee*complex(0,1))/3.',
                order = {'QED':1})

GC_2 = Coupling(name = 'GC_2',
                value = '(-2*ee*complex(0,1))/3.',
                order = {'QED':1})

GC_3 = Coupling(name = 'GC_3',
                value = '-(ee*complex(0,1))',
                order = {'QED':1})

GC_4 = Coupling(name = 'GC_4',
                value = 'ee*complex(0,1)',
                order = {'QED':1})

GC_5 = Coupling(name = 'GC_5',
                value = '-2*ee*complex(0,1)',
                order = {'QED':1})

GC_6 = Coupling(name = 'GC_6',
                value = '2*ee*complex(0,1)',
                order = {'QED':1})

GC_7 = Coupling(name = 'GC_7',
                value = '-(ee**2*complex(0,1))',
                order = {'QED':2})

GC_8 = Coupling(name = 'GC_8',
                value = 'ee**2*complex(0,1)',
                order = {'QED':2})

GC_9 = Coupling(name = 'GC_9',
                value = '-2*ee**2*complex(0,1)',
                order = {'QED':2})

GC_10 = Coupling(name = 'GC_10',
                 value = '2*ee**2*complex(0,1)',
                 order = {'QED':2})

GC_11 = Coupling(name = 'GC_11',
                 value = '-2*gs',
                 order = {'QCD':1})

GC_12 = Coupling(name = 'GC_12',
                 value = '-gs',
                 order = {'QCD':1})

GC_13 = Coupling(name = 'GC_13',
                 value = 'complex(0,1)*gs',
                 order = {'QCD':1})

GC_14 = Coupling(name = 'GC_14',
                 value = '-(complex(0,1)*gs**2)',
                 order = {'QCD':2})

GC_15 = Coupling(name = 'GC_15',
                 value = 'complex(0,1)*gs**2',
                 order = {'QCD':2})

GC_16 = Coupling(name = 'GC_16',
                 value = '-3*ca**3*complex(0,1)*l2*sa + 3*ca**3*complex(0,1)*l3*sa + 3*ca**3*complex(0,1)*l4*sa + 3*ca**3*complex(0,1)*l5*sa + 3*ca*complex(0,1)*l1*sa**3 - 3*ca*complex(0,1)*l3*sa**3 - 3*ca*complex(0,1)*l4*sa**3 - 3*ca*complex(0,1)*l5*sa**3',
                 order = {'QED':2})

GC_17 = Coupling(name = 'GC_17',
                 value = '3*ca**3*complex(0,1)*l1*sa - 3*ca**3*complex(0,1)*l3*sa - 3*ca**3*complex(0,1)*l4*sa - 3*ca**3*complex(0,1)*l5*sa - 3*ca*complex(0,1)*l2*sa**3 + 3*ca*complex(0,1)*l3*sa**3 + 3*ca*complex(0,1)*l4*sa**3 + 3*ca*complex(0,1)*l5*sa**3',
                 order = {'QED':2})

GC_18 = Coupling(name = 'GC_18',
                 value = '-3*ca**4*complex(0,1)*l2 - 6*ca**2*complex(0,1)*l3*sa**2 - 6*ca**2*complex(0,1)*l4*sa**2 - 6*ca**2*complex(0,1)*l5*sa**2 - 3*complex(0,1)*l1*sa**4',
                 order = {'QED':2})

GC_19 = Coupling(name = 'GC_19',
                 value = '-3*ca**4*complex(0,1)*l1 - 6*ca**2*complex(0,1)*l3*sa**2 - 6*ca**2*complex(0,1)*l4*sa**2 - 6*ca**2*complex(0,1)*l5*sa**2 - 3*complex(0,1)*l2*sa**4',
                 order = {'QED':2})

GC_20 = Coupling(name = 'GC_20',
                 value = '-(ca**4*complex(0,1)*l3) - ca**4*complex(0,1)*l4 - ca**4*complex(0,1)*l5 - 3*ca**2*complex(0,1)*l1*sa**2 - 3*ca**2*complex(0,1)*l2*sa**2 + 4*ca**2*complex(0,1)*l3*sa**2 + 4*ca**2*complex(0,1)*l4*sa**2 + 4*ca**2*complex(0,1)*l5*sa**2 - complex(0,1)*l3*sa**4 - complex(0,1)*l4*sa**4 - complex(0,1)*l5*sa**4',
                 order = {'QED':2})

GC_21 = Coupling(name = 'GC_21',
                 value = '(cb*ee**2*complex(0,1)*sa)/(2.*cw) - (ca*ee**2*complex(0,1)*sb)/(2.*cw)',
                 order = {'QED':2})

GC_22 = Coupling(name = 'GC_22',
                 value = '-(cb*ee**2*complex(0,1)*sa)/(2.*cw) + (ca*ee**2*complex(0,1)*sb)/(2.*cw)',
                 order = {'QED':2})

GC_23 = Coupling(name = 'GC_23',
                 value = '-(ca*cb*ee**2*complex(0,1))/(2.*cw) - (ee**2*complex(0,1)*sa*sb)/(2.*cw)',
                 order = {'QED':2})

GC_24 = Coupling(name = 'GC_24',
                 value = '(ca*cb*ee**2*complex(0,1))/(2.*cw) + (ee**2*complex(0,1)*sa*sb)/(2.*cw)',
                 order = {'QED':2})

GC_25 = Coupling(name = 'GC_25',
                 value = '-(cb**2*ee*complex(0,1)) - ee*complex(0,1)*sb**2',
                 order = {'QED':1})

GC_26 = Coupling(name = 'GC_26',
                 value = 'cb**2*ee*complex(0,1) + ee*complex(0,1)*sb**2',
                 order = {'QED':1})

GC_27 = Coupling(name = 'GC_27',
                 value = '-(cb**2*ee**2*complex(0,1)) - ee**2*complex(0,1)*sb**2',
                 order = {'QED':2})

GC_28 = Coupling(name = 'GC_28',
                 value = '-2*cb**2*ee**2*complex(0,1) - 2*ee**2*complex(0,1)*sb**2',
                 order = {'QED':2})

GC_29 = Coupling(name = 'GC_29',
                 value = '2*cb**2*ee**2*complex(0,1) + 2*ee**2*complex(0,1)*sb**2',
                 order = {'QED':2})

GC_30 = Coupling(name = 'GC_30',
                 value = '-(cb**2*ee**2)/(2.*cw) - (ee**2*sb**2)/(2.*cw)',
                 order = {'QED':2})

GC_31 = Coupling(name = 'GC_31',
                 value = '(cb**2*ee**2)/(2.*cw) + (ee**2*sb**2)/(2.*cw)',
                 order = {'QED':2})

GC_32 = Coupling(name = 'GC_32',
                 value = '-(ca*cb**2*complex(0,1)*l2*sa) + ca*cb**2*complex(0,1)*l3*sa + ca**2*cb*complex(0,1)*l4*sb + ca**2*cb*complex(0,1)*l5*sb - cb*complex(0,1)*l4*sa**2*sb - cb*complex(0,1)*l5*sa**2*sb + ca*complex(0,1)*l1*sa*sb**2 - ca*complex(0,1)*l3*sa*sb**2',
                 order = {'QED':2})

GC_33 = Coupling(name = 'GC_33',
                 value = 'ca*cb**2*complex(0,1)*l1*sa - ca*cb**2*complex(0,1)*l3*sa - ca**2*cb*complex(0,1)*l4*sb - ca**2*cb*complex(0,1)*l5*sb + cb*complex(0,1)*l4*sa**2*sb + cb*complex(0,1)*l5*sa**2*sb - ca*complex(0,1)*l2*sa*sb**2 + ca*complex(0,1)*l3*sa*sb**2',
                 order = {'QED':2})

GC_34 = Coupling(name = 'GC_34',
                 value = 'ca*cb**2*complex(0,1)*l4*sa + ca*cb**2*complex(0,1)*l5*sa - ca**2*cb*complex(0,1)*l2*sb + ca**2*cb*complex(0,1)*l3*sb + cb*complex(0,1)*l1*sa**2*sb - cb*complex(0,1)*l3*sa**2*sb - ca*complex(0,1)*l4*sa*sb**2 - ca*complex(0,1)*l5*sa*sb**2',
                 order = {'QED':2})

GC_35 = Coupling(name = 'GC_35',
                 value = 'ca*cb**2*complex(0,1)*l1*sa - ca*cb**2*complex(0,1)*l3*sa - ca*cb**2*complex(0,1)*l4*sa + ca*cb**2*complex(0,1)*l5*sa - 2*ca**2*cb*complex(0,1)*l5*sb + 2*cb*complex(0,1)*l5*sa**2*sb - ca*complex(0,1)*l2*sa*sb**2 + ca*complex(0,1)*l3*sa*sb**2 + ca*complex(0,1)*l4*sa*sb**2 - ca*complex(0,1)*l5*sa*sb**2',
                 order = {'QED':2})

GC_36 = Coupling(name = 'GC_36',
                 value = '-(ca*cb**2*complex(0,1)*l2*sa) + ca*cb**2*complex(0,1)*l3*sa + ca*cb**2*complex(0,1)*l4*sa - ca*cb**2*complex(0,1)*l5*sa + 2*ca**2*cb*complex(0,1)*l5*sb - 2*cb*complex(0,1)*l5*sa**2*sb + ca*complex(0,1)*l1*sa*sb**2 - ca*complex(0,1)*l3*sa*sb**2 - ca*complex(0,1)*l4*sa*sb**2 + ca*complex(0,1)*l5*sa*sb**2',
                 order = {'QED':2})

GC_37 = Coupling(name = 'GC_37',
                 value = '-(ca*cb**2*complex(0,1)*l4*sa) - ca*cb**2*complex(0,1)*l5*sa + ca**2*cb*complex(0,1)*l1*sb - ca**2*cb*complex(0,1)*l3*sb - cb*complex(0,1)*l2*sa**2*sb + cb*complex(0,1)*l3*sa**2*sb + ca*complex(0,1)*l4*sa*sb**2 + ca*complex(0,1)*l5*sa*sb**2',
                 order = {'QED':2})

GC_38 = Coupling(name = 'GC_38',
                 value = '2*ca*cb**2*complex(0,1)*l5*sa - ca**2*cb*complex(0,1)*l2*sb + ca**2*cb*complex(0,1)*l3*sb + ca**2*cb*complex(0,1)*l4*sb - ca**2*cb*complex(0,1)*l5*sb + cb*complex(0,1)*l1*sa**2*sb - cb*complex(0,1)*l3*sa**2*sb - cb*complex(0,1)*l4*sa**2*sb + cb*complex(0,1)*l5*sa**2*sb - 2*ca*complex(0,1)*l5*sa*sb**2',
                 order = {'QED':2})

GC_39 = Coupling(name = 'GC_39',
                 value = '-2*ca*cb**2*complex(0,1)*l5*sa + ca**2*cb*complex(0,1)*l1*sb - ca**2*cb*complex(0,1)*l3*sb - ca**2*cb*complex(0,1)*l4*sb + ca**2*cb*complex(0,1)*l5*sb - cb*complex(0,1)*l2*sa**2*sb + cb*complex(0,1)*l3*sa**2*sb + cb*complex(0,1)*l4*sa**2*sb - cb*complex(0,1)*l5*sa**2*sb + 2*ca*complex(0,1)*l5*sa*sb**2',
                 order = {'QED':2})

GC_40 = Coupling(name = 'GC_40',
                 value = '-(ca**2*cb**2*complex(0,1)*l2) - cb**2*complex(0,1)*l3*sa**2 - 2*ca*cb*complex(0,1)*l4*sa*sb - 2*ca*cb*complex(0,1)*l5*sa*sb - ca**2*complex(0,1)*l3*sb**2 - complex(0,1)*l1*sa**2*sb**2',
                 order = {'QED':2})

GC_41 = Coupling(name = 'GC_41',
                 value = '-(ca**2*cb**2*complex(0,1)*l2) - cb**2*complex(0,1)*l3*sa**2 - cb**2*complex(0,1)*l4*sa**2 + cb**2*complex(0,1)*l5*sa**2 - 4*ca*cb*complex(0,1)*l5*sa*sb - ca**2*complex(0,1)*l3*sb**2 - ca**2*complex(0,1)*l4*sb**2 + ca**2*complex(0,1)*l5*sb**2 - complex(0,1)*l1*sa**2*sb**2',
                 order = {'QED':2})

GC_42 = Coupling(name = 'GC_42',
                 value = '-(ca**2*cb**2*complex(0,1)*l1) - cb**2*complex(0,1)*l3*sa**2 - 2*ca*cb*complex(0,1)*l4*sa*sb - 2*ca*cb*complex(0,1)*l5*sa*sb - ca**2*complex(0,1)*l3*sb**2 - complex(0,1)*l2*sa**2*sb**2',
                 order = {'QED':2})

GC_43 = Coupling(name = 'GC_43',
                 value = '-(ca**2*cb**2*complex(0,1)*l1) - cb**2*complex(0,1)*l3*sa**2 - cb**2*complex(0,1)*l4*sa**2 + cb**2*complex(0,1)*l5*sa**2 - 4*ca*cb*complex(0,1)*l5*sa*sb - ca**2*complex(0,1)*l3*sb**2 - ca**2*complex(0,1)*l4*sb**2 + ca**2*complex(0,1)*l5*sb**2 - complex(0,1)*l2*sa**2*sb**2',
                 order = {'QED':2})

GC_44 = Coupling(name = 'GC_44',
                 value = '-(ca**2*cb**2*complex(0,1)*l3) - cb**2*complex(0,1)*l2*sa**2 + 2*ca*cb*complex(0,1)*l4*sa*sb + 2*ca*cb*complex(0,1)*l5*sa*sb - ca**2*complex(0,1)*l1*sb**2 - complex(0,1)*l3*sa**2*sb**2',
                 order = {'QED':2})

GC_45 = Coupling(name = 'GC_45',
                 value = '-(ca**2*cb**2*complex(0,1)*l3) - cb**2*complex(0,1)*l1*sa**2 + 2*ca*cb*complex(0,1)*l4*sa*sb + 2*ca*cb*complex(0,1)*l5*sa*sb - ca**2*complex(0,1)*l2*sb**2 - complex(0,1)*l3*sa**2*sb**2',
                 order = {'QED':2})

GC_46 = Coupling(name = 'GC_46',
                 value = '-(ca**2*cb**2*complex(0,1)*l4)/2. - (ca**2*cb**2*complex(0,1)*l5)/2. + (cb**2*complex(0,1)*l4*sa**2)/2. + (cb**2*complex(0,1)*l5*sa**2)/2. - ca*cb*complex(0,1)*l1*sa*sb - ca*cb*complex(0,1)*l2*sa*sb + 2*ca*cb*complex(0,1)*l3*sa*sb + (ca**2*complex(0,1)*l4*sb**2)/2. + (ca**2*complex(0,1)*l5*sb**2)/2. - (complex(0,1)*l4*sa**2*sb**2)/2. - (complex(0,1)*l5*sa**2*sb**2)/2.',
                 order = {'QED':2})

GC_47 = Coupling(name = 'GC_47',
                 value = '-(ca**2*cb**2*complex(0,1)*l5) + cb**2*complex(0,1)*l5*sa**2 - ca*cb*complex(0,1)*l1*sa*sb - ca*cb*complex(0,1)*l2*sa*sb + 2*ca*cb*complex(0,1)*l3*sa*sb + 2*ca*cb*complex(0,1)*l4*sa*sb - 2*ca*cb*complex(0,1)*l5*sa*sb + ca**2*complex(0,1)*l5*sb**2 - complex(0,1)*l5*sa**2*sb**2',
                 order = {'QED':2})

GC_48 = Coupling(name = 'GC_48',
                 value = '-(ca**2*cb**2*complex(0,1)*l3) - ca**2*cb**2*complex(0,1)*l4 + ca**2*cb**2*complex(0,1)*l5 - cb**2*complex(0,1)*l2*sa**2 + 4*ca*cb*complex(0,1)*l5*sa*sb - ca**2*complex(0,1)*l1*sb**2 - complex(0,1)*l3*sa**2*sb**2 - complex(0,1)*l4*sa**2*sb**2 + complex(0,1)*l5*sa**2*sb**2',
                 order = {'QED':2})

GC_49 = Coupling(name = 'GC_49',
                 value = '-(ca**2*cb**2*complex(0,1)*l3) - ca**2*cb**2*complex(0,1)*l4 + ca**2*cb**2*complex(0,1)*l5 - cb**2*complex(0,1)*l1*sa**2 + 4*ca*cb*complex(0,1)*l5*sa*sb - ca**2*complex(0,1)*l2*sb**2 - complex(0,1)*l3*sa**2*sb**2 - complex(0,1)*l4*sa**2*sb**2 + complex(0,1)*l5*sa**2*sb**2',
                 order = {'QED':2})

GC_50 = Coupling(name = 'GC_50',
                 value = '(cb**3*ee**2*sa)/(4.*cw**2) - (ca*cb**2*ee**2*sb)/(4.*cw**2) + (cb*ee**2*sa*sb**2)/(4.*cw**2) - (ca*ee**2*sb**3)/(4.*cw**2)',
                 order = {'QED':2})

GC_51 = Coupling(name = 'GC_51',
                 value = '-(cb**3*ee**2*sa)/(4.*cw**2) + (ca*cb**2*ee**2*sb)/(4.*cw**2) - (cb*ee**2*sa*sb**2)/(4.*cw**2) + (ca*ee**2*sb**3)/(4.*cw**2)',
                 order = {'QED':2})

GC_52 = Coupling(name = 'GC_52',
                 value = '-(cb**3*l4*sa)/2. + (cb**3*l5*sa)/2. + (ca*cb**2*l4*sb)/2. - (ca*cb**2*l5*sb)/2. - (cb*l4*sa*sb**2)/2. + (cb*l5*sa*sb**2)/2. + (ca*l4*sb**3)/2. - (ca*l5*sb**3)/2.',
                 order = {'QED':2})

GC_53 = Coupling(name = 'GC_53',
                 value = '(cb**3*l4*sa)/2. - (cb**3*l5*sa)/2. - (ca*cb**2*l4*sb)/2. + (ca*cb**2*l5*sb)/2. + (cb*l4*sa*sb**2)/2. - (cb*l5*sa*sb**2)/2. - (ca*l4*sb**3)/2. + (ca*l5*sb**3)/2.',
                 order = {'QED':2})

GC_54 = Coupling(name = 'GC_54',
                 value = '-(cb**3*complex(0,1)*l2*sb) + cb**3*complex(0,1)*l3*sb + cb**3*complex(0,1)*l4*sb + cb**3*complex(0,1)*l5*sb + cb*complex(0,1)*l1*sb**3 - cb*complex(0,1)*l3*sb**3 - cb*complex(0,1)*l4*sb**3 - cb*complex(0,1)*l5*sb**3',
                 order = {'QED':2})

GC_55 = Coupling(name = 'GC_55',
                 value = 'cb**3*complex(0,1)*l1*sb - cb**3*complex(0,1)*l3*sb - cb**3*complex(0,1)*l4*sb - cb**3*complex(0,1)*l5*sb - cb*complex(0,1)*l2*sb**3 + cb*complex(0,1)*l3*sb**3 + cb*complex(0,1)*l4*sb**3 + cb*complex(0,1)*l5*sb**3',
                 order = {'QED':2})

GC_56 = Coupling(name = 'GC_56',
                 value = '-2*cb**3*complex(0,1)*l2*sb + 2*cb**3*complex(0,1)*l3*sb + 2*cb**3*complex(0,1)*l4*sb + 2*cb**3*complex(0,1)*l5*sb + 2*cb*complex(0,1)*l1*sb**3 - 2*cb*complex(0,1)*l3*sb**3 - 2*cb*complex(0,1)*l4*sb**3 - 2*cb*complex(0,1)*l5*sb**3',
                 order = {'QED':2})

GC_57 = Coupling(name = 'GC_57',
                 value = '2*cb**3*complex(0,1)*l1*sb - 2*cb**3*complex(0,1)*l3*sb - 2*cb**3*complex(0,1)*l4*sb - 2*cb**3*complex(0,1)*l5*sb - 2*cb*complex(0,1)*l2*sb**3 + 2*cb*complex(0,1)*l3*sb**3 + 2*cb*complex(0,1)*l4*sb**3 + 2*cb*complex(0,1)*l5*sb**3',
                 order = {'QED':2})

GC_58 = Coupling(name = 'GC_58',
                 value = '-3*cb**3*complex(0,1)*l2*sb + 3*cb**3*complex(0,1)*l3*sb + 3*cb**3*complex(0,1)*l4*sb + 3*cb**3*complex(0,1)*l5*sb + 3*cb*complex(0,1)*l1*sb**3 - 3*cb*complex(0,1)*l3*sb**3 - 3*cb*complex(0,1)*l4*sb**3 - 3*cb*complex(0,1)*l5*sb**3',
                 order = {'QED':2})

GC_59 = Coupling(name = 'GC_59',
                 value = '3*cb**3*complex(0,1)*l1*sb - 3*cb**3*complex(0,1)*l3*sb - 3*cb**3*complex(0,1)*l4*sb - 3*cb**3*complex(0,1)*l5*sb - 3*cb*complex(0,1)*l2*sb**3 + 3*cb*complex(0,1)*l3*sb**3 + 3*cb*complex(0,1)*l4*sb**3 + 3*cb*complex(0,1)*l5*sb**3',
                 order = {'QED':2})

GC_60 = Coupling(name = 'GC_60',
                 value = '-(ca*cb**3*ee**2)/(4.*cw**2) - (cb**2*ee**2*sa*sb)/(4.*cw**2) - (ca*cb*ee**2*sb**2)/(4.*cw**2) - (ee**2*sa*sb**3)/(4.*cw**2)',
                 order = {'QED':2})

GC_61 = Coupling(name = 'GC_61',
                 value = '(ca*cb**3*ee**2)/(4.*cw**2) + (cb**2*ee**2*sa*sb)/(4.*cw**2) + (ca*cb*ee**2*sb**2)/(4.*cw**2) + (ee**2*sa*sb**3)/(4.*cw**2)',
                 order = {'QED':2})

GC_62 = Coupling(name = 'GC_62',
                 value = '(ca*cb**3*l4)/2. - (ca*cb**3*l5)/2. + (cb**2*l4*sa*sb)/2. - (cb**2*l5*sa*sb)/2. + (ca*cb*l4*sb**2)/2. - (ca*cb*l5*sb**2)/2. + (l4*sa*sb**3)/2. - (l5*sa*sb**3)/2.',
                 order = {'QED':2})

GC_63 = Coupling(name = 'GC_63',
                 value = '-(ca*cb**3*l4)/2. + (ca*cb**3*l5)/2. - (cb**2*l4*sa*sb)/2. + (cb**2*l5*sa*sb)/2. - (ca*cb*l4*sb**2)/2. + (ca*cb*l5*sb**2)/2. - (l4*sa*sb**3)/2. + (l5*sa*sb**3)/2.',
                 order = {'QED':2})

GC_64 = Coupling(name = 'GC_64',
                 value = '-(cb**4*complex(0,1)*l2) - 2*cb**2*complex(0,1)*l3*sb**2 - 2*cb**2*complex(0,1)*l4*sb**2 - 2*cb**2*complex(0,1)*l5*sb**2 - complex(0,1)*l1*sb**4',
                 order = {'QED':2})

GC_65 = Coupling(name = 'GC_65',
                 value = '-2*cb**4*complex(0,1)*l2 - 4*cb**2*complex(0,1)*l3*sb**2 - 4*cb**2*complex(0,1)*l4*sb**2 - 4*cb**2*complex(0,1)*l5*sb**2 - 2*complex(0,1)*l1*sb**4',
                 order = {'QED':2})

GC_66 = Coupling(name = 'GC_66',
                 value = '-3*cb**4*complex(0,1)*l2 - 6*cb**2*complex(0,1)*l3*sb**2 - 6*cb**2*complex(0,1)*l4*sb**2 - 6*cb**2*complex(0,1)*l5*sb**2 - 3*complex(0,1)*l1*sb**4',
                 order = {'QED':2})

GC_67 = Coupling(name = 'GC_67',
                 value = '-(cb**4*complex(0,1)*l1) - 2*cb**2*complex(0,1)*l3*sb**2 - 2*cb**2*complex(0,1)*l4*sb**2 - 2*cb**2*complex(0,1)*l5*sb**2 - complex(0,1)*l2*sb**4',
                 order = {'QED':2})

GC_68 = Coupling(name = 'GC_68',
                 value = '-2*cb**4*complex(0,1)*l1 - 4*cb**2*complex(0,1)*l3*sb**2 - 4*cb**2*complex(0,1)*l4*sb**2 - 4*cb**2*complex(0,1)*l5*sb**2 - 2*complex(0,1)*l2*sb**4',
                 order = {'QED':2})

GC_69 = Coupling(name = 'GC_69',
                 value = '-3*cb**4*complex(0,1)*l1 - 6*cb**2*complex(0,1)*l3*sb**2 - 6*cb**2*complex(0,1)*l4*sb**2 - 6*cb**2*complex(0,1)*l5*sb**2 - 3*complex(0,1)*l2*sb**4',
                 order = {'QED':2})

GC_70 = Coupling(name = 'GC_70',
                 value = '-(cb**4*complex(0,1)*l3) - cb**2*complex(0,1)*l1*sb**2 - cb**2*complex(0,1)*l2*sb**2 + 2*cb**2*complex(0,1)*l4*sb**2 + 2*cb**2*complex(0,1)*l5*sb**2 - complex(0,1)*l3*sb**4',
                 order = {'QED':2})

GC_71 = Coupling(name = 'GC_71',
                 value = '-(cb**4*complex(0,1)*l3) - cb**4*complex(0,1)*l4 - 2*cb**2*complex(0,1)*l1*sb**2 - 2*cb**2*complex(0,1)*l2*sb**2 + 2*cb**2*complex(0,1)*l3*sb**2 + 2*cb**2*complex(0,1)*l4*sb**2 + 4*cb**2*complex(0,1)*l5*sb**2 - complex(0,1)*l3*sb**4 - complex(0,1)*l4*sb**4',
                 order = {'QED':2})

GC_72 = Coupling(name = 'GC_72',
                 value = '-(cb**4*complex(0,1)*l4)/2. - (cb**4*complex(0,1)*l5)/2. - cb**2*complex(0,1)*l1*sb**2 - cb**2*complex(0,1)*l2*sb**2 + 2*cb**2*complex(0,1)*l3*sb**2 + cb**2*complex(0,1)*l4*sb**2 + cb**2*complex(0,1)*l5*sb**2 - (complex(0,1)*l4*sb**4)/2. - (complex(0,1)*l5*sb**4)/2.',
                 order = {'QED':2})

GC_73 = Coupling(name = 'GC_73',
                 value = '-(cb**4*complex(0,1)*l3) - cb**4*complex(0,1)*l4 - cb**4*complex(0,1)*l5 - 3*cb**2*complex(0,1)*l1*sb**2 - 3*cb**2*complex(0,1)*l2*sb**2 + 4*cb**2*complex(0,1)*l3*sb**2 + 4*cb**2*complex(0,1)*l4*sb**2 + 4*cb**2*complex(0,1)*l5*sb**2 - complex(0,1)*l3*sb**4 - complex(0,1)*l4*sb**4 - complex(0,1)*l5*sb**4',
                 order = {'QED':2})

GC_74 = Coupling(name = 'GC_74',
                 value = '-2*cb**4*complex(0,1)*l5 - 2*cb**2*complex(0,1)*l1*sb**2 - 2*cb**2*complex(0,1)*l2*sb**2 + 4*cb**2*complex(0,1)*l3*sb**2 + 4*cb**2*complex(0,1)*l4*sb**2 - 2*complex(0,1)*l5*sb**4',
                 order = {'QED':2})

GC_75 = Coupling(name = 'GC_75',
                 value = '-(ca**2*ee**2*complex(0,1))/(4.*sw**2) - (ee**2*complex(0,1)*sa**2)/(4.*sw**2)',
                 order = {'QED':2})

GC_76 = Coupling(name = 'GC_76',
                 value = '-(ca**2*ee**2*complex(0,1))/(2.*sw**2) - (ee**2*complex(0,1)*sa**2)/(2.*sw**2)',
                 order = {'QED':2})

GC_77 = Coupling(name = 'GC_77',
                 value = '(ca**2*ee**2*complex(0,1))/(2.*sw**2) + (ee**2*complex(0,1)*sa**2)/(2.*sw**2)',
                 order = {'QED':2})

GC_78 = Coupling(name = 'GC_78',
                 value = '(cb*ee**2*sa)/(2.*sw**2) - (ca*ee**2*sb)/(2.*sw**2)',
                 order = {'QED':2})

GC_79 = Coupling(name = 'GC_79',
                 value = '(cb*ee**2*sa)/(4.*sw**2) - (ca*ee**2*sb)/(4.*sw**2)',
                 order = {'QED':2})

GC_80 = Coupling(name = 'GC_80',
                 value = '-(cb*ee**2*sa)/(4.*sw**2) + (ca*ee**2*sb)/(4.*sw**2)',
                 order = {'QED':2})

GC_81 = Coupling(name = 'GC_81',
                 value = '-(cb*ee**2*sa)/(2.*sw**2) + (ca*ee**2*sb)/(2.*sw**2)',
                 order = {'QED':2})

GC_82 = Coupling(name = 'GC_82',
                 value = '(cb*ee**2*complex(0,1)*sa)/(4.*cw) - (ca*ee**2*complex(0,1)*sb)/(4.*cw) + (cb*cw*ee**2*complex(0,1)*sa)/(4.*sw**2) - (ca*cw*ee**2*complex(0,1)*sb)/(4.*sw**2)',
                 order = {'QED':2})

GC_83 = Coupling(name = 'GC_83',
                 value = '-(cb*ee**2*complex(0,1)*sa)/(4.*cw) + (ca*ee**2*complex(0,1)*sb)/(4.*cw) + (cb*cw*ee**2*complex(0,1)*sa)/(4.*sw**2) - (ca*cw*ee**2*complex(0,1)*sb)/(4.*sw**2)',
                 order = {'QED':2})

GC_84 = Coupling(name = 'GC_84',
                 value = '(cb*ee**2*complex(0,1)*sa)/(4.*cw) - (ca*ee**2*complex(0,1)*sb)/(4.*cw) - (cb*cw*ee**2*complex(0,1)*sa)/(4.*sw**2) + (ca*cw*ee**2*complex(0,1)*sb)/(4.*sw**2)',
                 order = {'QED':2})

GC_85 = Coupling(name = 'GC_85',
                 value = '-(cb*ee**2*complex(0,1)*sa)/(4.*cw) + (ca*ee**2*complex(0,1)*sb)/(4.*cw) - (cb*cw*ee**2*complex(0,1)*sa)/(4.*sw**2) + (ca*cw*ee**2*complex(0,1)*sb)/(4.*sw**2)',
                 order = {'QED':2})

GC_86 = Coupling(name = 'GC_86',
                 value = '(cb*ee**2*complex(0,1)*sa)/(2.*cw) - (ca*ee**2*complex(0,1)*sb)/(2.*cw) + (cb*cw*ee**2*complex(0,1)*sa)/(2.*sw**2) - (ca*cw*ee**2*complex(0,1)*sb)/(2.*sw**2)',
                 order = {'QED':2})

GC_87 = Coupling(name = 'GC_87',
                 value = '-(cb*ee**2*complex(0,1)*sa)/(2.*cw) + (ca*ee**2*complex(0,1)*sb)/(2.*cw) + (cb*cw*ee**2*complex(0,1)*sa)/(2.*sw**2) - (ca*cw*ee**2*complex(0,1)*sb)/(2.*sw**2)',
                 order = {'QED':2})

GC_88 = Coupling(name = 'GC_88',
                 value = '(cb*ee**2*complex(0,1)*sa)/(2.*cw) - (ca*ee**2*complex(0,1)*sb)/(2.*cw) - (cb*cw*ee**2*complex(0,1)*sa)/(2.*sw**2) + (ca*cw*ee**2*complex(0,1)*sb)/(2.*sw**2)',
                 order = {'QED':2})

GC_89 = Coupling(name = 'GC_89',
                 value = '-(cb*ee**2*complex(0,1)*sa)/(2.*cw) + (ca*ee**2*complex(0,1)*sb)/(2.*cw) - (cb*cw*ee**2*complex(0,1)*sa)/(2.*sw**2) + (ca*cw*ee**2*complex(0,1)*sb)/(2.*sw**2)',
                 order = {'QED':2})

GC_90 = Coupling(name = 'GC_90',
                 value = '-(ca*cb*ee**2)/(2.*sw**2) - (ee**2*sa*sb)/(2.*sw**2)',
                 order = {'QED':2})

GC_91 = Coupling(name = 'GC_91',
                 value = '-(ca*cb*ee**2)/(4.*sw**2) - (ee**2*sa*sb)/(4.*sw**2)',
                 order = {'QED':2})

GC_92 = Coupling(name = 'GC_92',
                 value = '(ca*cb*ee**2)/(4.*sw**2) + (ee**2*sa*sb)/(4.*sw**2)',
                 order = {'QED':2})

GC_93 = Coupling(name = 'GC_93',
                 value = '(ca*cb*ee**2)/(2.*sw**2) + (ee**2*sa*sb)/(2.*sw**2)',
                 order = {'QED':2})

GC_94 = Coupling(name = 'GC_94',
                 value = '(ca*cb*ee**2*complex(0,1))/(4.*cw) + (ee**2*complex(0,1)*sa*sb)/(4.*cw) - (ca*cb*cw*ee**2*complex(0,1))/(4.*sw**2) - (cw*ee**2*complex(0,1)*sa*sb)/(4.*sw**2)',
                 order = {'QED':2})

GC_95 = Coupling(name = 'GC_95',
                 value = '(ca*cb*ee**2*complex(0,1))/(4.*cw) + (ee**2*complex(0,1)*sa*sb)/(4.*cw) + (ca*cb*cw*ee**2*complex(0,1))/(4.*sw**2) + (cw*ee**2*complex(0,1)*sa*sb)/(4.*sw**2)',
                 order = {'QED':2})

GC_96 = Coupling(name = 'GC_96',
                 value = '-(ca*cb*ee**2*complex(0,1))/(2.*cw) - (ee**2*complex(0,1)*sa*sb)/(2.*cw) - (ca*cb*cw*ee**2*complex(0,1))/(2.*sw**2) - (cw*ee**2*complex(0,1)*sa*sb)/(2.*sw**2)',
                 order = {'QED':2})

GC_97 = Coupling(name = 'GC_97',
                 value = '-(ca*cb*ee**2*complex(0,1))/(2.*cw) - (ee**2*complex(0,1)*sa*sb)/(2.*cw) + (ca*cb*cw*ee**2*complex(0,1))/(2.*sw**2) + (cw*ee**2*complex(0,1)*sa*sb)/(2.*sw**2)',
                 order = {'QED':2})

GC_98 = Coupling(name = 'GC_98',
                 value = '-(cb**2*ee**2*complex(0,1))/(4.*sw**2) - (ee**2*complex(0,1)*sb**2)/(4.*sw**2)',
                 order = {'QED':2})

GC_99 = Coupling(name = 'GC_99',
                 value = '-(cb**2*ee**2*complex(0,1))/(2.*sw**2) - (ee**2*complex(0,1)*sb**2)/(2.*sw**2)',
                 order = {'QED':2})

GC_100 = Coupling(name = 'GC_100',
                  value = '(cb**2*ee**2*complex(0,1))/(2.*sw**2) + (ee**2*complex(0,1)*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_101 = Coupling(name = 'GC_101',
                  value = '(cb**2*ee**2*complex(0,1))/sw**2 + (ee**2*complex(0,1)*sb**2)/sw**2',
                  order = {'QED':2})

GC_102 = Coupling(name = 'GC_102',
                  value = '-(ca**2*cb**2*complex(0,1)*l4)/2. - (ca**2*cb**2*complex(0,1)*l5)/2. + (cb**2*complex(0,1)*l4*sa**2)/2. + (cb**2*complex(0,1)*l5*sa**2)/2. - ca*cb*complex(0,1)*l1*sa*sb - ca*cb*complex(0,1)*l2*sa*sb + 2*ca*cb*complex(0,1)*l3*sa*sb + (ca**2*complex(0,1)*l4*sb**2)/2. + (ca**2*complex(0,1)*l5*sb**2)/2. - (complex(0,1)*l4*sa**2*sb**2)/2. - (complex(0,1)*l5*sa**2*sb**2)/2. - (cb**2*ee**2*complex(0,1)*sa**2)/(4.*sw**2) + (ca*cb*ee**2*complex(0,1)*sa*sb)/(2.*sw**2) - (ca**2*ee**2*complex(0,1)*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_103 = Coupling(name = 'GC_103',
                  value = '-(ca**2*cb**2*complex(0,1)*l5) - (cb**2*ee**2*complex(0,1)*sa**2)/(4.*cw**2) + cb**2*complex(0,1)*l5*sa**2 + (ca*cb*ee**2*complex(0,1)*sa*sb)/(2.*cw**2) - ca*cb*complex(0,1)*l1*sa*sb - ca*cb*complex(0,1)*l2*sa*sb + 2*ca*cb*complex(0,1)*l3*sa*sb + 2*ca*cb*complex(0,1)*l4*sa*sb - 2*ca*cb*complex(0,1)*l5*sa*sb - (ca**2*ee**2*complex(0,1)*sb**2)/(4.*cw**2) + ca**2*complex(0,1)*l5*sb**2 - complex(0,1)*l5*sa**2*sb**2 - (cb**2*ee**2*complex(0,1)*sa**2)/(4.*sw**2) + (ca*cb*ee**2*complex(0,1)*sa*sb)/(2.*sw**2) - (ca**2*ee**2*complex(0,1)*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_104 = Coupling(name = 'GC_104',
                  value = '-(ca**2*cb**2*complex(0,1)*l3) - cb**2*complex(0,1)*l2*sa**2 + 2*ca*cb*complex(0,1)*l4*sa*sb + 2*ca*cb*complex(0,1)*l5*sa*sb - ca**2*complex(0,1)*l1*sb**2 - complex(0,1)*l3*sa**2*sb**2 + (cb**2*ee**2*complex(0,1)*sa**2)/(4.*sw**2) - (ca*cb*ee**2*complex(0,1)*sa*sb)/(2.*sw**2) + (ca**2*ee**2*complex(0,1)*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_105 = Coupling(name = 'GC_105',
                  value = '-(ca**2*cb**2*complex(0,1)*l3) - cb**2*complex(0,1)*l1*sa**2 + 2*ca*cb*complex(0,1)*l4*sa*sb + 2*ca*cb*complex(0,1)*l5*sa*sb - ca**2*complex(0,1)*l2*sb**2 - complex(0,1)*l3*sa**2*sb**2 + (cb**2*ee**2*complex(0,1)*sa**2)/(4.*sw**2) - (ca*cb*ee**2*complex(0,1)*sa*sb)/(2.*sw**2) + (ca**2*ee**2*complex(0,1)*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_106 = Coupling(name = 'GC_106',
                  value = '-(ca**2*cb**2*complex(0,1)*l3) - ca**2*cb**2*complex(0,1)*l4 + ca**2*cb**2*complex(0,1)*l5 + (cb**2*ee**2*complex(0,1)*sa**2)/(4.*cw**2) - cb**2*complex(0,1)*l2*sa**2 - (ca*cb*ee**2*complex(0,1)*sa*sb)/(2.*cw**2) + 4*ca*cb*complex(0,1)*l5*sa*sb + (ca**2*ee**2*complex(0,1)*sb**2)/(4.*cw**2) - ca**2*complex(0,1)*l1*sb**2 - complex(0,1)*l3*sa**2*sb**2 - complex(0,1)*l4*sa**2*sb**2 + complex(0,1)*l5*sa**2*sb**2 + (cb**2*ee**2*complex(0,1)*sa**2)/(4.*sw**2) - (ca*cb*ee**2*complex(0,1)*sa*sb)/(2.*sw**2) + (ca**2*ee**2*complex(0,1)*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_107 = Coupling(name = 'GC_107',
                  value = '-(ca**2*cb**2*complex(0,1)*l3) - ca**2*cb**2*complex(0,1)*l4 + ca**2*cb**2*complex(0,1)*l5 + (cb**2*ee**2*complex(0,1)*sa**2)/(4.*cw**2) - cb**2*complex(0,1)*l1*sa**2 - (ca*cb*ee**2*complex(0,1)*sa*sb)/(2.*cw**2) + 4*ca*cb*complex(0,1)*l5*sa*sb + (ca**2*ee**2*complex(0,1)*sb**2)/(4.*cw**2) - ca**2*complex(0,1)*l2*sb**2 - complex(0,1)*l3*sa**2*sb**2 - complex(0,1)*l4*sa**2*sb**2 + complex(0,1)*l5*sa**2*sb**2 + (cb**2*ee**2*complex(0,1)*sa**2)/(4.*sw**2) - (ca*cb*ee**2*complex(0,1)*sa*sb)/(2.*sw**2) + (ca**2*ee**2*complex(0,1)*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_108 = Coupling(name = 'GC_108',
                  value = '-(ca**2*cb**2*complex(0,1)*l3) - cb**2*complex(0,1)*l2*sa**2 + 2*ca*cb*complex(0,1)*l4*sa*sb + 2*ca*cb*complex(0,1)*l5*sa*sb - ca**2*complex(0,1)*l1*sb**2 - complex(0,1)*l3*sa**2*sb**2 - (cb**2*ee**2*complex(0,1)*sa**2)/(2.*sw**2) + (ca*cb*ee**2*complex(0,1)*sa*sb)/sw**2 - (ca**2*ee**2*complex(0,1)*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_109 = Coupling(name = 'GC_109',
                  value = '-(ca**2*cb**2*complex(0,1)*l3) - cb**2*complex(0,1)*l1*sa**2 + 2*ca*cb*complex(0,1)*l4*sa*sb + 2*ca*cb*complex(0,1)*l5*sa*sb - ca**2*complex(0,1)*l2*sb**2 - complex(0,1)*l3*sa**2*sb**2 - (cb**2*ee**2*complex(0,1)*sa**2)/(2.*sw**2) + (ca*cb*ee**2*complex(0,1)*sa*sb)/sw**2 - (ca**2*ee**2*complex(0,1)*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_110 = Coupling(name = 'GC_110',
                  value = '-(ca**2*cb**2*complex(0,1)*l3) - ca**2*cb**2*complex(0,1)*l4 + ca**2*cb**2*complex(0,1)*l5 - (cb**2*ee**2*complex(0,1)*sa**2)/(2.*cw**2) - cb**2*complex(0,1)*l2*sa**2 + (ca*cb*ee**2*complex(0,1)*sa*sb)/cw**2 + 4*ca*cb*complex(0,1)*l5*sa*sb - (ca**2*ee**2*complex(0,1)*sb**2)/(2.*cw**2) - ca**2*complex(0,1)*l1*sb**2 - complex(0,1)*l3*sa**2*sb**2 - complex(0,1)*l4*sa**2*sb**2 + complex(0,1)*l5*sa**2*sb**2 - (cb**2*ee**2*complex(0,1)*sa**2)/(2.*sw**2) + (ca*cb*ee**2*complex(0,1)*sa*sb)/sw**2 - (ca**2*ee**2*complex(0,1)*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_111 = Coupling(name = 'GC_111',
                  value = '-(ca**2*cb**2*complex(0,1)*l3) - ca**2*cb**2*complex(0,1)*l4 + ca**2*cb**2*complex(0,1)*l5 - (cb**2*ee**2*complex(0,1)*sa**2)/(2.*cw**2) - cb**2*complex(0,1)*l1*sa**2 + (ca*cb*ee**2*complex(0,1)*sa*sb)/cw**2 + 4*ca*cb*complex(0,1)*l5*sa*sb - (ca**2*ee**2*complex(0,1)*sb**2)/(2.*cw**2) - ca**2*complex(0,1)*l2*sb**2 - complex(0,1)*l3*sa**2*sb**2 - complex(0,1)*l4*sa**2*sb**2 + complex(0,1)*l5*sa**2*sb**2 - (cb**2*ee**2*complex(0,1)*sa**2)/(2.*sw**2) + (ca*cb*ee**2*complex(0,1)*sa*sb)/sw**2 - (ca**2*ee**2*complex(0,1)*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_112 = Coupling(name = 'GC_112',
                  value = '-(cb**2*ee**2)/(2.*cw) - (ee**2*sb**2)/(2.*cw) - (cb**2*cw*ee**2)/(2.*sw**2) - (cw*ee**2*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_113 = Coupling(name = 'GC_113',
                  value = '(cb**2*ee**2)/(2.*cw) + (ee**2*sb**2)/(2.*cw) - (cb**2*cw*ee**2)/(2.*sw**2) - (cw*ee**2*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_114 = Coupling(name = 'GC_114',
                  value = '-(cb**2*ee**2)/(4.*cw) - (ee**2*sb**2)/(4.*cw) - (cb**2*cw*ee**2)/(4.*sw**2) - (cw*ee**2*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_115 = Coupling(name = 'GC_115',
                  value = '(cb**2*ee**2)/(4.*cw) + (ee**2*sb**2)/(4.*cw) - (cb**2*cw*ee**2)/(4.*sw**2) - (cw*ee**2*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_116 = Coupling(name = 'GC_116',
                  value = '-(cb**2*ee**2)/(4.*cw) - (ee**2*sb**2)/(4.*cw) + (cb**2*cw*ee**2)/(4.*sw**2) + (cw*ee**2*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_117 = Coupling(name = 'GC_117',
                  value = '(cb**2*ee**2)/(4.*cw) + (ee**2*sb**2)/(4.*cw) + (cb**2*cw*ee**2)/(4.*sw**2) + (cw*ee**2*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_118 = Coupling(name = 'GC_118',
                  value = '-(cb**2*ee**2)/(2.*cw) - (ee**2*sb**2)/(2.*cw) + (cb**2*cw*ee**2)/(2.*sw**2) + (cw*ee**2*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_119 = Coupling(name = 'GC_119',
                  value = '(cb**2*ee**2)/(2.*cw) + (ee**2*sb**2)/(2.*cw) + (cb**2*cw*ee**2)/(2.*sw**2) + (cw*ee**2*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_120 = Coupling(name = 'GC_120',
                  value = '-(ca*cb**2*complex(0,1)*l2*sa) + ca*cb**2*complex(0,1)*l3*sa + ca**2*cb*complex(0,1)*l4*sb + ca**2*cb*complex(0,1)*l5*sb - cb*complex(0,1)*l4*sa**2*sb - cb*complex(0,1)*l5*sa**2*sb + ca*complex(0,1)*l1*sa*sb**2 - ca*complex(0,1)*l3*sa*sb**2 + (ca*cb**2*ee**2*complex(0,1)*sa)/(4.*sw**2) - (ca**2*cb*ee**2*complex(0,1)*sb)/(4.*sw**2) + (cb*ee**2*complex(0,1)*sa**2*sb)/(4.*sw**2) - (ca*ee**2*complex(0,1)*sa*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_121 = Coupling(name = 'GC_121',
                  value = '(ca*cb**2*ee**2*complex(0,1)*sa)/(4.*cw**2) - ca*cb**2*complex(0,1)*l2*sa + ca*cb**2*complex(0,1)*l3*sa + ca*cb**2*complex(0,1)*l4*sa - ca*cb**2*complex(0,1)*l5*sa - (ca**2*cb*ee**2*complex(0,1)*sb)/(4.*cw**2) + 2*ca**2*cb*complex(0,1)*l5*sb + (cb*ee**2*complex(0,1)*sa**2*sb)/(4.*cw**2) - 2*cb*complex(0,1)*l5*sa**2*sb - (ca*ee**2*complex(0,1)*sa*sb**2)/(4.*cw**2) + ca*complex(0,1)*l1*sa*sb**2 - ca*complex(0,1)*l3*sa*sb**2 - ca*complex(0,1)*l4*sa*sb**2 + ca*complex(0,1)*l5*sa*sb**2 + (ca*cb**2*ee**2*complex(0,1)*sa)/(4.*sw**2) - (ca**2*cb*ee**2*complex(0,1)*sb)/(4.*sw**2) + (cb*ee**2*complex(0,1)*sa**2*sb)/(4.*sw**2) - (ca*ee**2*complex(0,1)*sa*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_122 = Coupling(name = 'GC_122',
                  value = '-(ca*cb**2*complex(0,1)*l4*sa) - ca*cb**2*complex(0,1)*l5*sa + ca**2*cb*complex(0,1)*l1*sb - ca**2*cb*complex(0,1)*l3*sb - cb*complex(0,1)*l2*sa**2*sb + cb*complex(0,1)*l3*sa**2*sb + ca*complex(0,1)*l4*sa*sb**2 + ca*complex(0,1)*l5*sa*sb**2 + (ca*cb**2*ee**2*complex(0,1)*sa)/(4.*sw**2) - (ca**2*cb*ee**2*complex(0,1)*sb)/(4.*sw**2) + (cb*ee**2*complex(0,1)*sa**2*sb)/(4.*sw**2) - (ca*ee**2*complex(0,1)*sa*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_123 = Coupling(name = 'GC_123',
                  value = '(ca*cb**2*ee**2*complex(0,1)*sa)/(4.*cw**2) - 2*ca*cb**2*complex(0,1)*l5*sa - (ca**2*cb*ee**2*complex(0,1)*sb)/(4.*cw**2) + ca**2*cb*complex(0,1)*l1*sb - ca**2*cb*complex(0,1)*l3*sb - ca**2*cb*complex(0,1)*l4*sb + ca**2*cb*complex(0,1)*l5*sb + (cb*ee**2*complex(0,1)*sa**2*sb)/(4.*cw**2) - cb*complex(0,1)*l2*sa**2*sb + cb*complex(0,1)*l3*sa**2*sb + cb*complex(0,1)*l4*sa**2*sb - cb*complex(0,1)*l5*sa**2*sb - (ca*ee**2*complex(0,1)*sa*sb**2)/(4.*cw**2) + 2*ca*complex(0,1)*l5*sa*sb**2 + (ca*cb**2*ee**2*complex(0,1)*sa)/(4.*sw**2) - (ca**2*cb*ee**2*complex(0,1)*sb)/(4.*sw**2) + (cb*ee**2*complex(0,1)*sa**2*sb)/(4.*sw**2) - (ca*ee**2*complex(0,1)*sa*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_124 = Coupling(name = 'GC_124',
                  value = 'ca*cb**2*complex(0,1)*l1*sa - ca*cb**2*complex(0,1)*l3*sa - ca**2*cb*complex(0,1)*l4*sb - ca**2*cb*complex(0,1)*l5*sb + cb*complex(0,1)*l4*sa**2*sb + cb*complex(0,1)*l5*sa**2*sb - ca*complex(0,1)*l2*sa*sb**2 + ca*complex(0,1)*l3*sa*sb**2 - (ca*cb**2*ee**2*complex(0,1)*sa)/(4.*sw**2) + (ca**2*cb*ee**2*complex(0,1)*sb)/(4.*sw**2) - (cb*ee**2*complex(0,1)*sa**2*sb)/(4.*sw**2) + (ca*ee**2*complex(0,1)*sa*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_125 = Coupling(name = 'GC_125',
                  value = 'ca*cb**2*complex(0,1)*l4*sa + ca*cb**2*complex(0,1)*l5*sa - ca**2*cb*complex(0,1)*l2*sb + ca**2*cb*complex(0,1)*l3*sb + cb*complex(0,1)*l1*sa**2*sb - cb*complex(0,1)*l3*sa**2*sb - ca*complex(0,1)*l4*sa*sb**2 - ca*complex(0,1)*l5*sa*sb**2 - (ca*cb**2*ee**2*complex(0,1)*sa)/(4.*sw**2) + (ca**2*cb*ee**2*complex(0,1)*sb)/(4.*sw**2) - (cb*ee**2*complex(0,1)*sa**2*sb)/(4.*sw**2) + (ca*ee**2*complex(0,1)*sa*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_126 = Coupling(name = 'GC_126',
                  value = '-(ca*cb**2*ee**2*complex(0,1)*sa)/(4.*cw**2) + ca*cb**2*complex(0,1)*l1*sa - ca*cb**2*complex(0,1)*l3*sa - ca*cb**2*complex(0,1)*l4*sa + ca*cb**2*complex(0,1)*l5*sa + (ca**2*cb*ee**2*complex(0,1)*sb)/(4.*cw**2) - 2*ca**2*cb*complex(0,1)*l5*sb - (cb*ee**2*complex(0,1)*sa**2*sb)/(4.*cw**2) + 2*cb*complex(0,1)*l5*sa**2*sb + (ca*ee**2*complex(0,1)*sa*sb**2)/(4.*cw**2) - ca*complex(0,1)*l2*sa*sb**2 + ca*complex(0,1)*l3*sa*sb**2 + ca*complex(0,1)*l4*sa*sb**2 - ca*complex(0,1)*l5*sa*sb**2 - (ca*cb**2*ee**2*complex(0,1)*sa)/(4.*sw**2) + (ca**2*cb*ee**2*complex(0,1)*sb)/(4.*sw**2) - (cb*ee**2*complex(0,1)*sa**2*sb)/(4.*sw**2) + (ca*ee**2*complex(0,1)*sa*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_127 = Coupling(name = 'GC_127',
                  value = '-(ca*cb**2*ee**2*complex(0,1)*sa)/(4.*cw**2) + 2*ca*cb**2*complex(0,1)*l5*sa + (ca**2*cb*ee**2*complex(0,1)*sb)/(4.*cw**2) - ca**2*cb*complex(0,1)*l2*sb + ca**2*cb*complex(0,1)*l3*sb + ca**2*cb*complex(0,1)*l4*sb - ca**2*cb*complex(0,1)*l5*sb - (cb*ee**2*complex(0,1)*sa**2*sb)/(4.*cw**2) + cb*complex(0,1)*l1*sa**2*sb - cb*complex(0,1)*l3*sa**2*sb - cb*complex(0,1)*l4*sa**2*sb + cb*complex(0,1)*l5*sa**2*sb + (ca*ee**2*complex(0,1)*sa*sb**2)/(4.*cw**2) - 2*ca*complex(0,1)*l5*sa*sb**2 - (ca*cb**2*ee**2*complex(0,1)*sa)/(4.*sw**2) + (ca**2*cb*ee**2*complex(0,1)*sb)/(4.*sw**2) - (cb*ee**2*complex(0,1)*sa**2*sb)/(4.*sw**2) + (ca*ee**2*complex(0,1)*sa*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_128 = Coupling(name = 'GC_128',
                  value = 'ca*cb**2*complex(0,1)*l1*sa - ca*cb**2*complex(0,1)*l3*sa - ca**2*cb*complex(0,1)*l4*sb - ca**2*cb*complex(0,1)*l5*sb + cb*complex(0,1)*l4*sa**2*sb + cb*complex(0,1)*l5*sa**2*sb - ca*complex(0,1)*l2*sa*sb**2 + ca*complex(0,1)*l3*sa*sb**2 + (ca*cb**2*ee**2*complex(0,1)*sa)/(2.*sw**2) - (ca**2*cb*ee**2*complex(0,1)*sb)/(2.*sw**2) + (cb*ee**2*complex(0,1)*sa**2*sb)/(2.*sw**2) - (ca*ee**2*complex(0,1)*sa*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_129 = Coupling(name = 'GC_129',
                  value = 'ca*cb**2*complex(0,1)*l4*sa + ca*cb**2*complex(0,1)*l5*sa - ca**2*cb*complex(0,1)*l2*sb + ca**2*cb*complex(0,1)*l3*sb + cb*complex(0,1)*l1*sa**2*sb - cb*complex(0,1)*l3*sa**2*sb - ca*complex(0,1)*l4*sa*sb**2 - ca*complex(0,1)*l5*sa*sb**2 + (ca*cb**2*ee**2*complex(0,1)*sa)/(2.*sw**2) - (ca**2*cb*ee**2*complex(0,1)*sb)/(2.*sw**2) + (cb*ee**2*complex(0,1)*sa**2*sb)/(2.*sw**2) - (ca*ee**2*complex(0,1)*sa*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_130 = Coupling(name = 'GC_130',
                  value = '(ca*cb**2*ee**2*complex(0,1)*sa)/(2.*cw**2) + ca*cb**2*complex(0,1)*l1*sa - ca*cb**2*complex(0,1)*l3*sa - ca*cb**2*complex(0,1)*l4*sa + ca*cb**2*complex(0,1)*l5*sa - (ca**2*cb*ee**2*complex(0,1)*sb)/(2.*cw**2) - 2*ca**2*cb*complex(0,1)*l5*sb + (cb*ee**2*complex(0,1)*sa**2*sb)/(2.*cw**2) + 2*cb*complex(0,1)*l5*sa**2*sb - (ca*ee**2*complex(0,1)*sa*sb**2)/(2.*cw**2) - ca*complex(0,1)*l2*sa*sb**2 + ca*complex(0,1)*l3*sa*sb**2 + ca*complex(0,1)*l4*sa*sb**2 - ca*complex(0,1)*l5*sa*sb**2 + (ca*cb**2*ee**2*complex(0,1)*sa)/(2.*sw**2) - (ca**2*cb*ee**2*complex(0,1)*sb)/(2.*sw**2) + (cb*ee**2*complex(0,1)*sa**2*sb)/(2.*sw**2) - (ca*ee**2*complex(0,1)*sa*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_131 = Coupling(name = 'GC_131',
                  value = '(ca*cb**2*ee**2*complex(0,1)*sa)/(2.*cw**2) + 2*ca*cb**2*complex(0,1)*l5*sa - (ca**2*cb*ee**2*complex(0,1)*sb)/(2.*cw**2) - ca**2*cb*complex(0,1)*l2*sb + ca**2*cb*complex(0,1)*l3*sb + ca**2*cb*complex(0,1)*l4*sb - ca**2*cb*complex(0,1)*l5*sb + (cb*ee**2*complex(0,1)*sa**2*sb)/(2.*cw**2) + cb*complex(0,1)*l1*sa**2*sb - cb*complex(0,1)*l3*sa**2*sb - cb*complex(0,1)*l4*sa**2*sb + cb*complex(0,1)*l5*sa**2*sb - (ca*ee**2*complex(0,1)*sa*sb**2)/(2.*cw**2) - 2*ca*complex(0,1)*l5*sa*sb**2 + (ca*cb**2*ee**2*complex(0,1)*sa)/(2.*sw**2) - (ca**2*cb*ee**2*complex(0,1)*sb)/(2.*sw**2) + (cb*ee**2*complex(0,1)*sa**2*sb)/(2.*sw**2) - (ca*ee**2*complex(0,1)*sa*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_132 = Coupling(name = 'GC_132',
                  value = '-(ca*cb**2*complex(0,1)*l2*sa) + ca*cb**2*complex(0,1)*l3*sa + ca**2*cb*complex(0,1)*l4*sb + ca**2*cb*complex(0,1)*l5*sb - cb*complex(0,1)*l4*sa**2*sb - cb*complex(0,1)*l5*sa**2*sb + ca*complex(0,1)*l1*sa*sb**2 - ca*complex(0,1)*l3*sa*sb**2 - (ca*cb**2*ee**2*complex(0,1)*sa)/(2.*sw**2) + (ca**2*cb*ee**2*complex(0,1)*sb)/(2.*sw**2) - (cb*ee**2*complex(0,1)*sa**2*sb)/(2.*sw**2) + (ca*ee**2*complex(0,1)*sa*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_133 = Coupling(name = 'GC_133',
                  value = '-(ca*cb**2*ee**2*complex(0,1)*sa)/(2.*cw**2) - ca*cb**2*complex(0,1)*l2*sa + ca*cb**2*complex(0,1)*l3*sa + ca*cb**2*complex(0,1)*l4*sa - ca*cb**2*complex(0,1)*l5*sa + (ca**2*cb*ee**2*complex(0,1)*sb)/(2.*cw**2) + 2*ca**2*cb*complex(0,1)*l5*sb - (cb*ee**2*complex(0,1)*sa**2*sb)/(2.*cw**2) - 2*cb*complex(0,1)*l5*sa**2*sb + (ca*ee**2*complex(0,1)*sa*sb**2)/(2.*cw**2) + ca*complex(0,1)*l1*sa*sb**2 - ca*complex(0,1)*l3*sa*sb**2 - ca*complex(0,1)*l4*sa*sb**2 + ca*complex(0,1)*l5*sa*sb**2 - (ca*cb**2*ee**2*complex(0,1)*sa)/(2.*sw**2) + (ca**2*cb*ee**2*complex(0,1)*sb)/(2.*sw**2) - (cb*ee**2*complex(0,1)*sa**2*sb)/(2.*sw**2) + (ca*ee**2*complex(0,1)*sa*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_134 = Coupling(name = 'GC_134',
                  value = '-(ca*cb**2*complex(0,1)*l4*sa) - ca*cb**2*complex(0,1)*l5*sa + ca**2*cb*complex(0,1)*l1*sb - ca**2*cb*complex(0,1)*l3*sb - cb*complex(0,1)*l2*sa**2*sb + cb*complex(0,1)*l3*sa**2*sb + ca*complex(0,1)*l4*sa*sb**2 + ca*complex(0,1)*l5*sa*sb**2 - (ca*cb**2*ee**2*complex(0,1)*sa)/(2.*sw**2) + (ca**2*cb*ee**2*complex(0,1)*sb)/(2.*sw**2) - (cb*ee**2*complex(0,1)*sa**2*sb)/(2.*sw**2) + (ca*ee**2*complex(0,1)*sa*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_135 = Coupling(name = 'GC_135',
                  value = '-(ca*cb**2*ee**2*complex(0,1)*sa)/(2.*cw**2) - 2*ca*cb**2*complex(0,1)*l5*sa + (ca**2*cb*ee**2*complex(0,1)*sb)/(2.*cw**2) + ca**2*cb*complex(0,1)*l1*sb - ca**2*cb*complex(0,1)*l3*sb - ca**2*cb*complex(0,1)*l4*sb + ca**2*cb*complex(0,1)*l5*sb - (cb*ee**2*complex(0,1)*sa**2*sb)/(2.*cw**2) - cb*complex(0,1)*l2*sa**2*sb + cb*complex(0,1)*l3*sa**2*sb + cb*complex(0,1)*l4*sa**2*sb - cb*complex(0,1)*l5*sa**2*sb + (ca*ee**2*complex(0,1)*sa*sb**2)/(2.*cw**2) + 2*ca*complex(0,1)*l5*sa*sb**2 - (ca*cb**2*ee**2*complex(0,1)*sa)/(2.*sw**2) + (ca**2*cb*ee**2*complex(0,1)*sb)/(2.*sw**2) - (cb*ee**2*complex(0,1)*sa**2*sb)/(2.*sw**2) + (ca*ee**2*complex(0,1)*sa*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_136 = Coupling(name = 'GC_136',
                  value = '-(ca**2*cb**2*complex(0,1)*l4)/2. - (ca**2*cb**2*complex(0,1)*l5)/2. + (cb**2*complex(0,1)*l4*sa**2)/2. + (cb**2*complex(0,1)*l5*sa**2)/2. - ca*cb*complex(0,1)*l1*sa*sb - ca*cb*complex(0,1)*l2*sa*sb + 2*ca*cb*complex(0,1)*l3*sa*sb + (ca**2*complex(0,1)*l4*sb**2)/2. + (ca**2*complex(0,1)*l5*sb**2)/2. - (complex(0,1)*l4*sa**2*sb**2)/2. - (complex(0,1)*l5*sa**2*sb**2)/2. - (ca**2*cb**2*ee**2*complex(0,1))/(4.*sw**2) + (cb**2*ee**2*complex(0,1)*sa**2)/(4.*sw**2) - (ca*cb*ee**2*complex(0,1)*sa*sb)/sw**2 + (ca**2*ee**2*complex(0,1)*sb**2)/(4.*sw**2) - (ee**2*complex(0,1)*sa**2*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_137 = Coupling(name = 'GC_137',
                  value = '-(ca**2*cb**2*ee**2*complex(0,1))/(4.*cw**2) - ca**2*cb**2*complex(0,1)*l5 + (cb**2*ee**2*complex(0,1)*sa**2)/(4.*cw**2) + cb**2*complex(0,1)*l5*sa**2 - (ca*cb*ee**2*complex(0,1)*sa*sb)/cw**2 - ca*cb*complex(0,1)*l1*sa*sb - ca*cb*complex(0,1)*l2*sa*sb + 2*ca*cb*complex(0,1)*l3*sa*sb + 2*ca*cb*complex(0,1)*l4*sa*sb - 2*ca*cb*complex(0,1)*l5*sa*sb + (ca**2*ee**2*complex(0,1)*sb**2)/(4.*cw**2) + ca**2*complex(0,1)*l5*sb**2 - (ee**2*complex(0,1)*sa**2*sb**2)/(4.*cw**2) - complex(0,1)*l5*sa**2*sb**2 - (ca**2*cb**2*ee**2*complex(0,1))/(4.*sw**2) + (cb**2*ee**2*complex(0,1)*sa**2)/(4.*sw**2) - (ca*cb*ee**2*complex(0,1)*sa*sb)/sw**2 + (ca**2*ee**2*complex(0,1)*sb**2)/(4.*sw**2) - (ee**2*complex(0,1)*sa**2*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_138 = Coupling(name = 'GC_138',
                  value = '-(ca**2*cb**2*complex(0,1)*l2) - cb**2*complex(0,1)*l3*sa**2 - 2*ca*cb*complex(0,1)*l4*sa*sb - 2*ca*cb*complex(0,1)*l5*sa*sb - ca**2*complex(0,1)*l3*sb**2 - complex(0,1)*l1*sa**2*sb**2 + (ca**2*cb**2*ee**2*complex(0,1))/(4.*sw**2) + (ca*cb*ee**2*complex(0,1)*sa*sb)/(2.*sw**2) + (ee**2*complex(0,1)*sa**2*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_139 = Coupling(name = 'GC_139',
                  value = '(ca**2*cb**2*ee**2*complex(0,1))/(4.*cw**2) - ca**2*cb**2*complex(0,1)*l2 - cb**2*complex(0,1)*l3*sa**2 - cb**2*complex(0,1)*l4*sa**2 + cb**2*complex(0,1)*l5*sa**2 + (ca*cb*ee**2*complex(0,1)*sa*sb)/(2.*cw**2) - 4*ca*cb*complex(0,1)*l5*sa*sb - ca**2*complex(0,1)*l3*sb**2 - ca**2*complex(0,1)*l4*sb**2 + ca**2*complex(0,1)*l5*sb**2 + (ee**2*complex(0,1)*sa**2*sb**2)/(4.*cw**2) - complex(0,1)*l1*sa**2*sb**2 + (ca**2*cb**2*ee**2*complex(0,1))/(4.*sw**2) + (ca*cb*ee**2*complex(0,1)*sa*sb)/(2.*sw**2) + (ee**2*complex(0,1)*sa**2*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_140 = Coupling(name = 'GC_140',
                  value = '-(ca**2*cb**2*complex(0,1)*l1) - cb**2*complex(0,1)*l3*sa**2 - 2*ca*cb*complex(0,1)*l4*sa*sb - 2*ca*cb*complex(0,1)*l5*sa*sb - ca**2*complex(0,1)*l3*sb**2 - complex(0,1)*l2*sa**2*sb**2 + (ca**2*cb**2*ee**2*complex(0,1))/(4.*sw**2) + (ca*cb*ee**2*complex(0,1)*sa*sb)/(2.*sw**2) + (ee**2*complex(0,1)*sa**2*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_141 = Coupling(name = 'GC_141',
                  value = '(ca**2*cb**2*ee**2*complex(0,1))/(4.*cw**2) - ca**2*cb**2*complex(0,1)*l1 - cb**2*complex(0,1)*l3*sa**2 - cb**2*complex(0,1)*l4*sa**2 + cb**2*complex(0,1)*l5*sa**2 + (ca*cb*ee**2*complex(0,1)*sa*sb)/(2.*cw**2) - 4*ca*cb*complex(0,1)*l5*sa*sb - ca**2*complex(0,1)*l3*sb**2 - ca**2*complex(0,1)*l4*sb**2 + ca**2*complex(0,1)*l5*sb**2 + (ee**2*complex(0,1)*sa**2*sb**2)/(4.*cw**2) - complex(0,1)*l2*sa**2*sb**2 + (ca**2*cb**2*ee**2*complex(0,1))/(4.*sw**2) + (ca*cb*ee**2*complex(0,1)*sa*sb)/(2.*sw**2) + (ee**2*complex(0,1)*sa**2*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_142 = Coupling(name = 'GC_142',
                  value = '-(ca**2*cb**2*complex(0,1)*l4)/2. - (ca**2*cb**2*complex(0,1)*l5)/2. + (cb**2*complex(0,1)*l4*sa**2)/2. + (cb**2*complex(0,1)*l5*sa**2)/2. - ca*cb*complex(0,1)*l1*sa*sb - ca*cb*complex(0,1)*l2*sa*sb + 2*ca*cb*complex(0,1)*l3*sa*sb + (ca**2*complex(0,1)*l4*sb**2)/2. + (ca**2*complex(0,1)*l5*sb**2)/2. - (complex(0,1)*l4*sa**2*sb**2)/2. - (complex(0,1)*l5*sa**2*sb**2)/2. + (ca**2*cb**2*ee**2*complex(0,1))/(4.*sw**2) + (ca*cb*ee**2*complex(0,1)*sa*sb)/(2.*sw**2) + (ee**2*complex(0,1)*sa**2*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_143 = Coupling(name = 'GC_143',
                  value = '(ca**2*cb**2*ee**2*complex(0,1))/(4.*cw**2) - ca**2*cb**2*complex(0,1)*l5 + cb**2*complex(0,1)*l5*sa**2 + (ca*cb*ee**2*complex(0,1)*sa*sb)/(2.*cw**2) - ca*cb*complex(0,1)*l1*sa*sb - ca*cb*complex(0,1)*l2*sa*sb + 2*ca*cb*complex(0,1)*l3*sa*sb + 2*ca*cb*complex(0,1)*l4*sa*sb - 2*ca*cb*complex(0,1)*l5*sa*sb + ca**2*complex(0,1)*l5*sb**2 + (ee**2*complex(0,1)*sa**2*sb**2)/(4.*cw**2) - complex(0,1)*l5*sa**2*sb**2 + (ca**2*cb**2*ee**2*complex(0,1))/(4.*sw**2) + (ca*cb*ee**2*complex(0,1)*sa*sb)/(2.*sw**2) + (ee**2*complex(0,1)*sa**2*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_144 = Coupling(name = 'GC_144',
                  value = '-(ca**2*cb**2*complex(0,1)*l2) - cb**2*complex(0,1)*l3*sa**2 - 2*ca*cb*complex(0,1)*l4*sa*sb - 2*ca*cb*complex(0,1)*l5*sa*sb - ca**2*complex(0,1)*l3*sb**2 - complex(0,1)*l1*sa**2*sb**2 - (ca**2*cb**2*ee**2*complex(0,1))/(2.*sw**2) - (ca*cb*ee**2*complex(0,1)*sa*sb)/sw**2 - (ee**2*complex(0,1)*sa**2*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_145 = Coupling(name = 'GC_145',
                  value = '-(ca**2*cb**2*ee**2*complex(0,1))/(2.*cw**2) - ca**2*cb**2*complex(0,1)*l2 - cb**2*complex(0,1)*l3*sa**2 - cb**2*complex(0,1)*l4*sa**2 + cb**2*complex(0,1)*l5*sa**2 - (ca*cb*ee**2*complex(0,1)*sa*sb)/cw**2 - 4*ca*cb*complex(0,1)*l5*sa*sb - ca**2*complex(0,1)*l3*sb**2 - ca**2*complex(0,1)*l4*sb**2 + ca**2*complex(0,1)*l5*sb**2 - (ee**2*complex(0,1)*sa**2*sb**2)/(2.*cw**2) - complex(0,1)*l1*sa**2*sb**2 - (ca**2*cb**2*ee**2*complex(0,1))/(2.*sw**2) - (ca*cb*ee**2*complex(0,1)*sa*sb)/sw**2 - (ee**2*complex(0,1)*sa**2*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_146 = Coupling(name = 'GC_146',
                  value = '-(ca**2*cb**2*complex(0,1)*l1) - cb**2*complex(0,1)*l3*sa**2 - 2*ca*cb*complex(0,1)*l4*sa*sb - 2*ca*cb*complex(0,1)*l5*sa*sb - ca**2*complex(0,1)*l3*sb**2 - complex(0,1)*l2*sa**2*sb**2 - (ca**2*cb**2*ee**2*complex(0,1))/(2.*sw**2) - (ca*cb*ee**2*complex(0,1)*sa*sb)/sw**2 - (ee**2*complex(0,1)*sa**2*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_147 = Coupling(name = 'GC_147',
                  value = '-(ca**2*cb**2*ee**2*complex(0,1))/(2.*cw**2) - ca**2*cb**2*complex(0,1)*l1 - cb**2*complex(0,1)*l3*sa**2 - cb**2*complex(0,1)*l4*sa**2 + cb**2*complex(0,1)*l5*sa**2 - (ca*cb*ee**2*complex(0,1)*sa*sb)/cw**2 - 4*ca*cb*complex(0,1)*l5*sa*sb - ca**2*complex(0,1)*l3*sb**2 - ca**2*complex(0,1)*l4*sb**2 + ca**2*complex(0,1)*l5*sb**2 - (ee**2*complex(0,1)*sa**2*sb**2)/(2.*cw**2) - complex(0,1)*l2*sa**2*sb**2 - (ca**2*cb**2*ee**2*complex(0,1))/(2.*sw**2) - (ca*cb*ee**2*complex(0,1)*sa*sb)/sw**2 - (ee**2*complex(0,1)*sa**2*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_148 = Coupling(name = 'GC_148',
                  value = '-(cb**3*ee**2*sa)/(4.*cw**2) + (ca*cb**2*ee**2*sb)/(4.*cw**2) - (cb*ee**2*sa*sb**2)/(4.*cw**2) + (ca*ee**2*sb**3)/(4.*cw**2) + (cb**3*ee**2*sa)/(4.*sw**2) - (ca*cb**2*ee**2*sb)/(4.*sw**2) + (cb*ee**2*sa*sb**2)/(4.*sw**2) - (ca*ee**2*sb**3)/(4.*sw**2)',
                  order = {'QED':2})

GC_149 = Coupling(name = 'GC_149',
                  value = '-(cb**3*l4*sa)/2. + (cb**3*l5*sa)/2. + (ca*cb**2*l4*sb)/2. - (ca*cb**2*l5*sb)/2. - (cb*l4*sa*sb**2)/2. + (cb*l5*sa*sb**2)/2. + (ca*l4*sb**3)/2. - (ca*l5*sb**3)/2. + (cb**3*ee**2*sa)/(4.*sw**2) - (ca*cb**2*ee**2*sb)/(4.*sw**2) + (cb*ee**2*sa*sb**2)/(4.*sw**2) - (ca*ee**2*sb**3)/(4.*sw**2)',
                  order = {'QED':2})

GC_150 = Coupling(name = 'GC_150',
                  value = '(cb**3*l4*sa)/2. - (cb**3*l5*sa)/2. - (ca*cb**2*l4*sb)/2. + (ca*cb**2*l5*sb)/2. + (cb*l4*sa*sb**2)/2. - (cb*l5*sa*sb**2)/2. - (ca*l4*sb**3)/2. + (ca*l5*sb**3)/2. + (cb**3*ee**2*sa)/(4.*sw**2) - (ca*cb**2*ee**2*sb)/(4.*sw**2) + (cb*ee**2*sa*sb**2)/(4.*sw**2) - (ca*ee**2*sb**3)/(4.*sw**2)',
                  order = {'QED':2})

GC_151 = Coupling(name = 'GC_151',
                  value = '(cb**3*ee**2*sa)/(4.*cw**2) - (ca*cb**2*ee**2*sb)/(4.*cw**2) + (cb*ee**2*sa*sb**2)/(4.*cw**2) - (ca*ee**2*sb**3)/(4.*cw**2) - (cb**3*ee**2*sa)/(4.*sw**2) + (ca*cb**2*ee**2*sb)/(4.*sw**2) - (cb*ee**2*sa*sb**2)/(4.*sw**2) + (ca*ee**2*sb**3)/(4.*sw**2)',
                  order = {'QED':2})

GC_152 = Coupling(name = 'GC_152',
                  value = '-(cb**3*l4*sa)/2. + (cb**3*l5*sa)/2. + (ca*cb**2*l4*sb)/2. - (ca*cb**2*l5*sb)/2. - (cb*l4*sa*sb**2)/2. + (cb*l5*sa*sb**2)/2. + (ca*l4*sb**3)/2. - (ca*l5*sb**3)/2. - (cb**3*ee**2*sa)/(4.*sw**2) + (ca*cb**2*ee**2*sb)/(4.*sw**2) - (cb*ee**2*sa*sb**2)/(4.*sw**2) + (ca*ee**2*sb**3)/(4.*sw**2)',
                  order = {'QED':2})

GC_153 = Coupling(name = 'GC_153',
                  value = '(cb**3*l4*sa)/2. - (cb**3*l5*sa)/2. - (ca*cb**2*l4*sb)/2. + (ca*cb**2*l5*sb)/2. + (cb*l4*sa*sb**2)/2. - (cb*l5*sa*sb**2)/2. - (ca*l4*sb**3)/2. + (ca*l5*sb**3)/2. - (cb**3*ee**2*sa)/(4.*sw**2) + (ca*cb**2*ee**2*sb)/(4.*sw**2) - (cb*ee**2*sa*sb**2)/(4.*sw**2) + (ca*ee**2*sb**3)/(4.*sw**2)',
                  order = {'QED':2})

GC_154 = Coupling(name = 'GC_154',
                  value = '(ca*cb**3*ee**2)/(4.*cw**2) + (cb**2*ee**2*sa*sb)/(4.*cw**2) + (ca*cb*ee**2*sb**2)/(4.*cw**2) + (ee**2*sa*sb**3)/(4.*cw**2) - (ca*cb**3*ee**2)/(4.*sw**2) - (cb**2*ee**2*sa*sb)/(4.*sw**2) - (ca*cb*ee**2*sb**2)/(4.*sw**2) - (ee**2*sa*sb**3)/(4.*sw**2)',
                  order = {'QED':2})

GC_155 = Coupling(name = 'GC_155',
                  value = '(ca*cb**3*l4)/2. - (ca*cb**3*l5)/2. + (cb**2*l4*sa*sb)/2. - (cb**2*l5*sa*sb)/2. + (ca*cb*l4*sb**2)/2. - (ca*cb*l5*sb**2)/2. + (l4*sa*sb**3)/2. - (l5*sa*sb**3)/2. - (ca*cb**3*ee**2)/(4.*sw**2) - (cb**2*ee**2*sa*sb)/(4.*sw**2) - (ca*cb*ee**2*sb**2)/(4.*sw**2) - (ee**2*sa*sb**3)/(4.*sw**2)',
                  order = {'QED':2})

GC_156 = Coupling(name = 'GC_156',
                  value = '-(ca*cb**3*l4)/2. + (ca*cb**3*l5)/2. - (cb**2*l4*sa*sb)/2. + (cb**2*l5*sa*sb)/2. - (ca*cb*l4*sb**2)/2. + (ca*cb*l5*sb**2)/2. - (l4*sa*sb**3)/2. + (l5*sa*sb**3)/2. - (ca*cb**3*ee**2)/(4.*sw**2) - (cb**2*ee**2*sa*sb)/(4.*sw**2) - (ca*cb*ee**2*sb**2)/(4.*sw**2) - (ee**2*sa*sb**3)/(4.*sw**2)',
                  order = {'QED':2})

GC_157 = Coupling(name = 'GC_157',
                  value = '-(ca*cb**3*ee**2)/(4.*cw**2) - (cb**2*ee**2*sa*sb)/(4.*cw**2) - (ca*cb*ee**2*sb**2)/(4.*cw**2) - (ee**2*sa*sb**3)/(4.*cw**2) + (ca*cb**3*ee**2)/(4.*sw**2) + (cb**2*ee**2*sa*sb)/(4.*sw**2) + (ca*cb*ee**2*sb**2)/(4.*sw**2) + (ee**2*sa*sb**3)/(4.*sw**2)',
                  order = {'QED':2})

GC_158 = Coupling(name = 'GC_158',
                  value = '(ca*cb**3*l4)/2. - (ca*cb**3*l5)/2. + (cb**2*l4*sa*sb)/2. - (cb**2*l5*sa*sb)/2. + (ca*cb*l4*sb**2)/2. - (ca*cb*l5*sb**2)/2. + (l4*sa*sb**3)/2. - (l5*sa*sb**3)/2. + (ca*cb**3*ee**2)/(4.*sw**2) + (cb**2*ee**2*sa*sb)/(4.*sw**2) + (ca*cb*ee**2*sb**2)/(4.*sw**2) + (ee**2*sa*sb**3)/(4.*sw**2)',
                  order = {'QED':2})

GC_159 = Coupling(name = 'GC_159',
                  value = '-(ca*cb**3*l4)/2. + (ca*cb**3*l5)/2. - (cb**2*l4*sa*sb)/2. + (cb**2*l5*sa*sb)/2. - (ca*cb*l4*sb**2)/2. + (ca*cb*l5*sb**2)/2. - (l4*sa*sb**3)/2. + (l5*sa*sb**3)/2. + (ca*cb**3*ee**2)/(4.*sw**2) + (cb**2*ee**2*sa*sb)/(4.*sw**2) + (ca*cb*ee**2*sb**2)/(4.*sw**2) + (ee**2*sa*sb**3)/(4.*sw**2)',
                  order = {'QED':2})

GC_160 = Coupling(name = 'GC_160',
                  value = '-(cb**4*ee**2*complex(0,1))/(4.*cw**2) - 2*cb**4*complex(0,1)*l2 - (cb**2*ee**2*complex(0,1)*sb**2)/(2.*cw**2) - 4*cb**2*complex(0,1)*l3*sb**2 - 4*cb**2*complex(0,1)*l4*sb**2 - 4*cb**2*complex(0,1)*l5*sb**2 - (ee**2*complex(0,1)*sb**4)/(4.*cw**2) - 2*complex(0,1)*l1*sb**4 - (cb**4*ee**2*complex(0,1))/(4.*sw**2) - (cb**2*ee**2*complex(0,1)*sb**2)/(2.*sw**2) - (ee**2*complex(0,1)*sb**4)/(4.*sw**2)',
                  order = {'QED':2})

GC_161 = Coupling(name = 'GC_161',
                  value = '-(cb**4*ee**2*complex(0,1))/(4.*cw**2) - 2*cb**4*complex(0,1)*l1 - (cb**2*ee**2*complex(0,1)*sb**2)/(2.*cw**2) - 4*cb**2*complex(0,1)*l3*sb**2 - 4*cb**2*complex(0,1)*l4*sb**2 - 4*cb**2*complex(0,1)*l5*sb**2 - (ee**2*complex(0,1)*sb**4)/(4.*cw**2) - 2*complex(0,1)*l2*sb**4 - (cb**4*ee**2*complex(0,1))/(4.*sw**2) - (cb**2*ee**2*complex(0,1)*sb**2)/(2.*sw**2) - (ee**2*complex(0,1)*sb**4)/(4.*sw**2)',
                  order = {'QED':2})

GC_162 = Coupling(name = 'GC_162',
                  value = '-(cb**4*ee**2*complex(0,1))/(4.*cw**2) - cb**4*complex(0,1)*l3 - cb**4*complex(0,1)*l4 - (cb**2*ee**2*complex(0,1)*sb**2)/(2.*cw**2) - 2*cb**2*complex(0,1)*l1*sb**2 - 2*cb**2*complex(0,1)*l2*sb**2 + 2*cb**2*complex(0,1)*l3*sb**2 + 2*cb**2*complex(0,1)*l4*sb**2 + 4*cb**2*complex(0,1)*l5*sb**2 - (ee**2*complex(0,1)*sb**4)/(4.*cw**2) - complex(0,1)*l3*sb**4 - complex(0,1)*l4*sb**4 - (cb**4*ee**2*complex(0,1))/(4.*sw**2) - (cb**2*ee**2*complex(0,1)*sb**2)/(2.*sw**2) - (ee**2*complex(0,1)*sb**4)/(4.*sw**2)',
                  order = {'QED':2})

GC_163 = Coupling(name = 'GC_163',
                  value = '-(cb**4*complex(0,1)*l4)/2. - (cb**4*complex(0,1)*l5)/2. - cb**2*complex(0,1)*l1*sb**2 - cb**2*complex(0,1)*l2*sb**2 + 2*cb**2*complex(0,1)*l3*sb**2 + cb**2*complex(0,1)*l4*sb**2 + cb**2*complex(0,1)*l5*sb**2 - (complex(0,1)*l4*sb**4)/2. - (complex(0,1)*l5*sb**4)/2. - (cb**4*ee**2*complex(0,1))/(4.*sw**2) - (cb**2*ee**2*complex(0,1)*sb**2)/(2.*sw**2) - (ee**2*complex(0,1)*sb**4)/(4.*sw**2)',
                  order = {'QED':2})

GC_164 = Coupling(name = 'GC_164',
                  value = '-(cb**4*complex(0,1)*l2) - 2*cb**2*complex(0,1)*l3*sb**2 - 2*cb**2*complex(0,1)*l4*sb**2 - 2*cb**2*complex(0,1)*l5*sb**2 - complex(0,1)*l1*sb**4 + (cb**4*ee**2*complex(0,1))/(4.*sw**2) + (cb**2*ee**2*complex(0,1)*sb**2)/(2.*sw**2) + (ee**2*complex(0,1)*sb**4)/(4.*sw**2)',
                  order = {'QED':2})

GC_165 = Coupling(name = 'GC_165',
                  value = '-(cb**4*complex(0,1)*l1) - 2*cb**2*complex(0,1)*l3*sb**2 - 2*cb**2*complex(0,1)*l4*sb**2 - 2*cb**2*complex(0,1)*l5*sb**2 - complex(0,1)*l2*sb**4 + (cb**4*ee**2*complex(0,1))/(4.*sw**2) + (cb**2*ee**2*complex(0,1)*sb**2)/(2.*sw**2) + (ee**2*complex(0,1)*sb**4)/(4.*sw**2)',
                  order = {'QED':2})

GC_166 = Coupling(name = 'GC_166',
                  value = '(cb**4*ee**2*complex(0,1))/(4.*cw**2) - cb**4*complex(0,1)*l3 - cb**4*complex(0,1)*l4 + (cb**2*ee**2*complex(0,1)*sb**2)/(2.*cw**2) - 2*cb**2*complex(0,1)*l1*sb**2 - 2*cb**2*complex(0,1)*l2*sb**2 + 2*cb**2*complex(0,1)*l3*sb**2 + 2*cb**2*complex(0,1)*l4*sb**2 + 4*cb**2*complex(0,1)*l5*sb**2 + (ee**2*complex(0,1)*sb**4)/(4.*cw**2) - complex(0,1)*l3*sb**4 - complex(0,1)*l4*sb**4 + (cb**4*ee**2*complex(0,1))/(4.*sw**2) + (cb**2*ee**2*complex(0,1)*sb**2)/(2.*sw**2) + (ee**2*complex(0,1)*sb**4)/(4.*sw**2)',
                  order = {'QED':2})

GC_167 = Coupling(name = 'GC_167',
                  value = '-(cb**4*complex(0,1)*l4)/2. - (cb**4*complex(0,1)*l5)/2. - cb**2*complex(0,1)*l1*sb**2 - cb**2*complex(0,1)*l2*sb**2 + 2*cb**2*complex(0,1)*l3*sb**2 + cb**2*complex(0,1)*l4*sb**2 + cb**2*complex(0,1)*l5*sb**2 - (complex(0,1)*l4*sb**4)/2. - (complex(0,1)*l5*sb**4)/2. + (cb**4*ee**2*complex(0,1))/(4.*sw**2) + (cb**2*ee**2*complex(0,1)*sb**2)/(2.*sw**2) + (ee**2*complex(0,1)*sb**4)/(4.*sw**2)',
                  order = {'QED':2})

GC_168 = Coupling(name = 'GC_168',
                  value = '-(cb**4*complex(0,1)*l2) - 2*cb**2*complex(0,1)*l3*sb**2 - 2*cb**2*complex(0,1)*l4*sb**2 - 2*cb**2*complex(0,1)*l5*sb**2 - complex(0,1)*l1*sb**4 - (cb**4*ee**2*complex(0,1))/(2.*sw**2) - (cb**2*ee**2*complex(0,1)*sb**2)/sw**2 - (ee**2*complex(0,1)*sb**4)/(2.*sw**2)',
                  order = {'QED':2})

GC_169 = Coupling(name = 'GC_169',
                  value = '-(cb**4*complex(0,1)*l1) - 2*cb**2*complex(0,1)*l3*sb**2 - 2*cb**2*complex(0,1)*l4*sb**2 - 2*cb**2*complex(0,1)*l5*sb**2 - complex(0,1)*l2*sb**4 - (cb**4*ee**2*complex(0,1))/(2.*sw**2) - (cb**2*ee**2*complex(0,1)*sb**2)/sw**2 - (ee**2*complex(0,1)*sb**4)/(2.*sw**2)',
                  order = {'QED':2})

GC_170 = Coupling(name = 'GC_170',
                  value = '(cb**4*ee**2*complex(0,1))/(2.*cw**2) - 2*cb**4*complex(0,1)*l2 + (cb**2*ee**2*complex(0,1)*sb**2)/cw**2 - 4*cb**2*complex(0,1)*l3*sb**2 - 4*cb**2*complex(0,1)*l4*sb**2 - 4*cb**2*complex(0,1)*l5*sb**2 + (ee**2*complex(0,1)*sb**4)/(2.*cw**2) - 2*complex(0,1)*l1*sb**4 + (cb**4*ee**2*complex(0,1))/(2.*sw**2) + (cb**2*ee**2*complex(0,1)*sb**2)/sw**2 + (ee**2*complex(0,1)*sb**4)/(2.*sw**2)',
                  order = {'QED':2})

GC_171 = Coupling(name = 'GC_171',
                  value = '(cb**4*ee**2*complex(0,1))/(2.*cw**2) - 2*cb**4*complex(0,1)*l1 + (cb**2*ee**2*complex(0,1)*sb**2)/cw**2 - 4*cb**2*complex(0,1)*l3*sb**2 - 4*cb**2*complex(0,1)*l4*sb**2 - 4*cb**2*complex(0,1)*l5*sb**2 + (ee**2*complex(0,1)*sb**4)/(2.*cw**2) - 2*complex(0,1)*l2*sb**4 + (cb**4*ee**2*complex(0,1))/(2.*sw**2) + (cb**2*ee**2*complex(0,1)*sb**2)/sw**2 + (ee**2*complex(0,1)*sb**4)/(2.*sw**2)',
                  order = {'QED':2})

GC_172 = Coupling(name = 'GC_172',
                  value = '(cb*ee*complex(0,1)*sa)/(2.*sw) - (ca*ee*complex(0,1)*sb)/(2.*sw)',
                  order = {'QED':1})

GC_173 = Coupling(name = 'GC_173',
                  value = '-(cb*ee*complex(0,1)*sa)/(2.*sw) + (ca*ee*complex(0,1)*sb)/(2.*sw)',
                  order = {'QED':1})

GC_174 = Coupling(name = 'GC_174',
                  value = '(cb*ee**2*complex(0,1)*sa)/(2.*sw) - (ca*ee**2*complex(0,1)*sb)/(2.*sw)',
                  order = {'QED':2})

GC_175 = Coupling(name = 'GC_175',
                  value = '-(cb*ee**2*complex(0,1)*sa)/(2.*sw) + (ca*ee**2*complex(0,1)*sb)/(2.*sw)',
                  order = {'QED':2})

GC_176 = Coupling(name = 'GC_176',
                  value = '(cb*ee**2*complex(0,1)*sa)/sw - (ca*ee**2*complex(0,1)*sb)/sw',
                  order = {'QED':2})

GC_177 = Coupling(name = 'GC_177',
                  value = '-((cb*ee**2*complex(0,1)*sa)/sw) + (ca*ee**2*complex(0,1)*sb)/sw',
                  order = {'QED':2})

GC_178 = Coupling(name = 'GC_178',
                  value = '-(ca*cb*ee*complex(0,1))/(2.*sw) - (ee*complex(0,1)*sa*sb)/(2.*sw)',
                  order = {'QED':1})

GC_179 = Coupling(name = 'GC_179',
                  value = '(ca*cb*ee*complex(0,1))/(2.*sw) + (ee*complex(0,1)*sa*sb)/(2.*sw)',
                  order = {'QED':1})

GC_180 = Coupling(name = 'GC_180',
                  value = '-(ca*cb*ee**2*complex(0,1))/(2.*sw) - (ee**2*complex(0,1)*sa*sb)/(2.*sw)',
                  order = {'QED':2})

GC_181 = Coupling(name = 'GC_181',
                  value = '(ca*cb*ee**2*complex(0,1))/(2.*sw) + (ee**2*complex(0,1)*sa*sb)/(2.*sw)',
                  order = {'QED':2})

GC_182 = Coupling(name = 'GC_182',
                  value = '-((ca*cb*ee**2*complex(0,1))/sw) - (ee**2*complex(0,1)*sa*sb)/sw',
                  order = {'QED':2})

GC_183 = Coupling(name = 'GC_183',
                  value = '-(cb**2*ee)/(2.*sw) - (ee*sb**2)/(2.*sw)',
                  order = {'QED':1})

GC_184 = Coupling(name = 'GC_184',
                  value = '(cb**2*ee)/(2.*sw) + (ee*sb**2)/(2.*sw)',
                  order = {'QED':1})

GC_185 = Coupling(name = 'GC_185',
                  value = '-((cb**2*ee**2)/sw) - (ee**2*sb**2)/sw',
                  order = {'QED':2})

GC_186 = Coupling(name = 'GC_186',
                  value = '-(cb**2*ee**2)/(2.*sw) - (ee**2*sb**2)/(2.*sw)',
                  order = {'QED':2})

GC_187 = Coupling(name = 'GC_187',
                  value = '(cb**2*ee**2)/(2.*sw) + (ee**2*sb**2)/(2.*sw)',
                  order = {'QED':2})

GC_188 = Coupling(name = 'GC_188',
                  value = '(cb**2*ee**2)/sw + (ee**2*sb**2)/sw',
                  order = {'QED':2})

GC_189 = Coupling(name = 'GC_189',
                  value = '-((ee**2*complex(0,1))/sw**2)',
                  order = {'QED':2})

GC_190 = Coupling(name = 'GC_190',
                  value = '(ee**2*complex(0,1))/sw**2',
                  order = {'QED':2})

GC_191 = Coupling(name = 'GC_191',
                  value = '(-2*ee**2*complex(0,1))/sw**2',
                  order = {'QED':2})

GC_192 = Coupling(name = 'GC_192',
                  value = '(2*ee**2*complex(0,1))/sw**2',
                  order = {'QED':2})

GC_193 = Coupling(name = 'GC_193',
                  value = '-((cw**2*ee**2*complex(0,1))/sw**2)',
                  order = {'QED':2})

GC_194 = Coupling(name = 'GC_194',
                  value = '(cw**2*ee**2*complex(0,1))/sw**2',
                  order = {'QED':2})

GC_195 = Coupling(name = 'GC_195',
                  value = '(-2*cw**2*ee**2*complex(0,1))/sw**2',
                  order = {'QED':2})

GC_196 = Coupling(name = 'GC_196',
                  value = '(2*cw**2*ee**2*complex(0,1))/sw**2',
                  order = {'QED':2})

GC_197 = Coupling(name = 'GC_197',
                  value = '(ee*complex(0,1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_198 = Coupling(name = 'GC_198',
                  value = '-((cw*ee*complex(0,1))/sw)',
                  order = {'QED':1})

GC_199 = Coupling(name = 'GC_199',
                  value = '(cw*ee*complex(0,1))/sw',
                  order = {'QED':1})

GC_200 = Coupling(name = 'GC_200',
                  value = '(-2*cw*ee*complex(0,1))/sw',
                  order = {'QED':1})

GC_201 = Coupling(name = 'GC_201',
                  value = '(2*cw*ee*complex(0,1))/sw',
                  order = {'QED':1})

GC_202 = Coupling(name = 'GC_202',
                  value = '-((cw*ee**2*complex(0,1))/sw)',
                  order = {'QED':2})

GC_203 = Coupling(name = 'GC_203',
                  value = '(cw*ee**2*complex(0,1))/sw',
                  order = {'QED':2})

GC_204 = Coupling(name = 'GC_204',
                  value = '(-2*cw*ee**2*complex(0,1))/sw',
                  order = {'QED':2})

GC_205 = Coupling(name = 'GC_205',
                  value = '(2*cw*ee**2*complex(0,1))/sw',
                  order = {'QED':2})

GC_206 = Coupling(name = 'GC_206',
                  value = '(ee*complex(0,1)*sw)/(3.*cw)',
                  order = {'QED':1})

GC_207 = Coupling(name = 'GC_207',
                  value = '(-2*ee*complex(0,1)*sw)/(3.*cw)',
                  order = {'QED':1})

GC_208 = Coupling(name = 'GC_208',
                  value = '(ee*complex(0,1)*sw)/cw',
                  order = {'QED':1})

GC_209 = Coupling(name = 'GC_209',
                  value = '-(cw*ee*complex(0,1))/(2.*sw) - (ee*complex(0,1)*sw)/(6.*cw)',
                  order = {'QED':1})

GC_210 = Coupling(name = 'GC_210',
                  value = '(cw*ee*complex(0,1))/(2.*sw) - (ee*complex(0,1)*sw)/(6.*cw)',
                  order = {'QED':1})

GC_211 = Coupling(name = 'GC_211',
                  value = '-(cw*ee*complex(0,1))/(2.*sw) + (ee*complex(0,1)*sw)/(2.*cw)',
                  order = {'QED':1})

GC_212 = Coupling(name = 'GC_212',
                  value = '(cw*ee*complex(0,1))/(2.*sw) + (ee*complex(0,1)*sw)/(2.*cw)',
                  order = {'QED':1})

GC_213 = Coupling(name = 'GC_213',
                  value = '(cb*cw*ee*sa)/(2.*sw) - (ca*cw*ee*sb)/(2.*sw) + (cb*ee*sa*sw)/(2.*cw) - (ca*ee*sb*sw)/(2.*cw)',
                  order = {'QED':1})

GC_214 = Coupling(name = 'GC_214',
                  value = '-(cb*cw*ee*sa)/(2.*sw) + (ca*cw*ee*sb)/(2.*sw) - (cb*ee*sa*sw)/(2.*cw) + (ca*ee*sb*sw)/(2.*cw)',
                  order = {'QED':1})

GC_215 = Coupling(name = 'GC_215',
                  value = '-(ca*cb*cw*ee)/(2.*sw) - (cw*ee*sa*sb)/(2.*sw) - (ca*cb*ee*sw)/(2.*cw) - (ee*sa*sb*sw)/(2.*cw)',
                  order = {'QED':1})

GC_216 = Coupling(name = 'GC_216',
                  value = '(ca*cb*cw*ee)/(2.*sw) + (cw*ee*sa*sb)/(2.*sw) + (ca*cb*ee*sw)/(2.*cw) + (ee*sa*sb*sw)/(2.*cw)',
                  order = {'QED':1})

GC_217 = Coupling(name = 'GC_217',
                  value = '(cb**2*cw*ee*complex(0,1))/(2.*sw) + (cw*ee*complex(0,1)*sb**2)/(2.*sw) - (cb**2*ee*complex(0,1)*sw)/(2.*cw) - (ee*complex(0,1)*sb**2*sw)/(2.*cw)',
                  order = {'QED':1})

GC_218 = Coupling(name = 'GC_218',
                  value = '-(cb**2*cw*ee*complex(0,1))/(2.*sw) - (cw*ee*complex(0,1)*sb**2)/(2.*sw) + (cb**2*ee*complex(0,1)*sw)/(2.*cw) + (ee*complex(0,1)*sb**2*sw)/(2.*cw)',
                  order = {'QED':1})

GC_219 = Coupling(name = 'GC_219',
                  value = '(cb**2*cw*ee**2*complex(0,1))/(2.*sw) + (cw*ee**2*complex(0,1)*sb**2)/(2.*sw) - (cb**2*ee**2*complex(0,1)*sw)/(2.*cw) - (ee**2*complex(0,1)*sb**2*sw)/(2.*cw)',
                  order = {'QED':2})

GC_220 = Coupling(name = 'GC_220',
                  value = '(cb**2*cw*ee**2*complex(0,1))/sw + (cw*ee**2*complex(0,1)*sb**2)/sw - (cb**2*ee**2*complex(0,1)*sw)/cw - (ee**2*complex(0,1)*sb**2*sw)/cw',
                  order = {'QED':2})

GC_221 = Coupling(name = 'GC_221',
                  value = '-((cb**2*cw*ee**2*complex(0,1))/sw) - (cw*ee**2*complex(0,1)*sb**2)/sw + (cb**2*ee**2*complex(0,1)*sw)/cw + (ee**2*complex(0,1)*sb**2*sw)/cw',
                  order = {'QED':2})

GC_222 = Coupling(name = 'GC_222',
                  value = '-(ca**2*ee**2*complex(0,1))/2. - (ee**2*complex(0,1)*sa**2)/2. - (ca**2*cw**2*ee**2*complex(0,1))/(4.*sw**2) - (cw**2*ee**2*complex(0,1)*sa**2)/(4.*sw**2) - (ca**2*ee**2*complex(0,1)*sw**2)/(4.*cw**2) - (ee**2*complex(0,1)*sa**2*sw**2)/(4.*cw**2)',
                  order = {'QED':2})

GC_223 = Coupling(name = 'GC_223',
                  value = '-(ca**2*ee**2*complex(0,1)) - ee**2*complex(0,1)*sa**2 - (ca**2*cw**2*ee**2*complex(0,1))/(2.*sw**2) - (cw**2*ee**2*complex(0,1)*sa**2)/(2.*sw**2) - (ca**2*ee**2*complex(0,1)*sw**2)/(2.*cw**2) - (ee**2*complex(0,1)*sa**2*sw**2)/(2.*cw**2)',
                  order = {'QED':2})

GC_224 = Coupling(name = 'GC_224',
                  value = 'ca**2*ee**2*complex(0,1) + ee**2*complex(0,1)*sa**2 + (ca**2*cw**2*ee**2*complex(0,1))/(2.*sw**2) + (cw**2*ee**2*complex(0,1)*sa**2)/(2.*sw**2) + (ca**2*ee**2*complex(0,1)*sw**2)/(2.*cw**2) + (ee**2*complex(0,1)*sa**2*sw**2)/(2.*cw**2)',
                  order = {'QED':2})

GC_225 = Coupling(name = 'GC_225',
                  value = '-(cb**2*ee**2*complex(0,1))/2. - (ee**2*complex(0,1)*sb**2)/2. - (cb**2*cw**2*ee**2*complex(0,1))/(4.*sw**2) - (cw**2*ee**2*complex(0,1)*sb**2)/(4.*sw**2) - (cb**2*ee**2*complex(0,1)*sw**2)/(4.*cw**2) - (ee**2*complex(0,1)*sb**2*sw**2)/(4.*cw**2)',
                  order = {'QED':2})

GC_226 = Coupling(name = 'GC_226',
                  value = '(cb**2*ee**2*complex(0,1))/2. + (ee**2*complex(0,1)*sb**2)/2. - (cb**2*cw**2*ee**2*complex(0,1))/(4.*sw**2) - (cw**2*ee**2*complex(0,1)*sb**2)/(4.*sw**2) - (cb**2*ee**2*complex(0,1)*sw**2)/(4.*cw**2) - (ee**2*complex(0,1)*sb**2*sw**2)/(4.*cw**2)',
                  order = {'QED':2})

GC_227 = Coupling(name = 'GC_227',
                  value = '-(cb**2*ee**2*complex(0,1)) - ee**2*complex(0,1)*sb**2 - (cb**2*cw**2*ee**2*complex(0,1))/(2.*sw**2) - (cw**2*ee**2*complex(0,1)*sb**2)/(2.*sw**2) - (cb**2*ee**2*complex(0,1)*sw**2)/(2.*cw**2) - (ee**2*complex(0,1)*sb**2*sw**2)/(2.*cw**2)',
                  order = {'QED':2})

GC_228 = Coupling(name = 'GC_228',
                  value = 'cb**2*ee**2*complex(0,1) + ee**2*complex(0,1)*sb**2 - (cb**2*cw**2*ee**2*complex(0,1))/(2.*sw**2) - (cw**2*ee**2*complex(0,1)*sb**2)/(2.*sw**2) - (cb**2*ee**2*complex(0,1)*sw**2)/(2.*cw**2) - (ee**2*complex(0,1)*sb**2*sw**2)/(2.*cw**2)',
                  order = {'QED':2})

GC_229 = Coupling(name = 'GC_229',
                  value = '-(cb**2*ee**2*complex(0,1)) - ee**2*complex(0,1)*sb**2 + (cb**2*cw**2*ee**2*complex(0,1))/(2.*sw**2) + (cw**2*ee**2*complex(0,1)*sb**2)/(2.*sw**2) + (cb**2*ee**2*complex(0,1)*sw**2)/(2.*cw**2) + (ee**2*complex(0,1)*sb**2*sw**2)/(2.*cw**2)',
                  order = {'QED':2})

GC_230 = Coupling(name = 'GC_230',
                  value = 'cb**2*ee**2*complex(0,1) + ee**2*complex(0,1)*sb**2 + (cb**2*cw**2*ee**2*complex(0,1))/(2.*sw**2) + (cw**2*ee**2*complex(0,1)*sb**2)/(2.*sw**2) + (cb**2*ee**2*complex(0,1)*sw**2)/(2.*cw**2) + (ee**2*complex(0,1)*sb**2*sw**2)/(2.*cw**2)',
                  order = {'QED':2})

GC_231 = Coupling(name = 'GC_231',
                  value = '(ee**2*complex(0,1)*sb*vev1)/(2.*cw) - (cb*ee**2*complex(0,1)*vev2)/(2.*cw)',
                  order = {'QED':1})

GC_232 = Coupling(name = 'GC_232',
                  value = '-(ee**2*complex(0,1)*sb*vev1)/(2.*cw) + (cb*ee**2*complex(0,1)*vev2)/(2.*cw)',
                  order = {'QED':1})

GC_233 = Coupling(name = 'GC_233',
                  value = '3*ca**2*complex(0,1)*l1*sa*vev1 - 2*ca**2*complex(0,1)*l3*sa*vev1 - 2*ca**2*complex(0,1)*l4*sa*vev1 - 2*ca**2*complex(0,1)*l5*sa*vev1 + complex(0,1)*l3*sa**3*vev1 + complex(0,1)*l4*sa**3*vev1 + complex(0,1)*l5*sa**3*vev1 - ca**3*complex(0,1)*l3*vev2 - ca**3*complex(0,1)*l4*vev2 - ca**3*complex(0,1)*l5*vev2 - 3*ca*complex(0,1)*l2*sa**2*vev2 + 2*ca*complex(0,1)*l3*sa**2*vev2 + 2*ca*complex(0,1)*l4*sa**2*vev2 + 2*ca*complex(0,1)*l5*sa**2*vev2',
                  order = {'QED':1})

GC_234 = Coupling(name = 'GC_234',
                  value = '3*ca**2*complex(0,1)*l3*sa*vev1 + 3*ca**2*complex(0,1)*l4*sa*vev1 + 3*ca**2*complex(0,1)*l5*sa*vev1 + 3*complex(0,1)*l1*sa**3*vev1 - 3*ca**3*complex(0,1)*l2*vev2 - 3*ca*complex(0,1)*l3*sa**2*vev2 - 3*ca*complex(0,1)*l4*sa**2*vev2 - 3*ca*complex(0,1)*l5*sa**2*vev2',
                  order = {'QED':1})

GC_235 = Coupling(name = 'GC_235',
                  value = '-3*ca**3*complex(0,1)*l1*vev1 - 3*ca*complex(0,1)*l3*sa**2*vev1 - 3*ca*complex(0,1)*l4*sa**2*vev1 - 3*ca*complex(0,1)*l5*sa**2*vev1 - 3*ca**2*complex(0,1)*l3*sa*vev2 - 3*ca**2*complex(0,1)*l4*sa*vev2 - 3*ca**2*complex(0,1)*l5*sa*vev2 - 3*complex(0,1)*l2*sa**3*vev2',
                  order = {'QED':1})

GC_236 = Coupling(name = 'GC_236',
                  value = '-(ca**3*complex(0,1)*l3*vev1) - ca**3*complex(0,1)*l4*vev1 - ca**3*complex(0,1)*l5*vev1 - 3*ca*complex(0,1)*l1*sa**2*vev1 + 2*ca*complex(0,1)*l3*sa**2*vev1 + 2*ca*complex(0,1)*l4*sa**2*vev1 + 2*ca*complex(0,1)*l5*sa**2*vev1 - 3*ca**2*complex(0,1)*l2*sa*vev2 + 2*ca**2*complex(0,1)*l3*sa*vev2 + 2*ca**2*complex(0,1)*l4*sa*vev2 + 2*ca**2*complex(0,1)*l5*sa*vev2 - complex(0,1)*l3*sa**3*vev2 - complex(0,1)*l4*sa**3*vev2 - complex(0,1)*l5*sa**3*vev2',
                  order = {'QED':1})

GC_237 = Coupling(name = 'GC_237',
                  value = '-(cb*ee**2*complex(0,1)*vev1)/(2.*cw) - (ee**2*complex(0,1)*sb*vev2)/(2.*cw)',
                  order = {'QED':1})

GC_238 = Coupling(name = 'GC_238',
                  value = '(cb*ee**2*complex(0,1)*vev1)/(2.*cw) + (ee**2*complex(0,1)*sb*vev2)/(2.*cw)',
                  order = {'QED':1})

GC_239 = Coupling(name = 'GC_239',
                  value = '(cb**2*ee**2*sb*vev1)/(4.*cw**2) + (ee**2*sb**3*vev1)/(4.*cw**2) - (cb**3*ee**2*vev2)/(4.*cw**2) - (cb*ee**2*sb**2*vev2)/(4.*cw**2)',
                  order = {'QED':1})

GC_240 = Coupling(name = 'GC_240',
                  value = '-(cb**2*ee**2*sb*vev1)/(4.*cw**2) - (ee**2*sb**3*vev1)/(4.*cw**2) + (cb**3*ee**2*vev2)/(4.*cw**2) + (cb*ee**2*sb**2*vev2)/(4.*cw**2)',
                  order = {'QED':1})

GC_241 = Coupling(name = 'GC_241',
                  value = 'cb**2*complex(0,1)*l1*sa*vev1 - ca*cb*complex(0,1)*l4*sb*vev1 - ca*cb*complex(0,1)*l5*sb*vev1 + complex(0,1)*l3*sa*sb**2*vev1 - ca*cb**2*complex(0,1)*l3*vev2 + cb*complex(0,1)*l4*sa*sb*vev2 + cb*complex(0,1)*l5*sa*sb*vev2 - ca*complex(0,1)*l2*sb**2*vev2',
                  order = {'QED':1})

GC_242 = Coupling(name = 'GC_242',
                  value = 'cb**2*complex(0,1)*l1*sa*vev1 - 2*ca*cb*complex(0,1)*l5*sb*vev1 + complex(0,1)*l3*sa*sb**2*vev1 + complex(0,1)*l4*sa*sb**2*vev1 - complex(0,1)*l5*sa*sb**2*vev1 - ca*cb**2*complex(0,1)*l3*vev2 - ca*cb**2*complex(0,1)*l4*vev2 + ca*cb**2*complex(0,1)*l5*vev2 + 2*cb*complex(0,1)*l5*sa*sb*vev2 - ca*complex(0,1)*l2*sb**2*vev2',
                  order = {'QED':1})

GC_243 = Coupling(name = 'GC_243',
                  value = 'cb**2*complex(0,1)*l3*sa*vev1 + ca*cb*complex(0,1)*l4*sb*vev1 + ca*cb*complex(0,1)*l5*sb*vev1 + complex(0,1)*l1*sa*sb**2*vev1 - ca*cb**2*complex(0,1)*l2*vev2 - cb*complex(0,1)*l4*sa*sb*vev2 - cb*complex(0,1)*l5*sa*sb*vev2 - ca*complex(0,1)*l3*sb**2*vev2',
                  order = {'QED':1})

GC_244 = Coupling(name = 'GC_244',
                  value = '-(cb**2*complex(0,1)*l4*sa*vev1)/2. - (cb**2*complex(0,1)*l5*sa*vev1)/2. + ca*cb*complex(0,1)*l1*sb*vev1 - ca*cb*complex(0,1)*l3*sb*vev1 + (complex(0,1)*l4*sa*sb**2*vev1)/2. + (complex(0,1)*l5*sa*sb**2*vev1)/2. - (ca*cb**2*complex(0,1)*l4*vev2)/2. - (ca*cb**2*complex(0,1)*l5*vev2)/2. - cb*complex(0,1)*l2*sa*sb*vev2 + cb*complex(0,1)*l3*sa*sb*vev2 + (ca*complex(0,1)*l4*sb**2*vev2)/2. + (ca*complex(0,1)*l5*sb**2*vev2)/2.',
                  order = {'QED':1})

GC_245 = Coupling(name = 'GC_245',
                  value = '-(cb**2*complex(0,1)*l5*sa*vev1) + ca*cb*complex(0,1)*l1*sb*vev1 - ca*cb*complex(0,1)*l3*sb*vev1 - ca*cb*complex(0,1)*l4*sb*vev1 + ca*cb*complex(0,1)*l5*sb*vev1 + complex(0,1)*l5*sa*sb**2*vev1 - ca*cb**2*complex(0,1)*l5*vev2 - cb*complex(0,1)*l2*sa*sb*vev2 + cb*complex(0,1)*l3*sa*sb*vev2 + cb*complex(0,1)*l4*sa*sb*vev2 - cb*complex(0,1)*l5*sa*sb*vev2 + ca*complex(0,1)*l5*sb**2*vev2',
                  order = {'QED':1})

GC_246 = Coupling(name = 'GC_246',
                  value = 'cb**2*complex(0,1)*l3*sa*vev1 + cb**2*complex(0,1)*l4*sa*vev1 - cb**2*complex(0,1)*l5*sa*vev1 + 2*ca*cb*complex(0,1)*l5*sb*vev1 + complex(0,1)*l1*sa*sb**2*vev1 - ca*cb**2*complex(0,1)*l2*vev2 - 2*cb*complex(0,1)*l5*sa*sb*vev2 - ca*complex(0,1)*l3*sb**2*vev2 - ca*complex(0,1)*l4*sb**2*vev2 + ca*complex(0,1)*l5*sb**2*vev2',
                  order = {'QED':1})

GC_247 = Coupling(name = 'GC_247',
                  value = '-(cb**2*l4*sb*vev1)/2. + (cb**2*l5*sb*vev1)/2. - (l4*sb**3*vev1)/2. + (l5*sb**3*vev1)/2. + (cb**3*l4*vev2)/2. - (cb**3*l5*vev2)/2. + (cb*l4*sb**2*vev2)/2. - (cb*l5*sb**2*vev2)/2.',
                  order = {'QED':1})

GC_248 = Coupling(name = 'GC_248',
                  value = '(cb**2*l4*sb*vev1)/2. - (cb**2*l5*sb*vev1)/2. + (l4*sb**3*vev1)/2. - (l5*sb**3*vev1)/2. - (cb**3*l4*vev2)/2. + (cb**3*l5*vev2)/2. - (cb*l4*sb**2*vev2)/2. + (cb*l5*sb**2*vev2)/2.',
                  order = {'QED':1})

GC_249 = Coupling(name = 'GC_249',
                  value = '-(ca*cb**2*complex(0,1)*l1*vev1) - cb*complex(0,1)*l4*sa*sb*vev1 - cb*complex(0,1)*l5*sa*sb*vev1 - ca*complex(0,1)*l3*sb**2*vev1 - cb**2*complex(0,1)*l3*sa*vev2 - ca*cb*complex(0,1)*l4*sb*vev2 - ca*cb*complex(0,1)*l5*sb*vev2 - complex(0,1)*l2*sa*sb**2*vev2',
                  order = {'QED':1})

GC_250 = Coupling(name = 'GC_250',
                  value = '-(ca*cb**2*complex(0,1)*l1*vev1) - 2*cb*complex(0,1)*l5*sa*sb*vev1 - ca*complex(0,1)*l3*sb**2*vev1 - ca*complex(0,1)*l4*sb**2*vev1 + ca*complex(0,1)*l5*sb**2*vev1 - cb**2*complex(0,1)*l3*sa*vev2 - cb**2*complex(0,1)*l4*sa*vev2 + cb**2*complex(0,1)*l5*sa*vev2 - 2*ca*cb*complex(0,1)*l5*sb*vev2 - complex(0,1)*l2*sa*sb**2*vev2',
                  order = {'QED':1})

GC_251 = Coupling(name = 'GC_251',
                  value = '-(ca*cb**2*complex(0,1)*l3*vev1) + cb*complex(0,1)*l4*sa*sb*vev1 + cb*complex(0,1)*l5*sa*sb*vev1 - ca*complex(0,1)*l1*sb**2*vev1 - cb**2*complex(0,1)*l2*sa*vev2 + ca*cb*complex(0,1)*l4*sb*vev2 + ca*cb*complex(0,1)*l5*sb*vev2 - complex(0,1)*l3*sa*sb**2*vev2',
                  order = {'QED':1})

GC_252 = Coupling(name = 'GC_252',
                  value = '-(ca*cb**2*complex(0,1)*l4*vev1)/2. - (ca*cb**2*complex(0,1)*l5*vev1)/2. - cb*complex(0,1)*l1*sa*sb*vev1 + cb*complex(0,1)*l3*sa*sb*vev1 + (ca*complex(0,1)*l4*sb**2*vev1)/2. + (ca*complex(0,1)*l5*sb**2*vev1)/2. + (cb**2*complex(0,1)*l4*sa*vev2)/2. + (cb**2*complex(0,1)*l5*sa*vev2)/2. - ca*cb*complex(0,1)*l2*sb*vev2 + ca*cb*complex(0,1)*l3*sb*vev2 - (complex(0,1)*l4*sa*sb**2*vev2)/2. - (complex(0,1)*l5*sa*sb**2*vev2)/2.',
                  order = {'QED':1})

GC_253 = Coupling(name = 'GC_253',
                  value = '-(ca*cb**2*complex(0,1)*l5*vev1) - cb*complex(0,1)*l1*sa*sb*vev1 + cb*complex(0,1)*l3*sa*sb*vev1 + cb*complex(0,1)*l4*sa*sb*vev1 - cb*complex(0,1)*l5*sa*sb*vev1 + ca*complex(0,1)*l5*sb**2*vev1 + cb**2*complex(0,1)*l5*sa*vev2 - ca*cb*complex(0,1)*l2*sb*vev2 + ca*cb*complex(0,1)*l3*sb*vev2 + ca*cb*complex(0,1)*l4*sb*vev2 - ca*cb*complex(0,1)*l5*sb*vev2 - complex(0,1)*l5*sa*sb**2*vev2',
                  order = {'QED':1})

GC_254 = Coupling(name = 'GC_254',
                  value = '-(ca*cb**2*complex(0,1)*l3*vev1) - ca*cb**2*complex(0,1)*l4*vev1 + ca*cb**2*complex(0,1)*l5*vev1 + 2*cb*complex(0,1)*l5*sa*sb*vev1 - ca*complex(0,1)*l1*sb**2*vev1 - cb**2*complex(0,1)*l2*sa*vev2 + 2*ca*cb*complex(0,1)*l5*sb*vev2 - complex(0,1)*l3*sa*sb**2*vev2 - complex(0,1)*l4*sa*sb**2*vev2 + complex(0,1)*l5*sa*sb**2*vev2',
                  order = {'QED':1})

GC_255 = Coupling(name = 'GC_255',
                  value = '-(cb**3*ee**2*vev1)/(4.*cw**2) - (cb*ee**2*sb**2*vev1)/(4.*cw**2) - (cb**2*ee**2*sb*vev2)/(4.*cw**2) - (ee**2*sb**3*vev2)/(4.*cw**2)',
                  order = {'QED':1})

GC_256 = Coupling(name = 'GC_256',
                  value = '(cb**3*ee**2*vev1)/(4.*cw**2) + (cb*ee**2*sb**2*vev1)/(4.*cw**2) + (cb**2*ee**2*sb*vev2)/(4.*cw**2) + (ee**2*sb**3*vev2)/(4.*cw**2)',
                  order = {'QED':1})

GC_257 = Coupling(name = 'GC_257',
                  value = '(cb**3*l4*vev1)/2. - (cb**3*l5*vev1)/2. + (cb*l4*sb**2*vev1)/2. - (cb*l5*sb**2*vev1)/2. + (cb**2*l4*sb*vev2)/2. - (cb**2*l5*sb*vev2)/2. + (l4*sb**3*vev2)/2. - (l5*sb**3*vev2)/2.',
                  order = {'QED':1})

GC_258 = Coupling(name = 'GC_258',
                  value = '-(cb**3*l4*vev1)/2. + (cb**3*l5*vev1)/2. - (cb*l4*sb**2*vev1)/2. + (cb*l5*sb**2*vev1)/2. - (cb**2*l4*sb*vev2)/2. + (cb**2*l5*sb*vev2)/2. - (l4*sb**3*vev2)/2. + (l5*sb**3*vev2)/2.',
                  order = {'QED':1})

GC_259 = Coupling(name = 'GC_259',
                  value = '(ee**2*complex(0,1)*sa*vev1)/(4.*sw**2) - (ca*ee**2*complex(0,1)*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_260 = Coupling(name = 'GC_260',
                  value = '(ee**2*complex(0,1)*sa*vev1)/(2.*sw**2) - (ca*ee**2*complex(0,1)*vev2)/(2.*sw**2)',
                  order = {'QED':1})

GC_261 = Coupling(name = 'GC_261',
                  value = '-(ee**2*complex(0,1)*sa*vev1)/(2.*sw**2) + (ca*ee**2*complex(0,1)*vev2)/(2.*sw**2)',
                  order = {'QED':1})

GC_262 = Coupling(name = 'GC_262',
                  value = '(ee**2*sb*vev1)/(2.*sw**2) - (cb*ee**2*vev2)/(2.*sw**2)',
                  order = {'QED':1})

GC_263 = Coupling(name = 'GC_263',
                  value = '(ee**2*sb*vev1)/(4.*sw**2) - (cb*ee**2*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_264 = Coupling(name = 'GC_264',
                  value = '-(ee**2*sb*vev1)/(4.*sw**2) + (cb*ee**2*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_265 = Coupling(name = 'GC_265',
                  value = '-(ee**2*sb*vev1)/(2.*sw**2) + (cb*ee**2*vev2)/(2.*sw**2)',
                  order = {'QED':1})

GC_266 = Coupling(name = 'GC_266',
                  value = '-(ee**2*complex(0,1)*sb*vev1)/(4.*cw) + (cw*ee**2*complex(0,1)*sb*vev1)/(4.*sw**2) + (cb*ee**2*complex(0,1)*vev2)/(4.*cw) - (cb*cw*ee**2*complex(0,1)*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_267 = Coupling(name = 'GC_267',
                  value = '-(ee**2*complex(0,1)*sb*vev1)/(4.*cw) - (cw*ee**2*complex(0,1)*sb*vev1)/(4.*sw**2) + (cb*ee**2*complex(0,1)*vev2)/(4.*cw) + (cb*cw*ee**2*complex(0,1)*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_268 = Coupling(name = 'GC_268',
                  value = '(ee**2*complex(0,1)*sb*vev1)/(2.*cw) + (cw*ee**2*complex(0,1)*sb*vev1)/(2.*sw**2) - (cb*ee**2*complex(0,1)*vev2)/(2.*cw) - (cb*cw*ee**2*complex(0,1)*vev2)/(2.*sw**2)',
                  order = {'QED':1})

GC_269 = Coupling(name = 'GC_269',
                  value = '(ee**2*complex(0,1)*sb*vev1)/(2.*cw) - (cw*ee**2*complex(0,1)*sb*vev1)/(2.*sw**2) - (cb*ee**2*complex(0,1)*vev2)/(2.*cw) + (cb*cw*ee**2*complex(0,1)*vev2)/(2.*sw**2)',
                  order = {'QED':1})

GC_270 = Coupling(name = 'GC_270',
                  value = '-(ca*ee**2*complex(0,1)*vev1)/(4.*sw**2) - (ee**2*complex(0,1)*sa*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_271 = Coupling(name = 'GC_271',
                  value = '-(ca*ee**2*complex(0,1)*vev1)/(2.*sw**2) - (ee**2*complex(0,1)*sa*vev2)/(2.*sw**2)',
                  order = {'QED':1})

GC_272 = Coupling(name = 'GC_272',
                  value = '(ca*ee**2*complex(0,1)*vev1)/(2.*sw**2) + (ee**2*complex(0,1)*sa*vev2)/(2.*sw**2)',
                  order = {'QED':1})

GC_273 = Coupling(name = 'GC_273',
                  value = '-(cb*ee**2*vev1)/(2.*sw**2) - (ee**2*sb*vev2)/(2.*sw**2)',
                  order = {'QED':1})

GC_274 = Coupling(name = 'GC_274',
                  value = '-(cb*ee**2*vev1)/(4.*sw**2) - (ee**2*sb*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_275 = Coupling(name = 'GC_275',
                  value = '(cb*ee**2*vev1)/(4.*sw**2) + (ee**2*sb*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_276 = Coupling(name = 'GC_276',
                  value = '(cb*ee**2*vev1)/(2.*sw**2) + (ee**2*sb*vev2)/(2.*sw**2)',
                  order = {'QED':1})

GC_277 = Coupling(name = 'GC_277',
                  value = '-(ca*cb**2*complex(0,1)*l3*vev1) + cb*complex(0,1)*l4*sa*sb*vev1 + cb*complex(0,1)*l5*sa*sb*vev1 - ca*complex(0,1)*l1*sb**2*vev1 - (cb*ee**2*complex(0,1)*sa*sb*vev1)/(4.*sw**2) + (ca*ee**2*complex(0,1)*sb**2*vev1)/(4.*sw**2) - cb**2*complex(0,1)*l2*sa*vev2 + ca*cb*complex(0,1)*l4*sb*vev2 + ca*cb*complex(0,1)*l5*sb*vev2 - complex(0,1)*l3*sa*sb**2*vev2 + (cb**2*ee**2*complex(0,1)*sa*vev2)/(4.*sw**2) - (ca*cb*ee**2*complex(0,1)*sb*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_278 = Coupling(name = 'GC_278',
                  value = '-(ca*cb**2*complex(0,1)*l3*vev1) - ca*cb**2*complex(0,1)*l4*vev1 + ca*cb**2*complex(0,1)*l5*vev1 - (cb*ee**2*complex(0,1)*sa*sb*vev1)/(4.*cw**2) + 2*cb*complex(0,1)*l5*sa*sb*vev1 + (ca*ee**2*complex(0,1)*sb**2*vev1)/(4.*cw**2) - ca*complex(0,1)*l1*sb**2*vev1 - (cb*ee**2*complex(0,1)*sa*sb*vev1)/(4.*sw**2) + (ca*ee**2*complex(0,1)*sb**2*vev1)/(4.*sw**2) + (cb**2*ee**2*complex(0,1)*sa*vev2)/(4.*cw**2) - cb**2*complex(0,1)*l2*sa*vev2 - (ca*cb*ee**2*complex(0,1)*sb*vev2)/(4.*cw**2) + 2*ca*cb*complex(0,1)*l5*sb*vev2 - complex(0,1)*l3*sa*sb**2*vev2 - complex(0,1)*l4*sa*sb**2*vev2 + complex(0,1)*l5*sa*sb**2*vev2 + (cb**2*ee**2*complex(0,1)*sa*vev2)/(4.*sw**2) - (ca*cb*ee**2*complex(0,1)*sb*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_279 = Coupling(name = 'GC_279',
                  value = '-(ca*cb**2*complex(0,1)*l4*vev1)/2. - (ca*cb**2*complex(0,1)*l5*vev1)/2. - cb*complex(0,1)*l1*sa*sb*vev1 + cb*complex(0,1)*l3*sa*sb*vev1 + (ca*complex(0,1)*l4*sb**2*vev1)/2. + (ca*complex(0,1)*l5*sb**2*vev1)/2. + (cb*ee**2*complex(0,1)*sa*sb*vev1)/(4.*sw**2) - (ca*ee**2*complex(0,1)*sb**2*vev1)/(4.*sw**2) + (cb**2*complex(0,1)*l4*sa*vev2)/2. + (cb**2*complex(0,1)*l5*sa*vev2)/2. - ca*cb*complex(0,1)*l2*sb*vev2 + ca*cb*complex(0,1)*l3*sb*vev2 - (complex(0,1)*l4*sa*sb**2*vev2)/2. - (complex(0,1)*l5*sa*sb**2*vev2)/2. - (cb**2*ee**2*complex(0,1)*sa*vev2)/(4.*sw**2) + (ca*cb*ee**2*complex(0,1)*sb*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_280 = Coupling(name = 'GC_280',
                  value = '-(ca*cb**2*complex(0,1)*l5*vev1) + (cb*ee**2*complex(0,1)*sa*sb*vev1)/(4.*cw**2) - cb*complex(0,1)*l1*sa*sb*vev1 + cb*complex(0,1)*l3*sa*sb*vev1 + cb*complex(0,1)*l4*sa*sb*vev1 - cb*complex(0,1)*l5*sa*sb*vev1 - (ca*ee**2*complex(0,1)*sb**2*vev1)/(4.*cw**2) + ca*complex(0,1)*l5*sb**2*vev1 + (cb*ee**2*complex(0,1)*sa*sb*vev1)/(4.*sw**2) - (ca*ee**2*complex(0,1)*sb**2*vev1)/(4.*sw**2) - (cb**2*ee**2*complex(0,1)*sa*vev2)/(4.*cw**2) + cb**2*complex(0,1)*l5*sa*vev2 + (ca*cb*ee**2*complex(0,1)*sb*vev2)/(4.*cw**2) - ca*cb*complex(0,1)*l2*sb*vev2 + ca*cb*complex(0,1)*l3*sb*vev2 + ca*cb*complex(0,1)*l4*sb*vev2 - ca*cb*complex(0,1)*l5*sb*vev2 - complex(0,1)*l5*sa*sb**2*vev2 - (cb**2*ee**2*complex(0,1)*sa*vev2)/(4.*sw**2) + (ca*cb*ee**2*complex(0,1)*sb*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_281 = Coupling(name = 'GC_281',
                  value = '-(ca*cb**2*complex(0,1)*l3*vev1) + cb*complex(0,1)*l4*sa*sb*vev1 + cb*complex(0,1)*l5*sa*sb*vev1 - ca*complex(0,1)*l1*sb**2*vev1 + (cb*ee**2*complex(0,1)*sa*sb*vev1)/(2.*sw**2) - (ca*ee**2*complex(0,1)*sb**2*vev1)/(2.*sw**2) - cb**2*complex(0,1)*l2*sa*vev2 + ca*cb*complex(0,1)*l4*sb*vev2 + ca*cb*complex(0,1)*l5*sb*vev2 - complex(0,1)*l3*sa*sb**2*vev2 - (cb**2*ee**2*complex(0,1)*sa*vev2)/(2.*sw**2) + (ca*cb*ee**2*complex(0,1)*sb*vev2)/(2.*sw**2)',
                  order = {'QED':1})

GC_282 = Coupling(name = 'GC_282',
                  value = '-(ca*cb**2*complex(0,1)*l3*vev1) - ca*cb**2*complex(0,1)*l4*vev1 + ca*cb**2*complex(0,1)*l5*vev1 + (cb*ee**2*complex(0,1)*sa*sb*vev1)/(2.*cw**2) + 2*cb*complex(0,1)*l5*sa*sb*vev1 - (ca*ee**2*complex(0,1)*sb**2*vev1)/(2.*cw**2) - ca*complex(0,1)*l1*sb**2*vev1 + (cb*ee**2*complex(0,1)*sa*sb*vev1)/(2.*sw**2) - (ca*ee**2*complex(0,1)*sb**2*vev1)/(2.*sw**2) - (cb**2*ee**2*complex(0,1)*sa*vev2)/(2.*cw**2) - cb**2*complex(0,1)*l2*sa*vev2 + (ca*cb*ee**2*complex(0,1)*sb*vev2)/(2.*cw**2) + 2*ca*cb*complex(0,1)*l5*sb*vev2 - complex(0,1)*l3*sa*sb**2*vev2 - complex(0,1)*l4*sa*sb**2*vev2 + complex(0,1)*l5*sa*sb**2*vev2 - (cb**2*ee**2*complex(0,1)*sa*vev2)/(2.*sw**2) + (ca*cb*ee**2*complex(0,1)*sb*vev2)/(2.*sw**2)',
                  order = {'QED':1})

GC_283 = Coupling(name = 'GC_283',
                  value = '(cb*ee**2*complex(0,1)*vev1)/(4.*cw) - (cb*cw*ee**2*complex(0,1)*vev1)/(4.*sw**2) + (ee**2*complex(0,1)*sb*vev2)/(4.*cw) - (cw*ee**2*complex(0,1)*sb*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_284 = Coupling(name = 'GC_284',
                  value = '(cb*ee**2*complex(0,1)*vev1)/(4.*cw) + (cb*cw*ee**2*complex(0,1)*vev1)/(4.*sw**2) + (ee**2*complex(0,1)*sb*vev2)/(4.*cw) + (cw*ee**2*complex(0,1)*sb*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_285 = Coupling(name = 'GC_285',
                  value = '-(cb*ee**2*complex(0,1)*vev1)/(2.*cw) - (cb*cw*ee**2*complex(0,1)*vev1)/(2.*sw**2) - (ee**2*complex(0,1)*sb*vev2)/(2.*cw) - (cw*ee**2*complex(0,1)*sb*vev2)/(2.*sw**2)',
                  order = {'QED':1})

GC_286 = Coupling(name = 'GC_286',
                  value = '-(cb*ee**2*complex(0,1)*vev1)/(2.*cw) + (cb*cw*ee**2*complex(0,1)*vev1)/(2.*sw**2) - (ee**2*complex(0,1)*sb*vev2)/(2.*cw) + (cw*ee**2*complex(0,1)*sb*vev2)/(2.*sw**2)',
                  order = {'QED':1})

GC_287 = Coupling(name = 'GC_287',
                  value = 'cb**2*complex(0,1)*l3*sa*vev1 + ca*cb*complex(0,1)*l4*sb*vev1 + ca*cb*complex(0,1)*l5*sb*vev1 + complex(0,1)*l1*sa*sb**2*vev1 - (ca*cb*ee**2*complex(0,1)*sb*vev1)/(4.*sw**2) - (ee**2*complex(0,1)*sa*sb**2*vev1)/(4.*sw**2) - ca*cb**2*complex(0,1)*l2*vev2 - cb*complex(0,1)*l4*sa*sb*vev2 - cb*complex(0,1)*l5*sa*sb*vev2 - ca*complex(0,1)*l3*sb**2*vev2 + (ca*cb**2*ee**2*complex(0,1)*vev2)/(4.*sw**2) + (cb*ee**2*complex(0,1)*sa*sb*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_288 = Coupling(name = 'GC_288',
                  value = '-(cb**2*complex(0,1)*l4*sa*vev1)/2. - (cb**2*complex(0,1)*l5*sa*vev1)/2. + ca*cb*complex(0,1)*l1*sb*vev1 - ca*cb*complex(0,1)*l3*sb*vev1 + (complex(0,1)*l4*sa*sb**2*vev1)/2. + (complex(0,1)*l5*sa*sb**2*vev1)/2. - (ca*cb*ee**2*complex(0,1)*sb*vev1)/(4.*sw**2) - (ee**2*complex(0,1)*sa*sb**2*vev1)/(4.*sw**2) - (ca*cb**2*complex(0,1)*l4*vev2)/2. - (ca*cb**2*complex(0,1)*l5*vev2)/2. - cb*complex(0,1)*l2*sa*sb*vev2 + cb*complex(0,1)*l3*sa*sb*vev2 + (ca*complex(0,1)*l4*sb**2*vev2)/2. + (ca*complex(0,1)*l5*sb**2*vev2)/2. + (ca*cb**2*ee**2*complex(0,1)*vev2)/(4.*sw**2) + (cb*ee**2*complex(0,1)*sa*sb*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_289 = Coupling(name = 'GC_289',
                  value = '-(cb**2*complex(0,1)*l5*sa*vev1) - (ca*cb*ee**2*complex(0,1)*sb*vev1)/(4.*cw**2) + ca*cb*complex(0,1)*l1*sb*vev1 - ca*cb*complex(0,1)*l3*sb*vev1 - ca*cb*complex(0,1)*l4*sb*vev1 + ca*cb*complex(0,1)*l5*sb*vev1 - (ee**2*complex(0,1)*sa*sb**2*vev1)/(4.*cw**2) + complex(0,1)*l5*sa*sb**2*vev1 - (ca*cb*ee**2*complex(0,1)*sb*vev1)/(4.*sw**2) - (ee**2*complex(0,1)*sa*sb**2*vev1)/(4.*sw**2) + (ca*cb**2*ee**2*complex(0,1)*vev2)/(4.*cw**2) - ca*cb**2*complex(0,1)*l5*vev2 + (cb*ee**2*complex(0,1)*sa*sb*vev2)/(4.*cw**2) - cb*complex(0,1)*l2*sa*sb*vev2 + cb*complex(0,1)*l3*sa*sb*vev2 + cb*complex(0,1)*l4*sa*sb*vev2 - cb*complex(0,1)*l5*sa*sb*vev2 + ca*complex(0,1)*l5*sb**2*vev2 + (ca*cb**2*ee**2*complex(0,1)*vev2)/(4.*sw**2) + (cb*ee**2*complex(0,1)*sa*sb*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_290 = Coupling(name = 'GC_290',
                  value = 'cb**2*complex(0,1)*l3*sa*vev1 + cb**2*complex(0,1)*l4*sa*vev1 - cb**2*complex(0,1)*l5*sa*vev1 - (ca*cb*ee**2*complex(0,1)*sb*vev1)/(4.*cw**2) + 2*ca*cb*complex(0,1)*l5*sb*vev1 - (ee**2*complex(0,1)*sa*sb**2*vev1)/(4.*cw**2) + complex(0,1)*l1*sa*sb**2*vev1 - (ca*cb*ee**2*complex(0,1)*sb*vev1)/(4.*sw**2) - (ee**2*complex(0,1)*sa*sb**2*vev1)/(4.*sw**2) + (ca*cb**2*ee**2*complex(0,1)*vev2)/(4.*cw**2) - ca*cb**2*complex(0,1)*l2*vev2 + (cb*ee**2*complex(0,1)*sa*sb*vev2)/(4.*cw**2) - 2*cb*complex(0,1)*l5*sa*sb*vev2 - ca*complex(0,1)*l3*sb**2*vev2 - ca*complex(0,1)*l4*sb**2*vev2 + ca*complex(0,1)*l5*sb**2*vev2 + (ca*cb**2*ee**2*complex(0,1)*vev2)/(4.*sw**2) + (cb*ee**2*complex(0,1)*sa*sb*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_291 = Coupling(name = 'GC_291',
                  value = 'cb**2*complex(0,1)*l3*sa*vev1 + ca*cb*complex(0,1)*l4*sb*vev1 + ca*cb*complex(0,1)*l5*sb*vev1 + complex(0,1)*l1*sa*sb**2*vev1 + (ca*cb*ee**2*complex(0,1)*sb*vev1)/(2.*sw**2) + (ee**2*complex(0,1)*sa*sb**2*vev1)/(2.*sw**2) - ca*cb**2*complex(0,1)*l2*vev2 - cb*complex(0,1)*l4*sa*sb*vev2 - cb*complex(0,1)*l5*sa*sb*vev2 - ca*complex(0,1)*l3*sb**2*vev2 - (ca*cb**2*ee**2*complex(0,1)*vev2)/(2.*sw**2) - (cb*ee**2*complex(0,1)*sa*sb*vev2)/(2.*sw**2)',
                  order = {'QED':1})

GC_292 = Coupling(name = 'GC_292',
                  value = 'cb**2*complex(0,1)*l3*sa*vev1 + cb**2*complex(0,1)*l4*sa*vev1 - cb**2*complex(0,1)*l5*sa*vev1 + (ca*cb*ee**2*complex(0,1)*sb*vev1)/(2.*cw**2) + 2*ca*cb*complex(0,1)*l5*sb*vev1 + (ee**2*complex(0,1)*sa*sb**2*vev1)/(2.*cw**2) + complex(0,1)*l1*sa*sb**2*vev1 + (ca*cb*ee**2*complex(0,1)*sb*vev1)/(2.*sw**2) + (ee**2*complex(0,1)*sa*sb**2*vev1)/(2.*sw**2) - (ca*cb**2*ee**2*complex(0,1)*vev2)/(2.*cw**2) - ca*cb**2*complex(0,1)*l2*vev2 - (cb*ee**2*complex(0,1)*sa*sb*vev2)/(2.*cw**2) - 2*cb*complex(0,1)*l5*sa*sb*vev2 - ca*complex(0,1)*l3*sb**2*vev2 - ca*complex(0,1)*l4*sb**2*vev2 + ca*complex(0,1)*l5*sb**2*vev2 - (ca*cb**2*ee**2*complex(0,1)*vev2)/(2.*sw**2) - (cb*ee**2*complex(0,1)*sa*sb*vev2)/(2.*sw**2)',
                  order = {'QED':1})

GC_293 = Coupling(name = 'GC_293',
                  value = '-(cb**2*complex(0,1)*l4*sa*vev1)/2. - (cb**2*complex(0,1)*l5*sa*vev1)/2. + ca*cb*complex(0,1)*l1*sb*vev1 - ca*cb*complex(0,1)*l3*sb*vev1 + (complex(0,1)*l4*sa*sb**2*vev1)/2. + (complex(0,1)*l5*sa*sb**2*vev1)/2. + (cb**2*ee**2*complex(0,1)*sa*vev1)/(4.*sw**2) - (ca*cb*ee**2*complex(0,1)*sb*vev1)/(4.*sw**2) - (ca*cb**2*complex(0,1)*l4*vev2)/2. - (ca*cb**2*complex(0,1)*l5*vev2)/2. - cb*complex(0,1)*l2*sa*sb*vev2 + cb*complex(0,1)*l3*sa*sb*vev2 + (ca*complex(0,1)*l4*sb**2*vev2)/2. + (ca*complex(0,1)*l5*sb**2*vev2)/2. + (cb*ee**2*complex(0,1)*sa*sb*vev2)/(4.*sw**2) - (ca*ee**2*complex(0,1)*sb**2*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_294 = Coupling(name = 'GC_294',
                  value = '(cb**2*ee**2*complex(0,1)*sa*vev1)/(4.*cw**2) - cb**2*complex(0,1)*l5*sa*vev1 - (ca*cb*ee**2*complex(0,1)*sb*vev1)/(4.*cw**2) + ca*cb*complex(0,1)*l1*sb*vev1 - ca*cb*complex(0,1)*l3*sb*vev1 - ca*cb*complex(0,1)*l4*sb*vev1 + ca*cb*complex(0,1)*l5*sb*vev1 + complex(0,1)*l5*sa*sb**2*vev1 + (cb**2*ee**2*complex(0,1)*sa*vev1)/(4.*sw**2) - (ca*cb*ee**2*complex(0,1)*sb*vev1)/(4.*sw**2) - ca*cb**2*complex(0,1)*l5*vev2 + (cb*ee**2*complex(0,1)*sa*sb*vev2)/(4.*cw**2) - cb*complex(0,1)*l2*sa*sb*vev2 + cb*complex(0,1)*l3*sa*sb*vev2 + cb*complex(0,1)*l4*sa*sb*vev2 - cb*complex(0,1)*l5*sa*sb*vev2 - (ca*ee**2*complex(0,1)*sb**2*vev2)/(4.*cw**2) + ca*complex(0,1)*l5*sb**2*vev2 + (cb*ee**2*complex(0,1)*sa*sb*vev2)/(4.*sw**2) - (ca*ee**2*complex(0,1)*sb**2*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_295 = Coupling(name = 'GC_295',
                  value = 'cb**2*complex(0,1)*l1*sa*vev1 - ca*cb*complex(0,1)*l4*sb*vev1 - ca*cb*complex(0,1)*l5*sb*vev1 + complex(0,1)*l3*sa*sb**2*vev1 - (cb**2*ee**2*complex(0,1)*sa*vev1)/(4.*sw**2) + (ca*cb*ee**2*complex(0,1)*sb*vev1)/(4.*sw**2) - ca*cb**2*complex(0,1)*l3*vev2 + cb*complex(0,1)*l4*sa*sb*vev2 + cb*complex(0,1)*l5*sa*sb*vev2 - ca*complex(0,1)*l2*sb**2*vev2 - (cb*ee**2*complex(0,1)*sa*sb*vev2)/(4.*sw**2) + (ca*ee**2*complex(0,1)*sb**2*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_296 = Coupling(name = 'GC_296',
                  value = '-(cb**2*ee**2*complex(0,1)*sa*vev1)/(4.*cw**2) + cb**2*complex(0,1)*l1*sa*vev1 + (ca*cb*ee**2*complex(0,1)*sb*vev1)/(4.*cw**2) - 2*ca*cb*complex(0,1)*l5*sb*vev1 + complex(0,1)*l3*sa*sb**2*vev1 + complex(0,1)*l4*sa*sb**2*vev1 - complex(0,1)*l5*sa*sb**2*vev1 - (cb**2*ee**2*complex(0,1)*sa*vev1)/(4.*sw**2) + (ca*cb*ee**2*complex(0,1)*sb*vev1)/(4.*sw**2) - ca*cb**2*complex(0,1)*l3*vev2 - ca*cb**2*complex(0,1)*l4*vev2 + ca*cb**2*complex(0,1)*l5*vev2 - (cb*ee**2*complex(0,1)*sa*sb*vev2)/(4.*cw**2) + 2*cb*complex(0,1)*l5*sa*sb*vev2 + (ca*ee**2*complex(0,1)*sb**2*vev2)/(4.*cw**2) - ca*complex(0,1)*l2*sb**2*vev2 - (cb*ee**2*complex(0,1)*sa*sb*vev2)/(4.*sw**2) + (ca*ee**2*complex(0,1)*sb**2*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_297 = Coupling(name = 'GC_297',
                  value = '-(cb**2*complex(0,1)*l4*sa*vev1)/2. - (cb**2*complex(0,1)*l5*sa*vev1)/2. + ca*cb*complex(0,1)*l1*sb*vev1 - ca*cb*complex(0,1)*l3*sb*vev1 + (complex(0,1)*l4*sa*sb**2*vev1)/2. + (complex(0,1)*l5*sa*sb**2*vev1)/2. - (cb**2*ee**2*complex(0,1)*sa*vev1)/(4.*sw**2) + (ca*cb*ee**2*complex(0,1)*sb*vev1)/(2.*sw**2) + (ee**2*complex(0,1)*sa*sb**2*vev1)/(4.*sw**2) - (ca*cb**2*complex(0,1)*l4*vev2)/2. - (ca*cb**2*complex(0,1)*l5*vev2)/2. - cb*complex(0,1)*l2*sa*sb*vev2 + cb*complex(0,1)*l3*sa*sb*vev2 + (ca*complex(0,1)*l4*sb**2*vev2)/2. + (ca*complex(0,1)*l5*sb**2*vev2)/2. - (ca*cb**2*ee**2*complex(0,1)*vev2)/(4.*sw**2) - (cb*ee**2*complex(0,1)*sa*sb*vev2)/(2.*sw**2) + (ca*ee**2*complex(0,1)*sb**2*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_298 = Coupling(name = 'GC_298',
                  value = '-(cb**2*ee**2*complex(0,1)*sa*vev1)/(4.*cw**2) - cb**2*complex(0,1)*l5*sa*vev1 + (ca*cb*ee**2*complex(0,1)*sb*vev1)/(2.*cw**2) + ca*cb*complex(0,1)*l1*sb*vev1 - ca*cb*complex(0,1)*l3*sb*vev1 - ca*cb*complex(0,1)*l4*sb*vev1 + ca*cb*complex(0,1)*l5*sb*vev1 + (ee**2*complex(0,1)*sa*sb**2*vev1)/(4.*cw**2) + complex(0,1)*l5*sa*sb**2*vev1 - (cb**2*ee**2*complex(0,1)*sa*vev1)/(4.*sw**2) + (ca*cb*ee**2*complex(0,1)*sb*vev1)/(2.*sw**2) + (ee**2*complex(0,1)*sa*sb**2*vev1)/(4.*sw**2) - (ca*cb**2*ee**2*complex(0,1)*vev2)/(4.*cw**2) - ca*cb**2*complex(0,1)*l5*vev2 - (cb*ee**2*complex(0,1)*sa*sb*vev2)/(2.*cw**2) - cb*complex(0,1)*l2*sa*sb*vev2 + cb*complex(0,1)*l3*sa*sb*vev2 + cb*complex(0,1)*l4*sa*sb*vev2 - cb*complex(0,1)*l5*sa*sb*vev2 + (ca*ee**2*complex(0,1)*sb**2*vev2)/(4.*cw**2) + ca*complex(0,1)*l5*sb**2*vev2 - (ca*cb**2*ee**2*complex(0,1)*vev2)/(4.*sw**2) - (cb*ee**2*complex(0,1)*sa*sb*vev2)/(2.*sw**2) + (ca*ee**2*complex(0,1)*sb**2*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_299 = Coupling(name = 'GC_299',
                  value = 'cb**2*complex(0,1)*l1*sa*vev1 - ca*cb*complex(0,1)*l4*sb*vev1 - ca*cb*complex(0,1)*l5*sb*vev1 + complex(0,1)*l3*sa*sb**2*vev1 + (cb**2*ee**2*complex(0,1)*sa*vev1)/(2.*sw**2) - (ca*cb*ee**2*complex(0,1)*sb*vev1)/(2.*sw**2) - ca*cb**2*complex(0,1)*l3*vev2 + cb*complex(0,1)*l4*sa*sb*vev2 + cb*complex(0,1)*l5*sa*sb*vev2 - ca*complex(0,1)*l2*sb**2*vev2 + (cb*ee**2*complex(0,1)*sa*sb*vev2)/(2.*sw**2) - (ca*ee**2*complex(0,1)*sb**2*vev2)/(2.*sw**2)',
                  order = {'QED':1})

GC_300 = Coupling(name = 'GC_300',
                  value = '(cb**2*ee**2*complex(0,1)*sa*vev1)/(2.*cw**2) + cb**2*complex(0,1)*l1*sa*vev1 - (ca*cb*ee**2*complex(0,1)*sb*vev1)/(2.*cw**2) - 2*ca*cb*complex(0,1)*l5*sb*vev1 + complex(0,1)*l3*sa*sb**2*vev1 + complex(0,1)*l4*sa*sb**2*vev1 - complex(0,1)*l5*sa*sb**2*vev1 + (cb**2*ee**2*complex(0,1)*sa*vev1)/(2.*sw**2) - (ca*cb*ee**2*complex(0,1)*sb*vev1)/(2.*sw**2) - ca*cb**2*complex(0,1)*l3*vev2 - ca*cb**2*complex(0,1)*l4*vev2 + ca*cb**2*complex(0,1)*l5*vev2 + (cb*ee**2*complex(0,1)*sa*sb*vev2)/(2.*cw**2) + 2*cb*complex(0,1)*l5*sa*sb*vev2 - (ca*ee**2*complex(0,1)*sb**2*vev2)/(2.*cw**2) - ca*complex(0,1)*l2*sb**2*vev2 + (cb*ee**2*complex(0,1)*sa*sb*vev2)/(2.*sw**2) - (ca*ee**2*complex(0,1)*sb**2*vev2)/(2.*sw**2)',
                  order = {'QED':1})

GC_301 = Coupling(name = 'GC_301',
                  value = '-(cb**2*ee**2*sb*vev1)/(4.*cw**2) - (ee**2*sb**3*vev1)/(4.*cw**2) + (cb**2*ee**2*sb*vev1)/(4.*sw**2) + (ee**2*sb**3*vev1)/(4.*sw**2) + (cb**3*ee**2*vev2)/(4.*cw**2) + (cb*ee**2*sb**2*vev2)/(4.*cw**2) - (cb**3*ee**2*vev2)/(4.*sw**2) - (cb*ee**2*sb**2*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_302 = Coupling(name = 'GC_302',
                  value = '-(cb**2*l4*sb*vev1)/2. + (cb**2*l5*sb*vev1)/2. - (l4*sb**3*vev1)/2. + (l5*sb**3*vev1)/2. + (cb**2*ee**2*sb*vev1)/(4.*sw**2) + (ee**2*sb**3*vev1)/(4.*sw**2) + (cb**3*l4*vev2)/2. - (cb**3*l5*vev2)/2. + (cb*l4*sb**2*vev2)/2. - (cb*l5*sb**2*vev2)/2. - (cb**3*ee**2*vev2)/(4.*sw**2) - (cb*ee**2*sb**2*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_303 = Coupling(name = 'GC_303',
                  value = '(cb**2*l4*sb*vev1)/2. - (cb**2*l5*sb*vev1)/2. + (l4*sb**3*vev1)/2. - (l5*sb**3*vev1)/2. + (cb**2*ee**2*sb*vev1)/(4.*sw**2) + (ee**2*sb**3*vev1)/(4.*sw**2) - (cb**3*l4*vev2)/2. + (cb**3*l5*vev2)/2. - (cb*l4*sb**2*vev2)/2. + (cb*l5*sb**2*vev2)/2. - (cb**3*ee**2*vev2)/(4.*sw**2) - (cb*ee**2*sb**2*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_304 = Coupling(name = 'GC_304',
                  value = '(cb**2*ee**2*sb*vev1)/(4.*cw**2) + (ee**2*sb**3*vev1)/(4.*cw**2) - (cb**2*ee**2*sb*vev1)/(4.*sw**2) - (ee**2*sb**3*vev1)/(4.*sw**2) - (cb**3*ee**2*vev2)/(4.*cw**2) - (cb*ee**2*sb**2*vev2)/(4.*cw**2) + (cb**3*ee**2*vev2)/(4.*sw**2) + (cb*ee**2*sb**2*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_305 = Coupling(name = 'GC_305',
                  value = '-(cb**2*l4*sb*vev1)/2. + (cb**2*l5*sb*vev1)/2. - (l4*sb**3*vev1)/2. + (l5*sb**3*vev1)/2. - (cb**2*ee**2*sb*vev1)/(4.*sw**2) - (ee**2*sb**3*vev1)/(4.*sw**2) + (cb**3*l4*vev2)/2. - (cb**3*l5*vev2)/2. + (cb*l4*sb**2*vev2)/2. - (cb*l5*sb**2*vev2)/2. + (cb**3*ee**2*vev2)/(4.*sw**2) + (cb*ee**2*sb**2*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_306 = Coupling(name = 'GC_306',
                  value = '(cb**2*l4*sb*vev1)/2. - (cb**2*l5*sb*vev1)/2. + (l4*sb**3*vev1)/2. - (l5*sb**3*vev1)/2. - (cb**2*ee**2*sb*vev1)/(4.*sw**2) - (ee**2*sb**3*vev1)/(4.*sw**2) - (cb**3*l4*vev2)/2. + (cb**3*l5*vev2)/2. - (cb*l4*sb**2*vev2)/2. + (cb*l5*sb**2*vev2)/2. + (cb**3*ee**2*vev2)/(4.*sw**2) + (cb*ee**2*sb**2*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_307 = Coupling(name = 'GC_307',
                  value = '-(ca*cb**2*complex(0,1)*l4*vev1)/2. - (ca*cb**2*complex(0,1)*l5*vev1)/2. - cb*complex(0,1)*l1*sa*sb*vev1 + cb*complex(0,1)*l3*sa*sb*vev1 + (ca*complex(0,1)*l4*sb**2*vev1)/2. + (ca*complex(0,1)*l5*sb**2*vev1)/2. - (ca*cb**2*ee**2*complex(0,1)*vev1)/(4.*sw**2) - (cb*ee**2*complex(0,1)*sa*sb*vev1)/(2.*sw**2) + (ca*ee**2*complex(0,1)*sb**2*vev1)/(4.*sw**2) + (cb**2*complex(0,1)*l4*sa*vev2)/2. + (cb**2*complex(0,1)*l5*sa*vev2)/2. - ca*cb*complex(0,1)*l2*sb*vev2 + ca*cb*complex(0,1)*l3*sb*vev2 - (complex(0,1)*l4*sa*sb**2*vev2)/2. - (complex(0,1)*l5*sa*sb**2*vev2)/2. + (cb**2*ee**2*complex(0,1)*sa*vev2)/(4.*sw**2) - (ca*cb*ee**2*complex(0,1)*sb*vev2)/(2.*sw**2) - (ee**2*complex(0,1)*sa*sb**2*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_308 = Coupling(name = 'GC_308',
                  value = '-(ca*cb**2*ee**2*complex(0,1)*vev1)/(4.*cw**2) - ca*cb**2*complex(0,1)*l5*vev1 - (cb*ee**2*complex(0,1)*sa*sb*vev1)/(2.*cw**2) - cb*complex(0,1)*l1*sa*sb*vev1 + cb*complex(0,1)*l3*sa*sb*vev1 + cb*complex(0,1)*l4*sa*sb*vev1 - cb*complex(0,1)*l5*sa*sb*vev1 + (ca*ee**2*complex(0,1)*sb**2*vev1)/(4.*cw**2) + ca*complex(0,1)*l5*sb**2*vev1 - (ca*cb**2*ee**2*complex(0,1)*vev1)/(4.*sw**2) - (cb*ee**2*complex(0,1)*sa*sb*vev1)/(2.*sw**2) + (ca*ee**2*complex(0,1)*sb**2*vev1)/(4.*sw**2) + (cb**2*ee**2*complex(0,1)*sa*vev2)/(4.*cw**2) + cb**2*complex(0,1)*l5*sa*vev2 - (ca*cb*ee**2*complex(0,1)*sb*vev2)/(2.*cw**2) - ca*cb*complex(0,1)*l2*sb*vev2 + ca*cb*complex(0,1)*l3*sb*vev2 + ca*cb*complex(0,1)*l4*sb*vev2 - ca*cb*complex(0,1)*l5*sb*vev2 - (ee**2*complex(0,1)*sa*sb**2*vev2)/(4.*cw**2) - complex(0,1)*l5*sa*sb**2*vev2 + (cb**2*ee**2*complex(0,1)*sa*vev2)/(4.*sw**2) - (ca*cb*ee**2*complex(0,1)*sb*vev2)/(2.*sw**2) - (ee**2*complex(0,1)*sa*sb**2*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_309 = Coupling(name = 'GC_309',
                  value = '-(ca*cb**2*complex(0,1)*l1*vev1) - cb*complex(0,1)*l4*sa*sb*vev1 - cb*complex(0,1)*l5*sa*sb*vev1 - ca*complex(0,1)*l3*sb**2*vev1 + (ca*cb**2*ee**2*complex(0,1)*vev1)/(4.*sw**2) + (cb*ee**2*complex(0,1)*sa*sb*vev1)/(4.*sw**2) - cb**2*complex(0,1)*l3*sa*vev2 - ca*cb*complex(0,1)*l4*sb*vev2 - ca*cb*complex(0,1)*l5*sb*vev2 - complex(0,1)*l2*sa*sb**2*vev2 + (ca*cb*ee**2*complex(0,1)*sb*vev2)/(4.*sw**2) + (ee**2*complex(0,1)*sa*sb**2*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_310 = Coupling(name = 'GC_310',
                  value = '(ca*cb**2*ee**2*complex(0,1)*vev1)/(4.*cw**2) - ca*cb**2*complex(0,1)*l1*vev1 + (cb*ee**2*complex(0,1)*sa*sb*vev1)/(4.*cw**2) - 2*cb*complex(0,1)*l5*sa*sb*vev1 - ca*complex(0,1)*l3*sb**2*vev1 - ca*complex(0,1)*l4*sb**2*vev1 + ca*complex(0,1)*l5*sb**2*vev1 + (ca*cb**2*ee**2*complex(0,1)*vev1)/(4.*sw**2) + (cb*ee**2*complex(0,1)*sa*sb*vev1)/(4.*sw**2) - cb**2*complex(0,1)*l3*sa*vev2 - cb**2*complex(0,1)*l4*sa*vev2 + cb**2*complex(0,1)*l5*sa*vev2 + (ca*cb*ee**2*complex(0,1)*sb*vev2)/(4.*cw**2) - 2*ca*cb*complex(0,1)*l5*sb*vev2 + (ee**2*complex(0,1)*sa*sb**2*vev2)/(4.*cw**2) - complex(0,1)*l2*sa*sb**2*vev2 + (ca*cb*ee**2*complex(0,1)*sb*vev2)/(4.*sw**2) + (ee**2*complex(0,1)*sa*sb**2*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_311 = Coupling(name = 'GC_311',
                  value = '-(ca*cb**2*complex(0,1)*l4*vev1)/2. - (ca*cb**2*complex(0,1)*l5*vev1)/2. - cb*complex(0,1)*l1*sa*sb*vev1 + cb*complex(0,1)*l3*sa*sb*vev1 + (ca*complex(0,1)*l4*sb**2*vev1)/2. + (ca*complex(0,1)*l5*sb**2*vev1)/2. + (ca*cb**2*ee**2*complex(0,1)*vev1)/(4.*sw**2) + (cb*ee**2*complex(0,1)*sa*sb*vev1)/(4.*sw**2) + (cb**2*complex(0,1)*l4*sa*vev2)/2. + (cb**2*complex(0,1)*l5*sa*vev2)/2. - ca*cb*complex(0,1)*l2*sb*vev2 + ca*cb*complex(0,1)*l3*sb*vev2 - (complex(0,1)*l4*sa*sb**2*vev2)/2. - (complex(0,1)*l5*sa*sb**2*vev2)/2. + (ca*cb*ee**2*complex(0,1)*sb*vev2)/(4.*sw**2) + (ee**2*complex(0,1)*sa*sb**2*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_312 = Coupling(name = 'GC_312',
                  value = '(ca*cb**2*ee**2*complex(0,1)*vev1)/(4.*cw**2) - ca*cb**2*complex(0,1)*l5*vev1 + (cb*ee**2*complex(0,1)*sa*sb*vev1)/(4.*cw**2) - cb*complex(0,1)*l1*sa*sb*vev1 + cb*complex(0,1)*l3*sa*sb*vev1 + cb*complex(0,1)*l4*sa*sb*vev1 - cb*complex(0,1)*l5*sa*sb*vev1 + ca*complex(0,1)*l5*sb**2*vev1 + (ca*cb**2*ee**2*complex(0,1)*vev1)/(4.*sw**2) + (cb*ee**2*complex(0,1)*sa*sb*vev1)/(4.*sw**2) + cb**2*complex(0,1)*l5*sa*vev2 + (ca*cb*ee**2*complex(0,1)*sb*vev2)/(4.*cw**2) - ca*cb*complex(0,1)*l2*sb*vev2 + ca*cb*complex(0,1)*l3*sb*vev2 + ca*cb*complex(0,1)*l4*sb*vev2 - ca*cb*complex(0,1)*l5*sb*vev2 + (ee**2*complex(0,1)*sa*sb**2*vev2)/(4.*cw**2) - complex(0,1)*l5*sa*sb**2*vev2 + (ca*cb*ee**2*complex(0,1)*sb*vev2)/(4.*sw**2) + (ee**2*complex(0,1)*sa*sb**2*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_313 = Coupling(name = 'GC_313',
                  value = '-(ca*cb**2*complex(0,1)*l1*vev1) - cb*complex(0,1)*l4*sa*sb*vev1 - cb*complex(0,1)*l5*sa*sb*vev1 - ca*complex(0,1)*l3*sb**2*vev1 - (ca*cb**2*ee**2*complex(0,1)*vev1)/(2.*sw**2) - (cb*ee**2*complex(0,1)*sa*sb*vev1)/(2.*sw**2) - cb**2*complex(0,1)*l3*sa*vev2 - ca*cb*complex(0,1)*l4*sb*vev2 - ca*cb*complex(0,1)*l5*sb*vev2 - complex(0,1)*l2*sa*sb**2*vev2 - (ca*cb*ee**2*complex(0,1)*sb*vev2)/(2.*sw**2) - (ee**2*complex(0,1)*sa*sb**2*vev2)/(2.*sw**2)',
                  order = {'QED':1})

GC_314 = Coupling(name = 'GC_314',
                  value = '-(ca*cb**2*ee**2*complex(0,1)*vev1)/(2.*cw**2) - ca*cb**2*complex(0,1)*l1*vev1 - (cb*ee**2*complex(0,1)*sa*sb*vev1)/(2.*cw**2) - 2*cb*complex(0,1)*l5*sa*sb*vev1 - ca*complex(0,1)*l3*sb**2*vev1 - ca*complex(0,1)*l4*sb**2*vev1 + ca*complex(0,1)*l5*sb**2*vev1 - (ca*cb**2*ee**2*complex(0,1)*vev1)/(2.*sw**2) - (cb*ee**2*complex(0,1)*sa*sb*vev1)/(2.*sw**2) - cb**2*complex(0,1)*l3*sa*vev2 - cb**2*complex(0,1)*l4*sa*vev2 + cb**2*complex(0,1)*l5*sa*vev2 - (ca*cb*ee**2*complex(0,1)*sb*vev2)/(2.*cw**2) - 2*ca*cb*complex(0,1)*l5*sb*vev2 - (ee**2*complex(0,1)*sa*sb**2*vev2)/(2.*cw**2) - complex(0,1)*l2*sa*sb**2*vev2 - (ca*cb*ee**2*complex(0,1)*sb*vev2)/(2.*sw**2) - (ee**2*complex(0,1)*sa*sb**2*vev2)/(2.*sw**2)',
                  order = {'QED':1})

GC_315 = Coupling(name = 'GC_315',
                  value = '(cb**3*ee**2*vev1)/(4.*cw**2) + (cb*ee**2*sb**2*vev1)/(4.*cw**2) - (cb**3*ee**2*vev1)/(4.*sw**2) - (cb*ee**2*sb**2*vev1)/(4.*sw**2) + (cb**2*ee**2*sb*vev2)/(4.*cw**2) + (ee**2*sb**3*vev2)/(4.*cw**2) - (cb**2*ee**2*sb*vev2)/(4.*sw**2) - (ee**2*sb**3*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_316 = Coupling(name = 'GC_316',
                  value = '(cb**3*l4*vev1)/2. - (cb**3*l5*vev1)/2. + (cb*l4*sb**2*vev1)/2. - (cb*l5*sb**2*vev1)/2. - (cb**3*ee**2*vev1)/(4.*sw**2) - (cb*ee**2*sb**2*vev1)/(4.*sw**2) + (cb**2*l4*sb*vev2)/2. - (cb**2*l5*sb*vev2)/2. + (l4*sb**3*vev2)/2. - (l5*sb**3*vev2)/2. - (cb**2*ee**2*sb*vev2)/(4.*sw**2) - (ee**2*sb**3*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_317 = Coupling(name = 'GC_317',
                  value = '-(cb**3*l4*vev1)/2. + (cb**3*l5*vev1)/2. - (cb*l4*sb**2*vev1)/2. + (cb*l5*sb**2*vev1)/2. - (cb**3*ee**2*vev1)/(4.*sw**2) - (cb*ee**2*sb**2*vev1)/(4.*sw**2) - (cb**2*l4*sb*vev2)/2. + (cb**2*l5*sb*vev2)/2. - (l4*sb**3*vev2)/2. + (l5*sb**3*vev2)/2. - (cb**2*ee**2*sb*vev2)/(4.*sw**2) - (ee**2*sb**3*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_318 = Coupling(name = 'GC_318',
                  value = '-(cb**3*ee**2*vev1)/(4.*cw**2) - (cb*ee**2*sb**2*vev1)/(4.*cw**2) + (cb**3*ee**2*vev1)/(4.*sw**2) + (cb*ee**2*sb**2*vev1)/(4.*sw**2) - (cb**2*ee**2*sb*vev2)/(4.*cw**2) - (ee**2*sb**3*vev2)/(4.*cw**2) + (cb**2*ee**2*sb*vev2)/(4.*sw**2) + (ee**2*sb**3*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_319 = Coupling(name = 'GC_319',
                  value = '(cb**3*l4*vev1)/2. - (cb**3*l5*vev1)/2. + (cb*l4*sb**2*vev1)/2. - (cb*l5*sb**2*vev1)/2. + (cb**3*ee**2*vev1)/(4.*sw**2) + (cb*ee**2*sb**2*vev1)/(4.*sw**2) + (cb**2*l4*sb*vev2)/2. - (cb**2*l5*sb*vev2)/2. + (l4*sb**3*vev2)/2. - (l5*sb**3*vev2)/2. + (cb**2*ee**2*sb*vev2)/(4.*sw**2) + (ee**2*sb**3*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_320 = Coupling(name = 'GC_320',
                  value = '-(cb**3*l4*vev1)/2. + (cb**3*l5*vev1)/2. - (cb*l4*sb**2*vev1)/2. + (cb*l5*sb**2*vev1)/2. + (cb**3*ee**2*vev1)/(4.*sw**2) + (cb*ee**2*sb**2*vev1)/(4.*sw**2) - (cb**2*l4*sb*vev2)/2. + (cb**2*l5*sb*vev2)/2. - (l4*sb**3*vev2)/2. + (l5*sb**3*vev2)/2. + (cb**2*ee**2*sb*vev2)/(4.*sw**2) + (ee**2*sb**3*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_321 = Coupling(name = 'GC_321',
                  value = '(ee**2*complex(0,1)*sb*vev1)/(2.*sw) - (cb*ee**2*complex(0,1)*vev2)/(2.*sw)',
                  order = {'QED':1})

GC_322 = Coupling(name = 'GC_322',
                  value = '-(ee**2*complex(0,1)*sb*vev1)/(2.*sw) + (cb*ee**2*complex(0,1)*vev2)/(2.*sw)',
                  order = {'QED':1})

GC_323 = Coupling(name = 'GC_323',
                  value = '(ee**2*complex(0,1)*sb*vev1)/sw - (cb*ee**2*complex(0,1)*vev2)/sw',
                  order = {'QED':1})

GC_324 = Coupling(name = 'GC_324',
                  value = '-(cb*ee**2*complex(0,1)*vev1)/(2.*sw) - (ee**2*complex(0,1)*sb*vev2)/(2.*sw)',
                  order = {'QED':1})

GC_325 = Coupling(name = 'GC_325',
                  value = '(cb*ee**2*complex(0,1)*vev1)/(2.*sw) + (ee**2*complex(0,1)*sb*vev2)/(2.*sw)',
                  order = {'QED':1})

GC_326 = Coupling(name = 'GC_326',
                  value = '-((cb*ee**2*complex(0,1)*vev1)/sw) - (ee**2*complex(0,1)*sb*vev2)/sw',
                  order = {'QED':1})

GC_327 = Coupling(name = 'GC_327',
                  value = '(ee**2*complex(0,1)*sa*vev1)/2. + (cw**2*ee**2*complex(0,1)*sa*vev1)/(4.*sw**2) + (ee**2*complex(0,1)*sa*sw**2*vev1)/(4.*cw**2) - (ca*ee**2*complex(0,1)*vev2)/2. - (ca*cw**2*ee**2*complex(0,1)*vev2)/(4.*sw**2) - (ca*ee**2*complex(0,1)*sw**2*vev2)/(4.*cw**2)',
                  order = {'QED':1})

GC_328 = Coupling(name = 'GC_328',
                  value = 'ee**2*complex(0,1)*sa*vev1 + (cw**2*ee**2*complex(0,1)*sa*vev1)/(2.*sw**2) + (ee**2*complex(0,1)*sa*sw**2*vev1)/(2.*cw**2) - ca*ee**2*complex(0,1)*vev2 - (ca*cw**2*ee**2*complex(0,1)*vev2)/(2.*sw**2) - (ca*ee**2*complex(0,1)*sw**2*vev2)/(2.*cw**2)',
                  order = {'QED':1})

GC_329 = Coupling(name = 'GC_329',
                  value = '-(ee**2*complex(0,1)*sa*vev1) - (cw**2*ee**2*complex(0,1)*sa*vev1)/(2.*sw**2) - (ee**2*complex(0,1)*sa*sw**2*vev1)/(2.*cw**2) + ca*ee**2*complex(0,1)*vev2 + (ca*cw**2*ee**2*complex(0,1)*vev2)/(2.*sw**2) + (ca*ee**2*complex(0,1)*sw**2*vev2)/(2.*cw**2)',
                  order = {'QED':1})

GC_330 = Coupling(name = 'GC_330',
                  value = '-(ca*ee**2*complex(0,1)*vev1)/2. - (ca*cw**2*ee**2*complex(0,1)*vev1)/(4.*sw**2) - (ca*ee**2*complex(0,1)*sw**2*vev1)/(4.*cw**2) - (ee**2*complex(0,1)*sa*vev2)/2. - (cw**2*ee**2*complex(0,1)*sa*vev2)/(4.*sw**2) - (ee**2*complex(0,1)*sa*sw**2*vev2)/(4.*cw**2)',
                  order = {'QED':1})

GC_331 = Coupling(name = 'GC_331',
                  value = '-(ca*ee**2*complex(0,1)*vev1) - (ca*cw**2*ee**2*complex(0,1)*vev1)/(2.*sw**2) - (ca*ee**2*complex(0,1)*sw**2*vev1)/(2.*cw**2) - ee**2*complex(0,1)*sa*vev2 - (cw**2*ee**2*complex(0,1)*sa*vev2)/(2.*sw**2) - (ee**2*complex(0,1)*sa*sw**2*vev2)/(2.*cw**2)',
                  order = {'QED':1})

GC_332 = Coupling(name = 'GC_332',
                  value = 'ca*ee**2*complex(0,1)*vev1 + (ca*cw**2*ee**2*complex(0,1)*vev1)/(2.*sw**2) + (ca*ee**2*complex(0,1)*sw**2*vev1)/(2.*cw**2) + ee**2*complex(0,1)*sa*vev2 + (cw**2*ee**2*complex(0,1)*sa*vev2)/(2.*sw**2) + (ee**2*complex(0,1)*sa*sw**2*vev2)/(2.*cw**2)',
                  order = {'QED':1})

GC_333 = Coupling(name = 'GC_333',
                  value = '(complex(0,1)*sa*yc1)/cmath.sqrt(2) - (ca*complex(0,1)*yc2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_334 = Coupling(name = 'GC_334',
                  value = '-(complex(0,1)*sb*yc1) + cb*complex(0,1)*yc2',
                  order = {'QED':1})

GC_335 = Coupling(name = 'GC_335',
                  value = '-((sb*yc1)/cmath.sqrt(2)) + (cb*yc2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_336 = Coupling(name = 'GC_336',
                  value = '-((ca*complex(0,1)*yc1)/cmath.sqrt(2)) - (complex(0,1)*sa*yc2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_337 = Coupling(name = 'GC_337',
                  value = 'cb*complex(0,1)*yc1 + complex(0,1)*sb*yc2',
                  order = {'QED':1})

GC_338 = Coupling(name = 'GC_338',
                  value = '(cb*yc1)/cmath.sqrt(2) + (sb*yc2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_339 = Coupling(name = 'GC_339',
                  value = '-((complex(0,1)*sa*yb2)/cmath.sqrt(2)) - (ca*complex(0,1)*yd13x3)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_340 = Coupling(name = 'GC_340',
                  value = '-(complex(0,1)*sb*yb2) - cb*complex(0,1)*yd13x3',
                  order = {'QED':1})

GC_341 = Coupling(name = 'GC_341',
                  value = '-((sb*yb2)/cmath.sqrt(2)) - (cb*yd13x3)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_342 = Coupling(name = 'GC_342',
                  value = '-((ca*complex(0,1)*yb2)/cmath.sqrt(2)) + (complex(0,1)*sa*yd13x3)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_343 = Coupling(name = 'GC_343',
                  value = '-(cb*complex(0,1)*yb2) + complex(0,1)*sb*yd13x3',
                  order = {'QED':1})

GC_344 = Coupling(name = 'GC_344',
                  value = '-((cb*yb2)/cmath.sqrt(2)) + (sb*yd13x3)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_345 = Coupling(name = 'GC_345',
                  value = '(complex(0,1)*sa*ydo1)/cmath.sqrt(2) - (ca*complex(0,1)*ydo2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_346 = Coupling(name = 'GC_346',
                  value = 'complex(0,1)*sb*ydo1 - cb*complex(0,1)*ydo2',
                  order = {'QED':1})

GC_347 = Coupling(name = 'GC_347',
                  value = '(sb*ydo1)/cmath.sqrt(2) - (cb*ydo2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_348 = Coupling(name = 'GC_348',
                  value = '-((ca*complex(0,1)*ydo1)/cmath.sqrt(2)) - (complex(0,1)*sa*ydo2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_349 = Coupling(name = 'GC_349',
                  value = '-(cb*complex(0,1)*ydo1) - complex(0,1)*sb*ydo2',
                  order = {'QED':1})

GC_350 = Coupling(name = 'GC_350',
                  value = '-((cb*ydo1)/cmath.sqrt(2)) - (sb*ydo2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_351 = Coupling(name = 'GC_351',
                  value = '(complex(0,1)*sa*ye1)/cmath.sqrt(2) - (ca*complex(0,1)*ye2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_352 = Coupling(name = 'GC_352',
                  value = 'complex(0,1)*sb*ye1 - cb*complex(0,1)*ye2',
                  order = {'QED':1})

GC_353 = Coupling(name = 'GC_353',
                  value = '(sb*ye1)/cmath.sqrt(2) - (cb*ye2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_354 = Coupling(name = 'GC_354',
                  value = '-((ca*complex(0,1)*ye1)/cmath.sqrt(2)) - (complex(0,1)*sa*ye2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_355 = Coupling(name = 'GC_355',
                  value = '-(cb*complex(0,1)*ye1) - complex(0,1)*sb*ye2',
                  order = {'QED':1})

GC_356 = Coupling(name = 'GC_356',
                  value = '-((cb*ye1)/cmath.sqrt(2)) - (sb*ye2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_357 = Coupling(name = 'GC_357',
                  value = '(complex(0,1)*sa*ym1)/cmath.sqrt(2) - (ca*complex(0,1)*ym2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_358 = Coupling(name = 'GC_358',
                  value = 'complex(0,1)*sb*ym1 - cb*complex(0,1)*ym2',
                  order = {'QED':1})

GC_359 = Coupling(name = 'GC_359',
                  value = '(sb*ym1)/cmath.sqrt(2) - (cb*ym2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_360 = Coupling(name = 'GC_360',
                  value = '-((ca*complex(0,1)*ym1)/cmath.sqrt(2)) - (complex(0,1)*sa*ym2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_361 = Coupling(name = 'GC_361',
                  value = '-(cb*complex(0,1)*ym1) - complex(0,1)*sb*ym2',
                  order = {'QED':1})

GC_362 = Coupling(name = 'GC_362',
                  value = '-((cb*ym1)/cmath.sqrt(2)) - (sb*ym2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_363 = Coupling(name = 'GC_363',
                  value = '(complex(0,1)*sa*yd12x2)/cmath.sqrt(2) - (ca*complex(0,1)*ys2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_364 = Coupling(name = 'GC_364',
                  value = 'complex(0,1)*sb*yd12x2 - cb*complex(0,1)*ys2',
                  order = {'QED':1})

GC_365 = Coupling(name = 'GC_365',
                  value = '(sb*yd12x2)/cmath.sqrt(2) - (cb*ys2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_366 = Coupling(name = 'GC_366',
                  value = '-((ca*complex(0,1)*yd12x2)/cmath.sqrt(2)) - (complex(0,1)*sa*ys2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_367 = Coupling(name = 'GC_367',
                  value = '-(cb*complex(0,1)*yd12x2) - complex(0,1)*sb*ys2',
                  order = {'QED':1})

GC_368 = Coupling(name = 'GC_368',
                  value = '-((cb*yd12x2)/cmath.sqrt(2)) - (sb*ys2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_369 = Coupling(name = 'GC_369',
                  value = '(complex(0,1)*sa*yt1)/cmath.sqrt(2) - (ca*complex(0,1)*yt2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_370 = Coupling(name = 'GC_370',
                  value = '-(complex(0,1)*sb*yt1) + cb*complex(0,1)*yt2',
                  order = {'QED':1})

GC_371 = Coupling(name = 'GC_371',
                  value = '-((sb*yt1)/cmath.sqrt(2)) + (cb*yt2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_372 = Coupling(name = 'GC_372',
                  value = '-((ca*complex(0,1)*yt1)/cmath.sqrt(2)) - (complex(0,1)*sa*yt2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_373 = Coupling(name = 'GC_373',
                  value = 'cb*complex(0,1)*yt1 + complex(0,1)*sb*yt2',
                  order = {'QED':1})

GC_374 = Coupling(name = 'GC_374',
                  value = '(cb*yt1)/cmath.sqrt(2) + (sb*yt2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_375 = Coupling(name = 'GC_375',
                  value = '(complex(0,1)*sa*ytau1)/cmath.sqrt(2) - (ca*complex(0,1)*ytau2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_376 = Coupling(name = 'GC_376',
                  value = 'complex(0,1)*sb*ytau1 - cb*complex(0,1)*ytau2',
                  order = {'QED':1})

GC_377 = Coupling(name = 'GC_377',
                  value = '(sb*ytau1)/cmath.sqrt(2) - (cb*ytau2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_378 = Coupling(name = 'GC_378',
                  value = '-((ca*complex(0,1)*ytau1)/cmath.sqrt(2)) - (complex(0,1)*sa*ytau2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_379 = Coupling(name = 'GC_379',
                  value = '-(cb*complex(0,1)*ytau1) - complex(0,1)*sb*ytau2',
                  order = {'QED':1})

GC_380 = Coupling(name = 'GC_380',
                  value = '-((cb*ytau1)/cmath.sqrt(2)) - (sb*ytau2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_381 = Coupling(name = 'GC_381',
                  value = '(complex(0,1)*sa*yup1)/cmath.sqrt(2) - (ca*complex(0,1)*yup2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_382 = Coupling(name = 'GC_382',
                  value = '-(complex(0,1)*sb*yup1) + cb*complex(0,1)*yup2',
                  order = {'QED':1})

GC_383 = Coupling(name = 'GC_383',
                  value = '-((sb*yup1)/cmath.sqrt(2)) + (cb*yup2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_384 = Coupling(name = 'GC_384',
                  value = '-((ca*complex(0,1)*yup1)/cmath.sqrt(2)) - (complex(0,1)*sa*yup2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_385 = Coupling(name = 'GC_385',
                  value = 'cb*complex(0,1)*yup1 + complex(0,1)*sb*yup2',
                  order = {'QED':1})

GC_386 = Coupling(name = 'GC_386',
                  value = '(cb*yup1)/cmath.sqrt(2) + (sb*yup2)/cmath.sqrt(2)',
                  order = {'QED':1})

