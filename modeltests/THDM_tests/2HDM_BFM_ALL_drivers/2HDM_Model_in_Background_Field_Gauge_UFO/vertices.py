# This file was automatically created by FeynRules 2.3.23
# Mathematica version: 9.0 for Linux x86 (64-bit) (November 20, 2012)
# Date: Tue 22 Aug 2017 21:01:32


from .object_library import all_vertices, Vertex
from . import particles as P
from . import couplings as C
from . import lorentz as L


V_1 = Vertex(name = 'V_1',
             particles = [ P.ghZ, P.ghZ__tilde__, P.G0, P.G0 ],
             color = [ '1' ],
             lorentz = [ L.UUSS1 ],
             couplings = {(0,0):C.GC_227})

V_2 = Vertex(name = 'V_2',
             particles = [ P.ghWp, P.ghZ__tilde__, P.G0, P.G__minus__ ],
             color = [ '1' ],
             lorentz = [ L.UUSS1 ],
             couplings = {(0,0):C.GC_31})

V_3 = Vertex(name = 'V_3',
             particles = [ P.ghZ, P.ghWm__tilde__, P.G0, P.G__minus__ ],
             color = [ '1' ],
             lorentz = [ L.UUSS1 ],
             couplings = {(0,0):C.GC_31})

V_4 = Vertex(name = 'V_4',
             particles = [ P.ghWm, P.ghZ__tilde__, P.G0, P.G__plus__ ],
             color = [ '1' ],
             lorentz = [ L.UUSS1 ],
             couplings = {(0,0):C.GC_30})

V_5 = Vertex(name = 'V_5',
             particles = [ P.ghZ, P.ghWp__tilde__, P.G0, P.G__plus__ ],
             color = [ '1' ],
             lorentz = [ L.UUSS1 ],
             couplings = {(0,0):C.GC_30})

V_6 = Vertex(name = 'V_6',
             particles = [ P.ghA, P.ghA__tilde__, P.G__minus__, P.G__plus__ ],
             color = [ '1' ],
             lorentz = [ L.UUSS1 ],
             couplings = {(0,0):C.GC_28})

V_7 = Vertex(name = 'V_7',
             particles = [ P.ghZ, P.ghZ__tilde__, P.G__minus__, P.G__plus__ ],
             color = [ '1' ],
             lorentz = [ L.UUSS1 ],
             couplings = {(0,0):C.GC_228})

V_8 = Vertex(name = 'V_8',
             particles = [ P.ghZ, P.ghZ__tilde__, P.G0, P.GQ0 ],
             color = [ '1' ],
             lorentz = [ L.UUSS1 ],
             couplings = {(0,0):C.GC_225})

V_9 = Vertex(name = 'V_9',
             particles = [ P.ghWp, P.ghZ__tilde__, P.G__minus__, P.GQ0 ],
             color = [ '1' ],
             lorentz = [ L.UUSS1 ],
             couplings = {(0,0):C.GC_117})

V_10 = Vertex(name = 'V_10',
              particles = [ P.ghZ, P.ghWm__tilde__, P.G__minus__, P.GQ0 ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_115})

V_11 = Vertex(name = 'V_11',
              particles = [ P.ghWm, P.ghZ__tilde__, P.G__plus__, P.GQ0 ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_114})

V_12 = Vertex(name = 'V_12',
              particles = [ P.ghZ, P.ghWp__tilde__, P.G__plus__, P.GQ0 ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_116})

V_13 = Vertex(name = 'V_13',
              particles = [ P.ghWp, P.ghZ__tilde__, P.G0, P.GQ__minus__ ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_115})

V_14 = Vertex(name = 'V_14',
              particles = [ P.ghZ, P.ghWm__tilde__, P.G0, P.GQ__minus__ ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_117})

V_15 = Vertex(name = 'V_15',
              particles = [ P.ghA, P.ghA__tilde__, P.G__plus__, P.GQ__minus__ ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_27})

V_16 = Vertex(name = 'V_16',
              particles = [ P.ghZ, P.ghZ__tilde__, P.G__plus__, P.GQ__minus__ ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_226})

V_17 = Vertex(name = 'V_17',
              particles = [ P.G__plus__, P.G__plus__, P.GQ__minus__, P.GQ__minus__ ],
              color = [ '1' ],
              lorentz = [ L.SSSS1 ],
              couplings = {(0,0):C.GC_171})

V_18 = Vertex(name = 'V_18',
              particles = [ P.ghWm, P.ghZ__tilde__, P.G0, P.GQ__plus__ ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_116})

V_19 = Vertex(name = 'V_19',
              particles = [ P.ghZ, P.ghWp__tilde__, P.G0, P.GQ__plus__ ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_114})

V_20 = Vertex(name = 'V_20',
              particles = [ P.ghA, P.ghA__tilde__, P.G__minus__, P.GQ__plus__ ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_27})

V_21 = Vertex(name = 'V_21',
              particles = [ P.ghZ, P.ghZ__tilde__, P.G__minus__, P.GQ__plus__ ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_226})

V_22 = Vertex(name = 'V_22',
              particles = [ P.G__minus__, P.G__plus__, P.GQ__minus__, P.GQ__plus__ ],
              color = [ '1' ],
              lorentz = [ L.SSSS1 ],
              couplings = {(0,0):C.GC_161})

V_23 = Vertex(name = 'V_23',
              particles = [ P.G__minus__, P.G__minus__, P.GQ__plus__, P.GQ__plus__ ],
              color = [ '1' ],
              lorentz = [ L.SSSS1 ],
              couplings = {(0,0):C.GC_171})

V_24 = Vertex(name = 'V_24',
              particles = [ P.ghZ, P.ghZ__tilde__, P.Ha, P.Ha ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_227})

V_25 = Vertex(name = 'V_25',
              particles = [ P.ghZ, P.ghZ__tilde__, P.Ha, P.HaQ ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_225})

V_26 = Vertex(name = 'V_26',
              particles = [ P.ghWp, P.ghZ__tilde__, P.G__minus__, P.Hh ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_24})

V_27 = Vertex(name = 'V_27',
              particles = [ P.ghZ, P.ghWm__tilde__, P.G__minus__, P.Hh ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_24})

V_28 = Vertex(name = 'V_28',
              particles = [ P.ghWm, P.ghZ__tilde__, P.G__plus__, P.Hh ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_24})

V_29 = Vertex(name = 'V_29',
              particles = [ P.ghZ, P.ghWp__tilde__, P.G__plus__, P.Hh ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_24})

V_30 = Vertex(name = 'V_30',
              particles = [ P.ghWp, P.ghZ__tilde__, P.GQ__minus__, P.Hh ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_94})

V_31 = Vertex(name = 'V_31',
              particles = [ P.ghZ, P.ghWm__tilde__, P.GQ__minus__, P.Hh ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_95})

V_32 = Vertex(name = 'V_32',
              particles = [ P.G__plus__, P.GQ0, P.GQ__minus__, P.Hh ],
              color = [ '1' ],
              lorentz = [ L.SSSS1 ],
              couplings = {(0,0):C.GC_60})

V_33 = Vertex(name = 'V_33',
              particles = [ P.ghWm, P.ghZ__tilde__, P.GQ__plus__, P.Hh ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_94})

V_34 = Vertex(name = 'V_34',
              particles = [ P.ghZ, P.ghWp__tilde__, P.GQ__plus__, P.Hh ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_95})

V_35 = Vertex(name = 'V_35',
              particles = [ P.G__minus__, P.GQ0, P.GQ__plus__, P.Hh ],
              color = [ '1' ],
              lorentz = [ L.SSSS1 ],
              couplings = {(0,0):C.GC_61})

V_36 = Vertex(name = 'V_36',
              particles = [ P.ghZ, P.ghZ__tilde__, P.Hh, P.Hh ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_223})

V_37 = Vertex(name = 'V_37',
              particles = [ P.GQ0, P.GQ0, P.Hh, P.Hh ],
              color = [ '1' ],
              lorentz = [ L.SSSS1 ],
              couplings = {(0,0):C.GC_147})

V_38 = Vertex(name = 'V_38',
              particles = [ P.ghWp, P.ghZ__tilde__, P.G__minus__, P.HhQ ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_95})

V_39 = Vertex(name = 'V_39',
              particles = [ P.ghZ, P.ghWm__tilde__, P.G__minus__, P.HhQ ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_94})

V_40 = Vertex(name = 'V_40',
              particles = [ P.ghWm, P.ghZ__tilde__, P.G__plus__, P.HhQ ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_95})

V_41 = Vertex(name = 'V_41',
              particles = [ P.ghZ, P.ghWp__tilde__, P.G__plus__, P.HhQ ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_94})

V_42 = Vertex(name = 'V_42',
              particles = [ P.G0, P.G__plus__, P.GQ__minus__, P.HhQ ],
              color = [ '1' ],
              lorentz = [ L.SSSS1 ],
              couplings = {(0,0):C.GC_61})

V_43 = Vertex(name = 'V_43',
              particles = [ P.G0, P.G__minus__, P.GQ__plus__, P.HhQ ],
              color = [ '1' ],
              lorentz = [ L.SSSS1 ],
              couplings = {(0,0):C.GC_60})

V_44 = Vertex(name = 'V_44',
              particles = [ P.ghZ, P.ghZ__tilde__, P.Hh, P.HhQ ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_222})

V_45 = Vertex(name = 'V_45',
              particles = [ P.G0, P.GQ0, P.Hh, P.HhQ ],
              color = [ '1' ],
              lorentz = [ L.SSSS1 ],
              couplings = {(0,0):C.GC_141})

V_46 = Vertex(name = 'V_46',
              particles = [ P.G0, P.G0, P.HhQ, P.HhQ ],
              color = [ '1' ],
              lorentz = [ L.SSSS1 ],
              couplings = {(0,0):C.GC_147})

V_47 = Vertex(name = 'V_47',
              particles = [ P.G__plus__, P.GQ__minus__, P.HaQ, P.Hl ],
              color = [ '1' ],
              lorentz = [ L.SSSS1 ],
              couplings = {(0,0):C.GC_157})

V_48 = Vertex(name = 'V_48',
              particles = [ P.G__minus__, P.GQ__plus__, P.HaQ, P.Hl ],
              color = [ '1' ],
              lorentz = [ L.SSSS1 ],
              couplings = {(0,0):C.GC_154})

V_49 = Vertex(name = 'V_49',
              particles = [ P.GQ0, P.HaQ, P.Hh, P.Hl ],
              color = [ '1' ],
              lorentz = [ L.SSSS1 ],
              couplings = {(0,0):C.GC_137})

V_50 = Vertex(name = 'V_50',
              particles = [ P.G0, P.HaQ, P.HhQ, P.Hl ],
              color = [ '1' ],
              lorentz = [ L.SSSS1 ],
              couplings = {(0,0):C.GC_143})

V_51 = Vertex(name = 'V_51',
              particles = [ P.ghZ, P.ghZ__tilde__, P.Hl, P.Hl ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_223})

V_52 = Vertex(name = 'V_52',
              particles = [ P.HaQ, P.HaQ, P.Hl, P.Hl ],
              color = [ '1' ],
              lorentz = [ L.SSSS1 ],
              couplings = {(0,0):C.GC_145})

V_53 = Vertex(name = 'V_53',
              particles = [ P.G__plus__, P.GQ__minus__, P.Ha, P.HlQ ],
              color = [ '1' ],
              lorentz = [ L.SSSS1 ],
              couplings = {(0,0):C.GC_154})

V_54 = Vertex(name = 'V_54',
              particles = [ P.G__minus__, P.GQ__plus__, P.Ha, P.HlQ ],
              color = [ '1' ],
              lorentz = [ L.SSSS1 ],
              couplings = {(0,0):C.GC_157})

V_55 = Vertex(name = 'V_55',
              particles = [ P.GQ0, P.Ha, P.Hh, P.HlQ ],
              color = [ '1' ],
              lorentz = [ L.SSSS1 ],
              couplings = {(0,0):C.GC_143})

V_56 = Vertex(name = 'V_56',
              particles = [ P.G0, P.Ha, P.HhQ, P.HlQ ],
              color = [ '1' ],
              lorentz = [ L.SSSS1 ],
              couplings = {(0,0):C.GC_137})

V_57 = Vertex(name = 'V_57',
              particles = [ P.ghZ, P.ghZ__tilde__, P.Hl, P.HlQ ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_222})

V_58 = Vertex(name = 'V_58',
              particles = [ P.Ha, P.HaQ, P.Hl, P.HlQ ],
              color = [ '1' ],
              lorentz = [ L.SSSS1 ],
              couplings = {(0,0):C.GC_139})

V_59 = Vertex(name = 'V_59',
              particles = [ P.Ha, P.Ha, P.HlQ, P.HlQ ],
              color = [ '1' ],
              lorentz = [ L.SSSS1 ],
              couplings = {(0,0):C.GC_145})

V_60 = Vertex(name = 'V_60',
              particles = [ P.ghWp, P.ghZ__tilde__, P.Ha, P.H__minus__ ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_31})

V_61 = Vertex(name = 'V_61',
              particles = [ P.ghZ, P.ghWm__tilde__, P.Ha, P.H__minus__ ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_31})

V_62 = Vertex(name = 'V_62',
              particles = [ P.ghWp, P.ghZ__tilde__, P.HaQ, P.H__minus__ ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_117})

V_63 = Vertex(name = 'V_63',
              particles = [ P.ghZ, P.ghWm__tilde__, P.HaQ, P.H__minus__ ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_115})

V_64 = Vertex(name = 'V_64',
              particles = [ P.ghWp, P.ghZ__tilde__, P.Hl, P.H__minus__ ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_24})

V_65 = Vertex(name = 'V_65',
              particles = [ P.ghZ, P.ghWm__tilde__, P.Hl, P.H__minus__ ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_24})

V_66 = Vertex(name = 'V_66',
              particles = [ P.ghWp, P.ghZ__tilde__, P.HlQ, P.H__minus__ ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_95})

V_67 = Vertex(name = 'V_67',
              particles = [ P.ghZ, P.ghWm__tilde__, P.HlQ, P.H__minus__ ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_94})

V_68 = Vertex(name = 'V_68',
              particles = [ P.ghWm, P.ghZ__tilde__, P.Ha, P.H__plus__ ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_30})

V_69 = Vertex(name = 'V_69',
              particles = [ P.ghZ, P.ghWp__tilde__, P.Ha, P.H__plus__ ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_30})

V_70 = Vertex(name = 'V_70',
              particles = [ P.ghWm, P.ghZ__tilde__, P.HaQ, P.H__plus__ ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_114})

V_71 = Vertex(name = 'V_71',
              particles = [ P.ghZ, P.ghWp__tilde__, P.HaQ, P.H__plus__ ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_116})

V_72 = Vertex(name = 'V_72',
              particles = [ P.ghWm, P.ghZ__tilde__, P.Hl, P.H__plus__ ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_24})

V_73 = Vertex(name = 'V_73',
              particles = [ P.ghZ, P.ghWp__tilde__, P.Hl, P.H__plus__ ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_24})

V_74 = Vertex(name = 'V_74',
              particles = [ P.ghWm, P.ghZ__tilde__, P.HlQ, P.H__plus__ ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_95})

V_75 = Vertex(name = 'V_75',
              particles = [ P.ghZ, P.ghWp__tilde__, P.HlQ, P.H__plus__ ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_94})

V_76 = Vertex(name = 'V_76',
              particles = [ P.ghA, P.ghA__tilde__, P.H__minus__, P.H__plus__ ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_28})

V_77 = Vertex(name = 'V_77',
              particles = [ P.ghZ, P.ghZ__tilde__, P.H__minus__, P.H__plus__ ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_228})

V_78 = Vertex(name = 'V_78',
              particles = [ P.ghWp, P.ghZ__tilde__, P.Ha, P.HQ__minus__ ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_115})

V_79 = Vertex(name = 'V_79',
              particles = [ P.ghZ, P.ghWm__tilde__, P.Ha, P.HQ__minus__ ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_117})

V_80 = Vertex(name = 'V_80',
              particles = [ P.ghWp, P.ghZ__tilde__, P.Hl, P.HQ__minus__ ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_94})

V_81 = Vertex(name = 'V_81',
              particles = [ P.ghZ, P.ghWm__tilde__, P.Hl, P.HQ__minus__ ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_95})

V_82 = Vertex(name = 'V_82',
              particles = [ P.ghA, P.ghA__tilde__, P.H__plus__, P.HQ__minus__ ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_27})

V_83 = Vertex(name = 'V_83',
              particles = [ P.ghZ, P.ghZ__tilde__, P.H__plus__, P.HQ__minus__ ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_226})

V_84 = Vertex(name = 'V_84',
              particles = [ P.G__plus__, P.GQ__minus__, P.H__plus__, P.HQ__minus__ ],
              color = [ '1' ],
              lorentz = [ L.SSSS1 ],
              couplings = {(0,0):C.GC_166})

V_85 = Vertex(name = 'V_85',
              particles = [ P.G__minus__, P.GQ__plus__, P.H__plus__, P.HQ__minus__ ],
              color = [ '1' ],
              lorentz = [ L.SSSS1 ],
              couplings = {(0,0):C.GC_162})

V_86 = Vertex(name = 'V_86',
              particles = [ P.GQ0, P.Hh, P.H__plus__, P.HQ__minus__ ],
              color = [ '1' ],
              lorentz = [ L.SSSS1 ],
              couplings = {(0,0):C.GC_157})

V_87 = Vertex(name = 'V_87',
              particles = [ P.G0, P.HhQ, P.H__plus__, P.HQ__minus__ ],
              color = [ '1' ],
              lorentz = [ L.SSSS1 ],
              couplings = {(0,0):C.GC_154})

V_88 = Vertex(name = 'V_88',
              particles = [ P.HaQ, P.Hl, P.H__plus__, P.HQ__minus__ ],
              color = [ '1' ],
              lorentz = [ L.SSSS1 ],
              couplings = {(0,0):C.GC_60})

V_89 = Vertex(name = 'V_89',
              particles = [ P.Ha, P.HlQ, P.H__plus__, P.HQ__minus__ ],
              color = [ '1' ],
              lorentz = [ L.SSSS1 ],
              couplings = {(0,0):C.GC_61})

V_90 = Vertex(name = 'V_90',
              particles = [ P.H__plus__, P.H__plus__, P.HQ__minus__, P.HQ__minus__ ],
              color = [ '1' ],
              lorentz = [ L.SSSS1 ],
              couplings = {(0,0):C.GC_170})

V_91 = Vertex(name = 'V_91',
              particles = [ P.ghWm, P.ghZ__tilde__, P.Ha, P.HQ__plus__ ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_116})

V_92 = Vertex(name = 'V_92',
              particles = [ P.ghZ, P.ghWp__tilde__, P.Ha, P.HQ__plus__ ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_114})

V_93 = Vertex(name = 'V_93',
              particles = [ P.ghWm, P.ghZ__tilde__, P.Hl, P.HQ__plus__ ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_94})

V_94 = Vertex(name = 'V_94',
              particles = [ P.ghZ, P.ghWp__tilde__, P.Hl, P.HQ__plus__ ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_95})

V_95 = Vertex(name = 'V_95',
              particles = [ P.ghA, P.ghA__tilde__, P.H__minus__, P.HQ__plus__ ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_27})

V_96 = Vertex(name = 'V_96',
              particles = [ P.ghZ, P.ghZ__tilde__, P.H__minus__, P.HQ__plus__ ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_226})

V_97 = Vertex(name = 'V_97',
              particles = [ P.G__plus__, P.GQ__minus__, P.H__minus__, P.HQ__plus__ ],
              color = [ '1' ],
              lorentz = [ L.SSSS1 ],
              couplings = {(0,0):C.GC_162})

V_98 = Vertex(name = 'V_98',
              particles = [ P.G__minus__, P.GQ__plus__, P.H__minus__, P.HQ__plus__ ],
              color = [ '1' ],
              lorentz = [ L.SSSS1 ],
              couplings = {(0,0):C.GC_166})

V_99 = Vertex(name = 'V_99',
              particles = [ P.GQ0, P.Hh, P.H__minus__, P.HQ__plus__ ],
              color = [ '1' ],
              lorentz = [ L.SSSS1 ],
              couplings = {(0,0):C.GC_154})

V_100 = Vertex(name = 'V_100',
               particles = [ P.G0, P.HhQ, P.H__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_157})

V_101 = Vertex(name = 'V_101',
               particles = [ P.HaQ, P.Hl, P.H__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_61})

V_102 = Vertex(name = 'V_102',
               particles = [ P.Ha, P.HlQ, P.H__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_60})

V_103 = Vertex(name = 'V_103',
               particles = [ P.H__minus__, P.H__plus__, P.HQ__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_160})

V_104 = Vertex(name = 'V_104',
               particles = [ P.H__minus__, P.H__minus__, P.HQ__plus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_170})

V_105 = Vertex(name = 'V_105',
               particles = [ P.G0, P.G0, P.G0, P.G0 ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_69})

V_106 = Vertex(name = 'V_106',
               particles = [ P.G0, P.G0, P.G__minus__, P.G__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_67})

V_107 = Vertex(name = 'V_107',
               particles = [ P.G__minus__, P.G__minus__, P.G__plus__, P.G__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_68})

V_108 = Vertex(name = 'V_108',
               particles = [ P.G0, P.G0, P.G0, P.GQ0 ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_69})

V_109 = Vertex(name = 'V_109',
               particles = [ P.G0, P.G__minus__, P.G__plus__, P.GQ0 ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_67})

V_110 = Vertex(name = 'V_110',
               particles = [ P.G0, P.G0, P.GQ0, P.GQ0 ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_69})

V_111 = Vertex(name = 'V_111',
               particles = [ P.G__minus__, P.G__plus__, P.GQ0, P.GQ0 ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_169})

V_112 = Vertex(name = 'V_112',
               particles = [ P.G0, P.GQ0, P.GQ0, P.GQ0 ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_69})

V_113 = Vertex(name = 'V_113',
               particles = [ P.GQ0, P.GQ0, P.GQ0, P.GQ0 ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_69})

V_114 = Vertex(name = 'V_114',
               particles = [ P.G0, P.G0, P.G__plus__, P.GQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_67})

V_115 = Vertex(name = 'V_115',
               particles = [ P.G__minus__, P.G__plus__, P.G__plus__, P.GQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_68})

V_116 = Vertex(name = 'V_116',
               particles = [ P.G0, P.G__plus__, P.GQ0, P.GQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_165})

V_117 = Vertex(name = 'V_117',
               particles = [ P.G__plus__, P.GQ0, P.GQ0, P.GQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_67})

V_118 = Vertex(name = 'V_118',
               particles = [ P.G0, P.G0, P.G__minus__, P.GQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_67})

V_119 = Vertex(name = 'V_119',
               particles = [ P.G__minus__, P.G__minus__, P.G__plus__, P.GQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_68})

V_120 = Vertex(name = 'V_120',
               particles = [ P.G0, P.G__minus__, P.GQ0, P.GQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_165})

V_121 = Vertex(name = 'V_121',
               particles = [ P.G__minus__, P.GQ0, P.GQ0, P.GQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_67})

V_122 = Vertex(name = 'V_122',
               particles = [ P.G0, P.G0, P.GQ__minus__, P.GQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_169})

V_123 = Vertex(name = 'V_123',
               particles = [ P.G0, P.GQ0, P.GQ__minus__, P.GQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_67})

V_124 = Vertex(name = 'V_124',
               particles = [ P.GQ0, P.GQ0, P.GQ__minus__, P.GQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_67})

V_125 = Vertex(name = 'V_125',
               particles = [ P.G__plus__, P.GQ__minus__, P.GQ__minus__, P.GQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_68})

V_126 = Vertex(name = 'V_126',
               particles = [ P.G__minus__, P.GQ__minus__, P.GQ__plus__, P.GQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_68})

V_127 = Vertex(name = 'V_127',
               particles = [ P.GQ__minus__, P.GQ__minus__, P.GQ__plus__, P.GQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_68})

V_128 = Vertex(name = 'V_128',
               particles = [ P.G0, P.G0, P.Hh, P.Hh ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_43})

V_129 = Vertex(name = 'V_129',
               particles = [ P.G__minus__, P.G__plus__, P.Hh, P.Hh ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_42})

V_130 = Vertex(name = 'V_130',
               particles = [ P.G0, P.GQ0, P.Hh, P.Hh ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_43})

V_131 = Vertex(name = 'V_131',
               particles = [ P.G__plus__, P.GQ__minus__, P.Hh, P.Hh ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_42})

V_132 = Vertex(name = 'V_132',
               particles = [ P.G__minus__, P.GQ__plus__, P.Hh, P.Hh ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_42})

V_133 = Vertex(name = 'V_133',
               particles = [ P.GQ__minus__, P.GQ__plus__, P.Hh, P.Hh ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_146})

V_134 = Vertex(name = 'V_134',
               particles = [ P.Hh, P.Hh, P.Hh, P.Hh ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_19})

V_135 = Vertex(name = 'V_135',
               particles = [ P.G0, P.G0, P.Hh, P.HhQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_43})

V_136 = Vertex(name = 'V_136',
               particles = [ P.G__minus__, P.G__plus__, P.Hh, P.HhQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_42})

V_137 = Vertex(name = 'V_137',
               particles = [ P.GQ0, P.GQ0, P.Hh, P.HhQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_43})

V_138 = Vertex(name = 'V_138',
               particles = [ P.G__plus__, P.GQ__minus__, P.Hh, P.HhQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_140})

V_139 = Vertex(name = 'V_139',
               particles = [ P.G__minus__, P.GQ__plus__, P.Hh, P.HhQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_140})

V_140 = Vertex(name = 'V_140',
               particles = [ P.GQ__minus__, P.GQ__plus__, P.Hh, P.HhQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_42})

V_141 = Vertex(name = 'V_141',
               particles = [ P.Hh, P.Hh, P.Hh, P.HhQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_19})

V_142 = Vertex(name = 'V_142',
               particles = [ P.G__minus__, P.G__plus__, P.HhQ, P.HhQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_146})

V_143 = Vertex(name = 'V_143',
               particles = [ P.G0, P.GQ0, P.HhQ, P.HhQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_43})

V_144 = Vertex(name = 'V_144',
               particles = [ P.GQ0, P.GQ0, P.HhQ, P.HhQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_43})

V_145 = Vertex(name = 'V_145',
               particles = [ P.G__plus__, P.GQ__minus__, P.HhQ, P.HhQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_42})

V_146 = Vertex(name = 'V_146',
               particles = [ P.G__minus__, P.GQ__plus__, P.HhQ, P.HhQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_42})

V_147 = Vertex(name = 'V_147',
               particles = [ P.GQ__minus__, P.GQ__plus__, P.HhQ, P.HhQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_42})

V_148 = Vertex(name = 'V_148',
               particles = [ P.Hh, P.Hh, P.HhQ, P.HhQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_19})

V_149 = Vertex(name = 'V_149',
               particles = [ P.Hh, P.HhQ, P.HhQ, P.HhQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_19})

V_150 = Vertex(name = 'V_150',
               particles = [ P.HhQ, P.HhQ, P.HhQ, P.HhQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_19})

V_151 = Vertex(name = 'V_151',
               particles = [ P.Ha, P.Ha, P.Ha, P.Ha ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_66})

V_152 = Vertex(name = 'V_152',
               particles = [ P.Ha, P.Ha, P.Ha, P.HaQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_66})

V_153 = Vertex(name = 'V_153',
               particles = [ P.Ha, P.Ha, P.HaQ, P.HaQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_66})

V_154 = Vertex(name = 'V_154',
               particles = [ P.Ha, P.HaQ, P.HaQ, P.HaQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_66})

V_155 = Vertex(name = 'V_155',
               particles = [ P.HaQ, P.HaQ, P.HaQ, P.HaQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_66})

V_156 = Vertex(name = 'V_156',
               particles = [ P.Ha, P.Ha, P.Hl, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_41})

V_157 = Vertex(name = 'V_157',
               particles = [ P.Ha, P.HaQ, P.Hl, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_41})

V_158 = Vertex(name = 'V_158',
               particles = [ P.Hl, P.Hl, P.Hl, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_18})

V_159 = Vertex(name = 'V_159',
               particles = [ P.Ha, P.Ha, P.Hl, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_41})

V_160 = Vertex(name = 'V_160',
               particles = [ P.HaQ, P.HaQ, P.Hl, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_41})

V_161 = Vertex(name = 'V_161',
               particles = [ P.Hl, P.Hl, P.Hl, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_18})

V_162 = Vertex(name = 'V_162',
               particles = [ P.Ha, P.HaQ, P.HlQ, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_41})

V_163 = Vertex(name = 'V_163',
               particles = [ P.HaQ, P.HaQ, P.HlQ, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_41})

V_164 = Vertex(name = 'V_164',
               particles = [ P.Hl, P.Hl, P.HlQ, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_18})

V_165 = Vertex(name = 'V_165',
               particles = [ P.Hl, P.HlQ, P.HlQ, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_18})

V_166 = Vertex(name = 'V_166',
               particles = [ P.HlQ, P.HlQ, P.HlQ, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_18})

V_167 = Vertex(name = 'V_167',
               particles = [ P.Ha, P.Ha, P.H__minus__, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_64})

V_168 = Vertex(name = 'V_168',
               particles = [ P.Ha, P.HaQ, P.H__minus__, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_64})

V_169 = Vertex(name = 'V_169',
               particles = [ P.HaQ, P.HaQ, P.H__minus__, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_168})

V_170 = Vertex(name = 'V_170',
               particles = [ P.Hl, P.Hl, P.H__minus__, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_40})

V_171 = Vertex(name = 'V_171',
               particles = [ P.Hl, P.HlQ, P.H__minus__, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_40})

V_172 = Vertex(name = 'V_172',
               particles = [ P.HlQ, P.HlQ, P.H__minus__, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_144})

V_173 = Vertex(name = 'V_173',
               particles = [ P.H__minus__, P.H__minus__, P.H__plus__, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_65})

V_174 = Vertex(name = 'V_174',
               particles = [ P.Ha, P.Ha, P.H__plus__, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_64})

V_175 = Vertex(name = 'V_175',
               particles = [ P.Ha, P.HaQ, P.H__plus__, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_164})

V_176 = Vertex(name = 'V_176',
               particles = [ P.HaQ, P.HaQ, P.H__plus__, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_64})

V_177 = Vertex(name = 'V_177',
               particles = [ P.Hl, P.Hl, P.H__plus__, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_40})

V_178 = Vertex(name = 'V_178',
               particles = [ P.Hl, P.HlQ, P.H__plus__, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_138})

V_179 = Vertex(name = 'V_179',
               particles = [ P.HlQ, P.HlQ, P.H__plus__, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_40})

V_180 = Vertex(name = 'V_180',
               particles = [ P.H__minus__, P.H__plus__, P.H__plus__, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_65})

V_181 = Vertex(name = 'V_181',
               particles = [ P.Ha, P.Ha, P.H__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_64})

V_182 = Vertex(name = 'V_182',
               particles = [ P.Ha, P.HaQ, P.H__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_164})

V_183 = Vertex(name = 'V_183',
               particles = [ P.HaQ, P.HaQ, P.H__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_64})

V_184 = Vertex(name = 'V_184',
               particles = [ P.Hl, P.Hl, P.H__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_40})

V_185 = Vertex(name = 'V_185',
               particles = [ P.Hl, P.HlQ, P.H__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_138})

V_186 = Vertex(name = 'V_186',
               particles = [ P.HlQ, P.HlQ, P.H__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_40})

V_187 = Vertex(name = 'V_187',
               particles = [ P.H__minus__, P.H__minus__, P.H__plus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_65})

V_188 = Vertex(name = 'V_188',
               particles = [ P.Ha, P.Ha, P.HQ__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_168})

V_189 = Vertex(name = 'V_189',
               particles = [ P.Ha, P.HaQ, P.HQ__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_64})

V_190 = Vertex(name = 'V_190',
               particles = [ P.HaQ, P.HaQ, P.HQ__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_64})

V_191 = Vertex(name = 'V_191',
               particles = [ P.Hl, P.Hl, P.HQ__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_144})

V_192 = Vertex(name = 'V_192',
               particles = [ P.Hl, P.HlQ, P.HQ__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_40})

V_193 = Vertex(name = 'V_193',
               particles = [ P.HlQ, P.HlQ, P.HQ__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_40})

V_194 = Vertex(name = 'V_194',
               particles = [ P.H__plus__, P.HQ__minus__, P.HQ__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_65})

V_195 = Vertex(name = 'V_195',
               particles = [ P.H__minus__, P.HQ__minus__, P.HQ__plus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_65})

V_196 = Vertex(name = 'V_196',
               particles = [ P.HQ__minus__, P.HQ__minus__, P.HQ__plus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_65})

V_197 = Vertex(name = 'V_197',
               particles = [ P.G0, P.G0, P.Ha, P.Ha ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_73})

V_198 = Vertex(name = 'V_198',
               particles = [ P.G__minus__, P.G__plus__, P.Ha, P.Ha ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_70})

V_199 = Vertex(name = 'V_199',
               particles = [ P.G0, P.GQ0, P.Ha, P.Ha ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_73})

V_200 = Vertex(name = 'V_200',
               particles = [ P.GQ0, P.GQ0, P.Ha, P.Ha ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_73})

V_201 = Vertex(name = 'V_201',
               particles = [ P.G__plus__, P.GQ__minus__, P.Ha, P.Ha ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_70})

V_202 = Vertex(name = 'V_202',
               particles = [ P.G__minus__, P.GQ__plus__, P.Ha, P.Ha ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_70})

V_203 = Vertex(name = 'V_203',
               particles = [ P.GQ__minus__, P.GQ__plus__, P.Ha, P.Ha ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_70})

V_204 = Vertex(name = 'V_204',
               particles = [ P.G0, P.G0, P.Ha, P.HaQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_73})

V_205 = Vertex(name = 'V_205',
               particles = [ P.G__minus__, P.G__plus__, P.Ha, P.HaQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_70})

V_206 = Vertex(name = 'V_206',
               particles = [ P.G0, P.GQ0, P.Ha, P.HaQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_73})

V_207 = Vertex(name = 'V_207',
               particles = [ P.GQ0, P.GQ0, P.Ha, P.HaQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_73})

V_208 = Vertex(name = 'V_208',
               particles = [ P.G__plus__, P.GQ__minus__, P.Ha, P.HaQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_70})

V_209 = Vertex(name = 'V_209',
               particles = [ P.G__minus__, P.GQ__plus__, P.Ha, P.HaQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_70})

V_210 = Vertex(name = 'V_210',
               particles = [ P.GQ__minus__, P.GQ__plus__, P.Ha, P.HaQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_70})

V_211 = Vertex(name = 'V_211',
               particles = [ P.G0, P.G0, P.HaQ, P.HaQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_73})

V_212 = Vertex(name = 'V_212',
               particles = [ P.G__minus__, P.G__plus__, P.HaQ, P.HaQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_70})

V_213 = Vertex(name = 'V_213',
               particles = [ P.G0, P.GQ0, P.HaQ, P.HaQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_73})

V_214 = Vertex(name = 'V_214',
               particles = [ P.GQ0, P.GQ0, P.HaQ, P.HaQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_73})

V_215 = Vertex(name = 'V_215',
               particles = [ P.G__plus__, P.GQ__minus__, P.HaQ, P.HaQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_70})

V_216 = Vertex(name = 'V_216',
               particles = [ P.G__minus__, P.GQ__plus__, P.HaQ, P.HaQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_70})

V_217 = Vertex(name = 'V_217',
               particles = [ P.GQ__minus__, P.GQ__plus__, P.HaQ, P.HaQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_70})

V_218 = Vertex(name = 'V_218',
               particles = [ P.Ha, P.Ha, P.Hh, P.Hh ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_48})

V_219 = Vertex(name = 'V_219',
               particles = [ P.Ha, P.HaQ, P.Hh, P.Hh ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_48})

V_220 = Vertex(name = 'V_220',
               particles = [ P.HaQ, P.HaQ, P.Hh, P.Hh ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_110})

V_221 = Vertex(name = 'V_221',
               particles = [ P.Ha, P.Ha, P.Hh, P.HhQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_48})

V_222 = Vertex(name = 'V_222',
               particles = [ P.Ha, P.HaQ, P.Hh, P.HhQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_106})

V_223 = Vertex(name = 'V_223',
               particles = [ P.HaQ, P.HaQ, P.Hh, P.HhQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_48})

V_224 = Vertex(name = 'V_224',
               particles = [ P.Ha, P.Ha, P.HhQ, P.HhQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_110})

V_225 = Vertex(name = 'V_225',
               particles = [ P.Ha, P.HaQ, P.HhQ, P.HhQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_48})

V_226 = Vertex(name = 'V_226',
               particles = [ P.HaQ, P.HaQ, P.HhQ, P.HhQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_48})

V_227 = Vertex(name = 'V_227',
               particles = [ P.G0, P.G0, P.Hl, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_49})

V_228 = Vertex(name = 'V_228',
               particles = [ P.G__minus__, P.G__plus__, P.Hl, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_45})

V_229 = Vertex(name = 'V_229',
               particles = [ P.G0, P.GQ0, P.Hl, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_49})

V_230 = Vertex(name = 'V_230',
               particles = [ P.GQ0, P.GQ0, P.Hl, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_111})

V_231 = Vertex(name = 'V_231',
               particles = [ P.G__plus__, P.GQ__minus__, P.Hl, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_45})

V_232 = Vertex(name = 'V_232',
               particles = [ P.G__minus__, P.GQ__plus__, P.Hl, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_45})

V_233 = Vertex(name = 'V_233',
               particles = [ P.GQ__minus__, P.GQ__plus__, P.Hl, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_109})

V_234 = Vertex(name = 'V_234',
               particles = [ P.Hh, P.Hh, P.Hl, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_20})

V_235 = Vertex(name = 'V_235',
               particles = [ P.Hh, P.HhQ, P.Hl, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_20})

V_236 = Vertex(name = 'V_236',
               particles = [ P.HhQ, P.HhQ, P.Hl, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_20})

V_237 = Vertex(name = 'V_237',
               particles = [ P.G0, P.G0, P.Hl, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_49})

V_238 = Vertex(name = 'V_238',
               particles = [ P.G__minus__, P.G__plus__, P.Hl, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_45})

V_239 = Vertex(name = 'V_239',
               particles = [ P.G0, P.GQ0, P.Hl, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_107})

V_240 = Vertex(name = 'V_240',
               particles = [ P.GQ0, P.GQ0, P.Hl, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_49})

V_241 = Vertex(name = 'V_241',
               particles = [ P.G__plus__, P.GQ__minus__, P.Hl, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_105})

V_242 = Vertex(name = 'V_242',
               particles = [ P.G__minus__, P.GQ__plus__, P.Hl, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_105})

V_243 = Vertex(name = 'V_243',
               particles = [ P.GQ__minus__, P.GQ__plus__, P.Hl, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_45})

V_244 = Vertex(name = 'V_244',
               particles = [ P.Hh, P.Hh, P.Hl, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_20})

V_245 = Vertex(name = 'V_245',
               particles = [ P.Hh, P.HhQ, P.Hl, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_20})

V_246 = Vertex(name = 'V_246',
               particles = [ P.HhQ, P.HhQ, P.Hl, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_20})

V_247 = Vertex(name = 'V_247',
               particles = [ P.G0, P.G0, P.HlQ, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_111})

V_248 = Vertex(name = 'V_248',
               particles = [ P.G__minus__, P.G__plus__, P.HlQ, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_109})

V_249 = Vertex(name = 'V_249',
               particles = [ P.G0, P.GQ0, P.HlQ, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_49})

V_250 = Vertex(name = 'V_250',
               particles = [ P.GQ0, P.GQ0, P.HlQ, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_49})

V_251 = Vertex(name = 'V_251',
               particles = [ P.G__plus__, P.GQ__minus__, P.HlQ, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_45})

V_252 = Vertex(name = 'V_252',
               particles = [ P.G__minus__, P.GQ__plus__, P.HlQ, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_45})

V_253 = Vertex(name = 'V_253',
               particles = [ P.GQ__minus__, P.GQ__plus__, P.HlQ, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_45})

V_254 = Vertex(name = 'V_254',
               particles = [ P.Hh, P.Hh, P.HlQ, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_20})

V_255 = Vertex(name = 'V_255',
               particles = [ P.Hh, P.HhQ, P.HlQ, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_20})

V_256 = Vertex(name = 'V_256',
               particles = [ P.HhQ, P.HhQ, P.HlQ, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_20})

V_257 = Vertex(name = 'V_257',
               particles = [ P.G0, P.G0, P.H__minus__, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_70})

V_258 = Vertex(name = 'V_258',
               particles = [ P.G__minus__, P.G__plus__, P.H__minus__, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_71})

V_259 = Vertex(name = 'V_259',
               particles = [ P.G0, P.GQ0, P.H__minus__, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_70})

V_260 = Vertex(name = 'V_260',
               particles = [ P.GQ0, P.GQ0, P.H__minus__, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_70})

V_261 = Vertex(name = 'V_261',
               particles = [ P.G__plus__, P.GQ__minus__, P.H__minus__, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_71})

V_262 = Vertex(name = 'V_262',
               particles = [ P.G__minus__, P.GQ__plus__, P.H__minus__, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_71})

V_263 = Vertex(name = 'V_263',
               particles = [ P.GQ__minus__, P.GQ__plus__, P.H__minus__, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_71})

V_264 = Vertex(name = 'V_264',
               particles = [ P.Hh, P.Hh, P.H__minus__, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_44})

V_265 = Vertex(name = 'V_265',
               particles = [ P.Hh, P.HhQ, P.H__minus__, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_44})

V_266 = Vertex(name = 'V_266',
               particles = [ P.HhQ, P.HhQ, P.H__minus__, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_108})

V_267 = Vertex(name = 'V_267',
               particles = [ P.G0, P.G0, P.H__plus__, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_70})

V_268 = Vertex(name = 'V_268',
               particles = [ P.G__minus__, P.G__plus__, P.H__plus__, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_71})

V_269 = Vertex(name = 'V_269',
               particles = [ P.G0, P.GQ0, P.H__plus__, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_70})

V_270 = Vertex(name = 'V_270',
               particles = [ P.GQ0, P.GQ0, P.H__plus__, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_70})

V_271 = Vertex(name = 'V_271',
               particles = [ P.GQ__minus__, P.GQ__plus__, P.H__plus__, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_71})

V_272 = Vertex(name = 'V_272',
               particles = [ P.Hh, P.Hh, P.H__plus__, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_44})

V_273 = Vertex(name = 'V_273',
               particles = [ P.Hh, P.HhQ, P.H__plus__, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_104})

V_274 = Vertex(name = 'V_274',
               particles = [ P.HhQ, P.HhQ, P.H__plus__, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_44})

V_275 = Vertex(name = 'V_275',
               particles = [ P.G0, P.G0, P.H__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_70})

V_276 = Vertex(name = 'V_276',
               particles = [ P.G__minus__, P.G__plus__, P.H__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_71})

V_277 = Vertex(name = 'V_277',
               particles = [ P.G0, P.GQ0, P.H__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_70})

V_278 = Vertex(name = 'V_278',
               particles = [ P.GQ0, P.GQ0, P.H__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_70})

V_279 = Vertex(name = 'V_279',
               particles = [ P.GQ__minus__, P.GQ__plus__, P.H__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_71})

V_280 = Vertex(name = 'V_280',
               particles = [ P.Hh, P.Hh, P.H__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_44})

V_281 = Vertex(name = 'V_281',
               particles = [ P.Hh, P.HhQ, P.H__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_104})

V_282 = Vertex(name = 'V_282',
               particles = [ P.HhQ, P.HhQ, P.H__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_44})

V_283 = Vertex(name = 'V_283',
               particles = [ P.G0, P.G0, P.HQ__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_70})

V_284 = Vertex(name = 'V_284',
               particles = [ P.G__minus__, P.G__plus__, P.HQ__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_71})

V_285 = Vertex(name = 'V_285',
               particles = [ P.G0, P.GQ0, P.HQ__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_70})

V_286 = Vertex(name = 'V_286',
               particles = [ P.GQ0, P.GQ0, P.HQ__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_70})

V_287 = Vertex(name = 'V_287',
               particles = [ P.G__plus__, P.GQ__minus__, P.HQ__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_71})

V_288 = Vertex(name = 'V_288',
               particles = [ P.G__minus__, P.GQ__plus__, P.HQ__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_71})

V_289 = Vertex(name = 'V_289',
               particles = [ P.GQ__minus__, P.GQ__plus__, P.HQ__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_71})

V_290 = Vertex(name = 'V_290',
               particles = [ P.Hh, P.Hh, P.HQ__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_108})

V_291 = Vertex(name = 'V_291',
               particles = [ P.Hh, P.HhQ, P.HQ__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_44})

V_292 = Vertex(name = 'V_292',
               particles = [ P.HhQ, P.HhQ, P.HQ__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_44})

V_293 = Vertex(name = 'V_293',
               particles = [ P.G0, P.G__plus__, P.Ha, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_72})

V_294 = Vertex(name = 'V_294',
               particles = [ P.G__plus__, P.GQ0, P.Ha, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_72})

V_295 = Vertex(name = 'V_295',
               particles = [ P.G0, P.GQ__plus__, P.Ha, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_72})

V_296 = Vertex(name = 'V_296',
               particles = [ P.GQ0, P.GQ__plus__, P.Ha, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_72})

V_297 = Vertex(name = 'V_297',
               particles = [ P.G0, P.G__plus__, P.HaQ, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_72})

V_298 = Vertex(name = 'V_298',
               particles = [ P.G__plus__, P.GQ0, P.HaQ, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_163})

V_299 = Vertex(name = 'V_299',
               particles = [ P.G0, P.GQ__plus__, P.HaQ, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_167})

V_300 = Vertex(name = 'V_300',
               particles = [ P.GQ0, P.GQ__plus__, P.HaQ, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_72})

V_301 = Vertex(name = 'V_301',
               particles = [ P.G__plus__, P.Ha, P.Hh, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_63})

V_302 = Vertex(name = 'V_302',
               particles = [ P.GQ__plus__, P.Ha, P.Hh, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_63})

V_303 = Vertex(name = 'V_303',
               particles = [ P.G__plus__, P.HaQ, P.Hh, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_63})

V_304 = Vertex(name = 'V_304',
               particles = [ P.GQ__plus__, P.HaQ, P.Hh, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_159})

V_305 = Vertex(name = 'V_305',
               particles = [ P.G__plus__, P.Ha, P.HhQ, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_63})

V_306 = Vertex(name = 'V_306',
               particles = [ P.GQ__plus__, P.Ha, P.HhQ, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_63})

V_307 = Vertex(name = 'V_307',
               particles = [ P.G__plus__, P.HaQ, P.HhQ, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_156})

V_308 = Vertex(name = 'V_308',
               particles = [ P.GQ__plus__, P.HaQ, P.HhQ, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_63})

V_309 = Vertex(name = 'V_309',
               particles = [ P.G0, P.G__plus__, P.Hl, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_62})

V_310 = Vertex(name = 'V_310',
               particles = [ P.G__plus__, P.GQ0, P.Hl, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_62})

V_311 = Vertex(name = 'V_311',
               particles = [ P.G0, P.GQ__plus__, P.Hl, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_62})

V_312 = Vertex(name = 'V_312',
               particles = [ P.GQ0, P.GQ__plus__, P.Hl, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_62})

V_313 = Vertex(name = 'V_313',
               particles = [ P.G__plus__, P.Hh, P.Hl, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_46})

V_314 = Vertex(name = 'V_314',
               particles = [ P.GQ__plus__, P.Hh, P.Hl, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_46})

V_315 = Vertex(name = 'V_315',
               particles = [ P.G__plus__, P.HhQ, P.Hl, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_46})

V_316 = Vertex(name = 'V_316',
               particles = [ P.GQ__plus__, P.HhQ, P.Hl, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_102})

V_317 = Vertex(name = 'V_317',
               particles = [ P.G0, P.G__plus__, P.HlQ, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_62})

V_318 = Vertex(name = 'V_318',
               particles = [ P.G__plus__, P.GQ0, P.HlQ, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_158})

V_319 = Vertex(name = 'V_319',
               particles = [ P.G0, P.GQ__plus__, P.HlQ, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_155})

V_320 = Vertex(name = 'V_320',
               particles = [ P.GQ0, P.GQ__plus__, P.HlQ, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_62})

V_321 = Vertex(name = 'V_321',
               particles = [ P.G__plus__, P.Hh, P.HlQ, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_46})

V_322 = Vertex(name = 'V_322',
               particles = [ P.GQ__plus__, P.Hh, P.HlQ, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_142})

V_323 = Vertex(name = 'V_323',
               particles = [ P.G__plus__, P.HhQ, P.HlQ, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_136})

V_324 = Vertex(name = 'V_324',
               particles = [ P.GQ__plus__, P.HhQ, P.HlQ, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_46})

V_325 = Vertex(name = 'V_325',
               particles = [ P.G0, P.G__minus__, P.Ha, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_72})

V_326 = Vertex(name = 'V_326',
               particles = [ P.G__minus__, P.GQ0, P.Ha, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_72})

V_327 = Vertex(name = 'V_327',
               particles = [ P.G0, P.GQ__minus__, P.Ha, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_72})

V_328 = Vertex(name = 'V_328',
               particles = [ P.GQ0, P.GQ__minus__, P.Ha, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_72})

V_329 = Vertex(name = 'V_329',
               particles = [ P.G0, P.G__minus__, P.HaQ, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_72})

V_330 = Vertex(name = 'V_330',
               particles = [ P.G__minus__, P.GQ0, P.HaQ, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_163})

V_331 = Vertex(name = 'V_331',
               particles = [ P.G0, P.GQ__minus__, P.HaQ, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_167})

V_332 = Vertex(name = 'V_332',
               particles = [ P.GQ0, P.GQ__minus__, P.HaQ, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_72})

V_333 = Vertex(name = 'V_333',
               particles = [ P.G__minus__, P.Ha, P.Hh, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_62})

V_334 = Vertex(name = 'V_334',
               particles = [ P.GQ__minus__, P.Ha, P.Hh, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_62})

V_335 = Vertex(name = 'V_335',
               particles = [ P.G__minus__, P.HaQ, P.Hh, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_62})

V_336 = Vertex(name = 'V_336',
               particles = [ P.GQ__minus__, P.HaQ, P.Hh, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_155})

V_337 = Vertex(name = 'V_337',
               particles = [ P.G__minus__, P.Ha, P.HhQ, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_62})

V_338 = Vertex(name = 'V_338',
               particles = [ P.GQ__minus__, P.Ha, P.HhQ, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_62})

V_339 = Vertex(name = 'V_339',
               particles = [ P.G__minus__, P.HaQ, P.HhQ, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_158})

V_340 = Vertex(name = 'V_340',
               particles = [ P.GQ__minus__, P.HaQ, P.HhQ, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_62})

V_341 = Vertex(name = 'V_341',
               particles = [ P.G0, P.G__minus__, P.Hl, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_63})

V_342 = Vertex(name = 'V_342',
               particles = [ P.G__minus__, P.GQ0, P.Hl, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_63})

V_343 = Vertex(name = 'V_343',
               particles = [ P.G0, P.GQ__minus__, P.Hl, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_63})

V_344 = Vertex(name = 'V_344',
               particles = [ P.GQ0, P.GQ__minus__, P.Hl, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_63})

V_345 = Vertex(name = 'V_345',
               particles = [ P.G__minus__, P.Hh, P.Hl, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_46})

V_346 = Vertex(name = 'V_346',
               particles = [ P.GQ__minus__, P.Hh, P.Hl, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_46})

V_347 = Vertex(name = 'V_347',
               particles = [ P.G__minus__, P.HhQ, P.Hl, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_46})

V_348 = Vertex(name = 'V_348',
               particles = [ P.GQ__minus__, P.HhQ, P.Hl, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_102})

V_349 = Vertex(name = 'V_349',
               particles = [ P.G0, P.G__minus__, P.HlQ, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_63})

V_350 = Vertex(name = 'V_350',
               particles = [ P.G__minus__, P.GQ0, P.HlQ, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_156})

V_351 = Vertex(name = 'V_351',
               particles = [ P.G0, P.GQ__minus__, P.HlQ, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_159})

V_352 = Vertex(name = 'V_352',
               particles = [ P.GQ0, P.GQ__minus__, P.HlQ, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_63})

V_353 = Vertex(name = 'V_353',
               particles = [ P.G__minus__, P.Hh, P.HlQ, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_46})

V_354 = Vertex(name = 'V_354',
               particles = [ P.GQ__minus__, P.Hh, P.HlQ, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_142})

V_355 = Vertex(name = 'V_355',
               particles = [ P.G__minus__, P.HhQ, P.HlQ, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_136})

V_356 = Vertex(name = 'V_356',
               particles = [ P.GQ__minus__, P.HhQ, P.HlQ, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_46})

V_357 = Vertex(name = 'V_357',
               particles = [ P.G0, P.G__plus__, P.Ha, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_72})

V_358 = Vertex(name = 'V_358',
               particles = [ P.G__plus__, P.GQ0, P.Ha, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_167})

V_359 = Vertex(name = 'V_359',
               particles = [ P.G0, P.GQ__plus__, P.Ha, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_163})

V_360 = Vertex(name = 'V_360',
               particles = [ P.GQ0, P.GQ__plus__, P.Ha, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_72})

V_361 = Vertex(name = 'V_361',
               particles = [ P.G0, P.G__plus__, P.HaQ, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_72})

V_362 = Vertex(name = 'V_362',
               particles = [ P.G__plus__, P.GQ0, P.HaQ, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_72})

V_363 = Vertex(name = 'V_363',
               particles = [ P.G0, P.GQ__plus__, P.HaQ, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_72})

V_364 = Vertex(name = 'V_364',
               particles = [ P.GQ0, P.GQ__plus__, P.HaQ, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_72})

V_365 = Vertex(name = 'V_365',
               particles = [ P.G__plus__, P.Ha, P.Hh, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_63})

V_366 = Vertex(name = 'V_366',
               particles = [ P.GQ__plus__, P.Ha, P.Hh, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_156})

V_367 = Vertex(name = 'V_367',
               particles = [ P.G__plus__, P.HaQ, P.Hh, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_63})

V_368 = Vertex(name = 'V_368',
               particles = [ P.GQ__plus__, P.HaQ, P.Hh, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_63})

V_369 = Vertex(name = 'V_369',
               particles = [ P.G__plus__, P.Ha, P.HhQ, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_159})

V_370 = Vertex(name = 'V_370',
               particles = [ P.GQ__plus__, P.Ha, P.HhQ, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_63})

V_371 = Vertex(name = 'V_371',
               particles = [ P.G__plus__, P.HaQ, P.HhQ, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_63})

V_372 = Vertex(name = 'V_372',
               particles = [ P.GQ__plus__, P.HaQ, P.HhQ, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_63})

V_373 = Vertex(name = 'V_373',
               particles = [ P.G0, P.G__plus__, P.Hl, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_62})

V_374 = Vertex(name = 'V_374',
               particles = [ P.G__plus__, P.GQ0, P.Hl, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_155})

V_375 = Vertex(name = 'V_375',
               particles = [ P.G0, P.GQ__plus__, P.Hl, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_158})

V_376 = Vertex(name = 'V_376',
               particles = [ P.GQ0, P.GQ__plus__, P.Hl, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_62})

V_377 = Vertex(name = 'V_377',
               particles = [ P.G__plus__, P.Hh, P.Hl, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_46})

V_378 = Vertex(name = 'V_378',
               particles = [ P.GQ__plus__, P.Hh, P.Hl, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_136})

V_379 = Vertex(name = 'V_379',
               particles = [ P.G__plus__, P.HhQ, P.Hl, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_142})

V_380 = Vertex(name = 'V_380',
               particles = [ P.GQ__plus__, P.HhQ, P.Hl, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_46})

V_381 = Vertex(name = 'V_381',
               particles = [ P.G0, P.G__plus__, P.HlQ, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_62})

V_382 = Vertex(name = 'V_382',
               particles = [ P.G__plus__, P.GQ0, P.HlQ, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_62})

V_383 = Vertex(name = 'V_383',
               particles = [ P.G0, P.GQ__plus__, P.HlQ, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_62})

V_384 = Vertex(name = 'V_384',
               particles = [ P.GQ0, P.GQ__plus__, P.HlQ, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_62})

V_385 = Vertex(name = 'V_385',
               particles = [ P.G__plus__, P.Hh, P.HlQ, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_102})

V_386 = Vertex(name = 'V_386',
               particles = [ P.GQ__plus__, P.Hh, P.HlQ, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_46})

V_387 = Vertex(name = 'V_387',
               particles = [ P.G__plus__, P.HhQ, P.HlQ, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_46})

V_388 = Vertex(name = 'V_388',
               particles = [ P.GQ__plus__, P.HhQ, P.HlQ, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_46})

V_389 = Vertex(name = 'V_389',
               particles = [ P.G0, P.G__minus__, P.Ha, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_72})

V_390 = Vertex(name = 'V_390',
               particles = [ P.G__minus__, P.GQ0, P.Ha, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_167})

V_391 = Vertex(name = 'V_391',
               particles = [ P.G0, P.GQ__minus__, P.Ha, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_163})

V_392 = Vertex(name = 'V_392',
               particles = [ P.GQ0, P.GQ__minus__, P.Ha, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_72})

V_393 = Vertex(name = 'V_393',
               particles = [ P.G0, P.G__minus__, P.HaQ, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_72})

V_394 = Vertex(name = 'V_394',
               particles = [ P.G__minus__, P.GQ0, P.HaQ, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_72})

V_395 = Vertex(name = 'V_395',
               particles = [ P.G0, P.GQ__minus__, P.HaQ, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_72})

V_396 = Vertex(name = 'V_396',
               particles = [ P.GQ0, P.GQ__minus__, P.HaQ, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_72})

V_397 = Vertex(name = 'V_397',
               particles = [ P.G__minus__, P.Ha, P.Hh, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_62})

V_398 = Vertex(name = 'V_398',
               particles = [ P.GQ__minus__, P.Ha, P.Hh, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_158})

V_399 = Vertex(name = 'V_399',
               particles = [ P.G__minus__, P.HaQ, P.Hh, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_62})

V_400 = Vertex(name = 'V_400',
               particles = [ P.GQ__minus__, P.HaQ, P.Hh, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_62})

V_401 = Vertex(name = 'V_401',
               particles = [ P.G__minus__, P.Ha, P.HhQ, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_155})

V_402 = Vertex(name = 'V_402',
               particles = [ P.GQ__minus__, P.Ha, P.HhQ, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_62})

V_403 = Vertex(name = 'V_403',
               particles = [ P.G__minus__, P.HaQ, P.HhQ, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_62})

V_404 = Vertex(name = 'V_404',
               particles = [ P.GQ__minus__, P.HaQ, P.HhQ, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_62})

V_405 = Vertex(name = 'V_405',
               particles = [ P.G0, P.G__minus__, P.Hl, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_63})

V_406 = Vertex(name = 'V_406',
               particles = [ P.G__minus__, P.GQ0, P.Hl, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_159})

V_407 = Vertex(name = 'V_407',
               particles = [ P.G0, P.GQ__minus__, P.Hl, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_156})

V_408 = Vertex(name = 'V_408',
               particles = [ P.GQ0, P.GQ__minus__, P.Hl, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_63})

V_409 = Vertex(name = 'V_409',
               particles = [ P.G__minus__, P.Hh, P.Hl, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_46})

V_410 = Vertex(name = 'V_410',
               particles = [ P.GQ__minus__, P.Hh, P.Hl, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_136})

V_411 = Vertex(name = 'V_411',
               particles = [ P.G__minus__, P.HhQ, P.Hl, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_142})

V_412 = Vertex(name = 'V_412',
               particles = [ P.GQ__minus__, P.HhQ, P.Hl, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_46})

V_413 = Vertex(name = 'V_413',
               particles = [ P.G0, P.G__minus__, P.HlQ, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_63})

V_414 = Vertex(name = 'V_414',
               particles = [ P.G__minus__, P.GQ0, P.HlQ, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_63})

V_415 = Vertex(name = 'V_415',
               particles = [ P.G0, P.GQ__minus__, P.HlQ, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_63})

V_416 = Vertex(name = 'V_416',
               particles = [ P.GQ0, P.GQ__minus__, P.HlQ, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_63})

V_417 = Vertex(name = 'V_417',
               particles = [ P.G__minus__, P.Hh, P.HlQ, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_102})

V_418 = Vertex(name = 'V_418',
               particles = [ P.GQ__minus__, P.Hh, P.HlQ, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_46})

V_419 = Vertex(name = 'V_419',
               particles = [ P.G__minus__, P.HhQ, P.HlQ, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_46})

V_420 = Vertex(name = 'V_420',
               particles = [ P.GQ__minus__, P.HhQ, P.HlQ, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_46})

V_421 = Vertex(name = 'V_421',
               particles = [ P.G0, P.Ha, P.Hh, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_47})

V_422 = Vertex(name = 'V_422',
               particles = [ P.GQ0, P.Ha, P.Hh, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_47})

V_423 = Vertex(name = 'V_423',
               particles = [ P.G0, P.HaQ, P.Hh, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_47})

V_424 = Vertex(name = 'V_424',
               particles = [ P.G0, P.Ha, P.HhQ, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_47})

V_425 = Vertex(name = 'V_425',
               particles = [ P.GQ0, P.Ha, P.HhQ, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_103})

V_426 = Vertex(name = 'V_426',
               particles = [ P.GQ0, P.HaQ, P.HhQ, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_47})

V_427 = Vertex(name = 'V_427',
               particles = [ P.G0, P.Ha, P.Hh, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_47})

V_428 = Vertex(name = 'V_428',
               particles = [ P.G0, P.HaQ, P.Hh, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_103})

V_429 = Vertex(name = 'V_429',
               particles = [ P.GQ0, P.HaQ, P.Hh, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_47})

V_430 = Vertex(name = 'V_430',
               particles = [ P.GQ0, P.Ha, P.HhQ, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_47})

V_431 = Vertex(name = 'V_431',
               particles = [ P.G0, P.HaQ, P.HhQ, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_47})

V_432 = Vertex(name = 'V_432',
               particles = [ P.GQ0, P.HaQ, P.HhQ, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_47})

V_433 = Vertex(name = 'V_433',
               particles = [ P.G__plus__, P.G__plus__, P.H__minus__, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_74})

V_434 = Vertex(name = 'V_434',
               particles = [ P.G__plus__, P.GQ__plus__, P.H__minus__, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_74})

V_435 = Vertex(name = 'V_435',
               particles = [ P.GQ__plus__, P.GQ__plus__, P.H__minus__, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_74})

V_436 = Vertex(name = 'V_436',
               particles = [ P.G__minus__, P.G__minus__, P.H__plus__, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_74})

V_437 = Vertex(name = 'V_437',
               particles = [ P.G__minus__, P.GQ__minus__, P.H__plus__, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_74})

V_438 = Vertex(name = 'V_438',
               particles = [ P.GQ__minus__, P.GQ__minus__, P.H__plus__, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_74})

V_439 = Vertex(name = 'V_439',
               particles = [ P.G__plus__, P.G__plus__, P.H__minus__, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_74})

V_440 = Vertex(name = 'V_440',
               particles = [ P.G__plus__, P.GQ__plus__, P.H__minus__, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_74})

V_441 = Vertex(name = 'V_441',
               particles = [ P.GQ__plus__, P.GQ__plus__, P.H__minus__, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_74})

V_442 = Vertex(name = 'V_442',
               particles = [ P.G__plus__, P.G__plus__, P.HQ__minus__, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_74})

V_443 = Vertex(name = 'V_443',
               particles = [ P.G__plus__, P.GQ__plus__, P.HQ__minus__, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_74})

V_444 = Vertex(name = 'V_444',
               particles = [ P.GQ__plus__, P.GQ__plus__, P.HQ__minus__, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_74})

V_445 = Vertex(name = 'V_445',
               particles = [ P.G__minus__, P.G__minus__, P.H__plus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_74})

V_446 = Vertex(name = 'V_446',
               particles = [ P.G__minus__, P.GQ__minus__, P.H__plus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_74})

V_447 = Vertex(name = 'V_447',
               particles = [ P.GQ__minus__, P.GQ__minus__, P.H__plus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_74})

V_448 = Vertex(name = 'V_448',
               particles = [ P.G__minus__, P.G__minus__, P.HQ__plus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_74})

V_449 = Vertex(name = 'V_449',
               particles = [ P.G__minus__, P.GQ__minus__, P.HQ__plus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_74})

V_450 = Vertex(name = 'V_450',
               particles = [ P.GQ__minus__, P.GQ__minus__, P.HQ__plus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_74})

V_451 = Vertex(name = 'V_451',
               particles = [ P.G__plus__, P.GQ__minus__, P.HaQ, P.Hh ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_148})

V_452 = Vertex(name = 'V_452',
               particles = [ P.G__minus__, P.GQ__plus__, P.HaQ, P.Hh ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_151})

V_453 = Vertex(name = 'V_453',
               particles = [ P.GQ0, P.HaQ, P.Hh, P.Hh ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_135})

V_454 = Vertex(name = 'V_454',
               particles = [ P.G__plus__, P.GQ__minus__, P.Ha, P.HhQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_151})

V_455 = Vertex(name = 'V_455',
               particles = [ P.G__minus__, P.GQ__plus__, P.Ha, P.HhQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_148})

V_456 = Vertex(name = 'V_456',
               particles = [ P.GQ0, P.Ha, P.Hh, P.HhQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_123})

V_457 = Vertex(name = 'V_457',
               particles = [ P.G0, P.HaQ, P.Hh, P.HhQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_123})

V_458 = Vertex(name = 'V_458',
               particles = [ P.G0, P.Ha, P.HhQ, P.HhQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_135})

V_459 = Vertex(name = 'V_459',
               particles = [ P.ghWp, P.ghZ__tilde__, P.G__minus__, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_22})

V_460 = Vertex(name = 'V_460',
               particles = [ P.ghZ, P.ghWm__tilde__, P.G__minus__, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_22})

V_461 = Vertex(name = 'V_461',
               particles = [ P.ghWm, P.ghZ__tilde__, P.G__plus__, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_22})

V_462 = Vertex(name = 'V_462',
               particles = [ P.ghZ, P.ghWp__tilde__, P.G__plus__, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_22})

V_463 = Vertex(name = 'V_463',
               particles = [ P.ghWp, P.ghZ__tilde__, P.GQ__minus__, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_83})

V_464 = Vertex(name = 'V_464',
               particles = [ P.ghZ, P.ghWm__tilde__, P.GQ__minus__, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_85})

V_465 = Vertex(name = 'V_465',
               particles = [ P.G__plus__, P.GQ0, P.GQ__minus__, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_50})

V_466 = Vertex(name = 'V_466',
               particles = [ P.ghWm, P.ghZ__tilde__, P.GQ__plus__, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_83})

V_467 = Vertex(name = 'V_467',
               particles = [ P.ghZ, P.ghWp__tilde__, P.GQ__plus__, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_85})

V_468 = Vertex(name = 'V_468',
               particles = [ P.G__minus__, P.GQ0, P.GQ__plus__, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_51})

V_469 = Vertex(name = 'V_469',
               particles = [ P.GQ0, P.GQ0, P.Hh, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_130})

V_470 = Vertex(name = 'V_470',
               particles = [ P.HaQ, P.HaQ, P.Hh, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_133})

V_471 = Vertex(name = 'V_471',
               particles = [ P.G0, P.GQ0, P.HhQ, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_126})

V_472 = Vertex(name = 'V_472',
               particles = [ P.Ha, P.HaQ, P.HhQ, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_121})

V_473 = Vertex(name = 'V_473',
               particles = [ P.GQ0, P.HaQ, P.Hl, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_131})

V_474 = Vertex(name = 'V_474',
               particles = [ P.ghWp, P.ghZ__tilde__, P.G__minus__, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_85})

V_475 = Vertex(name = 'V_475',
               particles = [ P.ghZ, P.ghWm__tilde__, P.G__minus__, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_83})

V_476 = Vertex(name = 'V_476',
               particles = [ P.ghWm, P.ghZ__tilde__, P.G__plus__, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_85})

V_477 = Vertex(name = 'V_477',
               particles = [ P.ghZ, P.ghWp__tilde__, P.G__plus__, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_83})

V_478 = Vertex(name = 'V_478',
               particles = [ P.G0, P.G__plus__, P.GQ__minus__, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_51})

V_479 = Vertex(name = 'V_479',
               particles = [ P.G0, P.G__minus__, P.GQ__plus__, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_50})

V_480 = Vertex(name = 'V_480',
               particles = [ P.G0, P.GQ0, P.Hh, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_126})

V_481 = Vertex(name = 'V_481',
               particles = [ P.Ha, P.HaQ, P.Hh, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_121})

V_482 = Vertex(name = 'V_482',
               particles = [ P.G0, P.G0, P.HhQ, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_130})

V_483 = Vertex(name = 'V_483',
               particles = [ P.Ha, P.Ha, P.HhQ, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_133})

V_484 = Vertex(name = 'V_484',
               particles = [ P.GQ0, P.Ha, P.Hl, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_127})

V_485 = Vertex(name = 'V_485',
               particles = [ P.G0, P.HaQ, P.Hl, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_127})

V_486 = Vertex(name = 'V_486',
               particles = [ P.G0, P.Ha, P.HlQ, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_131})

V_487 = Vertex(name = 'V_487',
               particles = [ P.ghWp, P.ghZ__tilde__, P.Hh, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_21})

V_488 = Vertex(name = 'V_488',
               particles = [ P.ghZ, P.ghWm__tilde__, P.Hh, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_21})

V_489 = Vertex(name = 'V_489',
               particles = [ P.ghWp, P.ghZ__tilde__, P.HhQ, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_82})

V_490 = Vertex(name = 'V_490',
               particles = [ P.ghZ, P.ghWm__tilde__, P.HhQ, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_84})

V_491 = Vertex(name = 'V_491',
               particles = [ P.ghWm, P.ghZ__tilde__, P.Hh, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_21})

V_492 = Vertex(name = 'V_492',
               particles = [ P.ghZ, P.ghWp__tilde__, P.Hh, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_21})

V_493 = Vertex(name = 'V_493',
               particles = [ P.ghWm, P.ghZ__tilde__, P.HhQ, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_82})

V_494 = Vertex(name = 'V_494',
               particles = [ P.ghZ, P.ghWp__tilde__, P.HhQ, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_84})

V_495 = Vertex(name = 'V_495',
               particles = [ P.ghWp, P.ghZ__tilde__, P.Hh, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_84})

V_496 = Vertex(name = 'V_496',
               particles = [ P.ghZ, P.ghWm__tilde__, P.Hh, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_82})

V_497 = Vertex(name = 'V_497',
               particles = [ P.HaQ, P.Hh, P.H__plus__, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_51})

V_498 = Vertex(name = 'V_498',
               particles = [ P.Ha, P.HhQ, P.H__plus__, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_50})

V_499 = Vertex(name = 'V_499',
               particles = [ P.GQ0, P.Hl, P.H__plus__, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_151})

V_500 = Vertex(name = 'V_500',
               particles = [ P.G0, P.HlQ, P.H__plus__, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_148})

V_501 = Vertex(name = 'V_501',
               particles = [ P.ghWm, P.ghZ__tilde__, P.Hh, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_84})

V_502 = Vertex(name = 'V_502',
               particles = [ P.ghZ, P.ghWp__tilde__, P.Hh, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_82})

V_503 = Vertex(name = 'V_503',
               particles = [ P.HaQ, P.Hh, P.H__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_50})

V_504 = Vertex(name = 'V_504',
               particles = [ P.Ha, P.HhQ, P.H__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_51})

V_505 = Vertex(name = 'V_505',
               particles = [ P.GQ0, P.Hl, P.H__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_148})

V_506 = Vertex(name = 'V_506',
               particles = [ P.G0, P.HlQ, P.H__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_151})

V_507 = Vertex(name = 'V_507',
               particles = [ P.G0, P.G0, P.Hh, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_35})

V_508 = Vertex(name = 'V_508',
               particles = [ P.G__minus__, P.G__plus__, P.Hh, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_33})

V_509 = Vertex(name = 'V_509',
               particles = [ P.G0, P.GQ0, P.Hh, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_35})

V_510 = Vertex(name = 'V_510',
               particles = [ P.G__plus__, P.GQ__minus__, P.Hh, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_33})

V_511 = Vertex(name = 'V_511',
               particles = [ P.G__minus__, P.GQ__plus__, P.Hh, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_33})

V_512 = Vertex(name = 'V_512',
               particles = [ P.GQ__minus__, P.GQ__plus__, P.Hh, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_128})

V_513 = Vertex(name = 'V_513',
               particles = [ P.Hh, P.Hh, P.Hh, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_17})

V_514 = Vertex(name = 'V_514',
               particles = [ P.G0, P.G0, P.HhQ, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_35})

V_515 = Vertex(name = 'V_515',
               particles = [ P.G__minus__, P.G__plus__, P.HhQ, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_33})

V_516 = Vertex(name = 'V_516',
               particles = [ P.GQ0, P.GQ0, P.HhQ, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_35})

V_517 = Vertex(name = 'V_517',
               particles = [ P.G__plus__, P.GQ__minus__, P.HhQ, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_124})

V_518 = Vertex(name = 'V_518',
               particles = [ P.G__minus__, P.GQ__plus__, P.HhQ, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_124})

V_519 = Vertex(name = 'V_519',
               particles = [ P.GQ__minus__, P.GQ__plus__, P.HhQ, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_33})

V_520 = Vertex(name = 'V_520',
               particles = [ P.Hh, P.Hh, P.HhQ, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_17})

V_521 = Vertex(name = 'V_521',
               particles = [ P.Hh, P.HhQ, P.HhQ, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_17})

V_522 = Vertex(name = 'V_522',
               particles = [ P.HhQ, P.HhQ, P.HhQ, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_17})

V_523 = Vertex(name = 'V_523',
               particles = [ P.G0, P.G0, P.Hh, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_35})

V_524 = Vertex(name = 'V_524',
               particles = [ P.G__minus__, P.G__plus__, P.Hh, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_33})

V_525 = Vertex(name = 'V_525',
               particles = [ P.GQ0, P.GQ0, P.Hh, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_35})

V_526 = Vertex(name = 'V_526',
               particles = [ P.G__plus__, P.GQ__minus__, P.Hh, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_124})

V_527 = Vertex(name = 'V_527',
               particles = [ P.G__minus__, P.GQ__plus__, P.Hh, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_124})

V_528 = Vertex(name = 'V_528',
               particles = [ P.GQ__minus__, P.GQ__plus__, P.Hh, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_33})

V_529 = Vertex(name = 'V_529',
               particles = [ P.Hh, P.Hh, P.Hh, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_17})

V_530 = Vertex(name = 'V_530',
               particles = [ P.G__minus__, P.G__plus__, P.HhQ, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_128})

V_531 = Vertex(name = 'V_531',
               particles = [ P.G0, P.GQ0, P.HhQ, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_35})

V_532 = Vertex(name = 'V_532',
               particles = [ P.GQ0, P.GQ0, P.HhQ, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_35})

V_533 = Vertex(name = 'V_533',
               particles = [ P.G__plus__, P.GQ__minus__, P.HhQ, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_33})

V_534 = Vertex(name = 'V_534',
               particles = [ P.G__minus__, P.GQ__plus__, P.HhQ, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_33})

V_535 = Vertex(name = 'V_535',
               particles = [ P.GQ__minus__, P.GQ__plus__, P.HhQ, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_33})

V_536 = Vertex(name = 'V_536',
               particles = [ P.Hh, P.Hh, P.HhQ, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_17})

V_537 = Vertex(name = 'V_537',
               particles = [ P.Hh, P.HhQ, P.HhQ, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_17})

V_538 = Vertex(name = 'V_538',
               particles = [ P.HhQ, P.HhQ, P.HhQ, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_17})

V_539 = Vertex(name = 'V_539',
               particles = [ P.Ha, P.Ha, P.Hh, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_36})

V_540 = Vertex(name = 'V_540',
               particles = [ P.Ha, P.HaQ, P.Hh, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_36})

V_541 = Vertex(name = 'V_541',
               particles = [ P.Ha, P.Ha, P.HhQ, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_36})

V_542 = Vertex(name = 'V_542',
               particles = [ P.HaQ, P.HaQ, P.HhQ, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_36})

V_543 = Vertex(name = 'V_543',
               particles = [ P.Hh, P.Hl, P.Hl, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_16})

V_544 = Vertex(name = 'V_544',
               particles = [ P.HhQ, P.Hl, P.Hl, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_16})

V_545 = Vertex(name = 'V_545',
               particles = [ P.Ha, P.Ha, P.Hh, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_36})

V_546 = Vertex(name = 'V_546',
               particles = [ P.HaQ, P.HaQ, P.Hh, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_36})

V_547 = Vertex(name = 'V_547',
               particles = [ P.Ha, P.HaQ, P.HhQ, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_36})

V_548 = Vertex(name = 'V_548',
               particles = [ P.HaQ, P.HaQ, P.HhQ, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_36})

V_549 = Vertex(name = 'V_549',
               particles = [ P.Hh, P.Hl, P.Hl, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_16})

V_550 = Vertex(name = 'V_550',
               particles = [ P.HhQ, P.Hl, P.Hl, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_16})

V_551 = Vertex(name = 'V_551',
               particles = [ P.Hh, P.Hl, P.HlQ, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_16})

V_552 = Vertex(name = 'V_552',
               particles = [ P.HhQ, P.Hl, P.HlQ, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_16})

V_553 = Vertex(name = 'V_553',
               particles = [ P.Hh, P.HlQ, P.HlQ, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_16})

V_554 = Vertex(name = 'V_554',
               particles = [ P.HhQ, P.HlQ, P.HlQ, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_16})

V_555 = Vertex(name = 'V_555',
               particles = [ P.Hh, P.Hl, P.H__minus__, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_32})

V_556 = Vertex(name = 'V_556',
               particles = [ P.HhQ, P.Hl, P.H__minus__, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_32})

V_557 = Vertex(name = 'V_557',
               particles = [ P.Hh, P.HlQ, P.H__minus__, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_32})

V_558 = Vertex(name = 'V_558',
               particles = [ P.HhQ, P.HlQ, P.H__minus__, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_132})

V_559 = Vertex(name = 'V_559',
               particles = [ P.Hh, P.Hl, P.H__plus__, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_32})

V_560 = Vertex(name = 'V_560',
               particles = [ P.HhQ, P.Hl, P.H__plus__, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_120})

V_561 = Vertex(name = 'V_561',
               particles = [ P.Hh, P.HlQ, P.H__plus__, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_120})

V_562 = Vertex(name = 'V_562',
               particles = [ P.HhQ, P.HlQ, P.H__plus__, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_32})

V_563 = Vertex(name = 'V_563',
               particles = [ P.Hh, P.Hl, P.H__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_32})

V_564 = Vertex(name = 'V_564',
               particles = [ P.HhQ, P.Hl, P.H__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_120})

V_565 = Vertex(name = 'V_565',
               particles = [ P.Hh, P.HlQ, P.H__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_120})

V_566 = Vertex(name = 'V_566',
               particles = [ P.HhQ, P.HlQ, P.H__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_32})

V_567 = Vertex(name = 'V_567',
               particles = [ P.Hh, P.Hl, P.HQ__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_132})

V_568 = Vertex(name = 'V_568',
               particles = [ P.HhQ, P.Hl, P.HQ__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_32})

V_569 = Vertex(name = 'V_569',
               particles = [ P.Hh, P.HlQ, P.HQ__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_32})

V_570 = Vertex(name = 'V_570',
               particles = [ P.HhQ, P.HlQ, P.HQ__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_32})

V_571 = Vertex(name = 'V_571',
               particles = [ P.G0, P.G__plus__, P.Hh, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_53})

V_572 = Vertex(name = 'V_572',
               particles = [ P.G__plus__, P.GQ0, P.Hh, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_53})

V_573 = Vertex(name = 'V_573',
               particles = [ P.G0, P.GQ__plus__, P.Hh, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_53})

V_574 = Vertex(name = 'V_574',
               particles = [ P.GQ0, P.GQ__plus__, P.Hh, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_53})

V_575 = Vertex(name = 'V_575',
               particles = [ P.G__plus__, P.Hh, P.Hh, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_37})

V_576 = Vertex(name = 'V_576',
               particles = [ P.GQ__plus__, P.Hh, P.Hh, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_37})

V_577 = Vertex(name = 'V_577',
               particles = [ P.G0, P.G__plus__, P.HhQ, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_53})

V_578 = Vertex(name = 'V_578',
               particles = [ P.G__plus__, P.GQ0, P.HhQ, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_150})

V_579 = Vertex(name = 'V_579',
               particles = [ P.G0, P.GQ__plus__, P.HhQ, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_153})

V_580 = Vertex(name = 'V_580',
               particles = [ P.GQ0, P.GQ__plus__, P.HhQ, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_53})

V_581 = Vertex(name = 'V_581',
               particles = [ P.G__plus__, P.Hh, P.HhQ, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_37})

V_582 = Vertex(name = 'V_582',
               particles = [ P.GQ__plus__, P.Hh, P.HhQ, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_122})

V_583 = Vertex(name = 'V_583',
               particles = [ P.G__plus__, P.HhQ, P.HhQ, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_134})

V_584 = Vertex(name = 'V_584',
               particles = [ P.GQ__plus__, P.HhQ, P.HhQ, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_37})

V_585 = Vertex(name = 'V_585',
               particles = [ P.G__plus__, P.Ha, P.Hl, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_53})

V_586 = Vertex(name = 'V_586',
               particles = [ P.GQ__plus__, P.Ha, P.Hl, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_53})

V_587 = Vertex(name = 'V_587',
               particles = [ P.G__plus__, P.HaQ, P.Hl, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_53})

V_588 = Vertex(name = 'V_588',
               particles = [ P.GQ__plus__, P.HaQ, P.Hl, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_153})

V_589 = Vertex(name = 'V_589',
               particles = [ P.G__plus__, P.Hl, P.Hl, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_34})

V_590 = Vertex(name = 'V_590',
               particles = [ P.GQ__plus__, P.Hl, P.Hl, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_34})

V_591 = Vertex(name = 'V_591',
               particles = [ P.G__plus__, P.Ha, P.HlQ, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_53})

V_592 = Vertex(name = 'V_592',
               particles = [ P.GQ__plus__, P.Ha, P.HlQ, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_53})

V_593 = Vertex(name = 'V_593',
               particles = [ P.G__plus__, P.HaQ, P.HlQ, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_150})

V_594 = Vertex(name = 'V_594',
               particles = [ P.GQ__plus__, P.HaQ, P.HlQ, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_53})

V_595 = Vertex(name = 'V_595',
               particles = [ P.G__plus__, P.Hl, P.HlQ, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_34})

V_596 = Vertex(name = 'V_596',
               particles = [ P.GQ__plus__, P.Hl, P.HlQ, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_125})

V_597 = Vertex(name = 'V_597',
               particles = [ P.G__plus__, P.HlQ, P.HlQ, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_129})

V_598 = Vertex(name = 'V_598',
               particles = [ P.GQ__plus__, P.HlQ, P.HlQ, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_34})

V_599 = Vertex(name = 'V_599',
               particles = [ P.G0, P.G__minus__, P.Hh, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_52})

V_600 = Vertex(name = 'V_600',
               particles = [ P.G__minus__, P.GQ0, P.Hh, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_52})

V_601 = Vertex(name = 'V_601',
               particles = [ P.G0, P.GQ__minus__, P.Hh, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_52})

V_602 = Vertex(name = 'V_602',
               particles = [ P.GQ0, P.GQ__minus__, P.Hh, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_52})

V_603 = Vertex(name = 'V_603',
               particles = [ P.G__minus__, P.Hh, P.Hh, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_37})

V_604 = Vertex(name = 'V_604',
               particles = [ P.GQ__minus__, P.Hh, P.Hh, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_37})

V_605 = Vertex(name = 'V_605',
               particles = [ P.G0, P.G__minus__, P.HhQ, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_52})

V_606 = Vertex(name = 'V_606',
               particles = [ P.G__minus__, P.GQ0, P.HhQ, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_152})

V_607 = Vertex(name = 'V_607',
               particles = [ P.G0, P.GQ__minus__, P.HhQ, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_149})

V_608 = Vertex(name = 'V_608',
               particles = [ P.GQ0, P.GQ__minus__, P.HhQ, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_52})

V_609 = Vertex(name = 'V_609',
               particles = [ P.G__minus__, P.Hh, P.HhQ, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_37})

V_610 = Vertex(name = 'V_610',
               particles = [ P.GQ__minus__, P.Hh, P.HhQ, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_122})

V_611 = Vertex(name = 'V_611',
               particles = [ P.G__minus__, P.HhQ, P.HhQ, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_134})

V_612 = Vertex(name = 'V_612',
               particles = [ P.GQ__minus__, P.HhQ, P.HhQ, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_37})

V_613 = Vertex(name = 'V_613',
               particles = [ P.G__minus__, P.Ha, P.Hl, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_52})

V_614 = Vertex(name = 'V_614',
               particles = [ P.GQ__minus__, P.Ha, P.Hl, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_52})

V_615 = Vertex(name = 'V_615',
               particles = [ P.G__minus__, P.HaQ, P.Hl, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_52})

V_616 = Vertex(name = 'V_616',
               particles = [ P.GQ__minus__, P.HaQ, P.Hl, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_149})

V_617 = Vertex(name = 'V_617',
               particles = [ P.G__minus__, P.Hl, P.Hl, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_34})

V_618 = Vertex(name = 'V_618',
               particles = [ P.GQ__minus__, P.Hl, P.Hl, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_34})

V_619 = Vertex(name = 'V_619',
               particles = [ P.G__minus__, P.Ha, P.HlQ, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_52})

V_620 = Vertex(name = 'V_620',
               particles = [ P.GQ__minus__, P.Ha, P.HlQ, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_52})

V_621 = Vertex(name = 'V_621',
               particles = [ P.G__minus__, P.HaQ, P.HlQ, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_152})

V_622 = Vertex(name = 'V_622',
               particles = [ P.GQ__minus__, P.HaQ, P.HlQ, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_52})

V_623 = Vertex(name = 'V_623',
               particles = [ P.G__minus__, P.Hl, P.HlQ, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_34})

V_624 = Vertex(name = 'V_624',
               particles = [ P.GQ__minus__, P.Hl, P.HlQ, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_125})

V_625 = Vertex(name = 'V_625',
               particles = [ P.G__minus__, P.HlQ, P.HlQ, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_129})

V_626 = Vertex(name = 'V_626',
               particles = [ P.GQ__minus__, P.HlQ, P.HlQ, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_34})

V_627 = Vertex(name = 'V_627',
               particles = [ P.G0, P.G__plus__, P.Hh, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_53})

V_628 = Vertex(name = 'V_628',
               particles = [ P.G__plus__, P.GQ0, P.Hh, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_153})

V_629 = Vertex(name = 'V_629',
               particles = [ P.G0, P.GQ__plus__, P.Hh, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_150})

V_630 = Vertex(name = 'V_630',
               particles = [ P.GQ0, P.GQ__plus__, P.Hh, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_53})

V_631 = Vertex(name = 'V_631',
               particles = [ P.G__plus__, P.Hh, P.Hh, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_37})

V_632 = Vertex(name = 'V_632',
               particles = [ P.GQ__plus__, P.Hh, P.Hh, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_134})

V_633 = Vertex(name = 'V_633',
               particles = [ P.G0, P.G__plus__, P.HhQ, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_53})

V_634 = Vertex(name = 'V_634',
               particles = [ P.G__plus__, P.GQ0, P.HhQ, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_53})

V_635 = Vertex(name = 'V_635',
               particles = [ P.G0, P.GQ__plus__, P.HhQ, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_53})

V_636 = Vertex(name = 'V_636',
               particles = [ P.GQ0, P.GQ__plus__, P.HhQ, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_53})

V_637 = Vertex(name = 'V_637',
               particles = [ P.G__plus__, P.Hh, P.HhQ, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_122})

V_638 = Vertex(name = 'V_638',
               particles = [ P.GQ__plus__, P.Hh, P.HhQ, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_37})

V_639 = Vertex(name = 'V_639',
               particles = [ P.G__plus__, P.HhQ, P.HhQ, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_37})

V_640 = Vertex(name = 'V_640',
               particles = [ P.GQ__plus__, P.HhQ, P.HhQ, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_37})

V_641 = Vertex(name = 'V_641',
               particles = [ P.G__plus__, P.Ha, P.Hl, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_53})

V_642 = Vertex(name = 'V_642',
               particles = [ P.GQ__plus__, P.Ha, P.Hl, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_150})

V_643 = Vertex(name = 'V_643',
               particles = [ P.G__plus__, P.HaQ, P.Hl, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_53})

V_644 = Vertex(name = 'V_644',
               particles = [ P.GQ__plus__, P.HaQ, P.Hl, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_53})

V_645 = Vertex(name = 'V_645',
               particles = [ P.G__plus__, P.Hl, P.Hl, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_34})

V_646 = Vertex(name = 'V_646',
               particles = [ P.GQ__plus__, P.Hl, P.Hl, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_129})

V_647 = Vertex(name = 'V_647',
               particles = [ P.G__plus__, P.Ha, P.HlQ, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_153})

V_648 = Vertex(name = 'V_648',
               particles = [ P.GQ__plus__, P.Ha, P.HlQ, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_53})

V_649 = Vertex(name = 'V_649',
               particles = [ P.G__plus__, P.HaQ, P.HlQ, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_53})

V_650 = Vertex(name = 'V_650',
               particles = [ P.GQ__plus__, P.HaQ, P.HlQ, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_53})

V_651 = Vertex(name = 'V_651',
               particles = [ P.G__plus__, P.Hl, P.HlQ, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_125})

V_652 = Vertex(name = 'V_652',
               particles = [ P.GQ__plus__, P.Hl, P.HlQ, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_34})

V_653 = Vertex(name = 'V_653',
               particles = [ P.G__plus__, P.HlQ, P.HlQ, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_34})

V_654 = Vertex(name = 'V_654',
               particles = [ P.GQ__plus__, P.HlQ, P.HlQ, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_34})

V_655 = Vertex(name = 'V_655',
               particles = [ P.G0, P.G__minus__, P.Hh, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_52})

V_656 = Vertex(name = 'V_656',
               particles = [ P.G__minus__, P.GQ0, P.Hh, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_149})

V_657 = Vertex(name = 'V_657',
               particles = [ P.G0, P.GQ__minus__, P.Hh, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_152})

V_658 = Vertex(name = 'V_658',
               particles = [ P.GQ0, P.GQ__minus__, P.Hh, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_52})

V_659 = Vertex(name = 'V_659',
               particles = [ P.G__minus__, P.Hh, P.Hh, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_37})

V_660 = Vertex(name = 'V_660',
               particles = [ P.GQ__minus__, P.Hh, P.Hh, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_134})

V_661 = Vertex(name = 'V_661',
               particles = [ P.G0, P.G__minus__, P.HhQ, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_52})

V_662 = Vertex(name = 'V_662',
               particles = [ P.G__minus__, P.GQ0, P.HhQ, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_52})

V_663 = Vertex(name = 'V_663',
               particles = [ P.G0, P.GQ__minus__, P.HhQ, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_52})

V_664 = Vertex(name = 'V_664',
               particles = [ P.GQ0, P.GQ__minus__, P.HhQ, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_52})

V_665 = Vertex(name = 'V_665',
               particles = [ P.G__minus__, P.Hh, P.HhQ, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_122})

V_666 = Vertex(name = 'V_666',
               particles = [ P.GQ__minus__, P.Hh, P.HhQ, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_37})

V_667 = Vertex(name = 'V_667',
               particles = [ P.G__minus__, P.HhQ, P.HhQ, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_37})

V_668 = Vertex(name = 'V_668',
               particles = [ P.GQ__minus__, P.HhQ, P.HhQ, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_37})

V_669 = Vertex(name = 'V_669',
               particles = [ P.G__minus__, P.Ha, P.Hl, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_52})

V_670 = Vertex(name = 'V_670',
               particles = [ P.GQ__minus__, P.Ha, P.Hl, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_152})

V_671 = Vertex(name = 'V_671',
               particles = [ P.G__minus__, P.HaQ, P.Hl, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_52})

V_672 = Vertex(name = 'V_672',
               particles = [ P.GQ__minus__, P.HaQ, P.Hl, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_52})

V_673 = Vertex(name = 'V_673',
               particles = [ P.G__minus__, P.Hl, P.Hl, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_34})

V_674 = Vertex(name = 'V_674',
               particles = [ P.GQ__minus__, P.Hl, P.Hl, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_129})

V_675 = Vertex(name = 'V_675',
               particles = [ P.G__minus__, P.Ha, P.HlQ, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_149})

V_676 = Vertex(name = 'V_676',
               particles = [ P.GQ__minus__, P.Ha, P.HlQ, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_52})

V_677 = Vertex(name = 'V_677',
               particles = [ P.G__minus__, P.HaQ, P.HlQ, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_52})

V_678 = Vertex(name = 'V_678',
               particles = [ P.GQ__minus__, P.HaQ, P.HlQ, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_52})

V_679 = Vertex(name = 'V_679',
               particles = [ P.G__minus__, P.Hl, P.HlQ, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_125})

V_680 = Vertex(name = 'V_680',
               particles = [ P.GQ__minus__, P.Hl, P.HlQ, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_34})

V_681 = Vertex(name = 'V_681',
               particles = [ P.G__minus__, P.HlQ, P.HlQ, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_34})

V_682 = Vertex(name = 'V_682',
               particles = [ P.GQ__minus__, P.HlQ, P.HlQ, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_34})

V_683 = Vertex(name = 'V_683',
               particles = [ P.G0, P.Ha, P.Hh, P.Hh ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_39})

V_684 = Vertex(name = 'V_684',
               particles = [ P.GQ0, P.Ha, P.Hh, P.Hh ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_39})

V_685 = Vertex(name = 'V_685',
               particles = [ P.G0, P.HaQ, P.Hh, P.Hh ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_39})

V_686 = Vertex(name = 'V_686',
               particles = [ P.G0, P.Ha, P.Hh, P.HhQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_39})

V_687 = Vertex(name = 'V_687',
               particles = [ P.GQ0, P.HaQ, P.Hh, P.HhQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_39})

V_688 = Vertex(name = 'V_688',
               particles = [ P.GQ0, P.Ha, P.HhQ, P.HhQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_39})

V_689 = Vertex(name = 'V_689',
               particles = [ P.G0, P.HaQ, P.HhQ, P.HhQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_39})

V_690 = Vertex(name = 'V_690',
               particles = [ P.GQ0, P.HaQ, P.HhQ, P.HhQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_39})

V_691 = Vertex(name = 'V_691',
               particles = [ P.G0, P.Ha, P.Hl, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_38})

V_692 = Vertex(name = 'V_692',
               particles = [ P.GQ0, P.Ha, P.Hl, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_38})

V_693 = Vertex(name = 'V_693',
               particles = [ P.G0, P.HaQ, P.Hl, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_38})

V_694 = Vertex(name = 'V_694',
               particles = [ P.G0, P.Ha, P.Hl, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_38})

V_695 = Vertex(name = 'V_695',
               particles = [ P.GQ0, P.HaQ, P.Hl, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_38})

V_696 = Vertex(name = 'V_696',
               particles = [ P.GQ0, P.Ha, P.HlQ, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_38})

V_697 = Vertex(name = 'V_697',
               particles = [ P.G0, P.HaQ, P.HlQ, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_38})

V_698 = Vertex(name = 'V_698',
               particles = [ P.GQ0, P.HaQ, P.HlQ, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_38})

V_699 = Vertex(name = 'V_699',
               particles = [ P.G0, P.G0, P.G0, P.Ha ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_59})

V_700 = Vertex(name = 'V_700',
               particles = [ P.G0, P.G__minus__, P.G__plus__, P.Ha ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_55})

V_701 = Vertex(name = 'V_701',
               particles = [ P.G0, P.G0, P.GQ0, P.Ha ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_59})

V_702 = Vertex(name = 'V_702',
               particles = [ P.G__minus__, P.G__plus__, P.GQ0, P.Ha ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_55})

V_703 = Vertex(name = 'V_703',
               particles = [ P.G0, P.GQ0, P.GQ0, P.Ha ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_59})

V_704 = Vertex(name = 'V_704',
               particles = [ P.GQ0, P.GQ0, P.GQ0, P.Ha ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_59})

V_705 = Vertex(name = 'V_705',
               particles = [ P.G0, P.G__plus__, P.GQ__minus__, P.Ha ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_55})

V_706 = Vertex(name = 'V_706',
               particles = [ P.G__plus__, P.GQ0, P.GQ__minus__, P.Ha ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_55})

V_707 = Vertex(name = 'V_707',
               particles = [ P.G0, P.G__minus__, P.GQ__plus__, P.Ha ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_55})

V_708 = Vertex(name = 'V_708',
               particles = [ P.G__minus__, P.GQ0, P.GQ__plus__, P.Ha ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_55})

V_709 = Vertex(name = 'V_709',
               particles = [ P.G0, P.GQ__minus__, P.GQ__plus__, P.Ha ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_55})

V_710 = Vertex(name = 'V_710',
               particles = [ P.GQ0, P.GQ__minus__, P.GQ__plus__, P.Ha ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_55})

V_711 = Vertex(name = 'V_711',
               particles = [ P.G0, P.G0, P.G0, P.HaQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_59})

V_712 = Vertex(name = 'V_712',
               particles = [ P.G0, P.G__minus__, P.G__plus__, P.HaQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_55})

V_713 = Vertex(name = 'V_713',
               particles = [ P.G0, P.G0, P.GQ0, P.HaQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_59})

V_714 = Vertex(name = 'V_714',
               particles = [ P.G__minus__, P.G__plus__, P.GQ0, P.HaQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_55})

V_715 = Vertex(name = 'V_715',
               particles = [ P.G0, P.GQ0, P.GQ0, P.HaQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_59})

V_716 = Vertex(name = 'V_716',
               particles = [ P.GQ0, P.GQ0, P.GQ0, P.HaQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_59})

V_717 = Vertex(name = 'V_717',
               particles = [ P.G0, P.G__plus__, P.GQ__minus__, P.HaQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_55})

V_718 = Vertex(name = 'V_718',
               particles = [ P.G__plus__, P.GQ0, P.GQ__minus__, P.HaQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_55})

V_719 = Vertex(name = 'V_719',
               particles = [ P.G0, P.G__minus__, P.GQ__plus__, P.HaQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_55})

V_720 = Vertex(name = 'V_720',
               particles = [ P.G__minus__, P.GQ0, P.GQ__plus__, P.HaQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_55})

V_721 = Vertex(name = 'V_721',
               particles = [ P.G0, P.GQ__minus__, P.GQ__plus__, P.HaQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_55})

V_722 = Vertex(name = 'V_722',
               particles = [ P.GQ0, P.GQ__minus__, P.GQ__plus__, P.HaQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_55})

V_723 = Vertex(name = 'V_723',
               particles = [ P.G0, P.G0, P.G__plus__, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_55})

V_724 = Vertex(name = 'V_724',
               particles = [ P.G__minus__, P.G__plus__, P.G__plus__, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_57})

V_725 = Vertex(name = 'V_725',
               particles = [ P.G0, P.G__plus__, P.GQ0, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_55})

V_726 = Vertex(name = 'V_726',
               particles = [ P.G__plus__, P.GQ0, P.GQ0, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_55})

V_727 = Vertex(name = 'V_727',
               particles = [ P.G__plus__, P.G__plus__, P.GQ__minus__, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_57})

V_728 = Vertex(name = 'V_728',
               particles = [ P.G0, P.G0, P.GQ__plus__, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_55})

V_729 = Vertex(name = 'V_729',
               particles = [ P.G__minus__, P.G__plus__, P.GQ__plus__, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_57})

V_730 = Vertex(name = 'V_730',
               particles = [ P.G0, P.GQ0, P.GQ__plus__, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_55})

V_731 = Vertex(name = 'V_731',
               particles = [ P.GQ0, P.GQ0, P.GQ__plus__, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_55})

V_732 = Vertex(name = 'V_732',
               particles = [ P.G__plus__, P.GQ__minus__, P.GQ__plus__, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_57})

V_733 = Vertex(name = 'V_733',
               particles = [ P.G__minus__, P.GQ__plus__, P.GQ__plus__, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_57})

V_734 = Vertex(name = 'V_734',
               particles = [ P.GQ__minus__, P.GQ__plus__, P.GQ__plus__, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_57})

V_735 = Vertex(name = 'V_735',
               particles = [ P.G0, P.G0, P.G__minus__, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_55})

V_736 = Vertex(name = 'V_736',
               particles = [ P.G__minus__, P.G__minus__, P.G__plus__, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_57})

V_737 = Vertex(name = 'V_737',
               particles = [ P.G0, P.G__minus__, P.GQ0, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_55})

V_738 = Vertex(name = 'V_738',
               particles = [ P.G__minus__, P.GQ0, P.GQ0, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_55})

V_739 = Vertex(name = 'V_739',
               particles = [ P.G0, P.G0, P.GQ__minus__, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_55})

V_740 = Vertex(name = 'V_740',
               particles = [ P.G__minus__, P.G__plus__, P.GQ__minus__, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_57})

V_741 = Vertex(name = 'V_741',
               particles = [ P.G0, P.GQ0, P.GQ__minus__, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_55})

V_742 = Vertex(name = 'V_742',
               particles = [ P.GQ0, P.GQ0, P.GQ__minus__, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_55})

V_743 = Vertex(name = 'V_743',
               particles = [ P.G__plus__, P.GQ__minus__, P.GQ__minus__, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_57})

V_744 = Vertex(name = 'V_744',
               particles = [ P.G__minus__, P.G__minus__, P.GQ__plus__, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_57})

V_745 = Vertex(name = 'V_745',
               particles = [ P.G__minus__, P.GQ__minus__, P.GQ__plus__, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_57})

V_746 = Vertex(name = 'V_746',
               particles = [ P.GQ__minus__, P.GQ__minus__, P.GQ__plus__, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_57})

V_747 = Vertex(name = 'V_747',
               particles = [ P.G0, P.G0, P.G__plus__, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_55})

V_748 = Vertex(name = 'V_748',
               particles = [ P.G__minus__, P.G__plus__, P.G__plus__, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_57})

V_749 = Vertex(name = 'V_749',
               particles = [ P.G0, P.G__plus__, P.GQ0, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_55})

V_750 = Vertex(name = 'V_750',
               particles = [ P.G__plus__, P.GQ0, P.GQ0, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_55})

V_751 = Vertex(name = 'V_751',
               particles = [ P.G__plus__, P.G__plus__, P.GQ__minus__, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_57})

V_752 = Vertex(name = 'V_752',
               particles = [ P.G0, P.G0, P.GQ__plus__, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_55})

V_753 = Vertex(name = 'V_753',
               particles = [ P.G__minus__, P.G__plus__, P.GQ__plus__, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_57})

V_754 = Vertex(name = 'V_754',
               particles = [ P.G0, P.GQ0, P.GQ__plus__, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_55})

V_755 = Vertex(name = 'V_755',
               particles = [ P.GQ0, P.GQ0, P.GQ__plus__, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_55})

V_756 = Vertex(name = 'V_756',
               particles = [ P.G__plus__, P.GQ__minus__, P.GQ__plus__, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_57})

V_757 = Vertex(name = 'V_757',
               particles = [ P.G__minus__, P.GQ__plus__, P.GQ__plus__, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_57})

V_758 = Vertex(name = 'V_758',
               particles = [ P.GQ__minus__, P.GQ__plus__, P.GQ__plus__, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_57})

V_759 = Vertex(name = 'V_759',
               particles = [ P.G0, P.G0, P.G__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_55})

V_760 = Vertex(name = 'V_760',
               particles = [ P.G__minus__, P.G__minus__, P.G__plus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_57})

V_761 = Vertex(name = 'V_761',
               particles = [ P.G0, P.G__minus__, P.GQ0, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_55})

V_762 = Vertex(name = 'V_762',
               particles = [ P.G__minus__, P.GQ0, P.GQ0, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_55})

V_763 = Vertex(name = 'V_763',
               particles = [ P.G0, P.G0, P.GQ__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_55})

V_764 = Vertex(name = 'V_764',
               particles = [ P.G__minus__, P.G__plus__, P.GQ__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_57})

V_765 = Vertex(name = 'V_765',
               particles = [ P.G0, P.GQ0, P.GQ__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_55})

V_766 = Vertex(name = 'V_766',
               particles = [ P.GQ0, P.GQ0, P.GQ__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_55})

V_767 = Vertex(name = 'V_767',
               particles = [ P.G__plus__, P.GQ__minus__, P.GQ__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_57})

V_768 = Vertex(name = 'V_768',
               particles = [ P.G__minus__, P.G__minus__, P.GQ__plus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_57})

V_769 = Vertex(name = 'V_769',
               particles = [ P.G__minus__, P.GQ__minus__, P.GQ__plus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_57})

V_770 = Vertex(name = 'V_770',
               particles = [ P.GQ__minus__, P.GQ__minus__, P.GQ__plus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_57})

V_771 = Vertex(name = 'V_771',
               particles = [ P.G0, P.Ha, P.Ha, P.Ha ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_58})

V_772 = Vertex(name = 'V_772',
               particles = [ P.GQ0, P.Ha, P.Ha, P.Ha ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_58})

V_773 = Vertex(name = 'V_773',
               particles = [ P.G0, P.Ha, P.Ha, P.HaQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_58})

V_774 = Vertex(name = 'V_774',
               particles = [ P.GQ0, P.Ha, P.Ha, P.HaQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_58})

V_775 = Vertex(name = 'V_775',
               particles = [ P.G0, P.Ha, P.HaQ, P.HaQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_58})

V_776 = Vertex(name = 'V_776',
               particles = [ P.GQ0, P.Ha, P.HaQ, P.HaQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_58})

V_777 = Vertex(name = 'V_777',
               particles = [ P.G0, P.HaQ, P.HaQ, P.HaQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_58})

V_778 = Vertex(name = 'V_778',
               particles = [ P.GQ0, P.HaQ, P.HaQ, P.HaQ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_58})

V_779 = Vertex(name = 'V_779',
               particles = [ P.G__plus__, P.Ha, P.Ha, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_54})

V_780 = Vertex(name = 'V_780',
               particles = [ P.GQ__plus__, P.Ha, P.Ha, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_54})

V_781 = Vertex(name = 'V_781',
               particles = [ P.G__plus__, P.Ha, P.HaQ, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_54})

V_782 = Vertex(name = 'V_782',
               particles = [ P.GQ__plus__, P.Ha, P.HaQ, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_54})

V_783 = Vertex(name = 'V_783',
               particles = [ P.G__plus__, P.HaQ, P.HaQ, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_54})

V_784 = Vertex(name = 'V_784',
               particles = [ P.GQ__plus__, P.HaQ, P.HaQ, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_54})

V_785 = Vertex(name = 'V_785',
               particles = [ P.G__minus__, P.Ha, P.Ha, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_54})

V_786 = Vertex(name = 'V_786',
               particles = [ P.GQ__minus__, P.Ha, P.Ha, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_54})

V_787 = Vertex(name = 'V_787',
               particles = [ P.G__minus__, P.Ha, P.HaQ, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_54})

V_788 = Vertex(name = 'V_788',
               particles = [ P.GQ__minus__, P.Ha, P.HaQ, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_54})

V_789 = Vertex(name = 'V_789',
               particles = [ P.G__minus__, P.HaQ, P.HaQ, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_54})

V_790 = Vertex(name = 'V_790',
               particles = [ P.GQ__minus__, P.HaQ, P.HaQ, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_54})

V_791 = Vertex(name = 'V_791',
               particles = [ P.G0, P.Ha, P.H__minus__, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_54})

V_792 = Vertex(name = 'V_792',
               particles = [ P.GQ0, P.Ha, P.H__minus__, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_54})

V_793 = Vertex(name = 'V_793',
               particles = [ P.G0, P.HaQ, P.H__minus__, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_54})

V_794 = Vertex(name = 'V_794',
               particles = [ P.GQ0, P.HaQ, P.H__minus__, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_54})

V_795 = Vertex(name = 'V_795',
               particles = [ P.G__plus__, P.H__minus__, P.H__minus__, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_56})

V_796 = Vertex(name = 'V_796',
               particles = [ P.GQ__plus__, P.H__minus__, P.H__minus__, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_56})

V_797 = Vertex(name = 'V_797',
               particles = [ P.G__minus__, P.H__minus__, P.H__plus__, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_56})

V_798 = Vertex(name = 'V_798',
               particles = [ P.GQ__minus__, P.H__minus__, P.H__plus__, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_56})

V_799 = Vertex(name = 'V_799',
               particles = [ P.G__plus__, P.Ha, P.Ha, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_54})

V_800 = Vertex(name = 'V_800',
               particles = [ P.GQ__plus__, P.Ha, P.Ha, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_54})

V_801 = Vertex(name = 'V_801',
               particles = [ P.G__plus__, P.Ha, P.HaQ, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_54})

V_802 = Vertex(name = 'V_802',
               particles = [ P.GQ__plus__, P.Ha, P.HaQ, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_54})

V_803 = Vertex(name = 'V_803',
               particles = [ P.G__plus__, P.HaQ, P.HaQ, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_54})

V_804 = Vertex(name = 'V_804',
               particles = [ P.GQ__plus__, P.HaQ, P.HaQ, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_54})

V_805 = Vertex(name = 'V_805',
               particles = [ P.G0, P.Ha, P.H__plus__, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_54})

V_806 = Vertex(name = 'V_806',
               particles = [ P.GQ0, P.Ha, P.H__plus__, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_54})

V_807 = Vertex(name = 'V_807',
               particles = [ P.G0, P.HaQ, P.H__plus__, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_54})

V_808 = Vertex(name = 'V_808',
               particles = [ P.GQ0, P.HaQ, P.H__plus__, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_54})

V_809 = Vertex(name = 'V_809',
               particles = [ P.G__plus__, P.H__minus__, P.H__plus__, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_56})

V_810 = Vertex(name = 'V_810',
               particles = [ P.GQ__plus__, P.H__minus__, P.H__plus__, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_56})

V_811 = Vertex(name = 'V_811',
               particles = [ P.G__minus__, P.H__plus__, P.H__plus__, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_56})

V_812 = Vertex(name = 'V_812',
               particles = [ P.GQ__minus__, P.H__plus__, P.H__plus__, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_56})

V_813 = Vertex(name = 'V_813',
               particles = [ P.G__plus__, P.H__plus__, P.HQ__minus__, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_56})

V_814 = Vertex(name = 'V_814',
               particles = [ P.GQ__plus__, P.H__plus__, P.HQ__minus__, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_56})

V_815 = Vertex(name = 'V_815',
               particles = [ P.G__minus__, P.Ha, P.Ha, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_54})

V_816 = Vertex(name = 'V_816',
               particles = [ P.GQ__minus__, P.Ha, P.Ha, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_54})

V_817 = Vertex(name = 'V_817',
               particles = [ P.G__minus__, P.Ha, P.HaQ, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_54})

V_818 = Vertex(name = 'V_818',
               particles = [ P.GQ__minus__, P.Ha, P.HaQ, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_54})

V_819 = Vertex(name = 'V_819',
               particles = [ P.G__minus__, P.HaQ, P.HaQ, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_54})

V_820 = Vertex(name = 'V_820',
               particles = [ P.GQ__minus__, P.HaQ, P.HaQ, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_54})

V_821 = Vertex(name = 'V_821',
               particles = [ P.G0, P.Ha, P.H__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_54})

V_822 = Vertex(name = 'V_822',
               particles = [ P.GQ0, P.Ha, P.H__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_54})

V_823 = Vertex(name = 'V_823',
               particles = [ P.G0, P.HaQ, P.H__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_54})

V_824 = Vertex(name = 'V_824',
               particles = [ P.GQ0, P.HaQ, P.H__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_54})

V_825 = Vertex(name = 'V_825',
               particles = [ P.G__plus__, P.H__minus__, P.H__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_56})

V_826 = Vertex(name = 'V_826',
               particles = [ P.GQ__plus__, P.H__minus__, P.H__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_56})

V_827 = Vertex(name = 'V_827',
               particles = [ P.G__minus__, P.H__minus__, P.H__plus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_56})

V_828 = Vertex(name = 'V_828',
               particles = [ P.GQ__minus__, P.H__minus__, P.H__plus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_56})

V_829 = Vertex(name = 'V_829',
               particles = [ P.G0, P.Ha, P.HQ__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_54})

V_830 = Vertex(name = 'V_830',
               particles = [ P.GQ0, P.Ha, P.HQ__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_54})

V_831 = Vertex(name = 'V_831',
               particles = [ P.G0, P.HaQ, P.HQ__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_54})

V_832 = Vertex(name = 'V_832',
               particles = [ P.GQ0, P.HaQ, P.HQ__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_54})

V_833 = Vertex(name = 'V_833',
               particles = [ P.G__plus__, P.H__minus__, P.HQ__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_56})

V_834 = Vertex(name = 'V_834',
               particles = [ P.GQ__plus__, P.H__minus__, P.HQ__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_56})

V_835 = Vertex(name = 'V_835',
               particles = [ P.G__minus__, P.H__plus__, P.HQ__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_56})

V_836 = Vertex(name = 'V_836',
               particles = [ P.GQ__minus__, P.H__plus__, P.HQ__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_56})

V_837 = Vertex(name = 'V_837',
               particles = [ P.G__plus__, P.HQ__minus__, P.HQ__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_56})

V_838 = Vertex(name = 'V_838',
               particles = [ P.GQ__plus__, P.HQ__minus__, P.HQ__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_56})

V_839 = Vertex(name = 'V_839',
               particles = [ P.G__minus__, P.H__minus__, P.HQ__plus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_56})

V_840 = Vertex(name = 'V_840',
               particles = [ P.GQ__minus__, P.H__minus__, P.HQ__plus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_56})

V_841 = Vertex(name = 'V_841',
               particles = [ P.G__minus__, P.HQ__minus__, P.HQ__plus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_56})

V_842 = Vertex(name = 'V_842',
               particles = [ P.GQ__minus__, P.HQ__minus__, P.HQ__plus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSSS1 ],
               couplings = {(0,0):C.GC_56})

V_843 = Vertex(name = 'V_843',
               particles = [ P.ghWm, P.ghWm__tilde__, P.G0, P.G0 ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_99})

V_844 = Vertex(name = 'V_844',
               particles = [ P.ghWp, P.ghWp__tilde__, P.G0, P.G0 ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_99})

V_845 = Vertex(name = 'V_845',
               particles = [ P.ghWm, P.ghWm__tilde__, P.G__minus__, P.G__plus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_99})

V_846 = Vertex(name = 'V_846',
               particles = [ P.ghWp, P.ghWp__tilde__, P.G__minus__, P.G__plus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_99})

V_847 = Vertex(name = 'V_847',
               particles = [ P.ghWm, P.ghWm__tilde__, P.G0, P.GQ0 ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_98})

V_848 = Vertex(name = 'V_848',
               particles = [ P.ghWp, P.ghWp__tilde__, P.G0, P.GQ0 ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_98})

V_849 = Vertex(name = 'V_849',
               particles = [ P.ghWm, P.ghWm__tilde__, P.G__plus__, P.GQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_99})

V_850 = Vertex(name = 'V_850',
               particles = [ P.ghWp, P.ghWp__tilde__, P.G__minus__, P.GQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_99})

V_851 = Vertex(name = 'V_851',
               particles = [ P.ghWm, P.ghWm__tilde__, P.Ha, P.Ha ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_99})

V_852 = Vertex(name = 'V_852',
               particles = [ P.ghWp, P.ghWp__tilde__, P.Ha, P.Ha ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_99})

V_853 = Vertex(name = 'V_853',
               particles = [ P.ghWm, P.ghWm__tilde__, P.Ha, P.HaQ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_98})

V_854 = Vertex(name = 'V_854',
               particles = [ P.ghWp, P.ghWp__tilde__, P.Ha, P.HaQ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_98})

V_855 = Vertex(name = 'V_855',
               particles = [ P.ghWm, P.ghWm__tilde__, P.GQ0, P.Hh ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_91})

V_856 = Vertex(name = 'V_856',
               particles = [ P.ghWp, P.ghWp__tilde__, P.GQ0, P.Hh ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_92})

V_857 = Vertex(name = 'V_857',
               particles = [ P.ghWm, P.ghWm__tilde__, P.Hh, P.Hh ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_76})

V_858 = Vertex(name = 'V_858',
               particles = [ P.ghWp, P.ghWp__tilde__, P.Hh, P.Hh ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_76})

V_859 = Vertex(name = 'V_859',
               particles = [ P.ghWm, P.ghWm__tilde__, P.G0, P.HhQ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_92})

V_860 = Vertex(name = 'V_860',
               particles = [ P.ghWp, P.ghWp__tilde__, P.G0, P.HhQ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_91})

V_861 = Vertex(name = 'V_861',
               particles = [ P.ghWm, P.ghWm__tilde__, P.Hh, P.HhQ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_75})

V_862 = Vertex(name = 'V_862',
               particles = [ P.ghWp, P.ghWp__tilde__, P.Hh, P.HhQ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_75})

V_863 = Vertex(name = 'V_863',
               particles = [ P.ghWm, P.ghWm__tilde__, P.HaQ, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_91})

V_864 = Vertex(name = 'V_864',
               particles = [ P.ghWp, P.ghWp__tilde__, P.HaQ, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_92})

V_865 = Vertex(name = 'V_865',
               particles = [ P.ghWm, P.ghWm__tilde__, P.Hl, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_76})

V_866 = Vertex(name = 'V_866',
               particles = [ P.ghWp, P.ghWp__tilde__, P.Hl, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_76})

V_867 = Vertex(name = 'V_867',
               particles = [ P.ghWm, P.ghWm__tilde__, P.Ha, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_92})

V_868 = Vertex(name = 'V_868',
               particles = [ P.ghWp, P.ghWp__tilde__, P.Ha, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_91})

V_869 = Vertex(name = 'V_869',
               particles = [ P.ghWm, P.ghWm__tilde__, P.Hl, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_75})

V_870 = Vertex(name = 'V_870',
               particles = [ P.ghWp, P.ghWp__tilde__, P.Hl, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_75})

V_871 = Vertex(name = 'V_871',
               particles = [ P.ghWm, P.ghWm__tilde__, P.H__minus__, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_99})

V_872 = Vertex(name = 'V_872',
               particles = [ P.ghWp, P.ghWp__tilde__, P.H__minus__, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_99})

V_873 = Vertex(name = 'V_873',
               particles = [ P.ghWm, P.ghWm__tilde__, P.H__plus__, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_99})

V_874 = Vertex(name = 'V_874',
               particles = [ P.ghWp, P.ghWp__tilde__, P.H__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_99})

V_875 = Vertex(name = 'V_875',
               particles = [ P.ghWm, P.ghWm__tilde__, P.HaQ, P.Hh ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_80})

V_876 = Vertex(name = 'V_876',
               particles = [ P.ghWp, P.ghWp__tilde__, P.HaQ, P.Hh ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_79})

V_877 = Vertex(name = 'V_877',
               particles = [ P.ghWm, P.ghWm__tilde__, P.Ha, P.HhQ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_79})

V_878 = Vertex(name = 'V_878',
               particles = [ P.ghWp, P.ghWp__tilde__, P.Ha, P.HhQ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_80})

V_879 = Vertex(name = 'V_879',
               particles = [ P.ghWm, P.ghWm__tilde__, P.GQ0, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_79})

V_880 = Vertex(name = 'V_880',
               particles = [ P.ghWp, P.ghWp__tilde__, P.GQ0, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_80})

V_881 = Vertex(name = 'V_881',
               particles = [ P.ghWm, P.ghWm__tilde__, P.G0, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_80})

V_882 = Vertex(name = 'V_882',
               particles = [ P.ghWp, P.ghWp__tilde__, P.G0, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_79})

V_883 = Vertex(name = 'V_883',
               particles = [ P.ghA, P.ghWm__tilde__, P.G0, P.G__minus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_187})

V_884 = Vertex(name = 'V_884',
               particles = [ P.ghWp, P.ghA__tilde__, P.G0, P.G__minus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_187})

V_885 = Vertex(name = 'V_885',
               particles = [ P.ghWm, P.ghA__tilde__, P.G0, P.G__plus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_186})

V_886 = Vertex(name = 'V_886',
               particles = [ P.ghA, P.ghWp__tilde__, P.G0, P.G__plus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_186})

V_887 = Vertex(name = 'V_887',
               particles = [ P.ghA, P.ghZ__tilde__, P.G__minus__, P.G__plus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_220})

V_888 = Vertex(name = 'V_888',
               particles = [ P.ghZ, P.ghA__tilde__, P.G__minus__, P.G__plus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_220})

V_889 = Vertex(name = 'V_889',
               particles = [ P.ghA, P.ghWm__tilde__, P.G__minus__, P.GQ0 ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_187})

V_890 = Vertex(name = 'V_890',
               particles = [ P.ghA, P.ghWp__tilde__, P.G__plus__, P.GQ0 ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_186})

V_891 = Vertex(name = 'V_891',
               particles = [ P.ghWp, P.ghA__tilde__, P.G0, P.GQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_187})

V_892 = Vertex(name = 'V_892',
               particles = [ P.ghA, P.ghZ__tilde__, P.G__plus__, P.GQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_219})

V_893 = Vertex(name = 'V_893',
               particles = [ P.ghZ, P.ghA__tilde__, P.G__plus__, P.GQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_219})

V_894 = Vertex(name = 'V_894',
               particles = [ P.ghWm, P.ghA__tilde__, P.G0, P.GQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_186})

V_895 = Vertex(name = 'V_895',
               particles = [ P.ghA, P.ghZ__tilde__, P.G__minus__, P.GQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_219})

V_896 = Vertex(name = 'V_896',
               particles = [ P.ghZ, P.ghA__tilde__, P.G__minus__, P.GQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_219})

V_897 = Vertex(name = 'V_897',
               particles = [ P.ghA, P.ghWm__tilde__, P.G__minus__, P.Hh ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_181})

V_898 = Vertex(name = 'V_898',
               particles = [ P.ghWp, P.ghA__tilde__, P.G__minus__, P.Hh ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_181})

V_899 = Vertex(name = 'V_899',
               particles = [ P.ghWm, P.ghA__tilde__, P.G__plus__, P.Hh ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_181})

V_900 = Vertex(name = 'V_900',
               particles = [ P.ghA, P.ghWp__tilde__, P.G__plus__, P.Hh ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_181})

V_901 = Vertex(name = 'V_901',
               particles = [ P.ghWp, P.ghA__tilde__, P.GQ__minus__, P.Hh ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_181})

V_902 = Vertex(name = 'V_902',
               particles = [ P.ghWm, P.ghA__tilde__, P.GQ__plus__, P.Hh ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_181})

V_903 = Vertex(name = 'V_903',
               particles = [ P.ghA, P.ghWm__tilde__, P.G__minus__, P.HhQ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_181})

V_904 = Vertex(name = 'V_904',
               particles = [ P.ghA, P.ghWp__tilde__, P.G__plus__, P.HhQ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_181})

V_905 = Vertex(name = 'V_905',
               particles = [ P.ghA, P.ghWm__tilde__, P.Ha, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_187})

V_906 = Vertex(name = 'V_906',
               particles = [ P.ghWp, P.ghA__tilde__, P.Ha, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_187})

V_907 = Vertex(name = 'V_907',
               particles = [ P.ghA, P.ghWm__tilde__, P.HaQ, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_187})

V_908 = Vertex(name = 'V_908',
               particles = [ P.ghA, P.ghWm__tilde__, P.Hl, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_181})

V_909 = Vertex(name = 'V_909',
               particles = [ P.ghWp, P.ghA__tilde__, P.Hl, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_181})

V_910 = Vertex(name = 'V_910',
               particles = [ P.ghA, P.ghWm__tilde__, P.HlQ, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_181})

V_911 = Vertex(name = 'V_911',
               particles = [ P.ghWm, P.ghA__tilde__, P.Ha, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_186})

V_912 = Vertex(name = 'V_912',
               particles = [ P.ghA, P.ghWp__tilde__, P.Ha, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_186})

V_913 = Vertex(name = 'V_913',
               particles = [ P.ghA, P.ghWp__tilde__, P.HaQ, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_186})

V_914 = Vertex(name = 'V_914',
               particles = [ P.ghWm, P.ghA__tilde__, P.Hl, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_181})

V_915 = Vertex(name = 'V_915',
               particles = [ P.ghA, P.ghWp__tilde__, P.Hl, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_181})

V_916 = Vertex(name = 'V_916',
               particles = [ P.ghA, P.ghWp__tilde__, P.HlQ, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_181})

V_917 = Vertex(name = 'V_917',
               particles = [ P.ghA, P.ghZ__tilde__, P.H__minus__, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_220})

V_918 = Vertex(name = 'V_918',
               particles = [ P.ghZ, P.ghA__tilde__, P.H__minus__, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_220})

V_919 = Vertex(name = 'V_919',
               particles = [ P.ghWp, P.ghA__tilde__, P.Ha, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_187})

V_920 = Vertex(name = 'V_920',
               particles = [ P.ghWp, P.ghA__tilde__, P.Hl, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_181})

V_921 = Vertex(name = 'V_921',
               particles = [ P.ghA, P.ghZ__tilde__, P.H__plus__, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_219})

V_922 = Vertex(name = 'V_922',
               particles = [ P.ghZ, P.ghA__tilde__, P.H__plus__, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_219})

V_923 = Vertex(name = 'V_923',
               particles = [ P.ghWm, P.ghA__tilde__, P.Ha, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_186})

V_924 = Vertex(name = 'V_924',
               particles = [ P.ghWm, P.ghA__tilde__, P.Hl, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_181})

V_925 = Vertex(name = 'V_925',
               particles = [ P.ghA, P.ghZ__tilde__, P.H__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_219})

V_926 = Vertex(name = 'V_926',
               particles = [ P.ghZ, P.ghA__tilde__, P.H__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_219})

V_927 = Vertex(name = 'V_927',
               particles = [ P.ghA, P.ghWm__tilde__, P.G__minus__, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_175})

V_928 = Vertex(name = 'V_928',
               particles = [ P.ghWp, P.ghA__tilde__, P.G__minus__, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_175})

V_929 = Vertex(name = 'V_929',
               particles = [ P.ghWm, P.ghA__tilde__, P.G__plus__, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_175})

V_930 = Vertex(name = 'V_930',
               particles = [ P.ghA, P.ghWp__tilde__, P.G__plus__, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_175})

V_931 = Vertex(name = 'V_931',
               particles = [ P.ghWp, P.ghA__tilde__, P.GQ__minus__, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_175})

V_932 = Vertex(name = 'V_932',
               particles = [ P.ghWm, P.ghA__tilde__, P.GQ__plus__, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_175})

V_933 = Vertex(name = 'V_933',
               particles = [ P.ghA, P.ghWm__tilde__, P.G__minus__, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_175})

V_934 = Vertex(name = 'V_934',
               particles = [ P.ghA, P.ghWp__tilde__, P.G__plus__, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_175})

V_935 = Vertex(name = 'V_935',
               particles = [ P.ghA, P.ghWm__tilde__, P.Hh, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_174})

V_936 = Vertex(name = 'V_936',
               particles = [ P.ghWp, P.ghA__tilde__, P.Hh, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_174})

V_937 = Vertex(name = 'V_937',
               particles = [ P.ghA, P.ghWm__tilde__, P.HhQ, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_174})

V_938 = Vertex(name = 'V_938',
               particles = [ P.ghWm, P.ghA__tilde__, P.Hh, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_174})

V_939 = Vertex(name = 'V_939',
               particles = [ P.ghA, P.ghWp__tilde__, P.Hh, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_174})

V_940 = Vertex(name = 'V_940',
               particles = [ P.ghA, P.ghWp__tilde__, P.HhQ, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_174})

V_941 = Vertex(name = 'V_941',
               particles = [ P.ghWp, P.ghA__tilde__, P.Hh, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_174})

V_942 = Vertex(name = 'V_942',
               particles = [ P.ghWm, P.ghA__tilde__, P.Hh, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.UUSS1 ],
               couplings = {(0,0):C.GC_174})

V_943 = Vertex(name = 'V_943',
               particles = [ P.ghWp, P.ghZ__tilde__, P.G__minus__ ],
               color = [ '1' ],
               lorentz = [ L.UUS1 ],
               couplings = {(0,0):C.GC_238})

V_944 = Vertex(name = 'V_944',
               particles = [ P.ghZ, P.ghWm__tilde__, P.G__minus__ ],
               color = [ '1' ],
               lorentz = [ L.UUS1 ],
               couplings = {(0,0):C.GC_238})

V_945 = Vertex(name = 'V_945',
               particles = [ P.ghWm, P.ghZ__tilde__, P.G__plus__ ],
               color = [ '1' ],
               lorentz = [ L.UUS1 ],
               couplings = {(0,0):C.GC_238})

V_946 = Vertex(name = 'V_946',
               particles = [ P.ghZ, P.ghWp__tilde__, P.G__plus__ ],
               color = [ '1' ],
               lorentz = [ L.UUS1 ],
               couplings = {(0,0):C.GC_238})

V_947 = Vertex(name = 'V_947',
               particles = [ P.ghWp, P.ghZ__tilde__, P.GQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.UUS1 ],
               couplings = {(0,0):C.GC_283})

V_948 = Vertex(name = 'V_948',
               particles = [ P.ghZ, P.ghWm__tilde__, P.GQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.UUS1 ],
               couplings = {(0,0):C.GC_284})

V_949 = Vertex(name = 'V_949',
               particles = [ P.G__plus__, P.GQ0, P.GQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSS1 ],
               couplings = {(0,0):C.GC_255})

V_950 = Vertex(name = 'V_950',
               particles = [ P.ghWm, P.ghZ__tilde__, P.GQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.UUS1 ],
               couplings = {(0,0):C.GC_283})

V_951 = Vertex(name = 'V_951',
               particles = [ P.ghZ, P.ghWp__tilde__, P.GQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.UUS1 ],
               couplings = {(0,0):C.GC_284})

V_952 = Vertex(name = 'V_952',
               particles = [ P.G__minus__, P.GQ0, P.GQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSS1 ],
               couplings = {(0,0):C.GC_256})

V_953 = Vertex(name = 'V_953',
               particles = [ P.ghZ, P.ghZ__tilde__, P.Hh ],
               color = [ '1' ],
               lorentz = [ L.UUS1 ],
               couplings = {(0,0):C.GC_331})

V_954 = Vertex(name = 'V_954',
               particles = [ P.GQ0, P.GQ0, P.Hh ],
               color = [ '1' ],
               lorentz = [ L.SSS1 ],
               couplings = {(0,0):C.GC_314})

V_955 = Vertex(name = 'V_955',
               particles = [ P.ghZ, P.ghZ__tilde__, P.HhQ ],
               color = [ '1' ],
               lorentz = [ L.UUS1 ],
               couplings = {(0,0):C.GC_330})

V_956 = Vertex(name = 'V_956',
               particles = [ P.G0, P.GQ0, P.HhQ ],
               color = [ '1' ],
               lorentz = [ L.SSS1 ],
               couplings = {(0,0):C.GC_310})

V_957 = Vertex(name = 'V_957',
               particles = [ P.GQ0, P.HaQ, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.SSS1 ],
               couplings = {(0,0):C.GC_308})

V_958 = Vertex(name = 'V_958',
               particles = [ P.GQ0, P.Ha, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSS1 ],
               couplings = {(0,0):C.GC_312})

V_959 = Vertex(name = 'V_959',
               particles = [ P.GQ0, P.H__plus__, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSS1 ],
               couplings = {(0,0):C.GC_318})

V_960 = Vertex(name = 'V_960',
               particles = [ P.GQ0, P.H__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSS1 ],
               couplings = {(0,0):C.GC_315})

V_961 = Vertex(name = 'V_961',
               particles = [ P.G0, P.G0, P.Hh ],
               color = [ '1' ],
               lorentz = [ L.SSS1 ],
               couplings = {(0,0):C.GC_250})

V_962 = Vertex(name = 'V_962',
               particles = [ P.G__minus__, P.G__plus__, P.Hh ],
               color = [ '1' ],
               lorentz = [ L.SSS1 ],
               couplings = {(0,0):C.GC_249})

V_963 = Vertex(name = 'V_963',
               particles = [ P.G0, P.GQ0, P.Hh ],
               color = [ '1' ],
               lorentz = [ L.SSS1 ],
               couplings = {(0,0):C.GC_250})

V_964 = Vertex(name = 'V_964',
               particles = [ P.G__plus__, P.GQ__minus__, P.Hh ],
               color = [ '1' ],
               lorentz = [ L.SSS1 ],
               couplings = {(0,0):C.GC_249})

V_965 = Vertex(name = 'V_965',
               particles = [ P.G__minus__, P.GQ__plus__, P.Hh ],
               color = [ '1' ],
               lorentz = [ L.SSS1 ],
               couplings = {(0,0):C.GC_249})

V_966 = Vertex(name = 'V_966',
               particles = [ P.GQ__minus__, P.GQ__plus__, P.Hh ],
               color = [ '1' ],
               lorentz = [ L.SSS1 ],
               couplings = {(0,0):C.GC_313})

V_967 = Vertex(name = 'V_967',
               particles = [ P.Hh, P.Hh, P.Hh ],
               color = [ '1' ],
               lorentz = [ L.SSS1 ],
               couplings = {(0,0):C.GC_235})

V_968 = Vertex(name = 'V_968',
               particles = [ P.G0, P.G0, P.HhQ ],
               color = [ '1' ],
               lorentz = [ L.SSS1 ],
               couplings = {(0,0):C.GC_250})

V_969 = Vertex(name = 'V_969',
               particles = [ P.G__minus__, P.G__plus__, P.HhQ ],
               color = [ '1' ],
               lorentz = [ L.SSS1 ],
               couplings = {(0,0):C.GC_249})

V_970 = Vertex(name = 'V_970',
               particles = [ P.GQ0, P.GQ0, P.HhQ ],
               color = [ '1' ],
               lorentz = [ L.SSS1 ],
               couplings = {(0,0):C.GC_250})

V_971 = Vertex(name = 'V_971',
               particles = [ P.G__plus__, P.GQ__minus__, P.HhQ ],
               color = [ '1' ],
               lorentz = [ L.SSS1 ],
               couplings = {(0,0):C.GC_309})

V_972 = Vertex(name = 'V_972',
               particles = [ P.G__minus__, P.GQ__plus__, P.HhQ ],
               color = [ '1' ],
               lorentz = [ L.SSS1 ],
               couplings = {(0,0):C.GC_309})

V_973 = Vertex(name = 'V_973',
               particles = [ P.GQ__minus__, P.GQ__plus__, P.HhQ ],
               color = [ '1' ],
               lorentz = [ L.SSS1 ],
               couplings = {(0,0):C.GC_249})

V_974 = Vertex(name = 'V_974',
               particles = [ P.Hh, P.Hh, P.HhQ ],
               color = [ '1' ],
               lorentz = [ L.SSS1 ],
               couplings = {(0,0):C.GC_235})

V_975 = Vertex(name = 'V_975',
               particles = [ P.Hh, P.HhQ, P.HhQ ],
               color = [ '1' ],
               lorentz = [ L.SSS1 ],
               couplings = {(0,0):C.GC_235})

V_976 = Vertex(name = 'V_976',
               particles = [ P.HhQ, P.HhQ, P.HhQ ],
               color = [ '1' ],
               lorentz = [ L.SSS1 ],
               couplings = {(0,0):C.GC_235})

V_977 = Vertex(name = 'V_977',
               particles = [ P.Ha, P.Ha, P.Hh ],
               color = [ '1' ],
               lorentz = [ L.SSS1 ],
               couplings = {(0,0):C.GC_254})

V_978 = Vertex(name = 'V_978',
               particles = [ P.Ha, P.HaQ, P.Hh ],
               color = [ '1' ],
               lorentz = [ L.SSS1 ],
               couplings = {(0,0):C.GC_254})

V_979 = Vertex(name = 'V_979',
               particles = [ P.HaQ, P.HaQ, P.Hh ],
               color = [ '1' ],
               lorentz = [ L.SSS1 ],
               couplings = {(0,0):C.GC_282})

V_980 = Vertex(name = 'V_980',
               particles = [ P.Ha, P.Ha, P.HhQ ],
               color = [ '1' ],
               lorentz = [ L.SSS1 ],
               couplings = {(0,0):C.GC_254})

V_981 = Vertex(name = 'V_981',
               particles = [ P.Ha, P.HaQ, P.HhQ ],
               color = [ '1' ],
               lorentz = [ L.SSS1 ],
               couplings = {(0,0):C.GC_278})

V_982 = Vertex(name = 'V_982',
               particles = [ P.HaQ, P.HaQ, P.HhQ ],
               color = [ '1' ],
               lorentz = [ L.SSS1 ],
               couplings = {(0,0):C.GC_254})

V_983 = Vertex(name = 'V_983',
               particles = [ P.Hh, P.Hl, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.SSS1 ],
               couplings = {(0,0):C.GC_236})

V_984 = Vertex(name = 'V_984',
               particles = [ P.HhQ, P.Hl, P.Hl ],
               color = [ '1' ],
               lorentz = [ L.SSS1 ],
               couplings = {(0,0):C.GC_236})

V_985 = Vertex(name = 'V_985',
               particles = [ P.Hh, P.Hl, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSS1 ],
               couplings = {(0,0):C.GC_236})

V_986 = Vertex(name = 'V_986',
               particles = [ P.HhQ, P.Hl, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSS1 ],
               couplings = {(0,0):C.GC_236})

V_987 = Vertex(name = 'V_987',
               particles = [ P.Hh, P.HlQ, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSS1 ],
               couplings = {(0,0):C.GC_236})

V_988 = Vertex(name = 'V_988',
               particles = [ P.HhQ, P.HlQ, P.HlQ ],
               color = [ '1' ],
               lorentz = [ L.SSS1 ],
               couplings = {(0,0):C.GC_236})

V_989 = Vertex(name = 'V_989',
               particles = [ P.Hh, P.H__minus__, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSS1 ],
               couplings = {(0,0):C.GC_251})

V_990 = Vertex(name = 'V_990',
               particles = [ P.HhQ, P.H__minus__, P.H__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSS1 ],
               couplings = {(0,0):C.GC_251})

V_991 = Vertex(name = 'V_991',
               particles = [ P.Hh, P.H__plus__, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSS1 ],
               couplings = {(0,0):C.GC_251})

V_992 = Vertex(name = 'V_992',
               particles = [ P.HhQ, P.H__plus__, P.HQ__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSS1 ],
               couplings = {(0,0):C.GC_277})

V_993 = Vertex(name = 'V_993',
               particles = [ P.Hh, P.H__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSS1 ],
               couplings = {(0,0):C.GC_251})

V_994 = Vertex(name = 'V_994',
               particles = [ P.HhQ, P.H__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSS1 ],
               couplings = {(0,0):C.GC_277})

V_995 = Vertex(name = 'V_995',
               particles = [ P.Hh, P.HQ__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSS1 ],
               couplings = {(0,0):C.GC_281})

V_996 = Vertex(name = 'V_996',
               particles = [ P.HhQ, P.HQ__minus__, P.HQ__plus__ ],
               color = [ '1' ],
               lorentz = [ L.SSS1 ],
               couplings = {(0,0):C.GC_251})

V_997 = Vertex(name = 'V_997',
               particles = [ P.G__plus__, P.Ha, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSS1 ],
               couplings = {(0,0):C.GC_258})

V_998 = Vertex(name = 'V_998',
               particles = [ P.GQ__plus__, P.Ha, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSS1 ],
               couplings = {(0,0):C.GC_258})

V_999 = Vertex(name = 'V_999',
               particles = [ P.G__plus__, P.HaQ, P.H__minus__ ],
               color = [ '1' ],
               lorentz = [ L.SSS1 ],
               couplings = {(0,0):C.GC_258})

V_1000 = Vertex(name = 'V_1000',
                particles = [ P.GQ__plus__, P.HaQ, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_320})

V_1001 = Vertex(name = 'V_1001',
                particles = [ P.G__plus__, P.Hl, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_252})

V_1002 = Vertex(name = 'V_1002',
                particles = [ P.GQ__plus__, P.Hl, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_252})

V_1003 = Vertex(name = 'V_1003',
                particles = [ P.G__plus__, P.HlQ, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_252})

V_1004 = Vertex(name = 'V_1004',
                particles = [ P.GQ__plus__, P.HlQ, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_311})

V_1005 = Vertex(name = 'V_1005',
                particles = [ P.G__minus__, P.Ha, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_257})

V_1006 = Vertex(name = 'V_1006',
                particles = [ P.GQ__minus__, P.Ha, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_257})

V_1007 = Vertex(name = 'V_1007',
                particles = [ P.G__minus__, P.HaQ, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_257})

V_1008 = Vertex(name = 'V_1008',
                particles = [ P.GQ__minus__, P.HaQ, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_316})

V_1009 = Vertex(name = 'V_1009',
                particles = [ P.G__minus__, P.Hl, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_252})

V_1010 = Vertex(name = 'V_1010',
                particles = [ P.GQ__minus__, P.Hl, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_252})

V_1011 = Vertex(name = 'V_1011',
                particles = [ P.G__minus__, P.HlQ, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_252})

V_1012 = Vertex(name = 'V_1012',
                particles = [ P.GQ__minus__, P.HlQ, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_311})

V_1013 = Vertex(name = 'V_1013',
                particles = [ P.G__plus__, P.Ha, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_258})

V_1014 = Vertex(name = 'V_1014',
                particles = [ P.GQ__plus__, P.Ha, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_317})

V_1015 = Vertex(name = 'V_1015',
                particles = [ P.G__plus__, P.HaQ, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_258})

V_1016 = Vertex(name = 'V_1016',
                particles = [ P.GQ__plus__, P.HaQ, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_258})

V_1017 = Vertex(name = 'V_1017',
                particles = [ P.G__plus__, P.Hl, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_252})

V_1018 = Vertex(name = 'V_1018',
                particles = [ P.GQ__plus__, P.Hl, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_307})

V_1019 = Vertex(name = 'V_1019',
                particles = [ P.G__plus__, P.HlQ, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_279})

V_1020 = Vertex(name = 'V_1020',
                particles = [ P.GQ__plus__, P.HlQ, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_252})

V_1021 = Vertex(name = 'V_1021',
                particles = [ P.G__minus__, P.Ha, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_257})

V_1022 = Vertex(name = 'V_1022',
                particles = [ P.GQ__minus__, P.Ha, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_319})

V_1023 = Vertex(name = 'V_1023',
                particles = [ P.G__minus__, P.HaQ, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_257})

V_1024 = Vertex(name = 'V_1024',
                particles = [ P.GQ__minus__, P.HaQ, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_257})

V_1025 = Vertex(name = 'V_1025',
                particles = [ P.G__minus__, P.Hl, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_252})

V_1026 = Vertex(name = 'V_1026',
                particles = [ P.GQ__minus__, P.Hl, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_307})

V_1027 = Vertex(name = 'V_1027',
                particles = [ P.G__minus__, P.HlQ, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_279})

V_1028 = Vertex(name = 'V_1028',
                particles = [ P.GQ__minus__, P.HlQ, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_252})

V_1029 = Vertex(name = 'V_1029',
                particles = [ P.G0, P.Ha, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_253})

V_1030 = Vertex(name = 'V_1030',
                particles = [ P.GQ0, P.Ha, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_253})

V_1031 = Vertex(name = 'V_1031',
                particles = [ P.G0, P.HaQ, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_253})

V_1032 = Vertex(name = 'V_1032',
                particles = [ P.G0, P.Ha, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_253})

V_1033 = Vertex(name = 'V_1033',
                particles = [ P.G0, P.HaQ, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_280})

V_1034 = Vertex(name = 'V_1034',
                particles = [ P.GQ0, P.HaQ, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_253})

V_1035 = Vertex(name = 'V_1035',
                particles = [ P.GQ0, P.HaQ, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_298})

V_1036 = Vertex(name = 'V_1036',
                particles = [ P.GQ0, P.Ha, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_294})

V_1037 = Vertex(name = 'V_1037',
                particles = [ P.ghZ, P.ghZ__tilde__, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.UUS1 ],
                couplings = {(0,0):C.GC_328})

V_1038 = Vertex(name = 'V_1038',
                particles = [ P.GQ0, P.GQ0, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_300})

V_1039 = Vertex(name = 'V_1039',
                particles = [ P.ghZ, P.ghZ__tilde__, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.UUS1 ],
                couplings = {(0,0):C.GC_327})

V_1040 = Vertex(name = 'V_1040',
                particles = [ P.G0, P.GQ0, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_296})

V_1041 = Vertex(name = 'V_1041',
                particles = [ P.G0, P.G0, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_242})

V_1042 = Vertex(name = 'V_1042',
                particles = [ P.G__minus__, P.G__plus__, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_241})

V_1043 = Vertex(name = 'V_1043',
                particles = [ P.G0, P.GQ0, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_242})

V_1044 = Vertex(name = 'V_1044',
                particles = [ P.G__plus__, P.GQ__minus__, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_241})

V_1045 = Vertex(name = 'V_1045',
                particles = [ P.G__minus__, P.GQ__plus__, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_241})

V_1046 = Vertex(name = 'V_1046',
                particles = [ P.GQ__minus__, P.GQ__plus__, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_299})

V_1047 = Vertex(name = 'V_1047',
                particles = [ P.Hh, P.Hh, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_233})

V_1048 = Vertex(name = 'V_1048',
                particles = [ P.Hh, P.HhQ, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_233})

V_1049 = Vertex(name = 'V_1049',
                particles = [ P.HhQ, P.HhQ, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_233})

V_1050 = Vertex(name = 'V_1050',
                particles = [ P.G0, P.G0, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_242})

V_1051 = Vertex(name = 'V_1051',
                particles = [ P.G__minus__, P.G__plus__, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_241})

V_1052 = Vertex(name = 'V_1052',
                particles = [ P.GQ0, P.GQ0, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_242})

V_1053 = Vertex(name = 'V_1053',
                particles = [ P.G__plus__, P.GQ__minus__, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_295})

V_1054 = Vertex(name = 'V_1054',
                particles = [ P.G__minus__, P.GQ__plus__, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_295})

V_1055 = Vertex(name = 'V_1055',
                particles = [ P.GQ__minus__, P.GQ__plus__, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_241})

V_1056 = Vertex(name = 'V_1056',
                particles = [ P.Hh, P.Hh, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_233})

V_1057 = Vertex(name = 'V_1057',
                particles = [ P.Hh, P.HhQ, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_233})

V_1058 = Vertex(name = 'V_1058',
                particles = [ P.HhQ, P.HhQ, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_233})

V_1059 = Vertex(name = 'V_1059',
                particles = [ P.Ha, P.Ha, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_246})

V_1060 = Vertex(name = 'V_1060',
                particles = [ P.Ha, P.HaQ, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_246})

V_1061 = Vertex(name = 'V_1061',
                particles = [ P.HaQ, P.HaQ, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_292})

V_1062 = Vertex(name = 'V_1062',
                particles = [ P.Hl, P.Hl, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_234})

V_1063 = Vertex(name = 'V_1063',
                particles = [ P.Ha, P.Ha, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_246})

V_1064 = Vertex(name = 'V_1064',
                particles = [ P.Ha, P.HaQ, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_290})

V_1065 = Vertex(name = 'V_1065',
                particles = [ P.HaQ, P.HaQ, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_246})

V_1066 = Vertex(name = 'V_1066',
                particles = [ P.Hl, P.Hl, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_234})

V_1067 = Vertex(name = 'V_1067',
                particles = [ P.Hl, P.HlQ, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_234})

V_1068 = Vertex(name = 'V_1068',
                particles = [ P.HlQ, P.HlQ, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_234})

V_1069 = Vertex(name = 'V_1069',
                particles = [ P.Hl, P.H__minus__, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_243})

V_1070 = Vertex(name = 'V_1070',
                particles = [ P.HlQ, P.H__minus__, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_243})

V_1071 = Vertex(name = 'V_1071',
                particles = [ P.Hl, P.H__plus__, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_243})

V_1072 = Vertex(name = 'V_1072',
                particles = [ P.HlQ, P.H__plus__, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_287})

V_1073 = Vertex(name = 'V_1073',
                particles = [ P.Hl, P.H__minus__, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_243})

V_1074 = Vertex(name = 'V_1074',
                particles = [ P.HlQ, P.H__minus__, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_287})

V_1075 = Vertex(name = 'V_1075',
                particles = [ P.Hl, P.HQ__minus__, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_291})

V_1076 = Vertex(name = 'V_1076',
                particles = [ P.HlQ, P.HQ__minus__, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_243})

V_1077 = Vertex(name = 'V_1077',
                particles = [ P.G__plus__, P.Hh, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_244})

V_1078 = Vertex(name = 'V_1078',
                particles = [ P.GQ__plus__, P.Hh, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_244})

V_1079 = Vertex(name = 'V_1079',
                particles = [ P.G__plus__, P.HhQ, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_244})

V_1080 = Vertex(name = 'V_1080',
                particles = [ P.GQ__plus__, P.HhQ, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_293})

V_1081 = Vertex(name = 'V_1081',
                particles = [ P.G__minus__, P.Hh, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_244})

V_1082 = Vertex(name = 'V_1082',
                particles = [ P.GQ__minus__, P.Hh, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_244})

V_1083 = Vertex(name = 'V_1083',
                particles = [ P.G__minus__, P.HhQ, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_244})

V_1084 = Vertex(name = 'V_1084',
                particles = [ P.GQ__minus__, P.HhQ, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_293})

V_1085 = Vertex(name = 'V_1085',
                particles = [ P.G__plus__, P.Hh, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_244})

V_1086 = Vertex(name = 'V_1086',
                particles = [ P.GQ__plus__, P.Hh, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_297})

V_1087 = Vertex(name = 'V_1087',
                particles = [ P.G__plus__, P.HhQ, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_288})

V_1088 = Vertex(name = 'V_1088',
                particles = [ P.GQ__plus__, P.HhQ, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_244})

V_1089 = Vertex(name = 'V_1089',
                particles = [ P.G__minus__, P.Hh, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_244})

V_1090 = Vertex(name = 'V_1090',
                particles = [ P.GQ__minus__, P.Hh, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_297})

V_1091 = Vertex(name = 'V_1091',
                particles = [ P.G__minus__, P.HhQ, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_288})

V_1092 = Vertex(name = 'V_1092',
                particles = [ P.GQ__minus__, P.HhQ, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_244})

V_1093 = Vertex(name = 'V_1093',
                particles = [ P.G0, P.Ha, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_245})

V_1094 = Vertex(name = 'V_1094',
                particles = [ P.GQ0, P.Ha, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_245})

V_1095 = Vertex(name = 'V_1095',
                particles = [ P.G0, P.HaQ, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_245})

V_1096 = Vertex(name = 'V_1096',
                particles = [ P.G0, P.Ha, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_245})

V_1097 = Vertex(name = 'V_1097',
                particles = [ P.G0, P.HaQ, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_289})

V_1098 = Vertex(name = 'V_1098',
                particles = [ P.GQ0, P.HaQ, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_245})

V_1099 = Vertex(name = 'V_1099',
                particles = [ P.G__plus__, P.GQ__minus__, P.HaQ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_304})

V_1100 = Vertex(name = 'V_1100',
                particles = [ P.G__minus__, P.GQ__plus__, P.HaQ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_301})

V_1101 = Vertex(name = 'V_1101',
                particles = [ P.ghWp, P.ghZ__tilde__, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.UUS1 ],
                couplings = {(0,0):C.GC_232})

V_1102 = Vertex(name = 'V_1102',
                particles = [ P.ghZ, P.ghWm__tilde__, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.UUS1 ],
                couplings = {(0,0):C.GC_232})

V_1103 = Vertex(name = 'V_1103',
                particles = [ P.ghWm, P.ghZ__tilde__, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.UUS1 ],
                couplings = {(0,0):C.GC_232})

V_1104 = Vertex(name = 'V_1104',
                particles = [ P.ghZ, P.ghWp__tilde__, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.UUS1 ],
                couplings = {(0,0):C.GC_232})

V_1105 = Vertex(name = 'V_1105',
                particles = [ P.ghWp, P.ghZ__tilde__, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.UUS1 ],
                couplings = {(0,0):C.GC_266})

V_1106 = Vertex(name = 'V_1106',
                particles = [ P.ghZ, P.ghWm__tilde__, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.UUS1 ],
                couplings = {(0,0):C.GC_267})

V_1107 = Vertex(name = 'V_1107',
                particles = [ P.HaQ, P.H__plus__, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_239})

V_1108 = Vertex(name = 'V_1108',
                particles = [ P.ghWm, P.ghZ__tilde__, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.UUS1 ],
                couplings = {(0,0):C.GC_266})

V_1109 = Vertex(name = 'V_1109',
                particles = [ P.ghZ, P.ghWp__tilde__, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.UUS1 ],
                couplings = {(0,0):C.GC_267})

V_1110 = Vertex(name = 'V_1110',
                particles = [ P.HaQ, P.H__minus__, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_240})

V_1111 = Vertex(name = 'V_1111',
                particles = [ P.G0, P.G__plus__, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_247})

V_1112 = Vertex(name = 'V_1112',
                particles = [ P.G__plus__, P.GQ0, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_247})

V_1113 = Vertex(name = 'V_1113',
                particles = [ P.G0, P.GQ__plus__, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_247})

V_1114 = Vertex(name = 'V_1114',
                particles = [ P.GQ0, P.GQ__plus__, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_247})

V_1115 = Vertex(name = 'V_1115',
                particles = [ P.G0, P.G__minus__, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_248})

V_1116 = Vertex(name = 'V_1116',
                particles = [ P.G__minus__, P.GQ0, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_248})

V_1117 = Vertex(name = 'V_1117',
                particles = [ P.G0, P.GQ__minus__, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_248})

V_1118 = Vertex(name = 'V_1118',
                particles = [ P.GQ0, P.GQ__minus__, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_248})

V_1119 = Vertex(name = 'V_1119',
                particles = [ P.G0, P.G__plus__, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_247})

V_1120 = Vertex(name = 'V_1120',
                particles = [ P.G__plus__, P.GQ0, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_302})

V_1121 = Vertex(name = 'V_1121',
                particles = [ P.G0, P.GQ__plus__, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_305})

V_1122 = Vertex(name = 'V_1122',
                particles = [ P.GQ0, P.GQ__plus__, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_247})

V_1123 = Vertex(name = 'V_1123',
                particles = [ P.G0, P.G__minus__, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_248})

V_1124 = Vertex(name = 'V_1124',
                particles = [ P.G__minus__, P.GQ0, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_306})

V_1125 = Vertex(name = 'V_1125',
                particles = [ P.G0, P.GQ__minus__, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_303})

V_1126 = Vertex(name = 'V_1126',
                particles = [ P.GQ0, P.GQ__minus__, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.SSS1 ],
                couplings = {(0,0):C.GC_248})

V_1127 = Vertex(name = 'V_1127',
                particles = [ P.ghWm, P.ghWm__tilde__, P.GQ0 ],
                color = [ '1' ],
                lorentz = [ L.UUS1 ],
                couplings = {(0,0):C.GC_274})

V_1128 = Vertex(name = 'V_1128',
                particles = [ P.ghWp, P.ghWp__tilde__, P.GQ0 ],
                color = [ '1' ],
                lorentz = [ L.UUS1 ],
                couplings = {(0,0):C.GC_275})

V_1129 = Vertex(name = 'V_1129',
                particles = [ P.ghWm, P.ghWm__tilde__, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.UUS1 ],
                couplings = {(0,0):C.GC_271})

V_1130 = Vertex(name = 'V_1130',
                particles = [ P.ghWp, P.ghWp__tilde__, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.UUS1 ],
                couplings = {(0,0):C.GC_271})

V_1131 = Vertex(name = 'V_1131',
                particles = [ P.ghWm, P.ghWm__tilde__, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.UUS1 ],
                couplings = {(0,0):C.GC_270})

V_1132 = Vertex(name = 'V_1132',
                particles = [ P.ghWp, P.ghWp__tilde__, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.UUS1 ],
                couplings = {(0,0):C.GC_270})

V_1133 = Vertex(name = 'V_1133',
                particles = [ P.ghWm, P.ghWm__tilde__, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.UUS1 ],
                couplings = {(0,0):C.GC_260})

V_1134 = Vertex(name = 'V_1134',
                particles = [ P.ghWp, P.ghWp__tilde__, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.UUS1 ],
                couplings = {(0,0):C.GC_260})

V_1135 = Vertex(name = 'V_1135',
                particles = [ P.ghWm, P.ghWm__tilde__, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.UUS1 ],
                couplings = {(0,0):C.GC_259})

V_1136 = Vertex(name = 'V_1136',
                particles = [ P.ghWp, P.ghWp__tilde__, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.UUS1 ],
                couplings = {(0,0):C.GC_259})

V_1137 = Vertex(name = 'V_1137',
                particles = [ P.ghWm, P.ghWm__tilde__, P.HaQ ],
                color = [ '1' ],
                lorentz = [ L.UUS1 ],
                couplings = {(0,0):C.GC_263})

V_1138 = Vertex(name = 'V_1138',
                particles = [ P.ghWp, P.ghWp__tilde__, P.HaQ ],
                color = [ '1' ],
                lorentz = [ L.UUS1 ],
                couplings = {(0,0):C.GC_264})

V_1139 = Vertex(name = 'V_1139',
                particles = [ P.ghA, P.ghWm__tilde__, P.G__minus__ ],
                color = [ '1' ],
                lorentz = [ L.UUS1 ],
                couplings = {(0,0):C.GC_325})

V_1140 = Vertex(name = 'V_1140',
                particles = [ P.ghWp, P.ghA__tilde__, P.G__minus__ ],
                color = [ '1' ],
                lorentz = [ L.UUS1 ],
                couplings = {(0,0):C.GC_325})

V_1141 = Vertex(name = 'V_1141',
                particles = [ P.ghWm, P.ghA__tilde__, P.G__plus__ ],
                color = [ '1' ],
                lorentz = [ L.UUS1 ],
                couplings = {(0,0):C.GC_325})

V_1142 = Vertex(name = 'V_1142',
                particles = [ P.ghA, P.ghWp__tilde__, P.G__plus__ ],
                color = [ '1' ],
                lorentz = [ L.UUS1 ],
                couplings = {(0,0):C.GC_325})

V_1143 = Vertex(name = 'V_1143',
                particles = [ P.ghWp, P.ghA__tilde__, P.GQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.UUS1 ],
                couplings = {(0,0):C.GC_325})

V_1144 = Vertex(name = 'V_1144',
                particles = [ P.ghWm, P.ghA__tilde__, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.UUS1 ],
                couplings = {(0,0):C.GC_325})

V_1145 = Vertex(name = 'V_1145',
                particles = [ P.ghA, P.ghWm__tilde__, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.UUS1 ],
                couplings = {(0,0):C.GC_322})

V_1146 = Vertex(name = 'V_1146',
                particles = [ P.ghWp, P.ghA__tilde__, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.UUS1 ],
                couplings = {(0,0):C.GC_322})

V_1147 = Vertex(name = 'V_1147',
                particles = [ P.ghWm, P.ghA__tilde__, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.UUS1 ],
                couplings = {(0,0):C.GC_322})

V_1148 = Vertex(name = 'V_1148',
                particles = [ P.ghA, P.ghWp__tilde__, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.UUS1 ],
                couplings = {(0,0):C.GC_322})

V_1149 = Vertex(name = 'V_1149',
                particles = [ P.ghWp, P.ghA__tilde__, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.UUS1 ],
                couplings = {(0,0):C.GC_322})

V_1150 = Vertex(name = 'V_1150',
                particles = [ P.ghWm, P.ghA__tilde__, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.UUS1 ],
                couplings = {(0,0):C.GC_322})

V_1151 = Vertex(name = 'V_1151',
                particles = [ P.ghWm, P.ghWm__tilde__, P.A, P.A ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_10})

V_1152 = Vertex(name = 'V_1152',
                particles = [ P.ghWp, P.ghWp__tilde__, P.A, P.A ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_10})

V_1153 = Vertex(name = 'V_1153',
                particles = [ P.A, P.A, P.G__minus__, P.G__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_29})

V_1154 = Vertex(name = 'V_1154',
                particles = [ P.A, P.A, P.G__plus__, P.GQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_29})

V_1155 = Vertex(name = 'V_1155',
                particles = [ P.A, P.A, P.G__minus__, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_29})

V_1156 = Vertex(name = 'V_1156',
                particles = [ P.A, P.A, P.GQ__minus__, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_29})

V_1157 = Vertex(name = 'V_1157',
                particles = [ P.A, P.A, P.H__minus__, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_29})

V_1158 = Vertex(name = 'V_1158',
                particles = [ P.A, P.A, P.H__plus__, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_29})

V_1159 = Vertex(name = 'V_1159',
                particles = [ P.A, P.A, P.H__minus__, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_29})

V_1160 = Vertex(name = 'V_1160',
                particles = [ P.A, P.A, P.HQ__minus__, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_29})

V_1161 = Vertex(name = 'V_1161',
                particles = [ P.ghWm, P.ghWm__tilde__, P.A, P.AQ ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_8})

V_1162 = Vertex(name = 'V_1162',
                particles = [ P.ghWp, P.ghWp__tilde__, P.A, P.AQ ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_8})

V_1163 = Vertex(name = 'V_1163',
                particles = [ P.A, P.AQ, P.G__minus__, P.G__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_29})

V_1164 = Vertex(name = 'V_1164',
                particles = [ P.A, P.AQ, P.G__plus__, P.GQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_29})

V_1165 = Vertex(name = 'V_1165',
                particles = [ P.A, P.AQ, P.G__minus__, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_29})

V_1166 = Vertex(name = 'V_1166',
                particles = [ P.A, P.AQ, P.GQ__minus__, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_29})

V_1167 = Vertex(name = 'V_1167',
                particles = [ P.A, P.AQ, P.H__minus__, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_29})

V_1168 = Vertex(name = 'V_1168',
                particles = [ P.A, P.AQ, P.H__plus__, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_29})

V_1169 = Vertex(name = 'V_1169',
                particles = [ P.A, P.AQ, P.H__minus__, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_29})

V_1170 = Vertex(name = 'V_1170',
                particles = [ P.A, P.AQ, P.HQ__minus__, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_29})

V_1171 = Vertex(name = 'V_1171',
                particles = [ P.AQ, P.AQ, P.G__minus__, P.G__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_29})

V_1172 = Vertex(name = 'V_1172',
                particles = [ P.AQ, P.AQ, P.G__plus__, P.GQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_29})

V_1173 = Vertex(name = 'V_1173',
                particles = [ P.AQ, P.AQ, P.G__minus__, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_29})

V_1174 = Vertex(name = 'V_1174',
                particles = [ P.AQ, P.AQ, P.GQ__minus__, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_29})

V_1175 = Vertex(name = 'V_1175',
                particles = [ P.AQ, P.AQ, P.H__minus__, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_29})

V_1176 = Vertex(name = 'V_1176',
                particles = [ P.AQ, P.AQ, P.H__plus__, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_29})

V_1177 = Vertex(name = 'V_1177',
                particles = [ P.AQ, P.AQ, P.H__minus__, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_29})

V_1178 = Vertex(name = 'V_1178',
                particles = [ P.AQ, P.AQ, P.HQ__minus__, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_29})

V_1179 = Vertex(name = 'V_1179',
                particles = [ P.ghWm, P.ghWm__tilde__, P.A ],
                color = [ '1' ],
                lorentz = [ L.UUV1 ],
                couplings = {(0,0):C.GC_5})

V_1180 = Vertex(name = 'V_1180',
                particles = [ P.ghWm, P.ghWm__tilde__, P.AQ ],
                color = [ '1' ],
                lorentz = [ L.UUV2 ],
                couplings = {(0,0):C.GC_3})

V_1181 = Vertex(name = 'V_1181',
                particles = [ P.ghWp, P.ghWp__tilde__, P.A ],
                color = [ '1' ],
                lorentz = [ L.UUV1 ],
                couplings = {(0,0):C.GC_6})

V_1182 = Vertex(name = 'V_1182',
                particles = [ P.ghWp, P.ghWp__tilde__, P.AQ ],
                color = [ '1' ],
                lorentz = [ L.UUV2 ],
                couplings = {(0,0):C.GC_4})

V_1183 = Vertex(name = 'V_1183',
                particles = [ P.A, P.G__minus__, P.G__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_26})

V_1184 = Vertex(name = 'V_1184',
                particles = [ P.A, P.G__minus__, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_26})

V_1185 = Vertex(name = 'V_1185',
                particles = [ P.AQ, P.G__minus__, P.G__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_26})

V_1186 = Vertex(name = 'V_1186',
                particles = [ P.AQ, P.G__minus__, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS5 ],
                couplings = {(0,0):C.GC_25})

V_1187 = Vertex(name = 'V_1187',
                particles = [ P.A, P.G__plus__, P.GQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_25})

V_1188 = Vertex(name = 'V_1188',
                particles = [ P.AQ, P.G__plus__, P.GQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS5 ],
                couplings = {(0,0):C.GC_26})

V_1189 = Vertex(name = 'V_1189',
                particles = [ P.A, P.GQ__minus__, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_26})

V_1190 = Vertex(name = 'V_1190',
                particles = [ P.AQ, P.GQ__minus__, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_26})

V_1191 = Vertex(name = 'V_1191',
                particles = [ P.A, P.H__minus__, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_26})

V_1192 = Vertex(name = 'V_1192',
                particles = [ P.A, P.H__minus__, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_26})

V_1193 = Vertex(name = 'V_1193',
                particles = [ P.AQ, P.H__minus__, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_26})

V_1194 = Vertex(name = 'V_1194',
                particles = [ P.AQ, P.H__minus__, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS5 ],
                couplings = {(0,0):C.GC_25})

V_1195 = Vertex(name = 'V_1195',
                particles = [ P.A, P.H__plus__, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_25})

V_1196 = Vertex(name = 'V_1196',
                particles = [ P.AQ, P.H__plus__, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS5 ],
                couplings = {(0,0):C.GC_26})

V_1197 = Vertex(name = 'V_1197',
                particles = [ P.A, P.HQ__minus__, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_26})

V_1198 = Vertex(name = 'V_1198',
                particles = [ P.AQ, P.HQ__minus__, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_26})

V_1199 = Vertex(name = 'V_1199',
                particles = [ P.ghA, P.ghWm__tilde__, P.W__minus__ ],
                color = [ '1' ],
                lorentz = [ L.UUV1 ],
                couplings = {(0,0):C.GC_6})

V_1200 = Vertex(name = 'V_1200',
                particles = [ P.ghWp, P.ghA__tilde__, P.W__minus__ ],
                color = [ '1' ],
                lorentz = [ L.UUV1 ],
                couplings = {(0,0):C.GC_5})

V_1201 = Vertex(name = 'V_1201',
                particles = [ P.ghWp, P.ghZ__tilde__, P.W__minus__ ],
                color = [ '1' ],
                lorentz = [ L.UUV1 ],
                couplings = {(0,0):C.GC_201})

V_1202 = Vertex(name = 'V_1202',
                particles = [ P.ghZ, P.ghWm__tilde__, P.W__minus__ ],
                color = [ '1' ],
                lorentz = [ L.UUV1 ],
                couplings = {(0,0):C.GC_200})

V_1203 = Vertex(name = 'V_1203',
                particles = [ P.ghWm, P.ghA__tilde__, P.W__plus__ ],
                color = [ '1' ],
                lorentz = [ L.UUV1 ],
                couplings = {(0,0):C.GC_6})

V_1204 = Vertex(name = 'V_1204',
                particles = [ P.ghA, P.ghWp__tilde__, P.W__plus__ ],
                color = [ '1' ],
                lorentz = [ L.UUV1 ],
                couplings = {(0,0):C.GC_5})

V_1205 = Vertex(name = 'V_1205',
                particles = [ P.ghWm, P.ghZ__tilde__, P.W__plus__ ],
                color = [ '1' ],
                lorentz = [ L.UUV1 ],
                couplings = {(0,0):C.GC_200})

V_1206 = Vertex(name = 'V_1206',
                particles = [ P.ghZ, P.ghWp__tilde__, P.W__plus__ ],
                color = [ '1' ],
                lorentz = [ L.UUV1 ],
                couplings = {(0,0):C.GC_201})

V_1207 = Vertex(name = 'V_1207',
                particles = [ P.ghA, P.ghWm__tilde__, P.WQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.UUV2 ],
                couplings = {(0,0):C.GC_4})

V_1208 = Vertex(name = 'V_1208',
                particles = [ P.ghWp, P.ghA__tilde__, P.WQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.UUV2 ],
                couplings = {(0,0):C.GC_3})

V_1209 = Vertex(name = 'V_1209',
                particles = [ P.ghWp, P.ghZ__tilde__, P.WQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.UUV2 ],
                couplings = {(0,0):C.GC_199})

V_1210 = Vertex(name = 'V_1210',
                particles = [ P.ghZ, P.ghWm__tilde__, P.WQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.UUV2 ],
                couplings = {(0,0):C.GC_198})

V_1211 = Vertex(name = 'V_1211',
                particles = [ P.WQ__minus__, P.G__plus__, P.GQ0 ],
                color = [ '1' ],
                lorentz = [ L.VSS5 ],
                couplings = {(0,0):C.GC_184})

V_1212 = Vertex(name = 'V_1212',
                particles = [ P.WQ__minus__, P.G0, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS5 ],
                couplings = {(0,0):C.GC_183})

V_1213 = Vertex(name = 'V_1213',
                particles = [ P.WQ__minus__, P.GQ__plus__, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VSS3 ],
                couplings = {(0,0):C.GC_179})

V_1214 = Vertex(name = 'V_1214',
                particles = [ P.WQ__minus__, P.G__plus__, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VSS5 ],
                couplings = {(0,0):C.GC_178})

V_1215 = Vertex(name = 'V_1215',
                particles = [ P.WQ__minus__, P.HaQ, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS3 ],
                couplings = {(0,0):C.GC_184})

V_1216 = Vertex(name = 'V_1216',
                particles = [ P.WQ__minus__, P.HlQ, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS3 ],
                couplings = {(0,0):C.GC_178})

V_1217 = Vertex(name = 'V_1217',
                particles = [ P.WQ__minus__, P.Ha, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS5 ],
                couplings = {(0,0):C.GC_183})

V_1218 = Vertex(name = 'V_1218',
                particles = [ P.WQ__minus__, P.Hl, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS5 ],
                couplings = {(0,0):C.GC_179})

V_1219 = Vertex(name = 'V_1219',
                particles = [ P.WQ__minus__, P.GQ__plus__, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VSS3 ],
                couplings = {(0,0):C.GC_173})

V_1220 = Vertex(name = 'V_1220',
                particles = [ P.WQ__minus__, P.G__plus__, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VSS5 ],
                couplings = {(0,0):C.GC_172})

V_1221 = Vertex(name = 'V_1221',
                particles = [ P.WQ__minus__, P.HhQ, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS3 ],
                couplings = {(0,0):C.GC_173})

V_1222 = Vertex(name = 'V_1222',
                particles = [ P.WQ__minus__, P.Hh, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS5 ],
                couplings = {(0,0):C.GC_172})

V_1223 = Vertex(name = 'V_1223',
                particles = [ P.ghWm, P.ghA__tilde__, P.WQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.UUV2 ],
                couplings = {(0,0):C.GC_4})

V_1224 = Vertex(name = 'V_1224',
                particles = [ P.ghA, P.ghWp__tilde__, P.WQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.UUV2 ],
                couplings = {(0,0):C.GC_3})

V_1225 = Vertex(name = 'V_1225',
                particles = [ P.ghWm, P.ghZ__tilde__, P.WQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.UUV2 ],
                couplings = {(0,0):C.GC_198})

V_1226 = Vertex(name = 'V_1226',
                particles = [ P.ghZ, P.ghWp__tilde__, P.WQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.UUV2 ],
                couplings = {(0,0):C.GC_199})

V_1227 = Vertex(name = 'V_1227',
                particles = [ P.WQ__plus__, P.G__minus__, P.GQ0 ],
                color = [ '1' ],
                lorentz = [ L.VSS5 ],
                couplings = {(0,0):C.GC_184})

V_1228 = Vertex(name = 'V_1228',
                particles = [ P.WQ__plus__, P.G0, P.GQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS5 ],
                couplings = {(0,0):C.GC_183})

V_1229 = Vertex(name = 'V_1229',
                particles = [ P.WQ__plus__, P.GQ__minus__, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VSS3 ],
                couplings = {(0,0):C.GC_178})

V_1230 = Vertex(name = 'V_1230',
                particles = [ P.WQ__plus__, P.G__minus__, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VSS5 ],
                couplings = {(0,0):C.GC_179})

V_1231 = Vertex(name = 'V_1231',
                particles = [ P.WQ__plus__, P.HaQ, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS3 ],
                couplings = {(0,0):C.GC_184})

V_1232 = Vertex(name = 'V_1232',
                particles = [ P.WQ__plus__, P.HlQ, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS3 ],
                couplings = {(0,0):C.GC_179})

V_1233 = Vertex(name = 'V_1233',
                particles = [ P.WQ__plus__, P.Ha, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS5 ],
                couplings = {(0,0):C.GC_183})

V_1234 = Vertex(name = 'V_1234',
                particles = [ P.WQ__plus__, P.Hl, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS5 ],
                couplings = {(0,0):C.GC_178})

V_1235 = Vertex(name = 'V_1235',
                particles = [ P.WQ__plus__, P.GQ__minus__, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VSS3 ],
                couplings = {(0,0):C.GC_172})

V_1236 = Vertex(name = 'V_1236',
                particles = [ P.WQ__plus__, P.G__minus__, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VSS5 ],
                couplings = {(0,0):C.GC_173})

V_1237 = Vertex(name = 'V_1237',
                particles = [ P.WQ__plus__, P.HhQ, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS3 ],
                couplings = {(0,0):C.GC_172})

V_1238 = Vertex(name = 'V_1238',
                particles = [ P.WQ__plus__, P.Hh, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS5 ],
                couplings = {(0,0):C.GC_173})

V_1239 = Vertex(name = 'V_1239',
                particles = [ P.ghWm, P.ghWm__tilde__, P.Z ],
                color = [ '1' ],
                lorentz = [ L.UUV1 ],
                couplings = {(0,0):C.GC_201})

V_1240 = Vertex(name = 'V_1240',
                particles = [ P.ghWp, P.ghWp__tilde__, P.Z ],
                color = [ '1' ],
                lorentz = [ L.UUV1 ],
                couplings = {(0,0):C.GC_200})

V_1241 = Vertex(name = 'V_1241',
                particles = [ P.ghWm, P.ghWm__tilde__, P.ZQ ],
                color = [ '1' ],
                lorentz = [ L.UUV2 ],
                couplings = {(0,0):C.GC_199})

V_1242 = Vertex(name = 'V_1242',
                particles = [ P.ghWp, P.ghWp__tilde__, P.ZQ ],
                color = [ '1' ],
                lorentz = [ L.UUV2 ],
                couplings = {(0,0):C.GC_198})

V_1243 = Vertex(name = 'V_1243',
                particles = [ P.ZQ, P.G__plus__, P.GQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS5 ],
                couplings = {(0,0):C.GC_218})

V_1244 = Vertex(name = 'V_1244',
                particles = [ P.ZQ, P.G__minus__, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS5 ],
                couplings = {(0,0):C.GC_217})

V_1245 = Vertex(name = 'V_1245',
                particles = [ P.ZQ, P.GQ0, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VSS3 ],
                couplings = {(0,0):C.GC_215})

V_1246 = Vertex(name = 'V_1246',
                particles = [ P.ZQ, P.G0, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VSS5 ],
                couplings = {(0,0):C.GC_216})

V_1247 = Vertex(name = 'V_1247',
                particles = [ P.ZQ, P.HaQ, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VSS3 ],
                couplings = {(0,0):C.GC_215})

V_1248 = Vertex(name = 'V_1248',
                particles = [ P.ZQ, P.Ha, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VSS5 ],
                couplings = {(0,0):C.GC_216})

V_1249 = Vertex(name = 'V_1249',
                particles = [ P.ZQ, P.H__plus__, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS5 ],
                couplings = {(0,0):C.GC_218})

V_1250 = Vertex(name = 'V_1250',
                particles = [ P.ZQ, P.H__minus__, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS5 ],
                couplings = {(0,0):C.GC_217})

V_1251 = Vertex(name = 'V_1251',
                particles = [ P.ZQ, P.HaQ, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VSS3 ],
                couplings = {(0,0):C.GC_214})

V_1252 = Vertex(name = 'V_1252',
                particles = [ P.ZQ, P.Ha, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VSS5 ],
                couplings = {(0,0):C.GC_213})

V_1253 = Vertex(name = 'V_1253',
                particles = [ P.ZQ, P.GQ0, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VSS3 ],
                couplings = {(0,0):C.GC_213})

V_1254 = Vertex(name = 'V_1254',
                particles = [ P.ZQ, P.G0, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VSS5 ],
                couplings = {(0,0):C.GC_214})

V_1255 = Vertex(name = 'V_1255',
                particles = [ P.g, P.g, P.g ],
                color = [ 'f(1,2,3)' ],
                lorentz = [ L.VVV1 ],
                couplings = {(0,0):C.GC_12})

V_1256 = Vertex(name = 'V_1256',
                particles = [ P.g, P.g, P.gQ ],
                color = [ 'f(1,2,3)' ],
                lorentz = [ L.VVV1 ],
                couplings = {(0,0):C.GC_12})

V_1257 = Vertex(name = 'V_1257',
                particles = [ P.g, P.g, P.g, P.g ],
                color = [ 'f(-1,1,2)*f(3,4,-1)', 'f(-1,1,3)*f(2,4,-1)', 'f(-1,1,4)*f(2,3,-1)' ],
                lorentz = [ L.VVVV10, L.VVVV3, L.VVVV6 ],
                couplings = {(1,2):C.GC_15,(0,1):C.GC_15,(2,0):C.GC_15})

V_1258 = Vertex(name = 'V_1258',
                particles = [ P.ghG, P.ghG__tilde__, P.g ],
                color = [ 'f(1,2,3)' ],
                lorentz = [ L.UUV1 ],
                couplings = {(0,0):C.GC_11})

V_1259 = Vertex(name = 'V_1259',
                particles = [ P.ghG, P.ghG__tilde__, P.gQ ],
                color = [ 'f(1,2,3)' ],
                lorentz = [ L.UUV2 ],
                couplings = {(0,0):C.GC_12})

V_1260 = Vertex(name = 'V_1260',
                particles = [ P.ghG, P.ghG__tilde__, P.g, P.g ],
                color = [ 'f(-1,1,3)*f(2,4,-1)', 'f(-1,1,4)*f(2,3,-1)' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(1,0):C.GC_15,(0,0):C.GC_15})

V_1261 = Vertex(name = 'V_1261',
                particles = [ P.g, P.gQ, P.gQ ],
                color = [ 'f(1,2,3)' ],
                lorentz = [ L.VVV2 ],
                couplings = {(0,0):C.GC_12})

V_1262 = Vertex(name = 'V_1262',
                particles = [ P.g, P.g, P.g, P.gQ ],
                color = [ 'f(-1,1,2)*f(3,4,-1)', 'f(-1,1,3)*f(2,4,-1)', 'f(-1,1,4)*f(2,3,-1)' ],
                lorentz = [ L.VVVV10, L.VVVV3, L.VVVV6 ],
                couplings = {(1,2):C.GC_15,(0,1):C.GC_15,(2,0):C.GC_15})

V_1263 = Vertex(name = 'V_1263',
                particles = [ P.g, P.g, P.gQ, P.gQ ],
                color = [ 'f(-1,1,2)*f(3,4,-1)', 'f(-1,1,3)*f(2,4,-1)', 'f(-1,1,4)*f(2,3,-1)' ],
                lorentz = [ L.VVVV13, L.VVVV3, L.VVVV7 ],
                couplings = {(2,0):C.GC_14,(1,2):C.GC_15,(0,1):C.GC_15})

V_1264 = Vertex(name = 'V_1264',
                particles = [ P.ghG, P.ghG__tilde__, P.g, P.gQ ],
                color = [ 'f(-1,1,3)*f(2,4,-1)' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_15})

V_1265 = Vertex(name = 'V_1265',
                particles = [ P.gQ, P.gQ, P.gQ ],
                color = [ 'f(1,2,3)' ],
                lorentz = [ L.VVV1 ],
                couplings = {(0,0):C.GC_12})

V_1266 = Vertex(name = 'V_1266',
                particles = [ P.g, P.gQ, P.gQ, P.gQ ],
                color = [ 'f(-1,1,2)*f(3,4,-1)', 'f(-1,1,3)*f(2,4,-1)', 'f(-1,1,4)*f(2,3,-1)' ],
                lorentz = [ L.VVVV10, L.VVVV3, L.VVVV6 ],
                couplings = {(1,2):C.GC_15,(0,1):C.GC_15,(2,0):C.GC_15})

V_1267 = Vertex(name = 'V_1267',
                particles = [ P.gQ, P.gQ, P.gQ, P.gQ ],
                color = [ 'f(-1,1,2)*f(3,4,-1)', 'f(-1,1,3)*f(2,4,-1)', 'f(-1,1,4)*f(2,3,-1)' ],
                lorentz = [ L.VVVV10, L.VVVV3, L.VVVV6 ],
                couplings = {(1,2):C.GC_15,(0,1):C.GC_15,(2,0):C.GC_15})

V_1268 = Vertex(name = 'V_1268',
                particles = [ P.u__tilde__, P.d, P.G__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS1, L.FFS3 ],
                couplings = {(0,0):C.GC_349,(0,1):C.GC_385})

V_1269 = Vertex(name = 'V_1269',
                particles = [ P.c__tilde__, P.s, P.G__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS1, L.FFS3 ],
                couplings = {(0,0):C.GC_367,(0,1):C.GC_337})

V_1270 = Vertex(name = 'V_1270',
                particles = [ P.t__tilde__, P.b, P.G__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS1, L.FFS3 ],
                couplings = {(0,0):C.GC_340,(0,1):C.GC_373})

V_1271 = Vertex(name = 'V_1271',
                particles = [ P.u__tilde__, P.d, P.GQ__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS1, L.FFS3 ],
                couplings = {(0,0):C.GC_349,(0,1):C.GC_385})

V_1272 = Vertex(name = 'V_1272',
                particles = [ P.c__tilde__, P.s, P.GQ__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS1, L.FFS3 ],
                couplings = {(0,0):C.GC_367,(0,1):C.GC_337})

V_1273 = Vertex(name = 'V_1273',
                particles = [ P.t__tilde__, P.b, P.GQ__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS1, L.FFS3 ],
                couplings = {(0,0):C.GC_340,(0,1):C.GC_373})

V_1274 = Vertex(name = 'V_1274',
                particles = [ P.u__tilde__, P.d, P.H__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS1, L.FFS3 ],
                couplings = {(0,0):C.GC_346,(0,1):C.GC_382})

V_1275 = Vertex(name = 'V_1275',
                particles = [ P.c__tilde__, P.s, P.H__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS1, L.FFS3 ],
                couplings = {(0,0):C.GC_364,(0,1):C.GC_334})

V_1276 = Vertex(name = 'V_1276',
                particles = [ P.t__tilde__, P.b, P.H__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS1, L.FFS3 ],
                couplings = {(0,0):C.GC_343,(0,1):C.GC_370})

V_1277 = Vertex(name = 'V_1277',
                particles = [ P.u__tilde__, P.d, P.HQ__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS1, L.FFS3 ],
                couplings = {(0,0):C.GC_346,(0,1):C.GC_382})

V_1278 = Vertex(name = 'V_1278',
                particles = [ P.c__tilde__, P.s, P.HQ__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS1, L.FFS3 ],
                couplings = {(0,0):C.GC_364,(0,1):C.GC_334})

V_1279 = Vertex(name = 'V_1279',
                particles = [ P.t__tilde__, P.b, P.HQ__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS1, L.FFS3 ],
                couplings = {(0,0):C.GC_343,(0,1):C.GC_370})

V_1280 = Vertex(name = 'V_1280',
                particles = [ P.d__tilde__, P.d, P.G0 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2 ],
                couplings = {(0,0):C.GC_350})

V_1281 = Vertex(name = 'V_1281',
                particles = [ P.s__tilde__, P.s, P.G0 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2 ],
                couplings = {(0,0):C.GC_368})

V_1282 = Vertex(name = 'V_1282',
                particles = [ P.b__tilde__, P.b, P.G0 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2 ],
                couplings = {(0,0):C.GC_341})

V_1283 = Vertex(name = 'V_1283',
                particles = [ P.d__tilde__, P.d, P.GQ0 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2 ],
                couplings = {(0,0):C.GC_350})

V_1284 = Vertex(name = 'V_1284',
                particles = [ P.s__tilde__, P.s, P.GQ0 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2 ],
                couplings = {(0,0):C.GC_368})

V_1285 = Vertex(name = 'V_1285',
                particles = [ P.b__tilde__, P.b, P.GQ0 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2 ],
                couplings = {(0,0):C.GC_341})

V_1286 = Vertex(name = 'V_1286',
                particles = [ P.d__tilde__, P.d, P.Hh ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS4 ],
                couplings = {(0,0):C.GC_348})

V_1287 = Vertex(name = 'V_1287',
                particles = [ P.s__tilde__, P.s, P.Hh ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS4 ],
                couplings = {(0,0):C.GC_366})

V_1288 = Vertex(name = 'V_1288',
                particles = [ P.b__tilde__, P.b, P.Hh ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS4 ],
                couplings = {(0,0):C.GC_339})

V_1289 = Vertex(name = 'V_1289',
                particles = [ P.d__tilde__, P.d, P.HhQ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS4 ],
                couplings = {(0,0):C.GC_348})

V_1290 = Vertex(name = 'V_1290',
                particles = [ P.s__tilde__, P.s, P.HhQ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS4 ],
                couplings = {(0,0):C.GC_366})

V_1291 = Vertex(name = 'V_1291',
                particles = [ P.b__tilde__, P.b, P.HhQ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS4 ],
                couplings = {(0,0):C.GC_339})

V_1292 = Vertex(name = 'V_1292',
                particles = [ P.d__tilde__, P.d, P.Hl ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS4 ],
                couplings = {(0,0):C.GC_345})

V_1293 = Vertex(name = 'V_1293',
                particles = [ P.s__tilde__, P.s, P.Hl ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS4 ],
                couplings = {(0,0):C.GC_363})

V_1294 = Vertex(name = 'V_1294',
                particles = [ P.b__tilde__, P.b, P.Hl ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS4 ],
                couplings = {(0,0):C.GC_342})

V_1295 = Vertex(name = 'V_1295',
                particles = [ P.d__tilde__, P.d, P.HlQ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS4 ],
                couplings = {(0,0):C.GC_345})

V_1296 = Vertex(name = 'V_1296',
                particles = [ P.s__tilde__, P.s, P.HlQ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS4 ],
                couplings = {(0,0):C.GC_363})

V_1297 = Vertex(name = 'V_1297',
                particles = [ P.b__tilde__, P.b, P.HlQ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS4 ],
                couplings = {(0,0):C.GC_342})

V_1298 = Vertex(name = 'V_1298',
                particles = [ P.d__tilde__, P.d, P.Ha ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2 ],
                couplings = {(0,0):C.GC_347})

V_1299 = Vertex(name = 'V_1299',
                particles = [ P.s__tilde__, P.s, P.Ha ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2 ],
                couplings = {(0,0):C.GC_365})

V_1300 = Vertex(name = 'V_1300',
                particles = [ P.b__tilde__, P.b, P.Ha ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2 ],
                couplings = {(0,0):C.GC_344})

V_1301 = Vertex(name = 'V_1301',
                particles = [ P.d__tilde__, P.d, P.HaQ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2 ],
                couplings = {(0,0):C.GC_347})

V_1302 = Vertex(name = 'V_1302',
                particles = [ P.s__tilde__, P.s, P.HaQ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2 ],
                couplings = {(0,0):C.GC_365})

V_1303 = Vertex(name = 'V_1303',
                particles = [ P.b__tilde__, P.b, P.HaQ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2 ],
                couplings = {(0,0):C.GC_344})

V_1304 = Vertex(name = 'V_1304',
                particles = [ P.nu_e__tilde__, P.e__minus__, P.G__plus__ ],
                color = [ '1' ],
                lorentz = [ L.FFS1 ],
                couplings = {(0,0):C.GC_355})

V_1305 = Vertex(name = 'V_1305',
                particles = [ P.nu_mu__tilde__, P.mu__minus__, P.G__plus__ ],
                color = [ '1' ],
                lorentz = [ L.FFS1 ],
                couplings = {(0,0):C.GC_361})

V_1306 = Vertex(name = 'V_1306',
                particles = [ P.nu_tau__tilde__, P.tau__minus__, P.G__plus__ ],
                color = [ '1' ],
                lorentz = [ L.FFS1 ],
                couplings = {(0,0):C.GC_379})

V_1307 = Vertex(name = 'V_1307',
                particles = [ P.nu_e__tilde__, P.e__minus__, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.FFS1 ],
                couplings = {(0,0):C.GC_355})

V_1308 = Vertex(name = 'V_1308',
                particles = [ P.nu_mu__tilde__, P.mu__minus__, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.FFS1 ],
                couplings = {(0,0):C.GC_361})

V_1309 = Vertex(name = 'V_1309',
                particles = [ P.nu_tau__tilde__, P.tau__minus__, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.FFS1 ],
                couplings = {(0,0):C.GC_379})

V_1310 = Vertex(name = 'V_1310',
                particles = [ P.nu_e__tilde__, P.e__minus__, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.FFS1 ],
                couplings = {(0,0):C.GC_352})

V_1311 = Vertex(name = 'V_1311',
                particles = [ P.nu_mu__tilde__, P.mu__minus__, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.FFS1 ],
                couplings = {(0,0):C.GC_358})

V_1312 = Vertex(name = 'V_1312',
                particles = [ P.nu_tau__tilde__, P.tau__minus__, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.FFS1 ],
                couplings = {(0,0):C.GC_376})

V_1313 = Vertex(name = 'V_1313',
                particles = [ P.nu_e__tilde__, P.e__minus__, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.FFS1 ],
                couplings = {(0,0):C.GC_352})

V_1314 = Vertex(name = 'V_1314',
                particles = [ P.nu_mu__tilde__, P.mu__minus__, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.FFS1 ],
                couplings = {(0,0):C.GC_358})

V_1315 = Vertex(name = 'V_1315',
                particles = [ P.nu_tau__tilde__, P.tau__minus__, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.FFS1 ],
                couplings = {(0,0):C.GC_376})

V_1316 = Vertex(name = 'V_1316',
                particles = [ P.e__plus__, P.e__minus__, P.G0 ],
                color = [ '1' ],
                lorentz = [ L.FFS2 ],
                couplings = {(0,0):C.GC_356})

V_1317 = Vertex(name = 'V_1317',
                particles = [ P.mu__plus__, P.mu__minus__, P.G0 ],
                color = [ '1' ],
                lorentz = [ L.FFS2 ],
                couplings = {(0,0):C.GC_362})

V_1318 = Vertex(name = 'V_1318',
                particles = [ P.tau__plus__, P.tau__minus__, P.G0 ],
                color = [ '1' ],
                lorentz = [ L.FFS2 ],
                couplings = {(0,0):C.GC_380})

V_1319 = Vertex(name = 'V_1319',
                particles = [ P.e__plus__, P.e__minus__, P.GQ0 ],
                color = [ '1' ],
                lorentz = [ L.FFS2 ],
                couplings = {(0,0):C.GC_356})

V_1320 = Vertex(name = 'V_1320',
                particles = [ P.mu__plus__, P.mu__minus__, P.GQ0 ],
                color = [ '1' ],
                lorentz = [ L.FFS2 ],
                couplings = {(0,0):C.GC_362})

V_1321 = Vertex(name = 'V_1321',
                particles = [ P.tau__plus__, P.tau__minus__, P.GQ0 ],
                color = [ '1' ],
                lorentz = [ L.FFS2 ],
                couplings = {(0,0):C.GC_380})

V_1322 = Vertex(name = 'V_1322',
                particles = [ P.e__plus__, P.e__minus__, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.FFS4 ],
                couplings = {(0,0):C.GC_354})

V_1323 = Vertex(name = 'V_1323',
                particles = [ P.mu__plus__, P.mu__minus__, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.FFS4 ],
                couplings = {(0,0):C.GC_360})

V_1324 = Vertex(name = 'V_1324',
                particles = [ P.tau__plus__, P.tau__minus__, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.FFS4 ],
                couplings = {(0,0):C.GC_378})

V_1325 = Vertex(name = 'V_1325',
                particles = [ P.e__plus__, P.e__minus__, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.FFS4 ],
                couplings = {(0,0):C.GC_354})

V_1326 = Vertex(name = 'V_1326',
                particles = [ P.mu__plus__, P.mu__minus__, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.FFS4 ],
                couplings = {(0,0):C.GC_360})

V_1327 = Vertex(name = 'V_1327',
                particles = [ P.tau__plus__, P.tau__minus__, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.FFS4 ],
                couplings = {(0,0):C.GC_378})

V_1328 = Vertex(name = 'V_1328',
                particles = [ P.e__plus__, P.e__minus__, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.FFS4 ],
                couplings = {(0,0):C.GC_351})

V_1329 = Vertex(name = 'V_1329',
                particles = [ P.mu__plus__, P.mu__minus__, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.FFS4 ],
                couplings = {(0,0):C.GC_357})

V_1330 = Vertex(name = 'V_1330',
                particles = [ P.tau__plus__, P.tau__minus__, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.FFS4 ],
                couplings = {(0,0):C.GC_375})

V_1331 = Vertex(name = 'V_1331',
                particles = [ P.e__plus__, P.e__minus__, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.FFS4 ],
                couplings = {(0,0):C.GC_351})

V_1332 = Vertex(name = 'V_1332',
                particles = [ P.mu__plus__, P.mu__minus__, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.FFS4 ],
                couplings = {(0,0):C.GC_357})

V_1333 = Vertex(name = 'V_1333',
                particles = [ P.tau__plus__, P.tau__minus__, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.FFS4 ],
                couplings = {(0,0):C.GC_375})

V_1334 = Vertex(name = 'V_1334',
                particles = [ P.e__plus__, P.e__minus__, P.Ha ],
                color = [ '1' ],
                lorentz = [ L.FFS2 ],
                couplings = {(0,0):C.GC_353})

V_1335 = Vertex(name = 'V_1335',
                particles = [ P.mu__plus__, P.mu__minus__, P.Ha ],
                color = [ '1' ],
                lorentz = [ L.FFS2 ],
                couplings = {(0,0):C.GC_359})

V_1336 = Vertex(name = 'V_1336',
                particles = [ P.tau__plus__, P.tau__minus__, P.Ha ],
                color = [ '1' ],
                lorentz = [ L.FFS2 ],
                couplings = {(0,0):C.GC_377})

V_1337 = Vertex(name = 'V_1337',
                particles = [ P.e__plus__, P.e__minus__, P.HaQ ],
                color = [ '1' ],
                lorentz = [ L.FFS2 ],
                couplings = {(0,0):C.GC_353})

V_1338 = Vertex(name = 'V_1338',
                particles = [ P.mu__plus__, P.mu__minus__, P.HaQ ],
                color = [ '1' ],
                lorentz = [ L.FFS2 ],
                couplings = {(0,0):C.GC_359})

V_1339 = Vertex(name = 'V_1339',
                particles = [ P.tau__plus__, P.tau__minus__, P.HaQ ],
                color = [ '1' ],
                lorentz = [ L.FFS2 ],
                couplings = {(0,0):C.GC_377})

V_1340 = Vertex(name = 'V_1340',
                particles = [ P.d__tilde__, P.u, P.G__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS1, L.FFS3 ],
                couplings = {(0,0):C.GC_385,(0,1):C.GC_349})

V_1341 = Vertex(name = 'V_1341',
                particles = [ P.s__tilde__, P.c, P.G__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS1, L.FFS3 ],
                couplings = {(0,0):C.GC_337,(0,1):C.GC_367})

V_1342 = Vertex(name = 'V_1342',
                particles = [ P.b__tilde__, P.t, P.G__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS1, L.FFS3 ],
                couplings = {(0,0):C.GC_373,(0,1):C.GC_340})

V_1343 = Vertex(name = 'V_1343',
                particles = [ P.d__tilde__, P.u, P.GQ__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS1, L.FFS3 ],
                couplings = {(0,0):C.GC_385,(0,1):C.GC_349})

V_1344 = Vertex(name = 'V_1344',
                particles = [ P.s__tilde__, P.c, P.GQ__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS1, L.FFS3 ],
                couplings = {(0,0):C.GC_337,(0,1):C.GC_367})

V_1345 = Vertex(name = 'V_1345',
                particles = [ P.b__tilde__, P.t, P.GQ__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS1, L.FFS3 ],
                couplings = {(0,0):C.GC_373,(0,1):C.GC_340})

V_1346 = Vertex(name = 'V_1346',
                particles = [ P.d__tilde__, P.u, P.H__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS1, L.FFS3 ],
                couplings = {(0,0):C.GC_382,(0,1):C.GC_346})

V_1347 = Vertex(name = 'V_1347',
                particles = [ P.s__tilde__, P.c, P.H__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS1, L.FFS3 ],
                couplings = {(0,0):C.GC_334,(0,1):C.GC_364})

V_1348 = Vertex(name = 'V_1348',
                particles = [ P.b__tilde__, P.t, P.H__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS1, L.FFS3 ],
                couplings = {(0,0):C.GC_370,(0,1):C.GC_343})

V_1349 = Vertex(name = 'V_1349',
                particles = [ P.d__tilde__, P.u, P.HQ__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS1, L.FFS3 ],
                couplings = {(0,0):C.GC_382,(0,1):C.GC_346})

V_1350 = Vertex(name = 'V_1350',
                particles = [ P.s__tilde__, P.c, P.HQ__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS1, L.FFS3 ],
                couplings = {(0,0):C.GC_334,(0,1):C.GC_364})

V_1351 = Vertex(name = 'V_1351',
                particles = [ P.b__tilde__, P.t, P.HQ__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS1, L.FFS3 ],
                couplings = {(0,0):C.GC_370,(0,1):C.GC_343})

V_1352 = Vertex(name = 'V_1352',
                particles = [ P.u__tilde__, P.u, P.G0 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2 ],
                couplings = {(0,0):C.GC_386})

V_1353 = Vertex(name = 'V_1353',
                particles = [ P.c__tilde__, P.c, P.G0 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2 ],
                couplings = {(0,0):C.GC_338})

V_1354 = Vertex(name = 'V_1354',
                particles = [ P.t__tilde__, P.t, P.G0 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2 ],
                couplings = {(0,0):C.GC_374})

V_1355 = Vertex(name = 'V_1355',
                particles = [ P.u__tilde__, P.u, P.GQ0 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2 ],
                couplings = {(0,0):C.GC_386})

V_1356 = Vertex(name = 'V_1356',
                particles = [ P.c__tilde__, P.c, P.GQ0 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2 ],
                couplings = {(0,0):C.GC_338})

V_1357 = Vertex(name = 'V_1357',
                particles = [ P.t__tilde__, P.t, P.GQ0 ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2 ],
                couplings = {(0,0):C.GC_374})

V_1358 = Vertex(name = 'V_1358',
                particles = [ P.u__tilde__, P.u, P.Hh ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS4 ],
                couplings = {(0,0):C.GC_384})

V_1359 = Vertex(name = 'V_1359',
                particles = [ P.c__tilde__, P.c, P.Hh ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS4 ],
                couplings = {(0,0):C.GC_336})

V_1360 = Vertex(name = 'V_1360',
                particles = [ P.t__tilde__, P.t, P.Hh ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS4 ],
                couplings = {(0,0):C.GC_372})

V_1361 = Vertex(name = 'V_1361',
                particles = [ P.u__tilde__, P.u, P.HhQ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS4 ],
                couplings = {(0,0):C.GC_384})

V_1362 = Vertex(name = 'V_1362',
                particles = [ P.c__tilde__, P.c, P.HhQ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS4 ],
                couplings = {(0,0):C.GC_336})

V_1363 = Vertex(name = 'V_1363',
                particles = [ P.t__tilde__, P.t, P.HhQ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS4 ],
                couplings = {(0,0):C.GC_372})

V_1364 = Vertex(name = 'V_1364',
                particles = [ P.u__tilde__, P.u, P.Hl ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS4 ],
                couplings = {(0,0):C.GC_381})

V_1365 = Vertex(name = 'V_1365',
                particles = [ P.c__tilde__, P.c, P.Hl ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS4 ],
                couplings = {(0,0):C.GC_333})

V_1366 = Vertex(name = 'V_1366',
                particles = [ P.t__tilde__, P.t, P.Hl ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS4 ],
                couplings = {(0,0):C.GC_369})

V_1367 = Vertex(name = 'V_1367',
                particles = [ P.u__tilde__, P.u, P.HlQ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS4 ],
                couplings = {(0,0):C.GC_381})

V_1368 = Vertex(name = 'V_1368',
                particles = [ P.c__tilde__, P.c, P.HlQ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS4 ],
                couplings = {(0,0):C.GC_333})

V_1369 = Vertex(name = 'V_1369',
                particles = [ P.t__tilde__, P.t, P.HlQ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS4 ],
                couplings = {(0,0):C.GC_369})

V_1370 = Vertex(name = 'V_1370',
                particles = [ P.u__tilde__, P.u, P.Ha ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2 ],
                couplings = {(0,0):C.GC_383})

V_1371 = Vertex(name = 'V_1371',
                particles = [ P.c__tilde__, P.c, P.Ha ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2 ],
                couplings = {(0,0):C.GC_335})

V_1372 = Vertex(name = 'V_1372',
                particles = [ P.t__tilde__, P.t, P.Ha ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2 ],
                couplings = {(0,0):C.GC_371})

V_1373 = Vertex(name = 'V_1373',
                particles = [ P.u__tilde__, P.u, P.HaQ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2 ],
                couplings = {(0,0):C.GC_383})

V_1374 = Vertex(name = 'V_1374',
                particles = [ P.c__tilde__, P.c, P.HaQ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2 ],
                couplings = {(0,0):C.GC_335})

V_1375 = Vertex(name = 'V_1375',
                particles = [ P.t__tilde__, P.t, P.HaQ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2 ],
                couplings = {(0,0):C.GC_371})

V_1376 = Vertex(name = 'V_1376',
                particles = [ P.ghA, P.ghWm__tilde__, P.A, P.W__minus__ ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_7})

V_1377 = Vertex(name = 'V_1377',
                particles = [ P.ghWp, P.ghA__tilde__, P.A, P.W__minus__ ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_7})

V_1378 = Vertex(name = 'V_1378',
                particles = [ P.ghWp, P.ghZ__tilde__, P.A, P.W__minus__ ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_203})

V_1379 = Vertex(name = 'V_1379',
                particles = [ P.ghZ, P.ghWm__tilde__, P.A, P.W__minus__ ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_203})

V_1380 = Vertex(name = 'V_1380',
                particles = [ P.A, P.W__minus__, P.G0, P.G__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_187})

V_1381 = Vertex(name = 'V_1381',
                particles = [ P.A, P.W__minus__, P.G__plus__, P.GQ0 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_187})

V_1382 = Vertex(name = 'V_1382',
                particles = [ P.A, P.W__minus__, P.G0, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_187})

V_1383 = Vertex(name = 'V_1383',
                particles = [ P.A, P.W__minus__, P.GQ0, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_187})

V_1384 = Vertex(name = 'V_1384',
                particles = [ P.A, P.W__minus__, P.G__plus__, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_180})

V_1385 = Vertex(name = 'V_1385',
                particles = [ P.A, P.W__minus__, P.GQ__plus__, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_180})

V_1386 = Vertex(name = 'V_1386',
                particles = [ P.A, P.W__minus__, P.G__plus__, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_180})

V_1387 = Vertex(name = 'V_1387',
                particles = [ P.A, P.W__minus__, P.GQ__plus__, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_180})

V_1388 = Vertex(name = 'V_1388',
                particles = [ P.A, P.W__minus__, P.Ha, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_187})

V_1389 = Vertex(name = 'V_1389',
                particles = [ P.A, P.W__minus__, P.HaQ, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_187})

V_1390 = Vertex(name = 'V_1390',
                particles = [ P.A, P.W__minus__, P.Hl, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_180})

V_1391 = Vertex(name = 'V_1391',
                particles = [ P.A, P.W__minus__, P.HlQ, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_180})

V_1392 = Vertex(name = 'V_1392',
                particles = [ P.A, P.W__minus__, P.Ha, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_187})

V_1393 = Vertex(name = 'V_1393',
                particles = [ P.A, P.W__minus__, P.HaQ, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_187})

V_1394 = Vertex(name = 'V_1394',
                particles = [ P.A, P.W__minus__, P.Hl, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_180})

V_1395 = Vertex(name = 'V_1395',
                particles = [ P.A, P.W__minus__, P.HlQ, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_180})

V_1396 = Vertex(name = 'V_1396',
                particles = [ P.A, P.W__minus__, P.G__plus__, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_174})

V_1397 = Vertex(name = 'V_1397',
                particles = [ P.A, P.W__minus__, P.GQ__plus__, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_174})

V_1398 = Vertex(name = 'V_1398',
                particles = [ P.A, P.W__minus__, P.G__plus__, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_174})

V_1399 = Vertex(name = 'V_1399',
                particles = [ P.A, P.W__minus__, P.GQ__plus__, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_174})

V_1400 = Vertex(name = 'V_1400',
                particles = [ P.A, P.W__minus__, P.Hh, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_175})

V_1401 = Vertex(name = 'V_1401',
                particles = [ P.A, P.W__minus__, P.HhQ, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_175})

V_1402 = Vertex(name = 'V_1402',
                particles = [ P.A, P.W__minus__, P.Hh, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_175})

V_1403 = Vertex(name = 'V_1403',
                particles = [ P.A, P.W__minus__, P.HhQ, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_175})

V_1404 = Vertex(name = 'V_1404',
                particles = [ P.A, P.W__minus__, P.G__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_324})

V_1405 = Vertex(name = 'V_1405',
                particles = [ P.A, P.W__minus__, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_324})

V_1406 = Vertex(name = 'V_1406',
                particles = [ P.A, P.W__minus__, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_321})

V_1407 = Vertex(name = 'V_1407',
                particles = [ P.A, P.W__minus__, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_321})

V_1408 = Vertex(name = 'V_1408',
                particles = [ P.ghA, P.ghWm__tilde__, P.AQ, P.W__minus__ ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_7})

V_1409 = Vertex(name = 'V_1409',
                particles = [ P.ghZ, P.ghWm__tilde__, P.AQ, P.W__minus__ ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_203})

V_1410 = Vertex(name = 'V_1410',
                particles = [ P.AQ, P.W__minus__, P.G0, P.G__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_187})

V_1411 = Vertex(name = 'V_1411',
                particles = [ P.AQ, P.W__minus__, P.G0, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_188})

V_1412 = Vertex(name = 'V_1412',
                particles = [ P.AQ, P.W__minus__, P.GQ0, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_187})

V_1413 = Vertex(name = 'V_1413',
                particles = [ P.AQ, P.W__minus__, P.G__plus__, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_180})

V_1414 = Vertex(name = 'V_1414',
                particles = [ P.AQ, P.W__minus__, P.GQ__plus__, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_182})

V_1415 = Vertex(name = 'V_1415',
                particles = [ P.AQ, P.W__minus__, P.GQ__plus__, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_180})

V_1416 = Vertex(name = 'V_1416',
                particles = [ P.AQ, P.W__minus__, P.Ha, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_187})

V_1417 = Vertex(name = 'V_1417',
                particles = [ P.AQ, P.W__minus__, P.Hl, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_180})

V_1418 = Vertex(name = 'V_1418',
                particles = [ P.AQ, P.W__minus__, P.Ha, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_188})

V_1419 = Vertex(name = 'V_1419',
                particles = [ P.AQ, P.W__minus__, P.HaQ, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_187})

V_1420 = Vertex(name = 'V_1420',
                particles = [ P.AQ, P.W__minus__, P.Hl, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_182})

V_1421 = Vertex(name = 'V_1421',
                particles = [ P.AQ, P.W__minus__, P.HlQ, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_180})

V_1422 = Vertex(name = 'V_1422',
                particles = [ P.AQ, P.W__minus__, P.G__plus__, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_174})

V_1423 = Vertex(name = 'V_1423',
                particles = [ P.AQ, P.W__minus__, P.GQ__plus__, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_176})

V_1424 = Vertex(name = 'V_1424',
                particles = [ P.AQ, P.W__minus__, P.GQ__plus__, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_174})

V_1425 = Vertex(name = 'V_1425',
                particles = [ P.AQ, P.W__minus__, P.Hh, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_175})

V_1426 = Vertex(name = 'V_1426',
                particles = [ P.AQ, P.W__minus__, P.Hh, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_177})

V_1427 = Vertex(name = 'V_1427',
                particles = [ P.AQ, P.W__minus__, P.HhQ, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_175})

V_1428 = Vertex(name = 'V_1428',
                particles = [ P.AQ, P.W__minus__, P.G__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_324})

V_1429 = Vertex(name = 'V_1429',
                particles = [ P.AQ, P.W__minus__, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_326})

V_1430 = Vertex(name = 'V_1430',
                particles = [ P.AQ, P.W__minus__, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_321})

V_1431 = Vertex(name = 'V_1431',
                particles = [ P.AQ, P.W__minus__, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_323})

V_1432 = Vertex(name = 'V_1432',
                particles = [ P.W__minus__, P.G0, P.G__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_184})

V_1433 = Vertex(name = 'V_1433',
                particles = [ P.W__minus__, P.G0, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_184})

V_1434 = Vertex(name = 'V_1434',
                particles = [ P.W__minus__, P.G__plus__, P.GQ0 ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_183})

V_1435 = Vertex(name = 'V_1435',
                particles = [ P.W__minus__, P.G__plus__, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_179})

V_1436 = Vertex(name = 'V_1436',
                particles = [ P.W__minus__, P.G__plus__, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_179})

V_1437 = Vertex(name = 'V_1437',
                particles = [ P.W__minus__, P.G__plus__, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_173})

V_1438 = Vertex(name = 'V_1438',
                particles = [ P.W__minus__, P.G__plus__, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_173})

V_1439 = Vertex(name = 'V_1439',
                particles = [ P.W__minus__, P.GQ0, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_184})

V_1440 = Vertex(name = 'V_1440',
                particles = [ P.W__minus__, P.GQ__plus__, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_179})

V_1441 = Vertex(name = 'V_1441',
                particles = [ P.W__minus__, P.GQ__plus__, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_179})

V_1442 = Vertex(name = 'V_1442',
                particles = [ P.W__minus__, P.GQ__plus__, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_173})

V_1443 = Vertex(name = 'V_1443',
                particles = [ P.W__minus__, P.GQ__plus__, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_173})

V_1444 = Vertex(name = 'V_1444',
                particles = [ P.W__minus__, P.Ha, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_184})

V_1445 = Vertex(name = 'V_1445',
                particles = [ P.W__minus__, P.Ha, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_184})

V_1446 = Vertex(name = 'V_1446',
                particles = [ P.W__minus__, P.HaQ, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_184})

V_1447 = Vertex(name = 'V_1447',
                particles = [ P.W__minus__, P.HaQ, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_184})

V_1448 = Vertex(name = 'V_1448',
                particles = [ P.W__minus__, P.Hh, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_173})

V_1449 = Vertex(name = 'V_1449',
                particles = [ P.W__minus__, P.Hh, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_173})

V_1450 = Vertex(name = 'V_1450',
                particles = [ P.W__minus__, P.HhQ, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_173})

V_1451 = Vertex(name = 'V_1451',
                particles = [ P.W__minus__, P.HhQ, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_173})

V_1452 = Vertex(name = 'V_1452',
                particles = [ P.W__minus__, P.Hl, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_178})

V_1453 = Vertex(name = 'V_1453',
                particles = [ P.W__minus__, P.Hl, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_178})

V_1454 = Vertex(name = 'V_1454',
                particles = [ P.W__minus__, P.HlQ, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_178})

V_1455 = Vertex(name = 'V_1455',
                particles = [ P.W__minus__, P.HlQ, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_178})

V_1456 = Vertex(name = 'V_1456',
                particles = [ P.A, P.W__minus__, P.W__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVV1 ],
                couplings = {(0,0):C.GC_3})

V_1457 = Vertex(name = 'V_1457',
                particles = [ P.AQ, P.W__minus__, P.W__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVV1 ],
                couplings = {(0,0):C.GC_3})

V_1458 = Vertex(name = 'V_1458',
                particles = [ P.A, P.W__minus__, P.WQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVV1 ],
                couplings = {(0,0):C.GC_3})

V_1459 = Vertex(name = 'V_1459',
                particles = [ P.AQ, P.W__minus__, P.WQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVV3 ],
                couplings = {(0,0):C.GC_3})

V_1460 = Vertex(name = 'V_1460',
                particles = [ P.ghWp, P.ghWm__tilde__, P.W__minus__, P.W__minus__ ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_191})

V_1461 = Vertex(name = 'V_1461',
                particles = [ P.ghWm, P.ghA__tilde__, P.A, P.W__plus__ ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_7})

V_1462 = Vertex(name = 'V_1462',
                particles = [ P.ghA, P.ghWp__tilde__, P.A, P.W__plus__ ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_7})

V_1463 = Vertex(name = 'V_1463',
                particles = [ P.ghWm, P.ghZ__tilde__, P.A, P.W__plus__ ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_203})

V_1464 = Vertex(name = 'V_1464',
                particles = [ P.ghZ, P.ghWp__tilde__, P.A, P.W__plus__ ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_203})

V_1465 = Vertex(name = 'V_1465',
                particles = [ P.A, P.W__plus__, P.G0, P.G__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_186})

V_1466 = Vertex(name = 'V_1466',
                particles = [ P.A, P.W__plus__, P.G__minus__, P.GQ0 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_186})

V_1467 = Vertex(name = 'V_1467',
                particles = [ P.A, P.W__plus__, P.G0, P.GQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_186})

V_1468 = Vertex(name = 'V_1468',
                particles = [ P.A, P.W__plus__, P.GQ0, P.GQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_186})

V_1469 = Vertex(name = 'V_1469',
                particles = [ P.A, P.W__plus__, P.G__minus__, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_180})

V_1470 = Vertex(name = 'V_1470',
                particles = [ P.A, P.W__plus__, P.GQ__minus__, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_180})

V_1471 = Vertex(name = 'V_1471',
                particles = [ P.A, P.W__plus__, P.G__minus__, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_180})

V_1472 = Vertex(name = 'V_1472',
                particles = [ P.A, P.W__plus__, P.GQ__minus__, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_180})

V_1473 = Vertex(name = 'V_1473',
                particles = [ P.A, P.W__plus__, P.Ha, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_186})

V_1474 = Vertex(name = 'V_1474',
                particles = [ P.A, P.W__plus__, P.HaQ, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_186})

V_1475 = Vertex(name = 'V_1475',
                particles = [ P.A, P.W__plus__, P.Hl, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_180})

V_1476 = Vertex(name = 'V_1476',
                particles = [ P.A, P.W__plus__, P.HlQ, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_180})

V_1477 = Vertex(name = 'V_1477',
                particles = [ P.A, P.W__plus__, P.Ha, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_186})

V_1478 = Vertex(name = 'V_1478',
                particles = [ P.A, P.W__plus__, P.HaQ, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_186})

V_1479 = Vertex(name = 'V_1479',
                particles = [ P.A, P.W__plus__, P.Hl, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_180})

V_1480 = Vertex(name = 'V_1480',
                particles = [ P.A, P.W__plus__, P.HlQ, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_180})

V_1481 = Vertex(name = 'V_1481',
                particles = [ P.A, P.W__plus__, P.G__minus__, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_174})

V_1482 = Vertex(name = 'V_1482',
                particles = [ P.A, P.W__plus__, P.GQ__minus__, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_174})

V_1483 = Vertex(name = 'V_1483',
                particles = [ P.A, P.W__plus__, P.G__minus__, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_174})

V_1484 = Vertex(name = 'V_1484',
                particles = [ P.A, P.W__plus__, P.GQ__minus__, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_174})

V_1485 = Vertex(name = 'V_1485',
                particles = [ P.A, P.W__plus__, P.Hh, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_175})

V_1486 = Vertex(name = 'V_1486',
                particles = [ P.A, P.W__plus__, P.HhQ, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_175})

V_1487 = Vertex(name = 'V_1487',
                particles = [ P.A, P.W__plus__, P.Hh, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_175})

V_1488 = Vertex(name = 'V_1488',
                particles = [ P.A, P.W__plus__, P.HhQ, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_175})

V_1489 = Vertex(name = 'V_1489',
                particles = [ P.A, P.W__plus__, P.G__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_324})

V_1490 = Vertex(name = 'V_1490',
                particles = [ P.A, P.W__plus__, P.GQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_324})

V_1491 = Vertex(name = 'V_1491',
                particles = [ P.A, P.W__plus__, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_321})

V_1492 = Vertex(name = 'V_1492',
                particles = [ P.A, P.W__plus__, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_321})

V_1493 = Vertex(name = 'V_1493',
                particles = [ P.ghA, P.ghWp__tilde__, P.AQ, P.W__plus__ ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_7})

V_1494 = Vertex(name = 'V_1494',
                particles = [ P.ghZ, P.ghWp__tilde__, P.AQ, P.W__plus__ ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_203})

V_1495 = Vertex(name = 'V_1495',
                particles = [ P.AQ, P.W__plus__, P.G0, P.G__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_186})

V_1496 = Vertex(name = 'V_1496',
                particles = [ P.AQ, P.W__plus__, P.G0, P.GQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_185})

V_1497 = Vertex(name = 'V_1497',
                particles = [ P.AQ, P.W__plus__, P.GQ0, P.GQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_186})

V_1498 = Vertex(name = 'V_1498',
                particles = [ P.AQ, P.W__plus__, P.G__minus__, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_180})

V_1499 = Vertex(name = 'V_1499',
                particles = [ P.AQ, P.W__plus__, P.GQ__minus__, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_182})

V_1500 = Vertex(name = 'V_1500',
                particles = [ P.AQ, P.W__plus__, P.GQ__minus__, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_180})

V_1501 = Vertex(name = 'V_1501',
                particles = [ P.AQ, P.W__plus__, P.Ha, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_186})

V_1502 = Vertex(name = 'V_1502',
                particles = [ P.AQ, P.W__plus__, P.Hl, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_180})

V_1503 = Vertex(name = 'V_1503',
                particles = [ P.AQ, P.W__plus__, P.Ha, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_185})

V_1504 = Vertex(name = 'V_1504',
                particles = [ P.AQ, P.W__plus__, P.HaQ, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_186})

V_1505 = Vertex(name = 'V_1505',
                particles = [ P.AQ, P.W__plus__, P.Hl, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_182})

V_1506 = Vertex(name = 'V_1506',
                particles = [ P.AQ, P.W__plus__, P.HlQ, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_180})

V_1507 = Vertex(name = 'V_1507',
                particles = [ P.AQ, P.W__plus__, P.G__minus__, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_174})

V_1508 = Vertex(name = 'V_1508',
                particles = [ P.AQ, P.W__plus__, P.GQ__minus__, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_176})

V_1509 = Vertex(name = 'V_1509',
                particles = [ P.AQ, P.W__plus__, P.GQ__minus__, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_174})

V_1510 = Vertex(name = 'V_1510',
                particles = [ P.AQ, P.W__plus__, P.Hh, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_175})

V_1511 = Vertex(name = 'V_1511',
                particles = [ P.AQ, P.W__plus__, P.Hh, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_177})

V_1512 = Vertex(name = 'V_1512',
                particles = [ P.AQ, P.W__plus__, P.HhQ, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_175})

V_1513 = Vertex(name = 'V_1513',
                particles = [ P.AQ, P.W__plus__, P.G__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_324})

V_1514 = Vertex(name = 'V_1514',
                particles = [ P.AQ, P.W__plus__, P.GQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_326})

V_1515 = Vertex(name = 'V_1515',
                particles = [ P.AQ, P.W__plus__, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_321})

V_1516 = Vertex(name = 'V_1516',
                particles = [ P.AQ, P.W__plus__, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_323})

V_1517 = Vertex(name = 'V_1517',
                particles = [ P.W__plus__, P.G0, P.G__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_184})

V_1518 = Vertex(name = 'V_1518',
                particles = [ P.W__plus__, P.G0, P.GQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_184})

V_1519 = Vertex(name = 'V_1519',
                particles = [ P.W__plus__, P.G__minus__, P.GQ0 ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_183})

V_1520 = Vertex(name = 'V_1520',
                particles = [ P.W__plus__, P.G__minus__, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_178})

V_1521 = Vertex(name = 'V_1521',
                particles = [ P.W__plus__, P.G__minus__, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_178})

V_1522 = Vertex(name = 'V_1522',
                particles = [ P.W__plus__, P.G__minus__, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_172})

V_1523 = Vertex(name = 'V_1523',
                particles = [ P.W__plus__, P.G__minus__, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_172})

V_1524 = Vertex(name = 'V_1524',
                particles = [ P.W__plus__, P.GQ0, P.GQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_184})

V_1525 = Vertex(name = 'V_1525',
                particles = [ P.W__plus__, P.GQ__minus__, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_178})

V_1526 = Vertex(name = 'V_1526',
                particles = [ P.W__plus__, P.GQ__minus__, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_178})

V_1527 = Vertex(name = 'V_1527',
                particles = [ P.W__plus__, P.GQ__minus__, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_172})

V_1528 = Vertex(name = 'V_1528',
                particles = [ P.W__plus__, P.GQ__minus__, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_172})

V_1529 = Vertex(name = 'V_1529',
                particles = [ P.W__plus__, P.Ha, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_184})

V_1530 = Vertex(name = 'V_1530',
                particles = [ P.W__plus__, P.Ha, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_184})

V_1531 = Vertex(name = 'V_1531',
                particles = [ P.W__plus__, P.HaQ, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_184})

V_1532 = Vertex(name = 'V_1532',
                particles = [ P.W__plus__, P.HaQ, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_184})

V_1533 = Vertex(name = 'V_1533',
                particles = [ P.W__plus__, P.Hh, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_172})

V_1534 = Vertex(name = 'V_1534',
                particles = [ P.W__plus__, P.Hh, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_172})

V_1535 = Vertex(name = 'V_1535',
                particles = [ P.W__plus__, P.HhQ, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_172})

V_1536 = Vertex(name = 'V_1536',
                particles = [ P.W__plus__, P.HhQ, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_172})

V_1537 = Vertex(name = 'V_1537',
                particles = [ P.W__plus__, P.Hl, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_179})

V_1538 = Vertex(name = 'V_1538',
                particles = [ P.W__plus__, P.Hl, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_179})

V_1539 = Vertex(name = 'V_1539',
                particles = [ P.W__plus__, P.HlQ, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_179})

V_1540 = Vertex(name = 'V_1540',
                particles = [ P.W__plus__, P.HlQ, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_179})

V_1541 = Vertex(name = 'V_1541',
                particles = [ P.A, P.W__plus__, P.WQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVV1 ],
                couplings = {(0,0):C.GC_4})

V_1542 = Vertex(name = 'V_1542',
                particles = [ P.AQ, P.W__plus__, P.WQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVV3 ],
                couplings = {(0,0):C.GC_4})

V_1543 = Vertex(name = 'V_1543',
                particles = [ P.ghA, P.ghA__tilde__, P.W__minus__, P.W__plus__ ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_10})

V_1544 = Vertex(name = 'V_1544',
                particles = [ P.W__minus__, P.W__plus__, P.G0, P.G0 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_100})

V_1545 = Vertex(name = 'V_1545',
                particles = [ P.ghWm, P.ghWm__tilde__, P.W__minus__, P.W__plus__ ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_190})

V_1546 = Vertex(name = 'V_1546',
                particles = [ P.ghWp, P.ghWp__tilde__, P.W__minus__, P.W__plus__ ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_190})

V_1547 = Vertex(name = 'V_1547',
                particles = [ P.ghZ, P.ghZ__tilde__, P.W__minus__, P.W__plus__ ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_196})

V_1548 = Vertex(name = 'V_1548',
                particles = [ P.W__minus__, P.W__plus__, P.G__minus__, P.G__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_100})

V_1549 = Vertex(name = 'V_1549',
                particles = [ P.W__minus__, P.W__plus__, P.G0, P.GQ0 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_100})

V_1550 = Vertex(name = 'V_1550',
                particles = [ P.W__minus__, P.W__plus__, P.GQ0, P.GQ0 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_100})

V_1551 = Vertex(name = 'V_1551',
                particles = [ P.W__minus__, P.W__plus__, P.G__plus__, P.GQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_100})

V_1552 = Vertex(name = 'V_1552',
                particles = [ P.W__minus__, P.W__plus__, P.G__minus__, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_100})

V_1553 = Vertex(name = 'V_1553',
                particles = [ P.W__minus__, P.W__plus__, P.GQ__minus__, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_100})

V_1554 = Vertex(name = 'V_1554',
                particles = [ P.W__minus__, P.W__plus__, P.Ha, P.Ha ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_100})

V_1555 = Vertex(name = 'V_1555',
                particles = [ P.W__minus__, P.W__plus__, P.Ha, P.HaQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_100})

V_1556 = Vertex(name = 'V_1556',
                particles = [ P.W__minus__, P.W__plus__, P.HaQ, P.HaQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_100})

V_1557 = Vertex(name = 'V_1557',
                particles = [ P.W__minus__, P.W__plus__, P.Hh, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_77})

V_1558 = Vertex(name = 'V_1558',
                particles = [ P.W__minus__, P.W__plus__, P.Hh, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_77})

V_1559 = Vertex(name = 'V_1559',
                particles = [ P.W__minus__, P.W__plus__, P.HhQ, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_77})

V_1560 = Vertex(name = 'V_1560',
                particles = [ P.W__minus__, P.W__plus__, P.Hl, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_77})

V_1561 = Vertex(name = 'V_1561',
                particles = [ P.W__minus__, P.W__plus__, P.Hl, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_77})

V_1562 = Vertex(name = 'V_1562',
                particles = [ P.W__minus__, P.W__plus__, P.HlQ, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_77})

V_1563 = Vertex(name = 'V_1563',
                particles = [ P.W__minus__, P.W__plus__, P.H__minus__, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_100})

V_1564 = Vertex(name = 'V_1564',
                particles = [ P.W__minus__, P.W__plus__, P.H__plus__, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_100})

V_1565 = Vertex(name = 'V_1565',
                particles = [ P.W__minus__, P.W__plus__, P.H__minus__, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_100})

V_1566 = Vertex(name = 'V_1566',
                particles = [ P.W__minus__, P.W__plus__, P.HQ__minus__, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_100})

V_1567 = Vertex(name = 'V_1567',
                particles = [ P.ghA, P.ghZ__tilde__, P.W__minus__, P.W__plus__ ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_204})

V_1568 = Vertex(name = 'V_1568',
                particles = [ P.ghZ, P.ghA__tilde__, P.W__minus__, P.W__plus__ ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_204})

V_1569 = Vertex(name = 'V_1569',
                particles = [ P.W__minus__, P.W__plus__, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_272})

V_1570 = Vertex(name = 'V_1570',
                particles = [ P.W__minus__, P.W__plus__, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_272})

V_1571 = Vertex(name = 'V_1571',
                particles = [ P.W__minus__, P.W__plus__, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_261})

V_1572 = Vertex(name = 'V_1572',
                particles = [ P.W__minus__, P.W__plus__, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_261})

V_1573 = Vertex(name = 'V_1573',
                particles = [ P.A, P.A, P.W__minus__, P.W__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVVV5 ],
                couplings = {(0,0):C.GC_8})

V_1574 = Vertex(name = 'V_1574',
                particles = [ P.A, P.AQ, P.W__minus__, P.W__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVVV5 ],
                couplings = {(0,0):C.GC_8})

V_1575 = Vertex(name = 'V_1575',
                particles = [ P.AQ, P.AQ, P.W__minus__, P.W__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVVV4 ],
                couplings = {(0,0):C.GC_9})

V_1576 = Vertex(name = 'V_1576',
                particles = [ P.W__minus__, P.W__plus__, P.Z ],
                color = [ '1' ],
                lorentz = [ L.VVV1 ],
                couplings = {(0,0):C.GC_199})

V_1577 = Vertex(name = 'V_1577',
                particles = [ P.W__minus__, P.W__plus__, P.ZQ ],
                color = [ '1' ],
                lorentz = [ L.VVV1 ],
                couplings = {(0,0):C.GC_199})

V_1578 = Vertex(name = 'V_1578',
                particles = [ P.ghWm, P.ghWp__tilde__, P.W__plus__, P.W__plus__ ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_191})

V_1579 = Vertex(name = 'V_1579',
                particles = [ P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVVV5 ],
                couplings = {(0,0):C.GC_189})

V_1580 = Vertex(name = 'V_1580',
                particles = [ P.ghWp, P.ghA__tilde__, P.A, P.WQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_7})

V_1581 = Vertex(name = 'V_1581',
                particles = [ P.ghWp, P.ghZ__tilde__, P.A, P.WQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_203})

V_1582 = Vertex(name = 'V_1582',
                particles = [ P.A, P.WQ__minus__, P.G0, P.G__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_187})

V_1583 = Vertex(name = 'V_1583',
                particles = [ P.A, P.WQ__minus__, P.G__plus__, P.GQ0 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_188})

V_1584 = Vertex(name = 'V_1584',
                particles = [ P.A, P.WQ__minus__, P.GQ0, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_187})

V_1585 = Vertex(name = 'V_1585',
                particles = [ P.A, P.WQ__minus__, P.G__plus__, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_180})

V_1586 = Vertex(name = 'V_1586',
                particles = [ P.A, P.WQ__minus__, P.G__plus__, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_182})

V_1587 = Vertex(name = 'V_1587',
                particles = [ P.A, P.WQ__minus__, P.GQ__plus__, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_180})

V_1588 = Vertex(name = 'V_1588',
                particles = [ P.A, P.WQ__minus__, P.Ha, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_187})

V_1589 = Vertex(name = 'V_1589',
                particles = [ P.A, P.WQ__minus__, P.HaQ, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_188})

V_1590 = Vertex(name = 'V_1590',
                particles = [ P.A, P.WQ__minus__, P.Hl, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_180})

V_1591 = Vertex(name = 'V_1591',
                particles = [ P.A, P.WQ__minus__, P.HlQ, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_182})

V_1592 = Vertex(name = 'V_1592',
                particles = [ P.A, P.WQ__minus__, P.HaQ, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_187})

V_1593 = Vertex(name = 'V_1593',
                particles = [ P.A, P.WQ__minus__, P.HlQ, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_180})

V_1594 = Vertex(name = 'V_1594',
                particles = [ P.A, P.WQ__minus__, P.G__plus__, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_174})

V_1595 = Vertex(name = 'V_1595',
                particles = [ P.A, P.WQ__minus__, P.G__plus__, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_176})

V_1596 = Vertex(name = 'V_1596',
                particles = [ P.A, P.WQ__minus__, P.GQ__plus__, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_174})

V_1597 = Vertex(name = 'V_1597',
                particles = [ P.A, P.WQ__minus__, P.Hh, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_175})

V_1598 = Vertex(name = 'V_1598',
                particles = [ P.A, P.WQ__minus__, P.HhQ, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_177})

V_1599 = Vertex(name = 'V_1599',
                particles = [ P.A, P.WQ__minus__, P.HhQ, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_175})

V_1600 = Vertex(name = 'V_1600',
                particles = [ P.A, P.WQ__minus__, P.G__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_324})

V_1601 = Vertex(name = 'V_1601',
                particles = [ P.A, P.WQ__minus__, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_321})

V_1602 = Vertex(name = 'V_1602',
                particles = [ P.AQ, P.WQ__minus__, P.G0, P.G__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_187})

V_1603 = Vertex(name = 'V_1603',
                particles = [ P.AQ, P.WQ__minus__, P.G__plus__, P.GQ0 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_187})

V_1604 = Vertex(name = 'V_1604',
                particles = [ P.AQ, P.WQ__minus__, P.G0, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_187})

V_1605 = Vertex(name = 'V_1605',
                particles = [ P.AQ, P.WQ__minus__, P.GQ0, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_187})

V_1606 = Vertex(name = 'V_1606',
                particles = [ P.AQ, P.WQ__minus__, P.G__plus__, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_180})

V_1607 = Vertex(name = 'V_1607',
                particles = [ P.AQ, P.WQ__minus__, P.GQ__plus__, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_180})

V_1608 = Vertex(name = 'V_1608',
                particles = [ P.AQ, P.WQ__minus__, P.G__plus__, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_180})

V_1609 = Vertex(name = 'V_1609',
                particles = [ P.AQ, P.WQ__minus__, P.GQ__plus__, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_180})

V_1610 = Vertex(name = 'V_1610',
                particles = [ P.AQ, P.WQ__minus__, P.Ha, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_187})

V_1611 = Vertex(name = 'V_1611',
                particles = [ P.AQ, P.WQ__minus__, P.HaQ, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_187})

V_1612 = Vertex(name = 'V_1612',
                particles = [ P.AQ, P.WQ__minus__, P.Hl, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_180})

V_1613 = Vertex(name = 'V_1613',
                particles = [ P.AQ, P.WQ__minus__, P.HlQ, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_180})

V_1614 = Vertex(name = 'V_1614',
                particles = [ P.AQ, P.WQ__minus__, P.Ha, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_187})

V_1615 = Vertex(name = 'V_1615',
                particles = [ P.AQ, P.WQ__minus__, P.HaQ, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_187})

V_1616 = Vertex(name = 'V_1616',
                particles = [ P.AQ, P.WQ__minus__, P.Hl, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_180})

V_1617 = Vertex(name = 'V_1617',
                particles = [ P.AQ, P.WQ__minus__, P.HlQ, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_180})

V_1618 = Vertex(name = 'V_1618',
                particles = [ P.AQ, P.WQ__minus__, P.G__plus__, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_174})

V_1619 = Vertex(name = 'V_1619',
                particles = [ P.AQ, P.WQ__minus__, P.GQ__plus__, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_174})

V_1620 = Vertex(name = 'V_1620',
                particles = [ P.AQ, P.WQ__minus__, P.G__plus__, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_174})

V_1621 = Vertex(name = 'V_1621',
                particles = [ P.AQ, P.WQ__minus__, P.GQ__plus__, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_174})

V_1622 = Vertex(name = 'V_1622',
                particles = [ P.AQ, P.WQ__minus__, P.Hh, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_175})

V_1623 = Vertex(name = 'V_1623',
                particles = [ P.AQ, P.WQ__minus__, P.HhQ, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_175})

V_1624 = Vertex(name = 'V_1624',
                particles = [ P.AQ, P.WQ__minus__, P.Hh, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_175})

V_1625 = Vertex(name = 'V_1625',
                particles = [ P.AQ, P.WQ__minus__, P.HhQ, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_175})

V_1626 = Vertex(name = 'V_1626',
                particles = [ P.AQ, P.WQ__minus__, P.G__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_324})

V_1627 = Vertex(name = 'V_1627',
                particles = [ P.AQ, P.WQ__minus__, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_324})

V_1628 = Vertex(name = 'V_1628',
                particles = [ P.AQ, P.WQ__minus__, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_321})

V_1629 = Vertex(name = 'V_1629',
                particles = [ P.AQ, P.WQ__minus__, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_321})

V_1630 = Vertex(name = 'V_1630',
                particles = [ P.WQ__minus__, P.G0, P.G__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_184})

V_1631 = Vertex(name = 'V_1631',
                particles = [ P.WQ__minus__, P.G__plus__, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_179})

V_1632 = Vertex(name = 'V_1632',
                particles = [ P.WQ__minus__, P.G__plus__, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_173})

V_1633 = Vertex(name = 'V_1633',
                particles = [ P.WQ__minus__, P.GQ0, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_184})

V_1634 = Vertex(name = 'V_1634',
                particles = [ P.WQ__minus__, P.GQ__plus__, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_179})

V_1635 = Vertex(name = 'V_1635',
                particles = [ P.WQ__minus__, P.GQ__plus__, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_173})

V_1636 = Vertex(name = 'V_1636',
                particles = [ P.WQ__minus__, P.Ha, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_184})

V_1637 = Vertex(name = 'V_1637',
                particles = [ P.WQ__minus__, P.HaQ, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_184})

V_1638 = Vertex(name = 'V_1638',
                particles = [ P.WQ__minus__, P.Hh, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_173})

V_1639 = Vertex(name = 'V_1639',
                particles = [ P.WQ__minus__, P.HhQ, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_173})

V_1640 = Vertex(name = 'V_1640',
                particles = [ P.WQ__minus__, P.Hl, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_178})

V_1641 = Vertex(name = 'V_1641',
                particles = [ P.WQ__minus__, P.HlQ, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_178})

V_1642 = Vertex(name = 'V_1642',
                particles = [ P.A, P.WQ__minus__, P.WQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVV2 ],
                couplings = {(0,0):C.GC_3})

V_1643 = Vertex(name = 'V_1643',
                particles = [ P.AQ, P.WQ__minus__, P.WQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVV1 ],
                couplings = {(0,0):C.GC_3})

V_1644 = Vertex(name = 'V_1644',
                particles = [ P.ghWp, P.ghWm__tilde__, P.W__minus__, P.WQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_189})

V_1645 = Vertex(name = 'V_1645',
                particles = [ P.ghA, P.ghA__tilde__, P.W__plus__, P.WQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_8})

V_1646 = Vertex(name = 'V_1646',
                particles = [ P.W__plus__, P.WQ__minus__, P.G0, P.G0 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_100})

V_1647 = Vertex(name = 'V_1647',
                particles = [ P.ghWm, P.ghWm__tilde__, P.W__plus__, P.WQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_190})

V_1648 = Vertex(name = 'V_1648',
                particles = [ P.ghZ, P.ghZ__tilde__, P.W__plus__, P.WQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_194})

V_1649 = Vertex(name = 'V_1649',
                particles = [ P.W__plus__, P.WQ__minus__, P.G__minus__, P.G__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_100})

V_1650 = Vertex(name = 'V_1650',
                particles = [ P.W__plus__, P.WQ__minus__, P.G0, P.GQ0 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_100})

V_1651 = Vertex(name = 'V_1651',
                particles = [ P.W__plus__, P.WQ__minus__, P.GQ0, P.GQ0 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_100})

V_1652 = Vertex(name = 'V_1652',
                particles = [ P.W__plus__, P.WQ__minus__, P.G__minus__, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_101})

V_1653 = Vertex(name = 'V_1653',
                particles = [ P.W__plus__, P.WQ__minus__, P.GQ__minus__, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_100})

V_1654 = Vertex(name = 'V_1654',
                particles = [ P.W__plus__, P.WQ__minus__, P.Ha, P.Ha ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_100})

V_1655 = Vertex(name = 'V_1655',
                particles = [ P.W__plus__, P.WQ__minus__, P.Ha, P.HaQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_100})

V_1656 = Vertex(name = 'V_1656',
                particles = [ P.W__plus__, P.WQ__minus__, P.HaQ, P.HaQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_100})

V_1657 = Vertex(name = 'V_1657',
                particles = [ P.W__plus__, P.WQ__minus__, P.GQ0, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_90})

V_1658 = Vertex(name = 'V_1658',
                particles = [ P.W__plus__, P.WQ__minus__, P.Hh, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_77})

V_1659 = Vertex(name = 'V_1659',
                particles = [ P.W__plus__, P.WQ__minus__, P.G0, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_93})

V_1660 = Vertex(name = 'V_1660',
                particles = [ P.W__plus__, P.WQ__minus__, P.Hh, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_77})

V_1661 = Vertex(name = 'V_1661',
                particles = [ P.W__plus__, P.WQ__minus__, P.HhQ, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_77})

V_1662 = Vertex(name = 'V_1662',
                particles = [ P.W__plus__, P.WQ__minus__, P.HaQ, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_90})

V_1663 = Vertex(name = 'V_1663',
                particles = [ P.W__plus__, P.WQ__minus__, P.Hl, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_77})

V_1664 = Vertex(name = 'V_1664',
                particles = [ P.W__plus__, P.WQ__minus__, P.Ha, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_93})

V_1665 = Vertex(name = 'V_1665',
                particles = [ P.W__plus__, P.WQ__minus__, P.Hl, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_77})

V_1666 = Vertex(name = 'V_1666',
                particles = [ P.W__plus__, P.WQ__minus__, P.HlQ, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_77})

V_1667 = Vertex(name = 'V_1667',
                particles = [ P.W__plus__, P.WQ__minus__, P.H__minus__, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_100})

V_1668 = Vertex(name = 'V_1668',
                particles = [ P.W__plus__, P.WQ__minus__, P.H__minus__, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_101})

V_1669 = Vertex(name = 'V_1669',
                particles = [ P.W__plus__, P.WQ__minus__, P.HQ__minus__, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_100})

V_1670 = Vertex(name = 'V_1670',
                particles = [ P.W__plus__, P.WQ__minus__, P.HaQ, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_81})

V_1671 = Vertex(name = 'V_1671',
                particles = [ P.W__plus__, P.WQ__minus__, P.Ha, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_78})

V_1672 = Vertex(name = 'V_1672',
                particles = [ P.W__plus__, P.WQ__minus__, P.GQ0, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_78})

V_1673 = Vertex(name = 'V_1673',
                particles = [ P.W__plus__, P.WQ__minus__, P.G0, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_81})

V_1674 = Vertex(name = 'V_1674',
                particles = [ P.ghA, P.ghZ__tilde__, P.W__plus__, P.WQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_202})

V_1675 = Vertex(name = 'V_1675',
                particles = [ P.ghZ, P.ghA__tilde__, P.W__plus__, P.WQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_202})

V_1676 = Vertex(name = 'V_1676',
                particles = [ P.W__plus__, P.WQ__minus__, P.GQ0 ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_273})

V_1677 = Vertex(name = 'V_1677',
                particles = [ P.W__plus__, P.WQ__minus__, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_272})

V_1678 = Vertex(name = 'V_1678',
                particles = [ P.W__plus__, P.WQ__minus__, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_272})

V_1679 = Vertex(name = 'V_1679',
                particles = [ P.W__plus__, P.WQ__minus__, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_261})

V_1680 = Vertex(name = 'V_1680',
                particles = [ P.W__plus__, P.WQ__minus__, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_261})

V_1681 = Vertex(name = 'V_1681',
                particles = [ P.W__plus__, P.WQ__minus__, P.HaQ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_262})

V_1682 = Vertex(name = 'V_1682',
                particles = [ P.A, P.A, P.W__plus__, P.WQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVVV5 ],
                couplings = {(0,0):C.GC_8})

V_1683 = Vertex(name = 'V_1683',
                particles = [ P.A, P.AQ, P.W__plus__, P.WQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVVV9 ],
                couplings = {(0,0):C.GC_10})

V_1684 = Vertex(name = 'V_1684',
                particles = [ P.AQ, P.AQ, P.W__plus__, P.WQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVVV5 ],
                couplings = {(0,0):C.GC_8})

V_1685 = Vertex(name = 'V_1685',
                particles = [ P.W__plus__, P.WQ__minus__, P.ZQ ],
                color = [ '1' ],
                lorentz = [ L.VVV2 ],
                couplings = {(0,0):C.GC_198})

V_1686 = Vertex(name = 'V_1686',
                particles = [ P.W__plus__, P.WQ__minus__, P.Z ],
                color = [ '1' ],
                lorentz = [ L.VVV1 ],
                couplings = {(0,0):C.GC_198})

V_1687 = Vertex(name = 'V_1687',
                particles = [ P.W__minus__, P.W__plus__, P.W__plus__, P.WQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVVV11 ],
                couplings = {(0,0):C.GC_192})

V_1688 = Vertex(name = 'V_1688',
                particles = [ P.W__plus__, P.W__plus__, P.WQ__minus__, P.WQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVVV4 ],
                couplings = {(0,0):C.GC_192})

V_1689 = Vertex(name = 'V_1689',
                particles = [ P.ghWm, P.ghA__tilde__, P.A, P.WQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_7})

V_1690 = Vertex(name = 'V_1690',
                particles = [ P.ghWm, P.ghZ__tilde__, P.A, P.WQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_203})

V_1691 = Vertex(name = 'V_1691',
                particles = [ P.A, P.WQ__plus__, P.G0, P.G__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_186})

V_1692 = Vertex(name = 'V_1692',
                particles = [ P.A, P.WQ__plus__, P.G__minus__, P.GQ0 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_185})

V_1693 = Vertex(name = 'V_1693',
                particles = [ P.A, P.WQ__plus__, P.GQ0, P.GQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_186})

V_1694 = Vertex(name = 'V_1694',
                particles = [ P.A, P.WQ__plus__, P.G__minus__, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_180})

V_1695 = Vertex(name = 'V_1695',
                particles = [ P.A, P.WQ__plus__, P.G__minus__, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_182})

V_1696 = Vertex(name = 'V_1696',
                particles = [ P.A, P.WQ__plus__, P.GQ__minus__, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_180})

V_1697 = Vertex(name = 'V_1697',
                particles = [ P.A, P.WQ__plus__, P.Ha, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_186})

V_1698 = Vertex(name = 'V_1698',
                particles = [ P.A, P.WQ__plus__, P.HaQ, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_185})

V_1699 = Vertex(name = 'V_1699',
                particles = [ P.A, P.WQ__plus__, P.Hl, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_180})

V_1700 = Vertex(name = 'V_1700',
                particles = [ P.A, P.WQ__plus__, P.HlQ, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_182})

V_1701 = Vertex(name = 'V_1701',
                particles = [ P.A, P.WQ__plus__, P.HaQ, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_186})

V_1702 = Vertex(name = 'V_1702',
                particles = [ P.A, P.WQ__plus__, P.HlQ, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_180})

V_1703 = Vertex(name = 'V_1703',
                particles = [ P.A, P.WQ__plus__, P.G__minus__, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_174})

V_1704 = Vertex(name = 'V_1704',
                particles = [ P.A, P.WQ__plus__, P.G__minus__, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_176})

V_1705 = Vertex(name = 'V_1705',
                particles = [ P.A, P.WQ__plus__, P.GQ__minus__, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_174})

V_1706 = Vertex(name = 'V_1706',
                particles = [ P.A, P.WQ__plus__, P.Hh, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_175})

V_1707 = Vertex(name = 'V_1707',
                particles = [ P.A, P.WQ__plus__, P.HhQ, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_177})

V_1708 = Vertex(name = 'V_1708',
                particles = [ P.A, P.WQ__plus__, P.HhQ, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_175})

V_1709 = Vertex(name = 'V_1709',
                particles = [ P.A, P.WQ__plus__, P.G__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_324})

V_1710 = Vertex(name = 'V_1710',
                particles = [ P.A, P.WQ__plus__, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_321})

V_1711 = Vertex(name = 'V_1711',
                particles = [ P.AQ, P.WQ__plus__, P.G0, P.G__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_186})

V_1712 = Vertex(name = 'V_1712',
                particles = [ P.AQ, P.WQ__plus__, P.G__minus__, P.GQ0 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_186})

V_1713 = Vertex(name = 'V_1713',
                particles = [ P.AQ, P.WQ__plus__, P.G0, P.GQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_186})

V_1714 = Vertex(name = 'V_1714',
                particles = [ P.AQ, P.WQ__plus__, P.GQ0, P.GQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_186})

V_1715 = Vertex(name = 'V_1715',
                particles = [ P.AQ, P.WQ__plus__, P.G__minus__, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_180})

V_1716 = Vertex(name = 'V_1716',
                particles = [ P.AQ, P.WQ__plus__, P.GQ__minus__, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_180})

V_1717 = Vertex(name = 'V_1717',
                particles = [ P.AQ, P.WQ__plus__, P.G__minus__, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_180})

V_1718 = Vertex(name = 'V_1718',
                particles = [ P.AQ, P.WQ__plus__, P.GQ__minus__, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_180})

V_1719 = Vertex(name = 'V_1719',
                particles = [ P.AQ, P.WQ__plus__, P.Ha, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_186})

V_1720 = Vertex(name = 'V_1720',
                particles = [ P.AQ, P.WQ__plus__, P.HaQ, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_186})

V_1721 = Vertex(name = 'V_1721',
                particles = [ P.AQ, P.WQ__plus__, P.Hl, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_180})

V_1722 = Vertex(name = 'V_1722',
                particles = [ P.AQ, P.WQ__plus__, P.HlQ, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_180})

V_1723 = Vertex(name = 'V_1723',
                particles = [ P.AQ, P.WQ__plus__, P.Ha, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_186})

V_1724 = Vertex(name = 'V_1724',
                particles = [ P.AQ, P.WQ__plus__, P.HaQ, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_186})

V_1725 = Vertex(name = 'V_1725',
                particles = [ P.AQ, P.WQ__plus__, P.Hl, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_180})

V_1726 = Vertex(name = 'V_1726',
                particles = [ P.AQ, P.WQ__plus__, P.HlQ, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_180})

V_1727 = Vertex(name = 'V_1727',
                particles = [ P.AQ, P.WQ__plus__, P.G__minus__, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_174})

V_1728 = Vertex(name = 'V_1728',
                particles = [ P.AQ, P.WQ__plus__, P.GQ__minus__, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_174})

V_1729 = Vertex(name = 'V_1729',
                particles = [ P.AQ, P.WQ__plus__, P.G__minus__, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_174})

V_1730 = Vertex(name = 'V_1730',
                particles = [ P.AQ, P.WQ__plus__, P.GQ__minus__, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_174})

V_1731 = Vertex(name = 'V_1731',
                particles = [ P.AQ, P.WQ__plus__, P.Hh, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_175})

V_1732 = Vertex(name = 'V_1732',
                particles = [ P.AQ, P.WQ__plus__, P.HhQ, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_175})

V_1733 = Vertex(name = 'V_1733',
                particles = [ P.AQ, P.WQ__plus__, P.Hh, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_175})

V_1734 = Vertex(name = 'V_1734',
                particles = [ P.AQ, P.WQ__plus__, P.HhQ, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_175})

V_1735 = Vertex(name = 'V_1735',
                particles = [ P.AQ, P.WQ__plus__, P.G__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_324})

V_1736 = Vertex(name = 'V_1736',
                particles = [ P.AQ, P.WQ__plus__, P.GQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_324})

V_1737 = Vertex(name = 'V_1737',
                particles = [ P.AQ, P.WQ__plus__, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_321})

V_1738 = Vertex(name = 'V_1738',
                particles = [ P.AQ, P.WQ__plus__, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_321})

V_1739 = Vertex(name = 'V_1739',
                particles = [ P.WQ__plus__, P.G0, P.G__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_184})

V_1740 = Vertex(name = 'V_1740',
                particles = [ P.WQ__plus__, P.G__minus__, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_178})

V_1741 = Vertex(name = 'V_1741',
                particles = [ P.WQ__plus__, P.G__minus__, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_172})

V_1742 = Vertex(name = 'V_1742',
                particles = [ P.WQ__plus__, P.GQ0, P.GQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_184})

V_1743 = Vertex(name = 'V_1743',
                particles = [ P.WQ__plus__, P.GQ__minus__, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_178})

V_1744 = Vertex(name = 'V_1744',
                particles = [ P.WQ__plus__, P.GQ__minus__, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_172})

V_1745 = Vertex(name = 'V_1745',
                particles = [ P.WQ__plus__, P.Ha, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_184})

V_1746 = Vertex(name = 'V_1746',
                particles = [ P.WQ__plus__, P.HaQ, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_184})

V_1747 = Vertex(name = 'V_1747',
                particles = [ P.WQ__plus__, P.Hh, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_172})

V_1748 = Vertex(name = 'V_1748',
                particles = [ P.WQ__plus__, P.HhQ, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_172})

V_1749 = Vertex(name = 'V_1749',
                particles = [ P.WQ__plus__, P.Hl, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_179})

V_1750 = Vertex(name = 'V_1750',
                particles = [ P.WQ__plus__, P.HlQ, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_179})

V_1751 = Vertex(name = 'V_1751',
                particles = [ P.ghA, P.ghA__tilde__, P.W__minus__, P.WQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_8})

V_1752 = Vertex(name = 'V_1752',
                particles = [ P.W__minus__, P.WQ__plus__, P.G0, P.G0 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_100})

V_1753 = Vertex(name = 'V_1753',
                particles = [ P.ghWp, P.ghWp__tilde__, P.W__minus__, P.WQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_190})

V_1754 = Vertex(name = 'V_1754',
                particles = [ P.ghZ, P.ghZ__tilde__, P.W__minus__, P.WQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_194})

V_1755 = Vertex(name = 'V_1755',
                particles = [ P.W__minus__, P.WQ__plus__, P.G__minus__, P.G__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_100})

V_1756 = Vertex(name = 'V_1756',
                particles = [ P.W__minus__, P.WQ__plus__, P.G0, P.GQ0 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_100})

V_1757 = Vertex(name = 'V_1757',
                particles = [ P.W__minus__, P.WQ__plus__, P.GQ0, P.GQ0 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_100})

V_1758 = Vertex(name = 'V_1758',
                particles = [ P.W__minus__, P.WQ__plus__, P.G__plus__, P.GQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_101})

V_1759 = Vertex(name = 'V_1759',
                particles = [ P.W__minus__, P.WQ__plus__, P.GQ__minus__, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_100})

V_1760 = Vertex(name = 'V_1760',
                particles = [ P.W__minus__, P.WQ__plus__, P.Ha, P.Ha ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_100})

V_1761 = Vertex(name = 'V_1761',
                particles = [ P.W__minus__, P.WQ__plus__, P.Ha, P.HaQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_100})

V_1762 = Vertex(name = 'V_1762',
                particles = [ P.W__minus__, P.WQ__plus__, P.HaQ, P.HaQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_100})

V_1763 = Vertex(name = 'V_1763',
                particles = [ P.W__minus__, P.WQ__plus__, P.GQ0, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_93})

V_1764 = Vertex(name = 'V_1764',
                particles = [ P.W__minus__, P.WQ__plus__, P.Hh, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_77})

V_1765 = Vertex(name = 'V_1765',
                particles = [ P.W__minus__, P.WQ__plus__, P.G0, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_90})

V_1766 = Vertex(name = 'V_1766',
                particles = [ P.W__minus__, P.WQ__plus__, P.Hh, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_77})

V_1767 = Vertex(name = 'V_1767',
                particles = [ P.W__minus__, P.WQ__plus__, P.HhQ, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_77})

V_1768 = Vertex(name = 'V_1768',
                particles = [ P.W__minus__, P.WQ__plus__, P.HaQ, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_93})

V_1769 = Vertex(name = 'V_1769',
                particles = [ P.W__minus__, P.WQ__plus__, P.Hl, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_77})

V_1770 = Vertex(name = 'V_1770',
                particles = [ P.W__minus__, P.WQ__plus__, P.Ha, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_90})

V_1771 = Vertex(name = 'V_1771',
                particles = [ P.W__minus__, P.WQ__plus__, P.Hl, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_77})

V_1772 = Vertex(name = 'V_1772',
                particles = [ P.W__minus__, P.WQ__plus__, P.HlQ, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_77})

V_1773 = Vertex(name = 'V_1773',
                particles = [ P.W__minus__, P.WQ__plus__, P.H__minus__, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_100})

V_1774 = Vertex(name = 'V_1774',
                particles = [ P.W__minus__, P.WQ__plus__, P.H__plus__, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_101})

V_1775 = Vertex(name = 'V_1775',
                particles = [ P.W__minus__, P.WQ__plus__, P.HQ__minus__, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_100})

V_1776 = Vertex(name = 'V_1776',
                particles = [ P.W__minus__, P.WQ__plus__, P.HaQ, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_78})

V_1777 = Vertex(name = 'V_1777',
                particles = [ P.W__minus__, P.WQ__plus__, P.Ha, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_81})

V_1778 = Vertex(name = 'V_1778',
                particles = [ P.W__minus__, P.WQ__plus__, P.GQ0, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_81})

V_1779 = Vertex(name = 'V_1779',
                particles = [ P.W__minus__, P.WQ__plus__, P.G0, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_78})

V_1780 = Vertex(name = 'V_1780',
                particles = [ P.ghA, P.ghZ__tilde__, P.W__minus__, P.WQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_202})

V_1781 = Vertex(name = 'V_1781',
                particles = [ P.ghZ, P.ghA__tilde__, P.W__minus__, P.WQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_202})

V_1782 = Vertex(name = 'V_1782',
                particles = [ P.W__minus__, P.WQ__plus__, P.GQ0 ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_276})

V_1783 = Vertex(name = 'V_1783',
                particles = [ P.W__minus__, P.WQ__plus__, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_272})

V_1784 = Vertex(name = 'V_1784',
                particles = [ P.W__minus__, P.WQ__plus__, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_272})

V_1785 = Vertex(name = 'V_1785',
                particles = [ P.W__minus__, P.WQ__plus__, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_261})

V_1786 = Vertex(name = 'V_1786',
                particles = [ P.W__minus__, P.WQ__plus__, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_261})

V_1787 = Vertex(name = 'V_1787',
                particles = [ P.W__minus__, P.WQ__plus__, P.HaQ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_265})

V_1788 = Vertex(name = 'V_1788',
                particles = [ P.A, P.A, P.W__minus__, P.WQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVVV5 ],
                couplings = {(0,0):C.GC_8})

V_1789 = Vertex(name = 'V_1789',
                particles = [ P.A, P.AQ, P.W__minus__, P.WQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVVV9 ],
                couplings = {(0,0):C.GC_10})

V_1790 = Vertex(name = 'V_1790',
                particles = [ P.AQ, P.AQ, P.W__minus__, P.WQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVVV5 ],
                couplings = {(0,0):C.GC_8})

V_1791 = Vertex(name = 'V_1791',
                particles = [ P.W__minus__, P.WQ__plus__, P.ZQ ],
                color = [ '1' ],
                lorentz = [ L.VVV2 ],
                couplings = {(0,0):C.GC_199})

V_1792 = Vertex(name = 'V_1792',
                particles = [ P.W__minus__, P.WQ__plus__, P.Z ],
                color = [ '1' ],
                lorentz = [ L.VVV1 ],
                couplings = {(0,0):C.GC_199})

V_1793 = Vertex(name = 'V_1793',
                particles = [ P.ghWm, P.ghWp__tilde__, P.W__plus__, P.WQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_189})

V_1794 = Vertex(name = 'V_1794',
                particles = [ P.W__minus__, P.W__minus__, P.W__plus__, P.WQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVVV5 ],
                couplings = {(0,0):C.GC_189})

V_1795 = Vertex(name = 'V_1795',
                particles = [ P.WQ__minus__, P.WQ__plus__, P.G0, P.G0 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_100})

V_1796 = Vertex(name = 'V_1796',
                particles = [ P.WQ__minus__, P.WQ__plus__, P.G__minus__, P.G__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_100})

V_1797 = Vertex(name = 'V_1797',
                particles = [ P.WQ__minus__, P.WQ__plus__, P.G0, P.GQ0 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_100})

V_1798 = Vertex(name = 'V_1798',
                particles = [ P.WQ__minus__, P.WQ__plus__, P.GQ0, P.GQ0 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_100})

V_1799 = Vertex(name = 'V_1799',
                particles = [ P.WQ__minus__, P.WQ__plus__, P.G__plus__, P.GQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_100})

V_1800 = Vertex(name = 'V_1800',
                particles = [ P.WQ__minus__, P.WQ__plus__, P.G__minus__, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_100})

V_1801 = Vertex(name = 'V_1801',
                particles = [ P.WQ__minus__, P.WQ__plus__, P.GQ__minus__, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_100})

V_1802 = Vertex(name = 'V_1802',
                particles = [ P.WQ__minus__, P.WQ__plus__, P.Ha, P.Ha ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_100})

V_1803 = Vertex(name = 'V_1803',
                particles = [ P.WQ__minus__, P.WQ__plus__, P.Ha, P.HaQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_100})

V_1804 = Vertex(name = 'V_1804',
                particles = [ P.WQ__minus__, P.WQ__plus__, P.HaQ, P.HaQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_100})

V_1805 = Vertex(name = 'V_1805',
                particles = [ P.WQ__minus__, P.WQ__plus__, P.Hh, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_77})

V_1806 = Vertex(name = 'V_1806',
                particles = [ P.WQ__minus__, P.WQ__plus__, P.Hh, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_77})

V_1807 = Vertex(name = 'V_1807',
                particles = [ P.WQ__minus__, P.WQ__plus__, P.HhQ, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_77})

V_1808 = Vertex(name = 'V_1808',
                particles = [ P.WQ__minus__, P.WQ__plus__, P.Hl, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_77})

V_1809 = Vertex(name = 'V_1809',
                particles = [ P.WQ__minus__, P.WQ__plus__, P.Hl, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_77})

V_1810 = Vertex(name = 'V_1810',
                particles = [ P.WQ__minus__, P.WQ__plus__, P.HlQ, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_77})

V_1811 = Vertex(name = 'V_1811',
                particles = [ P.WQ__minus__, P.WQ__plus__, P.H__minus__, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_100})

V_1812 = Vertex(name = 'V_1812',
                particles = [ P.WQ__minus__, P.WQ__plus__, P.H__plus__, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_100})

V_1813 = Vertex(name = 'V_1813',
                particles = [ P.WQ__minus__, P.WQ__plus__, P.H__minus__, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_100})

V_1814 = Vertex(name = 'V_1814',
                particles = [ P.WQ__minus__, P.WQ__plus__, P.HQ__minus__, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_100})

V_1815 = Vertex(name = 'V_1815',
                particles = [ P.WQ__minus__, P.WQ__plus__, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_272})

V_1816 = Vertex(name = 'V_1816',
                particles = [ P.WQ__minus__, P.WQ__plus__, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_272})

V_1817 = Vertex(name = 'V_1817',
                particles = [ P.WQ__minus__, P.WQ__plus__, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_261})

V_1818 = Vertex(name = 'V_1818',
                particles = [ P.WQ__minus__, P.WQ__plus__, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_261})

V_1819 = Vertex(name = 'V_1819',
                particles = [ P.A, P.A, P.WQ__minus__, P.WQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVVV4 ],
                couplings = {(0,0):C.GC_9})

V_1820 = Vertex(name = 'V_1820',
                particles = [ P.A, P.AQ, P.WQ__minus__, P.WQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVVV5 ],
                couplings = {(0,0):C.GC_8})

V_1821 = Vertex(name = 'V_1821',
                particles = [ P.AQ, P.AQ, P.WQ__minus__, P.WQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVVV5 ],
                couplings = {(0,0):C.GC_8})

V_1822 = Vertex(name = 'V_1822',
                particles = [ P.W__minus__, P.W__plus__, P.WQ__minus__, P.WQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVVV12 ],
                couplings = {(0,0):C.GC_191})

V_1823 = Vertex(name = 'V_1823',
                particles = [ P.WQ__minus__, P.WQ__plus__, P.Z ],
                color = [ '1' ],
                lorentz = [ L.VVV4 ],
                couplings = {(0,0):C.GC_199})

V_1824 = Vertex(name = 'V_1824',
                particles = [ P.WQ__minus__, P.WQ__plus__, P.ZQ ],
                color = [ '1' ],
                lorentz = [ L.VVV1 ],
                couplings = {(0,0):C.GC_199})

V_1825 = Vertex(name = 'V_1825',
                particles = [ P.W__plus__, P.WQ__minus__, P.WQ__minus__, P.WQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVVV11 ],
                couplings = {(0,0):C.GC_192})

V_1826 = Vertex(name = 'V_1826',
                particles = [ P.W__minus__, P.W__minus__, P.WQ__plus__, P.WQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVVV4 ],
                couplings = {(0,0):C.GC_192})

V_1827 = Vertex(name = 'V_1827',
                particles = [ P.W__minus__, P.WQ__minus__, P.WQ__plus__, P.WQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVVV5 ],
                couplings = {(0,0):C.GC_189})

V_1828 = Vertex(name = 'V_1828',
                particles = [ P.WQ__minus__, P.WQ__minus__, P.WQ__plus__, P.WQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVVV5 ],
                couplings = {(0,0):C.GC_189})

V_1829 = Vertex(name = 'V_1829',
                particles = [ P.e__plus__, P.nu_e, P.G__minus__ ],
                color = [ '1' ],
                lorentz = [ L.FFS3 ],
                couplings = {(0,0):C.GC_355})

V_1830 = Vertex(name = 'V_1830',
                particles = [ P.mu__plus__, P.nu_mu, P.G__minus__ ],
                color = [ '1' ],
                lorentz = [ L.FFS3 ],
                couplings = {(0,0):C.GC_361})

V_1831 = Vertex(name = 'V_1831',
                particles = [ P.tau__plus__, P.nu_tau, P.G__minus__ ],
                color = [ '1' ],
                lorentz = [ L.FFS3 ],
                couplings = {(0,0):C.GC_379})

V_1832 = Vertex(name = 'V_1832',
                particles = [ P.e__plus__, P.nu_e, P.GQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.FFS3 ],
                couplings = {(0,0):C.GC_355})

V_1833 = Vertex(name = 'V_1833',
                particles = [ P.mu__plus__, P.nu_mu, P.GQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.FFS3 ],
                couplings = {(0,0):C.GC_361})

V_1834 = Vertex(name = 'V_1834',
                particles = [ P.tau__plus__, P.nu_tau, P.GQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.FFS3 ],
                couplings = {(0,0):C.GC_379})

V_1835 = Vertex(name = 'V_1835',
                particles = [ P.e__plus__, P.nu_e, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.FFS3 ],
                couplings = {(0,0):C.GC_352})

V_1836 = Vertex(name = 'V_1836',
                particles = [ P.mu__plus__, P.nu_mu, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.FFS3 ],
                couplings = {(0,0):C.GC_358})

V_1837 = Vertex(name = 'V_1837',
                particles = [ P.tau__plus__, P.nu_tau, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.FFS3 ],
                couplings = {(0,0):C.GC_376})

V_1838 = Vertex(name = 'V_1838',
                particles = [ P.e__plus__, P.nu_e, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.FFS3 ],
                couplings = {(0,0):C.GC_352})

V_1839 = Vertex(name = 'V_1839',
                particles = [ P.mu__plus__, P.nu_mu, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.FFS3 ],
                couplings = {(0,0):C.GC_358})

V_1840 = Vertex(name = 'V_1840',
                particles = [ P.tau__plus__, P.nu_tau, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.FFS3 ],
                couplings = {(0,0):C.GC_376})

V_1841 = Vertex(name = 'V_1841',
                particles = [ P.ghWm, P.ghWm__tilde__, P.A, P.Z ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_204})

V_1842 = Vertex(name = 'V_1842',
                particles = [ P.ghWp, P.ghWp__tilde__, P.A, P.Z ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_204})

V_1843 = Vertex(name = 'V_1843',
                particles = [ P.A, P.Z, P.G__minus__, P.G__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_221})

V_1844 = Vertex(name = 'V_1844',
                particles = [ P.A, P.Z, P.G__plus__, P.GQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_221})

V_1845 = Vertex(name = 'V_1845',
                particles = [ P.A, P.Z, P.G__minus__, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_221})

V_1846 = Vertex(name = 'V_1846',
                particles = [ P.A, P.Z, P.GQ__minus__, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_221})

V_1847 = Vertex(name = 'V_1847',
                particles = [ P.A, P.Z, P.H__minus__, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_221})

V_1848 = Vertex(name = 'V_1848',
                particles = [ P.A, P.Z, P.H__plus__, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_221})

V_1849 = Vertex(name = 'V_1849',
                particles = [ P.A, P.Z, P.H__minus__, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_221})

V_1850 = Vertex(name = 'V_1850',
                particles = [ P.A, P.Z, P.HQ__minus__, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_221})

V_1851 = Vertex(name = 'V_1851',
                particles = [ P.ghWm, P.ghWm__tilde__, P.AQ, P.Z ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_202})

V_1852 = Vertex(name = 'V_1852',
                particles = [ P.ghWp, P.ghWp__tilde__, P.AQ, P.Z ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_202})

V_1853 = Vertex(name = 'V_1853',
                particles = [ P.AQ, P.Z, P.G__minus__, P.G__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_221})

V_1854 = Vertex(name = 'V_1854',
                particles = [ P.AQ, P.Z, P.G__plus__, P.GQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_221})

V_1855 = Vertex(name = 'V_1855',
                particles = [ P.AQ, P.Z, P.G__minus__, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_221})

V_1856 = Vertex(name = 'V_1856',
                particles = [ P.AQ, P.Z, P.GQ__minus__, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_221})

V_1857 = Vertex(name = 'V_1857',
                particles = [ P.AQ, P.Z, P.H__minus__, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_221})

V_1858 = Vertex(name = 'V_1858',
                particles = [ P.AQ, P.Z, P.H__plus__, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_221})

V_1859 = Vertex(name = 'V_1859',
                particles = [ P.AQ, P.Z, P.H__minus__, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_221})

V_1860 = Vertex(name = 'V_1860',
                particles = [ P.AQ, P.Z, P.HQ__minus__, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_221})

V_1861 = Vertex(name = 'V_1861',
                particles = [ P.Z, P.G0, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_215})

V_1862 = Vertex(name = 'V_1862',
                particles = [ P.Z, P.G0, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_215})

V_1863 = Vertex(name = 'V_1863',
                particles = [ P.Z, P.G0, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_213})

V_1864 = Vertex(name = 'V_1864',
                particles = [ P.Z, P.G0, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_213})

V_1865 = Vertex(name = 'V_1865',
                particles = [ P.Z, P.G__minus__, P.G__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_218})

V_1866 = Vertex(name = 'V_1866',
                particles = [ P.Z, P.G__minus__, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_218})

V_1867 = Vertex(name = 'V_1867',
                particles = [ P.Z, P.G__plus__, P.GQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_217})

V_1868 = Vertex(name = 'V_1868',
                particles = [ P.Z, P.GQ0, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_215})

V_1869 = Vertex(name = 'V_1869',
                particles = [ P.Z, P.GQ0, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_215})

V_1870 = Vertex(name = 'V_1870',
                particles = [ P.Z, P.GQ0, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_213})

V_1871 = Vertex(name = 'V_1871',
                particles = [ P.Z, P.GQ0, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_213})

V_1872 = Vertex(name = 'V_1872',
                particles = [ P.Z, P.GQ__minus__, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_218})

V_1873 = Vertex(name = 'V_1873',
                particles = [ P.Z, P.Ha, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_215})

V_1874 = Vertex(name = 'V_1874',
                particles = [ P.Z, P.Ha, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_215})

V_1875 = Vertex(name = 'V_1875',
                particles = [ P.Z, P.Ha, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_214})

V_1876 = Vertex(name = 'V_1876',
                particles = [ P.Z, P.Ha, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_214})

V_1877 = Vertex(name = 'V_1877',
                particles = [ P.Z, P.HaQ, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_215})

V_1878 = Vertex(name = 'V_1878',
                particles = [ P.Z, P.HaQ, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_215})

V_1879 = Vertex(name = 'V_1879',
                particles = [ P.Z, P.HaQ, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_214})

V_1880 = Vertex(name = 'V_1880',
                particles = [ P.Z, P.HaQ, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_214})

V_1881 = Vertex(name = 'V_1881',
                particles = [ P.Z, P.H__minus__, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_218})

V_1882 = Vertex(name = 'V_1882',
                particles = [ P.Z, P.H__minus__, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_218})

V_1883 = Vertex(name = 'V_1883',
                particles = [ P.Z, P.H__plus__, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_217})

V_1884 = Vertex(name = 'V_1884',
                particles = [ P.Z, P.HQ__minus__, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS2 ],
                couplings = {(0,0):C.GC_218})

V_1885 = Vertex(name = 'V_1885',
                particles = [ P.W__minus__, P.Z, P.G0, P.G__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_31})

V_1886 = Vertex(name = 'V_1886',
                particles = [ P.W__minus__, P.Z, P.G__plus__, P.GQ0 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_31})

V_1887 = Vertex(name = 'V_1887',
                particles = [ P.W__minus__, P.Z, P.G0, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_31})

V_1888 = Vertex(name = 'V_1888',
                particles = [ P.W__minus__, P.Z, P.GQ0, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_31})

V_1889 = Vertex(name = 'V_1889',
                particles = [ P.W__minus__, P.Z, P.G__plus__, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_23})

V_1890 = Vertex(name = 'V_1890',
                particles = [ P.W__minus__, P.Z, P.GQ__plus__, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_23})

V_1891 = Vertex(name = 'V_1891',
                particles = [ P.W__minus__, P.Z, P.G__plus__, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_23})

V_1892 = Vertex(name = 'V_1892',
                particles = [ P.W__minus__, P.Z, P.GQ__plus__, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_23})

V_1893 = Vertex(name = 'V_1893',
                particles = [ P.W__minus__, P.Z, P.Ha, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_31})

V_1894 = Vertex(name = 'V_1894',
                particles = [ P.W__minus__, P.Z, P.HaQ, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_31})

V_1895 = Vertex(name = 'V_1895',
                particles = [ P.W__minus__, P.Z, P.Hl, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_23})

V_1896 = Vertex(name = 'V_1896',
                particles = [ P.W__minus__, P.Z, P.HlQ, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_23})

V_1897 = Vertex(name = 'V_1897',
                particles = [ P.W__minus__, P.Z, P.Ha, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_31})

V_1898 = Vertex(name = 'V_1898',
                particles = [ P.W__minus__, P.Z, P.HaQ, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_31})

V_1899 = Vertex(name = 'V_1899',
                particles = [ P.W__minus__, P.Z, P.Hl, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_23})

V_1900 = Vertex(name = 'V_1900',
                particles = [ P.W__minus__, P.Z, P.HlQ, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_23})

V_1901 = Vertex(name = 'V_1901',
                particles = [ P.W__minus__, P.Z, P.G__plus__, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_21})

V_1902 = Vertex(name = 'V_1902',
                particles = [ P.W__minus__, P.Z, P.GQ__plus__, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_21})

V_1903 = Vertex(name = 'V_1903',
                particles = [ P.W__minus__, P.Z, P.G__plus__, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_21})

V_1904 = Vertex(name = 'V_1904',
                particles = [ P.W__minus__, P.Z, P.GQ__plus__, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_21})

V_1905 = Vertex(name = 'V_1905',
                particles = [ P.W__minus__, P.Z, P.Hh, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_22})

V_1906 = Vertex(name = 'V_1906',
                particles = [ P.W__minus__, P.Z, P.HhQ, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_22})

V_1907 = Vertex(name = 'V_1907',
                particles = [ P.W__minus__, P.Z, P.Hh, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_22})

V_1908 = Vertex(name = 'V_1908',
                particles = [ P.W__minus__, P.Z, P.HhQ, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_22})

V_1909 = Vertex(name = 'V_1909',
                particles = [ P.ghWp, P.ghZ__tilde__, P.W__minus__, P.Z ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_193})

V_1910 = Vertex(name = 'V_1910',
                particles = [ P.ghZ, P.ghWm__tilde__, P.W__minus__, P.Z ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_193})

V_1911 = Vertex(name = 'V_1911',
                particles = [ P.ghA, P.ghWm__tilde__, P.W__minus__, P.Z ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_203})

V_1912 = Vertex(name = 'V_1912',
                particles = [ P.ghWp, P.ghA__tilde__, P.W__minus__, P.Z ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_203})

V_1913 = Vertex(name = 'V_1913',
                particles = [ P.W__minus__, P.Z, P.G__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_237})

V_1914 = Vertex(name = 'V_1914',
                particles = [ P.W__minus__, P.Z, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_237})

V_1915 = Vertex(name = 'V_1915',
                particles = [ P.W__minus__, P.Z, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_231})

V_1916 = Vertex(name = 'V_1916',
                particles = [ P.W__minus__, P.Z, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_231})

V_1917 = Vertex(name = 'V_1917',
                particles = [ P.W__plus__, P.Z, P.G0, P.G__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_30})

V_1918 = Vertex(name = 'V_1918',
                particles = [ P.W__plus__, P.Z, P.G__minus__, P.GQ0 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_30})

V_1919 = Vertex(name = 'V_1919',
                particles = [ P.W__plus__, P.Z, P.G0, P.GQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_30})

V_1920 = Vertex(name = 'V_1920',
                particles = [ P.W__plus__, P.Z, P.GQ0, P.GQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_30})

V_1921 = Vertex(name = 'V_1921',
                particles = [ P.W__plus__, P.Z, P.G__minus__, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_23})

V_1922 = Vertex(name = 'V_1922',
                particles = [ P.W__plus__, P.Z, P.GQ__minus__, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_23})

V_1923 = Vertex(name = 'V_1923',
                particles = [ P.W__plus__, P.Z, P.G__minus__, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_23})

V_1924 = Vertex(name = 'V_1924',
                particles = [ P.W__plus__, P.Z, P.GQ__minus__, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_23})

V_1925 = Vertex(name = 'V_1925',
                particles = [ P.W__plus__, P.Z, P.Ha, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_30})

V_1926 = Vertex(name = 'V_1926',
                particles = [ P.W__plus__, P.Z, P.HaQ, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_30})

V_1927 = Vertex(name = 'V_1927',
                particles = [ P.W__plus__, P.Z, P.Hl, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_23})

V_1928 = Vertex(name = 'V_1928',
                particles = [ P.W__plus__, P.Z, P.HlQ, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_23})

V_1929 = Vertex(name = 'V_1929',
                particles = [ P.W__plus__, P.Z, P.Ha, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_30})

V_1930 = Vertex(name = 'V_1930',
                particles = [ P.W__plus__, P.Z, P.HaQ, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_30})

V_1931 = Vertex(name = 'V_1931',
                particles = [ P.W__plus__, P.Z, P.Hl, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_23})

V_1932 = Vertex(name = 'V_1932',
                particles = [ P.W__plus__, P.Z, P.HlQ, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_23})

V_1933 = Vertex(name = 'V_1933',
                particles = [ P.W__plus__, P.Z, P.G__minus__, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_21})

V_1934 = Vertex(name = 'V_1934',
                particles = [ P.W__plus__, P.Z, P.GQ__minus__, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_21})

V_1935 = Vertex(name = 'V_1935',
                particles = [ P.W__plus__, P.Z, P.G__minus__, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_21})

V_1936 = Vertex(name = 'V_1936',
                particles = [ P.W__plus__, P.Z, P.GQ__minus__, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_21})

V_1937 = Vertex(name = 'V_1937',
                particles = [ P.W__plus__, P.Z, P.Hh, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_22})

V_1938 = Vertex(name = 'V_1938',
                particles = [ P.W__plus__, P.Z, P.HhQ, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_22})

V_1939 = Vertex(name = 'V_1939',
                particles = [ P.W__plus__, P.Z, P.Hh, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_22})

V_1940 = Vertex(name = 'V_1940',
                particles = [ P.W__plus__, P.Z, P.HhQ, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_22})

V_1941 = Vertex(name = 'V_1941',
                particles = [ P.ghWm, P.ghZ__tilde__, P.W__plus__, P.Z ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_193})

V_1942 = Vertex(name = 'V_1942',
                particles = [ P.ghZ, P.ghWp__tilde__, P.W__plus__, P.Z ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_193})

V_1943 = Vertex(name = 'V_1943',
                particles = [ P.ghWm, P.ghA__tilde__, P.W__plus__, P.Z ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_203})

V_1944 = Vertex(name = 'V_1944',
                particles = [ P.ghA, P.ghWp__tilde__, P.W__plus__, P.Z ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_203})

V_1945 = Vertex(name = 'V_1945',
                particles = [ P.W__plus__, P.Z, P.G__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_237})

V_1946 = Vertex(name = 'V_1946',
                particles = [ P.W__plus__, P.Z, P.GQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_237})

V_1947 = Vertex(name = 'V_1947',
                particles = [ P.W__plus__, P.Z, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_231})

V_1948 = Vertex(name = 'V_1948',
                particles = [ P.W__plus__, P.Z, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_231})

V_1949 = Vertex(name = 'V_1949',
                particles = [ P.A, P.W__minus__, P.W__plus__, P.Z ],
                color = [ '1' ],
                lorentz = [ L.VVVV11 ],
                couplings = {(0,0):C.GC_205})

V_1950 = Vertex(name = 'V_1950',
                particles = [ P.AQ, P.W__minus__, P.W__plus__, P.Z ],
                color = [ '1' ],
                lorentz = [ L.VVVV11 ],
                couplings = {(0,0):C.GC_205})

V_1951 = Vertex(name = 'V_1951',
                particles = [ P.WQ__minus__, P.Z, P.G0, P.G__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_31})

V_1952 = Vertex(name = 'V_1952',
                particles = [ P.WQ__minus__, P.Z, P.G__plus__, P.GQ0 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_113})

V_1953 = Vertex(name = 'V_1953',
                particles = [ P.WQ__minus__, P.Z, P.G0, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_119})

V_1954 = Vertex(name = 'V_1954',
                particles = [ P.WQ__minus__, P.Z, P.GQ0, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_31})

V_1955 = Vertex(name = 'V_1955',
                particles = [ P.WQ__minus__, P.Z, P.G__plus__, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_23})

V_1956 = Vertex(name = 'V_1956',
                particles = [ P.WQ__minus__, P.Z, P.GQ__plus__, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_96})

V_1957 = Vertex(name = 'V_1957',
                particles = [ P.WQ__minus__, P.Z, P.G__plus__, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_97})

V_1958 = Vertex(name = 'V_1958',
                particles = [ P.WQ__minus__, P.Z, P.GQ__plus__, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_23})

V_1959 = Vertex(name = 'V_1959',
                particles = [ P.WQ__minus__, P.Z, P.Ha, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_31})

V_1960 = Vertex(name = 'V_1960',
                particles = [ P.WQ__minus__, P.Z, P.HaQ, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_113})

V_1961 = Vertex(name = 'V_1961',
                particles = [ P.WQ__minus__, P.Z, P.Hl, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_23})

V_1962 = Vertex(name = 'V_1962',
                particles = [ P.WQ__minus__, P.Z, P.HlQ, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_97})

V_1963 = Vertex(name = 'V_1963',
                particles = [ P.WQ__minus__, P.Z, P.Ha, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_119})

V_1964 = Vertex(name = 'V_1964',
                particles = [ P.WQ__minus__, P.Z, P.HaQ, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_31})

V_1965 = Vertex(name = 'V_1965',
                particles = [ P.WQ__minus__, P.Z, P.Hl, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_96})

V_1966 = Vertex(name = 'V_1966',
                particles = [ P.WQ__minus__, P.Z, P.HlQ, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_23})

V_1967 = Vertex(name = 'V_1967',
                particles = [ P.WQ__minus__, P.Z, P.G__plus__, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_21})

V_1968 = Vertex(name = 'V_1968',
                particles = [ P.WQ__minus__, P.Z, P.GQ__plus__, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_86})

V_1969 = Vertex(name = 'V_1969',
                particles = [ P.WQ__minus__, P.Z, P.G__plus__, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_88})

V_1970 = Vertex(name = 'V_1970',
                particles = [ P.WQ__minus__, P.Z, P.GQ__plus__, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_21})

V_1971 = Vertex(name = 'V_1971',
                particles = [ P.WQ__minus__, P.Z, P.Hh, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_22})

V_1972 = Vertex(name = 'V_1972',
                particles = [ P.WQ__minus__, P.Z, P.HhQ, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_87})

V_1973 = Vertex(name = 'V_1973',
                particles = [ P.WQ__minus__, P.Z, P.Hh, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_89})

V_1974 = Vertex(name = 'V_1974',
                particles = [ P.WQ__minus__, P.Z, P.HhQ, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_22})

V_1975 = Vertex(name = 'V_1975',
                particles = [ P.ghWp, P.ghZ__tilde__, P.WQ__minus__, P.Z ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_193})

V_1976 = Vertex(name = 'V_1976',
                particles = [ P.ghWp, P.ghA__tilde__, P.WQ__minus__, P.Z ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_203})

V_1977 = Vertex(name = 'V_1977',
                particles = [ P.WQ__minus__, P.Z, P.G__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_237})

V_1978 = Vertex(name = 'V_1978',
                particles = [ P.WQ__minus__, P.Z, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_285})

V_1979 = Vertex(name = 'V_1979',
                particles = [ P.WQ__minus__, P.Z, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_231})

V_1980 = Vertex(name = 'V_1980',
                particles = [ P.WQ__minus__, P.Z, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_268})

V_1981 = Vertex(name = 'V_1981',
                particles = [ P.A, P.W__plus__, P.WQ__minus__, P.Z ],
                color = [ '1' ],
                lorentz = [ L.VVVV11 ],
                couplings = {(0,0):C.GC_205})

V_1982 = Vertex(name = 'V_1982',
                particles = [ P.AQ, P.W__plus__, P.WQ__minus__, P.Z ],
                color = [ '1' ],
                lorentz = [ L.VVVV8 ],
                couplings = {(0,0):C.GC_205})

V_1983 = Vertex(name = 'V_1983',
                particles = [ P.WQ__plus__, P.Z, P.G0, P.G__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_30})

V_1984 = Vertex(name = 'V_1984',
                particles = [ P.WQ__plus__, P.Z, P.G__minus__, P.GQ0 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_118})

V_1985 = Vertex(name = 'V_1985',
                particles = [ P.WQ__plus__, P.Z, P.G0, P.GQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_112})

V_1986 = Vertex(name = 'V_1986',
                particles = [ P.WQ__plus__, P.Z, P.GQ0, P.GQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_30})

V_1987 = Vertex(name = 'V_1987',
                particles = [ P.WQ__plus__, P.Z, P.G__minus__, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_23})

V_1988 = Vertex(name = 'V_1988',
                particles = [ P.WQ__plus__, P.Z, P.GQ__minus__, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_96})

V_1989 = Vertex(name = 'V_1989',
                particles = [ P.WQ__plus__, P.Z, P.G__minus__, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_97})

V_1990 = Vertex(name = 'V_1990',
                particles = [ P.WQ__plus__, P.Z, P.GQ__minus__, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_23})

V_1991 = Vertex(name = 'V_1991',
                particles = [ P.WQ__plus__, P.Z, P.Ha, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_30})

V_1992 = Vertex(name = 'V_1992',
                particles = [ P.WQ__plus__, P.Z, P.HaQ, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_118})

V_1993 = Vertex(name = 'V_1993',
                particles = [ P.WQ__plus__, P.Z, P.Hl, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_23})

V_1994 = Vertex(name = 'V_1994',
                particles = [ P.WQ__plus__, P.Z, P.HlQ, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_97})

V_1995 = Vertex(name = 'V_1995',
                particles = [ P.WQ__plus__, P.Z, P.Ha, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_112})

V_1996 = Vertex(name = 'V_1996',
                particles = [ P.WQ__plus__, P.Z, P.HaQ, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_30})

V_1997 = Vertex(name = 'V_1997',
                particles = [ P.WQ__plus__, P.Z, P.Hl, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_96})

V_1998 = Vertex(name = 'V_1998',
                particles = [ P.WQ__plus__, P.Z, P.HlQ, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_23})

V_1999 = Vertex(name = 'V_1999',
                particles = [ P.WQ__plus__, P.Z, P.G__minus__, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_21})

V_2000 = Vertex(name = 'V_2000',
                particles = [ P.WQ__plus__, P.Z, P.GQ__minus__, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_86})

V_2001 = Vertex(name = 'V_2001',
                particles = [ P.WQ__plus__, P.Z, P.G__minus__, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_88})

V_2002 = Vertex(name = 'V_2002',
                particles = [ P.WQ__plus__, P.Z, P.GQ__minus__, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_21})

V_2003 = Vertex(name = 'V_2003',
                particles = [ P.WQ__plus__, P.Z, P.Hh, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_22})

V_2004 = Vertex(name = 'V_2004',
                particles = [ P.WQ__plus__, P.Z, P.HhQ, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_87})

V_2005 = Vertex(name = 'V_2005',
                particles = [ P.WQ__plus__, P.Z, P.Hh, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_89})

V_2006 = Vertex(name = 'V_2006',
                particles = [ P.WQ__plus__, P.Z, P.HhQ, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_22})

V_2007 = Vertex(name = 'V_2007',
                particles = [ P.ghWm, P.ghZ__tilde__, P.WQ__plus__, P.Z ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_193})

V_2008 = Vertex(name = 'V_2008',
                particles = [ P.ghWm, P.ghA__tilde__, P.WQ__plus__, P.Z ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_203})

V_2009 = Vertex(name = 'V_2009',
                particles = [ P.WQ__plus__, P.Z, P.G__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_237})

V_2010 = Vertex(name = 'V_2010',
                particles = [ P.WQ__plus__, P.Z, P.GQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_285})

V_2011 = Vertex(name = 'V_2011',
                particles = [ P.WQ__plus__, P.Z, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_231})

V_2012 = Vertex(name = 'V_2012',
                particles = [ P.WQ__plus__, P.Z, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_268})

V_2013 = Vertex(name = 'V_2013',
                particles = [ P.A, P.W__minus__, P.WQ__plus__, P.Z ],
                color = [ '1' ],
                lorentz = [ L.VVVV11 ],
                couplings = {(0,0):C.GC_205})

V_2014 = Vertex(name = 'V_2014',
                particles = [ P.AQ, P.W__minus__, P.WQ__plus__, P.Z ],
                color = [ '1' ],
                lorentz = [ L.VVVV8 ],
                couplings = {(0,0):C.GC_205})

V_2015 = Vertex(name = 'V_2015',
                particles = [ P.AQ, P.WQ__minus__, P.WQ__plus__, P.Z ],
                color = [ '1' ],
                lorentz = [ L.VVVV11 ],
                couplings = {(0,0):C.GC_205})

V_2016 = Vertex(name = 'V_2016',
                particles = [ P.A, P.WQ__minus__, P.WQ__plus__, P.Z ],
                color = [ '1' ],
                lorentz = [ L.VVVV1 ],
                couplings = {(0,0):C.GC_205})

V_2017 = Vertex(name = 'V_2017',
                particles = [ P.Z, P.Z, P.G0, P.G0 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_230})

V_2018 = Vertex(name = 'V_2018',
                particles = [ P.Z, P.Z, P.G__minus__, P.G__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_229})

V_2019 = Vertex(name = 'V_2019',
                particles = [ P.Z, P.Z, P.G0, P.GQ0 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_230})

V_2020 = Vertex(name = 'V_2020',
                particles = [ P.Z, P.Z, P.GQ0, P.GQ0 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_230})

V_2021 = Vertex(name = 'V_2021',
                particles = [ P.Z, P.Z, P.G__plus__, P.GQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_229})

V_2022 = Vertex(name = 'V_2022',
                particles = [ P.Z, P.Z, P.G__minus__, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_229})

V_2023 = Vertex(name = 'V_2023',
                particles = [ P.Z, P.Z, P.GQ__minus__, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_229})

V_2024 = Vertex(name = 'V_2024',
                particles = [ P.Z, P.Z, P.Ha, P.Ha ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_230})

V_2025 = Vertex(name = 'V_2025',
                particles = [ P.Z, P.Z, P.Ha, P.HaQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_230})

V_2026 = Vertex(name = 'V_2026',
                particles = [ P.Z, P.Z, P.HaQ, P.HaQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_230})

V_2027 = Vertex(name = 'V_2027',
                particles = [ P.Z, P.Z, P.Hh, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_224})

V_2028 = Vertex(name = 'V_2028',
                particles = [ P.Z, P.Z, P.Hh, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_224})

V_2029 = Vertex(name = 'V_2029',
                particles = [ P.Z, P.Z, P.HhQ, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_224})

V_2030 = Vertex(name = 'V_2030',
                particles = [ P.Z, P.Z, P.Hl, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_224})

V_2031 = Vertex(name = 'V_2031',
                particles = [ P.Z, P.Z, P.Hl, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_224})

V_2032 = Vertex(name = 'V_2032',
                particles = [ P.Z, P.Z, P.HlQ, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_224})

V_2033 = Vertex(name = 'V_2033',
                particles = [ P.Z, P.Z, P.H__minus__, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_229})

V_2034 = Vertex(name = 'V_2034',
                particles = [ P.Z, P.Z, P.H__plus__, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_229})

V_2035 = Vertex(name = 'V_2035',
                particles = [ P.Z, P.Z, P.H__minus__, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_229})

V_2036 = Vertex(name = 'V_2036',
                particles = [ P.Z, P.Z, P.HQ__minus__, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_229})

V_2037 = Vertex(name = 'V_2037',
                particles = [ P.ghWm, P.ghWm__tilde__, P.Z, P.Z ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_196})

V_2038 = Vertex(name = 'V_2038',
                particles = [ P.ghWp, P.ghWp__tilde__, P.Z, P.Z ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_196})

V_2039 = Vertex(name = 'V_2039',
                particles = [ P.Z, P.Z, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_332})

V_2040 = Vertex(name = 'V_2040',
                particles = [ P.Z, P.Z, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_332})

V_2041 = Vertex(name = 'V_2041',
                particles = [ P.Z, P.Z, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_329})

V_2042 = Vertex(name = 'V_2042',
                particles = [ P.Z, P.Z, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_329})

V_2043 = Vertex(name = 'V_2043',
                particles = [ P.W__minus__, P.W__plus__, P.Z, P.Z ],
                color = [ '1' ],
                lorentz = [ L.VVVV5 ],
                couplings = {(0,0):C.GC_194})

V_2044 = Vertex(name = 'V_2044',
                particles = [ P.W__plus__, P.WQ__minus__, P.Z, P.Z ],
                color = [ '1' ],
                lorentz = [ L.VVVV5 ],
                couplings = {(0,0):C.GC_194})

V_2045 = Vertex(name = 'V_2045',
                particles = [ P.W__minus__, P.WQ__plus__, P.Z, P.Z ],
                color = [ '1' ],
                lorentz = [ L.VVVV1, L.VVVV2, L.VVVV4 ],
                couplings = {(0,0):C.GC_194,(0,1):C.GC_194,(0,2):C.GC_195})

V_2046 = Vertex(name = 'V_2046',
                particles = [ P.WQ__minus__, P.WQ__plus__, P.Z, P.Z ],
                color = [ '1' ],
                lorentz = [ L.VVVV4 ],
                couplings = {(0,0):C.GC_195})

V_2047 = Vertex(name = 'V_2047',
                particles = [ P.ghWm, P.ghWm__tilde__, P.A, P.ZQ ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_202})

V_2048 = Vertex(name = 'V_2048',
                particles = [ P.ghWp, P.ghWp__tilde__, P.A, P.ZQ ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_202})

V_2049 = Vertex(name = 'V_2049',
                particles = [ P.A, P.ZQ, P.G__minus__, P.G__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_221})

V_2050 = Vertex(name = 'V_2050',
                particles = [ P.A, P.ZQ, P.G__plus__, P.GQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_221})

V_2051 = Vertex(name = 'V_2051',
                particles = [ P.A, P.ZQ, P.G__minus__, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_221})

V_2052 = Vertex(name = 'V_2052',
                particles = [ P.A, P.ZQ, P.GQ__minus__, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_221})

V_2053 = Vertex(name = 'V_2053',
                particles = [ P.A, P.ZQ, P.H__minus__, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_221})

V_2054 = Vertex(name = 'V_2054',
                particles = [ P.A, P.ZQ, P.H__plus__, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_221})

V_2055 = Vertex(name = 'V_2055',
                particles = [ P.A, P.ZQ, P.H__minus__, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_221})

V_2056 = Vertex(name = 'V_2056',
                particles = [ P.A, P.ZQ, P.HQ__minus__, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_221})

V_2057 = Vertex(name = 'V_2057',
                particles = [ P.AQ, P.ZQ, P.G__minus__, P.G__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_221})

V_2058 = Vertex(name = 'V_2058',
                particles = [ P.AQ, P.ZQ, P.G__plus__, P.GQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_221})

V_2059 = Vertex(name = 'V_2059',
                particles = [ P.AQ, P.ZQ, P.G__minus__, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_221})

V_2060 = Vertex(name = 'V_2060',
                particles = [ P.AQ, P.ZQ, P.GQ__minus__, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_221})

V_2061 = Vertex(name = 'V_2061',
                particles = [ P.AQ, P.ZQ, P.H__minus__, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_221})

V_2062 = Vertex(name = 'V_2062',
                particles = [ P.AQ, P.ZQ, P.H__plus__, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_221})

V_2063 = Vertex(name = 'V_2063',
                particles = [ P.AQ, P.ZQ, P.H__minus__, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_221})

V_2064 = Vertex(name = 'V_2064',
                particles = [ P.AQ, P.ZQ, P.HQ__minus__, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_221})

V_2065 = Vertex(name = 'V_2065',
                particles = [ P.ZQ, P.G0, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VSS1, L.VSS4 ],
                couplings = {(0,0):C.GC_215,(0,1):C.GC_216})

V_2066 = Vertex(name = 'V_2066',
                particles = [ P.ZQ, P.G0, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VSS1, L.VSS4 ],
                couplings = {(0,0):C.GC_213,(0,1):C.GC_214})

V_2067 = Vertex(name = 'V_2067',
                particles = [ P.ZQ, P.G__minus__, P.G__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS1, L.VSS4 ],
                couplings = {(0,0):C.GC_218,(0,1):C.GC_217})

V_2068 = Vertex(name = 'V_2068',
                particles = [ P.ZQ, P.GQ0, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VSS1, L.VSS4 ],
                couplings = {(0,0):C.GC_215,(0,1):C.GC_216})

V_2069 = Vertex(name = 'V_2069',
                particles = [ P.ZQ, P.GQ0, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VSS1, L.VSS4 ],
                couplings = {(0,0):C.GC_213,(0,1):C.GC_214})

V_2070 = Vertex(name = 'V_2070',
                particles = [ P.ZQ, P.GQ__minus__, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS1, L.VSS4 ],
                couplings = {(0,0):C.GC_218,(0,1):C.GC_217})

V_2071 = Vertex(name = 'V_2071',
                particles = [ P.ZQ, P.Ha, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VSS1, L.VSS4 ],
                couplings = {(0,0):C.GC_215,(0,1):C.GC_216})

V_2072 = Vertex(name = 'V_2072',
                particles = [ P.ZQ, P.Ha, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VSS1, L.VSS4 ],
                couplings = {(0,0):C.GC_214,(0,1):C.GC_213})

V_2073 = Vertex(name = 'V_2073',
                particles = [ P.ZQ, P.HaQ, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VSS1, L.VSS4 ],
                couplings = {(0,0):C.GC_215,(0,1):C.GC_216})

V_2074 = Vertex(name = 'V_2074',
                particles = [ P.ZQ, P.HaQ, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VSS1, L.VSS4 ],
                couplings = {(0,0):C.GC_214,(0,1):C.GC_213})

V_2075 = Vertex(name = 'V_2075',
                particles = [ P.ZQ, P.H__minus__, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS1, L.VSS4 ],
                couplings = {(0,0):C.GC_218,(0,1):C.GC_217})

V_2076 = Vertex(name = 'V_2076',
                particles = [ P.ZQ, P.HQ__minus__, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VSS1, L.VSS4 ],
                couplings = {(0,0):C.GC_218,(0,1):C.GC_217})

V_2077 = Vertex(name = 'V_2077',
                particles = [ P.W__minus__, P.ZQ, P.G0, P.G__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_31})

V_2078 = Vertex(name = 'V_2078',
                particles = [ P.W__minus__, P.ZQ, P.G__plus__, P.GQ0 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_119})

V_2079 = Vertex(name = 'V_2079',
                particles = [ P.W__minus__, P.ZQ, P.G0, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_113})

V_2080 = Vertex(name = 'V_2080',
                particles = [ P.W__minus__, P.ZQ, P.GQ0, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_31})

V_2081 = Vertex(name = 'V_2081',
                particles = [ P.W__minus__, P.ZQ, P.G__plus__, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_23})

V_2082 = Vertex(name = 'V_2082',
                particles = [ P.W__minus__, P.ZQ, P.GQ__plus__, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_97})

V_2083 = Vertex(name = 'V_2083',
                particles = [ P.W__minus__, P.ZQ, P.G__plus__, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_96})

V_2084 = Vertex(name = 'V_2084',
                particles = [ P.W__minus__, P.ZQ, P.GQ__plus__, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_23})

V_2085 = Vertex(name = 'V_2085',
                particles = [ P.W__minus__, P.ZQ, P.Ha, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_31})

V_2086 = Vertex(name = 'V_2086',
                particles = [ P.W__minus__, P.ZQ, P.HaQ, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_119})

V_2087 = Vertex(name = 'V_2087',
                particles = [ P.W__minus__, P.ZQ, P.Hl, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_23})

V_2088 = Vertex(name = 'V_2088',
                particles = [ P.W__minus__, P.ZQ, P.HlQ, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_96})

V_2089 = Vertex(name = 'V_2089',
                particles = [ P.W__minus__, P.ZQ, P.Ha, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_113})

V_2090 = Vertex(name = 'V_2090',
                particles = [ P.W__minus__, P.ZQ, P.HaQ, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_31})

V_2091 = Vertex(name = 'V_2091',
                particles = [ P.W__minus__, P.ZQ, P.Hl, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_97})

V_2092 = Vertex(name = 'V_2092',
                particles = [ P.W__minus__, P.ZQ, P.HlQ, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_23})

V_2093 = Vertex(name = 'V_2093',
                particles = [ P.W__minus__, P.ZQ, P.G__plus__, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_21})

V_2094 = Vertex(name = 'V_2094',
                particles = [ P.W__minus__, P.ZQ, P.GQ__plus__, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_88})

V_2095 = Vertex(name = 'V_2095',
                particles = [ P.W__minus__, P.ZQ, P.G__plus__, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_86})

V_2096 = Vertex(name = 'V_2096',
                particles = [ P.W__minus__, P.ZQ, P.GQ__plus__, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_21})

V_2097 = Vertex(name = 'V_2097',
                particles = [ P.W__minus__, P.ZQ, P.Hh, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_22})

V_2098 = Vertex(name = 'V_2098',
                particles = [ P.W__minus__, P.ZQ, P.HhQ, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_89})

V_2099 = Vertex(name = 'V_2099',
                particles = [ P.W__minus__, P.ZQ, P.Hh, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_87})

V_2100 = Vertex(name = 'V_2100',
                particles = [ P.W__minus__, P.ZQ, P.HhQ, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_22})

V_2101 = Vertex(name = 'V_2101',
                particles = [ P.ghZ, P.ghWm__tilde__, P.W__minus__, P.ZQ ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_193})

V_2102 = Vertex(name = 'V_2102',
                particles = [ P.ghA, P.ghWm__tilde__, P.W__minus__, P.ZQ ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_203})

V_2103 = Vertex(name = 'V_2103',
                particles = [ P.W__minus__, P.ZQ, P.G__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_237})

V_2104 = Vertex(name = 'V_2104',
                particles = [ P.W__minus__, P.ZQ, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_286})

V_2105 = Vertex(name = 'V_2105',
                particles = [ P.W__minus__, P.ZQ, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_231})

V_2106 = Vertex(name = 'V_2106',
                particles = [ P.W__minus__, P.ZQ, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_269})

V_2107 = Vertex(name = 'V_2107',
                particles = [ P.W__plus__, P.ZQ, P.G0, P.G__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_30})

V_2108 = Vertex(name = 'V_2108',
                particles = [ P.W__plus__, P.ZQ, P.G__minus__, P.GQ0 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_112})

V_2109 = Vertex(name = 'V_2109',
                particles = [ P.W__plus__, P.ZQ, P.G0, P.GQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_118})

V_2110 = Vertex(name = 'V_2110',
                particles = [ P.W__plus__, P.ZQ, P.GQ0, P.GQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_30})

V_2111 = Vertex(name = 'V_2111',
                particles = [ P.W__plus__, P.ZQ, P.G__minus__, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_23})

V_2112 = Vertex(name = 'V_2112',
                particles = [ P.W__plus__, P.ZQ, P.GQ__minus__, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_97})

V_2113 = Vertex(name = 'V_2113',
                particles = [ P.W__plus__, P.ZQ, P.G__minus__, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_96})

V_2114 = Vertex(name = 'V_2114',
                particles = [ P.W__plus__, P.ZQ, P.GQ__minus__, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_23})

V_2115 = Vertex(name = 'V_2115',
                particles = [ P.W__plus__, P.ZQ, P.Ha, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_30})

V_2116 = Vertex(name = 'V_2116',
                particles = [ P.W__plus__, P.ZQ, P.HaQ, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_112})

V_2117 = Vertex(name = 'V_2117',
                particles = [ P.W__plus__, P.ZQ, P.Hl, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_23})

V_2118 = Vertex(name = 'V_2118',
                particles = [ P.W__plus__, P.ZQ, P.HlQ, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_96})

V_2119 = Vertex(name = 'V_2119',
                particles = [ P.W__plus__, P.ZQ, P.Ha, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_118})

V_2120 = Vertex(name = 'V_2120',
                particles = [ P.W__plus__, P.ZQ, P.HaQ, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_30})

V_2121 = Vertex(name = 'V_2121',
                particles = [ P.W__plus__, P.ZQ, P.Hl, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_97})

V_2122 = Vertex(name = 'V_2122',
                particles = [ P.W__plus__, P.ZQ, P.HlQ, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_23})

V_2123 = Vertex(name = 'V_2123',
                particles = [ P.W__plus__, P.ZQ, P.G__minus__, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_21})

V_2124 = Vertex(name = 'V_2124',
                particles = [ P.W__plus__, P.ZQ, P.GQ__minus__, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_88})

V_2125 = Vertex(name = 'V_2125',
                particles = [ P.W__plus__, P.ZQ, P.G__minus__, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_86})

V_2126 = Vertex(name = 'V_2126',
                particles = [ P.W__plus__, P.ZQ, P.GQ__minus__, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_21})

V_2127 = Vertex(name = 'V_2127',
                particles = [ P.W__plus__, P.ZQ, P.Hh, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_22})

V_2128 = Vertex(name = 'V_2128',
                particles = [ P.W__plus__, P.ZQ, P.HhQ, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_89})

V_2129 = Vertex(name = 'V_2129',
                particles = [ P.W__plus__, P.ZQ, P.Hh, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_87})

V_2130 = Vertex(name = 'V_2130',
                particles = [ P.W__plus__, P.ZQ, P.HhQ, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_22})

V_2131 = Vertex(name = 'V_2131',
                particles = [ P.ghZ, P.ghWp__tilde__, P.W__plus__, P.ZQ ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_193})

V_2132 = Vertex(name = 'V_2132',
                particles = [ P.ghA, P.ghWp__tilde__, P.W__plus__, P.ZQ ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_203})

V_2133 = Vertex(name = 'V_2133',
                particles = [ P.W__plus__, P.ZQ, P.G__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_237})

V_2134 = Vertex(name = 'V_2134',
                particles = [ P.W__plus__, P.ZQ, P.GQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_286})

V_2135 = Vertex(name = 'V_2135',
                particles = [ P.W__plus__, P.ZQ, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_231})

V_2136 = Vertex(name = 'V_2136',
                particles = [ P.W__plus__, P.ZQ, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_269})

V_2137 = Vertex(name = 'V_2137',
                particles = [ P.A, P.W__minus__, P.W__plus__, P.ZQ ],
                color = [ '1' ],
                lorentz = [ L.VVVV1, L.VVVV2, L.VVVV4 ],
                couplings = {(0,0):C.GC_205,(0,1):C.GC_202,(0,2):C.GC_202})

V_2138 = Vertex(name = 'V_2138',
                particles = [ P.AQ, P.W__minus__, P.W__plus__, P.ZQ ],
                color = [ '1' ],
                lorentz = [ L.VVVV1 ],
                couplings = {(0,0):C.GC_205})

V_2139 = Vertex(name = 'V_2139',
                particles = [ P.WQ__minus__, P.ZQ, P.G0, P.G__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_31})

V_2140 = Vertex(name = 'V_2140',
                particles = [ P.WQ__minus__, P.ZQ, P.G__plus__, P.GQ0 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_31})

V_2141 = Vertex(name = 'V_2141',
                particles = [ P.WQ__minus__, P.ZQ, P.G0, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_31})

V_2142 = Vertex(name = 'V_2142',
                particles = [ P.WQ__minus__, P.ZQ, P.GQ0, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_31})

V_2143 = Vertex(name = 'V_2143',
                particles = [ P.WQ__minus__, P.ZQ, P.G__plus__, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_23})

V_2144 = Vertex(name = 'V_2144',
                particles = [ P.WQ__minus__, P.ZQ, P.GQ__plus__, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_23})

V_2145 = Vertex(name = 'V_2145',
                particles = [ P.WQ__minus__, P.ZQ, P.G__plus__, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_23})

V_2146 = Vertex(name = 'V_2146',
                particles = [ P.WQ__minus__, P.ZQ, P.GQ__plus__, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_23})

V_2147 = Vertex(name = 'V_2147',
                particles = [ P.WQ__minus__, P.ZQ, P.Ha, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_31})

V_2148 = Vertex(name = 'V_2148',
                particles = [ P.WQ__minus__, P.ZQ, P.HaQ, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_31})

V_2149 = Vertex(name = 'V_2149',
                particles = [ P.WQ__minus__, P.ZQ, P.Hl, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_23})

V_2150 = Vertex(name = 'V_2150',
                particles = [ P.WQ__minus__, P.ZQ, P.HlQ, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_23})

V_2151 = Vertex(name = 'V_2151',
                particles = [ P.WQ__minus__, P.ZQ, P.Ha, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_31})

V_2152 = Vertex(name = 'V_2152',
                particles = [ P.WQ__minus__, P.ZQ, P.HaQ, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_31})

V_2153 = Vertex(name = 'V_2153',
                particles = [ P.WQ__minus__, P.ZQ, P.Hl, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_23})

V_2154 = Vertex(name = 'V_2154',
                particles = [ P.WQ__minus__, P.ZQ, P.HlQ, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_23})

V_2155 = Vertex(name = 'V_2155',
                particles = [ P.WQ__minus__, P.ZQ, P.G__plus__, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_21})

V_2156 = Vertex(name = 'V_2156',
                particles = [ P.WQ__minus__, P.ZQ, P.GQ__plus__, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_21})

V_2157 = Vertex(name = 'V_2157',
                particles = [ P.WQ__minus__, P.ZQ, P.G__plus__, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_21})

V_2158 = Vertex(name = 'V_2158',
                particles = [ P.WQ__minus__, P.ZQ, P.GQ__plus__, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_21})

V_2159 = Vertex(name = 'V_2159',
                particles = [ P.WQ__minus__, P.ZQ, P.Hh, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_22})

V_2160 = Vertex(name = 'V_2160',
                particles = [ P.WQ__minus__, P.ZQ, P.HhQ, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_22})

V_2161 = Vertex(name = 'V_2161',
                particles = [ P.WQ__minus__, P.ZQ, P.Hh, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_22})

V_2162 = Vertex(name = 'V_2162',
                particles = [ P.WQ__minus__, P.ZQ, P.HhQ, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_22})

V_2163 = Vertex(name = 'V_2163',
                particles = [ P.WQ__minus__, P.ZQ, P.G__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_237})

V_2164 = Vertex(name = 'V_2164',
                particles = [ P.WQ__minus__, P.ZQ, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_237})

V_2165 = Vertex(name = 'V_2165',
                particles = [ P.WQ__minus__, P.ZQ, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_231})

V_2166 = Vertex(name = 'V_2166',
                particles = [ P.WQ__minus__, P.ZQ, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_231})

V_2167 = Vertex(name = 'V_2167',
                particles = [ P.A, P.W__plus__, P.WQ__minus__, P.ZQ ],
                color = [ '1' ],
                lorentz = [ L.VVVV1, L.VVVV2, L.VVVV4 ],
                couplings = {(0,0):C.GC_205,(0,1):C.GC_204,(0,2):C.GC_202})

V_2168 = Vertex(name = 'V_2168',
                particles = [ P.AQ, P.W__plus__, P.WQ__minus__, P.ZQ ],
                color = [ '1' ],
                lorentz = [ L.VVVV1, L.VVVV2, L.VVVV4 ],
                couplings = {(0,0):C.GC_205,(0,1):C.GC_202,(0,2):C.GC_202})

V_2169 = Vertex(name = 'V_2169',
                particles = [ P.WQ__plus__, P.ZQ, P.G0, P.G__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_30})

V_2170 = Vertex(name = 'V_2170',
                particles = [ P.WQ__plus__, P.ZQ, P.G__minus__, P.GQ0 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_30})

V_2171 = Vertex(name = 'V_2171',
                particles = [ P.WQ__plus__, P.ZQ, P.G0, P.GQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_30})

V_2172 = Vertex(name = 'V_2172',
                particles = [ P.WQ__plus__, P.ZQ, P.GQ0, P.GQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_30})

V_2173 = Vertex(name = 'V_2173',
                particles = [ P.WQ__plus__, P.ZQ, P.G__minus__, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_23})

V_2174 = Vertex(name = 'V_2174',
                particles = [ P.WQ__plus__, P.ZQ, P.GQ__minus__, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_23})

V_2175 = Vertex(name = 'V_2175',
                particles = [ P.WQ__plus__, P.ZQ, P.G__minus__, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_23})

V_2176 = Vertex(name = 'V_2176',
                particles = [ P.WQ__plus__, P.ZQ, P.GQ__minus__, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_23})

V_2177 = Vertex(name = 'V_2177',
                particles = [ P.WQ__plus__, P.ZQ, P.Ha, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_30})

V_2178 = Vertex(name = 'V_2178',
                particles = [ P.WQ__plus__, P.ZQ, P.HaQ, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_30})

V_2179 = Vertex(name = 'V_2179',
                particles = [ P.WQ__plus__, P.ZQ, P.Hl, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_23})

V_2180 = Vertex(name = 'V_2180',
                particles = [ P.WQ__plus__, P.ZQ, P.HlQ, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_23})

V_2181 = Vertex(name = 'V_2181',
                particles = [ P.WQ__plus__, P.ZQ, P.Ha, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_30})

V_2182 = Vertex(name = 'V_2182',
                particles = [ P.WQ__plus__, P.ZQ, P.HaQ, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_30})

V_2183 = Vertex(name = 'V_2183',
                particles = [ P.WQ__plus__, P.ZQ, P.Hl, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_23})

V_2184 = Vertex(name = 'V_2184',
                particles = [ P.WQ__plus__, P.ZQ, P.HlQ, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_23})

V_2185 = Vertex(name = 'V_2185',
                particles = [ P.WQ__plus__, P.ZQ, P.G__minus__, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_21})

V_2186 = Vertex(name = 'V_2186',
                particles = [ P.WQ__plus__, P.ZQ, P.GQ__minus__, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_21})

V_2187 = Vertex(name = 'V_2187',
                particles = [ P.WQ__plus__, P.ZQ, P.G__minus__, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_21})

V_2188 = Vertex(name = 'V_2188',
                particles = [ P.WQ__plus__, P.ZQ, P.GQ__minus__, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_21})

V_2189 = Vertex(name = 'V_2189',
                particles = [ P.WQ__plus__, P.ZQ, P.Hh, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_22})

V_2190 = Vertex(name = 'V_2190',
                particles = [ P.WQ__plus__, P.ZQ, P.HhQ, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_22})

V_2191 = Vertex(name = 'V_2191',
                particles = [ P.WQ__plus__, P.ZQ, P.Hh, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_22})

V_2192 = Vertex(name = 'V_2192',
                particles = [ P.WQ__plus__, P.ZQ, P.HhQ, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_22})

V_2193 = Vertex(name = 'V_2193',
                particles = [ P.WQ__plus__, P.ZQ, P.G__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_237})

V_2194 = Vertex(name = 'V_2194',
                particles = [ P.WQ__plus__, P.ZQ, P.GQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_237})

V_2195 = Vertex(name = 'V_2195',
                particles = [ P.WQ__plus__, P.ZQ, P.H__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_231})

V_2196 = Vertex(name = 'V_2196',
                particles = [ P.WQ__plus__, P.ZQ, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_231})

V_2197 = Vertex(name = 'V_2197',
                particles = [ P.A, P.W__minus__, P.WQ__plus__, P.ZQ ],
                color = [ '1' ],
                lorentz = [ L.VVVV1, L.VVVV2, L.VVVV4 ],
                couplings = {(0,0):C.GC_205,(0,1):C.GC_204,(0,2):C.GC_202})

V_2198 = Vertex(name = 'V_2198',
                particles = [ P.AQ, P.W__minus__, P.WQ__plus__, P.ZQ ],
                color = [ '1' ],
                lorentz = [ L.VVVV1, L.VVVV2, L.VVVV4 ],
                couplings = {(0,0):C.GC_205,(0,1):C.GC_202,(0,2):C.GC_202})

V_2199 = Vertex(name = 'V_2199',
                particles = [ P.A, P.WQ__minus__, P.WQ__plus__, P.ZQ ],
                color = [ '1' ],
                lorentz = [ L.VVVV1, L.VVVV2, L.VVVV4 ],
                couplings = {(0,0):C.GC_205,(0,1):C.GC_202,(0,2):C.GC_202})

V_2200 = Vertex(name = 'V_2200',
                particles = [ P.AQ, P.WQ__minus__, P.WQ__plus__, P.ZQ ],
                color = [ '1' ],
                lorentz = [ L.VVVV1, L.VVVV2, L.VVVV4 ],
                couplings = {(0,0):C.GC_205,(0,1):C.GC_202,(0,2):C.GC_202})

V_2201 = Vertex(name = 'V_2201',
                particles = [ P.Z, P.ZQ, P.G0, P.G0 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_230})

V_2202 = Vertex(name = 'V_2202',
                particles = [ P.Z, P.ZQ, P.G__minus__, P.G__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_229})

V_2203 = Vertex(name = 'V_2203',
                particles = [ P.Z, P.ZQ, P.G0, P.GQ0 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_230})

V_2204 = Vertex(name = 'V_2204',
                particles = [ P.Z, P.ZQ, P.GQ0, P.GQ0 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_230})

V_2205 = Vertex(name = 'V_2205',
                particles = [ P.Z, P.ZQ, P.G__plus__, P.GQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_229})

V_2206 = Vertex(name = 'V_2206',
                particles = [ P.Z, P.ZQ, P.G__minus__, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_229})

V_2207 = Vertex(name = 'V_2207',
                particles = [ P.Z, P.ZQ, P.GQ__minus__, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_229})

V_2208 = Vertex(name = 'V_2208',
                particles = [ P.Z, P.ZQ, P.Ha, P.Ha ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_230})

V_2209 = Vertex(name = 'V_2209',
                particles = [ P.Z, P.ZQ, P.Ha, P.HaQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_230})

V_2210 = Vertex(name = 'V_2210',
                particles = [ P.Z, P.ZQ, P.HaQ, P.HaQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_230})

V_2211 = Vertex(name = 'V_2211',
                particles = [ P.Z, P.ZQ, P.Hh, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_224})

V_2212 = Vertex(name = 'V_2212',
                particles = [ P.Z, P.ZQ, P.Hh, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_224})

V_2213 = Vertex(name = 'V_2213',
                particles = [ P.Z, P.ZQ, P.HhQ, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_224})

V_2214 = Vertex(name = 'V_2214',
                particles = [ P.Z, P.ZQ, P.Hl, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_224})

V_2215 = Vertex(name = 'V_2215',
                particles = [ P.Z, P.ZQ, P.Hl, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_224})

V_2216 = Vertex(name = 'V_2216',
                particles = [ P.Z, P.ZQ, P.HlQ, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_224})

V_2217 = Vertex(name = 'V_2217',
                particles = [ P.Z, P.ZQ, P.H__minus__, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_229})

V_2218 = Vertex(name = 'V_2218',
                particles = [ P.Z, P.ZQ, P.H__plus__, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_229})

V_2219 = Vertex(name = 'V_2219',
                particles = [ P.Z, P.ZQ, P.H__minus__, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_229})

V_2220 = Vertex(name = 'V_2220',
                particles = [ P.Z, P.ZQ, P.HQ__minus__, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_229})

V_2221 = Vertex(name = 'V_2221',
                particles = [ P.ghWm, P.ghWm__tilde__, P.Z, P.ZQ ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_194})

V_2222 = Vertex(name = 'V_2222',
                particles = [ P.ghWp, P.ghWp__tilde__, P.Z, P.ZQ ],
                color = [ '1' ],
                lorentz = [ L.UUVV1 ],
                couplings = {(0,0):C.GC_194})

V_2223 = Vertex(name = 'V_2223',
                particles = [ P.Z, P.ZQ, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_332})

V_2224 = Vertex(name = 'V_2224',
                particles = [ P.Z, P.ZQ, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_332})

V_2225 = Vertex(name = 'V_2225',
                particles = [ P.Z, P.ZQ, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_329})

V_2226 = Vertex(name = 'V_2226',
                particles = [ P.Z, P.ZQ, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_329})

V_2227 = Vertex(name = 'V_2227',
                particles = [ P.W__minus__, P.W__plus__, P.Z, P.ZQ ],
                color = [ '1' ],
                lorentz = [ L.VVVV1, L.VVVV2, L.VVVV4 ],
                couplings = {(0,0):C.GC_194,(0,1):C.GC_194,(0,2):C.GC_195})

V_2228 = Vertex(name = 'V_2228',
                particles = [ P.W__plus__, P.WQ__minus__, P.Z, P.ZQ ],
                color = [ '1' ],
                lorentz = [ L.VVVV1, L.VVVV2, L.VVVV4 ],
                couplings = {(0,0):C.GC_196,(0,1):C.GC_194,(0,2):C.GC_195})

V_2229 = Vertex(name = 'V_2229',
                particles = [ P.W__minus__, P.WQ__plus__, P.Z, P.ZQ ],
                color = [ '1' ],
                lorentz = [ L.VVVV1, L.VVVV2, L.VVVV4 ],
                couplings = {(0,0):C.GC_196,(0,1):C.GC_194,(0,2):C.GC_195})

V_2230 = Vertex(name = 'V_2230',
                particles = [ P.WQ__minus__, P.WQ__plus__, P.Z, P.ZQ ],
                color = [ '1' ],
                lorentz = [ L.VVVV1, L.VVVV2, L.VVVV4 ],
                couplings = {(0,0):C.GC_194,(0,1):C.GC_194,(0,2):C.GC_195})

V_2231 = Vertex(name = 'V_2231',
                particles = [ P.ZQ, P.ZQ, P.G0, P.G0 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_230})

V_2232 = Vertex(name = 'V_2232',
                particles = [ P.ZQ, P.ZQ, P.G__minus__, P.G__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_229})

V_2233 = Vertex(name = 'V_2233',
                particles = [ P.ZQ, P.ZQ, P.G0, P.GQ0 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_230})

V_2234 = Vertex(name = 'V_2234',
                particles = [ P.ZQ, P.ZQ, P.GQ0, P.GQ0 ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_230})

V_2235 = Vertex(name = 'V_2235',
                particles = [ P.ZQ, P.ZQ, P.G__plus__, P.GQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_229})

V_2236 = Vertex(name = 'V_2236',
                particles = [ P.ZQ, P.ZQ, P.G__minus__, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_229})

V_2237 = Vertex(name = 'V_2237',
                particles = [ P.ZQ, P.ZQ, P.GQ__minus__, P.GQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_229})

V_2238 = Vertex(name = 'V_2238',
                particles = [ P.ZQ, P.ZQ, P.Ha, P.Ha ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_230})

V_2239 = Vertex(name = 'V_2239',
                particles = [ P.ZQ, P.ZQ, P.Ha, P.HaQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_230})

V_2240 = Vertex(name = 'V_2240',
                particles = [ P.ZQ, P.ZQ, P.HaQ, P.HaQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_230})

V_2241 = Vertex(name = 'V_2241',
                particles = [ P.ZQ, P.ZQ, P.Hh, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_224})

V_2242 = Vertex(name = 'V_2242',
                particles = [ P.ZQ, P.ZQ, P.Hh, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_224})

V_2243 = Vertex(name = 'V_2243',
                particles = [ P.ZQ, P.ZQ, P.HhQ, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_224})

V_2244 = Vertex(name = 'V_2244',
                particles = [ P.ZQ, P.ZQ, P.Hl, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_224})

V_2245 = Vertex(name = 'V_2245',
                particles = [ P.ZQ, P.ZQ, P.Hl, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_224})

V_2246 = Vertex(name = 'V_2246',
                particles = [ P.ZQ, P.ZQ, P.HlQ, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_224})

V_2247 = Vertex(name = 'V_2247',
                particles = [ P.ZQ, P.ZQ, P.H__minus__, P.H__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_229})

V_2248 = Vertex(name = 'V_2248',
                particles = [ P.ZQ, P.ZQ, P.H__plus__, P.HQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_229})

V_2249 = Vertex(name = 'V_2249',
                particles = [ P.ZQ, P.ZQ, P.H__minus__, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_229})

V_2250 = Vertex(name = 'V_2250',
                particles = [ P.ZQ, P.ZQ, P.HQ__minus__, P.HQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.VVSS1 ],
                couplings = {(0,0):C.GC_229})

V_2251 = Vertex(name = 'V_2251',
                particles = [ P.ZQ, P.ZQ, P.Hh ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_332})

V_2252 = Vertex(name = 'V_2252',
                particles = [ P.ZQ, P.ZQ, P.HhQ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_332})

V_2253 = Vertex(name = 'V_2253',
                particles = [ P.ZQ, P.ZQ, P.Hl ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_329})

V_2254 = Vertex(name = 'V_2254',
                particles = [ P.ZQ, P.ZQ, P.HlQ ],
                color = [ '1' ],
                lorentz = [ L.VVS1 ],
                couplings = {(0,0):C.GC_329})

V_2255 = Vertex(name = 'V_2255',
                particles = [ P.W__minus__, P.W__plus__, P.ZQ, P.ZQ ],
                color = [ '1' ],
                lorentz = [ L.VVVV4 ],
                couplings = {(0,0):C.GC_195})

V_2256 = Vertex(name = 'V_2256',
                particles = [ P.W__plus__, P.WQ__minus__, P.ZQ, P.ZQ ],
                color = [ '1' ],
                lorentz = [ L.VVVV1, L.VVVV2, L.VVVV4 ],
                couplings = {(0,0):C.GC_194,(0,1):C.GC_194,(0,2):C.GC_195})

V_2257 = Vertex(name = 'V_2257',
                particles = [ P.W__minus__, P.WQ__plus__, P.ZQ, P.ZQ ],
                color = [ '1' ],
                lorentz = [ L.VVVV1, L.VVVV2, L.VVVV4 ],
                couplings = {(0,0):C.GC_194,(0,1):C.GC_194,(0,2):C.GC_195})

V_2258 = Vertex(name = 'V_2258',
                particles = [ P.WQ__minus__, P.WQ__plus__, P.ZQ, P.ZQ ],
                color = [ '1' ],
                lorentz = [ L.VVVV1, L.VVVV2, L.VVVV4 ],
                couplings = {(0,0):C.GC_194,(0,1):C.GC_194,(0,2):C.GC_195})

V_2259 = Vertex(name = 'V_2259',
                particles = [ P.e__plus__, P.e__minus__, P.A ],
                color = [ '1' ],
                lorentz = [ L.FFV1 ],
                couplings = {(0,0):C.GC_4})

V_2260 = Vertex(name = 'V_2260',
                particles = [ P.mu__plus__, P.mu__minus__, P.A ],
                color = [ '1' ],
                lorentz = [ L.FFV1 ],
                couplings = {(0,0):C.GC_4})

V_2261 = Vertex(name = 'V_2261',
                particles = [ P.tau__plus__, P.tau__minus__, P.A ],
                color = [ '1' ],
                lorentz = [ L.FFV1 ],
                couplings = {(0,0):C.GC_4})

V_2262 = Vertex(name = 'V_2262',
                particles = [ P.e__plus__, P.e__minus__, P.AQ ],
                color = [ '1' ],
                lorentz = [ L.FFV1 ],
                couplings = {(0,0):C.GC_4})

V_2263 = Vertex(name = 'V_2263',
                particles = [ P.mu__plus__, P.mu__minus__, P.AQ ],
                color = [ '1' ],
                lorentz = [ L.FFV1 ],
                couplings = {(0,0):C.GC_4})

V_2264 = Vertex(name = 'V_2264',
                particles = [ P.tau__plus__, P.tau__minus__, P.AQ ],
                color = [ '1' ],
                lorentz = [ L.FFV1 ],
                couplings = {(0,0):C.GC_4})

V_2265 = Vertex(name = 'V_2265',
                particles = [ P.u__tilde__, P.u, P.A ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                couplings = {(0,0):C.GC_2})

V_2266 = Vertex(name = 'V_2266',
                particles = [ P.c__tilde__, P.c, P.A ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                couplings = {(0,0):C.GC_2})

V_2267 = Vertex(name = 'V_2267',
                particles = [ P.t__tilde__, P.t, P.A ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                couplings = {(0,0):C.GC_2})

V_2268 = Vertex(name = 'V_2268',
                particles = [ P.d__tilde__, P.d, P.A ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                couplings = {(0,0):C.GC_1})

V_2269 = Vertex(name = 'V_2269',
                particles = [ P.s__tilde__, P.s, P.A ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                couplings = {(0,0):C.GC_1})

V_2270 = Vertex(name = 'V_2270',
                particles = [ P.b__tilde__, P.b, P.A ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                couplings = {(0,0):C.GC_1})

V_2271 = Vertex(name = 'V_2271',
                particles = [ P.u__tilde__, P.u, P.AQ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                couplings = {(0,0):C.GC_2})

V_2272 = Vertex(name = 'V_2272',
                particles = [ P.c__tilde__, P.c, P.AQ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                couplings = {(0,0):C.GC_2})

V_2273 = Vertex(name = 'V_2273',
                particles = [ P.t__tilde__, P.t, P.AQ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                couplings = {(0,0):C.GC_2})

V_2274 = Vertex(name = 'V_2274',
                particles = [ P.d__tilde__, P.d, P.AQ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                couplings = {(0,0):C.GC_1})

V_2275 = Vertex(name = 'V_2275',
                particles = [ P.s__tilde__, P.s, P.AQ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                couplings = {(0,0):C.GC_1})

V_2276 = Vertex(name = 'V_2276',
                particles = [ P.b__tilde__, P.b, P.AQ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                couplings = {(0,0):C.GC_1})

V_2277 = Vertex(name = 'V_2277',
                particles = [ P.u__tilde__, P.u, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                couplings = {(0,0):C.GC_13})

V_2278 = Vertex(name = 'V_2278',
                particles = [ P.c__tilde__, P.c, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                couplings = {(0,0):C.GC_13})

V_2279 = Vertex(name = 'V_2279',
                particles = [ P.t__tilde__, P.t, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                couplings = {(0,0):C.GC_13})

V_2280 = Vertex(name = 'V_2280',
                particles = [ P.d__tilde__, P.d, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                couplings = {(0,0):C.GC_13})

V_2281 = Vertex(name = 'V_2281',
                particles = [ P.s__tilde__, P.s, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                couplings = {(0,0):C.GC_13})

V_2282 = Vertex(name = 'V_2282',
                particles = [ P.b__tilde__, P.b, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                couplings = {(0,0):C.GC_13})

V_2283 = Vertex(name = 'V_2283',
                particles = [ P.u__tilde__, P.u, P.gQ ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                couplings = {(0,0):C.GC_13})

V_2284 = Vertex(name = 'V_2284',
                particles = [ P.c__tilde__, P.c, P.gQ ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                couplings = {(0,0):C.GC_13})

V_2285 = Vertex(name = 'V_2285',
                particles = [ P.t__tilde__, P.t, P.gQ ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                couplings = {(0,0):C.GC_13})

V_2286 = Vertex(name = 'V_2286',
                particles = [ P.d__tilde__, P.d, P.gQ ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                couplings = {(0,0):C.GC_13})

V_2287 = Vertex(name = 'V_2287',
                particles = [ P.s__tilde__, P.s, P.gQ ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                couplings = {(0,0):C.GC_13})

V_2288 = Vertex(name = 'V_2288',
                particles = [ P.b__tilde__, P.b, P.gQ ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                couplings = {(0,0):C.GC_13})

V_2289 = Vertex(name = 'V_2289',
                particles = [ P.d__tilde__, P.u, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_197})

V_2290 = Vertex(name = 'V_2290',
                particles = [ P.s__tilde__, P.c, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_197})

V_2291 = Vertex(name = 'V_2291',
                particles = [ P.b__tilde__, P.t, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_197})

V_2292 = Vertex(name = 'V_2292',
                particles = [ P.d__tilde__, P.u, P.WQ__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_197})

V_2293 = Vertex(name = 'V_2293',
                particles = [ P.s__tilde__, P.c, P.WQ__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_197})

V_2294 = Vertex(name = 'V_2294',
                particles = [ P.b__tilde__, P.t, P.WQ__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_197})

V_2295 = Vertex(name = 'V_2295',
                particles = [ P.u__tilde__, P.d, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_197})

V_2296 = Vertex(name = 'V_2296',
                particles = [ P.c__tilde__, P.s, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_197})

V_2297 = Vertex(name = 'V_2297',
                particles = [ P.t__tilde__, P.b, P.W__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_197})

V_2298 = Vertex(name = 'V_2298',
                particles = [ P.u__tilde__, P.d, P.WQ__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_197})

V_2299 = Vertex(name = 'V_2299',
                particles = [ P.c__tilde__, P.s, P.WQ__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_197})

V_2300 = Vertex(name = 'V_2300',
                particles = [ P.t__tilde__, P.b, P.WQ__plus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_197})

V_2301 = Vertex(name = 'V_2301',
                particles = [ P.e__plus__, P.nu_e, P.W__minus__ ],
                color = [ '1' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_197})

V_2302 = Vertex(name = 'V_2302',
                particles = [ P.mu__plus__, P.nu_mu, P.W__minus__ ],
                color = [ '1' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_197})

V_2303 = Vertex(name = 'V_2303',
                particles = [ P.tau__plus__, P.nu_tau, P.W__minus__ ],
                color = [ '1' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_197})

V_2304 = Vertex(name = 'V_2304',
                particles = [ P.e__plus__, P.nu_e, P.WQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_197})

V_2305 = Vertex(name = 'V_2305',
                particles = [ P.mu__plus__, P.nu_mu, P.WQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_197})

V_2306 = Vertex(name = 'V_2306',
                particles = [ P.tau__plus__, P.nu_tau, P.WQ__minus__ ],
                color = [ '1' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_197})

V_2307 = Vertex(name = 'V_2307',
                particles = [ P.nu_e__tilde__, P.e__minus__, P.W__plus__ ],
                color = [ '1' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_197})

V_2308 = Vertex(name = 'V_2308',
                particles = [ P.nu_mu__tilde__, P.mu__minus__, P.W__plus__ ],
                color = [ '1' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_197})

V_2309 = Vertex(name = 'V_2309',
                particles = [ P.nu_tau__tilde__, P.tau__minus__, P.W__plus__ ],
                color = [ '1' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_197})

V_2310 = Vertex(name = 'V_2310',
                particles = [ P.nu_e__tilde__, P.e__minus__, P.WQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_197})

V_2311 = Vertex(name = 'V_2311',
                particles = [ P.nu_mu__tilde__, P.mu__minus__, P.WQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_197})

V_2312 = Vertex(name = 'V_2312',
                particles = [ P.nu_tau__tilde__, P.tau__minus__, P.WQ__plus__ ],
                color = [ '1' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_197})

V_2313 = Vertex(name = 'V_2313',
                particles = [ P.u__tilde__, P.u, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                couplings = {(0,0):C.GC_210,(0,1):C.GC_207})

V_2314 = Vertex(name = 'V_2314',
                particles = [ P.c__tilde__, P.c, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                couplings = {(0,0):C.GC_210,(0,1):C.GC_207})

V_2315 = Vertex(name = 'V_2315',
                particles = [ P.t__tilde__, P.t, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                couplings = {(0,0):C.GC_210,(0,1):C.GC_207})

V_2316 = Vertex(name = 'V_2316',
                particles = [ P.u__tilde__, P.u, P.ZQ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                couplings = {(0,0):C.GC_210,(0,1):C.GC_207})

V_2317 = Vertex(name = 'V_2317',
                particles = [ P.c__tilde__, P.c, P.ZQ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                couplings = {(0,0):C.GC_210,(0,1):C.GC_207})

V_2318 = Vertex(name = 'V_2318',
                particles = [ P.t__tilde__, P.t, P.ZQ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                couplings = {(0,0):C.GC_210,(0,1):C.GC_207})

V_2319 = Vertex(name = 'V_2319',
                particles = [ P.d__tilde__, P.d, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                couplings = {(0,0):C.GC_209,(0,1):C.GC_206})

V_2320 = Vertex(name = 'V_2320',
                particles = [ P.s__tilde__, P.s, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                couplings = {(0,0):C.GC_209,(0,1):C.GC_206})

V_2321 = Vertex(name = 'V_2321',
                particles = [ P.b__tilde__, P.b, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                couplings = {(0,0):C.GC_209,(0,1):C.GC_206})

V_2322 = Vertex(name = 'V_2322',
                particles = [ P.d__tilde__, P.d, P.ZQ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                couplings = {(0,0):C.GC_209,(0,1):C.GC_206})

V_2323 = Vertex(name = 'V_2323',
                particles = [ P.s__tilde__, P.s, P.ZQ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                couplings = {(0,0):C.GC_209,(0,1):C.GC_206})

V_2324 = Vertex(name = 'V_2324',
                particles = [ P.b__tilde__, P.b, P.ZQ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                couplings = {(0,0):C.GC_209,(0,1):C.GC_206})

V_2325 = Vertex(name = 'V_2325',
                particles = [ P.nu_e__tilde__, P.nu_e, P.Z ],
                color = [ '1' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_212})

V_2326 = Vertex(name = 'V_2326',
                particles = [ P.nu_mu__tilde__, P.nu_mu, P.Z ],
                color = [ '1' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_212})

V_2327 = Vertex(name = 'V_2327',
                particles = [ P.nu_tau__tilde__, P.nu_tau, P.Z ],
                color = [ '1' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_212})

V_2328 = Vertex(name = 'V_2328',
                particles = [ P.nu_e__tilde__, P.nu_e, P.ZQ ],
                color = [ '1' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_212})

V_2329 = Vertex(name = 'V_2329',
                particles = [ P.nu_mu__tilde__, P.nu_mu, P.ZQ ],
                color = [ '1' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_212})

V_2330 = Vertex(name = 'V_2330',
                particles = [ P.nu_tau__tilde__, P.nu_tau, P.ZQ ],
                color = [ '1' ],
                lorentz = [ L.FFV2 ],
                couplings = {(0,0):C.GC_212})

V_2331 = Vertex(name = 'V_2331',
                particles = [ P.e__plus__, P.e__minus__, P.Z ],
                color = [ '1' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                couplings = {(0,0):C.GC_211,(0,1):C.GC_208})

V_2332 = Vertex(name = 'V_2332',
                particles = [ P.mu__plus__, P.mu__minus__, P.Z ],
                color = [ '1' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                couplings = {(0,0):C.GC_211,(0,1):C.GC_208})

V_2333 = Vertex(name = 'V_2333',
                particles = [ P.tau__plus__, P.tau__minus__, P.Z ],
                color = [ '1' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                couplings = {(0,0):C.GC_211,(0,1):C.GC_208})

V_2334 = Vertex(name = 'V_2334',
                particles = [ P.e__plus__, P.e__minus__, P.ZQ ],
                color = [ '1' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                couplings = {(0,0):C.GC_211,(0,1):C.GC_208})

V_2335 = Vertex(name = 'V_2335',
                particles = [ P.mu__plus__, P.mu__minus__, P.ZQ ],
                color = [ '1' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                couplings = {(0,0):C.GC_211,(0,1):C.GC_208})

V_2336 = Vertex(name = 'V_2336',
                particles = [ P.tau__plus__, P.tau__minus__, P.ZQ ],
                color = [ '1' ],
                lorentz = [ L.FFV2, L.FFV3 ],
                couplings = {(0,0):C.GC_211,(0,1):C.GC_208})

