#==============================================================================#
#                                 test_H4F.py                                  #
#==============================================================================#
from __future__ import print_function

import os
import sys
import pyrecola
from rept1l.modeltests.modeltest_utils.model_tests import test_real_values
from rept1l.modeltests.modeltest_utils.model_tests import bold

particle_dict = {'e': 'e-', 'anti-e': 'e+',
                 'mu': 'mu-', 'anti-mu': 'mu+',
                 'nue': 'nu_e', 'anti-nue': 'nu_e~',
                 'num': 'nu_mu', 'anti-num': 'nu_mu~',
                 'dq': 'd', 'anti-dq': 'd~',
                 'uq': 'u', 'anti-uq': 'u~',
                 'cq': 'c', 'anti-cq': 'c~',
                 'sq': 's', 'anti-sq': 's~',
                 'anti-nuemu': ['nu_e~', 'mu-'],
                 'anti-nuedq': ['nu_e~', 'd'],
                 'anti-nueuq': ['nu_e~', 'u'],
                 'anti-nuenum': ['nu_e~', 'nu_mu'],
                 'anti-nuenue': ['nu_e~', 'nu_e'],
                 }

header_offset = 31

def get_particles(ps):
  """ Translate particles conventions Prophecy to Recola """
  ret = []
  for u in ps:
    if u in particle_dict:
      d = particle_dict[u]
      if type(d) is list:
        ret.extend(d)
      else:
        ret.append(d)
    else:
      raise Exception('Could not parse particles:' + str(ps))
  return ret

def read_process(filepath):
  """ Reads the process and stores all matrix elements (Born, NLO, momenta) in a
  dictionary. """
  with open(filepath, 'r') as f:
    lines = f.readlines()
    global header_offset
    offset = header_offset
    header = lines[:offset]
    process = header[2].split()[2:]
    process = [process[0][1:]] + process[1:]
    process = get_particles(process)
    PSPs = []
    for i in range(offset, len(lines), 6):
      momenta = lines[i+1:i+5]
      momenta = [[float(v) for v in u.split()] for u in momenta]
      if len(momenta) > 0:
        ME = lines[i+5].split()
        Born, NLO = ME
        Born = float(Born)
        NLO = float(NLO)
        PSP = {'momenta': momenta, 'Born': Born, 'NLO': NLO}
        PSPs.append(PSP)
    return {'process': process, 'PSPs': PSPs}

def build_tests(path):
  """ Builds the test cases (different Phasespacepoints) which are than computed
  by Recola and compared. """
  tests = {}
  dir = os.path.dirname(os.path.abspath(__file__))
  newpath = os.path.join(dir, path)
  for fn in os.listdir(newpath):
    if len(fn) > 16:
      filepath = os.path.join(newpath, fn)
      if os.path.isfile(filepath):
        PSPs = read_process(filepath)
        tests[tuple(PSPs['process'])] = PSPs['PSPs']
  return tests

def setup_recola():
  """ The setup in which the tests were performed """
  pyrecola.set_renoscheme_rcl('dZee_QED2', 'GFermi')
  pyrecola.set_renoscheme_rcl('db_QED2', 'MSRel')
  pyrecola.set_renoscheme_rcl('da_QED2', 'MSRel')
  pyrecola.set_parameter_rcl('MW', complex(80.39800, 0.))
  #pyrecola.set_parameter_rcl('WW', 0.0, 0.)
  pyrecola.set_parameter_rcl('WW', complex(2.0907035513551233, 0.))
  pyrecola.set_parameter_rcl('MZ', complex(91.18760, 0.))
  #pyrecola.set_parameter_rcl('WZ', 0.0, 0.)
  pyrecola.set_parameter_rcl('WZ', complex(2.4982398858455146, 0.))
  pyrecola.set_parameter_rcl('mh1', complex(125.0, 0.))
  pyrecola.set_parameter_rcl('Wh1', complex(0.0, 0.))
  pyrecola.set_parameter_rcl('MH1', complex(125.0, 0.))
  pyrecola.set_parameter_rcl('WH1', complex(0.0, 0.))
  pyrecola.set_parameter_rcl('MHL', complex(125.0, 0.))
  pyrecola.set_parameter_rcl('WHL', complex(0.0, 0.))
  pyrecola.set_parameter_rcl('mh2', complex(750.0, 0.))
  pyrecola.set_parameter_rcl('Wh2', complex(0.0, 0.))
  pyrecola.set_parameter_rcl('MH2', complex(750.0, 0.))
  pyrecola.set_parameter_rcl('WH2', complex(0.0, 0.))
  pyrecola.set_parameter_rcl('MHH', complex(750.0, 0.))
  pyrecola.set_parameter_rcl('WHH', complex(0.0, 0.))
  pyrecola.set_parameter_rcl('ma',  complex(300.0, 0.))
  pyrecola.set_parameter_rcl('Wa',  complex(0.0, 0.))
  pyrecola.set_parameter_rcl('MHA', complex(300.0, 0.))
  pyrecola.set_parameter_rcl('WHA', complex(0.0, 0.))
  pyrecola.set_parameter_rcl('mhc', complex(300.0, 0.))
  pyrecola.set_parameter_rcl('whc', complex(0.0, 0.))
  pyrecola.set_parameter_rcl('MHC', complex(300.0, 0.))
  pyrecola.set_parameter_rcl('WHC', complex(0.0, 0.))
  pyrecola.set_parameter_rcl('aS',  complex(0., 0.))
  pyrecola.set_parameter_rcl('aEW', complex(7.5562543385512175e-003, 0.))

  pyrecola.set_parameter_rcl('MT', complex(172.5, 0.))
  pyrecola.set_parameter_rcl('WT', complex(0., 0.))
  pyrecola.set_parameter_rcl('MTA', complex(1e-3, 0.))
  pyrecola.set_parameter_rcl('MM', complex(1e-3, 0.))
  pyrecola.set_parameter_rcl('ME', complex(1e-3, 0.))
  pyrecola.set_parameter_rcl('MB', complex(1e-3, 0.))
  pyrecola.set_parameter_rcl('MC', complex(1e-3, 0.))
  pyrecola.set_parameter_rcl('MS', complex(1e-3, 0.))
  pyrecola.set_parameter_rcl('MU', complex(1e-3, 0.))
  pyrecola.set_parameter_rcl('MD', complex(1e-3, 0.))

  # select Type 2
  pyrecola.set_parameter_rcl("h1u", complex(0., 0.))
  pyrecola.set_parameter_rcl("h2u", complex(1., 0.))
  pyrecola.set_parameter_rcl("h2d", complex(0., 0.))
  pyrecola.set_parameter_rcl("h1d", complex(1., 0.))
  pyrecola.set_parameter_rcl("h2l", complex(0., 0.))
  pyrecola.set_parameter_rcl("h1l", complex(1., 0.))

  #pyrecola.set_parameter_rcl('m12', 36002.424982745404, 0.)
  # in new versions msb2 is used as external parameter, setting m12 should only
  # print a warning
  pyrecola.set_parameter_rcl('msb2', complex(90006.062456863481, 0.))
  pyrecola.set_parameter_rcl('MSB',  complex(300.01010392462365, 0.))
  pyrecola.set_parameter_rcl('cb',   complex(0.44721359549995804, 0.))
  pyrecola.set_parameter_rcl('sa',   complex(-0.2, 0.))
  pyrecola.set_mu_uv_rcl(10.)
  pyrecola.set_mu_ms_rcl(10.)
  pyrecola.set_mu_ir_rcl(1.)
  pyrecola.set_masscut_rcl(3.)

def run_process(process, PSPs):
  pyrecola.define_process_rcl(1, process, 'NLO')
  pyrecola.generate_processes_rcl()
  ph = [125.0, 0., 0., 0.]

  warn = 0
  fail = 0
  for PSP in PSPs:
    print (bold('PS:'))
    for mom in PSP['momenta']:
      print("{: >15} {: >15} {: >15} {: >15}".format(*mom))

    momenta = [ph] + PSP['momenta']
    A1, A2, _ = pyrecola.compute_process_rcl(1, momenta, 'NLO')
    print (bold('Recola Born: {:>15}'.format(A1)))
    print (bold('Prophecy Born: {:>15}'.format(PSP['Born'])))
    print (bold('Recola NLO:  {:>15}'.format(A2)))
    print (bold('Prophecy NLO:  {:>15}'.format(PSP['NLO'])))
    calc = {'Born': A1, 'NLO': A2}
    expc = {'Born': PSP['Born'], 'NLO': PSP['NLO']}
    check = test_real_values(calc, expc, err_digits=5, warning_digits=7)
    if not check:
      fail += 1
    elif check == 'Warning':
      warn += 1
  pyrecola.reset_recola_rcl()
  return len(PSPs)-warn-fail, warn, fail

def main(mode=2):
  coloured = False
  #mode = 1
  # run the full comparison
  if mode == 1:
    # renormalization in Denner Scheme
    path = 'fullCMS_DS'
  # only run the bare loop (+R2) comparison
  elif mode == 2:
    path = 'bareCMS'
    pyrecola.set_lp_rcl(2, False)
  elif mode == 3:
    # renormalization in Tadpole Scheme
    path = 'fullCMS_TS'
    global header_offset
    header_offset = 33

  print("Running comparison in mode: " + path)
  tests = build_tests(path)
  print("Channels:", ' | '.join(str(u) for u in tests))
  s_total = 0
  w_total = 0
  f_total = 0
  stdout = sys.stdout

  dir = os.path.dirname(os.path.abspath(__file__))
  logpath = os.path.join(dir, 'logs')
  print('Writing logfiles to: ' + str(logpath))
  if not os.path.exists(logpath):
    os.mkdir(logpath)
  for k in tests:
    #pr = 'h1 -> ' + ' '.join(k)
    pr = 'Hl -> ' + ' '.join(k)
    logfile = os.path.join(logpath, pr + ' ' + path + '.log')
    sys.stdout = open(logfile, 'w')
    setup_recola()
    s, w, f = run_process(pr, tests[k])
    print(pr)
    print('Better/equal than 7 digits:', s)
    print('Between 5 and 7 digits:', w)
    print('Less than 5:', f)
    logfile = os.path.join(logpath, path + '.log')
    sys.stdout = open(logfile, 'a')
    print(pr)
    print('Better/equal than 7 digits:', s)
    print('Between 5 and 7 digits:', w)
    print('Less than 5:', f)
    sys.stdout = stdout
    print(pr)
    print('Better/equal than 7 digits:', s)
    print('Between 5 and 7 digits:', w)
    print('Less than 5:', f)
    s_total += s
    w_total += w
    f_total += f

  logfile = os.path.join(logpath, path + '.log')
  sys.stdout = open(logfile, 'a')
  print('Sum better/equal than 7 digits:', s_total)
  print('Sum between 5 and 7 digits:', w_total)
  print('Sum less than 5:', f_total)
  sys.stdout = stdout
  print('Sum better/equal than 7 digits:', s_total)
  print('Sum between 5 and 7 digits:', w_total)
  print('Sum less than 5:', f_total)

if __name__ == '__main__':
  main()
