#==============================================================================#
#                               setup_models.py                                #
#==============================================================================#

from __future__ import print_function

__doc__ = """Methods for automatic derivation and compilation of recola
modelfiles"""

#============#
#  Includes  #
#============#

import os
import sys
from tempfile import mkdtemp
from distutils.dir_util import copy_tree
from subprocess import call, Popen, PIPE

#===========#
#  Methods  #
#===========#

#==============================================================================#
#                                 setup_model                                  #
#==============================================================================#

def setup_model(driver_path):
  """ Creates a temporary modelpath.

  :return: returns the temporary modelpath
  """
  # create a temporary path where we put the modelfiles and compile them
  mpath = mkdtemp('_rept1l_test_model')
  # driver sources copyed to mpath

  # make sure drivers are found
  if not os.path.exists(driver_path):
    raise Exception('Could not find the drivers path: ' +
                    driver_path)
  else:
    print('Found drivers.')

  # make sure reptil is found
  if 'REPT1L_PATH' not in os.environ:
    raise Exception('Could not find REPT1L_PATH.')
  else:
    print('Found reptil path.')

  if 'RECOLA_PATH' not in os.environ:
    raise Exception('Could not find RECOLA_PATH.')
  else:
    print('Found recola path.')

  return mpath

#==============================================================================#
#                            generate_recola_model                             #
#==============================================================================#

def generate_recola_model(modelpath_reptil, modelpath_recola, driver_path,
                          vertices_file=None, perform_expansion=False,
                          copy_drivers=False, substitute_r2=False,
                          substitute_renos=False,
                          only_form=False):
  """ Copys the drivers in `driver_path` to `modelpath_reptil` and invokes the
  `run_model` script.

  :modelpath_reptil: Temporary output path of the rept1l model sources
  :modelpath_recola: Temporary ouput path of the recola model sources
  :driver_path: Path to the original rept1l model sources

  Optional settings:
  :vertices_file: Perform a ct expansion of a selection of vertices only
  :copy_drivers: Copy the drivers from driver_path to modelpath_reptil
  :substitute_r2: Generate the modelfile including R2 terms
                  (need to be computed first)
  :substitute_renos: Generate the modelfile including the ct expression
                     (need to be computed first)
  :only_form: Only derive form currents, neglecting recola offshell currents.
  """
  if copy_drivers:
    copy_tree(driver_path, modelpath_reptil)
  os.environ['REPTIL_MODEL_PATH'] = modelpath_reptil

  cmd = [os.path.join(os.environ['REPT1L_PATH'], 'tools/run_model'),
         modelpath_recola]

  if perform_expansion:
    ct_cmd = ['-cct']
    if vertices_file:
      expset = os.path.join(modelpath_reptil, vertices_file)
      ct_cmd += ['-ve', expset]
    cmd += ct_cmd

  if substitute_r2:
    cmd_r2 = '-cr2'
    cmd += [cmd_r2]

  if substitute_renos:
    cmd_reno = '-src'
    cmd += [cmd_reno]

  if only_form:
    cmd_form = '-frm'
    cmd += [cmd_form]

  cmd += ['-f']
  call(cmd)

#------------------------------------------------------------------------------#

def run_r2(modelpath_reptil, vertices_file=None, nc=False, np=None, orders={},
           threads=1):
  """ Runs the tool run_r2.

  :modelpath_reptil: Temporary output path of the rept1l model sources
  :vertices_file: Only compute the R2 terms for a selection of vertices.
  :nc: See `run_r2`
  :np: See `run_r2`
  """

  os.environ['REPTIL_MODEL_PATH'] = modelpath_reptil
  cmd = [os.path.join(os.environ['REPT1L_PATH'], 'tools/run_r2')]

  if vertices_file:
    expset = os.path.join(modelpath_reptil, vertices_file)
    cmd += ['-spf', expset]

  if nc:
    cmd_nc = '-nc'
    cmd += [cmd_nc]

  if np:
    cmd += ['-np', str(np)]

  if threads > 1:
    cmd += ['-j', str(threads)]

  if len(orders) > 0:
    for order in orders:
      cmd += ['-' + order] + [str(u) for u in orders[order]]

  call(cmd)

#------------------------------------------------------------------------------#

def run_tool(modelpath_reptil, tool_cll):
  """ Runs the tool with command `tool_cll`. Sets the environment variable for
  the rept1l model sources """
  os.environ['REPTIL_MODEL_PATH'] = modelpath_reptil
  cmd = os.path.join(os.environ['REPT1L_PATH'], 'tools/' + tool_cll)
  call(cmd, shell=True)

#------------------------------------------------------------------------------#

def run1(driverpath, model='sm', perform_expansion=True, vertices_file=None,
         only_form=False):
  """ Generates the RECOLA modelfile from UFO. The NLO expansion can be
  triggered  with `perform_expansion`. The model generation is called with setup
  `1`.

  :driverpath: source path to the rept1l model drivers
  """
  # run1
  basepath = setup_model(driverpath)
  print("basepath:", basepath)
  runpath = os.path.join(basepath, 'run1')
  os.makedirs(runpath)
  print("runpath:", runpath)
  generate_recola_model(basepath, runpath,
                        driverpath,
                        copy_drivers=True,
                        vertices_file=vertices_file,
                        perform_expansion=perform_expansion,
                        only_form=only_form)

  genpath = os.path.dirname(driverpath)
  gensmdc = os.path.join(genpath, 'generate_' + model + '.py')
  cmd = [gensmdc, basepath, runpath, driverpath, '1']
  p = Popen(cmd, stdout=PIPE)
  output, err = p.communicate()

  if 'True' not in output:
    print('First run failed.')
    sys.exit()
  else:
    print('First run succeeded.')

  return gensmdc, basepath, runpath

#------------------------------------------------------------------------------#

def run2(gencmd, basepath, runpath, driverpath, vertices_file=''):
  """ Reruns the model generation for setup `2`.

  :gencmd: Full path to the python script generating the model
  :basepath: Temporary path to the rept1l model drivers
  :runpath: output path of the recola model drivers
  """
  # run2
  cmd = [gencmd, basepath, runpath, driverpath, '2']
  if len(vertices_file) > 0:
    cmd += [vertices_file]

  p = Popen(cmd, stdout=PIPE)
  output, err = p.communicate()

  if 'True' not in output:
    print('Second run failed.')
    sys.exit()
  else:
    print('Second run succeeded.')

#------------------------------------------------------------------------------#

def run5(gencmd, basepath, runpath, driverpath, vertices_file=''):
  """ Reruns the model generation for setup `5`.

  :gencmd: Full path to the python script generating the model
  :basepath: Temporary path to the rept1l model drivers
  :runpath: output path of the recola model drivers
  """
  # run5
  cmd = [gencmd, basepath, runpath, driverpath, '5']
  if len(vertices_file) > 0:
    cmd += [vertices_file]

  p = Popen(cmd, stdout=PIPE)
  output, err = p.communicate()

  if 'True' not in output:
    print('Second run failed.')
    sys.exit()
  else:
    print('Second run succeeded.')

#------------------------------------------------------------------------------#

def runX(runid, gencmd, basepath, runpath, driverpath, vertices_file=''):
  """ Reruns the model generation for setup `5`.

  :runid: run identifier
  :gencmd: Full path to the python script generating the model
  :basepath: Temporary path to the rept1l model drivers
  :runpath: output path of the recola model drivers
  """
  cmd = [gencmd, basepath, runpath, driverpath, runid]
  if len(vertices_file) > 0:
    cmd += [vertices_file]

  p = Popen(cmd, stdout=PIPE)
  output, err = p.communicate()

  if 'True' not in output:
    print('Second run failed.')
    sys.exit()
  else:
    print('Second run succeeded.')

#------------------------------------------------------------------------------#

def run6(gencmd, basepath, runpath, driverpath, vertices_file=''):
  """ Reruns the model generation for setup `6`.

  :gencmd: Full path to the python script generating the model
  :basepath: Temporary path to the rept1l model drivers
  :runpath: output path of the recola model drivers
  """
  # run6
  cmd = [gencmd, basepath, runpath, driverpath, '6']
  if len(vertices_file) > 0:
    cmd += [vertices_file]

  p = Popen(cmd, stdout=PIPE)
  output, err = p.communicate()

  if 'True' not in output:
    print('Second run failed.')
    sys.exit()
  else:
    print('Second run succeeded.')

#------------------------------------------------------------------------------#

def run7(gencmd, basepath, runpath, driverpath, vertices_file=''):
  """ Reruns the model generation for setup `7`.

  :gencmd: Full path to the python script generating the model
  :basepath: Temporary path to the rept1l model drivers
  :runpath: output path of the recola model drivers
  """
  # run6
  cmd = [gencmd, basepath, runpath, driverpath, '7']
  if len(vertices_file) > 0:
    cmd += [vertices_file]

  p = Popen(cmd, stdout=PIPE)
  output, err = p.communicate()

  if 'True' not in output:
    print('Second run failed.')
    sys.exit()
  else:
    print('Second run succeeded.')

#------------------------------------------------------------------------------#

def run8(gencmd, basepath, runpath, driverpath, vertices_file=''):
  """ Reruns the model generation for setup `8`.

  :gencmd: Full path to the python script generating the model
  :basepath: Temporary path to the rept1l model drivers
  :runpath: output path of the recola model drivers
  """
  # run6
  cmd = [gencmd, basepath, runpath, driverpath, '8']
  if len(vertices_file) > 0:
    cmd += [vertices_file]

  p = Popen(cmd, stdout=PIPE)
  output, err = p.communicate()

  if 'True' not in output:
    print('Second run failed.')
    sys.exit()
  else:
    print('Second run succeeded.')

#------------------------------------------------------------------------------#

def run_model_ct_r2(gensmdc, basepath, runpath, driverpath,
                    vertices_file_r2=None, vertices_file_ct='',
                    outputpath=None, compile=True, **kwargs):
  """ Generates the model including ct and r2 terms """
  if outputpath is None:
    runpath = os.path.join(basepath, 'run_final')
  else:
    runpath = outputpath

  if not os.path.exists(runpath):
    os.makedirs(runpath)

  if 'threads' not in kwargs:
    from multiprocessing import cpu_count
    threads = cpu_count()*2
    kwargs['threads'] = threads

  run_r2(basepath, vertices_file=vertices_file_r2, nc=True, **kwargs)
  cmd = [gensmdc, basepath, runpath, driverpath, '3',
         vertices_file_ct]

  if not compile:
    cmd += ['0']

  p = Popen(cmd, stdout=PIPE)
  output, err = p.communicate()

  if 'True' not in output:
    print('Third run failed.')
    sys.exit()
  else:
    print('Third run succeeded.')

#==============================================================================#
#                                compile_model                                 #
#==============================================================================#

def compile_model(modelpath, threads=1):
  """ Compiles the recola model sources. """
  cwd = os.getcwd()
  modelpath_build = os.path.join(modelpath, 'build')

  # make build folder if there is none (assuming modelpath exists)
  if os.path.exists(modelpath) and (not os.path.exists(modelpath_build)):
    os.mkdir(modelpath_build)
  os.chdir(modelpath_build)
  cmd = ['cmake', '..', '-DCMAKE_Fortran_COMPILER=gfortran']
  call(cmd)
  if threads > 1:
    call(['make'])
  else:
    call(['make', '-j', str(threads)])
  os.environ['MODELFILE_PATH'] = modelpath
  os.chdir(cwd)

#==============================================================================#
#                                compile_recola                                #
#==============================================================================#

def compile_recola(threads=1):
  """ Compiles the recola library and links it to a modelfile. """
  cwd = os.getcwd()
  recolapath = os.environ['RECOLA_PATH']
  recolapath_build = os.path.join(recolapath, 'build')
  os.chdir(recolapath_build)

  # python3 ?
  cmd = ['cmake', '..', '-DCMAKE_Fortran_COMPILER=gfortran']
  if sys.version_info.major > 2:
    cmd += ['-Dwith_python3=On']
  else:
    cmd += ['-Dwith_python3=Off']

  call(cmd)
  if threads > 1:
    call(['make'])
  else:
    call(['make', '-j', str(threads)])

  os.chdir(recolapath)
  cmd = "ldd librecola.so | grep modelfile"
  p = Popen(cmd, stdout=PIPE, shell=True)
  output, err = p.communicate()

  os.chdir(cwd)
  return output.decode('utf8').split('=>')[1].split('(')[0].strip()
