# -*- coding: utf-8 -*-

from __future__ import print_function

import os
from six import iteritems
from subprocess import call, Popen, PIPE

coloured = False

def bold(x):
  if coloured:
    return '\033[1;30m' + x + '\033[1;m'
  else:
    return x

def compare_floats(a, b):
  """ Return the number of digits on which `a` and `b` agree """
  from math import log10
  if a == 0 and b == 0:
    return 15
  else:
    nonzero = a if abs(a) > abs(b) else b
    other = b if nonzero == a else a
    diff_normed = abs(nonzero - other)/abs(nonzero)
    if diff_normed != 0.:
      return int(-log10(diff_normed))
    else:
      return 15

def compare_result(digits, err_digits=8, warning_digits=13):
  err = range(err_digits)
  warn = range(err_digits, warning_digits)
  if digits < 0:
    return bold('Failed:') + ' Negative digits:', fail(), False
  if digits in err:
    return bold('Failed:'), fail(), False
  elif digits in warn:
    return bold('Warning:'), warning(), 'Warning'
  else:
    return bold('Success:'), success(), True

def success():
  if coloured:
    return u"\033[0;32m✓\033[1;m".encode('utf-8')
  else:
    return u"✓".encode('utf-8')

def warning():
  if coloured:
    return u"\033[0;33m?\033[1;m".encode('utf-8')
  else:
    return "?".encode('utf-8')

def fail():
  if coloured:
    return u"\033[0;31m✗\033[1;m".encode('utf-8')
  else:
    return u"✗".encode('utf-8')

def compute_ctvalues(modelpath, *exprs):
  from setup_models import compile_model
  compile_model(modelpath)
  os.chdir(modelpath)
  cmd = "./run | grep '" + '\|'.join(exprs) + "'"
  p = Popen(cmd, stdout=PIPE, shell=True)
  output, err = p.communicate()

  ret = {}
  res = output.split('\n')[:-1]
  for r in res:
    expr = r.split('(')[0].split(':')[0].strip()
    r_val = float(r.split('(')[1].split(',')[0].strip())
    i_val = float(r.split('(')[1].split(',')[1].split(')')[0].strip())
    ret[expr] = (r_val, i_val)
  return ret

def test_real_values(expec, calc, verbose_level=2, **kwargs):
  """
  :param expec: Expected values as dictionary
  :type  expec: dict

  :param expec: Calculated values as dictionary
  :type  expec: dict

  :param verbose_level: Level of verbosity:  0 mute, 1 failed or success cases
                                             2 with details
  :type  verbose_level: int

  :param return: returns status message
  :type  return: str
  """

  failed = False
  for expr, val in iteritems(expec):
    if expr not in calc:
      if verbose_level > 0:
        print('Failed comparision ' + expr + ': expected. ' +
              'Not found in the computed values. ' + fail())
      failed = True
      continue

    if val != 0:
      digits = compare_floats(val, calc[expr])
    else:
      digits = 15
      if verbose_level > 0:
        print(bold('Warning:'), 'Not comparing real part of expr: ', expr)
        print(bold('Warning:'), 'Value is expected to be zero: ', calc[expr][0])

    msg, sign, success = compare_result(digits, **kwargs)
    if verbose_level > 0:
      print(msg + expr if verbose_level == 1 else msg + ' Comparison ' + expr)
      if verbose_level > 1:
        print(' expected ' + str(val))
        print(' got ' + str(calc[expr]))
        print(' Digits: ' + str(digits) + ' ' + sign.decode('utf-8'))
    if msg == 'Fail':
      failed = True

  return success

def test_complex_values(expec, calc, verbose_level=2, **kwargs):
  """
  :param expec: Expected values as dictionary
  :type  expec: dict

  :param expec: Calculated values as dictionary
  :type  expec: dict

  :param verbose_level: Level of verbosity:  0 mute, 1 failed or success cases
                                             2 with details
  :type  verbose_level: int

  :param return: returns status message
  :type  return: str
  """

  failed = False
  for expr, val in iteritems(expec):
    if expr not in calc:
      if verbose_level > 0:
        print('Failed comparision ' + expr + ': expected. ' +
              'Not found in the computed values. ' + fail())
      failed = True
      continue

    if val[0] != 0:
      digitsr = compare_floats(val[0], calc[expr][0])
    else:
      digitsr = 15
      if verbose_level > 0:
        print(bold('Warning:'), 'Not comparing real part of expr: ', expr)
        print(bold('Warning:'), 'Value is expected to be zero: ', calc[expr][0])

    if val[1] != 0:
      digitsi = compare_floats(val[1], calc[expr][1])
    else:
      digitsi = 15
      if verbose_level > 0:
        print(bold('Warning:'), 'Not comparing real part of expr: ', expr)
        print(bold('Warning:'), 'Value is expected to be zero: ', calc[expr][0])

    digits = min(digitsr, digitsi)
    msg, sign, success = compare_result(digits, **kwargs)
    if verbose_level > 0:
      print(msg + expr if verbose_level == 1 else msg + ' Comparison ' + expr)
      if verbose_level > 1:
        print(' expected ' + str(val))
        print(' got ' + str(calc[expr]))
        print(' Digits: ' + str(digits) + ' ' + sign)
    if msg == 'Fail':
      failed = True

  return success

def test_offshell_selfenergy(pid, expec, verbose_level=2, **kwargs):
  """
  :param expec: Expected values for given (offshell)masses as dictionary
  :type  expec: dict

  :param expec: Calculated values as dictionary
  :type  expec: dict

  :param verbose_level: Level of verbosity:  0 mute, 1 failed or success cases
                                             2 with details
  :type  verbose_level: int

  :param return: returns status message
  :type  return: str
  """
  import pyrecola

  tests = {}
  for m, val in iteritems(expec):
    mass = m
    p1 = [mass, 0., 0., 0.]
    p2 = p1
    p12 = [p1, p2]

    pyrecola.compute_process_rcl(pid, p12, 'NLO')
    NLOr, NLOi = pyrecola.get_amplitude_rcl(pid, 'NLO', [0, 0], [0, 0], gs=0)
    expc = {str(pid) + '@m=' + str(m): val}
    calc = {str(pid) + '@m=' + str(m): (NLOr, NLOi)}
    tests[m] = test_complex_values(expc, calc, verbose_level=2, **kwargs)
  return tests

def test_amplitude_squared(pid, expec, compute_order='NLO', verbose_level=2,
                           **kwargs):
  """ Testing routine for comparing squared amplitudes with RECOLA.

  :param expec: Expected values for given (offshell)masses as dictionary
  :type  expec: dict

  :param expec: Calculated values as dictionary
  :type  expec: dict

  :param compute_order: Set the order at which the amplitude should be
                        computed. Either 'LO' or 'NLO'.
  :type  compute_order: str

  :param verbose_level: Level of verbosity:  0 mute, 1 failed or success cases
                                             2 with details
  :type  verbose_level: int

  :param return: returns status message
  :type  return: str
  """
  import pyrecola

  orders = ['LO', 'NLO', 'NLO-D4', 'NLO-CT', 'NLO-R2']
  tests = {}
  for test in expec:
    momenta = test['Momenta']
    legs = len(momenta)
    prid = test['prid']
    pyrecola.compute_process_rcl(pid, momenta, compute_order)
    for order in (u for u in orders if u in test):
      for run in test[order]:
        power = run['power']
        calc = pyrecola.get_squared_amplitude_rcl(pid, order, pow=power)
        expc = run['value']
        expc = {str(pid) + '@' + order: expc}
        calc = {str(pid) + '@' + order: calc}
        tests[prid + order] = test_real_values(expc, calc, verbose_level=2,
                                               **kwargs)

  return tests
