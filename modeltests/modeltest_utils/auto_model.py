################################################################################
#                                auto_model.py                                 #
################################################################################

from __future__ import print_function

import os
import rept1l_config
import subprocess
from distutils.dir_util import copy_tree
from rept1l.modeltests.modeltest_utils.setup_models import setup_model

# Environment name for Rept1l(UFO) model file path
if hasattr(rept1l_config, 'rept1l_environ'):
  model_env_name = rept1l_config.rept1l_environ
else:
  model_env_name = 'REPTIL_MODEL_PATH'


class SetupModel(object):
  """ Template class for setting up a Recola2 model file. """

  # path to Rept1l's tools
  tools_path = os.path.join(os.environ['REPT1L_PATH'], 'tools')
  # path to Rept1l's run_model script
  run_model_path = os.path.join(tools_path, 'run_model')
  # path to Rept1l's run_r2 script
  run_r2_path = os.path.join(tools_path, 'run_r2')
  run_ct_path = os.path.join(tools_path, 'run_ct')

  def __init__(self, driver_path, verbose=False):
    """ Set up a temporary model file. """
    self._driver_path = driver_path
    self._reptil_mpath = setup_model(driver_path)
    self._verbose = verbose
    if verbose:
      print("Rept1l model path:", self._reptil_mpath)

  def run_cmd(self, cmd):
    """ Runs a subprocess via check_call and stops in case of error exit
    code. """
    try:
      osstdout = subprocess.check_call(cmd)
    except subprocess.CalledProcessError:
      print('Failed cmd: `' + ' '.join(cmd) + '`')
      raise
    return osstdout

  def test_method(self, method):
    """ Tests if the class has callable `method`. """
    method = getattr(self, method, None)
    return callable(method)

  def run_model(self, recola_mpath, **kwargs):
    """ Runs the `run_model` script. Keyword args are passed on. """
    self._recola_mpath = recola_mpath
    if not os.path.exists(self._recola_mpath):
      os.mkdir(self._recola_mpath)
    if self._verbose:
      print("Recola model path:", self._recola_mpath)
    cmd = self.gen_run_model_cmd(**kwargs)
    if self.verbose:
      print("run_model cmd:", cmd)
    self.run_cmd(cmd)

  def gen_run_model_cmd(self, vertices_file=None,
                        include_vertices=None,
                        exclude_vertices=None,
                        perform_expansion=False,
                        pure_qcd=False,
                        copy_drivers=False,
                        subs_r2=False,
                        subs_ct=False,
                        only_form=False,
                        no_form=False,
                        odp=False,
                        mdlname=False,
                        mdlgauge=False,
                        **kwargs):
    """ Configures the `run_model` script command.

    Optional settings:
    :vertices_file: Perform a ct expansion of a selection of vertices only
    :include_vertices: Include vertices specified in file
    :exclude_vertices: Ignore vertices specified in file
    :copy_drivers: Copy the UFO drivers from _driver_path to self._reptil_mpath
    :subs_r2: Generate the modelfile including R2 terms
              (need to be computed first)
    :subs_ct: Generate the modelfile including the ct expression
              (need to be computed first)
    :only_form: Only derive form currents, omitting tree and loop
                offshell-currents.
    :odp: Overwrite default UFO parameters with predefined (model-dependent)
          ones. See `particles.defaultparamvalues.py`.
    """
    cls = self.__class__
    if copy_drivers:
      copy_tree(self._driver_path, self._reptil_mpath)
    os.environ[model_env_name] = self._reptil_mpath

    cmd = [cls.run_model_path, self._recola_mpath]

    if include_vertices:
      include_file = os.path.join(self._reptil_mpath, include_vertices)
      inc_cmd = ['-fr', include_file]
      cmd += inc_cmd

    if exclude_vertices:
      exclude_file = os.path.join(self._reptil_mpath, exclude_vertices)
      excl_cmd = ['-nfr', exclude_file]
      cmd += excl_cmd

    if perform_expansion:
      ct_cmd = ['-cct']
      if vertices_file:
        expset = os.path.join(self._reptil_mpath, vertices_file)
        ct_cmd += ['-ve', expset]
      cmd += ct_cmd

    if pure_qcd:
      ct_cmd = ['-ctqcd']
      cmd += ct_cmd

    if subs_r2:
      cmd_r2 = '-cr2'
      cmd += [cmd_r2]

    if subs_ct:
      cmd_reno = '-src'
      cmd += [cmd_reno]

    if only_form and not no_form:
      cmd_form = '-frm'
      cmd += [cmd_form]
    elif no_form:
      cmd_noform = '-nfrm'
      cmd += [cmd_noform]

    if odp:
      cmd_odp = '-odp'
      cmd += [cmd_odp]

    if mdlname:
      mdlname_cmd = ['-mdlname', mdlname]
      cmd += mdlname

    if mdlgauge:
      mdlgauge_cmd = ['-mdlgauge', mdlgauge]
      cmd += mdlgauge

    cmd += ['-f']
    self._cmd = cmd
    return cmd

  def run_r2(self, **kwargs):
    """ Runs the `run_r2` script. Keyword args are passed on. """
    cmd = self.gen_run_r2_cmd(**kwargs)
    if self.verbose:
      print("run_r2 cmd:", cmd)
    self.run_cmd(cmd)

  def gen_run_r2_cmd(self, vertices_file=None, particles_list=None,
                     nc=False, np=None, npc=None,
                     orders={}, threads=1, **kwargs):
    """ Configures the `run_r2` script command.

    :vertices_file: Only compute the R2 terms for a selection of vertices.
    :nc: See `run_r2`
    :np: See `run_r2`
    :orders: See `run_r2`
    :threads: See `run_r2`
    """

    cls = self.__class__
    os.environ['REPTIL_MODEL_PATH'] = self._reptil_mpath
    cmd = [cls.run_r2_path]

    if vertices_file:
      expset = os.path.join(self._reptil_mpath, vertices_file)
      cmd += ['-spf', expset]

    if particles_list:
      cmd += ['-sp', particles_list]

    if nc:
      cmd_nc = '-nc'
      cmd += [cmd_nc]

    if npc:
      cmd += ['-npc']

    if np:
      cmd += ['-np', str(np)]

    if threads > 1:
      cmd += ['-j', str(threads)]

    if len(orders) > 0:
      for order in orders:
        cmd += ['-' + order] + [str(u) for u in orders[order]]

    self.cmd = cmd
    return cmd

  def run_ct(self, **kwargs):
    """ Runs the `run_ct` script. Keyword args are passed on. """
    cmd = self.gen_run_ct_cmd(**kwargs)
    if self.verbose:
      print("run_ct cmd:", cmd)
    self.run_cmd(cmd)

  def gen_run_ct_cmd(self, threaded=True, **kwargs):
    """ Configures the `run_cmd` script command.  """
    cls = self.__class__
    os.environ['REPTIL_MODEL_PATH'] = self._reptil_mpath
    cmd = [cls.run_ct_path]

    if threaded:
      cmd += ['-j']

    self.cmd = cmd
    return cmd

  def compile_model(self, threads=1):
    """ Compiles the recola model file library. """
    cwd = os.getcwd()
    modelpath_build = os.path.join(self._recola_mpath, 'build')

    # make build folder if there is none (assuming modelpath exists)
    if os.path.exists(self._recola_mpath) and (not os.path.exists(modelpath_build)):
      os.mkdir(modelpath_build)
    os.chdir(modelpath_build)
    cmd = ['cmake', '..', '-DCMAKE_Fortran_COMPILER=gfortran']
    self.run_cmd(cmd)
    if threads > 1:
      cmd = ['make']
      self.run_cmd(cmd)
    else:
      cmd = ['make', '-j', str(threads)]
      self.run_cmd(cmd)
    os.environ['MODELFILE_PATH'] = self._recola_mpath
    if self.verbose:
      print('MODELFILE_PATH after `compile_model`: ' + self._recola_mpath)
    os.chdir(cwd)

  def compile_recola(self, threads=1):
    """ Compiles the recola library and links a model file to it. """
    cwd = os.getcwd()
    recolapath = os.environ['RECOLA_PATH']
    recolapath_build = os.path.join(recolapath, 'build')
    if not os.path.exists(recolapath_build):
      os.mkdir(recolapath_build)
    os.chdir(recolapath_build)
    self.run_cmd(['cmake', '..', '-DCMAKE_Fortran_COMPILER=gfortran'])
    if threads > 1:
      self.run_cmd(['make'])
    else:
      self.run_cmd(['make', '-j', str(threads)])

    os.chdir(cwd)


class GenModel(SetupModel):
  """ Template class for the generation of a renormalized Recola2 model files.
  Can be used to implement fully automated tests. """

  driverpath = None
  threads = 1
  ct_expansion = True
  pure_qcd = False
  pre_ct = None
  final_ct = None
  verbose = None
  include_vtx = None
  exclude_vtx = None
  outputpath = os.path.join(os.getcwd(), 'renomodel')
  bare_test_stop = False

  required = ['driverpath']

  def __init__(self, **kwargs):
    cls = self.__class__
    for r in cls.required:
      if getattr(cls, r) is None:
        raise Exception('`' + r + '` not set.')

    self.verbose = cls.verbose
    SetupModel.__init__(self, cls.driverpath, verbose=self.verbose)

    self.gen_bare_model('d4_modelfile')
    self.compile_model(threads=cls.threads)
    self.compile_recola(threads=cls.threads)

    if 'use_run_ct' in kwargs and kwargs['use_run_ct']:
      self.run_ct(**kwargs)

    if self.test_method("run_bare_test"):
      self.run_bare_test()

    if cls.bare_test_stop:
      if self.verbose:
        print('Bare test finished.')
      return

    if self.test_method("renormalize_model"):
      self.renormalize_model(**kwargs)

    if self.test_method("compute_rational"):
      self.compute_rational(**kwargs)

    self.gen_renormalized_model(cls.outputpath, **kwargs)
    if self.test_method("run_test"):
      self.compile_model(threads=cls.threads)
      self.compile_recola(threads=cls.threads)
      self.run_test()

  def register_ct(self):
    self.run_tool(['register_ct', '-f', '-s'])

  def run_tool(self, cmd):
    """ Runs a tool defined via the command `cmd` """
    cls = self.__class__
    cmd = [os.path.join(cls.tools_path, cmd[0])] + cmd[1:]
    if self.verbose:
      print("run_tool cmd:", cmd)
    self.run_cmd(cmd)

  def gen_bare_model(self, rmpath):
    """ Generates the bare model file (first pass). """
    rmpath = os.path.join(self._reptil_mpath, rmpath)
    cls = self.__class__
    self.run_model(rmpath,
                   copy_drivers=True,
                   vertices_file=cls.pre_ct,
                   include_vertices=cls.include_vtx,
                   exclude_vertices=cls.exclude_vtx,
                   perform_expansion=cls.ct_expansion,
                   pure_qcd=cls.pure_qcd,
                   only_form=not cls.bare_test_stop)

  def gen_renormalized_model(self, rmpath, **kwargs):
    """ Generates the renormalized model file (second pass). """
    cls = self.__class__
    if 'odp' in kwargs:
      odp = kwargs['odp']
    else:
      odp = False
    self.run_model(rmpath,
                   copy_drivers=False,
                   vertices_file=cls.final_ct,
                   include_vertices=cls.include_vtx,
                   exclude_vertices=cls.exclude_vtx,
                   subs_ct=True,
                   subs_r2=True,
                   perform_expansion=cls.ct_expansion,
                   pure_qcd=cls.pure_qcd,
                   only_form=False,
                   odp=odp,
                   no_form=True)

  def renormalize_sector(self, sector_method, *args, **kwargs):
    if 'bfm' in kwargs:
      bfm = kwargs['bfm']
    else:
      bfm = False
    if bfm:
      self.run_tool([sector_method] + list(args) + ['-bfm'])
    else:
      self.run_tool([sector_method] + list(args))

  def renormalize_qcd(self, *args, **kwargs):
    if self.verbose:
      print('Running renormalize_qcd.')
      print('Rept1l model path: ' + self._reptil_mpath)
      print('Recola model path: ' + self._recola_mpath)
      print("args:", args)
      print("kwargs:", kwargs)
    self.renormalize_sector('renormalize_qcd', *args, **kwargs)

  def renormalize_gsw(self, *args, **kwargs):
    if self.verbose:
      print('Running renormalize_gsw.')
      print('Rept1l model path: ' + self._reptil_mpath)
      print('Recola model path: ' + self._recola_mpath)
      print("args:", args)
      print("kwargs:", kwargs)
    self.renormalize_sector('renormalize_gsw', *args, **kwargs)

  def renormalize_sm_higgs_sector(self, *args, **kwargs):
    if self.verbose:
      print('Running renormalize_sm_higgs_sector.')
      print('Rept1l model path: ' + self._reptil_mpath)
      print('Recola model path: ' + self._recola_mpath)
      print("args:", args)
      print("kwargs:", kwargs)
    self.renormalize_sector('renormalize_sm_higgs_sector', *args, **kwargs)

  def renormalize_2hdm_higgs_sector(self, *args, **kwargs):
    if self.verbose:
      print('Running renormalize_2hdm_higgs_sector.')
      print('Rept1l model path: ' + self._reptil_mpath)
      print('Recola model path: ' + self._recola_mpath)
      print("args:", args)
      print("kwargs:", kwargs)
    self.renormalize_sector('renormalize_2hdm_higgs_sector', *args, **kwargs)

  def renormalize_higgs_singlet(self, *args, **kwargs):
    if self.verbose:
      print('Running renormalize_higgs_singlet.')
      print('Rept1l model path: ' + self._reptil_mpath)
      print('Recola model path: ' + self._recola_mpath)
      print("args:", args)
      print("kwargs:", kwargs)
    self.renormalize_sector('renormalize_higgs_singlet', *args, **kwargs)

  def renormalize_minimal_mixing(self, *args, **kwargs):
    if self.verbose:
      print('Running renormalize_minimal_mixing.')
      print('Rept1l model path: ' + self._reptil_mpath)
      print('Recola model path: ' + self._recola_mpath)
      print("args:", args)
      print("kwargs:", kwargs)
    self.renormalize_sector('renormalize_minimal_mixing', *args, **kwargs)

  def renormalize_higgs_triplet(self, *args, **kwargs):
    if self.verbose:
      print('Running renormalize_higgs_triplet.')
      print('Rept1l model path: ' + self._reptil_mpath)
      print('Recola model path: ' + self._recola_mpath)
      print("args:", args)
      print("kwargs:", kwargs)
    self.renormalize_sector('renormalize_higgs_triplet', *args, **kwargs)

  def renormalize_smsp(self, *args, **kwargs):
    if self.verbose:
      print('Running renormalize_smsp.')
      print('Rept1l model path: ' + self._reptil_mpath)
      print('Recola model path: ' + self._recola_mpath)
      print("args:", args)
      print("kwargs:", kwargs)
    self.renormalize_sector('renormalize_smsp', *args, **kwargs)


class GenBareModel(GenModel):
  """ Template class for the generation of a bare Recola2 model file. """
  threads = 8
  ct_expansion = None
  verbose = True
  bare_test_stop = True
