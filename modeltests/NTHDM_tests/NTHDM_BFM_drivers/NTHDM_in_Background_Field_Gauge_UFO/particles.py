# This file was automatically created by FeynRules 2.3.23
# Mathematica version: 9.0 for Linux x86 (64-bit) (November 20, 2012)
# Date: Sun 13 Aug 2017 19:42:30


from __future__ import division
from object_library import all_particles, Particle
import parameters as Param

import propagators as Prop

A = Particle(pdg_code = 22,
             name = 'A',
             antiname = 'A',
             spin = 3,
             color = 1,
             mass = Param.ZERO,
             width = Param.ZERO,
             texname = 'A',
             antitexname = 'A',
             charge = 0,
             GhostNumber = 0,
             LeptonNumber = 0,
             Y = 0)

AQ = Particle(pdg_code = 22,
              name = 'AQ',
              antiname = 'AQ',
              spin = 3,
              color = 1,
              mass = Param.ZERO,
              width = Param.ZERO,
              texname = 'AQ',
              antitexname = 'AQ',
              charge = 0,
              GhostNumber = 0,
              LeptonNumber = 0,
              Y = 0)

Z = Particle(pdg_code = 23,
             name = 'Z',
             antiname = 'Z',
             spin = 3,
             color = 1,
             mass = Param.MZ,
             width = Param.WZ,
             texname = 'Z',
             antitexname = 'Z',
             charge = 0,
             GhostNumber = 0,
             LeptonNumber = 0,
             Y = 0)

ZQ = Particle(pdg_code = 23,
              name = 'ZQ',
              antiname = 'ZQ',
              spin = 3,
              color = 1,
              mass = Param.MZ,
              width = Param.WZ,
              texname = 'ZQ',
              antitexname = 'ZQ',
              charge = 0,
              GhostNumber = 0,
              LeptonNumber = 0,
              Y = 0)

W__plus__ = Particle(pdg_code = 24,
                     name = 'W+',
                     antiname = 'W-',
                     spin = 3,
                     color = 1,
                     mass = Param.MW,
                     width = Param.WW,
                     texname = 'W+',
                     antitexname = 'W-',
                     charge = 1,
                     GhostNumber = 0,
                     LeptonNumber = 0,
                     Y = 0)

W__minus__ = W__plus__.anti()

WQ__plus__ = Particle(pdg_code = 24,
                      name = 'WQ+',
                      antiname = 'WQ-',
                      spin = 3,
                      color = 1,
                      mass = Param.MW,
                      width = Param.WW,
                      texname = 'WQ+',
                      antitexname = 'WQ-',
                      charge = 1,
                      GhostNumber = 0,
                      LeptonNumber = 0,
                      Y = 0)

WQ__minus__ = WQ__plus__.anti()

g = Particle(pdg_code = 21,
             name = 'g',
             antiname = 'g',
             spin = 3,
             color = 8,
             mass = Param.ZERO,
             width = Param.ZERO,
             texname = 'g',
             antitexname = 'g',
             charge = 0,
             GhostNumber = 0,
             LeptonNumber = 0,
             Y = 0)

gQ = Particle(pdg_code = 21,
              name = 'gQ',
              antiname = 'gQ',
              spin = 3,
              color = 8,
              mass = Param.ZERO,
              width = Param.ZERO,
              texname = 'gQ',
              antitexname = 'gQ',
              charge = 0,
              GhostNumber = 0,
              LeptonNumber = 0,
              Y = 0)

ghA = Particle(pdg_code = 9000001,
               name = 'ghA',
               antiname = 'ghA~',
               spin = -1,
               color = 1,
               mass = Param.ZERO,
               width = Param.ZERO,
               texname = 'ghA',
               antitexname = 'ghA~',
               charge = 0,
               GhostNumber = 1,
               LeptonNumber = 0,
               Y = 0)

ghA__tilde__ = ghA.anti()

ghZ = Particle(pdg_code = 9000002,
               name = 'ghZ',
               antiname = 'ghZ~',
               spin = -1,
               color = 1,
               mass = Param.MZ,
               width = Param.ZERO,
               texname = 'ghZ',
               antitexname = 'ghZ~',
               charge = 0,
               GhostNumber = 1,
               LeptonNumber = 0,
               Y = 0)

ghZ__tilde__ = ghZ.anti()

ghWp = Particle(pdg_code = 9000003,
                name = 'ghWp',
                antiname = 'ghWp~',
                spin = -1,
                color = 1,
                mass = Param.MW,
                width = Param.ZERO,
                texname = 'ghWp',
                antitexname = 'ghWp~',
                charge = 1,
                GhostNumber = 1,
                LeptonNumber = 0,
                Y = 0)

ghWp__tilde__ = ghWp.anti()

ghWm = Particle(pdg_code = 9000004,
                name = 'ghWm',
                antiname = 'ghWm~',
                spin = -1,
                color = 1,
                mass = Param.MW,
                width = Param.ZERO,
                texname = 'ghWm',
                antitexname = 'ghWm~',
                charge = -1,
                GhostNumber = 1,
                LeptonNumber = 0,
                Y = 0)

ghWm__tilde__ = ghWm.anti()

ghG = Particle(pdg_code = 82,
               name = 'ghG',
               antiname = 'ghG~',
               spin = -1,
               color = 8,
               mass = Param.ZERO,
               width = Param.ZERO,
               texname = 'ghG',
               antitexname = 'ghG~',
               charge = 0,
               GhostNumber = 1,
               LeptonNumber = 0,
               Y = 0)

ghG__tilde__ = ghG.anti()

nu_e = Particle(pdg_code = 12,
                name = 'nu_e',
                antiname = 'nu_e~',
                spin = 2,
                color = 1,
                mass = Param.ZERO,
                width = Param.ZERO,
                texname = 'nu_e',
                antitexname = 'nu_e~',
                charge = 0,
                GhostNumber = 0,
                LeptonNumber = 1,
                Y = 0)

nu_e__tilde__ = nu_e.anti()

nu_mu = Particle(pdg_code = 14,
                 name = 'nu_mu',
                 antiname = 'nu_mu~',
                 spin = 2,
                 color = 1,
                 mass = Param.ZERO,
                 width = Param.ZERO,
                 texname = 'nu_mu',
                 antitexname = 'nu_mu~',
                 charge = 0,
                 GhostNumber = 0,
                 LeptonNumber = 1,
                 Y = 0)

nu_mu__tilde__ = nu_mu.anti()

nu_tau = Particle(pdg_code = 16,
                  name = 'nu_tau',
                  antiname = 'nu_tau~',
                  spin = 2,
                  color = 1,
                  mass = Param.ZERO,
                  width = Param.ZERO,
                  texname = 'nu_tau',
                  antitexname = 'nu_tau~',
                  charge = 0,
                  GhostNumber = 0,
                  LeptonNumber = 1,
                  Y = 0)

nu_tau__tilde__ = nu_tau.anti()

e__minus__ = Particle(pdg_code = 11,
                      name = 'e-',
                      antiname = 'e+',
                      spin = 2,
                      color = 1,
                      mass = Param.ME,
                      width = Param.ZERO,
                      texname = 'e-',
                      antitexname = 'e+',
                      charge = -1,
                      GhostNumber = 0,
                      LeptonNumber = 1,
                      Y = 0)

e__plus__ = e__minus__.anti()

mu__minus__ = Particle(pdg_code = 13,
                       name = 'mu-',
                       antiname = 'mu+',
                       spin = 2,
                       color = 1,
                       mass = Param.MM,
                       width = Param.WM,
                       texname = 'mu-',
                       antitexname = 'mu+',
                       charge = -1,
                       GhostNumber = 0,
                       LeptonNumber = 1,
                       Y = 0)

mu__plus__ = mu__minus__.anti()

tau__minus__ = Particle(pdg_code = 15,
                        name = 'tau-',
                        antiname = 'tau+',
                        spin = 2,
                        color = 1,
                        mass = Param.MTA,
                        width = Param.WTA,
                        texname = 'tau-',
                        antitexname = 'tau+',
                        charge = -1,
                        GhostNumber = 0,
                        LeptonNumber = 1,
                        Y = 0)

tau__plus__ = tau__minus__.anti()

u = Particle(pdg_code = 2,
             name = 'u',
             antiname = 'u~',
             spin = 2,
             color = 3,
             mass = Param.MU,
             width = Param.ZERO,
             texname = 'u',
             antitexname = 'u~',
             charge = 2/3,
             GhostNumber = 0,
             LeptonNumber = 0,
             Y = 0)

u__tilde__ = u.anti()

c = Particle(pdg_code = 4,
             name = 'c',
             antiname = 'c~',
             spin = 2,
             color = 3,
             mass = Param.MC,
             width = Param.WC,
             texname = 'c',
             antitexname = 'c~',
             charge = 2/3,
             GhostNumber = 0,
             LeptonNumber = 0,
             Y = 0)

c__tilde__ = c.anti()

t = Particle(pdg_code = 6,
             name = 't',
             antiname = 't~',
             spin = 2,
             color = 3,
             mass = Param.MT,
             width = Param.WT,
             texname = 't',
             antitexname = 't~',
             charge = 2/3,
             GhostNumber = 0,
             LeptonNumber = 0,
             Y = 0)

t__tilde__ = t.anti()

d = Particle(pdg_code = 1,
             name = 'd',
             antiname = 'd~',
             spin = 2,
             color = 3,
             mass = Param.MD,
             width = Param.ZERO,
             texname = 'd',
             antitexname = 'd~',
             charge = -1/3,
             GhostNumber = 0,
             LeptonNumber = 0,
             Y = 0)

d__tilde__ = d.anti()

s = Particle(pdg_code = 3,
             name = 's',
             antiname = 's~',
             spin = 2,
             color = 3,
             mass = Param.MS,
             width = Param.ZERO,
             texname = 's',
             antitexname = 's~',
             charge = -1/3,
             GhostNumber = 0,
             LeptonNumber = 0,
             Y = 0)

s__tilde__ = s.anti()

b = Particle(pdg_code = 5,
             name = 'b',
             antiname = 'b~',
             spin = 2,
             color = 3,
             mass = Param.MB,
             width = Param.WB,
             texname = 'b',
             antitexname = 'b~',
             charge = -1/3,
             GhostNumber = 0,
             LeptonNumber = 0,
             Y = 0)

b__tilde__ = b.anti()

G__plus__ = Particle(pdg_code = 251,
                     name = 'G+',
                     antiname = 'G-',
                     spin = 1,
                     color = 1,
                     mass = Param.MW,
                     width = Param.WW,
                     texname = 'G+',
                     antitexname = 'G-',
                     goldstone = True,
                     charge = 1,
                     GhostNumber = 0,
                     LeptonNumber = 0,
                     Y = 0)

G__minus__ = G__plus__.anti()

GQ__plus__ = Particle(pdg_code = 251,
                      name = 'GQ+',
                      antiname = 'GQ-',
                      spin = 1,
                      color = 1,
                      mass = Param.MW,
                      width = Param.WW,
                      texname = 'GQ+',
                      antitexname = 'GQ-',
                      goldstone = True,
                      charge = 1,
                      GhostNumber = 0,
                      LeptonNumber = 0,
                      Y = 0)

GQ__minus__ = GQ__plus__.anti()

G0 = Particle(pdg_code = 250,
              name = 'G0',
              antiname = 'G0',
              spin = 1,
              color = 1,
              mass = Param.MZ,
              width = Param.WZ,
              texname = 'G0',
              antitexname = 'G0',
              goldstone = True,
              charge = 0,
              GhostNumber = 0,
              LeptonNumber = 0,
              Y = 0)

GQ0 = Particle(pdg_code = 250,
               name = 'GQ0',
               antiname = 'GQ0',
               spin = 1,
               color = 1,
               mass = Param.MZ,
               width = Param.WZ,
               texname = 'GQ0',
               antitexname = 'GQ0',
               goldstone = True,
               charge = 0,
               GhostNumber = 0,
               LeptonNumber = 0,
               Y = 0)

H__plus__ = Particle(pdg_code = 37,
                     name = 'H+',
                     antiname = 'H-',
                     spin = 1,
                     color = 1,
                     mass = Param.MHC,
                     width = Param.WHC,
                     texname = 'H+',
                     antitexname = 'H-',
                     charge = 1,
                     GhostNumber = 0,
                     LeptonNumber = 0,
                     Y = 0)

H__minus__ = H__plus__.anti()

HQ__plus__ = Particle(pdg_code = 37,
                      name = 'HQ+',
                      antiname = 'HQ-',
                      spin = 1,
                      color = 1,
                      mass = Param.MHC,
                      width = Param.WHC,
                      texname = 'HQ+',
                      antitexname = 'HQ-',
                      charge = 1,
                      GhostNumber = 0,
                      LeptonNumber = 0,
                      Y = 0)

HQ__minus__ = HQ__plus__.anti()

Hl = Particle(pdg_code = 25,
              name = 'Hl',
              antiname = 'Hl',
              spin = 1,
              color = 1,
              mass = Param.MHL,
              width = Param.WHL,
              texname = 'Hl',
              antitexname = 'Hl',
              charge = 0,
              GhostNumber = 0,
              LeptonNumber = 0,
              Y = 0)

HlQ = Particle(pdg_code = 25,
               name = 'HlQ',
               antiname = 'HlQ',
               spin = 1,
               color = 1,
               mass = Param.MHL,
               width = Param.WHL,
               texname = 'HlQ',
               antitexname = 'HlQ',
               charge = 0,
               GhostNumber = 0,
               LeptonNumber = 0,
               Y = 0)

Hh = Particle(pdg_code = 35,
              name = 'Hh',
              antiname = 'Hh',
              spin = 1,
              color = 1,
              mass = Param.MHH,
              width = Param.WHH,
              texname = 'Hh',
              antitexname = 'Hh',
              charge = 0,
              GhostNumber = 0,
              LeptonNumber = 0,
              Y = 0)

HhQ = Particle(pdg_code = 35,
               name = 'HhQ',
               antiname = 'HhQ',
               spin = 1,
               color = 1,
               mass = Param.MHH,
               width = Param.WHH,
               texname = 'HhQ',
               antitexname = 'HhQ',
               charge = 0,
               GhostNumber = 0,
               LeptonNumber = 0,
               Y = 0)

Hm = Particle(pdg_code = 45,
              name = 'Hm',
              antiname = 'Hm',
              spin = 1,
              color = 1,
              mass = Param.MHM,
              width = Param.WHM,
              texname = 'Hm',
              antitexname = 'Hm',
              charge = 0,
              GhostNumber = 0,
              LeptonNumber = 0,
              Y = 0)

HmQ = Particle(pdg_code = 45,
               name = 'HmQ',
               antiname = 'HmQ',
               spin = 1,
               color = 1,
               mass = Param.MHM,
               width = Param.WHM,
               texname = 'HmQ',
               antitexname = 'HmQ',
               charge = 0,
               GhostNumber = 0,
               LeptonNumber = 0,
               Y = 0)

Ha = Particle(pdg_code = 36,
              name = 'Ha',
              antiname = 'Ha',
              spin = 1,
              color = 1,
              mass = Param.MHA,
              width = Param.WHA,
              texname = 'Ha',
              antitexname = 'Ha',
              charge = 0,
              GhostNumber = 0,
              LeptonNumber = 0,
              Y = 0)

HaQ = Particle(pdg_code = 36,
               name = 'HaQ',
               antiname = 'HaQ',
               spin = 1,
               color = 1,
               mass = Param.MHA,
               width = Param.WHA,
               texname = 'HaQ',
               antitexname = 'HaQ',
               charge = 0,
               GhostNumber = 0,
               LeptonNumber = 0,
               Y = 0)

