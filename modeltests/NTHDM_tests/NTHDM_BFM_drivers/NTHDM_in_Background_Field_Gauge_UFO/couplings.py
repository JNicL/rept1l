# This file was automatically created by FeynRules 2.3.23
# Mathematica version: 9.0 for Linux x86 (64-bit) (November 20, 2012)
# Date: Sun 13 Aug 2017 19:42:31


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



GC_1 = Coupling(name = 'GC_1',
                value = '(ee*complex(0,1))/3.',
                order = {'QED':1})

GC_2 = Coupling(name = 'GC_2',
                value = '(-2*ee*complex(0,1))/3.',
                order = {'QED':1})

GC_3 = Coupling(name = 'GC_3',
                value = '-(ee*complex(0,1))',
                order = {'QED':1})

GC_4 = Coupling(name = 'GC_4',
                value = 'ee*complex(0,1)',
                order = {'QED':1})

GC_5 = Coupling(name = 'GC_5',
                value = '-2*ee*complex(0,1)',
                order = {'QED':1})

GC_6 = Coupling(name = 'GC_6',
                value = '2*ee*complex(0,1)',
                order = {'QED':1})

GC_7 = Coupling(name = 'GC_7',
                value = '-(ee**2*complex(0,1))',
                order = {'QED':2})

GC_8 = Coupling(name = 'GC_8',
                value = 'ee**2*complex(0,1)',
                order = {'QED':2})

GC_9 = Coupling(name = 'GC_9',
                value = '-2*ee**2*complex(0,1)',
                order = {'QED':2})

GC_10 = Coupling(name = 'GC_10',
                 value = '2*ee**2*complex(0,1)',
                 order = {'QED':2})

GC_11 = Coupling(name = 'GC_11',
                 value = '-2*gs',
                 order = {'QCD':1})

GC_12 = Coupling(name = 'GC_12',
                 value = '-gs',
                 order = {'QCD':1})

GC_13 = Coupling(name = 'GC_13',
                 value = 'complex(0,1)*gs',
                 order = {'QCD':1})

GC_14 = Coupling(name = 'GC_14',
                 value = 'gs',
                 order = {'QCD':1})

GC_15 = Coupling(name = 'GC_15',
                 value = '-(complex(0,1)*gs**2)',
                 order = {'QCD':2})

GC_16 = Coupling(name = 'GC_16',
                 value = 'complex(0,1)*gs**2',
                 order = {'QCD':2})

GC_17 = Coupling(name = 'GC_17',
                 value = '-3*ca1**4*ca2**4*complex(0,1)*l1 - 6*ca1**2*ca2**4*complex(0,1)*l3*sa1**2 - 6*ca1**2*ca2**4*complex(0,1)*l4*sa1**2 - 6*ca1**2*ca2**4*complex(0,1)*l5*sa1**2 - 3*ca2**4*complex(0,1)*l2*sa1**4 - 6*ca1**2*ca2**2*complex(0,1)*l7*sa2**2 - 6*ca2**2*complex(0,1)*l8*sa1**2*sa2**2 - 3*complex(0,1)*l6*sa2**4',
                 order = {'QED':2})

GC_18 = Coupling(name = 'GC_18',
                 value = '3*ca1**4*ca2**3*ca3*complex(0,1)*l1*sa2 - 3*ca1**2*ca2**3*ca3*complex(0,1)*l7*sa2 + 6*ca1**2*ca2**3*ca3*complex(0,1)*l3*sa1**2*sa2 + 6*ca1**2*ca2**3*ca3*complex(0,1)*l4*sa1**2*sa2 + 6*ca1**2*ca2**3*ca3*complex(0,1)*l5*sa1**2*sa2 - 3*ca2**3*ca3*complex(0,1)*l8*sa1**2*sa2 + 3*ca2**3*ca3*complex(0,1)*l2*sa1**4*sa2 - 3*ca2*ca3*complex(0,1)*l6*sa2**3 + 3*ca1**2*ca2*ca3*complex(0,1)*l7*sa2**3 + 3*ca2*ca3*complex(0,1)*l8*sa1**2*sa2**3 - 3*ca1**3*ca2**3*complex(0,1)*l1*sa1*sa3 + 3*ca1**3*ca2**3*complex(0,1)*l3*sa1*sa3 + 3*ca1**3*ca2**3*complex(0,1)*l4*sa1*sa3 + 3*ca1**3*ca2**3*complex(0,1)*l5*sa1*sa3 + 3*ca1*ca2**3*complex(0,1)*l2*sa1**3*sa3 - 3*ca1*ca2**3*complex(0,1)*l3*sa1**3*sa3 - 3*ca1*ca2**3*complex(0,1)*l4*sa1**3*sa3 - 3*ca1*ca2**3*complex(0,1)*l5*sa1**3*sa3 - 3*ca1*ca2*complex(0,1)*l7*sa1*sa2**2*sa3 + 3*ca1*ca2*complex(0,1)*l8*sa1*sa2**2*sa3',
                 order = {'QED':2})

GC_19 = Coupling(name = 'GC_19',
                 value = '3*ca1**3*ca2**3*ca3*complex(0,1)*l1*sa1 - 3*ca1**3*ca2**3*ca3*complex(0,1)*l3*sa1 - 3*ca1**3*ca2**3*ca3*complex(0,1)*l4*sa1 - 3*ca1**3*ca2**3*ca3*complex(0,1)*l5*sa1 - 3*ca1*ca2**3*ca3*complex(0,1)*l2*sa1**3 + 3*ca1*ca2**3*ca3*complex(0,1)*l3*sa1**3 + 3*ca1*ca2**3*ca3*complex(0,1)*l4*sa1**3 + 3*ca1*ca2**3*ca3*complex(0,1)*l5*sa1**3 + 3*ca1*ca2*ca3*complex(0,1)*l7*sa1*sa2**2 - 3*ca1*ca2*ca3*complex(0,1)*l8*sa1*sa2**2 + 3*ca1**4*ca2**3*complex(0,1)*l1*sa2*sa3 - 3*ca1**2*ca2**3*complex(0,1)*l7*sa2*sa3 + 6*ca1**2*ca2**3*complex(0,1)*l3*sa1**2*sa2*sa3 + 6*ca1**2*ca2**3*complex(0,1)*l4*sa1**2*sa2*sa3 + 6*ca1**2*ca2**3*complex(0,1)*l5*sa1**2*sa2*sa3 - 3*ca2**3*complex(0,1)*l8*sa1**2*sa2*sa3 + 3*ca2**3*complex(0,1)*l2*sa1**4*sa2*sa3 - 3*ca2*complex(0,1)*l6*sa2**3*sa3 + 3*ca1**2*ca2*complex(0,1)*l7*sa2**3*sa3 + 3*ca2*complex(0,1)*l8*sa1**2*sa2**3*sa3',
                 order = {'QED':2})

GC_20 = Coupling(name = 'GC_20',
                 value = '-(ca1**2*ca2**4*ca3**2*complex(0,1)*l7) - ca2**4*ca3**2*complex(0,1)*l8*sa1**2 - 3*ca1**4*ca2**2*ca3**2*complex(0,1)*l1*sa2**2 - 3*ca2**2*ca3**2*complex(0,1)*l6*sa2**2 + 4*ca1**2*ca2**2*ca3**2*complex(0,1)*l7*sa2**2 - 6*ca1**2*ca2**2*ca3**2*complex(0,1)*l3*sa1**2*sa2**2 - 6*ca1**2*ca2**2*ca3**2*complex(0,1)*l4*sa1**2*sa2**2 - 6*ca1**2*ca2**2*ca3**2*complex(0,1)*l5*sa1**2*sa2**2 + 4*ca2**2*ca3**2*complex(0,1)*l8*sa1**2*sa2**2 - 3*ca2**2*ca3**2*complex(0,1)*l2*sa1**4*sa2**2 - ca1**2*ca3**2*complex(0,1)*l7*sa2**4 - ca3**2*complex(0,1)*l8*sa1**2*sa2**4 + 6*ca1**3*ca2**2*ca3*complex(0,1)*l1*sa1*sa2*sa3 - 6*ca1**3*ca2**2*ca3*complex(0,1)*l3*sa1*sa2*sa3 - 6*ca1**3*ca2**2*ca3*complex(0,1)*l4*sa1*sa2*sa3 - 6*ca1**3*ca2**2*ca3*complex(0,1)*l5*sa1*sa2*sa3 - 4*ca1*ca2**2*ca3*complex(0,1)*l7*sa1*sa2*sa3 + 4*ca1*ca2**2*ca3*complex(0,1)*l8*sa1*sa2*sa3 - 6*ca1*ca2**2*ca3*complex(0,1)*l2*sa1**3*sa2*sa3 + 6*ca1*ca2**2*ca3*complex(0,1)*l3*sa1**3*sa2*sa3 + 6*ca1*ca2**2*ca3*complex(0,1)*l4*sa1**3*sa2*sa3 + 6*ca1*ca2**2*ca3*complex(0,1)*l5*sa1**3*sa2*sa3 + 2*ca1*ca3*complex(0,1)*l7*sa1*sa2**3*sa3 - 2*ca1*ca3*complex(0,1)*l8*sa1*sa2**3*sa3 - ca1**4*ca2**2*complex(0,1)*l3*sa3**2 - ca1**4*ca2**2*complex(0,1)*l4*sa3**2 - ca1**4*ca2**2*complex(0,1)*l5*sa3**2 - 3*ca1**2*ca2**2*complex(0,1)*l1*sa1**2*sa3**2 - 3*ca1**2*ca2**2*complex(0,1)*l2*sa1**2*sa3**2 + 4*ca1**2*ca2**2*complex(0,1)*l3*sa1**2*sa3**2 + 4*ca1**2*ca2**2*complex(0,1)*l4*sa1**2*sa3**2 + 4*ca1**2*ca2**2*complex(0,1)*l5*sa1**2*sa3**2 - ca2**2*complex(0,1)*l3*sa1**4*sa3**2 - ca2**2*complex(0,1)*l4*sa1**4*sa3**2 - ca2**2*complex(0,1)*l5*sa1**4*sa3**2 - ca1**2*complex(0,1)*l8*sa2**2*sa3**2 - complex(0,1)*l7*sa1**2*sa2**2*sa3**2',
                 order = {'QED':2})

GC_21 = Coupling(name = 'GC_21',
                 value = '-3*ca1**3*ca2**2*ca3**2*complex(0,1)*l1*sa1*sa2 + 3*ca1**3*ca2**2*ca3**2*complex(0,1)*l3*sa1*sa2 + 3*ca1**3*ca2**2*ca3**2*complex(0,1)*l4*sa1*sa2 + 3*ca1**3*ca2**2*ca3**2*complex(0,1)*l5*sa1*sa2 + 2*ca1*ca2**2*ca3**2*complex(0,1)*l7*sa1*sa2 - 2*ca1*ca2**2*ca3**2*complex(0,1)*l8*sa1*sa2 + 3*ca1*ca2**2*ca3**2*complex(0,1)*l2*sa1**3*sa2 - 3*ca1*ca2**2*ca3**2*complex(0,1)*l3*sa1**3*sa2 - 3*ca1*ca2**2*ca3**2*complex(0,1)*l4*sa1**3*sa2 - 3*ca1*ca2**2*ca3**2*complex(0,1)*l5*sa1**3*sa2 - ca1*ca3**2*complex(0,1)*l7*sa1*sa2**3 + ca1*ca3**2*complex(0,1)*l8*sa1*sa2**3 + ca1**4*ca2**2*ca3*complex(0,1)*l3*sa3 + ca1**4*ca2**2*ca3*complex(0,1)*l4*sa3 + ca1**4*ca2**2*ca3*complex(0,1)*l5*sa3 - ca1**2*ca2**4*ca3*complex(0,1)*l7*sa3 + 3*ca1**2*ca2**2*ca3*complex(0,1)*l1*sa1**2*sa3 + 3*ca1**2*ca2**2*ca3*complex(0,1)*l2*sa1**2*sa3 - 4*ca1**2*ca2**2*ca3*complex(0,1)*l3*sa1**2*sa3 - 4*ca1**2*ca2**2*ca3*complex(0,1)*l4*sa1**2*sa3 - 4*ca1**2*ca2**2*ca3*complex(0,1)*l5*sa1**2*sa3 - ca2**4*ca3*complex(0,1)*l8*sa1**2*sa3 + ca2**2*ca3*complex(0,1)*l3*sa1**4*sa3 + ca2**2*ca3*complex(0,1)*l4*sa1**4*sa3 + ca2**2*ca3*complex(0,1)*l5*sa1**4*sa3 - 3*ca1**4*ca2**2*ca3*complex(0,1)*l1*sa2**2*sa3 - 3*ca2**2*ca3*complex(0,1)*l6*sa2**2*sa3 + 4*ca1**2*ca2**2*ca3*complex(0,1)*l7*sa2**2*sa3 + ca1**2*ca3*complex(0,1)*l8*sa2**2*sa3 - 6*ca1**2*ca2**2*ca3*complex(0,1)*l3*sa1**2*sa2**2*sa3 - 6*ca1**2*ca2**2*ca3*complex(0,1)*l4*sa1**2*sa2**2*sa3 - 6*ca1**2*ca2**2*ca3*complex(0,1)*l5*sa1**2*sa2**2*sa3 + ca3*complex(0,1)*l7*sa1**2*sa2**2*sa3 + 4*ca2**2*ca3*complex(0,1)*l8*sa1**2*sa2**2*sa3 - 3*ca2**2*ca3*complex(0,1)*l2*sa1**4*sa2**2*sa3 - ca1**2*ca3*complex(0,1)*l7*sa2**4*sa3 - ca3*complex(0,1)*l8*sa1**2*sa2**4*sa3 + 3*ca1**3*ca2**2*complex(0,1)*l1*sa1*sa2*sa3**2 - 3*ca1**3*ca2**2*complex(0,1)*l3*sa1*sa2*sa3**2 - 3*ca1**3*ca2**2*complex(0,1)*l4*sa1*sa2*sa3**2 - 3*ca1**3*ca2**2*complex(0,1)*l5*sa1*sa2*sa3**2 - 2*ca1*ca2**2*complex(0,1)*l7*sa1*sa2*sa3**2 + 2*ca1*ca2**2*complex(0,1)*l8*sa1*sa2*sa3**2 - 3*ca1*ca2**2*complex(0,1)*l2*sa1**3*sa2*sa3**2 + 3*ca1*ca2**2*complex(0,1)*l3*sa1**3*sa2*sa3**2 + 3*ca1*ca2**2*complex(0,1)*l4*sa1**3*sa2*sa3**2 + 3*ca1*ca2**2*complex(0,1)*l5*sa1**3*sa2*sa3**2 + ca1*complex(0,1)*l7*sa1*sa2**3*sa3**2 - ca1*complex(0,1)*l8*sa1*sa2**3*sa3**2',
                 order = {'QED':2})

GC_22 = Coupling(name = 'GC_22',
                 value = '-(ca1**4*ca2**2*ca3**2*complex(0,1)*l3) - ca1**4*ca2**2*ca3**2*complex(0,1)*l4 - ca1**4*ca2**2*ca3**2*complex(0,1)*l5 - 3*ca1**2*ca2**2*ca3**2*complex(0,1)*l1*sa1**2 - 3*ca1**2*ca2**2*ca3**2*complex(0,1)*l2*sa1**2 + 4*ca1**2*ca2**2*ca3**2*complex(0,1)*l3*sa1**2 + 4*ca1**2*ca2**2*ca3**2*complex(0,1)*l4*sa1**2 + 4*ca1**2*ca2**2*ca3**2*complex(0,1)*l5*sa1**2 - ca2**2*ca3**2*complex(0,1)*l3*sa1**4 - ca2**2*ca3**2*complex(0,1)*l4*sa1**4 - ca2**2*ca3**2*complex(0,1)*l5*sa1**4 - ca1**2*ca3**2*complex(0,1)*l8*sa2**2 - ca3**2*complex(0,1)*l7*sa1**2*sa2**2 - 6*ca1**3*ca2**2*ca3*complex(0,1)*l1*sa1*sa2*sa3 + 6*ca1**3*ca2**2*ca3*complex(0,1)*l3*sa1*sa2*sa3 + 6*ca1**3*ca2**2*ca3*complex(0,1)*l4*sa1*sa2*sa3 + 6*ca1**3*ca2**2*ca3*complex(0,1)*l5*sa1*sa2*sa3 + 4*ca1*ca2**2*ca3*complex(0,1)*l7*sa1*sa2*sa3 - 4*ca1*ca2**2*ca3*complex(0,1)*l8*sa1*sa2*sa3 + 6*ca1*ca2**2*ca3*complex(0,1)*l2*sa1**3*sa2*sa3 - 6*ca1*ca2**2*ca3*complex(0,1)*l3*sa1**3*sa2*sa3 - 6*ca1*ca2**2*ca3*complex(0,1)*l4*sa1**3*sa2*sa3 - 6*ca1*ca2**2*ca3*complex(0,1)*l5*sa1**3*sa2*sa3 - 2*ca1*ca3*complex(0,1)*l7*sa1*sa2**3*sa3 + 2*ca1*ca3*complex(0,1)*l8*sa1*sa2**3*sa3 - ca1**2*ca2**4*complex(0,1)*l7*sa3**2 - ca2**4*complex(0,1)*l8*sa1**2*sa3**2 - 3*ca1**4*ca2**2*complex(0,1)*l1*sa2**2*sa3**2 - 3*ca2**2*complex(0,1)*l6*sa2**2*sa3**2 + 4*ca1**2*ca2**2*complex(0,1)*l7*sa2**2*sa3**2 - 6*ca1**2*ca2**2*complex(0,1)*l3*sa1**2*sa2**2*sa3**2 - 6*ca1**2*ca2**2*complex(0,1)*l4*sa1**2*sa2**2*sa3**2 - 6*ca1**2*ca2**2*complex(0,1)*l5*sa1**2*sa2**2*sa3**2 + 4*ca2**2*complex(0,1)*l8*sa1**2*sa2**2*sa3**2 - 3*ca2**2*complex(0,1)*l2*sa1**4*sa2**2*sa3**2 - ca1**2*complex(0,1)*l7*sa2**4*sa3**2 - complex(0,1)*l8*sa1**2*sa2**4*sa3**2',
                 order = {'QED':2})

GC_23 = Coupling(name = 'GC_23',
                 value = '-3*ca2**3*ca3**3*complex(0,1)*l6*sa2 + 3*ca1**2*ca2**3*ca3**3*complex(0,1)*l7*sa2 + 3*ca2**3*ca3**3*complex(0,1)*l8*sa1**2*sa2 + 3*ca1**4*ca2*ca3**3*complex(0,1)*l1*sa2**3 - 3*ca1**2*ca2*ca3**3*complex(0,1)*l7*sa2**3 + 6*ca1**2*ca2*ca3**3*complex(0,1)*l3*sa1**2*sa2**3 + 6*ca1**2*ca2*ca3**3*complex(0,1)*l4*sa1**2*sa2**3 + 6*ca1**2*ca2*ca3**3*complex(0,1)*l5*sa1**2*sa2**3 - 3*ca2*ca3**3*complex(0,1)*l8*sa1**2*sa2**3 + 3*ca2*ca3**3*complex(0,1)*l2*sa1**4*sa2**3 - 3*ca1*ca2**3*ca3**2*complex(0,1)*l7*sa1*sa3 + 3*ca1*ca2**3*ca3**2*complex(0,1)*l8*sa1*sa3 - 9*ca1**3*ca2*ca3**2*complex(0,1)*l1*sa1*sa2**2*sa3 + 9*ca1**3*ca2*ca3**2*complex(0,1)*l3*sa1*sa2**2*sa3 + 9*ca1**3*ca2*ca3**2*complex(0,1)*l4*sa1*sa2**2*sa3 + 9*ca1**3*ca2*ca3**2*complex(0,1)*l5*sa1*sa2**2*sa3 + 6*ca1*ca2*ca3**2*complex(0,1)*l7*sa1*sa2**2*sa3 - 6*ca1*ca2*ca3**2*complex(0,1)*l8*sa1*sa2**2*sa3 + 9*ca1*ca2*ca3**2*complex(0,1)*l2*sa1**3*sa2**2*sa3 - 9*ca1*ca2*ca3**2*complex(0,1)*l3*sa1**3*sa2**2*sa3 - 9*ca1*ca2*ca3**2*complex(0,1)*l4*sa1**3*sa2**2*sa3 - 9*ca1*ca2*ca3**2*complex(0,1)*l5*sa1**3*sa2**2*sa3 + 3*ca1**4*ca2*ca3*complex(0,1)*l3*sa2*sa3**2 + 3*ca1**4*ca2*ca3*complex(0,1)*l4*sa2*sa3**2 + 3*ca1**4*ca2*ca3*complex(0,1)*l5*sa2*sa3**2 - 3*ca1**2*ca2*ca3*complex(0,1)*l8*sa2*sa3**2 + 9*ca1**2*ca2*ca3*complex(0,1)*l1*sa1**2*sa2*sa3**2 + 9*ca1**2*ca2*ca3*complex(0,1)*l2*sa1**2*sa2*sa3**2 - 12*ca1**2*ca2*ca3*complex(0,1)*l3*sa1**2*sa2*sa3**2 - 12*ca1**2*ca2*ca3*complex(0,1)*l4*sa1**2*sa2*sa3**2 - 12*ca1**2*ca2*ca3*complex(0,1)*l5*sa1**2*sa2*sa3**2 - 3*ca2*ca3*complex(0,1)*l7*sa1**2*sa2*sa3**2 + 3*ca2*ca3*complex(0,1)*l3*sa1**4*sa2*sa3**2 + 3*ca2*ca3*complex(0,1)*l4*sa1**4*sa2*sa3**2 + 3*ca2*ca3*complex(0,1)*l5*sa1**4*sa2*sa3**2 + 3*ca1**3*ca2*complex(0,1)*l2*sa1*sa3**3 - 3*ca1**3*ca2*complex(0,1)*l3*sa1*sa3**3 - 3*ca1**3*ca2*complex(0,1)*l4*sa1*sa3**3 - 3*ca1**3*ca2*complex(0,1)*l5*sa1*sa3**3 - 3*ca1*ca2*complex(0,1)*l1*sa1**3*sa3**3 + 3*ca1*ca2*complex(0,1)*l3*sa1**3*sa3**3 + 3*ca1*ca2*complex(0,1)*l4*sa1**3*sa3**3 + 3*ca1*ca2*complex(0,1)*l5*sa1**3*sa3**3',
                 order = {'QED':2})

GC_24 = Coupling(name = 'GC_24',
                 value = 'ca1*ca2**3*ca3**3*complex(0,1)*l7*sa1 - ca1*ca2**3*ca3**3*complex(0,1)*l8*sa1 + 3*ca1**3*ca2*ca3**3*complex(0,1)*l1*sa1*sa2**2 - 3*ca1**3*ca2*ca3**3*complex(0,1)*l3*sa1*sa2**2 - 3*ca1**3*ca2*ca3**3*complex(0,1)*l4*sa1*sa2**2 - 3*ca1**3*ca2*ca3**3*complex(0,1)*l5*sa1*sa2**2 - 2*ca1*ca2*ca3**3*complex(0,1)*l7*sa1*sa2**2 + 2*ca1*ca2*ca3**3*complex(0,1)*l8*sa1*sa2**2 - 3*ca1*ca2*ca3**3*complex(0,1)*l2*sa1**3*sa2**2 + 3*ca1*ca2*ca3**3*complex(0,1)*l3*sa1**3*sa2**2 + 3*ca1*ca2*ca3**3*complex(0,1)*l4*sa1**3*sa2**2 + 3*ca1*ca2*ca3**3*complex(0,1)*l5*sa1**3*sa2**2 - 2*ca1**4*ca2*ca3**2*complex(0,1)*l3*sa2*sa3 - 2*ca1**4*ca2*ca3**2*complex(0,1)*l4*sa2*sa3 - 2*ca1**4*ca2*ca3**2*complex(0,1)*l5*sa2*sa3 - 3*ca2**3*ca3**2*complex(0,1)*l6*sa2*sa3 + 3*ca1**2*ca2**3*ca3**2*complex(0,1)*l7*sa2*sa3 + 2*ca1**2*ca2*ca3**2*complex(0,1)*l8*sa2*sa3 - 6*ca1**2*ca2*ca3**2*complex(0,1)*l1*sa1**2*sa2*sa3 - 6*ca1**2*ca2*ca3**2*complex(0,1)*l2*sa1**2*sa2*sa3 + 8*ca1**2*ca2*ca3**2*complex(0,1)*l3*sa1**2*sa2*sa3 + 8*ca1**2*ca2*ca3**2*complex(0,1)*l4*sa1**2*sa2*sa3 + 8*ca1**2*ca2*ca3**2*complex(0,1)*l5*sa1**2*sa2*sa3 + 2*ca2*ca3**2*complex(0,1)*l7*sa1**2*sa2*sa3 + 3*ca2**3*ca3**2*complex(0,1)*l8*sa1**2*sa2*sa3 - 2*ca2*ca3**2*complex(0,1)*l3*sa1**4*sa2*sa3 - 2*ca2*ca3**2*complex(0,1)*l4*sa1**4*sa2*sa3 - 2*ca2*ca3**2*complex(0,1)*l5*sa1**4*sa2*sa3 + 3*ca1**4*ca2*ca3**2*complex(0,1)*l1*sa2**3*sa3 - 3*ca1**2*ca2*ca3**2*complex(0,1)*l7*sa2**3*sa3 + 6*ca1**2*ca2*ca3**2*complex(0,1)*l3*sa1**2*sa2**3*sa3 + 6*ca1**2*ca2*ca3**2*complex(0,1)*l4*sa1**2*sa2**3*sa3 + 6*ca1**2*ca2*ca3**2*complex(0,1)*l5*sa1**2*sa2**3*sa3 - 3*ca2*ca3**2*complex(0,1)*l8*sa1**2*sa2**3*sa3 + 3*ca2*ca3**2*complex(0,1)*l2*sa1**4*sa2**3*sa3 - 3*ca1**3*ca2*ca3*complex(0,1)*l2*sa1*sa3**2 + 3*ca1**3*ca2*ca3*complex(0,1)*l3*sa1*sa3**2 + 3*ca1**3*ca2*ca3*complex(0,1)*l4*sa1*sa3**2 + 3*ca1**3*ca2*ca3*complex(0,1)*l5*sa1*sa3**2 - 2*ca1*ca2**3*ca3*complex(0,1)*l7*sa1*sa3**2 + 2*ca1*ca2**3*ca3*complex(0,1)*l8*sa1*sa3**2 + 3*ca1*ca2*ca3*complex(0,1)*l1*sa1**3*sa3**2 - 3*ca1*ca2*ca3*complex(0,1)*l3*sa1**3*sa3**2 - 3*ca1*ca2*ca3*complex(0,1)*l4*sa1**3*sa3**2 - 3*ca1*ca2*ca3*complex(0,1)*l5*sa1**3*sa3**2 - 6*ca1**3*ca2*ca3*complex(0,1)*l1*sa1*sa2**2*sa3**2 + 6*ca1**3*ca2*ca3*complex(0,1)*l3*sa1*sa2**2*sa3**2 + 6*ca1**3*ca2*ca3*complex(0,1)*l4*sa1*sa2**2*sa3**2 + 6*ca1**3*ca2*ca3*complex(0,1)*l5*sa1*sa2**2*sa3**2 + 4*ca1*ca2*ca3*complex(0,1)*l7*sa1*sa2**2*sa3**2 - 4*ca1*ca2*ca3*complex(0,1)*l8*sa1*sa2**2*sa3**2 + 6*ca1*ca2*ca3*complex(0,1)*l2*sa1**3*sa2**2*sa3**2 - 6*ca1*ca2*ca3*complex(0,1)*l3*sa1**3*sa2**2*sa3**2 - 6*ca1*ca2*ca3*complex(0,1)*l4*sa1**3*sa2**2*sa3**2 - 6*ca1*ca2*ca3*complex(0,1)*l5*sa1**3*sa2**2*sa3**2 + ca1**4*ca2*complex(0,1)*l3*sa2*sa3**3 + ca1**4*ca2*complex(0,1)*l4*sa2*sa3**3 + ca1**4*ca2*complex(0,1)*l5*sa2*sa3**3 - ca1**2*ca2*complex(0,1)*l8*sa2*sa3**3 + 3*ca1**2*ca2*complex(0,1)*l1*sa1**2*sa2*sa3**3 + 3*ca1**2*ca2*complex(0,1)*l2*sa1**2*sa2*sa3**3 - 4*ca1**2*ca2*complex(0,1)*l3*sa1**2*sa2*sa3**3 - 4*ca1**2*ca2*complex(0,1)*l4*sa1**2*sa2*sa3**3 - 4*ca1**2*ca2*complex(0,1)*l5*sa1**2*sa2*sa3**3 - ca2*complex(0,1)*l7*sa1**2*sa2*sa3**3 + ca2*complex(0,1)*l3*sa1**4*sa2*sa3**3 + ca2*complex(0,1)*l4*sa1**4*sa2*sa3**3 + ca2*complex(0,1)*l5*sa1**4*sa2*sa3**3',
                 order = {'QED':2})

GC_25 = Coupling(name = 'GC_25',
                 value = 'ca1**4*ca2*ca3**3*complex(0,1)*l3*sa2 + ca1**4*ca2*ca3**3*complex(0,1)*l4*sa2 + ca1**4*ca2*ca3**3*complex(0,1)*l5*sa2 - ca1**2*ca2*ca3**3*complex(0,1)*l8*sa2 + 3*ca1**2*ca2*ca3**3*complex(0,1)*l1*sa1**2*sa2 + 3*ca1**2*ca2*ca3**3*complex(0,1)*l2*sa1**2*sa2 - 4*ca1**2*ca2*ca3**3*complex(0,1)*l3*sa1**2*sa2 - 4*ca1**2*ca2*ca3**3*complex(0,1)*l4*sa1**2*sa2 - 4*ca1**2*ca2*ca3**3*complex(0,1)*l5*sa1**2*sa2 - ca2*ca3**3*complex(0,1)*l7*sa1**2*sa2 + ca2*ca3**3*complex(0,1)*l3*sa1**4*sa2 + ca2*ca3**3*complex(0,1)*l4*sa1**4*sa2 + ca2*ca3**3*complex(0,1)*l5*sa1**4*sa2 + 3*ca1**3*ca2*ca3**2*complex(0,1)*l2*sa1*sa3 - 3*ca1**3*ca2*ca3**2*complex(0,1)*l3*sa1*sa3 - 3*ca1**3*ca2*ca3**2*complex(0,1)*l4*sa1*sa3 - 3*ca1**3*ca2*ca3**2*complex(0,1)*l5*sa1*sa3 + 2*ca1*ca2**3*ca3**2*complex(0,1)*l7*sa1*sa3 - 2*ca1*ca2**3*ca3**2*complex(0,1)*l8*sa1*sa3 - 3*ca1*ca2*ca3**2*complex(0,1)*l1*sa1**3*sa3 + 3*ca1*ca2*ca3**2*complex(0,1)*l3*sa1**3*sa3 + 3*ca1*ca2*ca3**2*complex(0,1)*l4*sa1**3*sa3 + 3*ca1*ca2*ca3**2*complex(0,1)*l5*sa1**3*sa3 + 6*ca1**3*ca2*ca3**2*complex(0,1)*l1*sa1*sa2**2*sa3 - 6*ca1**3*ca2*ca3**2*complex(0,1)*l3*sa1*sa2**2*sa3 - 6*ca1**3*ca2*ca3**2*complex(0,1)*l4*sa1*sa2**2*sa3 - 6*ca1**3*ca2*ca3**2*complex(0,1)*l5*sa1*sa2**2*sa3 - 4*ca1*ca2*ca3**2*complex(0,1)*l7*sa1*sa2**2*sa3 + 4*ca1*ca2*ca3**2*complex(0,1)*l8*sa1*sa2**2*sa3 - 6*ca1*ca2*ca3**2*complex(0,1)*l2*sa1**3*sa2**2*sa3 + 6*ca1*ca2*ca3**2*complex(0,1)*l3*sa1**3*sa2**2*sa3 + 6*ca1*ca2*ca3**2*complex(0,1)*l4*sa1**3*sa2**2*sa3 + 6*ca1*ca2*ca3**2*complex(0,1)*l5*sa1**3*sa2**2*sa3 - 2*ca1**4*ca2*ca3*complex(0,1)*l3*sa2*sa3**2 - 2*ca1**4*ca2*ca3*complex(0,1)*l4*sa2*sa3**2 - 2*ca1**4*ca2*ca3*complex(0,1)*l5*sa2*sa3**2 - 3*ca2**3*ca3*complex(0,1)*l6*sa2*sa3**2 + 3*ca1**2*ca2**3*ca3*complex(0,1)*l7*sa2*sa3**2 + 2*ca1**2*ca2*ca3*complex(0,1)*l8*sa2*sa3**2 - 6*ca1**2*ca2*ca3*complex(0,1)*l1*sa1**2*sa2*sa3**2 - 6*ca1**2*ca2*ca3*complex(0,1)*l2*sa1**2*sa2*sa3**2 + 8*ca1**2*ca2*ca3*complex(0,1)*l3*sa1**2*sa2*sa3**2 + 8*ca1**2*ca2*ca3*complex(0,1)*l4*sa1**2*sa2*sa3**2 + 8*ca1**2*ca2*ca3*complex(0,1)*l5*sa1**2*sa2*sa3**2 + 2*ca2*ca3*complex(0,1)*l7*sa1**2*sa2*sa3**2 + 3*ca2**3*ca3*complex(0,1)*l8*sa1**2*sa2*sa3**2 - 2*ca2*ca3*complex(0,1)*l3*sa1**4*sa2*sa3**2 - 2*ca2*ca3*complex(0,1)*l4*sa1**4*sa2*sa3**2 - 2*ca2*ca3*complex(0,1)*l5*sa1**4*sa2*sa3**2 + 3*ca1**4*ca2*ca3*complex(0,1)*l1*sa2**3*sa3**2 - 3*ca1**2*ca2*ca3*complex(0,1)*l7*sa2**3*sa3**2 + 6*ca1**2*ca2*ca3*complex(0,1)*l3*sa1**2*sa2**3*sa3**2 + 6*ca1**2*ca2*ca3*complex(0,1)*l4*sa1**2*sa2**3*sa3**2 + 6*ca1**2*ca2*ca3*complex(0,1)*l5*sa1**2*sa2**3*sa3**2 - 3*ca2*ca3*complex(0,1)*l8*sa1**2*sa2**3*sa3**2 + 3*ca2*ca3*complex(0,1)*l2*sa1**4*sa2**3*sa3**2 - ca1*ca2**3*complex(0,1)*l7*sa1*sa3**3 + ca1*ca2**3*complex(0,1)*l8*sa1*sa3**3 - 3*ca1**3*ca2*complex(0,1)*l1*sa1*sa2**2*sa3**3 + 3*ca1**3*ca2*complex(0,1)*l3*sa1*sa2**2*sa3**3 + 3*ca1**3*ca2*complex(0,1)*l4*sa1*sa2**2*sa3**3 + 3*ca1**3*ca2*complex(0,1)*l5*sa1*sa2**2*sa3**3 + 2*ca1*ca2*complex(0,1)*l7*sa1*sa2**2*sa3**3 - 2*ca1*ca2*complex(0,1)*l8*sa1*sa2**2*sa3**3 + 3*ca1*ca2*complex(0,1)*l2*sa1**3*sa2**2*sa3**3 - 3*ca1*ca2*complex(0,1)*l3*sa1**3*sa2**2*sa3**3 - 3*ca1*ca2*complex(0,1)*l4*sa1**3*sa2**2*sa3**3 - 3*ca1*ca2*complex(0,1)*l5*sa1**3*sa2**2*sa3**3',
                 order = {'QED':2})

GC_26 = Coupling(name = 'GC_26',
                 value = '-3*ca1**3*ca2*ca3**3*complex(0,1)*l2*sa1 + 3*ca1**3*ca2*ca3**3*complex(0,1)*l3*sa1 + 3*ca1**3*ca2*ca3**3*complex(0,1)*l4*sa1 + 3*ca1**3*ca2*ca3**3*complex(0,1)*l5*sa1 + 3*ca1*ca2*ca3**3*complex(0,1)*l1*sa1**3 - 3*ca1*ca2*ca3**3*complex(0,1)*l3*sa1**3 - 3*ca1*ca2*ca3**3*complex(0,1)*l4*sa1**3 - 3*ca1*ca2*ca3**3*complex(0,1)*l5*sa1**3 + 3*ca1**4*ca2*ca3**2*complex(0,1)*l3*sa2*sa3 + 3*ca1**4*ca2*ca3**2*complex(0,1)*l4*sa2*sa3 + 3*ca1**4*ca2*ca3**2*complex(0,1)*l5*sa2*sa3 - 3*ca1**2*ca2*ca3**2*complex(0,1)*l8*sa2*sa3 + 9*ca1**2*ca2*ca3**2*complex(0,1)*l1*sa1**2*sa2*sa3 + 9*ca1**2*ca2*ca3**2*complex(0,1)*l2*sa1**2*sa2*sa3 - 12*ca1**2*ca2*ca3**2*complex(0,1)*l3*sa1**2*sa2*sa3 - 12*ca1**2*ca2*ca3**2*complex(0,1)*l4*sa1**2*sa2*sa3 - 12*ca1**2*ca2*ca3**2*complex(0,1)*l5*sa1**2*sa2*sa3 - 3*ca2*ca3**2*complex(0,1)*l7*sa1**2*sa2*sa3 + 3*ca2*ca3**2*complex(0,1)*l3*sa1**4*sa2*sa3 + 3*ca2*ca3**2*complex(0,1)*l4*sa1**4*sa2*sa3 + 3*ca2*ca3**2*complex(0,1)*l5*sa1**4*sa2*sa3 + 3*ca1*ca2**3*ca3*complex(0,1)*l7*sa1*sa3**2 - 3*ca1*ca2**3*ca3*complex(0,1)*l8*sa1*sa3**2 + 9*ca1**3*ca2*ca3*complex(0,1)*l1*sa1*sa2**2*sa3**2 - 9*ca1**3*ca2*ca3*complex(0,1)*l3*sa1*sa2**2*sa3**2 - 9*ca1**3*ca2*ca3*complex(0,1)*l4*sa1*sa2**2*sa3**2 - 9*ca1**3*ca2*ca3*complex(0,1)*l5*sa1*sa2**2*sa3**2 - 6*ca1*ca2*ca3*complex(0,1)*l7*sa1*sa2**2*sa3**2 + 6*ca1*ca2*ca3*complex(0,1)*l8*sa1*sa2**2*sa3**2 - 9*ca1*ca2*ca3*complex(0,1)*l2*sa1**3*sa2**2*sa3**2 + 9*ca1*ca2*ca3*complex(0,1)*l3*sa1**3*sa2**2*sa3**2 + 9*ca1*ca2*ca3*complex(0,1)*l4*sa1**3*sa2**2*sa3**2 + 9*ca1*ca2*ca3*complex(0,1)*l5*sa1**3*sa2**2*sa3**2 - 3*ca2**3*complex(0,1)*l6*sa2*sa3**3 + 3*ca1**2*ca2**3*complex(0,1)*l7*sa2*sa3**3 + 3*ca2**3*complex(0,1)*l8*sa1**2*sa2*sa3**3 + 3*ca1**4*ca2*complex(0,1)*l1*sa2**3*sa3**3 - 3*ca1**2*ca2*complex(0,1)*l7*sa2**3*sa3**3 + 6*ca1**2*ca2*complex(0,1)*l3*sa1**2*sa2**3*sa3**3 + 6*ca1**2*ca2*complex(0,1)*l4*sa1**2*sa2**3*sa3**3 + 6*ca1**2*ca2*complex(0,1)*l5*sa1**2*sa2**3*sa3**3 - 3*ca2*complex(0,1)*l8*sa1**2*sa2**3*sa3**3 + 3*ca2*complex(0,1)*l2*sa1**4*sa2**3*sa3**3',
                 order = {'QED':2})

GC_27 = Coupling(name = 'GC_27',
                 value = '-3*ca2**4*ca3**4*complex(0,1)*l6 - 6*ca1**2*ca2**2*ca3**4*complex(0,1)*l7*sa2**2 - 6*ca2**2*ca3**4*complex(0,1)*l8*sa1**2*sa2**2 - 3*ca1**4*ca3**4*complex(0,1)*l1*sa2**4 - 6*ca1**2*ca3**4*complex(0,1)*l3*sa1**2*sa2**4 - 6*ca1**2*ca3**4*complex(0,1)*l4*sa1**2*sa2**4 - 6*ca1**2*ca3**4*complex(0,1)*l5*sa1**2*sa2**4 - 3*ca3**4*complex(0,1)*l2*sa1**4*sa2**4 + 12*ca1*ca2**2*ca3**3*complex(0,1)*l7*sa1*sa2*sa3 - 12*ca1*ca2**2*ca3**3*complex(0,1)*l8*sa1*sa2*sa3 + 12*ca1**3*ca3**3*complex(0,1)*l1*sa1*sa2**3*sa3 - 12*ca1**3*ca3**3*complex(0,1)*l3*sa1*sa2**3*sa3 - 12*ca1**3*ca3**3*complex(0,1)*l4*sa1*sa2**3*sa3 - 12*ca1**3*ca3**3*complex(0,1)*l5*sa1*sa2**3*sa3 - 12*ca1*ca3**3*complex(0,1)*l2*sa1**3*sa2**3*sa3 + 12*ca1*ca3**3*complex(0,1)*l3*sa1**3*sa2**3*sa3 + 12*ca1*ca3**3*complex(0,1)*l4*sa1**3*sa2**3*sa3 + 12*ca1*ca3**3*complex(0,1)*l5*sa1**3*sa2**3*sa3 - 6*ca1**2*ca2**2*ca3**2*complex(0,1)*l8*sa3**2 - 6*ca2**2*ca3**2*complex(0,1)*l7*sa1**2*sa3**2 - 6*ca1**4*ca3**2*complex(0,1)*l3*sa2**2*sa3**2 - 6*ca1**4*ca3**2*complex(0,1)*l4*sa2**2*sa3**2 - 6*ca1**4*ca3**2*complex(0,1)*l5*sa2**2*sa3**2 - 18*ca1**2*ca3**2*complex(0,1)*l1*sa1**2*sa2**2*sa3**2 - 18*ca1**2*ca3**2*complex(0,1)*l2*sa1**2*sa2**2*sa3**2 + 24*ca1**2*ca3**2*complex(0,1)*l3*sa1**2*sa2**2*sa3**2 + 24*ca1**2*ca3**2*complex(0,1)*l4*sa1**2*sa2**2*sa3**2 + 24*ca1**2*ca3**2*complex(0,1)*l5*sa1**2*sa2**2*sa3**2 - 6*ca3**2*complex(0,1)*l3*sa1**4*sa2**2*sa3**2 - 6*ca3**2*complex(0,1)*l4*sa1**4*sa2**2*sa3**2 - 6*ca3**2*complex(0,1)*l5*sa1**4*sa2**2*sa3**2 - 12*ca1**3*ca3*complex(0,1)*l2*sa1*sa2*sa3**3 + 12*ca1**3*ca3*complex(0,1)*l3*sa1*sa2*sa3**3 + 12*ca1**3*ca3*complex(0,1)*l4*sa1*sa2*sa3**3 + 12*ca1**3*ca3*complex(0,1)*l5*sa1*sa2*sa3**3 + 12*ca1*ca3*complex(0,1)*l1*sa1**3*sa2*sa3**3 - 12*ca1*ca3*complex(0,1)*l3*sa1**3*sa2*sa3**3 - 12*ca1*ca3*complex(0,1)*l4*sa1**3*sa2*sa3**3 - 12*ca1*ca3*complex(0,1)*l5*sa1**3*sa2*sa3**3 - 3*ca1**4*complex(0,1)*l2*sa3**4 - 6*ca1**2*complex(0,1)*l3*sa1**2*sa3**4 - 6*ca1**2*complex(0,1)*l4*sa1**2*sa3**4 - 6*ca1**2*complex(0,1)*l5*sa1**2*sa3**4 - 3*complex(0,1)*l1*sa1**4*sa3**4',
                 order = {'QED':2})

GC_28 = Coupling(name = 'GC_28',
                 value = '-3*ca1*ca2**2*ca3**4*complex(0,1)*l7*sa1*sa2 + 3*ca1*ca2**2*ca3**4*complex(0,1)*l8*sa1*sa2 - 3*ca1**3*ca3**4*complex(0,1)*l1*sa1*sa2**3 + 3*ca1**3*ca3**4*complex(0,1)*l3*sa1*sa2**3 + 3*ca1**3*ca3**4*complex(0,1)*l4*sa1*sa2**3 + 3*ca1**3*ca3**4*complex(0,1)*l5*sa1*sa2**3 + 3*ca1*ca3**4*complex(0,1)*l2*sa1**3*sa2**3 - 3*ca1*ca3**4*complex(0,1)*l3*sa1**3*sa2**3 - 3*ca1*ca3**4*complex(0,1)*l4*sa1**3*sa2**3 - 3*ca1*ca3**4*complex(0,1)*l5*sa1**3*sa2**3 - 3*ca2**4*ca3**3*complex(0,1)*l6*sa3 + 3*ca1**2*ca2**2*ca3**3*complex(0,1)*l8*sa3 + 3*ca2**2*ca3**3*complex(0,1)*l7*sa1**2*sa3 + 3*ca1**4*ca3**3*complex(0,1)*l3*sa2**2*sa3 + 3*ca1**4*ca3**3*complex(0,1)*l4*sa2**2*sa3 + 3*ca1**4*ca3**3*complex(0,1)*l5*sa2**2*sa3 - 6*ca1**2*ca2**2*ca3**3*complex(0,1)*l7*sa2**2*sa3 + 9*ca1**2*ca3**3*complex(0,1)*l1*sa1**2*sa2**2*sa3 + 9*ca1**2*ca3**3*complex(0,1)*l2*sa1**2*sa2**2*sa3 - 12*ca1**2*ca3**3*complex(0,1)*l3*sa1**2*sa2**2*sa3 - 12*ca1**2*ca3**3*complex(0,1)*l4*sa1**2*sa2**2*sa3 - 12*ca1**2*ca3**3*complex(0,1)*l5*sa1**2*sa2**2*sa3 - 6*ca2**2*ca3**3*complex(0,1)*l8*sa1**2*sa2**2*sa3 + 3*ca3**3*complex(0,1)*l3*sa1**4*sa2**2*sa3 + 3*ca3**3*complex(0,1)*l4*sa1**4*sa2**2*sa3 + 3*ca3**3*complex(0,1)*l5*sa1**4*sa2**2*sa3 - 3*ca1**4*ca3**3*complex(0,1)*l1*sa2**4*sa3 - 6*ca1**2*ca3**3*complex(0,1)*l3*sa1**2*sa2**4*sa3 - 6*ca1**2*ca3**3*complex(0,1)*l4*sa1**2*sa2**4*sa3 - 6*ca1**2*ca3**3*complex(0,1)*l5*sa1**2*sa2**4*sa3 - 3*ca3**3*complex(0,1)*l2*sa1**4*sa2**4*sa3 + 9*ca1**3*ca3**2*complex(0,1)*l2*sa1*sa2*sa3**2 - 9*ca1**3*ca3**2*complex(0,1)*l3*sa1*sa2*sa3**2 - 9*ca1**3*ca3**2*complex(0,1)*l4*sa1*sa2*sa3**2 - 9*ca1**3*ca3**2*complex(0,1)*l5*sa1*sa2*sa3**2 + 9*ca1*ca2**2*ca3**2*complex(0,1)*l7*sa1*sa2*sa3**2 - 9*ca1*ca2**2*ca3**2*complex(0,1)*l8*sa1*sa2*sa3**2 - 9*ca1*ca3**2*complex(0,1)*l1*sa1**3*sa2*sa3**2 + 9*ca1*ca3**2*complex(0,1)*l3*sa1**3*sa2*sa3**2 + 9*ca1*ca3**2*complex(0,1)*l4*sa1**3*sa2*sa3**2 + 9*ca1*ca3**2*complex(0,1)*l5*sa1**3*sa2*sa3**2 + 9*ca1**3*ca3**2*complex(0,1)*l1*sa1*sa2**3*sa3**2 - 9*ca1**3*ca3**2*complex(0,1)*l3*sa1*sa2**3*sa3**2 - 9*ca1**3*ca3**2*complex(0,1)*l4*sa1*sa2**3*sa3**2 - 9*ca1**3*ca3**2*complex(0,1)*l5*sa1*sa2**3*sa3**2 - 9*ca1*ca3**2*complex(0,1)*l2*sa1**3*sa2**3*sa3**2 + 9*ca1*ca3**2*complex(0,1)*l3*sa1**3*sa2**3*sa3**2 + 9*ca1*ca3**2*complex(0,1)*l4*sa1**3*sa2**3*sa3**2 + 9*ca1*ca3**2*complex(0,1)*l5*sa1**3*sa2**3*sa3**2 + 3*ca1**4*ca3*complex(0,1)*l2*sa3**3 - 3*ca1**2*ca2**2*ca3*complex(0,1)*l8*sa3**3 + 6*ca1**2*ca3*complex(0,1)*l3*sa1**2*sa3**3 + 6*ca1**2*ca3*complex(0,1)*l4*sa1**2*sa3**3 + 6*ca1**2*ca3*complex(0,1)*l5*sa1**2*sa3**3 - 3*ca2**2*ca3*complex(0,1)*l7*sa1**2*sa3**3 + 3*ca3*complex(0,1)*l1*sa1**4*sa3**3 - 3*ca1**4*ca3*complex(0,1)*l3*sa2**2*sa3**3 - 3*ca1**4*ca3*complex(0,1)*l4*sa2**2*sa3**3 - 3*ca1**4*ca3*complex(0,1)*l5*sa2**2*sa3**3 - 9*ca1**2*ca3*complex(0,1)*l1*sa1**2*sa2**2*sa3**3 - 9*ca1**2*ca3*complex(0,1)*l2*sa1**2*sa2**2*sa3**3 + 12*ca1**2*ca3*complex(0,1)*l3*sa1**2*sa2**2*sa3**3 + 12*ca1**2*ca3*complex(0,1)*l4*sa1**2*sa2**2*sa3**3 + 12*ca1**2*ca3*complex(0,1)*l5*sa1**2*sa2**2*sa3**3 - 3*ca3*complex(0,1)*l3*sa1**4*sa2**2*sa3**3 - 3*ca3*complex(0,1)*l4*sa1**4*sa2**2*sa3**3 - 3*ca3*complex(0,1)*l5*sa1**4*sa2**2*sa3**3 - 3*ca1**3*complex(0,1)*l2*sa1*sa2*sa3**4 + 3*ca1**3*complex(0,1)*l3*sa1*sa2*sa3**4 + 3*ca1**3*complex(0,1)*l4*sa1*sa2*sa3**4 + 3*ca1**3*complex(0,1)*l5*sa1*sa2*sa3**4 + 3*ca1*complex(0,1)*l1*sa1**3*sa2*sa3**4 - 3*ca1*complex(0,1)*l3*sa1**3*sa2*sa3**4 - 3*ca1*complex(0,1)*l4*sa1**3*sa2*sa3**4 - 3*ca1*complex(0,1)*l5*sa1**3*sa2*sa3**4',
                 order = {'QED':2})

GC_29 = Coupling(name = 'GC_29',
                 value = '-(ca1**2*ca2**2*ca3**4*complex(0,1)*l8) - ca2**2*ca3**4*complex(0,1)*l7*sa1**2 - ca1**4*ca3**4*complex(0,1)*l3*sa2**2 - ca1**4*ca3**4*complex(0,1)*l4*sa2**2 - ca1**4*ca3**4*complex(0,1)*l5*sa2**2 - 3*ca1**2*ca3**4*complex(0,1)*l1*sa1**2*sa2**2 - 3*ca1**2*ca3**4*complex(0,1)*l2*sa1**2*sa2**2 + 4*ca1**2*ca3**4*complex(0,1)*l3*sa1**2*sa2**2 + 4*ca1**2*ca3**4*complex(0,1)*l4*sa1**2*sa2**2 + 4*ca1**2*ca3**4*complex(0,1)*l5*sa1**2*sa2**2 - ca3**4*complex(0,1)*l3*sa1**4*sa2**2 - ca3**4*complex(0,1)*l4*sa1**4*sa2**2 - ca3**4*complex(0,1)*l5*sa1**4*sa2**2 - 6*ca1**3*ca3**3*complex(0,1)*l2*sa1*sa2*sa3 + 6*ca1**3*ca3**3*complex(0,1)*l3*sa1*sa2*sa3 + 6*ca1**3*ca3**3*complex(0,1)*l4*sa1*sa2*sa3 + 6*ca1**3*ca3**3*complex(0,1)*l5*sa1*sa2*sa3 - 6*ca1*ca2**2*ca3**3*complex(0,1)*l7*sa1*sa2*sa3 + 6*ca1*ca2**2*ca3**3*complex(0,1)*l8*sa1*sa2*sa3 + 6*ca1*ca3**3*complex(0,1)*l1*sa1**3*sa2*sa3 - 6*ca1*ca3**3*complex(0,1)*l3*sa1**3*sa2*sa3 - 6*ca1*ca3**3*complex(0,1)*l4*sa1**3*sa2*sa3 - 6*ca1*ca3**3*complex(0,1)*l5*sa1**3*sa2*sa3 - 6*ca1**3*ca3**3*complex(0,1)*l1*sa1*sa2**3*sa3 + 6*ca1**3*ca3**3*complex(0,1)*l3*sa1*sa2**3*sa3 + 6*ca1**3*ca3**3*complex(0,1)*l4*sa1*sa2**3*sa3 + 6*ca1**3*ca3**3*complex(0,1)*l5*sa1*sa2**3*sa3 + 6*ca1*ca3**3*complex(0,1)*l2*sa1**3*sa2**3*sa3 - 6*ca1*ca3**3*complex(0,1)*l3*sa1**3*sa2**3*sa3 - 6*ca1*ca3**3*complex(0,1)*l4*sa1**3*sa2**3*sa3 - 6*ca1*ca3**3*complex(0,1)*l5*sa1**3*sa2**3*sa3 - 3*ca1**4*ca3**2*complex(0,1)*l2*sa3**2 - 3*ca2**4*ca3**2*complex(0,1)*l6*sa3**2 + 4*ca1**2*ca2**2*ca3**2*complex(0,1)*l8*sa3**2 - 6*ca1**2*ca3**2*complex(0,1)*l3*sa1**2*sa3**2 - 6*ca1**2*ca3**2*complex(0,1)*l4*sa1**2*sa3**2 - 6*ca1**2*ca3**2*complex(0,1)*l5*sa1**2*sa3**2 + 4*ca2**2*ca3**2*complex(0,1)*l7*sa1**2*sa3**2 - 3*ca3**2*complex(0,1)*l1*sa1**4*sa3**2 + 4*ca1**4*ca3**2*complex(0,1)*l3*sa2**2*sa3**2 + 4*ca1**4*ca3**2*complex(0,1)*l4*sa2**2*sa3**2 + 4*ca1**4*ca3**2*complex(0,1)*l5*sa2**2*sa3**2 - 6*ca1**2*ca2**2*ca3**2*complex(0,1)*l7*sa2**2*sa3**2 + 12*ca1**2*ca3**2*complex(0,1)*l1*sa1**2*sa2**2*sa3**2 + 12*ca1**2*ca3**2*complex(0,1)*l2*sa1**2*sa2**2*sa3**2 - 16*ca1**2*ca3**2*complex(0,1)*l3*sa1**2*sa2**2*sa3**2 - 16*ca1**2*ca3**2*complex(0,1)*l4*sa1**2*sa2**2*sa3**2 - 16*ca1**2*ca3**2*complex(0,1)*l5*sa1**2*sa2**2*sa3**2 - 6*ca2**2*ca3**2*complex(0,1)*l8*sa1**2*sa2**2*sa3**2 + 4*ca3**2*complex(0,1)*l3*sa1**4*sa2**2*sa3**2 + 4*ca3**2*complex(0,1)*l4*sa1**4*sa2**2*sa3**2 + 4*ca3**2*complex(0,1)*l5*sa1**4*sa2**2*sa3**2 - 3*ca1**4*ca3**2*complex(0,1)*l1*sa2**4*sa3**2 - 6*ca1**2*ca3**2*complex(0,1)*l3*sa1**2*sa2**4*sa3**2 - 6*ca1**2*ca3**2*complex(0,1)*l4*sa1**2*sa2**4*sa3**2 - 6*ca1**2*ca3**2*complex(0,1)*l5*sa1**2*sa2**4*sa3**2 - 3*ca3**2*complex(0,1)*l2*sa1**4*sa2**4*sa3**2 + 6*ca1**3*ca3*complex(0,1)*l2*sa1*sa2*sa3**3 - 6*ca1**3*ca3*complex(0,1)*l3*sa1*sa2*sa3**3 - 6*ca1**3*ca3*complex(0,1)*l4*sa1*sa2*sa3**3 - 6*ca1**3*ca3*complex(0,1)*l5*sa1*sa2*sa3**3 + 6*ca1*ca2**2*ca3*complex(0,1)*l7*sa1*sa2*sa3**3 - 6*ca1*ca2**2*ca3*complex(0,1)*l8*sa1*sa2*sa3**3 - 6*ca1*ca3*complex(0,1)*l1*sa1**3*sa2*sa3**3 + 6*ca1*ca3*complex(0,1)*l3*sa1**3*sa2*sa3**3 + 6*ca1*ca3*complex(0,1)*l4*sa1**3*sa2*sa3**3 + 6*ca1*ca3*complex(0,1)*l5*sa1**3*sa2*sa3**3 + 6*ca1**3*ca3*complex(0,1)*l1*sa1*sa2**3*sa3**3 - 6*ca1**3*ca3*complex(0,1)*l3*sa1*sa2**3*sa3**3 - 6*ca1**3*ca3*complex(0,1)*l4*sa1*sa2**3*sa3**3 - 6*ca1**3*ca3*complex(0,1)*l5*sa1*sa2**3*sa3**3 - 6*ca1*ca3*complex(0,1)*l2*sa1**3*sa2**3*sa3**3 + 6*ca1*ca3*complex(0,1)*l3*sa1**3*sa2**3*sa3**3 + 6*ca1*ca3*complex(0,1)*l4*sa1**3*sa2**3*sa3**3 + 6*ca1*ca3*complex(0,1)*l5*sa1**3*sa2**3*sa3**3 - ca1**2*ca2**2*complex(0,1)*l8*sa3**4 - ca2**2*complex(0,1)*l7*sa1**2*sa3**4 - ca1**4*complex(0,1)*l3*sa2**2*sa3**4 - ca1**4*complex(0,1)*l4*sa2**2*sa3**4 - ca1**4*complex(0,1)*l5*sa2**2*sa3**4 - 3*ca1**2*complex(0,1)*l1*sa1**2*sa2**2*sa3**4 - 3*ca1**2*complex(0,1)*l2*sa1**2*sa2**2*sa3**4 + 4*ca1**2*complex(0,1)*l3*sa1**2*sa2**2*sa3**4 + 4*ca1**2*complex(0,1)*l4*sa1**2*sa2**2*sa3**4 + 4*ca1**2*complex(0,1)*l5*sa1**2*sa2**2*sa3**4 - complex(0,1)*l3*sa1**4*sa2**2*sa3**4 - complex(0,1)*l4*sa1**4*sa2**2*sa3**4 - complex(0,1)*l5*sa1**4*sa2**2*sa3**4',
                 order = {'QED':2})

GC_30 = Coupling(name = 'GC_30',
                 value = '3*ca1**3*ca3**4*complex(0,1)*l2*sa1*sa2 - 3*ca1**3*ca3**4*complex(0,1)*l3*sa1*sa2 - 3*ca1**3*ca3**4*complex(0,1)*l4*sa1*sa2 - 3*ca1**3*ca3**4*complex(0,1)*l5*sa1*sa2 - 3*ca1*ca3**4*complex(0,1)*l1*sa1**3*sa2 + 3*ca1*ca3**4*complex(0,1)*l3*sa1**3*sa2 + 3*ca1*ca3**4*complex(0,1)*l4*sa1**3*sa2 + 3*ca1*ca3**4*complex(0,1)*l5*sa1**3*sa2 + 3*ca1**4*ca3**3*complex(0,1)*l2*sa3 - 3*ca1**2*ca2**2*ca3**3*complex(0,1)*l8*sa3 + 6*ca1**2*ca3**3*complex(0,1)*l3*sa1**2*sa3 + 6*ca1**2*ca3**3*complex(0,1)*l4*sa1**2*sa3 + 6*ca1**2*ca3**3*complex(0,1)*l5*sa1**2*sa3 - 3*ca2**2*ca3**3*complex(0,1)*l7*sa1**2*sa3 + 3*ca3**3*complex(0,1)*l1*sa1**4*sa3 - 3*ca1**4*ca3**3*complex(0,1)*l3*sa2**2*sa3 - 3*ca1**4*ca3**3*complex(0,1)*l4*sa2**2*sa3 - 3*ca1**4*ca3**3*complex(0,1)*l5*sa2**2*sa3 - 9*ca1**2*ca3**3*complex(0,1)*l1*sa1**2*sa2**2*sa3 - 9*ca1**2*ca3**3*complex(0,1)*l2*sa1**2*sa2**2*sa3 + 12*ca1**2*ca3**3*complex(0,1)*l3*sa1**2*sa2**2*sa3 + 12*ca1**2*ca3**3*complex(0,1)*l4*sa1**2*sa2**2*sa3 + 12*ca1**2*ca3**3*complex(0,1)*l5*sa1**2*sa2**2*sa3 - 3*ca3**3*complex(0,1)*l3*sa1**4*sa2**2*sa3 - 3*ca3**3*complex(0,1)*l4*sa1**4*sa2**2*sa3 - 3*ca3**3*complex(0,1)*l5*sa1**4*sa2**2*sa3 - 9*ca1**3*ca3**2*complex(0,1)*l2*sa1*sa2*sa3**2 + 9*ca1**3*ca3**2*complex(0,1)*l3*sa1*sa2*sa3**2 + 9*ca1**3*ca3**2*complex(0,1)*l4*sa1*sa2*sa3**2 + 9*ca1**3*ca3**2*complex(0,1)*l5*sa1*sa2*sa3**2 - 9*ca1*ca2**2*ca3**2*complex(0,1)*l7*sa1*sa2*sa3**2 + 9*ca1*ca2**2*ca3**2*complex(0,1)*l8*sa1*sa2*sa3**2 + 9*ca1*ca3**2*complex(0,1)*l1*sa1**3*sa2*sa3**2 - 9*ca1*ca3**2*complex(0,1)*l3*sa1**3*sa2*sa3**2 - 9*ca1*ca3**2*complex(0,1)*l4*sa1**3*sa2*sa3**2 - 9*ca1*ca3**2*complex(0,1)*l5*sa1**3*sa2*sa3**2 - 9*ca1**3*ca3**2*complex(0,1)*l1*sa1*sa2**3*sa3**2 + 9*ca1**3*ca3**2*complex(0,1)*l3*sa1*sa2**3*sa3**2 + 9*ca1**3*ca3**2*complex(0,1)*l4*sa1*sa2**3*sa3**2 + 9*ca1**3*ca3**2*complex(0,1)*l5*sa1*sa2**3*sa3**2 + 9*ca1*ca3**2*complex(0,1)*l2*sa1**3*sa2**3*sa3**2 - 9*ca1*ca3**2*complex(0,1)*l3*sa1**3*sa2**3*sa3**2 - 9*ca1*ca3**2*complex(0,1)*l4*sa1**3*sa2**3*sa3**2 - 9*ca1*ca3**2*complex(0,1)*l5*sa1**3*sa2**3*sa3**2 - 3*ca2**4*ca3*complex(0,1)*l6*sa3**3 + 3*ca1**2*ca2**2*ca3*complex(0,1)*l8*sa3**3 + 3*ca2**2*ca3*complex(0,1)*l7*sa1**2*sa3**3 + 3*ca1**4*ca3*complex(0,1)*l3*sa2**2*sa3**3 + 3*ca1**4*ca3*complex(0,1)*l4*sa2**2*sa3**3 + 3*ca1**4*ca3*complex(0,1)*l5*sa2**2*sa3**3 - 6*ca1**2*ca2**2*ca3*complex(0,1)*l7*sa2**2*sa3**3 + 9*ca1**2*ca3*complex(0,1)*l1*sa1**2*sa2**2*sa3**3 + 9*ca1**2*ca3*complex(0,1)*l2*sa1**2*sa2**2*sa3**3 - 12*ca1**2*ca3*complex(0,1)*l3*sa1**2*sa2**2*sa3**3 - 12*ca1**2*ca3*complex(0,1)*l4*sa1**2*sa2**2*sa3**3 - 12*ca1**2*ca3*complex(0,1)*l5*sa1**2*sa2**2*sa3**3 - 6*ca2**2*ca3*complex(0,1)*l8*sa1**2*sa2**2*sa3**3 + 3*ca3*complex(0,1)*l3*sa1**4*sa2**2*sa3**3 + 3*ca3*complex(0,1)*l4*sa1**4*sa2**2*sa3**3 + 3*ca3*complex(0,1)*l5*sa1**4*sa2**2*sa3**3 - 3*ca1**4*ca3*complex(0,1)*l1*sa2**4*sa3**3 - 6*ca1**2*ca3*complex(0,1)*l3*sa1**2*sa2**4*sa3**3 - 6*ca1**2*ca3*complex(0,1)*l4*sa1**2*sa2**4*sa3**3 - 6*ca1**2*ca3*complex(0,1)*l5*sa1**2*sa2**4*sa3**3 - 3*ca3*complex(0,1)*l2*sa1**4*sa2**4*sa3**3 + 3*ca1*ca2**2*complex(0,1)*l7*sa1*sa2*sa3**4 - 3*ca1*ca2**2*complex(0,1)*l8*sa1*sa2*sa3**4 + 3*ca1**3*complex(0,1)*l1*sa1*sa2**3*sa3**4 - 3*ca1**3*complex(0,1)*l3*sa1*sa2**3*sa3**4 - 3*ca1**3*complex(0,1)*l4*sa1*sa2**3*sa3**4 - 3*ca1**3*complex(0,1)*l5*sa1*sa2**3*sa3**4 - 3*ca1*complex(0,1)*l2*sa1**3*sa2**3*sa3**4 + 3*ca1*complex(0,1)*l3*sa1**3*sa2**3*sa3**4 + 3*ca1*complex(0,1)*l4*sa1**3*sa2**3*sa3**4 + 3*ca1*complex(0,1)*l5*sa1**3*sa2**3*sa3**4',
                 order = {'QED':2})

GC_31 = Coupling(name = 'GC_31',
                 value = '-3*ca1**4*ca3**4*complex(0,1)*l2 - 6*ca1**2*ca3**4*complex(0,1)*l3*sa1**2 - 6*ca1**2*ca3**4*complex(0,1)*l4*sa1**2 - 6*ca1**2*ca3**4*complex(0,1)*l5*sa1**2 - 3*ca3**4*complex(0,1)*l1*sa1**4 + 12*ca1**3*ca3**3*complex(0,1)*l2*sa1*sa2*sa3 - 12*ca1**3*ca3**3*complex(0,1)*l3*sa1*sa2*sa3 - 12*ca1**3*ca3**3*complex(0,1)*l4*sa1*sa2*sa3 - 12*ca1**3*ca3**3*complex(0,1)*l5*sa1*sa2*sa3 - 12*ca1*ca3**3*complex(0,1)*l1*sa1**3*sa2*sa3 + 12*ca1*ca3**3*complex(0,1)*l3*sa1**3*sa2*sa3 + 12*ca1*ca3**3*complex(0,1)*l4*sa1**3*sa2*sa3 + 12*ca1*ca3**3*complex(0,1)*l5*sa1**3*sa2*sa3 - 6*ca1**2*ca2**2*ca3**2*complex(0,1)*l8*sa3**2 - 6*ca2**2*ca3**2*complex(0,1)*l7*sa1**2*sa3**2 - 6*ca1**4*ca3**2*complex(0,1)*l3*sa2**2*sa3**2 - 6*ca1**4*ca3**2*complex(0,1)*l4*sa2**2*sa3**2 - 6*ca1**4*ca3**2*complex(0,1)*l5*sa2**2*sa3**2 - 18*ca1**2*ca3**2*complex(0,1)*l1*sa1**2*sa2**2*sa3**2 - 18*ca1**2*ca3**2*complex(0,1)*l2*sa1**2*sa2**2*sa3**2 + 24*ca1**2*ca3**2*complex(0,1)*l3*sa1**2*sa2**2*sa3**2 + 24*ca1**2*ca3**2*complex(0,1)*l4*sa1**2*sa2**2*sa3**2 + 24*ca1**2*ca3**2*complex(0,1)*l5*sa1**2*sa2**2*sa3**2 - 6*ca3**2*complex(0,1)*l3*sa1**4*sa2**2*sa3**2 - 6*ca3**2*complex(0,1)*l4*sa1**4*sa2**2*sa3**2 - 6*ca3**2*complex(0,1)*l5*sa1**4*sa2**2*sa3**2 - 12*ca1*ca2**2*ca3*complex(0,1)*l7*sa1*sa2*sa3**3 + 12*ca1*ca2**2*ca3*complex(0,1)*l8*sa1*sa2*sa3**3 - 12*ca1**3*ca3*complex(0,1)*l1*sa1*sa2**3*sa3**3 + 12*ca1**3*ca3*complex(0,1)*l3*sa1*sa2**3*sa3**3 + 12*ca1**3*ca3*complex(0,1)*l4*sa1*sa2**3*sa3**3 + 12*ca1**3*ca3*complex(0,1)*l5*sa1*sa2**3*sa3**3 + 12*ca1*ca3*complex(0,1)*l2*sa1**3*sa2**3*sa3**3 - 12*ca1*ca3*complex(0,1)*l3*sa1**3*sa2**3*sa3**3 - 12*ca1*ca3*complex(0,1)*l4*sa1**3*sa2**3*sa3**3 - 12*ca1*ca3*complex(0,1)*l5*sa1**3*sa2**3*sa3**3 - 3*ca2**4*complex(0,1)*l6*sa3**4 - 6*ca1**2*ca2**2*complex(0,1)*l7*sa2**2*sa3**4 - 6*ca2**2*complex(0,1)*l8*sa1**2*sa2**2*sa3**4 - 3*ca1**4*complex(0,1)*l1*sa2**4*sa3**4 - 6*ca1**2*complex(0,1)*l3*sa1**2*sa2**4*sa3**4 - 6*ca1**2*complex(0,1)*l4*sa1**2*sa2**4*sa3**4 - 6*ca1**2*complex(0,1)*l5*sa1**2*sa2**4*sa3**4 - 3*complex(0,1)*l2*sa1**4*sa2**4*sa3**4',
                 order = {'QED':2})

GC_32 = Coupling(name = 'GC_32',
                 value = '(ca2*cb*ee**2*complex(0,1)*sa1)/(2.*cw) - (ca1*ca2*ee**2*complex(0,1)*sb)/(2.*cw)',
                 order = {'QED':2})

GC_33 = Coupling(name = 'GC_33',
                 value = '-(ca2*cb*ee**2*complex(0,1)*sa1)/(2.*cw) + (ca1*ca2*ee**2*complex(0,1)*sb)/(2.*cw)',
                 order = {'QED':2})

GC_34 = Coupling(name = 'GC_34',
                 value = '-(ca1*ca2*cb*ee**2*complex(0,1))/(2.*cw) - (ca2*ee**2*complex(0,1)*sa1*sb)/(2.*cw)',
                 order = {'QED':2})

GC_35 = Coupling(name = 'GC_35',
                 value = '(ca1*ca2*cb*ee**2*complex(0,1))/(2.*cw) + (ca2*ee**2*complex(0,1)*sa1*sb)/(2.*cw)',
                 order = {'QED':2})

GC_36 = Coupling(name = 'GC_36',
                 value = '-(ca1*ca3*cb*ee**2*complex(0,1)*sa2)/(2.*cw) + (cb*ee**2*complex(0,1)*sa1*sa3)/(2.*cw) - (ca3*ee**2*complex(0,1)*sa1*sa2*sb)/(2.*cw) - (ca1*ee**2*complex(0,1)*sa3*sb)/(2.*cw)',
                 order = {'QED':2})

GC_37 = Coupling(name = 'GC_37',
                 value = '(ca1*ca3*cb*ee**2*complex(0,1)*sa2)/(2.*cw) - (cb*ee**2*complex(0,1)*sa1*sa3)/(2.*cw) + (ca3*ee**2*complex(0,1)*sa1*sa2*sb)/(2.*cw) + (ca1*ee**2*complex(0,1)*sa3*sb)/(2.*cw)',
                 order = {'QED':2})

GC_38 = Coupling(name = 'GC_38',
                 value = '-(ca3*cb*ee**2*complex(0,1)*sa1*sa2)/(2.*cw) - (ca1*cb*ee**2*complex(0,1)*sa3)/(2.*cw) + (ca1*ca3*ee**2*complex(0,1)*sa2*sb)/(2.*cw) - (ee**2*complex(0,1)*sa1*sa3*sb)/(2.*cw)',
                 order = {'QED':2})

GC_39 = Coupling(name = 'GC_39',
                 value = '(ca3*cb*ee**2*complex(0,1)*sa1*sa2)/(2.*cw) + (ca1*cb*ee**2*complex(0,1)*sa3)/(2.*cw) - (ca1*ca3*ee**2*complex(0,1)*sa2*sb)/(2.*cw) + (ee**2*complex(0,1)*sa1*sa3*sb)/(2.*cw)',
                 order = {'QED':2})

GC_40 = Coupling(name = 'GC_40',
                 value = '-(ca1*ca3*cb*ee**2*complex(0,1))/(2.*cw) + (cb*ee**2*complex(0,1)*sa1*sa2*sa3)/(2.*cw) - (ca3*ee**2*complex(0,1)*sa1*sb)/(2.*cw) - (ca1*ee**2*complex(0,1)*sa2*sa3*sb)/(2.*cw)',
                 order = {'QED':2})

GC_41 = Coupling(name = 'GC_41',
                 value = '(ca1*ca3*cb*ee**2*complex(0,1))/(2.*cw) - (cb*ee**2*complex(0,1)*sa1*sa2*sa3)/(2.*cw) + (ca3*ee**2*complex(0,1)*sa1*sb)/(2.*cw) + (ca1*ee**2*complex(0,1)*sa2*sa3*sb)/(2.*cw)',
                 order = {'QED':2})

GC_42 = Coupling(name = 'GC_42',
                 value = '-(ca3*cb*ee**2*complex(0,1)*sa1)/(2.*cw) - (ca1*cb*ee**2*complex(0,1)*sa2*sa3)/(2.*cw) + (ca1*ca3*ee**2*complex(0,1)*sb)/(2.*cw) - (ee**2*complex(0,1)*sa1*sa2*sa3*sb)/(2.*cw)',
                 order = {'QED':2})

GC_43 = Coupling(name = 'GC_43',
                 value = '(ca3*cb*ee**2*complex(0,1)*sa1)/(2.*cw) + (ca1*cb*ee**2*complex(0,1)*sa2*sa3)/(2.*cw) - (ca1*ca3*ee**2*complex(0,1)*sb)/(2.*cw) + (ee**2*complex(0,1)*sa1*sa2*sa3*sb)/(2.*cw)',
                 order = {'QED':2})

GC_44 = Coupling(name = 'GC_44',
                 value = '-(cb**2*ee*complex(0,1)) - ee*complex(0,1)*sb**2',
                 order = {'QED':1})

GC_45 = Coupling(name = 'GC_45',
                 value = 'cb**2*ee*complex(0,1) + ee*complex(0,1)*sb**2',
                 order = {'QED':1})

GC_46 = Coupling(name = 'GC_46',
                 value = '-(cb**2*ee**2*complex(0,1)) - ee**2*complex(0,1)*sb**2',
                 order = {'QED':2})

GC_47 = Coupling(name = 'GC_47',
                 value = '-2*cb**2*ee**2*complex(0,1) - 2*ee**2*complex(0,1)*sb**2',
                 order = {'QED':2})

GC_48 = Coupling(name = 'GC_48',
                 value = '2*cb**2*ee**2*complex(0,1) + 2*ee**2*complex(0,1)*sb**2',
                 order = {'QED':2})

GC_49 = Coupling(name = 'GC_49',
                 value = '-(cb**2*ee**2)/(2.*cw) - (ee**2*sb**2)/(2.*cw)',
                 order = {'QED':2})

GC_50 = Coupling(name = 'GC_50',
                 value = '(cb**2*ee**2)/(2.*cw) + (ee**2*sb**2)/(2.*cw)',
                 order = {'QED':2})

GC_51 = Coupling(name = 'GC_51',
                 value = '-(ca1*ca2**2*cb**2*complex(0,1)*l4*sa1) - ca1*ca2**2*cb**2*complex(0,1)*l5*sa1 + ca1**2*ca2**2*cb*complex(0,1)*l1*sb - ca1**2*ca2**2*cb*complex(0,1)*l3*sb - ca2**2*cb*complex(0,1)*l2*sa1**2*sb + ca2**2*cb*complex(0,1)*l3*sa1**2*sb + cb*complex(0,1)*l7*sa2**2*sb - cb*complex(0,1)*l8*sa2**2*sb + ca1*ca2**2*complex(0,1)*l4*sa1*sb**2 + ca1*ca2**2*complex(0,1)*l5*sa1*sb**2',
                 order = {'QED':2})

GC_52 = Coupling(name = 'GC_52',
                 value = '-2*ca1*ca2**2*cb**2*complex(0,1)*l5*sa1 + ca1**2*ca2**2*cb*complex(0,1)*l1*sb - ca1**2*ca2**2*cb*complex(0,1)*l3*sb - ca1**2*ca2**2*cb*complex(0,1)*l4*sb + ca1**2*ca2**2*cb*complex(0,1)*l5*sb - ca2**2*cb*complex(0,1)*l2*sa1**2*sb + ca2**2*cb*complex(0,1)*l3*sa1**2*sb + ca2**2*cb*complex(0,1)*l4*sa1**2*sb - ca2**2*cb*complex(0,1)*l5*sa1**2*sb + cb*complex(0,1)*l7*sa2**2*sb - cb*complex(0,1)*l8*sa2**2*sb + 2*ca1*ca2**2*complex(0,1)*l5*sa1*sb**2',
                 order = {'QED':2})

GC_53 = Coupling(name = 'GC_53',
                 value = '-(ca1**2*ca2**2*cb**2*complex(0,1)*l3) - ca2**2*cb**2*complex(0,1)*l2*sa1**2 - cb**2*complex(0,1)*l8*sa2**2 + 2*ca1*ca2**2*cb*complex(0,1)*l4*sa1*sb + 2*ca1*ca2**2*cb*complex(0,1)*l5*sa1*sb - ca1**2*ca2**2*complex(0,1)*l1*sb**2 - ca2**2*complex(0,1)*l3*sa1**2*sb**2 - complex(0,1)*l7*sa2**2*sb**2',
                 order = {'QED':2})

GC_54 = Coupling(name = 'GC_54',
                 value = '-(ca1**2*ca2**2*cb**2*complex(0,1)*l3) - ca1**2*ca2**2*cb**2*complex(0,1)*l4 + ca1**2*ca2**2*cb**2*complex(0,1)*l5 - ca2**2*cb**2*complex(0,1)*l2*sa1**2 - cb**2*complex(0,1)*l8*sa2**2 + 4*ca1*ca2**2*cb*complex(0,1)*l5*sa1*sb - ca1**2*ca2**2*complex(0,1)*l1*sb**2 - ca2**2*complex(0,1)*l3*sa1**2*sb**2 - ca2**2*complex(0,1)*l4*sa1**2*sb**2 + ca2**2*complex(0,1)*l5*sa1**2*sb**2 - complex(0,1)*l7*sa2**2*sb**2',
                 order = {'QED':2})

GC_55 = Coupling(name = 'GC_55',
                 value = '-(ca1**2*ca2**2*cb**2*complex(0,1)*l1) - ca2**2*cb**2*complex(0,1)*l3*sa1**2 - cb**2*complex(0,1)*l7*sa2**2 - 2*ca1*ca2**2*cb*complex(0,1)*l4*sa1*sb - 2*ca1*ca2**2*cb*complex(0,1)*l5*sa1*sb - ca1**2*ca2**2*complex(0,1)*l3*sb**2 - ca2**2*complex(0,1)*l2*sa1**2*sb**2 - complex(0,1)*l8*sa2**2*sb**2',
                 order = {'QED':2})

GC_56 = Coupling(name = 'GC_56',
                 value = '-(ca1**2*ca2**2*cb**2*complex(0,1)*l1) - ca2**2*cb**2*complex(0,1)*l3*sa1**2 - ca2**2*cb**2*complex(0,1)*l4*sa1**2 + ca2**2*cb**2*complex(0,1)*l5*sa1**2 - cb**2*complex(0,1)*l7*sa2**2 - 4*ca1*ca2**2*cb*complex(0,1)*l5*sa1*sb - ca1**2*ca2**2*complex(0,1)*l3*sb**2 - ca1**2*ca2**2*complex(0,1)*l4*sb**2 + ca1**2*ca2**2*complex(0,1)*l5*sb**2 - ca2**2*complex(0,1)*l2*sa1**2*sb**2 - complex(0,1)*l8*sa2**2*sb**2',
                 order = {'QED':2})

GC_57 = Coupling(name = 'GC_57',
                 value = 'ca1**2*ca2*ca3*cb**2*complex(0,1)*l1*sa2 - ca2*ca3*cb**2*complex(0,1)*l7*sa2 + ca2*ca3*cb**2*complex(0,1)*l3*sa1**2*sa2 - ca1*ca2*cb**2*complex(0,1)*l1*sa1*sa3 + ca1*ca2*cb**2*complex(0,1)*l3*sa1*sa3 + 2*ca1*ca2*ca3*cb*complex(0,1)*l4*sa1*sa2*sb + 2*ca1*ca2*ca3*cb*complex(0,1)*l5*sa1*sa2*sb + ca1**2*ca2*cb*complex(0,1)*l4*sa3*sb + ca1**2*ca2*cb*complex(0,1)*l5*sa3*sb - ca2*cb*complex(0,1)*l4*sa1**2*sa3*sb - ca2*cb*complex(0,1)*l5*sa1**2*sa3*sb + ca1**2*ca2*ca3*complex(0,1)*l3*sa2*sb**2 - ca2*ca3*complex(0,1)*l8*sa2*sb**2 + ca2*ca3*complex(0,1)*l2*sa1**2*sa2*sb**2 + ca1*ca2*complex(0,1)*l2*sa1*sa3*sb**2 - ca1*ca2*complex(0,1)*l3*sa1*sa3*sb**2',
                 order = {'QED':2})

GC_58 = Coupling(name = 'GC_58',
                 value = 'ca1**2*ca2*ca3*cb**2*complex(0,1)*l3*sa2 - ca2*ca3*cb**2*complex(0,1)*l8*sa2 + ca2*ca3*cb**2*complex(0,1)*l2*sa1**2*sa2 + ca1*ca2*cb**2*complex(0,1)*l2*sa1*sa3 - ca1*ca2*cb**2*complex(0,1)*l3*sa1*sa3 - 2*ca1*ca2*ca3*cb*complex(0,1)*l4*sa1*sa2*sb - 2*ca1*ca2*ca3*cb*complex(0,1)*l5*sa1*sa2*sb - ca1**2*ca2*cb*complex(0,1)*l4*sa3*sb - ca1**2*ca2*cb*complex(0,1)*l5*sa3*sb + ca2*cb*complex(0,1)*l4*sa1**2*sa3*sb + ca2*cb*complex(0,1)*l5*sa1**2*sa3*sb + ca1**2*ca2*ca3*complex(0,1)*l1*sa2*sb**2 - ca2*ca3*complex(0,1)*l7*sa2*sb**2 + ca2*ca3*complex(0,1)*l3*sa1**2*sa2*sb**2 - ca1*ca2*complex(0,1)*l1*sa1*sa3*sb**2 + ca1*ca2*complex(0,1)*l3*sa1*sa3*sb**2',
                 order = {'QED':2})

GC_59 = Coupling(name = 'GC_59',
                 value = 'ca1**2*ca2*ca3*cb**2*complex(0,1)*l3*sa2 + ca1**2*ca2*ca3*cb**2*complex(0,1)*l4*sa2 - ca1**2*ca2*ca3*cb**2*complex(0,1)*l5*sa2 - ca2*ca3*cb**2*complex(0,1)*l8*sa2 + ca2*ca3*cb**2*complex(0,1)*l2*sa1**2*sa2 + ca1*ca2*cb**2*complex(0,1)*l2*sa1*sa3 - ca1*ca2*cb**2*complex(0,1)*l3*sa1*sa3 - ca1*ca2*cb**2*complex(0,1)*l4*sa1*sa3 + ca1*ca2*cb**2*complex(0,1)*l5*sa1*sa3 - 4*ca1*ca2*ca3*cb*complex(0,1)*l5*sa1*sa2*sb - 2*ca1**2*ca2*cb*complex(0,1)*l5*sa3*sb + 2*ca2*cb*complex(0,1)*l5*sa1**2*sa3*sb + ca1**2*ca2*ca3*complex(0,1)*l1*sa2*sb**2 - ca2*ca3*complex(0,1)*l7*sa2*sb**2 + ca2*ca3*complex(0,1)*l3*sa1**2*sa2*sb**2 + ca2*ca3*complex(0,1)*l4*sa1**2*sa2*sb**2 - ca2*ca3*complex(0,1)*l5*sa1**2*sa2*sb**2 - ca1*ca2*complex(0,1)*l1*sa1*sa3*sb**2 + ca1*ca2*complex(0,1)*l3*sa1*sa3*sb**2 + ca1*ca2*complex(0,1)*l4*sa1*sa3*sb**2 - ca1*ca2*complex(0,1)*l5*sa1*sa3*sb**2',
                 order = {'QED':2})

GC_60 = Coupling(name = 'GC_60',
                 value = 'ca1**2*ca2*ca3*cb**2*complex(0,1)*l1*sa2 - ca2*ca3*cb**2*complex(0,1)*l7*sa2 + ca2*ca3*cb**2*complex(0,1)*l3*sa1**2*sa2 + ca2*ca3*cb**2*complex(0,1)*l4*sa1**2*sa2 - ca2*ca3*cb**2*complex(0,1)*l5*sa1**2*sa2 - ca1*ca2*cb**2*complex(0,1)*l1*sa1*sa3 + ca1*ca2*cb**2*complex(0,1)*l3*sa1*sa3 + ca1*ca2*cb**2*complex(0,1)*l4*sa1*sa3 - ca1*ca2*cb**2*complex(0,1)*l5*sa1*sa3 + 4*ca1*ca2*ca3*cb*complex(0,1)*l5*sa1*sa2*sb + 2*ca1**2*ca2*cb*complex(0,1)*l5*sa3*sb - 2*ca2*cb*complex(0,1)*l5*sa1**2*sa3*sb + ca1**2*ca2*ca3*complex(0,1)*l3*sa2*sb**2 + ca1**2*ca2*ca3*complex(0,1)*l4*sa2*sb**2 - ca1**2*ca2*ca3*complex(0,1)*l5*sa2*sb**2 - ca2*ca3*complex(0,1)*l8*sa2*sb**2 + ca2*ca3*complex(0,1)*l2*sa1**2*sa2*sb**2 + ca1*ca2*complex(0,1)*l2*sa1*sa3*sb**2 - ca1*ca2*complex(0,1)*l3*sa1*sa3*sb**2 - ca1*ca2*complex(0,1)*l4*sa1*sa3*sb**2 + ca1*ca2*complex(0,1)*l5*sa1*sa3*sb**2',
                 order = {'QED':2})

GC_61 = Coupling(name = 'GC_61',
                 value = 'ca1*ca2*ca3*cb**2*complex(0,1)*l4*sa1*sa2 + ca1*ca2*ca3*cb**2*complex(0,1)*l5*sa1*sa2 + (ca1**2*ca2*cb**2*complex(0,1)*l4*sa3)/2. + (ca1**2*ca2*cb**2*complex(0,1)*l5*sa3)/2. - (ca2*cb**2*complex(0,1)*l4*sa1**2*sa3)/2. - (ca2*cb**2*complex(0,1)*l5*sa1**2*sa3)/2. - ca1**2*ca2*ca3*cb*complex(0,1)*l1*sa2*sb + ca1**2*ca2*ca3*cb*complex(0,1)*l3*sa2*sb + ca2*ca3*cb*complex(0,1)*l7*sa2*sb - ca2*ca3*cb*complex(0,1)*l8*sa2*sb + ca2*ca3*cb*complex(0,1)*l2*sa1**2*sa2*sb - ca2*ca3*cb*complex(0,1)*l3*sa1**2*sa2*sb + ca1*ca2*cb*complex(0,1)*l1*sa1*sa3*sb + ca1*ca2*cb*complex(0,1)*l2*sa1*sa3*sb - 2*ca1*ca2*cb*complex(0,1)*l3*sa1*sa3*sb - ca1*ca2*ca3*complex(0,1)*l4*sa1*sa2*sb**2 - ca1*ca2*ca3*complex(0,1)*l5*sa1*sa2*sb**2 - (ca1**2*ca2*complex(0,1)*l4*sa3*sb**2)/2. - (ca1**2*ca2*complex(0,1)*l5*sa3*sb**2)/2. + (ca2*complex(0,1)*l4*sa1**2*sa3*sb**2)/2. + (ca2*complex(0,1)*l5*sa1**2*sa3*sb**2)/2.',
                 order = {'QED':2})

GC_62 = Coupling(name = 'GC_62',
                 value = '2*ca1*ca2*ca3*cb**2*complex(0,1)*l5*sa1*sa2 + ca1**2*ca2*cb**2*complex(0,1)*l5*sa3 - ca2*cb**2*complex(0,1)*l5*sa1**2*sa3 - ca1**2*ca2*ca3*cb*complex(0,1)*l1*sa2*sb + ca1**2*ca2*ca3*cb*complex(0,1)*l3*sa2*sb + ca1**2*ca2*ca3*cb*complex(0,1)*l4*sa2*sb - ca1**2*ca2*ca3*cb*complex(0,1)*l5*sa2*sb + ca2*ca3*cb*complex(0,1)*l7*sa2*sb - ca2*ca3*cb*complex(0,1)*l8*sa2*sb + ca2*ca3*cb*complex(0,1)*l2*sa1**2*sa2*sb - ca2*ca3*cb*complex(0,1)*l3*sa1**2*sa2*sb - ca2*ca3*cb*complex(0,1)*l4*sa1**2*sa2*sb + ca2*ca3*cb*complex(0,1)*l5*sa1**2*sa2*sb + ca1*ca2*cb*complex(0,1)*l1*sa1*sa3*sb + ca1*ca2*cb*complex(0,1)*l2*sa1*sa3*sb - 2*ca1*ca2*cb*complex(0,1)*l3*sa1*sa3*sb - 2*ca1*ca2*cb*complex(0,1)*l4*sa1*sa3*sb + 2*ca1*ca2*cb*complex(0,1)*l5*sa1*sa3*sb - 2*ca1*ca2*ca3*complex(0,1)*l5*sa1*sa2*sb**2 - ca1**2*ca2*complex(0,1)*l5*sa3*sb**2 + ca2*complex(0,1)*l5*sa1**2*sa3*sb**2',
                 order = {'QED':2})

GC_63 = Coupling(name = 'GC_63',
                 value = '-(ca1**2*ca2*ca3*cb**2*complex(0,1)*l4)/2. - (ca1**2*ca2*ca3*cb**2*complex(0,1)*l5)/2. + (ca2*ca3*cb**2*complex(0,1)*l4*sa1**2)/2. + (ca2*ca3*cb**2*complex(0,1)*l5*sa1**2)/2. + ca1*ca2*cb**2*complex(0,1)*l4*sa1*sa2*sa3 + ca1*ca2*cb**2*complex(0,1)*l5*sa1*sa2*sa3 - ca1*ca2*ca3*cb*complex(0,1)*l1*sa1*sb - ca1*ca2*ca3*cb*complex(0,1)*l2*sa1*sb + 2*ca1*ca2*ca3*cb*complex(0,1)*l3*sa1*sb - ca1**2*ca2*cb*complex(0,1)*l1*sa2*sa3*sb + ca1**2*ca2*cb*complex(0,1)*l3*sa2*sa3*sb + ca2*cb*complex(0,1)*l7*sa2*sa3*sb - ca2*cb*complex(0,1)*l8*sa2*sa3*sb + ca2*cb*complex(0,1)*l2*sa1**2*sa2*sa3*sb - ca2*cb*complex(0,1)*l3*sa1**2*sa2*sa3*sb + (ca1**2*ca2*ca3*complex(0,1)*l4*sb**2)/2. + (ca1**2*ca2*ca3*complex(0,1)*l5*sb**2)/2. - (ca2*ca3*complex(0,1)*l4*sa1**2*sb**2)/2. - (ca2*ca3*complex(0,1)*l5*sa1**2*sb**2)/2. - ca1*ca2*complex(0,1)*l4*sa1*sa2*sa3*sb**2 - ca1*ca2*complex(0,1)*l5*sa1*sa2*sa3*sb**2',
                 order = {'QED':2})

GC_64 = Coupling(name = 'GC_64',
                 value = '-(ca1**2*ca2*ca3*cb**2*complex(0,1)*l5) + ca2*ca3*cb**2*complex(0,1)*l5*sa1**2 + 2*ca1*ca2*cb**2*complex(0,1)*l5*sa1*sa2*sa3 - ca1*ca2*ca3*cb*complex(0,1)*l1*sa1*sb - ca1*ca2*ca3*cb*complex(0,1)*l2*sa1*sb + 2*ca1*ca2*ca3*cb*complex(0,1)*l3*sa1*sb + 2*ca1*ca2*ca3*cb*complex(0,1)*l4*sa1*sb - 2*ca1*ca2*ca3*cb*complex(0,1)*l5*sa1*sb - ca1**2*ca2*cb*complex(0,1)*l1*sa2*sa3*sb + ca1**2*ca2*cb*complex(0,1)*l3*sa2*sa3*sb + ca1**2*ca2*cb*complex(0,1)*l4*sa2*sa3*sb - ca1**2*ca2*cb*complex(0,1)*l5*sa2*sa3*sb + ca2*cb*complex(0,1)*l7*sa2*sa3*sb - ca2*cb*complex(0,1)*l8*sa2*sa3*sb + ca2*cb*complex(0,1)*l2*sa1**2*sa2*sa3*sb - ca2*cb*complex(0,1)*l3*sa1**2*sa2*sa3*sb - ca2*cb*complex(0,1)*l4*sa1**2*sa2*sa3*sb + ca2*cb*complex(0,1)*l5*sa1**2*sa2*sa3*sb + ca1**2*ca2*ca3*complex(0,1)*l5*sb**2 - ca2*ca3*complex(0,1)*l5*sa1**2*sb**2 - 2*ca1*ca2*complex(0,1)*l5*sa1*sa2*sa3*sb**2',
                 order = {'QED':2})

GC_65 = Coupling(name = 'GC_65',
                 value = 'ca1*ca2*ca3*cb**2*complex(0,1)*l1*sa1 - ca1*ca2*ca3*cb**2*complex(0,1)*l3*sa1 + ca1**2*ca2*cb**2*complex(0,1)*l1*sa2*sa3 - ca2*cb**2*complex(0,1)*l7*sa2*sa3 + ca2*cb**2*complex(0,1)*l3*sa1**2*sa2*sa3 - ca1**2*ca2*ca3*cb*complex(0,1)*l4*sb - ca1**2*ca2*ca3*cb*complex(0,1)*l5*sb + ca2*ca3*cb*complex(0,1)*l4*sa1**2*sb + ca2*ca3*cb*complex(0,1)*l5*sa1**2*sb + 2*ca1*ca2*cb*complex(0,1)*l4*sa1*sa2*sa3*sb + 2*ca1*ca2*cb*complex(0,1)*l5*sa1*sa2*sa3*sb - ca1*ca2*ca3*complex(0,1)*l2*sa1*sb**2 + ca1*ca2*ca3*complex(0,1)*l3*sa1*sb**2 + ca1**2*ca2*complex(0,1)*l3*sa2*sa3*sb**2 - ca2*complex(0,1)*l8*sa2*sa3*sb**2 + ca2*complex(0,1)*l2*sa1**2*sa2*sa3*sb**2',
                 order = {'QED':2})

GC_66 = Coupling(name = 'GC_66',
                 value = 'ca1*ca2*ca3*cb**2*complex(0,1)*l1*sa1 - ca1*ca2*ca3*cb**2*complex(0,1)*l3*sa1 - ca1*ca2*ca3*cb**2*complex(0,1)*l4*sa1 + ca1*ca2*ca3*cb**2*complex(0,1)*l5*sa1 + ca1**2*ca2*cb**2*complex(0,1)*l1*sa2*sa3 - ca2*cb**2*complex(0,1)*l7*sa2*sa3 + ca2*cb**2*complex(0,1)*l3*sa1**2*sa2*sa3 + ca2*cb**2*complex(0,1)*l4*sa1**2*sa2*sa3 - ca2*cb**2*complex(0,1)*l5*sa1**2*sa2*sa3 - 2*ca1**2*ca2*ca3*cb*complex(0,1)*l5*sb + 2*ca2*ca3*cb*complex(0,1)*l5*sa1**2*sb + 4*ca1*ca2*cb*complex(0,1)*l5*sa1*sa2*sa3*sb - ca1*ca2*ca3*complex(0,1)*l2*sa1*sb**2 + ca1*ca2*ca3*complex(0,1)*l3*sa1*sb**2 + ca1*ca2*ca3*complex(0,1)*l4*sa1*sb**2 - ca1*ca2*ca3*complex(0,1)*l5*sa1*sb**2 + ca1**2*ca2*complex(0,1)*l3*sa2*sa3*sb**2 + ca1**2*ca2*complex(0,1)*l4*sa2*sa3*sb**2 - ca1**2*ca2*complex(0,1)*l5*sa2*sa3*sb**2 - ca2*complex(0,1)*l8*sa2*sa3*sb**2 + ca2*complex(0,1)*l2*sa1**2*sa2*sa3*sb**2',
                 order = {'QED':2})

GC_67 = Coupling(name = 'GC_67',
                 value = '-(ca1*ca2*ca3*cb**2*complex(0,1)*l2*sa1) + ca1*ca2*ca3*cb**2*complex(0,1)*l3*sa1 + ca1**2*ca2*cb**2*complex(0,1)*l3*sa2*sa3 - ca2*cb**2*complex(0,1)*l8*sa2*sa3 + ca2*cb**2*complex(0,1)*l2*sa1**2*sa2*sa3 + ca1**2*ca2*ca3*cb*complex(0,1)*l4*sb + ca1**2*ca2*ca3*cb*complex(0,1)*l5*sb - ca2*ca3*cb*complex(0,1)*l4*sa1**2*sb - ca2*ca3*cb*complex(0,1)*l5*sa1**2*sb - 2*ca1*ca2*cb*complex(0,1)*l4*sa1*sa2*sa3*sb - 2*ca1*ca2*cb*complex(0,1)*l5*sa1*sa2*sa3*sb + ca1*ca2*ca3*complex(0,1)*l1*sa1*sb**2 - ca1*ca2*ca3*complex(0,1)*l3*sa1*sb**2 + ca1**2*ca2*complex(0,1)*l1*sa2*sa3*sb**2 - ca2*complex(0,1)*l7*sa2*sa3*sb**2 + ca2*complex(0,1)*l3*sa1**2*sa2*sa3*sb**2',
                 order = {'QED':2})

GC_68 = Coupling(name = 'GC_68',
                 value = '-(ca1*ca2*ca3*cb**2*complex(0,1)*l2*sa1) + ca1*ca2*ca3*cb**2*complex(0,1)*l3*sa1 + ca1*ca2*ca3*cb**2*complex(0,1)*l4*sa1 - ca1*ca2*ca3*cb**2*complex(0,1)*l5*sa1 + ca1**2*ca2*cb**2*complex(0,1)*l3*sa2*sa3 + ca1**2*ca2*cb**2*complex(0,1)*l4*sa2*sa3 - ca1**2*ca2*cb**2*complex(0,1)*l5*sa2*sa3 - ca2*cb**2*complex(0,1)*l8*sa2*sa3 + ca2*cb**2*complex(0,1)*l2*sa1**2*sa2*sa3 + 2*ca1**2*ca2*ca3*cb*complex(0,1)*l5*sb - 2*ca2*ca3*cb*complex(0,1)*l5*sa1**2*sb - 4*ca1*ca2*cb*complex(0,1)*l5*sa1*sa2*sa3*sb + ca1*ca2*ca3*complex(0,1)*l1*sa1*sb**2 - ca1*ca2*ca3*complex(0,1)*l3*sa1*sb**2 - ca1*ca2*ca3*complex(0,1)*l4*sa1*sb**2 + ca1*ca2*ca3*complex(0,1)*l5*sa1*sb**2 + ca1**2*ca2*complex(0,1)*l1*sa2*sa3*sb**2 - ca2*complex(0,1)*l7*sa2*sa3*sb**2 + ca2*complex(0,1)*l3*sa1**2*sa2*sa3*sb**2 + ca2*complex(0,1)*l4*sa1**2*sa2*sa3*sb**2 - ca2*complex(0,1)*l5*sa1**2*sa2*sa3*sb**2',
                 order = {'QED':2})

GC_69 = Coupling(name = 'GC_69',
                 value = '-(ca1*ca3**2*cb**2*complex(0,1)*l4*sa1*sa2**2) - ca1*ca3**2*cb**2*complex(0,1)*l5*sa1*sa2**2 - ca1**2*ca3*cb**2*complex(0,1)*l4*sa2*sa3 - ca1**2*ca3*cb**2*complex(0,1)*l5*sa2*sa3 + ca3*cb**2*complex(0,1)*l4*sa1**2*sa2*sa3 + ca3*cb**2*complex(0,1)*l5*sa1**2*sa2*sa3 + ca1*cb**2*complex(0,1)*l4*sa1*sa3**2 + ca1*cb**2*complex(0,1)*l5*sa1*sa3**2 + ca2**2*ca3**2*cb*complex(0,1)*l7*sb - ca2**2*ca3**2*cb*complex(0,1)*l8*sb + ca1**2*ca3**2*cb*complex(0,1)*l1*sa2**2*sb - ca1**2*ca3**2*cb*complex(0,1)*l3*sa2**2*sb - ca3**2*cb*complex(0,1)*l2*sa1**2*sa2**2*sb + ca3**2*cb*complex(0,1)*l3*sa1**2*sa2**2*sb - 2*ca1*ca3*cb*complex(0,1)*l1*sa1*sa2*sa3*sb - 2*ca1*ca3*cb*complex(0,1)*l2*sa1*sa2*sa3*sb + 4*ca1*ca3*cb*complex(0,1)*l3*sa1*sa2*sa3*sb - ca1**2*cb*complex(0,1)*l2*sa3**2*sb + ca1**2*cb*complex(0,1)*l3*sa3**2*sb + cb*complex(0,1)*l1*sa1**2*sa3**2*sb - cb*complex(0,1)*l3*sa1**2*sa3**2*sb + ca1*ca3**2*complex(0,1)*l4*sa1*sa2**2*sb**2 + ca1*ca3**2*complex(0,1)*l5*sa1*sa2**2*sb**2 + ca1**2*ca3*complex(0,1)*l4*sa2*sa3*sb**2 + ca1**2*ca3*complex(0,1)*l5*sa2*sa3*sb**2 - ca3*complex(0,1)*l4*sa1**2*sa2*sa3*sb**2 - ca3*complex(0,1)*l5*sa1**2*sa2*sa3*sb**2 - ca1*complex(0,1)*l4*sa1*sa3**2*sb**2 - ca1*complex(0,1)*l5*sa1*sa3**2*sb**2',
                 order = {'QED':2})

GC_70 = Coupling(name = 'GC_70',
                 value = '-2*ca1*ca3**2*cb**2*complex(0,1)*l5*sa1*sa2**2 - 2*ca1**2*ca3*cb**2*complex(0,1)*l5*sa2*sa3 + 2*ca3*cb**2*complex(0,1)*l5*sa1**2*sa2*sa3 + 2*ca1*cb**2*complex(0,1)*l5*sa1*sa3**2 + ca2**2*ca3**2*cb*complex(0,1)*l7*sb - ca2**2*ca3**2*cb*complex(0,1)*l8*sb + ca1**2*ca3**2*cb*complex(0,1)*l1*sa2**2*sb - ca1**2*ca3**2*cb*complex(0,1)*l3*sa2**2*sb - ca1**2*ca3**2*cb*complex(0,1)*l4*sa2**2*sb + ca1**2*ca3**2*cb*complex(0,1)*l5*sa2**2*sb - ca3**2*cb*complex(0,1)*l2*sa1**2*sa2**2*sb + ca3**2*cb*complex(0,1)*l3*sa1**2*sa2**2*sb + ca3**2*cb*complex(0,1)*l4*sa1**2*sa2**2*sb - ca3**2*cb*complex(0,1)*l5*sa1**2*sa2**2*sb - 2*ca1*ca3*cb*complex(0,1)*l1*sa1*sa2*sa3*sb - 2*ca1*ca3*cb*complex(0,1)*l2*sa1*sa2*sa3*sb + 4*ca1*ca3*cb*complex(0,1)*l3*sa1*sa2*sa3*sb + 4*ca1*ca3*cb*complex(0,1)*l4*sa1*sa2*sa3*sb - 4*ca1*ca3*cb*complex(0,1)*l5*sa1*sa2*sa3*sb - ca1**2*cb*complex(0,1)*l2*sa3**2*sb + ca1**2*cb*complex(0,1)*l3*sa3**2*sb + ca1**2*cb*complex(0,1)*l4*sa3**2*sb - ca1**2*cb*complex(0,1)*l5*sa3**2*sb + cb*complex(0,1)*l1*sa1**2*sa3**2*sb - cb*complex(0,1)*l3*sa1**2*sa3**2*sb - cb*complex(0,1)*l4*sa1**2*sa3**2*sb + cb*complex(0,1)*l5*sa1**2*sa3**2*sb + 2*ca1*ca3**2*complex(0,1)*l5*sa1*sa2**2*sb**2 + 2*ca1**2*ca3*complex(0,1)*l5*sa2*sa3*sb**2 - 2*ca3*complex(0,1)*l5*sa1**2*sa2*sa3*sb**2 - 2*ca1*complex(0,1)*l5*sa1*sa3**2*sb**2',
                 order = {'QED':2})

GC_71 = Coupling(name = 'GC_71',
                 value = '-(ca2**2*ca3**2*cb**2*complex(0,1)*l8) - ca1**2*ca3**2*cb**2*complex(0,1)*l3*sa2**2 - ca3**2*cb**2*complex(0,1)*l2*sa1**2*sa2**2 - 2*ca1*ca3*cb**2*complex(0,1)*l2*sa1*sa2*sa3 + 2*ca1*ca3*cb**2*complex(0,1)*l3*sa1*sa2*sa3 - ca1**2*cb**2*complex(0,1)*l2*sa3**2 - cb**2*complex(0,1)*l3*sa1**2*sa3**2 + 2*ca1*ca3**2*cb*complex(0,1)*l4*sa1*sa2**2*sb + 2*ca1*ca3**2*cb*complex(0,1)*l5*sa1*sa2**2*sb + 2*ca1**2*ca3*cb*complex(0,1)*l4*sa2*sa3*sb + 2*ca1**2*ca3*cb*complex(0,1)*l5*sa2*sa3*sb - 2*ca3*cb*complex(0,1)*l4*sa1**2*sa2*sa3*sb - 2*ca3*cb*complex(0,1)*l5*sa1**2*sa2*sa3*sb - 2*ca1*cb*complex(0,1)*l4*sa1*sa3**2*sb - 2*ca1*cb*complex(0,1)*l5*sa1*sa3**2*sb - ca2**2*ca3**2*complex(0,1)*l7*sb**2 - ca1**2*ca3**2*complex(0,1)*l1*sa2**2*sb**2 - ca3**2*complex(0,1)*l3*sa1**2*sa2**2*sb**2 + 2*ca1*ca3*complex(0,1)*l1*sa1*sa2*sa3*sb**2 - 2*ca1*ca3*complex(0,1)*l3*sa1*sa2*sa3*sb**2 - ca1**2*complex(0,1)*l3*sa3**2*sb**2 - complex(0,1)*l1*sa1**2*sa3**2*sb**2',
                 order = {'QED':2})

GC_72 = Coupling(name = 'GC_72',
                 value = '-(ca2**2*ca3**2*cb**2*complex(0,1)*l8) - ca1**2*ca3**2*cb**2*complex(0,1)*l3*sa2**2 - ca1**2*ca3**2*cb**2*complex(0,1)*l4*sa2**2 + ca1**2*ca3**2*cb**2*complex(0,1)*l5*sa2**2 - ca3**2*cb**2*complex(0,1)*l2*sa1**2*sa2**2 - 2*ca1*ca3*cb**2*complex(0,1)*l2*sa1*sa2*sa3 + 2*ca1*ca3*cb**2*complex(0,1)*l3*sa1*sa2*sa3 + 2*ca1*ca3*cb**2*complex(0,1)*l4*sa1*sa2*sa3 - 2*ca1*ca3*cb**2*complex(0,1)*l5*sa1*sa2*sa3 - ca1**2*cb**2*complex(0,1)*l2*sa3**2 - cb**2*complex(0,1)*l3*sa1**2*sa3**2 - cb**2*complex(0,1)*l4*sa1**2*sa3**2 + cb**2*complex(0,1)*l5*sa1**2*sa3**2 + 4*ca1*ca3**2*cb*complex(0,1)*l5*sa1*sa2**2*sb + 4*ca1**2*ca3*cb*complex(0,1)*l5*sa2*sa3*sb - 4*ca3*cb*complex(0,1)*l5*sa1**2*sa2*sa3*sb - 4*ca1*cb*complex(0,1)*l5*sa1*sa3**2*sb - ca2**2*ca3**2*complex(0,1)*l7*sb**2 - ca1**2*ca3**2*complex(0,1)*l1*sa2**2*sb**2 - ca3**2*complex(0,1)*l3*sa1**2*sa2**2*sb**2 - ca3**2*complex(0,1)*l4*sa1**2*sa2**2*sb**2 + ca3**2*complex(0,1)*l5*sa1**2*sa2**2*sb**2 + 2*ca1*ca3*complex(0,1)*l1*sa1*sa2*sa3*sb**2 - 2*ca1*ca3*complex(0,1)*l3*sa1*sa2*sa3*sb**2 - 2*ca1*ca3*complex(0,1)*l4*sa1*sa2*sa3*sb**2 + 2*ca1*ca3*complex(0,1)*l5*sa1*sa2*sa3*sb**2 - ca1**2*complex(0,1)*l3*sa3**2*sb**2 - ca1**2*complex(0,1)*l4*sa3**2*sb**2 + ca1**2*complex(0,1)*l5*sa3**2*sb**2 - complex(0,1)*l1*sa1**2*sa3**2*sb**2',
                 order = {'QED':2})

GC_73 = Coupling(name = 'GC_73',
                 value = '-(ca2**2*ca3**2*cb**2*complex(0,1)*l7) - ca1**2*ca3**2*cb**2*complex(0,1)*l1*sa2**2 - ca3**2*cb**2*complex(0,1)*l3*sa1**2*sa2**2 + 2*ca1*ca3*cb**2*complex(0,1)*l1*sa1*sa2*sa3 - 2*ca1*ca3*cb**2*complex(0,1)*l3*sa1*sa2*sa3 - ca1**2*cb**2*complex(0,1)*l3*sa3**2 - cb**2*complex(0,1)*l1*sa1**2*sa3**2 - 2*ca1*ca3**2*cb*complex(0,1)*l4*sa1*sa2**2*sb - 2*ca1*ca3**2*cb*complex(0,1)*l5*sa1*sa2**2*sb - 2*ca1**2*ca3*cb*complex(0,1)*l4*sa2*sa3*sb - 2*ca1**2*ca3*cb*complex(0,1)*l5*sa2*sa3*sb + 2*ca3*cb*complex(0,1)*l4*sa1**2*sa2*sa3*sb + 2*ca3*cb*complex(0,1)*l5*sa1**2*sa2*sa3*sb + 2*ca1*cb*complex(0,1)*l4*sa1*sa3**2*sb + 2*ca1*cb*complex(0,1)*l5*sa1*sa3**2*sb - ca2**2*ca3**2*complex(0,1)*l8*sb**2 - ca1**2*ca3**2*complex(0,1)*l3*sa2**2*sb**2 - ca3**2*complex(0,1)*l2*sa1**2*sa2**2*sb**2 - 2*ca1*ca3*complex(0,1)*l2*sa1*sa2*sa3*sb**2 + 2*ca1*ca3*complex(0,1)*l3*sa1*sa2*sa3*sb**2 - ca1**2*complex(0,1)*l2*sa3**2*sb**2 - complex(0,1)*l3*sa1**2*sa3**2*sb**2',
                 order = {'QED':2})

GC_74 = Coupling(name = 'GC_74',
                 value = '-(ca2**2*ca3**2*cb**2*complex(0,1)*l7) - ca1**2*ca3**2*cb**2*complex(0,1)*l1*sa2**2 - ca3**2*cb**2*complex(0,1)*l3*sa1**2*sa2**2 - ca3**2*cb**2*complex(0,1)*l4*sa1**2*sa2**2 + ca3**2*cb**2*complex(0,1)*l5*sa1**2*sa2**2 + 2*ca1*ca3*cb**2*complex(0,1)*l1*sa1*sa2*sa3 - 2*ca1*ca3*cb**2*complex(0,1)*l3*sa1*sa2*sa3 - 2*ca1*ca3*cb**2*complex(0,1)*l4*sa1*sa2*sa3 + 2*ca1*ca3*cb**2*complex(0,1)*l5*sa1*sa2*sa3 - ca1**2*cb**2*complex(0,1)*l3*sa3**2 - ca1**2*cb**2*complex(0,1)*l4*sa3**2 + ca1**2*cb**2*complex(0,1)*l5*sa3**2 - cb**2*complex(0,1)*l1*sa1**2*sa3**2 - 4*ca1*ca3**2*cb*complex(0,1)*l5*sa1*sa2**2*sb - 4*ca1**2*ca3*cb*complex(0,1)*l5*sa2*sa3*sb + 4*ca3*cb*complex(0,1)*l5*sa1**2*sa2*sa3*sb + 4*ca1*cb*complex(0,1)*l5*sa1*sa3**2*sb - ca2**2*ca3**2*complex(0,1)*l8*sb**2 - ca1**2*ca3**2*complex(0,1)*l3*sa2**2*sb**2 - ca1**2*ca3**2*complex(0,1)*l4*sa2**2*sb**2 + ca1**2*ca3**2*complex(0,1)*l5*sa2**2*sb**2 - ca3**2*complex(0,1)*l2*sa1**2*sa2**2*sb**2 - 2*ca1*ca3*complex(0,1)*l2*sa1*sa2*sa3*sb**2 + 2*ca1*ca3*complex(0,1)*l3*sa1*sa2*sa3*sb**2 + 2*ca1*ca3*complex(0,1)*l4*sa1*sa2*sa3*sb**2 - 2*ca1*ca3*complex(0,1)*l5*sa1*sa2*sa3*sb**2 - ca1**2*complex(0,1)*l2*sa3**2*sb**2 - complex(0,1)*l3*sa1**2*sa3**2*sb**2 - complex(0,1)*l4*sa1**2*sa3**2*sb**2 + complex(0,1)*l5*sa1**2*sa3**2*sb**2',
                 order = {'QED':2})

GC_75 = Coupling(name = 'GC_75',
                 value = 'ca1*ca3**2*cb**2*complex(0,1)*l2*sa1*sa2 - ca1*ca3**2*cb**2*complex(0,1)*l3*sa1*sa2 + ca1**2*ca3*cb**2*complex(0,1)*l2*sa3 - ca2**2*ca3*cb**2*complex(0,1)*l8*sa3 + ca3*cb**2*complex(0,1)*l3*sa1**2*sa3 - ca1**2*ca3*cb**2*complex(0,1)*l3*sa2**2*sa3 - ca3*cb**2*complex(0,1)*l2*sa1**2*sa2**2*sa3 - ca1*cb**2*complex(0,1)*l2*sa1*sa2*sa3**2 + ca1*cb**2*complex(0,1)*l3*sa1*sa2*sa3**2 - ca1**2*ca3**2*cb*complex(0,1)*l4*sa2*sb - ca1**2*ca3**2*cb*complex(0,1)*l5*sa2*sb + ca3**2*cb*complex(0,1)*l4*sa1**2*sa2*sb + ca3**2*cb*complex(0,1)*l5*sa1**2*sa2*sb + 2*ca1*ca3*cb*complex(0,1)*l4*sa1*sa3*sb + 2*ca1*ca3*cb*complex(0,1)*l5*sa1*sa3*sb + 2*ca1*ca3*cb*complex(0,1)*l4*sa1*sa2**2*sa3*sb + 2*ca1*ca3*cb*complex(0,1)*l5*sa1*sa2**2*sa3*sb + ca1**2*cb*complex(0,1)*l4*sa2*sa3**2*sb + ca1**2*cb*complex(0,1)*l5*sa2*sa3**2*sb - cb*complex(0,1)*l4*sa1**2*sa2*sa3**2*sb - cb*complex(0,1)*l5*sa1**2*sa2*sa3**2*sb - ca1*ca3**2*complex(0,1)*l1*sa1*sa2*sb**2 + ca1*ca3**2*complex(0,1)*l3*sa1*sa2*sb**2 + ca1**2*ca3*complex(0,1)*l3*sa3*sb**2 - ca2**2*ca3*complex(0,1)*l7*sa3*sb**2 + ca3*complex(0,1)*l1*sa1**2*sa3*sb**2 - ca1**2*ca3*complex(0,1)*l1*sa2**2*sa3*sb**2 - ca3*complex(0,1)*l3*sa1**2*sa2**2*sa3*sb**2 + ca1*complex(0,1)*l1*sa1*sa2*sa3**2*sb**2 - ca1*complex(0,1)*l3*sa1*sa2*sa3**2*sb**2',
                 order = {'QED':2})

GC_76 = Coupling(name = 'GC_76',
                 value = '-(ca1*ca3**2*cb**2*complex(0,1)*l1*sa1*sa2) + ca1*ca3**2*cb**2*complex(0,1)*l3*sa1*sa2 + ca1**2*ca3*cb**2*complex(0,1)*l3*sa3 - ca2**2*ca3*cb**2*complex(0,1)*l7*sa3 + ca3*cb**2*complex(0,1)*l1*sa1**2*sa3 - ca1**2*ca3*cb**2*complex(0,1)*l1*sa2**2*sa3 - ca3*cb**2*complex(0,1)*l3*sa1**2*sa2**2*sa3 + ca1*cb**2*complex(0,1)*l1*sa1*sa2*sa3**2 - ca1*cb**2*complex(0,1)*l3*sa1*sa2*sa3**2 + ca1**2*ca3**2*cb*complex(0,1)*l4*sa2*sb + ca1**2*ca3**2*cb*complex(0,1)*l5*sa2*sb - ca3**2*cb*complex(0,1)*l4*sa1**2*sa2*sb - ca3**2*cb*complex(0,1)*l5*sa1**2*sa2*sb - 2*ca1*ca3*cb*complex(0,1)*l4*sa1*sa3*sb - 2*ca1*ca3*cb*complex(0,1)*l5*sa1*sa3*sb - 2*ca1*ca3*cb*complex(0,1)*l4*sa1*sa2**2*sa3*sb - 2*ca1*ca3*cb*complex(0,1)*l5*sa1*sa2**2*sa3*sb - ca1**2*cb*complex(0,1)*l4*sa2*sa3**2*sb - ca1**2*cb*complex(0,1)*l5*sa2*sa3**2*sb + cb*complex(0,1)*l4*sa1**2*sa2*sa3**2*sb + cb*complex(0,1)*l5*sa1**2*sa2*sa3**2*sb + ca1*ca3**2*complex(0,1)*l2*sa1*sa2*sb**2 - ca1*ca3**2*complex(0,1)*l3*sa1*sa2*sb**2 + ca1**2*ca3*complex(0,1)*l2*sa3*sb**2 - ca2**2*ca3*complex(0,1)*l8*sa3*sb**2 + ca3*complex(0,1)*l3*sa1**2*sa3*sb**2 - ca1**2*ca3*complex(0,1)*l3*sa2**2*sa3*sb**2 - ca3*complex(0,1)*l2*sa1**2*sa2**2*sa3*sb**2 - ca1*complex(0,1)*l2*sa1*sa2*sa3**2*sb**2 + ca1*complex(0,1)*l3*sa1*sa2*sa3**2*sb**2',
                 order = {'QED':2})

GC_77 = Coupling(name = 'GC_77',
                 value = '-(ca1*ca3**2*cb**2*complex(0,1)*l1*sa1*sa2) + ca1*ca3**2*cb**2*complex(0,1)*l3*sa1*sa2 + ca1*ca3**2*cb**2*complex(0,1)*l4*sa1*sa2 - ca1*ca3**2*cb**2*complex(0,1)*l5*sa1*sa2 + ca1**2*ca3*cb**2*complex(0,1)*l3*sa3 + ca1**2*ca3*cb**2*complex(0,1)*l4*sa3 - ca1**2*ca3*cb**2*complex(0,1)*l5*sa3 - ca2**2*ca3*cb**2*complex(0,1)*l7*sa3 + ca3*cb**2*complex(0,1)*l1*sa1**2*sa3 - ca1**2*ca3*cb**2*complex(0,1)*l1*sa2**2*sa3 - ca3*cb**2*complex(0,1)*l3*sa1**2*sa2**2*sa3 - ca3*cb**2*complex(0,1)*l4*sa1**2*sa2**2*sa3 + ca3*cb**2*complex(0,1)*l5*sa1**2*sa2**2*sa3 + ca1*cb**2*complex(0,1)*l1*sa1*sa2*sa3**2 - ca1*cb**2*complex(0,1)*l3*sa1*sa2*sa3**2 - ca1*cb**2*complex(0,1)*l4*sa1*sa2*sa3**2 + ca1*cb**2*complex(0,1)*l5*sa1*sa2*sa3**2 + 2*ca1**2*ca3**2*cb*complex(0,1)*l5*sa2*sb - 2*ca3**2*cb*complex(0,1)*l5*sa1**2*sa2*sb - 4*ca1*ca3*cb*complex(0,1)*l5*sa1*sa3*sb - 4*ca1*ca3*cb*complex(0,1)*l5*sa1*sa2**2*sa3*sb - 2*ca1**2*cb*complex(0,1)*l5*sa2*sa3**2*sb + 2*cb*complex(0,1)*l5*sa1**2*sa2*sa3**2*sb + ca1*ca3**2*complex(0,1)*l2*sa1*sa2*sb**2 - ca1*ca3**2*complex(0,1)*l3*sa1*sa2*sb**2 - ca1*ca3**2*complex(0,1)*l4*sa1*sa2*sb**2 + ca1*ca3**2*complex(0,1)*l5*sa1*sa2*sb**2 + ca1**2*ca3*complex(0,1)*l2*sa3*sb**2 - ca2**2*ca3*complex(0,1)*l8*sa3*sb**2 + ca3*complex(0,1)*l3*sa1**2*sa3*sb**2 + ca3*complex(0,1)*l4*sa1**2*sa3*sb**2 - ca3*complex(0,1)*l5*sa1**2*sa3*sb**2 - ca1**2*ca3*complex(0,1)*l3*sa2**2*sa3*sb**2 - ca1**2*ca3*complex(0,1)*l4*sa2**2*sa3*sb**2 + ca1**2*ca3*complex(0,1)*l5*sa2**2*sa3*sb**2 - ca3*complex(0,1)*l2*sa1**2*sa2**2*sa3*sb**2 - ca1*complex(0,1)*l2*sa1*sa2*sa3**2*sb**2 + ca1*complex(0,1)*l3*sa1*sa2*sa3**2*sb**2 + ca1*complex(0,1)*l4*sa1*sa2*sa3**2*sb**2 - ca1*complex(0,1)*l5*sa1*sa2*sa3**2*sb**2',
                 order = {'QED':2})

GC_78 = Coupling(name = 'GC_78',
                 value = 'ca1*ca3**2*cb**2*complex(0,1)*l2*sa1*sa2 - ca1*ca3**2*cb**2*complex(0,1)*l3*sa1*sa2 - ca1*ca3**2*cb**2*complex(0,1)*l4*sa1*sa2 + ca1*ca3**2*cb**2*complex(0,1)*l5*sa1*sa2 + ca1**2*ca3*cb**2*complex(0,1)*l2*sa3 - ca2**2*ca3*cb**2*complex(0,1)*l8*sa3 + ca3*cb**2*complex(0,1)*l3*sa1**2*sa3 + ca3*cb**2*complex(0,1)*l4*sa1**2*sa3 - ca3*cb**2*complex(0,1)*l5*sa1**2*sa3 - ca1**2*ca3*cb**2*complex(0,1)*l3*sa2**2*sa3 - ca1**2*ca3*cb**2*complex(0,1)*l4*sa2**2*sa3 + ca1**2*ca3*cb**2*complex(0,1)*l5*sa2**2*sa3 - ca3*cb**2*complex(0,1)*l2*sa1**2*sa2**2*sa3 - ca1*cb**2*complex(0,1)*l2*sa1*sa2*sa3**2 + ca1*cb**2*complex(0,1)*l3*sa1*sa2*sa3**2 + ca1*cb**2*complex(0,1)*l4*sa1*sa2*sa3**2 - ca1*cb**2*complex(0,1)*l5*sa1*sa2*sa3**2 - 2*ca1**2*ca3**2*cb*complex(0,1)*l5*sa2*sb + 2*ca3**2*cb*complex(0,1)*l5*sa1**2*sa2*sb + 4*ca1*ca3*cb*complex(0,1)*l5*sa1*sa3*sb + 4*ca1*ca3*cb*complex(0,1)*l5*sa1*sa2**2*sa3*sb + 2*ca1**2*cb*complex(0,1)*l5*sa2*sa3**2*sb - 2*cb*complex(0,1)*l5*sa1**2*sa2*sa3**2*sb - ca1*ca3**2*complex(0,1)*l1*sa1*sa2*sb**2 + ca1*ca3**2*complex(0,1)*l3*sa1*sa2*sb**2 + ca1*ca3**2*complex(0,1)*l4*sa1*sa2*sb**2 - ca1*ca3**2*complex(0,1)*l5*sa1*sa2*sb**2 + ca1**2*ca3*complex(0,1)*l3*sa3*sb**2 + ca1**2*ca3*complex(0,1)*l4*sa3*sb**2 - ca1**2*ca3*complex(0,1)*l5*sa3*sb**2 - ca2**2*ca3*complex(0,1)*l7*sa3*sb**2 + ca3*complex(0,1)*l1*sa1**2*sa3*sb**2 - ca1**2*ca3*complex(0,1)*l1*sa2**2*sa3*sb**2 - ca3*complex(0,1)*l3*sa1**2*sa2**2*sa3*sb**2 - ca3*complex(0,1)*l4*sa1**2*sa2**2*sa3*sb**2 + ca3*complex(0,1)*l5*sa1**2*sa2**2*sa3*sb**2 + ca1*complex(0,1)*l1*sa1*sa2*sa3**2*sb**2 - ca1*complex(0,1)*l3*sa1*sa2*sa3**2*sb**2 - ca1*complex(0,1)*l4*sa1*sa2*sa3**2*sb**2 + ca1*complex(0,1)*l5*sa1*sa2*sa3**2*sb**2',
                 order = {'QED':2})

GC_79 = Coupling(name = 'GC_79',
                 value = '(ca1**2*ca3**2*cb**2*complex(0,1)*l4*sa2)/2. + (ca1**2*ca3**2*cb**2*complex(0,1)*l5*sa2)/2. - (ca3**2*cb**2*complex(0,1)*l4*sa1**2*sa2)/2. - (ca3**2*cb**2*complex(0,1)*l5*sa1**2*sa2)/2. - ca1*ca3*cb**2*complex(0,1)*l4*sa1*sa3 - ca1*ca3*cb**2*complex(0,1)*l5*sa1*sa3 - ca1*ca3*cb**2*complex(0,1)*l4*sa1*sa2**2*sa3 - ca1*ca3*cb**2*complex(0,1)*l5*sa1*sa2**2*sa3 - (ca1**2*cb**2*complex(0,1)*l4*sa2*sa3**2)/2. - (ca1**2*cb**2*complex(0,1)*l5*sa2*sa3**2)/2. + (cb**2*complex(0,1)*l4*sa1**2*sa2*sa3**2)/2. + (cb**2*complex(0,1)*l5*sa1**2*sa2*sa3**2)/2. + ca1*ca3**2*cb*complex(0,1)*l1*sa1*sa2*sb + ca1*ca3**2*cb*complex(0,1)*l2*sa1*sa2*sb - 2*ca1*ca3**2*cb*complex(0,1)*l3*sa1*sa2*sb + ca1**2*ca3*cb*complex(0,1)*l2*sa3*sb - ca1**2*ca3*cb*complex(0,1)*l3*sa3*sb + ca2**2*ca3*cb*complex(0,1)*l7*sa3*sb - ca2**2*ca3*cb*complex(0,1)*l8*sa3*sb - ca3*cb*complex(0,1)*l1*sa1**2*sa3*sb + ca3*cb*complex(0,1)*l3*sa1**2*sa3*sb + ca1**2*ca3*cb*complex(0,1)*l1*sa2**2*sa3*sb - ca1**2*ca3*cb*complex(0,1)*l3*sa2**2*sa3*sb - ca3*cb*complex(0,1)*l2*sa1**2*sa2**2*sa3*sb + ca3*cb*complex(0,1)*l3*sa1**2*sa2**2*sa3*sb - ca1*cb*complex(0,1)*l1*sa1*sa2*sa3**2*sb - ca1*cb*complex(0,1)*l2*sa1*sa2*sa3**2*sb + 2*ca1*cb*complex(0,1)*l3*sa1*sa2*sa3**2*sb - (ca1**2*ca3**2*complex(0,1)*l4*sa2*sb**2)/2. - (ca1**2*ca3**2*complex(0,1)*l5*sa2*sb**2)/2. + (ca3**2*complex(0,1)*l4*sa1**2*sa2*sb**2)/2. + (ca3**2*complex(0,1)*l5*sa1**2*sa2*sb**2)/2. + ca1*ca3*complex(0,1)*l4*sa1*sa3*sb**2 + ca1*ca3*complex(0,1)*l5*sa1*sa3*sb**2 + ca1*ca3*complex(0,1)*l4*sa1*sa2**2*sa3*sb**2 + ca1*ca3*complex(0,1)*l5*sa1*sa2**2*sa3*sb**2 + (ca1**2*complex(0,1)*l4*sa2*sa3**2*sb**2)/2. + (ca1**2*complex(0,1)*l5*sa2*sa3**2*sb**2)/2. - (complex(0,1)*l4*sa1**2*sa2*sa3**2*sb**2)/2. - (complex(0,1)*l5*sa1**2*sa2*sa3**2*sb**2)/2.',
                 order = {'QED':2})

GC_80 = Coupling(name = 'GC_80',
                 value = 'ca1**2*ca3**2*cb**2*complex(0,1)*l5*sa2 - ca3**2*cb**2*complex(0,1)*l5*sa1**2*sa2 - 2*ca1*ca3*cb**2*complex(0,1)*l5*sa1*sa3 - 2*ca1*ca3*cb**2*complex(0,1)*l5*sa1*sa2**2*sa3 - ca1**2*cb**2*complex(0,1)*l5*sa2*sa3**2 + cb**2*complex(0,1)*l5*sa1**2*sa2*sa3**2 + ca1*ca3**2*cb*complex(0,1)*l1*sa1*sa2*sb + ca1*ca3**2*cb*complex(0,1)*l2*sa1*sa2*sb - 2*ca1*ca3**2*cb*complex(0,1)*l3*sa1*sa2*sb - 2*ca1*ca3**2*cb*complex(0,1)*l4*sa1*sa2*sb + 2*ca1*ca3**2*cb*complex(0,1)*l5*sa1*sa2*sb + ca1**2*ca3*cb*complex(0,1)*l2*sa3*sb - ca1**2*ca3*cb*complex(0,1)*l3*sa3*sb - ca1**2*ca3*cb*complex(0,1)*l4*sa3*sb + ca1**2*ca3*cb*complex(0,1)*l5*sa3*sb + ca2**2*ca3*cb*complex(0,1)*l7*sa3*sb - ca2**2*ca3*cb*complex(0,1)*l8*sa3*sb - ca3*cb*complex(0,1)*l1*sa1**2*sa3*sb + ca3*cb*complex(0,1)*l3*sa1**2*sa3*sb + ca3*cb*complex(0,1)*l4*sa1**2*sa3*sb - ca3*cb*complex(0,1)*l5*sa1**2*sa3*sb + ca1**2*ca3*cb*complex(0,1)*l1*sa2**2*sa3*sb - ca1**2*ca3*cb*complex(0,1)*l3*sa2**2*sa3*sb - ca1**2*ca3*cb*complex(0,1)*l4*sa2**2*sa3*sb + ca1**2*ca3*cb*complex(0,1)*l5*sa2**2*sa3*sb - ca3*cb*complex(0,1)*l2*sa1**2*sa2**2*sa3*sb + ca3*cb*complex(0,1)*l3*sa1**2*sa2**2*sa3*sb + ca3*cb*complex(0,1)*l4*sa1**2*sa2**2*sa3*sb - ca3*cb*complex(0,1)*l5*sa1**2*sa2**2*sa3*sb - ca1*cb*complex(0,1)*l1*sa1*sa2*sa3**2*sb - ca1*cb*complex(0,1)*l2*sa1*sa2*sa3**2*sb + 2*ca1*cb*complex(0,1)*l3*sa1*sa2*sa3**2*sb + 2*ca1*cb*complex(0,1)*l4*sa1*sa2*sa3**2*sb - 2*ca1*cb*complex(0,1)*l5*sa1*sa2*sa3**2*sb - ca1**2*ca3**2*complex(0,1)*l5*sa2*sb**2 + ca3**2*complex(0,1)*l5*sa1**2*sa2*sb**2 + 2*ca1*ca3*complex(0,1)*l5*sa1*sa3*sb**2 + 2*ca1*ca3*complex(0,1)*l5*sa1*sa2**2*sa3*sb**2 + ca1**2*complex(0,1)*l5*sa2*sa3**2*sb**2 - complex(0,1)*l5*sa1**2*sa2*sa3**2*sb**2',
                 order = {'QED':2})

GC_81 = Coupling(name = 'GC_81',
                 value = 'ca1*ca3**2*cb**2*complex(0,1)*l4*sa1 + ca1*ca3**2*cb**2*complex(0,1)*l5*sa1 + ca1**2*ca3*cb**2*complex(0,1)*l4*sa2*sa3 + ca1**2*ca3*cb**2*complex(0,1)*l5*sa2*sa3 - ca3*cb**2*complex(0,1)*l4*sa1**2*sa2*sa3 - ca3*cb**2*complex(0,1)*l5*sa1**2*sa2*sa3 - ca1*cb**2*complex(0,1)*l4*sa1*sa2**2*sa3**2 - ca1*cb**2*complex(0,1)*l5*sa1*sa2**2*sa3**2 - ca1**2*ca3**2*cb*complex(0,1)*l2*sb + ca1**2*ca3**2*cb*complex(0,1)*l3*sb + ca3**2*cb*complex(0,1)*l1*sa1**2*sb - ca3**2*cb*complex(0,1)*l3*sa1**2*sb + 2*ca1*ca3*cb*complex(0,1)*l1*sa1*sa2*sa3*sb + 2*ca1*ca3*cb*complex(0,1)*l2*sa1*sa2*sa3*sb - 4*ca1*ca3*cb*complex(0,1)*l3*sa1*sa2*sa3*sb + ca2**2*cb*complex(0,1)*l7*sa3**2*sb - ca2**2*cb*complex(0,1)*l8*sa3**2*sb + ca1**2*cb*complex(0,1)*l1*sa2**2*sa3**2*sb - ca1**2*cb*complex(0,1)*l3*sa2**2*sa3**2*sb - cb*complex(0,1)*l2*sa1**2*sa2**2*sa3**2*sb + cb*complex(0,1)*l3*sa1**2*sa2**2*sa3**2*sb - ca1*ca3**2*complex(0,1)*l4*sa1*sb**2 - ca1*ca3**2*complex(0,1)*l5*sa1*sb**2 - ca1**2*ca3*complex(0,1)*l4*sa2*sa3*sb**2 - ca1**2*ca3*complex(0,1)*l5*sa2*sa3*sb**2 + ca3*complex(0,1)*l4*sa1**2*sa2*sa3*sb**2 + ca3*complex(0,1)*l5*sa1**2*sa2*sa3*sb**2 + ca1*complex(0,1)*l4*sa1*sa2**2*sa3**2*sb**2 + ca1*complex(0,1)*l5*sa1*sa2**2*sa3**2*sb**2',
                 order = {'QED':2})

GC_82 = Coupling(name = 'GC_82',
                 value = '2*ca1*ca3**2*cb**2*complex(0,1)*l5*sa1 + 2*ca1**2*ca3*cb**2*complex(0,1)*l5*sa2*sa3 - 2*ca3*cb**2*complex(0,1)*l5*sa1**2*sa2*sa3 - 2*ca1*cb**2*complex(0,1)*l5*sa1*sa2**2*sa3**2 - ca1**2*ca3**2*cb*complex(0,1)*l2*sb + ca1**2*ca3**2*cb*complex(0,1)*l3*sb + ca1**2*ca3**2*cb*complex(0,1)*l4*sb - ca1**2*ca3**2*cb*complex(0,1)*l5*sb + ca3**2*cb*complex(0,1)*l1*sa1**2*sb - ca3**2*cb*complex(0,1)*l3*sa1**2*sb - ca3**2*cb*complex(0,1)*l4*sa1**2*sb + ca3**2*cb*complex(0,1)*l5*sa1**2*sb + 2*ca1*ca3*cb*complex(0,1)*l1*sa1*sa2*sa3*sb + 2*ca1*ca3*cb*complex(0,1)*l2*sa1*sa2*sa3*sb - 4*ca1*ca3*cb*complex(0,1)*l3*sa1*sa2*sa3*sb - 4*ca1*ca3*cb*complex(0,1)*l4*sa1*sa2*sa3*sb + 4*ca1*ca3*cb*complex(0,1)*l5*sa1*sa2*sa3*sb + ca2**2*cb*complex(0,1)*l7*sa3**2*sb - ca2**2*cb*complex(0,1)*l8*sa3**2*sb + ca1**2*cb*complex(0,1)*l1*sa2**2*sa3**2*sb - ca1**2*cb*complex(0,1)*l3*sa2**2*sa3**2*sb - ca1**2*cb*complex(0,1)*l4*sa2**2*sa3**2*sb + ca1**2*cb*complex(0,1)*l5*sa2**2*sa3**2*sb - cb*complex(0,1)*l2*sa1**2*sa2**2*sa3**2*sb + cb*complex(0,1)*l3*sa1**2*sa2**2*sa3**2*sb + cb*complex(0,1)*l4*sa1**2*sa2**2*sa3**2*sb - cb*complex(0,1)*l5*sa1**2*sa2**2*sa3**2*sb - 2*ca1*ca3**2*complex(0,1)*l5*sa1*sb**2 - 2*ca1**2*ca3*complex(0,1)*l5*sa2*sa3*sb**2 + 2*ca3*complex(0,1)*l5*sa1**2*sa2*sa3*sb**2 + 2*ca1*complex(0,1)*l5*sa1*sa2**2*sa3**2*sb**2',
                 order = {'QED':2})

GC_83 = Coupling(name = 'GC_83',
                 value = '-(ca1**2*ca3**2*cb**2*complex(0,1)*l3) - ca3**2*cb**2*complex(0,1)*l1*sa1**2 - 2*ca1*ca3*cb**2*complex(0,1)*l1*sa1*sa2*sa3 + 2*ca1*ca3*cb**2*complex(0,1)*l3*sa1*sa2*sa3 - ca2**2*cb**2*complex(0,1)*l7*sa3**2 - ca1**2*cb**2*complex(0,1)*l1*sa2**2*sa3**2 - cb**2*complex(0,1)*l3*sa1**2*sa2**2*sa3**2 + 2*ca1*ca3**2*cb*complex(0,1)*l4*sa1*sb + 2*ca1*ca3**2*cb*complex(0,1)*l5*sa1*sb + 2*ca1**2*ca3*cb*complex(0,1)*l4*sa2*sa3*sb + 2*ca1**2*ca3*cb*complex(0,1)*l5*sa2*sa3*sb - 2*ca3*cb*complex(0,1)*l4*sa1**2*sa2*sa3*sb - 2*ca3*cb*complex(0,1)*l5*sa1**2*sa2*sa3*sb - 2*ca1*cb*complex(0,1)*l4*sa1*sa2**2*sa3**2*sb - 2*ca1*cb*complex(0,1)*l5*sa1*sa2**2*sa3**2*sb - ca1**2*ca3**2*complex(0,1)*l2*sb**2 - ca3**2*complex(0,1)*l3*sa1**2*sb**2 + 2*ca1*ca3*complex(0,1)*l2*sa1*sa2*sa3*sb**2 - 2*ca1*ca3*complex(0,1)*l3*sa1*sa2*sa3*sb**2 - ca2**2*complex(0,1)*l8*sa3**2*sb**2 - ca1**2*complex(0,1)*l3*sa2**2*sa3**2*sb**2 - complex(0,1)*l2*sa1**2*sa2**2*sa3**2*sb**2',
                 order = {'QED':2})

GC_84 = Coupling(name = 'GC_84',
                 value = '-(ca1**2*ca3**2*cb**2*complex(0,1)*l3) - ca1**2*ca3**2*cb**2*complex(0,1)*l4 + ca1**2*ca3**2*cb**2*complex(0,1)*l5 - ca3**2*cb**2*complex(0,1)*l1*sa1**2 - 2*ca1*ca3*cb**2*complex(0,1)*l1*sa1*sa2*sa3 + 2*ca1*ca3*cb**2*complex(0,1)*l3*sa1*sa2*sa3 + 2*ca1*ca3*cb**2*complex(0,1)*l4*sa1*sa2*sa3 - 2*ca1*ca3*cb**2*complex(0,1)*l5*sa1*sa2*sa3 - ca2**2*cb**2*complex(0,1)*l7*sa3**2 - ca1**2*cb**2*complex(0,1)*l1*sa2**2*sa3**2 - cb**2*complex(0,1)*l3*sa1**2*sa2**2*sa3**2 - cb**2*complex(0,1)*l4*sa1**2*sa2**2*sa3**2 + cb**2*complex(0,1)*l5*sa1**2*sa2**2*sa3**2 + 4*ca1*ca3**2*cb*complex(0,1)*l5*sa1*sb + 4*ca1**2*ca3*cb*complex(0,1)*l5*sa2*sa3*sb - 4*ca3*cb*complex(0,1)*l5*sa1**2*sa2*sa3*sb - 4*ca1*cb*complex(0,1)*l5*sa1*sa2**2*sa3**2*sb - ca1**2*ca3**2*complex(0,1)*l2*sb**2 - ca3**2*complex(0,1)*l3*sa1**2*sb**2 - ca3**2*complex(0,1)*l4*sa1**2*sb**2 + ca3**2*complex(0,1)*l5*sa1**2*sb**2 + 2*ca1*ca3*complex(0,1)*l2*sa1*sa2*sa3*sb**2 - 2*ca1*ca3*complex(0,1)*l3*sa1*sa2*sa3*sb**2 - 2*ca1*ca3*complex(0,1)*l4*sa1*sa2*sa3*sb**2 + 2*ca1*ca3*complex(0,1)*l5*sa1*sa2*sa3*sb**2 - ca2**2*complex(0,1)*l8*sa3**2*sb**2 - ca1**2*complex(0,1)*l3*sa2**2*sa3**2*sb**2 - ca1**2*complex(0,1)*l4*sa2**2*sa3**2*sb**2 + ca1**2*complex(0,1)*l5*sa2**2*sa3**2*sb**2 - complex(0,1)*l2*sa1**2*sa2**2*sa3**2*sb**2',
                 order = {'QED':2})

GC_85 = Coupling(name = 'GC_85',
                 value = '-(ca1**2*ca3**2*cb**2*complex(0,1)*l2) - ca3**2*cb**2*complex(0,1)*l3*sa1**2 + 2*ca1*ca3*cb**2*complex(0,1)*l2*sa1*sa2*sa3 - 2*ca1*ca3*cb**2*complex(0,1)*l3*sa1*sa2*sa3 - ca2**2*cb**2*complex(0,1)*l8*sa3**2 - ca1**2*cb**2*complex(0,1)*l3*sa2**2*sa3**2 - cb**2*complex(0,1)*l2*sa1**2*sa2**2*sa3**2 - 2*ca1*ca3**2*cb*complex(0,1)*l4*sa1*sb - 2*ca1*ca3**2*cb*complex(0,1)*l5*sa1*sb - 2*ca1**2*ca3*cb*complex(0,1)*l4*sa2*sa3*sb - 2*ca1**2*ca3*cb*complex(0,1)*l5*sa2*sa3*sb + 2*ca3*cb*complex(0,1)*l4*sa1**2*sa2*sa3*sb + 2*ca3*cb*complex(0,1)*l5*sa1**2*sa2*sa3*sb + 2*ca1*cb*complex(0,1)*l4*sa1*sa2**2*sa3**2*sb + 2*ca1*cb*complex(0,1)*l5*sa1*sa2**2*sa3**2*sb - ca1**2*ca3**2*complex(0,1)*l3*sb**2 - ca3**2*complex(0,1)*l1*sa1**2*sb**2 - 2*ca1*ca3*complex(0,1)*l1*sa1*sa2*sa3*sb**2 + 2*ca1*ca3*complex(0,1)*l3*sa1*sa2*sa3*sb**2 - ca2**2*complex(0,1)*l7*sa3**2*sb**2 - ca1**2*complex(0,1)*l1*sa2**2*sa3**2*sb**2 - complex(0,1)*l3*sa1**2*sa2**2*sa3**2*sb**2',
                 order = {'QED':2})

GC_86 = Coupling(name = 'GC_86',
                 value = '-(ca1**2*ca3**2*cb**2*complex(0,1)*l2) - ca3**2*cb**2*complex(0,1)*l3*sa1**2 - ca3**2*cb**2*complex(0,1)*l4*sa1**2 + ca3**2*cb**2*complex(0,1)*l5*sa1**2 + 2*ca1*ca3*cb**2*complex(0,1)*l2*sa1*sa2*sa3 - 2*ca1*ca3*cb**2*complex(0,1)*l3*sa1*sa2*sa3 - 2*ca1*ca3*cb**2*complex(0,1)*l4*sa1*sa2*sa3 + 2*ca1*ca3*cb**2*complex(0,1)*l5*sa1*sa2*sa3 - ca2**2*cb**2*complex(0,1)*l8*sa3**2 - ca1**2*cb**2*complex(0,1)*l3*sa2**2*sa3**2 - ca1**2*cb**2*complex(0,1)*l4*sa2**2*sa3**2 + ca1**2*cb**2*complex(0,1)*l5*sa2**2*sa3**2 - cb**2*complex(0,1)*l2*sa1**2*sa2**2*sa3**2 - 4*ca1*ca3**2*cb*complex(0,1)*l5*sa1*sb - 4*ca1**2*ca3*cb*complex(0,1)*l5*sa2*sa3*sb + 4*ca3*cb*complex(0,1)*l5*sa1**2*sa2*sa3*sb + 4*ca1*cb*complex(0,1)*l5*sa1*sa2**2*sa3**2*sb - ca1**2*ca3**2*complex(0,1)*l3*sb**2 - ca1**2*ca3**2*complex(0,1)*l4*sb**2 + ca1**2*ca3**2*complex(0,1)*l5*sb**2 - ca3**2*complex(0,1)*l1*sa1**2*sb**2 - 2*ca1*ca3*complex(0,1)*l1*sa1*sa2*sa3*sb**2 + 2*ca1*ca3*complex(0,1)*l3*sa1*sa2*sa3*sb**2 + 2*ca1*ca3*complex(0,1)*l4*sa1*sa2*sa3*sb**2 - 2*ca1*ca3*complex(0,1)*l5*sa1*sa2*sa3*sb**2 - ca2**2*complex(0,1)*l7*sa3**2*sb**2 - ca1**2*complex(0,1)*l1*sa2**2*sa3**2*sb**2 - complex(0,1)*l3*sa1**2*sa2**2*sa3**2*sb**2 - complex(0,1)*l4*sa1**2*sa2**2*sa3**2*sb**2 + complex(0,1)*l5*sa1**2*sa2**2*sa3**2*sb**2',
                 order = {'QED':2})

GC_87 = Coupling(name = 'GC_87',
                 value = '(ca2*cb**3*ee**2*sa1)/(4.*cw**2) - (ca1*ca2*cb**2*ee**2*sb)/(4.*cw**2) + (ca2*cb*ee**2*sa1*sb**2)/(4.*cw**2) - (ca1*ca2*ee**2*sb**3)/(4.*cw**2)',
                 order = {'QED':2})

GC_88 = Coupling(name = 'GC_88',
                 value = '-(ca2*cb**3*ee**2*sa1)/(4.*cw**2) + (ca1*ca2*cb**2*ee**2*sb)/(4.*cw**2) - (ca2*cb*ee**2*sa1*sb**2)/(4.*cw**2) + (ca1*ca2*ee**2*sb**3)/(4.*cw**2)',
                 order = {'QED':2})

GC_89 = Coupling(name = 'GC_89',
                 value = '-(ca2*cb**3*l4*sa1)/2. + (ca2*cb**3*l5*sa1)/2. + (ca1*ca2*cb**2*l4*sb)/2. - (ca1*ca2*cb**2*l5*sb)/2. - (ca2*cb*l4*sa1*sb**2)/2. + (ca2*cb*l5*sa1*sb**2)/2. + (ca1*ca2*l4*sb**3)/2. - (ca1*ca2*l5*sb**3)/2.',
                 order = {'QED':2})

GC_90 = Coupling(name = 'GC_90',
                 value = '(ca2*cb**3*l4*sa1)/2. - (ca2*cb**3*l5*sa1)/2. - (ca1*ca2*cb**2*l4*sb)/2. + (ca1*ca2*cb**2*l5*sb)/2. + (ca2*cb*l4*sa1*sb**2)/2. - (ca2*cb*l5*sa1*sb**2)/2. - (ca1*ca2*l4*sb**3)/2. + (ca1*ca2*l5*sb**3)/2.',
                 order = {'QED':2})

GC_91 = Coupling(name = 'GC_91',
                 value = '-(cb**3*complex(0,1)*l2*sb) + cb**3*complex(0,1)*l3*sb + cb**3*complex(0,1)*l4*sb + cb**3*complex(0,1)*l5*sb + cb*complex(0,1)*l1*sb**3 - cb*complex(0,1)*l3*sb**3 - cb*complex(0,1)*l4*sb**3 - cb*complex(0,1)*l5*sb**3',
                 order = {'QED':2})

GC_92 = Coupling(name = 'GC_92',
                 value = 'cb**3*complex(0,1)*l1*sb - cb**3*complex(0,1)*l3*sb - cb**3*complex(0,1)*l4*sb - cb**3*complex(0,1)*l5*sb - cb*complex(0,1)*l2*sb**3 + cb*complex(0,1)*l3*sb**3 + cb*complex(0,1)*l4*sb**3 + cb*complex(0,1)*l5*sb**3',
                 order = {'QED':2})

GC_93 = Coupling(name = 'GC_93',
                 value = '-2*cb**3*complex(0,1)*l2*sb + 2*cb**3*complex(0,1)*l3*sb + 2*cb**3*complex(0,1)*l4*sb + 2*cb**3*complex(0,1)*l5*sb + 2*cb*complex(0,1)*l1*sb**3 - 2*cb*complex(0,1)*l3*sb**3 - 2*cb*complex(0,1)*l4*sb**3 - 2*cb*complex(0,1)*l5*sb**3',
                 order = {'QED':2})

GC_94 = Coupling(name = 'GC_94',
                 value = '2*cb**3*complex(0,1)*l1*sb - 2*cb**3*complex(0,1)*l3*sb - 2*cb**3*complex(0,1)*l4*sb - 2*cb**3*complex(0,1)*l5*sb - 2*cb*complex(0,1)*l2*sb**3 + 2*cb*complex(0,1)*l3*sb**3 + 2*cb*complex(0,1)*l4*sb**3 + 2*cb*complex(0,1)*l5*sb**3',
                 order = {'QED':2})

GC_95 = Coupling(name = 'GC_95',
                 value = '-3*cb**3*complex(0,1)*l2*sb + 3*cb**3*complex(0,1)*l3*sb + 3*cb**3*complex(0,1)*l4*sb + 3*cb**3*complex(0,1)*l5*sb + 3*cb*complex(0,1)*l1*sb**3 - 3*cb*complex(0,1)*l3*sb**3 - 3*cb*complex(0,1)*l4*sb**3 - 3*cb*complex(0,1)*l5*sb**3',
                 order = {'QED':2})

GC_96 = Coupling(name = 'GC_96',
                 value = '3*cb**3*complex(0,1)*l1*sb - 3*cb**3*complex(0,1)*l3*sb - 3*cb**3*complex(0,1)*l4*sb - 3*cb**3*complex(0,1)*l5*sb - 3*cb*complex(0,1)*l2*sb**3 + 3*cb*complex(0,1)*l3*sb**3 + 3*cb*complex(0,1)*l4*sb**3 + 3*cb*complex(0,1)*l5*sb**3',
                 order = {'QED':2})

GC_97 = Coupling(name = 'GC_97',
                 value = '-(ca1*ca2*cb**3*ee**2)/(4.*cw**2) - (ca2*cb**2*ee**2*sa1*sb)/(4.*cw**2) - (ca1*ca2*cb*ee**2*sb**2)/(4.*cw**2) - (ca2*ee**2*sa1*sb**3)/(4.*cw**2)',
                 order = {'QED':2})

GC_98 = Coupling(name = 'GC_98',
                 value = '(ca1*ca2*cb**3*ee**2)/(4.*cw**2) + (ca2*cb**2*ee**2*sa1*sb)/(4.*cw**2) + (ca1*ca2*cb*ee**2*sb**2)/(4.*cw**2) + (ca2*ee**2*sa1*sb**3)/(4.*cw**2)',
                 order = {'QED':2})

GC_99 = Coupling(name = 'GC_99',
                 value = '(ca1*ca2*cb**3*l4)/2. - (ca1*ca2*cb**3*l5)/2. + (ca2*cb**2*l4*sa1*sb)/2. - (ca2*cb**2*l5*sa1*sb)/2. + (ca1*ca2*cb*l4*sb**2)/2. - (ca1*ca2*cb*l5*sb**2)/2. + (ca2*l4*sa1*sb**3)/2. - (ca2*l5*sa1*sb**3)/2.',
                 order = {'QED':2})

GC_100 = Coupling(name = 'GC_100',
                  value = '-(ca1*ca2*cb**3*l4)/2. + (ca1*ca2*cb**3*l5)/2. - (ca2*cb**2*l4*sa1*sb)/2. + (ca2*cb**2*l5*sa1*sb)/2. - (ca1*ca2*cb*l4*sb**2)/2. + (ca1*ca2*cb*l5*sb**2)/2. - (ca2*l4*sa1*sb**3)/2. + (ca2*l5*sa1*sb**3)/2.',
                  order = {'QED':2})

GC_101 = Coupling(name = 'GC_101',
                  value = '-(ca1*ca3*cb**3*ee**2*sa2)/(4.*cw**2) + (cb**3*ee**2*sa1*sa3)/(4.*cw**2) - (ca3*cb**2*ee**2*sa1*sa2*sb)/(4.*cw**2) - (ca1*cb**2*ee**2*sa3*sb)/(4.*cw**2) - (ca1*ca3*cb*ee**2*sa2*sb**2)/(4.*cw**2) + (cb*ee**2*sa1*sa3*sb**2)/(4.*cw**2) - (ca3*ee**2*sa1*sa2*sb**3)/(4.*cw**2) - (ca1*ee**2*sa3*sb**3)/(4.*cw**2)',
                  order = {'QED':2})

GC_102 = Coupling(name = 'GC_102',
                  value = '(ca1*ca3*cb**3*ee**2*sa2)/(4.*cw**2) - (cb**3*ee**2*sa1*sa3)/(4.*cw**2) + (ca3*cb**2*ee**2*sa1*sa2*sb)/(4.*cw**2) + (ca1*cb**2*ee**2*sa3*sb)/(4.*cw**2) + (ca1*ca3*cb*ee**2*sa2*sb**2)/(4.*cw**2) - (cb*ee**2*sa1*sa3*sb**2)/(4.*cw**2) + (ca3*ee**2*sa1*sa2*sb**3)/(4.*cw**2) + (ca1*ee**2*sa3*sb**3)/(4.*cw**2)',
                  order = {'QED':2})

GC_103 = Coupling(name = 'GC_103',
                  value = '(ca1*ca3*cb**3*l4*sa2)/2. - (ca1*ca3*cb**3*l5*sa2)/2. - (cb**3*l4*sa1*sa3)/2. + (cb**3*l5*sa1*sa3)/2. + (ca3*cb**2*l4*sa1*sa2*sb)/2. - (ca3*cb**2*l5*sa1*sa2*sb)/2. + (ca1*cb**2*l4*sa3*sb)/2. - (ca1*cb**2*l5*sa3*sb)/2. + (ca1*ca3*cb*l4*sa2*sb**2)/2. - (ca1*ca3*cb*l5*sa2*sb**2)/2. - (cb*l4*sa1*sa3*sb**2)/2. + (cb*l5*sa1*sa3*sb**2)/2. + (ca3*l4*sa1*sa2*sb**3)/2. - (ca3*l5*sa1*sa2*sb**3)/2. + (ca1*l4*sa3*sb**3)/2. - (ca1*l5*sa3*sb**3)/2.',
                  order = {'QED':2})

GC_104 = Coupling(name = 'GC_104',
                  value = '-(ca1*ca3*cb**3*l4*sa2)/2. + (ca1*ca3*cb**3*l5*sa2)/2. + (cb**3*l4*sa1*sa3)/2. - (cb**3*l5*sa1*sa3)/2. - (ca3*cb**2*l4*sa1*sa2*sb)/2. + (ca3*cb**2*l5*sa1*sa2*sb)/2. - (ca1*cb**2*l4*sa3*sb)/2. + (ca1*cb**2*l5*sa3*sb)/2. - (ca1*ca3*cb*l4*sa2*sb**2)/2. + (ca1*ca3*cb*l5*sa2*sb**2)/2. + (cb*l4*sa1*sa3*sb**2)/2. - (cb*l5*sa1*sa3*sb**2)/2. - (ca3*l4*sa1*sa2*sb**3)/2. + (ca3*l5*sa1*sa2*sb**3)/2. - (ca1*l4*sa3*sb**3)/2. + (ca1*l5*sa3*sb**3)/2.',
                  order = {'QED':2})

GC_105 = Coupling(name = 'GC_105',
                  value = '-(ca3*cb**3*ee**2*sa1*sa2)/(4.*cw**2) - (ca1*cb**3*ee**2*sa3)/(4.*cw**2) + (ca1*ca3*cb**2*ee**2*sa2*sb)/(4.*cw**2) - (cb**2*ee**2*sa1*sa3*sb)/(4.*cw**2) - (ca3*cb*ee**2*sa1*sa2*sb**2)/(4.*cw**2) - (ca1*cb*ee**2*sa3*sb**2)/(4.*cw**2) + (ca1*ca3*ee**2*sa2*sb**3)/(4.*cw**2) - (ee**2*sa1*sa3*sb**3)/(4.*cw**2)',
                  order = {'QED':2})

GC_106 = Coupling(name = 'GC_106',
                  value = '(ca3*cb**3*ee**2*sa1*sa2)/(4.*cw**2) + (ca1*cb**3*ee**2*sa3)/(4.*cw**2) - (ca1*ca3*cb**2*ee**2*sa2*sb)/(4.*cw**2) + (cb**2*ee**2*sa1*sa3*sb)/(4.*cw**2) + (ca3*cb*ee**2*sa1*sa2*sb**2)/(4.*cw**2) + (ca1*cb*ee**2*sa3*sb**2)/(4.*cw**2) - (ca1*ca3*ee**2*sa2*sb**3)/(4.*cw**2) + (ee**2*sa1*sa3*sb**3)/(4.*cw**2)',
                  order = {'QED':2})

GC_107 = Coupling(name = 'GC_107',
                  value = '(ca3*cb**3*l4*sa1*sa2)/2. - (ca3*cb**3*l5*sa1*sa2)/2. + (ca1*cb**3*l4*sa3)/2. - (ca1*cb**3*l5*sa3)/2. - (ca1*ca3*cb**2*l4*sa2*sb)/2. + (ca1*ca3*cb**2*l5*sa2*sb)/2. + (cb**2*l4*sa1*sa3*sb)/2. - (cb**2*l5*sa1*sa3*sb)/2. + (ca3*cb*l4*sa1*sa2*sb**2)/2. - (ca3*cb*l5*sa1*sa2*sb**2)/2. + (ca1*cb*l4*sa3*sb**2)/2. - (ca1*cb*l5*sa3*sb**2)/2. - (ca1*ca3*l4*sa2*sb**3)/2. + (ca1*ca3*l5*sa2*sb**3)/2. + (l4*sa1*sa3*sb**3)/2. - (l5*sa1*sa3*sb**3)/2.',
                  order = {'QED':2})

GC_108 = Coupling(name = 'GC_108',
                  value = '-(ca3*cb**3*l4*sa1*sa2)/2. + (ca3*cb**3*l5*sa1*sa2)/2. - (ca1*cb**3*l4*sa3)/2. + (ca1*cb**3*l5*sa3)/2. + (ca1*ca3*cb**2*l4*sa2*sb)/2. - (ca1*ca3*cb**2*l5*sa2*sb)/2. - (cb**2*l4*sa1*sa3*sb)/2. + (cb**2*l5*sa1*sa3*sb)/2. - (ca3*cb*l4*sa1*sa2*sb**2)/2. + (ca3*cb*l5*sa1*sa2*sb**2)/2. - (ca1*cb*l4*sa3*sb**2)/2. + (ca1*cb*l5*sa3*sb**2)/2. + (ca1*ca3*l4*sa2*sb**3)/2. - (ca1*ca3*l5*sa2*sb**3)/2. - (l4*sa1*sa3*sb**3)/2. + (l5*sa1*sa3*sb**3)/2.',
                  order = {'QED':2})

GC_109 = Coupling(name = 'GC_109',
                  value = '-(ca1*ca3*cb**3*ee**2)/(4.*cw**2) + (cb**3*ee**2*sa1*sa2*sa3)/(4.*cw**2) - (ca3*cb**2*ee**2*sa1*sb)/(4.*cw**2) - (ca1*cb**2*ee**2*sa2*sa3*sb)/(4.*cw**2) - (ca1*ca3*cb*ee**2*sb**2)/(4.*cw**2) + (cb*ee**2*sa1*sa2*sa3*sb**2)/(4.*cw**2) - (ca3*ee**2*sa1*sb**3)/(4.*cw**2) - (ca1*ee**2*sa2*sa3*sb**3)/(4.*cw**2)',
                  order = {'QED':2})

GC_110 = Coupling(name = 'GC_110',
                  value = '(ca1*ca3*cb**3*ee**2)/(4.*cw**2) - (cb**3*ee**2*sa1*sa2*sa3)/(4.*cw**2) + (ca3*cb**2*ee**2*sa1*sb)/(4.*cw**2) + (ca1*cb**2*ee**2*sa2*sa3*sb)/(4.*cw**2) + (ca1*ca3*cb*ee**2*sb**2)/(4.*cw**2) - (cb*ee**2*sa1*sa2*sa3*sb**2)/(4.*cw**2) + (ca3*ee**2*sa1*sb**3)/(4.*cw**2) + (ca1*ee**2*sa2*sa3*sb**3)/(4.*cw**2)',
                  order = {'QED':2})

GC_111 = Coupling(name = 'GC_111',
                  value = '(ca1*ca3*cb**3*l4)/2. - (ca1*ca3*cb**3*l5)/2. - (cb**3*l4*sa1*sa2*sa3)/2. + (cb**3*l5*sa1*sa2*sa3)/2. + (ca3*cb**2*l4*sa1*sb)/2. - (ca3*cb**2*l5*sa1*sb)/2. + (ca1*cb**2*l4*sa2*sa3*sb)/2. - (ca1*cb**2*l5*sa2*sa3*sb)/2. + (ca1*ca3*cb*l4*sb**2)/2. - (ca1*ca3*cb*l5*sb**2)/2. - (cb*l4*sa1*sa2*sa3*sb**2)/2. + (cb*l5*sa1*sa2*sa3*sb**2)/2. + (ca3*l4*sa1*sb**3)/2. - (ca3*l5*sa1*sb**3)/2. + (ca1*l4*sa2*sa3*sb**3)/2. - (ca1*l5*sa2*sa3*sb**3)/2.',
                  order = {'QED':2})

GC_112 = Coupling(name = 'GC_112',
                  value = '-(ca1*ca3*cb**3*l4)/2. + (ca1*ca3*cb**3*l5)/2. + (cb**3*l4*sa1*sa2*sa3)/2. - (cb**3*l5*sa1*sa2*sa3)/2. - (ca3*cb**2*l4*sa1*sb)/2. + (ca3*cb**2*l5*sa1*sb)/2. - (ca1*cb**2*l4*sa2*sa3*sb)/2. + (ca1*cb**2*l5*sa2*sa3*sb)/2. - (ca1*ca3*cb*l4*sb**2)/2. + (ca1*ca3*cb*l5*sb**2)/2. + (cb*l4*sa1*sa2*sa3*sb**2)/2. - (cb*l5*sa1*sa2*sa3*sb**2)/2. - (ca3*l4*sa1*sb**3)/2. + (ca3*l5*sa1*sb**3)/2. - (ca1*l4*sa2*sa3*sb**3)/2. + (ca1*l5*sa2*sa3*sb**3)/2.',
                  order = {'QED':2})

GC_113 = Coupling(name = 'GC_113',
                  value = '-(ca3*cb**3*ee**2*sa1)/(4.*cw**2) - (ca1*cb**3*ee**2*sa2*sa3)/(4.*cw**2) + (ca1*ca3*cb**2*ee**2*sb)/(4.*cw**2) - (cb**2*ee**2*sa1*sa2*sa3*sb)/(4.*cw**2) - (ca3*cb*ee**2*sa1*sb**2)/(4.*cw**2) - (ca1*cb*ee**2*sa2*sa3*sb**2)/(4.*cw**2) + (ca1*ca3*ee**2*sb**3)/(4.*cw**2) - (ee**2*sa1*sa2*sa3*sb**3)/(4.*cw**2)',
                  order = {'QED':2})

GC_114 = Coupling(name = 'GC_114',
                  value = '(ca3*cb**3*ee**2*sa1)/(4.*cw**2) + (ca1*cb**3*ee**2*sa2*sa3)/(4.*cw**2) - (ca1*ca3*cb**2*ee**2*sb)/(4.*cw**2) + (cb**2*ee**2*sa1*sa2*sa3*sb)/(4.*cw**2) + (ca3*cb*ee**2*sa1*sb**2)/(4.*cw**2) + (ca1*cb*ee**2*sa2*sa3*sb**2)/(4.*cw**2) - (ca1*ca3*ee**2*sb**3)/(4.*cw**2) + (ee**2*sa1*sa2*sa3*sb**3)/(4.*cw**2)',
                  order = {'QED':2})

GC_115 = Coupling(name = 'GC_115',
                  value = '(ca3*cb**3*l4*sa1)/2. - (ca3*cb**3*l5*sa1)/2. + (ca1*cb**3*l4*sa2*sa3)/2. - (ca1*cb**3*l5*sa2*sa3)/2. - (ca1*ca3*cb**2*l4*sb)/2. + (ca1*ca3*cb**2*l5*sb)/2. + (cb**2*l4*sa1*sa2*sa3*sb)/2. - (cb**2*l5*sa1*sa2*sa3*sb)/2. + (ca3*cb*l4*sa1*sb**2)/2. - (ca3*cb*l5*sa1*sb**2)/2. + (ca1*cb*l4*sa2*sa3*sb**2)/2. - (ca1*cb*l5*sa2*sa3*sb**2)/2. - (ca1*ca3*l4*sb**3)/2. + (ca1*ca3*l5*sb**3)/2. + (l4*sa1*sa2*sa3*sb**3)/2. - (l5*sa1*sa2*sa3*sb**3)/2.',
                  order = {'QED':2})

GC_116 = Coupling(name = 'GC_116',
                  value = '-(ca3*cb**3*l4*sa1)/2. + (ca3*cb**3*l5*sa1)/2. - (ca1*cb**3*l4*sa2*sa3)/2. + (ca1*cb**3*l5*sa2*sa3)/2. + (ca1*ca3*cb**2*l4*sb)/2. - (ca1*ca3*cb**2*l5*sb)/2. - (cb**2*l4*sa1*sa2*sa3*sb)/2. + (cb**2*l5*sa1*sa2*sa3*sb)/2. - (ca3*cb*l4*sa1*sb**2)/2. + (ca3*cb*l5*sa1*sb**2)/2. - (ca1*cb*l4*sa2*sa3*sb**2)/2. + (ca1*cb*l5*sa2*sa3*sb**2)/2. + (ca1*ca3*l4*sb**3)/2. - (ca1*ca3*l5*sb**3)/2. - (l4*sa1*sa2*sa3*sb**3)/2. + (l5*sa1*sa2*sa3*sb**3)/2.',
                  order = {'QED':2})

GC_117 = Coupling(name = 'GC_117',
                  value = '-(cb**4*complex(0,1)*l2) - 2*cb**2*complex(0,1)*l3*sb**2 - 2*cb**2*complex(0,1)*l4*sb**2 - 2*cb**2*complex(0,1)*l5*sb**2 - complex(0,1)*l1*sb**4',
                  order = {'QED':2})

GC_118 = Coupling(name = 'GC_118',
                  value = '-2*cb**4*complex(0,1)*l2 - 4*cb**2*complex(0,1)*l3*sb**2 - 4*cb**2*complex(0,1)*l4*sb**2 - 4*cb**2*complex(0,1)*l5*sb**2 - 2*complex(0,1)*l1*sb**4',
                  order = {'QED':2})

GC_119 = Coupling(name = 'GC_119',
                  value = '-3*cb**4*complex(0,1)*l2 - 6*cb**2*complex(0,1)*l3*sb**2 - 6*cb**2*complex(0,1)*l4*sb**2 - 6*cb**2*complex(0,1)*l5*sb**2 - 3*complex(0,1)*l1*sb**4',
                  order = {'QED':2})

GC_120 = Coupling(name = 'GC_120',
                  value = '-(cb**4*complex(0,1)*l1) - 2*cb**2*complex(0,1)*l3*sb**2 - 2*cb**2*complex(0,1)*l4*sb**2 - 2*cb**2*complex(0,1)*l5*sb**2 - complex(0,1)*l2*sb**4',
                  order = {'QED':2})

GC_121 = Coupling(name = 'GC_121',
                  value = '-2*cb**4*complex(0,1)*l1 - 4*cb**2*complex(0,1)*l3*sb**2 - 4*cb**2*complex(0,1)*l4*sb**2 - 4*cb**2*complex(0,1)*l5*sb**2 - 2*complex(0,1)*l2*sb**4',
                  order = {'QED':2})

GC_122 = Coupling(name = 'GC_122',
                  value = '-3*cb**4*complex(0,1)*l1 - 6*cb**2*complex(0,1)*l3*sb**2 - 6*cb**2*complex(0,1)*l4*sb**2 - 6*cb**2*complex(0,1)*l5*sb**2 - 3*complex(0,1)*l2*sb**4',
                  order = {'QED':2})

GC_123 = Coupling(name = 'GC_123',
                  value = '-(cb**4*complex(0,1)*l3) - cb**2*complex(0,1)*l1*sb**2 - cb**2*complex(0,1)*l2*sb**2 + 2*cb**2*complex(0,1)*l4*sb**2 + 2*cb**2*complex(0,1)*l5*sb**2 - complex(0,1)*l3*sb**4',
                  order = {'QED':2})

GC_124 = Coupling(name = 'GC_124',
                  value = '-(cb**4*complex(0,1)*l3) - cb**4*complex(0,1)*l4 - 2*cb**2*complex(0,1)*l1*sb**2 - 2*cb**2*complex(0,1)*l2*sb**2 + 2*cb**2*complex(0,1)*l3*sb**2 + 2*cb**2*complex(0,1)*l4*sb**2 + 4*cb**2*complex(0,1)*l5*sb**2 - complex(0,1)*l3*sb**4 - complex(0,1)*l4*sb**4',
                  order = {'QED':2})

GC_125 = Coupling(name = 'GC_125',
                  value = '-(cb**4*complex(0,1)*l4)/2. - (cb**4*complex(0,1)*l5)/2. - cb**2*complex(0,1)*l1*sb**2 - cb**2*complex(0,1)*l2*sb**2 + 2*cb**2*complex(0,1)*l3*sb**2 + cb**2*complex(0,1)*l4*sb**2 + cb**2*complex(0,1)*l5*sb**2 - (complex(0,1)*l4*sb**4)/2. - (complex(0,1)*l5*sb**4)/2.',
                  order = {'QED':2})

GC_126 = Coupling(name = 'GC_126',
                  value = '-(cb**4*complex(0,1)*l3) - cb**4*complex(0,1)*l4 - cb**4*complex(0,1)*l5 - 3*cb**2*complex(0,1)*l1*sb**2 - 3*cb**2*complex(0,1)*l2*sb**2 + 4*cb**2*complex(0,1)*l3*sb**2 + 4*cb**2*complex(0,1)*l4*sb**2 + 4*cb**2*complex(0,1)*l5*sb**2 - complex(0,1)*l3*sb**4 - complex(0,1)*l4*sb**4 - complex(0,1)*l5*sb**4',
                  order = {'QED':2})

GC_127 = Coupling(name = 'GC_127',
                  value = '-2*cb**4*complex(0,1)*l5 - 2*cb**2*complex(0,1)*l1*sb**2 - 2*cb**2*complex(0,1)*l2*sb**2 + 4*cb**2*complex(0,1)*l3*sb**2 + 4*cb**2*complex(0,1)*l4*sb**2 - 2*complex(0,1)*l5*sb**4',
                  order = {'QED':2})

GC_128 = Coupling(name = 'GC_128',
                  value = '-(ca1**2*ca2**2*ee**2*complex(0,1))/(4.*sw**2) - (ca2**2*ee**2*complex(0,1)*sa1**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_129 = Coupling(name = 'GC_129',
                  value = '-(ca1**2*ca2**2*ee**2*complex(0,1))/(2.*sw**2) - (ca2**2*ee**2*complex(0,1)*sa1**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_130 = Coupling(name = 'GC_130',
                  value = '(ca1**2*ca2**2*ee**2*complex(0,1))/(2.*sw**2) + (ca2**2*ee**2*complex(0,1)*sa1**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_131 = Coupling(name = 'GC_131',
                  value = '(ca1**2*ca2*ca3*ee**2*complex(0,1)*sa2)/(4.*sw**2) + (ca2*ca3*ee**2*complex(0,1)*sa1**2*sa2)/(4.*sw**2)',
                  order = {'QED':2})

GC_132 = Coupling(name = 'GC_132',
                  value = '-(ca1**2*ca2*ca3*ee**2*complex(0,1)*sa2)/(2.*sw**2) - (ca2*ca3*ee**2*complex(0,1)*sa1**2*sa2)/(2.*sw**2)',
                  order = {'QED':2})

GC_133 = Coupling(name = 'GC_133',
                  value = '(ca1**2*ca2*ca3*ee**2*complex(0,1)*sa2)/(2.*sw**2) + (ca2*ca3*ee**2*complex(0,1)*sa1**2*sa2)/(2.*sw**2)',
                  order = {'QED':2})

GC_134 = Coupling(name = 'GC_134',
                  value = '(ca1**2*ca2*ee**2*complex(0,1)*sa2*sa3)/(4.*sw**2) + (ca2*ee**2*complex(0,1)*sa1**2*sa2*sa3)/(4.*sw**2)',
                  order = {'QED':2})

GC_135 = Coupling(name = 'GC_135',
                  value = '-(ca1**2*ca2*ee**2*complex(0,1)*sa2*sa3)/(2.*sw**2) - (ca2*ee**2*complex(0,1)*sa1**2*sa2*sa3)/(2.*sw**2)',
                  order = {'QED':2})

GC_136 = Coupling(name = 'GC_136',
                  value = '(ca1**2*ca2*ee**2*complex(0,1)*sa2*sa3)/(2.*sw**2) + (ca2*ee**2*complex(0,1)*sa1**2*sa2*sa3)/(2.*sw**2)',
                  order = {'QED':2})

GC_137 = Coupling(name = 'GC_137',
                  value = '(ca1**2*ca3*ee**2*complex(0,1)*sa3)/(4.*sw**2) + (ca3*ee**2*complex(0,1)*sa1**2*sa3)/(4.*sw**2) - (ca1**2*ca3*ee**2*complex(0,1)*sa2**2*sa3)/(4.*sw**2) - (ca3*ee**2*complex(0,1)*sa1**2*sa2**2*sa3)/(4.*sw**2)',
                  order = {'QED':2})

GC_138 = Coupling(name = 'GC_138',
                  value = '(ca1**2*ca3*ee**2*complex(0,1)*sa3)/(2.*sw**2) + (ca3*ee**2*complex(0,1)*sa1**2*sa3)/(2.*sw**2) - (ca1**2*ca3*ee**2*complex(0,1)*sa2**2*sa3)/(2.*sw**2) - (ca3*ee**2*complex(0,1)*sa1**2*sa2**2*sa3)/(2.*sw**2)',
                  order = {'QED':2})

GC_139 = Coupling(name = 'GC_139',
                  value = '-(ca1**2*ca3*ee**2*complex(0,1)*sa3)/(2.*sw**2) - (ca3*ee**2*complex(0,1)*sa1**2*sa3)/(2.*sw**2) + (ca1**2*ca3*ee**2*complex(0,1)*sa2**2*sa3)/(2.*sw**2) + (ca3*ee**2*complex(0,1)*sa1**2*sa2**2*sa3)/(2.*sw**2)',
                  order = {'QED':2})

GC_140 = Coupling(name = 'GC_140',
                  value = '-(ca1**2*ca3**2*ee**2*complex(0,1)*sa2**2)/(4.*sw**2) - (ca3**2*ee**2*complex(0,1)*sa1**2*sa2**2)/(4.*sw**2) - (ca1**2*ee**2*complex(0,1)*sa3**2)/(4.*sw**2) - (ee**2*complex(0,1)*sa1**2*sa3**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_141 = Coupling(name = 'GC_141',
                  value = '-(ca1**2*ca3**2*ee**2*complex(0,1)*sa2**2)/(2.*sw**2) - (ca3**2*ee**2*complex(0,1)*sa1**2*sa2**2)/(2.*sw**2) - (ca1**2*ee**2*complex(0,1)*sa3**2)/(2.*sw**2) - (ee**2*complex(0,1)*sa1**2*sa3**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_142 = Coupling(name = 'GC_142',
                  value = '(ca1**2*ca3**2*ee**2*complex(0,1)*sa2**2)/(2.*sw**2) + (ca3**2*ee**2*complex(0,1)*sa1**2*sa2**2)/(2.*sw**2) + (ca1**2*ee**2*complex(0,1)*sa3**2)/(2.*sw**2) + (ee**2*complex(0,1)*sa1**2*sa3**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_143 = Coupling(name = 'GC_143',
                  value = '-(ca1**2*ca3**2*ee**2*complex(0,1))/(4.*sw**2) - (ca3**2*ee**2*complex(0,1)*sa1**2)/(4.*sw**2) - (ca1**2*ee**2*complex(0,1)*sa2**2*sa3**2)/(4.*sw**2) - (ee**2*complex(0,1)*sa1**2*sa2**2*sa3**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_144 = Coupling(name = 'GC_144',
                  value = '-(ca1**2*ca3**2*ee**2*complex(0,1))/(2.*sw**2) - (ca3**2*ee**2*complex(0,1)*sa1**2)/(2.*sw**2) - (ca1**2*ee**2*complex(0,1)*sa2**2*sa3**2)/(2.*sw**2) - (ee**2*complex(0,1)*sa1**2*sa2**2*sa3**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_145 = Coupling(name = 'GC_145',
                  value = '(ca1**2*ca3**2*ee**2*complex(0,1))/(2.*sw**2) + (ca3**2*ee**2*complex(0,1)*sa1**2)/(2.*sw**2) + (ca1**2*ee**2*complex(0,1)*sa2**2*sa3**2)/(2.*sw**2) + (ee**2*complex(0,1)*sa1**2*sa2**2*sa3**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_146 = Coupling(name = 'GC_146',
                  value = '(ca2*cb*ee**2*sa1)/(2.*sw**2) - (ca1*ca2*ee**2*sb)/(2.*sw**2)',
                  order = {'QED':2})

GC_147 = Coupling(name = 'GC_147',
                  value = '(ca2*cb*ee**2*sa1)/(4.*sw**2) - (ca1*ca2*ee**2*sb)/(4.*sw**2)',
                  order = {'QED':2})

GC_148 = Coupling(name = 'GC_148',
                  value = '-(ca2*cb*ee**2*sa1)/(4.*sw**2) + (ca1*ca2*ee**2*sb)/(4.*sw**2)',
                  order = {'QED':2})

GC_149 = Coupling(name = 'GC_149',
                  value = '-(ca2*cb*ee**2*sa1)/(2.*sw**2) + (ca1*ca2*ee**2*sb)/(2.*sw**2)',
                  order = {'QED':2})

GC_150 = Coupling(name = 'GC_150',
                  value = '(ca2*cb*ee**2*complex(0,1)*sa1)/(4.*cw) - (ca1*ca2*ee**2*complex(0,1)*sb)/(4.*cw) + (ca2*cb*cw*ee**2*complex(0,1)*sa1)/(4.*sw**2) - (ca1*ca2*cw*ee**2*complex(0,1)*sb)/(4.*sw**2)',
                  order = {'QED':2})

GC_151 = Coupling(name = 'GC_151',
                  value = '(ca2*cb*ee**2*complex(0,1)*sa1)/(4.*cw) - (ca1*ca2*ee**2*complex(0,1)*sb)/(4.*cw) - (ca2*cb*cw*ee**2*complex(0,1)*sa1)/(4.*sw**2) + (ca1*ca2*cw*ee**2*complex(0,1)*sb)/(4.*sw**2)',
                  order = {'QED':2})

GC_152 = Coupling(name = 'GC_152',
                  value = '-(ca2*cb*ee**2*complex(0,1)*sa1)/(2.*cw) + (ca1*ca2*ee**2*complex(0,1)*sb)/(2.*cw) + (ca2*cb*cw*ee**2*complex(0,1)*sa1)/(2.*sw**2) - (ca1*ca2*cw*ee**2*complex(0,1)*sb)/(2.*sw**2)',
                  order = {'QED':2})

GC_153 = Coupling(name = 'GC_153',
                  value = '-(ca2*cb*ee**2*complex(0,1)*sa1)/(2.*cw) + (ca1*ca2*ee**2*complex(0,1)*sb)/(2.*cw) - (ca2*cb*cw*ee**2*complex(0,1)*sa1)/(2.*sw**2) + (ca1*ca2*cw*ee**2*complex(0,1)*sb)/(2.*sw**2)',
                  order = {'QED':2})

GC_154 = Coupling(name = 'GC_154',
                  value = '-(ca1*ca2*cb*ee**2)/(2.*sw**2) - (ca2*ee**2*sa1*sb)/(2.*sw**2)',
                  order = {'QED':2})

GC_155 = Coupling(name = 'GC_155',
                  value = '-(ca1*ca2*cb*ee**2)/(4.*sw**2) - (ca2*ee**2*sa1*sb)/(4.*sw**2)',
                  order = {'QED':2})

GC_156 = Coupling(name = 'GC_156',
                  value = '(ca1*ca2*cb*ee**2)/(4.*sw**2) + (ca2*ee**2*sa1*sb)/(4.*sw**2)',
                  order = {'QED':2})

GC_157 = Coupling(name = 'GC_157',
                  value = '(ca1*ca2*cb*ee**2)/(2.*sw**2) + (ca2*ee**2*sa1*sb)/(2.*sw**2)',
                  order = {'QED':2})

GC_158 = Coupling(name = 'GC_158',
                  value = '(ca1*ca2*cb*ee**2*complex(0,1))/(4.*cw) + (ca2*ee**2*complex(0,1)*sa1*sb)/(4.*cw) - (ca1*ca2*cb*cw*ee**2*complex(0,1))/(4.*sw**2) - (ca2*cw*ee**2*complex(0,1)*sa1*sb)/(4.*sw**2)',
                  order = {'QED':2})

GC_159 = Coupling(name = 'GC_159',
                  value = '(ca1*ca2*cb*ee**2*complex(0,1))/(4.*cw) + (ca2*ee**2*complex(0,1)*sa1*sb)/(4.*cw) + (ca1*ca2*cb*cw*ee**2*complex(0,1))/(4.*sw**2) + (ca2*cw*ee**2*complex(0,1)*sa1*sb)/(4.*sw**2)',
                  order = {'QED':2})

GC_160 = Coupling(name = 'GC_160',
                  value = '-(ca1*ca2*cb*ee**2*complex(0,1))/(2.*cw) - (ca2*ee**2*complex(0,1)*sa1*sb)/(2.*cw) - (ca1*ca2*cb*cw*ee**2*complex(0,1))/(2.*sw**2) - (ca2*cw*ee**2*complex(0,1)*sa1*sb)/(2.*sw**2)',
                  order = {'QED':2})

GC_161 = Coupling(name = 'GC_161',
                  value = '-(ca1*ca2*cb*ee**2*complex(0,1))/(2.*cw) - (ca2*ee**2*complex(0,1)*sa1*sb)/(2.*cw) + (ca1*ca2*cb*cw*ee**2*complex(0,1))/(2.*sw**2) + (ca2*cw*ee**2*complex(0,1)*sa1*sb)/(2.*sw**2)',
                  order = {'QED':2})

GC_162 = Coupling(name = 'GC_162',
                  value = '-(ca1*ca3*cb*ee**2*sa2)/(2.*sw**2) + (cb*ee**2*sa1*sa3)/(2.*sw**2) - (ca3*ee**2*sa1*sa2*sb)/(2.*sw**2) - (ca1*ee**2*sa3*sb)/(2.*sw**2)',
                  order = {'QED':2})

GC_163 = Coupling(name = 'GC_163',
                  value = '-(ca1*ca3*cb*ee**2*sa2)/(4.*sw**2) + (cb*ee**2*sa1*sa3)/(4.*sw**2) - (ca3*ee**2*sa1*sa2*sb)/(4.*sw**2) - (ca1*ee**2*sa3*sb)/(4.*sw**2)',
                  order = {'QED':2})

GC_164 = Coupling(name = 'GC_164',
                  value = '(ca1*ca3*cb*ee**2*sa2)/(4.*sw**2) - (cb*ee**2*sa1*sa3)/(4.*sw**2) + (ca3*ee**2*sa1*sa2*sb)/(4.*sw**2) + (ca1*ee**2*sa3*sb)/(4.*sw**2)',
                  order = {'QED':2})

GC_165 = Coupling(name = 'GC_165',
                  value = '(ca1*ca3*cb*ee**2*sa2)/(2.*sw**2) - (cb*ee**2*sa1*sa3)/(2.*sw**2) + (ca3*ee**2*sa1*sa2*sb)/(2.*sw**2) + (ca1*ee**2*sa3*sb)/(2.*sw**2)',
                  order = {'QED':2})

GC_166 = Coupling(name = 'GC_166',
                  value = '-(ca1*ca3*cb*ee**2*complex(0,1)*sa2)/(4.*cw) + (cb*ee**2*complex(0,1)*sa1*sa3)/(4.*cw) - (ca3*ee**2*complex(0,1)*sa1*sa2*sb)/(4.*cw) - (ca1*ee**2*complex(0,1)*sa3*sb)/(4.*cw) - (ca1*ca3*cb*cw*ee**2*complex(0,1)*sa2)/(4.*sw**2) + (cb*cw*ee**2*complex(0,1)*sa1*sa3)/(4.*sw**2) - (ca3*cw*ee**2*complex(0,1)*sa1*sa2*sb)/(4.*sw**2) - (ca1*cw*ee**2*complex(0,1)*sa3*sb)/(4.*sw**2)',
                  order = {'QED':2})

GC_167 = Coupling(name = 'GC_167',
                  value = '-(ca1*ca3*cb*ee**2*complex(0,1)*sa2)/(4.*cw) + (cb*ee**2*complex(0,1)*sa1*sa3)/(4.*cw) - (ca3*ee**2*complex(0,1)*sa1*sa2*sb)/(4.*cw) - (ca1*ee**2*complex(0,1)*sa3*sb)/(4.*cw) + (ca1*ca3*cb*cw*ee**2*complex(0,1)*sa2)/(4.*sw**2) - (cb*cw*ee**2*complex(0,1)*sa1*sa3)/(4.*sw**2) + (ca3*cw*ee**2*complex(0,1)*sa1*sa2*sb)/(4.*sw**2) + (ca1*cw*ee**2*complex(0,1)*sa3*sb)/(4.*sw**2)',
                  order = {'QED':2})

GC_168 = Coupling(name = 'GC_168',
                  value = '(ca1*ca3*cb*ee**2*complex(0,1)*sa2)/(2.*cw) - (cb*ee**2*complex(0,1)*sa1*sa3)/(2.*cw) + (ca3*ee**2*complex(0,1)*sa1*sa2*sb)/(2.*cw) + (ca1*ee**2*complex(0,1)*sa3*sb)/(2.*cw) - (ca1*ca3*cb*cw*ee**2*complex(0,1)*sa2)/(2.*sw**2) + (cb*cw*ee**2*complex(0,1)*sa1*sa3)/(2.*sw**2) - (ca3*cw*ee**2*complex(0,1)*sa1*sa2*sb)/(2.*sw**2) - (ca1*cw*ee**2*complex(0,1)*sa3*sb)/(2.*sw**2)',
                  order = {'QED':2})

GC_169 = Coupling(name = 'GC_169',
                  value = '(ca1*ca3*cb*ee**2*complex(0,1)*sa2)/(2.*cw) - (cb*ee**2*complex(0,1)*sa1*sa3)/(2.*cw) + (ca3*ee**2*complex(0,1)*sa1*sa2*sb)/(2.*cw) + (ca1*ee**2*complex(0,1)*sa3*sb)/(2.*cw) + (ca1*ca3*cb*cw*ee**2*complex(0,1)*sa2)/(2.*sw**2) - (cb*cw*ee**2*complex(0,1)*sa1*sa3)/(2.*sw**2) + (ca3*cw*ee**2*complex(0,1)*sa1*sa2*sb)/(2.*sw**2) + (ca1*cw*ee**2*complex(0,1)*sa3*sb)/(2.*sw**2)',
                  order = {'QED':2})

GC_170 = Coupling(name = 'GC_170',
                  value = '-(ca3*cb*ee**2*sa1*sa2)/(2.*sw**2) - (ca1*cb*ee**2*sa3)/(2.*sw**2) + (ca1*ca3*ee**2*sa2*sb)/(2.*sw**2) - (ee**2*sa1*sa3*sb)/(2.*sw**2)',
                  order = {'QED':2})

GC_171 = Coupling(name = 'GC_171',
                  value = '-(ca3*cb*ee**2*sa1*sa2)/(4.*sw**2) - (ca1*cb*ee**2*sa3)/(4.*sw**2) + (ca1*ca3*ee**2*sa2*sb)/(4.*sw**2) - (ee**2*sa1*sa3*sb)/(4.*sw**2)',
                  order = {'QED':2})

GC_172 = Coupling(name = 'GC_172',
                  value = '(ca3*cb*ee**2*sa1*sa2)/(4.*sw**2) + (ca1*cb*ee**2*sa3)/(4.*sw**2) - (ca1*ca3*ee**2*sa2*sb)/(4.*sw**2) + (ee**2*sa1*sa3*sb)/(4.*sw**2)',
                  order = {'QED':2})

GC_173 = Coupling(name = 'GC_173',
                  value = '(ca3*cb*ee**2*sa1*sa2)/(2.*sw**2) + (ca1*cb*ee**2*sa3)/(2.*sw**2) - (ca1*ca3*ee**2*sa2*sb)/(2.*sw**2) + (ee**2*sa1*sa3*sb)/(2.*sw**2)',
                  order = {'QED':2})

GC_174 = Coupling(name = 'GC_174',
                  value = '-(ca3*cb*ee**2*complex(0,1)*sa1*sa2)/(4.*cw) - (ca1*cb*ee**2*complex(0,1)*sa3)/(4.*cw) + (ca1*ca3*ee**2*complex(0,1)*sa2*sb)/(4.*cw) - (ee**2*complex(0,1)*sa1*sa3*sb)/(4.*cw) - (ca3*cb*cw*ee**2*complex(0,1)*sa1*sa2)/(4.*sw**2) - (ca1*cb*cw*ee**2*complex(0,1)*sa3)/(4.*sw**2) + (ca1*ca3*cw*ee**2*complex(0,1)*sa2*sb)/(4.*sw**2) - (cw*ee**2*complex(0,1)*sa1*sa3*sb)/(4.*sw**2)',
                  order = {'QED':2})

GC_175 = Coupling(name = 'GC_175',
                  value = '-(ca3*cb*ee**2*complex(0,1)*sa1*sa2)/(4.*cw) - (ca1*cb*ee**2*complex(0,1)*sa3)/(4.*cw) + (ca1*ca3*ee**2*complex(0,1)*sa2*sb)/(4.*cw) - (ee**2*complex(0,1)*sa1*sa3*sb)/(4.*cw) + (ca3*cb*cw*ee**2*complex(0,1)*sa1*sa2)/(4.*sw**2) + (ca1*cb*cw*ee**2*complex(0,1)*sa3)/(4.*sw**2) - (ca1*ca3*cw*ee**2*complex(0,1)*sa2*sb)/(4.*sw**2) + (cw*ee**2*complex(0,1)*sa1*sa3*sb)/(4.*sw**2)',
                  order = {'QED':2})

GC_176 = Coupling(name = 'GC_176',
                  value = '(ca3*cb*ee**2*complex(0,1)*sa1*sa2)/(2.*cw) + (ca1*cb*ee**2*complex(0,1)*sa3)/(2.*cw) - (ca1*ca3*ee**2*complex(0,1)*sa2*sb)/(2.*cw) + (ee**2*complex(0,1)*sa1*sa3*sb)/(2.*cw) - (ca3*cb*cw*ee**2*complex(0,1)*sa1*sa2)/(2.*sw**2) - (ca1*cb*cw*ee**2*complex(0,1)*sa3)/(2.*sw**2) + (ca1*ca3*cw*ee**2*complex(0,1)*sa2*sb)/(2.*sw**2) - (cw*ee**2*complex(0,1)*sa1*sa3*sb)/(2.*sw**2)',
                  order = {'QED':2})

GC_177 = Coupling(name = 'GC_177',
                  value = '(ca3*cb*ee**2*complex(0,1)*sa1*sa2)/(2.*cw) + (ca1*cb*ee**2*complex(0,1)*sa3)/(2.*cw) - (ca1*ca3*ee**2*complex(0,1)*sa2*sb)/(2.*cw) + (ee**2*complex(0,1)*sa1*sa3*sb)/(2.*cw) + (ca3*cb*cw*ee**2*complex(0,1)*sa1*sa2)/(2.*sw**2) + (ca1*cb*cw*ee**2*complex(0,1)*sa3)/(2.*sw**2) - (ca1*ca3*cw*ee**2*complex(0,1)*sa2*sb)/(2.*sw**2) + (cw*ee**2*complex(0,1)*sa1*sa3*sb)/(2.*sw**2)',
                  order = {'QED':2})

GC_178 = Coupling(name = 'GC_178',
                  value = '-(ca1*ca3*cb*ee**2)/(2.*sw**2) + (cb*ee**2*sa1*sa2*sa3)/(2.*sw**2) - (ca3*ee**2*sa1*sb)/(2.*sw**2) - (ca1*ee**2*sa2*sa3*sb)/(2.*sw**2)',
                  order = {'QED':2})

GC_179 = Coupling(name = 'GC_179',
                  value = '-(ca1*ca3*cb*ee**2)/(4.*sw**2) + (cb*ee**2*sa1*sa2*sa3)/(4.*sw**2) - (ca3*ee**2*sa1*sb)/(4.*sw**2) - (ca1*ee**2*sa2*sa3*sb)/(4.*sw**2)',
                  order = {'QED':2})

GC_180 = Coupling(name = 'GC_180',
                  value = '(ca1*ca3*cb*ee**2)/(4.*sw**2) - (cb*ee**2*sa1*sa2*sa3)/(4.*sw**2) + (ca3*ee**2*sa1*sb)/(4.*sw**2) + (ca1*ee**2*sa2*sa3*sb)/(4.*sw**2)',
                  order = {'QED':2})

GC_181 = Coupling(name = 'GC_181',
                  value = '(ca1*ca3*cb*ee**2)/(2.*sw**2) - (cb*ee**2*sa1*sa2*sa3)/(2.*sw**2) + (ca3*ee**2*sa1*sb)/(2.*sw**2) + (ca1*ee**2*sa2*sa3*sb)/(2.*sw**2)',
                  order = {'QED':2})

GC_182 = Coupling(name = 'GC_182',
                  value = '(ca1*ca3*cb*ee**2*complex(0,1))/(4.*cw) - (cb*ee**2*complex(0,1)*sa1*sa2*sa3)/(4.*cw) + (ca3*ee**2*complex(0,1)*sa1*sb)/(4.*cw) + (ca1*ee**2*complex(0,1)*sa2*sa3*sb)/(4.*cw) - (ca1*ca3*cb*cw*ee**2*complex(0,1))/(4.*sw**2) + (cb*cw*ee**2*complex(0,1)*sa1*sa2*sa3)/(4.*sw**2) - (ca3*cw*ee**2*complex(0,1)*sa1*sb)/(4.*sw**2) - (ca1*cw*ee**2*complex(0,1)*sa2*sa3*sb)/(4.*sw**2)',
                  order = {'QED':2})

GC_183 = Coupling(name = 'GC_183',
                  value = '(ca1*ca3*cb*ee**2*complex(0,1))/(4.*cw) - (cb*ee**2*complex(0,1)*sa1*sa2*sa3)/(4.*cw) + (ca3*ee**2*complex(0,1)*sa1*sb)/(4.*cw) + (ca1*ee**2*complex(0,1)*sa2*sa3*sb)/(4.*cw) + (ca1*ca3*cb*cw*ee**2*complex(0,1))/(4.*sw**2) - (cb*cw*ee**2*complex(0,1)*sa1*sa2*sa3)/(4.*sw**2) + (ca3*cw*ee**2*complex(0,1)*sa1*sb)/(4.*sw**2) + (ca1*cw*ee**2*complex(0,1)*sa2*sa3*sb)/(4.*sw**2)',
                  order = {'QED':2})

GC_184 = Coupling(name = 'GC_184',
                  value = '-(ca1*ca3*cb*ee**2*complex(0,1))/(2.*cw) + (cb*ee**2*complex(0,1)*sa1*sa2*sa3)/(2.*cw) - (ca3*ee**2*complex(0,1)*sa1*sb)/(2.*cw) - (ca1*ee**2*complex(0,1)*sa2*sa3*sb)/(2.*cw) - (ca1*ca3*cb*cw*ee**2*complex(0,1))/(2.*sw**2) + (cb*cw*ee**2*complex(0,1)*sa1*sa2*sa3)/(2.*sw**2) - (ca3*cw*ee**2*complex(0,1)*sa1*sb)/(2.*sw**2) - (ca1*cw*ee**2*complex(0,1)*sa2*sa3*sb)/(2.*sw**2)',
                  order = {'QED':2})

GC_185 = Coupling(name = 'GC_185',
                  value = '-(ca1*ca3*cb*ee**2*complex(0,1))/(2.*cw) + (cb*ee**2*complex(0,1)*sa1*sa2*sa3)/(2.*cw) - (ca3*ee**2*complex(0,1)*sa1*sb)/(2.*cw) - (ca1*ee**2*complex(0,1)*sa2*sa3*sb)/(2.*cw) + (ca1*ca3*cb*cw*ee**2*complex(0,1))/(2.*sw**2) - (cb*cw*ee**2*complex(0,1)*sa1*sa2*sa3)/(2.*sw**2) + (ca3*cw*ee**2*complex(0,1)*sa1*sb)/(2.*sw**2) + (ca1*cw*ee**2*complex(0,1)*sa2*sa3*sb)/(2.*sw**2)',
                  order = {'QED':2})

GC_186 = Coupling(name = 'GC_186',
                  value = '-(ca3*cb*ee**2*sa1)/(2.*sw**2) - (ca1*cb*ee**2*sa2*sa3)/(2.*sw**2) + (ca1*ca3*ee**2*sb)/(2.*sw**2) - (ee**2*sa1*sa2*sa3*sb)/(2.*sw**2)',
                  order = {'QED':2})

GC_187 = Coupling(name = 'GC_187',
                  value = '-(ca3*cb*ee**2*sa1)/(4.*sw**2) - (ca1*cb*ee**2*sa2*sa3)/(4.*sw**2) + (ca1*ca3*ee**2*sb)/(4.*sw**2) - (ee**2*sa1*sa2*sa3*sb)/(4.*sw**2)',
                  order = {'QED':2})

GC_188 = Coupling(name = 'GC_188',
                  value = '(ca3*cb*ee**2*sa1)/(4.*sw**2) + (ca1*cb*ee**2*sa2*sa3)/(4.*sw**2) - (ca1*ca3*ee**2*sb)/(4.*sw**2) + (ee**2*sa1*sa2*sa3*sb)/(4.*sw**2)',
                  order = {'QED':2})

GC_189 = Coupling(name = 'GC_189',
                  value = '(ca3*cb*ee**2*sa1)/(2.*sw**2) + (ca1*cb*ee**2*sa2*sa3)/(2.*sw**2) - (ca1*ca3*ee**2*sb)/(2.*sw**2) + (ee**2*sa1*sa2*sa3*sb)/(2.*sw**2)',
                  order = {'QED':2})

GC_190 = Coupling(name = 'GC_190',
                  value = '-(ca3*cb*ee**2*complex(0,1)*sa1)/(4.*cw) - (ca1*cb*ee**2*complex(0,1)*sa2*sa3)/(4.*cw) + (ca1*ca3*ee**2*complex(0,1)*sb)/(4.*cw) - (ee**2*complex(0,1)*sa1*sa2*sa3*sb)/(4.*cw) - (ca3*cb*cw*ee**2*complex(0,1)*sa1)/(4.*sw**2) - (ca1*cb*cw*ee**2*complex(0,1)*sa2*sa3)/(4.*sw**2) + (ca1*ca3*cw*ee**2*complex(0,1)*sb)/(4.*sw**2) - (cw*ee**2*complex(0,1)*sa1*sa2*sa3*sb)/(4.*sw**2)',
                  order = {'QED':2})

GC_191 = Coupling(name = 'GC_191',
                  value = '-(ca3*cb*ee**2*complex(0,1)*sa1)/(4.*cw) - (ca1*cb*ee**2*complex(0,1)*sa2*sa3)/(4.*cw) + (ca1*ca3*ee**2*complex(0,1)*sb)/(4.*cw) - (ee**2*complex(0,1)*sa1*sa2*sa3*sb)/(4.*cw) + (ca3*cb*cw*ee**2*complex(0,1)*sa1)/(4.*sw**2) + (ca1*cb*cw*ee**2*complex(0,1)*sa2*sa3)/(4.*sw**2) - (ca1*ca3*cw*ee**2*complex(0,1)*sb)/(4.*sw**2) + (cw*ee**2*complex(0,1)*sa1*sa2*sa3*sb)/(4.*sw**2)',
                  order = {'QED':2})

GC_192 = Coupling(name = 'GC_192',
                  value = '(ca3*cb*ee**2*complex(0,1)*sa1)/(2.*cw) + (ca1*cb*ee**2*complex(0,1)*sa2*sa3)/(2.*cw) - (ca1*ca3*ee**2*complex(0,1)*sb)/(2.*cw) + (ee**2*complex(0,1)*sa1*sa2*sa3*sb)/(2.*cw) - (ca3*cb*cw*ee**2*complex(0,1)*sa1)/(2.*sw**2) - (ca1*cb*cw*ee**2*complex(0,1)*sa2*sa3)/(2.*sw**2) + (ca1*ca3*cw*ee**2*complex(0,1)*sb)/(2.*sw**2) - (cw*ee**2*complex(0,1)*sa1*sa2*sa3*sb)/(2.*sw**2)',
                  order = {'QED':2})

GC_193 = Coupling(name = 'GC_193',
                  value = '(ca3*cb*ee**2*complex(0,1)*sa1)/(2.*cw) + (ca1*cb*ee**2*complex(0,1)*sa2*sa3)/(2.*cw) - (ca1*ca3*ee**2*complex(0,1)*sb)/(2.*cw) + (ee**2*complex(0,1)*sa1*sa2*sa3*sb)/(2.*cw) + (ca3*cb*cw*ee**2*complex(0,1)*sa1)/(2.*sw**2) + (ca1*cb*cw*ee**2*complex(0,1)*sa2*sa3)/(2.*sw**2) - (ca1*ca3*cw*ee**2*complex(0,1)*sb)/(2.*sw**2) + (cw*ee**2*complex(0,1)*sa1*sa2*sa3*sb)/(2.*sw**2)',
                  order = {'QED':2})

GC_194 = Coupling(name = 'GC_194',
                  value = '-(cb**2*ee**2*complex(0,1))/(4.*sw**2) - (ee**2*complex(0,1)*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_195 = Coupling(name = 'GC_195',
                  value = '-(cb**2*ee**2*complex(0,1))/(2.*sw**2) - (ee**2*complex(0,1)*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_196 = Coupling(name = 'GC_196',
                  value = '(cb**2*ee**2*complex(0,1))/(2.*sw**2) + (ee**2*complex(0,1)*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_197 = Coupling(name = 'GC_197',
                  value = '(cb**2*ee**2*complex(0,1))/sw**2 + (ee**2*complex(0,1)*sb**2)/sw**2',
                  order = {'QED':2})

GC_198 = Coupling(name = 'GC_198',
                  value = '-(ca1**2*ca2**2*cb**2*complex(0,1)*l3) - ca2**2*cb**2*complex(0,1)*l2*sa1**2 - cb**2*complex(0,1)*l8*sa2**2 + 2*ca1*ca2**2*cb*complex(0,1)*l4*sa1*sb + 2*ca1*ca2**2*cb*complex(0,1)*l5*sa1*sb - ca1**2*ca2**2*complex(0,1)*l1*sb**2 - ca2**2*complex(0,1)*l3*sa1**2*sb**2 - complex(0,1)*l7*sa2**2*sb**2 + (ca2**2*cb**2*ee**2*complex(0,1)*sa1**2)/(4.*sw**2) - (ca1*ca2**2*cb*ee**2*complex(0,1)*sa1*sb)/(2.*sw**2) + (ca1**2*ca2**2*ee**2*complex(0,1)*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_199 = Coupling(name = 'GC_199',
                  value = '-(ca1**2*ca2**2*cb**2*complex(0,1)*l3) - ca1**2*ca2**2*cb**2*complex(0,1)*l4 + ca1**2*ca2**2*cb**2*complex(0,1)*l5 + (ca2**2*cb**2*ee**2*complex(0,1)*sa1**2)/(4.*cw**2) - ca2**2*cb**2*complex(0,1)*l2*sa1**2 - cb**2*complex(0,1)*l8*sa2**2 - (ca1*ca2**2*cb*ee**2*complex(0,1)*sa1*sb)/(2.*cw**2) + 4*ca1*ca2**2*cb*complex(0,1)*l5*sa1*sb + (ca1**2*ca2**2*ee**2*complex(0,1)*sb**2)/(4.*cw**2) - ca1**2*ca2**2*complex(0,1)*l1*sb**2 - ca2**2*complex(0,1)*l3*sa1**2*sb**2 - ca2**2*complex(0,1)*l4*sa1**2*sb**2 + ca2**2*complex(0,1)*l5*sa1**2*sb**2 - complex(0,1)*l7*sa2**2*sb**2 + (ca2**2*cb**2*ee**2*complex(0,1)*sa1**2)/(4.*sw**2) - (ca1*ca2**2*cb*ee**2*complex(0,1)*sa1*sb)/(2.*sw**2) + (ca1**2*ca2**2*ee**2*complex(0,1)*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_200 = Coupling(name = 'GC_200',
                  value = '-(ca1**2*ca2**2*cb**2*complex(0,1)*l3) - ca2**2*cb**2*complex(0,1)*l2*sa1**2 - cb**2*complex(0,1)*l8*sa2**2 + 2*ca1*ca2**2*cb*complex(0,1)*l4*sa1*sb + 2*ca1*ca2**2*cb*complex(0,1)*l5*sa1*sb - ca1**2*ca2**2*complex(0,1)*l1*sb**2 - ca2**2*complex(0,1)*l3*sa1**2*sb**2 - complex(0,1)*l7*sa2**2*sb**2 - (ca2**2*cb**2*ee**2*complex(0,1)*sa1**2)/(2.*sw**2) + (ca1*ca2**2*cb*ee**2*complex(0,1)*sa1*sb)/sw**2 - (ca1**2*ca2**2*ee**2*complex(0,1)*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_201 = Coupling(name = 'GC_201',
                  value = '-(ca1**2*ca2**2*cb**2*complex(0,1)*l3) - ca1**2*ca2**2*cb**2*complex(0,1)*l4 + ca1**2*ca2**2*cb**2*complex(0,1)*l5 - (ca2**2*cb**2*ee**2*complex(0,1)*sa1**2)/(2.*cw**2) - ca2**2*cb**2*complex(0,1)*l2*sa1**2 - cb**2*complex(0,1)*l8*sa2**2 + (ca1*ca2**2*cb*ee**2*complex(0,1)*sa1*sb)/cw**2 + 4*ca1*ca2**2*cb*complex(0,1)*l5*sa1*sb - (ca1**2*ca2**2*ee**2*complex(0,1)*sb**2)/(2.*cw**2) - ca1**2*ca2**2*complex(0,1)*l1*sb**2 - ca2**2*complex(0,1)*l3*sa1**2*sb**2 - ca2**2*complex(0,1)*l4*sa1**2*sb**2 + ca2**2*complex(0,1)*l5*sa1**2*sb**2 - complex(0,1)*l7*sa2**2*sb**2 - (ca2**2*cb**2*ee**2*complex(0,1)*sa1**2)/(2.*sw**2) + (ca1*ca2**2*cb*ee**2*complex(0,1)*sa1*sb)/sw**2 - (ca1**2*ca2**2*ee**2*complex(0,1)*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_202 = Coupling(name = 'GC_202',
                  value = '-(cb**2*ee**2)/(2.*cw) - (ee**2*sb**2)/(2.*cw) - (cb**2*cw*ee**2)/(2.*sw**2) - (cw*ee**2*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_203 = Coupling(name = 'GC_203',
                  value = '(cb**2*ee**2)/(2.*cw) + (ee**2*sb**2)/(2.*cw) - (cb**2*cw*ee**2)/(2.*sw**2) - (cw*ee**2*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_204 = Coupling(name = 'GC_204',
                  value = '-(cb**2*ee**2)/(4.*cw) - (ee**2*sb**2)/(4.*cw) - (cb**2*cw*ee**2)/(4.*sw**2) - (cw*ee**2*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_205 = Coupling(name = 'GC_205',
                  value = '(cb**2*ee**2)/(4.*cw) + (ee**2*sb**2)/(4.*cw) - (cb**2*cw*ee**2)/(4.*sw**2) - (cw*ee**2*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_206 = Coupling(name = 'GC_206',
                  value = '-(cb**2*ee**2)/(4.*cw) - (ee**2*sb**2)/(4.*cw) + (cb**2*cw*ee**2)/(4.*sw**2) + (cw*ee**2*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_207 = Coupling(name = 'GC_207',
                  value = '(cb**2*ee**2)/(4.*cw) + (ee**2*sb**2)/(4.*cw) + (cb**2*cw*ee**2)/(4.*sw**2) + (cw*ee**2*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_208 = Coupling(name = 'GC_208',
                  value = '-(cb**2*ee**2)/(2.*cw) - (ee**2*sb**2)/(2.*cw) + (cb**2*cw*ee**2)/(2.*sw**2) + (cw*ee**2*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_209 = Coupling(name = 'GC_209',
                  value = '(cb**2*ee**2)/(2.*cw) + (ee**2*sb**2)/(2.*cw) + (cb**2*cw*ee**2)/(2.*sw**2) + (cw*ee**2*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_210 = Coupling(name = 'GC_210',
                  value = '-(ca1*ca2**2*cb**2*complex(0,1)*l4*sa1) - ca1*ca2**2*cb**2*complex(0,1)*l5*sa1 + ca1**2*ca2**2*cb*complex(0,1)*l1*sb - ca1**2*ca2**2*cb*complex(0,1)*l3*sb - ca2**2*cb*complex(0,1)*l2*sa1**2*sb + ca2**2*cb*complex(0,1)*l3*sa1**2*sb + cb*complex(0,1)*l7*sa2**2*sb - cb*complex(0,1)*l8*sa2**2*sb + ca1*ca2**2*complex(0,1)*l4*sa1*sb**2 + ca1*ca2**2*complex(0,1)*l5*sa1*sb**2 + (ca1*ca2**2*cb**2*ee**2*complex(0,1)*sa1)/(4.*sw**2) - (ca1**2*ca2**2*cb*ee**2*complex(0,1)*sb)/(4.*sw**2) + (ca2**2*cb*ee**2*complex(0,1)*sa1**2*sb)/(4.*sw**2) - (ca1*ca2**2*ee**2*complex(0,1)*sa1*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_211 = Coupling(name = 'GC_211',
                  value = '(ca1*ca2**2*cb**2*ee**2*complex(0,1)*sa1)/(4.*cw**2) - 2*ca1*ca2**2*cb**2*complex(0,1)*l5*sa1 - (ca1**2*ca2**2*cb*ee**2*complex(0,1)*sb)/(4.*cw**2) + ca1**2*ca2**2*cb*complex(0,1)*l1*sb - ca1**2*ca2**2*cb*complex(0,1)*l3*sb - ca1**2*ca2**2*cb*complex(0,1)*l4*sb + ca1**2*ca2**2*cb*complex(0,1)*l5*sb + (ca2**2*cb*ee**2*complex(0,1)*sa1**2*sb)/(4.*cw**2) - ca2**2*cb*complex(0,1)*l2*sa1**2*sb + ca2**2*cb*complex(0,1)*l3*sa1**2*sb + ca2**2*cb*complex(0,1)*l4*sa1**2*sb - ca2**2*cb*complex(0,1)*l5*sa1**2*sb + cb*complex(0,1)*l7*sa2**2*sb - cb*complex(0,1)*l8*sa2**2*sb - (ca1*ca2**2*ee**2*complex(0,1)*sa1*sb**2)/(4.*cw**2) + 2*ca1*ca2**2*complex(0,1)*l5*sa1*sb**2 + (ca1*ca2**2*cb**2*ee**2*complex(0,1)*sa1)/(4.*sw**2) - (ca1**2*ca2**2*cb*ee**2*complex(0,1)*sb)/(4.*sw**2) + (ca2**2*cb*ee**2*complex(0,1)*sa1**2*sb)/(4.*sw**2) - (ca1*ca2**2*ee**2*complex(0,1)*sa1*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_212 = Coupling(name = 'GC_212',
                  value = '-(ca1*ca2**2*cb**2*complex(0,1)*l4*sa1) - ca1*ca2**2*cb**2*complex(0,1)*l5*sa1 + ca1**2*ca2**2*cb*complex(0,1)*l1*sb - ca1**2*ca2**2*cb*complex(0,1)*l3*sb - ca2**2*cb*complex(0,1)*l2*sa1**2*sb + ca2**2*cb*complex(0,1)*l3*sa1**2*sb + cb*complex(0,1)*l7*sa2**2*sb - cb*complex(0,1)*l8*sa2**2*sb + ca1*ca2**2*complex(0,1)*l4*sa1*sb**2 + ca1*ca2**2*complex(0,1)*l5*sa1*sb**2 - (ca1*ca2**2*cb**2*ee**2*complex(0,1)*sa1)/(2.*sw**2) + (ca1**2*ca2**2*cb*ee**2*complex(0,1)*sb)/(2.*sw**2) - (ca2**2*cb*ee**2*complex(0,1)*sa1**2*sb)/(2.*sw**2) + (ca1*ca2**2*ee**2*complex(0,1)*sa1*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_213 = Coupling(name = 'GC_213',
                  value = '-(ca1*ca2**2*cb**2*ee**2*complex(0,1)*sa1)/(2.*cw**2) - 2*ca1*ca2**2*cb**2*complex(0,1)*l5*sa1 + (ca1**2*ca2**2*cb*ee**2*complex(0,1)*sb)/(2.*cw**2) + ca1**2*ca2**2*cb*complex(0,1)*l1*sb - ca1**2*ca2**2*cb*complex(0,1)*l3*sb - ca1**2*ca2**2*cb*complex(0,1)*l4*sb + ca1**2*ca2**2*cb*complex(0,1)*l5*sb - (ca2**2*cb*ee**2*complex(0,1)*sa1**2*sb)/(2.*cw**2) - ca2**2*cb*complex(0,1)*l2*sa1**2*sb + ca2**2*cb*complex(0,1)*l3*sa1**2*sb + ca2**2*cb*complex(0,1)*l4*sa1**2*sb - ca2**2*cb*complex(0,1)*l5*sa1**2*sb + cb*complex(0,1)*l7*sa2**2*sb - cb*complex(0,1)*l8*sa2**2*sb + (ca1*ca2**2*ee**2*complex(0,1)*sa1*sb**2)/(2.*cw**2) + 2*ca1*ca2**2*complex(0,1)*l5*sa1*sb**2 - (ca1*ca2**2*cb**2*ee**2*complex(0,1)*sa1)/(2.*sw**2) + (ca1**2*ca2**2*cb*ee**2*complex(0,1)*sb)/(2.*sw**2) - (ca2**2*cb*ee**2*complex(0,1)*sa1**2*sb)/(2.*sw**2) + (ca1*ca2**2*ee**2*complex(0,1)*sa1*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_214 = Coupling(name = 'GC_214',
                  value = '-(ca1**2*ca2**2*cb**2*complex(0,1)*l1) - ca2**2*cb**2*complex(0,1)*l3*sa1**2 - cb**2*complex(0,1)*l7*sa2**2 - 2*ca1*ca2**2*cb*complex(0,1)*l4*sa1*sb - 2*ca1*ca2**2*cb*complex(0,1)*l5*sa1*sb - ca1**2*ca2**2*complex(0,1)*l3*sb**2 - ca2**2*complex(0,1)*l2*sa1**2*sb**2 - complex(0,1)*l8*sa2**2*sb**2 + (ca1**2*ca2**2*cb**2*ee**2*complex(0,1))/(4.*sw**2) + (ca1*ca2**2*cb*ee**2*complex(0,1)*sa1*sb)/(2.*sw**2) + (ca2**2*ee**2*complex(0,1)*sa1**2*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_215 = Coupling(name = 'GC_215',
                  value = '(ca1**2*ca2**2*cb**2*ee**2*complex(0,1))/(4.*cw**2) - ca1**2*ca2**2*cb**2*complex(0,1)*l1 - ca2**2*cb**2*complex(0,1)*l3*sa1**2 - ca2**2*cb**2*complex(0,1)*l4*sa1**2 + ca2**2*cb**2*complex(0,1)*l5*sa1**2 - cb**2*complex(0,1)*l7*sa2**2 + (ca1*ca2**2*cb*ee**2*complex(0,1)*sa1*sb)/(2.*cw**2) - 4*ca1*ca2**2*cb*complex(0,1)*l5*sa1*sb - ca1**2*ca2**2*complex(0,1)*l3*sb**2 - ca1**2*ca2**2*complex(0,1)*l4*sb**2 + ca1**2*ca2**2*complex(0,1)*l5*sb**2 + (ca2**2*ee**2*complex(0,1)*sa1**2*sb**2)/(4.*cw**2) - ca2**2*complex(0,1)*l2*sa1**2*sb**2 - complex(0,1)*l8*sa2**2*sb**2 + (ca1**2*ca2**2*cb**2*ee**2*complex(0,1))/(4.*sw**2) + (ca1*ca2**2*cb*ee**2*complex(0,1)*sa1*sb)/(2.*sw**2) + (ca2**2*ee**2*complex(0,1)*sa1**2*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_216 = Coupling(name = 'GC_216',
                  value = '-(ca1**2*ca2**2*cb**2*complex(0,1)*l1) - ca2**2*cb**2*complex(0,1)*l3*sa1**2 - cb**2*complex(0,1)*l7*sa2**2 - 2*ca1*ca2**2*cb*complex(0,1)*l4*sa1*sb - 2*ca1*ca2**2*cb*complex(0,1)*l5*sa1*sb - ca1**2*ca2**2*complex(0,1)*l3*sb**2 - ca2**2*complex(0,1)*l2*sa1**2*sb**2 - complex(0,1)*l8*sa2**2*sb**2 - (ca1**2*ca2**2*cb**2*ee**2*complex(0,1))/(2.*sw**2) - (ca1*ca2**2*cb*ee**2*complex(0,1)*sa1*sb)/sw**2 - (ca2**2*ee**2*complex(0,1)*sa1**2*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_217 = Coupling(name = 'GC_217',
                  value = '-(ca1**2*ca2**2*cb**2*ee**2*complex(0,1))/(2.*cw**2) - ca1**2*ca2**2*cb**2*complex(0,1)*l1 - ca2**2*cb**2*complex(0,1)*l3*sa1**2 - ca2**2*cb**2*complex(0,1)*l4*sa1**2 + ca2**2*cb**2*complex(0,1)*l5*sa1**2 - cb**2*complex(0,1)*l7*sa2**2 - (ca1*ca2**2*cb*ee**2*complex(0,1)*sa1*sb)/cw**2 - 4*ca1*ca2**2*cb*complex(0,1)*l5*sa1*sb - ca1**2*ca2**2*complex(0,1)*l3*sb**2 - ca1**2*ca2**2*complex(0,1)*l4*sb**2 + ca1**2*ca2**2*complex(0,1)*l5*sb**2 - (ca2**2*ee**2*complex(0,1)*sa1**2*sb**2)/(2.*cw**2) - ca2**2*complex(0,1)*l2*sa1**2*sb**2 - complex(0,1)*l8*sa2**2*sb**2 - (ca1**2*ca2**2*cb**2*ee**2*complex(0,1))/(2.*sw**2) - (ca1*ca2**2*cb*ee**2*complex(0,1)*sa1*sb)/sw**2 - (ca2**2*ee**2*complex(0,1)*sa1**2*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_218 = Coupling(name = 'GC_218',
                  value = 'ca1*ca2*ca3*cb**2*complex(0,1)*l4*sa1*sa2 + ca1*ca2*ca3*cb**2*complex(0,1)*l5*sa1*sa2 + (ca1**2*ca2*cb**2*complex(0,1)*l4*sa3)/2. + (ca1**2*ca2*cb**2*complex(0,1)*l5*sa3)/2. - (ca2*cb**2*complex(0,1)*l4*sa1**2*sa3)/2. - (ca2*cb**2*complex(0,1)*l5*sa1**2*sa3)/2. - ca1**2*ca2*ca3*cb*complex(0,1)*l1*sa2*sb + ca1**2*ca2*ca3*cb*complex(0,1)*l3*sa2*sb + ca2*ca3*cb*complex(0,1)*l7*sa2*sb - ca2*ca3*cb*complex(0,1)*l8*sa2*sb + ca2*ca3*cb*complex(0,1)*l2*sa1**2*sa2*sb - ca2*ca3*cb*complex(0,1)*l3*sa1**2*sa2*sb + ca1*ca2*cb*complex(0,1)*l1*sa1*sa3*sb + ca1*ca2*cb*complex(0,1)*l2*sa1*sa3*sb - 2*ca1*ca2*cb*complex(0,1)*l3*sa1*sa3*sb - ca1*ca2*ca3*complex(0,1)*l4*sa1*sa2*sb**2 - ca1*ca2*ca3*complex(0,1)*l5*sa1*sa2*sb**2 - (ca1**2*ca2*complex(0,1)*l4*sa3*sb**2)/2. - (ca1**2*ca2*complex(0,1)*l5*sa3*sb**2)/2. + (ca2*complex(0,1)*l4*sa1**2*sa3*sb**2)/2. + (ca2*complex(0,1)*l5*sa1**2*sa3*sb**2)/2. - (ca1*ca2*ca3*cb**2*ee**2*complex(0,1)*sa1*sa2)/(4.*sw**2) + (ca2*cb**2*ee**2*complex(0,1)*sa1**2*sa3)/(4.*sw**2) + (ca1**2*ca2*ca3*cb*ee**2*complex(0,1)*sa2*sb)/(4.*sw**2) - (ca2*ca3*cb*ee**2*complex(0,1)*sa1**2*sa2*sb)/(4.*sw**2) - (ca1*ca2*cb*ee**2*complex(0,1)*sa1*sa3*sb)/(2.*sw**2) + (ca1*ca2*ca3*ee**2*complex(0,1)*sa1*sa2*sb**2)/(4.*sw**2) + (ca1**2*ca2*ee**2*complex(0,1)*sa3*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_219 = Coupling(name = 'GC_219',
                  value = '-(ca1*ca2*ca3*cb**2*ee**2*complex(0,1)*sa1*sa2)/(4.*cw**2) + 2*ca1*ca2*ca3*cb**2*complex(0,1)*l5*sa1*sa2 + ca1**2*ca2*cb**2*complex(0,1)*l5*sa3 + (ca2*cb**2*ee**2*complex(0,1)*sa1**2*sa3)/(4.*cw**2) - ca2*cb**2*complex(0,1)*l5*sa1**2*sa3 + (ca1**2*ca2*ca3*cb*ee**2*complex(0,1)*sa2*sb)/(4.*cw**2) - ca1**2*ca2*ca3*cb*complex(0,1)*l1*sa2*sb + ca1**2*ca2*ca3*cb*complex(0,1)*l3*sa2*sb + ca1**2*ca2*ca3*cb*complex(0,1)*l4*sa2*sb - ca1**2*ca2*ca3*cb*complex(0,1)*l5*sa2*sb + ca2*ca3*cb*complex(0,1)*l7*sa2*sb - ca2*ca3*cb*complex(0,1)*l8*sa2*sb - (ca2*ca3*cb*ee**2*complex(0,1)*sa1**2*sa2*sb)/(4.*cw**2) + ca2*ca3*cb*complex(0,1)*l2*sa1**2*sa2*sb - ca2*ca3*cb*complex(0,1)*l3*sa1**2*sa2*sb - ca2*ca3*cb*complex(0,1)*l4*sa1**2*sa2*sb + ca2*ca3*cb*complex(0,1)*l5*sa1**2*sa2*sb - (ca1*ca2*cb*ee**2*complex(0,1)*sa1*sa3*sb)/(2.*cw**2) + ca1*ca2*cb*complex(0,1)*l1*sa1*sa3*sb + ca1*ca2*cb*complex(0,1)*l2*sa1*sa3*sb - 2*ca1*ca2*cb*complex(0,1)*l3*sa1*sa3*sb - 2*ca1*ca2*cb*complex(0,1)*l4*sa1*sa3*sb + 2*ca1*ca2*cb*complex(0,1)*l5*sa1*sa3*sb + (ca1*ca2*ca3*ee**2*complex(0,1)*sa1*sa2*sb**2)/(4.*cw**2) - 2*ca1*ca2*ca3*complex(0,1)*l5*sa1*sa2*sb**2 + (ca1**2*ca2*ee**2*complex(0,1)*sa3*sb**2)/(4.*cw**2) - ca1**2*ca2*complex(0,1)*l5*sa3*sb**2 + ca2*complex(0,1)*l5*sa1**2*sa3*sb**2 - (ca1*ca2*ca3*cb**2*ee**2*complex(0,1)*sa1*sa2)/(4.*sw**2) + (ca2*cb**2*ee**2*complex(0,1)*sa1**2*sa3)/(4.*sw**2) + (ca1**2*ca2*ca3*cb*ee**2*complex(0,1)*sa2*sb)/(4.*sw**2) - (ca2*ca3*cb*ee**2*complex(0,1)*sa1**2*sa2*sb)/(4.*sw**2) - (ca1*ca2*cb*ee**2*complex(0,1)*sa1*sa3*sb)/(2.*sw**2) + (ca1*ca2*ca3*ee**2*complex(0,1)*sa1*sa2*sb**2)/(4.*sw**2) + (ca1**2*ca2*ee**2*complex(0,1)*sa3*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_220 = Coupling(name = 'GC_220',
                  value = 'ca1**2*ca2*ca3*cb**2*complex(0,1)*l1*sa2 - ca2*ca3*cb**2*complex(0,1)*l7*sa2 + ca2*ca3*cb**2*complex(0,1)*l3*sa1**2*sa2 - ca1*ca2*cb**2*complex(0,1)*l1*sa1*sa3 + ca1*ca2*cb**2*complex(0,1)*l3*sa1*sa3 + 2*ca1*ca2*ca3*cb*complex(0,1)*l4*sa1*sa2*sb + 2*ca1*ca2*ca3*cb*complex(0,1)*l5*sa1*sa2*sb + ca1**2*ca2*cb*complex(0,1)*l4*sa3*sb + ca1**2*ca2*cb*complex(0,1)*l5*sa3*sb - ca2*cb*complex(0,1)*l4*sa1**2*sa3*sb - ca2*cb*complex(0,1)*l5*sa1**2*sa3*sb + ca1**2*ca2*ca3*complex(0,1)*l3*sa2*sb**2 - ca2*ca3*complex(0,1)*l8*sa2*sb**2 + ca2*ca3*complex(0,1)*l2*sa1**2*sa2*sb**2 + ca1*ca2*complex(0,1)*l2*sa1*sa3*sb**2 - ca1*ca2*complex(0,1)*l3*sa1*sa3*sb**2 - (ca1**2*ca2*ca3*cb**2*ee**2*complex(0,1)*sa2)/(4.*sw**2) + (ca1*ca2*cb**2*ee**2*complex(0,1)*sa1*sa3)/(4.*sw**2) - (ca1*ca2*ca3*cb*ee**2*complex(0,1)*sa1*sa2*sb)/(2.*sw**2) - (ca1**2*ca2*cb*ee**2*complex(0,1)*sa3*sb)/(4.*sw**2) + (ca2*cb*ee**2*complex(0,1)*sa1**2*sa3*sb)/(4.*sw**2) - (ca2*ca3*ee**2*complex(0,1)*sa1**2*sa2*sb**2)/(4.*sw**2) - (ca1*ca2*ee**2*complex(0,1)*sa1*sa3*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_221 = Coupling(name = 'GC_221',
                  value = '-(ca1**2*ca2*ca3*cb**2*ee**2*complex(0,1)*sa2)/(4.*cw**2) + ca1**2*ca2*ca3*cb**2*complex(0,1)*l1*sa2 - ca2*ca3*cb**2*complex(0,1)*l7*sa2 + ca2*ca3*cb**2*complex(0,1)*l3*sa1**2*sa2 + ca2*ca3*cb**2*complex(0,1)*l4*sa1**2*sa2 - ca2*ca3*cb**2*complex(0,1)*l5*sa1**2*sa2 + (ca1*ca2*cb**2*ee**2*complex(0,1)*sa1*sa3)/(4.*cw**2) - ca1*ca2*cb**2*complex(0,1)*l1*sa1*sa3 + ca1*ca2*cb**2*complex(0,1)*l3*sa1*sa3 + ca1*ca2*cb**2*complex(0,1)*l4*sa1*sa3 - ca1*ca2*cb**2*complex(0,1)*l5*sa1*sa3 - (ca1*ca2*ca3*cb*ee**2*complex(0,1)*sa1*sa2*sb)/(2.*cw**2) + 4*ca1*ca2*ca3*cb*complex(0,1)*l5*sa1*sa2*sb - (ca1**2*ca2*cb*ee**2*complex(0,1)*sa3*sb)/(4.*cw**2) + 2*ca1**2*ca2*cb*complex(0,1)*l5*sa3*sb + (ca2*cb*ee**2*complex(0,1)*sa1**2*sa3*sb)/(4.*cw**2) - 2*ca2*cb*complex(0,1)*l5*sa1**2*sa3*sb + ca1**2*ca2*ca3*complex(0,1)*l3*sa2*sb**2 + ca1**2*ca2*ca3*complex(0,1)*l4*sa2*sb**2 - ca1**2*ca2*ca3*complex(0,1)*l5*sa2*sb**2 - ca2*ca3*complex(0,1)*l8*sa2*sb**2 - (ca2*ca3*ee**2*complex(0,1)*sa1**2*sa2*sb**2)/(4.*cw**2) + ca2*ca3*complex(0,1)*l2*sa1**2*sa2*sb**2 - (ca1*ca2*ee**2*complex(0,1)*sa1*sa3*sb**2)/(4.*cw**2) + ca1*ca2*complex(0,1)*l2*sa1*sa3*sb**2 - ca1*ca2*complex(0,1)*l3*sa1*sa3*sb**2 - ca1*ca2*complex(0,1)*l4*sa1*sa3*sb**2 + ca1*ca2*complex(0,1)*l5*sa1*sa3*sb**2 - (ca1**2*ca2*ca3*cb**2*ee**2*complex(0,1)*sa2)/(4.*sw**2) + (ca1*ca2*cb**2*ee**2*complex(0,1)*sa1*sa3)/(4.*sw**2) - (ca1*ca2*ca3*cb*ee**2*complex(0,1)*sa1*sa2*sb)/(2.*sw**2) - (ca1**2*ca2*cb*ee**2*complex(0,1)*sa3*sb)/(4.*sw**2) + (ca2*cb*ee**2*complex(0,1)*sa1**2*sa3*sb)/(4.*sw**2) - (ca2*ca3*ee**2*complex(0,1)*sa1**2*sa2*sb**2)/(4.*sw**2) - (ca1*ca2*ee**2*complex(0,1)*sa1*sa3*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_222 = Coupling(name = 'GC_222',
                  value = 'ca1**2*ca2*ca3*cb**2*complex(0,1)*l3*sa2 - ca2*ca3*cb**2*complex(0,1)*l8*sa2 + ca2*ca3*cb**2*complex(0,1)*l2*sa1**2*sa2 + ca1*ca2*cb**2*complex(0,1)*l2*sa1*sa3 - ca1*ca2*cb**2*complex(0,1)*l3*sa1*sa3 - 2*ca1*ca2*ca3*cb*complex(0,1)*l4*sa1*sa2*sb - 2*ca1*ca2*ca3*cb*complex(0,1)*l5*sa1*sa2*sb - ca1**2*ca2*cb*complex(0,1)*l4*sa3*sb - ca1**2*ca2*cb*complex(0,1)*l5*sa3*sb + ca2*cb*complex(0,1)*l4*sa1**2*sa3*sb + ca2*cb*complex(0,1)*l5*sa1**2*sa3*sb + ca1**2*ca2*ca3*complex(0,1)*l1*sa2*sb**2 - ca2*ca3*complex(0,1)*l7*sa2*sb**2 + ca2*ca3*complex(0,1)*l3*sa1**2*sa2*sb**2 - ca1*ca2*complex(0,1)*l1*sa1*sa3*sb**2 + ca1*ca2*complex(0,1)*l3*sa1*sa3*sb**2 - (ca2*ca3*cb**2*ee**2*complex(0,1)*sa1**2*sa2)/(4.*sw**2) - (ca1*ca2*cb**2*ee**2*complex(0,1)*sa1*sa3)/(4.*sw**2) + (ca1*ca2*ca3*cb*ee**2*complex(0,1)*sa1*sa2*sb)/(2.*sw**2) + (ca1**2*ca2*cb*ee**2*complex(0,1)*sa3*sb)/(4.*sw**2) - (ca2*cb*ee**2*complex(0,1)*sa1**2*sa3*sb)/(4.*sw**2) - (ca1**2*ca2*ca3*ee**2*complex(0,1)*sa2*sb**2)/(4.*sw**2) + (ca1*ca2*ee**2*complex(0,1)*sa1*sa3*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_223 = Coupling(name = 'GC_223',
                  value = 'ca1**2*ca2*ca3*cb**2*complex(0,1)*l3*sa2 + ca1**2*ca2*ca3*cb**2*complex(0,1)*l4*sa2 - ca1**2*ca2*ca3*cb**2*complex(0,1)*l5*sa2 - ca2*ca3*cb**2*complex(0,1)*l8*sa2 - (ca2*ca3*cb**2*ee**2*complex(0,1)*sa1**2*sa2)/(4.*cw**2) + ca2*ca3*cb**2*complex(0,1)*l2*sa1**2*sa2 - (ca1*ca2*cb**2*ee**2*complex(0,1)*sa1*sa3)/(4.*cw**2) + ca1*ca2*cb**2*complex(0,1)*l2*sa1*sa3 - ca1*ca2*cb**2*complex(0,1)*l3*sa1*sa3 - ca1*ca2*cb**2*complex(0,1)*l4*sa1*sa3 + ca1*ca2*cb**2*complex(0,1)*l5*sa1*sa3 + (ca1*ca2*ca3*cb*ee**2*complex(0,1)*sa1*sa2*sb)/(2.*cw**2) - 4*ca1*ca2*ca3*cb*complex(0,1)*l5*sa1*sa2*sb + (ca1**2*ca2*cb*ee**2*complex(0,1)*sa3*sb)/(4.*cw**2) - 2*ca1**2*ca2*cb*complex(0,1)*l5*sa3*sb - (ca2*cb*ee**2*complex(0,1)*sa1**2*sa3*sb)/(4.*cw**2) + 2*ca2*cb*complex(0,1)*l5*sa1**2*sa3*sb - (ca1**2*ca2*ca3*ee**2*complex(0,1)*sa2*sb**2)/(4.*cw**2) + ca1**2*ca2*ca3*complex(0,1)*l1*sa2*sb**2 - ca2*ca3*complex(0,1)*l7*sa2*sb**2 + ca2*ca3*complex(0,1)*l3*sa1**2*sa2*sb**2 + ca2*ca3*complex(0,1)*l4*sa1**2*sa2*sb**2 - ca2*ca3*complex(0,1)*l5*sa1**2*sa2*sb**2 + (ca1*ca2*ee**2*complex(0,1)*sa1*sa3*sb**2)/(4.*cw**2) - ca1*ca2*complex(0,1)*l1*sa1*sa3*sb**2 + ca1*ca2*complex(0,1)*l3*sa1*sa3*sb**2 + ca1*ca2*complex(0,1)*l4*sa1*sa3*sb**2 - ca1*ca2*complex(0,1)*l5*sa1*sa3*sb**2 - (ca2*ca3*cb**2*ee**2*complex(0,1)*sa1**2*sa2)/(4.*sw**2) - (ca1*ca2*cb**2*ee**2*complex(0,1)*sa1*sa3)/(4.*sw**2) + (ca1*ca2*ca3*cb*ee**2*complex(0,1)*sa1*sa2*sb)/(2.*sw**2) + (ca1**2*ca2*cb*ee**2*complex(0,1)*sa3*sb)/(4.*sw**2) - (ca2*cb*ee**2*complex(0,1)*sa1**2*sa3*sb)/(4.*sw**2) - (ca1**2*ca2*ca3*ee**2*complex(0,1)*sa2*sb**2)/(4.*sw**2) + (ca1*ca2*ee**2*complex(0,1)*sa1*sa3*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_224 = Coupling(name = 'GC_224',
                  value = 'ca1**2*ca2*ca3*cb**2*complex(0,1)*l3*sa2 - ca2*ca3*cb**2*complex(0,1)*l8*sa2 + ca2*ca3*cb**2*complex(0,1)*l2*sa1**2*sa2 + ca1*ca2*cb**2*complex(0,1)*l2*sa1*sa3 - ca1*ca2*cb**2*complex(0,1)*l3*sa1*sa3 - 2*ca1*ca2*ca3*cb*complex(0,1)*l4*sa1*sa2*sb - 2*ca1*ca2*ca3*cb*complex(0,1)*l5*sa1*sa2*sb - ca1**2*ca2*cb*complex(0,1)*l4*sa3*sb - ca1**2*ca2*cb*complex(0,1)*l5*sa3*sb + ca2*cb*complex(0,1)*l4*sa1**2*sa3*sb + ca2*cb*complex(0,1)*l5*sa1**2*sa3*sb + ca1**2*ca2*ca3*complex(0,1)*l1*sa2*sb**2 - ca2*ca3*complex(0,1)*l7*sa2*sb**2 + ca2*ca3*complex(0,1)*l3*sa1**2*sa2*sb**2 - ca1*ca2*complex(0,1)*l1*sa1*sa3*sb**2 + ca1*ca2*complex(0,1)*l3*sa1*sa3*sb**2 + (ca2*ca3*cb**2*ee**2*complex(0,1)*sa1**2*sa2)/(2.*sw**2) + (ca1*ca2*cb**2*ee**2*complex(0,1)*sa1*sa3)/(2.*sw**2) - (ca1*ca2*ca3*cb*ee**2*complex(0,1)*sa1*sa2*sb)/sw**2 - (ca1**2*ca2*cb*ee**2*complex(0,1)*sa3*sb)/(2.*sw**2) + (ca2*cb*ee**2*complex(0,1)*sa1**2*sa3*sb)/(2.*sw**2) + (ca1**2*ca2*ca3*ee**2*complex(0,1)*sa2*sb**2)/(2.*sw**2) - (ca1*ca2*ee**2*complex(0,1)*sa1*sa3*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_225 = Coupling(name = 'GC_225',
                  value = 'ca1**2*ca2*ca3*cb**2*complex(0,1)*l3*sa2 + ca1**2*ca2*ca3*cb**2*complex(0,1)*l4*sa2 - ca1**2*ca2*ca3*cb**2*complex(0,1)*l5*sa2 - ca2*ca3*cb**2*complex(0,1)*l8*sa2 + (ca2*ca3*cb**2*ee**2*complex(0,1)*sa1**2*sa2)/(2.*cw**2) + ca2*ca3*cb**2*complex(0,1)*l2*sa1**2*sa2 + (ca1*ca2*cb**2*ee**2*complex(0,1)*sa1*sa3)/(2.*cw**2) + ca1*ca2*cb**2*complex(0,1)*l2*sa1*sa3 - ca1*ca2*cb**2*complex(0,1)*l3*sa1*sa3 - ca1*ca2*cb**2*complex(0,1)*l4*sa1*sa3 + ca1*ca2*cb**2*complex(0,1)*l5*sa1*sa3 - (ca1*ca2*ca3*cb*ee**2*complex(0,1)*sa1*sa2*sb)/cw**2 - 4*ca1*ca2*ca3*cb*complex(0,1)*l5*sa1*sa2*sb - (ca1**2*ca2*cb*ee**2*complex(0,1)*sa3*sb)/(2.*cw**2) - 2*ca1**2*ca2*cb*complex(0,1)*l5*sa3*sb + (ca2*cb*ee**2*complex(0,1)*sa1**2*sa3*sb)/(2.*cw**2) + 2*ca2*cb*complex(0,1)*l5*sa1**2*sa3*sb + (ca1**2*ca2*ca3*ee**2*complex(0,1)*sa2*sb**2)/(2.*cw**2) + ca1**2*ca2*ca3*complex(0,1)*l1*sa2*sb**2 - ca2*ca3*complex(0,1)*l7*sa2*sb**2 + ca2*ca3*complex(0,1)*l3*sa1**2*sa2*sb**2 + ca2*ca3*complex(0,1)*l4*sa1**2*sa2*sb**2 - ca2*ca3*complex(0,1)*l5*sa1**2*sa2*sb**2 - (ca1*ca2*ee**2*complex(0,1)*sa1*sa3*sb**2)/(2.*cw**2) - ca1*ca2*complex(0,1)*l1*sa1*sa3*sb**2 + ca1*ca2*complex(0,1)*l3*sa1*sa3*sb**2 + ca1*ca2*complex(0,1)*l4*sa1*sa3*sb**2 - ca1*ca2*complex(0,1)*l5*sa1*sa3*sb**2 + (ca2*ca3*cb**2*ee**2*complex(0,1)*sa1**2*sa2)/(2.*sw**2) + (ca1*ca2*cb**2*ee**2*complex(0,1)*sa1*sa3)/(2.*sw**2) - (ca1*ca2*ca3*cb*ee**2*complex(0,1)*sa1*sa2*sb)/sw**2 - (ca1**2*ca2*cb*ee**2*complex(0,1)*sa3*sb)/(2.*sw**2) + (ca2*cb*ee**2*complex(0,1)*sa1**2*sa3*sb)/(2.*sw**2) + (ca1**2*ca2*ca3*ee**2*complex(0,1)*sa2*sb**2)/(2.*sw**2) - (ca1*ca2*ee**2*complex(0,1)*sa1*sa3*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_226 = Coupling(name = 'GC_226',
                  value = 'ca1**2*ca2*ca3*cb**2*complex(0,1)*l1*sa2 - ca2*ca3*cb**2*complex(0,1)*l7*sa2 + ca2*ca3*cb**2*complex(0,1)*l3*sa1**2*sa2 - ca1*ca2*cb**2*complex(0,1)*l1*sa1*sa3 + ca1*ca2*cb**2*complex(0,1)*l3*sa1*sa3 + 2*ca1*ca2*ca3*cb*complex(0,1)*l4*sa1*sa2*sb + 2*ca1*ca2*ca3*cb*complex(0,1)*l5*sa1*sa2*sb + ca1**2*ca2*cb*complex(0,1)*l4*sa3*sb + ca1**2*ca2*cb*complex(0,1)*l5*sa3*sb - ca2*cb*complex(0,1)*l4*sa1**2*sa3*sb - ca2*cb*complex(0,1)*l5*sa1**2*sa3*sb + ca1**2*ca2*ca3*complex(0,1)*l3*sa2*sb**2 - ca2*ca3*complex(0,1)*l8*sa2*sb**2 + ca2*ca3*complex(0,1)*l2*sa1**2*sa2*sb**2 + ca1*ca2*complex(0,1)*l2*sa1*sa3*sb**2 - ca1*ca2*complex(0,1)*l3*sa1*sa3*sb**2 + (ca1**2*ca2*ca3*cb**2*ee**2*complex(0,1)*sa2)/(2.*sw**2) - (ca1*ca2*cb**2*ee**2*complex(0,1)*sa1*sa3)/(2.*sw**2) + (ca1*ca2*ca3*cb*ee**2*complex(0,1)*sa1*sa2*sb)/sw**2 + (ca1**2*ca2*cb*ee**2*complex(0,1)*sa3*sb)/(2.*sw**2) - (ca2*cb*ee**2*complex(0,1)*sa1**2*sa3*sb)/(2.*sw**2) + (ca2*ca3*ee**2*complex(0,1)*sa1**2*sa2*sb**2)/(2.*sw**2) + (ca1*ca2*ee**2*complex(0,1)*sa1*sa3*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_227 = Coupling(name = 'GC_227',
                  value = '(ca1**2*ca2*ca3*cb**2*ee**2*complex(0,1)*sa2)/(2.*cw**2) + ca1**2*ca2*ca3*cb**2*complex(0,1)*l1*sa2 - ca2*ca3*cb**2*complex(0,1)*l7*sa2 + ca2*ca3*cb**2*complex(0,1)*l3*sa1**2*sa2 + ca2*ca3*cb**2*complex(0,1)*l4*sa1**2*sa2 - ca2*ca3*cb**2*complex(0,1)*l5*sa1**2*sa2 - (ca1*ca2*cb**2*ee**2*complex(0,1)*sa1*sa3)/(2.*cw**2) - ca1*ca2*cb**2*complex(0,1)*l1*sa1*sa3 + ca1*ca2*cb**2*complex(0,1)*l3*sa1*sa3 + ca1*ca2*cb**2*complex(0,1)*l4*sa1*sa3 - ca1*ca2*cb**2*complex(0,1)*l5*sa1*sa3 + (ca1*ca2*ca3*cb*ee**2*complex(0,1)*sa1*sa2*sb)/cw**2 + 4*ca1*ca2*ca3*cb*complex(0,1)*l5*sa1*sa2*sb + (ca1**2*ca2*cb*ee**2*complex(0,1)*sa3*sb)/(2.*cw**2) + 2*ca1**2*ca2*cb*complex(0,1)*l5*sa3*sb - (ca2*cb*ee**2*complex(0,1)*sa1**2*sa3*sb)/(2.*cw**2) - 2*ca2*cb*complex(0,1)*l5*sa1**2*sa3*sb + ca1**2*ca2*ca3*complex(0,1)*l3*sa2*sb**2 + ca1**2*ca2*ca3*complex(0,1)*l4*sa2*sb**2 - ca1**2*ca2*ca3*complex(0,1)*l5*sa2*sb**2 - ca2*ca3*complex(0,1)*l8*sa2*sb**2 + (ca2*ca3*ee**2*complex(0,1)*sa1**2*sa2*sb**2)/(2.*cw**2) + ca2*ca3*complex(0,1)*l2*sa1**2*sa2*sb**2 + (ca1*ca2*ee**2*complex(0,1)*sa1*sa3*sb**2)/(2.*cw**2) + ca1*ca2*complex(0,1)*l2*sa1*sa3*sb**2 - ca1*ca2*complex(0,1)*l3*sa1*sa3*sb**2 - ca1*ca2*complex(0,1)*l4*sa1*sa3*sb**2 + ca1*ca2*complex(0,1)*l5*sa1*sa3*sb**2 + (ca1**2*ca2*ca3*cb**2*ee**2*complex(0,1)*sa2)/(2.*sw**2) - (ca1*ca2*cb**2*ee**2*complex(0,1)*sa1*sa3)/(2.*sw**2) + (ca1*ca2*ca3*cb*ee**2*complex(0,1)*sa1*sa2*sb)/sw**2 + (ca1**2*ca2*cb*ee**2*complex(0,1)*sa3*sb)/(2.*sw**2) - (ca2*cb*ee**2*complex(0,1)*sa1**2*sa3*sb)/(2.*sw**2) + (ca2*ca3*ee**2*complex(0,1)*sa1**2*sa2*sb**2)/(2.*sw**2) + (ca1*ca2*ee**2*complex(0,1)*sa1*sa3*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_228 = Coupling(name = 'GC_228',
                  value = 'ca1*ca2*ca3*cb**2*complex(0,1)*l4*sa1*sa2 + ca1*ca2*ca3*cb**2*complex(0,1)*l5*sa1*sa2 + (ca1**2*ca2*cb**2*complex(0,1)*l4*sa3)/2. + (ca1**2*ca2*cb**2*complex(0,1)*l5*sa3)/2. - (ca2*cb**2*complex(0,1)*l4*sa1**2*sa3)/2. - (ca2*cb**2*complex(0,1)*l5*sa1**2*sa3)/2. - ca1**2*ca2*ca3*cb*complex(0,1)*l1*sa2*sb + ca1**2*ca2*ca3*cb*complex(0,1)*l3*sa2*sb + ca2*ca3*cb*complex(0,1)*l7*sa2*sb - ca2*ca3*cb*complex(0,1)*l8*sa2*sb + ca2*ca3*cb*complex(0,1)*l2*sa1**2*sa2*sb - ca2*ca3*cb*complex(0,1)*l3*sa1**2*sa2*sb + ca1*ca2*cb*complex(0,1)*l1*sa1*sa3*sb + ca1*ca2*cb*complex(0,1)*l2*sa1*sa3*sb - 2*ca1*ca2*cb*complex(0,1)*l3*sa1*sa3*sb - ca1*ca2*ca3*complex(0,1)*l4*sa1*sa2*sb**2 - ca1*ca2*ca3*complex(0,1)*l5*sa1*sa2*sb**2 - (ca1**2*ca2*complex(0,1)*l4*sa3*sb**2)/2. - (ca1**2*ca2*complex(0,1)*l5*sa3*sb**2)/2. + (ca2*complex(0,1)*l4*sa1**2*sa3*sb**2)/2. + (ca2*complex(0,1)*l5*sa1**2*sa3*sb**2)/2. - (ca1*ca2*ca3*cb**2*ee**2*complex(0,1)*sa1*sa2)/(4.*sw**2) - (ca1**2*ca2*cb**2*ee**2*complex(0,1)*sa3)/(4.*sw**2) + (ca1**2*ca2*ca3*cb*ee**2*complex(0,1)*sa2*sb)/(4.*sw**2) - (ca2*ca3*cb*ee**2*complex(0,1)*sa1**2*sa2*sb)/(4.*sw**2) - (ca1*ca2*cb*ee**2*complex(0,1)*sa1*sa3*sb)/(2.*sw**2) + (ca1*ca2*ca3*ee**2*complex(0,1)*sa1*sa2*sb**2)/(4.*sw**2) - (ca2*ee**2*complex(0,1)*sa1**2*sa3*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_229 = Coupling(name = 'GC_229',
                  value = '-(ca1*ca2*ca3*cb**2*ee**2*complex(0,1)*sa1*sa2)/(4.*cw**2) + 2*ca1*ca2*ca3*cb**2*complex(0,1)*l5*sa1*sa2 - (ca1**2*ca2*cb**2*ee**2*complex(0,1)*sa3)/(4.*cw**2) + ca1**2*ca2*cb**2*complex(0,1)*l5*sa3 - ca2*cb**2*complex(0,1)*l5*sa1**2*sa3 + (ca1**2*ca2*ca3*cb*ee**2*complex(0,1)*sa2*sb)/(4.*cw**2) - ca1**2*ca2*ca3*cb*complex(0,1)*l1*sa2*sb + ca1**2*ca2*ca3*cb*complex(0,1)*l3*sa2*sb + ca1**2*ca2*ca3*cb*complex(0,1)*l4*sa2*sb - ca1**2*ca2*ca3*cb*complex(0,1)*l5*sa2*sb + ca2*ca3*cb*complex(0,1)*l7*sa2*sb - ca2*ca3*cb*complex(0,1)*l8*sa2*sb - (ca2*ca3*cb*ee**2*complex(0,1)*sa1**2*sa2*sb)/(4.*cw**2) + ca2*ca3*cb*complex(0,1)*l2*sa1**2*sa2*sb - ca2*ca3*cb*complex(0,1)*l3*sa1**2*sa2*sb - ca2*ca3*cb*complex(0,1)*l4*sa1**2*sa2*sb + ca2*ca3*cb*complex(0,1)*l5*sa1**2*sa2*sb - (ca1*ca2*cb*ee**2*complex(0,1)*sa1*sa3*sb)/(2.*cw**2) + ca1*ca2*cb*complex(0,1)*l1*sa1*sa3*sb + ca1*ca2*cb*complex(0,1)*l2*sa1*sa3*sb - 2*ca1*ca2*cb*complex(0,1)*l3*sa1*sa3*sb - 2*ca1*ca2*cb*complex(0,1)*l4*sa1*sa3*sb + 2*ca1*ca2*cb*complex(0,1)*l5*sa1*sa3*sb + (ca1*ca2*ca3*ee**2*complex(0,1)*sa1*sa2*sb**2)/(4.*cw**2) - 2*ca1*ca2*ca3*complex(0,1)*l5*sa1*sa2*sb**2 - ca1**2*ca2*complex(0,1)*l5*sa3*sb**2 - (ca2*ee**2*complex(0,1)*sa1**2*sa3*sb**2)/(4.*cw**2) + ca2*complex(0,1)*l5*sa1**2*sa3*sb**2 - (ca1*ca2*ca3*cb**2*ee**2*complex(0,1)*sa1*sa2)/(4.*sw**2) - (ca1**2*ca2*cb**2*ee**2*complex(0,1)*sa3)/(4.*sw**2) + (ca1**2*ca2*ca3*cb*ee**2*complex(0,1)*sa2*sb)/(4.*sw**2) - (ca2*ca3*cb*ee**2*complex(0,1)*sa1**2*sa2*sb)/(4.*sw**2) - (ca1*ca2*cb*ee**2*complex(0,1)*sa1*sa3*sb)/(2.*sw**2) + (ca1*ca2*ca3*ee**2*complex(0,1)*sa1*sa2*sb**2)/(4.*sw**2) - (ca2*ee**2*complex(0,1)*sa1**2*sa3*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_230 = Coupling(name = 'GC_230',
                  value = 'ca1*ca2*ca3*cb**2*complex(0,1)*l4*sa1*sa2 + ca1*ca2*ca3*cb**2*complex(0,1)*l5*sa1*sa2 + (ca1**2*ca2*cb**2*complex(0,1)*l4*sa3)/2. + (ca1**2*ca2*cb**2*complex(0,1)*l5*sa3)/2. - (ca2*cb**2*complex(0,1)*l4*sa1**2*sa3)/2. - (ca2*cb**2*complex(0,1)*l5*sa1**2*sa3)/2. - ca1**2*ca2*ca3*cb*complex(0,1)*l1*sa2*sb + ca1**2*ca2*ca3*cb*complex(0,1)*l3*sa2*sb + ca2*ca3*cb*complex(0,1)*l7*sa2*sb - ca2*ca3*cb*complex(0,1)*l8*sa2*sb + ca2*ca3*cb*complex(0,1)*l2*sa1**2*sa2*sb - ca2*ca3*cb*complex(0,1)*l3*sa1**2*sa2*sb + ca1*ca2*cb*complex(0,1)*l1*sa1*sa3*sb + ca1*ca2*cb*complex(0,1)*l2*sa1*sa3*sb - 2*ca1*ca2*cb*complex(0,1)*l3*sa1*sa3*sb - ca1*ca2*ca3*complex(0,1)*l4*sa1*sa2*sb**2 - ca1*ca2*ca3*complex(0,1)*l5*sa1*sa2*sb**2 - (ca1**2*ca2*complex(0,1)*l4*sa3*sb**2)/2. - (ca1**2*ca2*complex(0,1)*l5*sa3*sb**2)/2. + (ca2*complex(0,1)*l4*sa1**2*sa3*sb**2)/2. + (ca2*complex(0,1)*l5*sa1**2*sa3*sb**2)/2. + (ca1*ca2*ca3*cb**2*ee**2*complex(0,1)*sa1*sa2)/(2.*sw**2) + (ca1**2*ca2*cb**2*ee**2*complex(0,1)*sa3)/(4.*sw**2) - (ca2*cb**2*ee**2*complex(0,1)*sa1**2*sa3)/(4.*sw**2) - (ca1**2*ca2*ca3*cb*ee**2*complex(0,1)*sa2*sb)/(2.*sw**2) + (ca2*ca3*cb*ee**2*complex(0,1)*sa1**2*sa2*sb)/(2.*sw**2) + (ca1*ca2*cb*ee**2*complex(0,1)*sa1*sa3*sb)/sw**2 - (ca1*ca2*ca3*ee**2*complex(0,1)*sa1*sa2*sb**2)/(2.*sw**2) - (ca1**2*ca2*ee**2*complex(0,1)*sa3*sb**2)/(4.*sw**2) + (ca2*ee**2*complex(0,1)*sa1**2*sa3*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_231 = Coupling(name = 'GC_231',
                  value = '(ca1*ca2*ca3*cb**2*ee**2*complex(0,1)*sa1*sa2)/(2.*cw**2) + 2*ca1*ca2*ca3*cb**2*complex(0,1)*l5*sa1*sa2 + (ca1**2*ca2*cb**2*ee**2*complex(0,1)*sa3)/(4.*cw**2) + ca1**2*ca2*cb**2*complex(0,1)*l5*sa3 - (ca2*cb**2*ee**2*complex(0,1)*sa1**2*sa3)/(4.*cw**2) - ca2*cb**2*complex(0,1)*l5*sa1**2*sa3 - (ca1**2*ca2*ca3*cb*ee**2*complex(0,1)*sa2*sb)/(2.*cw**2) - ca1**2*ca2*ca3*cb*complex(0,1)*l1*sa2*sb + ca1**2*ca2*ca3*cb*complex(0,1)*l3*sa2*sb + ca1**2*ca2*ca3*cb*complex(0,1)*l4*sa2*sb - ca1**2*ca2*ca3*cb*complex(0,1)*l5*sa2*sb + ca2*ca3*cb*complex(0,1)*l7*sa2*sb - ca2*ca3*cb*complex(0,1)*l8*sa2*sb + (ca2*ca3*cb*ee**2*complex(0,1)*sa1**2*sa2*sb)/(2.*cw**2) + ca2*ca3*cb*complex(0,1)*l2*sa1**2*sa2*sb - ca2*ca3*cb*complex(0,1)*l3*sa1**2*sa2*sb - ca2*ca3*cb*complex(0,1)*l4*sa1**2*sa2*sb + ca2*ca3*cb*complex(0,1)*l5*sa1**2*sa2*sb + (ca1*ca2*cb*ee**2*complex(0,1)*sa1*sa3*sb)/cw**2 + ca1*ca2*cb*complex(0,1)*l1*sa1*sa3*sb + ca1*ca2*cb*complex(0,1)*l2*sa1*sa3*sb - 2*ca1*ca2*cb*complex(0,1)*l3*sa1*sa3*sb - 2*ca1*ca2*cb*complex(0,1)*l4*sa1*sa3*sb + 2*ca1*ca2*cb*complex(0,1)*l5*sa1*sa3*sb - (ca1*ca2*ca3*ee**2*complex(0,1)*sa1*sa2*sb**2)/(2.*cw**2) - 2*ca1*ca2*ca3*complex(0,1)*l5*sa1*sa2*sb**2 - (ca1**2*ca2*ee**2*complex(0,1)*sa3*sb**2)/(4.*cw**2) - ca1**2*ca2*complex(0,1)*l5*sa3*sb**2 + (ca2*ee**2*complex(0,1)*sa1**2*sa3*sb**2)/(4.*cw**2) + ca2*complex(0,1)*l5*sa1**2*sa3*sb**2 + (ca1*ca2*ca3*cb**2*ee**2*complex(0,1)*sa1*sa2)/(2.*sw**2) + (ca1**2*ca2*cb**2*ee**2*complex(0,1)*sa3)/(4.*sw**2) - (ca2*cb**2*ee**2*complex(0,1)*sa1**2*sa3)/(4.*sw**2) - (ca1**2*ca2*ca3*cb*ee**2*complex(0,1)*sa2*sb)/(2.*sw**2) + (ca2*ca3*cb*ee**2*complex(0,1)*sa1**2*sa2*sb)/(2.*sw**2) + (ca1*ca2*cb*ee**2*complex(0,1)*sa1*sa3*sb)/sw**2 - (ca1*ca2*ca3*ee**2*complex(0,1)*sa1*sa2*sb**2)/(2.*sw**2) - (ca1**2*ca2*ee**2*complex(0,1)*sa3*sb**2)/(4.*sw**2) + (ca2*ee**2*complex(0,1)*sa1**2*sa3*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_232 = Coupling(name = 'GC_232',
                  value = '-(ca1*ca2*ca3*cb**2*complex(0,1)*l2*sa1) + ca1*ca2*ca3*cb**2*complex(0,1)*l3*sa1 + ca1**2*ca2*cb**2*complex(0,1)*l3*sa2*sa3 - ca2*cb**2*complex(0,1)*l8*sa2*sa3 + ca2*cb**2*complex(0,1)*l2*sa1**2*sa2*sa3 + ca1**2*ca2*ca3*cb*complex(0,1)*l4*sb + ca1**2*ca2*ca3*cb*complex(0,1)*l5*sb - ca2*ca3*cb*complex(0,1)*l4*sa1**2*sb - ca2*ca3*cb*complex(0,1)*l5*sa1**2*sb - 2*ca1*ca2*cb*complex(0,1)*l4*sa1*sa2*sa3*sb - 2*ca1*ca2*cb*complex(0,1)*l5*sa1*sa2*sa3*sb + ca1*ca2*ca3*complex(0,1)*l1*sa1*sb**2 - ca1*ca2*ca3*complex(0,1)*l3*sa1*sb**2 + ca1**2*ca2*complex(0,1)*l1*sa2*sa3*sb**2 - ca2*complex(0,1)*l7*sa2*sa3*sb**2 + ca2*complex(0,1)*l3*sa1**2*sa2*sa3*sb**2 + (ca1*ca2*ca3*cb**2*ee**2*complex(0,1)*sa1)/(4.*sw**2) - (ca2*cb**2*ee**2*complex(0,1)*sa1**2*sa2*sa3)/(4.*sw**2) - (ca1**2*ca2*ca3*cb*ee**2*complex(0,1)*sb)/(4.*sw**2) + (ca2*ca3*cb*ee**2*complex(0,1)*sa1**2*sb)/(4.*sw**2) + (ca1*ca2*cb*ee**2*complex(0,1)*sa1*sa2*sa3*sb)/(2.*sw**2) - (ca1*ca2*ca3*ee**2*complex(0,1)*sa1*sb**2)/(4.*sw**2) - (ca1**2*ca2*ee**2*complex(0,1)*sa2*sa3*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_233 = Coupling(name = 'GC_233',
                  value = '(ca1*ca2*ca3*cb**2*ee**2*complex(0,1)*sa1)/(4.*cw**2) - ca1*ca2*ca3*cb**2*complex(0,1)*l2*sa1 + ca1*ca2*ca3*cb**2*complex(0,1)*l3*sa1 + ca1*ca2*ca3*cb**2*complex(0,1)*l4*sa1 - ca1*ca2*ca3*cb**2*complex(0,1)*l5*sa1 + ca1**2*ca2*cb**2*complex(0,1)*l3*sa2*sa3 + ca1**2*ca2*cb**2*complex(0,1)*l4*sa2*sa3 - ca1**2*ca2*cb**2*complex(0,1)*l5*sa2*sa3 - ca2*cb**2*complex(0,1)*l8*sa2*sa3 - (ca2*cb**2*ee**2*complex(0,1)*sa1**2*sa2*sa3)/(4.*cw**2) + ca2*cb**2*complex(0,1)*l2*sa1**2*sa2*sa3 - (ca1**2*ca2*ca3*cb*ee**2*complex(0,1)*sb)/(4.*cw**2) + 2*ca1**2*ca2*ca3*cb*complex(0,1)*l5*sb + (ca2*ca3*cb*ee**2*complex(0,1)*sa1**2*sb)/(4.*cw**2) - 2*ca2*ca3*cb*complex(0,1)*l5*sa1**2*sb + (ca1*ca2*cb*ee**2*complex(0,1)*sa1*sa2*sa3*sb)/(2.*cw**2) - 4*ca1*ca2*cb*complex(0,1)*l5*sa1*sa2*sa3*sb - (ca1*ca2*ca3*ee**2*complex(0,1)*sa1*sb**2)/(4.*cw**2) + ca1*ca2*ca3*complex(0,1)*l1*sa1*sb**2 - ca1*ca2*ca3*complex(0,1)*l3*sa1*sb**2 - ca1*ca2*ca3*complex(0,1)*l4*sa1*sb**2 + ca1*ca2*ca3*complex(0,1)*l5*sa1*sb**2 - (ca1**2*ca2*ee**2*complex(0,1)*sa2*sa3*sb**2)/(4.*cw**2) + ca1**2*ca2*complex(0,1)*l1*sa2*sa3*sb**2 - ca2*complex(0,1)*l7*sa2*sa3*sb**2 + ca2*complex(0,1)*l3*sa1**2*sa2*sa3*sb**2 + ca2*complex(0,1)*l4*sa1**2*sa2*sa3*sb**2 - ca2*complex(0,1)*l5*sa1**2*sa2*sa3*sb**2 + (ca1*ca2*ca3*cb**2*ee**2*complex(0,1)*sa1)/(4.*sw**2) - (ca2*cb**2*ee**2*complex(0,1)*sa1**2*sa2*sa3)/(4.*sw**2) - (ca1**2*ca2*ca3*cb*ee**2*complex(0,1)*sb)/(4.*sw**2) + (ca2*ca3*cb*ee**2*complex(0,1)*sa1**2*sb)/(4.*sw**2) + (ca1*ca2*cb*ee**2*complex(0,1)*sa1*sa2*sa3*sb)/(2.*sw**2) - (ca1*ca2*ca3*ee**2*complex(0,1)*sa1*sb**2)/(4.*sw**2) - (ca1**2*ca2*ee**2*complex(0,1)*sa2*sa3*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_234 = Coupling(name = 'GC_234',
                  value = '-(ca1*ca2*ca3*cb**2*complex(0,1)*l2*sa1) + ca1*ca2*ca3*cb**2*complex(0,1)*l3*sa1 + ca1**2*ca2*cb**2*complex(0,1)*l3*sa2*sa3 - ca2*cb**2*complex(0,1)*l8*sa2*sa3 + ca2*cb**2*complex(0,1)*l2*sa1**2*sa2*sa3 + ca1**2*ca2*ca3*cb*complex(0,1)*l4*sb + ca1**2*ca2*ca3*cb*complex(0,1)*l5*sb - ca2*ca3*cb*complex(0,1)*l4*sa1**2*sb - ca2*ca3*cb*complex(0,1)*l5*sa1**2*sb - 2*ca1*ca2*cb*complex(0,1)*l4*sa1*sa2*sa3*sb - 2*ca1*ca2*cb*complex(0,1)*l5*sa1*sa2*sa3*sb + ca1*ca2*ca3*complex(0,1)*l1*sa1*sb**2 - ca1*ca2*ca3*complex(0,1)*l3*sa1*sb**2 + ca1**2*ca2*complex(0,1)*l1*sa2*sa3*sb**2 - ca2*complex(0,1)*l7*sa2*sa3*sb**2 + ca2*complex(0,1)*l3*sa1**2*sa2*sa3*sb**2 - (ca1*ca2*ca3*cb**2*ee**2*complex(0,1)*sa1)/(2.*sw**2) + (ca2*cb**2*ee**2*complex(0,1)*sa1**2*sa2*sa3)/(2.*sw**2) + (ca1**2*ca2*ca3*cb*ee**2*complex(0,1)*sb)/(2.*sw**2) - (ca2*ca3*cb*ee**2*complex(0,1)*sa1**2*sb)/(2.*sw**2) - (ca1*ca2*cb*ee**2*complex(0,1)*sa1*sa2*sa3*sb)/sw**2 + (ca1*ca2*ca3*ee**2*complex(0,1)*sa1*sb**2)/(2.*sw**2) + (ca1**2*ca2*ee**2*complex(0,1)*sa2*sa3*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_235 = Coupling(name = 'GC_235',
                  value = '-(ca1*ca2*ca3*cb**2*ee**2*complex(0,1)*sa1)/(2.*cw**2) - ca1*ca2*ca3*cb**2*complex(0,1)*l2*sa1 + ca1*ca2*ca3*cb**2*complex(0,1)*l3*sa1 + ca1*ca2*ca3*cb**2*complex(0,1)*l4*sa1 - ca1*ca2*ca3*cb**2*complex(0,1)*l5*sa1 + ca1**2*ca2*cb**2*complex(0,1)*l3*sa2*sa3 + ca1**2*ca2*cb**2*complex(0,1)*l4*sa2*sa3 - ca1**2*ca2*cb**2*complex(0,1)*l5*sa2*sa3 - ca2*cb**2*complex(0,1)*l8*sa2*sa3 + (ca2*cb**2*ee**2*complex(0,1)*sa1**2*sa2*sa3)/(2.*cw**2) + ca2*cb**2*complex(0,1)*l2*sa1**2*sa2*sa3 + (ca1**2*ca2*ca3*cb*ee**2*complex(0,1)*sb)/(2.*cw**2) + 2*ca1**2*ca2*ca3*cb*complex(0,1)*l5*sb - (ca2*ca3*cb*ee**2*complex(0,1)*sa1**2*sb)/(2.*cw**2) - 2*ca2*ca3*cb*complex(0,1)*l5*sa1**2*sb - (ca1*ca2*cb*ee**2*complex(0,1)*sa1*sa2*sa3*sb)/cw**2 - 4*ca1*ca2*cb*complex(0,1)*l5*sa1*sa2*sa3*sb + (ca1*ca2*ca3*ee**2*complex(0,1)*sa1*sb**2)/(2.*cw**2) + ca1*ca2*ca3*complex(0,1)*l1*sa1*sb**2 - ca1*ca2*ca3*complex(0,1)*l3*sa1*sb**2 - ca1*ca2*ca3*complex(0,1)*l4*sa1*sb**2 + ca1*ca2*ca3*complex(0,1)*l5*sa1*sb**2 + (ca1**2*ca2*ee**2*complex(0,1)*sa2*sa3*sb**2)/(2.*cw**2) + ca1**2*ca2*complex(0,1)*l1*sa2*sa3*sb**2 - ca2*complex(0,1)*l7*sa2*sa3*sb**2 + ca2*complex(0,1)*l3*sa1**2*sa2*sa3*sb**2 + ca2*complex(0,1)*l4*sa1**2*sa2*sa3*sb**2 - ca2*complex(0,1)*l5*sa1**2*sa2*sa3*sb**2 - (ca1*ca2*ca3*cb**2*ee**2*complex(0,1)*sa1)/(2.*sw**2) + (ca2*cb**2*ee**2*complex(0,1)*sa1**2*sa2*sa3)/(2.*sw**2) + (ca1**2*ca2*ca3*cb*ee**2*complex(0,1)*sb)/(2.*sw**2) - (ca2*ca3*cb*ee**2*complex(0,1)*sa1**2*sb)/(2.*sw**2) - (ca1*ca2*cb*ee**2*complex(0,1)*sa1*sa2*sa3*sb)/sw**2 + (ca1*ca2*ca3*ee**2*complex(0,1)*sa1*sb**2)/(2.*sw**2) + (ca1**2*ca2*ee**2*complex(0,1)*sa2*sa3*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_236 = Coupling(name = 'GC_236',
                  value = '-(ca1**2*ca2*ca3*cb**2*complex(0,1)*l4)/2. - (ca1**2*ca2*ca3*cb**2*complex(0,1)*l5)/2. + (ca2*ca3*cb**2*complex(0,1)*l4*sa1**2)/2. + (ca2*ca3*cb**2*complex(0,1)*l5*sa1**2)/2. + ca1*ca2*cb**2*complex(0,1)*l4*sa1*sa2*sa3 + ca1*ca2*cb**2*complex(0,1)*l5*sa1*sa2*sa3 - ca1*ca2*ca3*cb*complex(0,1)*l1*sa1*sb - ca1*ca2*ca3*cb*complex(0,1)*l2*sa1*sb + 2*ca1*ca2*ca3*cb*complex(0,1)*l3*sa1*sb - ca1**2*ca2*cb*complex(0,1)*l1*sa2*sa3*sb + ca1**2*ca2*cb*complex(0,1)*l3*sa2*sa3*sb + ca2*cb*complex(0,1)*l7*sa2*sa3*sb - ca2*cb*complex(0,1)*l8*sa2*sa3*sb + ca2*cb*complex(0,1)*l2*sa1**2*sa2*sa3*sb - ca2*cb*complex(0,1)*l3*sa1**2*sa2*sa3*sb + (ca1**2*ca2*ca3*complex(0,1)*l4*sb**2)/2. + (ca1**2*ca2*ca3*complex(0,1)*l5*sb**2)/2. - (ca2*ca3*complex(0,1)*l4*sa1**2*sb**2)/2. - (ca2*ca3*complex(0,1)*l5*sa1**2*sb**2)/2. - ca1*ca2*complex(0,1)*l4*sa1*sa2*sa3*sb**2 - ca1*ca2*complex(0,1)*l5*sa1*sa2*sa3*sb**2 - (ca2*ca3*cb**2*ee**2*complex(0,1)*sa1**2)/(4.*sw**2) - (ca1*ca2*cb**2*ee**2*complex(0,1)*sa1*sa2*sa3)/(4.*sw**2) + (ca1*ca2*ca3*cb*ee**2*complex(0,1)*sa1*sb)/(2.*sw**2) + (ca1**2*ca2*cb*ee**2*complex(0,1)*sa2*sa3*sb)/(4.*sw**2) - (ca2*cb*ee**2*complex(0,1)*sa1**2*sa2*sa3*sb)/(4.*sw**2) - (ca1**2*ca2*ca3*ee**2*complex(0,1)*sb**2)/(4.*sw**2) + (ca1*ca2*ee**2*complex(0,1)*sa1*sa2*sa3*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_237 = Coupling(name = 'GC_237',
                  value = '-(ca1**2*ca2*ca3*cb**2*complex(0,1)*l5) - (ca2*ca3*cb**2*ee**2*complex(0,1)*sa1**2)/(4.*cw**2) + ca2*ca3*cb**2*complex(0,1)*l5*sa1**2 - (ca1*ca2*cb**2*ee**2*complex(0,1)*sa1*sa2*sa3)/(4.*cw**2) + 2*ca1*ca2*cb**2*complex(0,1)*l5*sa1*sa2*sa3 + (ca1*ca2*ca3*cb*ee**2*complex(0,1)*sa1*sb)/(2.*cw**2) - ca1*ca2*ca3*cb*complex(0,1)*l1*sa1*sb - ca1*ca2*ca3*cb*complex(0,1)*l2*sa1*sb + 2*ca1*ca2*ca3*cb*complex(0,1)*l3*sa1*sb + 2*ca1*ca2*ca3*cb*complex(0,1)*l4*sa1*sb - 2*ca1*ca2*ca3*cb*complex(0,1)*l5*sa1*sb + (ca1**2*ca2*cb*ee**2*complex(0,1)*sa2*sa3*sb)/(4.*cw**2) - ca1**2*ca2*cb*complex(0,1)*l1*sa2*sa3*sb + ca1**2*ca2*cb*complex(0,1)*l3*sa2*sa3*sb + ca1**2*ca2*cb*complex(0,1)*l4*sa2*sa3*sb - ca1**2*ca2*cb*complex(0,1)*l5*sa2*sa3*sb + ca2*cb*complex(0,1)*l7*sa2*sa3*sb - ca2*cb*complex(0,1)*l8*sa2*sa3*sb - (ca2*cb*ee**2*complex(0,1)*sa1**2*sa2*sa3*sb)/(4.*cw**2) + ca2*cb*complex(0,1)*l2*sa1**2*sa2*sa3*sb - ca2*cb*complex(0,1)*l3*sa1**2*sa2*sa3*sb - ca2*cb*complex(0,1)*l4*sa1**2*sa2*sa3*sb + ca2*cb*complex(0,1)*l5*sa1**2*sa2*sa3*sb - (ca1**2*ca2*ca3*ee**2*complex(0,1)*sb**2)/(4.*cw**2) + ca1**2*ca2*ca3*complex(0,1)*l5*sb**2 - ca2*ca3*complex(0,1)*l5*sa1**2*sb**2 + (ca1*ca2*ee**2*complex(0,1)*sa1*sa2*sa3*sb**2)/(4.*cw**2) - 2*ca1*ca2*complex(0,1)*l5*sa1*sa2*sa3*sb**2 - (ca2*ca3*cb**2*ee**2*complex(0,1)*sa1**2)/(4.*sw**2) - (ca1*ca2*cb**2*ee**2*complex(0,1)*sa1*sa2*sa3)/(4.*sw**2) + (ca1*ca2*ca3*cb*ee**2*complex(0,1)*sa1*sb)/(2.*sw**2) + (ca1**2*ca2*cb*ee**2*complex(0,1)*sa2*sa3*sb)/(4.*sw**2) - (ca2*cb*ee**2*complex(0,1)*sa1**2*sa2*sa3*sb)/(4.*sw**2) - (ca1**2*ca2*ca3*ee**2*complex(0,1)*sb**2)/(4.*sw**2) + (ca1*ca2*ee**2*complex(0,1)*sa1*sa2*sa3*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_238 = Coupling(name = 'GC_238',
                  value = '-(ca1**2*ca2*ca3*cb**2*complex(0,1)*l4)/2. - (ca1**2*ca2*ca3*cb**2*complex(0,1)*l5)/2. + (ca2*ca3*cb**2*complex(0,1)*l4*sa1**2)/2. + (ca2*ca3*cb**2*complex(0,1)*l5*sa1**2)/2. + ca1*ca2*cb**2*complex(0,1)*l4*sa1*sa2*sa3 + ca1*ca2*cb**2*complex(0,1)*l5*sa1*sa2*sa3 - ca1*ca2*ca3*cb*complex(0,1)*l1*sa1*sb - ca1*ca2*ca3*cb*complex(0,1)*l2*sa1*sb + 2*ca1*ca2*ca3*cb*complex(0,1)*l3*sa1*sb - ca1**2*ca2*cb*complex(0,1)*l1*sa2*sa3*sb + ca1**2*ca2*cb*complex(0,1)*l3*sa2*sa3*sb + ca2*cb*complex(0,1)*l7*sa2*sa3*sb - ca2*cb*complex(0,1)*l8*sa2*sa3*sb + ca2*cb*complex(0,1)*l2*sa1**2*sa2*sa3*sb - ca2*cb*complex(0,1)*l3*sa1**2*sa2*sa3*sb + (ca1**2*ca2*ca3*complex(0,1)*l4*sb**2)/2. + (ca1**2*ca2*ca3*complex(0,1)*l5*sb**2)/2. - (ca2*ca3*complex(0,1)*l4*sa1**2*sb**2)/2. - (ca2*ca3*complex(0,1)*l5*sa1**2*sb**2)/2. - ca1*ca2*complex(0,1)*l4*sa1*sa2*sa3*sb**2 - ca1*ca2*complex(0,1)*l5*sa1*sa2*sa3*sb**2 + (ca1**2*ca2*ca3*cb**2*ee**2*complex(0,1))/(4.*sw**2) - (ca1*ca2*cb**2*ee**2*complex(0,1)*sa1*sa2*sa3)/(4.*sw**2) + (ca1*ca2*ca3*cb*ee**2*complex(0,1)*sa1*sb)/(2.*sw**2) + (ca1**2*ca2*cb*ee**2*complex(0,1)*sa2*sa3*sb)/(4.*sw**2) - (ca2*cb*ee**2*complex(0,1)*sa1**2*sa2*sa3*sb)/(4.*sw**2) + (ca2*ca3*ee**2*complex(0,1)*sa1**2*sb**2)/(4.*sw**2) + (ca1*ca2*ee**2*complex(0,1)*sa1*sa2*sa3*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_239 = Coupling(name = 'GC_239',
                  value = '(ca1**2*ca2*ca3*cb**2*ee**2*complex(0,1))/(4.*cw**2) - ca1**2*ca2*ca3*cb**2*complex(0,1)*l5 + ca2*ca3*cb**2*complex(0,1)*l5*sa1**2 - (ca1*ca2*cb**2*ee**2*complex(0,1)*sa1*sa2*sa3)/(4.*cw**2) + 2*ca1*ca2*cb**2*complex(0,1)*l5*sa1*sa2*sa3 + (ca1*ca2*ca3*cb*ee**2*complex(0,1)*sa1*sb)/(2.*cw**2) - ca1*ca2*ca3*cb*complex(0,1)*l1*sa1*sb - ca1*ca2*ca3*cb*complex(0,1)*l2*sa1*sb + 2*ca1*ca2*ca3*cb*complex(0,1)*l3*sa1*sb + 2*ca1*ca2*ca3*cb*complex(0,1)*l4*sa1*sb - 2*ca1*ca2*ca3*cb*complex(0,1)*l5*sa1*sb + (ca1**2*ca2*cb*ee**2*complex(0,1)*sa2*sa3*sb)/(4.*cw**2) - ca1**2*ca2*cb*complex(0,1)*l1*sa2*sa3*sb + ca1**2*ca2*cb*complex(0,1)*l3*sa2*sa3*sb + ca1**2*ca2*cb*complex(0,1)*l4*sa2*sa3*sb - ca1**2*ca2*cb*complex(0,1)*l5*sa2*sa3*sb + ca2*cb*complex(0,1)*l7*sa2*sa3*sb - ca2*cb*complex(0,1)*l8*sa2*sa3*sb - (ca2*cb*ee**2*complex(0,1)*sa1**2*sa2*sa3*sb)/(4.*cw**2) + ca2*cb*complex(0,1)*l2*sa1**2*sa2*sa3*sb - ca2*cb*complex(0,1)*l3*sa1**2*sa2*sa3*sb - ca2*cb*complex(0,1)*l4*sa1**2*sa2*sa3*sb + ca2*cb*complex(0,1)*l5*sa1**2*sa2*sa3*sb + ca1**2*ca2*ca3*complex(0,1)*l5*sb**2 + (ca2*ca3*ee**2*complex(0,1)*sa1**2*sb**2)/(4.*cw**2) - ca2*ca3*complex(0,1)*l5*sa1**2*sb**2 + (ca1*ca2*ee**2*complex(0,1)*sa1*sa2*sa3*sb**2)/(4.*cw**2) - 2*ca1*ca2*complex(0,1)*l5*sa1*sa2*sa3*sb**2 + (ca1**2*ca2*ca3*cb**2*ee**2*complex(0,1))/(4.*sw**2) - (ca1*ca2*cb**2*ee**2*complex(0,1)*sa1*sa2*sa3)/(4.*sw**2) + (ca1*ca2*ca3*cb*ee**2*complex(0,1)*sa1*sb)/(2.*sw**2) + (ca1**2*ca2*cb*ee**2*complex(0,1)*sa2*sa3*sb)/(4.*sw**2) - (ca2*cb*ee**2*complex(0,1)*sa1**2*sa2*sa3*sb)/(4.*sw**2) + (ca2*ca3*ee**2*complex(0,1)*sa1**2*sb**2)/(4.*sw**2) + (ca1*ca2*ee**2*complex(0,1)*sa1*sa2*sa3*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_240 = Coupling(name = 'GC_240',
                  value = '-(ca1**2*ca2*ca3*cb**2*complex(0,1)*l4)/2. - (ca1**2*ca2*ca3*cb**2*complex(0,1)*l5)/2. + (ca2*ca3*cb**2*complex(0,1)*l4*sa1**2)/2. + (ca2*ca3*cb**2*complex(0,1)*l5*sa1**2)/2. + ca1*ca2*cb**2*complex(0,1)*l4*sa1*sa2*sa3 + ca1*ca2*cb**2*complex(0,1)*l5*sa1*sa2*sa3 - ca1*ca2*ca3*cb*complex(0,1)*l1*sa1*sb - ca1*ca2*ca3*cb*complex(0,1)*l2*sa1*sb + 2*ca1*ca2*ca3*cb*complex(0,1)*l3*sa1*sb - ca1**2*ca2*cb*complex(0,1)*l1*sa2*sa3*sb + ca1**2*ca2*cb*complex(0,1)*l3*sa2*sa3*sb + ca2*cb*complex(0,1)*l7*sa2*sa3*sb - ca2*cb*complex(0,1)*l8*sa2*sa3*sb + ca2*cb*complex(0,1)*l2*sa1**2*sa2*sa3*sb - ca2*cb*complex(0,1)*l3*sa1**2*sa2*sa3*sb + (ca1**2*ca2*ca3*complex(0,1)*l4*sb**2)/2. + (ca1**2*ca2*ca3*complex(0,1)*l5*sb**2)/2. - (ca2*ca3*complex(0,1)*l4*sa1**2*sb**2)/2. - (ca2*ca3*complex(0,1)*l5*sa1**2*sb**2)/2. - ca1*ca2*complex(0,1)*l4*sa1*sa2*sa3*sb**2 - ca1*ca2*complex(0,1)*l5*sa1*sa2*sa3*sb**2 - (ca1**2*ca2*ca3*cb**2*ee**2*complex(0,1))/(4.*sw**2) + (ca2*ca3*cb**2*ee**2*complex(0,1)*sa1**2)/(4.*sw**2) + (ca1*ca2*cb**2*ee**2*complex(0,1)*sa1*sa2*sa3)/(2.*sw**2) - (ca1*ca2*ca3*cb*ee**2*complex(0,1)*sa1*sb)/sw**2 - (ca1**2*ca2*cb*ee**2*complex(0,1)*sa2*sa3*sb)/(2.*sw**2) + (ca2*cb*ee**2*complex(0,1)*sa1**2*sa2*sa3*sb)/(2.*sw**2) + (ca1**2*ca2*ca3*ee**2*complex(0,1)*sb**2)/(4.*sw**2) - (ca2*ca3*ee**2*complex(0,1)*sa1**2*sb**2)/(4.*sw**2) - (ca1*ca2*ee**2*complex(0,1)*sa1*sa2*sa3*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_241 = Coupling(name = 'GC_241',
                  value = '-(ca1**2*ca2*ca3*cb**2*ee**2*complex(0,1))/(4.*cw**2) - ca1**2*ca2*ca3*cb**2*complex(0,1)*l5 + (ca2*ca3*cb**2*ee**2*complex(0,1)*sa1**2)/(4.*cw**2) + ca2*ca3*cb**2*complex(0,1)*l5*sa1**2 + (ca1*ca2*cb**2*ee**2*complex(0,1)*sa1*sa2*sa3)/(2.*cw**2) + 2*ca1*ca2*cb**2*complex(0,1)*l5*sa1*sa2*sa3 - (ca1*ca2*ca3*cb*ee**2*complex(0,1)*sa1*sb)/cw**2 - ca1*ca2*ca3*cb*complex(0,1)*l1*sa1*sb - ca1*ca2*ca3*cb*complex(0,1)*l2*sa1*sb + 2*ca1*ca2*ca3*cb*complex(0,1)*l3*sa1*sb + 2*ca1*ca2*ca3*cb*complex(0,1)*l4*sa1*sb - 2*ca1*ca2*ca3*cb*complex(0,1)*l5*sa1*sb - (ca1**2*ca2*cb*ee**2*complex(0,1)*sa2*sa3*sb)/(2.*cw**2) - ca1**2*ca2*cb*complex(0,1)*l1*sa2*sa3*sb + ca1**2*ca2*cb*complex(0,1)*l3*sa2*sa3*sb + ca1**2*ca2*cb*complex(0,1)*l4*sa2*sa3*sb - ca1**2*ca2*cb*complex(0,1)*l5*sa2*sa3*sb + ca2*cb*complex(0,1)*l7*sa2*sa3*sb - ca2*cb*complex(0,1)*l8*sa2*sa3*sb + (ca2*cb*ee**2*complex(0,1)*sa1**2*sa2*sa3*sb)/(2.*cw**2) + ca2*cb*complex(0,1)*l2*sa1**2*sa2*sa3*sb - ca2*cb*complex(0,1)*l3*sa1**2*sa2*sa3*sb - ca2*cb*complex(0,1)*l4*sa1**2*sa2*sa3*sb + ca2*cb*complex(0,1)*l5*sa1**2*sa2*sa3*sb + (ca1**2*ca2*ca3*ee**2*complex(0,1)*sb**2)/(4.*cw**2) + ca1**2*ca2*ca3*complex(0,1)*l5*sb**2 - (ca2*ca3*ee**2*complex(0,1)*sa1**2*sb**2)/(4.*cw**2) - ca2*ca3*complex(0,1)*l5*sa1**2*sb**2 - (ca1*ca2*ee**2*complex(0,1)*sa1*sa2*sa3*sb**2)/(2.*cw**2) - 2*ca1*ca2*complex(0,1)*l5*sa1*sa2*sa3*sb**2 - (ca1**2*ca2*ca3*cb**2*ee**2*complex(0,1))/(4.*sw**2) + (ca2*ca3*cb**2*ee**2*complex(0,1)*sa1**2)/(4.*sw**2) + (ca1*ca2*cb**2*ee**2*complex(0,1)*sa1*sa2*sa3)/(2.*sw**2) - (ca1*ca2*ca3*cb*ee**2*complex(0,1)*sa1*sb)/sw**2 - (ca1**2*ca2*cb*ee**2*complex(0,1)*sa2*sa3*sb)/(2.*sw**2) + (ca2*cb*ee**2*complex(0,1)*sa1**2*sa2*sa3*sb)/(2.*sw**2) + (ca1**2*ca2*ca3*ee**2*complex(0,1)*sb**2)/(4.*sw**2) - (ca2*ca3*ee**2*complex(0,1)*sa1**2*sb**2)/(4.*sw**2) - (ca1*ca2*ee**2*complex(0,1)*sa1*sa2*sa3*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_242 = Coupling(name = 'GC_242',
                  value = 'ca1*ca2*ca3*cb**2*complex(0,1)*l1*sa1 - ca1*ca2*ca3*cb**2*complex(0,1)*l3*sa1 + ca1**2*ca2*cb**2*complex(0,1)*l1*sa2*sa3 - ca2*cb**2*complex(0,1)*l7*sa2*sa3 + ca2*cb**2*complex(0,1)*l3*sa1**2*sa2*sa3 - ca1**2*ca2*ca3*cb*complex(0,1)*l4*sb - ca1**2*ca2*ca3*cb*complex(0,1)*l5*sb + ca2*ca3*cb*complex(0,1)*l4*sa1**2*sb + ca2*ca3*cb*complex(0,1)*l5*sa1**2*sb + 2*ca1*ca2*cb*complex(0,1)*l4*sa1*sa2*sa3*sb + 2*ca1*ca2*cb*complex(0,1)*l5*sa1*sa2*sa3*sb - ca1*ca2*ca3*complex(0,1)*l2*sa1*sb**2 + ca1*ca2*ca3*complex(0,1)*l3*sa1*sb**2 + ca1**2*ca2*complex(0,1)*l3*sa2*sa3*sb**2 - ca2*complex(0,1)*l8*sa2*sa3*sb**2 + ca2*complex(0,1)*l2*sa1**2*sa2*sa3*sb**2 - (ca1*ca2*ca3*cb**2*ee**2*complex(0,1)*sa1)/(4.*sw**2) - (ca1**2*ca2*cb**2*ee**2*complex(0,1)*sa2*sa3)/(4.*sw**2) + (ca1**2*ca2*ca3*cb*ee**2*complex(0,1)*sb)/(4.*sw**2) - (ca2*ca3*cb*ee**2*complex(0,1)*sa1**2*sb)/(4.*sw**2) - (ca1*ca2*cb*ee**2*complex(0,1)*sa1*sa2*sa3*sb)/(2.*sw**2) + (ca1*ca2*ca3*ee**2*complex(0,1)*sa1*sb**2)/(4.*sw**2) - (ca2*ee**2*complex(0,1)*sa1**2*sa2*sa3*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_243 = Coupling(name = 'GC_243',
                  value = '-(ca1*ca2*ca3*cb**2*ee**2*complex(0,1)*sa1)/(4.*cw**2) + ca1*ca2*ca3*cb**2*complex(0,1)*l1*sa1 - ca1*ca2*ca3*cb**2*complex(0,1)*l3*sa1 - ca1*ca2*ca3*cb**2*complex(0,1)*l4*sa1 + ca1*ca2*ca3*cb**2*complex(0,1)*l5*sa1 - (ca1**2*ca2*cb**2*ee**2*complex(0,1)*sa2*sa3)/(4.*cw**2) + ca1**2*ca2*cb**2*complex(0,1)*l1*sa2*sa3 - ca2*cb**2*complex(0,1)*l7*sa2*sa3 + ca2*cb**2*complex(0,1)*l3*sa1**2*sa2*sa3 + ca2*cb**2*complex(0,1)*l4*sa1**2*sa2*sa3 - ca2*cb**2*complex(0,1)*l5*sa1**2*sa2*sa3 + (ca1**2*ca2*ca3*cb*ee**2*complex(0,1)*sb)/(4.*cw**2) - 2*ca1**2*ca2*ca3*cb*complex(0,1)*l5*sb - (ca2*ca3*cb*ee**2*complex(0,1)*sa1**2*sb)/(4.*cw**2) + 2*ca2*ca3*cb*complex(0,1)*l5*sa1**2*sb - (ca1*ca2*cb*ee**2*complex(0,1)*sa1*sa2*sa3*sb)/(2.*cw**2) + 4*ca1*ca2*cb*complex(0,1)*l5*sa1*sa2*sa3*sb + (ca1*ca2*ca3*ee**2*complex(0,1)*sa1*sb**2)/(4.*cw**2) - ca1*ca2*ca3*complex(0,1)*l2*sa1*sb**2 + ca1*ca2*ca3*complex(0,1)*l3*sa1*sb**2 + ca1*ca2*ca3*complex(0,1)*l4*sa1*sb**2 - ca1*ca2*ca3*complex(0,1)*l5*sa1*sb**2 + ca1**2*ca2*complex(0,1)*l3*sa2*sa3*sb**2 + ca1**2*ca2*complex(0,1)*l4*sa2*sa3*sb**2 - ca1**2*ca2*complex(0,1)*l5*sa2*sa3*sb**2 - ca2*complex(0,1)*l8*sa2*sa3*sb**2 - (ca2*ee**2*complex(0,1)*sa1**2*sa2*sa3*sb**2)/(4.*cw**2) + ca2*complex(0,1)*l2*sa1**2*sa2*sa3*sb**2 - (ca1*ca2*ca3*cb**2*ee**2*complex(0,1)*sa1)/(4.*sw**2) - (ca1**2*ca2*cb**2*ee**2*complex(0,1)*sa2*sa3)/(4.*sw**2) + (ca1**2*ca2*ca3*cb*ee**2*complex(0,1)*sb)/(4.*sw**2) - (ca2*ca3*cb*ee**2*complex(0,1)*sa1**2*sb)/(4.*sw**2) - (ca1*ca2*cb*ee**2*complex(0,1)*sa1*sa2*sa3*sb)/(2.*sw**2) + (ca1*ca2*ca3*ee**2*complex(0,1)*sa1*sb**2)/(4.*sw**2) - (ca2*ee**2*complex(0,1)*sa1**2*sa2*sa3*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_244 = Coupling(name = 'GC_244',
                  value = 'ca1*ca2*ca3*cb**2*complex(0,1)*l1*sa1 - ca1*ca2*ca3*cb**2*complex(0,1)*l3*sa1 + ca1**2*ca2*cb**2*complex(0,1)*l1*sa2*sa3 - ca2*cb**2*complex(0,1)*l7*sa2*sa3 + ca2*cb**2*complex(0,1)*l3*sa1**2*sa2*sa3 - ca1**2*ca2*ca3*cb*complex(0,1)*l4*sb - ca1**2*ca2*ca3*cb*complex(0,1)*l5*sb + ca2*ca3*cb*complex(0,1)*l4*sa1**2*sb + ca2*ca3*cb*complex(0,1)*l5*sa1**2*sb + 2*ca1*ca2*cb*complex(0,1)*l4*sa1*sa2*sa3*sb + 2*ca1*ca2*cb*complex(0,1)*l5*sa1*sa2*sa3*sb - ca1*ca2*ca3*complex(0,1)*l2*sa1*sb**2 + ca1*ca2*ca3*complex(0,1)*l3*sa1*sb**2 + ca1**2*ca2*complex(0,1)*l3*sa2*sa3*sb**2 - ca2*complex(0,1)*l8*sa2*sa3*sb**2 + ca2*complex(0,1)*l2*sa1**2*sa2*sa3*sb**2 + (ca1*ca2*ca3*cb**2*ee**2*complex(0,1)*sa1)/(2.*sw**2) + (ca1**2*ca2*cb**2*ee**2*complex(0,1)*sa2*sa3)/(2.*sw**2) - (ca1**2*ca2*ca3*cb*ee**2*complex(0,1)*sb)/(2.*sw**2) + (ca2*ca3*cb*ee**2*complex(0,1)*sa1**2*sb)/(2.*sw**2) + (ca1*ca2*cb*ee**2*complex(0,1)*sa1*sa2*sa3*sb)/sw**2 - (ca1*ca2*ca3*ee**2*complex(0,1)*sa1*sb**2)/(2.*sw**2) + (ca2*ee**2*complex(0,1)*sa1**2*sa2*sa3*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_245 = Coupling(name = 'GC_245',
                  value = '(ca1*ca2*ca3*cb**2*ee**2*complex(0,1)*sa1)/(2.*cw**2) + ca1*ca2*ca3*cb**2*complex(0,1)*l1*sa1 - ca1*ca2*ca3*cb**2*complex(0,1)*l3*sa1 - ca1*ca2*ca3*cb**2*complex(0,1)*l4*sa1 + ca1*ca2*ca3*cb**2*complex(0,1)*l5*sa1 + (ca1**2*ca2*cb**2*ee**2*complex(0,1)*sa2*sa3)/(2.*cw**2) + ca1**2*ca2*cb**2*complex(0,1)*l1*sa2*sa3 - ca2*cb**2*complex(0,1)*l7*sa2*sa3 + ca2*cb**2*complex(0,1)*l3*sa1**2*sa2*sa3 + ca2*cb**2*complex(0,1)*l4*sa1**2*sa2*sa3 - ca2*cb**2*complex(0,1)*l5*sa1**2*sa2*sa3 - (ca1**2*ca2*ca3*cb*ee**2*complex(0,1)*sb)/(2.*cw**2) - 2*ca1**2*ca2*ca3*cb*complex(0,1)*l5*sb + (ca2*ca3*cb*ee**2*complex(0,1)*sa1**2*sb)/(2.*cw**2) + 2*ca2*ca3*cb*complex(0,1)*l5*sa1**2*sb + (ca1*ca2*cb*ee**2*complex(0,1)*sa1*sa2*sa3*sb)/cw**2 + 4*ca1*ca2*cb*complex(0,1)*l5*sa1*sa2*sa3*sb - (ca1*ca2*ca3*ee**2*complex(0,1)*sa1*sb**2)/(2.*cw**2) - ca1*ca2*ca3*complex(0,1)*l2*sa1*sb**2 + ca1*ca2*ca3*complex(0,1)*l3*sa1*sb**2 + ca1*ca2*ca3*complex(0,1)*l4*sa1*sb**2 - ca1*ca2*ca3*complex(0,1)*l5*sa1*sb**2 + ca1**2*ca2*complex(0,1)*l3*sa2*sa3*sb**2 + ca1**2*ca2*complex(0,1)*l4*sa2*sa3*sb**2 - ca1**2*ca2*complex(0,1)*l5*sa2*sa3*sb**2 - ca2*complex(0,1)*l8*sa2*sa3*sb**2 + (ca2*ee**2*complex(0,1)*sa1**2*sa2*sa3*sb**2)/(2.*cw**2) + ca2*complex(0,1)*l2*sa1**2*sa2*sa3*sb**2 + (ca1*ca2*ca3*cb**2*ee**2*complex(0,1)*sa1)/(2.*sw**2) + (ca1**2*ca2*cb**2*ee**2*complex(0,1)*sa2*sa3)/(2.*sw**2) - (ca1**2*ca2*ca3*cb*ee**2*complex(0,1)*sb)/(2.*sw**2) + (ca2*ca3*cb*ee**2*complex(0,1)*sa1**2*sb)/(2.*sw**2) + (ca1*ca2*cb*ee**2*complex(0,1)*sa1*sa2*sa3*sb)/sw**2 - (ca1*ca2*ca3*ee**2*complex(0,1)*sa1*sb**2)/(2.*sw**2) + (ca2*ee**2*complex(0,1)*sa1**2*sa2*sa3*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_246 = Coupling(name = 'GC_246',
                  value = '-(ca2**2*ca3**2*cb**2*complex(0,1)*l7) - ca1**2*ca3**2*cb**2*complex(0,1)*l1*sa2**2 - ca3**2*cb**2*complex(0,1)*l3*sa1**2*sa2**2 + 2*ca1*ca3*cb**2*complex(0,1)*l1*sa1*sa2*sa3 - 2*ca1*ca3*cb**2*complex(0,1)*l3*sa1*sa2*sa3 - ca1**2*cb**2*complex(0,1)*l3*sa3**2 - cb**2*complex(0,1)*l1*sa1**2*sa3**2 - 2*ca1*ca3**2*cb*complex(0,1)*l4*sa1*sa2**2*sb - 2*ca1*ca3**2*cb*complex(0,1)*l5*sa1*sa2**2*sb - 2*ca1**2*ca3*cb*complex(0,1)*l4*sa2*sa3*sb - 2*ca1**2*ca3*cb*complex(0,1)*l5*sa2*sa3*sb + 2*ca3*cb*complex(0,1)*l4*sa1**2*sa2*sa3*sb + 2*ca3*cb*complex(0,1)*l5*sa1**2*sa2*sa3*sb + 2*ca1*cb*complex(0,1)*l4*sa1*sa3**2*sb + 2*ca1*cb*complex(0,1)*l5*sa1*sa3**2*sb - ca2**2*ca3**2*complex(0,1)*l8*sb**2 - ca1**2*ca3**2*complex(0,1)*l3*sa2**2*sb**2 - ca3**2*complex(0,1)*l2*sa1**2*sa2**2*sb**2 - 2*ca1*ca3*complex(0,1)*l2*sa1*sa2*sa3*sb**2 + 2*ca1*ca3*complex(0,1)*l3*sa1*sa2*sa3*sb**2 - ca1**2*complex(0,1)*l2*sa3**2*sb**2 - complex(0,1)*l3*sa1**2*sa3**2*sb**2 + (ca1**2*ca3**2*cb**2*ee**2*complex(0,1)*sa2**2)/(4.*sw**2) - (ca1*ca3*cb**2*ee**2*complex(0,1)*sa1*sa2*sa3)/(2.*sw**2) + (cb**2*ee**2*complex(0,1)*sa1**2*sa3**2)/(4.*sw**2) + (ca1*ca3**2*cb*ee**2*complex(0,1)*sa1*sa2**2*sb)/(2.*sw**2) + (ca1**2*ca3*cb*ee**2*complex(0,1)*sa2*sa3*sb)/(2.*sw**2) - (ca3*cb*ee**2*complex(0,1)*sa1**2*sa2*sa3*sb)/(2.*sw**2) - (ca1*cb*ee**2*complex(0,1)*sa1*sa3**2*sb)/(2.*sw**2) + (ca3**2*ee**2*complex(0,1)*sa1**2*sa2**2*sb**2)/(4.*sw**2) + (ca1*ca3*ee**2*complex(0,1)*sa1*sa2*sa3*sb**2)/(2.*sw**2) + (ca1**2*ee**2*complex(0,1)*sa3**2*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_247 = Coupling(name = 'GC_247',
                  value = '-(ca2**2*ca3**2*cb**2*complex(0,1)*l7) + (ca1**2*ca3**2*cb**2*ee**2*complex(0,1)*sa2**2)/(4.*cw**2) - ca1**2*ca3**2*cb**2*complex(0,1)*l1*sa2**2 - ca3**2*cb**2*complex(0,1)*l3*sa1**2*sa2**2 - ca3**2*cb**2*complex(0,1)*l4*sa1**2*sa2**2 + ca3**2*cb**2*complex(0,1)*l5*sa1**2*sa2**2 - (ca1*ca3*cb**2*ee**2*complex(0,1)*sa1*sa2*sa3)/(2.*cw**2) + 2*ca1*ca3*cb**2*complex(0,1)*l1*sa1*sa2*sa3 - 2*ca1*ca3*cb**2*complex(0,1)*l3*sa1*sa2*sa3 - 2*ca1*ca3*cb**2*complex(0,1)*l4*sa1*sa2*sa3 + 2*ca1*ca3*cb**2*complex(0,1)*l5*sa1*sa2*sa3 - ca1**2*cb**2*complex(0,1)*l3*sa3**2 - ca1**2*cb**2*complex(0,1)*l4*sa3**2 + ca1**2*cb**2*complex(0,1)*l5*sa3**2 + (cb**2*ee**2*complex(0,1)*sa1**2*sa3**2)/(4.*cw**2) - cb**2*complex(0,1)*l1*sa1**2*sa3**2 + (ca1*ca3**2*cb*ee**2*complex(0,1)*sa1*sa2**2*sb)/(2.*cw**2) - 4*ca1*ca3**2*cb*complex(0,1)*l5*sa1*sa2**2*sb + (ca1**2*ca3*cb*ee**2*complex(0,1)*sa2*sa3*sb)/(2.*cw**2) - 4*ca1**2*ca3*cb*complex(0,1)*l5*sa2*sa3*sb - (ca3*cb*ee**2*complex(0,1)*sa1**2*sa2*sa3*sb)/(2.*cw**2) + 4*ca3*cb*complex(0,1)*l5*sa1**2*sa2*sa3*sb - (ca1*cb*ee**2*complex(0,1)*sa1*sa3**2*sb)/(2.*cw**2) + 4*ca1*cb*complex(0,1)*l5*sa1*sa3**2*sb - ca2**2*ca3**2*complex(0,1)*l8*sb**2 - ca1**2*ca3**2*complex(0,1)*l3*sa2**2*sb**2 - ca1**2*ca3**2*complex(0,1)*l4*sa2**2*sb**2 + ca1**2*ca3**2*complex(0,1)*l5*sa2**2*sb**2 + (ca3**2*ee**2*complex(0,1)*sa1**2*sa2**2*sb**2)/(4.*cw**2) - ca3**2*complex(0,1)*l2*sa1**2*sa2**2*sb**2 + (ca1*ca3*ee**2*complex(0,1)*sa1*sa2*sa3*sb**2)/(2.*cw**2) - 2*ca1*ca3*complex(0,1)*l2*sa1*sa2*sa3*sb**2 + 2*ca1*ca3*complex(0,1)*l3*sa1*sa2*sa3*sb**2 + 2*ca1*ca3*complex(0,1)*l4*sa1*sa2*sa3*sb**2 - 2*ca1*ca3*complex(0,1)*l5*sa1*sa2*sa3*sb**2 + (ca1**2*ee**2*complex(0,1)*sa3**2*sb**2)/(4.*cw**2) - ca1**2*complex(0,1)*l2*sa3**2*sb**2 - complex(0,1)*l3*sa1**2*sa3**2*sb**2 - complex(0,1)*l4*sa1**2*sa3**2*sb**2 + complex(0,1)*l5*sa1**2*sa3**2*sb**2 + (ca1**2*ca3**2*cb**2*ee**2*complex(0,1)*sa2**2)/(4.*sw**2) - (ca1*ca3*cb**2*ee**2*complex(0,1)*sa1*sa2*sa3)/(2.*sw**2) + (cb**2*ee**2*complex(0,1)*sa1**2*sa3**2)/(4.*sw**2) + (ca1*ca3**2*cb*ee**2*complex(0,1)*sa1*sa2**2*sb)/(2.*sw**2) + (ca1**2*ca3*cb*ee**2*complex(0,1)*sa2*sa3*sb)/(2.*sw**2) - (ca3*cb*ee**2*complex(0,1)*sa1**2*sa2*sa3*sb)/(2.*sw**2) - (ca1*cb*ee**2*complex(0,1)*sa1*sa3**2*sb)/(2.*sw**2) + (ca3**2*ee**2*complex(0,1)*sa1**2*sa2**2*sb**2)/(4.*sw**2) + (ca1*ca3*ee**2*complex(0,1)*sa1*sa2*sa3*sb**2)/(2.*sw**2) + (ca1**2*ee**2*complex(0,1)*sa3**2*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_248 = Coupling(name = 'GC_248',
                  value = '-(ca2**2*ca3**2*cb**2*complex(0,1)*l7) - ca1**2*ca3**2*cb**2*complex(0,1)*l1*sa2**2 - ca3**2*cb**2*complex(0,1)*l3*sa1**2*sa2**2 + 2*ca1*ca3*cb**2*complex(0,1)*l1*sa1*sa2*sa3 - 2*ca1*ca3*cb**2*complex(0,1)*l3*sa1*sa2*sa3 - ca1**2*cb**2*complex(0,1)*l3*sa3**2 - cb**2*complex(0,1)*l1*sa1**2*sa3**2 - 2*ca1*ca3**2*cb*complex(0,1)*l4*sa1*sa2**2*sb - 2*ca1*ca3**2*cb*complex(0,1)*l5*sa1*sa2**2*sb - 2*ca1**2*ca3*cb*complex(0,1)*l4*sa2*sa3*sb - 2*ca1**2*ca3*cb*complex(0,1)*l5*sa2*sa3*sb + 2*ca3*cb*complex(0,1)*l4*sa1**2*sa2*sa3*sb + 2*ca3*cb*complex(0,1)*l5*sa1**2*sa2*sa3*sb + 2*ca1*cb*complex(0,1)*l4*sa1*sa3**2*sb + 2*ca1*cb*complex(0,1)*l5*sa1*sa3**2*sb - ca2**2*ca3**2*complex(0,1)*l8*sb**2 - ca1**2*ca3**2*complex(0,1)*l3*sa2**2*sb**2 - ca3**2*complex(0,1)*l2*sa1**2*sa2**2*sb**2 - 2*ca1*ca3*complex(0,1)*l2*sa1*sa2*sa3*sb**2 + 2*ca1*ca3*complex(0,1)*l3*sa1*sa2*sa3*sb**2 - ca1**2*complex(0,1)*l2*sa3**2*sb**2 - complex(0,1)*l3*sa1**2*sa3**2*sb**2 - (ca1**2*ca3**2*cb**2*ee**2*complex(0,1)*sa2**2)/(2.*sw**2) + (ca1*ca3*cb**2*ee**2*complex(0,1)*sa1*sa2*sa3)/sw**2 - (cb**2*ee**2*complex(0,1)*sa1**2*sa3**2)/(2.*sw**2) - (ca1*ca3**2*cb*ee**2*complex(0,1)*sa1*sa2**2*sb)/sw**2 - (ca1**2*ca3*cb*ee**2*complex(0,1)*sa2*sa3*sb)/sw**2 + (ca3*cb*ee**2*complex(0,1)*sa1**2*sa2*sa3*sb)/sw**2 + (ca1*cb*ee**2*complex(0,1)*sa1*sa3**2*sb)/sw**2 - (ca3**2*ee**2*complex(0,1)*sa1**2*sa2**2*sb**2)/(2.*sw**2) - (ca1*ca3*ee**2*complex(0,1)*sa1*sa2*sa3*sb**2)/sw**2 - (ca1**2*ee**2*complex(0,1)*sa3**2*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_249 = Coupling(name = 'GC_249',
                  value = '-(ca2**2*ca3**2*cb**2*complex(0,1)*l7) - (ca1**2*ca3**2*cb**2*ee**2*complex(0,1)*sa2**2)/(2.*cw**2) - ca1**2*ca3**2*cb**2*complex(0,1)*l1*sa2**2 - ca3**2*cb**2*complex(0,1)*l3*sa1**2*sa2**2 - ca3**2*cb**2*complex(0,1)*l4*sa1**2*sa2**2 + ca3**2*cb**2*complex(0,1)*l5*sa1**2*sa2**2 + (ca1*ca3*cb**2*ee**2*complex(0,1)*sa1*sa2*sa3)/cw**2 + 2*ca1*ca3*cb**2*complex(0,1)*l1*sa1*sa2*sa3 - 2*ca1*ca3*cb**2*complex(0,1)*l3*sa1*sa2*sa3 - 2*ca1*ca3*cb**2*complex(0,1)*l4*sa1*sa2*sa3 + 2*ca1*ca3*cb**2*complex(0,1)*l5*sa1*sa2*sa3 - ca1**2*cb**2*complex(0,1)*l3*sa3**2 - ca1**2*cb**2*complex(0,1)*l4*sa3**2 + ca1**2*cb**2*complex(0,1)*l5*sa3**2 - (cb**2*ee**2*complex(0,1)*sa1**2*sa3**2)/(2.*cw**2) - cb**2*complex(0,1)*l1*sa1**2*sa3**2 - (ca1*ca3**2*cb*ee**2*complex(0,1)*sa1*sa2**2*sb)/cw**2 - 4*ca1*ca3**2*cb*complex(0,1)*l5*sa1*sa2**2*sb - (ca1**2*ca3*cb*ee**2*complex(0,1)*sa2*sa3*sb)/cw**2 - 4*ca1**2*ca3*cb*complex(0,1)*l5*sa2*sa3*sb + (ca3*cb*ee**2*complex(0,1)*sa1**2*sa2*sa3*sb)/cw**2 + 4*ca3*cb*complex(0,1)*l5*sa1**2*sa2*sa3*sb + (ca1*cb*ee**2*complex(0,1)*sa1*sa3**2*sb)/cw**2 + 4*ca1*cb*complex(0,1)*l5*sa1*sa3**2*sb - ca2**2*ca3**2*complex(0,1)*l8*sb**2 - ca1**2*ca3**2*complex(0,1)*l3*sa2**2*sb**2 - ca1**2*ca3**2*complex(0,1)*l4*sa2**2*sb**2 + ca1**2*ca3**2*complex(0,1)*l5*sa2**2*sb**2 - (ca3**2*ee**2*complex(0,1)*sa1**2*sa2**2*sb**2)/(2.*cw**2) - ca3**2*complex(0,1)*l2*sa1**2*sa2**2*sb**2 - (ca1*ca3*ee**2*complex(0,1)*sa1*sa2*sa3*sb**2)/cw**2 - 2*ca1*ca3*complex(0,1)*l2*sa1*sa2*sa3*sb**2 + 2*ca1*ca3*complex(0,1)*l3*sa1*sa2*sa3*sb**2 + 2*ca1*ca3*complex(0,1)*l4*sa1*sa2*sa3*sb**2 - 2*ca1*ca3*complex(0,1)*l5*sa1*sa2*sa3*sb**2 - (ca1**2*ee**2*complex(0,1)*sa3**2*sb**2)/(2.*cw**2) - ca1**2*complex(0,1)*l2*sa3**2*sb**2 - complex(0,1)*l3*sa1**2*sa3**2*sb**2 - complex(0,1)*l4*sa1**2*sa3**2*sb**2 + complex(0,1)*l5*sa1**2*sa3**2*sb**2 - (ca1**2*ca3**2*cb**2*ee**2*complex(0,1)*sa2**2)/(2.*sw**2) + (ca1*ca3*cb**2*ee**2*complex(0,1)*sa1*sa2*sa3)/sw**2 - (cb**2*ee**2*complex(0,1)*sa1**2*sa3**2)/(2.*sw**2) - (ca1*ca3**2*cb*ee**2*complex(0,1)*sa1*sa2**2*sb)/sw**2 - (ca1**2*ca3*cb*ee**2*complex(0,1)*sa2*sa3*sb)/sw**2 + (ca3*cb*ee**2*complex(0,1)*sa1**2*sa2*sa3*sb)/sw**2 + (ca1*cb*ee**2*complex(0,1)*sa1*sa3**2*sb)/sw**2 - (ca3**2*ee**2*complex(0,1)*sa1**2*sa2**2*sb**2)/(2.*sw**2) - (ca1*ca3*ee**2*complex(0,1)*sa1*sa2*sa3*sb**2)/sw**2 - (ca1**2*ee**2*complex(0,1)*sa3**2*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_250 = Coupling(name = 'GC_250',
                  value = '-(ca1*ca3**2*cb**2*complex(0,1)*l4*sa1*sa2**2) - ca1*ca3**2*cb**2*complex(0,1)*l5*sa1*sa2**2 - ca1**2*ca3*cb**2*complex(0,1)*l4*sa2*sa3 - ca1**2*ca3*cb**2*complex(0,1)*l5*sa2*sa3 + ca3*cb**2*complex(0,1)*l4*sa1**2*sa2*sa3 + ca3*cb**2*complex(0,1)*l5*sa1**2*sa2*sa3 + ca1*cb**2*complex(0,1)*l4*sa1*sa3**2 + ca1*cb**2*complex(0,1)*l5*sa1*sa3**2 + ca2**2*ca3**2*cb*complex(0,1)*l7*sb - ca2**2*ca3**2*cb*complex(0,1)*l8*sb + ca1**2*ca3**2*cb*complex(0,1)*l1*sa2**2*sb - ca1**2*ca3**2*cb*complex(0,1)*l3*sa2**2*sb - ca3**2*cb*complex(0,1)*l2*sa1**2*sa2**2*sb + ca3**2*cb*complex(0,1)*l3*sa1**2*sa2**2*sb - 2*ca1*ca3*cb*complex(0,1)*l1*sa1*sa2*sa3*sb - 2*ca1*ca3*cb*complex(0,1)*l2*sa1*sa2*sa3*sb + 4*ca1*ca3*cb*complex(0,1)*l3*sa1*sa2*sa3*sb - ca1**2*cb*complex(0,1)*l2*sa3**2*sb + ca1**2*cb*complex(0,1)*l3*sa3**2*sb + cb*complex(0,1)*l1*sa1**2*sa3**2*sb - cb*complex(0,1)*l3*sa1**2*sa3**2*sb + ca1*ca3**2*complex(0,1)*l4*sa1*sa2**2*sb**2 + ca1*ca3**2*complex(0,1)*l5*sa1*sa2**2*sb**2 + ca1**2*ca3*complex(0,1)*l4*sa2*sa3*sb**2 + ca1**2*ca3*complex(0,1)*l5*sa2*sa3*sb**2 - ca3*complex(0,1)*l4*sa1**2*sa2*sa3*sb**2 - ca3*complex(0,1)*l5*sa1**2*sa2*sa3*sb**2 - ca1*complex(0,1)*l4*sa1*sa3**2*sb**2 - ca1*complex(0,1)*l5*sa1*sa3**2*sb**2 + (ca1*ca3**2*cb**2*ee**2*complex(0,1)*sa1*sa2**2)/(4.*sw**2) + (ca1**2*ca3*cb**2*ee**2*complex(0,1)*sa2*sa3)/(4.*sw**2) - (ca3*cb**2*ee**2*complex(0,1)*sa1**2*sa2*sa3)/(4.*sw**2) - (ca1*cb**2*ee**2*complex(0,1)*sa1*sa3**2)/(4.*sw**2) - (ca1**2*ca3**2*cb*ee**2*complex(0,1)*sa2**2*sb)/(4.*sw**2) + (ca3**2*cb*ee**2*complex(0,1)*sa1**2*sa2**2*sb)/(4.*sw**2) + (ca1*ca3*cb*ee**2*complex(0,1)*sa1*sa2*sa3*sb)/sw**2 + (ca1**2*cb*ee**2*complex(0,1)*sa3**2*sb)/(4.*sw**2) - (cb*ee**2*complex(0,1)*sa1**2*sa3**2*sb)/(4.*sw**2) - (ca1*ca3**2*ee**2*complex(0,1)*sa1*sa2**2*sb**2)/(4.*sw**2) - (ca1**2*ca3*ee**2*complex(0,1)*sa2*sa3*sb**2)/(4.*sw**2) + (ca3*ee**2*complex(0,1)*sa1**2*sa2*sa3*sb**2)/(4.*sw**2) + (ca1*ee**2*complex(0,1)*sa1*sa3**2*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_251 = Coupling(name = 'GC_251',
                  value = '(ca1*ca3**2*cb**2*ee**2*complex(0,1)*sa1*sa2**2)/(4.*cw**2) - 2*ca1*ca3**2*cb**2*complex(0,1)*l5*sa1*sa2**2 + (ca1**2*ca3*cb**2*ee**2*complex(0,1)*sa2*sa3)/(4.*cw**2) - 2*ca1**2*ca3*cb**2*complex(0,1)*l5*sa2*sa3 - (ca3*cb**2*ee**2*complex(0,1)*sa1**2*sa2*sa3)/(4.*cw**2) + 2*ca3*cb**2*complex(0,1)*l5*sa1**2*sa2*sa3 - (ca1*cb**2*ee**2*complex(0,1)*sa1*sa3**2)/(4.*cw**2) + 2*ca1*cb**2*complex(0,1)*l5*sa1*sa3**2 + ca2**2*ca3**2*cb*complex(0,1)*l7*sb - ca2**2*ca3**2*cb*complex(0,1)*l8*sb - (ca1**2*ca3**2*cb*ee**2*complex(0,1)*sa2**2*sb)/(4.*cw**2) + ca1**2*ca3**2*cb*complex(0,1)*l1*sa2**2*sb - ca1**2*ca3**2*cb*complex(0,1)*l3*sa2**2*sb - ca1**2*ca3**2*cb*complex(0,1)*l4*sa2**2*sb + ca1**2*ca3**2*cb*complex(0,1)*l5*sa2**2*sb + (ca3**2*cb*ee**2*complex(0,1)*sa1**2*sa2**2*sb)/(4.*cw**2) - ca3**2*cb*complex(0,1)*l2*sa1**2*sa2**2*sb + ca3**2*cb*complex(0,1)*l3*sa1**2*sa2**2*sb + ca3**2*cb*complex(0,1)*l4*sa1**2*sa2**2*sb - ca3**2*cb*complex(0,1)*l5*sa1**2*sa2**2*sb + (ca1*ca3*cb*ee**2*complex(0,1)*sa1*sa2*sa3*sb)/cw**2 - 2*ca1*ca3*cb*complex(0,1)*l1*sa1*sa2*sa3*sb - 2*ca1*ca3*cb*complex(0,1)*l2*sa1*sa2*sa3*sb + 4*ca1*ca3*cb*complex(0,1)*l3*sa1*sa2*sa3*sb + 4*ca1*ca3*cb*complex(0,1)*l4*sa1*sa2*sa3*sb - 4*ca1*ca3*cb*complex(0,1)*l5*sa1*sa2*sa3*sb + (ca1**2*cb*ee**2*complex(0,1)*sa3**2*sb)/(4.*cw**2) - ca1**2*cb*complex(0,1)*l2*sa3**2*sb + ca1**2*cb*complex(0,1)*l3*sa3**2*sb + ca1**2*cb*complex(0,1)*l4*sa3**2*sb - ca1**2*cb*complex(0,1)*l5*sa3**2*sb - (cb*ee**2*complex(0,1)*sa1**2*sa3**2*sb)/(4.*cw**2) + cb*complex(0,1)*l1*sa1**2*sa3**2*sb - cb*complex(0,1)*l3*sa1**2*sa3**2*sb - cb*complex(0,1)*l4*sa1**2*sa3**2*sb + cb*complex(0,1)*l5*sa1**2*sa3**2*sb - (ca1*ca3**2*ee**2*complex(0,1)*sa1*sa2**2*sb**2)/(4.*cw**2) + 2*ca1*ca3**2*complex(0,1)*l5*sa1*sa2**2*sb**2 - (ca1**2*ca3*ee**2*complex(0,1)*sa2*sa3*sb**2)/(4.*cw**2) + 2*ca1**2*ca3*complex(0,1)*l5*sa2*sa3*sb**2 + (ca3*ee**2*complex(0,1)*sa1**2*sa2*sa3*sb**2)/(4.*cw**2) - 2*ca3*complex(0,1)*l5*sa1**2*sa2*sa3*sb**2 + (ca1*ee**2*complex(0,1)*sa1*sa3**2*sb**2)/(4.*cw**2) - 2*ca1*complex(0,1)*l5*sa1*sa3**2*sb**2 + (ca1*ca3**2*cb**2*ee**2*complex(0,1)*sa1*sa2**2)/(4.*sw**2) + (ca1**2*ca3*cb**2*ee**2*complex(0,1)*sa2*sa3)/(4.*sw**2) - (ca3*cb**2*ee**2*complex(0,1)*sa1**2*sa2*sa3)/(4.*sw**2) - (ca1*cb**2*ee**2*complex(0,1)*sa1*sa3**2)/(4.*sw**2) - (ca1**2*ca3**2*cb*ee**2*complex(0,1)*sa2**2*sb)/(4.*sw**2) + (ca3**2*cb*ee**2*complex(0,1)*sa1**2*sa2**2*sb)/(4.*sw**2) + (ca1*ca3*cb*ee**2*complex(0,1)*sa1*sa2*sa3*sb)/sw**2 + (ca1**2*cb*ee**2*complex(0,1)*sa3**2*sb)/(4.*sw**2) - (cb*ee**2*complex(0,1)*sa1**2*sa3**2*sb)/(4.*sw**2) - (ca1*ca3**2*ee**2*complex(0,1)*sa1*sa2**2*sb**2)/(4.*sw**2) - (ca1**2*ca3*ee**2*complex(0,1)*sa2*sa3*sb**2)/(4.*sw**2) + (ca3*ee**2*complex(0,1)*sa1**2*sa2*sa3*sb**2)/(4.*sw**2) + (ca1*ee**2*complex(0,1)*sa1*sa3**2*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_252 = Coupling(name = 'GC_252',
                  value = '-(ca1*ca3**2*cb**2*complex(0,1)*l4*sa1*sa2**2) - ca1*ca3**2*cb**2*complex(0,1)*l5*sa1*sa2**2 - ca1**2*ca3*cb**2*complex(0,1)*l4*sa2*sa3 - ca1**2*ca3*cb**2*complex(0,1)*l5*sa2*sa3 + ca3*cb**2*complex(0,1)*l4*sa1**2*sa2*sa3 + ca3*cb**2*complex(0,1)*l5*sa1**2*sa2*sa3 + ca1*cb**2*complex(0,1)*l4*sa1*sa3**2 + ca1*cb**2*complex(0,1)*l5*sa1*sa3**2 + ca2**2*ca3**2*cb*complex(0,1)*l7*sb - ca2**2*ca3**2*cb*complex(0,1)*l8*sb + ca1**2*ca3**2*cb*complex(0,1)*l1*sa2**2*sb - ca1**2*ca3**2*cb*complex(0,1)*l3*sa2**2*sb - ca3**2*cb*complex(0,1)*l2*sa1**2*sa2**2*sb + ca3**2*cb*complex(0,1)*l3*sa1**2*sa2**2*sb - 2*ca1*ca3*cb*complex(0,1)*l1*sa1*sa2*sa3*sb - 2*ca1*ca3*cb*complex(0,1)*l2*sa1*sa2*sa3*sb + 4*ca1*ca3*cb*complex(0,1)*l3*sa1*sa2*sa3*sb - ca1**2*cb*complex(0,1)*l2*sa3**2*sb + ca1**2*cb*complex(0,1)*l3*sa3**2*sb + cb*complex(0,1)*l1*sa1**2*sa3**2*sb - cb*complex(0,1)*l3*sa1**2*sa3**2*sb + ca1*ca3**2*complex(0,1)*l4*sa1*sa2**2*sb**2 + ca1*ca3**2*complex(0,1)*l5*sa1*sa2**2*sb**2 + ca1**2*ca3*complex(0,1)*l4*sa2*sa3*sb**2 + ca1**2*ca3*complex(0,1)*l5*sa2*sa3*sb**2 - ca3*complex(0,1)*l4*sa1**2*sa2*sa3*sb**2 - ca3*complex(0,1)*l5*sa1**2*sa2*sa3*sb**2 - ca1*complex(0,1)*l4*sa1*sa3**2*sb**2 - ca1*complex(0,1)*l5*sa1*sa3**2*sb**2 - (ca1*ca3**2*cb**2*ee**2*complex(0,1)*sa1*sa2**2)/(2.*sw**2) - (ca1**2*ca3*cb**2*ee**2*complex(0,1)*sa2*sa3)/(2.*sw**2) + (ca3*cb**2*ee**2*complex(0,1)*sa1**2*sa2*sa3)/(2.*sw**2) + (ca1*cb**2*ee**2*complex(0,1)*sa1*sa3**2)/(2.*sw**2) + (ca1**2*ca3**2*cb*ee**2*complex(0,1)*sa2**2*sb)/(2.*sw**2) - (ca3**2*cb*ee**2*complex(0,1)*sa1**2*sa2**2*sb)/(2.*sw**2) - (2*ca1*ca3*cb*ee**2*complex(0,1)*sa1*sa2*sa3*sb)/sw**2 - (ca1**2*cb*ee**2*complex(0,1)*sa3**2*sb)/(2.*sw**2) + (cb*ee**2*complex(0,1)*sa1**2*sa3**2*sb)/(2.*sw**2) + (ca1*ca3**2*ee**2*complex(0,1)*sa1*sa2**2*sb**2)/(2.*sw**2) + (ca1**2*ca3*ee**2*complex(0,1)*sa2*sa3*sb**2)/(2.*sw**2) - (ca3*ee**2*complex(0,1)*sa1**2*sa2*sa3*sb**2)/(2.*sw**2) - (ca1*ee**2*complex(0,1)*sa1*sa3**2*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_253 = Coupling(name = 'GC_253',
                  value = '-(ca1*ca3**2*cb**2*ee**2*complex(0,1)*sa1*sa2**2)/(2.*cw**2) - 2*ca1*ca3**2*cb**2*complex(0,1)*l5*sa1*sa2**2 - (ca1**2*ca3*cb**2*ee**2*complex(0,1)*sa2*sa3)/(2.*cw**2) - 2*ca1**2*ca3*cb**2*complex(0,1)*l5*sa2*sa3 + (ca3*cb**2*ee**2*complex(0,1)*sa1**2*sa2*sa3)/(2.*cw**2) + 2*ca3*cb**2*complex(0,1)*l5*sa1**2*sa2*sa3 + (ca1*cb**2*ee**2*complex(0,1)*sa1*sa3**2)/(2.*cw**2) + 2*ca1*cb**2*complex(0,1)*l5*sa1*sa3**2 + ca2**2*ca3**2*cb*complex(0,1)*l7*sb - ca2**2*ca3**2*cb*complex(0,1)*l8*sb + (ca1**2*ca3**2*cb*ee**2*complex(0,1)*sa2**2*sb)/(2.*cw**2) + ca1**2*ca3**2*cb*complex(0,1)*l1*sa2**2*sb - ca1**2*ca3**2*cb*complex(0,1)*l3*sa2**2*sb - ca1**2*ca3**2*cb*complex(0,1)*l4*sa2**2*sb + ca1**2*ca3**2*cb*complex(0,1)*l5*sa2**2*sb - (ca3**2*cb*ee**2*complex(0,1)*sa1**2*sa2**2*sb)/(2.*cw**2) - ca3**2*cb*complex(0,1)*l2*sa1**2*sa2**2*sb + ca3**2*cb*complex(0,1)*l3*sa1**2*sa2**2*sb + ca3**2*cb*complex(0,1)*l4*sa1**2*sa2**2*sb - ca3**2*cb*complex(0,1)*l5*sa1**2*sa2**2*sb - (2*ca1*ca3*cb*ee**2*complex(0,1)*sa1*sa2*sa3*sb)/cw**2 - 2*ca1*ca3*cb*complex(0,1)*l1*sa1*sa2*sa3*sb - 2*ca1*ca3*cb*complex(0,1)*l2*sa1*sa2*sa3*sb + 4*ca1*ca3*cb*complex(0,1)*l3*sa1*sa2*sa3*sb + 4*ca1*ca3*cb*complex(0,1)*l4*sa1*sa2*sa3*sb - 4*ca1*ca3*cb*complex(0,1)*l5*sa1*sa2*sa3*sb - (ca1**2*cb*ee**2*complex(0,1)*sa3**2*sb)/(2.*cw**2) - ca1**2*cb*complex(0,1)*l2*sa3**2*sb + ca1**2*cb*complex(0,1)*l3*sa3**2*sb + ca1**2*cb*complex(0,1)*l4*sa3**2*sb - ca1**2*cb*complex(0,1)*l5*sa3**2*sb + (cb*ee**2*complex(0,1)*sa1**2*sa3**2*sb)/(2.*cw**2) + cb*complex(0,1)*l1*sa1**2*sa3**2*sb - cb*complex(0,1)*l3*sa1**2*sa3**2*sb - cb*complex(0,1)*l4*sa1**2*sa3**2*sb + cb*complex(0,1)*l5*sa1**2*sa3**2*sb + (ca1*ca3**2*ee**2*complex(0,1)*sa1*sa2**2*sb**2)/(2.*cw**2) + 2*ca1*ca3**2*complex(0,1)*l5*sa1*sa2**2*sb**2 + (ca1**2*ca3*ee**2*complex(0,1)*sa2*sa3*sb**2)/(2.*cw**2) + 2*ca1**2*ca3*complex(0,1)*l5*sa2*sa3*sb**2 - (ca3*ee**2*complex(0,1)*sa1**2*sa2*sa3*sb**2)/(2.*cw**2) - 2*ca3*complex(0,1)*l5*sa1**2*sa2*sa3*sb**2 - (ca1*ee**2*complex(0,1)*sa1*sa3**2*sb**2)/(2.*cw**2) - 2*ca1*complex(0,1)*l5*sa1*sa3**2*sb**2 - (ca1*ca3**2*cb**2*ee**2*complex(0,1)*sa1*sa2**2)/(2.*sw**2) - (ca1**2*ca3*cb**2*ee**2*complex(0,1)*sa2*sa3)/(2.*sw**2) + (ca3*cb**2*ee**2*complex(0,1)*sa1**2*sa2*sa3)/(2.*sw**2) + (ca1*cb**2*ee**2*complex(0,1)*sa1*sa3**2)/(2.*sw**2) + (ca1**2*ca3**2*cb*ee**2*complex(0,1)*sa2**2*sb)/(2.*sw**2) - (ca3**2*cb*ee**2*complex(0,1)*sa1**2*sa2**2*sb)/(2.*sw**2) - (2*ca1*ca3*cb*ee**2*complex(0,1)*sa1*sa2*sa3*sb)/sw**2 - (ca1**2*cb*ee**2*complex(0,1)*sa3**2*sb)/(2.*sw**2) + (cb*ee**2*complex(0,1)*sa1**2*sa3**2*sb)/(2.*sw**2) + (ca1*ca3**2*ee**2*complex(0,1)*sa1*sa2**2*sb**2)/(2.*sw**2) + (ca1**2*ca3*ee**2*complex(0,1)*sa2*sa3*sb**2)/(2.*sw**2) - (ca3*ee**2*complex(0,1)*sa1**2*sa2*sa3*sb**2)/(2.*sw**2) - (ca1*ee**2*complex(0,1)*sa1*sa3**2*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_254 = Coupling(name = 'GC_254',
                  value = '-(ca2**2*ca3**2*cb**2*complex(0,1)*l8) - ca1**2*ca3**2*cb**2*complex(0,1)*l3*sa2**2 - ca3**2*cb**2*complex(0,1)*l2*sa1**2*sa2**2 - 2*ca1*ca3*cb**2*complex(0,1)*l2*sa1*sa2*sa3 + 2*ca1*ca3*cb**2*complex(0,1)*l3*sa1*sa2*sa3 - ca1**2*cb**2*complex(0,1)*l2*sa3**2 - cb**2*complex(0,1)*l3*sa1**2*sa3**2 + 2*ca1*ca3**2*cb*complex(0,1)*l4*sa1*sa2**2*sb + 2*ca1*ca3**2*cb*complex(0,1)*l5*sa1*sa2**2*sb + 2*ca1**2*ca3*cb*complex(0,1)*l4*sa2*sa3*sb + 2*ca1**2*ca3*cb*complex(0,1)*l5*sa2*sa3*sb - 2*ca3*cb*complex(0,1)*l4*sa1**2*sa2*sa3*sb - 2*ca3*cb*complex(0,1)*l5*sa1**2*sa2*sa3*sb - 2*ca1*cb*complex(0,1)*l4*sa1*sa3**2*sb - 2*ca1*cb*complex(0,1)*l5*sa1*sa3**2*sb - ca2**2*ca3**2*complex(0,1)*l7*sb**2 - ca1**2*ca3**2*complex(0,1)*l1*sa2**2*sb**2 - ca3**2*complex(0,1)*l3*sa1**2*sa2**2*sb**2 + 2*ca1*ca3*complex(0,1)*l1*sa1*sa2*sa3*sb**2 - 2*ca1*ca3*complex(0,1)*l3*sa1*sa2*sa3*sb**2 - ca1**2*complex(0,1)*l3*sa3**2*sb**2 - complex(0,1)*l1*sa1**2*sa3**2*sb**2 + (ca3**2*cb**2*ee**2*complex(0,1)*sa1**2*sa2**2)/(4.*sw**2) + (ca1*ca3*cb**2*ee**2*complex(0,1)*sa1*sa2*sa3)/(2.*sw**2) + (ca1**2*cb**2*ee**2*complex(0,1)*sa3**2)/(4.*sw**2) - (ca1*ca3**2*cb*ee**2*complex(0,1)*sa1*sa2**2*sb)/(2.*sw**2) - (ca1**2*ca3*cb*ee**2*complex(0,1)*sa2*sa3*sb)/(2.*sw**2) + (ca3*cb*ee**2*complex(0,1)*sa1**2*sa2*sa3*sb)/(2.*sw**2) + (ca1*cb*ee**2*complex(0,1)*sa1*sa3**2*sb)/(2.*sw**2) + (ca1**2*ca3**2*ee**2*complex(0,1)*sa2**2*sb**2)/(4.*sw**2) - (ca1*ca3*ee**2*complex(0,1)*sa1*sa2*sa3*sb**2)/(2.*sw**2) + (ee**2*complex(0,1)*sa1**2*sa3**2*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_255 = Coupling(name = 'GC_255',
                  value = '-(ca2**2*ca3**2*cb**2*complex(0,1)*l8) - ca1**2*ca3**2*cb**2*complex(0,1)*l3*sa2**2 - ca1**2*ca3**2*cb**2*complex(0,1)*l4*sa2**2 + ca1**2*ca3**2*cb**2*complex(0,1)*l5*sa2**2 + (ca3**2*cb**2*ee**2*complex(0,1)*sa1**2*sa2**2)/(4.*cw**2) - ca3**2*cb**2*complex(0,1)*l2*sa1**2*sa2**2 + (ca1*ca3*cb**2*ee**2*complex(0,1)*sa1*sa2*sa3)/(2.*cw**2) - 2*ca1*ca3*cb**2*complex(0,1)*l2*sa1*sa2*sa3 + 2*ca1*ca3*cb**2*complex(0,1)*l3*sa1*sa2*sa3 + 2*ca1*ca3*cb**2*complex(0,1)*l4*sa1*sa2*sa3 - 2*ca1*ca3*cb**2*complex(0,1)*l5*sa1*sa2*sa3 + (ca1**2*cb**2*ee**2*complex(0,1)*sa3**2)/(4.*cw**2) - ca1**2*cb**2*complex(0,1)*l2*sa3**2 - cb**2*complex(0,1)*l3*sa1**2*sa3**2 - cb**2*complex(0,1)*l4*sa1**2*sa3**2 + cb**2*complex(0,1)*l5*sa1**2*sa3**2 - (ca1*ca3**2*cb*ee**2*complex(0,1)*sa1*sa2**2*sb)/(2.*cw**2) + 4*ca1*ca3**2*cb*complex(0,1)*l5*sa1*sa2**2*sb - (ca1**2*ca3*cb*ee**2*complex(0,1)*sa2*sa3*sb)/(2.*cw**2) + 4*ca1**2*ca3*cb*complex(0,1)*l5*sa2*sa3*sb + (ca3*cb*ee**2*complex(0,1)*sa1**2*sa2*sa3*sb)/(2.*cw**2) - 4*ca3*cb*complex(0,1)*l5*sa1**2*sa2*sa3*sb + (ca1*cb*ee**2*complex(0,1)*sa1*sa3**2*sb)/(2.*cw**2) - 4*ca1*cb*complex(0,1)*l5*sa1*sa3**2*sb - ca2**2*ca3**2*complex(0,1)*l7*sb**2 + (ca1**2*ca3**2*ee**2*complex(0,1)*sa2**2*sb**2)/(4.*cw**2) - ca1**2*ca3**2*complex(0,1)*l1*sa2**2*sb**2 - ca3**2*complex(0,1)*l3*sa1**2*sa2**2*sb**2 - ca3**2*complex(0,1)*l4*sa1**2*sa2**2*sb**2 + ca3**2*complex(0,1)*l5*sa1**2*sa2**2*sb**2 - (ca1*ca3*ee**2*complex(0,1)*sa1*sa2*sa3*sb**2)/(2.*cw**2) + 2*ca1*ca3*complex(0,1)*l1*sa1*sa2*sa3*sb**2 - 2*ca1*ca3*complex(0,1)*l3*sa1*sa2*sa3*sb**2 - 2*ca1*ca3*complex(0,1)*l4*sa1*sa2*sa3*sb**2 + 2*ca1*ca3*complex(0,1)*l5*sa1*sa2*sa3*sb**2 - ca1**2*complex(0,1)*l3*sa3**2*sb**2 - ca1**2*complex(0,1)*l4*sa3**2*sb**2 + ca1**2*complex(0,1)*l5*sa3**2*sb**2 + (ee**2*complex(0,1)*sa1**2*sa3**2*sb**2)/(4.*cw**2) - complex(0,1)*l1*sa1**2*sa3**2*sb**2 + (ca3**2*cb**2*ee**2*complex(0,1)*sa1**2*sa2**2)/(4.*sw**2) + (ca1*ca3*cb**2*ee**2*complex(0,1)*sa1*sa2*sa3)/(2.*sw**2) + (ca1**2*cb**2*ee**2*complex(0,1)*sa3**2)/(4.*sw**2) - (ca1*ca3**2*cb*ee**2*complex(0,1)*sa1*sa2**2*sb)/(2.*sw**2) - (ca1**2*ca3*cb*ee**2*complex(0,1)*sa2*sa3*sb)/(2.*sw**2) + (ca3*cb*ee**2*complex(0,1)*sa1**2*sa2*sa3*sb)/(2.*sw**2) + (ca1*cb*ee**2*complex(0,1)*sa1*sa3**2*sb)/(2.*sw**2) + (ca1**2*ca3**2*ee**2*complex(0,1)*sa2**2*sb**2)/(4.*sw**2) - (ca1*ca3*ee**2*complex(0,1)*sa1*sa2*sa3*sb**2)/(2.*sw**2) + (ee**2*complex(0,1)*sa1**2*sa3**2*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_256 = Coupling(name = 'GC_256',
                  value = '-(ca2**2*ca3**2*cb**2*complex(0,1)*l8) - ca1**2*ca3**2*cb**2*complex(0,1)*l3*sa2**2 - ca3**2*cb**2*complex(0,1)*l2*sa1**2*sa2**2 - 2*ca1*ca3*cb**2*complex(0,1)*l2*sa1*sa2*sa3 + 2*ca1*ca3*cb**2*complex(0,1)*l3*sa1*sa2*sa3 - ca1**2*cb**2*complex(0,1)*l2*sa3**2 - cb**2*complex(0,1)*l3*sa1**2*sa3**2 + 2*ca1*ca3**2*cb*complex(0,1)*l4*sa1*sa2**2*sb + 2*ca1*ca3**2*cb*complex(0,1)*l5*sa1*sa2**2*sb + 2*ca1**2*ca3*cb*complex(0,1)*l4*sa2*sa3*sb + 2*ca1**2*ca3*cb*complex(0,1)*l5*sa2*sa3*sb - 2*ca3*cb*complex(0,1)*l4*sa1**2*sa2*sa3*sb - 2*ca3*cb*complex(0,1)*l5*sa1**2*sa2*sa3*sb - 2*ca1*cb*complex(0,1)*l4*sa1*sa3**2*sb - 2*ca1*cb*complex(0,1)*l5*sa1*sa3**2*sb - ca2**2*ca3**2*complex(0,1)*l7*sb**2 - ca1**2*ca3**2*complex(0,1)*l1*sa2**2*sb**2 - ca3**2*complex(0,1)*l3*sa1**2*sa2**2*sb**2 + 2*ca1*ca3*complex(0,1)*l1*sa1*sa2*sa3*sb**2 - 2*ca1*ca3*complex(0,1)*l3*sa1*sa2*sa3*sb**2 - ca1**2*complex(0,1)*l3*sa3**2*sb**2 - complex(0,1)*l1*sa1**2*sa3**2*sb**2 - (ca3**2*cb**2*ee**2*complex(0,1)*sa1**2*sa2**2)/(2.*sw**2) - (ca1*ca3*cb**2*ee**2*complex(0,1)*sa1*sa2*sa3)/sw**2 - (ca1**2*cb**2*ee**2*complex(0,1)*sa3**2)/(2.*sw**2) + (ca1*ca3**2*cb*ee**2*complex(0,1)*sa1*sa2**2*sb)/sw**2 + (ca1**2*ca3*cb*ee**2*complex(0,1)*sa2*sa3*sb)/sw**2 - (ca3*cb*ee**2*complex(0,1)*sa1**2*sa2*sa3*sb)/sw**2 - (ca1*cb*ee**2*complex(0,1)*sa1*sa3**2*sb)/sw**2 - (ca1**2*ca3**2*ee**2*complex(0,1)*sa2**2*sb**2)/(2.*sw**2) + (ca1*ca3*ee**2*complex(0,1)*sa1*sa2*sa3*sb**2)/sw**2 - (ee**2*complex(0,1)*sa1**2*sa3**2*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_257 = Coupling(name = 'GC_257',
                  value = '-(ca2**2*ca3**2*cb**2*complex(0,1)*l8) - ca1**2*ca3**2*cb**2*complex(0,1)*l3*sa2**2 - ca1**2*ca3**2*cb**2*complex(0,1)*l4*sa2**2 + ca1**2*ca3**2*cb**2*complex(0,1)*l5*sa2**2 - (ca3**2*cb**2*ee**2*complex(0,1)*sa1**2*sa2**2)/(2.*cw**2) - ca3**2*cb**2*complex(0,1)*l2*sa1**2*sa2**2 - (ca1*ca3*cb**2*ee**2*complex(0,1)*sa1*sa2*sa3)/cw**2 - 2*ca1*ca3*cb**2*complex(0,1)*l2*sa1*sa2*sa3 + 2*ca1*ca3*cb**2*complex(0,1)*l3*sa1*sa2*sa3 + 2*ca1*ca3*cb**2*complex(0,1)*l4*sa1*sa2*sa3 - 2*ca1*ca3*cb**2*complex(0,1)*l5*sa1*sa2*sa3 - (ca1**2*cb**2*ee**2*complex(0,1)*sa3**2)/(2.*cw**2) - ca1**2*cb**2*complex(0,1)*l2*sa3**2 - cb**2*complex(0,1)*l3*sa1**2*sa3**2 - cb**2*complex(0,1)*l4*sa1**2*sa3**2 + cb**2*complex(0,1)*l5*sa1**2*sa3**2 + (ca1*ca3**2*cb*ee**2*complex(0,1)*sa1*sa2**2*sb)/cw**2 + 4*ca1*ca3**2*cb*complex(0,1)*l5*sa1*sa2**2*sb + (ca1**2*ca3*cb*ee**2*complex(0,1)*sa2*sa3*sb)/cw**2 + 4*ca1**2*ca3*cb*complex(0,1)*l5*sa2*sa3*sb - (ca3*cb*ee**2*complex(0,1)*sa1**2*sa2*sa3*sb)/cw**2 - 4*ca3*cb*complex(0,1)*l5*sa1**2*sa2*sa3*sb - (ca1*cb*ee**2*complex(0,1)*sa1*sa3**2*sb)/cw**2 - 4*ca1*cb*complex(0,1)*l5*sa1*sa3**2*sb - ca2**2*ca3**2*complex(0,1)*l7*sb**2 - (ca1**2*ca3**2*ee**2*complex(0,1)*sa2**2*sb**2)/(2.*cw**2) - ca1**2*ca3**2*complex(0,1)*l1*sa2**2*sb**2 - ca3**2*complex(0,1)*l3*sa1**2*sa2**2*sb**2 - ca3**2*complex(0,1)*l4*sa1**2*sa2**2*sb**2 + ca3**2*complex(0,1)*l5*sa1**2*sa2**2*sb**2 + (ca1*ca3*ee**2*complex(0,1)*sa1*sa2*sa3*sb**2)/cw**2 + 2*ca1*ca3*complex(0,1)*l1*sa1*sa2*sa3*sb**2 - 2*ca1*ca3*complex(0,1)*l3*sa1*sa2*sa3*sb**2 - 2*ca1*ca3*complex(0,1)*l4*sa1*sa2*sa3*sb**2 + 2*ca1*ca3*complex(0,1)*l5*sa1*sa2*sa3*sb**2 - ca1**2*complex(0,1)*l3*sa3**2*sb**2 - ca1**2*complex(0,1)*l4*sa3**2*sb**2 + ca1**2*complex(0,1)*l5*sa3**2*sb**2 - (ee**2*complex(0,1)*sa1**2*sa3**2*sb**2)/(2.*cw**2) - complex(0,1)*l1*sa1**2*sa3**2*sb**2 - (ca3**2*cb**2*ee**2*complex(0,1)*sa1**2*sa2**2)/(2.*sw**2) - (ca1*ca3*cb**2*ee**2*complex(0,1)*sa1*sa2*sa3)/sw**2 - (ca1**2*cb**2*ee**2*complex(0,1)*sa3**2)/(2.*sw**2) + (ca1*ca3**2*cb*ee**2*complex(0,1)*sa1*sa2**2*sb)/sw**2 + (ca1**2*ca3*cb*ee**2*complex(0,1)*sa2*sa3*sb)/sw**2 - (ca3*cb*ee**2*complex(0,1)*sa1**2*sa2*sa3*sb)/sw**2 - (ca1*cb*ee**2*complex(0,1)*sa1*sa3**2*sb)/sw**2 - (ca1**2*ca3**2*ee**2*complex(0,1)*sa2**2*sb**2)/(2.*sw**2) + (ca1*ca3*ee**2*complex(0,1)*sa1*sa2*sa3*sb**2)/sw**2 - (ee**2*complex(0,1)*sa1**2*sa3**2*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_258 = Coupling(name = 'GC_258',
                  value = '(ca1**2*ca3**2*cb**2*complex(0,1)*l4*sa2)/2. + (ca1**2*ca3**2*cb**2*complex(0,1)*l5*sa2)/2. - (ca3**2*cb**2*complex(0,1)*l4*sa1**2*sa2)/2. - (ca3**2*cb**2*complex(0,1)*l5*sa1**2*sa2)/2. - ca1*ca3*cb**2*complex(0,1)*l4*sa1*sa3 - ca1*ca3*cb**2*complex(0,1)*l5*sa1*sa3 - ca1*ca3*cb**2*complex(0,1)*l4*sa1*sa2**2*sa3 - ca1*ca3*cb**2*complex(0,1)*l5*sa1*sa2**2*sa3 - (ca1**2*cb**2*complex(0,1)*l4*sa2*sa3**2)/2. - (ca1**2*cb**2*complex(0,1)*l5*sa2*sa3**2)/2. + (cb**2*complex(0,1)*l4*sa1**2*sa2*sa3**2)/2. + (cb**2*complex(0,1)*l5*sa1**2*sa2*sa3**2)/2. + ca1*ca3**2*cb*complex(0,1)*l1*sa1*sa2*sb + ca1*ca3**2*cb*complex(0,1)*l2*sa1*sa2*sb - 2*ca1*ca3**2*cb*complex(0,1)*l3*sa1*sa2*sb + ca1**2*ca3*cb*complex(0,1)*l2*sa3*sb - ca1**2*ca3*cb*complex(0,1)*l3*sa3*sb + ca2**2*ca3*cb*complex(0,1)*l7*sa3*sb - ca2**2*ca3*cb*complex(0,1)*l8*sa3*sb - ca3*cb*complex(0,1)*l1*sa1**2*sa3*sb + ca3*cb*complex(0,1)*l3*sa1**2*sa3*sb + ca1**2*ca3*cb*complex(0,1)*l1*sa2**2*sa3*sb - ca1**2*ca3*cb*complex(0,1)*l3*sa2**2*sa3*sb - ca3*cb*complex(0,1)*l2*sa1**2*sa2**2*sa3*sb + ca3*cb*complex(0,1)*l3*sa1**2*sa2**2*sa3*sb - ca1*cb*complex(0,1)*l1*sa1*sa2*sa3**2*sb - ca1*cb*complex(0,1)*l2*sa1*sa2*sa3**2*sb + 2*ca1*cb*complex(0,1)*l3*sa1*sa2*sa3**2*sb - (ca1**2*ca3**2*complex(0,1)*l4*sa2*sb**2)/2. - (ca1**2*ca3**2*complex(0,1)*l5*sa2*sb**2)/2. + (ca3**2*complex(0,1)*l4*sa1**2*sa2*sb**2)/2. + (ca3**2*complex(0,1)*l5*sa1**2*sa2*sb**2)/2. + ca1*ca3*complex(0,1)*l4*sa1*sa3*sb**2 + ca1*ca3*complex(0,1)*l5*sa1*sa3*sb**2 + ca1*ca3*complex(0,1)*l4*sa1*sa2**2*sa3*sb**2 + ca1*ca3*complex(0,1)*l5*sa1*sa2**2*sa3*sb**2 + (ca1**2*complex(0,1)*l4*sa2*sa3**2*sb**2)/2. + (ca1**2*complex(0,1)*l5*sa2*sa3**2*sb**2)/2. - (complex(0,1)*l4*sa1**2*sa2*sa3**2*sb**2)/2. - (complex(0,1)*l5*sa1**2*sa2*sa3**2*sb**2)/2. - (ca1**2*ca3**2*cb**2*ee**2*complex(0,1)*sa2)/(4.*sw**2) + (ca1*ca3*cb**2*ee**2*complex(0,1)*sa1*sa3)/(4.*sw**2) + (ca1*ca3*cb**2*ee**2*complex(0,1)*sa1*sa2**2*sa3)/(4.*sw**2) - (cb**2*ee**2*complex(0,1)*sa1**2*sa2*sa3**2)/(4.*sw**2) - (ca1*ca3**2*cb*ee**2*complex(0,1)*sa1*sa2*sb)/(2.*sw**2) - (ca1**2*ca3*cb*ee**2*complex(0,1)*sa3*sb)/(4.*sw**2) + (ca3*cb*ee**2*complex(0,1)*sa1**2*sa3*sb)/(4.*sw**2) - (ca1**2*ca3*cb*ee**2*complex(0,1)*sa2**2*sa3*sb)/(4.*sw**2) + (ca3*cb*ee**2*complex(0,1)*sa1**2*sa2**2*sa3*sb)/(4.*sw**2) + (ca1*cb*ee**2*complex(0,1)*sa1*sa2*sa3**2*sb)/(2.*sw**2) - (ca3**2*ee**2*complex(0,1)*sa1**2*sa2*sb**2)/(4.*sw**2) - (ca1*ca3*ee**2*complex(0,1)*sa1*sa3*sb**2)/(4.*sw**2) - (ca1*ca3*ee**2*complex(0,1)*sa1*sa2**2*sa3*sb**2)/(4.*sw**2) - (ca1**2*ee**2*complex(0,1)*sa2*sa3**2*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_259 = Coupling(name = 'GC_259',
                  value = '-(ca1**2*ca3**2*cb**2*ee**2*complex(0,1)*sa2)/(4.*cw**2) + ca1**2*ca3**2*cb**2*complex(0,1)*l5*sa2 - ca3**2*cb**2*complex(0,1)*l5*sa1**2*sa2 + (ca1*ca3*cb**2*ee**2*complex(0,1)*sa1*sa3)/(4.*cw**2) - 2*ca1*ca3*cb**2*complex(0,1)*l5*sa1*sa3 + (ca1*ca3*cb**2*ee**2*complex(0,1)*sa1*sa2**2*sa3)/(4.*cw**2) - 2*ca1*ca3*cb**2*complex(0,1)*l5*sa1*sa2**2*sa3 - ca1**2*cb**2*complex(0,1)*l5*sa2*sa3**2 - (cb**2*ee**2*complex(0,1)*sa1**2*sa2*sa3**2)/(4.*cw**2) + cb**2*complex(0,1)*l5*sa1**2*sa2*sa3**2 - (ca1*ca3**2*cb*ee**2*complex(0,1)*sa1*sa2*sb)/(2.*cw**2) + ca1*ca3**2*cb*complex(0,1)*l1*sa1*sa2*sb + ca1*ca3**2*cb*complex(0,1)*l2*sa1*sa2*sb - 2*ca1*ca3**2*cb*complex(0,1)*l3*sa1*sa2*sb - 2*ca1*ca3**2*cb*complex(0,1)*l4*sa1*sa2*sb + 2*ca1*ca3**2*cb*complex(0,1)*l5*sa1*sa2*sb - (ca1**2*ca3*cb*ee**2*complex(0,1)*sa3*sb)/(4.*cw**2) + ca1**2*ca3*cb*complex(0,1)*l2*sa3*sb - ca1**2*ca3*cb*complex(0,1)*l3*sa3*sb - ca1**2*ca3*cb*complex(0,1)*l4*sa3*sb + ca1**2*ca3*cb*complex(0,1)*l5*sa3*sb + ca2**2*ca3*cb*complex(0,1)*l7*sa3*sb - ca2**2*ca3*cb*complex(0,1)*l8*sa3*sb + (ca3*cb*ee**2*complex(0,1)*sa1**2*sa3*sb)/(4.*cw**2) - ca3*cb*complex(0,1)*l1*sa1**2*sa3*sb + ca3*cb*complex(0,1)*l3*sa1**2*sa3*sb + ca3*cb*complex(0,1)*l4*sa1**2*sa3*sb - ca3*cb*complex(0,1)*l5*sa1**2*sa3*sb - (ca1**2*ca3*cb*ee**2*complex(0,1)*sa2**2*sa3*sb)/(4.*cw**2) + ca1**2*ca3*cb*complex(0,1)*l1*sa2**2*sa3*sb - ca1**2*ca3*cb*complex(0,1)*l3*sa2**2*sa3*sb - ca1**2*ca3*cb*complex(0,1)*l4*sa2**2*sa3*sb + ca1**2*ca3*cb*complex(0,1)*l5*sa2**2*sa3*sb + (ca3*cb*ee**2*complex(0,1)*sa1**2*sa2**2*sa3*sb)/(4.*cw**2) - ca3*cb*complex(0,1)*l2*sa1**2*sa2**2*sa3*sb + ca3*cb*complex(0,1)*l3*sa1**2*sa2**2*sa3*sb + ca3*cb*complex(0,1)*l4*sa1**2*sa2**2*sa3*sb - ca3*cb*complex(0,1)*l5*sa1**2*sa2**2*sa3*sb + (ca1*cb*ee**2*complex(0,1)*sa1*sa2*sa3**2*sb)/(2.*cw**2) - ca1*cb*complex(0,1)*l1*sa1*sa2*sa3**2*sb - ca1*cb*complex(0,1)*l2*sa1*sa2*sa3**2*sb + 2*ca1*cb*complex(0,1)*l3*sa1*sa2*sa3**2*sb + 2*ca1*cb*complex(0,1)*l4*sa1*sa2*sa3**2*sb - 2*ca1*cb*complex(0,1)*l5*sa1*sa2*sa3**2*sb - ca1**2*ca3**2*complex(0,1)*l5*sa2*sb**2 - (ca3**2*ee**2*complex(0,1)*sa1**2*sa2*sb**2)/(4.*cw**2) + ca3**2*complex(0,1)*l5*sa1**2*sa2*sb**2 - (ca1*ca3*ee**2*complex(0,1)*sa1*sa3*sb**2)/(4.*cw**2) + 2*ca1*ca3*complex(0,1)*l5*sa1*sa3*sb**2 - (ca1*ca3*ee**2*complex(0,1)*sa1*sa2**2*sa3*sb**2)/(4.*cw**2) + 2*ca1*ca3*complex(0,1)*l5*sa1*sa2**2*sa3*sb**2 - (ca1**2*ee**2*complex(0,1)*sa2*sa3**2*sb**2)/(4.*cw**2) + ca1**2*complex(0,1)*l5*sa2*sa3**2*sb**2 - complex(0,1)*l5*sa1**2*sa2*sa3**2*sb**2 - (ca1**2*ca3**2*cb**2*ee**2*complex(0,1)*sa2)/(4.*sw**2) + (ca1*ca3*cb**2*ee**2*complex(0,1)*sa1*sa3)/(4.*sw**2) + (ca1*ca3*cb**2*ee**2*complex(0,1)*sa1*sa2**2*sa3)/(4.*sw**2) - (cb**2*ee**2*complex(0,1)*sa1**2*sa2*sa3**2)/(4.*sw**2) - (ca1*ca3**2*cb*ee**2*complex(0,1)*sa1*sa2*sb)/(2.*sw**2) - (ca1**2*ca3*cb*ee**2*complex(0,1)*sa3*sb)/(4.*sw**2) + (ca3*cb*ee**2*complex(0,1)*sa1**2*sa3*sb)/(4.*sw**2) - (ca1**2*ca3*cb*ee**2*complex(0,1)*sa2**2*sa3*sb)/(4.*sw**2) + (ca3*cb*ee**2*complex(0,1)*sa1**2*sa2**2*sa3*sb)/(4.*sw**2) + (ca1*cb*ee**2*complex(0,1)*sa1*sa2*sa3**2*sb)/(2.*sw**2) - (ca3**2*ee**2*complex(0,1)*sa1**2*sa2*sb**2)/(4.*sw**2) - (ca1*ca3*ee**2*complex(0,1)*sa1*sa3*sb**2)/(4.*sw**2) - (ca1*ca3*ee**2*complex(0,1)*sa1*sa2**2*sa3*sb**2)/(4.*sw**2) - (ca1**2*ee**2*complex(0,1)*sa2*sa3**2*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_260 = Coupling(name = 'GC_260',
                  value = 'ca1*ca3**2*cb**2*complex(0,1)*l2*sa1*sa2 - ca1*ca3**2*cb**2*complex(0,1)*l3*sa1*sa2 + ca1**2*ca3*cb**2*complex(0,1)*l2*sa3 - ca2**2*ca3*cb**2*complex(0,1)*l8*sa3 + ca3*cb**2*complex(0,1)*l3*sa1**2*sa3 - ca1**2*ca3*cb**2*complex(0,1)*l3*sa2**2*sa3 - ca3*cb**2*complex(0,1)*l2*sa1**2*sa2**2*sa3 - ca1*cb**2*complex(0,1)*l2*sa1*sa2*sa3**2 + ca1*cb**2*complex(0,1)*l3*sa1*sa2*sa3**2 - ca1**2*ca3**2*cb*complex(0,1)*l4*sa2*sb - ca1**2*ca3**2*cb*complex(0,1)*l5*sa2*sb + ca3**2*cb*complex(0,1)*l4*sa1**2*sa2*sb + ca3**2*cb*complex(0,1)*l5*sa1**2*sa2*sb + 2*ca1*ca3*cb*complex(0,1)*l4*sa1*sa3*sb + 2*ca1*ca3*cb*complex(0,1)*l5*sa1*sa3*sb + 2*ca1*ca3*cb*complex(0,1)*l4*sa1*sa2**2*sa3*sb + 2*ca1*ca3*cb*complex(0,1)*l5*sa1*sa2**2*sa3*sb + ca1**2*cb*complex(0,1)*l4*sa2*sa3**2*sb + ca1**2*cb*complex(0,1)*l5*sa2*sa3**2*sb - cb*complex(0,1)*l4*sa1**2*sa2*sa3**2*sb - cb*complex(0,1)*l5*sa1**2*sa2*sa3**2*sb - ca1*ca3**2*complex(0,1)*l1*sa1*sa2*sb**2 + ca1*ca3**2*complex(0,1)*l3*sa1*sa2*sb**2 + ca1**2*ca3*complex(0,1)*l3*sa3*sb**2 - ca2**2*ca3*complex(0,1)*l7*sa3*sb**2 + ca3*complex(0,1)*l1*sa1**2*sa3*sb**2 - ca1**2*ca3*complex(0,1)*l1*sa2**2*sa3*sb**2 - ca3*complex(0,1)*l3*sa1**2*sa2**2*sa3*sb**2 + ca1*complex(0,1)*l1*sa1*sa2*sa3**2*sb**2 - ca1*complex(0,1)*l3*sa1*sa2*sa3**2*sb**2 - (ca1*ca3**2*cb**2*ee**2*complex(0,1)*sa1*sa2)/(4.*sw**2) - (ca1**2*ca3*cb**2*ee**2*complex(0,1)*sa3)/(4.*sw**2) + (ca3*cb**2*ee**2*complex(0,1)*sa1**2*sa2**2*sa3)/(4.*sw**2) + (ca1*cb**2*ee**2*complex(0,1)*sa1*sa2*sa3**2)/(4.*sw**2) + (ca1**2*ca3**2*cb*ee**2*complex(0,1)*sa2*sb)/(4.*sw**2) - (ca3**2*cb*ee**2*complex(0,1)*sa1**2*sa2*sb)/(4.*sw**2) - (ca1*ca3*cb*ee**2*complex(0,1)*sa1*sa3*sb)/(2.*sw**2) - (ca1*ca3*cb*ee**2*complex(0,1)*sa1*sa2**2*sa3*sb)/(2.*sw**2) - (ca1**2*cb*ee**2*complex(0,1)*sa2*sa3**2*sb)/(4.*sw**2) + (cb*ee**2*complex(0,1)*sa1**2*sa2*sa3**2*sb)/(4.*sw**2) + (ca1*ca3**2*ee**2*complex(0,1)*sa1*sa2*sb**2)/(4.*sw**2) - (ca3*ee**2*complex(0,1)*sa1**2*sa3*sb**2)/(4.*sw**2) + (ca1**2*ca3*ee**2*complex(0,1)*sa2**2*sa3*sb**2)/(4.*sw**2) - (ca1*ee**2*complex(0,1)*sa1*sa2*sa3**2*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_261 = Coupling(name = 'GC_261',
                  value = '-(ca1*ca3**2*cb**2*ee**2*complex(0,1)*sa1*sa2)/(4.*cw**2) + ca1*ca3**2*cb**2*complex(0,1)*l2*sa1*sa2 - ca1*ca3**2*cb**2*complex(0,1)*l3*sa1*sa2 - ca1*ca3**2*cb**2*complex(0,1)*l4*sa1*sa2 + ca1*ca3**2*cb**2*complex(0,1)*l5*sa1*sa2 - (ca1**2*ca3*cb**2*ee**2*complex(0,1)*sa3)/(4.*cw**2) + ca1**2*ca3*cb**2*complex(0,1)*l2*sa3 - ca2**2*ca3*cb**2*complex(0,1)*l8*sa3 + ca3*cb**2*complex(0,1)*l3*sa1**2*sa3 + ca3*cb**2*complex(0,1)*l4*sa1**2*sa3 - ca3*cb**2*complex(0,1)*l5*sa1**2*sa3 - ca1**2*ca3*cb**2*complex(0,1)*l3*sa2**2*sa3 - ca1**2*ca3*cb**2*complex(0,1)*l4*sa2**2*sa3 + ca1**2*ca3*cb**2*complex(0,1)*l5*sa2**2*sa3 + (ca3*cb**2*ee**2*complex(0,1)*sa1**2*sa2**2*sa3)/(4.*cw**2) - ca3*cb**2*complex(0,1)*l2*sa1**2*sa2**2*sa3 + (ca1*cb**2*ee**2*complex(0,1)*sa1*sa2*sa3**2)/(4.*cw**2) - ca1*cb**2*complex(0,1)*l2*sa1*sa2*sa3**2 + ca1*cb**2*complex(0,1)*l3*sa1*sa2*sa3**2 + ca1*cb**2*complex(0,1)*l4*sa1*sa2*sa3**2 - ca1*cb**2*complex(0,1)*l5*sa1*sa2*sa3**2 + (ca1**2*ca3**2*cb*ee**2*complex(0,1)*sa2*sb)/(4.*cw**2) - 2*ca1**2*ca3**2*cb*complex(0,1)*l5*sa2*sb - (ca3**2*cb*ee**2*complex(0,1)*sa1**2*sa2*sb)/(4.*cw**2) + 2*ca3**2*cb*complex(0,1)*l5*sa1**2*sa2*sb - (ca1*ca3*cb*ee**2*complex(0,1)*sa1*sa3*sb)/(2.*cw**2) + 4*ca1*ca3*cb*complex(0,1)*l5*sa1*sa3*sb - (ca1*ca3*cb*ee**2*complex(0,1)*sa1*sa2**2*sa3*sb)/(2.*cw**2) + 4*ca1*ca3*cb*complex(0,1)*l5*sa1*sa2**2*sa3*sb - (ca1**2*cb*ee**2*complex(0,1)*sa2*sa3**2*sb)/(4.*cw**2) + 2*ca1**2*cb*complex(0,1)*l5*sa2*sa3**2*sb + (cb*ee**2*complex(0,1)*sa1**2*sa2*sa3**2*sb)/(4.*cw**2) - 2*cb*complex(0,1)*l5*sa1**2*sa2*sa3**2*sb + (ca1*ca3**2*ee**2*complex(0,1)*sa1*sa2*sb**2)/(4.*cw**2) - ca1*ca3**2*complex(0,1)*l1*sa1*sa2*sb**2 + ca1*ca3**2*complex(0,1)*l3*sa1*sa2*sb**2 + ca1*ca3**2*complex(0,1)*l4*sa1*sa2*sb**2 - ca1*ca3**2*complex(0,1)*l5*sa1*sa2*sb**2 + ca1**2*ca3*complex(0,1)*l3*sa3*sb**2 + ca1**2*ca3*complex(0,1)*l4*sa3*sb**2 - ca1**2*ca3*complex(0,1)*l5*sa3*sb**2 - ca2**2*ca3*complex(0,1)*l7*sa3*sb**2 - (ca3*ee**2*complex(0,1)*sa1**2*sa3*sb**2)/(4.*cw**2) + ca3*complex(0,1)*l1*sa1**2*sa3*sb**2 + (ca1**2*ca3*ee**2*complex(0,1)*sa2**2*sa3*sb**2)/(4.*cw**2) - ca1**2*ca3*complex(0,1)*l1*sa2**2*sa3*sb**2 - ca3*complex(0,1)*l3*sa1**2*sa2**2*sa3*sb**2 - ca3*complex(0,1)*l4*sa1**2*sa2**2*sa3*sb**2 + ca3*complex(0,1)*l5*sa1**2*sa2**2*sa3*sb**2 - (ca1*ee**2*complex(0,1)*sa1*sa2*sa3**2*sb**2)/(4.*cw**2) + ca1*complex(0,1)*l1*sa1*sa2*sa3**2*sb**2 - ca1*complex(0,1)*l3*sa1*sa2*sa3**2*sb**2 - ca1*complex(0,1)*l4*sa1*sa2*sa3**2*sb**2 + ca1*complex(0,1)*l5*sa1*sa2*sa3**2*sb**2 - (ca1*ca3**2*cb**2*ee**2*complex(0,1)*sa1*sa2)/(4.*sw**2) - (ca1**2*ca3*cb**2*ee**2*complex(0,1)*sa3)/(4.*sw**2) + (ca3*cb**2*ee**2*complex(0,1)*sa1**2*sa2**2*sa3)/(4.*sw**2) + (ca1*cb**2*ee**2*complex(0,1)*sa1*sa2*sa3**2)/(4.*sw**2) + (ca1**2*ca3**2*cb*ee**2*complex(0,1)*sa2*sb)/(4.*sw**2) - (ca3**2*cb*ee**2*complex(0,1)*sa1**2*sa2*sb)/(4.*sw**2) - (ca1*ca3*cb*ee**2*complex(0,1)*sa1*sa3*sb)/(2.*sw**2) - (ca1*ca3*cb*ee**2*complex(0,1)*sa1*sa2**2*sa3*sb)/(2.*sw**2) - (ca1**2*cb*ee**2*complex(0,1)*sa2*sa3**2*sb)/(4.*sw**2) + (cb*ee**2*complex(0,1)*sa1**2*sa2*sa3**2*sb)/(4.*sw**2) + (ca1*ca3**2*ee**2*complex(0,1)*sa1*sa2*sb**2)/(4.*sw**2) - (ca3*ee**2*complex(0,1)*sa1**2*sa3*sb**2)/(4.*sw**2) + (ca1**2*ca3*ee**2*complex(0,1)*sa2**2*sa3*sb**2)/(4.*sw**2) - (ca1*ee**2*complex(0,1)*sa1*sa2*sa3**2*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_262 = Coupling(name = 'GC_262',
                  value = '-(ca1*ca3**2*cb**2*complex(0,1)*l1*sa1*sa2) + ca1*ca3**2*cb**2*complex(0,1)*l3*sa1*sa2 + ca1**2*ca3*cb**2*complex(0,1)*l3*sa3 - ca2**2*ca3*cb**2*complex(0,1)*l7*sa3 + ca3*cb**2*complex(0,1)*l1*sa1**2*sa3 - ca1**2*ca3*cb**2*complex(0,1)*l1*sa2**2*sa3 - ca3*cb**2*complex(0,1)*l3*sa1**2*sa2**2*sa3 + ca1*cb**2*complex(0,1)*l1*sa1*sa2*sa3**2 - ca1*cb**2*complex(0,1)*l3*sa1*sa2*sa3**2 + ca1**2*ca3**2*cb*complex(0,1)*l4*sa2*sb + ca1**2*ca3**2*cb*complex(0,1)*l5*sa2*sb - ca3**2*cb*complex(0,1)*l4*sa1**2*sa2*sb - ca3**2*cb*complex(0,1)*l5*sa1**2*sa2*sb - 2*ca1*ca3*cb*complex(0,1)*l4*sa1*sa3*sb - 2*ca1*ca3*cb*complex(0,1)*l5*sa1*sa3*sb - 2*ca1*ca3*cb*complex(0,1)*l4*sa1*sa2**2*sa3*sb - 2*ca1*ca3*cb*complex(0,1)*l5*sa1*sa2**2*sa3*sb - ca1**2*cb*complex(0,1)*l4*sa2*sa3**2*sb - ca1**2*cb*complex(0,1)*l5*sa2*sa3**2*sb + cb*complex(0,1)*l4*sa1**2*sa2*sa3**2*sb + cb*complex(0,1)*l5*sa1**2*sa2*sa3**2*sb + ca1*ca3**2*complex(0,1)*l2*sa1*sa2*sb**2 - ca1*ca3**2*complex(0,1)*l3*sa1*sa2*sb**2 + ca1**2*ca3*complex(0,1)*l2*sa3*sb**2 - ca2**2*ca3*complex(0,1)*l8*sa3*sb**2 + ca3*complex(0,1)*l3*sa1**2*sa3*sb**2 - ca1**2*ca3*complex(0,1)*l3*sa2**2*sa3*sb**2 - ca3*complex(0,1)*l2*sa1**2*sa2**2*sa3*sb**2 - ca1*complex(0,1)*l2*sa1*sa2*sa3**2*sb**2 + ca1*complex(0,1)*l3*sa1*sa2*sa3**2*sb**2 + (ca1*ca3**2*cb**2*ee**2*complex(0,1)*sa1*sa2)/(4.*sw**2) - (ca3*cb**2*ee**2*complex(0,1)*sa1**2*sa3)/(4.*sw**2) + (ca1**2*ca3*cb**2*ee**2*complex(0,1)*sa2**2*sa3)/(4.*sw**2) - (ca1*cb**2*ee**2*complex(0,1)*sa1*sa2*sa3**2)/(4.*sw**2) - (ca1**2*ca3**2*cb*ee**2*complex(0,1)*sa2*sb)/(4.*sw**2) + (ca3**2*cb*ee**2*complex(0,1)*sa1**2*sa2*sb)/(4.*sw**2) + (ca1*ca3*cb*ee**2*complex(0,1)*sa1*sa3*sb)/(2.*sw**2) + (ca1*ca3*cb*ee**2*complex(0,1)*sa1*sa2**2*sa3*sb)/(2.*sw**2) + (ca1**2*cb*ee**2*complex(0,1)*sa2*sa3**2*sb)/(4.*sw**2) - (cb*ee**2*complex(0,1)*sa1**2*sa2*sa3**2*sb)/(4.*sw**2) - (ca1*ca3**2*ee**2*complex(0,1)*sa1*sa2*sb**2)/(4.*sw**2) - (ca1**2*ca3*ee**2*complex(0,1)*sa3*sb**2)/(4.*sw**2) + (ca3*ee**2*complex(0,1)*sa1**2*sa2**2*sa3*sb**2)/(4.*sw**2) + (ca1*ee**2*complex(0,1)*sa1*sa2*sa3**2*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_263 = Coupling(name = 'GC_263',
                  value = '(ca1*ca3**2*cb**2*ee**2*complex(0,1)*sa1*sa2)/(4.*cw**2) - ca1*ca3**2*cb**2*complex(0,1)*l1*sa1*sa2 + ca1*ca3**2*cb**2*complex(0,1)*l3*sa1*sa2 + ca1*ca3**2*cb**2*complex(0,1)*l4*sa1*sa2 - ca1*ca3**2*cb**2*complex(0,1)*l5*sa1*sa2 + ca1**2*ca3*cb**2*complex(0,1)*l3*sa3 + ca1**2*ca3*cb**2*complex(0,1)*l4*sa3 - ca1**2*ca3*cb**2*complex(0,1)*l5*sa3 - ca2**2*ca3*cb**2*complex(0,1)*l7*sa3 - (ca3*cb**2*ee**2*complex(0,1)*sa1**2*sa3)/(4.*cw**2) + ca3*cb**2*complex(0,1)*l1*sa1**2*sa3 + (ca1**2*ca3*cb**2*ee**2*complex(0,1)*sa2**2*sa3)/(4.*cw**2) - ca1**2*ca3*cb**2*complex(0,1)*l1*sa2**2*sa3 - ca3*cb**2*complex(0,1)*l3*sa1**2*sa2**2*sa3 - ca3*cb**2*complex(0,1)*l4*sa1**2*sa2**2*sa3 + ca3*cb**2*complex(0,1)*l5*sa1**2*sa2**2*sa3 - (ca1*cb**2*ee**2*complex(0,1)*sa1*sa2*sa3**2)/(4.*cw**2) + ca1*cb**2*complex(0,1)*l1*sa1*sa2*sa3**2 - ca1*cb**2*complex(0,1)*l3*sa1*sa2*sa3**2 - ca1*cb**2*complex(0,1)*l4*sa1*sa2*sa3**2 + ca1*cb**2*complex(0,1)*l5*sa1*sa2*sa3**2 - (ca1**2*ca3**2*cb*ee**2*complex(0,1)*sa2*sb)/(4.*cw**2) + 2*ca1**2*ca3**2*cb*complex(0,1)*l5*sa2*sb + (ca3**2*cb*ee**2*complex(0,1)*sa1**2*sa2*sb)/(4.*cw**2) - 2*ca3**2*cb*complex(0,1)*l5*sa1**2*sa2*sb + (ca1*ca3*cb*ee**2*complex(0,1)*sa1*sa3*sb)/(2.*cw**2) - 4*ca1*ca3*cb*complex(0,1)*l5*sa1*sa3*sb + (ca1*ca3*cb*ee**2*complex(0,1)*sa1*sa2**2*sa3*sb)/(2.*cw**2) - 4*ca1*ca3*cb*complex(0,1)*l5*sa1*sa2**2*sa3*sb + (ca1**2*cb*ee**2*complex(0,1)*sa2*sa3**2*sb)/(4.*cw**2) - 2*ca1**2*cb*complex(0,1)*l5*sa2*sa3**2*sb - (cb*ee**2*complex(0,1)*sa1**2*sa2*sa3**2*sb)/(4.*cw**2) + 2*cb*complex(0,1)*l5*sa1**2*sa2*sa3**2*sb - (ca1*ca3**2*ee**2*complex(0,1)*sa1*sa2*sb**2)/(4.*cw**2) + ca1*ca3**2*complex(0,1)*l2*sa1*sa2*sb**2 - ca1*ca3**2*complex(0,1)*l3*sa1*sa2*sb**2 - ca1*ca3**2*complex(0,1)*l4*sa1*sa2*sb**2 + ca1*ca3**2*complex(0,1)*l5*sa1*sa2*sb**2 - (ca1**2*ca3*ee**2*complex(0,1)*sa3*sb**2)/(4.*cw**2) + ca1**2*ca3*complex(0,1)*l2*sa3*sb**2 - ca2**2*ca3*complex(0,1)*l8*sa3*sb**2 + ca3*complex(0,1)*l3*sa1**2*sa3*sb**2 + ca3*complex(0,1)*l4*sa1**2*sa3*sb**2 - ca3*complex(0,1)*l5*sa1**2*sa3*sb**2 - ca1**2*ca3*complex(0,1)*l3*sa2**2*sa3*sb**2 - ca1**2*ca3*complex(0,1)*l4*sa2**2*sa3*sb**2 + ca1**2*ca3*complex(0,1)*l5*sa2**2*sa3*sb**2 + (ca3*ee**2*complex(0,1)*sa1**2*sa2**2*sa3*sb**2)/(4.*cw**2) - ca3*complex(0,1)*l2*sa1**2*sa2**2*sa3*sb**2 + (ca1*ee**2*complex(0,1)*sa1*sa2*sa3**2*sb**2)/(4.*cw**2) - ca1*complex(0,1)*l2*sa1*sa2*sa3**2*sb**2 + ca1*complex(0,1)*l3*sa1*sa2*sa3**2*sb**2 + ca1*complex(0,1)*l4*sa1*sa2*sa3**2*sb**2 - ca1*complex(0,1)*l5*sa1*sa2*sa3**2*sb**2 + (ca1*ca3**2*cb**2*ee**2*complex(0,1)*sa1*sa2)/(4.*sw**2) - (ca3*cb**2*ee**2*complex(0,1)*sa1**2*sa3)/(4.*sw**2) + (ca1**2*ca3*cb**2*ee**2*complex(0,1)*sa2**2*sa3)/(4.*sw**2) - (ca1*cb**2*ee**2*complex(0,1)*sa1*sa2*sa3**2)/(4.*sw**2) - (ca1**2*ca3**2*cb*ee**2*complex(0,1)*sa2*sb)/(4.*sw**2) + (ca3**2*cb*ee**2*complex(0,1)*sa1**2*sa2*sb)/(4.*sw**2) + (ca1*ca3*cb*ee**2*complex(0,1)*sa1*sa3*sb)/(2.*sw**2) + (ca1*ca3*cb*ee**2*complex(0,1)*sa1*sa2**2*sa3*sb)/(2.*sw**2) + (ca1**2*cb*ee**2*complex(0,1)*sa2*sa3**2*sb)/(4.*sw**2) - (cb*ee**2*complex(0,1)*sa1**2*sa2*sa3**2*sb)/(4.*sw**2) - (ca1*ca3**2*ee**2*complex(0,1)*sa1*sa2*sb**2)/(4.*sw**2) - (ca1**2*ca3*ee**2*complex(0,1)*sa3*sb**2)/(4.*sw**2) + (ca3*ee**2*complex(0,1)*sa1**2*sa2**2*sa3*sb**2)/(4.*sw**2) + (ca1*ee**2*complex(0,1)*sa1*sa2*sa3**2*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_264 = Coupling(name = 'GC_264',
                  value = '-(ca1*ca3**2*cb**2*complex(0,1)*l1*sa1*sa2) + ca1*ca3**2*cb**2*complex(0,1)*l3*sa1*sa2 + ca1**2*ca3*cb**2*complex(0,1)*l3*sa3 - ca2**2*ca3*cb**2*complex(0,1)*l7*sa3 + ca3*cb**2*complex(0,1)*l1*sa1**2*sa3 - ca1**2*ca3*cb**2*complex(0,1)*l1*sa2**2*sa3 - ca3*cb**2*complex(0,1)*l3*sa1**2*sa2**2*sa3 + ca1*cb**2*complex(0,1)*l1*sa1*sa2*sa3**2 - ca1*cb**2*complex(0,1)*l3*sa1*sa2*sa3**2 + ca1**2*ca3**2*cb*complex(0,1)*l4*sa2*sb + ca1**2*ca3**2*cb*complex(0,1)*l5*sa2*sb - ca3**2*cb*complex(0,1)*l4*sa1**2*sa2*sb - ca3**2*cb*complex(0,1)*l5*sa1**2*sa2*sb - 2*ca1*ca3*cb*complex(0,1)*l4*sa1*sa3*sb - 2*ca1*ca3*cb*complex(0,1)*l5*sa1*sa3*sb - 2*ca1*ca3*cb*complex(0,1)*l4*sa1*sa2**2*sa3*sb - 2*ca1*ca3*cb*complex(0,1)*l5*sa1*sa2**2*sa3*sb - ca1**2*cb*complex(0,1)*l4*sa2*sa3**2*sb - ca1**2*cb*complex(0,1)*l5*sa2*sa3**2*sb + cb*complex(0,1)*l4*sa1**2*sa2*sa3**2*sb + cb*complex(0,1)*l5*sa1**2*sa2*sa3**2*sb + ca1*ca3**2*complex(0,1)*l2*sa1*sa2*sb**2 - ca1*ca3**2*complex(0,1)*l3*sa1*sa2*sb**2 + ca1**2*ca3*complex(0,1)*l2*sa3*sb**2 - ca2**2*ca3*complex(0,1)*l8*sa3*sb**2 + ca3*complex(0,1)*l3*sa1**2*sa3*sb**2 - ca1**2*ca3*complex(0,1)*l3*sa2**2*sa3*sb**2 - ca3*complex(0,1)*l2*sa1**2*sa2**2*sa3*sb**2 - ca1*complex(0,1)*l2*sa1*sa2*sa3**2*sb**2 + ca1*complex(0,1)*l3*sa1*sa2*sa3**2*sb**2 - (ca1*ca3**2*cb**2*ee**2*complex(0,1)*sa1*sa2)/(2.*sw**2) + (ca3*cb**2*ee**2*complex(0,1)*sa1**2*sa3)/(2.*sw**2) - (ca1**2*ca3*cb**2*ee**2*complex(0,1)*sa2**2*sa3)/(2.*sw**2) + (ca1*cb**2*ee**2*complex(0,1)*sa1*sa2*sa3**2)/(2.*sw**2) + (ca1**2*ca3**2*cb*ee**2*complex(0,1)*sa2*sb)/(2.*sw**2) - (ca3**2*cb*ee**2*complex(0,1)*sa1**2*sa2*sb)/(2.*sw**2) - (ca1*ca3*cb*ee**2*complex(0,1)*sa1*sa3*sb)/sw**2 - (ca1*ca3*cb*ee**2*complex(0,1)*sa1*sa2**2*sa3*sb)/sw**2 - (ca1**2*cb*ee**2*complex(0,1)*sa2*sa3**2*sb)/(2.*sw**2) + (cb*ee**2*complex(0,1)*sa1**2*sa2*sa3**2*sb)/(2.*sw**2) + (ca1*ca3**2*ee**2*complex(0,1)*sa1*sa2*sb**2)/(2.*sw**2) + (ca1**2*ca3*ee**2*complex(0,1)*sa3*sb**2)/(2.*sw**2) - (ca3*ee**2*complex(0,1)*sa1**2*sa2**2*sa3*sb**2)/(2.*sw**2) - (ca1*ee**2*complex(0,1)*sa1*sa2*sa3**2*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_265 = Coupling(name = 'GC_265',
                  value = '-(ca1*ca3**2*cb**2*ee**2*complex(0,1)*sa1*sa2)/(2.*cw**2) - ca1*ca3**2*cb**2*complex(0,1)*l1*sa1*sa2 + ca1*ca3**2*cb**2*complex(0,1)*l3*sa1*sa2 + ca1*ca3**2*cb**2*complex(0,1)*l4*sa1*sa2 - ca1*ca3**2*cb**2*complex(0,1)*l5*sa1*sa2 + ca1**2*ca3*cb**2*complex(0,1)*l3*sa3 + ca1**2*ca3*cb**2*complex(0,1)*l4*sa3 - ca1**2*ca3*cb**2*complex(0,1)*l5*sa3 - ca2**2*ca3*cb**2*complex(0,1)*l7*sa3 + (ca3*cb**2*ee**2*complex(0,1)*sa1**2*sa3)/(2.*cw**2) + ca3*cb**2*complex(0,1)*l1*sa1**2*sa3 - (ca1**2*ca3*cb**2*ee**2*complex(0,1)*sa2**2*sa3)/(2.*cw**2) - ca1**2*ca3*cb**2*complex(0,1)*l1*sa2**2*sa3 - ca3*cb**2*complex(0,1)*l3*sa1**2*sa2**2*sa3 - ca3*cb**2*complex(0,1)*l4*sa1**2*sa2**2*sa3 + ca3*cb**2*complex(0,1)*l5*sa1**2*sa2**2*sa3 + (ca1*cb**2*ee**2*complex(0,1)*sa1*sa2*sa3**2)/(2.*cw**2) + ca1*cb**2*complex(0,1)*l1*sa1*sa2*sa3**2 - ca1*cb**2*complex(0,1)*l3*sa1*sa2*sa3**2 - ca1*cb**2*complex(0,1)*l4*sa1*sa2*sa3**2 + ca1*cb**2*complex(0,1)*l5*sa1*sa2*sa3**2 + (ca1**2*ca3**2*cb*ee**2*complex(0,1)*sa2*sb)/(2.*cw**2) + 2*ca1**2*ca3**2*cb*complex(0,1)*l5*sa2*sb - (ca3**2*cb*ee**2*complex(0,1)*sa1**2*sa2*sb)/(2.*cw**2) - 2*ca3**2*cb*complex(0,1)*l5*sa1**2*sa2*sb - (ca1*ca3*cb*ee**2*complex(0,1)*sa1*sa3*sb)/cw**2 - 4*ca1*ca3*cb*complex(0,1)*l5*sa1*sa3*sb - (ca1*ca3*cb*ee**2*complex(0,1)*sa1*sa2**2*sa3*sb)/cw**2 - 4*ca1*ca3*cb*complex(0,1)*l5*sa1*sa2**2*sa3*sb - (ca1**2*cb*ee**2*complex(0,1)*sa2*sa3**2*sb)/(2.*cw**2) - 2*ca1**2*cb*complex(0,1)*l5*sa2*sa3**2*sb + (cb*ee**2*complex(0,1)*sa1**2*sa2*sa3**2*sb)/(2.*cw**2) + 2*cb*complex(0,1)*l5*sa1**2*sa2*sa3**2*sb + (ca1*ca3**2*ee**2*complex(0,1)*sa1*sa2*sb**2)/(2.*cw**2) + ca1*ca3**2*complex(0,1)*l2*sa1*sa2*sb**2 - ca1*ca3**2*complex(0,1)*l3*sa1*sa2*sb**2 - ca1*ca3**2*complex(0,1)*l4*sa1*sa2*sb**2 + ca1*ca3**2*complex(0,1)*l5*sa1*sa2*sb**2 + (ca1**2*ca3*ee**2*complex(0,1)*sa3*sb**2)/(2.*cw**2) + ca1**2*ca3*complex(0,1)*l2*sa3*sb**2 - ca2**2*ca3*complex(0,1)*l8*sa3*sb**2 + ca3*complex(0,1)*l3*sa1**2*sa3*sb**2 + ca3*complex(0,1)*l4*sa1**2*sa3*sb**2 - ca3*complex(0,1)*l5*sa1**2*sa3*sb**2 - ca1**2*ca3*complex(0,1)*l3*sa2**2*sa3*sb**2 - ca1**2*ca3*complex(0,1)*l4*sa2**2*sa3*sb**2 + ca1**2*ca3*complex(0,1)*l5*sa2**2*sa3*sb**2 - (ca3*ee**2*complex(0,1)*sa1**2*sa2**2*sa3*sb**2)/(2.*cw**2) - ca3*complex(0,1)*l2*sa1**2*sa2**2*sa3*sb**2 - (ca1*ee**2*complex(0,1)*sa1*sa2*sa3**2*sb**2)/(2.*cw**2) - ca1*complex(0,1)*l2*sa1*sa2*sa3**2*sb**2 + ca1*complex(0,1)*l3*sa1*sa2*sa3**2*sb**2 + ca1*complex(0,1)*l4*sa1*sa2*sa3**2*sb**2 - ca1*complex(0,1)*l5*sa1*sa2*sa3**2*sb**2 - (ca1*ca3**2*cb**2*ee**2*complex(0,1)*sa1*sa2)/(2.*sw**2) + (ca3*cb**2*ee**2*complex(0,1)*sa1**2*sa3)/(2.*sw**2) - (ca1**2*ca3*cb**2*ee**2*complex(0,1)*sa2**2*sa3)/(2.*sw**2) + (ca1*cb**2*ee**2*complex(0,1)*sa1*sa2*sa3**2)/(2.*sw**2) + (ca1**2*ca3**2*cb*ee**2*complex(0,1)*sa2*sb)/(2.*sw**2) - (ca3**2*cb*ee**2*complex(0,1)*sa1**2*sa2*sb)/(2.*sw**2) - (ca1*ca3*cb*ee**2*complex(0,1)*sa1*sa3*sb)/sw**2 - (ca1*ca3*cb*ee**2*complex(0,1)*sa1*sa2**2*sa3*sb)/sw**2 - (ca1**2*cb*ee**2*complex(0,1)*sa2*sa3**2*sb)/(2.*sw**2) + (cb*ee**2*complex(0,1)*sa1**2*sa2*sa3**2*sb)/(2.*sw**2) + (ca1*ca3**2*ee**2*complex(0,1)*sa1*sa2*sb**2)/(2.*sw**2) + (ca1**2*ca3*ee**2*complex(0,1)*sa3*sb**2)/(2.*sw**2) - (ca3*ee**2*complex(0,1)*sa1**2*sa2**2*sa3*sb**2)/(2.*sw**2) - (ca1*ee**2*complex(0,1)*sa1*sa2*sa3**2*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_266 = Coupling(name = 'GC_266',
                  value = 'ca1*ca3**2*cb**2*complex(0,1)*l2*sa1*sa2 - ca1*ca3**2*cb**2*complex(0,1)*l3*sa1*sa2 + ca1**2*ca3*cb**2*complex(0,1)*l2*sa3 - ca2**2*ca3*cb**2*complex(0,1)*l8*sa3 + ca3*cb**2*complex(0,1)*l3*sa1**2*sa3 - ca1**2*ca3*cb**2*complex(0,1)*l3*sa2**2*sa3 - ca3*cb**2*complex(0,1)*l2*sa1**2*sa2**2*sa3 - ca1*cb**2*complex(0,1)*l2*sa1*sa2*sa3**2 + ca1*cb**2*complex(0,1)*l3*sa1*sa2*sa3**2 - ca1**2*ca3**2*cb*complex(0,1)*l4*sa2*sb - ca1**2*ca3**2*cb*complex(0,1)*l5*sa2*sb + ca3**2*cb*complex(0,1)*l4*sa1**2*sa2*sb + ca3**2*cb*complex(0,1)*l5*sa1**2*sa2*sb + 2*ca1*ca3*cb*complex(0,1)*l4*sa1*sa3*sb + 2*ca1*ca3*cb*complex(0,1)*l5*sa1*sa3*sb + 2*ca1*ca3*cb*complex(0,1)*l4*sa1*sa2**2*sa3*sb + 2*ca1*ca3*cb*complex(0,1)*l5*sa1*sa2**2*sa3*sb + ca1**2*cb*complex(0,1)*l4*sa2*sa3**2*sb + ca1**2*cb*complex(0,1)*l5*sa2*sa3**2*sb - cb*complex(0,1)*l4*sa1**2*sa2*sa3**2*sb - cb*complex(0,1)*l5*sa1**2*sa2*sa3**2*sb - ca1*ca3**2*complex(0,1)*l1*sa1*sa2*sb**2 + ca1*ca3**2*complex(0,1)*l3*sa1*sa2*sb**2 + ca1**2*ca3*complex(0,1)*l3*sa3*sb**2 - ca2**2*ca3*complex(0,1)*l7*sa3*sb**2 + ca3*complex(0,1)*l1*sa1**2*sa3*sb**2 - ca1**2*ca3*complex(0,1)*l1*sa2**2*sa3*sb**2 - ca3*complex(0,1)*l3*sa1**2*sa2**2*sa3*sb**2 + ca1*complex(0,1)*l1*sa1*sa2*sa3**2*sb**2 - ca1*complex(0,1)*l3*sa1*sa2*sa3**2*sb**2 + (ca1*ca3**2*cb**2*ee**2*complex(0,1)*sa1*sa2)/(2.*sw**2) + (ca1**2*ca3*cb**2*ee**2*complex(0,1)*sa3)/(2.*sw**2) - (ca3*cb**2*ee**2*complex(0,1)*sa1**2*sa2**2*sa3)/(2.*sw**2) - (ca1*cb**2*ee**2*complex(0,1)*sa1*sa2*sa3**2)/(2.*sw**2) - (ca1**2*ca3**2*cb*ee**2*complex(0,1)*sa2*sb)/(2.*sw**2) + (ca3**2*cb*ee**2*complex(0,1)*sa1**2*sa2*sb)/(2.*sw**2) + (ca1*ca3*cb*ee**2*complex(0,1)*sa1*sa3*sb)/sw**2 + (ca1*ca3*cb*ee**2*complex(0,1)*sa1*sa2**2*sa3*sb)/sw**2 + (ca1**2*cb*ee**2*complex(0,1)*sa2*sa3**2*sb)/(2.*sw**2) - (cb*ee**2*complex(0,1)*sa1**2*sa2*sa3**2*sb)/(2.*sw**2) - (ca1*ca3**2*ee**2*complex(0,1)*sa1*sa2*sb**2)/(2.*sw**2) + (ca3*ee**2*complex(0,1)*sa1**2*sa3*sb**2)/(2.*sw**2) - (ca1**2*ca3*ee**2*complex(0,1)*sa2**2*sa3*sb**2)/(2.*sw**2) + (ca1*ee**2*complex(0,1)*sa1*sa2*sa3**2*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_267 = Coupling(name = 'GC_267',
                  value = '(ca1*ca3**2*cb**2*ee**2*complex(0,1)*sa1*sa2)/(2.*cw**2) + ca1*ca3**2*cb**2*complex(0,1)*l2*sa1*sa2 - ca1*ca3**2*cb**2*complex(0,1)*l3*sa1*sa2 - ca1*ca3**2*cb**2*complex(0,1)*l4*sa1*sa2 + ca1*ca3**2*cb**2*complex(0,1)*l5*sa1*sa2 + (ca1**2*ca3*cb**2*ee**2*complex(0,1)*sa3)/(2.*cw**2) + ca1**2*ca3*cb**2*complex(0,1)*l2*sa3 - ca2**2*ca3*cb**2*complex(0,1)*l8*sa3 + ca3*cb**2*complex(0,1)*l3*sa1**2*sa3 + ca3*cb**2*complex(0,1)*l4*sa1**2*sa3 - ca3*cb**2*complex(0,1)*l5*sa1**2*sa3 - ca1**2*ca3*cb**2*complex(0,1)*l3*sa2**2*sa3 - ca1**2*ca3*cb**2*complex(0,1)*l4*sa2**2*sa3 + ca1**2*ca3*cb**2*complex(0,1)*l5*sa2**2*sa3 - (ca3*cb**2*ee**2*complex(0,1)*sa1**2*sa2**2*sa3)/(2.*cw**2) - ca3*cb**2*complex(0,1)*l2*sa1**2*sa2**2*sa3 - (ca1*cb**2*ee**2*complex(0,1)*sa1*sa2*sa3**2)/(2.*cw**2) - ca1*cb**2*complex(0,1)*l2*sa1*sa2*sa3**2 + ca1*cb**2*complex(0,1)*l3*sa1*sa2*sa3**2 + ca1*cb**2*complex(0,1)*l4*sa1*sa2*sa3**2 - ca1*cb**2*complex(0,1)*l5*sa1*sa2*sa3**2 - (ca1**2*ca3**2*cb*ee**2*complex(0,1)*sa2*sb)/(2.*cw**2) - 2*ca1**2*ca3**2*cb*complex(0,1)*l5*sa2*sb + (ca3**2*cb*ee**2*complex(0,1)*sa1**2*sa2*sb)/(2.*cw**2) + 2*ca3**2*cb*complex(0,1)*l5*sa1**2*sa2*sb + (ca1*ca3*cb*ee**2*complex(0,1)*sa1*sa3*sb)/cw**2 + 4*ca1*ca3*cb*complex(0,1)*l5*sa1*sa3*sb + (ca1*ca3*cb*ee**2*complex(0,1)*sa1*sa2**2*sa3*sb)/cw**2 + 4*ca1*ca3*cb*complex(0,1)*l5*sa1*sa2**2*sa3*sb + (ca1**2*cb*ee**2*complex(0,1)*sa2*sa3**2*sb)/(2.*cw**2) + 2*ca1**2*cb*complex(0,1)*l5*sa2*sa3**2*sb - (cb*ee**2*complex(0,1)*sa1**2*sa2*sa3**2*sb)/(2.*cw**2) - 2*cb*complex(0,1)*l5*sa1**2*sa2*sa3**2*sb - (ca1*ca3**2*ee**2*complex(0,1)*sa1*sa2*sb**2)/(2.*cw**2) - ca1*ca3**2*complex(0,1)*l1*sa1*sa2*sb**2 + ca1*ca3**2*complex(0,1)*l3*sa1*sa2*sb**2 + ca1*ca3**2*complex(0,1)*l4*sa1*sa2*sb**2 - ca1*ca3**2*complex(0,1)*l5*sa1*sa2*sb**2 + ca1**2*ca3*complex(0,1)*l3*sa3*sb**2 + ca1**2*ca3*complex(0,1)*l4*sa3*sb**2 - ca1**2*ca3*complex(0,1)*l5*sa3*sb**2 - ca2**2*ca3*complex(0,1)*l7*sa3*sb**2 + (ca3*ee**2*complex(0,1)*sa1**2*sa3*sb**2)/(2.*cw**2) + ca3*complex(0,1)*l1*sa1**2*sa3*sb**2 - (ca1**2*ca3*ee**2*complex(0,1)*sa2**2*sa3*sb**2)/(2.*cw**2) - ca1**2*ca3*complex(0,1)*l1*sa2**2*sa3*sb**2 - ca3*complex(0,1)*l3*sa1**2*sa2**2*sa3*sb**2 - ca3*complex(0,1)*l4*sa1**2*sa2**2*sa3*sb**2 + ca3*complex(0,1)*l5*sa1**2*sa2**2*sa3*sb**2 + (ca1*ee**2*complex(0,1)*sa1*sa2*sa3**2*sb**2)/(2.*cw**2) + ca1*complex(0,1)*l1*sa1*sa2*sa3**2*sb**2 - ca1*complex(0,1)*l3*sa1*sa2*sa3**2*sb**2 - ca1*complex(0,1)*l4*sa1*sa2*sa3**2*sb**2 + ca1*complex(0,1)*l5*sa1*sa2*sa3**2*sb**2 + (ca1*ca3**2*cb**2*ee**2*complex(0,1)*sa1*sa2)/(2.*sw**2) + (ca1**2*ca3*cb**2*ee**2*complex(0,1)*sa3)/(2.*sw**2) - (ca3*cb**2*ee**2*complex(0,1)*sa1**2*sa2**2*sa3)/(2.*sw**2) - (ca1*cb**2*ee**2*complex(0,1)*sa1*sa2*sa3**2)/(2.*sw**2) - (ca1**2*ca3**2*cb*ee**2*complex(0,1)*sa2*sb)/(2.*sw**2) + (ca3**2*cb*ee**2*complex(0,1)*sa1**2*sa2*sb)/(2.*sw**2) + (ca1*ca3*cb*ee**2*complex(0,1)*sa1*sa3*sb)/sw**2 + (ca1*ca3*cb*ee**2*complex(0,1)*sa1*sa2**2*sa3*sb)/sw**2 + (ca1**2*cb*ee**2*complex(0,1)*sa2*sa3**2*sb)/(2.*sw**2) - (cb*ee**2*complex(0,1)*sa1**2*sa2*sa3**2*sb)/(2.*sw**2) - (ca1*ca3**2*ee**2*complex(0,1)*sa1*sa2*sb**2)/(2.*sw**2) + (ca3*ee**2*complex(0,1)*sa1**2*sa3*sb**2)/(2.*sw**2) - (ca1**2*ca3*ee**2*complex(0,1)*sa2**2*sa3*sb**2)/(2.*sw**2) + (ca1*ee**2*complex(0,1)*sa1*sa2*sa3**2*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_268 = Coupling(name = 'GC_268',
                  value = '(ca1**2*ca3**2*cb**2*complex(0,1)*l4*sa2)/2. + (ca1**2*ca3**2*cb**2*complex(0,1)*l5*sa2)/2. - (ca3**2*cb**2*complex(0,1)*l4*sa1**2*sa2)/2. - (ca3**2*cb**2*complex(0,1)*l5*sa1**2*sa2)/2. - ca1*ca3*cb**2*complex(0,1)*l4*sa1*sa3 - ca1*ca3*cb**2*complex(0,1)*l5*sa1*sa3 - ca1*ca3*cb**2*complex(0,1)*l4*sa1*sa2**2*sa3 - ca1*ca3*cb**2*complex(0,1)*l5*sa1*sa2**2*sa3 - (ca1**2*cb**2*complex(0,1)*l4*sa2*sa3**2)/2. - (ca1**2*cb**2*complex(0,1)*l5*sa2*sa3**2)/2. + (cb**2*complex(0,1)*l4*sa1**2*sa2*sa3**2)/2. + (cb**2*complex(0,1)*l5*sa1**2*sa2*sa3**2)/2. + ca1*ca3**2*cb*complex(0,1)*l1*sa1*sa2*sb + ca1*ca3**2*cb*complex(0,1)*l2*sa1*sa2*sb - 2*ca1*ca3**2*cb*complex(0,1)*l3*sa1*sa2*sb + ca1**2*ca3*cb*complex(0,1)*l2*sa3*sb - ca1**2*ca3*cb*complex(0,1)*l3*sa3*sb + ca2**2*ca3*cb*complex(0,1)*l7*sa3*sb - ca2**2*ca3*cb*complex(0,1)*l8*sa3*sb - ca3*cb*complex(0,1)*l1*sa1**2*sa3*sb + ca3*cb*complex(0,1)*l3*sa1**2*sa3*sb + ca1**2*ca3*cb*complex(0,1)*l1*sa2**2*sa3*sb - ca1**2*ca3*cb*complex(0,1)*l3*sa2**2*sa3*sb - ca3*cb*complex(0,1)*l2*sa1**2*sa2**2*sa3*sb + ca3*cb*complex(0,1)*l3*sa1**2*sa2**2*sa3*sb - ca1*cb*complex(0,1)*l1*sa1*sa2*sa3**2*sb - ca1*cb*complex(0,1)*l2*sa1*sa2*sa3**2*sb + 2*ca1*cb*complex(0,1)*l3*sa1*sa2*sa3**2*sb - (ca1**2*ca3**2*complex(0,1)*l4*sa2*sb**2)/2. - (ca1**2*ca3**2*complex(0,1)*l5*sa2*sb**2)/2. + (ca3**2*complex(0,1)*l4*sa1**2*sa2*sb**2)/2. + (ca3**2*complex(0,1)*l5*sa1**2*sa2*sb**2)/2. + ca1*ca3*complex(0,1)*l4*sa1*sa3*sb**2 + ca1*ca3*complex(0,1)*l5*sa1*sa3*sb**2 + ca1*ca3*complex(0,1)*l4*sa1*sa2**2*sa3*sb**2 + ca1*ca3*complex(0,1)*l5*sa1*sa2**2*sa3*sb**2 + (ca1**2*complex(0,1)*l4*sa2*sa3**2*sb**2)/2. + (ca1**2*complex(0,1)*l5*sa2*sa3**2*sb**2)/2. - (complex(0,1)*l4*sa1**2*sa2*sa3**2*sb**2)/2. - (complex(0,1)*l5*sa1**2*sa2*sa3**2*sb**2)/2. + (ca1**2*ca3**2*cb**2*ee**2*complex(0,1)*sa2)/(4.*sw**2) - (ca3**2*cb**2*ee**2*complex(0,1)*sa1**2*sa2)/(4.*sw**2) - (ca1*ca3*cb**2*ee**2*complex(0,1)*sa1*sa3)/(2.*sw**2) - (ca1*ca3*cb**2*ee**2*complex(0,1)*sa1*sa2**2*sa3)/(2.*sw**2) - (ca1**2*cb**2*ee**2*complex(0,1)*sa2*sa3**2)/(4.*sw**2) + (cb**2*ee**2*complex(0,1)*sa1**2*sa2*sa3**2)/(4.*sw**2) + (ca1*ca3**2*cb*ee**2*complex(0,1)*sa1*sa2*sb)/sw**2 + (ca1**2*ca3*cb*ee**2*complex(0,1)*sa3*sb)/(2.*sw**2) - (ca3*cb*ee**2*complex(0,1)*sa1**2*sa3*sb)/(2.*sw**2) + (ca1**2*ca3*cb*ee**2*complex(0,1)*sa2**2*sa3*sb)/(2.*sw**2) - (ca3*cb*ee**2*complex(0,1)*sa1**2*sa2**2*sa3*sb)/(2.*sw**2) - (ca1*cb*ee**2*complex(0,1)*sa1*sa2*sa3**2*sb)/sw**2 - (ca1**2*ca3**2*ee**2*complex(0,1)*sa2*sb**2)/(4.*sw**2) + (ca3**2*ee**2*complex(0,1)*sa1**2*sa2*sb**2)/(4.*sw**2) + (ca1*ca3*ee**2*complex(0,1)*sa1*sa3*sb**2)/(2.*sw**2) + (ca1*ca3*ee**2*complex(0,1)*sa1*sa2**2*sa3*sb**2)/(2.*sw**2) + (ca1**2*ee**2*complex(0,1)*sa2*sa3**2*sb**2)/(4.*sw**2) - (ee**2*complex(0,1)*sa1**2*sa2*sa3**2*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_269 = Coupling(name = 'GC_269',
                  value = '(ca1**2*ca3**2*cb**2*ee**2*complex(0,1)*sa2)/(4.*cw**2) + ca1**2*ca3**2*cb**2*complex(0,1)*l5*sa2 - (ca3**2*cb**2*ee**2*complex(0,1)*sa1**2*sa2)/(4.*cw**2) - ca3**2*cb**2*complex(0,1)*l5*sa1**2*sa2 - (ca1*ca3*cb**2*ee**2*complex(0,1)*sa1*sa3)/(2.*cw**2) - 2*ca1*ca3*cb**2*complex(0,1)*l5*sa1*sa3 - (ca1*ca3*cb**2*ee**2*complex(0,1)*sa1*sa2**2*sa3)/(2.*cw**2) - 2*ca1*ca3*cb**2*complex(0,1)*l5*sa1*sa2**2*sa3 - (ca1**2*cb**2*ee**2*complex(0,1)*sa2*sa3**2)/(4.*cw**2) - ca1**2*cb**2*complex(0,1)*l5*sa2*sa3**2 + (cb**2*ee**2*complex(0,1)*sa1**2*sa2*sa3**2)/(4.*cw**2) + cb**2*complex(0,1)*l5*sa1**2*sa2*sa3**2 + (ca1*ca3**2*cb*ee**2*complex(0,1)*sa1*sa2*sb)/cw**2 + ca1*ca3**2*cb*complex(0,1)*l1*sa1*sa2*sb + ca1*ca3**2*cb*complex(0,1)*l2*sa1*sa2*sb - 2*ca1*ca3**2*cb*complex(0,1)*l3*sa1*sa2*sb - 2*ca1*ca3**2*cb*complex(0,1)*l4*sa1*sa2*sb + 2*ca1*ca3**2*cb*complex(0,1)*l5*sa1*sa2*sb + (ca1**2*ca3*cb*ee**2*complex(0,1)*sa3*sb)/(2.*cw**2) + ca1**2*ca3*cb*complex(0,1)*l2*sa3*sb - ca1**2*ca3*cb*complex(0,1)*l3*sa3*sb - ca1**2*ca3*cb*complex(0,1)*l4*sa3*sb + ca1**2*ca3*cb*complex(0,1)*l5*sa3*sb + ca2**2*ca3*cb*complex(0,1)*l7*sa3*sb - ca2**2*ca3*cb*complex(0,1)*l8*sa3*sb - (ca3*cb*ee**2*complex(0,1)*sa1**2*sa3*sb)/(2.*cw**2) - ca3*cb*complex(0,1)*l1*sa1**2*sa3*sb + ca3*cb*complex(0,1)*l3*sa1**2*sa3*sb + ca3*cb*complex(0,1)*l4*sa1**2*sa3*sb - ca3*cb*complex(0,1)*l5*sa1**2*sa3*sb + (ca1**2*ca3*cb*ee**2*complex(0,1)*sa2**2*sa3*sb)/(2.*cw**2) + ca1**2*ca3*cb*complex(0,1)*l1*sa2**2*sa3*sb - ca1**2*ca3*cb*complex(0,1)*l3*sa2**2*sa3*sb - ca1**2*ca3*cb*complex(0,1)*l4*sa2**2*sa3*sb + ca1**2*ca3*cb*complex(0,1)*l5*sa2**2*sa3*sb - (ca3*cb*ee**2*complex(0,1)*sa1**2*sa2**2*sa3*sb)/(2.*cw**2) - ca3*cb*complex(0,1)*l2*sa1**2*sa2**2*sa3*sb + ca3*cb*complex(0,1)*l3*sa1**2*sa2**2*sa3*sb + ca3*cb*complex(0,1)*l4*sa1**2*sa2**2*sa3*sb - ca3*cb*complex(0,1)*l5*sa1**2*sa2**2*sa3*sb - (ca1*cb*ee**2*complex(0,1)*sa1*sa2*sa3**2*sb)/cw**2 - ca1*cb*complex(0,1)*l1*sa1*sa2*sa3**2*sb - ca1*cb*complex(0,1)*l2*sa1*sa2*sa3**2*sb + 2*ca1*cb*complex(0,1)*l3*sa1*sa2*sa3**2*sb + 2*ca1*cb*complex(0,1)*l4*sa1*sa2*sa3**2*sb - 2*ca1*cb*complex(0,1)*l5*sa1*sa2*sa3**2*sb - (ca1**2*ca3**2*ee**2*complex(0,1)*sa2*sb**2)/(4.*cw**2) - ca1**2*ca3**2*complex(0,1)*l5*sa2*sb**2 + (ca3**2*ee**2*complex(0,1)*sa1**2*sa2*sb**2)/(4.*cw**2) + ca3**2*complex(0,1)*l5*sa1**2*sa2*sb**2 + (ca1*ca3*ee**2*complex(0,1)*sa1*sa3*sb**2)/(2.*cw**2) + 2*ca1*ca3*complex(0,1)*l5*sa1*sa3*sb**2 + (ca1*ca3*ee**2*complex(0,1)*sa1*sa2**2*sa3*sb**2)/(2.*cw**2) + 2*ca1*ca3*complex(0,1)*l5*sa1*sa2**2*sa3*sb**2 + (ca1**2*ee**2*complex(0,1)*sa2*sa3**2*sb**2)/(4.*cw**2) + ca1**2*complex(0,1)*l5*sa2*sa3**2*sb**2 - (ee**2*complex(0,1)*sa1**2*sa2*sa3**2*sb**2)/(4.*cw**2) - complex(0,1)*l5*sa1**2*sa2*sa3**2*sb**2 + (ca1**2*ca3**2*cb**2*ee**2*complex(0,1)*sa2)/(4.*sw**2) - (ca3**2*cb**2*ee**2*complex(0,1)*sa1**2*sa2)/(4.*sw**2) - (ca1*ca3*cb**2*ee**2*complex(0,1)*sa1*sa3)/(2.*sw**2) - (ca1*ca3*cb**2*ee**2*complex(0,1)*sa1*sa2**2*sa3)/(2.*sw**2) - (ca1**2*cb**2*ee**2*complex(0,1)*sa2*sa3**2)/(4.*sw**2) + (cb**2*ee**2*complex(0,1)*sa1**2*sa2*sa3**2)/(4.*sw**2) + (ca1*ca3**2*cb*ee**2*complex(0,1)*sa1*sa2*sb)/sw**2 + (ca1**2*ca3*cb*ee**2*complex(0,1)*sa3*sb)/(2.*sw**2) - (ca3*cb*ee**2*complex(0,1)*sa1**2*sa3*sb)/(2.*sw**2) + (ca1**2*ca3*cb*ee**2*complex(0,1)*sa2**2*sa3*sb)/(2.*sw**2) - (ca3*cb*ee**2*complex(0,1)*sa1**2*sa2**2*sa3*sb)/(2.*sw**2) - (ca1*cb*ee**2*complex(0,1)*sa1*sa2*sa3**2*sb)/sw**2 - (ca1**2*ca3**2*ee**2*complex(0,1)*sa2*sb**2)/(4.*sw**2) + (ca3**2*ee**2*complex(0,1)*sa1**2*sa2*sb**2)/(4.*sw**2) + (ca1*ca3*ee**2*complex(0,1)*sa1*sa3*sb**2)/(2.*sw**2) + (ca1*ca3*ee**2*complex(0,1)*sa1*sa2**2*sa3*sb**2)/(2.*sw**2) + (ca1**2*ee**2*complex(0,1)*sa2*sa3**2*sb**2)/(4.*sw**2) - (ee**2*complex(0,1)*sa1**2*sa2*sa3**2*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_270 = Coupling(name = 'GC_270',
                  value = '(ca1**2*ca3**2*cb**2*complex(0,1)*l4*sa2)/2. + (ca1**2*ca3**2*cb**2*complex(0,1)*l5*sa2)/2. - (ca3**2*cb**2*complex(0,1)*l4*sa1**2*sa2)/2. - (ca3**2*cb**2*complex(0,1)*l5*sa1**2*sa2)/2. - ca1*ca3*cb**2*complex(0,1)*l4*sa1*sa3 - ca1*ca3*cb**2*complex(0,1)*l5*sa1*sa3 - ca1*ca3*cb**2*complex(0,1)*l4*sa1*sa2**2*sa3 - ca1*ca3*cb**2*complex(0,1)*l5*sa1*sa2**2*sa3 - (ca1**2*cb**2*complex(0,1)*l4*sa2*sa3**2)/2. - (ca1**2*cb**2*complex(0,1)*l5*sa2*sa3**2)/2. + (cb**2*complex(0,1)*l4*sa1**2*sa2*sa3**2)/2. + (cb**2*complex(0,1)*l5*sa1**2*sa2*sa3**2)/2. + ca1*ca3**2*cb*complex(0,1)*l1*sa1*sa2*sb + ca1*ca3**2*cb*complex(0,1)*l2*sa1*sa2*sb - 2*ca1*ca3**2*cb*complex(0,1)*l3*sa1*sa2*sb + ca1**2*ca3*cb*complex(0,1)*l2*sa3*sb - ca1**2*ca3*cb*complex(0,1)*l3*sa3*sb + ca2**2*ca3*cb*complex(0,1)*l7*sa3*sb - ca2**2*ca3*cb*complex(0,1)*l8*sa3*sb - ca3*cb*complex(0,1)*l1*sa1**2*sa3*sb + ca3*cb*complex(0,1)*l3*sa1**2*sa3*sb + ca1**2*ca3*cb*complex(0,1)*l1*sa2**2*sa3*sb - ca1**2*ca3*cb*complex(0,1)*l3*sa2**2*sa3*sb - ca3*cb*complex(0,1)*l2*sa1**2*sa2**2*sa3*sb + ca3*cb*complex(0,1)*l3*sa1**2*sa2**2*sa3*sb - ca1*cb*complex(0,1)*l1*sa1*sa2*sa3**2*sb - ca1*cb*complex(0,1)*l2*sa1*sa2*sa3**2*sb + 2*ca1*cb*complex(0,1)*l3*sa1*sa2*sa3**2*sb - (ca1**2*ca3**2*complex(0,1)*l4*sa2*sb**2)/2. - (ca1**2*ca3**2*complex(0,1)*l5*sa2*sb**2)/2. + (ca3**2*complex(0,1)*l4*sa1**2*sa2*sb**2)/2. + (ca3**2*complex(0,1)*l5*sa1**2*sa2*sb**2)/2. + ca1*ca3*complex(0,1)*l4*sa1*sa3*sb**2 + ca1*ca3*complex(0,1)*l5*sa1*sa3*sb**2 + ca1*ca3*complex(0,1)*l4*sa1*sa2**2*sa3*sb**2 + ca1*ca3*complex(0,1)*l5*sa1*sa2**2*sa3*sb**2 + (ca1**2*complex(0,1)*l4*sa2*sa3**2*sb**2)/2. + (ca1**2*complex(0,1)*l5*sa2*sa3**2*sb**2)/2. - (complex(0,1)*l4*sa1**2*sa2*sa3**2*sb**2)/2. - (complex(0,1)*l5*sa1**2*sa2*sa3**2*sb**2)/2. + (ca3**2*cb**2*ee**2*complex(0,1)*sa1**2*sa2)/(4.*sw**2) + (ca1*ca3*cb**2*ee**2*complex(0,1)*sa1*sa3)/(4.*sw**2) + (ca1*ca3*cb**2*ee**2*complex(0,1)*sa1*sa2**2*sa3)/(4.*sw**2) + (ca1**2*cb**2*ee**2*complex(0,1)*sa2*sa3**2)/(4.*sw**2) - (ca1*ca3**2*cb*ee**2*complex(0,1)*sa1*sa2*sb)/(2.*sw**2) - (ca1**2*ca3*cb*ee**2*complex(0,1)*sa3*sb)/(4.*sw**2) + (ca3*cb*ee**2*complex(0,1)*sa1**2*sa3*sb)/(4.*sw**2) - (ca1**2*ca3*cb*ee**2*complex(0,1)*sa2**2*sa3*sb)/(4.*sw**2) + (ca3*cb*ee**2*complex(0,1)*sa1**2*sa2**2*sa3*sb)/(4.*sw**2) + (ca1*cb*ee**2*complex(0,1)*sa1*sa2*sa3**2*sb)/(2.*sw**2) + (ca1**2*ca3**2*ee**2*complex(0,1)*sa2*sb**2)/(4.*sw**2) - (ca1*ca3*ee**2*complex(0,1)*sa1*sa3*sb**2)/(4.*sw**2) - (ca1*ca3*ee**2*complex(0,1)*sa1*sa2**2*sa3*sb**2)/(4.*sw**2) + (ee**2*complex(0,1)*sa1**2*sa2*sa3**2*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_271 = Coupling(name = 'GC_271',
                  value = 'ca1**2*ca3**2*cb**2*complex(0,1)*l5*sa2 + (ca3**2*cb**2*ee**2*complex(0,1)*sa1**2*sa2)/(4.*cw**2) - ca3**2*cb**2*complex(0,1)*l5*sa1**2*sa2 + (ca1*ca3*cb**2*ee**2*complex(0,1)*sa1*sa3)/(4.*cw**2) - 2*ca1*ca3*cb**2*complex(0,1)*l5*sa1*sa3 + (ca1*ca3*cb**2*ee**2*complex(0,1)*sa1*sa2**2*sa3)/(4.*cw**2) - 2*ca1*ca3*cb**2*complex(0,1)*l5*sa1*sa2**2*sa3 + (ca1**2*cb**2*ee**2*complex(0,1)*sa2*sa3**2)/(4.*cw**2) - ca1**2*cb**2*complex(0,1)*l5*sa2*sa3**2 + cb**2*complex(0,1)*l5*sa1**2*sa2*sa3**2 - (ca1*ca3**2*cb*ee**2*complex(0,1)*sa1*sa2*sb)/(2.*cw**2) + ca1*ca3**2*cb*complex(0,1)*l1*sa1*sa2*sb + ca1*ca3**2*cb*complex(0,1)*l2*sa1*sa2*sb - 2*ca1*ca3**2*cb*complex(0,1)*l3*sa1*sa2*sb - 2*ca1*ca3**2*cb*complex(0,1)*l4*sa1*sa2*sb + 2*ca1*ca3**2*cb*complex(0,1)*l5*sa1*sa2*sb - (ca1**2*ca3*cb*ee**2*complex(0,1)*sa3*sb)/(4.*cw**2) + ca1**2*ca3*cb*complex(0,1)*l2*sa3*sb - ca1**2*ca3*cb*complex(0,1)*l3*sa3*sb - ca1**2*ca3*cb*complex(0,1)*l4*sa3*sb + ca1**2*ca3*cb*complex(0,1)*l5*sa3*sb + ca2**2*ca3*cb*complex(0,1)*l7*sa3*sb - ca2**2*ca3*cb*complex(0,1)*l8*sa3*sb + (ca3*cb*ee**2*complex(0,1)*sa1**2*sa3*sb)/(4.*cw**2) - ca3*cb*complex(0,1)*l1*sa1**2*sa3*sb + ca3*cb*complex(0,1)*l3*sa1**2*sa3*sb + ca3*cb*complex(0,1)*l4*sa1**2*sa3*sb - ca3*cb*complex(0,1)*l5*sa1**2*sa3*sb - (ca1**2*ca3*cb*ee**2*complex(0,1)*sa2**2*sa3*sb)/(4.*cw**2) + ca1**2*ca3*cb*complex(0,1)*l1*sa2**2*sa3*sb - ca1**2*ca3*cb*complex(0,1)*l3*sa2**2*sa3*sb - ca1**2*ca3*cb*complex(0,1)*l4*sa2**2*sa3*sb + ca1**2*ca3*cb*complex(0,1)*l5*sa2**2*sa3*sb + (ca3*cb*ee**2*complex(0,1)*sa1**2*sa2**2*sa3*sb)/(4.*cw**2) - ca3*cb*complex(0,1)*l2*sa1**2*sa2**2*sa3*sb + ca3*cb*complex(0,1)*l3*sa1**2*sa2**2*sa3*sb + ca3*cb*complex(0,1)*l4*sa1**2*sa2**2*sa3*sb - ca3*cb*complex(0,1)*l5*sa1**2*sa2**2*sa3*sb + (ca1*cb*ee**2*complex(0,1)*sa1*sa2*sa3**2*sb)/(2.*cw**2) - ca1*cb*complex(0,1)*l1*sa1*sa2*sa3**2*sb - ca1*cb*complex(0,1)*l2*sa1*sa2*sa3**2*sb + 2*ca1*cb*complex(0,1)*l3*sa1*sa2*sa3**2*sb + 2*ca1*cb*complex(0,1)*l4*sa1*sa2*sa3**2*sb - 2*ca1*cb*complex(0,1)*l5*sa1*sa2*sa3**2*sb + (ca1**2*ca3**2*ee**2*complex(0,1)*sa2*sb**2)/(4.*cw**2) - ca1**2*ca3**2*complex(0,1)*l5*sa2*sb**2 + ca3**2*complex(0,1)*l5*sa1**2*sa2*sb**2 - (ca1*ca3*ee**2*complex(0,1)*sa1*sa3*sb**2)/(4.*cw**2) + 2*ca1*ca3*complex(0,1)*l5*sa1*sa3*sb**2 - (ca1*ca3*ee**2*complex(0,1)*sa1*sa2**2*sa3*sb**2)/(4.*cw**2) + 2*ca1*ca3*complex(0,1)*l5*sa1*sa2**2*sa3*sb**2 + ca1**2*complex(0,1)*l5*sa2*sa3**2*sb**2 + (ee**2*complex(0,1)*sa1**2*sa2*sa3**2*sb**2)/(4.*cw**2) - complex(0,1)*l5*sa1**2*sa2*sa3**2*sb**2 + (ca3**2*cb**2*ee**2*complex(0,1)*sa1**2*sa2)/(4.*sw**2) + (ca1*ca3*cb**2*ee**2*complex(0,1)*sa1*sa3)/(4.*sw**2) + (ca1*ca3*cb**2*ee**2*complex(0,1)*sa1*sa2**2*sa3)/(4.*sw**2) + (ca1**2*cb**2*ee**2*complex(0,1)*sa2*sa3**2)/(4.*sw**2) - (ca1*ca3**2*cb*ee**2*complex(0,1)*sa1*sa2*sb)/(2.*sw**2) - (ca1**2*ca3*cb*ee**2*complex(0,1)*sa3*sb)/(4.*sw**2) + (ca3*cb*ee**2*complex(0,1)*sa1**2*sa3*sb)/(4.*sw**2) - (ca1**2*ca3*cb*ee**2*complex(0,1)*sa2**2*sa3*sb)/(4.*sw**2) + (ca3*cb*ee**2*complex(0,1)*sa1**2*sa2**2*sa3*sb)/(4.*sw**2) + (ca1*cb*ee**2*complex(0,1)*sa1*sa2*sa3**2*sb)/(2.*sw**2) + (ca1**2*ca3**2*ee**2*complex(0,1)*sa2*sb**2)/(4.*sw**2) - (ca1*ca3*ee**2*complex(0,1)*sa1*sa3*sb**2)/(4.*sw**2) - (ca1*ca3*ee**2*complex(0,1)*sa1*sa2**2*sa3*sb**2)/(4.*sw**2) + (ee**2*complex(0,1)*sa1**2*sa2*sa3**2*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_272 = Coupling(name = 'GC_272',
                  value = '-(ca1**2*ca3**2*cb**2*complex(0,1)*l2) - ca3**2*cb**2*complex(0,1)*l3*sa1**2 + 2*ca1*ca3*cb**2*complex(0,1)*l2*sa1*sa2*sa3 - 2*ca1*ca3*cb**2*complex(0,1)*l3*sa1*sa2*sa3 - ca2**2*cb**2*complex(0,1)*l8*sa3**2 - ca1**2*cb**2*complex(0,1)*l3*sa2**2*sa3**2 - cb**2*complex(0,1)*l2*sa1**2*sa2**2*sa3**2 - 2*ca1*ca3**2*cb*complex(0,1)*l4*sa1*sb - 2*ca1*ca3**2*cb*complex(0,1)*l5*sa1*sb - 2*ca1**2*ca3*cb*complex(0,1)*l4*sa2*sa3*sb - 2*ca1**2*ca3*cb*complex(0,1)*l5*sa2*sa3*sb + 2*ca3*cb*complex(0,1)*l4*sa1**2*sa2*sa3*sb + 2*ca3*cb*complex(0,1)*l5*sa1**2*sa2*sa3*sb + 2*ca1*cb*complex(0,1)*l4*sa1*sa2**2*sa3**2*sb + 2*ca1*cb*complex(0,1)*l5*sa1*sa2**2*sa3**2*sb - ca1**2*ca3**2*complex(0,1)*l3*sb**2 - ca3**2*complex(0,1)*l1*sa1**2*sb**2 - 2*ca1*ca3*complex(0,1)*l1*sa1*sa2*sa3*sb**2 + 2*ca1*ca3*complex(0,1)*l3*sa1*sa2*sa3*sb**2 - ca2**2*complex(0,1)*l7*sa3**2*sb**2 - ca1**2*complex(0,1)*l1*sa2**2*sa3**2*sb**2 - complex(0,1)*l3*sa1**2*sa2**2*sa3**2*sb**2 + (ca1**2*ca3**2*cb**2*ee**2*complex(0,1))/(4.*sw**2) - (ca1*ca3*cb**2*ee**2*complex(0,1)*sa1*sa2*sa3)/(2.*sw**2) + (cb**2*ee**2*complex(0,1)*sa1**2*sa2**2*sa3**2)/(4.*sw**2) + (ca1*ca3**2*cb*ee**2*complex(0,1)*sa1*sb)/(2.*sw**2) + (ca1**2*ca3*cb*ee**2*complex(0,1)*sa2*sa3*sb)/(2.*sw**2) - (ca3*cb*ee**2*complex(0,1)*sa1**2*sa2*sa3*sb)/(2.*sw**2) - (ca1*cb*ee**2*complex(0,1)*sa1*sa2**2*sa3**2*sb)/(2.*sw**2) + (ca3**2*ee**2*complex(0,1)*sa1**2*sb**2)/(4.*sw**2) + (ca1*ca3*ee**2*complex(0,1)*sa1*sa2*sa3*sb**2)/(2.*sw**2) + (ca1**2*ee**2*complex(0,1)*sa2**2*sa3**2*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_273 = Coupling(name = 'GC_273',
                  value = '(ca1**2*ca3**2*cb**2*ee**2*complex(0,1))/(4.*cw**2) - ca1**2*ca3**2*cb**2*complex(0,1)*l2 - ca3**2*cb**2*complex(0,1)*l3*sa1**2 - ca3**2*cb**2*complex(0,1)*l4*sa1**2 + ca3**2*cb**2*complex(0,1)*l5*sa1**2 - (ca1*ca3*cb**2*ee**2*complex(0,1)*sa1*sa2*sa3)/(2.*cw**2) + 2*ca1*ca3*cb**2*complex(0,1)*l2*sa1*sa2*sa3 - 2*ca1*ca3*cb**2*complex(0,1)*l3*sa1*sa2*sa3 - 2*ca1*ca3*cb**2*complex(0,1)*l4*sa1*sa2*sa3 + 2*ca1*ca3*cb**2*complex(0,1)*l5*sa1*sa2*sa3 - ca2**2*cb**2*complex(0,1)*l8*sa3**2 - ca1**2*cb**2*complex(0,1)*l3*sa2**2*sa3**2 - ca1**2*cb**2*complex(0,1)*l4*sa2**2*sa3**2 + ca1**2*cb**2*complex(0,1)*l5*sa2**2*sa3**2 + (cb**2*ee**2*complex(0,1)*sa1**2*sa2**2*sa3**2)/(4.*cw**2) - cb**2*complex(0,1)*l2*sa1**2*sa2**2*sa3**2 + (ca1*ca3**2*cb*ee**2*complex(0,1)*sa1*sb)/(2.*cw**2) - 4*ca1*ca3**2*cb*complex(0,1)*l5*sa1*sb + (ca1**2*ca3*cb*ee**2*complex(0,1)*sa2*sa3*sb)/(2.*cw**2) - 4*ca1**2*ca3*cb*complex(0,1)*l5*sa2*sa3*sb - (ca3*cb*ee**2*complex(0,1)*sa1**2*sa2*sa3*sb)/(2.*cw**2) + 4*ca3*cb*complex(0,1)*l5*sa1**2*sa2*sa3*sb - (ca1*cb*ee**2*complex(0,1)*sa1*sa2**2*sa3**2*sb)/(2.*cw**2) + 4*ca1*cb*complex(0,1)*l5*sa1*sa2**2*sa3**2*sb - ca1**2*ca3**2*complex(0,1)*l3*sb**2 - ca1**2*ca3**2*complex(0,1)*l4*sb**2 + ca1**2*ca3**2*complex(0,1)*l5*sb**2 + (ca3**2*ee**2*complex(0,1)*sa1**2*sb**2)/(4.*cw**2) - ca3**2*complex(0,1)*l1*sa1**2*sb**2 + (ca1*ca3*ee**2*complex(0,1)*sa1*sa2*sa3*sb**2)/(2.*cw**2) - 2*ca1*ca3*complex(0,1)*l1*sa1*sa2*sa3*sb**2 + 2*ca1*ca3*complex(0,1)*l3*sa1*sa2*sa3*sb**2 + 2*ca1*ca3*complex(0,1)*l4*sa1*sa2*sa3*sb**2 - 2*ca1*ca3*complex(0,1)*l5*sa1*sa2*sa3*sb**2 - ca2**2*complex(0,1)*l7*sa3**2*sb**2 + (ca1**2*ee**2*complex(0,1)*sa2**2*sa3**2*sb**2)/(4.*cw**2) - ca1**2*complex(0,1)*l1*sa2**2*sa3**2*sb**2 - complex(0,1)*l3*sa1**2*sa2**2*sa3**2*sb**2 - complex(0,1)*l4*sa1**2*sa2**2*sa3**2*sb**2 + complex(0,1)*l5*sa1**2*sa2**2*sa3**2*sb**2 + (ca1**2*ca3**2*cb**2*ee**2*complex(0,1))/(4.*sw**2) - (ca1*ca3*cb**2*ee**2*complex(0,1)*sa1*sa2*sa3)/(2.*sw**2) + (cb**2*ee**2*complex(0,1)*sa1**2*sa2**2*sa3**2)/(4.*sw**2) + (ca1*ca3**2*cb*ee**2*complex(0,1)*sa1*sb)/(2.*sw**2) + (ca1**2*ca3*cb*ee**2*complex(0,1)*sa2*sa3*sb)/(2.*sw**2) - (ca3*cb*ee**2*complex(0,1)*sa1**2*sa2*sa3*sb)/(2.*sw**2) - (ca1*cb*ee**2*complex(0,1)*sa1*sa2**2*sa3**2*sb)/(2.*sw**2) + (ca3**2*ee**2*complex(0,1)*sa1**2*sb**2)/(4.*sw**2) + (ca1*ca3*ee**2*complex(0,1)*sa1*sa2*sa3*sb**2)/(2.*sw**2) + (ca1**2*ee**2*complex(0,1)*sa2**2*sa3**2*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_274 = Coupling(name = 'GC_274',
                  value = '-(ca1**2*ca3**2*cb**2*complex(0,1)*l2) - ca3**2*cb**2*complex(0,1)*l3*sa1**2 + 2*ca1*ca3*cb**2*complex(0,1)*l2*sa1*sa2*sa3 - 2*ca1*ca3*cb**2*complex(0,1)*l3*sa1*sa2*sa3 - ca2**2*cb**2*complex(0,1)*l8*sa3**2 - ca1**2*cb**2*complex(0,1)*l3*sa2**2*sa3**2 - cb**2*complex(0,1)*l2*sa1**2*sa2**2*sa3**2 - 2*ca1*ca3**2*cb*complex(0,1)*l4*sa1*sb - 2*ca1*ca3**2*cb*complex(0,1)*l5*sa1*sb - 2*ca1**2*ca3*cb*complex(0,1)*l4*sa2*sa3*sb - 2*ca1**2*ca3*cb*complex(0,1)*l5*sa2*sa3*sb + 2*ca3*cb*complex(0,1)*l4*sa1**2*sa2*sa3*sb + 2*ca3*cb*complex(0,1)*l5*sa1**2*sa2*sa3*sb + 2*ca1*cb*complex(0,1)*l4*sa1*sa2**2*sa3**2*sb + 2*ca1*cb*complex(0,1)*l5*sa1*sa2**2*sa3**2*sb - ca1**2*ca3**2*complex(0,1)*l3*sb**2 - ca3**2*complex(0,1)*l1*sa1**2*sb**2 - 2*ca1*ca3*complex(0,1)*l1*sa1*sa2*sa3*sb**2 + 2*ca1*ca3*complex(0,1)*l3*sa1*sa2*sa3*sb**2 - ca2**2*complex(0,1)*l7*sa3**2*sb**2 - ca1**2*complex(0,1)*l1*sa2**2*sa3**2*sb**2 - complex(0,1)*l3*sa1**2*sa2**2*sa3**2*sb**2 - (ca1**2*ca3**2*cb**2*ee**2*complex(0,1))/(2.*sw**2) + (ca1*ca3*cb**2*ee**2*complex(0,1)*sa1*sa2*sa3)/sw**2 - (cb**2*ee**2*complex(0,1)*sa1**2*sa2**2*sa3**2)/(2.*sw**2) - (ca1*ca3**2*cb*ee**2*complex(0,1)*sa1*sb)/sw**2 - (ca1**2*ca3*cb*ee**2*complex(0,1)*sa2*sa3*sb)/sw**2 + (ca3*cb*ee**2*complex(0,1)*sa1**2*sa2*sa3*sb)/sw**2 + (ca1*cb*ee**2*complex(0,1)*sa1*sa2**2*sa3**2*sb)/sw**2 - (ca3**2*ee**2*complex(0,1)*sa1**2*sb**2)/(2.*sw**2) - (ca1*ca3*ee**2*complex(0,1)*sa1*sa2*sa3*sb**2)/sw**2 - (ca1**2*ee**2*complex(0,1)*sa2**2*sa3**2*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_275 = Coupling(name = 'GC_275',
                  value = '-(ca1**2*ca3**2*cb**2*ee**2*complex(0,1))/(2.*cw**2) - ca1**2*ca3**2*cb**2*complex(0,1)*l2 - ca3**2*cb**2*complex(0,1)*l3*sa1**2 - ca3**2*cb**2*complex(0,1)*l4*sa1**2 + ca3**2*cb**2*complex(0,1)*l5*sa1**2 + (ca1*ca3*cb**2*ee**2*complex(0,1)*sa1*sa2*sa3)/cw**2 + 2*ca1*ca3*cb**2*complex(0,1)*l2*sa1*sa2*sa3 - 2*ca1*ca3*cb**2*complex(0,1)*l3*sa1*sa2*sa3 - 2*ca1*ca3*cb**2*complex(0,1)*l4*sa1*sa2*sa3 + 2*ca1*ca3*cb**2*complex(0,1)*l5*sa1*sa2*sa3 - ca2**2*cb**2*complex(0,1)*l8*sa3**2 - ca1**2*cb**2*complex(0,1)*l3*sa2**2*sa3**2 - ca1**2*cb**2*complex(0,1)*l4*sa2**2*sa3**2 + ca1**2*cb**2*complex(0,1)*l5*sa2**2*sa3**2 - (cb**2*ee**2*complex(0,1)*sa1**2*sa2**2*sa3**2)/(2.*cw**2) - cb**2*complex(0,1)*l2*sa1**2*sa2**2*sa3**2 - (ca1*ca3**2*cb*ee**2*complex(0,1)*sa1*sb)/cw**2 - 4*ca1*ca3**2*cb*complex(0,1)*l5*sa1*sb - (ca1**2*ca3*cb*ee**2*complex(0,1)*sa2*sa3*sb)/cw**2 - 4*ca1**2*ca3*cb*complex(0,1)*l5*sa2*sa3*sb + (ca3*cb*ee**2*complex(0,1)*sa1**2*sa2*sa3*sb)/cw**2 + 4*ca3*cb*complex(0,1)*l5*sa1**2*sa2*sa3*sb + (ca1*cb*ee**2*complex(0,1)*sa1*sa2**2*sa3**2*sb)/cw**2 + 4*ca1*cb*complex(0,1)*l5*sa1*sa2**2*sa3**2*sb - ca1**2*ca3**2*complex(0,1)*l3*sb**2 - ca1**2*ca3**2*complex(0,1)*l4*sb**2 + ca1**2*ca3**2*complex(0,1)*l5*sb**2 - (ca3**2*ee**2*complex(0,1)*sa1**2*sb**2)/(2.*cw**2) - ca3**2*complex(0,1)*l1*sa1**2*sb**2 - (ca1*ca3*ee**2*complex(0,1)*sa1*sa2*sa3*sb**2)/cw**2 - 2*ca1*ca3*complex(0,1)*l1*sa1*sa2*sa3*sb**2 + 2*ca1*ca3*complex(0,1)*l3*sa1*sa2*sa3*sb**2 + 2*ca1*ca3*complex(0,1)*l4*sa1*sa2*sa3*sb**2 - 2*ca1*ca3*complex(0,1)*l5*sa1*sa2*sa3*sb**2 - ca2**2*complex(0,1)*l7*sa3**2*sb**2 - (ca1**2*ee**2*complex(0,1)*sa2**2*sa3**2*sb**2)/(2.*cw**2) - ca1**2*complex(0,1)*l1*sa2**2*sa3**2*sb**2 - complex(0,1)*l3*sa1**2*sa2**2*sa3**2*sb**2 - complex(0,1)*l4*sa1**2*sa2**2*sa3**2*sb**2 + complex(0,1)*l5*sa1**2*sa2**2*sa3**2*sb**2 - (ca1**2*ca3**2*cb**2*ee**2*complex(0,1))/(2.*sw**2) + (ca1*ca3*cb**2*ee**2*complex(0,1)*sa1*sa2*sa3)/sw**2 - (cb**2*ee**2*complex(0,1)*sa1**2*sa2**2*sa3**2)/(2.*sw**2) - (ca1*ca3**2*cb*ee**2*complex(0,1)*sa1*sb)/sw**2 - (ca1**2*ca3*cb*ee**2*complex(0,1)*sa2*sa3*sb)/sw**2 + (ca3*cb*ee**2*complex(0,1)*sa1**2*sa2*sa3*sb)/sw**2 + (ca1*cb*ee**2*complex(0,1)*sa1*sa2**2*sa3**2*sb)/sw**2 - (ca3**2*ee**2*complex(0,1)*sa1**2*sb**2)/(2.*sw**2) - (ca1*ca3*ee**2*complex(0,1)*sa1*sa2*sa3*sb**2)/sw**2 - (ca1**2*ee**2*complex(0,1)*sa2**2*sa3**2*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_276 = Coupling(name = 'GC_276',
                  value = 'ca1*ca3**2*cb**2*complex(0,1)*l4*sa1 + ca1*ca3**2*cb**2*complex(0,1)*l5*sa1 + ca1**2*ca3*cb**2*complex(0,1)*l4*sa2*sa3 + ca1**2*ca3*cb**2*complex(0,1)*l5*sa2*sa3 - ca3*cb**2*complex(0,1)*l4*sa1**2*sa2*sa3 - ca3*cb**2*complex(0,1)*l5*sa1**2*sa2*sa3 - ca1*cb**2*complex(0,1)*l4*sa1*sa2**2*sa3**2 - ca1*cb**2*complex(0,1)*l5*sa1*sa2**2*sa3**2 - ca1**2*ca3**2*cb*complex(0,1)*l2*sb + ca1**2*ca3**2*cb*complex(0,1)*l3*sb + ca3**2*cb*complex(0,1)*l1*sa1**2*sb - ca3**2*cb*complex(0,1)*l3*sa1**2*sb + 2*ca1*ca3*cb*complex(0,1)*l1*sa1*sa2*sa3*sb + 2*ca1*ca3*cb*complex(0,1)*l2*sa1*sa2*sa3*sb - 4*ca1*ca3*cb*complex(0,1)*l3*sa1*sa2*sa3*sb + ca2**2*cb*complex(0,1)*l7*sa3**2*sb - ca2**2*cb*complex(0,1)*l8*sa3**2*sb + ca1**2*cb*complex(0,1)*l1*sa2**2*sa3**2*sb - ca1**2*cb*complex(0,1)*l3*sa2**2*sa3**2*sb - cb*complex(0,1)*l2*sa1**2*sa2**2*sa3**2*sb + cb*complex(0,1)*l3*sa1**2*sa2**2*sa3**2*sb - ca1*ca3**2*complex(0,1)*l4*sa1*sb**2 - ca1*ca3**2*complex(0,1)*l5*sa1*sb**2 - ca1**2*ca3*complex(0,1)*l4*sa2*sa3*sb**2 - ca1**2*ca3*complex(0,1)*l5*sa2*sa3*sb**2 + ca3*complex(0,1)*l4*sa1**2*sa2*sa3*sb**2 + ca3*complex(0,1)*l5*sa1**2*sa2*sa3*sb**2 + ca1*complex(0,1)*l4*sa1*sa2**2*sa3**2*sb**2 + ca1*complex(0,1)*l5*sa1*sa2**2*sa3**2*sb**2 - (ca1*ca3**2*cb**2*ee**2*complex(0,1)*sa1)/(4.*sw**2) - (ca1**2*ca3*cb**2*ee**2*complex(0,1)*sa2*sa3)/(4.*sw**2) + (ca3*cb**2*ee**2*complex(0,1)*sa1**2*sa2*sa3)/(4.*sw**2) + (ca1*cb**2*ee**2*complex(0,1)*sa1*sa2**2*sa3**2)/(4.*sw**2) + (ca1**2*ca3**2*cb*ee**2*complex(0,1)*sb)/(4.*sw**2) - (ca3**2*cb*ee**2*complex(0,1)*sa1**2*sb)/(4.*sw**2) - (ca1*ca3*cb*ee**2*complex(0,1)*sa1*sa2*sa3*sb)/sw**2 - (ca1**2*cb*ee**2*complex(0,1)*sa2**2*sa3**2*sb)/(4.*sw**2) + (cb*ee**2*complex(0,1)*sa1**2*sa2**2*sa3**2*sb)/(4.*sw**2) + (ca1*ca3**2*ee**2*complex(0,1)*sa1*sb**2)/(4.*sw**2) + (ca1**2*ca3*ee**2*complex(0,1)*sa2*sa3*sb**2)/(4.*sw**2) - (ca3*ee**2*complex(0,1)*sa1**2*sa2*sa3*sb**2)/(4.*sw**2) - (ca1*ee**2*complex(0,1)*sa1*sa2**2*sa3**2*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_277 = Coupling(name = 'GC_277',
                  value = '-(ca1*ca3**2*cb**2*ee**2*complex(0,1)*sa1)/(4.*cw**2) + 2*ca1*ca3**2*cb**2*complex(0,1)*l5*sa1 - (ca1**2*ca3*cb**2*ee**2*complex(0,1)*sa2*sa3)/(4.*cw**2) + 2*ca1**2*ca3*cb**2*complex(0,1)*l5*sa2*sa3 + (ca3*cb**2*ee**2*complex(0,1)*sa1**2*sa2*sa3)/(4.*cw**2) - 2*ca3*cb**2*complex(0,1)*l5*sa1**2*sa2*sa3 + (ca1*cb**2*ee**2*complex(0,1)*sa1*sa2**2*sa3**2)/(4.*cw**2) - 2*ca1*cb**2*complex(0,1)*l5*sa1*sa2**2*sa3**2 + (ca1**2*ca3**2*cb*ee**2*complex(0,1)*sb)/(4.*cw**2) - ca1**2*ca3**2*cb*complex(0,1)*l2*sb + ca1**2*ca3**2*cb*complex(0,1)*l3*sb + ca1**2*ca3**2*cb*complex(0,1)*l4*sb - ca1**2*ca3**2*cb*complex(0,1)*l5*sb - (ca3**2*cb*ee**2*complex(0,1)*sa1**2*sb)/(4.*cw**2) + ca3**2*cb*complex(0,1)*l1*sa1**2*sb - ca3**2*cb*complex(0,1)*l3*sa1**2*sb - ca3**2*cb*complex(0,1)*l4*sa1**2*sb + ca3**2*cb*complex(0,1)*l5*sa1**2*sb - (ca1*ca3*cb*ee**2*complex(0,1)*sa1*sa2*sa3*sb)/cw**2 + 2*ca1*ca3*cb*complex(0,1)*l1*sa1*sa2*sa3*sb + 2*ca1*ca3*cb*complex(0,1)*l2*sa1*sa2*sa3*sb - 4*ca1*ca3*cb*complex(0,1)*l3*sa1*sa2*sa3*sb - 4*ca1*ca3*cb*complex(0,1)*l4*sa1*sa2*sa3*sb + 4*ca1*ca3*cb*complex(0,1)*l5*sa1*sa2*sa3*sb + ca2**2*cb*complex(0,1)*l7*sa3**2*sb - ca2**2*cb*complex(0,1)*l8*sa3**2*sb - (ca1**2*cb*ee**2*complex(0,1)*sa2**2*sa3**2*sb)/(4.*cw**2) + ca1**2*cb*complex(0,1)*l1*sa2**2*sa3**2*sb - ca1**2*cb*complex(0,1)*l3*sa2**2*sa3**2*sb - ca1**2*cb*complex(0,1)*l4*sa2**2*sa3**2*sb + ca1**2*cb*complex(0,1)*l5*sa2**2*sa3**2*sb + (cb*ee**2*complex(0,1)*sa1**2*sa2**2*sa3**2*sb)/(4.*cw**2) - cb*complex(0,1)*l2*sa1**2*sa2**2*sa3**2*sb + cb*complex(0,1)*l3*sa1**2*sa2**2*sa3**2*sb + cb*complex(0,1)*l4*sa1**2*sa2**2*sa3**2*sb - cb*complex(0,1)*l5*sa1**2*sa2**2*sa3**2*sb + (ca1*ca3**2*ee**2*complex(0,1)*sa1*sb**2)/(4.*cw**2) - 2*ca1*ca3**2*complex(0,1)*l5*sa1*sb**2 + (ca1**2*ca3*ee**2*complex(0,1)*sa2*sa3*sb**2)/(4.*cw**2) - 2*ca1**2*ca3*complex(0,1)*l5*sa2*sa3*sb**2 - (ca3*ee**2*complex(0,1)*sa1**2*sa2*sa3*sb**2)/(4.*cw**2) + 2*ca3*complex(0,1)*l5*sa1**2*sa2*sa3*sb**2 - (ca1*ee**2*complex(0,1)*sa1*sa2**2*sa3**2*sb**2)/(4.*cw**2) + 2*ca1*complex(0,1)*l5*sa1*sa2**2*sa3**2*sb**2 - (ca1*ca3**2*cb**2*ee**2*complex(0,1)*sa1)/(4.*sw**2) - (ca1**2*ca3*cb**2*ee**2*complex(0,1)*sa2*sa3)/(4.*sw**2) + (ca3*cb**2*ee**2*complex(0,1)*sa1**2*sa2*sa3)/(4.*sw**2) + (ca1*cb**2*ee**2*complex(0,1)*sa1*sa2**2*sa3**2)/(4.*sw**2) + (ca1**2*ca3**2*cb*ee**2*complex(0,1)*sb)/(4.*sw**2) - (ca3**2*cb*ee**2*complex(0,1)*sa1**2*sb)/(4.*sw**2) - (ca1*ca3*cb*ee**2*complex(0,1)*sa1*sa2*sa3*sb)/sw**2 - (ca1**2*cb*ee**2*complex(0,1)*sa2**2*sa3**2*sb)/(4.*sw**2) + (cb*ee**2*complex(0,1)*sa1**2*sa2**2*sa3**2*sb)/(4.*sw**2) + (ca1*ca3**2*ee**2*complex(0,1)*sa1*sb**2)/(4.*sw**2) + (ca1**2*ca3*ee**2*complex(0,1)*sa2*sa3*sb**2)/(4.*sw**2) - (ca3*ee**2*complex(0,1)*sa1**2*sa2*sa3*sb**2)/(4.*sw**2) - (ca1*ee**2*complex(0,1)*sa1*sa2**2*sa3**2*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_278 = Coupling(name = 'GC_278',
                  value = 'ca1*ca3**2*cb**2*complex(0,1)*l4*sa1 + ca1*ca3**2*cb**2*complex(0,1)*l5*sa1 + ca1**2*ca3*cb**2*complex(0,1)*l4*sa2*sa3 + ca1**2*ca3*cb**2*complex(0,1)*l5*sa2*sa3 - ca3*cb**2*complex(0,1)*l4*sa1**2*sa2*sa3 - ca3*cb**2*complex(0,1)*l5*sa1**2*sa2*sa3 - ca1*cb**2*complex(0,1)*l4*sa1*sa2**2*sa3**2 - ca1*cb**2*complex(0,1)*l5*sa1*sa2**2*sa3**2 - ca1**2*ca3**2*cb*complex(0,1)*l2*sb + ca1**2*ca3**2*cb*complex(0,1)*l3*sb + ca3**2*cb*complex(0,1)*l1*sa1**2*sb - ca3**2*cb*complex(0,1)*l3*sa1**2*sb + 2*ca1*ca3*cb*complex(0,1)*l1*sa1*sa2*sa3*sb + 2*ca1*ca3*cb*complex(0,1)*l2*sa1*sa2*sa3*sb - 4*ca1*ca3*cb*complex(0,1)*l3*sa1*sa2*sa3*sb + ca2**2*cb*complex(0,1)*l7*sa3**2*sb - ca2**2*cb*complex(0,1)*l8*sa3**2*sb + ca1**2*cb*complex(0,1)*l1*sa2**2*sa3**2*sb - ca1**2*cb*complex(0,1)*l3*sa2**2*sa3**2*sb - cb*complex(0,1)*l2*sa1**2*sa2**2*sa3**2*sb + cb*complex(0,1)*l3*sa1**2*sa2**2*sa3**2*sb - ca1*ca3**2*complex(0,1)*l4*sa1*sb**2 - ca1*ca3**2*complex(0,1)*l5*sa1*sb**2 - ca1**2*ca3*complex(0,1)*l4*sa2*sa3*sb**2 - ca1**2*ca3*complex(0,1)*l5*sa2*sa3*sb**2 + ca3*complex(0,1)*l4*sa1**2*sa2*sa3*sb**2 + ca3*complex(0,1)*l5*sa1**2*sa2*sa3*sb**2 + ca1*complex(0,1)*l4*sa1*sa2**2*sa3**2*sb**2 + ca1*complex(0,1)*l5*sa1*sa2**2*sa3**2*sb**2 + (ca1*ca3**2*cb**2*ee**2*complex(0,1)*sa1)/(2.*sw**2) + (ca1**2*ca3*cb**2*ee**2*complex(0,1)*sa2*sa3)/(2.*sw**2) - (ca3*cb**2*ee**2*complex(0,1)*sa1**2*sa2*sa3)/(2.*sw**2) - (ca1*cb**2*ee**2*complex(0,1)*sa1*sa2**2*sa3**2)/(2.*sw**2) - (ca1**2*ca3**2*cb*ee**2*complex(0,1)*sb)/(2.*sw**2) + (ca3**2*cb*ee**2*complex(0,1)*sa1**2*sb)/(2.*sw**2) + (2*ca1*ca3*cb*ee**2*complex(0,1)*sa1*sa2*sa3*sb)/sw**2 + (ca1**2*cb*ee**2*complex(0,1)*sa2**2*sa3**2*sb)/(2.*sw**2) - (cb*ee**2*complex(0,1)*sa1**2*sa2**2*sa3**2*sb)/(2.*sw**2) - (ca1*ca3**2*ee**2*complex(0,1)*sa1*sb**2)/(2.*sw**2) - (ca1**2*ca3*ee**2*complex(0,1)*sa2*sa3*sb**2)/(2.*sw**2) + (ca3*ee**2*complex(0,1)*sa1**2*sa2*sa3*sb**2)/(2.*sw**2) + (ca1*ee**2*complex(0,1)*sa1*sa2**2*sa3**2*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_279 = Coupling(name = 'GC_279',
                  value = '(ca1*ca3**2*cb**2*ee**2*complex(0,1)*sa1)/(2.*cw**2) + 2*ca1*ca3**2*cb**2*complex(0,1)*l5*sa1 + (ca1**2*ca3*cb**2*ee**2*complex(0,1)*sa2*sa3)/(2.*cw**2) + 2*ca1**2*ca3*cb**2*complex(0,1)*l5*sa2*sa3 - (ca3*cb**2*ee**2*complex(0,1)*sa1**2*sa2*sa3)/(2.*cw**2) - 2*ca3*cb**2*complex(0,1)*l5*sa1**2*sa2*sa3 - (ca1*cb**2*ee**2*complex(0,1)*sa1*sa2**2*sa3**2)/(2.*cw**2) - 2*ca1*cb**2*complex(0,1)*l5*sa1*sa2**2*sa3**2 - (ca1**2*ca3**2*cb*ee**2*complex(0,1)*sb)/(2.*cw**2) - ca1**2*ca3**2*cb*complex(0,1)*l2*sb + ca1**2*ca3**2*cb*complex(0,1)*l3*sb + ca1**2*ca3**2*cb*complex(0,1)*l4*sb - ca1**2*ca3**2*cb*complex(0,1)*l5*sb + (ca3**2*cb*ee**2*complex(0,1)*sa1**2*sb)/(2.*cw**2) + ca3**2*cb*complex(0,1)*l1*sa1**2*sb - ca3**2*cb*complex(0,1)*l3*sa1**2*sb - ca3**2*cb*complex(0,1)*l4*sa1**2*sb + ca3**2*cb*complex(0,1)*l5*sa1**2*sb + (2*ca1*ca3*cb*ee**2*complex(0,1)*sa1*sa2*sa3*sb)/cw**2 + 2*ca1*ca3*cb*complex(0,1)*l1*sa1*sa2*sa3*sb + 2*ca1*ca3*cb*complex(0,1)*l2*sa1*sa2*sa3*sb - 4*ca1*ca3*cb*complex(0,1)*l3*sa1*sa2*sa3*sb - 4*ca1*ca3*cb*complex(0,1)*l4*sa1*sa2*sa3*sb + 4*ca1*ca3*cb*complex(0,1)*l5*sa1*sa2*sa3*sb + ca2**2*cb*complex(0,1)*l7*sa3**2*sb - ca2**2*cb*complex(0,1)*l8*sa3**2*sb + (ca1**2*cb*ee**2*complex(0,1)*sa2**2*sa3**2*sb)/(2.*cw**2) + ca1**2*cb*complex(0,1)*l1*sa2**2*sa3**2*sb - ca1**2*cb*complex(0,1)*l3*sa2**2*sa3**2*sb - ca1**2*cb*complex(0,1)*l4*sa2**2*sa3**2*sb + ca1**2*cb*complex(0,1)*l5*sa2**2*sa3**2*sb - (cb*ee**2*complex(0,1)*sa1**2*sa2**2*sa3**2*sb)/(2.*cw**2) - cb*complex(0,1)*l2*sa1**2*sa2**2*sa3**2*sb + cb*complex(0,1)*l3*sa1**2*sa2**2*sa3**2*sb + cb*complex(0,1)*l4*sa1**2*sa2**2*sa3**2*sb - cb*complex(0,1)*l5*sa1**2*sa2**2*sa3**2*sb - (ca1*ca3**2*ee**2*complex(0,1)*sa1*sb**2)/(2.*cw**2) - 2*ca1*ca3**2*complex(0,1)*l5*sa1*sb**2 - (ca1**2*ca3*ee**2*complex(0,1)*sa2*sa3*sb**2)/(2.*cw**2) - 2*ca1**2*ca3*complex(0,1)*l5*sa2*sa3*sb**2 + (ca3*ee**2*complex(0,1)*sa1**2*sa2*sa3*sb**2)/(2.*cw**2) + 2*ca3*complex(0,1)*l5*sa1**2*sa2*sa3*sb**2 + (ca1*ee**2*complex(0,1)*sa1*sa2**2*sa3**2*sb**2)/(2.*cw**2) + 2*ca1*complex(0,1)*l5*sa1*sa2**2*sa3**2*sb**2 + (ca1*ca3**2*cb**2*ee**2*complex(0,1)*sa1)/(2.*sw**2) + (ca1**2*ca3*cb**2*ee**2*complex(0,1)*sa2*sa3)/(2.*sw**2) - (ca3*cb**2*ee**2*complex(0,1)*sa1**2*sa2*sa3)/(2.*sw**2) - (ca1*cb**2*ee**2*complex(0,1)*sa1*sa2**2*sa3**2)/(2.*sw**2) - (ca1**2*ca3**2*cb*ee**2*complex(0,1)*sb)/(2.*sw**2) + (ca3**2*cb*ee**2*complex(0,1)*sa1**2*sb)/(2.*sw**2) + (2*ca1*ca3*cb*ee**2*complex(0,1)*sa1*sa2*sa3*sb)/sw**2 + (ca1**2*cb*ee**2*complex(0,1)*sa2**2*sa3**2*sb)/(2.*sw**2) - (cb*ee**2*complex(0,1)*sa1**2*sa2**2*sa3**2*sb)/(2.*sw**2) - (ca1*ca3**2*ee**2*complex(0,1)*sa1*sb**2)/(2.*sw**2) - (ca1**2*ca3*ee**2*complex(0,1)*sa2*sa3*sb**2)/(2.*sw**2) + (ca3*ee**2*complex(0,1)*sa1**2*sa2*sa3*sb**2)/(2.*sw**2) + (ca1*ee**2*complex(0,1)*sa1*sa2**2*sa3**2*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_280 = Coupling(name = 'GC_280',
                  value = '-(ca1**2*ca3**2*cb**2*complex(0,1)*l3) - ca3**2*cb**2*complex(0,1)*l1*sa1**2 - 2*ca1*ca3*cb**2*complex(0,1)*l1*sa1*sa2*sa3 + 2*ca1*ca3*cb**2*complex(0,1)*l3*sa1*sa2*sa3 - ca2**2*cb**2*complex(0,1)*l7*sa3**2 - ca1**2*cb**2*complex(0,1)*l1*sa2**2*sa3**2 - cb**2*complex(0,1)*l3*sa1**2*sa2**2*sa3**2 + 2*ca1*ca3**2*cb*complex(0,1)*l4*sa1*sb + 2*ca1*ca3**2*cb*complex(0,1)*l5*sa1*sb + 2*ca1**2*ca3*cb*complex(0,1)*l4*sa2*sa3*sb + 2*ca1**2*ca3*cb*complex(0,1)*l5*sa2*sa3*sb - 2*ca3*cb*complex(0,1)*l4*sa1**2*sa2*sa3*sb - 2*ca3*cb*complex(0,1)*l5*sa1**2*sa2*sa3*sb - 2*ca1*cb*complex(0,1)*l4*sa1*sa2**2*sa3**2*sb - 2*ca1*cb*complex(0,1)*l5*sa1*sa2**2*sa3**2*sb - ca1**2*ca3**2*complex(0,1)*l2*sb**2 - ca3**2*complex(0,1)*l3*sa1**2*sb**2 + 2*ca1*ca3*complex(0,1)*l2*sa1*sa2*sa3*sb**2 - 2*ca1*ca3*complex(0,1)*l3*sa1*sa2*sa3*sb**2 - ca2**2*complex(0,1)*l8*sa3**2*sb**2 - ca1**2*complex(0,1)*l3*sa2**2*sa3**2*sb**2 - complex(0,1)*l2*sa1**2*sa2**2*sa3**2*sb**2 + (ca3**2*cb**2*ee**2*complex(0,1)*sa1**2)/(4.*sw**2) + (ca1*ca3*cb**2*ee**2*complex(0,1)*sa1*sa2*sa3)/(2.*sw**2) + (ca1**2*cb**2*ee**2*complex(0,1)*sa2**2*sa3**2)/(4.*sw**2) - (ca1*ca3**2*cb*ee**2*complex(0,1)*sa1*sb)/(2.*sw**2) - (ca1**2*ca3*cb*ee**2*complex(0,1)*sa2*sa3*sb)/(2.*sw**2) + (ca3*cb*ee**2*complex(0,1)*sa1**2*sa2*sa3*sb)/(2.*sw**2) + (ca1*cb*ee**2*complex(0,1)*sa1*sa2**2*sa3**2*sb)/(2.*sw**2) + (ca1**2*ca3**2*ee**2*complex(0,1)*sb**2)/(4.*sw**2) - (ca1*ca3*ee**2*complex(0,1)*sa1*sa2*sa3*sb**2)/(2.*sw**2) + (ee**2*complex(0,1)*sa1**2*sa2**2*sa3**2*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_281 = Coupling(name = 'GC_281',
                  value = '-(ca1**2*ca3**2*cb**2*complex(0,1)*l3) - ca1**2*ca3**2*cb**2*complex(0,1)*l4 + ca1**2*ca3**2*cb**2*complex(0,1)*l5 + (ca3**2*cb**2*ee**2*complex(0,1)*sa1**2)/(4.*cw**2) - ca3**2*cb**2*complex(0,1)*l1*sa1**2 + (ca1*ca3*cb**2*ee**2*complex(0,1)*sa1*sa2*sa3)/(2.*cw**2) - 2*ca1*ca3*cb**2*complex(0,1)*l1*sa1*sa2*sa3 + 2*ca1*ca3*cb**2*complex(0,1)*l3*sa1*sa2*sa3 + 2*ca1*ca3*cb**2*complex(0,1)*l4*sa1*sa2*sa3 - 2*ca1*ca3*cb**2*complex(0,1)*l5*sa1*sa2*sa3 - ca2**2*cb**2*complex(0,1)*l7*sa3**2 + (ca1**2*cb**2*ee**2*complex(0,1)*sa2**2*sa3**2)/(4.*cw**2) - ca1**2*cb**2*complex(0,1)*l1*sa2**2*sa3**2 - cb**2*complex(0,1)*l3*sa1**2*sa2**2*sa3**2 - cb**2*complex(0,1)*l4*sa1**2*sa2**2*sa3**2 + cb**2*complex(0,1)*l5*sa1**2*sa2**2*sa3**2 - (ca1*ca3**2*cb*ee**2*complex(0,1)*sa1*sb)/(2.*cw**2) + 4*ca1*ca3**2*cb*complex(0,1)*l5*sa1*sb - (ca1**2*ca3*cb*ee**2*complex(0,1)*sa2*sa3*sb)/(2.*cw**2) + 4*ca1**2*ca3*cb*complex(0,1)*l5*sa2*sa3*sb + (ca3*cb*ee**2*complex(0,1)*sa1**2*sa2*sa3*sb)/(2.*cw**2) - 4*ca3*cb*complex(0,1)*l5*sa1**2*sa2*sa3*sb + (ca1*cb*ee**2*complex(0,1)*sa1*sa2**2*sa3**2*sb)/(2.*cw**2) - 4*ca1*cb*complex(0,1)*l5*sa1*sa2**2*sa3**2*sb + (ca1**2*ca3**2*ee**2*complex(0,1)*sb**2)/(4.*cw**2) - ca1**2*ca3**2*complex(0,1)*l2*sb**2 - ca3**2*complex(0,1)*l3*sa1**2*sb**2 - ca3**2*complex(0,1)*l4*sa1**2*sb**2 + ca3**2*complex(0,1)*l5*sa1**2*sb**2 - (ca1*ca3*ee**2*complex(0,1)*sa1*sa2*sa3*sb**2)/(2.*cw**2) + 2*ca1*ca3*complex(0,1)*l2*sa1*sa2*sa3*sb**2 - 2*ca1*ca3*complex(0,1)*l3*sa1*sa2*sa3*sb**2 - 2*ca1*ca3*complex(0,1)*l4*sa1*sa2*sa3*sb**2 + 2*ca1*ca3*complex(0,1)*l5*sa1*sa2*sa3*sb**2 - ca2**2*complex(0,1)*l8*sa3**2*sb**2 - ca1**2*complex(0,1)*l3*sa2**2*sa3**2*sb**2 - ca1**2*complex(0,1)*l4*sa2**2*sa3**2*sb**2 + ca1**2*complex(0,1)*l5*sa2**2*sa3**2*sb**2 + (ee**2*complex(0,1)*sa1**2*sa2**2*sa3**2*sb**2)/(4.*cw**2) - complex(0,1)*l2*sa1**2*sa2**2*sa3**2*sb**2 + (ca3**2*cb**2*ee**2*complex(0,1)*sa1**2)/(4.*sw**2) + (ca1*ca3*cb**2*ee**2*complex(0,1)*sa1*sa2*sa3)/(2.*sw**2) + (ca1**2*cb**2*ee**2*complex(0,1)*sa2**2*sa3**2)/(4.*sw**2) - (ca1*ca3**2*cb*ee**2*complex(0,1)*sa1*sb)/(2.*sw**2) - (ca1**2*ca3*cb*ee**2*complex(0,1)*sa2*sa3*sb)/(2.*sw**2) + (ca3*cb*ee**2*complex(0,1)*sa1**2*sa2*sa3*sb)/(2.*sw**2) + (ca1*cb*ee**2*complex(0,1)*sa1*sa2**2*sa3**2*sb)/(2.*sw**2) + (ca1**2*ca3**2*ee**2*complex(0,1)*sb**2)/(4.*sw**2) - (ca1*ca3*ee**2*complex(0,1)*sa1*sa2*sa3*sb**2)/(2.*sw**2) + (ee**2*complex(0,1)*sa1**2*sa2**2*sa3**2*sb**2)/(4.*sw**2)',
                  order = {'QED':2})

GC_282 = Coupling(name = 'GC_282',
                  value = '-(ca1**2*ca3**2*cb**2*complex(0,1)*l3) - ca3**2*cb**2*complex(0,1)*l1*sa1**2 - 2*ca1*ca3*cb**2*complex(0,1)*l1*sa1*sa2*sa3 + 2*ca1*ca3*cb**2*complex(0,1)*l3*sa1*sa2*sa3 - ca2**2*cb**2*complex(0,1)*l7*sa3**2 - ca1**2*cb**2*complex(0,1)*l1*sa2**2*sa3**2 - cb**2*complex(0,1)*l3*sa1**2*sa2**2*sa3**2 + 2*ca1*ca3**2*cb*complex(0,1)*l4*sa1*sb + 2*ca1*ca3**2*cb*complex(0,1)*l5*sa1*sb + 2*ca1**2*ca3*cb*complex(0,1)*l4*sa2*sa3*sb + 2*ca1**2*ca3*cb*complex(0,1)*l5*sa2*sa3*sb - 2*ca3*cb*complex(0,1)*l4*sa1**2*sa2*sa3*sb - 2*ca3*cb*complex(0,1)*l5*sa1**2*sa2*sa3*sb - 2*ca1*cb*complex(0,1)*l4*sa1*sa2**2*sa3**2*sb - 2*ca1*cb*complex(0,1)*l5*sa1*sa2**2*sa3**2*sb - ca1**2*ca3**2*complex(0,1)*l2*sb**2 - ca3**2*complex(0,1)*l3*sa1**2*sb**2 + 2*ca1*ca3*complex(0,1)*l2*sa1*sa2*sa3*sb**2 - 2*ca1*ca3*complex(0,1)*l3*sa1*sa2*sa3*sb**2 - ca2**2*complex(0,1)*l8*sa3**2*sb**2 - ca1**2*complex(0,1)*l3*sa2**2*sa3**2*sb**2 - complex(0,1)*l2*sa1**2*sa2**2*sa3**2*sb**2 - (ca3**2*cb**2*ee**2*complex(0,1)*sa1**2)/(2.*sw**2) - (ca1*ca3*cb**2*ee**2*complex(0,1)*sa1*sa2*sa3)/sw**2 - (ca1**2*cb**2*ee**2*complex(0,1)*sa2**2*sa3**2)/(2.*sw**2) + (ca1*ca3**2*cb*ee**2*complex(0,1)*sa1*sb)/sw**2 + (ca1**2*ca3*cb*ee**2*complex(0,1)*sa2*sa3*sb)/sw**2 - (ca3*cb*ee**2*complex(0,1)*sa1**2*sa2*sa3*sb)/sw**2 - (ca1*cb*ee**2*complex(0,1)*sa1*sa2**2*sa3**2*sb)/sw**2 - (ca1**2*ca3**2*ee**2*complex(0,1)*sb**2)/(2.*sw**2) + (ca1*ca3*ee**2*complex(0,1)*sa1*sa2*sa3*sb**2)/sw**2 - (ee**2*complex(0,1)*sa1**2*sa2**2*sa3**2*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_283 = Coupling(name = 'GC_283',
                  value = '-(ca1**2*ca3**2*cb**2*complex(0,1)*l3) - ca1**2*ca3**2*cb**2*complex(0,1)*l4 + ca1**2*ca3**2*cb**2*complex(0,1)*l5 - (ca3**2*cb**2*ee**2*complex(0,1)*sa1**2)/(2.*cw**2) - ca3**2*cb**2*complex(0,1)*l1*sa1**2 - (ca1*ca3*cb**2*ee**2*complex(0,1)*sa1*sa2*sa3)/cw**2 - 2*ca1*ca3*cb**2*complex(0,1)*l1*sa1*sa2*sa3 + 2*ca1*ca3*cb**2*complex(0,1)*l3*sa1*sa2*sa3 + 2*ca1*ca3*cb**2*complex(0,1)*l4*sa1*sa2*sa3 - 2*ca1*ca3*cb**2*complex(0,1)*l5*sa1*sa2*sa3 - ca2**2*cb**2*complex(0,1)*l7*sa3**2 - (ca1**2*cb**2*ee**2*complex(0,1)*sa2**2*sa3**2)/(2.*cw**2) - ca1**2*cb**2*complex(0,1)*l1*sa2**2*sa3**2 - cb**2*complex(0,1)*l3*sa1**2*sa2**2*sa3**2 - cb**2*complex(0,1)*l4*sa1**2*sa2**2*sa3**2 + cb**2*complex(0,1)*l5*sa1**2*sa2**2*sa3**2 + (ca1*ca3**2*cb*ee**2*complex(0,1)*sa1*sb)/cw**2 + 4*ca1*ca3**2*cb*complex(0,1)*l5*sa1*sb + (ca1**2*ca3*cb*ee**2*complex(0,1)*sa2*sa3*sb)/cw**2 + 4*ca1**2*ca3*cb*complex(0,1)*l5*sa2*sa3*sb - (ca3*cb*ee**2*complex(0,1)*sa1**2*sa2*sa3*sb)/cw**2 - 4*ca3*cb*complex(0,1)*l5*sa1**2*sa2*sa3*sb - (ca1*cb*ee**2*complex(0,1)*sa1*sa2**2*sa3**2*sb)/cw**2 - 4*ca1*cb*complex(0,1)*l5*sa1*sa2**2*sa3**2*sb - (ca1**2*ca3**2*ee**2*complex(0,1)*sb**2)/(2.*cw**2) - ca1**2*ca3**2*complex(0,1)*l2*sb**2 - ca3**2*complex(0,1)*l3*sa1**2*sb**2 - ca3**2*complex(0,1)*l4*sa1**2*sb**2 + ca3**2*complex(0,1)*l5*sa1**2*sb**2 + (ca1*ca3*ee**2*complex(0,1)*sa1*sa2*sa3*sb**2)/cw**2 + 2*ca1*ca3*complex(0,1)*l2*sa1*sa2*sa3*sb**2 - 2*ca1*ca3*complex(0,1)*l3*sa1*sa2*sa3*sb**2 - 2*ca1*ca3*complex(0,1)*l4*sa1*sa2*sa3*sb**2 + 2*ca1*ca3*complex(0,1)*l5*sa1*sa2*sa3*sb**2 - ca2**2*complex(0,1)*l8*sa3**2*sb**2 - ca1**2*complex(0,1)*l3*sa2**2*sa3**2*sb**2 - ca1**2*complex(0,1)*l4*sa2**2*sa3**2*sb**2 + ca1**2*complex(0,1)*l5*sa2**2*sa3**2*sb**2 - (ee**2*complex(0,1)*sa1**2*sa2**2*sa3**2*sb**2)/(2.*cw**2) - complex(0,1)*l2*sa1**2*sa2**2*sa3**2*sb**2 - (ca3**2*cb**2*ee**2*complex(0,1)*sa1**2)/(2.*sw**2) - (ca1*ca3*cb**2*ee**2*complex(0,1)*sa1*sa2*sa3)/sw**2 - (ca1**2*cb**2*ee**2*complex(0,1)*sa2**2*sa3**2)/(2.*sw**2) + (ca1*ca3**2*cb*ee**2*complex(0,1)*sa1*sb)/sw**2 + (ca1**2*ca3*cb*ee**2*complex(0,1)*sa2*sa3*sb)/sw**2 - (ca3*cb*ee**2*complex(0,1)*sa1**2*sa2*sa3*sb)/sw**2 - (ca1*cb*ee**2*complex(0,1)*sa1*sa2**2*sa3**2*sb)/sw**2 - (ca1**2*ca3**2*ee**2*complex(0,1)*sb**2)/(2.*sw**2) + (ca1*ca3*ee**2*complex(0,1)*sa1*sa2*sa3*sb**2)/sw**2 - (ee**2*complex(0,1)*sa1**2*sa2**2*sa3**2*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_284 = Coupling(name = 'GC_284',
                  value = '-(ca2*cb**3*ee**2*sa1)/(4.*cw**2) + (ca1*ca2*cb**2*ee**2*sb)/(4.*cw**2) - (ca2*cb*ee**2*sa1*sb**2)/(4.*cw**2) + (ca1*ca2*ee**2*sb**3)/(4.*cw**2) + (ca2*cb**3*ee**2*sa1)/(4.*sw**2) - (ca1*ca2*cb**2*ee**2*sb)/(4.*sw**2) + (ca2*cb*ee**2*sa1*sb**2)/(4.*sw**2) - (ca1*ca2*ee**2*sb**3)/(4.*sw**2)',
                  order = {'QED':2})

GC_285 = Coupling(name = 'GC_285',
                  value = '-(ca2*cb**3*l4*sa1)/2. + (ca2*cb**3*l5*sa1)/2. + (ca1*ca2*cb**2*l4*sb)/2. - (ca1*ca2*cb**2*l5*sb)/2. - (ca2*cb*l4*sa1*sb**2)/2. + (ca2*cb*l5*sa1*sb**2)/2. + (ca1*ca2*l4*sb**3)/2. - (ca1*ca2*l5*sb**3)/2. + (ca2*cb**3*ee**2*sa1)/(4.*sw**2) - (ca1*ca2*cb**2*ee**2*sb)/(4.*sw**2) + (ca2*cb*ee**2*sa1*sb**2)/(4.*sw**2) - (ca1*ca2*ee**2*sb**3)/(4.*sw**2)',
                  order = {'QED':2})

GC_286 = Coupling(name = 'GC_286',
                  value = '(ca2*cb**3*l4*sa1)/2. - (ca2*cb**3*l5*sa1)/2. - (ca1*ca2*cb**2*l4*sb)/2. + (ca1*ca2*cb**2*l5*sb)/2. + (ca2*cb*l4*sa1*sb**2)/2. - (ca2*cb*l5*sa1*sb**2)/2. - (ca1*ca2*l4*sb**3)/2. + (ca1*ca2*l5*sb**3)/2. + (ca2*cb**3*ee**2*sa1)/(4.*sw**2) - (ca1*ca2*cb**2*ee**2*sb)/(4.*sw**2) + (ca2*cb*ee**2*sa1*sb**2)/(4.*sw**2) - (ca1*ca2*ee**2*sb**3)/(4.*sw**2)',
                  order = {'QED':2})

GC_287 = Coupling(name = 'GC_287',
                  value = '(ca2*cb**3*ee**2*sa1)/(4.*cw**2) - (ca1*ca2*cb**2*ee**2*sb)/(4.*cw**2) + (ca2*cb*ee**2*sa1*sb**2)/(4.*cw**2) - (ca1*ca2*ee**2*sb**3)/(4.*cw**2) - (ca2*cb**3*ee**2*sa1)/(4.*sw**2) + (ca1*ca2*cb**2*ee**2*sb)/(4.*sw**2) - (ca2*cb*ee**2*sa1*sb**2)/(4.*sw**2) + (ca1*ca2*ee**2*sb**3)/(4.*sw**2)',
                  order = {'QED':2})

GC_288 = Coupling(name = 'GC_288',
                  value = '-(ca2*cb**3*l4*sa1)/2. + (ca2*cb**3*l5*sa1)/2. + (ca1*ca2*cb**2*l4*sb)/2. - (ca1*ca2*cb**2*l5*sb)/2. - (ca2*cb*l4*sa1*sb**2)/2. + (ca2*cb*l5*sa1*sb**2)/2. + (ca1*ca2*l4*sb**3)/2. - (ca1*ca2*l5*sb**3)/2. - (ca2*cb**3*ee**2*sa1)/(4.*sw**2) + (ca1*ca2*cb**2*ee**2*sb)/(4.*sw**2) - (ca2*cb*ee**2*sa1*sb**2)/(4.*sw**2) + (ca1*ca2*ee**2*sb**3)/(4.*sw**2)',
                  order = {'QED':2})

GC_289 = Coupling(name = 'GC_289',
                  value = '(ca2*cb**3*l4*sa1)/2. - (ca2*cb**3*l5*sa1)/2. - (ca1*ca2*cb**2*l4*sb)/2. + (ca1*ca2*cb**2*l5*sb)/2. + (ca2*cb*l4*sa1*sb**2)/2. - (ca2*cb*l5*sa1*sb**2)/2. - (ca1*ca2*l4*sb**3)/2. + (ca1*ca2*l5*sb**3)/2. - (ca2*cb**3*ee**2*sa1)/(4.*sw**2) + (ca1*ca2*cb**2*ee**2*sb)/(4.*sw**2) - (ca2*cb*ee**2*sa1*sb**2)/(4.*sw**2) + (ca1*ca2*ee**2*sb**3)/(4.*sw**2)',
                  order = {'QED':2})

GC_290 = Coupling(name = 'GC_290',
                  value = '(ca1*ca2*cb**3*ee**2)/(4.*cw**2) + (ca2*cb**2*ee**2*sa1*sb)/(4.*cw**2) + (ca1*ca2*cb*ee**2*sb**2)/(4.*cw**2) + (ca2*ee**2*sa1*sb**3)/(4.*cw**2) - (ca1*ca2*cb**3*ee**2)/(4.*sw**2) - (ca2*cb**2*ee**2*sa1*sb)/(4.*sw**2) - (ca1*ca2*cb*ee**2*sb**2)/(4.*sw**2) - (ca2*ee**2*sa1*sb**3)/(4.*sw**2)',
                  order = {'QED':2})

GC_291 = Coupling(name = 'GC_291',
                  value = '(ca1*ca2*cb**3*l4)/2. - (ca1*ca2*cb**3*l5)/2. + (ca2*cb**2*l4*sa1*sb)/2. - (ca2*cb**2*l5*sa1*sb)/2. + (ca1*ca2*cb*l4*sb**2)/2. - (ca1*ca2*cb*l5*sb**2)/2. + (ca2*l4*sa1*sb**3)/2. - (ca2*l5*sa1*sb**3)/2. - (ca1*ca2*cb**3*ee**2)/(4.*sw**2) - (ca2*cb**2*ee**2*sa1*sb)/(4.*sw**2) - (ca1*ca2*cb*ee**2*sb**2)/(4.*sw**2) - (ca2*ee**2*sa1*sb**3)/(4.*sw**2)',
                  order = {'QED':2})

GC_292 = Coupling(name = 'GC_292',
                  value = '-(ca1*ca2*cb**3*l4)/2. + (ca1*ca2*cb**3*l5)/2. - (ca2*cb**2*l4*sa1*sb)/2. + (ca2*cb**2*l5*sa1*sb)/2. - (ca1*ca2*cb*l4*sb**2)/2. + (ca1*ca2*cb*l5*sb**2)/2. - (ca2*l4*sa1*sb**3)/2. + (ca2*l5*sa1*sb**3)/2. - (ca1*ca2*cb**3*ee**2)/(4.*sw**2) - (ca2*cb**2*ee**2*sa1*sb)/(4.*sw**2) - (ca1*ca2*cb*ee**2*sb**2)/(4.*sw**2) - (ca2*ee**2*sa1*sb**3)/(4.*sw**2)',
                  order = {'QED':2})

GC_293 = Coupling(name = 'GC_293',
                  value = '-(ca1*ca2*cb**3*ee**2)/(4.*cw**2) - (ca2*cb**2*ee**2*sa1*sb)/(4.*cw**2) - (ca1*ca2*cb*ee**2*sb**2)/(4.*cw**2) - (ca2*ee**2*sa1*sb**3)/(4.*cw**2) + (ca1*ca2*cb**3*ee**2)/(4.*sw**2) + (ca2*cb**2*ee**2*sa1*sb)/(4.*sw**2) + (ca1*ca2*cb*ee**2*sb**2)/(4.*sw**2) + (ca2*ee**2*sa1*sb**3)/(4.*sw**2)',
                  order = {'QED':2})

GC_294 = Coupling(name = 'GC_294',
                  value = '(ca1*ca2*cb**3*l4)/2. - (ca1*ca2*cb**3*l5)/2. + (ca2*cb**2*l4*sa1*sb)/2. - (ca2*cb**2*l5*sa1*sb)/2. + (ca1*ca2*cb*l4*sb**2)/2. - (ca1*ca2*cb*l5*sb**2)/2. + (ca2*l4*sa1*sb**3)/2. - (ca2*l5*sa1*sb**3)/2. + (ca1*ca2*cb**3*ee**2)/(4.*sw**2) + (ca2*cb**2*ee**2*sa1*sb)/(4.*sw**2) + (ca1*ca2*cb*ee**2*sb**2)/(4.*sw**2) + (ca2*ee**2*sa1*sb**3)/(4.*sw**2)',
                  order = {'QED':2})

GC_295 = Coupling(name = 'GC_295',
                  value = '-(ca1*ca2*cb**3*l4)/2. + (ca1*ca2*cb**3*l5)/2. - (ca2*cb**2*l4*sa1*sb)/2. + (ca2*cb**2*l5*sa1*sb)/2. - (ca1*ca2*cb*l4*sb**2)/2. + (ca1*ca2*cb*l5*sb**2)/2. - (ca2*l4*sa1*sb**3)/2. + (ca2*l5*sa1*sb**3)/2. + (ca1*ca2*cb**3*ee**2)/(4.*sw**2) + (ca2*cb**2*ee**2*sa1*sb)/(4.*sw**2) + (ca1*ca2*cb*ee**2*sb**2)/(4.*sw**2) + (ca2*ee**2*sa1*sb**3)/(4.*sw**2)',
                  order = {'QED':2})

GC_296 = Coupling(name = 'GC_296',
                  value = '(ca1*ca3*cb**3*ee**2*sa2)/(4.*cw**2) - (cb**3*ee**2*sa1*sa3)/(4.*cw**2) + (ca3*cb**2*ee**2*sa1*sa2*sb)/(4.*cw**2) + (ca1*cb**2*ee**2*sa3*sb)/(4.*cw**2) + (ca1*ca3*cb*ee**2*sa2*sb**2)/(4.*cw**2) - (cb*ee**2*sa1*sa3*sb**2)/(4.*cw**2) + (ca3*ee**2*sa1*sa2*sb**3)/(4.*cw**2) + (ca1*ee**2*sa3*sb**3)/(4.*cw**2) - (ca1*ca3*cb**3*ee**2*sa2)/(4.*sw**2) + (cb**3*ee**2*sa1*sa3)/(4.*sw**2) - (ca3*cb**2*ee**2*sa1*sa2*sb)/(4.*sw**2) - (ca1*cb**2*ee**2*sa3*sb)/(4.*sw**2) - (ca1*ca3*cb*ee**2*sa2*sb**2)/(4.*sw**2) + (cb*ee**2*sa1*sa3*sb**2)/(4.*sw**2) - (ca3*ee**2*sa1*sa2*sb**3)/(4.*sw**2) - (ca1*ee**2*sa3*sb**3)/(4.*sw**2)',
                  order = {'QED':2})

GC_297 = Coupling(name = 'GC_297',
                  value = '(ca1*ca3*cb**3*l4*sa2)/2. - (ca1*ca3*cb**3*l5*sa2)/2. - (cb**3*l4*sa1*sa3)/2. + (cb**3*l5*sa1*sa3)/2. + (ca3*cb**2*l4*sa1*sa2*sb)/2. - (ca3*cb**2*l5*sa1*sa2*sb)/2. + (ca1*cb**2*l4*sa3*sb)/2. - (ca1*cb**2*l5*sa3*sb)/2. + (ca1*ca3*cb*l4*sa2*sb**2)/2. - (ca1*ca3*cb*l5*sa2*sb**2)/2. - (cb*l4*sa1*sa3*sb**2)/2. + (cb*l5*sa1*sa3*sb**2)/2. + (ca3*l4*sa1*sa2*sb**3)/2. - (ca3*l5*sa1*sa2*sb**3)/2. + (ca1*l4*sa3*sb**3)/2. - (ca1*l5*sa3*sb**3)/2. - (ca1*ca3*cb**3*ee**2*sa2)/(4.*sw**2) + (cb**3*ee**2*sa1*sa3)/(4.*sw**2) - (ca3*cb**2*ee**2*sa1*sa2*sb)/(4.*sw**2) - (ca1*cb**2*ee**2*sa3*sb)/(4.*sw**2) - (ca1*ca3*cb*ee**2*sa2*sb**2)/(4.*sw**2) + (cb*ee**2*sa1*sa3*sb**2)/(4.*sw**2) - (ca3*ee**2*sa1*sa2*sb**3)/(4.*sw**2) - (ca1*ee**2*sa3*sb**3)/(4.*sw**2)',
                  order = {'QED':2})

GC_298 = Coupling(name = 'GC_298',
                  value = '-(ca1*ca3*cb**3*l4*sa2)/2. + (ca1*ca3*cb**3*l5*sa2)/2. + (cb**3*l4*sa1*sa3)/2. - (cb**3*l5*sa1*sa3)/2. - (ca3*cb**2*l4*sa1*sa2*sb)/2. + (ca3*cb**2*l5*sa1*sa2*sb)/2. - (ca1*cb**2*l4*sa3*sb)/2. + (ca1*cb**2*l5*sa3*sb)/2. - (ca1*ca3*cb*l4*sa2*sb**2)/2. + (ca1*ca3*cb*l5*sa2*sb**2)/2. + (cb*l4*sa1*sa3*sb**2)/2. - (cb*l5*sa1*sa3*sb**2)/2. - (ca3*l4*sa1*sa2*sb**3)/2. + (ca3*l5*sa1*sa2*sb**3)/2. - (ca1*l4*sa3*sb**3)/2. + (ca1*l5*sa3*sb**3)/2. - (ca1*ca3*cb**3*ee**2*sa2)/(4.*sw**2) + (cb**3*ee**2*sa1*sa3)/(4.*sw**2) - (ca3*cb**2*ee**2*sa1*sa2*sb)/(4.*sw**2) - (ca1*cb**2*ee**2*sa3*sb)/(4.*sw**2) - (ca1*ca3*cb*ee**2*sa2*sb**2)/(4.*sw**2) + (cb*ee**2*sa1*sa3*sb**2)/(4.*sw**2) - (ca3*ee**2*sa1*sa2*sb**3)/(4.*sw**2) - (ca1*ee**2*sa3*sb**3)/(4.*sw**2)',
                  order = {'QED':2})

GC_299 = Coupling(name = 'GC_299',
                  value = '-(ca1*ca3*cb**3*ee**2*sa2)/(4.*cw**2) + (cb**3*ee**2*sa1*sa3)/(4.*cw**2) - (ca3*cb**2*ee**2*sa1*sa2*sb)/(4.*cw**2) - (ca1*cb**2*ee**2*sa3*sb)/(4.*cw**2) - (ca1*ca3*cb*ee**2*sa2*sb**2)/(4.*cw**2) + (cb*ee**2*sa1*sa3*sb**2)/(4.*cw**2) - (ca3*ee**2*sa1*sa2*sb**3)/(4.*cw**2) - (ca1*ee**2*sa3*sb**3)/(4.*cw**2) + (ca1*ca3*cb**3*ee**2*sa2)/(4.*sw**2) - (cb**3*ee**2*sa1*sa3)/(4.*sw**2) + (ca3*cb**2*ee**2*sa1*sa2*sb)/(4.*sw**2) + (ca1*cb**2*ee**2*sa3*sb)/(4.*sw**2) + (ca1*ca3*cb*ee**2*sa2*sb**2)/(4.*sw**2) - (cb*ee**2*sa1*sa3*sb**2)/(4.*sw**2) + (ca3*ee**2*sa1*sa2*sb**3)/(4.*sw**2) + (ca1*ee**2*sa3*sb**3)/(4.*sw**2)',
                  order = {'QED':2})

GC_300 = Coupling(name = 'GC_300',
                  value = '(ca1*ca3*cb**3*l4*sa2)/2. - (ca1*ca3*cb**3*l5*sa2)/2. - (cb**3*l4*sa1*sa3)/2. + (cb**3*l5*sa1*sa3)/2. + (ca3*cb**2*l4*sa1*sa2*sb)/2. - (ca3*cb**2*l5*sa1*sa2*sb)/2. + (ca1*cb**2*l4*sa3*sb)/2. - (ca1*cb**2*l5*sa3*sb)/2. + (ca1*ca3*cb*l4*sa2*sb**2)/2. - (ca1*ca3*cb*l5*sa2*sb**2)/2. - (cb*l4*sa1*sa3*sb**2)/2. + (cb*l5*sa1*sa3*sb**2)/2. + (ca3*l4*sa1*sa2*sb**3)/2. - (ca3*l5*sa1*sa2*sb**3)/2. + (ca1*l4*sa3*sb**3)/2. - (ca1*l5*sa3*sb**3)/2. + (ca1*ca3*cb**3*ee**2*sa2)/(4.*sw**2) - (cb**3*ee**2*sa1*sa3)/(4.*sw**2) + (ca3*cb**2*ee**2*sa1*sa2*sb)/(4.*sw**2) + (ca1*cb**2*ee**2*sa3*sb)/(4.*sw**2) + (ca1*ca3*cb*ee**2*sa2*sb**2)/(4.*sw**2) - (cb*ee**2*sa1*sa3*sb**2)/(4.*sw**2) + (ca3*ee**2*sa1*sa2*sb**3)/(4.*sw**2) + (ca1*ee**2*sa3*sb**3)/(4.*sw**2)',
                  order = {'QED':2})

GC_301 = Coupling(name = 'GC_301',
                  value = '-(ca1*ca3*cb**3*l4*sa2)/2. + (ca1*ca3*cb**3*l5*sa2)/2. + (cb**3*l4*sa1*sa3)/2. - (cb**3*l5*sa1*sa3)/2. - (ca3*cb**2*l4*sa1*sa2*sb)/2. + (ca3*cb**2*l5*sa1*sa2*sb)/2. - (ca1*cb**2*l4*sa3*sb)/2. + (ca1*cb**2*l5*sa3*sb)/2. - (ca1*ca3*cb*l4*sa2*sb**2)/2. + (ca1*ca3*cb*l5*sa2*sb**2)/2. + (cb*l4*sa1*sa3*sb**2)/2. - (cb*l5*sa1*sa3*sb**2)/2. - (ca3*l4*sa1*sa2*sb**3)/2. + (ca3*l5*sa1*sa2*sb**3)/2. - (ca1*l4*sa3*sb**3)/2. + (ca1*l5*sa3*sb**3)/2. + (ca1*ca3*cb**3*ee**2*sa2)/(4.*sw**2) - (cb**3*ee**2*sa1*sa3)/(4.*sw**2) + (ca3*cb**2*ee**2*sa1*sa2*sb)/(4.*sw**2) + (ca1*cb**2*ee**2*sa3*sb)/(4.*sw**2) + (ca1*ca3*cb*ee**2*sa2*sb**2)/(4.*sw**2) - (cb*ee**2*sa1*sa3*sb**2)/(4.*sw**2) + (ca3*ee**2*sa1*sa2*sb**3)/(4.*sw**2) + (ca1*ee**2*sa3*sb**3)/(4.*sw**2)',
                  order = {'QED':2})

GC_302 = Coupling(name = 'GC_302',
                  value = '(ca3*cb**3*ee**2*sa1*sa2)/(4.*cw**2) + (ca1*cb**3*ee**2*sa3)/(4.*cw**2) - (ca1*ca3*cb**2*ee**2*sa2*sb)/(4.*cw**2) + (cb**2*ee**2*sa1*sa3*sb)/(4.*cw**2) + (ca3*cb*ee**2*sa1*sa2*sb**2)/(4.*cw**2) + (ca1*cb*ee**2*sa3*sb**2)/(4.*cw**2) - (ca1*ca3*ee**2*sa2*sb**3)/(4.*cw**2) + (ee**2*sa1*sa3*sb**3)/(4.*cw**2) - (ca3*cb**3*ee**2*sa1*sa2)/(4.*sw**2) - (ca1*cb**3*ee**2*sa3)/(4.*sw**2) + (ca1*ca3*cb**2*ee**2*sa2*sb)/(4.*sw**2) - (cb**2*ee**2*sa1*sa3*sb)/(4.*sw**2) - (ca3*cb*ee**2*sa1*sa2*sb**2)/(4.*sw**2) - (ca1*cb*ee**2*sa3*sb**2)/(4.*sw**2) + (ca1*ca3*ee**2*sa2*sb**3)/(4.*sw**2) - (ee**2*sa1*sa3*sb**3)/(4.*sw**2)',
                  order = {'QED':2})

GC_303 = Coupling(name = 'GC_303',
                  value = '(ca3*cb**3*l4*sa1*sa2)/2. - (ca3*cb**3*l5*sa1*sa2)/2. + (ca1*cb**3*l4*sa3)/2. - (ca1*cb**3*l5*sa3)/2. - (ca1*ca3*cb**2*l4*sa2*sb)/2. + (ca1*ca3*cb**2*l5*sa2*sb)/2. + (cb**2*l4*sa1*sa3*sb)/2. - (cb**2*l5*sa1*sa3*sb)/2. + (ca3*cb*l4*sa1*sa2*sb**2)/2. - (ca3*cb*l5*sa1*sa2*sb**2)/2. + (ca1*cb*l4*sa3*sb**2)/2. - (ca1*cb*l5*sa3*sb**2)/2. - (ca1*ca3*l4*sa2*sb**3)/2. + (ca1*ca3*l5*sa2*sb**3)/2. + (l4*sa1*sa3*sb**3)/2. - (l5*sa1*sa3*sb**3)/2. - (ca3*cb**3*ee**2*sa1*sa2)/(4.*sw**2) - (ca1*cb**3*ee**2*sa3)/(4.*sw**2) + (ca1*ca3*cb**2*ee**2*sa2*sb)/(4.*sw**2) - (cb**2*ee**2*sa1*sa3*sb)/(4.*sw**2) - (ca3*cb*ee**2*sa1*sa2*sb**2)/(4.*sw**2) - (ca1*cb*ee**2*sa3*sb**2)/(4.*sw**2) + (ca1*ca3*ee**2*sa2*sb**3)/(4.*sw**2) - (ee**2*sa1*sa3*sb**3)/(4.*sw**2)',
                  order = {'QED':2})

GC_304 = Coupling(name = 'GC_304',
                  value = '-(ca3*cb**3*l4*sa1*sa2)/2. + (ca3*cb**3*l5*sa1*sa2)/2. - (ca1*cb**3*l4*sa3)/2. + (ca1*cb**3*l5*sa3)/2. + (ca1*ca3*cb**2*l4*sa2*sb)/2. - (ca1*ca3*cb**2*l5*sa2*sb)/2. - (cb**2*l4*sa1*sa3*sb)/2. + (cb**2*l5*sa1*sa3*sb)/2. - (ca3*cb*l4*sa1*sa2*sb**2)/2. + (ca3*cb*l5*sa1*sa2*sb**2)/2. - (ca1*cb*l4*sa3*sb**2)/2. + (ca1*cb*l5*sa3*sb**2)/2. + (ca1*ca3*l4*sa2*sb**3)/2. - (ca1*ca3*l5*sa2*sb**3)/2. - (l4*sa1*sa3*sb**3)/2. + (l5*sa1*sa3*sb**3)/2. - (ca3*cb**3*ee**2*sa1*sa2)/(4.*sw**2) - (ca1*cb**3*ee**2*sa3)/(4.*sw**2) + (ca1*ca3*cb**2*ee**2*sa2*sb)/(4.*sw**2) - (cb**2*ee**2*sa1*sa3*sb)/(4.*sw**2) - (ca3*cb*ee**2*sa1*sa2*sb**2)/(4.*sw**2) - (ca1*cb*ee**2*sa3*sb**2)/(4.*sw**2) + (ca1*ca3*ee**2*sa2*sb**3)/(4.*sw**2) - (ee**2*sa1*sa3*sb**3)/(4.*sw**2)',
                  order = {'QED':2})

GC_305 = Coupling(name = 'GC_305',
                  value = '-(ca3*cb**3*ee**2*sa1*sa2)/(4.*cw**2) - (ca1*cb**3*ee**2*sa3)/(4.*cw**2) + (ca1*ca3*cb**2*ee**2*sa2*sb)/(4.*cw**2) - (cb**2*ee**2*sa1*sa3*sb)/(4.*cw**2) - (ca3*cb*ee**2*sa1*sa2*sb**2)/(4.*cw**2) - (ca1*cb*ee**2*sa3*sb**2)/(4.*cw**2) + (ca1*ca3*ee**2*sa2*sb**3)/(4.*cw**2) - (ee**2*sa1*sa3*sb**3)/(4.*cw**2) + (ca3*cb**3*ee**2*sa1*sa2)/(4.*sw**2) + (ca1*cb**3*ee**2*sa3)/(4.*sw**2) - (ca1*ca3*cb**2*ee**2*sa2*sb)/(4.*sw**2) + (cb**2*ee**2*sa1*sa3*sb)/(4.*sw**2) + (ca3*cb*ee**2*sa1*sa2*sb**2)/(4.*sw**2) + (ca1*cb*ee**2*sa3*sb**2)/(4.*sw**2) - (ca1*ca3*ee**2*sa2*sb**3)/(4.*sw**2) + (ee**2*sa1*sa3*sb**3)/(4.*sw**2)',
                  order = {'QED':2})

GC_306 = Coupling(name = 'GC_306',
                  value = '(ca3*cb**3*l4*sa1*sa2)/2. - (ca3*cb**3*l5*sa1*sa2)/2. + (ca1*cb**3*l4*sa3)/2. - (ca1*cb**3*l5*sa3)/2. - (ca1*ca3*cb**2*l4*sa2*sb)/2. + (ca1*ca3*cb**2*l5*sa2*sb)/2. + (cb**2*l4*sa1*sa3*sb)/2. - (cb**2*l5*sa1*sa3*sb)/2. + (ca3*cb*l4*sa1*sa2*sb**2)/2. - (ca3*cb*l5*sa1*sa2*sb**2)/2. + (ca1*cb*l4*sa3*sb**2)/2. - (ca1*cb*l5*sa3*sb**2)/2. - (ca1*ca3*l4*sa2*sb**3)/2. + (ca1*ca3*l5*sa2*sb**3)/2. + (l4*sa1*sa3*sb**3)/2. - (l5*sa1*sa3*sb**3)/2. + (ca3*cb**3*ee**2*sa1*sa2)/(4.*sw**2) + (ca1*cb**3*ee**2*sa3)/(4.*sw**2) - (ca1*ca3*cb**2*ee**2*sa2*sb)/(4.*sw**2) + (cb**2*ee**2*sa1*sa3*sb)/(4.*sw**2) + (ca3*cb*ee**2*sa1*sa2*sb**2)/(4.*sw**2) + (ca1*cb*ee**2*sa3*sb**2)/(4.*sw**2) - (ca1*ca3*ee**2*sa2*sb**3)/(4.*sw**2) + (ee**2*sa1*sa3*sb**3)/(4.*sw**2)',
                  order = {'QED':2})

GC_307 = Coupling(name = 'GC_307',
                  value = '-(ca3*cb**3*l4*sa1*sa2)/2. + (ca3*cb**3*l5*sa1*sa2)/2. - (ca1*cb**3*l4*sa3)/2. + (ca1*cb**3*l5*sa3)/2. + (ca1*ca3*cb**2*l4*sa2*sb)/2. - (ca1*ca3*cb**2*l5*sa2*sb)/2. - (cb**2*l4*sa1*sa3*sb)/2. + (cb**2*l5*sa1*sa3*sb)/2. - (ca3*cb*l4*sa1*sa2*sb**2)/2. + (ca3*cb*l5*sa1*sa2*sb**2)/2. - (ca1*cb*l4*sa3*sb**2)/2. + (ca1*cb*l5*sa3*sb**2)/2. + (ca1*ca3*l4*sa2*sb**3)/2. - (ca1*ca3*l5*sa2*sb**3)/2. - (l4*sa1*sa3*sb**3)/2. + (l5*sa1*sa3*sb**3)/2. + (ca3*cb**3*ee**2*sa1*sa2)/(4.*sw**2) + (ca1*cb**3*ee**2*sa3)/(4.*sw**2) - (ca1*ca3*cb**2*ee**2*sa2*sb)/(4.*sw**2) + (cb**2*ee**2*sa1*sa3*sb)/(4.*sw**2) + (ca3*cb*ee**2*sa1*sa2*sb**2)/(4.*sw**2) + (ca1*cb*ee**2*sa3*sb**2)/(4.*sw**2) - (ca1*ca3*ee**2*sa2*sb**3)/(4.*sw**2) + (ee**2*sa1*sa3*sb**3)/(4.*sw**2)',
                  order = {'QED':2})

GC_308 = Coupling(name = 'GC_308',
                  value = '(ca1*ca3*cb**3*ee**2)/(4.*cw**2) - (cb**3*ee**2*sa1*sa2*sa3)/(4.*cw**2) + (ca3*cb**2*ee**2*sa1*sb)/(4.*cw**2) + (ca1*cb**2*ee**2*sa2*sa3*sb)/(4.*cw**2) + (ca1*ca3*cb*ee**2*sb**2)/(4.*cw**2) - (cb*ee**2*sa1*sa2*sa3*sb**2)/(4.*cw**2) + (ca3*ee**2*sa1*sb**3)/(4.*cw**2) + (ca1*ee**2*sa2*sa3*sb**3)/(4.*cw**2) - (ca1*ca3*cb**3*ee**2)/(4.*sw**2) + (cb**3*ee**2*sa1*sa2*sa3)/(4.*sw**2) - (ca3*cb**2*ee**2*sa1*sb)/(4.*sw**2) - (ca1*cb**2*ee**2*sa2*sa3*sb)/(4.*sw**2) - (ca1*ca3*cb*ee**2*sb**2)/(4.*sw**2) + (cb*ee**2*sa1*sa2*sa3*sb**2)/(4.*sw**2) - (ca3*ee**2*sa1*sb**3)/(4.*sw**2) - (ca1*ee**2*sa2*sa3*sb**3)/(4.*sw**2)',
                  order = {'QED':2})

GC_309 = Coupling(name = 'GC_309',
                  value = '(ca1*ca3*cb**3*l4)/2. - (ca1*ca3*cb**3*l5)/2. - (cb**3*l4*sa1*sa2*sa3)/2. + (cb**3*l5*sa1*sa2*sa3)/2. + (ca3*cb**2*l4*sa1*sb)/2. - (ca3*cb**2*l5*sa1*sb)/2. + (ca1*cb**2*l4*sa2*sa3*sb)/2. - (ca1*cb**2*l5*sa2*sa3*sb)/2. + (ca1*ca3*cb*l4*sb**2)/2. - (ca1*ca3*cb*l5*sb**2)/2. - (cb*l4*sa1*sa2*sa3*sb**2)/2. + (cb*l5*sa1*sa2*sa3*sb**2)/2. + (ca3*l4*sa1*sb**3)/2. - (ca3*l5*sa1*sb**3)/2. + (ca1*l4*sa2*sa3*sb**3)/2. - (ca1*l5*sa2*sa3*sb**3)/2. - (ca1*ca3*cb**3*ee**2)/(4.*sw**2) + (cb**3*ee**2*sa1*sa2*sa3)/(4.*sw**2) - (ca3*cb**2*ee**2*sa1*sb)/(4.*sw**2) - (ca1*cb**2*ee**2*sa2*sa3*sb)/(4.*sw**2) - (ca1*ca3*cb*ee**2*sb**2)/(4.*sw**2) + (cb*ee**2*sa1*sa2*sa3*sb**2)/(4.*sw**2) - (ca3*ee**2*sa1*sb**3)/(4.*sw**2) - (ca1*ee**2*sa2*sa3*sb**3)/(4.*sw**2)',
                  order = {'QED':2})

GC_310 = Coupling(name = 'GC_310',
                  value = '-(ca1*ca3*cb**3*l4)/2. + (ca1*ca3*cb**3*l5)/2. + (cb**3*l4*sa1*sa2*sa3)/2. - (cb**3*l5*sa1*sa2*sa3)/2. - (ca3*cb**2*l4*sa1*sb)/2. + (ca3*cb**2*l5*sa1*sb)/2. - (ca1*cb**2*l4*sa2*sa3*sb)/2. + (ca1*cb**2*l5*sa2*sa3*sb)/2. - (ca1*ca3*cb*l4*sb**2)/2. + (ca1*ca3*cb*l5*sb**2)/2. + (cb*l4*sa1*sa2*sa3*sb**2)/2. - (cb*l5*sa1*sa2*sa3*sb**2)/2. - (ca3*l4*sa1*sb**3)/2. + (ca3*l5*sa1*sb**3)/2. - (ca1*l4*sa2*sa3*sb**3)/2. + (ca1*l5*sa2*sa3*sb**3)/2. - (ca1*ca3*cb**3*ee**2)/(4.*sw**2) + (cb**3*ee**2*sa1*sa2*sa3)/(4.*sw**2) - (ca3*cb**2*ee**2*sa1*sb)/(4.*sw**2) - (ca1*cb**2*ee**2*sa2*sa3*sb)/(4.*sw**2) - (ca1*ca3*cb*ee**2*sb**2)/(4.*sw**2) + (cb*ee**2*sa1*sa2*sa3*sb**2)/(4.*sw**2) - (ca3*ee**2*sa1*sb**3)/(4.*sw**2) - (ca1*ee**2*sa2*sa3*sb**3)/(4.*sw**2)',
                  order = {'QED':2})

GC_311 = Coupling(name = 'GC_311',
                  value = '-(ca1*ca3*cb**3*ee**2)/(4.*cw**2) + (cb**3*ee**2*sa1*sa2*sa3)/(4.*cw**2) - (ca3*cb**2*ee**2*sa1*sb)/(4.*cw**2) - (ca1*cb**2*ee**2*sa2*sa3*sb)/(4.*cw**2) - (ca1*ca3*cb*ee**2*sb**2)/(4.*cw**2) + (cb*ee**2*sa1*sa2*sa3*sb**2)/(4.*cw**2) - (ca3*ee**2*sa1*sb**3)/(4.*cw**2) - (ca1*ee**2*sa2*sa3*sb**3)/(4.*cw**2) + (ca1*ca3*cb**3*ee**2)/(4.*sw**2) - (cb**3*ee**2*sa1*sa2*sa3)/(4.*sw**2) + (ca3*cb**2*ee**2*sa1*sb)/(4.*sw**2) + (ca1*cb**2*ee**2*sa2*sa3*sb)/(4.*sw**2) + (ca1*ca3*cb*ee**2*sb**2)/(4.*sw**2) - (cb*ee**2*sa1*sa2*sa3*sb**2)/(4.*sw**2) + (ca3*ee**2*sa1*sb**3)/(4.*sw**2) + (ca1*ee**2*sa2*sa3*sb**3)/(4.*sw**2)',
                  order = {'QED':2})

GC_312 = Coupling(name = 'GC_312',
                  value = '(ca1*ca3*cb**3*l4)/2. - (ca1*ca3*cb**3*l5)/2. - (cb**3*l4*sa1*sa2*sa3)/2. + (cb**3*l5*sa1*sa2*sa3)/2. + (ca3*cb**2*l4*sa1*sb)/2. - (ca3*cb**2*l5*sa1*sb)/2. + (ca1*cb**2*l4*sa2*sa3*sb)/2. - (ca1*cb**2*l5*sa2*sa3*sb)/2. + (ca1*ca3*cb*l4*sb**2)/2. - (ca1*ca3*cb*l5*sb**2)/2. - (cb*l4*sa1*sa2*sa3*sb**2)/2. + (cb*l5*sa1*sa2*sa3*sb**2)/2. + (ca3*l4*sa1*sb**3)/2. - (ca3*l5*sa1*sb**3)/2. + (ca1*l4*sa2*sa3*sb**3)/2. - (ca1*l5*sa2*sa3*sb**3)/2. + (ca1*ca3*cb**3*ee**2)/(4.*sw**2) - (cb**3*ee**2*sa1*sa2*sa3)/(4.*sw**2) + (ca3*cb**2*ee**2*sa1*sb)/(4.*sw**2) + (ca1*cb**2*ee**2*sa2*sa3*sb)/(4.*sw**2) + (ca1*ca3*cb*ee**2*sb**2)/(4.*sw**2) - (cb*ee**2*sa1*sa2*sa3*sb**2)/(4.*sw**2) + (ca3*ee**2*sa1*sb**3)/(4.*sw**2) + (ca1*ee**2*sa2*sa3*sb**3)/(4.*sw**2)',
                  order = {'QED':2})

GC_313 = Coupling(name = 'GC_313',
                  value = '-(ca1*ca3*cb**3*l4)/2. + (ca1*ca3*cb**3*l5)/2. + (cb**3*l4*sa1*sa2*sa3)/2. - (cb**3*l5*sa1*sa2*sa3)/2. - (ca3*cb**2*l4*sa1*sb)/2. + (ca3*cb**2*l5*sa1*sb)/2. - (ca1*cb**2*l4*sa2*sa3*sb)/2. + (ca1*cb**2*l5*sa2*sa3*sb)/2. - (ca1*ca3*cb*l4*sb**2)/2. + (ca1*ca3*cb*l5*sb**2)/2. + (cb*l4*sa1*sa2*sa3*sb**2)/2. - (cb*l5*sa1*sa2*sa3*sb**2)/2. - (ca3*l4*sa1*sb**3)/2. + (ca3*l5*sa1*sb**3)/2. - (ca1*l4*sa2*sa3*sb**3)/2. + (ca1*l5*sa2*sa3*sb**3)/2. + (ca1*ca3*cb**3*ee**2)/(4.*sw**2) - (cb**3*ee**2*sa1*sa2*sa3)/(4.*sw**2) + (ca3*cb**2*ee**2*sa1*sb)/(4.*sw**2) + (ca1*cb**2*ee**2*sa2*sa3*sb)/(4.*sw**2) + (ca1*ca3*cb*ee**2*sb**2)/(4.*sw**2) - (cb*ee**2*sa1*sa2*sa3*sb**2)/(4.*sw**2) + (ca3*ee**2*sa1*sb**3)/(4.*sw**2) + (ca1*ee**2*sa2*sa3*sb**3)/(4.*sw**2)',
                  order = {'QED':2})

GC_314 = Coupling(name = 'GC_314',
                  value = '(ca3*cb**3*ee**2*sa1)/(4.*cw**2) + (ca1*cb**3*ee**2*sa2*sa3)/(4.*cw**2) - (ca1*ca3*cb**2*ee**2*sb)/(4.*cw**2) + (cb**2*ee**2*sa1*sa2*sa3*sb)/(4.*cw**2) + (ca3*cb*ee**2*sa1*sb**2)/(4.*cw**2) + (ca1*cb*ee**2*sa2*sa3*sb**2)/(4.*cw**2) - (ca1*ca3*ee**2*sb**3)/(4.*cw**2) + (ee**2*sa1*sa2*sa3*sb**3)/(4.*cw**2) - (ca3*cb**3*ee**2*sa1)/(4.*sw**2) - (ca1*cb**3*ee**2*sa2*sa3)/(4.*sw**2) + (ca1*ca3*cb**2*ee**2*sb)/(4.*sw**2) - (cb**2*ee**2*sa1*sa2*sa3*sb)/(4.*sw**2) - (ca3*cb*ee**2*sa1*sb**2)/(4.*sw**2) - (ca1*cb*ee**2*sa2*sa3*sb**2)/(4.*sw**2) + (ca1*ca3*ee**2*sb**3)/(4.*sw**2) - (ee**2*sa1*sa2*sa3*sb**3)/(4.*sw**2)',
                  order = {'QED':2})

GC_315 = Coupling(name = 'GC_315',
                  value = '(ca3*cb**3*l4*sa1)/2. - (ca3*cb**3*l5*sa1)/2. + (ca1*cb**3*l4*sa2*sa3)/2. - (ca1*cb**3*l5*sa2*sa3)/2. - (ca1*ca3*cb**2*l4*sb)/2. + (ca1*ca3*cb**2*l5*sb)/2. + (cb**2*l4*sa1*sa2*sa3*sb)/2. - (cb**2*l5*sa1*sa2*sa3*sb)/2. + (ca3*cb*l4*sa1*sb**2)/2. - (ca3*cb*l5*sa1*sb**2)/2. + (ca1*cb*l4*sa2*sa3*sb**2)/2. - (ca1*cb*l5*sa2*sa3*sb**2)/2. - (ca1*ca3*l4*sb**3)/2. + (ca1*ca3*l5*sb**3)/2. + (l4*sa1*sa2*sa3*sb**3)/2. - (l5*sa1*sa2*sa3*sb**3)/2. - (ca3*cb**3*ee**2*sa1)/(4.*sw**2) - (ca1*cb**3*ee**2*sa2*sa3)/(4.*sw**2) + (ca1*ca3*cb**2*ee**2*sb)/(4.*sw**2) - (cb**2*ee**2*sa1*sa2*sa3*sb)/(4.*sw**2) - (ca3*cb*ee**2*sa1*sb**2)/(4.*sw**2) - (ca1*cb*ee**2*sa2*sa3*sb**2)/(4.*sw**2) + (ca1*ca3*ee**2*sb**3)/(4.*sw**2) - (ee**2*sa1*sa2*sa3*sb**3)/(4.*sw**2)',
                  order = {'QED':2})

GC_316 = Coupling(name = 'GC_316',
                  value = '-(ca3*cb**3*l4*sa1)/2. + (ca3*cb**3*l5*sa1)/2. - (ca1*cb**3*l4*sa2*sa3)/2. + (ca1*cb**3*l5*sa2*sa3)/2. + (ca1*ca3*cb**2*l4*sb)/2. - (ca1*ca3*cb**2*l5*sb)/2. - (cb**2*l4*sa1*sa2*sa3*sb)/2. + (cb**2*l5*sa1*sa2*sa3*sb)/2. - (ca3*cb*l4*sa1*sb**2)/2. + (ca3*cb*l5*sa1*sb**2)/2. - (ca1*cb*l4*sa2*sa3*sb**2)/2. + (ca1*cb*l5*sa2*sa3*sb**2)/2. + (ca1*ca3*l4*sb**3)/2. - (ca1*ca3*l5*sb**3)/2. - (l4*sa1*sa2*sa3*sb**3)/2. + (l5*sa1*sa2*sa3*sb**3)/2. - (ca3*cb**3*ee**2*sa1)/(4.*sw**2) - (ca1*cb**3*ee**2*sa2*sa3)/(4.*sw**2) + (ca1*ca3*cb**2*ee**2*sb)/(4.*sw**2) - (cb**2*ee**2*sa1*sa2*sa3*sb)/(4.*sw**2) - (ca3*cb*ee**2*sa1*sb**2)/(4.*sw**2) - (ca1*cb*ee**2*sa2*sa3*sb**2)/(4.*sw**2) + (ca1*ca3*ee**2*sb**3)/(4.*sw**2) - (ee**2*sa1*sa2*sa3*sb**3)/(4.*sw**2)',
                  order = {'QED':2})

GC_317 = Coupling(name = 'GC_317',
                  value = '-(ca3*cb**3*ee**2*sa1)/(4.*cw**2) - (ca1*cb**3*ee**2*sa2*sa3)/(4.*cw**2) + (ca1*ca3*cb**2*ee**2*sb)/(4.*cw**2) - (cb**2*ee**2*sa1*sa2*sa3*sb)/(4.*cw**2) - (ca3*cb*ee**2*sa1*sb**2)/(4.*cw**2) - (ca1*cb*ee**2*sa2*sa3*sb**2)/(4.*cw**2) + (ca1*ca3*ee**2*sb**3)/(4.*cw**2) - (ee**2*sa1*sa2*sa3*sb**3)/(4.*cw**2) + (ca3*cb**3*ee**2*sa1)/(4.*sw**2) + (ca1*cb**3*ee**2*sa2*sa3)/(4.*sw**2) - (ca1*ca3*cb**2*ee**2*sb)/(4.*sw**2) + (cb**2*ee**2*sa1*sa2*sa3*sb)/(4.*sw**2) + (ca3*cb*ee**2*sa1*sb**2)/(4.*sw**2) + (ca1*cb*ee**2*sa2*sa3*sb**2)/(4.*sw**2) - (ca1*ca3*ee**2*sb**3)/(4.*sw**2) + (ee**2*sa1*sa2*sa3*sb**3)/(4.*sw**2)',
                  order = {'QED':2})

GC_318 = Coupling(name = 'GC_318',
                  value = '(ca3*cb**3*l4*sa1)/2. - (ca3*cb**3*l5*sa1)/2. + (ca1*cb**3*l4*sa2*sa3)/2. - (ca1*cb**3*l5*sa2*sa3)/2. - (ca1*ca3*cb**2*l4*sb)/2. + (ca1*ca3*cb**2*l5*sb)/2. + (cb**2*l4*sa1*sa2*sa3*sb)/2. - (cb**2*l5*sa1*sa2*sa3*sb)/2. + (ca3*cb*l4*sa1*sb**2)/2. - (ca3*cb*l5*sa1*sb**2)/2. + (ca1*cb*l4*sa2*sa3*sb**2)/2. - (ca1*cb*l5*sa2*sa3*sb**2)/2. - (ca1*ca3*l4*sb**3)/2. + (ca1*ca3*l5*sb**3)/2. + (l4*sa1*sa2*sa3*sb**3)/2. - (l5*sa1*sa2*sa3*sb**3)/2. + (ca3*cb**3*ee**2*sa1)/(4.*sw**2) + (ca1*cb**3*ee**2*sa2*sa3)/(4.*sw**2) - (ca1*ca3*cb**2*ee**2*sb)/(4.*sw**2) + (cb**2*ee**2*sa1*sa2*sa3*sb)/(4.*sw**2) + (ca3*cb*ee**2*sa1*sb**2)/(4.*sw**2) + (ca1*cb*ee**2*sa2*sa3*sb**2)/(4.*sw**2) - (ca1*ca3*ee**2*sb**3)/(4.*sw**2) + (ee**2*sa1*sa2*sa3*sb**3)/(4.*sw**2)',
                  order = {'QED':2})

GC_319 = Coupling(name = 'GC_319',
                  value = '-(ca3*cb**3*l4*sa1)/2. + (ca3*cb**3*l5*sa1)/2. - (ca1*cb**3*l4*sa2*sa3)/2. + (ca1*cb**3*l5*sa2*sa3)/2. + (ca1*ca3*cb**2*l4*sb)/2. - (ca1*ca3*cb**2*l5*sb)/2. - (cb**2*l4*sa1*sa2*sa3*sb)/2. + (cb**2*l5*sa1*sa2*sa3*sb)/2. - (ca3*cb*l4*sa1*sb**2)/2. + (ca3*cb*l5*sa1*sb**2)/2. - (ca1*cb*l4*sa2*sa3*sb**2)/2. + (ca1*cb*l5*sa2*sa3*sb**2)/2. + (ca1*ca3*l4*sb**3)/2. - (ca1*ca3*l5*sb**3)/2. - (l4*sa1*sa2*sa3*sb**3)/2. + (l5*sa1*sa2*sa3*sb**3)/2. + (ca3*cb**3*ee**2*sa1)/(4.*sw**2) + (ca1*cb**3*ee**2*sa2*sa3)/(4.*sw**2) - (ca1*ca3*cb**2*ee**2*sb)/(4.*sw**2) + (cb**2*ee**2*sa1*sa2*sa3*sb)/(4.*sw**2) + (ca3*cb*ee**2*sa1*sb**2)/(4.*sw**2) + (ca1*cb*ee**2*sa2*sa3*sb**2)/(4.*sw**2) - (ca1*ca3*ee**2*sb**3)/(4.*sw**2) + (ee**2*sa1*sa2*sa3*sb**3)/(4.*sw**2)',
                  order = {'QED':2})

GC_320 = Coupling(name = 'GC_320',
                  value = '-(cb**4*ee**2*complex(0,1))/(4.*cw**2) - 2*cb**4*complex(0,1)*l2 - (cb**2*ee**2*complex(0,1)*sb**2)/(2.*cw**2) - 4*cb**2*complex(0,1)*l3*sb**2 - 4*cb**2*complex(0,1)*l4*sb**2 - 4*cb**2*complex(0,1)*l5*sb**2 - (ee**2*complex(0,1)*sb**4)/(4.*cw**2) - 2*complex(0,1)*l1*sb**4 - (cb**4*ee**2*complex(0,1))/(4.*sw**2) - (cb**2*ee**2*complex(0,1)*sb**2)/(2.*sw**2) - (ee**2*complex(0,1)*sb**4)/(4.*sw**2)',
                  order = {'QED':2})

GC_321 = Coupling(name = 'GC_321',
                  value = '-(cb**4*ee**2*complex(0,1))/(4.*cw**2) - 2*cb**4*complex(0,1)*l1 - (cb**2*ee**2*complex(0,1)*sb**2)/(2.*cw**2) - 4*cb**2*complex(0,1)*l3*sb**2 - 4*cb**2*complex(0,1)*l4*sb**2 - 4*cb**2*complex(0,1)*l5*sb**2 - (ee**2*complex(0,1)*sb**4)/(4.*cw**2) - 2*complex(0,1)*l2*sb**4 - (cb**4*ee**2*complex(0,1))/(4.*sw**2) - (cb**2*ee**2*complex(0,1)*sb**2)/(2.*sw**2) - (ee**2*complex(0,1)*sb**4)/(4.*sw**2)',
                  order = {'QED':2})

GC_322 = Coupling(name = 'GC_322',
                  value = '-(cb**4*ee**2*complex(0,1))/(4.*cw**2) - cb**4*complex(0,1)*l3 - cb**4*complex(0,1)*l4 - (cb**2*ee**2*complex(0,1)*sb**2)/(2.*cw**2) - 2*cb**2*complex(0,1)*l1*sb**2 - 2*cb**2*complex(0,1)*l2*sb**2 + 2*cb**2*complex(0,1)*l3*sb**2 + 2*cb**2*complex(0,1)*l4*sb**2 + 4*cb**2*complex(0,1)*l5*sb**2 - (ee**2*complex(0,1)*sb**4)/(4.*cw**2) - complex(0,1)*l3*sb**4 - complex(0,1)*l4*sb**4 - (cb**4*ee**2*complex(0,1))/(4.*sw**2) - (cb**2*ee**2*complex(0,1)*sb**2)/(2.*sw**2) - (ee**2*complex(0,1)*sb**4)/(4.*sw**2)',
                  order = {'QED':2})

GC_323 = Coupling(name = 'GC_323',
                  value = '-(cb**4*complex(0,1)*l4)/2. - (cb**4*complex(0,1)*l5)/2. - cb**2*complex(0,1)*l1*sb**2 - cb**2*complex(0,1)*l2*sb**2 + 2*cb**2*complex(0,1)*l3*sb**2 + cb**2*complex(0,1)*l4*sb**2 + cb**2*complex(0,1)*l5*sb**2 - (complex(0,1)*l4*sb**4)/2. - (complex(0,1)*l5*sb**4)/2. - (cb**4*ee**2*complex(0,1))/(4.*sw**2) - (cb**2*ee**2*complex(0,1)*sb**2)/(2.*sw**2) - (ee**2*complex(0,1)*sb**4)/(4.*sw**2)',
                  order = {'QED':2})

GC_324 = Coupling(name = 'GC_324',
                  value = '-(cb**4*complex(0,1)*l2) - 2*cb**2*complex(0,1)*l3*sb**2 - 2*cb**2*complex(0,1)*l4*sb**2 - 2*cb**2*complex(0,1)*l5*sb**2 - complex(0,1)*l1*sb**4 + (cb**4*ee**2*complex(0,1))/(4.*sw**2) + (cb**2*ee**2*complex(0,1)*sb**2)/(2.*sw**2) + (ee**2*complex(0,1)*sb**4)/(4.*sw**2)',
                  order = {'QED':2})

GC_325 = Coupling(name = 'GC_325',
                  value = '-(cb**4*complex(0,1)*l1) - 2*cb**2*complex(0,1)*l3*sb**2 - 2*cb**2*complex(0,1)*l4*sb**2 - 2*cb**2*complex(0,1)*l5*sb**2 - complex(0,1)*l2*sb**4 + (cb**4*ee**2*complex(0,1))/(4.*sw**2) + (cb**2*ee**2*complex(0,1)*sb**2)/(2.*sw**2) + (ee**2*complex(0,1)*sb**4)/(4.*sw**2)',
                  order = {'QED':2})

GC_326 = Coupling(name = 'GC_326',
                  value = '(cb**4*ee**2*complex(0,1))/(4.*cw**2) - cb**4*complex(0,1)*l3 - cb**4*complex(0,1)*l4 + (cb**2*ee**2*complex(0,1)*sb**2)/(2.*cw**2) - 2*cb**2*complex(0,1)*l1*sb**2 - 2*cb**2*complex(0,1)*l2*sb**2 + 2*cb**2*complex(0,1)*l3*sb**2 + 2*cb**2*complex(0,1)*l4*sb**2 + 4*cb**2*complex(0,1)*l5*sb**2 + (ee**2*complex(0,1)*sb**4)/(4.*cw**2) - complex(0,1)*l3*sb**4 - complex(0,1)*l4*sb**4 + (cb**4*ee**2*complex(0,1))/(4.*sw**2) + (cb**2*ee**2*complex(0,1)*sb**2)/(2.*sw**2) + (ee**2*complex(0,1)*sb**4)/(4.*sw**2)',
                  order = {'QED':2})

GC_327 = Coupling(name = 'GC_327',
                  value = '-(cb**4*complex(0,1)*l4)/2. - (cb**4*complex(0,1)*l5)/2. - cb**2*complex(0,1)*l1*sb**2 - cb**2*complex(0,1)*l2*sb**2 + 2*cb**2*complex(0,1)*l3*sb**2 + cb**2*complex(0,1)*l4*sb**2 + cb**2*complex(0,1)*l5*sb**2 - (complex(0,1)*l4*sb**4)/2. - (complex(0,1)*l5*sb**4)/2. + (cb**4*ee**2*complex(0,1))/(4.*sw**2) + (cb**2*ee**2*complex(0,1)*sb**2)/(2.*sw**2) + (ee**2*complex(0,1)*sb**4)/(4.*sw**2)',
                  order = {'QED':2})

GC_328 = Coupling(name = 'GC_328',
                  value = '-(cb**4*complex(0,1)*l2) - 2*cb**2*complex(0,1)*l3*sb**2 - 2*cb**2*complex(0,1)*l4*sb**2 - 2*cb**2*complex(0,1)*l5*sb**2 - complex(0,1)*l1*sb**4 - (cb**4*ee**2*complex(0,1))/(2.*sw**2) - (cb**2*ee**2*complex(0,1)*sb**2)/sw**2 - (ee**2*complex(0,1)*sb**4)/(2.*sw**2)',
                  order = {'QED':2})

GC_329 = Coupling(name = 'GC_329',
                  value = '-(cb**4*complex(0,1)*l1) - 2*cb**2*complex(0,1)*l3*sb**2 - 2*cb**2*complex(0,1)*l4*sb**2 - 2*cb**2*complex(0,1)*l5*sb**2 - complex(0,1)*l2*sb**4 - (cb**4*ee**2*complex(0,1))/(2.*sw**2) - (cb**2*ee**2*complex(0,1)*sb**2)/sw**2 - (ee**2*complex(0,1)*sb**4)/(2.*sw**2)',
                  order = {'QED':2})

GC_330 = Coupling(name = 'GC_330',
                  value = '(cb**4*ee**2*complex(0,1))/(2.*cw**2) - 2*cb**4*complex(0,1)*l2 + (cb**2*ee**2*complex(0,1)*sb**2)/cw**2 - 4*cb**2*complex(0,1)*l3*sb**2 - 4*cb**2*complex(0,1)*l4*sb**2 - 4*cb**2*complex(0,1)*l5*sb**2 + (ee**2*complex(0,1)*sb**4)/(2.*cw**2) - 2*complex(0,1)*l1*sb**4 + (cb**4*ee**2*complex(0,1))/(2.*sw**2) + (cb**2*ee**2*complex(0,1)*sb**2)/sw**2 + (ee**2*complex(0,1)*sb**4)/(2.*sw**2)',
                  order = {'QED':2})

GC_331 = Coupling(name = 'GC_331',
                  value = '(cb**4*ee**2*complex(0,1))/(2.*cw**2) - 2*cb**4*complex(0,1)*l1 + (cb**2*ee**2*complex(0,1)*sb**2)/cw**2 - 4*cb**2*complex(0,1)*l3*sb**2 - 4*cb**2*complex(0,1)*l4*sb**2 - 4*cb**2*complex(0,1)*l5*sb**2 + (ee**2*complex(0,1)*sb**4)/(2.*cw**2) - 2*complex(0,1)*l2*sb**4 + (cb**4*ee**2*complex(0,1))/(2.*sw**2) + (cb**2*ee**2*complex(0,1)*sb**2)/sw**2 + (ee**2*complex(0,1)*sb**4)/(2.*sw**2)',
                  order = {'QED':2})

GC_332 = Coupling(name = 'GC_332',
                  value = '(ca2*cb*ee*complex(0,1)*sa1)/(2.*sw) - (ca1*ca2*ee*complex(0,1)*sb)/(2.*sw)',
                  order = {'QED':1})

GC_333 = Coupling(name = 'GC_333',
                  value = '-(ca2*cb*ee*complex(0,1)*sa1)/(2.*sw) + (ca1*ca2*ee*complex(0,1)*sb)/(2.*sw)',
                  order = {'QED':1})

GC_334 = Coupling(name = 'GC_334',
                  value = '(ca2*cb*ee**2*complex(0,1)*sa1)/(2.*sw) - (ca1*ca2*ee**2*complex(0,1)*sb)/(2.*sw)',
                  order = {'QED':2})

GC_335 = Coupling(name = 'GC_335',
                  value = '-(ca2*cb*ee**2*complex(0,1)*sa1)/(2.*sw) + (ca1*ca2*ee**2*complex(0,1)*sb)/(2.*sw)',
                  order = {'QED':2})

GC_336 = Coupling(name = 'GC_336',
                  value = '-((ca2*cb*ee**2*complex(0,1)*sa1)/sw) + (ca1*ca2*ee**2*complex(0,1)*sb)/sw',
                  order = {'QED':2})

GC_337 = Coupling(name = 'GC_337',
                  value = '-(ca1*ca2*cb*ee*complex(0,1))/(2.*sw) - (ca2*ee*complex(0,1)*sa1*sb)/(2.*sw)',
                  order = {'QED':1})

GC_338 = Coupling(name = 'GC_338',
                  value = '(ca1*ca2*cb*ee*complex(0,1))/(2.*sw) + (ca2*ee*complex(0,1)*sa1*sb)/(2.*sw)',
                  order = {'QED':1})

GC_339 = Coupling(name = 'GC_339',
                  value = '-(ca1*ca2*cb*ee**2*complex(0,1))/(2.*sw) - (ca2*ee**2*complex(0,1)*sa1*sb)/(2.*sw)',
                  order = {'QED':2})

GC_340 = Coupling(name = 'GC_340',
                  value = '(ca1*ca2*cb*ee**2*complex(0,1))/(2.*sw) + (ca2*ee**2*complex(0,1)*sa1*sb)/(2.*sw)',
                  order = {'QED':2})

GC_341 = Coupling(name = 'GC_341',
                  value = '-((ca1*ca2*cb*ee**2*complex(0,1))/sw) - (ca2*ee**2*complex(0,1)*sa1*sb)/sw',
                  order = {'QED':2})

GC_342 = Coupling(name = 'GC_342',
                  value = '-(ca1*ca3*cb*ee*complex(0,1)*sa2)/(2.*sw) + (cb*ee*complex(0,1)*sa1*sa3)/(2.*sw) - (ca3*ee*complex(0,1)*sa1*sa2*sb)/(2.*sw) - (ca1*ee*complex(0,1)*sa3*sb)/(2.*sw)',
                  order = {'QED':1})

GC_343 = Coupling(name = 'GC_343',
                  value = '(ca1*ca3*cb*ee*complex(0,1)*sa2)/(2.*sw) - (cb*ee*complex(0,1)*sa1*sa3)/(2.*sw) + (ca3*ee*complex(0,1)*sa1*sa2*sb)/(2.*sw) + (ca1*ee*complex(0,1)*sa3*sb)/(2.*sw)',
                  order = {'QED':1})

GC_344 = Coupling(name = 'GC_344',
                  value = '-(ca1*ca3*cb*ee**2*complex(0,1)*sa2)/(2.*sw) + (cb*ee**2*complex(0,1)*sa1*sa3)/(2.*sw) - (ca3*ee**2*complex(0,1)*sa1*sa2*sb)/(2.*sw) - (ca1*ee**2*complex(0,1)*sa3*sb)/(2.*sw)',
                  order = {'QED':2})

GC_345 = Coupling(name = 'GC_345',
                  value = '(ca1*ca3*cb*ee**2*complex(0,1)*sa2)/(2.*sw) - (cb*ee**2*complex(0,1)*sa1*sa3)/(2.*sw) + (ca3*ee**2*complex(0,1)*sa1*sa2*sb)/(2.*sw) + (ca1*ee**2*complex(0,1)*sa3*sb)/(2.*sw)',
                  order = {'QED':2})

GC_346 = Coupling(name = 'GC_346',
                  value = '(ca1*ca3*cb*ee**2*complex(0,1)*sa2)/sw - (cb*ee**2*complex(0,1)*sa1*sa3)/sw + (ca3*ee**2*complex(0,1)*sa1*sa2*sb)/sw + (ca1*ee**2*complex(0,1)*sa3*sb)/sw',
                  order = {'QED':2})

GC_347 = Coupling(name = 'GC_347',
                  value = '-(ca3*cb*ee*complex(0,1)*sa1*sa2)/(2.*sw) - (ca1*cb*ee*complex(0,1)*sa3)/(2.*sw) + (ca1*ca3*ee*complex(0,1)*sa2*sb)/(2.*sw) - (ee*complex(0,1)*sa1*sa3*sb)/(2.*sw)',
                  order = {'QED':1})

GC_348 = Coupling(name = 'GC_348',
                  value = '(ca3*cb*ee*complex(0,1)*sa1*sa2)/(2.*sw) + (ca1*cb*ee*complex(0,1)*sa3)/(2.*sw) - (ca1*ca3*ee*complex(0,1)*sa2*sb)/(2.*sw) + (ee*complex(0,1)*sa1*sa3*sb)/(2.*sw)',
                  order = {'QED':1})

GC_349 = Coupling(name = 'GC_349',
                  value = '-(ca3*cb*ee**2*complex(0,1)*sa1*sa2)/(2.*sw) - (ca1*cb*ee**2*complex(0,1)*sa3)/(2.*sw) + (ca1*ca3*ee**2*complex(0,1)*sa2*sb)/(2.*sw) - (ee**2*complex(0,1)*sa1*sa3*sb)/(2.*sw)',
                  order = {'QED':2})

GC_350 = Coupling(name = 'GC_350',
                  value = '(ca3*cb*ee**2*complex(0,1)*sa1*sa2)/(2.*sw) + (ca1*cb*ee**2*complex(0,1)*sa3)/(2.*sw) - (ca1*ca3*ee**2*complex(0,1)*sa2*sb)/(2.*sw) + (ee**2*complex(0,1)*sa1*sa3*sb)/(2.*sw)',
                  order = {'QED':2})

GC_351 = Coupling(name = 'GC_351',
                  value = '(ca3*cb*ee**2*complex(0,1)*sa1*sa2)/sw + (ca1*cb*ee**2*complex(0,1)*sa3)/sw - (ca1*ca3*ee**2*complex(0,1)*sa2*sb)/sw + (ee**2*complex(0,1)*sa1*sa3*sb)/sw',
                  order = {'QED':2})

GC_352 = Coupling(name = 'GC_352',
                  value = '-(ca1*ca3*cb*ee*complex(0,1))/(2.*sw) + (cb*ee*complex(0,1)*sa1*sa2*sa3)/(2.*sw) - (ca3*ee*complex(0,1)*sa1*sb)/(2.*sw) - (ca1*ee*complex(0,1)*sa2*sa3*sb)/(2.*sw)',
                  order = {'QED':1})

GC_353 = Coupling(name = 'GC_353',
                  value = '(ca1*ca3*cb*ee*complex(0,1))/(2.*sw) - (cb*ee*complex(0,1)*sa1*sa2*sa3)/(2.*sw) + (ca3*ee*complex(0,1)*sa1*sb)/(2.*sw) + (ca1*ee*complex(0,1)*sa2*sa3*sb)/(2.*sw)',
                  order = {'QED':1})

GC_354 = Coupling(name = 'GC_354',
                  value = '-(ca1*ca3*cb*ee**2*complex(0,1))/(2.*sw) + (cb*ee**2*complex(0,1)*sa1*sa2*sa3)/(2.*sw) - (ca3*ee**2*complex(0,1)*sa1*sb)/(2.*sw) - (ca1*ee**2*complex(0,1)*sa2*sa3*sb)/(2.*sw)',
                  order = {'QED':2})

GC_355 = Coupling(name = 'GC_355',
                  value = '(ca1*ca3*cb*ee**2*complex(0,1))/(2.*sw) - (cb*ee**2*complex(0,1)*sa1*sa2*sa3)/(2.*sw) + (ca3*ee**2*complex(0,1)*sa1*sb)/(2.*sw) + (ca1*ee**2*complex(0,1)*sa2*sa3*sb)/(2.*sw)',
                  order = {'QED':2})

GC_356 = Coupling(name = 'GC_356',
                  value = '-((ca1*ca3*cb*ee**2*complex(0,1))/sw) + (cb*ee**2*complex(0,1)*sa1*sa2*sa3)/sw - (ca3*ee**2*complex(0,1)*sa1*sb)/sw - (ca1*ee**2*complex(0,1)*sa2*sa3*sb)/sw',
                  order = {'QED':2})

GC_357 = Coupling(name = 'GC_357',
                  value = '-(ca3*cb*ee*complex(0,1)*sa1)/(2.*sw) - (ca1*cb*ee*complex(0,1)*sa2*sa3)/(2.*sw) + (ca1*ca3*ee*complex(0,1)*sb)/(2.*sw) - (ee*complex(0,1)*sa1*sa2*sa3*sb)/(2.*sw)',
                  order = {'QED':1})

GC_358 = Coupling(name = 'GC_358',
                  value = '(ca3*cb*ee*complex(0,1)*sa1)/(2.*sw) + (ca1*cb*ee*complex(0,1)*sa2*sa3)/(2.*sw) - (ca1*ca3*ee*complex(0,1)*sb)/(2.*sw) + (ee*complex(0,1)*sa1*sa2*sa3*sb)/(2.*sw)',
                  order = {'QED':1})

GC_359 = Coupling(name = 'GC_359',
                  value = '-(ca3*cb*ee**2*complex(0,1)*sa1)/(2.*sw) - (ca1*cb*ee**2*complex(0,1)*sa2*sa3)/(2.*sw) + (ca1*ca3*ee**2*complex(0,1)*sb)/(2.*sw) - (ee**2*complex(0,1)*sa1*sa2*sa3*sb)/(2.*sw)',
                  order = {'QED':2})

GC_360 = Coupling(name = 'GC_360',
                  value = '(ca3*cb*ee**2*complex(0,1)*sa1)/(2.*sw) + (ca1*cb*ee**2*complex(0,1)*sa2*sa3)/(2.*sw) - (ca1*ca3*ee**2*complex(0,1)*sb)/(2.*sw) + (ee**2*complex(0,1)*sa1*sa2*sa3*sb)/(2.*sw)',
                  order = {'QED':2})

GC_361 = Coupling(name = 'GC_361',
                  value = '(ca3*cb*ee**2*complex(0,1)*sa1)/sw + (ca1*cb*ee**2*complex(0,1)*sa2*sa3)/sw - (ca1*ca3*ee**2*complex(0,1)*sb)/sw + (ee**2*complex(0,1)*sa1*sa2*sa3*sb)/sw',
                  order = {'QED':2})

GC_362 = Coupling(name = 'GC_362',
                  value = '-(cb**2*ee)/(2.*sw) - (ee*sb**2)/(2.*sw)',
                  order = {'QED':1})

GC_363 = Coupling(name = 'GC_363',
                  value = '(cb**2*ee)/(2.*sw) + (ee*sb**2)/(2.*sw)',
                  order = {'QED':1})

GC_364 = Coupling(name = 'GC_364',
                  value = '-((cb**2*ee**2)/sw) - (ee**2*sb**2)/sw',
                  order = {'QED':2})

GC_365 = Coupling(name = 'GC_365',
                  value = '-(cb**2*ee**2)/(2.*sw) - (ee**2*sb**2)/(2.*sw)',
                  order = {'QED':2})

GC_366 = Coupling(name = 'GC_366',
                  value = '(cb**2*ee**2)/(2.*sw) + (ee**2*sb**2)/(2.*sw)',
                  order = {'QED':2})

GC_367 = Coupling(name = 'GC_367',
                  value = '(cb**2*ee**2)/sw + (ee**2*sb**2)/sw',
                  order = {'QED':2})

GC_368 = Coupling(name = 'GC_368',
                  value = '-((ee**2*complex(0,1))/sw**2)',
                  order = {'QED':2})

GC_369 = Coupling(name = 'GC_369',
                  value = '(ee**2*complex(0,1))/sw**2',
                  order = {'QED':2})

GC_370 = Coupling(name = 'GC_370',
                  value = '(-2*ee**2*complex(0,1))/sw**2',
                  order = {'QED':2})

GC_371 = Coupling(name = 'GC_371',
                  value = '(2*ee**2*complex(0,1))/sw**2',
                  order = {'QED':2})

GC_372 = Coupling(name = 'GC_372',
                  value = '-((cw**2*ee**2*complex(0,1))/sw**2)',
                  order = {'QED':2})

GC_373 = Coupling(name = 'GC_373',
                  value = '(cw**2*ee**2*complex(0,1))/sw**2',
                  order = {'QED':2})

GC_374 = Coupling(name = 'GC_374',
                  value = '(-2*cw**2*ee**2*complex(0,1))/sw**2',
                  order = {'QED':2})

GC_375 = Coupling(name = 'GC_375',
                  value = '(2*cw**2*ee**2*complex(0,1))/sw**2',
                  order = {'QED':2})

GC_376 = Coupling(name = 'GC_376',
                  value = '(ee*complex(0,1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_377 = Coupling(name = 'GC_377',
                  value = '-((cw*ee*complex(0,1))/sw)',
                  order = {'QED':1})

GC_378 = Coupling(name = 'GC_378',
                  value = '(cw*ee*complex(0,1))/sw',
                  order = {'QED':1})

GC_379 = Coupling(name = 'GC_379',
                  value = '(-2*cw*ee*complex(0,1))/sw',
                  order = {'QED':1})

GC_380 = Coupling(name = 'GC_380',
                  value = '(2*cw*ee*complex(0,1))/sw',
                  order = {'QED':1})

GC_381 = Coupling(name = 'GC_381',
                  value = '-((cw*ee**2*complex(0,1))/sw)',
                  order = {'QED':2})

GC_382 = Coupling(name = 'GC_382',
                  value = '(cw*ee**2*complex(0,1))/sw',
                  order = {'QED':2})

GC_383 = Coupling(name = 'GC_383',
                  value = '(-2*cw*ee**2*complex(0,1))/sw',
                  order = {'QED':2})

GC_384 = Coupling(name = 'GC_384',
                  value = '(2*cw*ee**2*complex(0,1))/sw',
                  order = {'QED':2})

GC_385 = Coupling(name = 'GC_385',
                  value = '(ee*complex(0,1)*sw)/(3.*cw)',
                  order = {'QED':1})

GC_386 = Coupling(name = 'GC_386',
                  value = '(-2*ee*complex(0,1)*sw)/(3.*cw)',
                  order = {'QED':1})

GC_387 = Coupling(name = 'GC_387',
                  value = '(ee*complex(0,1)*sw)/cw',
                  order = {'QED':1})

GC_388 = Coupling(name = 'GC_388',
                  value = '-(cw*ee*complex(0,1))/(2.*sw) - (ee*complex(0,1)*sw)/(6.*cw)',
                  order = {'QED':1})

GC_389 = Coupling(name = 'GC_389',
                  value = '(cw*ee*complex(0,1))/(2.*sw) - (ee*complex(0,1)*sw)/(6.*cw)',
                  order = {'QED':1})

GC_390 = Coupling(name = 'GC_390',
                  value = '-(cw*ee*complex(0,1))/(2.*sw) + (ee*complex(0,1)*sw)/(2.*cw)',
                  order = {'QED':1})

GC_391 = Coupling(name = 'GC_391',
                  value = '(cw*ee*complex(0,1))/(2.*sw) + (ee*complex(0,1)*sw)/(2.*cw)',
                  order = {'QED':1})

GC_392 = Coupling(name = 'GC_392',
                  value = '(ca2*cb*cw*ee*sa1)/(2.*sw) - (ca1*ca2*cw*ee*sb)/(2.*sw) + (ca2*cb*ee*sa1*sw)/(2.*cw) - (ca1*ca2*ee*sb*sw)/(2.*cw)',
                  order = {'QED':1})

GC_393 = Coupling(name = 'GC_393',
                  value = '-(ca2*cb*cw*ee*sa1)/(2.*sw) + (ca1*ca2*cw*ee*sb)/(2.*sw) - (ca2*cb*ee*sa1*sw)/(2.*cw) + (ca1*ca2*ee*sb*sw)/(2.*cw)',
                  order = {'QED':1})

GC_394 = Coupling(name = 'GC_394',
                  value = '-(ca1*ca2*cb*cw*ee)/(2.*sw) - (ca2*cw*ee*sa1*sb)/(2.*sw) - (ca1*ca2*cb*ee*sw)/(2.*cw) - (ca2*ee*sa1*sb*sw)/(2.*cw)',
                  order = {'QED':1})

GC_395 = Coupling(name = 'GC_395',
                  value = '(ca1*ca2*cb*cw*ee)/(2.*sw) + (ca2*cw*ee*sa1*sb)/(2.*sw) + (ca1*ca2*cb*ee*sw)/(2.*cw) + (ca2*ee*sa1*sb*sw)/(2.*cw)',
                  order = {'QED':1})

GC_396 = Coupling(name = 'GC_396',
                  value = '-(ca1*ca3*cb*cw*ee*sa2)/(2.*sw) + (cb*cw*ee*sa1*sa3)/(2.*sw) - (ca3*cw*ee*sa1*sa2*sb)/(2.*sw) - (ca1*cw*ee*sa3*sb)/(2.*sw) - (ca1*ca3*cb*ee*sa2*sw)/(2.*cw) + (cb*ee*sa1*sa3*sw)/(2.*cw) - (ca3*ee*sa1*sa2*sb*sw)/(2.*cw) - (ca1*ee*sa3*sb*sw)/(2.*cw)',
                  order = {'QED':1})

GC_397 = Coupling(name = 'GC_397',
                  value = '(ca1*ca3*cb*cw*ee*sa2)/(2.*sw) - (cb*cw*ee*sa1*sa3)/(2.*sw) + (ca3*cw*ee*sa1*sa2*sb)/(2.*sw) + (ca1*cw*ee*sa3*sb)/(2.*sw) + (ca1*ca3*cb*ee*sa2*sw)/(2.*cw) - (cb*ee*sa1*sa3*sw)/(2.*cw) + (ca3*ee*sa1*sa2*sb*sw)/(2.*cw) + (ca1*ee*sa3*sb*sw)/(2.*cw)',
                  order = {'QED':1})

GC_398 = Coupling(name = 'GC_398',
                  value = '-(ca3*cb*cw*ee*sa1*sa2)/(2.*sw) - (ca1*cb*cw*ee*sa3)/(2.*sw) + (ca1*ca3*cw*ee*sa2*sb)/(2.*sw) - (cw*ee*sa1*sa3*sb)/(2.*sw) - (ca3*cb*ee*sa1*sa2*sw)/(2.*cw) - (ca1*cb*ee*sa3*sw)/(2.*cw) + (ca1*ca3*ee*sa2*sb*sw)/(2.*cw) - (ee*sa1*sa3*sb*sw)/(2.*cw)',
                  order = {'QED':1})

GC_399 = Coupling(name = 'GC_399',
                  value = '(ca3*cb*cw*ee*sa1*sa2)/(2.*sw) + (ca1*cb*cw*ee*sa3)/(2.*sw) - (ca1*ca3*cw*ee*sa2*sb)/(2.*sw) + (cw*ee*sa1*sa3*sb)/(2.*sw) + (ca3*cb*ee*sa1*sa2*sw)/(2.*cw) + (ca1*cb*ee*sa3*sw)/(2.*cw) - (ca1*ca3*ee*sa2*sb*sw)/(2.*cw) + (ee*sa1*sa3*sb*sw)/(2.*cw)',
                  order = {'QED':1})

GC_400 = Coupling(name = 'GC_400',
                  value = '-(ca1*ca3*cb*cw*ee)/(2.*sw) + (cb*cw*ee*sa1*sa2*sa3)/(2.*sw) - (ca3*cw*ee*sa1*sb)/(2.*sw) - (ca1*cw*ee*sa2*sa3*sb)/(2.*sw) - (ca1*ca3*cb*ee*sw)/(2.*cw) + (cb*ee*sa1*sa2*sa3*sw)/(2.*cw) - (ca3*ee*sa1*sb*sw)/(2.*cw) - (ca1*ee*sa2*sa3*sb*sw)/(2.*cw)',
                  order = {'QED':1})

GC_401 = Coupling(name = 'GC_401',
                  value = '(ca1*ca3*cb*cw*ee)/(2.*sw) - (cb*cw*ee*sa1*sa2*sa3)/(2.*sw) + (ca3*cw*ee*sa1*sb)/(2.*sw) + (ca1*cw*ee*sa2*sa3*sb)/(2.*sw) + (ca1*ca3*cb*ee*sw)/(2.*cw) - (cb*ee*sa1*sa2*sa3*sw)/(2.*cw) + (ca3*ee*sa1*sb*sw)/(2.*cw) + (ca1*ee*sa2*sa3*sb*sw)/(2.*cw)',
                  order = {'QED':1})

GC_402 = Coupling(name = 'GC_402',
                  value = '-(ca3*cb*cw*ee*sa1)/(2.*sw) - (ca1*cb*cw*ee*sa2*sa3)/(2.*sw) + (ca1*ca3*cw*ee*sb)/(2.*sw) - (cw*ee*sa1*sa2*sa3*sb)/(2.*sw) - (ca3*cb*ee*sa1*sw)/(2.*cw) - (ca1*cb*ee*sa2*sa3*sw)/(2.*cw) + (ca1*ca3*ee*sb*sw)/(2.*cw) - (ee*sa1*sa2*sa3*sb*sw)/(2.*cw)',
                  order = {'QED':1})

GC_403 = Coupling(name = 'GC_403',
                  value = '(ca3*cb*cw*ee*sa1)/(2.*sw) + (ca1*cb*cw*ee*sa2*sa3)/(2.*sw) - (ca1*ca3*cw*ee*sb)/(2.*sw) + (cw*ee*sa1*sa2*sa3*sb)/(2.*sw) + (ca3*cb*ee*sa1*sw)/(2.*cw) + (ca1*cb*ee*sa2*sa3*sw)/(2.*cw) - (ca1*ca3*ee*sb*sw)/(2.*cw) + (ee*sa1*sa2*sa3*sb*sw)/(2.*cw)',
                  order = {'QED':1})

GC_404 = Coupling(name = 'GC_404',
                  value = '(cb**2*cw*ee*complex(0,1))/(2.*sw) + (cw*ee*complex(0,1)*sb**2)/(2.*sw) - (cb**2*ee*complex(0,1)*sw)/(2.*cw) - (ee*complex(0,1)*sb**2*sw)/(2.*cw)',
                  order = {'QED':1})

GC_405 = Coupling(name = 'GC_405',
                  value = '-(cb**2*cw*ee*complex(0,1))/(2.*sw) - (cw*ee*complex(0,1)*sb**2)/(2.*sw) + (cb**2*ee*complex(0,1)*sw)/(2.*cw) + (ee*complex(0,1)*sb**2*sw)/(2.*cw)',
                  order = {'QED':1})

GC_406 = Coupling(name = 'GC_406',
                  value = '(cb**2*cw*ee**2*complex(0,1))/(2.*sw) + (cw*ee**2*complex(0,1)*sb**2)/(2.*sw) - (cb**2*ee**2*complex(0,1)*sw)/(2.*cw) - (ee**2*complex(0,1)*sb**2*sw)/(2.*cw)',
                  order = {'QED':2})

GC_407 = Coupling(name = 'GC_407',
                  value = '(cb**2*cw*ee**2*complex(0,1))/sw + (cw*ee**2*complex(0,1)*sb**2)/sw - (cb**2*ee**2*complex(0,1)*sw)/cw - (ee**2*complex(0,1)*sb**2*sw)/cw',
                  order = {'QED':2})

GC_408 = Coupling(name = 'GC_408',
                  value = '-((cb**2*cw*ee**2*complex(0,1))/sw) - (cw*ee**2*complex(0,1)*sb**2)/sw + (cb**2*ee**2*complex(0,1)*sw)/cw + (ee**2*complex(0,1)*sb**2*sw)/cw',
                  order = {'QED':2})

GC_409 = Coupling(name = 'GC_409',
                  value = '-(ca1**2*ca2**2*ee**2*complex(0,1))/2. - (ca2**2*ee**2*complex(0,1)*sa1**2)/2. - (ca1**2*ca2**2*cw**2*ee**2*complex(0,1))/(4.*sw**2) - (ca2**2*cw**2*ee**2*complex(0,1)*sa1**2)/(4.*sw**2) - (ca1**2*ca2**2*ee**2*complex(0,1)*sw**2)/(4.*cw**2) - (ca2**2*ee**2*complex(0,1)*sa1**2*sw**2)/(4.*cw**2)',
                  order = {'QED':2})

GC_410 = Coupling(name = 'GC_410',
                  value = '-(ca1**2*ca2**2*ee**2*complex(0,1)) - ca2**2*ee**2*complex(0,1)*sa1**2 - (ca1**2*ca2**2*cw**2*ee**2*complex(0,1))/(2.*sw**2) - (ca2**2*cw**2*ee**2*complex(0,1)*sa1**2)/(2.*sw**2) - (ca1**2*ca2**2*ee**2*complex(0,1)*sw**2)/(2.*cw**2) - (ca2**2*ee**2*complex(0,1)*sa1**2*sw**2)/(2.*cw**2)',
                  order = {'QED':2})

GC_411 = Coupling(name = 'GC_411',
                  value = 'ca1**2*ca2**2*ee**2*complex(0,1) + ca2**2*ee**2*complex(0,1)*sa1**2 + (ca1**2*ca2**2*cw**2*ee**2*complex(0,1))/(2.*sw**2) + (ca2**2*cw**2*ee**2*complex(0,1)*sa1**2)/(2.*sw**2) + (ca1**2*ca2**2*ee**2*complex(0,1)*sw**2)/(2.*cw**2) + (ca2**2*ee**2*complex(0,1)*sa1**2*sw**2)/(2.*cw**2)',
                  order = {'QED':2})

GC_412 = Coupling(name = 'GC_412',
                  value = '(ca1**2*ca2*ca3*ee**2*complex(0,1)*sa2)/2. + (ca2*ca3*ee**2*complex(0,1)*sa1**2*sa2)/2. + (ca1**2*ca2*ca3*cw**2*ee**2*complex(0,1)*sa2)/(4.*sw**2) + (ca2*ca3*cw**2*ee**2*complex(0,1)*sa1**2*sa2)/(4.*sw**2) + (ca1**2*ca2*ca3*ee**2*complex(0,1)*sa2*sw**2)/(4.*cw**2) + (ca2*ca3*ee**2*complex(0,1)*sa1**2*sa2*sw**2)/(4.*cw**2)',
                  order = {'QED':2})

GC_413 = Coupling(name = 'GC_413',
                  value = '-(ca1**2*ca2*ca3*ee**2*complex(0,1)*sa2) - ca2*ca3*ee**2*complex(0,1)*sa1**2*sa2 - (ca1**2*ca2*ca3*cw**2*ee**2*complex(0,1)*sa2)/(2.*sw**2) - (ca2*ca3*cw**2*ee**2*complex(0,1)*sa1**2*sa2)/(2.*sw**2) - (ca1**2*ca2*ca3*ee**2*complex(0,1)*sa2*sw**2)/(2.*cw**2) - (ca2*ca3*ee**2*complex(0,1)*sa1**2*sa2*sw**2)/(2.*cw**2)',
                  order = {'QED':2})

GC_414 = Coupling(name = 'GC_414',
                  value = 'ca1**2*ca2*ca3*ee**2*complex(0,1)*sa2 + ca2*ca3*ee**2*complex(0,1)*sa1**2*sa2 + (ca1**2*ca2*ca3*cw**2*ee**2*complex(0,1)*sa2)/(2.*sw**2) + (ca2*ca3*cw**2*ee**2*complex(0,1)*sa1**2*sa2)/(2.*sw**2) + (ca1**2*ca2*ca3*ee**2*complex(0,1)*sa2*sw**2)/(2.*cw**2) + (ca2*ca3*ee**2*complex(0,1)*sa1**2*sa2*sw**2)/(2.*cw**2)',
                  order = {'QED':2})

GC_415 = Coupling(name = 'GC_415',
                  value = '(ca1**2*ca2*ee**2*complex(0,1)*sa2*sa3)/2. + (ca2*ee**2*complex(0,1)*sa1**2*sa2*sa3)/2. + (ca1**2*ca2*cw**2*ee**2*complex(0,1)*sa2*sa3)/(4.*sw**2) + (ca2*cw**2*ee**2*complex(0,1)*sa1**2*sa2*sa3)/(4.*sw**2) + (ca1**2*ca2*ee**2*complex(0,1)*sa2*sa3*sw**2)/(4.*cw**2) + (ca2*ee**2*complex(0,1)*sa1**2*sa2*sa3*sw**2)/(4.*cw**2)',
                  order = {'QED':2})

GC_416 = Coupling(name = 'GC_416',
                  value = '-(ca1**2*ca2*ee**2*complex(0,1)*sa2*sa3) - ca2*ee**2*complex(0,1)*sa1**2*sa2*sa3 - (ca1**2*ca2*cw**2*ee**2*complex(0,1)*sa2*sa3)/(2.*sw**2) - (ca2*cw**2*ee**2*complex(0,1)*sa1**2*sa2*sa3)/(2.*sw**2) - (ca1**2*ca2*ee**2*complex(0,1)*sa2*sa3*sw**2)/(2.*cw**2) - (ca2*ee**2*complex(0,1)*sa1**2*sa2*sa3*sw**2)/(2.*cw**2)',
                  order = {'QED':2})

GC_417 = Coupling(name = 'GC_417',
                  value = 'ca1**2*ca2*ee**2*complex(0,1)*sa2*sa3 + ca2*ee**2*complex(0,1)*sa1**2*sa2*sa3 + (ca1**2*ca2*cw**2*ee**2*complex(0,1)*sa2*sa3)/(2.*sw**2) + (ca2*cw**2*ee**2*complex(0,1)*sa1**2*sa2*sa3)/(2.*sw**2) + (ca1**2*ca2*ee**2*complex(0,1)*sa2*sa3*sw**2)/(2.*cw**2) + (ca2*ee**2*complex(0,1)*sa1**2*sa2*sa3*sw**2)/(2.*cw**2)',
                  order = {'QED':2})

GC_418 = Coupling(name = 'GC_418',
                  value = '(ca1**2*ca3*ee**2*complex(0,1)*sa3)/2. + (ca3*ee**2*complex(0,1)*sa1**2*sa3)/2. - (ca1**2*ca3*ee**2*complex(0,1)*sa2**2*sa3)/2. - (ca3*ee**2*complex(0,1)*sa1**2*sa2**2*sa3)/2. + (ca1**2*ca3*cw**2*ee**2*complex(0,1)*sa3)/(4.*sw**2) + (ca3*cw**2*ee**2*complex(0,1)*sa1**2*sa3)/(4.*sw**2) - (ca1**2*ca3*cw**2*ee**2*complex(0,1)*sa2**2*sa3)/(4.*sw**2) - (ca3*cw**2*ee**2*complex(0,1)*sa1**2*sa2**2*sa3)/(4.*sw**2) + (ca1**2*ca3*ee**2*complex(0,1)*sa3*sw**2)/(4.*cw**2) + (ca3*ee**2*complex(0,1)*sa1**2*sa3*sw**2)/(4.*cw**2) - (ca1**2*ca3*ee**2*complex(0,1)*sa2**2*sa3*sw**2)/(4.*cw**2) - (ca3*ee**2*complex(0,1)*sa1**2*sa2**2*sa3*sw**2)/(4.*cw**2)',
                  order = {'QED':2})

GC_419 = Coupling(name = 'GC_419',
                  value = 'ca1**2*ca3*ee**2*complex(0,1)*sa3 + ca3*ee**2*complex(0,1)*sa1**2*sa3 - ca1**2*ca3*ee**2*complex(0,1)*sa2**2*sa3 - ca3*ee**2*complex(0,1)*sa1**2*sa2**2*sa3 + (ca1**2*ca3*cw**2*ee**2*complex(0,1)*sa3)/(2.*sw**2) + (ca3*cw**2*ee**2*complex(0,1)*sa1**2*sa3)/(2.*sw**2) - (ca1**2*ca3*cw**2*ee**2*complex(0,1)*sa2**2*sa3)/(2.*sw**2) - (ca3*cw**2*ee**2*complex(0,1)*sa1**2*sa2**2*sa3)/(2.*sw**2) + (ca1**2*ca3*ee**2*complex(0,1)*sa3*sw**2)/(2.*cw**2) + (ca3*ee**2*complex(0,1)*sa1**2*sa3*sw**2)/(2.*cw**2) - (ca1**2*ca3*ee**2*complex(0,1)*sa2**2*sa3*sw**2)/(2.*cw**2) - (ca3*ee**2*complex(0,1)*sa1**2*sa2**2*sa3*sw**2)/(2.*cw**2)',
                  order = {'QED':2})

GC_420 = Coupling(name = 'GC_420',
                  value = '-(ca1**2*ca3*ee**2*complex(0,1)*sa3) - ca3*ee**2*complex(0,1)*sa1**2*sa3 + ca1**2*ca3*ee**2*complex(0,1)*sa2**2*sa3 + ca3*ee**2*complex(0,1)*sa1**2*sa2**2*sa3 - (ca1**2*ca3*cw**2*ee**2*complex(0,1)*sa3)/(2.*sw**2) - (ca3*cw**2*ee**2*complex(0,1)*sa1**2*sa3)/(2.*sw**2) + (ca1**2*ca3*cw**2*ee**2*complex(0,1)*sa2**2*sa3)/(2.*sw**2) + (ca3*cw**2*ee**2*complex(0,1)*sa1**2*sa2**2*sa3)/(2.*sw**2) - (ca1**2*ca3*ee**2*complex(0,1)*sa3*sw**2)/(2.*cw**2) - (ca3*ee**2*complex(0,1)*sa1**2*sa3*sw**2)/(2.*cw**2) + (ca1**2*ca3*ee**2*complex(0,1)*sa2**2*sa3*sw**2)/(2.*cw**2) + (ca3*ee**2*complex(0,1)*sa1**2*sa2**2*sa3*sw**2)/(2.*cw**2)',
                  order = {'QED':2})

GC_421 = Coupling(name = 'GC_421',
                  value = '-(ca1**2*ca3**2*ee**2*complex(0,1)*sa2**2)/2. - (ca3**2*ee**2*complex(0,1)*sa1**2*sa2**2)/2. - (ca1**2*ee**2*complex(0,1)*sa3**2)/2. - (ee**2*complex(0,1)*sa1**2*sa3**2)/2. - (ca1**2*ca3**2*cw**2*ee**2*complex(0,1)*sa2**2)/(4.*sw**2) - (ca3**2*cw**2*ee**2*complex(0,1)*sa1**2*sa2**2)/(4.*sw**2) - (ca1**2*cw**2*ee**2*complex(0,1)*sa3**2)/(4.*sw**2) - (cw**2*ee**2*complex(0,1)*sa1**2*sa3**2)/(4.*sw**2) - (ca1**2*ca3**2*ee**2*complex(0,1)*sa2**2*sw**2)/(4.*cw**2) - (ca3**2*ee**2*complex(0,1)*sa1**2*sa2**2*sw**2)/(4.*cw**2) - (ca1**2*ee**2*complex(0,1)*sa3**2*sw**2)/(4.*cw**2) - (ee**2*complex(0,1)*sa1**2*sa3**2*sw**2)/(4.*cw**2)',
                  order = {'QED':2})

GC_422 = Coupling(name = 'GC_422',
                  value = '-(ca1**2*ca3**2*ee**2*complex(0,1)*sa2**2) - ca3**2*ee**2*complex(0,1)*sa1**2*sa2**2 - ca1**2*ee**2*complex(0,1)*sa3**2 - ee**2*complex(0,1)*sa1**2*sa3**2 - (ca1**2*ca3**2*cw**2*ee**2*complex(0,1)*sa2**2)/(2.*sw**2) - (ca3**2*cw**2*ee**2*complex(0,1)*sa1**2*sa2**2)/(2.*sw**2) - (ca1**2*cw**2*ee**2*complex(0,1)*sa3**2)/(2.*sw**2) - (cw**2*ee**2*complex(0,1)*sa1**2*sa3**2)/(2.*sw**2) - (ca1**2*ca3**2*ee**2*complex(0,1)*sa2**2*sw**2)/(2.*cw**2) - (ca3**2*ee**2*complex(0,1)*sa1**2*sa2**2*sw**2)/(2.*cw**2) - (ca1**2*ee**2*complex(0,1)*sa3**2*sw**2)/(2.*cw**2) - (ee**2*complex(0,1)*sa1**2*sa3**2*sw**2)/(2.*cw**2)',
                  order = {'QED':2})

GC_423 = Coupling(name = 'GC_423',
                  value = 'ca1**2*ca3**2*ee**2*complex(0,1)*sa2**2 + ca3**2*ee**2*complex(0,1)*sa1**2*sa2**2 + ca1**2*ee**2*complex(0,1)*sa3**2 + ee**2*complex(0,1)*sa1**2*sa3**2 + (ca1**2*ca3**2*cw**2*ee**2*complex(0,1)*sa2**2)/(2.*sw**2) + (ca3**2*cw**2*ee**2*complex(0,1)*sa1**2*sa2**2)/(2.*sw**2) + (ca1**2*cw**2*ee**2*complex(0,1)*sa3**2)/(2.*sw**2) + (cw**2*ee**2*complex(0,1)*sa1**2*sa3**2)/(2.*sw**2) + (ca1**2*ca3**2*ee**2*complex(0,1)*sa2**2*sw**2)/(2.*cw**2) + (ca3**2*ee**2*complex(0,1)*sa1**2*sa2**2*sw**2)/(2.*cw**2) + (ca1**2*ee**2*complex(0,1)*sa3**2*sw**2)/(2.*cw**2) + (ee**2*complex(0,1)*sa1**2*sa3**2*sw**2)/(2.*cw**2)',
                  order = {'QED':2})

GC_424 = Coupling(name = 'GC_424',
                  value = '-(ca1**2*ca3**2*ee**2*complex(0,1))/2. - (ca3**2*ee**2*complex(0,1)*sa1**2)/2. - (ca1**2*ee**2*complex(0,1)*sa2**2*sa3**2)/2. - (ee**2*complex(0,1)*sa1**2*sa2**2*sa3**2)/2. - (ca1**2*ca3**2*cw**2*ee**2*complex(0,1))/(4.*sw**2) - (ca3**2*cw**2*ee**2*complex(0,1)*sa1**2)/(4.*sw**2) - (ca1**2*cw**2*ee**2*complex(0,1)*sa2**2*sa3**2)/(4.*sw**2) - (cw**2*ee**2*complex(0,1)*sa1**2*sa2**2*sa3**2)/(4.*sw**2) - (ca1**2*ca3**2*ee**2*complex(0,1)*sw**2)/(4.*cw**2) - (ca3**2*ee**2*complex(0,1)*sa1**2*sw**2)/(4.*cw**2) - (ca1**2*ee**2*complex(0,1)*sa2**2*sa3**2*sw**2)/(4.*cw**2) - (ee**2*complex(0,1)*sa1**2*sa2**2*sa3**2*sw**2)/(4.*cw**2)',
                  order = {'QED':2})

GC_425 = Coupling(name = 'GC_425',
                  value = '-(ca1**2*ca3**2*ee**2*complex(0,1)) - ca3**2*ee**2*complex(0,1)*sa1**2 - ca1**2*ee**2*complex(0,1)*sa2**2*sa3**2 - ee**2*complex(0,1)*sa1**2*sa2**2*sa3**2 - (ca1**2*ca3**2*cw**2*ee**2*complex(0,1))/(2.*sw**2) - (ca3**2*cw**2*ee**2*complex(0,1)*sa1**2)/(2.*sw**2) - (ca1**2*cw**2*ee**2*complex(0,1)*sa2**2*sa3**2)/(2.*sw**2) - (cw**2*ee**2*complex(0,1)*sa1**2*sa2**2*sa3**2)/(2.*sw**2) - (ca1**2*ca3**2*ee**2*complex(0,1)*sw**2)/(2.*cw**2) - (ca3**2*ee**2*complex(0,1)*sa1**2*sw**2)/(2.*cw**2) - (ca1**2*ee**2*complex(0,1)*sa2**2*sa3**2*sw**2)/(2.*cw**2) - (ee**2*complex(0,1)*sa1**2*sa2**2*sa3**2*sw**2)/(2.*cw**2)',
                  order = {'QED':2})

GC_426 = Coupling(name = 'GC_426',
                  value = 'ca1**2*ca3**2*ee**2*complex(0,1) + ca3**2*ee**2*complex(0,1)*sa1**2 + ca1**2*ee**2*complex(0,1)*sa2**2*sa3**2 + ee**2*complex(0,1)*sa1**2*sa2**2*sa3**2 + (ca1**2*ca3**2*cw**2*ee**2*complex(0,1))/(2.*sw**2) + (ca3**2*cw**2*ee**2*complex(0,1)*sa1**2)/(2.*sw**2) + (ca1**2*cw**2*ee**2*complex(0,1)*sa2**2*sa3**2)/(2.*sw**2) + (cw**2*ee**2*complex(0,1)*sa1**2*sa2**2*sa3**2)/(2.*sw**2) + (ca1**2*ca3**2*ee**2*complex(0,1)*sw**2)/(2.*cw**2) + (ca3**2*ee**2*complex(0,1)*sa1**2*sw**2)/(2.*cw**2) + (ca1**2*ee**2*complex(0,1)*sa2**2*sa3**2*sw**2)/(2.*cw**2) + (ee**2*complex(0,1)*sa1**2*sa2**2*sa3**2*sw**2)/(2.*cw**2)',
                  order = {'QED':2})

GC_427 = Coupling(name = 'GC_427',
                  value = '-(cb**2*ee**2*complex(0,1))/2. - (ee**2*complex(0,1)*sb**2)/2. - (cb**2*cw**2*ee**2*complex(0,1))/(4.*sw**2) - (cw**2*ee**2*complex(0,1)*sb**2)/(4.*sw**2) - (cb**2*ee**2*complex(0,1)*sw**2)/(4.*cw**2) - (ee**2*complex(0,1)*sb**2*sw**2)/(4.*cw**2)',
                  order = {'QED':2})

GC_428 = Coupling(name = 'GC_428',
                  value = '(cb**2*ee**2*complex(0,1))/2. + (ee**2*complex(0,1)*sb**2)/2. - (cb**2*cw**2*ee**2*complex(0,1))/(4.*sw**2) - (cw**2*ee**2*complex(0,1)*sb**2)/(4.*sw**2) - (cb**2*ee**2*complex(0,1)*sw**2)/(4.*cw**2) - (ee**2*complex(0,1)*sb**2*sw**2)/(4.*cw**2)',
                  order = {'QED':2})

GC_429 = Coupling(name = 'GC_429',
                  value = '-(cb**2*ee**2*complex(0,1)) - ee**2*complex(0,1)*sb**2 - (cb**2*cw**2*ee**2*complex(0,1))/(2.*sw**2) - (cw**2*ee**2*complex(0,1)*sb**2)/(2.*sw**2) - (cb**2*ee**2*complex(0,1)*sw**2)/(2.*cw**2) - (ee**2*complex(0,1)*sb**2*sw**2)/(2.*cw**2)',
                  order = {'QED':2})

GC_430 = Coupling(name = 'GC_430',
                  value = 'cb**2*ee**2*complex(0,1) + ee**2*complex(0,1)*sb**2 - (cb**2*cw**2*ee**2*complex(0,1))/(2.*sw**2) - (cw**2*ee**2*complex(0,1)*sb**2)/(2.*sw**2) - (cb**2*ee**2*complex(0,1)*sw**2)/(2.*cw**2) - (ee**2*complex(0,1)*sb**2*sw**2)/(2.*cw**2)',
                  order = {'QED':2})

GC_431 = Coupling(name = 'GC_431',
                  value = '-(cb**2*ee**2*complex(0,1)) - ee**2*complex(0,1)*sb**2 + (cb**2*cw**2*ee**2*complex(0,1))/(2.*sw**2) + (cw**2*ee**2*complex(0,1)*sb**2)/(2.*sw**2) + (cb**2*ee**2*complex(0,1)*sw**2)/(2.*cw**2) + (ee**2*complex(0,1)*sb**2*sw**2)/(2.*cw**2)',
                  order = {'QED':2})

GC_432 = Coupling(name = 'GC_432',
                  value = 'cb**2*ee**2*complex(0,1) + ee**2*complex(0,1)*sb**2 + (cb**2*cw**2*ee**2*complex(0,1))/(2.*sw**2) + (cw**2*ee**2*complex(0,1)*sb**2)/(2.*sw**2) + (cb**2*ee**2*complex(0,1)*sw**2)/(2.*cw**2) + (ee**2*complex(0,1)*sb**2*sw**2)/(2.*cw**2)',
                  order = {'QED':2})

GC_433 = Coupling(name = 'GC_433',
                  value = '(ee**2*complex(0,1)*sb*vev1)/(2.*cw) - (cb*ee**2*complex(0,1)*vev2)/(2.*cw)',
                  order = {'QED':1})

GC_434 = Coupling(name = 'GC_434',
                  value = '-(ee**2*complex(0,1)*sb*vev1)/(2.*cw) + (cb*ee**2*complex(0,1)*vev2)/(2.*cw)',
                  order = {'QED':1})

GC_435 = Coupling(name = 'GC_435',
                  value = '-(cb*ee**2*complex(0,1)*vev1)/(2.*cw) - (ee**2*complex(0,1)*sb*vev2)/(2.*cw)',
                  order = {'QED':1})

GC_436 = Coupling(name = 'GC_436',
                  value = '(cb*ee**2*complex(0,1)*vev1)/(2.*cw) + (ee**2*complex(0,1)*sb*vev2)/(2.*cw)',
                  order = {'QED':1})

GC_437 = Coupling(name = 'GC_437',
                  value = '(cb**2*ee**2*sb*vev1)/(4.*cw**2) + (ee**2*sb**3*vev1)/(4.*cw**2) - (cb**3*ee**2*vev2)/(4.*cw**2) - (cb*ee**2*sb**2*vev2)/(4.*cw**2)',
                  order = {'QED':1})

GC_438 = Coupling(name = 'GC_438',
                  value = '-(cb**2*ee**2*sb*vev1)/(4.*cw**2) - (ee**2*sb**3*vev1)/(4.*cw**2) + (cb**3*ee**2*vev2)/(4.*cw**2) + (cb*ee**2*sb**2*vev2)/(4.*cw**2)',
                  order = {'QED':1})

GC_439 = Coupling(name = 'GC_439',
                  value = '-(cb**2*l4*sb*vev1)/2. + (cb**2*l5*sb*vev1)/2. - (l4*sb**3*vev1)/2. + (l5*sb**3*vev1)/2. + (cb**3*l4*vev2)/2. - (cb**3*l5*vev2)/2. + (cb*l4*sb**2*vev2)/2. - (cb*l5*sb**2*vev2)/2.',
                  order = {'QED':1})

GC_440 = Coupling(name = 'GC_440',
                  value = '(cb**2*l4*sb*vev1)/2. - (cb**2*l5*sb*vev1)/2. + (l4*sb**3*vev1)/2. - (l5*sb**3*vev1)/2. - (cb**3*l4*vev2)/2. + (cb**3*l5*vev2)/2. - (cb*l4*sb**2*vev2)/2. + (cb*l5*sb**2*vev2)/2.',
                  order = {'QED':1})

GC_441 = Coupling(name = 'GC_441',
                  value = '-(cb**3*ee**2*vev1)/(4.*cw**2) - (cb*ee**2*sb**2*vev1)/(4.*cw**2) - (cb**2*ee**2*sb*vev2)/(4.*cw**2) - (ee**2*sb**3*vev2)/(4.*cw**2)',
                  order = {'QED':1})

GC_442 = Coupling(name = 'GC_442',
                  value = '(cb**3*ee**2*vev1)/(4.*cw**2) + (cb*ee**2*sb**2*vev1)/(4.*cw**2) + (cb**2*ee**2*sb*vev2)/(4.*cw**2) + (ee**2*sb**3*vev2)/(4.*cw**2)',
                  order = {'QED':1})

GC_443 = Coupling(name = 'GC_443',
                  value = '(cb**3*l4*vev1)/2. - (cb**3*l5*vev1)/2. + (cb*l4*sb**2*vev1)/2. - (cb*l5*sb**2*vev1)/2. + (cb**2*l4*sb*vev2)/2. - (cb**2*l5*sb*vev2)/2. + (l4*sb**3*vev2)/2. - (l5*sb**3*vev2)/2.',
                  order = {'QED':1})

GC_444 = Coupling(name = 'GC_444',
                  value = '-(cb**3*l4*vev1)/2. + (cb**3*l5*vev1)/2. - (cb*l4*sb**2*vev1)/2. + (cb*l5*sb**2*vev1)/2. - (cb**2*l4*sb*vev2)/2. + (cb**2*l5*sb*vev2)/2. - (l4*sb**3*vev2)/2. + (l5*sb**3*vev2)/2.',
                  order = {'QED':1})

GC_445 = Coupling(name = 'GC_445',
                  value = '(ee**2*sb*vev1)/(2.*sw**2) - (cb*ee**2*vev2)/(2.*sw**2)',
                  order = {'QED':1})

GC_446 = Coupling(name = 'GC_446',
                  value = '(ee**2*sb*vev1)/(4.*sw**2) - (cb*ee**2*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_447 = Coupling(name = 'GC_447',
                  value = '-(ee**2*sb*vev1)/(4.*sw**2) + (cb*ee**2*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_448 = Coupling(name = 'GC_448',
                  value = '-(ee**2*sb*vev1)/(2.*sw**2) + (cb*ee**2*vev2)/(2.*sw**2)',
                  order = {'QED':1})

GC_449 = Coupling(name = 'GC_449',
                  value = '-(ee**2*complex(0,1)*sb*vev1)/(4.*cw) + (cw*ee**2*complex(0,1)*sb*vev1)/(4.*sw**2) + (cb*ee**2*complex(0,1)*vev2)/(4.*cw) - (cb*cw*ee**2*complex(0,1)*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_450 = Coupling(name = 'GC_450',
                  value = '-(ee**2*complex(0,1)*sb*vev1)/(4.*cw) - (cw*ee**2*complex(0,1)*sb*vev1)/(4.*sw**2) + (cb*ee**2*complex(0,1)*vev2)/(4.*cw) + (cb*cw*ee**2*complex(0,1)*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_451 = Coupling(name = 'GC_451',
                  value = '(ee**2*complex(0,1)*sb*vev1)/(2.*cw) + (cw*ee**2*complex(0,1)*sb*vev1)/(2.*sw**2) - (cb*ee**2*complex(0,1)*vev2)/(2.*cw) - (cb*cw*ee**2*complex(0,1)*vev2)/(2.*sw**2)',
                  order = {'QED':1})

GC_452 = Coupling(name = 'GC_452',
                  value = '(ee**2*complex(0,1)*sb*vev1)/(2.*cw) - (cw*ee**2*complex(0,1)*sb*vev1)/(2.*sw**2) - (cb*ee**2*complex(0,1)*vev2)/(2.*cw) + (cb*cw*ee**2*complex(0,1)*vev2)/(2.*sw**2)',
                  order = {'QED':1})

GC_453 = Coupling(name = 'GC_453',
                  value = '-(ca1*ca2*ee**2*complex(0,1)*vev1)/(4.*sw**2) - (ca2*ee**2*complex(0,1)*sa1*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_454 = Coupling(name = 'GC_454',
                  value = '-(ca1*ca2*ee**2*complex(0,1)*vev1)/(2.*sw**2) - (ca2*ee**2*complex(0,1)*sa1*vev2)/(2.*sw**2)',
                  order = {'QED':1})

GC_455 = Coupling(name = 'GC_455',
                  value = '(ca1*ca2*ee**2*complex(0,1)*vev1)/(2.*sw**2) + (ca2*ee**2*complex(0,1)*sa1*vev2)/(2.*sw**2)',
                  order = {'QED':1})

GC_456 = Coupling(name = 'GC_456',
                  value = '(ca1*ca3*ee**2*complex(0,1)*sa2*vev1)/(4.*sw**2) - (ee**2*complex(0,1)*sa1*sa3*vev1)/(4.*sw**2) + (ca3*ee**2*complex(0,1)*sa1*sa2*vev2)/(4.*sw**2) + (ca1*ee**2*complex(0,1)*sa3*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_457 = Coupling(name = 'GC_457',
                  value = '-(ca1*ca3*ee**2*complex(0,1)*sa2*vev1)/(2.*sw**2) + (ee**2*complex(0,1)*sa1*sa3*vev1)/(2.*sw**2) - (ca3*ee**2*complex(0,1)*sa1*sa2*vev2)/(2.*sw**2) - (ca1*ee**2*complex(0,1)*sa3*vev2)/(2.*sw**2)',
                  order = {'QED':1})

GC_458 = Coupling(name = 'GC_458',
                  value = '(ca1*ca3*ee**2*complex(0,1)*sa2*vev1)/(2.*sw**2) - (ee**2*complex(0,1)*sa1*sa3*vev1)/(2.*sw**2) + (ca3*ee**2*complex(0,1)*sa1*sa2*vev2)/(2.*sw**2) + (ca1*ee**2*complex(0,1)*sa3*vev2)/(2.*sw**2)',
                  order = {'QED':1})

GC_459 = Coupling(name = 'GC_459',
                  value = '(ca3*ee**2*complex(0,1)*sa1*vev1)/(4.*sw**2) + (ca1*ee**2*complex(0,1)*sa2*sa3*vev1)/(4.*sw**2) - (ca1*ca3*ee**2*complex(0,1)*vev2)/(4.*sw**2) + (ee**2*complex(0,1)*sa1*sa2*sa3*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_460 = Coupling(name = 'GC_460',
                  value = '-(ca3*ee**2*complex(0,1)*sa1*vev1)/(2.*sw**2) - (ca1*ee**2*complex(0,1)*sa2*sa3*vev1)/(2.*sw**2) + (ca1*ca3*ee**2*complex(0,1)*vev2)/(2.*sw**2) - (ee**2*complex(0,1)*sa1*sa2*sa3*vev2)/(2.*sw**2)',
                  order = {'QED':1})

GC_461 = Coupling(name = 'GC_461',
                  value = '(ca3*ee**2*complex(0,1)*sa1*vev1)/(2.*sw**2) + (ca1*ee**2*complex(0,1)*sa2*sa3*vev1)/(2.*sw**2) - (ca1*ca3*ee**2*complex(0,1)*vev2)/(2.*sw**2) + (ee**2*complex(0,1)*sa1*sa2*sa3*vev2)/(2.*sw**2)',
                  order = {'QED':1})

GC_462 = Coupling(name = 'GC_462',
                  value = '-(cb*ee**2*vev1)/(2.*sw**2) - (ee**2*sb*vev2)/(2.*sw**2)',
                  order = {'QED':1})

GC_463 = Coupling(name = 'GC_463',
                  value = '-(cb*ee**2*vev1)/(4.*sw**2) - (ee**2*sb*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_464 = Coupling(name = 'GC_464',
                  value = '(cb*ee**2*vev1)/(4.*sw**2) + (ee**2*sb*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_465 = Coupling(name = 'GC_465',
                  value = '(cb*ee**2*vev1)/(2.*sw**2) + (ee**2*sb*vev2)/(2.*sw**2)',
                  order = {'QED':1})

GC_466 = Coupling(name = 'GC_466',
                  value = '(cb*ee**2*complex(0,1)*vev1)/(4.*cw) - (cb*cw*ee**2*complex(0,1)*vev1)/(4.*sw**2) + (ee**2*complex(0,1)*sb*vev2)/(4.*cw) - (cw*ee**2*complex(0,1)*sb*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_467 = Coupling(name = 'GC_467',
                  value = '(cb*ee**2*complex(0,1)*vev1)/(4.*cw) + (cb*cw*ee**2*complex(0,1)*vev1)/(4.*sw**2) + (ee**2*complex(0,1)*sb*vev2)/(4.*cw) + (cw*ee**2*complex(0,1)*sb*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_468 = Coupling(name = 'GC_468',
                  value = '-(cb*ee**2*complex(0,1)*vev1)/(2.*cw) - (cb*cw*ee**2*complex(0,1)*vev1)/(2.*sw**2) - (ee**2*complex(0,1)*sb*vev2)/(2.*cw) - (cw*ee**2*complex(0,1)*sb*vev2)/(2.*sw**2)',
                  order = {'QED':1})

GC_469 = Coupling(name = 'GC_469',
                  value = '-(cb*ee**2*complex(0,1)*vev1)/(2.*cw) + (cb*cw*ee**2*complex(0,1)*vev1)/(2.*sw**2) - (ee**2*complex(0,1)*sb*vev2)/(2.*cw) + (cw*ee**2*complex(0,1)*sb*vev2)/(2.*sw**2)',
                  order = {'QED':1})

GC_470 = Coupling(name = 'GC_470',
                  value = '-(cb**2*ee**2*sb*vev1)/(4.*cw**2) - (ee**2*sb**3*vev1)/(4.*cw**2) + (cb**2*ee**2*sb*vev1)/(4.*sw**2) + (ee**2*sb**3*vev1)/(4.*sw**2) + (cb**3*ee**2*vev2)/(4.*cw**2) + (cb*ee**2*sb**2*vev2)/(4.*cw**2) - (cb**3*ee**2*vev2)/(4.*sw**2) - (cb*ee**2*sb**2*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_471 = Coupling(name = 'GC_471',
                  value = '-(cb**2*l4*sb*vev1)/2. + (cb**2*l5*sb*vev1)/2. - (l4*sb**3*vev1)/2. + (l5*sb**3*vev1)/2. + (cb**2*ee**2*sb*vev1)/(4.*sw**2) + (ee**2*sb**3*vev1)/(4.*sw**2) + (cb**3*l4*vev2)/2. - (cb**3*l5*vev2)/2. + (cb*l4*sb**2*vev2)/2. - (cb*l5*sb**2*vev2)/2. - (cb**3*ee**2*vev2)/(4.*sw**2) - (cb*ee**2*sb**2*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_472 = Coupling(name = 'GC_472',
                  value = '(cb**2*l4*sb*vev1)/2. - (cb**2*l5*sb*vev1)/2. + (l4*sb**3*vev1)/2. - (l5*sb**3*vev1)/2. + (cb**2*ee**2*sb*vev1)/(4.*sw**2) + (ee**2*sb**3*vev1)/(4.*sw**2) - (cb**3*l4*vev2)/2. + (cb**3*l5*vev2)/2. - (cb*l4*sb**2*vev2)/2. + (cb*l5*sb**2*vev2)/2. - (cb**3*ee**2*vev2)/(4.*sw**2) - (cb*ee**2*sb**2*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_473 = Coupling(name = 'GC_473',
                  value = '(cb**2*ee**2*sb*vev1)/(4.*cw**2) + (ee**2*sb**3*vev1)/(4.*cw**2) - (cb**2*ee**2*sb*vev1)/(4.*sw**2) - (ee**2*sb**3*vev1)/(4.*sw**2) - (cb**3*ee**2*vev2)/(4.*cw**2) - (cb*ee**2*sb**2*vev2)/(4.*cw**2) + (cb**3*ee**2*vev2)/(4.*sw**2) + (cb*ee**2*sb**2*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_474 = Coupling(name = 'GC_474',
                  value = '-(cb**2*l4*sb*vev1)/2. + (cb**2*l5*sb*vev1)/2. - (l4*sb**3*vev1)/2. + (l5*sb**3*vev1)/2. - (cb**2*ee**2*sb*vev1)/(4.*sw**2) - (ee**2*sb**3*vev1)/(4.*sw**2) + (cb**3*l4*vev2)/2. - (cb**3*l5*vev2)/2. + (cb*l4*sb**2*vev2)/2. - (cb*l5*sb**2*vev2)/2. + (cb**3*ee**2*vev2)/(4.*sw**2) + (cb*ee**2*sb**2*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_475 = Coupling(name = 'GC_475',
                  value = '(cb**2*l4*sb*vev1)/2. - (cb**2*l5*sb*vev1)/2. + (l4*sb**3*vev1)/2. - (l5*sb**3*vev1)/2. - (cb**2*ee**2*sb*vev1)/(4.*sw**2) - (ee**2*sb**3*vev1)/(4.*sw**2) - (cb**3*l4*vev2)/2. + (cb**3*l5*vev2)/2. - (cb*l4*sb**2*vev2)/2. + (cb*l5*sb**2*vev2)/2. + (cb**3*ee**2*vev2)/(4.*sw**2) + (cb*ee**2*sb**2*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_476 = Coupling(name = 'GC_476',
                  value = '(cb**3*ee**2*vev1)/(4.*cw**2) + (cb*ee**2*sb**2*vev1)/(4.*cw**2) - (cb**3*ee**2*vev1)/(4.*sw**2) - (cb*ee**2*sb**2*vev1)/(4.*sw**2) + (cb**2*ee**2*sb*vev2)/(4.*cw**2) + (ee**2*sb**3*vev2)/(4.*cw**2) - (cb**2*ee**2*sb*vev2)/(4.*sw**2) - (ee**2*sb**3*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_477 = Coupling(name = 'GC_477',
                  value = '(cb**3*l4*vev1)/2. - (cb**3*l5*vev1)/2. + (cb*l4*sb**2*vev1)/2. - (cb*l5*sb**2*vev1)/2. - (cb**3*ee**2*vev1)/(4.*sw**2) - (cb*ee**2*sb**2*vev1)/(4.*sw**2) + (cb**2*l4*sb*vev2)/2. - (cb**2*l5*sb*vev2)/2. + (l4*sb**3*vev2)/2. - (l5*sb**3*vev2)/2. - (cb**2*ee**2*sb*vev2)/(4.*sw**2) - (ee**2*sb**3*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_478 = Coupling(name = 'GC_478',
                  value = '-(cb**3*l4*vev1)/2. + (cb**3*l5*vev1)/2. - (cb*l4*sb**2*vev1)/2. + (cb*l5*sb**2*vev1)/2. - (cb**3*ee**2*vev1)/(4.*sw**2) - (cb*ee**2*sb**2*vev1)/(4.*sw**2) - (cb**2*l4*sb*vev2)/2. + (cb**2*l5*sb*vev2)/2. - (l4*sb**3*vev2)/2. + (l5*sb**3*vev2)/2. - (cb**2*ee**2*sb*vev2)/(4.*sw**2) - (ee**2*sb**3*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_479 = Coupling(name = 'GC_479',
                  value = '-(cb**3*ee**2*vev1)/(4.*cw**2) - (cb*ee**2*sb**2*vev1)/(4.*cw**2) + (cb**3*ee**2*vev1)/(4.*sw**2) + (cb*ee**2*sb**2*vev1)/(4.*sw**2) - (cb**2*ee**2*sb*vev2)/(4.*cw**2) - (ee**2*sb**3*vev2)/(4.*cw**2) + (cb**2*ee**2*sb*vev2)/(4.*sw**2) + (ee**2*sb**3*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_480 = Coupling(name = 'GC_480',
                  value = '(cb**3*l4*vev1)/2. - (cb**3*l5*vev1)/2. + (cb*l4*sb**2*vev1)/2. - (cb*l5*sb**2*vev1)/2. + (cb**3*ee**2*vev1)/(4.*sw**2) + (cb*ee**2*sb**2*vev1)/(4.*sw**2) + (cb**2*l4*sb*vev2)/2. - (cb**2*l5*sb*vev2)/2. + (l4*sb**3*vev2)/2. - (l5*sb**3*vev2)/2. + (cb**2*ee**2*sb*vev2)/(4.*sw**2) + (ee**2*sb**3*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_481 = Coupling(name = 'GC_481',
                  value = '-(cb**3*l4*vev1)/2. + (cb**3*l5*vev1)/2. - (cb*l4*sb**2*vev1)/2. + (cb*l5*sb**2*vev1)/2. + (cb**3*ee**2*vev1)/(4.*sw**2) + (cb*ee**2*sb**2*vev1)/(4.*sw**2) - (cb**2*l4*sb*vev2)/2. + (cb**2*l5*sb*vev2)/2. - (l4*sb**3*vev2)/2. + (l5*sb**3*vev2)/2. + (cb**2*ee**2*sb*vev2)/(4.*sw**2) + (ee**2*sb**3*vev2)/(4.*sw**2)',
                  order = {'QED':1})

GC_482 = Coupling(name = 'GC_482',
                  value = '(ee**2*complex(0,1)*sb*vev1)/(2.*sw) - (cb*ee**2*complex(0,1)*vev2)/(2.*sw)',
                  order = {'QED':1})

GC_483 = Coupling(name = 'GC_483',
                  value = '-(ee**2*complex(0,1)*sb*vev1)/(2.*sw) + (cb*ee**2*complex(0,1)*vev2)/(2.*sw)',
                  order = {'QED':1})

GC_484 = Coupling(name = 'GC_484',
                  value = '(ee**2*complex(0,1)*sb*vev1)/sw - (cb*ee**2*complex(0,1)*vev2)/sw',
                  order = {'QED':1})

GC_485 = Coupling(name = 'GC_485',
                  value = '-(cb*ee**2*complex(0,1)*vev1)/(2.*sw) - (ee**2*complex(0,1)*sb*vev2)/(2.*sw)',
                  order = {'QED':1})

GC_486 = Coupling(name = 'GC_486',
                  value = '(cb*ee**2*complex(0,1)*vev1)/(2.*sw) + (ee**2*complex(0,1)*sb*vev2)/(2.*sw)',
                  order = {'QED':1})

GC_487 = Coupling(name = 'GC_487',
                  value = '-((cb*ee**2*complex(0,1)*vev1)/sw) - (ee**2*complex(0,1)*sb*vev2)/sw',
                  order = {'QED':1})

GC_488 = Coupling(name = 'GC_488',
                  value = '-(ca1*ca2*ee**2*complex(0,1)*vev1)/2. - (ca1*ca2*cw**2*ee**2*complex(0,1)*vev1)/(4.*sw**2) - (ca1*ca2*ee**2*complex(0,1)*sw**2*vev1)/(4.*cw**2) - (ca2*ee**2*complex(0,1)*sa1*vev2)/2. - (ca2*cw**2*ee**2*complex(0,1)*sa1*vev2)/(4.*sw**2) - (ca2*ee**2*complex(0,1)*sa1*sw**2*vev2)/(4.*cw**2)',
                  order = {'QED':1})

GC_489 = Coupling(name = 'GC_489',
                  value = '-(ca1*ca2*ee**2*complex(0,1)*vev1) - (ca1*ca2*cw**2*ee**2*complex(0,1)*vev1)/(2.*sw**2) - (ca1*ca2*ee**2*complex(0,1)*sw**2*vev1)/(2.*cw**2) - ca2*ee**2*complex(0,1)*sa1*vev2 - (ca2*cw**2*ee**2*complex(0,1)*sa1*vev2)/(2.*sw**2) - (ca2*ee**2*complex(0,1)*sa1*sw**2*vev2)/(2.*cw**2)',
                  order = {'QED':1})

GC_490 = Coupling(name = 'GC_490',
                  value = 'ca1*ca2*ee**2*complex(0,1)*vev1 + (ca1*ca2*cw**2*ee**2*complex(0,1)*vev1)/(2.*sw**2) + (ca1*ca2*ee**2*complex(0,1)*sw**2*vev1)/(2.*cw**2) + ca2*ee**2*complex(0,1)*sa1*vev2 + (ca2*cw**2*ee**2*complex(0,1)*sa1*vev2)/(2.*sw**2) + (ca2*ee**2*complex(0,1)*sa1*sw**2*vev2)/(2.*cw**2)',
                  order = {'QED':1})

GC_491 = Coupling(name = 'GC_491',
                  value = '(ca1*ca3*ee**2*complex(0,1)*sa2*vev1)/2. - (ee**2*complex(0,1)*sa1*sa3*vev1)/2. + (ca1*ca3*cw**2*ee**2*complex(0,1)*sa2*vev1)/(4.*sw**2) - (cw**2*ee**2*complex(0,1)*sa1*sa3*vev1)/(4.*sw**2) + (ca1*ca3*ee**2*complex(0,1)*sa2*sw**2*vev1)/(4.*cw**2) - (ee**2*complex(0,1)*sa1*sa3*sw**2*vev1)/(4.*cw**2) + (ca3*ee**2*complex(0,1)*sa1*sa2*vev2)/2. + (ca1*ee**2*complex(0,1)*sa3*vev2)/2. + (ca3*cw**2*ee**2*complex(0,1)*sa1*sa2*vev2)/(4.*sw**2) + (ca1*cw**2*ee**2*complex(0,1)*sa3*vev2)/(4.*sw**2) + (ca3*ee**2*complex(0,1)*sa1*sa2*sw**2*vev2)/(4.*cw**2) + (ca1*ee**2*complex(0,1)*sa3*sw**2*vev2)/(4.*cw**2)',
                  order = {'QED':1})

GC_492 = Coupling(name = 'GC_492',
                  value = '-(ca1*ca3*ee**2*complex(0,1)*sa2*vev1) + ee**2*complex(0,1)*sa1*sa3*vev1 - (ca1*ca3*cw**2*ee**2*complex(0,1)*sa2*vev1)/(2.*sw**2) + (cw**2*ee**2*complex(0,1)*sa1*sa3*vev1)/(2.*sw**2) - (ca1*ca3*ee**2*complex(0,1)*sa2*sw**2*vev1)/(2.*cw**2) + (ee**2*complex(0,1)*sa1*sa3*sw**2*vev1)/(2.*cw**2) - ca3*ee**2*complex(0,1)*sa1*sa2*vev2 - ca1*ee**2*complex(0,1)*sa3*vev2 - (ca3*cw**2*ee**2*complex(0,1)*sa1*sa2*vev2)/(2.*sw**2) - (ca1*cw**2*ee**2*complex(0,1)*sa3*vev2)/(2.*sw**2) - (ca3*ee**2*complex(0,1)*sa1*sa2*sw**2*vev2)/(2.*cw**2) - (ca1*ee**2*complex(0,1)*sa3*sw**2*vev2)/(2.*cw**2)',
                  order = {'QED':1})

GC_493 = Coupling(name = 'GC_493',
                  value = 'ca1*ca3*ee**2*complex(0,1)*sa2*vev1 - ee**2*complex(0,1)*sa1*sa3*vev1 + (ca1*ca3*cw**2*ee**2*complex(0,1)*sa2*vev1)/(2.*sw**2) - (cw**2*ee**2*complex(0,1)*sa1*sa3*vev1)/(2.*sw**2) + (ca1*ca3*ee**2*complex(0,1)*sa2*sw**2*vev1)/(2.*cw**2) - (ee**2*complex(0,1)*sa1*sa3*sw**2*vev1)/(2.*cw**2) + ca3*ee**2*complex(0,1)*sa1*sa2*vev2 + ca1*ee**2*complex(0,1)*sa3*vev2 + (ca3*cw**2*ee**2*complex(0,1)*sa1*sa2*vev2)/(2.*sw**2) + (ca1*cw**2*ee**2*complex(0,1)*sa3*vev2)/(2.*sw**2) + (ca3*ee**2*complex(0,1)*sa1*sa2*sw**2*vev2)/(2.*cw**2) + (ca1*ee**2*complex(0,1)*sa3*sw**2*vev2)/(2.*cw**2)',
                  order = {'QED':1})

GC_494 = Coupling(name = 'GC_494',
                  value = '(ca3*ee**2*complex(0,1)*sa1*vev1)/2. + (ca1*ee**2*complex(0,1)*sa2*sa3*vev1)/2. + (ca3*cw**2*ee**2*complex(0,1)*sa1*vev1)/(4.*sw**2) + (ca1*cw**2*ee**2*complex(0,1)*sa2*sa3*vev1)/(4.*sw**2) + (ca3*ee**2*complex(0,1)*sa1*sw**2*vev1)/(4.*cw**2) + (ca1*ee**2*complex(0,1)*sa2*sa3*sw**2*vev1)/(4.*cw**2) - (ca1*ca3*ee**2*complex(0,1)*vev2)/2. + (ee**2*complex(0,1)*sa1*sa2*sa3*vev2)/2. - (ca1*ca3*cw**2*ee**2*complex(0,1)*vev2)/(4.*sw**2) + (cw**2*ee**2*complex(0,1)*sa1*sa2*sa3*vev2)/(4.*sw**2) - (ca1*ca3*ee**2*complex(0,1)*sw**2*vev2)/(4.*cw**2) + (ee**2*complex(0,1)*sa1*sa2*sa3*sw**2*vev2)/(4.*cw**2)',
                  order = {'QED':1})

GC_495 = Coupling(name = 'GC_495',
                  value = '-(ca3*ee**2*complex(0,1)*sa1*vev1) - ca1*ee**2*complex(0,1)*sa2*sa3*vev1 - (ca3*cw**2*ee**2*complex(0,1)*sa1*vev1)/(2.*sw**2) - (ca1*cw**2*ee**2*complex(0,1)*sa2*sa3*vev1)/(2.*sw**2) - (ca3*ee**2*complex(0,1)*sa1*sw**2*vev1)/(2.*cw**2) - (ca1*ee**2*complex(0,1)*sa2*sa3*sw**2*vev1)/(2.*cw**2) + ca1*ca3*ee**2*complex(0,1)*vev2 - ee**2*complex(0,1)*sa1*sa2*sa3*vev2 + (ca1*ca3*cw**2*ee**2*complex(0,1)*vev2)/(2.*sw**2) - (cw**2*ee**2*complex(0,1)*sa1*sa2*sa3*vev2)/(2.*sw**2) + (ca1*ca3*ee**2*complex(0,1)*sw**2*vev2)/(2.*cw**2) - (ee**2*complex(0,1)*sa1*sa2*sa3*sw**2*vev2)/(2.*cw**2)',
                  order = {'QED':1})

GC_496 = Coupling(name = 'GC_496',
                  value = 'ca3*ee**2*complex(0,1)*sa1*vev1 + ca1*ee**2*complex(0,1)*sa2*sa3*vev1 + (ca3*cw**2*ee**2*complex(0,1)*sa1*vev1)/(2.*sw**2) + (ca1*cw**2*ee**2*complex(0,1)*sa2*sa3*vev1)/(2.*sw**2) + (ca3*ee**2*complex(0,1)*sa1*sw**2*vev1)/(2.*cw**2) + (ca1*ee**2*complex(0,1)*sa2*sa3*sw**2*vev1)/(2.*cw**2) - ca1*ca3*ee**2*complex(0,1)*vev2 + ee**2*complex(0,1)*sa1*sa2*sa3*vev2 - (ca1*ca3*cw**2*ee**2*complex(0,1)*vev2)/(2.*sw**2) + (cw**2*ee**2*complex(0,1)*sa1*sa2*sa3*vev2)/(2.*sw**2) - (ca1*ca3*ee**2*complex(0,1)*sw**2*vev2)/(2.*cw**2) + (ee**2*complex(0,1)*sa1*sa2*sa3*sw**2*vev2)/(2.*cw**2)',
                  order = {'QED':1})

GC_497 = Coupling(name = 'GC_497',
                  value = '-3*ca1**3*ca2**3*complex(0,1)*l1*vev1 - 3*ca1*ca2**3*complex(0,1)*l3*sa1**2*vev1 - 3*ca1*ca2**3*complex(0,1)*l4*sa1**2*vev1 - 3*ca1*ca2**3*complex(0,1)*l5*sa1**2*vev1 - 3*ca1*ca2*complex(0,1)*l7*sa2**2*vev1 - 3*ca1**2*ca2**3*complex(0,1)*l3*sa1*vev2 - 3*ca1**2*ca2**3*complex(0,1)*l4*sa1*vev2 - 3*ca1**2*ca2**3*complex(0,1)*l5*sa1*vev2 - 3*ca2**3*complex(0,1)*l2*sa1**3*vev2 - 3*ca2*complex(0,1)*l8*sa1*sa2**2*vev2 - 3*ca1**2*ca2**2*complex(0,1)*l7*sa2*vevs - 3*ca2**2*complex(0,1)*l8*sa1**2*sa2*vevs - 3*complex(0,1)*l6*sa2**3*vevs',
                  order = {'QED':1})

GC_498 = Coupling(name = 'GC_498',
                  value = '3*ca1**3*ca2**2*ca3*complex(0,1)*l1*sa2*vev1 - 2*ca1*ca2**2*ca3*complex(0,1)*l7*sa2*vev1 + 3*ca1*ca2**2*ca3*complex(0,1)*l3*sa1**2*sa2*vev1 + 3*ca1*ca2**2*ca3*complex(0,1)*l4*sa1**2*sa2*vev1 + 3*ca1*ca2**2*ca3*complex(0,1)*l5*sa1**2*sa2*vev1 + ca1*ca3*complex(0,1)*l7*sa2**3*vev1 - 3*ca1**2*ca2**2*complex(0,1)*l1*sa1*sa3*vev1 + 2*ca1**2*ca2**2*complex(0,1)*l3*sa1*sa3*vev1 + 2*ca1**2*ca2**2*complex(0,1)*l4*sa1*sa3*vev1 + 2*ca1**2*ca2**2*complex(0,1)*l5*sa1*sa3*vev1 - ca2**2*complex(0,1)*l3*sa1**3*sa3*vev1 - ca2**2*complex(0,1)*l4*sa1**3*sa3*vev1 - ca2**2*complex(0,1)*l5*sa1**3*sa3*vev1 - complex(0,1)*l7*sa1*sa2**2*sa3*vev1 + 3*ca1**2*ca2**2*ca3*complex(0,1)*l3*sa1*sa2*vev2 + 3*ca1**2*ca2**2*ca3*complex(0,1)*l4*sa1*sa2*vev2 + 3*ca1**2*ca2**2*ca3*complex(0,1)*l5*sa1*sa2*vev2 - 2*ca2**2*ca3*complex(0,1)*l8*sa1*sa2*vev2 + 3*ca2**2*ca3*complex(0,1)*l2*sa1**3*sa2*vev2 + ca3*complex(0,1)*l8*sa1*sa2**3*vev2 + ca1**3*ca2**2*complex(0,1)*l3*sa3*vev2 + ca1**3*ca2**2*complex(0,1)*l4*sa3*vev2 + ca1**3*ca2**2*complex(0,1)*l5*sa3*vev2 + 3*ca1*ca2**2*complex(0,1)*l2*sa1**2*sa3*vev2 - 2*ca1*ca2**2*complex(0,1)*l3*sa1**2*sa3*vev2 - 2*ca1*ca2**2*complex(0,1)*l4*sa1**2*sa3*vev2 - 2*ca1*ca2**2*complex(0,1)*l5*sa1**2*sa3*vev2 + ca1*complex(0,1)*l8*sa2**2*sa3*vev2 - ca1**2*ca2**3*ca3*complex(0,1)*l7*vevs - ca2**3*ca3*complex(0,1)*l8*sa1**2*vevs - 3*ca2*ca3*complex(0,1)*l6*sa2**2*vevs + 2*ca1**2*ca2*ca3*complex(0,1)*l7*sa2**2*vevs + 2*ca2*ca3*complex(0,1)*l8*sa1**2*sa2**2*vevs - 2*ca1*ca2*complex(0,1)*l7*sa1*sa2*sa3*vevs + 2*ca1*ca2*complex(0,1)*l8*sa1*sa2*sa3*vevs',
                  order = {'QED':1})

GC_499 = Coupling(name = 'GC_499',
                  value = '3*ca1**2*ca2**2*ca3*complex(0,1)*l1*sa1*vev1 - 2*ca1**2*ca2**2*ca3*complex(0,1)*l3*sa1*vev1 - 2*ca1**2*ca2**2*ca3*complex(0,1)*l4*sa1*vev1 - 2*ca1**2*ca2**2*ca3*complex(0,1)*l5*sa1*vev1 + ca2**2*ca3*complex(0,1)*l3*sa1**3*vev1 + ca2**2*ca3*complex(0,1)*l4*sa1**3*vev1 + ca2**2*ca3*complex(0,1)*l5*sa1**3*vev1 + ca3*complex(0,1)*l7*sa1*sa2**2*vev1 + 3*ca1**3*ca2**2*complex(0,1)*l1*sa2*sa3*vev1 - 2*ca1*ca2**2*complex(0,1)*l7*sa2*sa3*vev1 + 3*ca1*ca2**2*complex(0,1)*l3*sa1**2*sa2*sa3*vev1 + 3*ca1*ca2**2*complex(0,1)*l4*sa1**2*sa2*sa3*vev1 + 3*ca1*ca2**2*complex(0,1)*l5*sa1**2*sa2*sa3*vev1 + ca1*complex(0,1)*l7*sa2**3*sa3*vev1 - ca1**3*ca2**2*ca3*complex(0,1)*l3*vev2 - ca1**3*ca2**2*ca3*complex(0,1)*l4*vev2 - ca1**3*ca2**2*ca3*complex(0,1)*l5*vev2 - 3*ca1*ca2**2*ca3*complex(0,1)*l2*sa1**2*vev2 + 2*ca1*ca2**2*ca3*complex(0,1)*l3*sa1**2*vev2 + 2*ca1*ca2**2*ca3*complex(0,1)*l4*sa1**2*vev2 + 2*ca1*ca2**2*ca3*complex(0,1)*l5*sa1**2*vev2 - ca1*ca3*complex(0,1)*l8*sa2**2*vev2 + 3*ca1**2*ca2**2*complex(0,1)*l3*sa1*sa2*sa3*vev2 + 3*ca1**2*ca2**2*complex(0,1)*l4*sa1*sa2*sa3*vev2 + 3*ca1**2*ca2**2*complex(0,1)*l5*sa1*sa2*sa3*vev2 - 2*ca2**2*complex(0,1)*l8*sa1*sa2*sa3*vev2 + 3*ca2**2*complex(0,1)*l2*sa1**3*sa2*sa3*vev2 + complex(0,1)*l8*sa1*sa2**3*sa3*vev2 + 2*ca1*ca2*ca3*complex(0,1)*l7*sa1*sa2*vevs - 2*ca1*ca2*ca3*complex(0,1)*l8*sa1*sa2*vevs - ca1**2*ca2**3*complex(0,1)*l7*sa3*vevs - ca2**3*complex(0,1)*l8*sa1**2*sa3*vevs - 3*ca2*complex(0,1)*l6*sa2**2*sa3*vevs + 2*ca1**2*ca2*complex(0,1)*l7*sa2**2*sa3*vevs + 2*ca2*complex(0,1)*l8*sa1**2*sa2**2*sa3*vevs',
                  order = {'QED':1})

GC_500 = Coupling(name = 'GC_500',
                  value = '3*ca1*ca2**2*ca3**3*complex(0,1)*l7*sa2*vev1 + 3*ca1**3*ca3**3*complex(0,1)*l1*sa2**3*vev1 + 3*ca1*ca3**3*complex(0,1)*l3*sa1**2*sa2**3*vev1 + 3*ca1*ca3**3*complex(0,1)*l4*sa1**2*sa2**3*vev1 + 3*ca1*ca3**3*complex(0,1)*l5*sa1**2*sa2**3*vev1 - 3*ca2**2*ca3**2*complex(0,1)*l7*sa1*sa3*vev1 - 9*ca1**2*ca3**2*complex(0,1)*l1*sa1*sa2**2*sa3*vev1 + 6*ca1**2*ca3**2*complex(0,1)*l3*sa1*sa2**2*sa3*vev1 + 6*ca1**2*ca3**2*complex(0,1)*l4*sa1*sa2**2*sa3*vev1 + 6*ca1**2*ca3**2*complex(0,1)*l5*sa1*sa2**2*sa3*vev1 - 3*ca3**2*complex(0,1)*l3*sa1**3*sa2**2*sa3*vev1 - 3*ca3**2*complex(0,1)*l4*sa1**3*sa2**2*sa3*vev1 - 3*ca3**2*complex(0,1)*l5*sa1**3*sa2**2*sa3*vev1 + 3*ca1**3*ca3*complex(0,1)*l3*sa2*sa3**2*vev1 + 3*ca1**3*ca3*complex(0,1)*l4*sa2*sa3**2*vev1 + 3*ca1**3*ca3*complex(0,1)*l5*sa2*sa3**2*vev1 + 9*ca1*ca3*complex(0,1)*l1*sa1**2*sa2*sa3**2*vev1 - 6*ca1*ca3*complex(0,1)*l3*sa1**2*sa2*sa3**2*vev1 - 6*ca1*ca3*complex(0,1)*l4*sa1**2*sa2*sa3**2*vev1 - 6*ca1*ca3*complex(0,1)*l5*sa1**2*sa2*sa3**2*vev1 - 3*ca1**2*complex(0,1)*l3*sa1*sa3**3*vev1 - 3*ca1**2*complex(0,1)*l4*sa1*sa3**3*vev1 - 3*ca1**2*complex(0,1)*l5*sa1*sa3**3*vev1 - 3*complex(0,1)*l1*sa1**3*sa3**3*vev1 + 3*ca2**2*ca3**3*complex(0,1)*l8*sa1*sa2*vev2 + 3*ca1**2*ca3**3*complex(0,1)*l3*sa1*sa2**3*vev2 + 3*ca1**2*ca3**3*complex(0,1)*l4*sa1*sa2**3*vev2 + 3*ca1**2*ca3**3*complex(0,1)*l5*sa1*sa2**3*vev2 + 3*ca3**3*complex(0,1)*l2*sa1**3*sa2**3*vev2 + 3*ca1*ca2**2*ca3**2*complex(0,1)*l8*sa3*vev2 + 3*ca1**3*ca3**2*complex(0,1)*l3*sa2**2*sa3*vev2 + 3*ca1**3*ca3**2*complex(0,1)*l4*sa2**2*sa3*vev2 + 3*ca1**3*ca3**2*complex(0,1)*l5*sa2**2*sa3*vev2 + 9*ca1*ca3**2*complex(0,1)*l2*sa1**2*sa2**2*sa3*vev2 - 6*ca1*ca3**2*complex(0,1)*l3*sa1**2*sa2**2*sa3*vev2 - 6*ca1*ca3**2*complex(0,1)*l4*sa1**2*sa2**2*sa3*vev2 - 6*ca1*ca3**2*complex(0,1)*l5*sa1**2*sa2**2*sa3*vev2 + 9*ca1**2*ca3*complex(0,1)*l2*sa1*sa2*sa3**2*vev2 - 6*ca1**2*ca3*complex(0,1)*l3*sa1*sa2*sa3**2*vev2 - 6*ca1**2*ca3*complex(0,1)*l4*sa1*sa2*sa3**2*vev2 - 6*ca1**2*ca3*complex(0,1)*l5*sa1*sa2*sa3**2*vev2 + 3*ca3*complex(0,1)*l3*sa1**3*sa2*sa3**2*vev2 + 3*ca3*complex(0,1)*l4*sa1**3*sa2*sa3**2*vev2 + 3*ca3*complex(0,1)*l5*sa1**3*sa2*sa3**2*vev2 + 3*ca1**3*complex(0,1)*l2*sa3**3*vev2 + 3*ca1*complex(0,1)*l3*sa1**2*sa3**3*vev2 + 3*ca1*complex(0,1)*l4*sa1**2*sa3**3*vev2 + 3*ca1*complex(0,1)*l5*sa1**2*sa3**3*vev2 - 3*ca2**3*ca3**3*complex(0,1)*l6*vevs - 3*ca1**2*ca2*ca3**3*complex(0,1)*l7*sa2**2*vevs - 3*ca2*ca3**3*complex(0,1)*l8*sa1**2*sa2**2*vevs + 6*ca1*ca2*ca3**2*complex(0,1)*l7*sa1*sa2*sa3*vevs - 6*ca1*ca2*ca3**2*complex(0,1)*l8*sa1*sa2*sa3*vevs - 3*ca1**2*ca2*ca3*complex(0,1)*l8*sa3**2*vevs - 3*ca2*ca3*complex(0,1)*l7*sa1**2*sa3**2*vevs',
                  order = {'QED':1})

GC_501 = Coupling(name = 'GC_501',
                  value = '-(ca1*ca2**3*ca3**2*complex(0,1)*l7*vev1) - 3*ca1**3*ca2*ca3**2*complex(0,1)*l1*sa2**2*vev1 + 2*ca1*ca2*ca3**2*complex(0,1)*l7*sa2**2*vev1 - 3*ca1*ca2*ca3**2*complex(0,1)*l3*sa1**2*sa2**2*vev1 - 3*ca1*ca2*ca3**2*complex(0,1)*l4*sa1**2*sa2**2*vev1 - 3*ca1*ca2*ca3**2*complex(0,1)*l5*sa1**2*sa2**2*vev1 + 6*ca1**2*ca2*ca3*complex(0,1)*l1*sa1*sa2*sa3*vev1 - 4*ca1**2*ca2*ca3*complex(0,1)*l3*sa1*sa2*sa3*vev1 - 4*ca1**2*ca2*ca3*complex(0,1)*l4*sa1*sa2*sa3*vev1 - 4*ca1**2*ca2*ca3*complex(0,1)*l5*sa1*sa2*sa3*vev1 - 2*ca2*ca3*complex(0,1)*l7*sa1*sa2*sa3*vev1 + 2*ca2*ca3*complex(0,1)*l3*sa1**3*sa2*sa3*vev1 + 2*ca2*ca3*complex(0,1)*l4*sa1**3*sa2*sa3*vev1 + 2*ca2*ca3*complex(0,1)*l5*sa1**3*sa2*sa3*vev1 - ca1**3*ca2*complex(0,1)*l3*sa3**2*vev1 - ca1**3*ca2*complex(0,1)*l4*sa3**2*vev1 - ca1**3*ca2*complex(0,1)*l5*sa3**2*vev1 - 3*ca1*ca2*complex(0,1)*l1*sa1**2*sa3**2*vev1 + 2*ca1*ca2*complex(0,1)*l3*sa1**2*sa3**2*vev1 + 2*ca1*ca2*complex(0,1)*l4*sa1**2*sa3**2*vev1 + 2*ca1*ca2*complex(0,1)*l5*sa1**2*sa3**2*vev1 - ca2**3*ca3**2*complex(0,1)*l8*sa1*vev2 - 3*ca1**2*ca2*ca3**2*complex(0,1)*l3*sa1*sa2**2*vev2 - 3*ca1**2*ca2*ca3**2*complex(0,1)*l4*sa1*sa2**2*vev2 - 3*ca1**2*ca2*ca3**2*complex(0,1)*l5*sa1*sa2**2*vev2 + 2*ca2*ca3**2*complex(0,1)*l8*sa1*sa2**2*vev2 - 3*ca2*ca3**2*complex(0,1)*l2*sa1**3*sa2**2*vev2 - 2*ca1**3*ca2*ca3*complex(0,1)*l3*sa2*sa3*vev2 - 2*ca1**3*ca2*ca3*complex(0,1)*l4*sa2*sa3*vev2 - 2*ca1**3*ca2*ca3*complex(0,1)*l5*sa2*sa3*vev2 + 2*ca1*ca2*ca3*complex(0,1)*l8*sa2*sa3*vev2 - 6*ca1*ca2*ca3*complex(0,1)*l2*sa1**2*sa2*sa3*vev2 + 4*ca1*ca2*ca3*complex(0,1)*l3*sa1**2*sa2*sa3*vev2 + 4*ca1*ca2*ca3*complex(0,1)*l4*sa1**2*sa2*sa3*vev2 + 4*ca1*ca2*ca3*complex(0,1)*l5*sa1**2*sa2*sa3*vev2 - 3*ca1**2*ca2*complex(0,1)*l2*sa1*sa3**2*vev2 + 2*ca1**2*ca2*complex(0,1)*l3*sa1*sa3**2*vev2 + 2*ca1**2*ca2*complex(0,1)*l4*sa1*sa3**2*vev2 + 2*ca1**2*ca2*complex(0,1)*l5*sa1*sa3**2*vev2 - ca2*complex(0,1)*l3*sa1**3*sa3**2*vev2 - ca2*complex(0,1)*l4*sa1**3*sa3**2*vev2 - ca2*complex(0,1)*l5*sa1**3*sa3**2*vev2 - 3*ca2**2*ca3**2*complex(0,1)*l6*sa2*vevs + 2*ca1**2*ca2**2*ca3**2*complex(0,1)*l7*sa2*vevs + 2*ca2**2*ca3**2*complex(0,1)*l8*sa1**2*sa2*vevs - ca1**2*ca3**2*complex(0,1)*l7*sa2**3*vevs - ca3**2*complex(0,1)*l8*sa1**2*sa2**3*vevs - 2*ca1*ca2**2*ca3*complex(0,1)*l7*sa1*sa3*vevs + 2*ca1*ca2**2*ca3*complex(0,1)*l8*sa1*sa3*vevs + 2*ca1*ca3*complex(0,1)*l7*sa1*sa2**2*sa3*vevs - 2*ca1*ca3*complex(0,1)*l8*sa1*sa2**2*sa3*vevs - ca1**2*complex(0,1)*l8*sa2*sa3**2*vevs - complex(0,1)*l7*sa1**2*sa2*sa3**2*vevs',
                  order = {'QED':1})

GC_502 = Coupling(name = 'GC_502',
                  value = '-3*ca1**2*ca2*ca3**2*complex(0,1)*l1*sa1*sa2*vev1 + 2*ca1**2*ca2*ca3**2*complex(0,1)*l3*sa1*sa2*vev1 + 2*ca1**2*ca2*ca3**2*complex(0,1)*l4*sa1*sa2*vev1 + 2*ca1**2*ca2*ca3**2*complex(0,1)*l5*sa1*sa2*vev1 + ca2*ca3**2*complex(0,1)*l7*sa1*sa2*vev1 - ca2*ca3**2*complex(0,1)*l3*sa1**3*sa2*vev1 - ca2*ca3**2*complex(0,1)*l4*sa1**3*sa2*vev1 - ca2*ca3**2*complex(0,1)*l5*sa1**3*sa2*vev1 + ca1**3*ca2*ca3*complex(0,1)*l3*sa3*vev1 + ca1**3*ca2*ca3*complex(0,1)*l4*sa3*vev1 + ca1**3*ca2*ca3*complex(0,1)*l5*sa3*vev1 - ca1*ca2**3*ca3*complex(0,1)*l7*sa3*vev1 + 3*ca1*ca2*ca3*complex(0,1)*l1*sa1**2*sa3*vev1 - 2*ca1*ca2*ca3*complex(0,1)*l3*sa1**2*sa3*vev1 - 2*ca1*ca2*ca3*complex(0,1)*l4*sa1**2*sa3*vev1 - 2*ca1*ca2*ca3*complex(0,1)*l5*sa1**2*sa3*vev1 - 3*ca1**3*ca2*ca3*complex(0,1)*l1*sa2**2*sa3*vev1 + 2*ca1*ca2*ca3*complex(0,1)*l7*sa2**2*sa3*vev1 - 3*ca1*ca2*ca3*complex(0,1)*l3*sa1**2*sa2**2*sa3*vev1 - 3*ca1*ca2*ca3*complex(0,1)*l4*sa1**2*sa2**2*sa3*vev1 - 3*ca1*ca2*ca3*complex(0,1)*l5*sa1**2*sa2**2*sa3*vev1 + 3*ca1**2*ca2*complex(0,1)*l1*sa1*sa2*sa3**2*vev1 - 2*ca1**2*ca2*complex(0,1)*l3*sa1*sa2*sa3**2*vev1 - 2*ca1**2*ca2*complex(0,1)*l4*sa1*sa2*sa3**2*vev1 - 2*ca1**2*ca2*complex(0,1)*l5*sa1*sa2*sa3**2*vev1 - ca2*complex(0,1)*l7*sa1*sa2*sa3**2*vev1 + ca2*complex(0,1)*l3*sa1**3*sa2*sa3**2*vev1 + ca2*complex(0,1)*l4*sa1**3*sa2*sa3**2*vev1 + ca2*complex(0,1)*l5*sa1**3*sa2*sa3**2*vev1 + ca1**3*ca2*ca3**2*complex(0,1)*l3*sa2*vev2 + ca1**3*ca2*ca3**2*complex(0,1)*l4*sa2*vev2 + ca1**3*ca2*ca3**2*complex(0,1)*l5*sa2*vev2 - ca1*ca2*ca3**2*complex(0,1)*l8*sa2*vev2 + 3*ca1*ca2*ca3**2*complex(0,1)*l2*sa1**2*sa2*vev2 - 2*ca1*ca2*ca3**2*complex(0,1)*l3*sa1**2*sa2*vev2 - 2*ca1*ca2*ca3**2*complex(0,1)*l4*sa1**2*sa2*vev2 - 2*ca1*ca2*ca3**2*complex(0,1)*l5*sa1**2*sa2*vev2 + 3*ca1**2*ca2*ca3*complex(0,1)*l2*sa1*sa3*vev2 - 2*ca1**2*ca2*ca3*complex(0,1)*l3*sa1*sa3*vev2 - 2*ca1**2*ca2*ca3*complex(0,1)*l4*sa1*sa3*vev2 - 2*ca1**2*ca2*ca3*complex(0,1)*l5*sa1*sa3*vev2 - ca2**3*ca3*complex(0,1)*l8*sa1*sa3*vev2 + ca2*ca3*complex(0,1)*l3*sa1**3*sa3*vev2 + ca2*ca3*complex(0,1)*l4*sa1**3*sa3*vev2 + ca2*ca3*complex(0,1)*l5*sa1**3*sa3*vev2 - 3*ca1**2*ca2*ca3*complex(0,1)*l3*sa1*sa2**2*sa3*vev2 - 3*ca1**2*ca2*ca3*complex(0,1)*l4*sa1*sa2**2*sa3*vev2 - 3*ca1**2*ca2*ca3*complex(0,1)*l5*sa1*sa2**2*sa3*vev2 + 2*ca2*ca3*complex(0,1)*l8*sa1*sa2**2*sa3*vev2 - 3*ca2*ca3*complex(0,1)*l2*sa1**3*sa2**2*sa3*vev2 - ca1**3*ca2*complex(0,1)*l3*sa2*sa3**2*vev2 - ca1**3*ca2*complex(0,1)*l4*sa2*sa3**2*vev2 - ca1**3*ca2*complex(0,1)*l5*sa2*sa3**2*vev2 + ca1*ca2*complex(0,1)*l8*sa2*sa3**2*vev2 - 3*ca1*ca2*complex(0,1)*l2*sa1**2*sa2*sa3**2*vev2 + 2*ca1*ca2*complex(0,1)*l3*sa1**2*sa2*sa3**2*vev2 + 2*ca1*ca2*complex(0,1)*l4*sa1**2*sa2*sa3**2*vev2 + 2*ca1*ca2*complex(0,1)*l5*sa1**2*sa2*sa3**2*vev2 + ca1*ca2**2*ca3**2*complex(0,1)*l7*sa1*vevs - ca1*ca2**2*ca3**2*complex(0,1)*l8*sa1*vevs - ca1*ca3**2*complex(0,1)*l7*sa1*sa2**2*vevs + ca1*ca3**2*complex(0,1)*l8*sa1*sa2**2*vevs - 3*ca2**2*ca3*complex(0,1)*l6*sa2*sa3*vevs + 2*ca1**2*ca2**2*ca3*complex(0,1)*l7*sa2*sa3*vevs + ca1**2*ca3*complex(0,1)*l8*sa2*sa3*vevs + ca3*complex(0,1)*l7*sa1**2*sa2*sa3*vevs + 2*ca2**2*ca3*complex(0,1)*l8*sa1**2*sa2*sa3*vevs - ca1**2*ca3*complex(0,1)*l7*sa2**3*sa3*vevs - ca3*complex(0,1)*l8*sa1**2*sa2**3*sa3*vevs - ca1*ca2**2*complex(0,1)*l7*sa1*sa3**2*vevs + ca1*ca2**2*complex(0,1)*l8*sa1*sa3**2*vevs + ca1*complex(0,1)*l7*sa1*sa2**2*sa3**2*vevs - ca1*complex(0,1)*l8*sa1*sa2**2*sa3**2*vevs',
                  order = {'QED':1})

GC_503 = Coupling(name = 'GC_503',
                  value = '-(ca1**3*ca2*ca3**2*complex(0,1)*l3*vev1) - ca1**3*ca2*ca3**2*complex(0,1)*l4*vev1 - ca1**3*ca2*ca3**2*complex(0,1)*l5*vev1 - 3*ca1*ca2*ca3**2*complex(0,1)*l1*sa1**2*vev1 + 2*ca1*ca2*ca3**2*complex(0,1)*l3*sa1**2*vev1 + 2*ca1*ca2*ca3**2*complex(0,1)*l4*sa1**2*vev1 + 2*ca1*ca2*ca3**2*complex(0,1)*l5*sa1**2*vev1 - 6*ca1**2*ca2*ca3*complex(0,1)*l1*sa1*sa2*sa3*vev1 + 4*ca1**2*ca2*ca3*complex(0,1)*l3*sa1*sa2*sa3*vev1 + 4*ca1**2*ca2*ca3*complex(0,1)*l4*sa1*sa2*sa3*vev1 + 4*ca1**2*ca2*ca3*complex(0,1)*l5*sa1*sa2*sa3*vev1 + 2*ca2*ca3*complex(0,1)*l7*sa1*sa2*sa3*vev1 - 2*ca2*ca3*complex(0,1)*l3*sa1**3*sa2*sa3*vev1 - 2*ca2*ca3*complex(0,1)*l4*sa1**3*sa2*sa3*vev1 - 2*ca2*ca3*complex(0,1)*l5*sa1**3*sa2*sa3*vev1 - ca1*ca2**3*complex(0,1)*l7*sa3**2*vev1 - 3*ca1**3*ca2*complex(0,1)*l1*sa2**2*sa3**2*vev1 + 2*ca1*ca2*complex(0,1)*l7*sa2**2*sa3**2*vev1 - 3*ca1*ca2*complex(0,1)*l3*sa1**2*sa2**2*sa3**2*vev1 - 3*ca1*ca2*complex(0,1)*l4*sa1**2*sa2**2*sa3**2*vev1 - 3*ca1*ca2*complex(0,1)*l5*sa1**2*sa2**2*sa3**2*vev1 - 3*ca1**2*ca2*ca3**2*complex(0,1)*l2*sa1*vev2 + 2*ca1**2*ca2*ca3**2*complex(0,1)*l3*sa1*vev2 + 2*ca1**2*ca2*ca3**2*complex(0,1)*l4*sa1*vev2 + 2*ca1**2*ca2*ca3**2*complex(0,1)*l5*sa1*vev2 - ca2*ca3**2*complex(0,1)*l3*sa1**3*vev2 - ca2*ca3**2*complex(0,1)*l4*sa1**3*vev2 - ca2*ca3**2*complex(0,1)*l5*sa1**3*vev2 + 2*ca1**3*ca2*ca3*complex(0,1)*l3*sa2*sa3*vev2 + 2*ca1**3*ca2*ca3*complex(0,1)*l4*sa2*sa3*vev2 + 2*ca1**3*ca2*ca3*complex(0,1)*l5*sa2*sa3*vev2 - 2*ca1*ca2*ca3*complex(0,1)*l8*sa2*sa3*vev2 + 6*ca1*ca2*ca3*complex(0,1)*l2*sa1**2*sa2*sa3*vev2 - 4*ca1*ca2*ca3*complex(0,1)*l3*sa1**2*sa2*sa3*vev2 - 4*ca1*ca2*ca3*complex(0,1)*l4*sa1**2*sa2*sa3*vev2 - 4*ca1*ca2*ca3*complex(0,1)*l5*sa1**2*sa2*sa3*vev2 - ca2**3*complex(0,1)*l8*sa1*sa3**2*vev2 - 3*ca1**2*ca2*complex(0,1)*l3*sa1*sa2**2*sa3**2*vev2 - 3*ca1**2*ca2*complex(0,1)*l4*sa1*sa2**2*sa3**2*vev2 - 3*ca1**2*ca2*complex(0,1)*l5*sa1*sa2**2*sa3**2*vev2 + 2*ca2*complex(0,1)*l8*sa1*sa2**2*sa3**2*vev2 - 3*ca2*complex(0,1)*l2*sa1**3*sa2**2*sa3**2*vev2 - ca1**2*ca3**2*complex(0,1)*l8*sa2*vevs - ca3**2*complex(0,1)*l7*sa1**2*sa2*vevs + 2*ca1*ca2**2*ca3*complex(0,1)*l7*sa1*sa3*vevs - 2*ca1*ca2**2*ca3*complex(0,1)*l8*sa1*sa3*vevs - 2*ca1*ca3*complex(0,1)*l7*sa1*sa2**2*sa3*vevs + 2*ca1*ca3*complex(0,1)*l8*sa1*sa2**2*sa3*vevs - 3*ca2**2*complex(0,1)*l6*sa2*sa3**2*vevs + 2*ca1**2*ca2**2*complex(0,1)*l7*sa2*sa3**2*vevs + 2*ca2**2*complex(0,1)*l8*sa1**2*sa2*sa3**2*vevs - ca1**2*complex(0,1)*l7*sa2**3*sa3**2*vevs - complex(0,1)*l8*sa1**2*sa2**3*sa3**2*vevs',
                  order = {'QED':1})

GC_504 = Coupling(name = 'GC_504',
                  value = 'ca2**2*ca3**3*complex(0,1)*l7*sa1*vev1 + 3*ca1**2*ca3**3*complex(0,1)*l1*sa1*sa2**2*vev1 - 2*ca1**2*ca3**3*complex(0,1)*l3*sa1*sa2**2*vev1 - 2*ca1**2*ca3**3*complex(0,1)*l4*sa1*sa2**2*vev1 - 2*ca1**2*ca3**3*complex(0,1)*l5*sa1*sa2**2*vev1 + ca3**3*complex(0,1)*l3*sa1**3*sa2**2*vev1 + ca3**3*complex(0,1)*l4*sa1**3*sa2**2*vev1 + ca3**3*complex(0,1)*l5*sa1**3*sa2**2*vev1 - 2*ca1**3*ca3**2*complex(0,1)*l3*sa2*sa3*vev1 - 2*ca1**3*ca3**2*complex(0,1)*l4*sa2*sa3*vev1 - 2*ca1**3*ca3**2*complex(0,1)*l5*sa2*sa3*vev1 + 3*ca1*ca2**2*ca3**2*complex(0,1)*l7*sa2*sa3*vev1 - 6*ca1*ca3**2*complex(0,1)*l1*sa1**2*sa2*sa3*vev1 + 4*ca1*ca3**2*complex(0,1)*l3*sa1**2*sa2*sa3*vev1 + 4*ca1*ca3**2*complex(0,1)*l4*sa1**2*sa2*sa3*vev1 + 4*ca1*ca3**2*complex(0,1)*l5*sa1**2*sa2*sa3*vev1 + 3*ca1**3*ca3**2*complex(0,1)*l1*sa2**3*sa3*vev1 + 3*ca1*ca3**2*complex(0,1)*l3*sa1**2*sa2**3*sa3*vev1 + 3*ca1*ca3**2*complex(0,1)*l4*sa1**2*sa2**3*sa3*vev1 + 3*ca1*ca3**2*complex(0,1)*l5*sa1**2*sa2**3*sa3*vev1 + 3*ca1**2*ca3*complex(0,1)*l3*sa1*sa3**2*vev1 + 3*ca1**2*ca3*complex(0,1)*l4*sa1*sa3**2*vev1 + 3*ca1**2*ca3*complex(0,1)*l5*sa1*sa3**2*vev1 - 2*ca2**2*ca3*complex(0,1)*l7*sa1*sa3**2*vev1 + 3*ca3*complex(0,1)*l1*sa1**3*sa3**2*vev1 - 6*ca1**2*ca3*complex(0,1)*l1*sa1*sa2**2*sa3**2*vev1 + 4*ca1**2*ca3*complex(0,1)*l3*sa1*sa2**2*sa3**2*vev1 + 4*ca1**2*ca3*complex(0,1)*l4*sa1*sa2**2*sa3**2*vev1 + 4*ca1**2*ca3*complex(0,1)*l5*sa1*sa2**2*sa3**2*vev1 - 2*ca3*complex(0,1)*l3*sa1**3*sa2**2*sa3**2*vev1 - 2*ca3*complex(0,1)*l4*sa1**3*sa2**2*sa3**2*vev1 - 2*ca3*complex(0,1)*l5*sa1**3*sa2**2*sa3**2*vev1 + ca1**3*complex(0,1)*l3*sa2*sa3**3*vev1 + ca1**3*complex(0,1)*l4*sa2*sa3**3*vev1 + ca1**3*complex(0,1)*l5*sa2*sa3**3*vev1 + 3*ca1*complex(0,1)*l1*sa1**2*sa2*sa3**3*vev1 - 2*ca1*complex(0,1)*l3*sa1**2*sa2*sa3**3*vev1 - 2*ca1*complex(0,1)*l4*sa1**2*sa2*sa3**3*vev1 - 2*ca1*complex(0,1)*l5*sa1**2*sa2*sa3**3*vev1 - ca1*ca2**2*ca3**3*complex(0,1)*l8*vev2 - ca1**3*ca3**3*complex(0,1)*l3*sa2**2*vev2 - ca1**3*ca3**3*complex(0,1)*l4*sa2**2*vev2 - ca1**3*ca3**3*complex(0,1)*l5*sa2**2*vev2 - 3*ca1*ca3**3*complex(0,1)*l2*sa1**2*sa2**2*vev2 + 2*ca1*ca3**3*complex(0,1)*l3*sa1**2*sa2**2*vev2 + 2*ca1*ca3**3*complex(0,1)*l4*sa1**2*sa2**2*vev2 + 2*ca1*ca3**3*complex(0,1)*l5*sa1**2*sa2**2*vev2 - 6*ca1**2*ca3**2*complex(0,1)*l2*sa1*sa2*sa3*vev2 + 4*ca1**2*ca3**2*complex(0,1)*l3*sa1*sa2*sa3*vev2 + 4*ca1**2*ca3**2*complex(0,1)*l4*sa1*sa2*sa3*vev2 + 4*ca1**2*ca3**2*complex(0,1)*l5*sa1*sa2*sa3*vev2 + 3*ca2**2*ca3**2*complex(0,1)*l8*sa1*sa2*sa3*vev2 - 2*ca3**2*complex(0,1)*l3*sa1**3*sa2*sa3*vev2 - 2*ca3**2*complex(0,1)*l4*sa1**3*sa2*sa3*vev2 - 2*ca3**2*complex(0,1)*l5*sa1**3*sa2*sa3*vev2 + 3*ca1**2*ca3**2*complex(0,1)*l3*sa1*sa2**3*sa3*vev2 + 3*ca1**2*ca3**2*complex(0,1)*l4*sa1*sa2**3*sa3*vev2 + 3*ca1**2*ca3**2*complex(0,1)*l5*sa1*sa2**3*sa3*vev2 + 3*ca3**2*complex(0,1)*l2*sa1**3*sa2**3*sa3*vev2 - 3*ca1**3*ca3*complex(0,1)*l2*sa3**2*vev2 + 2*ca1*ca2**2*ca3*complex(0,1)*l8*sa3**2*vev2 - 3*ca1*ca3*complex(0,1)*l3*sa1**2*sa3**2*vev2 - 3*ca1*ca3*complex(0,1)*l4*sa1**2*sa3**2*vev2 - 3*ca1*ca3*complex(0,1)*l5*sa1**2*sa3**2*vev2 + 2*ca1**3*ca3*complex(0,1)*l3*sa2**2*sa3**2*vev2 + 2*ca1**3*ca3*complex(0,1)*l4*sa2**2*sa3**2*vev2 + 2*ca1**3*ca3*complex(0,1)*l5*sa2**2*sa3**2*vev2 + 6*ca1*ca3*complex(0,1)*l2*sa1**2*sa2**2*sa3**2*vev2 - 4*ca1*ca3*complex(0,1)*l3*sa1**2*sa2**2*sa3**2*vev2 - 4*ca1*ca3*complex(0,1)*l4*sa1**2*sa2**2*sa3**2*vev2 - 4*ca1*ca3*complex(0,1)*l5*sa1**2*sa2**2*sa3**2*vev2 + 3*ca1**2*complex(0,1)*l2*sa1*sa2*sa3**3*vev2 - 2*ca1**2*complex(0,1)*l3*sa1*sa2*sa3**3*vev2 - 2*ca1**2*complex(0,1)*l4*sa1*sa2*sa3**3*vev2 - 2*ca1**2*complex(0,1)*l5*sa1*sa2*sa3**3*vev2 + complex(0,1)*l3*sa1**3*sa2*sa3**3*vev2 + complex(0,1)*l4*sa1**3*sa2*sa3**3*vev2 + complex(0,1)*l5*sa1**3*sa2*sa3**3*vev2 - 2*ca1*ca2*ca3**3*complex(0,1)*l7*sa1*sa2*vevs + 2*ca1*ca2*ca3**3*complex(0,1)*l8*sa1*sa2*vevs - 3*ca2**3*ca3**2*complex(0,1)*l6*sa3*vevs + 2*ca1**2*ca2*ca3**2*complex(0,1)*l8*sa3*vevs + 2*ca2*ca3**2*complex(0,1)*l7*sa1**2*sa3*vevs - 3*ca1**2*ca2*ca3**2*complex(0,1)*l7*sa2**2*sa3*vevs - 3*ca2*ca3**2*complex(0,1)*l8*sa1**2*sa2**2*sa3*vevs + 4*ca1*ca2*ca3*complex(0,1)*l7*sa1*sa2*sa3**2*vevs - 4*ca1*ca2*ca3*complex(0,1)*l8*sa1*sa2*sa3**2*vevs - ca1**2*ca2*complex(0,1)*l8*sa3**3*vevs - ca2*complex(0,1)*l7*sa1**2*sa3**3*vevs',
                  order = {'QED':1})

GC_505 = Coupling(name = 'GC_505',
                  value = 'ca1**3*ca3**3*complex(0,1)*l3*sa2*vev1 + ca1**3*ca3**3*complex(0,1)*l4*sa2*vev1 + ca1**3*ca3**3*complex(0,1)*l5*sa2*vev1 + 3*ca1*ca3**3*complex(0,1)*l1*sa1**2*sa2*vev1 - 2*ca1*ca3**3*complex(0,1)*l3*sa1**2*sa2*vev1 - 2*ca1*ca3**3*complex(0,1)*l4*sa1**2*sa2*vev1 - 2*ca1*ca3**3*complex(0,1)*l5*sa1**2*sa2*vev1 - 3*ca1**2*ca3**2*complex(0,1)*l3*sa1*sa3*vev1 - 3*ca1**2*ca3**2*complex(0,1)*l4*sa1*sa3*vev1 - 3*ca1**2*ca3**2*complex(0,1)*l5*sa1*sa3*vev1 + 2*ca2**2*ca3**2*complex(0,1)*l7*sa1*sa3*vev1 - 3*ca3**2*complex(0,1)*l1*sa1**3*sa3*vev1 + 6*ca1**2*ca3**2*complex(0,1)*l1*sa1*sa2**2*sa3*vev1 - 4*ca1**2*ca3**2*complex(0,1)*l3*sa1*sa2**2*sa3*vev1 - 4*ca1**2*ca3**2*complex(0,1)*l4*sa1*sa2**2*sa3*vev1 - 4*ca1**2*ca3**2*complex(0,1)*l5*sa1*sa2**2*sa3*vev1 + 2*ca3**2*complex(0,1)*l3*sa1**3*sa2**2*sa3*vev1 + 2*ca3**2*complex(0,1)*l4*sa1**3*sa2**2*sa3*vev1 + 2*ca3**2*complex(0,1)*l5*sa1**3*sa2**2*sa3*vev1 - 2*ca1**3*ca3*complex(0,1)*l3*sa2*sa3**2*vev1 - 2*ca1**3*ca3*complex(0,1)*l4*sa2*sa3**2*vev1 - 2*ca1**3*ca3*complex(0,1)*l5*sa2*sa3**2*vev1 + 3*ca1*ca2**2*ca3*complex(0,1)*l7*sa2*sa3**2*vev1 - 6*ca1*ca3*complex(0,1)*l1*sa1**2*sa2*sa3**2*vev1 + 4*ca1*ca3*complex(0,1)*l3*sa1**2*sa2*sa3**2*vev1 + 4*ca1*ca3*complex(0,1)*l4*sa1**2*sa2*sa3**2*vev1 + 4*ca1*ca3*complex(0,1)*l5*sa1**2*sa2*sa3**2*vev1 + 3*ca1**3*ca3*complex(0,1)*l1*sa2**3*sa3**2*vev1 + 3*ca1*ca3*complex(0,1)*l3*sa1**2*sa2**3*sa3**2*vev1 + 3*ca1*ca3*complex(0,1)*l4*sa1**2*sa2**3*sa3**2*vev1 + 3*ca1*ca3*complex(0,1)*l5*sa1**2*sa2**3*sa3**2*vev1 - ca2**2*complex(0,1)*l7*sa1*sa3**3*vev1 - 3*ca1**2*complex(0,1)*l1*sa1*sa2**2*sa3**3*vev1 + 2*ca1**2*complex(0,1)*l3*sa1*sa2**2*sa3**3*vev1 + 2*ca1**2*complex(0,1)*l4*sa1*sa2**2*sa3**3*vev1 + 2*ca1**2*complex(0,1)*l5*sa1*sa2**2*sa3**3*vev1 - complex(0,1)*l3*sa1**3*sa2**2*sa3**3*vev1 - complex(0,1)*l4*sa1**3*sa2**2*sa3**3*vev1 - complex(0,1)*l5*sa1**3*sa2**2*sa3**3*vev1 + 3*ca1**2*ca3**3*complex(0,1)*l2*sa1*sa2*vev2 - 2*ca1**2*ca3**3*complex(0,1)*l3*sa1*sa2*vev2 - 2*ca1**2*ca3**3*complex(0,1)*l4*sa1*sa2*vev2 - 2*ca1**2*ca3**3*complex(0,1)*l5*sa1*sa2*vev2 + ca3**3*complex(0,1)*l3*sa1**3*sa2*vev2 + ca3**3*complex(0,1)*l4*sa1**3*sa2*vev2 + ca3**3*complex(0,1)*l5*sa1**3*sa2*vev2 + 3*ca1**3*ca3**2*complex(0,1)*l2*sa3*vev2 - 2*ca1*ca2**2*ca3**2*complex(0,1)*l8*sa3*vev2 + 3*ca1*ca3**2*complex(0,1)*l3*sa1**2*sa3*vev2 + 3*ca1*ca3**2*complex(0,1)*l4*sa1**2*sa3*vev2 + 3*ca1*ca3**2*complex(0,1)*l5*sa1**2*sa3*vev2 - 2*ca1**3*ca3**2*complex(0,1)*l3*sa2**2*sa3*vev2 - 2*ca1**3*ca3**2*complex(0,1)*l4*sa2**2*sa3*vev2 - 2*ca1**3*ca3**2*complex(0,1)*l5*sa2**2*sa3*vev2 - 6*ca1*ca3**2*complex(0,1)*l2*sa1**2*sa2**2*sa3*vev2 + 4*ca1*ca3**2*complex(0,1)*l3*sa1**2*sa2**2*sa3*vev2 + 4*ca1*ca3**2*complex(0,1)*l4*sa1**2*sa2**2*sa3*vev2 + 4*ca1*ca3**2*complex(0,1)*l5*sa1**2*sa2**2*sa3*vev2 - 6*ca1**2*ca3*complex(0,1)*l2*sa1*sa2*sa3**2*vev2 + 4*ca1**2*ca3*complex(0,1)*l3*sa1*sa2*sa3**2*vev2 + 4*ca1**2*ca3*complex(0,1)*l4*sa1*sa2*sa3**2*vev2 + 4*ca1**2*ca3*complex(0,1)*l5*sa1*sa2*sa3**2*vev2 + 3*ca2**2*ca3*complex(0,1)*l8*sa1*sa2*sa3**2*vev2 - 2*ca3*complex(0,1)*l3*sa1**3*sa2*sa3**2*vev2 - 2*ca3*complex(0,1)*l4*sa1**3*sa2*sa3**2*vev2 - 2*ca3*complex(0,1)*l5*sa1**3*sa2*sa3**2*vev2 + 3*ca1**2*ca3*complex(0,1)*l3*sa1*sa2**3*sa3**2*vev2 + 3*ca1**2*ca3*complex(0,1)*l4*sa1*sa2**3*sa3**2*vev2 + 3*ca1**2*ca3*complex(0,1)*l5*sa1*sa2**3*sa3**2*vev2 + 3*ca3*complex(0,1)*l2*sa1**3*sa2**3*sa3**2*vev2 + ca1*ca2**2*complex(0,1)*l8*sa3**3*vev2 + ca1**3*complex(0,1)*l3*sa2**2*sa3**3*vev2 + ca1**3*complex(0,1)*l4*sa2**2*sa3**3*vev2 + ca1**3*complex(0,1)*l5*sa2**2*sa3**3*vev2 + 3*ca1*complex(0,1)*l2*sa1**2*sa2**2*sa3**3*vev2 - 2*ca1*complex(0,1)*l3*sa1**2*sa2**2*sa3**3*vev2 - 2*ca1*complex(0,1)*l4*sa1**2*sa2**2*sa3**3*vev2 - 2*ca1*complex(0,1)*l5*sa1**2*sa2**2*sa3**3*vev2 - ca1**2*ca2*ca3**3*complex(0,1)*l8*vevs - ca2*ca3**3*complex(0,1)*l7*sa1**2*vevs - 4*ca1*ca2*ca3**2*complex(0,1)*l7*sa1*sa2*sa3*vevs + 4*ca1*ca2*ca3**2*complex(0,1)*l8*sa1*sa2*sa3*vevs - 3*ca2**3*ca3*complex(0,1)*l6*sa3**2*vevs + 2*ca1**2*ca2*ca3*complex(0,1)*l8*sa3**2*vevs + 2*ca2*ca3*complex(0,1)*l7*sa1**2*sa3**2*vevs - 3*ca1**2*ca2*ca3*complex(0,1)*l7*sa2**2*sa3**2*vevs - 3*ca2*ca3*complex(0,1)*l8*sa1**2*sa2**2*sa3**2*vevs + 2*ca1*ca2*complex(0,1)*l7*sa1*sa2*sa3**3*vevs - 2*ca1*ca2*complex(0,1)*l8*sa1*sa2*sa3**3*vevs',
                  order = {'QED':1})

GC_506 = Coupling(name = 'GC_506',
                  value = '3*ca1**2*ca3**3*complex(0,1)*l3*sa1*vev1 + 3*ca1**2*ca3**3*complex(0,1)*l4*sa1*vev1 + 3*ca1**2*ca3**3*complex(0,1)*l5*sa1*vev1 + 3*ca3**3*complex(0,1)*l1*sa1**3*vev1 + 3*ca1**3*ca3**2*complex(0,1)*l3*sa2*sa3*vev1 + 3*ca1**3*ca3**2*complex(0,1)*l4*sa2*sa3*vev1 + 3*ca1**3*ca3**2*complex(0,1)*l5*sa2*sa3*vev1 + 9*ca1*ca3**2*complex(0,1)*l1*sa1**2*sa2*sa3*vev1 - 6*ca1*ca3**2*complex(0,1)*l3*sa1**2*sa2*sa3*vev1 - 6*ca1*ca3**2*complex(0,1)*l4*sa1**2*sa2*sa3*vev1 - 6*ca1*ca3**2*complex(0,1)*l5*sa1**2*sa2*sa3*vev1 + 3*ca2**2*ca3*complex(0,1)*l7*sa1*sa3**2*vev1 + 9*ca1**2*ca3*complex(0,1)*l1*sa1*sa2**2*sa3**2*vev1 - 6*ca1**2*ca3*complex(0,1)*l3*sa1*sa2**2*sa3**2*vev1 - 6*ca1**2*ca3*complex(0,1)*l4*sa1*sa2**2*sa3**2*vev1 - 6*ca1**2*ca3*complex(0,1)*l5*sa1*sa2**2*sa3**2*vev1 + 3*ca3*complex(0,1)*l3*sa1**3*sa2**2*sa3**2*vev1 + 3*ca3*complex(0,1)*l4*sa1**3*sa2**2*sa3**2*vev1 + 3*ca3*complex(0,1)*l5*sa1**3*sa2**2*sa3**2*vev1 + 3*ca1*ca2**2*complex(0,1)*l7*sa2*sa3**3*vev1 + 3*ca1**3*complex(0,1)*l1*sa2**3*sa3**3*vev1 + 3*ca1*complex(0,1)*l3*sa1**2*sa2**3*sa3**3*vev1 + 3*ca1*complex(0,1)*l4*sa1**2*sa2**3*sa3**3*vev1 + 3*ca1*complex(0,1)*l5*sa1**2*sa2**3*sa3**3*vev1 - 3*ca1**3*ca3**3*complex(0,1)*l2*vev2 - 3*ca1*ca3**3*complex(0,1)*l3*sa1**2*vev2 - 3*ca1*ca3**3*complex(0,1)*l4*sa1**2*vev2 - 3*ca1*ca3**3*complex(0,1)*l5*sa1**2*vev2 + 9*ca1**2*ca3**2*complex(0,1)*l2*sa1*sa2*sa3*vev2 - 6*ca1**2*ca3**2*complex(0,1)*l3*sa1*sa2*sa3*vev2 - 6*ca1**2*ca3**2*complex(0,1)*l4*sa1*sa2*sa3*vev2 - 6*ca1**2*ca3**2*complex(0,1)*l5*sa1*sa2*sa3*vev2 + 3*ca3**2*complex(0,1)*l3*sa1**3*sa2*sa3*vev2 + 3*ca3**2*complex(0,1)*l4*sa1**3*sa2*sa3*vev2 + 3*ca3**2*complex(0,1)*l5*sa1**3*sa2*sa3*vev2 - 3*ca1*ca2**2*ca3*complex(0,1)*l8*sa3**2*vev2 - 3*ca1**3*ca3*complex(0,1)*l3*sa2**2*sa3**2*vev2 - 3*ca1**3*ca3*complex(0,1)*l4*sa2**2*sa3**2*vev2 - 3*ca1**3*ca3*complex(0,1)*l5*sa2**2*sa3**2*vev2 - 9*ca1*ca3*complex(0,1)*l2*sa1**2*sa2**2*sa3**2*vev2 + 6*ca1*ca3*complex(0,1)*l3*sa1**2*sa2**2*sa3**2*vev2 + 6*ca1*ca3*complex(0,1)*l4*sa1**2*sa2**2*sa3**2*vev2 + 6*ca1*ca3*complex(0,1)*l5*sa1**2*sa2**2*sa3**2*vev2 + 3*ca2**2*complex(0,1)*l8*sa1*sa2*sa3**3*vev2 + 3*ca1**2*complex(0,1)*l3*sa1*sa2**3*sa3**3*vev2 + 3*ca1**2*complex(0,1)*l4*sa1*sa2**3*sa3**3*vev2 + 3*ca1**2*complex(0,1)*l5*sa1*sa2**3*sa3**3*vev2 + 3*complex(0,1)*l2*sa1**3*sa2**3*sa3**3*vev2 - 3*ca1**2*ca2*ca3**2*complex(0,1)*l8*sa3*vevs - 3*ca2*ca3**2*complex(0,1)*l7*sa1**2*sa3*vevs - 6*ca1*ca2*ca3*complex(0,1)*l7*sa1*sa2*sa3**2*vevs + 6*ca1*ca2*ca3*complex(0,1)*l8*sa1*sa2*sa3**2*vevs - 3*ca2**3*complex(0,1)*l6*sa3**3*vevs - 3*ca1**2*ca2*complex(0,1)*l7*sa2**2*sa3**3*vevs - 3*ca2*complex(0,1)*l8*sa1**2*sa2**2*sa3**3*vevs',
                  order = {'QED':1})

GC_507 = Coupling(name = 'GC_507',
                  value = '(ca3*cb**2*complex(0,1)*l4*sa1*sa2*vev1)/2. + (ca3*cb**2*complex(0,1)*l5*sa1*sa2*vev1)/2. + (ca1*cb**2*complex(0,1)*l4*sa3*vev1)/2. + (ca1*cb**2*complex(0,1)*l5*sa3*vev1)/2. - ca1*ca3*cb*complex(0,1)*l1*sa2*sb*vev1 + ca1*ca3*cb*complex(0,1)*l3*sa2*sb*vev1 + cb*complex(0,1)*l1*sa1*sa3*sb*vev1 - cb*complex(0,1)*l3*sa1*sa3*sb*vev1 - (ca3*complex(0,1)*l4*sa1*sa2*sb**2*vev1)/2. - (ca3*complex(0,1)*l5*sa1*sa2*sb**2*vev1)/2. - (ca1*complex(0,1)*l4*sa3*sb**2*vev1)/2. - (ca1*complex(0,1)*l5*sa3*sb**2*vev1)/2. + (ca1*ca3*cb**2*complex(0,1)*l4*sa2*vev2)/2. + (ca1*ca3*cb**2*complex(0,1)*l5*sa2*vev2)/2. - (cb**2*complex(0,1)*l4*sa1*sa3*vev2)/2. - (cb**2*complex(0,1)*l5*sa1*sa3*vev2)/2. + ca3*cb*complex(0,1)*l2*sa1*sa2*sb*vev2 - ca3*cb*complex(0,1)*l3*sa1*sa2*sb*vev2 + ca1*cb*complex(0,1)*l2*sa3*sb*vev2 - ca1*cb*complex(0,1)*l3*sa3*sb*vev2 - (ca1*ca3*complex(0,1)*l4*sa2*sb**2*vev2)/2. - (ca1*ca3*complex(0,1)*l5*sa2*sb**2*vev2)/2. + (complex(0,1)*l4*sa1*sa3*sb**2*vev2)/2. + (complex(0,1)*l5*sa1*sa3*sb**2*vev2)/2. + ca2*ca3*cb*complex(0,1)*l7*sb*vevs - ca2*ca3*cb*complex(0,1)*l8*sb*vevs',
                  order = {'QED':1})

GC_508 = Coupling(name = 'GC_508',
                  value = 'ca3*cb**2*complex(0,1)*l5*sa1*sa2*vev1 + ca1*cb**2*complex(0,1)*l5*sa3*vev1 - ca1*ca3*cb*complex(0,1)*l1*sa2*sb*vev1 + ca1*ca3*cb*complex(0,1)*l3*sa2*sb*vev1 + ca1*ca3*cb*complex(0,1)*l4*sa2*sb*vev1 - ca1*ca3*cb*complex(0,1)*l5*sa2*sb*vev1 + cb*complex(0,1)*l1*sa1*sa3*sb*vev1 - cb*complex(0,1)*l3*sa1*sa3*sb*vev1 - cb*complex(0,1)*l4*sa1*sa3*sb*vev1 + cb*complex(0,1)*l5*sa1*sa3*sb*vev1 - ca3*complex(0,1)*l5*sa1*sa2*sb**2*vev1 - ca1*complex(0,1)*l5*sa3*sb**2*vev1 + ca1*ca3*cb**2*complex(0,1)*l5*sa2*vev2 - cb**2*complex(0,1)*l5*sa1*sa3*vev2 + ca3*cb*complex(0,1)*l2*sa1*sa2*sb*vev2 - ca3*cb*complex(0,1)*l3*sa1*sa2*sb*vev2 - ca3*cb*complex(0,1)*l4*sa1*sa2*sb*vev2 + ca3*cb*complex(0,1)*l5*sa1*sa2*sb*vev2 + ca1*cb*complex(0,1)*l2*sa3*sb*vev2 - ca1*cb*complex(0,1)*l3*sa3*sb*vev2 - ca1*cb*complex(0,1)*l4*sa3*sb*vev2 + ca1*cb*complex(0,1)*l5*sa3*sb*vev2 - ca1*ca3*complex(0,1)*l5*sa2*sb**2*vev2 + complex(0,1)*l5*sa1*sa3*sb**2*vev2 + ca2*ca3*cb*complex(0,1)*l7*sb*vevs - ca2*ca3*cb*complex(0,1)*l8*sb*vevs',
                  order = {'QED':1})

GC_509 = Coupling(name = 'GC_509',
                  value = '(ca3*cb**2*complex(0,1)*l4*sa1*sa2*vev1)/2. + (ca3*cb**2*complex(0,1)*l5*sa1*sa2*vev1)/2. + (ca1*cb**2*complex(0,1)*l4*sa3*vev1)/2. + (ca1*cb**2*complex(0,1)*l5*sa3*vev1)/2. - ca1*ca3*cb*complex(0,1)*l1*sa2*sb*vev1 + ca1*ca3*cb*complex(0,1)*l3*sa2*sb*vev1 + cb*complex(0,1)*l1*sa1*sa3*sb*vev1 - cb*complex(0,1)*l3*sa1*sa3*sb*vev1 - (ca3*complex(0,1)*l4*sa1*sa2*sb**2*vev1)/2. - (ca3*complex(0,1)*l5*sa1*sa2*sb**2*vev1)/2. - (ca1*complex(0,1)*l4*sa3*sb**2*vev1)/2. - (ca1*complex(0,1)*l5*sa3*sb**2*vev1)/2. + (ca1*ca3*cb*ee**2*complex(0,1)*sa2*sb*vev1)/(4.*sw**2) - (cb*ee**2*complex(0,1)*sa1*sa3*sb*vev1)/(4.*sw**2) + (ca3*ee**2*complex(0,1)*sa1*sa2*sb**2*vev1)/(4.*sw**2) + (ca1*ee**2*complex(0,1)*sa3*sb**2*vev1)/(4.*sw**2) + (ca1*ca3*cb**2*complex(0,1)*l4*sa2*vev2)/2. + (ca1*ca3*cb**2*complex(0,1)*l5*sa2*vev2)/2. - (cb**2*complex(0,1)*l4*sa1*sa3*vev2)/2. - (cb**2*complex(0,1)*l5*sa1*sa3*vev2)/2. + ca3*cb*complex(0,1)*l2*sa1*sa2*sb*vev2 - ca3*cb*complex(0,1)*l3*sa1*sa2*sb*vev2 + ca1*cb*complex(0,1)*l2*sa3*sb*vev2 - ca1*cb*complex(0,1)*l3*sa3*sb*vev2 - (ca1*ca3*complex(0,1)*l4*sa2*sb**2*vev2)/2. - (ca1*ca3*complex(0,1)*l5*sa2*sb**2*vev2)/2. + (complex(0,1)*l4*sa1*sa3*sb**2*vev2)/2. + (complex(0,1)*l5*sa1*sa3*sb**2*vev2)/2. - (ca1*ca3*cb**2*ee**2*complex(0,1)*sa2*vev2)/(4.*sw**2) + (cb**2*ee**2*complex(0,1)*sa1*sa3*vev2)/(4.*sw**2) - (ca3*cb*ee**2*complex(0,1)*sa1*sa2*sb*vev2)/(4.*sw**2) - (ca1*cb*ee**2*complex(0,1)*sa3*sb*vev2)/(4.*sw**2) + ca2*ca3*cb*complex(0,1)*l7*sb*vevs - ca2*ca3*cb*complex(0,1)*l8*sb*vevs',
                  order = {'QED':1})

GC_510 = Coupling(name = 'GC_510',
                  value = 'ca3*cb**2*complex(0,1)*l5*sa1*sa2*vev1 + ca1*cb**2*complex(0,1)*l5*sa3*vev1 + (ca1*ca3*cb*ee**2*complex(0,1)*sa2*sb*vev1)/(4.*cw**2) - ca1*ca3*cb*complex(0,1)*l1*sa2*sb*vev1 + ca1*ca3*cb*complex(0,1)*l3*sa2*sb*vev1 + ca1*ca3*cb*complex(0,1)*l4*sa2*sb*vev1 - ca1*ca3*cb*complex(0,1)*l5*sa2*sb*vev1 - (cb*ee**2*complex(0,1)*sa1*sa3*sb*vev1)/(4.*cw**2) + cb*complex(0,1)*l1*sa1*sa3*sb*vev1 - cb*complex(0,1)*l3*sa1*sa3*sb*vev1 - cb*complex(0,1)*l4*sa1*sa3*sb*vev1 + cb*complex(0,1)*l5*sa1*sa3*sb*vev1 + (ca3*ee**2*complex(0,1)*sa1*sa2*sb**2*vev1)/(4.*cw**2) - ca3*complex(0,1)*l5*sa1*sa2*sb**2*vev1 + (ca1*ee**2*complex(0,1)*sa3*sb**2*vev1)/(4.*cw**2) - ca1*complex(0,1)*l5*sa3*sb**2*vev1 + (ca1*ca3*cb*ee**2*complex(0,1)*sa2*sb*vev1)/(4.*sw**2) - (cb*ee**2*complex(0,1)*sa1*sa3*sb*vev1)/(4.*sw**2) + (ca3*ee**2*complex(0,1)*sa1*sa2*sb**2*vev1)/(4.*sw**2) + (ca1*ee**2*complex(0,1)*sa3*sb**2*vev1)/(4.*sw**2) - (ca1*ca3*cb**2*ee**2*complex(0,1)*sa2*vev2)/(4.*cw**2) + ca1*ca3*cb**2*complex(0,1)*l5*sa2*vev2 + (cb**2*ee**2*complex(0,1)*sa1*sa3*vev2)/(4.*cw**2) - cb**2*complex(0,1)*l5*sa1*sa3*vev2 - (ca3*cb*ee**2*complex(0,1)*sa1*sa2*sb*vev2)/(4.*cw**2) + ca3*cb*complex(0,1)*l2*sa1*sa2*sb*vev2 - ca3*cb*complex(0,1)*l3*sa1*sa2*sb*vev2 - ca3*cb*complex(0,1)*l4*sa1*sa2*sb*vev2 + ca3*cb*complex(0,1)*l5*sa1*sa2*sb*vev2 - (ca1*cb*ee**2*complex(0,1)*sa3*sb*vev2)/(4.*cw**2) + ca1*cb*complex(0,1)*l2*sa3*sb*vev2 - ca1*cb*complex(0,1)*l3*sa3*sb*vev2 - ca1*cb*complex(0,1)*l4*sa3*sb*vev2 + ca1*cb*complex(0,1)*l5*sa3*sb*vev2 - ca1*ca3*complex(0,1)*l5*sa2*sb**2*vev2 + complex(0,1)*l5*sa1*sa3*sb**2*vev2 - (ca1*ca3*cb**2*ee**2*complex(0,1)*sa2*vev2)/(4.*sw**2) + (cb**2*ee**2*complex(0,1)*sa1*sa3*vev2)/(4.*sw**2) - (ca3*cb*ee**2*complex(0,1)*sa1*sa2*sb*vev2)/(4.*sw**2) - (ca1*cb*ee**2*complex(0,1)*sa3*sb*vev2)/(4.*sw**2) + ca2*ca3*cb*complex(0,1)*l7*sb*vevs - ca2*ca3*cb*complex(0,1)*l8*sb*vevs',
                  order = {'QED':1})

GC_511 = Coupling(name = 'GC_511',
                  value = '(ca3*cb**2*complex(0,1)*l4*sa1*sa2*vev1)/2. + (ca3*cb**2*complex(0,1)*l5*sa1*sa2*vev1)/2. + (ca1*cb**2*complex(0,1)*l4*sa3*vev1)/2. + (ca1*cb**2*complex(0,1)*l5*sa3*vev1)/2. - ca1*ca3*cb*complex(0,1)*l1*sa2*sb*vev1 + ca1*ca3*cb*complex(0,1)*l3*sa2*sb*vev1 + cb*complex(0,1)*l1*sa1*sa3*sb*vev1 - cb*complex(0,1)*l3*sa1*sa3*sb*vev1 - (ca3*complex(0,1)*l4*sa1*sa2*sb**2*vev1)/2. - (ca3*complex(0,1)*l5*sa1*sa2*sb**2*vev1)/2. - (ca1*complex(0,1)*l4*sa3*sb**2*vev1)/2. - (ca1*complex(0,1)*l5*sa3*sb**2*vev1)/2. - (ca3*cb**2*ee**2*complex(0,1)*sa1*sa2*vev1)/(4.*sw**2) - (ca1*cb**2*ee**2*complex(0,1)*sa3*vev1)/(4.*sw**2) + (ca1*ca3*cb*ee**2*complex(0,1)*sa2*sb*vev1)/(4.*sw**2) - (cb*ee**2*complex(0,1)*sa1*sa3*sb*vev1)/(4.*sw**2) + (ca1*ca3*cb**2*complex(0,1)*l4*sa2*vev2)/2. + (ca1*ca3*cb**2*complex(0,1)*l5*sa2*vev2)/2. - (cb**2*complex(0,1)*l4*sa1*sa3*vev2)/2. - (cb**2*complex(0,1)*l5*sa1*sa3*vev2)/2. + ca3*cb*complex(0,1)*l2*sa1*sa2*sb*vev2 - ca3*cb*complex(0,1)*l3*sa1*sa2*sb*vev2 + ca1*cb*complex(0,1)*l2*sa3*sb*vev2 - ca1*cb*complex(0,1)*l3*sa3*sb*vev2 - (ca1*ca3*complex(0,1)*l4*sa2*sb**2*vev2)/2. - (ca1*ca3*complex(0,1)*l5*sa2*sb**2*vev2)/2. + (complex(0,1)*l4*sa1*sa3*sb**2*vev2)/2. + (complex(0,1)*l5*sa1*sa3*sb**2*vev2)/2. - (ca3*cb*ee**2*complex(0,1)*sa1*sa2*sb*vev2)/(4.*sw**2) - (ca1*cb*ee**2*complex(0,1)*sa3*sb*vev2)/(4.*sw**2) + (ca1*ca3*ee**2*complex(0,1)*sa2*sb**2*vev2)/(4.*sw**2) - (ee**2*complex(0,1)*sa1*sa3*sb**2*vev2)/(4.*sw**2) + ca2*ca3*cb*complex(0,1)*l7*sb*vevs - ca2*ca3*cb*complex(0,1)*l8*sb*vevs',
                  order = {'QED':1})

GC_512 = Coupling(name = 'GC_512',
                  value = '-(ca3*cb**2*ee**2*complex(0,1)*sa1*sa2*vev1)/(4.*cw**2) + ca3*cb**2*complex(0,1)*l5*sa1*sa2*vev1 - (ca1*cb**2*ee**2*complex(0,1)*sa3*vev1)/(4.*cw**2) + ca1*cb**2*complex(0,1)*l5*sa3*vev1 + (ca1*ca3*cb*ee**2*complex(0,1)*sa2*sb*vev1)/(4.*cw**2) - ca1*ca3*cb*complex(0,1)*l1*sa2*sb*vev1 + ca1*ca3*cb*complex(0,1)*l3*sa2*sb*vev1 + ca1*ca3*cb*complex(0,1)*l4*sa2*sb*vev1 - ca1*ca3*cb*complex(0,1)*l5*sa2*sb*vev1 - (cb*ee**2*complex(0,1)*sa1*sa3*sb*vev1)/(4.*cw**2) + cb*complex(0,1)*l1*sa1*sa3*sb*vev1 - cb*complex(0,1)*l3*sa1*sa3*sb*vev1 - cb*complex(0,1)*l4*sa1*sa3*sb*vev1 + cb*complex(0,1)*l5*sa1*sa3*sb*vev1 - ca3*complex(0,1)*l5*sa1*sa2*sb**2*vev1 - ca1*complex(0,1)*l5*sa3*sb**2*vev1 - (ca3*cb**2*ee**2*complex(0,1)*sa1*sa2*vev1)/(4.*sw**2) - (ca1*cb**2*ee**2*complex(0,1)*sa3*vev1)/(4.*sw**2) + (ca1*ca3*cb*ee**2*complex(0,1)*sa2*sb*vev1)/(4.*sw**2) - (cb*ee**2*complex(0,1)*sa1*sa3*sb*vev1)/(4.*sw**2) + ca1*ca3*cb**2*complex(0,1)*l5*sa2*vev2 - cb**2*complex(0,1)*l5*sa1*sa3*vev2 - (ca3*cb*ee**2*complex(0,1)*sa1*sa2*sb*vev2)/(4.*cw**2) + ca3*cb*complex(0,1)*l2*sa1*sa2*sb*vev2 - ca3*cb*complex(0,1)*l3*sa1*sa2*sb*vev2 - ca3*cb*complex(0,1)*l4*sa1*sa2*sb*vev2 + ca3*cb*complex(0,1)*l5*sa1*sa2*sb*vev2 - (ca1*cb*ee**2*complex(0,1)*sa3*sb*vev2)/(4.*cw**2) + ca1*cb*complex(0,1)*l2*sa3*sb*vev2 - ca1*cb*complex(0,1)*l3*sa3*sb*vev2 - ca1*cb*complex(0,1)*l4*sa3*sb*vev2 + ca1*cb*complex(0,1)*l5*sa3*sb*vev2 + (ca1*ca3*ee**2*complex(0,1)*sa2*sb**2*vev2)/(4.*cw**2) - ca1*ca3*complex(0,1)*l5*sa2*sb**2*vev2 - (ee**2*complex(0,1)*sa1*sa3*sb**2*vev2)/(4.*cw**2) + complex(0,1)*l5*sa1*sa3*sb**2*vev2 - (ca3*cb*ee**2*complex(0,1)*sa1*sa2*sb*vev2)/(4.*sw**2) - (ca1*cb*ee**2*complex(0,1)*sa3*sb*vev2)/(4.*sw**2) + (ca1*ca3*ee**2*complex(0,1)*sa2*sb**2*vev2)/(4.*sw**2) - (ee**2*complex(0,1)*sa1*sa3*sb**2*vev2)/(4.*sw**2) + ca2*ca3*cb*complex(0,1)*l7*sb*vevs - ca2*ca3*cb*complex(0,1)*l8*sb*vevs',
                  order = {'QED':1})

GC_513 = Coupling(name = 'GC_513',
                  value = '(ca3*cb**2*complex(0,1)*l4*sa1*sa2*vev1)/2. + (ca3*cb**2*complex(0,1)*l5*sa1*sa2*vev1)/2. + (ca1*cb**2*complex(0,1)*l4*sa3*vev1)/2. + (ca1*cb**2*complex(0,1)*l5*sa3*vev1)/2. - ca1*ca3*cb*complex(0,1)*l1*sa2*sb*vev1 + ca1*ca3*cb*complex(0,1)*l3*sa2*sb*vev1 + cb*complex(0,1)*l1*sa1*sa3*sb*vev1 - cb*complex(0,1)*l3*sa1*sa3*sb*vev1 - (ca3*complex(0,1)*l4*sa1*sa2*sb**2*vev1)/2. - (ca3*complex(0,1)*l5*sa1*sa2*sb**2*vev1)/2. - (ca1*complex(0,1)*l4*sa3*sb**2*vev1)/2. - (ca1*complex(0,1)*l5*sa3*sb**2*vev1)/2. + (ca3*cb**2*ee**2*complex(0,1)*sa1*sa2*vev1)/(4.*sw**2) + (ca1*cb**2*ee**2*complex(0,1)*sa3*vev1)/(4.*sw**2) - (ca1*ca3*cb*ee**2*complex(0,1)*sa2*sb*vev1)/(2.*sw**2) + (cb*ee**2*complex(0,1)*sa1*sa3*sb*vev1)/(2.*sw**2) - (ca3*ee**2*complex(0,1)*sa1*sa2*sb**2*vev1)/(4.*sw**2) - (ca1*ee**2*complex(0,1)*sa3*sb**2*vev1)/(4.*sw**2) + (ca1*ca3*cb**2*complex(0,1)*l4*sa2*vev2)/2. + (ca1*ca3*cb**2*complex(0,1)*l5*sa2*vev2)/2. - (cb**2*complex(0,1)*l4*sa1*sa3*vev2)/2. - (cb**2*complex(0,1)*l5*sa1*sa3*vev2)/2. + ca3*cb*complex(0,1)*l2*sa1*sa2*sb*vev2 - ca3*cb*complex(0,1)*l3*sa1*sa2*sb*vev2 + ca1*cb*complex(0,1)*l2*sa3*sb*vev2 - ca1*cb*complex(0,1)*l3*sa3*sb*vev2 - (ca1*ca3*complex(0,1)*l4*sa2*sb**2*vev2)/2. - (ca1*ca3*complex(0,1)*l5*sa2*sb**2*vev2)/2. + (complex(0,1)*l4*sa1*sa3*sb**2*vev2)/2. + (complex(0,1)*l5*sa1*sa3*sb**2*vev2)/2. + (ca1*ca3*cb**2*ee**2*complex(0,1)*sa2*vev2)/(4.*sw**2) - (cb**2*ee**2*complex(0,1)*sa1*sa3*vev2)/(4.*sw**2) + (ca3*cb*ee**2*complex(0,1)*sa1*sa2*sb*vev2)/(2.*sw**2) + (ca1*cb*ee**2*complex(0,1)*sa3*sb*vev2)/(2.*sw**2) - (ca1*ca3*ee**2*complex(0,1)*sa2*sb**2*vev2)/(4.*sw**2) + (ee**2*complex(0,1)*sa1*sa3*sb**2*vev2)/(4.*sw**2) + ca2*ca3*cb*complex(0,1)*l7*sb*vevs - ca2*ca3*cb*complex(0,1)*l8*sb*vevs',
                  order = {'QED':1})

GC_514 = Coupling(name = 'GC_514',
                  value = '(ca3*cb**2*ee**2*complex(0,1)*sa1*sa2*vev1)/(4.*cw**2) + ca3*cb**2*complex(0,1)*l5*sa1*sa2*vev1 + (ca1*cb**2*ee**2*complex(0,1)*sa3*vev1)/(4.*cw**2) + ca1*cb**2*complex(0,1)*l5*sa3*vev1 - (ca1*ca3*cb*ee**2*complex(0,1)*sa2*sb*vev1)/(2.*cw**2) - ca1*ca3*cb*complex(0,1)*l1*sa2*sb*vev1 + ca1*ca3*cb*complex(0,1)*l3*sa2*sb*vev1 + ca1*ca3*cb*complex(0,1)*l4*sa2*sb*vev1 - ca1*ca3*cb*complex(0,1)*l5*sa2*sb*vev1 + (cb*ee**2*complex(0,1)*sa1*sa3*sb*vev1)/(2.*cw**2) + cb*complex(0,1)*l1*sa1*sa3*sb*vev1 - cb*complex(0,1)*l3*sa1*sa3*sb*vev1 - cb*complex(0,1)*l4*sa1*sa3*sb*vev1 + cb*complex(0,1)*l5*sa1*sa3*sb*vev1 - (ca3*ee**2*complex(0,1)*sa1*sa2*sb**2*vev1)/(4.*cw**2) - ca3*complex(0,1)*l5*sa1*sa2*sb**2*vev1 - (ca1*ee**2*complex(0,1)*sa3*sb**2*vev1)/(4.*cw**2) - ca1*complex(0,1)*l5*sa3*sb**2*vev1 + (ca3*cb**2*ee**2*complex(0,1)*sa1*sa2*vev1)/(4.*sw**2) + (ca1*cb**2*ee**2*complex(0,1)*sa3*vev1)/(4.*sw**2) - (ca1*ca3*cb*ee**2*complex(0,1)*sa2*sb*vev1)/(2.*sw**2) + (cb*ee**2*complex(0,1)*sa1*sa3*sb*vev1)/(2.*sw**2) - (ca3*ee**2*complex(0,1)*sa1*sa2*sb**2*vev1)/(4.*sw**2) - (ca1*ee**2*complex(0,1)*sa3*sb**2*vev1)/(4.*sw**2) + (ca1*ca3*cb**2*ee**2*complex(0,1)*sa2*vev2)/(4.*cw**2) + ca1*ca3*cb**2*complex(0,1)*l5*sa2*vev2 - (cb**2*ee**2*complex(0,1)*sa1*sa3*vev2)/(4.*cw**2) - cb**2*complex(0,1)*l5*sa1*sa3*vev2 + (ca3*cb*ee**2*complex(0,1)*sa1*sa2*sb*vev2)/(2.*cw**2) + ca3*cb*complex(0,1)*l2*sa1*sa2*sb*vev2 - ca3*cb*complex(0,1)*l3*sa1*sa2*sb*vev2 - ca3*cb*complex(0,1)*l4*sa1*sa2*sb*vev2 + ca3*cb*complex(0,1)*l5*sa1*sa2*sb*vev2 + (ca1*cb*ee**2*complex(0,1)*sa3*sb*vev2)/(2.*cw**2) + ca1*cb*complex(0,1)*l2*sa3*sb*vev2 - ca1*cb*complex(0,1)*l3*sa3*sb*vev2 - ca1*cb*complex(0,1)*l4*sa3*sb*vev2 + ca1*cb*complex(0,1)*l5*sa3*sb*vev2 - (ca1*ca3*ee**2*complex(0,1)*sa2*sb**2*vev2)/(4.*cw**2) - ca1*ca3*complex(0,1)*l5*sa2*sb**2*vev2 + (ee**2*complex(0,1)*sa1*sa3*sb**2*vev2)/(4.*cw**2) + complex(0,1)*l5*sa1*sa3*sb**2*vev2 + (ca1*ca3*cb**2*ee**2*complex(0,1)*sa2*vev2)/(4.*sw**2) - (cb**2*ee**2*complex(0,1)*sa1*sa3*vev2)/(4.*sw**2) + (ca3*cb*ee**2*complex(0,1)*sa1*sa2*sb*vev2)/(2.*sw**2) + (ca1*cb*ee**2*complex(0,1)*sa3*sb*vev2)/(2.*sw**2) - (ca1*ca3*ee**2*complex(0,1)*sa2*sb**2*vev2)/(4.*sw**2) + (ee**2*complex(0,1)*sa1*sa3*sb**2*vev2)/(4.*sw**2) + ca2*ca3*cb*complex(0,1)*l7*sb*vevs - ca2*ca3*cb*complex(0,1)*l8*sb*vevs',
                  order = {'QED':1})

GC_515 = Coupling(name = 'GC_515',
                  value = '-(ca2*cb**2*complex(0,1)*l4*sa1*vev1)/2. - (ca2*cb**2*complex(0,1)*l5*sa1*vev1)/2. + ca1*ca2*cb*complex(0,1)*l1*sb*vev1 - ca1*ca2*cb*complex(0,1)*l3*sb*vev1 + (ca2*complex(0,1)*l4*sa1*sb**2*vev1)/2. + (ca2*complex(0,1)*l5*sa1*sb**2*vev1)/2. - (ca1*ca2*cb**2*complex(0,1)*l4*vev2)/2. - (ca1*ca2*cb**2*complex(0,1)*l5*vev2)/2. - ca2*cb*complex(0,1)*l2*sa1*sb*vev2 + ca2*cb*complex(0,1)*l3*sa1*sb*vev2 + (ca1*ca2*complex(0,1)*l4*sb**2*vev2)/2. + (ca1*ca2*complex(0,1)*l5*sb**2*vev2)/2. + cb*complex(0,1)*l7*sa2*sb*vevs - cb*complex(0,1)*l8*sa2*sb*vevs',
                  order = {'QED':1})

GC_516 = Coupling(name = 'GC_516',
                  value = '-(ca2*cb**2*complex(0,1)*l5*sa1*vev1) + ca1*ca2*cb*complex(0,1)*l1*sb*vev1 - ca1*ca2*cb*complex(0,1)*l3*sb*vev1 - ca1*ca2*cb*complex(0,1)*l4*sb*vev1 + ca1*ca2*cb*complex(0,1)*l5*sb*vev1 + ca2*complex(0,1)*l5*sa1*sb**2*vev1 - ca1*ca2*cb**2*complex(0,1)*l5*vev2 - ca2*cb*complex(0,1)*l2*sa1*sb*vev2 + ca2*cb*complex(0,1)*l3*sa1*sb*vev2 + ca2*cb*complex(0,1)*l4*sa1*sb*vev2 - ca2*cb*complex(0,1)*l5*sa1*sb*vev2 + ca1*ca2*complex(0,1)*l5*sb**2*vev2 + cb*complex(0,1)*l7*sa2*sb*vevs - cb*complex(0,1)*l8*sa2*sb*vevs',
                  order = {'QED':1})

GC_517 = Coupling(name = 'GC_517',
                  value = '-(ca2*cb**2*complex(0,1)*l4*sa1*vev1)/2. - (ca2*cb**2*complex(0,1)*l5*sa1*vev1)/2. + ca1*ca2*cb*complex(0,1)*l1*sb*vev1 - ca1*ca2*cb*complex(0,1)*l3*sb*vev1 + (ca2*complex(0,1)*l4*sa1*sb**2*vev1)/2. + (ca2*complex(0,1)*l5*sa1*sb**2*vev1)/2. - (ca1*ca2*cb*ee**2*complex(0,1)*sb*vev1)/(4.*sw**2) - (ca2*ee**2*complex(0,1)*sa1*sb**2*vev1)/(4.*sw**2) - (ca1*ca2*cb**2*complex(0,1)*l4*vev2)/2. - (ca1*ca2*cb**2*complex(0,1)*l5*vev2)/2. - ca2*cb*complex(0,1)*l2*sa1*sb*vev2 + ca2*cb*complex(0,1)*l3*sa1*sb*vev2 + (ca1*ca2*complex(0,1)*l4*sb**2*vev2)/2. + (ca1*ca2*complex(0,1)*l5*sb**2*vev2)/2. + (ca1*ca2*cb**2*ee**2*complex(0,1)*vev2)/(4.*sw**2) + (ca2*cb*ee**2*complex(0,1)*sa1*sb*vev2)/(4.*sw**2) + cb*complex(0,1)*l7*sa2*sb*vevs - cb*complex(0,1)*l8*sa2*sb*vevs',
                  order = {'QED':1})

GC_518 = Coupling(name = 'GC_518',
                  value = '-(ca2*cb**2*complex(0,1)*l5*sa1*vev1) - (ca1*ca2*cb*ee**2*complex(0,1)*sb*vev1)/(4.*cw**2) + ca1*ca2*cb*complex(0,1)*l1*sb*vev1 - ca1*ca2*cb*complex(0,1)*l3*sb*vev1 - ca1*ca2*cb*complex(0,1)*l4*sb*vev1 + ca1*ca2*cb*complex(0,1)*l5*sb*vev1 - (ca2*ee**2*complex(0,1)*sa1*sb**2*vev1)/(4.*cw**2) + ca2*complex(0,1)*l5*sa1*sb**2*vev1 - (ca1*ca2*cb*ee**2*complex(0,1)*sb*vev1)/(4.*sw**2) - (ca2*ee**2*complex(0,1)*sa1*sb**2*vev1)/(4.*sw**2) + (ca1*ca2*cb**2*ee**2*complex(0,1)*vev2)/(4.*cw**2) - ca1*ca2*cb**2*complex(0,1)*l5*vev2 + (ca2*cb*ee**2*complex(0,1)*sa1*sb*vev2)/(4.*cw**2) - ca2*cb*complex(0,1)*l2*sa1*sb*vev2 + ca2*cb*complex(0,1)*l3*sa1*sb*vev2 + ca2*cb*complex(0,1)*l4*sa1*sb*vev2 - ca2*cb*complex(0,1)*l5*sa1*sb*vev2 + ca1*ca2*complex(0,1)*l5*sb**2*vev2 + (ca1*ca2*cb**2*ee**2*complex(0,1)*vev2)/(4.*sw**2) + (ca2*cb*ee**2*complex(0,1)*sa1*sb*vev2)/(4.*sw**2) + cb*complex(0,1)*l7*sa2*sb*vevs - cb*complex(0,1)*l8*sa2*sb*vevs',
                  order = {'QED':1})

GC_519 = Coupling(name = 'GC_519',
                  value = '-(ca2*cb**2*complex(0,1)*l4*sa1*vev1)/2. - (ca2*cb**2*complex(0,1)*l5*sa1*vev1)/2. + ca1*ca2*cb*complex(0,1)*l1*sb*vev1 - ca1*ca2*cb*complex(0,1)*l3*sb*vev1 + (ca2*complex(0,1)*l4*sa1*sb**2*vev1)/2. + (ca2*complex(0,1)*l5*sa1*sb**2*vev1)/2. + (ca2*cb**2*ee**2*complex(0,1)*sa1*vev1)/(4.*sw**2) - (ca1*ca2*cb*ee**2*complex(0,1)*sb*vev1)/(4.*sw**2) - (ca1*ca2*cb**2*complex(0,1)*l4*vev2)/2. - (ca1*ca2*cb**2*complex(0,1)*l5*vev2)/2. - ca2*cb*complex(0,1)*l2*sa1*sb*vev2 + ca2*cb*complex(0,1)*l3*sa1*sb*vev2 + (ca1*ca2*complex(0,1)*l4*sb**2*vev2)/2. + (ca1*ca2*complex(0,1)*l5*sb**2*vev2)/2. + (ca2*cb*ee**2*complex(0,1)*sa1*sb*vev2)/(4.*sw**2) - (ca1*ca2*ee**2*complex(0,1)*sb**2*vev2)/(4.*sw**2) + cb*complex(0,1)*l7*sa2*sb*vevs - cb*complex(0,1)*l8*sa2*sb*vevs',
                  order = {'QED':1})

GC_520 = Coupling(name = 'GC_520',
                  value = '(ca2*cb**2*ee**2*complex(0,1)*sa1*vev1)/(4.*cw**2) - ca2*cb**2*complex(0,1)*l5*sa1*vev1 - (ca1*ca2*cb*ee**2*complex(0,1)*sb*vev1)/(4.*cw**2) + ca1*ca2*cb*complex(0,1)*l1*sb*vev1 - ca1*ca2*cb*complex(0,1)*l3*sb*vev1 - ca1*ca2*cb*complex(0,1)*l4*sb*vev1 + ca1*ca2*cb*complex(0,1)*l5*sb*vev1 + ca2*complex(0,1)*l5*sa1*sb**2*vev1 + (ca2*cb**2*ee**2*complex(0,1)*sa1*vev1)/(4.*sw**2) - (ca1*ca2*cb*ee**2*complex(0,1)*sb*vev1)/(4.*sw**2) - ca1*ca2*cb**2*complex(0,1)*l5*vev2 + (ca2*cb*ee**2*complex(0,1)*sa1*sb*vev2)/(4.*cw**2) - ca2*cb*complex(0,1)*l2*sa1*sb*vev2 + ca2*cb*complex(0,1)*l3*sa1*sb*vev2 + ca2*cb*complex(0,1)*l4*sa1*sb*vev2 - ca2*cb*complex(0,1)*l5*sa1*sb*vev2 - (ca1*ca2*ee**2*complex(0,1)*sb**2*vev2)/(4.*cw**2) + ca1*ca2*complex(0,1)*l5*sb**2*vev2 + (ca2*cb*ee**2*complex(0,1)*sa1*sb*vev2)/(4.*sw**2) - (ca1*ca2*ee**2*complex(0,1)*sb**2*vev2)/(4.*sw**2) + cb*complex(0,1)*l7*sa2*sb*vevs - cb*complex(0,1)*l8*sa2*sb*vevs',
                  order = {'QED':1})

GC_521 = Coupling(name = 'GC_521',
                  value = '-(ca2*cb**2*complex(0,1)*l4*sa1*vev1)/2. - (ca2*cb**2*complex(0,1)*l5*sa1*vev1)/2. + ca1*ca2*cb*complex(0,1)*l1*sb*vev1 - ca1*ca2*cb*complex(0,1)*l3*sb*vev1 + (ca2*complex(0,1)*l4*sa1*sb**2*vev1)/2. + (ca2*complex(0,1)*l5*sa1*sb**2*vev1)/2. - (ca2*cb**2*ee**2*complex(0,1)*sa1*vev1)/(4.*sw**2) + (ca1*ca2*cb*ee**2*complex(0,1)*sb*vev1)/(2.*sw**2) + (ca2*ee**2*complex(0,1)*sa1*sb**2*vev1)/(4.*sw**2) - (ca1*ca2*cb**2*complex(0,1)*l4*vev2)/2. - (ca1*ca2*cb**2*complex(0,1)*l5*vev2)/2. - ca2*cb*complex(0,1)*l2*sa1*sb*vev2 + ca2*cb*complex(0,1)*l3*sa1*sb*vev2 + (ca1*ca2*complex(0,1)*l4*sb**2*vev2)/2. + (ca1*ca2*complex(0,1)*l5*sb**2*vev2)/2. - (ca1*ca2*cb**2*ee**2*complex(0,1)*vev2)/(4.*sw**2) - (ca2*cb*ee**2*complex(0,1)*sa1*sb*vev2)/(2.*sw**2) + (ca1*ca2*ee**2*complex(0,1)*sb**2*vev2)/(4.*sw**2) + cb*complex(0,1)*l7*sa2*sb*vevs - cb*complex(0,1)*l8*sa2*sb*vevs',
                  order = {'QED':1})

GC_522 = Coupling(name = 'GC_522',
                  value = '-(ca2*cb**2*ee**2*complex(0,1)*sa1*vev1)/(4.*cw**2) - ca2*cb**2*complex(0,1)*l5*sa1*vev1 + (ca1*ca2*cb*ee**2*complex(0,1)*sb*vev1)/(2.*cw**2) + ca1*ca2*cb*complex(0,1)*l1*sb*vev1 - ca1*ca2*cb*complex(0,1)*l3*sb*vev1 - ca1*ca2*cb*complex(0,1)*l4*sb*vev1 + ca1*ca2*cb*complex(0,1)*l5*sb*vev1 + (ca2*ee**2*complex(0,1)*sa1*sb**2*vev1)/(4.*cw**2) + ca2*complex(0,1)*l5*sa1*sb**2*vev1 - (ca2*cb**2*ee**2*complex(0,1)*sa1*vev1)/(4.*sw**2) + (ca1*ca2*cb*ee**2*complex(0,1)*sb*vev1)/(2.*sw**2) + (ca2*ee**2*complex(0,1)*sa1*sb**2*vev1)/(4.*sw**2) - (ca1*ca2*cb**2*ee**2*complex(0,1)*vev2)/(4.*cw**2) - ca1*ca2*cb**2*complex(0,1)*l5*vev2 - (ca2*cb*ee**2*complex(0,1)*sa1*sb*vev2)/(2.*cw**2) - ca2*cb*complex(0,1)*l2*sa1*sb*vev2 + ca2*cb*complex(0,1)*l3*sa1*sb*vev2 + ca2*cb*complex(0,1)*l4*sa1*sb*vev2 - ca2*cb*complex(0,1)*l5*sa1*sb*vev2 + (ca1*ca2*ee**2*complex(0,1)*sb**2*vev2)/(4.*cw**2) + ca1*ca2*complex(0,1)*l5*sb**2*vev2 - (ca1*ca2*cb**2*ee**2*complex(0,1)*vev2)/(4.*sw**2) - (ca2*cb*ee**2*complex(0,1)*sa1*sb*vev2)/(2.*sw**2) + (ca1*ca2*ee**2*complex(0,1)*sb**2*vev2)/(4.*sw**2) + cb*complex(0,1)*l7*sa2*sb*vevs - cb*complex(0,1)*l8*sa2*sb*vevs',
                  order = {'QED':1})

GC_523 = Coupling(name = 'GC_523',
                  value = '-(ca1*ca3*cb**2*complex(0,1)*l4*vev1)/2. - (ca1*ca3*cb**2*complex(0,1)*l5*vev1)/2. + (cb**2*complex(0,1)*l4*sa1*sa2*sa3*vev1)/2. + (cb**2*complex(0,1)*l5*sa1*sa2*sa3*vev1)/2. - ca3*cb*complex(0,1)*l1*sa1*sb*vev1 + ca3*cb*complex(0,1)*l3*sa1*sb*vev1 - ca1*cb*complex(0,1)*l1*sa2*sa3*sb*vev1 + ca1*cb*complex(0,1)*l3*sa2*sa3*sb*vev1 + (ca1*ca3*complex(0,1)*l4*sb**2*vev1)/2. + (ca1*ca3*complex(0,1)*l5*sb**2*vev1)/2. - (complex(0,1)*l4*sa1*sa2*sa3*sb**2*vev1)/2. - (complex(0,1)*l5*sa1*sa2*sa3*sb**2*vev1)/2. + (ca3*cb**2*complex(0,1)*l4*sa1*vev2)/2. + (ca3*cb**2*complex(0,1)*l5*sa1*vev2)/2. + (ca1*cb**2*complex(0,1)*l4*sa2*sa3*vev2)/2. + (ca1*cb**2*complex(0,1)*l5*sa2*sa3*vev2)/2. - ca1*ca3*cb*complex(0,1)*l2*sb*vev2 + ca1*ca3*cb*complex(0,1)*l3*sb*vev2 + cb*complex(0,1)*l2*sa1*sa2*sa3*sb*vev2 - cb*complex(0,1)*l3*sa1*sa2*sa3*sb*vev2 - (ca3*complex(0,1)*l4*sa1*sb**2*vev2)/2. - (ca3*complex(0,1)*l5*sa1*sb**2*vev2)/2. - (ca1*complex(0,1)*l4*sa2*sa3*sb**2*vev2)/2. - (ca1*complex(0,1)*l5*sa2*sa3*sb**2*vev2)/2. + ca2*cb*complex(0,1)*l7*sa3*sb*vevs - ca2*cb*complex(0,1)*l8*sa3*sb*vevs',
                  order = {'QED':1})

GC_524 = Coupling(name = 'GC_524',
                  value = '-(ca1*ca3*cb**2*complex(0,1)*l5*vev1) + cb**2*complex(0,1)*l5*sa1*sa2*sa3*vev1 - ca3*cb*complex(0,1)*l1*sa1*sb*vev1 + ca3*cb*complex(0,1)*l3*sa1*sb*vev1 + ca3*cb*complex(0,1)*l4*sa1*sb*vev1 - ca3*cb*complex(0,1)*l5*sa1*sb*vev1 - ca1*cb*complex(0,1)*l1*sa2*sa3*sb*vev1 + ca1*cb*complex(0,1)*l3*sa2*sa3*sb*vev1 + ca1*cb*complex(0,1)*l4*sa2*sa3*sb*vev1 - ca1*cb*complex(0,1)*l5*sa2*sa3*sb*vev1 + ca1*ca3*complex(0,1)*l5*sb**2*vev1 - complex(0,1)*l5*sa1*sa2*sa3*sb**2*vev1 + ca3*cb**2*complex(0,1)*l5*sa1*vev2 + ca1*cb**2*complex(0,1)*l5*sa2*sa3*vev2 - ca1*ca3*cb*complex(0,1)*l2*sb*vev2 + ca1*ca3*cb*complex(0,1)*l3*sb*vev2 + ca1*ca3*cb*complex(0,1)*l4*sb*vev2 - ca1*ca3*cb*complex(0,1)*l5*sb*vev2 + cb*complex(0,1)*l2*sa1*sa2*sa3*sb*vev2 - cb*complex(0,1)*l3*sa1*sa2*sa3*sb*vev2 - cb*complex(0,1)*l4*sa1*sa2*sa3*sb*vev2 + cb*complex(0,1)*l5*sa1*sa2*sa3*sb*vev2 - ca3*complex(0,1)*l5*sa1*sb**2*vev2 - ca1*complex(0,1)*l5*sa2*sa3*sb**2*vev2 + ca2*cb*complex(0,1)*l7*sa3*sb*vevs - ca2*cb*complex(0,1)*l8*sa3*sb*vevs',
                  order = {'QED':1})

GC_525 = Coupling(name = 'GC_525',
                  value = '-(ca1*ca3*cb**2*complex(0,1)*l4*vev1)/2. - (ca1*ca3*cb**2*complex(0,1)*l5*vev1)/2. + (cb**2*complex(0,1)*l4*sa1*sa2*sa3*vev1)/2. + (cb**2*complex(0,1)*l5*sa1*sa2*sa3*vev1)/2. - ca3*cb*complex(0,1)*l1*sa1*sb*vev1 + ca3*cb*complex(0,1)*l3*sa1*sb*vev1 - ca1*cb*complex(0,1)*l1*sa2*sa3*sb*vev1 + ca1*cb*complex(0,1)*l3*sa2*sa3*sb*vev1 + (ca1*ca3*complex(0,1)*l4*sb**2*vev1)/2. + (ca1*ca3*complex(0,1)*l5*sb**2*vev1)/2. - (complex(0,1)*l4*sa1*sa2*sa3*sb**2*vev1)/2. - (complex(0,1)*l5*sa1*sa2*sa3*sb**2*vev1)/2. + (ca3*cb*ee**2*complex(0,1)*sa1*sb*vev1)/(4.*sw**2) + (ca1*cb*ee**2*complex(0,1)*sa2*sa3*sb*vev1)/(4.*sw**2) - (ca1*ca3*ee**2*complex(0,1)*sb**2*vev1)/(4.*sw**2) + (ee**2*complex(0,1)*sa1*sa2*sa3*sb**2*vev1)/(4.*sw**2) + (ca3*cb**2*complex(0,1)*l4*sa1*vev2)/2. + (ca3*cb**2*complex(0,1)*l5*sa1*vev2)/2. + (ca1*cb**2*complex(0,1)*l4*sa2*sa3*vev2)/2. + (ca1*cb**2*complex(0,1)*l5*sa2*sa3*vev2)/2. - ca1*ca3*cb*complex(0,1)*l2*sb*vev2 + ca1*ca3*cb*complex(0,1)*l3*sb*vev2 + cb*complex(0,1)*l2*sa1*sa2*sa3*sb*vev2 - cb*complex(0,1)*l3*sa1*sa2*sa3*sb*vev2 - (ca3*complex(0,1)*l4*sa1*sb**2*vev2)/2. - (ca3*complex(0,1)*l5*sa1*sb**2*vev2)/2. - (ca1*complex(0,1)*l4*sa2*sa3*sb**2*vev2)/2. - (ca1*complex(0,1)*l5*sa2*sa3*sb**2*vev2)/2. - (ca3*cb**2*ee**2*complex(0,1)*sa1*vev2)/(4.*sw**2) - (ca1*cb**2*ee**2*complex(0,1)*sa2*sa3*vev2)/(4.*sw**2) + (ca1*ca3*cb*ee**2*complex(0,1)*sb*vev2)/(4.*sw**2) - (cb*ee**2*complex(0,1)*sa1*sa2*sa3*sb*vev2)/(4.*sw**2) + ca2*cb*complex(0,1)*l7*sa3*sb*vevs - ca2*cb*complex(0,1)*l8*sa3*sb*vevs',
                  order = {'QED':1})

GC_526 = Coupling(name = 'GC_526',
                  value = '-(ca1*ca3*cb**2*complex(0,1)*l5*vev1) + cb**2*complex(0,1)*l5*sa1*sa2*sa3*vev1 + (ca3*cb*ee**2*complex(0,1)*sa1*sb*vev1)/(4.*cw**2) - ca3*cb*complex(0,1)*l1*sa1*sb*vev1 + ca3*cb*complex(0,1)*l3*sa1*sb*vev1 + ca3*cb*complex(0,1)*l4*sa1*sb*vev1 - ca3*cb*complex(0,1)*l5*sa1*sb*vev1 + (ca1*cb*ee**2*complex(0,1)*sa2*sa3*sb*vev1)/(4.*cw**2) - ca1*cb*complex(0,1)*l1*sa2*sa3*sb*vev1 + ca1*cb*complex(0,1)*l3*sa2*sa3*sb*vev1 + ca1*cb*complex(0,1)*l4*sa2*sa3*sb*vev1 - ca1*cb*complex(0,1)*l5*sa2*sa3*sb*vev1 - (ca1*ca3*ee**2*complex(0,1)*sb**2*vev1)/(4.*cw**2) + ca1*ca3*complex(0,1)*l5*sb**2*vev1 + (ee**2*complex(0,1)*sa1*sa2*sa3*sb**2*vev1)/(4.*cw**2) - complex(0,1)*l5*sa1*sa2*sa3*sb**2*vev1 + (ca3*cb*ee**2*complex(0,1)*sa1*sb*vev1)/(4.*sw**2) + (ca1*cb*ee**2*complex(0,1)*sa2*sa3*sb*vev1)/(4.*sw**2) - (ca1*ca3*ee**2*complex(0,1)*sb**2*vev1)/(4.*sw**2) + (ee**2*complex(0,1)*sa1*sa2*sa3*sb**2*vev1)/(4.*sw**2) - (ca3*cb**2*ee**2*complex(0,1)*sa1*vev2)/(4.*cw**2) + ca3*cb**2*complex(0,1)*l5*sa1*vev2 - (ca1*cb**2*ee**2*complex(0,1)*sa2*sa3*vev2)/(4.*cw**2) + ca1*cb**2*complex(0,1)*l5*sa2*sa3*vev2 + (ca1*ca3*cb*ee**2*complex(0,1)*sb*vev2)/(4.*cw**2) - ca1*ca3*cb*complex(0,1)*l2*sb*vev2 + ca1*ca3*cb*complex(0,1)*l3*sb*vev2 + ca1*ca3*cb*complex(0,1)*l4*sb*vev2 - ca1*ca3*cb*complex(0,1)*l5*sb*vev2 - (cb*ee**2*complex(0,1)*sa1*sa2*sa3*sb*vev2)/(4.*cw**2) + cb*complex(0,1)*l2*sa1*sa2*sa3*sb*vev2 - cb*complex(0,1)*l3*sa1*sa2*sa3*sb*vev2 - cb*complex(0,1)*l4*sa1*sa2*sa3*sb*vev2 + cb*complex(0,1)*l5*sa1*sa2*sa3*sb*vev2 - ca3*complex(0,1)*l5*sa1*sb**2*vev2 - ca1*complex(0,1)*l5*sa2*sa3*sb**2*vev2 - (ca3*cb**2*ee**2*complex(0,1)*sa1*vev2)/(4.*sw**2) - (ca1*cb**2*ee**2*complex(0,1)*sa2*sa3*vev2)/(4.*sw**2) + (ca1*ca3*cb*ee**2*complex(0,1)*sb*vev2)/(4.*sw**2) - (cb*ee**2*complex(0,1)*sa1*sa2*sa3*sb*vev2)/(4.*sw**2) + ca2*cb*complex(0,1)*l7*sa3*sb*vevs - ca2*cb*complex(0,1)*l8*sa3*sb*vevs',
                  order = {'QED':1})

GC_527 = Coupling(name = 'GC_527',
                  value = '-(ca1*ca3*cb**2*complex(0,1)*l4*vev1)/2. - (ca1*ca3*cb**2*complex(0,1)*l5*vev1)/2. + (cb**2*complex(0,1)*l4*sa1*sa2*sa3*vev1)/2. + (cb**2*complex(0,1)*l5*sa1*sa2*sa3*vev1)/2. - ca3*cb*complex(0,1)*l1*sa1*sb*vev1 + ca3*cb*complex(0,1)*l3*sa1*sb*vev1 - ca1*cb*complex(0,1)*l1*sa2*sa3*sb*vev1 + ca1*cb*complex(0,1)*l3*sa2*sa3*sb*vev1 + (ca1*ca3*complex(0,1)*l4*sb**2*vev1)/2. + (ca1*ca3*complex(0,1)*l5*sb**2*vev1)/2. - (complex(0,1)*l4*sa1*sa2*sa3*sb**2*vev1)/2. - (complex(0,1)*l5*sa1*sa2*sa3*sb**2*vev1)/2. - (ca1*ca3*cb**2*ee**2*complex(0,1)*vev1)/(4.*sw**2) + (cb**2*ee**2*complex(0,1)*sa1*sa2*sa3*vev1)/(4.*sw**2) - (ca3*cb*ee**2*complex(0,1)*sa1*sb*vev1)/(2.*sw**2) - (ca1*cb*ee**2*complex(0,1)*sa2*sa3*sb*vev1)/(2.*sw**2) + (ca1*ca3*ee**2*complex(0,1)*sb**2*vev1)/(4.*sw**2) - (ee**2*complex(0,1)*sa1*sa2*sa3*sb**2*vev1)/(4.*sw**2) + (ca3*cb**2*complex(0,1)*l4*sa1*vev2)/2. + (ca3*cb**2*complex(0,1)*l5*sa1*vev2)/2. + (ca1*cb**2*complex(0,1)*l4*sa2*sa3*vev2)/2. + (ca1*cb**2*complex(0,1)*l5*sa2*sa3*vev2)/2. - ca1*ca3*cb*complex(0,1)*l2*sb*vev2 + ca1*ca3*cb*complex(0,1)*l3*sb*vev2 + cb*complex(0,1)*l2*sa1*sa2*sa3*sb*vev2 - cb*complex(0,1)*l3*sa1*sa2*sa3*sb*vev2 - (ca3*complex(0,1)*l4*sa1*sb**2*vev2)/2. - (ca3*complex(0,1)*l5*sa1*sb**2*vev2)/2. - (ca1*complex(0,1)*l4*sa2*sa3*sb**2*vev2)/2. - (ca1*complex(0,1)*l5*sa2*sa3*sb**2*vev2)/2. + (ca3*cb**2*ee**2*complex(0,1)*sa1*vev2)/(4.*sw**2) + (ca1*cb**2*ee**2*complex(0,1)*sa2*sa3*vev2)/(4.*sw**2) - (ca1*ca3*cb*ee**2*complex(0,1)*sb*vev2)/(2.*sw**2) + (cb*ee**2*complex(0,1)*sa1*sa2*sa3*sb*vev2)/(2.*sw**2) - (ca3*ee**2*complex(0,1)*sa1*sb**2*vev2)/(4.*sw**2) - (ca1*ee**2*complex(0,1)*sa2*sa3*sb**2*vev2)/(4.*sw**2) + ca2*cb*complex(0,1)*l7*sa3*sb*vevs - ca2*cb*complex(0,1)*l8*sa3*sb*vevs',
                  order = {'QED':1})

GC_528 = Coupling(name = 'GC_528',
                  value = '-(ca1*ca3*cb**2*ee**2*complex(0,1)*vev1)/(4.*cw**2) - ca1*ca3*cb**2*complex(0,1)*l5*vev1 + (cb**2*ee**2*complex(0,1)*sa1*sa2*sa3*vev1)/(4.*cw**2) + cb**2*complex(0,1)*l5*sa1*sa2*sa3*vev1 - (ca3*cb*ee**2*complex(0,1)*sa1*sb*vev1)/(2.*cw**2) - ca3*cb*complex(0,1)*l1*sa1*sb*vev1 + ca3*cb*complex(0,1)*l3*sa1*sb*vev1 + ca3*cb*complex(0,1)*l4*sa1*sb*vev1 - ca3*cb*complex(0,1)*l5*sa1*sb*vev1 - (ca1*cb*ee**2*complex(0,1)*sa2*sa3*sb*vev1)/(2.*cw**2) - ca1*cb*complex(0,1)*l1*sa2*sa3*sb*vev1 + ca1*cb*complex(0,1)*l3*sa2*sa3*sb*vev1 + ca1*cb*complex(0,1)*l4*sa2*sa3*sb*vev1 - ca1*cb*complex(0,1)*l5*sa2*sa3*sb*vev1 + (ca1*ca3*ee**2*complex(0,1)*sb**2*vev1)/(4.*cw**2) + ca1*ca3*complex(0,1)*l5*sb**2*vev1 - (ee**2*complex(0,1)*sa1*sa2*sa3*sb**2*vev1)/(4.*cw**2) - complex(0,1)*l5*sa1*sa2*sa3*sb**2*vev1 - (ca1*ca3*cb**2*ee**2*complex(0,1)*vev1)/(4.*sw**2) + (cb**2*ee**2*complex(0,1)*sa1*sa2*sa3*vev1)/(4.*sw**2) - (ca3*cb*ee**2*complex(0,1)*sa1*sb*vev1)/(2.*sw**2) - (ca1*cb*ee**2*complex(0,1)*sa2*sa3*sb*vev1)/(2.*sw**2) + (ca1*ca3*ee**2*complex(0,1)*sb**2*vev1)/(4.*sw**2) - (ee**2*complex(0,1)*sa1*sa2*sa3*sb**2*vev1)/(4.*sw**2) + (ca3*cb**2*ee**2*complex(0,1)*sa1*vev2)/(4.*cw**2) + ca3*cb**2*complex(0,1)*l5*sa1*vev2 + (ca1*cb**2*ee**2*complex(0,1)*sa2*sa3*vev2)/(4.*cw**2) + ca1*cb**2*complex(0,1)*l5*sa2*sa3*vev2 - (ca1*ca3*cb*ee**2*complex(0,1)*sb*vev2)/(2.*cw**2) - ca1*ca3*cb*complex(0,1)*l2*sb*vev2 + ca1*ca3*cb*complex(0,1)*l3*sb*vev2 + ca1*ca3*cb*complex(0,1)*l4*sb*vev2 - ca1*ca3*cb*complex(0,1)*l5*sb*vev2 + (cb*ee**2*complex(0,1)*sa1*sa2*sa3*sb*vev2)/(2.*cw**2) + cb*complex(0,1)*l2*sa1*sa2*sa3*sb*vev2 - cb*complex(0,1)*l3*sa1*sa2*sa3*sb*vev2 - cb*complex(0,1)*l4*sa1*sa2*sa3*sb*vev2 + cb*complex(0,1)*l5*sa1*sa2*sa3*sb*vev2 - (ca3*ee**2*complex(0,1)*sa1*sb**2*vev2)/(4.*cw**2) - ca3*complex(0,1)*l5*sa1*sb**2*vev2 - (ca1*ee**2*complex(0,1)*sa2*sa3*sb**2*vev2)/(4.*cw**2) - ca1*complex(0,1)*l5*sa2*sa3*sb**2*vev2 + (ca3*cb**2*ee**2*complex(0,1)*sa1*vev2)/(4.*sw**2) + (ca1*cb**2*ee**2*complex(0,1)*sa2*sa3*vev2)/(4.*sw**2) - (ca1*ca3*cb*ee**2*complex(0,1)*sb*vev2)/(2.*sw**2) + (cb*ee**2*complex(0,1)*sa1*sa2*sa3*sb*vev2)/(2.*sw**2) - (ca3*ee**2*complex(0,1)*sa1*sb**2*vev2)/(4.*sw**2) - (ca1*ee**2*complex(0,1)*sa2*sa3*sb**2*vev2)/(4.*sw**2) + ca2*cb*complex(0,1)*l7*sa3*sb*vevs - ca2*cb*complex(0,1)*l8*sa3*sb*vevs',
                  order = {'QED':1})

GC_529 = Coupling(name = 'GC_529',
                  value = '-(ca1*ca3*cb**2*complex(0,1)*l4*vev1)/2. - (ca1*ca3*cb**2*complex(0,1)*l5*vev1)/2. + (cb**2*complex(0,1)*l4*sa1*sa2*sa3*vev1)/2. + (cb**2*complex(0,1)*l5*sa1*sa2*sa3*vev1)/2. - ca3*cb*complex(0,1)*l1*sa1*sb*vev1 + ca3*cb*complex(0,1)*l3*sa1*sb*vev1 - ca1*cb*complex(0,1)*l1*sa2*sa3*sb*vev1 + ca1*cb*complex(0,1)*l3*sa2*sa3*sb*vev1 + (ca1*ca3*complex(0,1)*l4*sb**2*vev1)/2. + (ca1*ca3*complex(0,1)*l5*sb**2*vev1)/2. - (complex(0,1)*l4*sa1*sa2*sa3*sb**2*vev1)/2. - (complex(0,1)*l5*sa1*sa2*sa3*sb**2*vev1)/2. + (ca1*ca3*cb**2*ee**2*complex(0,1)*vev1)/(4.*sw**2) - (cb**2*ee**2*complex(0,1)*sa1*sa2*sa3*vev1)/(4.*sw**2) + (ca3*cb*ee**2*complex(0,1)*sa1*sb*vev1)/(4.*sw**2) + (ca1*cb*ee**2*complex(0,1)*sa2*sa3*sb*vev1)/(4.*sw**2) + (ca3*cb**2*complex(0,1)*l4*sa1*vev2)/2. + (ca3*cb**2*complex(0,1)*l5*sa1*vev2)/2. + (ca1*cb**2*complex(0,1)*l4*sa2*sa3*vev2)/2. + (ca1*cb**2*complex(0,1)*l5*sa2*sa3*vev2)/2. - ca1*ca3*cb*complex(0,1)*l2*sb*vev2 + ca1*ca3*cb*complex(0,1)*l3*sb*vev2 + cb*complex(0,1)*l2*sa1*sa2*sa3*sb*vev2 - cb*complex(0,1)*l3*sa1*sa2*sa3*sb*vev2 - (ca3*complex(0,1)*l4*sa1*sb**2*vev2)/2. - (ca3*complex(0,1)*l5*sa1*sb**2*vev2)/2. - (ca1*complex(0,1)*l4*sa2*sa3*sb**2*vev2)/2. - (ca1*complex(0,1)*l5*sa2*sa3*sb**2*vev2)/2. + (ca1*ca3*cb*ee**2*complex(0,1)*sb*vev2)/(4.*sw**2) - (cb*ee**2*complex(0,1)*sa1*sa2*sa3*sb*vev2)/(4.*sw**2) + (ca3*ee**2*complex(0,1)*sa1*sb**2*vev2)/(4.*sw**2) + (ca1*ee**2*complex(0,1)*sa2*sa3*sb**2*vev2)/(4.*sw**2) + ca2*cb*complex(0,1)*l7*sa3*sb*vevs - ca2*cb*complex(0,1)*l8*sa3*sb*vevs',
                  order = {'QED':1})

GC_530 = Coupling(name = 'GC_530',
                  value = '(ca1*ca3*cb**2*ee**2*complex(0,1)*vev1)/(4.*cw**2) - ca1*ca3*cb**2*complex(0,1)*l5*vev1 - (cb**2*ee**2*complex(0,1)*sa1*sa2*sa3*vev1)/(4.*cw**2) + cb**2*complex(0,1)*l5*sa1*sa2*sa3*vev1 + (ca3*cb*ee**2*complex(0,1)*sa1*sb*vev1)/(4.*cw**2) - ca3*cb*complex(0,1)*l1*sa1*sb*vev1 + ca3*cb*complex(0,1)*l3*sa1*sb*vev1 + ca3*cb*complex(0,1)*l4*sa1*sb*vev1 - ca3*cb*complex(0,1)*l5*sa1*sb*vev1 + (ca1*cb*ee**2*complex(0,1)*sa2*sa3*sb*vev1)/(4.*cw**2) - ca1*cb*complex(0,1)*l1*sa2*sa3*sb*vev1 + ca1*cb*complex(0,1)*l3*sa2*sa3*sb*vev1 + ca1*cb*complex(0,1)*l4*sa2*sa3*sb*vev1 - ca1*cb*complex(0,1)*l5*sa2*sa3*sb*vev1 + ca1*ca3*complex(0,1)*l5*sb**2*vev1 - complex(0,1)*l5*sa1*sa2*sa3*sb**2*vev1 + (ca1*ca3*cb**2*ee**2*complex(0,1)*vev1)/(4.*sw**2) - (cb**2*ee**2*complex(0,1)*sa1*sa2*sa3*vev1)/(4.*sw**2) + (ca3*cb*ee**2*complex(0,1)*sa1*sb*vev1)/(4.*sw**2) + (ca1*cb*ee**2*complex(0,1)*sa2*sa3*sb*vev1)/(4.*sw**2) + ca3*cb**2*complex(0,1)*l5*sa1*vev2 + ca1*cb**2*complex(0,1)*l5*sa2*sa3*vev2 + (ca1*ca3*cb*ee**2*complex(0,1)*sb*vev2)/(4.*cw**2) - ca1*ca3*cb*complex(0,1)*l2*sb*vev2 + ca1*ca3*cb*complex(0,1)*l3*sb*vev2 + ca1*ca3*cb*complex(0,1)*l4*sb*vev2 - ca1*ca3*cb*complex(0,1)*l5*sb*vev2 - (cb*ee**2*complex(0,1)*sa1*sa2*sa3*sb*vev2)/(4.*cw**2) + cb*complex(0,1)*l2*sa1*sa2*sa3*sb*vev2 - cb*complex(0,1)*l3*sa1*sa2*sa3*sb*vev2 - cb*complex(0,1)*l4*sa1*sa2*sa3*sb*vev2 + cb*complex(0,1)*l5*sa1*sa2*sa3*sb*vev2 + (ca3*ee**2*complex(0,1)*sa1*sb**2*vev2)/(4.*cw**2) - ca3*complex(0,1)*l5*sa1*sb**2*vev2 + (ca1*ee**2*complex(0,1)*sa2*sa3*sb**2*vev2)/(4.*cw**2) - ca1*complex(0,1)*l5*sa2*sa3*sb**2*vev2 + (ca1*ca3*cb*ee**2*complex(0,1)*sb*vev2)/(4.*sw**2) - (cb*ee**2*complex(0,1)*sa1*sa2*sa3*sb*vev2)/(4.*sw**2) + (ca3*ee**2*complex(0,1)*sa1*sb**2*vev2)/(4.*sw**2) + (ca1*ee**2*complex(0,1)*sa2*sa3*sb**2*vev2)/(4.*sw**2) + ca2*cb*complex(0,1)*l7*sa3*sb*vevs - ca2*cb*complex(0,1)*l8*sa3*sb*vevs',
                  order = {'QED':1})

GC_531 = Coupling(name = 'GC_531',
                  value = 'ca1*ca3*cb**2*complex(0,1)*l3*sa2*vev1 - cb**2*complex(0,1)*l3*sa1*sa3*vev1 - ca3*cb*complex(0,1)*l4*sa1*sa2*sb*vev1 - ca3*cb*complex(0,1)*l5*sa1*sa2*sb*vev1 - ca1*cb*complex(0,1)*l4*sa3*sb*vev1 - ca1*cb*complex(0,1)*l5*sa3*sb*vev1 + ca1*ca3*complex(0,1)*l1*sa2*sb**2*vev1 - complex(0,1)*l1*sa1*sa3*sb**2*vev1 + ca3*cb**2*complex(0,1)*l2*sa1*sa2*vev2 + ca1*cb**2*complex(0,1)*l2*sa3*vev2 - ca1*ca3*cb*complex(0,1)*l4*sa2*sb*vev2 - ca1*ca3*cb*complex(0,1)*l5*sa2*sb*vev2 + cb*complex(0,1)*l4*sa1*sa3*sb*vev2 + cb*complex(0,1)*l5*sa1*sa3*sb*vev2 + ca3*complex(0,1)*l3*sa1*sa2*sb**2*vev2 + ca1*complex(0,1)*l3*sa3*sb**2*vev2 - ca2*ca3*cb**2*complex(0,1)*l8*vevs - ca2*ca3*complex(0,1)*l7*sb**2*vevs',
                  order = {'QED':1})

GC_532 = Coupling(name = 'GC_532',
                  value = 'ca1*ca3*cb**2*complex(0,1)*l3*sa2*vev1 + ca1*ca3*cb**2*complex(0,1)*l4*sa2*vev1 - ca1*ca3*cb**2*complex(0,1)*l5*sa2*vev1 - cb**2*complex(0,1)*l3*sa1*sa3*vev1 - cb**2*complex(0,1)*l4*sa1*sa3*vev1 + cb**2*complex(0,1)*l5*sa1*sa3*vev1 - 2*ca3*cb*complex(0,1)*l5*sa1*sa2*sb*vev1 - 2*ca1*cb*complex(0,1)*l5*sa3*sb*vev1 + ca1*ca3*complex(0,1)*l1*sa2*sb**2*vev1 - complex(0,1)*l1*sa1*sa3*sb**2*vev1 + ca3*cb**2*complex(0,1)*l2*sa1*sa2*vev2 + ca1*cb**2*complex(0,1)*l2*sa3*vev2 - 2*ca1*ca3*cb*complex(0,1)*l5*sa2*sb*vev2 + 2*cb*complex(0,1)*l5*sa1*sa3*sb*vev2 + ca3*complex(0,1)*l3*sa1*sa2*sb**2*vev2 + ca3*complex(0,1)*l4*sa1*sa2*sb**2*vev2 - ca3*complex(0,1)*l5*sa1*sa2*sb**2*vev2 + ca1*complex(0,1)*l3*sa3*sb**2*vev2 + ca1*complex(0,1)*l4*sa3*sb**2*vev2 - ca1*complex(0,1)*l5*sa3*sb**2*vev2 - ca2*ca3*cb**2*complex(0,1)*l8*vevs - ca2*ca3*complex(0,1)*l7*sb**2*vevs',
                  order = {'QED':1})

GC_533 = Coupling(name = 'GC_533',
                  value = 'ca1*ca3*cb**2*complex(0,1)*l3*sa2*vev1 - cb**2*complex(0,1)*l3*sa1*sa3*vev1 - ca3*cb*complex(0,1)*l4*sa1*sa2*sb*vev1 - ca3*cb*complex(0,1)*l5*sa1*sa2*sb*vev1 - ca1*cb*complex(0,1)*l4*sa3*sb*vev1 - ca1*cb*complex(0,1)*l5*sa3*sb*vev1 + ca1*ca3*complex(0,1)*l1*sa2*sb**2*vev1 - complex(0,1)*l1*sa1*sa3*sb**2*vev1 + (ca3*cb*ee**2*complex(0,1)*sa1*sa2*sb*vev1)/(4.*sw**2) + (ca1*cb*ee**2*complex(0,1)*sa3*sb*vev1)/(4.*sw**2) - (ca1*ca3*ee**2*complex(0,1)*sa2*sb**2*vev1)/(4.*sw**2) + (ee**2*complex(0,1)*sa1*sa3*sb**2*vev1)/(4.*sw**2) + ca3*cb**2*complex(0,1)*l2*sa1*sa2*vev2 + ca1*cb**2*complex(0,1)*l2*sa3*vev2 - ca1*ca3*cb*complex(0,1)*l4*sa2*sb*vev2 - ca1*ca3*cb*complex(0,1)*l5*sa2*sb*vev2 + cb*complex(0,1)*l4*sa1*sa3*sb*vev2 + cb*complex(0,1)*l5*sa1*sa3*sb*vev2 + ca3*complex(0,1)*l3*sa1*sa2*sb**2*vev2 + ca1*complex(0,1)*l3*sa3*sb**2*vev2 - (ca3*cb**2*ee**2*complex(0,1)*sa1*sa2*vev2)/(4.*sw**2) - (ca1*cb**2*ee**2*complex(0,1)*sa3*vev2)/(4.*sw**2) + (ca1*ca3*cb*ee**2*complex(0,1)*sa2*sb*vev2)/(4.*sw**2) - (cb*ee**2*complex(0,1)*sa1*sa3*sb*vev2)/(4.*sw**2) - ca2*ca3*cb**2*complex(0,1)*l8*vevs - ca2*ca3*complex(0,1)*l7*sb**2*vevs',
                  order = {'QED':1})

GC_534 = Coupling(name = 'GC_534',
                  value = 'ca1*ca3*cb**2*complex(0,1)*l3*sa2*vev1 + ca1*ca3*cb**2*complex(0,1)*l4*sa2*vev1 - ca1*ca3*cb**2*complex(0,1)*l5*sa2*vev1 - cb**2*complex(0,1)*l3*sa1*sa3*vev1 - cb**2*complex(0,1)*l4*sa1*sa3*vev1 + cb**2*complex(0,1)*l5*sa1*sa3*vev1 + (ca3*cb*ee**2*complex(0,1)*sa1*sa2*sb*vev1)/(4.*cw**2) - 2*ca3*cb*complex(0,1)*l5*sa1*sa2*sb*vev1 + (ca1*cb*ee**2*complex(0,1)*sa3*sb*vev1)/(4.*cw**2) - 2*ca1*cb*complex(0,1)*l5*sa3*sb*vev1 - (ca1*ca3*ee**2*complex(0,1)*sa2*sb**2*vev1)/(4.*cw**2) + ca1*ca3*complex(0,1)*l1*sa2*sb**2*vev1 + (ee**2*complex(0,1)*sa1*sa3*sb**2*vev1)/(4.*cw**2) - complex(0,1)*l1*sa1*sa3*sb**2*vev1 + (ca3*cb*ee**2*complex(0,1)*sa1*sa2*sb*vev1)/(4.*sw**2) + (ca1*cb*ee**2*complex(0,1)*sa3*sb*vev1)/(4.*sw**2) - (ca1*ca3*ee**2*complex(0,1)*sa2*sb**2*vev1)/(4.*sw**2) + (ee**2*complex(0,1)*sa1*sa3*sb**2*vev1)/(4.*sw**2) - (ca3*cb**2*ee**2*complex(0,1)*sa1*sa2*vev2)/(4.*cw**2) + ca3*cb**2*complex(0,1)*l2*sa1*sa2*vev2 - (ca1*cb**2*ee**2*complex(0,1)*sa3*vev2)/(4.*cw**2) + ca1*cb**2*complex(0,1)*l2*sa3*vev2 + (ca1*ca3*cb*ee**2*complex(0,1)*sa2*sb*vev2)/(4.*cw**2) - 2*ca1*ca3*cb*complex(0,1)*l5*sa2*sb*vev2 - (cb*ee**2*complex(0,1)*sa1*sa3*sb*vev2)/(4.*cw**2) + 2*cb*complex(0,1)*l5*sa1*sa3*sb*vev2 + ca3*complex(0,1)*l3*sa1*sa2*sb**2*vev2 + ca3*complex(0,1)*l4*sa1*sa2*sb**2*vev2 - ca3*complex(0,1)*l5*sa1*sa2*sb**2*vev2 + ca1*complex(0,1)*l3*sa3*sb**2*vev2 + ca1*complex(0,1)*l4*sa3*sb**2*vev2 - ca1*complex(0,1)*l5*sa3*sb**2*vev2 - (ca3*cb**2*ee**2*complex(0,1)*sa1*sa2*vev2)/(4.*sw**2) - (ca1*cb**2*ee**2*complex(0,1)*sa3*vev2)/(4.*sw**2) + (ca1*ca3*cb*ee**2*complex(0,1)*sa2*sb*vev2)/(4.*sw**2) - (cb*ee**2*complex(0,1)*sa1*sa3*sb*vev2)/(4.*sw**2) - ca2*ca3*cb**2*complex(0,1)*l8*vevs - ca2*ca3*complex(0,1)*l7*sb**2*vevs',
                  order = {'QED':1})

GC_535 = Coupling(name = 'GC_535',
                  value = 'ca1*ca3*cb**2*complex(0,1)*l3*sa2*vev1 - cb**2*complex(0,1)*l3*sa1*sa3*vev1 - ca3*cb*complex(0,1)*l4*sa1*sa2*sb*vev1 - ca3*cb*complex(0,1)*l5*sa1*sa2*sb*vev1 - ca1*cb*complex(0,1)*l4*sa3*sb*vev1 - ca1*cb*complex(0,1)*l5*sa3*sb*vev1 + ca1*ca3*complex(0,1)*l1*sa2*sb**2*vev1 - complex(0,1)*l1*sa1*sa3*sb**2*vev1 - (ca3*cb*ee**2*complex(0,1)*sa1*sa2*sb*vev1)/(2.*sw**2) - (ca1*cb*ee**2*complex(0,1)*sa3*sb*vev1)/(2.*sw**2) + (ca1*ca3*ee**2*complex(0,1)*sa2*sb**2*vev1)/(2.*sw**2) - (ee**2*complex(0,1)*sa1*sa3*sb**2*vev1)/(2.*sw**2) + ca3*cb**2*complex(0,1)*l2*sa1*sa2*vev2 + ca1*cb**2*complex(0,1)*l2*sa3*vev2 - ca1*ca3*cb*complex(0,1)*l4*sa2*sb*vev2 - ca1*ca3*cb*complex(0,1)*l5*sa2*sb*vev2 + cb*complex(0,1)*l4*sa1*sa3*sb*vev2 + cb*complex(0,1)*l5*sa1*sa3*sb*vev2 + ca3*complex(0,1)*l3*sa1*sa2*sb**2*vev2 + ca1*complex(0,1)*l3*sa3*sb**2*vev2 + (ca3*cb**2*ee**2*complex(0,1)*sa1*sa2*vev2)/(2.*sw**2) + (ca1*cb**2*ee**2*complex(0,1)*sa3*vev2)/(2.*sw**2) - (ca1*ca3*cb*ee**2*complex(0,1)*sa2*sb*vev2)/(2.*sw**2) + (cb*ee**2*complex(0,1)*sa1*sa3*sb*vev2)/(2.*sw**2) - ca2*ca3*cb**2*complex(0,1)*l8*vevs - ca2*ca3*complex(0,1)*l7*sb**2*vevs',
                  order = {'QED':1})

GC_536 = Coupling(name = 'GC_536',
                  value = 'ca1*ca3*cb**2*complex(0,1)*l3*sa2*vev1 + ca1*ca3*cb**2*complex(0,1)*l4*sa2*vev1 - ca1*ca3*cb**2*complex(0,1)*l5*sa2*vev1 - cb**2*complex(0,1)*l3*sa1*sa3*vev1 - cb**2*complex(0,1)*l4*sa1*sa3*vev1 + cb**2*complex(0,1)*l5*sa1*sa3*vev1 - (ca3*cb*ee**2*complex(0,1)*sa1*sa2*sb*vev1)/(2.*cw**2) - 2*ca3*cb*complex(0,1)*l5*sa1*sa2*sb*vev1 - (ca1*cb*ee**2*complex(0,1)*sa3*sb*vev1)/(2.*cw**2) - 2*ca1*cb*complex(0,1)*l5*sa3*sb*vev1 + (ca1*ca3*ee**2*complex(0,1)*sa2*sb**2*vev1)/(2.*cw**2) + ca1*ca3*complex(0,1)*l1*sa2*sb**2*vev1 - (ee**2*complex(0,1)*sa1*sa3*sb**2*vev1)/(2.*cw**2) - complex(0,1)*l1*sa1*sa3*sb**2*vev1 - (ca3*cb*ee**2*complex(0,1)*sa1*sa2*sb*vev1)/(2.*sw**2) - (ca1*cb*ee**2*complex(0,1)*sa3*sb*vev1)/(2.*sw**2) + (ca1*ca3*ee**2*complex(0,1)*sa2*sb**2*vev1)/(2.*sw**2) - (ee**2*complex(0,1)*sa1*sa3*sb**2*vev1)/(2.*sw**2) + (ca3*cb**2*ee**2*complex(0,1)*sa1*sa2*vev2)/(2.*cw**2) + ca3*cb**2*complex(0,1)*l2*sa1*sa2*vev2 + (ca1*cb**2*ee**2*complex(0,1)*sa3*vev2)/(2.*cw**2) + ca1*cb**2*complex(0,1)*l2*sa3*vev2 - (ca1*ca3*cb*ee**2*complex(0,1)*sa2*sb*vev2)/(2.*cw**2) - 2*ca1*ca3*cb*complex(0,1)*l5*sa2*sb*vev2 + (cb*ee**2*complex(0,1)*sa1*sa3*sb*vev2)/(2.*cw**2) + 2*cb*complex(0,1)*l5*sa1*sa3*sb*vev2 + ca3*complex(0,1)*l3*sa1*sa2*sb**2*vev2 + ca3*complex(0,1)*l4*sa1*sa2*sb**2*vev2 - ca3*complex(0,1)*l5*sa1*sa2*sb**2*vev2 + ca1*complex(0,1)*l3*sa3*sb**2*vev2 + ca1*complex(0,1)*l4*sa3*sb**2*vev2 - ca1*complex(0,1)*l5*sa3*sb**2*vev2 + (ca3*cb**2*ee**2*complex(0,1)*sa1*sa2*vev2)/(2.*sw**2) + (ca1*cb**2*ee**2*complex(0,1)*sa3*vev2)/(2.*sw**2) - (ca1*ca3*cb*ee**2*complex(0,1)*sa2*sb*vev2)/(2.*sw**2) + (cb*ee**2*complex(0,1)*sa1*sa3*sb*vev2)/(2.*sw**2) - ca2*ca3*cb**2*complex(0,1)*l8*vevs - ca2*ca3*complex(0,1)*l7*sb**2*vevs',
                  order = {'QED':1})

GC_537 = Coupling(name = 'GC_537',
                  value = 'ca1*ca3*cb**2*complex(0,1)*l1*sa2*vev1 - cb**2*complex(0,1)*l1*sa1*sa3*vev1 + ca3*cb*complex(0,1)*l4*sa1*sa2*sb*vev1 + ca3*cb*complex(0,1)*l5*sa1*sa2*sb*vev1 + ca1*cb*complex(0,1)*l4*sa3*sb*vev1 + ca1*cb*complex(0,1)*l5*sa3*sb*vev1 + ca1*ca3*complex(0,1)*l3*sa2*sb**2*vev1 - complex(0,1)*l3*sa1*sa3*sb**2*vev1 + ca3*cb**2*complex(0,1)*l3*sa1*sa2*vev2 + ca1*cb**2*complex(0,1)*l3*sa3*vev2 + ca1*ca3*cb*complex(0,1)*l4*sa2*sb*vev2 + ca1*ca3*cb*complex(0,1)*l5*sa2*sb*vev2 - cb*complex(0,1)*l4*sa1*sa3*sb*vev2 - cb*complex(0,1)*l5*sa1*sa3*sb*vev2 + ca3*complex(0,1)*l2*sa1*sa2*sb**2*vev2 + ca1*complex(0,1)*l2*sa3*sb**2*vev2 - ca2*ca3*cb**2*complex(0,1)*l7*vevs - ca2*ca3*complex(0,1)*l8*sb**2*vevs',
                  order = {'QED':1})

GC_538 = Coupling(name = 'GC_538',
                  value = 'ca1*ca3*cb**2*complex(0,1)*l1*sa2*vev1 - cb**2*complex(0,1)*l1*sa1*sa3*vev1 + 2*ca3*cb*complex(0,1)*l5*sa1*sa2*sb*vev1 + 2*ca1*cb*complex(0,1)*l5*sa3*sb*vev1 + ca1*ca3*complex(0,1)*l3*sa2*sb**2*vev1 + ca1*ca3*complex(0,1)*l4*sa2*sb**2*vev1 - ca1*ca3*complex(0,1)*l5*sa2*sb**2*vev1 - complex(0,1)*l3*sa1*sa3*sb**2*vev1 - complex(0,1)*l4*sa1*sa3*sb**2*vev1 + complex(0,1)*l5*sa1*sa3*sb**2*vev1 + ca3*cb**2*complex(0,1)*l3*sa1*sa2*vev2 + ca3*cb**2*complex(0,1)*l4*sa1*sa2*vev2 - ca3*cb**2*complex(0,1)*l5*sa1*sa2*vev2 + ca1*cb**2*complex(0,1)*l3*sa3*vev2 + ca1*cb**2*complex(0,1)*l4*sa3*vev2 - ca1*cb**2*complex(0,1)*l5*sa3*vev2 + 2*ca1*ca3*cb*complex(0,1)*l5*sa2*sb*vev2 - 2*cb*complex(0,1)*l5*sa1*sa3*sb*vev2 + ca3*complex(0,1)*l2*sa1*sa2*sb**2*vev2 + ca1*complex(0,1)*l2*sa3*sb**2*vev2 - ca2*ca3*cb**2*complex(0,1)*l7*vevs - ca2*ca3*complex(0,1)*l8*sb**2*vevs',
                  order = {'QED':1})

GC_539 = Coupling(name = 'GC_539',
                  value = 'ca1*ca3*cb**2*complex(0,1)*l1*sa2*vev1 - cb**2*complex(0,1)*l1*sa1*sa3*vev1 + ca3*cb*complex(0,1)*l4*sa1*sa2*sb*vev1 + ca3*cb*complex(0,1)*l5*sa1*sa2*sb*vev1 + ca1*cb*complex(0,1)*l4*sa3*sb*vev1 + ca1*cb*complex(0,1)*l5*sa3*sb*vev1 + ca1*ca3*complex(0,1)*l3*sa2*sb**2*vev1 - complex(0,1)*l3*sa1*sa3*sb**2*vev1 - (ca1*ca3*cb**2*ee**2*complex(0,1)*sa2*vev1)/(4.*sw**2) + (cb**2*ee**2*complex(0,1)*sa1*sa3*vev1)/(4.*sw**2) - (ca3*cb*ee**2*complex(0,1)*sa1*sa2*sb*vev1)/(4.*sw**2) - (ca1*cb*ee**2*complex(0,1)*sa3*sb*vev1)/(4.*sw**2) + ca3*cb**2*complex(0,1)*l3*sa1*sa2*vev2 + ca1*cb**2*complex(0,1)*l3*sa3*vev2 + ca1*ca3*cb*complex(0,1)*l4*sa2*sb*vev2 + ca1*ca3*cb*complex(0,1)*l5*sa2*sb*vev2 - cb*complex(0,1)*l4*sa1*sa3*sb*vev2 - cb*complex(0,1)*l5*sa1*sa3*sb*vev2 + ca3*complex(0,1)*l2*sa1*sa2*sb**2*vev2 + ca1*complex(0,1)*l2*sa3*sb**2*vev2 - (ca1*ca3*cb*ee**2*complex(0,1)*sa2*sb*vev2)/(4.*sw**2) + (cb*ee**2*complex(0,1)*sa1*sa3*sb*vev2)/(4.*sw**2) - (ca3*ee**2*complex(0,1)*sa1*sa2*sb**2*vev2)/(4.*sw**2) - (ca1*ee**2*complex(0,1)*sa3*sb**2*vev2)/(4.*sw**2) - ca2*ca3*cb**2*complex(0,1)*l7*vevs - ca2*ca3*complex(0,1)*l8*sb**2*vevs',
                  order = {'QED':1})

GC_540 = Coupling(name = 'GC_540',
                  value = '-(ca1*ca3*cb**2*ee**2*complex(0,1)*sa2*vev1)/(4.*cw**2) + ca1*ca3*cb**2*complex(0,1)*l1*sa2*vev1 + (cb**2*ee**2*complex(0,1)*sa1*sa3*vev1)/(4.*cw**2) - cb**2*complex(0,1)*l1*sa1*sa3*vev1 - (ca3*cb*ee**2*complex(0,1)*sa1*sa2*sb*vev1)/(4.*cw**2) + 2*ca3*cb*complex(0,1)*l5*sa1*sa2*sb*vev1 - (ca1*cb*ee**2*complex(0,1)*sa3*sb*vev1)/(4.*cw**2) + 2*ca1*cb*complex(0,1)*l5*sa3*sb*vev1 + ca1*ca3*complex(0,1)*l3*sa2*sb**2*vev1 + ca1*ca3*complex(0,1)*l4*sa2*sb**2*vev1 - ca1*ca3*complex(0,1)*l5*sa2*sb**2*vev1 - complex(0,1)*l3*sa1*sa3*sb**2*vev1 - complex(0,1)*l4*sa1*sa3*sb**2*vev1 + complex(0,1)*l5*sa1*sa3*sb**2*vev1 - (ca1*ca3*cb**2*ee**2*complex(0,1)*sa2*vev1)/(4.*sw**2) + (cb**2*ee**2*complex(0,1)*sa1*sa3*vev1)/(4.*sw**2) - (ca3*cb*ee**2*complex(0,1)*sa1*sa2*sb*vev1)/(4.*sw**2) - (ca1*cb*ee**2*complex(0,1)*sa3*sb*vev1)/(4.*sw**2) + ca3*cb**2*complex(0,1)*l3*sa1*sa2*vev2 + ca3*cb**2*complex(0,1)*l4*sa1*sa2*vev2 - ca3*cb**2*complex(0,1)*l5*sa1*sa2*vev2 + ca1*cb**2*complex(0,1)*l3*sa3*vev2 + ca1*cb**2*complex(0,1)*l4*sa3*vev2 - ca1*cb**2*complex(0,1)*l5*sa3*vev2 - (ca1*ca3*cb*ee**2*complex(0,1)*sa2*sb*vev2)/(4.*cw**2) + 2*ca1*ca3*cb*complex(0,1)*l5*sa2*sb*vev2 + (cb*ee**2*complex(0,1)*sa1*sa3*sb*vev2)/(4.*cw**2) - 2*cb*complex(0,1)*l5*sa1*sa3*sb*vev2 - (ca3*ee**2*complex(0,1)*sa1*sa2*sb**2*vev2)/(4.*cw**2) + ca3*complex(0,1)*l2*sa1*sa2*sb**2*vev2 - (ca1*ee**2*complex(0,1)*sa3*sb**2*vev2)/(4.*cw**2) + ca1*complex(0,1)*l2*sa3*sb**2*vev2 - (ca1*ca3*cb*ee**2*complex(0,1)*sa2*sb*vev2)/(4.*sw**2) + (cb*ee**2*complex(0,1)*sa1*sa3*sb*vev2)/(4.*sw**2) - (ca3*ee**2*complex(0,1)*sa1*sa2*sb**2*vev2)/(4.*sw**2) - (ca1*ee**2*complex(0,1)*sa3*sb**2*vev2)/(4.*sw**2) - ca2*ca3*cb**2*complex(0,1)*l7*vevs - ca2*ca3*complex(0,1)*l8*sb**2*vevs',
                  order = {'QED':1})

GC_541 = Coupling(name = 'GC_541',
                  value = 'ca1*ca3*cb**2*complex(0,1)*l1*sa2*vev1 - cb**2*complex(0,1)*l1*sa1*sa3*vev1 + ca3*cb*complex(0,1)*l4*sa1*sa2*sb*vev1 + ca3*cb*complex(0,1)*l5*sa1*sa2*sb*vev1 + ca1*cb*complex(0,1)*l4*sa3*sb*vev1 + ca1*cb*complex(0,1)*l5*sa3*sb*vev1 + ca1*ca3*complex(0,1)*l3*sa2*sb**2*vev1 - complex(0,1)*l3*sa1*sa3*sb**2*vev1 + (ca1*ca3*cb**2*ee**2*complex(0,1)*sa2*vev1)/(2.*sw**2) - (cb**2*ee**2*complex(0,1)*sa1*sa3*vev1)/(2.*sw**2) + (ca3*cb*ee**2*complex(0,1)*sa1*sa2*sb*vev1)/(2.*sw**2) + (ca1*cb*ee**2*complex(0,1)*sa3*sb*vev1)/(2.*sw**2) + ca3*cb**2*complex(0,1)*l3*sa1*sa2*vev2 + ca1*cb**2*complex(0,1)*l3*sa3*vev2 + ca1*ca3*cb*complex(0,1)*l4*sa2*sb*vev2 + ca1*ca3*cb*complex(0,1)*l5*sa2*sb*vev2 - cb*complex(0,1)*l4*sa1*sa3*sb*vev2 - cb*complex(0,1)*l5*sa1*sa3*sb*vev2 + ca3*complex(0,1)*l2*sa1*sa2*sb**2*vev2 + ca1*complex(0,1)*l2*sa3*sb**2*vev2 + (ca1*ca3*cb*ee**2*complex(0,1)*sa2*sb*vev2)/(2.*sw**2) - (cb*ee**2*complex(0,1)*sa1*sa3*sb*vev2)/(2.*sw**2) + (ca3*ee**2*complex(0,1)*sa1*sa2*sb**2*vev2)/(2.*sw**2) + (ca1*ee**2*complex(0,1)*sa3*sb**2*vev2)/(2.*sw**2) - ca2*ca3*cb**2*complex(0,1)*l7*vevs - ca2*ca3*complex(0,1)*l8*sb**2*vevs',
                  order = {'QED':1})

GC_542 = Coupling(name = 'GC_542',
                  value = '(ca1*ca3*cb**2*ee**2*complex(0,1)*sa2*vev1)/(2.*cw**2) + ca1*ca3*cb**2*complex(0,1)*l1*sa2*vev1 - (cb**2*ee**2*complex(0,1)*sa1*sa3*vev1)/(2.*cw**2) - cb**2*complex(0,1)*l1*sa1*sa3*vev1 + (ca3*cb*ee**2*complex(0,1)*sa1*sa2*sb*vev1)/(2.*cw**2) + 2*ca3*cb*complex(0,1)*l5*sa1*sa2*sb*vev1 + (ca1*cb*ee**2*complex(0,1)*sa3*sb*vev1)/(2.*cw**2) + 2*ca1*cb*complex(0,1)*l5*sa3*sb*vev1 + ca1*ca3*complex(0,1)*l3*sa2*sb**2*vev1 + ca1*ca3*complex(0,1)*l4*sa2*sb**2*vev1 - ca1*ca3*complex(0,1)*l5*sa2*sb**2*vev1 - complex(0,1)*l3*sa1*sa3*sb**2*vev1 - complex(0,1)*l4*sa1*sa3*sb**2*vev1 + complex(0,1)*l5*sa1*sa3*sb**2*vev1 + (ca1*ca3*cb**2*ee**2*complex(0,1)*sa2*vev1)/(2.*sw**2) - (cb**2*ee**2*complex(0,1)*sa1*sa3*vev1)/(2.*sw**2) + (ca3*cb*ee**2*complex(0,1)*sa1*sa2*sb*vev1)/(2.*sw**2) + (ca1*cb*ee**2*complex(0,1)*sa3*sb*vev1)/(2.*sw**2) + ca3*cb**2*complex(0,1)*l3*sa1*sa2*vev2 + ca3*cb**2*complex(0,1)*l4*sa1*sa2*vev2 - ca3*cb**2*complex(0,1)*l5*sa1*sa2*vev2 + ca1*cb**2*complex(0,1)*l3*sa3*vev2 + ca1*cb**2*complex(0,1)*l4*sa3*vev2 - ca1*cb**2*complex(0,1)*l5*sa3*vev2 + (ca1*ca3*cb*ee**2*complex(0,1)*sa2*sb*vev2)/(2.*cw**2) + 2*ca1*ca3*cb*complex(0,1)*l5*sa2*sb*vev2 - (cb*ee**2*complex(0,1)*sa1*sa3*sb*vev2)/(2.*cw**2) - 2*cb*complex(0,1)*l5*sa1*sa3*sb*vev2 + (ca3*ee**2*complex(0,1)*sa1*sa2*sb**2*vev2)/(2.*cw**2) + ca3*complex(0,1)*l2*sa1*sa2*sb**2*vev2 + (ca1*ee**2*complex(0,1)*sa3*sb**2*vev2)/(2.*cw**2) + ca1*complex(0,1)*l2*sa3*sb**2*vev2 + (ca1*ca3*cb*ee**2*complex(0,1)*sa2*sb*vev2)/(2.*sw**2) - (cb*ee**2*complex(0,1)*sa1*sa3*sb*vev2)/(2.*sw**2) + (ca3*ee**2*complex(0,1)*sa1*sa2*sb**2*vev2)/(2.*sw**2) + (ca1*ee**2*complex(0,1)*sa3*sb**2*vev2)/(2.*sw**2) - ca2*ca3*cb**2*complex(0,1)*l7*vevs - ca2*ca3*complex(0,1)*l8*sb**2*vevs',
                  order = {'QED':1})

GC_543 = Coupling(name = 'GC_543',
                  value = '-(ca1*ca2*cb**2*complex(0,1)*l3*vev1) + ca2*cb*complex(0,1)*l4*sa1*sb*vev1 + ca2*cb*complex(0,1)*l5*sa1*sb*vev1 - ca1*ca2*complex(0,1)*l1*sb**2*vev1 - ca2*cb**2*complex(0,1)*l2*sa1*vev2 + ca1*ca2*cb*complex(0,1)*l4*sb*vev2 + ca1*ca2*cb*complex(0,1)*l5*sb*vev2 - ca2*complex(0,1)*l3*sa1*sb**2*vev2 - cb**2*complex(0,1)*l8*sa2*vevs - complex(0,1)*l7*sa2*sb**2*vevs',
                  order = {'QED':1})

GC_544 = Coupling(name = 'GC_544',
                  value = '-(ca1*ca2*cb**2*complex(0,1)*l3*vev1) - ca1*ca2*cb**2*complex(0,1)*l4*vev1 + ca1*ca2*cb**2*complex(0,1)*l5*vev1 + 2*ca2*cb*complex(0,1)*l5*sa1*sb*vev1 - ca1*ca2*complex(0,1)*l1*sb**2*vev1 - ca2*cb**2*complex(0,1)*l2*sa1*vev2 + 2*ca1*ca2*cb*complex(0,1)*l5*sb*vev2 - ca2*complex(0,1)*l3*sa1*sb**2*vev2 - ca2*complex(0,1)*l4*sa1*sb**2*vev2 + ca2*complex(0,1)*l5*sa1*sb**2*vev2 - cb**2*complex(0,1)*l8*sa2*vevs - complex(0,1)*l7*sa2*sb**2*vevs',
                  order = {'QED':1})

GC_545 = Coupling(name = 'GC_545',
                  value = '-(ca1*ca2*cb**2*complex(0,1)*l3*vev1) + ca2*cb*complex(0,1)*l4*sa1*sb*vev1 + ca2*cb*complex(0,1)*l5*sa1*sb*vev1 - ca1*ca2*complex(0,1)*l1*sb**2*vev1 - (ca2*cb*ee**2*complex(0,1)*sa1*sb*vev1)/(4.*sw**2) + (ca1*ca2*ee**2*complex(0,1)*sb**2*vev1)/(4.*sw**2) - ca2*cb**2*complex(0,1)*l2*sa1*vev2 + ca1*ca2*cb*complex(0,1)*l4*sb*vev2 + ca1*ca2*cb*complex(0,1)*l5*sb*vev2 - ca2*complex(0,1)*l3*sa1*sb**2*vev2 + (ca2*cb**2*ee**2*complex(0,1)*sa1*vev2)/(4.*sw**2) - (ca1*ca2*cb*ee**2*complex(0,1)*sb*vev2)/(4.*sw**2) - cb**2*complex(0,1)*l8*sa2*vevs - complex(0,1)*l7*sa2*sb**2*vevs',
                  order = {'QED':1})

GC_546 = Coupling(name = 'GC_546',
                  value = '-(ca1*ca2*cb**2*complex(0,1)*l3*vev1) - ca1*ca2*cb**2*complex(0,1)*l4*vev1 + ca1*ca2*cb**2*complex(0,1)*l5*vev1 - (ca2*cb*ee**2*complex(0,1)*sa1*sb*vev1)/(4.*cw**2) + 2*ca2*cb*complex(0,1)*l5*sa1*sb*vev1 + (ca1*ca2*ee**2*complex(0,1)*sb**2*vev1)/(4.*cw**2) - ca1*ca2*complex(0,1)*l1*sb**2*vev1 - (ca2*cb*ee**2*complex(0,1)*sa1*sb*vev1)/(4.*sw**2) + (ca1*ca2*ee**2*complex(0,1)*sb**2*vev1)/(4.*sw**2) + (ca2*cb**2*ee**2*complex(0,1)*sa1*vev2)/(4.*cw**2) - ca2*cb**2*complex(0,1)*l2*sa1*vev2 - (ca1*ca2*cb*ee**2*complex(0,1)*sb*vev2)/(4.*cw**2) + 2*ca1*ca2*cb*complex(0,1)*l5*sb*vev2 - ca2*complex(0,1)*l3*sa1*sb**2*vev2 - ca2*complex(0,1)*l4*sa1*sb**2*vev2 + ca2*complex(0,1)*l5*sa1*sb**2*vev2 + (ca2*cb**2*ee**2*complex(0,1)*sa1*vev2)/(4.*sw**2) - (ca1*ca2*cb*ee**2*complex(0,1)*sb*vev2)/(4.*sw**2) - cb**2*complex(0,1)*l8*sa2*vevs - complex(0,1)*l7*sa2*sb**2*vevs',
                  order = {'QED':1})

GC_547 = Coupling(name = 'GC_547',
                  value = '-(ca1*ca2*cb**2*complex(0,1)*l3*vev1) + ca2*cb*complex(0,1)*l4*sa1*sb*vev1 + ca2*cb*complex(0,1)*l5*sa1*sb*vev1 - ca1*ca2*complex(0,1)*l1*sb**2*vev1 + (ca2*cb*ee**2*complex(0,1)*sa1*sb*vev1)/(2.*sw**2) - (ca1*ca2*ee**2*complex(0,1)*sb**2*vev1)/(2.*sw**2) - ca2*cb**2*complex(0,1)*l2*sa1*vev2 + ca1*ca2*cb*complex(0,1)*l4*sb*vev2 + ca1*ca2*cb*complex(0,1)*l5*sb*vev2 - ca2*complex(0,1)*l3*sa1*sb**2*vev2 - (ca2*cb**2*ee**2*complex(0,1)*sa1*vev2)/(2.*sw**2) + (ca1*ca2*cb*ee**2*complex(0,1)*sb*vev2)/(2.*sw**2) - cb**2*complex(0,1)*l8*sa2*vevs - complex(0,1)*l7*sa2*sb**2*vevs',
                  order = {'QED':1})

GC_548 = Coupling(name = 'GC_548',
                  value = '-(ca1*ca2*cb**2*complex(0,1)*l3*vev1) - ca1*ca2*cb**2*complex(0,1)*l4*vev1 + ca1*ca2*cb**2*complex(0,1)*l5*vev1 + (ca2*cb*ee**2*complex(0,1)*sa1*sb*vev1)/(2.*cw**2) + 2*ca2*cb*complex(0,1)*l5*sa1*sb*vev1 - (ca1*ca2*ee**2*complex(0,1)*sb**2*vev1)/(2.*cw**2) - ca1*ca2*complex(0,1)*l1*sb**2*vev1 + (ca2*cb*ee**2*complex(0,1)*sa1*sb*vev1)/(2.*sw**2) - (ca1*ca2*ee**2*complex(0,1)*sb**2*vev1)/(2.*sw**2) - (ca2*cb**2*ee**2*complex(0,1)*sa1*vev2)/(2.*cw**2) - ca2*cb**2*complex(0,1)*l2*sa1*vev2 + (ca1*ca2*cb*ee**2*complex(0,1)*sb*vev2)/(2.*cw**2) + 2*ca1*ca2*cb*complex(0,1)*l5*sb*vev2 - ca2*complex(0,1)*l3*sa1*sb**2*vev2 - ca2*complex(0,1)*l4*sa1*sb**2*vev2 + ca2*complex(0,1)*l5*sa1*sb**2*vev2 - (ca2*cb**2*ee**2*complex(0,1)*sa1*vev2)/(2.*sw**2) + (ca1*ca2*cb*ee**2*complex(0,1)*sb*vev2)/(2.*sw**2) - cb**2*complex(0,1)*l8*sa2*vevs - complex(0,1)*l7*sa2*sb**2*vevs',
                  order = {'QED':1})

GC_549 = Coupling(name = 'GC_549',
                  value = '-(ca1*ca2*cb**2*complex(0,1)*l1*vev1) - ca2*cb*complex(0,1)*l4*sa1*sb*vev1 - ca2*cb*complex(0,1)*l5*sa1*sb*vev1 - ca1*ca2*complex(0,1)*l3*sb**2*vev1 - ca2*cb**2*complex(0,1)*l3*sa1*vev2 - ca1*ca2*cb*complex(0,1)*l4*sb*vev2 - ca1*ca2*cb*complex(0,1)*l5*sb*vev2 - ca2*complex(0,1)*l2*sa1*sb**2*vev2 - cb**2*complex(0,1)*l7*sa2*vevs - complex(0,1)*l8*sa2*sb**2*vevs',
                  order = {'QED':1})

GC_550 = Coupling(name = 'GC_550',
                  value = '-(ca1*ca2*cb**2*complex(0,1)*l1*vev1) - 2*ca2*cb*complex(0,1)*l5*sa1*sb*vev1 - ca1*ca2*complex(0,1)*l3*sb**2*vev1 - ca1*ca2*complex(0,1)*l4*sb**2*vev1 + ca1*ca2*complex(0,1)*l5*sb**2*vev1 - ca2*cb**2*complex(0,1)*l3*sa1*vev2 - ca2*cb**2*complex(0,1)*l4*sa1*vev2 + ca2*cb**2*complex(0,1)*l5*sa1*vev2 - 2*ca1*ca2*cb*complex(0,1)*l5*sb*vev2 - ca2*complex(0,1)*l2*sa1*sb**2*vev2 - cb**2*complex(0,1)*l7*sa2*vevs - complex(0,1)*l8*sa2*sb**2*vevs',
                  order = {'QED':1})

GC_551 = Coupling(name = 'GC_551',
                  value = '-(ca1*ca2*cb**2*complex(0,1)*l1*vev1) - ca2*cb*complex(0,1)*l4*sa1*sb*vev1 - ca2*cb*complex(0,1)*l5*sa1*sb*vev1 - ca1*ca2*complex(0,1)*l3*sb**2*vev1 + (ca1*ca2*cb**2*ee**2*complex(0,1)*vev1)/(4.*sw**2) + (ca2*cb*ee**2*complex(0,1)*sa1*sb*vev1)/(4.*sw**2) - ca2*cb**2*complex(0,1)*l3*sa1*vev2 - ca1*ca2*cb*complex(0,1)*l4*sb*vev2 - ca1*ca2*cb*complex(0,1)*l5*sb*vev2 - ca2*complex(0,1)*l2*sa1*sb**2*vev2 + (ca1*ca2*cb*ee**2*complex(0,1)*sb*vev2)/(4.*sw**2) + (ca2*ee**2*complex(0,1)*sa1*sb**2*vev2)/(4.*sw**2) - cb**2*complex(0,1)*l7*sa2*vevs - complex(0,1)*l8*sa2*sb**2*vevs',
                  order = {'QED':1})

GC_552 = Coupling(name = 'GC_552',
                  value = '(ca1*ca2*cb**2*ee**2*complex(0,1)*vev1)/(4.*cw**2) - ca1*ca2*cb**2*complex(0,1)*l1*vev1 + (ca2*cb*ee**2*complex(0,1)*sa1*sb*vev1)/(4.*cw**2) - 2*ca2*cb*complex(0,1)*l5*sa1*sb*vev1 - ca1*ca2*complex(0,1)*l3*sb**2*vev1 - ca1*ca2*complex(0,1)*l4*sb**2*vev1 + ca1*ca2*complex(0,1)*l5*sb**2*vev1 + (ca1*ca2*cb**2*ee**2*complex(0,1)*vev1)/(4.*sw**2) + (ca2*cb*ee**2*complex(0,1)*sa1*sb*vev1)/(4.*sw**2) - ca2*cb**2*complex(0,1)*l3*sa1*vev2 - ca2*cb**2*complex(0,1)*l4*sa1*vev2 + ca2*cb**2*complex(0,1)*l5*sa1*vev2 + (ca1*ca2*cb*ee**2*complex(0,1)*sb*vev2)/(4.*cw**2) - 2*ca1*ca2*cb*complex(0,1)*l5*sb*vev2 + (ca2*ee**2*complex(0,1)*sa1*sb**2*vev2)/(4.*cw**2) - ca2*complex(0,1)*l2*sa1*sb**2*vev2 + (ca1*ca2*cb*ee**2*complex(0,1)*sb*vev2)/(4.*sw**2) + (ca2*ee**2*complex(0,1)*sa1*sb**2*vev2)/(4.*sw**2) - cb**2*complex(0,1)*l7*sa2*vevs - complex(0,1)*l8*sa2*sb**2*vevs',
                  order = {'QED':1})

GC_553 = Coupling(name = 'GC_553',
                  value = '-(ca1*ca2*cb**2*complex(0,1)*l1*vev1) - ca2*cb*complex(0,1)*l4*sa1*sb*vev1 - ca2*cb*complex(0,1)*l5*sa1*sb*vev1 - ca1*ca2*complex(0,1)*l3*sb**2*vev1 - (ca1*ca2*cb**2*ee**2*complex(0,1)*vev1)/(2.*sw**2) - (ca2*cb*ee**2*complex(0,1)*sa1*sb*vev1)/(2.*sw**2) - ca2*cb**2*complex(0,1)*l3*sa1*vev2 - ca1*ca2*cb*complex(0,1)*l4*sb*vev2 - ca1*ca2*cb*complex(0,1)*l5*sb*vev2 - ca2*complex(0,1)*l2*sa1*sb**2*vev2 - (ca1*ca2*cb*ee**2*complex(0,1)*sb*vev2)/(2.*sw**2) - (ca2*ee**2*complex(0,1)*sa1*sb**2*vev2)/(2.*sw**2) - cb**2*complex(0,1)*l7*sa2*vevs - complex(0,1)*l8*sa2*sb**2*vevs',
                  order = {'QED':1})

GC_554 = Coupling(name = 'GC_554',
                  value = '-(ca1*ca2*cb**2*ee**2*complex(0,1)*vev1)/(2.*cw**2) - ca1*ca2*cb**2*complex(0,1)*l1*vev1 - (ca2*cb*ee**2*complex(0,1)*sa1*sb*vev1)/(2.*cw**2) - 2*ca2*cb*complex(0,1)*l5*sa1*sb*vev1 - ca1*ca2*complex(0,1)*l3*sb**2*vev1 - ca1*ca2*complex(0,1)*l4*sb**2*vev1 + ca1*ca2*complex(0,1)*l5*sb**2*vev1 - (ca1*ca2*cb**2*ee**2*complex(0,1)*vev1)/(2.*sw**2) - (ca2*cb*ee**2*complex(0,1)*sa1*sb*vev1)/(2.*sw**2) - ca2*cb**2*complex(0,1)*l3*sa1*vev2 - ca2*cb**2*complex(0,1)*l4*sa1*vev2 + ca2*cb**2*complex(0,1)*l5*sa1*vev2 - (ca1*ca2*cb*ee**2*complex(0,1)*sb*vev2)/(2.*cw**2) - 2*ca1*ca2*cb*complex(0,1)*l5*sb*vev2 - (ca2*ee**2*complex(0,1)*sa1*sb**2*vev2)/(2.*cw**2) - ca2*complex(0,1)*l2*sa1*sb**2*vev2 - (ca1*ca2*cb*ee**2*complex(0,1)*sb*vev2)/(2.*sw**2) - (ca2*ee**2*complex(0,1)*sa1*sb**2*vev2)/(2.*sw**2) - cb**2*complex(0,1)*l7*sa2*vevs - complex(0,1)*l8*sa2*sb**2*vevs',
                  order = {'QED':1})

GC_555 = Coupling(name = 'GC_555',
                  value = 'ca3*cb**2*complex(0,1)*l3*sa1*vev1 + ca1*cb**2*complex(0,1)*l3*sa2*sa3*vev1 + ca1*ca3*cb*complex(0,1)*l4*sb*vev1 + ca1*ca3*cb*complex(0,1)*l5*sb*vev1 - cb*complex(0,1)*l4*sa1*sa2*sa3*sb*vev1 - cb*complex(0,1)*l5*sa1*sa2*sa3*sb*vev1 + ca3*complex(0,1)*l1*sa1*sb**2*vev1 + ca1*complex(0,1)*l1*sa2*sa3*sb**2*vev1 - ca1*ca3*cb**2*complex(0,1)*l2*vev2 + cb**2*complex(0,1)*l2*sa1*sa2*sa3*vev2 - ca3*cb*complex(0,1)*l4*sa1*sb*vev2 - ca3*cb*complex(0,1)*l5*sa1*sb*vev2 - ca1*cb*complex(0,1)*l4*sa2*sa3*sb*vev2 - ca1*cb*complex(0,1)*l5*sa2*sa3*sb*vev2 - ca1*ca3*complex(0,1)*l3*sb**2*vev2 + complex(0,1)*l3*sa1*sa2*sa3*sb**2*vev2 - ca2*cb**2*complex(0,1)*l8*sa3*vevs - ca2*complex(0,1)*l7*sa3*sb**2*vevs',
                  order = {'QED':1})

GC_556 = Coupling(name = 'GC_556',
                  value = 'ca3*cb**2*complex(0,1)*l3*sa1*vev1 + ca3*cb**2*complex(0,1)*l4*sa1*vev1 - ca3*cb**2*complex(0,1)*l5*sa1*vev1 + ca1*cb**2*complex(0,1)*l3*sa2*sa3*vev1 + ca1*cb**2*complex(0,1)*l4*sa2*sa3*vev1 - ca1*cb**2*complex(0,1)*l5*sa2*sa3*vev1 + 2*ca1*ca3*cb*complex(0,1)*l5*sb*vev1 - 2*cb*complex(0,1)*l5*sa1*sa2*sa3*sb*vev1 + ca3*complex(0,1)*l1*sa1*sb**2*vev1 + ca1*complex(0,1)*l1*sa2*sa3*sb**2*vev1 - ca1*ca3*cb**2*complex(0,1)*l2*vev2 + cb**2*complex(0,1)*l2*sa1*sa2*sa3*vev2 - 2*ca3*cb*complex(0,1)*l5*sa1*sb*vev2 - 2*ca1*cb*complex(0,1)*l5*sa2*sa3*sb*vev2 - ca1*ca3*complex(0,1)*l3*sb**2*vev2 - ca1*ca3*complex(0,1)*l4*sb**2*vev2 + ca1*ca3*complex(0,1)*l5*sb**2*vev2 + complex(0,1)*l3*sa1*sa2*sa3*sb**2*vev2 + complex(0,1)*l4*sa1*sa2*sa3*sb**2*vev2 - complex(0,1)*l5*sa1*sa2*sa3*sb**2*vev2 - ca2*cb**2*complex(0,1)*l8*sa3*vevs - ca2*complex(0,1)*l7*sa3*sb**2*vevs',
                  order = {'QED':1})

GC_557 = Coupling(name = 'GC_557',
                  value = 'ca3*cb**2*complex(0,1)*l3*sa1*vev1 + ca1*cb**2*complex(0,1)*l3*sa2*sa3*vev1 + ca1*ca3*cb*complex(0,1)*l4*sb*vev1 + ca1*ca3*cb*complex(0,1)*l5*sb*vev1 - cb*complex(0,1)*l4*sa1*sa2*sa3*sb*vev1 - cb*complex(0,1)*l5*sa1*sa2*sa3*sb*vev1 + ca3*complex(0,1)*l1*sa1*sb**2*vev1 + ca1*complex(0,1)*l1*sa2*sa3*sb**2*vev1 - (ca1*ca3*cb*ee**2*complex(0,1)*sb*vev1)/(4.*sw**2) + (cb*ee**2*complex(0,1)*sa1*sa2*sa3*sb*vev1)/(4.*sw**2) - (ca3*ee**2*complex(0,1)*sa1*sb**2*vev1)/(4.*sw**2) - (ca1*ee**2*complex(0,1)*sa2*sa3*sb**2*vev1)/(4.*sw**2) - ca1*ca3*cb**2*complex(0,1)*l2*vev2 + cb**2*complex(0,1)*l2*sa1*sa2*sa3*vev2 - ca3*cb*complex(0,1)*l4*sa1*sb*vev2 - ca3*cb*complex(0,1)*l5*sa1*sb*vev2 - ca1*cb*complex(0,1)*l4*sa2*sa3*sb*vev2 - ca1*cb*complex(0,1)*l5*sa2*sa3*sb*vev2 - ca1*ca3*complex(0,1)*l3*sb**2*vev2 + complex(0,1)*l3*sa1*sa2*sa3*sb**2*vev2 + (ca1*ca3*cb**2*ee**2*complex(0,1)*vev2)/(4.*sw**2) - (cb**2*ee**2*complex(0,1)*sa1*sa2*sa3*vev2)/(4.*sw**2) + (ca3*cb*ee**2*complex(0,1)*sa1*sb*vev2)/(4.*sw**2) + (ca1*cb*ee**2*complex(0,1)*sa2*sa3*sb*vev2)/(4.*sw**2) - ca2*cb**2*complex(0,1)*l8*sa3*vevs - ca2*complex(0,1)*l7*sa3*sb**2*vevs',
                  order = {'QED':1})

GC_558 = Coupling(name = 'GC_558',
                  value = 'ca3*cb**2*complex(0,1)*l3*sa1*vev1 + ca3*cb**2*complex(0,1)*l4*sa1*vev1 - ca3*cb**2*complex(0,1)*l5*sa1*vev1 + ca1*cb**2*complex(0,1)*l3*sa2*sa3*vev1 + ca1*cb**2*complex(0,1)*l4*sa2*sa3*vev1 - ca1*cb**2*complex(0,1)*l5*sa2*sa3*vev1 - (ca1*ca3*cb*ee**2*complex(0,1)*sb*vev1)/(4.*cw**2) + 2*ca1*ca3*cb*complex(0,1)*l5*sb*vev1 + (cb*ee**2*complex(0,1)*sa1*sa2*sa3*sb*vev1)/(4.*cw**2) - 2*cb*complex(0,1)*l5*sa1*sa2*sa3*sb*vev1 - (ca3*ee**2*complex(0,1)*sa1*sb**2*vev1)/(4.*cw**2) + ca3*complex(0,1)*l1*sa1*sb**2*vev1 - (ca1*ee**2*complex(0,1)*sa2*sa3*sb**2*vev1)/(4.*cw**2) + ca1*complex(0,1)*l1*sa2*sa3*sb**2*vev1 - (ca1*ca3*cb*ee**2*complex(0,1)*sb*vev1)/(4.*sw**2) + (cb*ee**2*complex(0,1)*sa1*sa2*sa3*sb*vev1)/(4.*sw**2) - (ca3*ee**2*complex(0,1)*sa1*sb**2*vev1)/(4.*sw**2) - (ca1*ee**2*complex(0,1)*sa2*sa3*sb**2*vev1)/(4.*sw**2) + (ca1*ca3*cb**2*ee**2*complex(0,1)*vev2)/(4.*cw**2) - ca1*ca3*cb**2*complex(0,1)*l2*vev2 - (cb**2*ee**2*complex(0,1)*sa1*sa2*sa3*vev2)/(4.*cw**2) + cb**2*complex(0,1)*l2*sa1*sa2*sa3*vev2 + (ca3*cb*ee**2*complex(0,1)*sa1*sb*vev2)/(4.*cw**2) - 2*ca3*cb*complex(0,1)*l5*sa1*sb*vev2 + (ca1*cb*ee**2*complex(0,1)*sa2*sa3*sb*vev2)/(4.*cw**2) - 2*ca1*cb*complex(0,1)*l5*sa2*sa3*sb*vev2 - ca1*ca3*complex(0,1)*l3*sb**2*vev2 - ca1*ca3*complex(0,1)*l4*sb**2*vev2 + ca1*ca3*complex(0,1)*l5*sb**2*vev2 + complex(0,1)*l3*sa1*sa2*sa3*sb**2*vev2 + complex(0,1)*l4*sa1*sa2*sa3*sb**2*vev2 - complex(0,1)*l5*sa1*sa2*sa3*sb**2*vev2 + (ca1*ca3*cb**2*ee**2*complex(0,1)*vev2)/(4.*sw**2) - (cb**2*ee**2*complex(0,1)*sa1*sa2*sa3*vev2)/(4.*sw**2) + (ca3*cb*ee**2*complex(0,1)*sa1*sb*vev2)/(4.*sw**2) + (ca1*cb*ee**2*complex(0,1)*sa2*sa3*sb*vev2)/(4.*sw**2) - ca2*cb**2*complex(0,1)*l8*sa3*vevs - ca2*complex(0,1)*l7*sa3*sb**2*vevs',
                  order = {'QED':1})

GC_559 = Coupling(name = 'GC_559',
                  value = 'ca3*cb**2*complex(0,1)*l3*sa1*vev1 + ca1*cb**2*complex(0,1)*l3*sa2*sa3*vev1 + ca1*ca3*cb*complex(0,1)*l4*sb*vev1 + ca1*ca3*cb*complex(0,1)*l5*sb*vev1 - cb*complex(0,1)*l4*sa1*sa2*sa3*sb*vev1 - cb*complex(0,1)*l5*sa1*sa2*sa3*sb*vev1 + ca3*complex(0,1)*l1*sa1*sb**2*vev1 + ca1*complex(0,1)*l1*sa2*sa3*sb**2*vev1 + (ca1*ca3*cb*ee**2*complex(0,1)*sb*vev1)/(2.*sw**2) - (cb*ee**2*complex(0,1)*sa1*sa2*sa3*sb*vev1)/(2.*sw**2) + (ca3*ee**2*complex(0,1)*sa1*sb**2*vev1)/(2.*sw**2) + (ca1*ee**2*complex(0,1)*sa2*sa3*sb**2*vev1)/(2.*sw**2) - ca1*ca3*cb**2*complex(0,1)*l2*vev2 + cb**2*complex(0,1)*l2*sa1*sa2*sa3*vev2 - ca3*cb*complex(0,1)*l4*sa1*sb*vev2 - ca3*cb*complex(0,1)*l5*sa1*sb*vev2 - ca1*cb*complex(0,1)*l4*sa2*sa3*sb*vev2 - ca1*cb*complex(0,1)*l5*sa2*sa3*sb*vev2 - ca1*ca3*complex(0,1)*l3*sb**2*vev2 + complex(0,1)*l3*sa1*sa2*sa3*sb**2*vev2 - (ca1*ca3*cb**2*ee**2*complex(0,1)*vev2)/(2.*sw**2) + (cb**2*ee**2*complex(0,1)*sa1*sa2*sa3*vev2)/(2.*sw**2) - (ca3*cb*ee**2*complex(0,1)*sa1*sb*vev2)/(2.*sw**2) - (ca1*cb*ee**2*complex(0,1)*sa2*sa3*sb*vev2)/(2.*sw**2) - ca2*cb**2*complex(0,1)*l8*sa3*vevs - ca2*complex(0,1)*l7*sa3*sb**2*vevs',
                  order = {'QED':1})

GC_560 = Coupling(name = 'GC_560',
                  value = 'ca3*cb**2*complex(0,1)*l3*sa1*vev1 + ca3*cb**2*complex(0,1)*l4*sa1*vev1 - ca3*cb**2*complex(0,1)*l5*sa1*vev1 + ca1*cb**2*complex(0,1)*l3*sa2*sa3*vev1 + ca1*cb**2*complex(0,1)*l4*sa2*sa3*vev1 - ca1*cb**2*complex(0,1)*l5*sa2*sa3*vev1 + (ca1*ca3*cb*ee**2*complex(0,1)*sb*vev1)/(2.*cw**2) + 2*ca1*ca3*cb*complex(0,1)*l5*sb*vev1 - (cb*ee**2*complex(0,1)*sa1*sa2*sa3*sb*vev1)/(2.*cw**2) - 2*cb*complex(0,1)*l5*sa1*sa2*sa3*sb*vev1 + (ca3*ee**2*complex(0,1)*sa1*sb**2*vev1)/(2.*cw**2) + ca3*complex(0,1)*l1*sa1*sb**2*vev1 + (ca1*ee**2*complex(0,1)*sa2*sa3*sb**2*vev1)/(2.*cw**2) + ca1*complex(0,1)*l1*sa2*sa3*sb**2*vev1 + (ca1*ca3*cb*ee**2*complex(0,1)*sb*vev1)/(2.*sw**2) - (cb*ee**2*complex(0,1)*sa1*sa2*sa3*sb*vev1)/(2.*sw**2) + (ca3*ee**2*complex(0,1)*sa1*sb**2*vev1)/(2.*sw**2) + (ca1*ee**2*complex(0,1)*sa2*sa3*sb**2*vev1)/(2.*sw**2) - (ca1*ca3*cb**2*ee**2*complex(0,1)*vev2)/(2.*cw**2) - ca1*ca3*cb**2*complex(0,1)*l2*vev2 + (cb**2*ee**2*complex(0,1)*sa1*sa2*sa3*vev2)/(2.*cw**2) + cb**2*complex(0,1)*l2*sa1*sa2*sa3*vev2 - (ca3*cb*ee**2*complex(0,1)*sa1*sb*vev2)/(2.*cw**2) - 2*ca3*cb*complex(0,1)*l5*sa1*sb*vev2 - (ca1*cb*ee**2*complex(0,1)*sa2*sa3*sb*vev2)/(2.*cw**2) - 2*ca1*cb*complex(0,1)*l5*sa2*sa3*sb*vev2 - ca1*ca3*complex(0,1)*l3*sb**2*vev2 - ca1*ca3*complex(0,1)*l4*sb**2*vev2 + ca1*ca3*complex(0,1)*l5*sb**2*vev2 + complex(0,1)*l3*sa1*sa2*sa3*sb**2*vev2 + complex(0,1)*l4*sa1*sa2*sa3*sb**2*vev2 - complex(0,1)*l5*sa1*sa2*sa3*sb**2*vev2 - (ca1*ca3*cb**2*ee**2*complex(0,1)*vev2)/(2.*sw**2) + (cb**2*ee**2*complex(0,1)*sa1*sa2*sa3*vev2)/(2.*sw**2) - (ca3*cb*ee**2*complex(0,1)*sa1*sb*vev2)/(2.*sw**2) - (ca1*cb*ee**2*complex(0,1)*sa2*sa3*sb*vev2)/(2.*sw**2) - ca2*cb**2*complex(0,1)*l8*sa3*vevs - ca2*complex(0,1)*l7*sa3*sb**2*vevs',
                  order = {'QED':1})

GC_561 = Coupling(name = 'GC_561',
                  value = 'ca3*cb**2*complex(0,1)*l1*sa1*vev1 + ca1*cb**2*complex(0,1)*l1*sa2*sa3*vev1 - ca1*ca3*cb*complex(0,1)*l4*sb*vev1 - ca1*ca3*cb*complex(0,1)*l5*sb*vev1 + cb*complex(0,1)*l4*sa1*sa2*sa3*sb*vev1 + cb*complex(0,1)*l5*sa1*sa2*sa3*sb*vev1 + ca3*complex(0,1)*l3*sa1*sb**2*vev1 + ca1*complex(0,1)*l3*sa2*sa3*sb**2*vev1 - ca1*ca3*cb**2*complex(0,1)*l3*vev2 + cb**2*complex(0,1)*l3*sa1*sa2*sa3*vev2 + ca3*cb*complex(0,1)*l4*sa1*sb*vev2 + ca3*cb*complex(0,1)*l5*sa1*sb*vev2 + ca1*cb*complex(0,1)*l4*sa2*sa3*sb*vev2 + ca1*cb*complex(0,1)*l5*sa2*sa3*sb*vev2 - ca1*ca3*complex(0,1)*l2*sb**2*vev2 + complex(0,1)*l2*sa1*sa2*sa3*sb**2*vev2 - ca2*cb**2*complex(0,1)*l7*sa3*vevs - ca2*complex(0,1)*l8*sa3*sb**2*vevs',
                  order = {'QED':1})

GC_562 = Coupling(name = 'GC_562',
                  value = 'ca3*cb**2*complex(0,1)*l1*sa1*vev1 + ca1*cb**2*complex(0,1)*l1*sa2*sa3*vev1 - 2*ca1*ca3*cb*complex(0,1)*l5*sb*vev1 + 2*cb*complex(0,1)*l5*sa1*sa2*sa3*sb*vev1 + ca3*complex(0,1)*l3*sa1*sb**2*vev1 + ca3*complex(0,1)*l4*sa1*sb**2*vev1 - ca3*complex(0,1)*l5*sa1*sb**2*vev1 + ca1*complex(0,1)*l3*sa2*sa3*sb**2*vev1 + ca1*complex(0,1)*l4*sa2*sa3*sb**2*vev1 - ca1*complex(0,1)*l5*sa2*sa3*sb**2*vev1 - ca1*ca3*cb**2*complex(0,1)*l3*vev2 - ca1*ca3*cb**2*complex(0,1)*l4*vev2 + ca1*ca3*cb**2*complex(0,1)*l5*vev2 + cb**2*complex(0,1)*l3*sa1*sa2*sa3*vev2 + cb**2*complex(0,1)*l4*sa1*sa2*sa3*vev2 - cb**2*complex(0,1)*l5*sa1*sa2*sa3*vev2 + 2*ca3*cb*complex(0,1)*l5*sa1*sb*vev2 + 2*ca1*cb*complex(0,1)*l5*sa2*sa3*sb*vev2 - ca1*ca3*complex(0,1)*l2*sb**2*vev2 + complex(0,1)*l2*sa1*sa2*sa3*sb**2*vev2 - ca2*cb**2*complex(0,1)*l7*sa3*vevs - ca2*complex(0,1)*l8*sa3*sb**2*vevs',
                  order = {'QED':1})

GC_563 = Coupling(name = 'GC_563',
                  value = 'ca3*cb**2*complex(0,1)*l1*sa1*vev1 + ca1*cb**2*complex(0,1)*l1*sa2*sa3*vev1 - ca1*ca3*cb*complex(0,1)*l4*sb*vev1 - ca1*ca3*cb*complex(0,1)*l5*sb*vev1 + cb*complex(0,1)*l4*sa1*sa2*sa3*sb*vev1 + cb*complex(0,1)*l5*sa1*sa2*sa3*sb*vev1 + ca3*complex(0,1)*l3*sa1*sb**2*vev1 + ca1*complex(0,1)*l3*sa2*sa3*sb**2*vev1 - (ca3*cb**2*ee**2*complex(0,1)*sa1*vev1)/(4.*sw**2) - (ca1*cb**2*ee**2*complex(0,1)*sa2*sa3*vev1)/(4.*sw**2) + (ca1*ca3*cb*ee**2*complex(0,1)*sb*vev1)/(4.*sw**2) - (cb*ee**2*complex(0,1)*sa1*sa2*sa3*sb*vev1)/(4.*sw**2) - ca1*ca3*cb**2*complex(0,1)*l3*vev2 + cb**2*complex(0,1)*l3*sa1*sa2*sa3*vev2 + ca3*cb*complex(0,1)*l4*sa1*sb*vev2 + ca3*cb*complex(0,1)*l5*sa1*sb*vev2 + ca1*cb*complex(0,1)*l4*sa2*sa3*sb*vev2 + ca1*cb*complex(0,1)*l5*sa2*sa3*sb*vev2 - ca1*ca3*complex(0,1)*l2*sb**2*vev2 + complex(0,1)*l2*sa1*sa2*sa3*sb**2*vev2 - (ca3*cb*ee**2*complex(0,1)*sa1*sb*vev2)/(4.*sw**2) - (ca1*cb*ee**2*complex(0,1)*sa2*sa3*sb*vev2)/(4.*sw**2) + (ca1*ca3*ee**2*complex(0,1)*sb**2*vev2)/(4.*sw**2) - (ee**2*complex(0,1)*sa1*sa2*sa3*sb**2*vev2)/(4.*sw**2) - ca2*cb**2*complex(0,1)*l7*sa3*vevs - ca2*complex(0,1)*l8*sa3*sb**2*vevs',
                  order = {'QED':1})

GC_564 = Coupling(name = 'GC_564',
                  value = '-(ca3*cb**2*ee**2*complex(0,1)*sa1*vev1)/(4.*cw**2) + ca3*cb**2*complex(0,1)*l1*sa1*vev1 - (ca1*cb**2*ee**2*complex(0,1)*sa2*sa3*vev1)/(4.*cw**2) + ca1*cb**2*complex(0,1)*l1*sa2*sa3*vev1 + (ca1*ca3*cb*ee**2*complex(0,1)*sb*vev1)/(4.*cw**2) - 2*ca1*ca3*cb*complex(0,1)*l5*sb*vev1 - (cb*ee**2*complex(0,1)*sa1*sa2*sa3*sb*vev1)/(4.*cw**2) + 2*cb*complex(0,1)*l5*sa1*sa2*sa3*sb*vev1 + ca3*complex(0,1)*l3*sa1*sb**2*vev1 + ca3*complex(0,1)*l4*sa1*sb**2*vev1 - ca3*complex(0,1)*l5*sa1*sb**2*vev1 + ca1*complex(0,1)*l3*sa2*sa3*sb**2*vev1 + ca1*complex(0,1)*l4*sa2*sa3*sb**2*vev1 - ca1*complex(0,1)*l5*sa2*sa3*sb**2*vev1 - (ca3*cb**2*ee**2*complex(0,1)*sa1*vev1)/(4.*sw**2) - (ca1*cb**2*ee**2*complex(0,1)*sa2*sa3*vev1)/(4.*sw**2) + (ca1*ca3*cb*ee**2*complex(0,1)*sb*vev1)/(4.*sw**2) - (cb*ee**2*complex(0,1)*sa1*sa2*sa3*sb*vev1)/(4.*sw**2) - ca1*ca3*cb**2*complex(0,1)*l3*vev2 - ca1*ca3*cb**2*complex(0,1)*l4*vev2 + ca1*ca3*cb**2*complex(0,1)*l5*vev2 + cb**2*complex(0,1)*l3*sa1*sa2*sa3*vev2 + cb**2*complex(0,1)*l4*sa1*sa2*sa3*vev2 - cb**2*complex(0,1)*l5*sa1*sa2*sa3*vev2 - (ca3*cb*ee**2*complex(0,1)*sa1*sb*vev2)/(4.*cw**2) + 2*ca3*cb*complex(0,1)*l5*sa1*sb*vev2 - (ca1*cb*ee**2*complex(0,1)*sa2*sa3*sb*vev2)/(4.*cw**2) + 2*ca1*cb*complex(0,1)*l5*sa2*sa3*sb*vev2 + (ca1*ca3*ee**2*complex(0,1)*sb**2*vev2)/(4.*cw**2) - ca1*ca3*complex(0,1)*l2*sb**2*vev2 - (ee**2*complex(0,1)*sa1*sa2*sa3*sb**2*vev2)/(4.*cw**2) + complex(0,1)*l2*sa1*sa2*sa3*sb**2*vev2 - (ca3*cb*ee**2*complex(0,1)*sa1*sb*vev2)/(4.*sw**2) - (ca1*cb*ee**2*complex(0,1)*sa2*sa3*sb*vev2)/(4.*sw**2) + (ca1*ca3*ee**2*complex(0,1)*sb**2*vev2)/(4.*sw**2) - (ee**2*complex(0,1)*sa1*sa2*sa3*sb**2*vev2)/(4.*sw**2) - ca2*cb**2*complex(0,1)*l7*sa3*vevs - ca2*complex(0,1)*l8*sa3*sb**2*vevs',
                  order = {'QED':1})

GC_565 = Coupling(name = 'GC_565',
                  value = 'ca3*cb**2*complex(0,1)*l1*sa1*vev1 + ca1*cb**2*complex(0,1)*l1*sa2*sa3*vev1 - ca1*ca3*cb*complex(0,1)*l4*sb*vev1 - ca1*ca3*cb*complex(0,1)*l5*sb*vev1 + cb*complex(0,1)*l4*sa1*sa2*sa3*sb*vev1 + cb*complex(0,1)*l5*sa1*sa2*sa3*sb*vev1 + ca3*complex(0,1)*l3*sa1*sb**2*vev1 + ca1*complex(0,1)*l3*sa2*sa3*sb**2*vev1 + (ca3*cb**2*ee**2*complex(0,1)*sa1*vev1)/(2.*sw**2) + (ca1*cb**2*ee**2*complex(0,1)*sa2*sa3*vev1)/(2.*sw**2) - (ca1*ca3*cb*ee**2*complex(0,1)*sb*vev1)/(2.*sw**2) + (cb*ee**2*complex(0,1)*sa1*sa2*sa3*sb*vev1)/(2.*sw**2) - ca1*ca3*cb**2*complex(0,1)*l3*vev2 + cb**2*complex(0,1)*l3*sa1*sa2*sa3*vev2 + ca3*cb*complex(0,1)*l4*sa1*sb*vev2 + ca3*cb*complex(0,1)*l5*sa1*sb*vev2 + ca1*cb*complex(0,1)*l4*sa2*sa3*sb*vev2 + ca1*cb*complex(0,1)*l5*sa2*sa3*sb*vev2 - ca1*ca3*complex(0,1)*l2*sb**2*vev2 + complex(0,1)*l2*sa1*sa2*sa3*sb**2*vev2 + (ca3*cb*ee**2*complex(0,1)*sa1*sb*vev2)/(2.*sw**2) + (ca1*cb*ee**2*complex(0,1)*sa2*sa3*sb*vev2)/(2.*sw**2) - (ca1*ca3*ee**2*complex(0,1)*sb**2*vev2)/(2.*sw**2) + (ee**2*complex(0,1)*sa1*sa2*sa3*sb**2*vev2)/(2.*sw**2) - ca2*cb**2*complex(0,1)*l7*sa3*vevs - ca2*complex(0,1)*l8*sa3*sb**2*vevs',
                  order = {'QED':1})

GC_566 = Coupling(name = 'GC_566',
                  value = '(ca3*cb**2*ee**2*complex(0,1)*sa1*vev1)/(2.*cw**2) + ca3*cb**2*complex(0,1)*l1*sa1*vev1 + (ca1*cb**2*ee**2*complex(0,1)*sa2*sa3*vev1)/(2.*cw**2) + ca1*cb**2*complex(0,1)*l1*sa2*sa3*vev1 - (ca1*ca3*cb*ee**2*complex(0,1)*sb*vev1)/(2.*cw**2) - 2*ca1*ca3*cb*complex(0,1)*l5*sb*vev1 + (cb*ee**2*complex(0,1)*sa1*sa2*sa3*sb*vev1)/(2.*cw**2) + 2*cb*complex(0,1)*l5*sa1*sa2*sa3*sb*vev1 + ca3*complex(0,1)*l3*sa1*sb**2*vev1 + ca3*complex(0,1)*l4*sa1*sb**2*vev1 - ca3*complex(0,1)*l5*sa1*sb**2*vev1 + ca1*complex(0,1)*l3*sa2*sa3*sb**2*vev1 + ca1*complex(0,1)*l4*sa2*sa3*sb**2*vev1 - ca1*complex(0,1)*l5*sa2*sa3*sb**2*vev1 + (ca3*cb**2*ee**2*complex(0,1)*sa1*vev1)/(2.*sw**2) + (ca1*cb**2*ee**2*complex(0,1)*sa2*sa3*vev1)/(2.*sw**2) - (ca1*ca3*cb*ee**2*complex(0,1)*sb*vev1)/(2.*sw**2) + (cb*ee**2*complex(0,1)*sa1*sa2*sa3*sb*vev1)/(2.*sw**2) - ca1*ca3*cb**2*complex(0,1)*l3*vev2 - ca1*ca3*cb**2*complex(0,1)*l4*vev2 + ca1*ca3*cb**2*complex(0,1)*l5*vev2 + cb**2*complex(0,1)*l3*sa1*sa2*sa3*vev2 + cb**2*complex(0,1)*l4*sa1*sa2*sa3*vev2 - cb**2*complex(0,1)*l5*sa1*sa2*sa3*vev2 + (ca3*cb*ee**2*complex(0,1)*sa1*sb*vev2)/(2.*cw**2) + 2*ca3*cb*complex(0,1)*l5*sa1*sb*vev2 + (ca1*cb*ee**2*complex(0,1)*sa2*sa3*sb*vev2)/(2.*cw**2) + 2*ca1*cb*complex(0,1)*l5*sa2*sa3*sb*vev2 - (ca1*ca3*ee**2*complex(0,1)*sb**2*vev2)/(2.*cw**2) - ca1*ca3*complex(0,1)*l2*sb**2*vev2 + (ee**2*complex(0,1)*sa1*sa2*sa3*sb**2*vev2)/(2.*cw**2) + complex(0,1)*l2*sa1*sa2*sa3*sb**2*vev2 + (ca3*cb*ee**2*complex(0,1)*sa1*sb*vev2)/(2.*sw**2) + (ca1*cb*ee**2*complex(0,1)*sa2*sa3*sb*vev2)/(2.*sw**2) - (ca1*ca3*ee**2*complex(0,1)*sb**2*vev2)/(2.*sw**2) + (ee**2*complex(0,1)*sa1*sa2*sa3*sb**2*vev2)/(2.*sw**2) - ca2*cb**2*complex(0,1)*l7*sa3*vevs - ca2*complex(0,1)*l8*sa3*sb**2*vevs',
                  order = {'QED':1})

GC_567 = Coupling(name = 'GC_567',
                  value = '-(complex(0,1)*sb*yc1) + cb*complex(0,1)*yc2',
                  order = {'QED':1})

GC_568 = Coupling(name = 'GC_568',
                  value = '(sb*yc1)/cmath.sqrt(2) - (cb*yc2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_569 = Coupling(name = 'GC_569',
                  value = '-((sb*yc1)/cmath.sqrt(2)) + (cb*yc2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_570 = Coupling(name = 'GC_570',
                  value = '-((ca1*ca2*complex(0,1)*yc1)/cmath.sqrt(2)) - (ca2*complex(0,1)*sa1*yc2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_571 = Coupling(name = 'GC_571',
                  value = '(ca1*ca3*complex(0,1)*sa2*yc1)/cmath.sqrt(2) - (complex(0,1)*sa1*sa3*yc1)/cmath.sqrt(2) + (ca3*complex(0,1)*sa1*sa2*yc2)/cmath.sqrt(2) + (ca1*complex(0,1)*sa3*yc2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_572 = Coupling(name = 'GC_572',
                  value = '(ca3*complex(0,1)*sa1*yc1)/cmath.sqrt(2) + (ca1*complex(0,1)*sa2*sa3*yc1)/cmath.sqrt(2) - (ca1*ca3*complex(0,1)*yc2)/cmath.sqrt(2) + (complex(0,1)*sa1*sa2*sa3*yc2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_573 = Coupling(name = 'GC_573',
                  value = 'cb*complex(0,1)*yc1 + complex(0,1)*sb*yc2',
                  order = {'QED':1})

GC_574 = Coupling(name = 'GC_574',
                  value = '-((cb*yc1)/cmath.sqrt(2)) - (sb*yc2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_575 = Coupling(name = 'GC_575',
                  value = '(cb*yc1)/cmath.sqrt(2) + (sb*yc2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_576 = Coupling(name = 'GC_576',
                  value = '-((ca2*complex(0,1)*sa1*yb2)/cmath.sqrt(2)) - (ca1*ca2*complex(0,1)*yd13x3)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_577 = Coupling(name = 'GC_577',
                  value = '-(complex(0,1)*sb*yb2) - cb*complex(0,1)*yd13x3',
                  order = {'QED':1})

GC_578 = Coupling(name = 'GC_578',
                  value = '-((sb*yb2)/cmath.sqrt(2)) - (cb*yd13x3)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_579 = Coupling(name = 'GC_579',
                  value = '(sb*yb2)/cmath.sqrt(2) + (cb*yd13x3)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_580 = Coupling(name = 'GC_580',
                  value = '(ca3*complex(0,1)*sa1*sa2*yb2)/cmath.sqrt(2) + (ca1*complex(0,1)*sa3*yb2)/cmath.sqrt(2) + (ca1*ca3*complex(0,1)*sa2*yd13x3)/cmath.sqrt(2) - (complex(0,1)*sa1*sa3*yd13x3)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_581 = Coupling(name = 'GC_581',
                  value = '-((ca1*ca3*complex(0,1)*yb2)/cmath.sqrt(2)) + (complex(0,1)*sa1*sa2*sa3*yb2)/cmath.sqrt(2) + (ca3*complex(0,1)*sa1*yd13x3)/cmath.sqrt(2) + (ca1*complex(0,1)*sa2*sa3*yd13x3)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_582 = Coupling(name = 'GC_582',
                  value = '-(cb*complex(0,1)*yb2) + complex(0,1)*sb*yd13x3',
                  order = {'QED':1})

GC_583 = Coupling(name = 'GC_583',
                  value = '(cb*yb2)/cmath.sqrt(2) - (sb*yd13x3)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_584 = Coupling(name = 'GC_584',
                  value = '-((cb*yb2)/cmath.sqrt(2)) + (sb*yd13x3)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_585 = Coupling(name = 'GC_585',
                  value = 'complex(0,1)*sb*ydo1 - cb*complex(0,1)*ydo2',
                  order = {'QED':1})

GC_586 = Coupling(name = 'GC_586',
                  value = '(sb*ydo1)/cmath.sqrt(2) - (cb*ydo2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_587 = Coupling(name = 'GC_587',
                  value = '-((sb*ydo1)/cmath.sqrt(2)) + (cb*ydo2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_588 = Coupling(name = 'GC_588',
                  value = '-((ca1*ca2*complex(0,1)*ydo1)/cmath.sqrt(2)) - (ca2*complex(0,1)*sa1*ydo2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_589 = Coupling(name = 'GC_589',
                  value = '(ca1*ca3*complex(0,1)*sa2*ydo1)/cmath.sqrt(2) - (complex(0,1)*sa1*sa3*ydo1)/cmath.sqrt(2) + (ca3*complex(0,1)*sa1*sa2*ydo2)/cmath.sqrt(2) + (ca1*complex(0,1)*sa3*ydo2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_590 = Coupling(name = 'GC_590',
                  value = '(ca3*complex(0,1)*sa1*ydo1)/cmath.sqrt(2) + (ca1*complex(0,1)*sa2*sa3*ydo1)/cmath.sqrt(2) - (ca1*ca3*complex(0,1)*ydo2)/cmath.sqrt(2) + (complex(0,1)*sa1*sa2*sa3*ydo2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_591 = Coupling(name = 'GC_591',
                  value = '-(cb*complex(0,1)*ydo1) - complex(0,1)*sb*ydo2',
                  order = {'QED':1})

GC_592 = Coupling(name = 'GC_592',
                  value = '-((cb*ydo1)/cmath.sqrt(2)) - (sb*ydo2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_593 = Coupling(name = 'GC_593',
                  value = '(cb*ydo1)/cmath.sqrt(2) + (sb*ydo2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_594 = Coupling(name = 'GC_594',
                  value = 'complex(0,1)*sb*ye1 - cb*complex(0,1)*ye2',
                  order = {'QED':1})

GC_595 = Coupling(name = 'GC_595',
                  value = '(sb*ye1)/cmath.sqrt(2) - (cb*ye2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_596 = Coupling(name = 'GC_596',
                  value = '-((sb*ye1)/cmath.sqrt(2)) + (cb*ye2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_597 = Coupling(name = 'GC_597',
                  value = '-((ca1*ca2*complex(0,1)*ye1)/cmath.sqrt(2)) - (ca2*complex(0,1)*sa1*ye2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_598 = Coupling(name = 'GC_598',
                  value = '(ca1*ca3*complex(0,1)*sa2*ye1)/cmath.sqrt(2) - (complex(0,1)*sa1*sa3*ye1)/cmath.sqrt(2) + (ca3*complex(0,1)*sa1*sa2*ye2)/cmath.sqrt(2) + (ca1*complex(0,1)*sa3*ye2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_599 = Coupling(name = 'GC_599',
                  value = '(ca3*complex(0,1)*sa1*ye1)/cmath.sqrt(2) + (ca1*complex(0,1)*sa2*sa3*ye1)/cmath.sqrt(2) - (ca1*ca3*complex(0,1)*ye2)/cmath.sqrt(2) + (complex(0,1)*sa1*sa2*sa3*ye2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_600 = Coupling(name = 'GC_600',
                  value = '-(cb*complex(0,1)*ye1) - complex(0,1)*sb*ye2',
                  order = {'QED':1})

GC_601 = Coupling(name = 'GC_601',
                  value = '-((cb*ye1)/cmath.sqrt(2)) - (sb*ye2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_602 = Coupling(name = 'GC_602',
                  value = '(cb*ye1)/cmath.sqrt(2) + (sb*ye2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_603 = Coupling(name = 'GC_603',
                  value = 'complex(0,1)*sb*ym1 - cb*complex(0,1)*ym2',
                  order = {'QED':1})

GC_604 = Coupling(name = 'GC_604',
                  value = '(sb*ym1)/cmath.sqrt(2) - (cb*ym2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_605 = Coupling(name = 'GC_605',
                  value = '-((sb*ym1)/cmath.sqrt(2)) + (cb*ym2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_606 = Coupling(name = 'GC_606',
                  value = '-((ca1*ca2*complex(0,1)*ym1)/cmath.sqrt(2)) - (ca2*complex(0,1)*sa1*ym2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_607 = Coupling(name = 'GC_607',
                  value = '(ca1*ca3*complex(0,1)*sa2*ym1)/cmath.sqrt(2) - (complex(0,1)*sa1*sa3*ym1)/cmath.sqrt(2) + (ca3*complex(0,1)*sa1*sa2*ym2)/cmath.sqrt(2) + (ca1*complex(0,1)*sa3*ym2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_608 = Coupling(name = 'GC_608',
                  value = '(ca3*complex(0,1)*sa1*ym1)/cmath.sqrt(2) + (ca1*complex(0,1)*sa2*sa3*ym1)/cmath.sqrt(2) - (ca1*ca3*complex(0,1)*ym2)/cmath.sqrt(2) + (complex(0,1)*sa1*sa2*sa3*ym2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_609 = Coupling(name = 'GC_609',
                  value = '-(cb*complex(0,1)*ym1) - complex(0,1)*sb*ym2',
                  order = {'QED':1})

GC_610 = Coupling(name = 'GC_610',
                  value = '-((cb*ym1)/cmath.sqrt(2)) - (sb*ym2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_611 = Coupling(name = 'GC_611',
                  value = '(cb*ym1)/cmath.sqrt(2) + (sb*ym2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_612 = Coupling(name = 'GC_612',
                  value = 'complex(0,1)*sb*yd12x2 - cb*complex(0,1)*ys2',
                  order = {'QED':1})

GC_613 = Coupling(name = 'GC_613',
                  value = '(sb*yd12x2)/cmath.sqrt(2) - (cb*ys2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_614 = Coupling(name = 'GC_614',
                  value = '-((sb*yd12x2)/cmath.sqrt(2)) + (cb*ys2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_615 = Coupling(name = 'GC_615',
                  value = '-((ca1*ca2*complex(0,1)*yd12x2)/cmath.sqrt(2)) - (ca2*complex(0,1)*sa1*ys2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_616 = Coupling(name = 'GC_616',
                  value = '(ca1*ca3*complex(0,1)*sa2*yd12x2)/cmath.sqrt(2) - (complex(0,1)*sa1*sa3*yd12x2)/cmath.sqrt(2) + (ca3*complex(0,1)*sa1*sa2*ys2)/cmath.sqrt(2) + (ca1*complex(0,1)*sa3*ys2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_617 = Coupling(name = 'GC_617',
                  value = '(ca3*complex(0,1)*sa1*yd12x2)/cmath.sqrt(2) + (ca1*complex(0,1)*sa2*sa3*yd12x2)/cmath.sqrt(2) - (ca1*ca3*complex(0,1)*ys2)/cmath.sqrt(2) + (complex(0,1)*sa1*sa2*sa3*ys2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_618 = Coupling(name = 'GC_618',
                  value = '-(cb*complex(0,1)*yd12x2) - complex(0,1)*sb*ys2',
                  order = {'QED':1})

GC_619 = Coupling(name = 'GC_619',
                  value = '-((cb*yd12x2)/cmath.sqrt(2)) - (sb*ys2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_620 = Coupling(name = 'GC_620',
                  value = '(cb*yd12x2)/cmath.sqrt(2) + (sb*ys2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_621 = Coupling(name = 'GC_621',
                  value = '-(complex(0,1)*sb*yt1) + cb*complex(0,1)*yt2',
                  order = {'QED':1})

GC_622 = Coupling(name = 'GC_622',
                  value = '(sb*yt1)/cmath.sqrt(2) - (cb*yt2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_623 = Coupling(name = 'GC_623',
                  value = '-((sb*yt1)/cmath.sqrt(2)) + (cb*yt2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_624 = Coupling(name = 'GC_624',
                  value = '-((ca1*ca2*complex(0,1)*yt1)/cmath.sqrt(2)) - (ca2*complex(0,1)*sa1*yt2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_625 = Coupling(name = 'GC_625',
                  value = '(ca1*ca3*complex(0,1)*sa2*yt1)/cmath.sqrt(2) - (complex(0,1)*sa1*sa3*yt1)/cmath.sqrt(2) + (ca3*complex(0,1)*sa1*sa2*yt2)/cmath.sqrt(2) + (ca1*complex(0,1)*sa3*yt2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_626 = Coupling(name = 'GC_626',
                  value = '(ca3*complex(0,1)*sa1*yt1)/cmath.sqrt(2) + (ca1*complex(0,1)*sa2*sa3*yt1)/cmath.sqrt(2) - (ca1*ca3*complex(0,1)*yt2)/cmath.sqrt(2) + (complex(0,1)*sa1*sa2*sa3*yt2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_627 = Coupling(name = 'GC_627',
                  value = 'cb*complex(0,1)*yt1 + complex(0,1)*sb*yt2',
                  order = {'QED':1})

GC_628 = Coupling(name = 'GC_628',
                  value = '-((cb*yt1)/cmath.sqrt(2)) - (sb*yt2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_629 = Coupling(name = 'GC_629',
                  value = '(cb*yt1)/cmath.sqrt(2) + (sb*yt2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_630 = Coupling(name = 'GC_630',
                  value = 'complex(0,1)*sb*ytau1 - cb*complex(0,1)*ytau2',
                  order = {'QED':1})

GC_631 = Coupling(name = 'GC_631',
                  value = '(sb*ytau1)/cmath.sqrt(2) - (cb*ytau2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_632 = Coupling(name = 'GC_632',
                  value = '-((sb*ytau1)/cmath.sqrt(2)) + (cb*ytau2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_633 = Coupling(name = 'GC_633',
                  value = '-((ca1*ca2*complex(0,1)*ytau1)/cmath.sqrt(2)) - (ca2*complex(0,1)*sa1*ytau2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_634 = Coupling(name = 'GC_634',
                  value = '(ca1*ca3*complex(0,1)*sa2*ytau1)/cmath.sqrt(2) - (complex(0,1)*sa1*sa3*ytau1)/cmath.sqrt(2) + (ca3*complex(0,1)*sa1*sa2*ytau2)/cmath.sqrt(2) + (ca1*complex(0,1)*sa3*ytau2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_635 = Coupling(name = 'GC_635',
                  value = '(ca3*complex(0,1)*sa1*ytau1)/cmath.sqrt(2) + (ca1*complex(0,1)*sa2*sa3*ytau1)/cmath.sqrt(2) - (ca1*ca3*complex(0,1)*ytau2)/cmath.sqrt(2) + (complex(0,1)*sa1*sa2*sa3*ytau2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_636 = Coupling(name = 'GC_636',
                  value = '-(cb*complex(0,1)*ytau1) - complex(0,1)*sb*ytau2',
                  order = {'QED':1})

GC_637 = Coupling(name = 'GC_637',
                  value = '-((cb*ytau1)/cmath.sqrt(2)) - (sb*ytau2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_638 = Coupling(name = 'GC_638',
                  value = '(cb*ytau1)/cmath.sqrt(2) + (sb*ytau2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_639 = Coupling(name = 'GC_639',
                  value = '-(complex(0,1)*sb*yup1) + cb*complex(0,1)*yup2',
                  order = {'QED':1})

GC_640 = Coupling(name = 'GC_640',
                  value = '(sb*yup1)/cmath.sqrt(2) - (cb*yup2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_641 = Coupling(name = 'GC_641',
                  value = '-((sb*yup1)/cmath.sqrt(2)) + (cb*yup2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_642 = Coupling(name = 'GC_642',
                  value = '-((ca1*ca2*complex(0,1)*yup1)/cmath.sqrt(2)) - (ca2*complex(0,1)*sa1*yup2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_643 = Coupling(name = 'GC_643',
                  value = '(ca1*ca3*complex(0,1)*sa2*yup1)/cmath.sqrt(2) - (complex(0,1)*sa1*sa3*yup1)/cmath.sqrt(2) + (ca3*complex(0,1)*sa1*sa2*yup2)/cmath.sqrt(2) + (ca1*complex(0,1)*sa3*yup2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_644 = Coupling(name = 'GC_644',
                  value = '(ca3*complex(0,1)*sa1*yup1)/cmath.sqrt(2) + (ca1*complex(0,1)*sa2*sa3*yup1)/cmath.sqrt(2) - (ca1*ca3*complex(0,1)*yup2)/cmath.sqrt(2) + (complex(0,1)*sa1*sa2*sa3*yup2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_645 = Coupling(name = 'GC_645',
                  value = 'cb*complex(0,1)*yup1 + complex(0,1)*sb*yup2',
                  order = {'QED':1})

GC_646 = Coupling(name = 'GC_646',
                  value = '-((cb*yup1)/cmath.sqrt(2)) - (sb*yup2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_647 = Coupling(name = 'GC_647',
                  value = '(cb*yup1)/cmath.sqrt(2) + (sb*yup2)/cmath.sqrt(2)',
                  order = {'QED':1})

