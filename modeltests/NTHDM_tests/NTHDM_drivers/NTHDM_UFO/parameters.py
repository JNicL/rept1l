# This file was automatically created by FeynRules 2.3.23
# Mathematica version: 9.0 for Linux x86 (64-bit) (November 20, 2012)
# Date: Sun 13 Aug 2017 16:16:57



from object_library import all_parameters, Parameter


from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot

# This is a default parameter object representing 0.
ZERO = Parameter(name = 'ZERO',
                 nature = 'internal',
                 type = 'real',
                 value = '0.0',
                 texname = '0')

# User-defined parameters.
MSB = Parameter(name = 'MSB',
                nature = 'external',
                type = 'real',
                value = 111.,
                texname = 'M_{\\text{SB}}',
                lhablock = 'Higgs',
                lhacode = [ 1 ])

cb = Parameter(name = 'cb',
               nature = 'external',
               type = 'real',
               value = 0.9950041652780258,
               texname = 'c_b',
               lhablock = 'Higgs',
               lhacode = [ 2 ])

tbs = Parameter(name = 'tbs',
                nature = 'external',
                type = 'real',
                value = 0.1,
                texname = 't_{\\text{bs}}',
                lhablock = 'Higgs',
                lhacode = [ 3 ])

sa1 = Parameter(name = 'sa1',
                nature = 'external',
                type = 'real',
                value = 0.29552020666133955,
                texname = 's_{\\text{a1}}',
                lhablock = 'Higgs',
                lhacode = [ 4 ])

sa2 = Parameter(name = 'sa2',
                nature = 'external',
                type = 'real',
                value = 0.3894183423086505,
                texname = 's_{\\text{a2}}',
                lhablock = 'Higgs',
                lhacode = [ 5 ])

sa3 = Parameter(name = 'sa3',
                nature = 'external',
                type = 'real',
                value = 0.479425538604203,
                texname = 's_{\\text{a3}}',
                lhablock = 'Higgs',
                lhacode = [ 6 ])

aEW = Parameter(name = 'aEW',
                nature = 'external',
                type = 'real',
                value = 0.007818608287724784,
                texname = '\\text{aEW}',
                lhablock = 'SMINPUTS',
                lhacode = [ 1 ])

aS = Parameter(name = 'aS',
               nature = 'external',
               type = 'real',
               value = 0.118,
               texname = '\\text{aS}',
               lhablock = 'SMINPUTS',
               lhacode = [ 2 ])

ymdo = Parameter(name = 'ymdo',
                 nature = 'external',
                 type = 'real',
                 value = 0.00504,
                 texname = '\\text{ymdo}',
                 lhablock = 'YUKAWA',
                 lhacode = [ 1 ])

ymup = Parameter(name = 'ymup',
                 nature = 'external',
                 type = 'real',
                 value = 0.00255,
                 texname = '\\text{ymup}',
                 lhablock = 'YUKAWA',
                 lhacode = [ 2 ])

yms = Parameter(name = 'yms',
                nature = 'external',
                type = 'real',
                value = 0.101,
                texname = '\\text{yms}',
                lhablock = 'YUKAWA',
                lhacode = [ 3 ])

ymc = Parameter(name = 'ymc',
                nature = 'external',
                type = 'real',
                value = 1.27,
                texname = '\\text{ymc}',
                lhablock = 'YUKAWA',
                lhacode = [ 4 ])

ymb = Parameter(name = 'ymb',
                nature = 'external',
                type = 'real',
                value = 4.7,
                texname = '\\text{ymb}',
                lhablock = 'YUKAWA',
                lhacode = [ 5 ])

ymt = Parameter(name = 'ymt',
                nature = 'external',
                type = 'real',
                value = 172,
                texname = '\\text{ymt}',
                lhablock = 'YUKAWA',
                lhacode = [ 6 ])

yme = Parameter(name = 'yme',
                nature = 'external',
                type = 'real',
                value = 0.000511,
                texname = '\\text{yme}',
                lhablock = 'YUKAWA',
                lhacode = [ 11 ])

ymm = Parameter(name = 'ymm',
                nature = 'external',
                type = 'real',
                value = 0.10566,
                texname = '\\text{ymm}',
                lhablock = 'YUKAWA',
                lhacode = [ 13 ])

ymtau = Parameter(name = 'ymtau',
                  nature = 'external',
                  type = 'real',
                  value = 1.777,
                  texname = '\\text{ymtau}',
                  lhablock = 'YUKAWA',
                  lhacode = [ 15 ])

h1u = Parameter(name = 'h1u',
                nature = 'external',
                type = 'real',
                value = 1.1,
                texname = '\\text{h1u}',
                lhablock = 'YUKAWA',
                lhacode = [ 16 ])

h1d = Parameter(name = 'h1d',
                nature = 'external',
                type = 'real',
                value = 1.2,
                texname = '\\text{h1d}',
                lhablock = 'YUKAWA',
                lhacode = [ 17 ])

h1l = Parameter(name = 'h1l',
                nature = 'external',
                type = 'real',
                value = 1.3,
                texname = '\\text{h1l}',
                lhablock = 'YUKAWA',
                lhacode = [ 18 ])

h2u = Parameter(name = 'h2u',
                nature = 'external',
                type = 'real',
                value = 1.4,
                texname = '\\text{h2u}',
                lhablock = 'YUKAWA',
                lhacode = [ 19 ])

h2d = Parameter(name = 'h2d',
                nature = 'external',
                type = 'real',
                value = 1.5,
                texname = '\\text{h2d}',
                lhablock = 'YUKAWA',
                lhacode = [ 20 ])

h2l = Parameter(name = 'h2l',
                nature = 'external',
                type = 'real',
                value = 1.6,
                texname = '\\text{h2l}',
                lhablock = 'YUKAWA',
                lhacode = [ 21 ])

MZ = Parameter(name = 'MZ',
               nature = 'external',
               type = 'real',
               value = 91.1876,
               texname = '\\text{MZ}',
               lhablock = 'MASS',
               lhacode = [ 23 ])

MW = Parameter(name = 'MW',
               nature = 'external',
               type = 'real',
               value = 80.399,
               texname = '\\text{MW}',
               lhablock = 'MASS',
               lhacode = [ 24 ])

ME = Parameter(name = 'ME',
               nature = 'external',
               type = 'real',
               value = 0.000511,
               texname = '\\text{ME}',
               lhablock = 'MASS',
               lhacode = [ 11 ])

MM = Parameter(name = 'MM',
               nature = 'external',
               type = 'real',
               value = 0.10566,
               texname = '\\text{MM}',
               lhablock = 'MASS',
               lhacode = [ 13 ])

MTA = Parameter(name = 'MTA',
                nature = 'external',
                type = 'real',
                value = 1.777,
                texname = '\\text{MTA}',
                lhablock = 'MASS',
                lhacode = [ 15 ])

MU = Parameter(name = 'MU',
               nature = 'external',
               type = 'real',
               value = 0.00255,
               texname = 'M',
               lhablock = 'MASS',
               lhacode = [ 2 ])

MC = Parameter(name = 'MC',
               nature = 'external',
               type = 'real',
               value = 1.27,
               texname = '\\text{MC}',
               lhablock = 'MASS',
               lhacode = [ 4 ])

MT = Parameter(name = 'MT',
               nature = 'external',
               type = 'real',
               value = 172,
               texname = '\\text{MT}',
               lhablock = 'MASS',
               lhacode = [ 6 ])

MD = Parameter(name = 'MD',
               nature = 'external',
               type = 'real',
               value = 0.00504,
               texname = '\\text{MD}',
               lhablock = 'MASS',
               lhacode = [ 1 ])

MS = Parameter(name = 'MS',
               nature = 'external',
               type = 'real',
               value = 0.101,
               texname = '\\text{MS}',
               lhablock = 'MASS',
               lhacode = [ 3 ])

MB = Parameter(name = 'MB',
               nature = 'external',
               type = 'real',
               value = 4.7,
               texname = '\\text{MB}',
               lhablock = 'MASS',
               lhacode = [ 5 ])

MHC = Parameter(name = 'MHC',
                nature = 'external',
                type = 'real',
                value = 160,
                texname = '\\text{MHC}',
                lhablock = 'MASS',
                lhacode = [ 37 ])

MHL = Parameter(name = 'MHL',
                nature = 'external',
                type = 'real',
                value = 120,
                texname = '\\text{MHL}',
                lhablock = 'MASS',
                lhacode = [ 25 ])

MHM = Parameter(name = 'MHM',
                nature = 'external',
                type = 'real',
                value = 130,
                texname = '\\text{MHM}',
                lhablock = 'MASS',
                lhacode = [ 35 ])

MHH = Parameter(name = 'MHH',
                nature = 'external',
                type = 'real',
                value = 140,
                texname = '\\text{MHH}',
                lhablock = 'MASS',
                lhacode = [ 35 ])

MHA = Parameter(name = 'MHA',
                nature = 'external',
                type = 'real',
                value = 150,
                texname = '\\text{MHA}',
                lhablock = 'MASS',
                lhacode = [ 36 ])

WZ = Parameter(name = 'WZ',
               nature = 'external',
               type = 'real',
               value = 2.4952,
               texname = '\\text{WZ}',
               lhablock = 'DECAY',
               lhacode = [ 23 ])

WW = Parameter(name = 'WW',
               nature = 'external',
               type = 'real',
               value = 2.085,
               texname = '\\text{WW}',
               lhablock = 'DECAY',
               lhacode = [ 24 ])

WM = Parameter(name = 'WM',
               nature = 'external',
               type = 'real',
               value = 0.01,
               texname = '\\text{WM}',
               lhablock = 'DECAY',
               lhacode = [ 13 ])

WTA = Parameter(name = 'WTA',
                nature = 'external',
                type = 'real',
                value = 0.01,
                texname = '\\text{WTA}',
                lhablock = 'DECAY',
                lhacode = [ 15 ])

WC = Parameter(name = 'WC',
               nature = 'external',
               type = 'real',
               value = 0.01,
               texname = '\\text{WC}',
               lhablock = 'DECAY',
               lhacode = [ 4 ])

WT = Parameter(name = 'WT',
               nature = 'external',
               type = 'real',
               value = 1.50833649,
               texname = '\\text{WT}',
               lhablock = 'DECAY',
               lhacode = [ 6 ])

WB = Parameter(name = 'WB',
               nature = 'external',
               type = 'real',
               value = 0.01,
               texname = '\\text{WB}',
               lhablock = 'DECAY',
               lhacode = [ 5 ])

WHC = Parameter(name = 'WHC',
                nature = 'external',
                type = 'real',
                value = 1,
                texname = '\\text{WHC}',
                lhablock = 'DECAY',
                lhacode = [ 37 ])

WHL = Parameter(name = 'WHL',
                nature = 'external',
                type = 'real',
                value = 1,
                texname = '\\text{WHL}',
                lhablock = 'DECAY',
                lhacode = [ 25 ])

WHM = Parameter(name = 'WHM',
                nature = 'external',
                type = 'real',
                value = 1,
                texname = '\\text{WHM}',
                lhablock = 'DECAY',
                lhacode = [ 35 ])

WHH = Parameter(name = 'WHH',
                nature = 'external',
                type = 'real',
                value = 1,
                texname = '\\text{WHH}',
                lhablock = 'DECAY',
                lhacode = [ 35 ])

WHA = Parameter(name = 'WHA',
                nature = 'external',
                type = 'real',
                value = 1,
                texname = '\\text{WHA}',
                lhablock = 'DECAY',
                lhacode = [ 36 ])

sw2 = Parameter(name = 'sw2',
                nature = 'internal',
                type = 'real',
                value = '1 - MW**2/MZ**2',
                texname = '\\text{sw2}')

ee = Parameter(name = 'ee',
               nature = 'internal',
               type = 'real',
               value = '2*cmath.sqrt(aEW)*cmath.sqrt(cmath.pi)',
               texname = 'e')

gs = Parameter(name = 'gs',
               nature = 'internal',
               type = 'real',
               value = '2*cmath.sqrt(aS)*cmath.sqrt(cmath.pi)',
               texname = 'g_s')

MPS = Parameter(name = 'MPS',
                nature = 'internal',
                type = 'real',
                value = 'cmath.sqrt(MHH**2 + MHL**2)/cmath.sqrt(2)',
                texname = 'M_{\\text{PS}}')

MPA = Parameter(name = 'MPA',
                nature = 'internal',
                type = 'real',
                value = 'MHA/cmath.sqrt(2)',
                texname = 'M_{\\text{PA}}')

MPC = Parameter(name = 'MPC',
                nature = 'internal',
                type = 'real',
                value = 'MHC/cmath.sqrt(2)',
                texname = 'M_{\\text{PC}}')

sb = Parameter(name = 'sb',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(1 - cb**2)',
               texname = 's_b')

ca1 = Parameter(name = 'ca1',
                nature = 'internal',
                type = 'real',
                value = 'cmath.sqrt(1 - sa1**2)',
                texname = 'c_{\\text{a1}}')

ca2 = Parameter(name = 'ca2',
                nature = 'internal',
                type = 'real',
                value = 'cmath.sqrt(1 - sa2**2)',
                texname = 'c_{\\text{a2}}')

ca3 = Parameter(name = 'ca3',
                nature = 'internal',
                type = 'real',
                value = 'cmath.sqrt(1 - sa3**2)',
                texname = 'c_{\\text{a3}}')

cw = Parameter(name = 'cw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(1 - sw2)',
               texname = 'c_w')

mu12 = Parameter(name = 'mu12',
                 nature = 'internal',
                 type = 'real',
                 value = 'cb*MSB**2*sb',
                 texname = '\\text{mu12}')

sw = Parameter(name = 'sw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(sw2)',
               texname = 's_w')

tb = Parameter(name = 'tb',
               nature = 'internal',
               type = 'real',
               value = 'sb/cb',
               texname = 't_b')

g1 = Parameter(name = 'g1',
               nature = 'internal',
               type = 'real',
               value = '-ee/(2.*cw)',
               texname = 'g_1')

gw = Parameter(name = 'gw',
               nature = 'internal',
               type = 'real',
               value = 'ee/sw',
               texname = 'g_w')

vev = Parameter(name = 'vev',
                nature = 'internal',
                type = 'real',
                value = '(2*MW*sw)/ee',
                texname = '\\text{vev}')

l1 = Parameter(name = 'l1',
               nature = 'internal',
               type = 'real',
               value = '(ca1**2*ca2**2*MHL**2 + MHH**2*(-(ca1*ca3*sa2) + sa1*sa3)**2 + MHM**2*(-(ca3*sa1) - ca1*sa2*sa3)**2 - MSB**2*sb**2)/(cb**2*vev**2)',
               texname = '\\lambda _1')

l2 = Parameter(name = 'l2',
               nature = 'internal',
               type = 'real',
               value = '(-(cb**2*MSB**2) + ca2**2*MHL**2*sa1**2 + MHH**2*(-(ca3*sa1*sa2) - ca1*sa3)**2 + MHM**2*(ca1*ca3 - sa1*sa2*sa3)**2)/(sb**2*vev**2)',
               texname = '\\lambda _2')

l3 = Parameter(name = 'l3',
               nature = 'internal',
               type = 'real',
               value = '(2*MHC**2 - MSB**2 + (ca1*ca2**2*MHL**2*sa1 + MHH**2*(-(ca3*sa1*sa2) - ca1*sa3)*(-(ca1*ca3*sa2) + sa1*sa3) + MHM**2*(-(ca3*sa1) - ca1*sa2*sa3)*(ca1*ca3 - sa1*sa2*sa3))/(cb*sb))/vev**2',
               texname = '\\lambda _3')

l4 = Parameter(name = 'l4',
               nature = 'internal',
               type = 'real',
               value = '(MHA**2 - 2*MHC**2 + MSB**2)/vev**2',
               texname = '\\lambda _4')

l5 = Parameter(name = 'l5',
               nature = 'internal',
               type = 'real',
               value = '(-MHA**2 + MSB**2)/vev**2',
               texname = '\\lambda _5')

vev1 = Parameter(name = 'vev1',
                 nature = 'internal',
                 type = 'real',
                 value = 'cb*vev',
                 texname = '\\text{vev1}')

vev2 = Parameter(name = 'vev2',
                 nature = 'internal',
                 type = 'real',
                 value = 'sb*vev',
                 texname = '\\text{vev2}')

vevs = Parameter(name = 'vevs',
                 nature = 'internal',
                 type = 'real',
                 value = 'tbs*vev',
                 texname = '\\text{vevs}')

l6 = Parameter(name = 'l6',
               nature = 'internal',
               type = 'real',
               value = '(ca2**2*ca3**2*MHH**2 + MHL**2*sa2**2 + ca2**2*MHM**2*sa3**2)/vevs**2',
               texname = '\\lambda _6')

l7 = Parameter(name = 'l7',
               nature = 'internal',
               type = 'real',
               value = '(ca1*ca2*MHL**2*sa2 + ca2*ca3*MHH**2*(-(ca1*ca3*sa2) + sa1*sa3) + ca2*MHM**2*sa3*(-(ca3*sa1) - ca1*sa2*sa3))/(cb*vev*vevs)',
               texname = '\\lambda _7')

l8 = Parameter(name = 'l8',
               nature = 'internal',
               type = 'real',
               value = '(ca2*MHL**2*sa1*sa2 + ca2*ca3*MHH**2*(-(ca3*sa1*sa2) - ca1*sa3) + ca2*MHM**2*sa3*(ca1*ca3 - sa1*sa2*sa3))/(sb*vev*vevs)',
               texname = '\\lambda _8')

yb2 = Parameter(name = 'yb2',
                nature = 'internal',
                type = 'real',
                value = '(h2d*ymb*cmath.sqrt(2))/vev2',
                texname = '\\text{yb2}')

yc1 = Parameter(name = 'yc1',
                nature = 'internal',
                type = 'real',
                value = '(h1u*ymc*cmath.sqrt(2))/vev1',
                texname = '\\text{yc1}')

yc2 = Parameter(name = 'yc2',
                nature = 'internal',
                type = 'real',
                value = '(h2u*ymc*cmath.sqrt(2))/vev2',
                texname = '\\text{yc2}')

yd12x2 = Parameter(name = 'yd12x2',
                   nature = 'internal',
                   type = 'real',
                   value = '(h1d*yms*cmath.sqrt(2))/vev1',
                   texname = '\\text{yd12x2}')

yd13x3 = Parameter(name = 'yd13x3',
                   nature = 'internal',
                   type = 'real',
                   value = '(h1d*ymb*cmath.sqrt(2))/vev1',
                   texname = '\\text{yd13x3}')

ydo1 = Parameter(name = 'ydo1',
                 nature = 'internal',
                 type = 'real',
                 value = '(h1d*ymdo*cmath.sqrt(2))/vev1',
                 texname = '\\text{ydo1}')

ydo2 = Parameter(name = 'ydo2',
                 nature = 'internal',
                 type = 'real',
                 value = '(h2d*ymdo*cmath.sqrt(2))/vev2',
                 texname = '\\text{ydo2}')

ye1 = Parameter(name = 'ye1',
                nature = 'internal',
                type = 'real',
                value = '(h1l*yme*cmath.sqrt(2))/vev1',
                texname = '\\text{ye1}')

ye2 = Parameter(name = 'ye2',
                nature = 'internal',
                type = 'real',
                value = '(h2l*yme*cmath.sqrt(2))/vev2',
                texname = '\\text{ye2}')

ym1 = Parameter(name = 'ym1',
                nature = 'internal',
                type = 'real',
                value = '(h1l*ymm*cmath.sqrt(2))/vev1',
                texname = '\\text{ym1}')

ym2 = Parameter(name = 'ym2',
                nature = 'internal',
                type = 'real',
                value = '(h2l*ymm*cmath.sqrt(2))/vev2',
                texname = '\\text{ym2}')

ys2 = Parameter(name = 'ys2',
                nature = 'internal',
                type = 'real',
                value = '(h2d*yms*cmath.sqrt(2))/vev2',
                texname = '\\text{ys2}')

yt1 = Parameter(name = 'yt1',
                nature = 'internal',
                type = 'real',
                value = '(h1u*ymt*cmath.sqrt(2))/vev1',
                texname = '\\text{yt1}')

yt2 = Parameter(name = 'yt2',
                nature = 'internal',
                type = 'real',
                value = '(h2u*ymt*cmath.sqrt(2))/vev2',
                texname = '\\text{yt2}')

ytau1 = Parameter(name = 'ytau1',
                  nature = 'internal',
                  type = 'real',
                  value = '(h1l*ymtau*cmath.sqrt(2))/vev1',
                  texname = '\\text{ytau1}')

ytau2 = Parameter(name = 'ytau2',
                  nature = 'internal',
                  type = 'real',
                  value = '(h2l*ymtau*cmath.sqrt(2))/vev2',
                  texname = '\\text{ytau2}')

yup1 = Parameter(name = 'yup1',
                 nature = 'internal',
                 type = 'real',
                 value = '(h1u*ymup*cmath.sqrt(2))/vev1',
                 texname = '\\text{yup1}')

yup2 = Parameter(name = 'yup2',
                 nature = 'internal',
                 type = 'real',
                 value = '(h2u*ymup*cmath.sqrt(2))/vev2',
                 texname = '\\text{yup2}')

mu11 = Parameter(name = 'mu11',
                 nature = 'internal',
                 type = 'real',
                 value = 'MSB**2*sb**2 - ((cb**2*l1 + (l3 + l4 + l5)*sb**2 + l7*tbs**2)*vev**2)/2.',
                 texname = '\\text{mu11}')

mu22 = Parameter(name = 'mu22',
                 nature = 'internal',
                 type = 'real',
                 value = 'cb**2*MSB**2 - ((cb**2*(l3 + l4 + l5) + l2*sb**2 + l8*tbs**2)*vev**2)/2.',
                 texname = '\\text{mu22}')

mus2 = Parameter(name = 'mus2',
                 nature = 'internal',
                 type = 'real',
                 value = '-((cb**2*l7 + l8*sb**2 + l6*tbs**2)*vev**2)/2.',
                 texname = '\\text{mus2}')

