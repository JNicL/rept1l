# This file was automatically created by FeynRules 2.3.23
# Mathematica version: 9.0 for Linux x86 (64-bit) (November 20, 2012)
# Date: Sun 13 Aug 2017 16:16:57


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



GC_1 = Coupling(name = 'GC_1',
                value = '(ee*complex(0,1))/3.',
                order = {'QED':1})

GC_2 = Coupling(name = 'GC_2',
                value = '(-2*ee*complex(0,1))/3.',
                order = {'QED':1})

GC_3 = Coupling(name = 'GC_3',
                value = '-(ee*complex(0,1))',
                order = {'QED':1})

GC_4 = Coupling(name = 'GC_4',
                value = 'ee*complex(0,1)',
                order = {'QED':1})

GC_5 = Coupling(name = 'GC_5',
                value = 'ee**2*complex(0,1)',
                order = {'QED':2})

GC_6 = Coupling(name = 'GC_6',
                value = '-gs',
                order = {'QCD':1})

GC_7 = Coupling(name = 'GC_7',
                value = 'complex(0,1)*gs',
                order = {'QCD':1})

GC_8 = Coupling(name = 'GC_8',
                value = 'complex(0,1)*gs**2',
                order = {'QCD':2})

GC_9 = Coupling(name = 'GC_9',
                value = '-3*ca1**4*ca2**4*complex(0,1)*l1 - 6*ca1**2*ca2**4*complex(0,1)*l3*sa1**2 - 6*ca1**2*ca2**4*complex(0,1)*l4*sa1**2 - 6*ca1**2*ca2**4*complex(0,1)*l5*sa1**2 - 3*ca2**4*complex(0,1)*l2*sa1**4 - 6*ca1**2*ca2**2*complex(0,1)*l7*sa2**2 - 6*ca2**2*complex(0,1)*l8*sa1**2*sa2**2 - 3*complex(0,1)*l6*sa2**4',
                order = {'QED':2})

GC_10 = Coupling(name = 'GC_10',
                 value = '3*ca1**4*ca2**3*ca3*complex(0,1)*l1*sa2 - 3*ca1**2*ca2**3*ca3*complex(0,1)*l7*sa2 + 6*ca1**2*ca2**3*ca3*complex(0,1)*l3*sa1**2*sa2 + 6*ca1**2*ca2**3*ca3*complex(0,1)*l4*sa1**2*sa2 + 6*ca1**2*ca2**3*ca3*complex(0,1)*l5*sa1**2*sa2 - 3*ca2**3*ca3*complex(0,1)*l8*sa1**2*sa2 + 3*ca2**3*ca3*complex(0,1)*l2*sa1**4*sa2 - 3*ca2*ca3*complex(0,1)*l6*sa2**3 + 3*ca1**2*ca2*ca3*complex(0,1)*l7*sa2**3 + 3*ca2*ca3*complex(0,1)*l8*sa1**2*sa2**3 - 3*ca1**3*ca2**3*complex(0,1)*l1*sa1*sa3 + 3*ca1**3*ca2**3*complex(0,1)*l3*sa1*sa3 + 3*ca1**3*ca2**3*complex(0,1)*l4*sa1*sa3 + 3*ca1**3*ca2**3*complex(0,1)*l5*sa1*sa3 + 3*ca1*ca2**3*complex(0,1)*l2*sa1**3*sa3 - 3*ca1*ca2**3*complex(0,1)*l3*sa1**3*sa3 - 3*ca1*ca2**3*complex(0,1)*l4*sa1**3*sa3 - 3*ca1*ca2**3*complex(0,1)*l5*sa1**3*sa3 - 3*ca1*ca2*complex(0,1)*l7*sa1*sa2**2*sa3 + 3*ca1*ca2*complex(0,1)*l8*sa1*sa2**2*sa3',
                 order = {'QED':2})

GC_11 = Coupling(name = 'GC_11',
                 value = '3*ca1**3*ca2**3*ca3*complex(0,1)*l1*sa1 - 3*ca1**3*ca2**3*ca3*complex(0,1)*l3*sa1 - 3*ca1**3*ca2**3*ca3*complex(0,1)*l4*sa1 - 3*ca1**3*ca2**3*ca3*complex(0,1)*l5*sa1 - 3*ca1*ca2**3*ca3*complex(0,1)*l2*sa1**3 + 3*ca1*ca2**3*ca3*complex(0,1)*l3*sa1**3 + 3*ca1*ca2**3*ca3*complex(0,1)*l4*sa1**3 + 3*ca1*ca2**3*ca3*complex(0,1)*l5*sa1**3 + 3*ca1*ca2*ca3*complex(0,1)*l7*sa1*sa2**2 - 3*ca1*ca2*ca3*complex(0,1)*l8*sa1*sa2**2 + 3*ca1**4*ca2**3*complex(0,1)*l1*sa2*sa3 - 3*ca1**2*ca2**3*complex(0,1)*l7*sa2*sa3 + 6*ca1**2*ca2**3*complex(0,1)*l3*sa1**2*sa2*sa3 + 6*ca1**2*ca2**3*complex(0,1)*l4*sa1**2*sa2*sa3 + 6*ca1**2*ca2**3*complex(0,1)*l5*sa1**2*sa2*sa3 - 3*ca2**3*complex(0,1)*l8*sa1**2*sa2*sa3 + 3*ca2**3*complex(0,1)*l2*sa1**4*sa2*sa3 - 3*ca2*complex(0,1)*l6*sa2**3*sa3 + 3*ca1**2*ca2*complex(0,1)*l7*sa2**3*sa3 + 3*ca2*complex(0,1)*l8*sa1**2*sa2**3*sa3',
                 order = {'QED':2})

GC_12 = Coupling(name = 'GC_12',
                 value = '-(ca1**2*ca2**4*ca3**2*complex(0,1)*l7) - ca2**4*ca3**2*complex(0,1)*l8*sa1**2 - 3*ca1**4*ca2**2*ca3**2*complex(0,1)*l1*sa2**2 - 3*ca2**2*ca3**2*complex(0,1)*l6*sa2**2 + 4*ca1**2*ca2**2*ca3**2*complex(0,1)*l7*sa2**2 - 6*ca1**2*ca2**2*ca3**2*complex(0,1)*l3*sa1**2*sa2**2 - 6*ca1**2*ca2**2*ca3**2*complex(0,1)*l4*sa1**2*sa2**2 - 6*ca1**2*ca2**2*ca3**2*complex(0,1)*l5*sa1**2*sa2**2 + 4*ca2**2*ca3**2*complex(0,1)*l8*sa1**2*sa2**2 - 3*ca2**2*ca3**2*complex(0,1)*l2*sa1**4*sa2**2 - ca1**2*ca3**2*complex(0,1)*l7*sa2**4 - ca3**2*complex(0,1)*l8*sa1**2*sa2**4 + 6*ca1**3*ca2**2*ca3*complex(0,1)*l1*sa1*sa2*sa3 - 6*ca1**3*ca2**2*ca3*complex(0,1)*l3*sa1*sa2*sa3 - 6*ca1**3*ca2**2*ca3*complex(0,1)*l4*sa1*sa2*sa3 - 6*ca1**3*ca2**2*ca3*complex(0,1)*l5*sa1*sa2*sa3 - 4*ca1*ca2**2*ca3*complex(0,1)*l7*sa1*sa2*sa3 + 4*ca1*ca2**2*ca3*complex(0,1)*l8*sa1*sa2*sa3 - 6*ca1*ca2**2*ca3*complex(0,1)*l2*sa1**3*sa2*sa3 + 6*ca1*ca2**2*ca3*complex(0,1)*l3*sa1**3*sa2*sa3 + 6*ca1*ca2**2*ca3*complex(0,1)*l4*sa1**3*sa2*sa3 + 6*ca1*ca2**2*ca3*complex(0,1)*l5*sa1**3*sa2*sa3 + 2*ca1*ca3*complex(0,1)*l7*sa1*sa2**3*sa3 - 2*ca1*ca3*complex(0,1)*l8*sa1*sa2**3*sa3 - ca1**4*ca2**2*complex(0,1)*l3*sa3**2 - ca1**4*ca2**2*complex(0,1)*l4*sa3**2 - ca1**4*ca2**2*complex(0,1)*l5*sa3**2 - 3*ca1**2*ca2**2*complex(0,1)*l1*sa1**2*sa3**2 - 3*ca1**2*ca2**2*complex(0,1)*l2*sa1**2*sa3**2 + 4*ca1**2*ca2**2*complex(0,1)*l3*sa1**2*sa3**2 + 4*ca1**2*ca2**2*complex(0,1)*l4*sa1**2*sa3**2 + 4*ca1**2*ca2**2*complex(0,1)*l5*sa1**2*sa3**2 - ca2**2*complex(0,1)*l3*sa1**4*sa3**2 - ca2**2*complex(0,1)*l4*sa1**4*sa3**2 - ca2**2*complex(0,1)*l5*sa1**4*sa3**2 - ca1**2*complex(0,1)*l8*sa2**2*sa3**2 - complex(0,1)*l7*sa1**2*sa2**2*sa3**2',
                 order = {'QED':2})

GC_13 = Coupling(name = 'GC_13',
                 value = '-3*ca1**3*ca2**2*ca3**2*complex(0,1)*l1*sa1*sa2 + 3*ca1**3*ca2**2*ca3**2*complex(0,1)*l3*sa1*sa2 + 3*ca1**3*ca2**2*ca3**2*complex(0,1)*l4*sa1*sa2 + 3*ca1**3*ca2**2*ca3**2*complex(0,1)*l5*sa1*sa2 + 2*ca1*ca2**2*ca3**2*complex(0,1)*l7*sa1*sa2 - 2*ca1*ca2**2*ca3**2*complex(0,1)*l8*sa1*sa2 + 3*ca1*ca2**2*ca3**2*complex(0,1)*l2*sa1**3*sa2 - 3*ca1*ca2**2*ca3**2*complex(0,1)*l3*sa1**3*sa2 - 3*ca1*ca2**2*ca3**2*complex(0,1)*l4*sa1**3*sa2 - 3*ca1*ca2**2*ca3**2*complex(0,1)*l5*sa1**3*sa2 - ca1*ca3**2*complex(0,1)*l7*sa1*sa2**3 + ca1*ca3**2*complex(0,1)*l8*sa1*sa2**3 + ca1**4*ca2**2*ca3*complex(0,1)*l3*sa3 + ca1**4*ca2**2*ca3*complex(0,1)*l4*sa3 + ca1**4*ca2**2*ca3*complex(0,1)*l5*sa3 - ca1**2*ca2**4*ca3*complex(0,1)*l7*sa3 + 3*ca1**2*ca2**2*ca3*complex(0,1)*l1*sa1**2*sa3 + 3*ca1**2*ca2**2*ca3*complex(0,1)*l2*sa1**2*sa3 - 4*ca1**2*ca2**2*ca3*complex(0,1)*l3*sa1**2*sa3 - 4*ca1**2*ca2**2*ca3*complex(0,1)*l4*sa1**2*sa3 - 4*ca1**2*ca2**2*ca3*complex(0,1)*l5*sa1**2*sa3 - ca2**4*ca3*complex(0,1)*l8*sa1**2*sa3 + ca2**2*ca3*complex(0,1)*l3*sa1**4*sa3 + ca2**2*ca3*complex(0,1)*l4*sa1**4*sa3 + ca2**2*ca3*complex(0,1)*l5*sa1**4*sa3 - 3*ca1**4*ca2**2*ca3*complex(0,1)*l1*sa2**2*sa3 - 3*ca2**2*ca3*complex(0,1)*l6*sa2**2*sa3 + 4*ca1**2*ca2**2*ca3*complex(0,1)*l7*sa2**2*sa3 + ca1**2*ca3*complex(0,1)*l8*sa2**2*sa3 - 6*ca1**2*ca2**2*ca3*complex(0,1)*l3*sa1**2*sa2**2*sa3 - 6*ca1**2*ca2**2*ca3*complex(0,1)*l4*sa1**2*sa2**2*sa3 - 6*ca1**2*ca2**2*ca3*complex(0,1)*l5*sa1**2*sa2**2*sa3 + ca3*complex(0,1)*l7*sa1**2*sa2**2*sa3 + 4*ca2**2*ca3*complex(0,1)*l8*sa1**2*sa2**2*sa3 - 3*ca2**2*ca3*complex(0,1)*l2*sa1**4*sa2**2*sa3 - ca1**2*ca3*complex(0,1)*l7*sa2**4*sa3 - ca3*complex(0,1)*l8*sa1**2*sa2**4*sa3 + 3*ca1**3*ca2**2*complex(0,1)*l1*sa1*sa2*sa3**2 - 3*ca1**3*ca2**2*complex(0,1)*l3*sa1*sa2*sa3**2 - 3*ca1**3*ca2**2*complex(0,1)*l4*sa1*sa2*sa3**2 - 3*ca1**3*ca2**2*complex(0,1)*l5*sa1*sa2*sa3**2 - 2*ca1*ca2**2*complex(0,1)*l7*sa1*sa2*sa3**2 + 2*ca1*ca2**2*complex(0,1)*l8*sa1*sa2*sa3**2 - 3*ca1*ca2**2*complex(0,1)*l2*sa1**3*sa2*sa3**2 + 3*ca1*ca2**2*complex(0,1)*l3*sa1**3*sa2*sa3**2 + 3*ca1*ca2**2*complex(0,1)*l4*sa1**3*sa2*sa3**2 + 3*ca1*ca2**2*complex(0,1)*l5*sa1**3*sa2*sa3**2 + ca1*complex(0,1)*l7*sa1*sa2**3*sa3**2 - ca1*complex(0,1)*l8*sa1*sa2**3*sa3**2',
                 order = {'QED':2})

GC_14 = Coupling(name = 'GC_14',
                 value = '-(ca1**4*ca2**2*ca3**2*complex(0,1)*l3) - ca1**4*ca2**2*ca3**2*complex(0,1)*l4 - ca1**4*ca2**2*ca3**2*complex(0,1)*l5 - 3*ca1**2*ca2**2*ca3**2*complex(0,1)*l1*sa1**2 - 3*ca1**2*ca2**2*ca3**2*complex(0,1)*l2*sa1**2 + 4*ca1**2*ca2**2*ca3**2*complex(0,1)*l3*sa1**2 + 4*ca1**2*ca2**2*ca3**2*complex(0,1)*l4*sa1**2 + 4*ca1**2*ca2**2*ca3**2*complex(0,1)*l5*sa1**2 - ca2**2*ca3**2*complex(0,1)*l3*sa1**4 - ca2**2*ca3**2*complex(0,1)*l4*sa1**4 - ca2**2*ca3**2*complex(0,1)*l5*sa1**4 - ca1**2*ca3**2*complex(0,1)*l8*sa2**2 - ca3**2*complex(0,1)*l7*sa1**2*sa2**2 - 6*ca1**3*ca2**2*ca3*complex(0,1)*l1*sa1*sa2*sa3 + 6*ca1**3*ca2**2*ca3*complex(0,1)*l3*sa1*sa2*sa3 + 6*ca1**3*ca2**2*ca3*complex(0,1)*l4*sa1*sa2*sa3 + 6*ca1**3*ca2**2*ca3*complex(0,1)*l5*sa1*sa2*sa3 + 4*ca1*ca2**2*ca3*complex(0,1)*l7*sa1*sa2*sa3 - 4*ca1*ca2**2*ca3*complex(0,1)*l8*sa1*sa2*sa3 + 6*ca1*ca2**2*ca3*complex(0,1)*l2*sa1**3*sa2*sa3 - 6*ca1*ca2**2*ca3*complex(0,1)*l3*sa1**3*sa2*sa3 - 6*ca1*ca2**2*ca3*complex(0,1)*l4*sa1**3*sa2*sa3 - 6*ca1*ca2**2*ca3*complex(0,1)*l5*sa1**3*sa2*sa3 - 2*ca1*ca3*complex(0,1)*l7*sa1*sa2**3*sa3 + 2*ca1*ca3*complex(0,1)*l8*sa1*sa2**3*sa3 - ca1**2*ca2**4*complex(0,1)*l7*sa3**2 - ca2**4*complex(0,1)*l8*sa1**2*sa3**2 - 3*ca1**4*ca2**2*complex(0,1)*l1*sa2**2*sa3**2 - 3*ca2**2*complex(0,1)*l6*sa2**2*sa3**2 + 4*ca1**2*ca2**2*complex(0,1)*l7*sa2**2*sa3**2 - 6*ca1**2*ca2**2*complex(0,1)*l3*sa1**2*sa2**2*sa3**2 - 6*ca1**2*ca2**2*complex(0,1)*l4*sa1**2*sa2**2*sa3**2 - 6*ca1**2*ca2**2*complex(0,1)*l5*sa1**2*sa2**2*sa3**2 + 4*ca2**2*complex(0,1)*l8*sa1**2*sa2**2*sa3**2 - 3*ca2**2*complex(0,1)*l2*sa1**4*sa2**2*sa3**2 - ca1**2*complex(0,1)*l7*sa2**4*sa3**2 - complex(0,1)*l8*sa1**2*sa2**4*sa3**2',
                 order = {'QED':2})

GC_15 = Coupling(name = 'GC_15',
                 value = '-3*ca2**3*ca3**3*complex(0,1)*l6*sa2 + 3*ca1**2*ca2**3*ca3**3*complex(0,1)*l7*sa2 + 3*ca2**3*ca3**3*complex(0,1)*l8*sa1**2*sa2 + 3*ca1**4*ca2*ca3**3*complex(0,1)*l1*sa2**3 - 3*ca1**2*ca2*ca3**3*complex(0,1)*l7*sa2**3 + 6*ca1**2*ca2*ca3**3*complex(0,1)*l3*sa1**2*sa2**3 + 6*ca1**2*ca2*ca3**3*complex(0,1)*l4*sa1**2*sa2**3 + 6*ca1**2*ca2*ca3**3*complex(0,1)*l5*sa1**2*sa2**3 - 3*ca2*ca3**3*complex(0,1)*l8*sa1**2*sa2**3 + 3*ca2*ca3**3*complex(0,1)*l2*sa1**4*sa2**3 - 3*ca1*ca2**3*ca3**2*complex(0,1)*l7*sa1*sa3 + 3*ca1*ca2**3*ca3**2*complex(0,1)*l8*sa1*sa3 - 9*ca1**3*ca2*ca3**2*complex(0,1)*l1*sa1*sa2**2*sa3 + 9*ca1**3*ca2*ca3**2*complex(0,1)*l3*sa1*sa2**2*sa3 + 9*ca1**3*ca2*ca3**2*complex(0,1)*l4*sa1*sa2**2*sa3 + 9*ca1**3*ca2*ca3**2*complex(0,1)*l5*sa1*sa2**2*sa3 + 6*ca1*ca2*ca3**2*complex(0,1)*l7*sa1*sa2**2*sa3 - 6*ca1*ca2*ca3**2*complex(0,1)*l8*sa1*sa2**2*sa3 + 9*ca1*ca2*ca3**2*complex(0,1)*l2*sa1**3*sa2**2*sa3 - 9*ca1*ca2*ca3**2*complex(0,1)*l3*sa1**3*sa2**2*sa3 - 9*ca1*ca2*ca3**2*complex(0,1)*l4*sa1**3*sa2**2*sa3 - 9*ca1*ca2*ca3**2*complex(0,1)*l5*sa1**3*sa2**2*sa3 + 3*ca1**4*ca2*ca3*complex(0,1)*l3*sa2*sa3**2 + 3*ca1**4*ca2*ca3*complex(0,1)*l4*sa2*sa3**2 + 3*ca1**4*ca2*ca3*complex(0,1)*l5*sa2*sa3**2 - 3*ca1**2*ca2*ca3*complex(0,1)*l8*sa2*sa3**2 + 9*ca1**2*ca2*ca3*complex(0,1)*l1*sa1**2*sa2*sa3**2 + 9*ca1**2*ca2*ca3*complex(0,1)*l2*sa1**2*sa2*sa3**2 - 12*ca1**2*ca2*ca3*complex(0,1)*l3*sa1**2*sa2*sa3**2 - 12*ca1**2*ca2*ca3*complex(0,1)*l4*sa1**2*sa2*sa3**2 - 12*ca1**2*ca2*ca3*complex(0,1)*l5*sa1**2*sa2*sa3**2 - 3*ca2*ca3*complex(0,1)*l7*sa1**2*sa2*sa3**2 + 3*ca2*ca3*complex(0,1)*l3*sa1**4*sa2*sa3**2 + 3*ca2*ca3*complex(0,1)*l4*sa1**4*sa2*sa3**2 + 3*ca2*ca3*complex(0,1)*l5*sa1**4*sa2*sa3**2 + 3*ca1**3*ca2*complex(0,1)*l2*sa1*sa3**3 - 3*ca1**3*ca2*complex(0,1)*l3*sa1*sa3**3 - 3*ca1**3*ca2*complex(0,1)*l4*sa1*sa3**3 - 3*ca1**3*ca2*complex(0,1)*l5*sa1*sa3**3 - 3*ca1*ca2*complex(0,1)*l1*sa1**3*sa3**3 + 3*ca1*ca2*complex(0,1)*l3*sa1**3*sa3**3 + 3*ca1*ca2*complex(0,1)*l4*sa1**3*sa3**3 + 3*ca1*ca2*complex(0,1)*l5*sa1**3*sa3**3',
                 order = {'QED':2})

GC_16 = Coupling(name = 'GC_16',
                 value = 'ca1*ca2**3*ca3**3*complex(0,1)*l7*sa1 - ca1*ca2**3*ca3**3*complex(0,1)*l8*sa1 + 3*ca1**3*ca2*ca3**3*complex(0,1)*l1*sa1*sa2**2 - 3*ca1**3*ca2*ca3**3*complex(0,1)*l3*sa1*sa2**2 - 3*ca1**3*ca2*ca3**3*complex(0,1)*l4*sa1*sa2**2 - 3*ca1**3*ca2*ca3**3*complex(0,1)*l5*sa1*sa2**2 - 2*ca1*ca2*ca3**3*complex(0,1)*l7*sa1*sa2**2 + 2*ca1*ca2*ca3**3*complex(0,1)*l8*sa1*sa2**2 - 3*ca1*ca2*ca3**3*complex(0,1)*l2*sa1**3*sa2**2 + 3*ca1*ca2*ca3**3*complex(0,1)*l3*sa1**3*sa2**2 + 3*ca1*ca2*ca3**3*complex(0,1)*l4*sa1**3*sa2**2 + 3*ca1*ca2*ca3**3*complex(0,1)*l5*sa1**3*sa2**2 - 2*ca1**4*ca2*ca3**2*complex(0,1)*l3*sa2*sa3 - 2*ca1**4*ca2*ca3**2*complex(0,1)*l4*sa2*sa3 - 2*ca1**4*ca2*ca3**2*complex(0,1)*l5*sa2*sa3 - 3*ca2**3*ca3**2*complex(0,1)*l6*sa2*sa3 + 3*ca1**2*ca2**3*ca3**2*complex(0,1)*l7*sa2*sa3 + 2*ca1**2*ca2*ca3**2*complex(0,1)*l8*sa2*sa3 - 6*ca1**2*ca2*ca3**2*complex(0,1)*l1*sa1**2*sa2*sa3 - 6*ca1**2*ca2*ca3**2*complex(0,1)*l2*sa1**2*sa2*sa3 + 8*ca1**2*ca2*ca3**2*complex(0,1)*l3*sa1**2*sa2*sa3 + 8*ca1**2*ca2*ca3**2*complex(0,1)*l4*sa1**2*sa2*sa3 + 8*ca1**2*ca2*ca3**2*complex(0,1)*l5*sa1**2*sa2*sa3 + 2*ca2*ca3**2*complex(0,1)*l7*sa1**2*sa2*sa3 + 3*ca2**3*ca3**2*complex(0,1)*l8*sa1**2*sa2*sa3 - 2*ca2*ca3**2*complex(0,1)*l3*sa1**4*sa2*sa3 - 2*ca2*ca3**2*complex(0,1)*l4*sa1**4*sa2*sa3 - 2*ca2*ca3**2*complex(0,1)*l5*sa1**4*sa2*sa3 + 3*ca1**4*ca2*ca3**2*complex(0,1)*l1*sa2**3*sa3 - 3*ca1**2*ca2*ca3**2*complex(0,1)*l7*sa2**3*sa3 + 6*ca1**2*ca2*ca3**2*complex(0,1)*l3*sa1**2*sa2**3*sa3 + 6*ca1**2*ca2*ca3**2*complex(0,1)*l4*sa1**2*sa2**3*sa3 + 6*ca1**2*ca2*ca3**2*complex(0,1)*l5*sa1**2*sa2**3*sa3 - 3*ca2*ca3**2*complex(0,1)*l8*sa1**2*sa2**3*sa3 + 3*ca2*ca3**2*complex(0,1)*l2*sa1**4*sa2**3*sa3 - 3*ca1**3*ca2*ca3*complex(0,1)*l2*sa1*sa3**2 + 3*ca1**3*ca2*ca3*complex(0,1)*l3*sa1*sa3**2 + 3*ca1**3*ca2*ca3*complex(0,1)*l4*sa1*sa3**2 + 3*ca1**3*ca2*ca3*complex(0,1)*l5*sa1*sa3**2 - 2*ca1*ca2**3*ca3*complex(0,1)*l7*sa1*sa3**2 + 2*ca1*ca2**3*ca3*complex(0,1)*l8*sa1*sa3**2 + 3*ca1*ca2*ca3*complex(0,1)*l1*sa1**3*sa3**2 - 3*ca1*ca2*ca3*complex(0,1)*l3*sa1**3*sa3**2 - 3*ca1*ca2*ca3*complex(0,1)*l4*sa1**3*sa3**2 - 3*ca1*ca2*ca3*complex(0,1)*l5*sa1**3*sa3**2 - 6*ca1**3*ca2*ca3*complex(0,1)*l1*sa1*sa2**2*sa3**2 + 6*ca1**3*ca2*ca3*complex(0,1)*l3*sa1*sa2**2*sa3**2 + 6*ca1**3*ca2*ca3*complex(0,1)*l4*sa1*sa2**2*sa3**2 + 6*ca1**3*ca2*ca3*complex(0,1)*l5*sa1*sa2**2*sa3**2 + 4*ca1*ca2*ca3*complex(0,1)*l7*sa1*sa2**2*sa3**2 - 4*ca1*ca2*ca3*complex(0,1)*l8*sa1*sa2**2*sa3**2 + 6*ca1*ca2*ca3*complex(0,1)*l2*sa1**3*sa2**2*sa3**2 - 6*ca1*ca2*ca3*complex(0,1)*l3*sa1**3*sa2**2*sa3**2 - 6*ca1*ca2*ca3*complex(0,1)*l4*sa1**3*sa2**2*sa3**2 - 6*ca1*ca2*ca3*complex(0,1)*l5*sa1**3*sa2**2*sa3**2 + ca1**4*ca2*complex(0,1)*l3*sa2*sa3**3 + ca1**4*ca2*complex(0,1)*l4*sa2*sa3**3 + ca1**4*ca2*complex(0,1)*l5*sa2*sa3**3 - ca1**2*ca2*complex(0,1)*l8*sa2*sa3**3 + 3*ca1**2*ca2*complex(0,1)*l1*sa1**2*sa2*sa3**3 + 3*ca1**2*ca2*complex(0,1)*l2*sa1**2*sa2*sa3**3 - 4*ca1**2*ca2*complex(0,1)*l3*sa1**2*sa2*sa3**3 - 4*ca1**2*ca2*complex(0,1)*l4*sa1**2*sa2*sa3**3 - 4*ca1**2*ca2*complex(0,1)*l5*sa1**2*sa2*sa3**3 - ca2*complex(0,1)*l7*sa1**2*sa2*sa3**3 + ca2*complex(0,1)*l3*sa1**4*sa2*sa3**3 + ca2*complex(0,1)*l4*sa1**4*sa2*sa3**3 + ca2*complex(0,1)*l5*sa1**4*sa2*sa3**3',
                 order = {'QED':2})

GC_17 = Coupling(name = 'GC_17',
                 value = 'ca1**4*ca2*ca3**3*complex(0,1)*l3*sa2 + ca1**4*ca2*ca3**3*complex(0,1)*l4*sa2 + ca1**4*ca2*ca3**3*complex(0,1)*l5*sa2 - ca1**2*ca2*ca3**3*complex(0,1)*l8*sa2 + 3*ca1**2*ca2*ca3**3*complex(0,1)*l1*sa1**2*sa2 + 3*ca1**2*ca2*ca3**3*complex(0,1)*l2*sa1**2*sa2 - 4*ca1**2*ca2*ca3**3*complex(0,1)*l3*sa1**2*sa2 - 4*ca1**2*ca2*ca3**3*complex(0,1)*l4*sa1**2*sa2 - 4*ca1**2*ca2*ca3**3*complex(0,1)*l5*sa1**2*sa2 - ca2*ca3**3*complex(0,1)*l7*sa1**2*sa2 + ca2*ca3**3*complex(0,1)*l3*sa1**4*sa2 + ca2*ca3**3*complex(0,1)*l4*sa1**4*sa2 + ca2*ca3**3*complex(0,1)*l5*sa1**4*sa2 + 3*ca1**3*ca2*ca3**2*complex(0,1)*l2*sa1*sa3 - 3*ca1**3*ca2*ca3**2*complex(0,1)*l3*sa1*sa3 - 3*ca1**3*ca2*ca3**2*complex(0,1)*l4*sa1*sa3 - 3*ca1**3*ca2*ca3**2*complex(0,1)*l5*sa1*sa3 + 2*ca1*ca2**3*ca3**2*complex(0,1)*l7*sa1*sa3 - 2*ca1*ca2**3*ca3**2*complex(0,1)*l8*sa1*sa3 - 3*ca1*ca2*ca3**2*complex(0,1)*l1*sa1**3*sa3 + 3*ca1*ca2*ca3**2*complex(0,1)*l3*sa1**3*sa3 + 3*ca1*ca2*ca3**2*complex(0,1)*l4*sa1**3*sa3 + 3*ca1*ca2*ca3**2*complex(0,1)*l5*sa1**3*sa3 + 6*ca1**3*ca2*ca3**2*complex(0,1)*l1*sa1*sa2**2*sa3 - 6*ca1**3*ca2*ca3**2*complex(0,1)*l3*sa1*sa2**2*sa3 - 6*ca1**3*ca2*ca3**2*complex(0,1)*l4*sa1*sa2**2*sa3 - 6*ca1**3*ca2*ca3**2*complex(0,1)*l5*sa1*sa2**2*sa3 - 4*ca1*ca2*ca3**2*complex(0,1)*l7*sa1*sa2**2*sa3 + 4*ca1*ca2*ca3**2*complex(0,1)*l8*sa1*sa2**2*sa3 - 6*ca1*ca2*ca3**2*complex(0,1)*l2*sa1**3*sa2**2*sa3 + 6*ca1*ca2*ca3**2*complex(0,1)*l3*sa1**3*sa2**2*sa3 + 6*ca1*ca2*ca3**2*complex(0,1)*l4*sa1**3*sa2**2*sa3 + 6*ca1*ca2*ca3**2*complex(0,1)*l5*sa1**3*sa2**2*sa3 - 2*ca1**4*ca2*ca3*complex(0,1)*l3*sa2*sa3**2 - 2*ca1**4*ca2*ca3*complex(0,1)*l4*sa2*sa3**2 - 2*ca1**4*ca2*ca3*complex(0,1)*l5*sa2*sa3**2 - 3*ca2**3*ca3*complex(0,1)*l6*sa2*sa3**2 + 3*ca1**2*ca2**3*ca3*complex(0,1)*l7*sa2*sa3**2 + 2*ca1**2*ca2*ca3*complex(0,1)*l8*sa2*sa3**2 - 6*ca1**2*ca2*ca3*complex(0,1)*l1*sa1**2*sa2*sa3**2 - 6*ca1**2*ca2*ca3*complex(0,1)*l2*sa1**2*sa2*sa3**2 + 8*ca1**2*ca2*ca3*complex(0,1)*l3*sa1**2*sa2*sa3**2 + 8*ca1**2*ca2*ca3*complex(0,1)*l4*sa1**2*sa2*sa3**2 + 8*ca1**2*ca2*ca3*complex(0,1)*l5*sa1**2*sa2*sa3**2 + 2*ca2*ca3*complex(0,1)*l7*sa1**2*sa2*sa3**2 + 3*ca2**3*ca3*complex(0,1)*l8*sa1**2*sa2*sa3**2 - 2*ca2*ca3*complex(0,1)*l3*sa1**4*sa2*sa3**2 - 2*ca2*ca3*complex(0,1)*l4*sa1**4*sa2*sa3**2 - 2*ca2*ca3*complex(0,1)*l5*sa1**4*sa2*sa3**2 + 3*ca1**4*ca2*ca3*complex(0,1)*l1*sa2**3*sa3**2 - 3*ca1**2*ca2*ca3*complex(0,1)*l7*sa2**3*sa3**2 + 6*ca1**2*ca2*ca3*complex(0,1)*l3*sa1**2*sa2**3*sa3**2 + 6*ca1**2*ca2*ca3*complex(0,1)*l4*sa1**2*sa2**3*sa3**2 + 6*ca1**2*ca2*ca3*complex(0,1)*l5*sa1**2*sa2**3*sa3**2 - 3*ca2*ca3*complex(0,1)*l8*sa1**2*sa2**3*sa3**2 + 3*ca2*ca3*complex(0,1)*l2*sa1**4*sa2**3*sa3**2 - ca1*ca2**3*complex(0,1)*l7*sa1*sa3**3 + ca1*ca2**3*complex(0,1)*l8*sa1*sa3**3 - 3*ca1**3*ca2*complex(0,1)*l1*sa1*sa2**2*sa3**3 + 3*ca1**3*ca2*complex(0,1)*l3*sa1*sa2**2*sa3**3 + 3*ca1**3*ca2*complex(0,1)*l4*sa1*sa2**2*sa3**3 + 3*ca1**3*ca2*complex(0,1)*l5*sa1*sa2**2*sa3**3 + 2*ca1*ca2*complex(0,1)*l7*sa1*sa2**2*sa3**3 - 2*ca1*ca2*complex(0,1)*l8*sa1*sa2**2*sa3**3 + 3*ca1*ca2*complex(0,1)*l2*sa1**3*sa2**2*sa3**3 - 3*ca1*ca2*complex(0,1)*l3*sa1**3*sa2**2*sa3**3 - 3*ca1*ca2*complex(0,1)*l4*sa1**3*sa2**2*sa3**3 - 3*ca1*ca2*complex(0,1)*l5*sa1**3*sa2**2*sa3**3',
                 order = {'QED':2})

GC_18 = Coupling(name = 'GC_18',
                 value = '-3*ca1**3*ca2*ca3**3*complex(0,1)*l2*sa1 + 3*ca1**3*ca2*ca3**3*complex(0,1)*l3*sa1 + 3*ca1**3*ca2*ca3**3*complex(0,1)*l4*sa1 + 3*ca1**3*ca2*ca3**3*complex(0,1)*l5*sa1 + 3*ca1*ca2*ca3**3*complex(0,1)*l1*sa1**3 - 3*ca1*ca2*ca3**3*complex(0,1)*l3*sa1**3 - 3*ca1*ca2*ca3**3*complex(0,1)*l4*sa1**3 - 3*ca1*ca2*ca3**3*complex(0,1)*l5*sa1**3 + 3*ca1**4*ca2*ca3**2*complex(0,1)*l3*sa2*sa3 + 3*ca1**4*ca2*ca3**2*complex(0,1)*l4*sa2*sa3 + 3*ca1**4*ca2*ca3**2*complex(0,1)*l5*sa2*sa3 - 3*ca1**2*ca2*ca3**2*complex(0,1)*l8*sa2*sa3 + 9*ca1**2*ca2*ca3**2*complex(0,1)*l1*sa1**2*sa2*sa3 + 9*ca1**2*ca2*ca3**2*complex(0,1)*l2*sa1**2*sa2*sa3 - 12*ca1**2*ca2*ca3**2*complex(0,1)*l3*sa1**2*sa2*sa3 - 12*ca1**2*ca2*ca3**2*complex(0,1)*l4*sa1**2*sa2*sa3 - 12*ca1**2*ca2*ca3**2*complex(0,1)*l5*sa1**2*sa2*sa3 - 3*ca2*ca3**2*complex(0,1)*l7*sa1**2*sa2*sa3 + 3*ca2*ca3**2*complex(0,1)*l3*sa1**4*sa2*sa3 + 3*ca2*ca3**2*complex(0,1)*l4*sa1**4*sa2*sa3 + 3*ca2*ca3**2*complex(0,1)*l5*sa1**4*sa2*sa3 + 3*ca1*ca2**3*ca3*complex(0,1)*l7*sa1*sa3**2 - 3*ca1*ca2**3*ca3*complex(0,1)*l8*sa1*sa3**2 + 9*ca1**3*ca2*ca3*complex(0,1)*l1*sa1*sa2**2*sa3**2 - 9*ca1**3*ca2*ca3*complex(0,1)*l3*sa1*sa2**2*sa3**2 - 9*ca1**3*ca2*ca3*complex(0,1)*l4*sa1*sa2**2*sa3**2 - 9*ca1**3*ca2*ca3*complex(0,1)*l5*sa1*sa2**2*sa3**2 - 6*ca1*ca2*ca3*complex(0,1)*l7*sa1*sa2**2*sa3**2 + 6*ca1*ca2*ca3*complex(0,1)*l8*sa1*sa2**2*sa3**2 - 9*ca1*ca2*ca3*complex(0,1)*l2*sa1**3*sa2**2*sa3**2 + 9*ca1*ca2*ca3*complex(0,1)*l3*sa1**3*sa2**2*sa3**2 + 9*ca1*ca2*ca3*complex(0,1)*l4*sa1**3*sa2**2*sa3**2 + 9*ca1*ca2*ca3*complex(0,1)*l5*sa1**3*sa2**2*sa3**2 - 3*ca2**3*complex(0,1)*l6*sa2*sa3**3 + 3*ca1**2*ca2**3*complex(0,1)*l7*sa2*sa3**3 + 3*ca2**3*complex(0,1)*l8*sa1**2*sa2*sa3**3 + 3*ca1**4*ca2*complex(0,1)*l1*sa2**3*sa3**3 - 3*ca1**2*ca2*complex(0,1)*l7*sa2**3*sa3**3 + 6*ca1**2*ca2*complex(0,1)*l3*sa1**2*sa2**3*sa3**3 + 6*ca1**2*ca2*complex(0,1)*l4*sa1**2*sa2**3*sa3**3 + 6*ca1**2*ca2*complex(0,1)*l5*sa1**2*sa2**3*sa3**3 - 3*ca2*complex(0,1)*l8*sa1**2*sa2**3*sa3**3 + 3*ca2*complex(0,1)*l2*sa1**4*sa2**3*sa3**3',
                 order = {'QED':2})

GC_19 = Coupling(name = 'GC_19',
                 value = '-3*ca2**4*ca3**4*complex(0,1)*l6 - 6*ca1**2*ca2**2*ca3**4*complex(0,1)*l7*sa2**2 - 6*ca2**2*ca3**4*complex(0,1)*l8*sa1**2*sa2**2 - 3*ca1**4*ca3**4*complex(0,1)*l1*sa2**4 - 6*ca1**2*ca3**4*complex(0,1)*l3*sa1**2*sa2**4 - 6*ca1**2*ca3**4*complex(0,1)*l4*sa1**2*sa2**4 - 6*ca1**2*ca3**4*complex(0,1)*l5*sa1**2*sa2**4 - 3*ca3**4*complex(0,1)*l2*sa1**4*sa2**4 + 12*ca1*ca2**2*ca3**3*complex(0,1)*l7*sa1*sa2*sa3 - 12*ca1*ca2**2*ca3**3*complex(0,1)*l8*sa1*sa2*sa3 + 12*ca1**3*ca3**3*complex(0,1)*l1*sa1*sa2**3*sa3 - 12*ca1**3*ca3**3*complex(0,1)*l3*sa1*sa2**3*sa3 - 12*ca1**3*ca3**3*complex(0,1)*l4*sa1*sa2**3*sa3 - 12*ca1**3*ca3**3*complex(0,1)*l5*sa1*sa2**3*sa3 - 12*ca1*ca3**3*complex(0,1)*l2*sa1**3*sa2**3*sa3 + 12*ca1*ca3**3*complex(0,1)*l3*sa1**3*sa2**3*sa3 + 12*ca1*ca3**3*complex(0,1)*l4*sa1**3*sa2**3*sa3 + 12*ca1*ca3**3*complex(0,1)*l5*sa1**3*sa2**3*sa3 - 6*ca1**2*ca2**2*ca3**2*complex(0,1)*l8*sa3**2 - 6*ca2**2*ca3**2*complex(0,1)*l7*sa1**2*sa3**2 - 6*ca1**4*ca3**2*complex(0,1)*l3*sa2**2*sa3**2 - 6*ca1**4*ca3**2*complex(0,1)*l4*sa2**2*sa3**2 - 6*ca1**4*ca3**2*complex(0,1)*l5*sa2**2*sa3**2 - 18*ca1**2*ca3**2*complex(0,1)*l1*sa1**2*sa2**2*sa3**2 - 18*ca1**2*ca3**2*complex(0,1)*l2*sa1**2*sa2**2*sa3**2 + 24*ca1**2*ca3**2*complex(0,1)*l3*sa1**2*sa2**2*sa3**2 + 24*ca1**2*ca3**2*complex(0,1)*l4*sa1**2*sa2**2*sa3**2 + 24*ca1**2*ca3**2*complex(0,1)*l5*sa1**2*sa2**2*sa3**2 - 6*ca3**2*complex(0,1)*l3*sa1**4*sa2**2*sa3**2 - 6*ca3**2*complex(0,1)*l4*sa1**4*sa2**2*sa3**2 - 6*ca3**2*complex(0,1)*l5*sa1**4*sa2**2*sa3**2 - 12*ca1**3*ca3*complex(0,1)*l2*sa1*sa2*sa3**3 + 12*ca1**3*ca3*complex(0,1)*l3*sa1*sa2*sa3**3 + 12*ca1**3*ca3*complex(0,1)*l4*sa1*sa2*sa3**3 + 12*ca1**3*ca3*complex(0,1)*l5*sa1*sa2*sa3**3 + 12*ca1*ca3*complex(0,1)*l1*sa1**3*sa2*sa3**3 - 12*ca1*ca3*complex(0,1)*l3*sa1**3*sa2*sa3**3 - 12*ca1*ca3*complex(0,1)*l4*sa1**3*sa2*sa3**3 - 12*ca1*ca3*complex(0,1)*l5*sa1**3*sa2*sa3**3 - 3*ca1**4*complex(0,1)*l2*sa3**4 - 6*ca1**2*complex(0,1)*l3*sa1**2*sa3**4 - 6*ca1**2*complex(0,1)*l4*sa1**2*sa3**4 - 6*ca1**2*complex(0,1)*l5*sa1**2*sa3**4 - 3*complex(0,1)*l1*sa1**4*sa3**4',
                 order = {'QED':2})

GC_20 = Coupling(name = 'GC_20',
                 value = '-3*ca1*ca2**2*ca3**4*complex(0,1)*l7*sa1*sa2 + 3*ca1*ca2**2*ca3**4*complex(0,1)*l8*sa1*sa2 - 3*ca1**3*ca3**4*complex(0,1)*l1*sa1*sa2**3 + 3*ca1**3*ca3**4*complex(0,1)*l3*sa1*sa2**3 + 3*ca1**3*ca3**4*complex(0,1)*l4*sa1*sa2**3 + 3*ca1**3*ca3**4*complex(0,1)*l5*sa1*sa2**3 + 3*ca1*ca3**4*complex(0,1)*l2*sa1**3*sa2**3 - 3*ca1*ca3**4*complex(0,1)*l3*sa1**3*sa2**3 - 3*ca1*ca3**4*complex(0,1)*l4*sa1**3*sa2**3 - 3*ca1*ca3**4*complex(0,1)*l5*sa1**3*sa2**3 - 3*ca2**4*ca3**3*complex(0,1)*l6*sa3 + 3*ca1**2*ca2**2*ca3**3*complex(0,1)*l8*sa3 + 3*ca2**2*ca3**3*complex(0,1)*l7*sa1**2*sa3 + 3*ca1**4*ca3**3*complex(0,1)*l3*sa2**2*sa3 + 3*ca1**4*ca3**3*complex(0,1)*l4*sa2**2*sa3 + 3*ca1**4*ca3**3*complex(0,1)*l5*sa2**2*sa3 - 6*ca1**2*ca2**2*ca3**3*complex(0,1)*l7*sa2**2*sa3 + 9*ca1**2*ca3**3*complex(0,1)*l1*sa1**2*sa2**2*sa3 + 9*ca1**2*ca3**3*complex(0,1)*l2*sa1**2*sa2**2*sa3 - 12*ca1**2*ca3**3*complex(0,1)*l3*sa1**2*sa2**2*sa3 - 12*ca1**2*ca3**3*complex(0,1)*l4*sa1**2*sa2**2*sa3 - 12*ca1**2*ca3**3*complex(0,1)*l5*sa1**2*sa2**2*sa3 - 6*ca2**2*ca3**3*complex(0,1)*l8*sa1**2*sa2**2*sa3 + 3*ca3**3*complex(0,1)*l3*sa1**4*sa2**2*sa3 + 3*ca3**3*complex(0,1)*l4*sa1**4*sa2**2*sa3 + 3*ca3**3*complex(0,1)*l5*sa1**4*sa2**2*sa3 - 3*ca1**4*ca3**3*complex(0,1)*l1*sa2**4*sa3 - 6*ca1**2*ca3**3*complex(0,1)*l3*sa1**2*sa2**4*sa3 - 6*ca1**2*ca3**3*complex(0,1)*l4*sa1**2*sa2**4*sa3 - 6*ca1**2*ca3**3*complex(0,1)*l5*sa1**2*sa2**4*sa3 - 3*ca3**3*complex(0,1)*l2*sa1**4*sa2**4*sa3 + 9*ca1**3*ca3**2*complex(0,1)*l2*sa1*sa2*sa3**2 - 9*ca1**3*ca3**2*complex(0,1)*l3*sa1*sa2*sa3**2 - 9*ca1**3*ca3**2*complex(0,1)*l4*sa1*sa2*sa3**2 - 9*ca1**3*ca3**2*complex(0,1)*l5*sa1*sa2*sa3**2 + 9*ca1*ca2**2*ca3**2*complex(0,1)*l7*sa1*sa2*sa3**2 - 9*ca1*ca2**2*ca3**2*complex(0,1)*l8*sa1*sa2*sa3**2 - 9*ca1*ca3**2*complex(0,1)*l1*sa1**3*sa2*sa3**2 + 9*ca1*ca3**2*complex(0,1)*l3*sa1**3*sa2*sa3**2 + 9*ca1*ca3**2*complex(0,1)*l4*sa1**3*sa2*sa3**2 + 9*ca1*ca3**2*complex(0,1)*l5*sa1**3*sa2*sa3**2 + 9*ca1**3*ca3**2*complex(0,1)*l1*sa1*sa2**3*sa3**2 - 9*ca1**3*ca3**2*complex(0,1)*l3*sa1*sa2**3*sa3**2 - 9*ca1**3*ca3**2*complex(0,1)*l4*sa1*sa2**3*sa3**2 - 9*ca1**3*ca3**2*complex(0,1)*l5*sa1*sa2**3*sa3**2 - 9*ca1*ca3**2*complex(0,1)*l2*sa1**3*sa2**3*sa3**2 + 9*ca1*ca3**2*complex(0,1)*l3*sa1**3*sa2**3*sa3**2 + 9*ca1*ca3**2*complex(0,1)*l4*sa1**3*sa2**3*sa3**2 + 9*ca1*ca3**2*complex(0,1)*l5*sa1**3*sa2**3*sa3**2 + 3*ca1**4*ca3*complex(0,1)*l2*sa3**3 - 3*ca1**2*ca2**2*ca3*complex(0,1)*l8*sa3**3 + 6*ca1**2*ca3*complex(0,1)*l3*sa1**2*sa3**3 + 6*ca1**2*ca3*complex(0,1)*l4*sa1**2*sa3**3 + 6*ca1**2*ca3*complex(0,1)*l5*sa1**2*sa3**3 - 3*ca2**2*ca3*complex(0,1)*l7*sa1**2*sa3**3 + 3*ca3*complex(0,1)*l1*sa1**4*sa3**3 - 3*ca1**4*ca3*complex(0,1)*l3*sa2**2*sa3**3 - 3*ca1**4*ca3*complex(0,1)*l4*sa2**2*sa3**3 - 3*ca1**4*ca3*complex(0,1)*l5*sa2**2*sa3**3 - 9*ca1**2*ca3*complex(0,1)*l1*sa1**2*sa2**2*sa3**3 - 9*ca1**2*ca3*complex(0,1)*l2*sa1**2*sa2**2*sa3**3 + 12*ca1**2*ca3*complex(0,1)*l3*sa1**2*sa2**2*sa3**3 + 12*ca1**2*ca3*complex(0,1)*l4*sa1**2*sa2**2*sa3**3 + 12*ca1**2*ca3*complex(0,1)*l5*sa1**2*sa2**2*sa3**3 - 3*ca3*complex(0,1)*l3*sa1**4*sa2**2*sa3**3 - 3*ca3*complex(0,1)*l4*sa1**4*sa2**2*sa3**3 - 3*ca3*complex(0,1)*l5*sa1**4*sa2**2*sa3**3 - 3*ca1**3*complex(0,1)*l2*sa1*sa2*sa3**4 + 3*ca1**3*complex(0,1)*l3*sa1*sa2*sa3**4 + 3*ca1**3*complex(0,1)*l4*sa1*sa2*sa3**4 + 3*ca1**3*complex(0,1)*l5*sa1*sa2*sa3**4 + 3*ca1*complex(0,1)*l1*sa1**3*sa2*sa3**4 - 3*ca1*complex(0,1)*l3*sa1**3*sa2*sa3**4 - 3*ca1*complex(0,1)*l4*sa1**3*sa2*sa3**4 - 3*ca1*complex(0,1)*l5*sa1**3*sa2*sa3**4',
                 order = {'QED':2})

GC_21 = Coupling(name = 'GC_21',
                 value = '-(ca1**2*ca2**2*ca3**4*complex(0,1)*l8) - ca2**2*ca3**4*complex(0,1)*l7*sa1**2 - ca1**4*ca3**4*complex(0,1)*l3*sa2**2 - ca1**4*ca3**4*complex(0,1)*l4*sa2**2 - ca1**4*ca3**4*complex(0,1)*l5*sa2**2 - 3*ca1**2*ca3**4*complex(0,1)*l1*sa1**2*sa2**2 - 3*ca1**2*ca3**4*complex(0,1)*l2*sa1**2*sa2**2 + 4*ca1**2*ca3**4*complex(0,1)*l3*sa1**2*sa2**2 + 4*ca1**2*ca3**4*complex(0,1)*l4*sa1**2*sa2**2 + 4*ca1**2*ca3**4*complex(0,1)*l5*sa1**2*sa2**2 - ca3**4*complex(0,1)*l3*sa1**4*sa2**2 - ca3**4*complex(0,1)*l4*sa1**4*sa2**2 - ca3**4*complex(0,1)*l5*sa1**4*sa2**2 - 6*ca1**3*ca3**3*complex(0,1)*l2*sa1*sa2*sa3 + 6*ca1**3*ca3**3*complex(0,1)*l3*sa1*sa2*sa3 + 6*ca1**3*ca3**3*complex(0,1)*l4*sa1*sa2*sa3 + 6*ca1**3*ca3**3*complex(0,1)*l5*sa1*sa2*sa3 - 6*ca1*ca2**2*ca3**3*complex(0,1)*l7*sa1*sa2*sa3 + 6*ca1*ca2**2*ca3**3*complex(0,1)*l8*sa1*sa2*sa3 + 6*ca1*ca3**3*complex(0,1)*l1*sa1**3*sa2*sa3 - 6*ca1*ca3**3*complex(0,1)*l3*sa1**3*sa2*sa3 - 6*ca1*ca3**3*complex(0,1)*l4*sa1**3*sa2*sa3 - 6*ca1*ca3**3*complex(0,1)*l5*sa1**3*sa2*sa3 - 6*ca1**3*ca3**3*complex(0,1)*l1*sa1*sa2**3*sa3 + 6*ca1**3*ca3**3*complex(0,1)*l3*sa1*sa2**3*sa3 + 6*ca1**3*ca3**3*complex(0,1)*l4*sa1*sa2**3*sa3 + 6*ca1**3*ca3**3*complex(0,1)*l5*sa1*sa2**3*sa3 + 6*ca1*ca3**3*complex(0,1)*l2*sa1**3*sa2**3*sa3 - 6*ca1*ca3**3*complex(0,1)*l3*sa1**3*sa2**3*sa3 - 6*ca1*ca3**3*complex(0,1)*l4*sa1**3*sa2**3*sa3 - 6*ca1*ca3**3*complex(0,1)*l5*sa1**3*sa2**3*sa3 - 3*ca1**4*ca3**2*complex(0,1)*l2*sa3**2 - 3*ca2**4*ca3**2*complex(0,1)*l6*sa3**2 + 4*ca1**2*ca2**2*ca3**2*complex(0,1)*l8*sa3**2 - 6*ca1**2*ca3**2*complex(0,1)*l3*sa1**2*sa3**2 - 6*ca1**2*ca3**2*complex(0,1)*l4*sa1**2*sa3**2 - 6*ca1**2*ca3**2*complex(0,1)*l5*sa1**2*sa3**2 + 4*ca2**2*ca3**2*complex(0,1)*l7*sa1**2*sa3**2 - 3*ca3**2*complex(0,1)*l1*sa1**4*sa3**2 + 4*ca1**4*ca3**2*complex(0,1)*l3*sa2**2*sa3**2 + 4*ca1**4*ca3**2*complex(0,1)*l4*sa2**2*sa3**2 + 4*ca1**4*ca3**2*complex(0,1)*l5*sa2**2*sa3**2 - 6*ca1**2*ca2**2*ca3**2*complex(0,1)*l7*sa2**2*sa3**2 + 12*ca1**2*ca3**2*complex(0,1)*l1*sa1**2*sa2**2*sa3**2 + 12*ca1**2*ca3**2*complex(0,1)*l2*sa1**2*sa2**2*sa3**2 - 16*ca1**2*ca3**2*complex(0,1)*l3*sa1**2*sa2**2*sa3**2 - 16*ca1**2*ca3**2*complex(0,1)*l4*sa1**2*sa2**2*sa3**2 - 16*ca1**2*ca3**2*complex(0,1)*l5*sa1**2*sa2**2*sa3**2 - 6*ca2**2*ca3**2*complex(0,1)*l8*sa1**2*sa2**2*sa3**2 + 4*ca3**2*complex(0,1)*l3*sa1**4*sa2**2*sa3**2 + 4*ca3**2*complex(0,1)*l4*sa1**4*sa2**2*sa3**2 + 4*ca3**2*complex(0,1)*l5*sa1**4*sa2**2*sa3**2 - 3*ca1**4*ca3**2*complex(0,1)*l1*sa2**4*sa3**2 - 6*ca1**2*ca3**2*complex(0,1)*l3*sa1**2*sa2**4*sa3**2 - 6*ca1**2*ca3**2*complex(0,1)*l4*sa1**2*sa2**4*sa3**2 - 6*ca1**2*ca3**2*complex(0,1)*l5*sa1**2*sa2**4*sa3**2 - 3*ca3**2*complex(0,1)*l2*sa1**4*sa2**4*sa3**2 + 6*ca1**3*ca3*complex(0,1)*l2*sa1*sa2*sa3**3 - 6*ca1**3*ca3*complex(0,1)*l3*sa1*sa2*sa3**3 - 6*ca1**3*ca3*complex(0,1)*l4*sa1*sa2*sa3**3 - 6*ca1**3*ca3*complex(0,1)*l5*sa1*sa2*sa3**3 + 6*ca1*ca2**2*ca3*complex(0,1)*l7*sa1*sa2*sa3**3 - 6*ca1*ca2**2*ca3*complex(0,1)*l8*sa1*sa2*sa3**3 - 6*ca1*ca3*complex(0,1)*l1*sa1**3*sa2*sa3**3 + 6*ca1*ca3*complex(0,1)*l3*sa1**3*sa2*sa3**3 + 6*ca1*ca3*complex(0,1)*l4*sa1**3*sa2*sa3**3 + 6*ca1*ca3*complex(0,1)*l5*sa1**3*sa2*sa3**3 + 6*ca1**3*ca3*complex(0,1)*l1*sa1*sa2**3*sa3**3 - 6*ca1**3*ca3*complex(0,1)*l3*sa1*sa2**3*sa3**3 - 6*ca1**3*ca3*complex(0,1)*l4*sa1*sa2**3*sa3**3 - 6*ca1**3*ca3*complex(0,1)*l5*sa1*sa2**3*sa3**3 - 6*ca1*ca3*complex(0,1)*l2*sa1**3*sa2**3*sa3**3 + 6*ca1*ca3*complex(0,1)*l3*sa1**3*sa2**3*sa3**3 + 6*ca1*ca3*complex(0,1)*l4*sa1**3*sa2**3*sa3**3 + 6*ca1*ca3*complex(0,1)*l5*sa1**3*sa2**3*sa3**3 - ca1**2*ca2**2*complex(0,1)*l8*sa3**4 - ca2**2*complex(0,1)*l7*sa1**2*sa3**4 - ca1**4*complex(0,1)*l3*sa2**2*sa3**4 - ca1**4*complex(0,1)*l4*sa2**2*sa3**4 - ca1**4*complex(0,1)*l5*sa2**2*sa3**4 - 3*ca1**2*complex(0,1)*l1*sa1**2*sa2**2*sa3**4 - 3*ca1**2*complex(0,1)*l2*sa1**2*sa2**2*sa3**4 + 4*ca1**2*complex(0,1)*l3*sa1**2*sa2**2*sa3**4 + 4*ca1**2*complex(0,1)*l4*sa1**2*sa2**2*sa3**4 + 4*ca1**2*complex(0,1)*l5*sa1**2*sa2**2*sa3**4 - complex(0,1)*l3*sa1**4*sa2**2*sa3**4 - complex(0,1)*l4*sa1**4*sa2**2*sa3**4 - complex(0,1)*l5*sa1**4*sa2**2*sa3**4',
                 order = {'QED':2})

GC_22 = Coupling(name = 'GC_22',
                 value = '3*ca1**3*ca3**4*complex(0,1)*l2*sa1*sa2 - 3*ca1**3*ca3**4*complex(0,1)*l3*sa1*sa2 - 3*ca1**3*ca3**4*complex(0,1)*l4*sa1*sa2 - 3*ca1**3*ca3**4*complex(0,1)*l5*sa1*sa2 - 3*ca1*ca3**4*complex(0,1)*l1*sa1**3*sa2 + 3*ca1*ca3**4*complex(0,1)*l3*sa1**3*sa2 + 3*ca1*ca3**4*complex(0,1)*l4*sa1**3*sa2 + 3*ca1*ca3**4*complex(0,1)*l5*sa1**3*sa2 + 3*ca1**4*ca3**3*complex(0,1)*l2*sa3 - 3*ca1**2*ca2**2*ca3**3*complex(0,1)*l8*sa3 + 6*ca1**2*ca3**3*complex(0,1)*l3*sa1**2*sa3 + 6*ca1**2*ca3**3*complex(0,1)*l4*sa1**2*sa3 + 6*ca1**2*ca3**3*complex(0,1)*l5*sa1**2*sa3 - 3*ca2**2*ca3**3*complex(0,1)*l7*sa1**2*sa3 + 3*ca3**3*complex(0,1)*l1*sa1**4*sa3 - 3*ca1**4*ca3**3*complex(0,1)*l3*sa2**2*sa3 - 3*ca1**4*ca3**3*complex(0,1)*l4*sa2**2*sa3 - 3*ca1**4*ca3**3*complex(0,1)*l5*sa2**2*sa3 - 9*ca1**2*ca3**3*complex(0,1)*l1*sa1**2*sa2**2*sa3 - 9*ca1**2*ca3**3*complex(0,1)*l2*sa1**2*sa2**2*sa3 + 12*ca1**2*ca3**3*complex(0,1)*l3*sa1**2*sa2**2*sa3 + 12*ca1**2*ca3**3*complex(0,1)*l4*sa1**2*sa2**2*sa3 + 12*ca1**2*ca3**3*complex(0,1)*l5*sa1**2*sa2**2*sa3 - 3*ca3**3*complex(0,1)*l3*sa1**4*sa2**2*sa3 - 3*ca3**3*complex(0,1)*l4*sa1**4*sa2**2*sa3 - 3*ca3**3*complex(0,1)*l5*sa1**4*sa2**2*sa3 - 9*ca1**3*ca3**2*complex(0,1)*l2*sa1*sa2*sa3**2 + 9*ca1**3*ca3**2*complex(0,1)*l3*sa1*sa2*sa3**2 + 9*ca1**3*ca3**2*complex(0,1)*l4*sa1*sa2*sa3**2 + 9*ca1**3*ca3**2*complex(0,1)*l5*sa1*sa2*sa3**2 - 9*ca1*ca2**2*ca3**2*complex(0,1)*l7*sa1*sa2*sa3**2 + 9*ca1*ca2**2*ca3**2*complex(0,1)*l8*sa1*sa2*sa3**2 + 9*ca1*ca3**2*complex(0,1)*l1*sa1**3*sa2*sa3**2 - 9*ca1*ca3**2*complex(0,1)*l3*sa1**3*sa2*sa3**2 - 9*ca1*ca3**2*complex(0,1)*l4*sa1**3*sa2*sa3**2 - 9*ca1*ca3**2*complex(0,1)*l5*sa1**3*sa2*sa3**2 - 9*ca1**3*ca3**2*complex(0,1)*l1*sa1*sa2**3*sa3**2 + 9*ca1**3*ca3**2*complex(0,1)*l3*sa1*sa2**3*sa3**2 + 9*ca1**3*ca3**2*complex(0,1)*l4*sa1*sa2**3*sa3**2 + 9*ca1**3*ca3**2*complex(0,1)*l5*sa1*sa2**3*sa3**2 + 9*ca1*ca3**2*complex(0,1)*l2*sa1**3*sa2**3*sa3**2 - 9*ca1*ca3**2*complex(0,1)*l3*sa1**3*sa2**3*sa3**2 - 9*ca1*ca3**2*complex(0,1)*l4*sa1**3*sa2**3*sa3**2 - 9*ca1*ca3**2*complex(0,1)*l5*sa1**3*sa2**3*sa3**2 - 3*ca2**4*ca3*complex(0,1)*l6*sa3**3 + 3*ca1**2*ca2**2*ca3*complex(0,1)*l8*sa3**3 + 3*ca2**2*ca3*complex(0,1)*l7*sa1**2*sa3**3 + 3*ca1**4*ca3*complex(0,1)*l3*sa2**2*sa3**3 + 3*ca1**4*ca3*complex(0,1)*l4*sa2**2*sa3**3 + 3*ca1**4*ca3*complex(0,1)*l5*sa2**2*sa3**3 - 6*ca1**2*ca2**2*ca3*complex(0,1)*l7*sa2**2*sa3**3 + 9*ca1**2*ca3*complex(0,1)*l1*sa1**2*sa2**2*sa3**3 + 9*ca1**2*ca3*complex(0,1)*l2*sa1**2*sa2**2*sa3**3 - 12*ca1**2*ca3*complex(0,1)*l3*sa1**2*sa2**2*sa3**3 - 12*ca1**2*ca3*complex(0,1)*l4*sa1**2*sa2**2*sa3**3 - 12*ca1**2*ca3*complex(0,1)*l5*sa1**2*sa2**2*sa3**3 - 6*ca2**2*ca3*complex(0,1)*l8*sa1**2*sa2**2*sa3**3 + 3*ca3*complex(0,1)*l3*sa1**4*sa2**2*sa3**3 + 3*ca3*complex(0,1)*l4*sa1**4*sa2**2*sa3**3 + 3*ca3*complex(0,1)*l5*sa1**4*sa2**2*sa3**3 - 3*ca1**4*ca3*complex(0,1)*l1*sa2**4*sa3**3 - 6*ca1**2*ca3*complex(0,1)*l3*sa1**2*sa2**4*sa3**3 - 6*ca1**2*ca3*complex(0,1)*l4*sa1**2*sa2**4*sa3**3 - 6*ca1**2*ca3*complex(0,1)*l5*sa1**2*sa2**4*sa3**3 - 3*ca3*complex(0,1)*l2*sa1**4*sa2**4*sa3**3 + 3*ca1*ca2**2*complex(0,1)*l7*sa1*sa2*sa3**4 - 3*ca1*ca2**2*complex(0,1)*l8*sa1*sa2*sa3**4 + 3*ca1**3*complex(0,1)*l1*sa1*sa2**3*sa3**4 - 3*ca1**3*complex(0,1)*l3*sa1*sa2**3*sa3**4 - 3*ca1**3*complex(0,1)*l4*sa1*sa2**3*sa3**4 - 3*ca1**3*complex(0,1)*l5*sa1*sa2**3*sa3**4 - 3*ca1*complex(0,1)*l2*sa1**3*sa2**3*sa3**4 + 3*ca1*complex(0,1)*l3*sa1**3*sa2**3*sa3**4 + 3*ca1*complex(0,1)*l4*sa1**3*sa2**3*sa3**4 + 3*ca1*complex(0,1)*l5*sa1**3*sa2**3*sa3**4',
                 order = {'QED':2})

GC_23 = Coupling(name = 'GC_23',
                 value = '-3*ca1**4*ca3**4*complex(0,1)*l2 - 6*ca1**2*ca3**4*complex(0,1)*l3*sa1**2 - 6*ca1**2*ca3**4*complex(0,1)*l4*sa1**2 - 6*ca1**2*ca3**4*complex(0,1)*l5*sa1**2 - 3*ca3**4*complex(0,1)*l1*sa1**4 + 12*ca1**3*ca3**3*complex(0,1)*l2*sa1*sa2*sa3 - 12*ca1**3*ca3**3*complex(0,1)*l3*sa1*sa2*sa3 - 12*ca1**3*ca3**3*complex(0,1)*l4*sa1*sa2*sa3 - 12*ca1**3*ca3**3*complex(0,1)*l5*sa1*sa2*sa3 - 12*ca1*ca3**3*complex(0,1)*l1*sa1**3*sa2*sa3 + 12*ca1*ca3**3*complex(0,1)*l3*sa1**3*sa2*sa3 + 12*ca1*ca3**3*complex(0,1)*l4*sa1**3*sa2*sa3 + 12*ca1*ca3**3*complex(0,1)*l5*sa1**3*sa2*sa3 - 6*ca1**2*ca2**2*ca3**2*complex(0,1)*l8*sa3**2 - 6*ca2**2*ca3**2*complex(0,1)*l7*sa1**2*sa3**2 - 6*ca1**4*ca3**2*complex(0,1)*l3*sa2**2*sa3**2 - 6*ca1**4*ca3**2*complex(0,1)*l4*sa2**2*sa3**2 - 6*ca1**4*ca3**2*complex(0,1)*l5*sa2**2*sa3**2 - 18*ca1**2*ca3**2*complex(0,1)*l1*sa1**2*sa2**2*sa3**2 - 18*ca1**2*ca3**2*complex(0,1)*l2*sa1**2*sa2**2*sa3**2 + 24*ca1**2*ca3**2*complex(0,1)*l3*sa1**2*sa2**2*sa3**2 + 24*ca1**2*ca3**2*complex(0,1)*l4*sa1**2*sa2**2*sa3**2 + 24*ca1**2*ca3**2*complex(0,1)*l5*sa1**2*sa2**2*sa3**2 - 6*ca3**2*complex(0,1)*l3*sa1**4*sa2**2*sa3**2 - 6*ca3**2*complex(0,1)*l4*sa1**4*sa2**2*sa3**2 - 6*ca3**2*complex(0,1)*l5*sa1**4*sa2**2*sa3**2 - 12*ca1*ca2**2*ca3*complex(0,1)*l7*sa1*sa2*sa3**3 + 12*ca1*ca2**2*ca3*complex(0,1)*l8*sa1*sa2*sa3**3 - 12*ca1**3*ca3*complex(0,1)*l1*sa1*sa2**3*sa3**3 + 12*ca1**3*ca3*complex(0,1)*l3*sa1*sa2**3*sa3**3 + 12*ca1**3*ca3*complex(0,1)*l4*sa1*sa2**3*sa3**3 + 12*ca1**3*ca3*complex(0,1)*l5*sa1*sa2**3*sa3**3 + 12*ca1*ca3*complex(0,1)*l2*sa1**3*sa2**3*sa3**3 - 12*ca1*ca3*complex(0,1)*l3*sa1**3*sa2**3*sa3**3 - 12*ca1*ca3*complex(0,1)*l4*sa1**3*sa2**3*sa3**3 - 12*ca1*ca3*complex(0,1)*l5*sa1**3*sa2**3*sa3**3 - 3*ca2**4*complex(0,1)*l6*sa3**4 - 6*ca1**2*ca2**2*complex(0,1)*l7*sa2**2*sa3**4 - 6*ca2**2*complex(0,1)*l8*sa1**2*sa2**2*sa3**4 - 3*ca1**4*complex(0,1)*l1*sa2**4*sa3**4 - 6*ca1**2*complex(0,1)*l3*sa1**2*sa2**4*sa3**4 - 6*ca1**2*complex(0,1)*l4*sa1**2*sa2**4*sa3**4 - 6*ca1**2*complex(0,1)*l5*sa1**2*sa2**4*sa3**4 - 3*complex(0,1)*l2*sa1**4*sa2**4*sa3**4',
                 order = {'QED':2})

GC_24 = Coupling(name = 'GC_24',
                 value = '-(ca2*cb*ee**2*complex(0,1)*sa1)/(2.*cw) + (ca1*ca2*ee**2*complex(0,1)*sb)/(2.*cw)',
                 order = {'QED':2})

GC_25 = Coupling(name = 'GC_25',
                 value = '-(ca1*ca2*cb*ee**2*complex(0,1))/(2.*cw) - (ca2*ee**2*complex(0,1)*sa1*sb)/(2.*cw)',
                 order = {'QED':2})

GC_26 = Coupling(name = 'GC_26',
                 value = '(ca1*ca3*cb*ee**2*complex(0,1)*sa2)/(2.*cw) - (cb*ee**2*complex(0,1)*sa1*sa3)/(2.*cw) + (ca3*ee**2*complex(0,1)*sa1*sa2*sb)/(2.*cw) + (ca1*ee**2*complex(0,1)*sa3*sb)/(2.*cw)',
                 order = {'QED':2})

GC_27 = Coupling(name = 'GC_27',
                 value = '(ca3*cb*ee**2*complex(0,1)*sa1*sa2)/(2.*cw) + (ca1*cb*ee**2*complex(0,1)*sa3)/(2.*cw) - (ca1*ca3*ee**2*complex(0,1)*sa2*sb)/(2.*cw) + (ee**2*complex(0,1)*sa1*sa3*sb)/(2.*cw)',
                 order = {'QED':2})

GC_28 = Coupling(name = 'GC_28',
                 value = '-(ca1*ca3*cb*ee**2*complex(0,1))/(2.*cw) + (cb*ee**2*complex(0,1)*sa1*sa2*sa3)/(2.*cw) - (ca3*ee**2*complex(0,1)*sa1*sb)/(2.*cw) - (ca1*ee**2*complex(0,1)*sa2*sa3*sb)/(2.*cw)',
                 order = {'QED':2})

GC_29 = Coupling(name = 'GC_29',
                 value = '(ca3*cb*ee**2*complex(0,1)*sa1)/(2.*cw) + (ca1*cb*ee**2*complex(0,1)*sa2*sa3)/(2.*cw) - (ca1*ca3*ee**2*complex(0,1)*sb)/(2.*cw) + (ee**2*complex(0,1)*sa1*sa2*sa3*sb)/(2.*cw)',
                 order = {'QED':2})

GC_30 = Coupling(name = 'GC_30',
                 value = 'cb**2*ee*complex(0,1) + ee*complex(0,1)*sb**2',
                 order = {'QED':1})

GC_31 = Coupling(name = 'GC_31',
                 value = '2*cb**2*ee**2*complex(0,1) + 2*ee**2*complex(0,1)*sb**2',
                 order = {'QED':2})

GC_32 = Coupling(name = 'GC_32',
                 value = '-(cb**2*ee**2)/(2.*cw) - (ee**2*sb**2)/(2.*cw)',
                 order = {'QED':2})

GC_33 = Coupling(name = 'GC_33',
                 value = '(cb**2*ee**2)/(2.*cw) + (ee**2*sb**2)/(2.*cw)',
                 order = {'QED':2})

GC_34 = Coupling(name = 'GC_34',
                 value = 'cb**2*ee*complex(0,1)*MW + ee*complex(0,1)*MW*sb**2',
                 order = {'QED':1})

GC_35 = Coupling(name = 'GC_35',
                 value = '-(ca1*ca2**2*cb**2*complex(0,1)*l4*sa1) - ca1*ca2**2*cb**2*complex(0,1)*l5*sa1 + ca1**2*ca2**2*cb*complex(0,1)*l1*sb - ca1**2*ca2**2*cb*complex(0,1)*l3*sb - ca2**2*cb*complex(0,1)*l2*sa1**2*sb + ca2**2*cb*complex(0,1)*l3*sa1**2*sb + cb*complex(0,1)*l7*sa2**2*sb - cb*complex(0,1)*l8*sa2**2*sb + ca1*ca2**2*complex(0,1)*l4*sa1*sb**2 + ca1*ca2**2*complex(0,1)*l5*sa1*sb**2',
                 order = {'QED':2})

GC_36 = Coupling(name = 'GC_36',
                 value = '-2*ca1*ca2**2*cb**2*complex(0,1)*l5*sa1 + ca1**2*ca2**2*cb*complex(0,1)*l1*sb - ca1**2*ca2**2*cb*complex(0,1)*l3*sb - ca1**2*ca2**2*cb*complex(0,1)*l4*sb + ca1**2*ca2**2*cb*complex(0,1)*l5*sb - ca2**2*cb*complex(0,1)*l2*sa1**2*sb + ca2**2*cb*complex(0,1)*l3*sa1**2*sb + ca2**2*cb*complex(0,1)*l4*sa1**2*sb - ca2**2*cb*complex(0,1)*l5*sa1**2*sb + cb*complex(0,1)*l7*sa2**2*sb - cb*complex(0,1)*l8*sa2**2*sb + 2*ca1*ca2**2*complex(0,1)*l5*sa1*sb**2',
                 order = {'QED':2})

GC_37 = Coupling(name = 'GC_37',
                 value = '-(ca1**2*ca2**2*cb**2*complex(0,1)*l3) - ca2**2*cb**2*complex(0,1)*l2*sa1**2 - cb**2*complex(0,1)*l8*sa2**2 + 2*ca1*ca2**2*cb*complex(0,1)*l4*sa1*sb + 2*ca1*ca2**2*cb*complex(0,1)*l5*sa1*sb - ca1**2*ca2**2*complex(0,1)*l1*sb**2 - ca2**2*complex(0,1)*l3*sa1**2*sb**2 - complex(0,1)*l7*sa2**2*sb**2',
                 order = {'QED':2})

GC_38 = Coupling(name = 'GC_38',
                 value = '-(ca1**2*ca2**2*cb**2*complex(0,1)*l3) - ca1**2*ca2**2*cb**2*complex(0,1)*l4 + ca1**2*ca2**2*cb**2*complex(0,1)*l5 - ca2**2*cb**2*complex(0,1)*l2*sa1**2 - cb**2*complex(0,1)*l8*sa2**2 + 4*ca1*ca2**2*cb*complex(0,1)*l5*sa1*sb - ca1**2*ca2**2*complex(0,1)*l1*sb**2 - ca2**2*complex(0,1)*l3*sa1**2*sb**2 - ca2**2*complex(0,1)*l4*sa1**2*sb**2 + ca2**2*complex(0,1)*l5*sa1**2*sb**2 - complex(0,1)*l7*sa2**2*sb**2',
                 order = {'QED':2})

GC_39 = Coupling(name = 'GC_39',
                 value = '-(ca1**2*ca2**2*cb**2*complex(0,1)*l1) - ca2**2*cb**2*complex(0,1)*l3*sa1**2 - cb**2*complex(0,1)*l7*sa2**2 - 2*ca1*ca2**2*cb*complex(0,1)*l4*sa1*sb - 2*ca1*ca2**2*cb*complex(0,1)*l5*sa1*sb - ca1**2*ca2**2*complex(0,1)*l3*sb**2 - ca2**2*complex(0,1)*l2*sa1**2*sb**2 - complex(0,1)*l8*sa2**2*sb**2',
                 order = {'QED':2})

GC_40 = Coupling(name = 'GC_40',
                 value = '-(ca1**2*ca2**2*cb**2*complex(0,1)*l1) - ca2**2*cb**2*complex(0,1)*l3*sa1**2 - ca2**2*cb**2*complex(0,1)*l4*sa1**2 + ca2**2*cb**2*complex(0,1)*l5*sa1**2 - cb**2*complex(0,1)*l7*sa2**2 - 4*ca1*ca2**2*cb*complex(0,1)*l5*sa1*sb - ca1**2*ca2**2*complex(0,1)*l3*sb**2 - ca1**2*ca2**2*complex(0,1)*l4*sb**2 + ca1**2*ca2**2*complex(0,1)*l5*sb**2 - ca2**2*complex(0,1)*l2*sa1**2*sb**2 - complex(0,1)*l8*sa2**2*sb**2',
                 order = {'QED':2})

GC_41 = Coupling(name = 'GC_41',
                 value = 'ca1**2*ca2*ca3*cb**2*complex(0,1)*l1*sa2 - ca2*ca3*cb**2*complex(0,1)*l7*sa2 + ca2*ca3*cb**2*complex(0,1)*l3*sa1**2*sa2 - ca1*ca2*cb**2*complex(0,1)*l1*sa1*sa3 + ca1*ca2*cb**2*complex(0,1)*l3*sa1*sa3 + 2*ca1*ca2*ca3*cb*complex(0,1)*l4*sa1*sa2*sb + 2*ca1*ca2*ca3*cb*complex(0,1)*l5*sa1*sa2*sb + ca1**2*ca2*cb*complex(0,1)*l4*sa3*sb + ca1**2*ca2*cb*complex(0,1)*l5*sa3*sb - ca2*cb*complex(0,1)*l4*sa1**2*sa3*sb - ca2*cb*complex(0,1)*l5*sa1**2*sa3*sb + ca1**2*ca2*ca3*complex(0,1)*l3*sa2*sb**2 - ca2*ca3*complex(0,1)*l8*sa2*sb**2 + ca2*ca3*complex(0,1)*l2*sa1**2*sa2*sb**2 + ca1*ca2*complex(0,1)*l2*sa1*sa3*sb**2 - ca1*ca2*complex(0,1)*l3*sa1*sa3*sb**2',
                 order = {'QED':2})

GC_42 = Coupling(name = 'GC_42',
                 value = 'ca1**2*ca2*ca3*cb**2*complex(0,1)*l3*sa2 - ca2*ca3*cb**2*complex(0,1)*l8*sa2 + ca2*ca3*cb**2*complex(0,1)*l2*sa1**2*sa2 + ca1*ca2*cb**2*complex(0,1)*l2*sa1*sa3 - ca1*ca2*cb**2*complex(0,1)*l3*sa1*sa3 - 2*ca1*ca2*ca3*cb*complex(0,1)*l4*sa1*sa2*sb - 2*ca1*ca2*ca3*cb*complex(0,1)*l5*sa1*sa2*sb - ca1**2*ca2*cb*complex(0,1)*l4*sa3*sb - ca1**2*ca2*cb*complex(0,1)*l5*sa3*sb + ca2*cb*complex(0,1)*l4*sa1**2*sa3*sb + ca2*cb*complex(0,1)*l5*sa1**2*sa3*sb + ca1**2*ca2*ca3*complex(0,1)*l1*sa2*sb**2 - ca2*ca3*complex(0,1)*l7*sa2*sb**2 + ca2*ca3*complex(0,1)*l3*sa1**2*sa2*sb**2 - ca1*ca2*complex(0,1)*l1*sa1*sa3*sb**2 + ca1*ca2*complex(0,1)*l3*sa1*sa3*sb**2',
                 order = {'QED':2})

GC_43 = Coupling(name = 'GC_43',
                 value = 'ca1**2*ca2*ca3*cb**2*complex(0,1)*l3*sa2 + ca1**2*ca2*ca3*cb**2*complex(0,1)*l4*sa2 - ca1**2*ca2*ca3*cb**2*complex(0,1)*l5*sa2 - ca2*ca3*cb**2*complex(0,1)*l8*sa2 + ca2*ca3*cb**2*complex(0,1)*l2*sa1**2*sa2 + ca1*ca2*cb**2*complex(0,1)*l2*sa1*sa3 - ca1*ca2*cb**2*complex(0,1)*l3*sa1*sa3 - ca1*ca2*cb**2*complex(0,1)*l4*sa1*sa3 + ca1*ca2*cb**2*complex(0,1)*l5*sa1*sa3 - 4*ca1*ca2*ca3*cb*complex(0,1)*l5*sa1*sa2*sb - 2*ca1**2*ca2*cb*complex(0,1)*l5*sa3*sb + 2*ca2*cb*complex(0,1)*l5*sa1**2*sa3*sb + ca1**2*ca2*ca3*complex(0,1)*l1*sa2*sb**2 - ca2*ca3*complex(0,1)*l7*sa2*sb**2 + ca2*ca3*complex(0,1)*l3*sa1**2*sa2*sb**2 + ca2*ca3*complex(0,1)*l4*sa1**2*sa2*sb**2 - ca2*ca3*complex(0,1)*l5*sa1**2*sa2*sb**2 - ca1*ca2*complex(0,1)*l1*sa1*sa3*sb**2 + ca1*ca2*complex(0,1)*l3*sa1*sa3*sb**2 + ca1*ca2*complex(0,1)*l4*sa1*sa3*sb**2 - ca1*ca2*complex(0,1)*l5*sa1*sa3*sb**2',
                 order = {'QED':2})

GC_44 = Coupling(name = 'GC_44',
                 value = 'ca1**2*ca2*ca3*cb**2*complex(0,1)*l1*sa2 - ca2*ca3*cb**2*complex(0,1)*l7*sa2 + ca2*ca3*cb**2*complex(0,1)*l3*sa1**2*sa2 + ca2*ca3*cb**2*complex(0,1)*l4*sa1**2*sa2 - ca2*ca3*cb**2*complex(0,1)*l5*sa1**2*sa2 - ca1*ca2*cb**2*complex(0,1)*l1*sa1*sa3 + ca1*ca2*cb**2*complex(0,1)*l3*sa1*sa3 + ca1*ca2*cb**2*complex(0,1)*l4*sa1*sa3 - ca1*ca2*cb**2*complex(0,1)*l5*sa1*sa3 + 4*ca1*ca2*ca3*cb*complex(0,1)*l5*sa1*sa2*sb + 2*ca1**2*ca2*cb*complex(0,1)*l5*sa3*sb - 2*ca2*cb*complex(0,1)*l5*sa1**2*sa3*sb + ca1**2*ca2*ca3*complex(0,1)*l3*sa2*sb**2 + ca1**2*ca2*ca3*complex(0,1)*l4*sa2*sb**2 - ca1**2*ca2*ca3*complex(0,1)*l5*sa2*sb**2 - ca2*ca3*complex(0,1)*l8*sa2*sb**2 + ca2*ca3*complex(0,1)*l2*sa1**2*sa2*sb**2 + ca1*ca2*complex(0,1)*l2*sa1*sa3*sb**2 - ca1*ca2*complex(0,1)*l3*sa1*sa3*sb**2 - ca1*ca2*complex(0,1)*l4*sa1*sa3*sb**2 + ca1*ca2*complex(0,1)*l5*sa1*sa3*sb**2',
                 order = {'QED':2})

GC_45 = Coupling(name = 'GC_45',
                 value = 'ca1*ca2*ca3*cb**2*complex(0,1)*l4*sa1*sa2 + ca1*ca2*ca3*cb**2*complex(0,1)*l5*sa1*sa2 + (ca1**2*ca2*cb**2*complex(0,1)*l4*sa3)/2. + (ca1**2*ca2*cb**2*complex(0,1)*l5*sa3)/2. - (ca2*cb**2*complex(0,1)*l4*sa1**2*sa3)/2. - (ca2*cb**2*complex(0,1)*l5*sa1**2*sa3)/2. - ca1**2*ca2*ca3*cb*complex(0,1)*l1*sa2*sb + ca1**2*ca2*ca3*cb*complex(0,1)*l3*sa2*sb + ca2*ca3*cb*complex(0,1)*l7*sa2*sb - ca2*ca3*cb*complex(0,1)*l8*sa2*sb + ca2*ca3*cb*complex(0,1)*l2*sa1**2*sa2*sb - ca2*ca3*cb*complex(0,1)*l3*sa1**2*sa2*sb + ca1*ca2*cb*complex(0,1)*l1*sa1*sa3*sb + ca1*ca2*cb*complex(0,1)*l2*sa1*sa3*sb - 2*ca1*ca2*cb*complex(0,1)*l3*sa1*sa3*sb - ca1*ca2*ca3*complex(0,1)*l4*sa1*sa2*sb**2 - ca1*ca2*ca3*complex(0,1)*l5*sa1*sa2*sb**2 - (ca1**2*ca2*complex(0,1)*l4*sa3*sb**2)/2. - (ca1**2*ca2*complex(0,1)*l5*sa3*sb**2)/2. + (ca2*complex(0,1)*l4*sa1**2*sa3*sb**2)/2. + (ca2*complex(0,1)*l5*sa1**2*sa3*sb**2)/2.',
                 order = {'QED':2})

GC_46 = Coupling(name = 'GC_46',
                 value = '2*ca1*ca2*ca3*cb**2*complex(0,1)*l5*sa1*sa2 + ca1**2*ca2*cb**2*complex(0,1)*l5*sa3 - ca2*cb**2*complex(0,1)*l5*sa1**2*sa3 - ca1**2*ca2*ca3*cb*complex(0,1)*l1*sa2*sb + ca1**2*ca2*ca3*cb*complex(0,1)*l3*sa2*sb + ca1**2*ca2*ca3*cb*complex(0,1)*l4*sa2*sb - ca1**2*ca2*ca3*cb*complex(0,1)*l5*sa2*sb + ca2*ca3*cb*complex(0,1)*l7*sa2*sb - ca2*ca3*cb*complex(0,1)*l8*sa2*sb + ca2*ca3*cb*complex(0,1)*l2*sa1**2*sa2*sb - ca2*ca3*cb*complex(0,1)*l3*sa1**2*sa2*sb - ca2*ca3*cb*complex(0,1)*l4*sa1**2*sa2*sb + ca2*ca3*cb*complex(0,1)*l5*sa1**2*sa2*sb + ca1*ca2*cb*complex(0,1)*l1*sa1*sa3*sb + ca1*ca2*cb*complex(0,1)*l2*sa1*sa3*sb - 2*ca1*ca2*cb*complex(0,1)*l3*sa1*sa3*sb - 2*ca1*ca2*cb*complex(0,1)*l4*sa1*sa3*sb + 2*ca1*ca2*cb*complex(0,1)*l5*sa1*sa3*sb - 2*ca1*ca2*ca3*complex(0,1)*l5*sa1*sa2*sb**2 - ca1**2*ca2*complex(0,1)*l5*sa3*sb**2 + ca2*complex(0,1)*l5*sa1**2*sa3*sb**2',
                 order = {'QED':2})

GC_47 = Coupling(name = 'GC_47',
                 value = '-(ca1**2*ca2*ca3*cb**2*complex(0,1)*l4)/2. - (ca1**2*ca2*ca3*cb**2*complex(0,1)*l5)/2. + (ca2*ca3*cb**2*complex(0,1)*l4*sa1**2)/2. + (ca2*ca3*cb**2*complex(0,1)*l5*sa1**2)/2. + ca1*ca2*cb**2*complex(0,1)*l4*sa1*sa2*sa3 + ca1*ca2*cb**2*complex(0,1)*l5*sa1*sa2*sa3 - ca1*ca2*ca3*cb*complex(0,1)*l1*sa1*sb - ca1*ca2*ca3*cb*complex(0,1)*l2*sa1*sb + 2*ca1*ca2*ca3*cb*complex(0,1)*l3*sa1*sb - ca1**2*ca2*cb*complex(0,1)*l1*sa2*sa3*sb + ca1**2*ca2*cb*complex(0,1)*l3*sa2*sa3*sb + ca2*cb*complex(0,1)*l7*sa2*sa3*sb - ca2*cb*complex(0,1)*l8*sa2*sa3*sb + ca2*cb*complex(0,1)*l2*sa1**2*sa2*sa3*sb - ca2*cb*complex(0,1)*l3*sa1**2*sa2*sa3*sb + (ca1**2*ca2*ca3*complex(0,1)*l4*sb**2)/2. + (ca1**2*ca2*ca3*complex(0,1)*l5*sb**2)/2. - (ca2*ca3*complex(0,1)*l4*sa1**2*sb**2)/2. - (ca2*ca3*complex(0,1)*l5*sa1**2*sb**2)/2. - ca1*ca2*complex(0,1)*l4*sa1*sa2*sa3*sb**2 - ca1*ca2*complex(0,1)*l5*sa1*sa2*sa3*sb**2',
                 order = {'QED':2})

GC_48 = Coupling(name = 'GC_48',
                 value = '-(ca1**2*ca2*ca3*cb**2*complex(0,1)*l5) + ca2*ca3*cb**2*complex(0,1)*l5*sa1**2 + 2*ca1*ca2*cb**2*complex(0,1)*l5*sa1*sa2*sa3 - ca1*ca2*ca3*cb*complex(0,1)*l1*sa1*sb - ca1*ca2*ca3*cb*complex(0,1)*l2*sa1*sb + 2*ca1*ca2*ca3*cb*complex(0,1)*l3*sa1*sb + 2*ca1*ca2*ca3*cb*complex(0,1)*l4*sa1*sb - 2*ca1*ca2*ca3*cb*complex(0,1)*l5*sa1*sb - ca1**2*ca2*cb*complex(0,1)*l1*sa2*sa3*sb + ca1**2*ca2*cb*complex(0,1)*l3*sa2*sa3*sb + ca1**2*ca2*cb*complex(0,1)*l4*sa2*sa3*sb - ca1**2*ca2*cb*complex(0,1)*l5*sa2*sa3*sb + ca2*cb*complex(0,1)*l7*sa2*sa3*sb - ca2*cb*complex(0,1)*l8*sa2*sa3*sb + ca2*cb*complex(0,1)*l2*sa1**2*sa2*sa3*sb - ca2*cb*complex(0,1)*l3*sa1**2*sa2*sa3*sb - ca2*cb*complex(0,1)*l4*sa1**2*sa2*sa3*sb + ca2*cb*complex(0,1)*l5*sa1**2*sa2*sa3*sb + ca1**2*ca2*ca3*complex(0,1)*l5*sb**2 - ca2*ca3*complex(0,1)*l5*sa1**2*sb**2 - 2*ca1*ca2*complex(0,1)*l5*sa1*sa2*sa3*sb**2',
                 order = {'QED':2})

GC_49 = Coupling(name = 'GC_49',
                 value = 'ca1*ca2*ca3*cb**2*complex(0,1)*l1*sa1 - ca1*ca2*ca3*cb**2*complex(0,1)*l3*sa1 + ca1**2*ca2*cb**2*complex(0,1)*l1*sa2*sa3 - ca2*cb**2*complex(0,1)*l7*sa2*sa3 + ca2*cb**2*complex(0,1)*l3*sa1**2*sa2*sa3 - ca1**2*ca2*ca3*cb*complex(0,1)*l4*sb - ca1**2*ca2*ca3*cb*complex(0,1)*l5*sb + ca2*ca3*cb*complex(0,1)*l4*sa1**2*sb + ca2*ca3*cb*complex(0,1)*l5*sa1**2*sb + 2*ca1*ca2*cb*complex(0,1)*l4*sa1*sa2*sa3*sb + 2*ca1*ca2*cb*complex(0,1)*l5*sa1*sa2*sa3*sb - ca1*ca2*ca3*complex(0,1)*l2*sa1*sb**2 + ca1*ca2*ca3*complex(0,1)*l3*sa1*sb**2 + ca1**2*ca2*complex(0,1)*l3*sa2*sa3*sb**2 - ca2*complex(0,1)*l8*sa2*sa3*sb**2 + ca2*complex(0,1)*l2*sa1**2*sa2*sa3*sb**2',
                 order = {'QED':2})

GC_50 = Coupling(name = 'GC_50',
                 value = 'ca1*ca2*ca3*cb**2*complex(0,1)*l1*sa1 - ca1*ca2*ca3*cb**2*complex(0,1)*l3*sa1 - ca1*ca2*ca3*cb**2*complex(0,1)*l4*sa1 + ca1*ca2*ca3*cb**2*complex(0,1)*l5*sa1 + ca1**2*ca2*cb**2*complex(0,1)*l1*sa2*sa3 - ca2*cb**2*complex(0,1)*l7*sa2*sa3 + ca2*cb**2*complex(0,1)*l3*sa1**2*sa2*sa3 + ca2*cb**2*complex(0,1)*l4*sa1**2*sa2*sa3 - ca2*cb**2*complex(0,1)*l5*sa1**2*sa2*sa3 - 2*ca1**2*ca2*ca3*cb*complex(0,1)*l5*sb + 2*ca2*ca3*cb*complex(0,1)*l5*sa1**2*sb + 4*ca1*ca2*cb*complex(0,1)*l5*sa1*sa2*sa3*sb - ca1*ca2*ca3*complex(0,1)*l2*sa1*sb**2 + ca1*ca2*ca3*complex(0,1)*l3*sa1*sb**2 + ca1*ca2*ca3*complex(0,1)*l4*sa1*sb**2 - ca1*ca2*ca3*complex(0,1)*l5*sa1*sb**2 + ca1**2*ca2*complex(0,1)*l3*sa2*sa3*sb**2 + ca1**2*ca2*complex(0,1)*l4*sa2*sa3*sb**2 - ca1**2*ca2*complex(0,1)*l5*sa2*sa3*sb**2 - ca2*complex(0,1)*l8*sa2*sa3*sb**2 + ca2*complex(0,1)*l2*sa1**2*sa2*sa3*sb**2',
                 order = {'QED':2})

GC_51 = Coupling(name = 'GC_51',
                 value = '-(ca1*ca2*ca3*cb**2*complex(0,1)*l2*sa1) + ca1*ca2*ca3*cb**2*complex(0,1)*l3*sa1 + ca1**2*ca2*cb**2*complex(0,1)*l3*sa2*sa3 - ca2*cb**2*complex(0,1)*l8*sa2*sa3 + ca2*cb**2*complex(0,1)*l2*sa1**2*sa2*sa3 + ca1**2*ca2*ca3*cb*complex(0,1)*l4*sb + ca1**2*ca2*ca3*cb*complex(0,1)*l5*sb - ca2*ca3*cb*complex(0,1)*l4*sa1**2*sb - ca2*ca3*cb*complex(0,1)*l5*sa1**2*sb - 2*ca1*ca2*cb*complex(0,1)*l4*sa1*sa2*sa3*sb - 2*ca1*ca2*cb*complex(0,1)*l5*sa1*sa2*sa3*sb + ca1*ca2*ca3*complex(0,1)*l1*sa1*sb**2 - ca1*ca2*ca3*complex(0,1)*l3*sa1*sb**2 + ca1**2*ca2*complex(0,1)*l1*sa2*sa3*sb**2 - ca2*complex(0,1)*l7*sa2*sa3*sb**2 + ca2*complex(0,1)*l3*sa1**2*sa2*sa3*sb**2',
                 order = {'QED':2})

GC_52 = Coupling(name = 'GC_52',
                 value = '-(ca1*ca2*ca3*cb**2*complex(0,1)*l2*sa1) + ca1*ca2*ca3*cb**2*complex(0,1)*l3*sa1 + ca1*ca2*ca3*cb**2*complex(0,1)*l4*sa1 - ca1*ca2*ca3*cb**2*complex(0,1)*l5*sa1 + ca1**2*ca2*cb**2*complex(0,1)*l3*sa2*sa3 + ca1**2*ca2*cb**2*complex(0,1)*l4*sa2*sa3 - ca1**2*ca2*cb**2*complex(0,1)*l5*sa2*sa3 - ca2*cb**2*complex(0,1)*l8*sa2*sa3 + ca2*cb**2*complex(0,1)*l2*sa1**2*sa2*sa3 + 2*ca1**2*ca2*ca3*cb*complex(0,1)*l5*sb - 2*ca2*ca3*cb*complex(0,1)*l5*sa1**2*sb - 4*ca1*ca2*cb*complex(0,1)*l5*sa1*sa2*sa3*sb + ca1*ca2*ca3*complex(0,1)*l1*sa1*sb**2 - ca1*ca2*ca3*complex(0,1)*l3*sa1*sb**2 - ca1*ca2*ca3*complex(0,1)*l4*sa1*sb**2 + ca1*ca2*ca3*complex(0,1)*l5*sa1*sb**2 + ca1**2*ca2*complex(0,1)*l1*sa2*sa3*sb**2 - ca2*complex(0,1)*l7*sa2*sa3*sb**2 + ca2*complex(0,1)*l3*sa1**2*sa2*sa3*sb**2 + ca2*complex(0,1)*l4*sa1**2*sa2*sa3*sb**2 - ca2*complex(0,1)*l5*sa1**2*sa2*sa3*sb**2',
                 order = {'QED':2})

GC_53 = Coupling(name = 'GC_53',
                 value = '-(ca1*ca3**2*cb**2*complex(0,1)*l4*sa1*sa2**2) - ca1*ca3**2*cb**2*complex(0,1)*l5*sa1*sa2**2 - ca1**2*ca3*cb**2*complex(0,1)*l4*sa2*sa3 - ca1**2*ca3*cb**2*complex(0,1)*l5*sa2*sa3 + ca3*cb**2*complex(0,1)*l4*sa1**2*sa2*sa3 + ca3*cb**2*complex(0,1)*l5*sa1**2*sa2*sa3 + ca1*cb**2*complex(0,1)*l4*sa1*sa3**2 + ca1*cb**2*complex(0,1)*l5*sa1*sa3**2 + ca2**2*ca3**2*cb*complex(0,1)*l7*sb - ca2**2*ca3**2*cb*complex(0,1)*l8*sb + ca1**2*ca3**2*cb*complex(0,1)*l1*sa2**2*sb - ca1**2*ca3**2*cb*complex(0,1)*l3*sa2**2*sb - ca3**2*cb*complex(0,1)*l2*sa1**2*sa2**2*sb + ca3**2*cb*complex(0,1)*l3*sa1**2*sa2**2*sb - 2*ca1*ca3*cb*complex(0,1)*l1*sa1*sa2*sa3*sb - 2*ca1*ca3*cb*complex(0,1)*l2*sa1*sa2*sa3*sb + 4*ca1*ca3*cb*complex(0,1)*l3*sa1*sa2*sa3*sb - ca1**2*cb*complex(0,1)*l2*sa3**2*sb + ca1**2*cb*complex(0,1)*l3*sa3**2*sb + cb*complex(0,1)*l1*sa1**2*sa3**2*sb - cb*complex(0,1)*l3*sa1**2*sa3**2*sb + ca1*ca3**2*complex(0,1)*l4*sa1*sa2**2*sb**2 + ca1*ca3**2*complex(0,1)*l5*sa1*sa2**2*sb**2 + ca1**2*ca3*complex(0,1)*l4*sa2*sa3*sb**2 + ca1**2*ca3*complex(0,1)*l5*sa2*sa3*sb**2 - ca3*complex(0,1)*l4*sa1**2*sa2*sa3*sb**2 - ca3*complex(0,1)*l5*sa1**2*sa2*sa3*sb**2 - ca1*complex(0,1)*l4*sa1*sa3**2*sb**2 - ca1*complex(0,1)*l5*sa1*sa3**2*sb**2',
                 order = {'QED':2})

GC_54 = Coupling(name = 'GC_54',
                 value = '-2*ca1*ca3**2*cb**2*complex(0,1)*l5*sa1*sa2**2 - 2*ca1**2*ca3*cb**2*complex(0,1)*l5*sa2*sa3 + 2*ca3*cb**2*complex(0,1)*l5*sa1**2*sa2*sa3 + 2*ca1*cb**2*complex(0,1)*l5*sa1*sa3**2 + ca2**2*ca3**2*cb*complex(0,1)*l7*sb - ca2**2*ca3**2*cb*complex(0,1)*l8*sb + ca1**2*ca3**2*cb*complex(0,1)*l1*sa2**2*sb - ca1**2*ca3**2*cb*complex(0,1)*l3*sa2**2*sb - ca1**2*ca3**2*cb*complex(0,1)*l4*sa2**2*sb + ca1**2*ca3**2*cb*complex(0,1)*l5*sa2**2*sb - ca3**2*cb*complex(0,1)*l2*sa1**2*sa2**2*sb + ca3**2*cb*complex(0,1)*l3*sa1**2*sa2**2*sb + ca3**2*cb*complex(0,1)*l4*sa1**2*sa2**2*sb - ca3**2*cb*complex(0,1)*l5*sa1**2*sa2**2*sb - 2*ca1*ca3*cb*complex(0,1)*l1*sa1*sa2*sa3*sb - 2*ca1*ca3*cb*complex(0,1)*l2*sa1*sa2*sa3*sb + 4*ca1*ca3*cb*complex(0,1)*l3*sa1*sa2*sa3*sb + 4*ca1*ca3*cb*complex(0,1)*l4*sa1*sa2*sa3*sb - 4*ca1*ca3*cb*complex(0,1)*l5*sa1*sa2*sa3*sb - ca1**2*cb*complex(0,1)*l2*sa3**2*sb + ca1**2*cb*complex(0,1)*l3*sa3**2*sb + ca1**2*cb*complex(0,1)*l4*sa3**2*sb - ca1**2*cb*complex(0,1)*l5*sa3**2*sb + cb*complex(0,1)*l1*sa1**2*sa3**2*sb - cb*complex(0,1)*l3*sa1**2*sa3**2*sb - cb*complex(0,1)*l4*sa1**2*sa3**2*sb + cb*complex(0,1)*l5*sa1**2*sa3**2*sb + 2*ca1*ca3**2*complex(0,1)*l5*sa1*sa2**2*sb**2 + 2*ca1**2*ca3*complex(0,1)*l5*sa2*sa3*sb**2 - 2*ca3*complex(0,1)*l5*sa1**2*sa2*sa3*sb**2 - 2*ca1*complex(0,1)*l5*sa1*sa3**2*sb**2',
                 order = {'QED':2})

GC_55 = Coupling(name = 'GC_55',
                 value = '-(ca2**2*ca3**2*cb**2*complex(0,1)*l8) - ca1**2*ca3**2*cb**2*complex(0,1)*l3*sa2**2 - ca3**2*cb**2*complex(0,1)*l2*sa1**2*sa2**2 - 2*ca1*ca3*cb**2*complex(0,1)*l2*sa1*sa2*sa3 + 2*ca1*ca3*cb**2*complex(0,1)*l3*sa1*sa2*sa3 - ca1**2*cb**2*complex(0,1)*l2*sa3**2 - cb**2*complex(0,1)*l3*sa1**2*sa3**2 + 2*ca1*ca3**2*cb*complex(0,1)*l4*sa1*sa2**2*sb + 2*ca1*ca3**2*cb*complex(0,1)*l5*sa1*sa2**2*sb + 2*ca1**2*ca3*cb*complex(0,1)*l4*sa2*sa3*sb + 2*ca1**2*ca3*cb*complex(0,1)*l5*sa2*sa3*sb - 2*ca3*cb*complex(0,1)*l4*sa1**2*sa2*sa3*sb - 2*ca3*cb*complex(0,1)*l5*sa1**2*sa2*sa3*sb - 2*ca1*cb*complex(0,1)*l4*sa1*sa3**2*sb - 2*ca1*cb*complex(0,1)*l5*sa1*sa3**2*sb - ca2**2*ca3**2*complex(0,1)*l7*sb**2 - ca1**2*ca3**2*complex(0,1)*l1*sa2**2*sb**2 - ca3**2*complex(0,1)*l3*sa1**2*sa2**2*sb**2 + 2*ca1*ca3*complex(0,1)*l1*sa1*sa2*sa3*sb**2 - 2*ca1*ca3*complex(0,1)*l3*sa1*sa2*sa3*sb**2 - ca1**2*complex(0,1)*l3*sa3**2*sb**2 - complex(0,1)*l1*sa1**2*sa3**2*sb**2',
                 order = {'QED':2})

GC_56 = Coupling(name = 'GC_56',
                 value = '-(ca2**2*ca3**2*cb**2*complex(0,1)*l8) - ca1**2*ca3**2*cb**2*complex(0,1)*l3*sa2**2 - ca1**2*ca3**2*cb**2*complex(0,1)*l4*sa2**2 + ca1**2*ca3**2*cb**2*complex(0,1)*l5*sa2**2 - ca3**2*cb**2*complex(0,1)*l2*sa1**2*sa2**2 - 2*ca1*ca3*cb**2*complex(0,1)*l2*sa1*sa2*sa3 + 2*ca1*ca3*cb**2*complex(0,1)*l3*sa1*sa2*sa3 + 2*ca1*ca3*cb**2*complex(0,1)*l4*sa1*sa2*sa3 - 2*ca1*ca3*cb**2*complex(0,1)*l5*sa1*sa2*sa3 - ca1**2*cb**2*complex(0,1)*l2*sa3**2 - cb**2*complex(0,1)*l3*sa1**2*sa3**2 - cb**2*complex(0,1)*l4*sa1**2*sa3**2 + cb**2*complex(0,1)*l5*sa1**2*sa3**2 + 4*ca1*ca3**2*cb*complex(0,1)*l5*sa1*sa2**2*sb + 4*ca1**2*ca3*cb*complex(0,1)*l5*sa2*sa3*sb - 4*ca3*cb*complex(0,1)*l5*sa1**2*sa2*sa3*sb - 4*ca1*cb*complex(0,1)*l5*sa1*sa3**2*sb - ca2**2*ca3**2*complex(0,1)*l7*sb**2 - ca1**2*ca3**2*complex(0,1)*l1*sa2**2*sb**2 - ca3**2*complex(0,1)*l3*sa1**2*sa2**2*sb**2 - ca3**2*complex(0,1)*l4*sa1**2*sa2**2*sb**2 + ca3**2*complex(0,1)*l5*sa1**2*sa2**2*sb**2 + 2*ca1*ca3*complex(0,1)*l1*sa1*sa2*sa3*sb**2 - 2*ca1*ca3*complex(0,1)*l3*sa1*sa2*sa3*sb**2 - 2*ca1*ca3*complex(0,1)*l4*sa1*sa2*sa3*sb**2 + 2*ca1*ca3*complex(0,1)*l5*sa1*sa2*sa3*sb**2 - ca1**2*complex(0,1)*l3*sa3**2*sb**2 - ca1**2*complex(0,1)*l4*sa3**2*sb**2 + ca1**2*complex(0,1)*l5*sa3**2*sb**2 - complex(0,1)*l1*sa1**2*sa3**2*sb**2',
                 order = {'QED':2})

GC_57 = Coupling(name = 'GC_57',
                 value = '-(ca2**2*ca3**2*cb**2*complex(0,1)*l7) - ca1**2*ca3**2*cb**2*complex(0,1)*l1*sa2**2 - ca3**2*cb**2*complex(0,1)*l3*sa1**2*sa2**2 + 2*ca1*ca3*cb**2*complex(0,1)*l1*sa1*sa2*sa3 - 2*ca1*ca3*cb**2*complex(0,1)*l3*sa1*sa2*sa3 - ca1**2*cb**2*complex(0,1)*l3*sa3**2 - cb**2*complex(0,1)*l1*sa1**2*sa3**2 - 2*ca1*ca3**2*cb*complex(0,1)*l4*sa1*sa2**2*sb - 2*ca1*ca3**2*cb*complex(0,1)*l5*sa1*sa2**2*sb - 2*ca1**2*ca3*cb*complex(0,1)*l4*sa2*sa3*sb - 2*ca1**2*ca3*cb*complex(0,1)*l5*sa2*sa3*sb + 2*ca3*cb*complex(0,1)*l4*sa1**2*sa2*sa3*sb + 2*ca3*cb*complex(0,1)*l5*sa1**2*sa2*sa3*sb + 2*ca1*cb*complex(0,1)*l4*sa1*sa3**2*sb + 2*ca1*cb*complex(0,1)*l5*sa1*sa3**2*sb - ca2**2*ca3**2*complex(0,1)*l8*sb**2 - ca1**2*ca3**2*complex(0,1)*l3*sa2**2*sb**2 - ca3**2*complex(0,1)*l2*sa1**2*sa2**2*sb**2 - 2*ca1*ca3*complex(0,1)*l2*sa1*sa2*sa3*sb**2 + 2*ca1*ca3*complex(0,1)*l3*sa1*sa2*sa3*sb**2 - ca1**2*complex(0,1)*l2*sa3**2*sb**2 - complex(0,1)*l3*sa1**2*sa3**2*sb**2',
                 order = {'QED':2})

GC_58 = Coupling(name = 'GC_58',
                 value = '-(ca2**2*ca3**2*cb**2*complex(0,1)*l7) - ca1**2*ca3**2*cb**2*complex(0,1)*l1*sa2**2 - ca3**2*cb**2*complex(0,1)*l3*sa1**2*sa2**2 - ca3**2*cb**2*complex(0,1)*l4*sa1**2*sa2**2 + ca3**2*cb**2*complex(0,1)*l5*sa1**2*sa2**2 + 2*ca1*ca3*cb**2*complex(0,1)*l1*sa1*sa2*sa3 - 2*ca1*ca3*cb**2*complex(0,1)*l3*sa1*sa2*sa3 - 2*ca1*ca3*cb**2*complex(0,1)*l4*sa1*sa2*sa3 + 2*ca1*ca3*cb**2*complex(0,1)*l5*sa1*sa2*sa3 - ca1**2*cb**2*complex(0,1)*l3*sa3**2 - ca1**2*cb**2*complex(0,1)*l4*sa3**2 + ca1**2*cb**2*complex(0,1)*l5*sa3**2 - cb**2*complex(0,1)*l1*sa1**2*sa3**2 - 4*ca1*ca3**2*cb*complex(0,1)*l5*sa1*sa2**2*sb - 4*ca1**2*ca3*cb*complex(0,1)*l5*sa2*sa3*sb + 4*ca3*cb*complex(0,1)*l5*sa1**2*sa2*sa3*sb + 4*ca1*cb*complex(0,1)*l5*sa1*sa3**2*sb - ca2**2*ca3**2*complex(0,1)*l8*sb**2 - ca1**2*ca3**2*complex(0,1)*l3*sa2**2*sb**2 - ca1**2*ca3**2*complex(0,1)*l4*sa2**2*sb**2 + ca1**2*ca3**2*complex(0,1)*l5*sa2**2*sb**2 - ca3**2*complex(0,1)*l2*sa1**2*sa2**2*sb**2 - 2*ca1*ca3*complex(0,1)*l2*sa1*sa2*sa3*sb**2 + 2*ca1*ca3*complex(0,1)*l3*sa1*sa2*sa3*sb**2 + 2*ca1*ca3*complex(0,1)*l4*sa1*sa2*sa3*sb**2 - 2*ca1*ca3*complex(0,1)*l5*sa1*sa2*sa3*sb**2 - ca1**2*complex(0,1)*l2*sa3**2*sb**2 - complex(0,1)*l3*sa1**2*sa3**2*sb**2 - complex(0,1)*l4*sa1**2*sa3**2*sb**2 + complex(0,1)*l5*sa1**2*sa3**2*sb**2',
                 order = {'QED':2})

GC_59 = Coupling(name = 'GC_59',
                 value = 'ca1*ca3**2*cb**2*complex(0,1)*l2*sa1*sa2 - ca1*ca3**2*cb**2*complex(0,1)*l3*sa1*sa2 + ca1**2*ca3*cb**2*complex(0,1)*l2*sa3 - ca2**2*ca3*cb**2*complex(0,1)*l8*sa3 + ca3*cb**2*complex(0,1)*l3*sa1**2*sa3 - ca1**2*ca3*cb**2*complex(0,1)*l3*sa2**2*sa3 - ca3*cb**2*complex(0,1)*l2*sa1**2*sa2**2*sa3 - ca1*cb**2*complex(0,1)*l2*sa1*sa2*sa3**2 + ca1*cb**2*complex(0,1)*l3*sa1*sa2*sa3**2 - ca1**2*ca3**2*cb*complex(0,1)*l4*sa2*sb - ca1**2*ca3**2*cb*complex(0,1)*l5*sa2*sb + ca3**2*cb*complex(0,1)*l4*sa1**2*sa2*sb + ca3**2*cb*complex(0,1)*l5*sa1**2*sa2*sb + 2*ca1*ca3*cb*complex(0,1)*l4*sa1*sa3*sb + 2*ca1*ca3*cb*complex(0,1)*l5*sa1*sa3*sb + 2*ca1*ca3*cb*complex(0,1)*l4*sa1*sa2**2*sa3*sb + 2*ca1*ca3*cb*complex(0,1)*l5*sa1*sa2**2*sa3*sb + ca1**2*cb*complex(0,1)*l4*sa2*sa3**2*sb + ca1**2*cb*complex(0,1)*l5*sa2*sa3**2*sb - cb*complex(0,1)*l4*sa1**2*sa2*sa3**2*sb - cb*complex(0,1)*l5*sa1**2*sa2*sa3**2*sb - ca1*ca3**2*complex(0,1)*l1*sa1*sa2*sb**2 + ca1*ca3**2*complex(0,1)*l3*sa1*sa2*sb**2 + ca1**2*ca3*complex(0,1)*l3*sa3*sb**2 - ca2**2*ca3*complex(0,1)*l7*sa3*sb**2 + ca3*complex(0,1)*l1*sa1**2*sa3*sb**2 - ca1**2*ca3*complex(0,1)*l1*sa2**2*sa3*sb**2 - ca3*complex(0,1)*l3*sa1**2*sa2**2*sa3*sb**2 + ca1*complex(0,1)*l1*sa1*sa2*sa3**2*sb**2 - ca1*complex(0,1)*l3*sa1*sa2*sa3**2*sb**2',
                 order = {'QED':2})

GC_60 = Coupling(name = 'GC_60',
                 value = '-(ca1*ca3**2*cb**2*complex(0,1)*l1*sa1*sa2) + ca1*ca3**2*cb**2*complex(0,1)*l3*sa1*sa2 + ca1**2*ca3*cb**2*complex(0,1)*l3*sa3 - ca2**2*ca3*cb**2*complex(0,1)*l7*sa3 + ca3*cb**2*complex(0,1)*l1*sa1**2*sa3 - ca1**2*ca3*cb**2*complex(0,1)*l1*sa2**2*sa3 - ca3*cb**2*complex(0,1)*l3*sa1**2*sa2**2*sa3 + ca1*cb**2*complex(0,1)*l1*sa1*sa2*sa3**2 - ca1*cb**2*complex(0,1)*l3*sa1*sa2*sa3**2 + ca1**2*ca3**2*cb*complex(0,1)*l4*sa2*sb + ca1**2*ca3**2*cb*complex(0,1)*l5*sa2*sb - ca3**2*cb*complex(0,1)*l4*sa1**2*sa2*sb - ca3**2*cb*complex(0,1)*l5*sa1**2*sa2*sb - 2*ca1*ca3*cb*complex(0,1)*l4*sa1*sa3*sb - 2*ca1*ca3*cb*complex(0,1)*l5*sa1*sa3*sb - 2*ca1*ca3*cb*complex(0,1)*l4*sa1*sa2**2*sa3*sb - 2*ca1*ca3*cb*complex(0,1)*l5*sa1*sa2**2*sa3*sb - ca1**2*cb*complex(0,1)*l4*sa2*sa3**2*sb - ca1**2*cb*complex(0,1)*l5*sa2*sa3**2*sb + cb*complex(0,1)*l4*sa1**2*sa2*sa3**2*sb + cb*complex(0,1)*l5*sa1**2*sa2*sa3**2*sb + ca1*ca3**2*complex(0,1)*l2*sa1*sa2*sb**2 - ca1*ca3**2*complex(0,1)*l3*sa1*sa2*sb**2 + ca1**2*ca3*complex(0,1)*l2*sa3*sb**2 - ca2**2*ca3*complex(0,1)*l8*sa3*sb**2 + ca3*complex(0,1)*l3*sa1**2*sa3*sb**2 - ca1**2*ca3*complex(0,1)*l3*sa2**2*sa3*sb**2 - ca3*complex(0,1)*l2*sa1**2*sa2**2*sa3*sb**2 - ca1*complex(0,1)*l2*sa1*sa2*sa3**2*sb**2 + ca1*complex(0,1)*l3*sa1*sa2*sa3**2*sb**2',
                 order = {'QED':2})

GC_61 = Coupling(name = 'GC_61',
                 value = '-(ca1*ca3**2*cb**2*complex(0,1)*l1*sa1*sa2) + ca1*ca3**2*cb**2*complex(0,1)*l3*sa1*sa2 + ca1*ca3**2*cb**2*complex(0,1)*l4*sa1*sa2 - ca1*ca3**2*cb**2*complex(0,1)*l5*sa1*sa2 + ca1**2*ca3*cb**2*complex(0,1)*l3*sa3 + ca1**2*ca3*cb**2*complex(0,1)*l4*sa3 - ca1**2*ca3*cb**2*complex(0,1)*l5*sa3 - ca2**2*ca3*cb**2*complex(0,1)*l7*sa3 + ca3*cb**2*complex(0,1)*l1*sa1**2*sa3 - ca1**2*ca3*cb**2*complex(0,1)*l1*sa2**2*sa3 - ca3*cb**2*complex(0,1)*l3*sa1**2*sa2**2*sa3 - ca3*cb**2*complex(0,1)*l4*sa1**2*sa2**2*sa3 + ca3*cb**2*complex(0,1)*l5*sa1**2*sa2**2*sa3 + ca1*cb**2*complex(0,1)*l1*sa1*sa2*sa3**2 - ca1*cb**2*complex(0,1)*l3*sa1*sa2*sa3**2 - ca1*cb**2*complex(0,1)*l4*sa1*sa2*sa3**2 + ca1*cb**2*complex(0,1)*l5*sa1*sa2*sa3**2 + 2*ca1**2*ca3**2*cb*complex(0,1)*l5*sa2*sb - 2*ca3**2*cb*complex(0,1)*l5*sa1**2*sa2*sb - 4*ca1*ca3*cb*complex(0,1)*l5*sa1*sa3*sb - 4*ca1*ca3*cb*complex(0,1)*l5*sa1*sa2**2*sa3*sb - 2*ca1**2*cb*complex(0,1)*l5*sa2*sa3**2*sb + 2*cb*complex(0,1)*l5*sa1**2*sa2*sa3**2*sb + ca1*ca3**2*complex(0,1)*l2*sa1*sa2*sb**2 - ca1*ca3**2*complex(0,1)*l3*sa1*sa2*sb**2 - ca1*ca3**2*complex(0,1)*l4*sa1*sa2*sb**2 + ca1*ca3**2*complex(0,1)*l5*sa1*sa2*sb**2 + ca1**2*ca3*complex(0,1)*l2*sa3*sb**2 - ca2**2*ca3*complex(0,1)*l8*sa3*sb**2 + ca3*complex(0,1)*l3*sa1**2*sa3*sb**2 + ca3*complex(0,1)*l4*sa1**2*sa3*sb**2 - ca3*complex(0,1)*l5*sa1**2*sa3*sb**2 - ca1**2*ca3*complex(0,1)*l3*sa2**2*sa3*sb**2 - ca1**2*ca3*complex(0,1)*l4*sa2**2*sa3*sb**2 + ca1**2*ca3*complex(0,1)*l5*sa2**2*sa3*sb**2 - ca3*complex(0,1)*l2*sa1**2*sa2**2*sa3*sb**2 - ca1*complex(0,1)*l2*sa1*sa2*sa3**2*sb**2 + ca1*complex(0,1)*l3*sa1*sa2*sa3**2*sb**2 + ca1*complex(0,1)*l4*sa1*sa2*sa3**2*sb**2 - ca1*complex(0,1)*l5*sa1*sa2*sa3**2*sb**2',
                 order = {'QED':2})

GC_62 = Coupling(name = 'GC_62',
                 value = 'ca1*ca3**2*cb**2*complex(0,1)*l2*sa1*sa2 - ca1*ca3**2*cb**2*complex(0,1)*l3*sa1*sa2 - ca1*ca3**2*cb**2*complex(0,1)*l4*sa1*sa2 + ca1*ca3**2*cb**2*complex(0,1)*l5*sa1*sa2 + ca1**2*ca3*cb**2*complex(0,1)*l2*sa3 - ca2**2*ca3*cb**2*complex(0,1)*l8*sa3 + ca3*cb**2*complex(0,1)*l3*sa1**2*sa3 + ca3*cb**2*complex(0,1)*l4*sa1**2*sa3 - ca3*cb**2*complex(0,1)*l5*sa1**2*sa3 - ca1**2*ca3*cb**2*complex(0,1)*l3*sa2**2*sa3 - ca1**2*ca3*cb**2*complex(0,1)*l4*sa2**2*sa3 + ca1**2*ca3*cb**2*complex(0,1)*l5*sa2**2*sa3 - ca3*cb**2*complex(0,1)*l2*sa1**2*sa2**2*sa3 - ca1*cb**2*complex(0,1)*l2*sa1*sa2*sa3**2 + ca1*cb**2*complex(0,1)*l3*sa1*sa2*sa3**2 + ca1*cb**2*complex(0,1)*l4*sa1*sa2*sa3**2 - ca1*cb**2*complex(0,1)*l5*sa1*sa2*sa3**2 - 2*ca1**2*ca3**2*cb*complex(0,1)*l5*sa2*sb + 2*ca3**2*cb*complex(0,1)*l5*sa1**2*sa2*sb + 4*ca1*ca3*cb*complex(0,1)*l5*sa1*sa3*sb + 4*ca1*ca3*cb*complex(0,1)*l5*sa1*sa2**2*sa3*sb + 2*ca1**2*cb*complex(0,1)*l5*sa2*sa3**2*sb - 2*cb*complex(0,1)*l5*sa1**2*sa2*sa3**2*sb - ca1*ca3**2*complex(0,1)*l1*sa1*sa2*sb**2 + ca1*ca3**2*complex(0,1)*l3*sa1*sa2*sb**2 + ca1*ca3**2*complex(0,1)*l4*sa1*sa2*sb**2 - ca1*ca3**2*complex(0,1)*l5*sa1*sa2*sb**2 + ca1**2*ca3*complex(0,1)*l3*sa3*sb**2 + ca1**2*ca3*complex(0,1)*l4*sa3*sb**2 - ca1**2*ca3*complex(0,1)*l5*sa3*sb**2 - ca2**2*ca3*complex(0,1)*l7*sa3*sb**2 + ca3*complex(0,1)*l1*sa1**2*sa3*sb**2 - ca1**2*ca3*complex(0,1)*l1*sa2**2*sa3*sb**2 - ca3*complex(0,1)*l3*sa1**2*sa2**2*sa3*sb**2 - ca3*complex(0,1)*l4*sa1**2*sa2**2*sa3*sb**2 + ca3*complex(0,1)*l5*sa1**2*sa2**2*sa3*sb**2 + ca1*complex(0,1)*l1*sa1*sa2*sa3**2*sb**2 - ca1*complex(0,1)*l3*sa1*sa2*sa3**2*sb**2 - ca1*complex(0,1)*l4*sa1*sa2*sa3**2*sb**2 + ca1*complex(0,1)*l5*sa1*sa2*sa3**2*sb**2',
                 order = {'QED':2})

GC_63 = Coupling(name = 'GC_63',
                 value = '(ca1**2*ca3**2*cb**2*complex(0,1)*l4*sa2)/2. + (ca1**2*ca3**2*cb**2*complex(0,1)*l5*sa2)/2. - (ca3**2*cb**2*complex(0,1)*l4*sa1**2*sa2)/2. - (ca3**2*cb**2*complex(0,1)*l5*sa1**2*sa2)/2. - ca1*ca3*cb**2*complex(0,1)*l4*sa1*sa3 - ca1*ca3*cb**2*complex(0,1)*l5*sa1*sa3 - ca1*ca3*cb**2*complex(0,1)*l4*sa1*sa2**2*sa3 - ca1*ca3*cb**2*complex(0,1)*l5*sa1*sa2**2*sa3 - (ca1**2*cb**2*complex(0,1)*l4*sa2*sa3**2)/2. - (ca1**2*cb**2*complex(0,1)*l5*sa2*sa3**2)/2. + (cb**2*complex(0,1)*l4*sa1**2*sa2*sa3**2)/2. + (cb**2*complex(0,1)*l5*sa1**2*sa2*sa3**2)/2. + ca1*ca3**2*cb*complex(0,1)*l1*sa1*sa2*sb + ca1*ca3**2*cb*complex(0,1)*l2*sa1*sa2*sb - 2*ca1*ca3**2*cb*complex(0,1)*l3*sa1*sa2*sb + ca1**2*ca3*cb*complex(0,1)*l2*sa3*sb - ca1**2*ca3*cb*complex(0,1)*l3*sa3*sb + ca2**2*ca3*cb*complex(0,1)*l7*sa3*sb - ca2**2*ca3*cb*complex(0,1)*l8*sa3*sb - ca3*cb*complex(0,1)*l1*sa1**2*sa3*sb + ca3*cb*complex(0,1)*l3*sa1**2*sa3*sb + ca1**2*ca3*cb*complex(0,1)*l1*sa2**2*sa3*sb - ca1**2*ca3*cb*complex(0,1)*l3*sa2**2*sa3*sb - ca3*cb*complex(0,1)*l2*sa1**2*sa2**2*sa3*sb + ca3*cb*complex(0,1)*l3*sa1**2*sa2**2*sa3*sb - ca1*cb*complex(0,1)*l1*sa1*sa2*sa3**2*sb - ca1*cb*complex(0,1)*l2*sa1*sa2*sa3**2*sb + 2*ca1*cb*complex(0,1)*l3*sa1*sa2*sa3**2*sb - (ca1**2*ca3**2*complex(0,1)*l4*sa2*sb**2)/2. - (ca1**2*ca3**2*complex(0,1)*l5*sa2*sb**2)/2. + (ca3**2*complex(0,1)*l4*sa1**2*sa2*sb**2)/2. + (ca3**2*complex(0,1)*l5*sa1**2*sa2*sb**2)/2. + ca1*ca3*complex(0,1)*l4*sa1*sa3*sb**2 + ca1*ca3*complex(0,1)*l5*sa1*sa3*sb**2 + ca1*ca3*complex(0,1)*l4*sa1*sa2**2*sa3*sb**2 + ca1*ca3*complex(0,1)*l5*sa1*sa2**2*sa3*sb**2 + (ca1**2*complex(0,1)*l4*sa2*sa3**2*sb**2)/2. + (ca1**2*complex(0,1)*l5*sa2*sa3**2*sb**2)/2. - (complex(0,1)*l4*sa1**2*sa2*sa3**2*sb**2)/2. - (complex(0,1)*l5*sa1**2*sa2*sa3**2*sb**2)/2.',
                 order = {'QED':2})

GC_64 = Coupling(name = 'GC_64',
                 value = 'ca1**2*ca3**2*cb**2*complex(0,1)*l5*sa2 - ca3**2*cb**2*complex(0,1)*l5*sa1**2*sa2 - 2*ca1*ca3*cb**2*complex(0,1)*l5*sa1*sa3 - 2*ca1*ca3*cb**2*complex(0,1)*l5*sa1*sa2**2*sa3 - ca1**2*cb**2*complex(0,1)*l5*sa2*sa3**2 + cb**2*complex(0,1)*l5*sa1**2*sa2*sa3**2 + ca1*ca3**2*cb*complex(0,1)*l1*sa1*sa2*sb + ca1*ca3**2*cb*complex(0,1)*l2*sa1*sa2*sb - 2*ca1*ca3**2*cb*complex(0,1)*l3*sa1*sa2*sb - 2*ca1*ca3**2*cb*complex(0,1)*l4*sa1*sa2*sb + 2*ca1*ca3**2*cb*complex(0,1)*l5*sa1*sa2*sb + ca1**2*ca3*cb*complex(0,1)*l2*sa3*sb - ca1**2*ca3*cb*complex(0,1)*l3*sa3*sb - ca1**2*ca3*cb*complex(0,1)*l4*sa3*sb + ca1**2*ca3*cb*complex(0,1)*l5*sa3*sb + ca2**2*ca3*cb*complex(0,1)*l7*sa3*sb - ca2**2*ca3*cb*complex(0,1)*l8*sa3*sb - ca3*cb*complex(0,1)*l1*sa1**2*sa3*sb + ca3*cb*complex(0,1)*l3*sa1**2*sa3*sb + ca3*cb*complex(0,1)*l4*sa1**2*sa3*sb - ca3*cb*complex(0,1)*l5*sa1**2*sa3*sb + ca1**2*ca3*cb*complex(0,1)*l1*sa2**2*sa3*sb - ca1**2*ca3*cb*complex(0,1)*l3*sa2**2*sa3*sb - ca1**2*ca3*cb*complex(0,1)*l4*sa2**2*sa3*sb + ca1**2*ca3*cb*complex(0,1)*l5*sa2**2*sa3*sb - ca3*cb*complex(0,1)*l2*sa1**2*sa2**2*sa3*sb + ca3*cb*complex(0,1)*l3*sa1**2*sa2**2*sa3*sb + ca3*cb*complex(0,1)*l4*sa1**2*sa2**2*sa3*sb - ca3*cb*complex(0,1)*l5*sa1**2*sa2**2*sa3*sb - ca1*cb*complex(0,1)*l1*sa1*sa2*sa3**2*sb - ca1*cb*complex(0,1)*l2*sa1*sa2*sa3**2*sb + 2*ca1*cb*complex(0,1)*l3*sa1*sa2*sa3**2*sb + 2*ca1*cb*complex(0,1)*l4*sa1*sa2*sa3**2*sb - 2*ca1*cb*complex(0,1)*l5*sa1*sa2*sa3**2*sb - ca1**2*ca3**2*complex(0,1)*l5*sa2*sb**2 + ca3**2*complex(0,1)*l5*sa1**2*sa2*sb**2 + 2*ca1*ca3*complex(0,1)*l5*sa1*sa3*sb**2 + 2*ca1*ca3*complex(0,1)*l5*sa1*sa2**2*sa3*sb**2 + ca1**2*complex(0,1)*l5*sa2*sa3**2*sb**2 - complex(0,1)*l5*sa1**2*sa2*sa3**2*sb**2',
                 order = {'QED':2})

GC_65 = Coupling(name = 'GC_65',
                 value = 'ca1*ca3**2*cb**2*complex(0,1)*l4*sa1 + ca1*ca3**2*cb**2*complex(0,1)*l5*sa1 + ca1**2*ca3*cb**2*complex(0,1)*l4*sa2*sa3 + ca1**2*ca3*cb**2*complex(0,1)*l5*sa2*sa3 - ca3*cb**2*complex(0,1)*l4*sa1**2*sa2*sa3 - ca3*cb**2*complex(0,1)*l5*sa1**2*sa2*sa3 - ca1*cb**2*complex(0,1)*l4*sa1*sa2**2*sa3**2 - ca1*cb**2*complex(0,1)*l5*sa1*sa2**2*sa3**2 - ca1**2*ca3**2*cb*complex(0,1)*l2*sb + ca1**2*ca3**2*cb*complex(0,1)*l3*sb + ca3**2*cb*complex(0,1)*l1*sa1**2*sb - ca3**2*cb*complex(0,1)*l3*sa1**2*sb + 2*ca1*ca3*cb*complex(0,1)*l1*sa1*sa2*sa3*sb + 2*ca1*ca3*cb*complex(0,1)*l2*sa1*sa2*sa3*sb - 4*ca1*ca3*cb*complex(0,1)*l3*sa1*sa2*sa3*sb + ca2**2*cb*complex(0,1)*l7*sa3**2*sb - ca2**2*cb*complex(0,1)*l8*sa3**2*sb + ca1**2*cb*complex(0,1)*l1*sa2**2*sa3**2*sb - ca1**2*cb*complex(0,1)*l3*sa2**2*sa3**2*sb - cb*complex(0,1)*l2*sa1**2*sa2**2*sa3**2*sb + cb*complex(0,1)*l3*sa1**2*sa2**2*sa3**2*sb - ca1*ca3**2*complex(0,1)*l4*sa1*sb**2 - ca1*ca3**2*complex(0,1)*l5*sa1*sb**2 - ca1**2*ca3*complex(0,1)*l4*sa2*sa3*sb**2 - ca1**2*ca3*complex(0,1)*l5*sa2*sa3*sb**2 + ca3*complex(0,1)*l4*sa1**2*sa2*sa3*sb**2 + ca3*complex(0,1)*l5*sa1**2*sa2*sa3*sb**2 + ca1*complex(0,1)*l4*sa1*sa2**2*sa3**2*sb**2 + ca1*complex(0,1)*l5*sa1*sa2**2*sa3**2*sb**2',
                 order = {'QED':2})

GC_66 = Coupling(name = 'GC_66',
                 value = '2*ca1*ca3**2*cb**2*complex(0,1)*l5*sa1 + 2*ca1**2*ca3*cb**2*complex(0,1)*l5*sa2*sa3 - 2*ca3*cb**2*complex(0,1)*l5*sa1**2*sa2*sa3 - 2*ca1*cb**2*complex(0,1)*l5*sa1*sa2**2*sa3**2 - ca1**2*ca3**2*cb*complex(0,1)*l2*sb + ca1**2*ca3**2*cb*complex(0,1)*l3*sb + ca1**2*ca3**2*cb*complex(0,1)*l4*sb - ca1**2*ca3**2*cb*complex(0,1)*l5*sb + ca3**2*cb*complex(0,1)*l1*sa1**2*sb - ca3**2*cb*complex(0,1)*l3*sa1**2*sb - ca3**2*cb*complex(0,1)*l4*sa1**2*sb + ca3**2*cb*complex(0,1)*l5*sa1**2*sb + 2*ca1*ca3*cb*complex(0,1)*l1*sa1*sa2*sa3*sb + 2*ca1*ca3*cb*complex(0,1)*l2*sa1*sa2*sa3*sb - 4*ca1*ca3*cb*complex(0,1)*l3*sa1*sa2*sa3*sb - 4*ca1*ca3*cb*complex(0,1)*l4*sa1*sa2*sa3*sb + 4*ca1*ca3*cb*complex(0,1)*l5*sa1*sa2*sa3*sb + ca2**2*cb*complex(0,1)*l7*sa3**2*sb - ca2**2*cb*complex(0,1)*l8*sa3**2*sb + ca1**2*cb*complex(0,1)*l1*sa2**2*sa3**2*sb - ca1**2*cb*complex(0,1)*l3*sa2**2*sa3**2*sb - ca1**2*cb*complex(0,1)*l4*sa2**2*sa3**2*sb + ca1**2*cb*complex(0,1)*l5*sa2**2*sa3**2*sb - cb*complex(0,1)*l2*sa1**2*sa2**2*sa3**2*sb + cb*complex(0,1)*l3*sa1**2*sa2**2*sa3**2*sb + cb*complex(0,1)*l4*sa1**2*sa2**2*sa3**2*sb - cb*complex(0,1)*l5*sa1**2*sa2**2*sa3**2*sb - 2*ca1*ca3**2*complex(0,1)*l5*sa1*sb**2 - 2*ca1**2*ca3*complex(0,1)*l5*sa2*sa3*sb**2 + 2*ca3*complex(0,1)*l5*sa1**2*sa2*sa3*sb**2 + 2*ca1*complex(0,1)*l5*sa1*sa2**2*sa3**2*sb**2',
                 order = {'QED':2})

GC_67 = Coupling(name = 'GC_67',
                 value = '-(ca1**2*ca3**2*cb**2*complex(0,1)*l3) - ca3**2*cb**2*complex(0,1)*l1*sa1**2 - 2*ca1*ca3*cb**2*complex(0,1)*l1*sa1*sa2*sa3 + 2*ca1*ca3*cb**2*complex(0,1)*l3*sa1*sa2*sa3 - ca2**2*cb**2*complex(0,1)*l7*sa3**2 - ca1**2*cb**2*complex(0,1)*l1*sa2**2*sa3**2 - cb**2*complex(0,1)*l3*sa1**2*sa2**2*sa3**2 + 2*ca1*ca3**2*cb*complex(0,1)*l4*sa1*sb + 2*ca1*ca3**2*cb*complex(0,1)*l5*sa1*sb + 2*ca1**2*ca3*cb*complex(0,1)*l4*sa2*sa3*sb + 2*ca1**2*ca3*cb*complex(0,1)*l5*sa2*sa3*sb - 2*ca3*cb*complex(0,1)*l4*sa1**2*sa2*sa3*sb - 2*ca3*cb*complex(0,1)*l5*sa1**2*sa2*sa3*sb - 2*ca1*cb*complex(0,1)*l4*sa1*sa2**2*sa3**2*sb - 2*ca1*cb*complex(0,1)*l5*sa1*sa2**2*sa3**2*sb - ca1**2*ca3**2*complex(0,1)*l2*sb**2 - ca3**2*complex(0,1)*l3*sa1**2*sb**2 + 2*ca1*ca3*complex(0,1)*l2*sa1*sa2*sa3*sb**2 - 2*ca1*ca3*complex(0,1)*l3*sa1*sa2*sa3*sb**2 - ca2**2*complex(0,1)*l8*sa3**2*sb**2 - ca1**2*complex(0,1)*l3*sa2**2*sa3**2*sb**2 - complex(0,1)*l2*sa1**2*sa2**2*sa3**2*sb**2',
                 order = {'QED':2})

GC_68 = Coupling(name = 'GC_68',
                 value = '-(ca1**2*ca3**2*cb**2*complex(0,1)*l3) - ca1**2*ca3**2*cb**2*complex(0,1)*l4 + ca1**2*ca3**2*cb**2*complex(0,1)*l5 - ca3**2*cb**2*complex(0,1)*l1*sa1**2 - 2*ca1*ca3*cb**2*complex(0,1)*l1*sa1*sa2*sa3 + 2*ca1*ca3*cb**2*complex(0,1)*l3*sa1*sa2*sa3 + 2*ca1*ca3*cb**2*complex(0,1)*l4*sa1*sa2*sa3 - 2*ca1*ca3*cb**2*complex(0,1)*l5*sa1*sa2*sa3 - ca2**2*cb**2*complex(0,1)*l7*sa3**2 - ca1**2*cb**2*complex(0,1)*l1*sa2**2*sa3**2 - cb**2*complex(0,1)*l3*sa1**2*sa2**2*sa3**2 - cb**2*complex(0,1)*l4*sa1**2*sa2**2*sa3**2 + cb**2*complex(0,1)*l5*sa1**2*sa2**2*sa3**2 + 4*ca1*ca3**2*cb*complex(0,1)*l5*sa1*sb + 4*ca1**2*ca3*cb*complex(0,1)*l5*sa2*sa3*sb - 4*ca3*cb*complex(0,1)*l5*sa1**2*sa2*sa3*sb - 4*ca1*cb*complex(0,1)*l5*sa1*sa2**2*sa3**2*sb - ca1**2*ca3**2*complex(0,1)*l2*sb**2 - ca3**2*complex(0,1)*l3*sa1**2*sb**2 - ca3**2*complex(0,1)*l4*sa1**2*sb**2 + ca3**2*complex(0,1)*l5*sa1**2*sb**2 + 2*ca1*ca3*complex(0,1)*l2*sa1*sa2*sa3*sb**2 - 2*ca1*ca3*complex(0,1)*l3*sa1*sa2*sa3*sb**2 - 2*ca1*ca3*complex(0,1)*l4*sa1*sa2*sa3*sb**2 + 2*ca1*ca3*complex(0,1)*l5*sa1*sa2*sa3*sb**2 - ca2**2*complex(0,1)*l8*sa3**2*sb**2 - ca1**2*complex(0,1)*l3*sa2**2*sa3**2*sb**2 - ca1**2*complex(0,1)*l4*sa2**2*sa3**2*sb**2 + ca1**2*complex(0,1)*l5*sa2**2*sa3**2*sb**2 - complex(0,1)*l2*sa1**2*sa2**2*sa3**2*sb**2',
                 order = {'QED':2})

GC_69 = Coupling(name = 'GC_69',
                 value = '-(ca1**2*ca3**2*cb**2*complex(0,1)*l2) - ca3**2*cb**2*complex(0,1)*l3*sa1**2 + 2*ca1*ca3*cb**2*complex(0,1)*l2*sa1*sa2*sa3 - 2*ca1*ca3*cb**2*complex(0,1)*l3*sa1*sa2*sa3 - ca2**2*cb**2*complex(0,1)*l8*sa3**2 - ca1**2*cb**2*complex(0,1)*l3*sa2**2*sa3**2 - cb**2*complex(0,1)*l2*sa1**2*sa2**2*sa3**2 - 2*ca1*ca3**2*cb*complex(0,1)*l4*sa1*sb - 2*ca1*ca3**2*cb*complex(0,1)*l5*sa1*sb - 2*ca1**2*ca3*cb*complex(0,1)*l4*sa2*sa3*sb - 2*ca1**2*ca3*cb*complex(0,1)*l5*sa2*sa3*sb + 2*ca3*cb*complex(0,1)*l4*sa1**2*sa2*sa3*sb + 2*ca3*cb*complex(0,1)*l5*sa1**2*sa2*sa3*sb + 2*ca1*cb*complex(0,1)*l4*sa1*sa2**2*sa3**2*sb + 2*ca1*cb*complex(0,1)*l5*sa1*sa2**2*sa3**2*sb - ca1**2*ca3**2*complex(0,1)*l3*sb**2 - ca3**2*complex(0,1)*l1*sa1**2*sb**2 - 2*ca1*ca3*complex(0,1)*l1*sa1*sa2*sa3*sb**2 + 2*ca1*ca3*complex(0,1)*l3*sa1*sa2*sa3*sb**2 - ca2**2*complex(0,1)*l7*sa3**2*sb**2 - ca1**2*complex(0,1)*l1*sa2**2*sa3**2*sb**2 - complex(0,1)*l3*sa1**2*sa2**2*sa3**2*sb**2',
                 order = {'QED':2})

GC_70 = Coupling(name = 'GC_70',
                 value = '-(ca1**2*ca3**2*cb**2*complex(0,1)*l2) - ca3**2*cb**2*complex(0,1)*l3*sa1**2 - ca3**2*cb**2*complex(0,1)*l4*sa1**2 + ca3**2*cb**2*complex(0,1)*l5*sa1**2 + 2*ca1*ca3*cb**2*complex(0,1)*l2*sa1*sa2*sa3 - 2*ca1*ca3*cb**2*complex(0,1)*l3*sa1*sa2*sa3 - 2*ca1*ca3*cb**2*complex(0,1)*l4*sa1*sa2*sa3 + 2*ca1*ca3*cb**2*complex(0,1)*l5*sa1*sa2*sa3 - ca2**2*cb**2*complex(0,1)*l8*sa3**2 - ca1**2*cb**2*complex(0,1)*l3*sa2**2*sa3**2 - ca1**2*cb**2*complex(0,1)*l4*sa2**2*sa3**2 + ca1**2*cb**2*complex(0,1)*l5*sa2**2*sa3**2 - cb**2*complex(0,1)*l2*sa1**2*sa2**2*sa3**2 - 4*ca1*ca3**2*cb*complex(0,1)*l5*sa1*sb - 4*ca1**2*ca3*cb*complex(0,1)*l5*sa2*sa3*sb + 4*ca3*cb*complex(0,1)*l5*sa1**2*sa2*sa3*sb + 4*ca1*cb*complex(0,1)*l5*sa1*sa2**2*sa3**2*sb - ca1**2*ca3**2*complex(0,1)*l3*sb**2 - ca1**2*ca3**2*complex(0,1)*l4*sb**2 + ca1**2*ca3**2*complex(0,1)*l5*sb**2 - ca3**2*complex(0,1)*l1*sa1**2*sb**2 - 2*ca1*ca3*complex(0,1)*l1*sa1*sa2*sa3*sb**2 + 2*ca1*ca3*complex(0,1)*l3*sa1*sa2*sa3*sb**2 + 2*ca1*ca3*complex(0,1)*l4*sa1*sa2*sa3*sb**2 - 2*ca1*ca3*complex(0,1)*l5*sa1*sa2*sa3*sb**2 - ca2**2*complex(0,1)*l7*sa3**2*sb**2 - ca1**2*complex(0,1)*l1*sa2**2*sa3**2*sb**2 - complex(0,1)*l3*sa1**2*sa2**2*sa3**2*sb**2 - complex(0,1)*l4*sa1**2*sa2**2*sa3**2*sb**2 + complex(0,1)*l5*sa1**2*sa2**2*sa3**2*sb**2',
                 order = {'QED':2})

GC_71 = Coupling(name = 'GC_71',
                 value = '-(ca2*cb**3*l4*sa1)/2. + (ca2*cb**3*l5*sa1)/2. + (ca1*ca2*cb**2*l4*sb)/2. - (ca1*ca2*cb**2*l5*sb)/2. - (ca2*cb*l4*sa1*sb**2)/2. + (ca2*cb*l5*sa1*sb**2)/2. + (ca1*ca2*l4*sb**3)/2. - (ca1*ca2*l5*sb**3)/2.',
                 order = {'QED':2})

GC_72 = Coupling(name = 'GC_72',
                 value = '(ca2*cb**3*l4*sa1)/2. - (ca2*cb**3*l5*sa1)/2. - (ca1*ca2*cb**2*l4*sb)/2. + (ca1*ca2*cb**2*l5*sb)/2. + (ca2*cb*l4*sa1*sb**2)/2. - (ca2*cb*l5*sa1*sb**2)/2. - (ca1*ca2*l4*sb**3)/2. + (ca1*ca2*l5*sb**3)/2.',
                 order = {'QED':2})

GC_73 = Coupling(name = 'GC_73',
                 value = '-(cb**3*complex(0,1)*l2*sb) + cb**3*complex(0,1)*l3*sb + cb**3*complex(0,1)*l4*sb + cb**3*complex(0,1)*l5*sb + cb*complex(0,1)*l1*sb**3 - cb*complex(0,1)*l3*sb**3 - cb*complex(0,1)*l4*sb**3 - cb*complex(0,1)*l5*sb**3',
                 order = {'QED':2})

GC_74 = Coupling(name = 'GC_74',
                 value = 'cb**3*complex(0,1)*l1*sb - cb**3*complex(0,1)*l3*sb - cb**3*complex(0,1)*l4*sb - cb**3*complex(0,1)*l5*sb - cb*complex(0,1)*l2*sb**3 + cb*complex(0,1)*l3*sb**3 + cb*complex(0,1)*l4*sb**3 + cb*complex(0,1)*l5*sb**3',
                 order = {'QED':2})

GC_75 = Coupling(name = 'GC_75',
                 value = '-2*cb**3*complex(0,1)*l2*sb + 2*cb**3*complex(0,1)*l3*sb + 2*cb**3*complex(0,1)*l4*sb + 2*cb**3*complex(0,1)*l5*sb + 2*cb*complex(0,1)*l1*sb**3 - 2*cb*complex(0,1)*l3*sb**3 - 2*cb*complex(0,1)*l4*sb**3 - 2*cb*complex(0,1)*l5*sb**3',
                 order = {'QED':2})

GC_76 = Coupling(name = 'GC_76',
                 value = '2*cb**3*complex(0,1)*l1*sb - 2*cb**3*complex(0,1)*l3*sb - 2*cb**3*complex(0,1)*l4*sb - 2*cb**3*complex(0,1)*l5*sb - 2*cb*complex(0,1)*l2*sb**3 + 2*cb*complex(0,1)*l3*sb**3 + 2*cb*complex(0,1)*l4*sb**3 + 2*cb*complex(0,1)*l5*sb**3',
                 order = {'QED':2})

GC_77 = Coupling(name = 'GC_77',
                 value = '-3*cb**3*complex(0,1)*l2*sb + 3*cb**3*complex(0,1)*l3*sb + 3*cb**3*complex(0,1)*l4*sb + 3*cb**3*complex(0,1)*l5*sb + 3*cb*complex(0,1)*l1*sb**3 - 3*cb*complex(0,1)*l3*sb**3 - 3*cb*complex(0,1)*l4*sb**3 - 3*cb*complex(0,1)*l5*sb**3',
                 order = {'QED':2})

GC_78 = Coupling(name = 'GC_78',
                 value = '3*cb**3*complex(0,1)*l1*sb - 3*cb**3*complex(0,1)*l3*sb - 3*cb**3*complex(0,1)*l4*sb - 3*cb**3*complex(0,1)*l5*sb - 3*cb*complex(0,1)*l2*sb**3 + 3*cb*complex(0,1)*l3*sb**3 + 3*cb*complex(0,1)*l4*sb**3 + 3*cb*complex(0,1)*l5*sb**3',
                 order = {'QED':2})

GC_79 = Coupling(name = 'GC_79',
                 value = '(ca1*ca2*cb**3*l4)/2. - (ca1*ca2*cb**3*l5)/2. + (ca2*cb**2*l4*sa1*sb)/2. - (ca2*cb**2*l5*sa1*sb)/2. + (ca1*ca2*cb*l4*sb**2)/2. - (ca1*ca2*cb*l5*sb**2)/2. + (ca2*l4*sa1*sb**3)/2. - (ca2*l5*sa1*sb**3)/2.',
                 order = {'QED':2})

GC_80 = Coupling(name = 'GC_80',
                 value = '-(ca1*ca2*cb**3*l4)/2. + (ca1*ca2*cb**3*l5)/2. - (ca2*cb**2*l4*sa1*sb)/2. + (ca2*cb**2*l5*sa1*sb)/2. - (ca1*ca2*cb*l4*sb**2)/2. + (ca1*ca2*cb*l5*sb**2)/2. - (ca2*l4*sa1*sb**3)/2. + (ca2*l5*sa1*sb**3)/2.',
                 order = {'QED':2})

GC_81 = Coupling(name = 'GC_81',
                 value = '(ca1*ca3*cb**3*l4*sa2)/2. - (ca1*ca3*cb**3*l5*sa2)/2. - (cb**3*l4*sa1*sa3)/2. + (cb**3*l5*sa1*sa3)/2. + (ca3*cb**2*l4*sa1*sa2*sb)/2. - (ca3*cb**2*l5*sa1*sa2*sb)/2. + (ca1*cb**2*l4*sa3*sb)/2. - (ca1*cb**2*l5*sa3*sb)/2. + (ca1*ca3*cb*l4*sa2*sb**2)/2. - (ca1*ca3*cb*l5*sa2*sb**2)/2. - (cb*l4*sa1*sa3*sb**2)/2. + (cb*l5*sa1*sa3*sb**2)/2. + (ca3*l4*sa1*sa2*sb**3)/2. - (ca3*l5*sa1*sa2*sb**3)/2. + (ca1*l4*sa3*sb**3)/2. - (ca1*l5*sa3*sb**3)/2.',
                 order = {'QED':2})

GC_82 = Coupling(name = 'GC_82',
                 value = '-(ca1*ca3*cb**3*l4*sa2)/2. + (ca1*ca3*cb**3*l5*sa2)/2. + (cb**3*l4*sa1*sa3)/2. - (cb**3*l5*sa1*sa3)/2. - (ca3*cb**2*l4*sa1*sa2*sb)/2. + (ca3*cb**2*l5*sa1*sa2*sb)/2. - (ca1*cb**2*l4*sa3*sb)/2. + (ca1*cb**2*l5*sa3*sb)/2. - (ca1*ca3*cb*l4*sa2*sb**2)/2. + (ca1*ca3*cb*l5*sa2*sb**2)/2. + (cb*l4*sa1*sa3*sb**2)/2. - (cb*l5*sa1*sa3*sb**2)/2. - (ca3*l4*sa1*sa2*sb**3)/2. + (ca3*l5*sa1*sa2*sb**3)/2. - (ca1*l4*sa3*sb**3)/2. + (ca1*l5*sa3*sb**3)/2.',
                 order = {'QED':2})

GC_83 = Coupling(name = 'GC_83',
                 value = '(ca3*cb**3*l4*sa1*sa2)/2. - (ca3*cb**3*l5*sa1*sa2)/2. + (ca1*cb**3*l4*sa3)/2. - (ca1*cb**3*l5*sa3)/2. - (ca1*ca3*cb**2*l4*sa2*sb)/2. + (ca1*ca3*cb**2*l5*sa2*sb)/2. + (cb**2*l4*sa1*sa3*sb)/2. - (cb**2*l5*sa1*sa3*sb)/2. + (ca3*cb*l4*sa1*sa2*sb**2)/2. - (ca3*cb*l5*sa1*sa2*sb**2)/2. + (ca1*cb*l4*sa3*sb**2)/2. - (ca1*cb*l5*sa3*sb**2)/2. - (ca1*ca3*l4*sa2*sb**3)/2. + (ca1*ca3*l5*sa2*sb**3)/2. + (l4*sa1*sa3*sb**3)/2. - (l5*sa1*sa3*sb**3)/2.',
                 order = {'QED':2})

GC_84 = Coupling(name = 'GC_84',
                 value = '-(ca3*cb**3*l4*sa1*sa2)/2. + (ca3*cb**3*l5*sa1*sa2)/2. - (ca1*cb**3*l4*sa3)/2. + (ca1*cb**3*l5*sa3)/2. + (ca1*ca3*cb**2*l4*sa2*sb)/2. - (ca1*ca3*cb**2*l5*sa2*sb)/2. - (cb**2*l4*sa1*sa3*sb)/2. + (cb**2*l5*sa1*sa3*sb)/2. - (ca3*cb*l4*sa1*sa2*sb**2)/2. + (ca3*cb*l5*sa1*sa2*sb**2)/2. - (ca1*cb*l4*sa3*sb**2)/2. + (ca1*cb*l5*sa3*sb**2)/2. + (ca1*ca3*l4*sa2*sb**3)/2. - (ca1*ca3*l5*sa2*sb**3)/2. - (l4*sa1*sa3*sb**3)/2. + (l5*sa1*sa3*sb**3)/2.',
                 order = {'QED':2})

GC_85 = Coupling(name = 'GC_85',
                 value = '(ca1*ca3*cb**3*l4)/2. - (ca1*ca3*cb**3*l5)/2. - (cb**3*l4*sa1*sa2*sa3)/2. + (cb**3*l5*sa1*sa2*sa3)/2. + (ca3*cb**2*l4*sa1*sb)/2. - (ca3*cb**2*l5*sa1*sb)/2. + (ca1*cb**2*l4*sa2*sa3*sb)/2. - (ca1*cb**2*l5*sa2*sa3*sb)/2. + (ca1*ca3*cb*l4*sb**2)/2. - (ca1*ca3*cb*l5*sb**2)/2. - (cb*l4*sa1*sa2*sa3*sb**2)/2. + (cb*l5*sa1*sa2*sa3*sb**2)/2. + (ca3*l4*sa1*sb**3)/2. - (ca3*l5*sa1*sb**3)/2. + (ca1*l4*sa2*sa3*sb**3)/2. - (ca1*l5*sa2*sa3*sb**3)/2.',
                 order = {'QED':2})

GC_86 = Coupling(name = 'GC_86',
                 value = '-(ca1*ca3*cb**3*l4)/2. + (ca1*ca3*cb**3*l5)/2. + (cb**3*l4*sa1*sa2*sa3)/2. - (cb**3*l5*sa1*sa2*sa3)/2. - (ca3*cb**2*l4*sa1*sb)/2. + (ca3*cb**2*l5*sa1*sb)/2. - (ca1*cb**2*l4*sa2*sa3*sb)/2. + (ca1*cb**2*l5*sa2*sa3*sb)/2. - (ca1*ca3*cb*l4*sb**2)/2. + (ca1*ca3*cb*l5*sb**2)/2. + (cb*l4*sa1*sa2*sa3*sb**2)/2. - (cb*l5*sa1*sa2*sa3*sb**2)/2. - (ca3*l4*sa1*sb**3)/2. + (ca3*l5*sa1*sb**3)/2. - (ca1*l4*sa2*sa3*sb**3)/2. + (ca1*l5*sa2*sa3*sb**3)/2.',
                 order = {'QED':2})

GC_87 = Coupling(name = 'GC_87',
                 value = '(ca3*cb**3*l4*sa1)/2. - (ca3*cb**3*l5*sa1)/2. + (ca1*cb**3*l4*sa2*sa3)/2. - (ca1*cb**3*l5*sa2*sa3)/2. - (ca1*ca3*cb**2*l4*sb)/2. + (ca1*ca3*cb**2*l5*sb)/2. + (cb**2*l4*sa1*sa2*sa3*sb)/2. - (cb**2*l5*sa1*sa2*sa3*sb)/2. + (ca3*cb*l4*sa1*sb**2)/2. - (ca3*cb*l5*sa1*sb**2)/2. + (ca1*cb*l4*sa2*sa3*sb**2)/2. - (ca1*cb*l5*sa2*sa3*sb**2)/2. - (ca1*ca3*l4*sb**3)/2. + (ca1*ca3*l5*sb**3)/2. + (l4*sa1*sa2*sa3*sb**3)/2. - (l5*sa1*sa2*sa3*sb**3)/2.',
                 order = {'QED':2})

GC_88 = Coupling(name = 'GC_88',
                 value = '-(ca3*cb**3*l4*sa1)/2. + (ca3*cb**3*l5*sa1)/2. - (ca1*cb**3*l4*sa2*sa3)/2. + (ca1*cb**3*l5*sa2*sa3)/2. + (ca1*ca3*cb**2*l4*sb)/2. - (ca1*ca3*cb**2*l5*sb)/2. - (cb**2*l4*sa1*sa2*sa3*sb)/2. + (cb**2*l5*sa1*sa2*sa3*sb)/2. - (ca3*cb*l4*sa1*sb**2)/2. + (ca3*cb*l5*sa1*sb**2)/2. - (ca1*cb*l4*sa2*sa3*sb**2)/2. + (ca1*cb*l5*sa2*sa3*sb**2)/2. + (ca1*ca3*l4*sb**3)/2. - (ca1*ca3*l5*sb**3)/2. - (l4*sa1*sa2*sa3*sb**3)/2. + (l5*sa1*sa2*sa3*sb**3)/2.',
                 order = {'QED':2})

GC_89 = Coupling(name = 'GC_89',
                 value = '-(cb**4*complex(0,1)*l2) - 2*cb**2*complex(0,1)*l3*sb**2 - 2*cb**2*complex(0,1)*l4*sb**2 - 2*cb**2*complex(0,1)*l5*sb**2 - complex(0,1)*l1*sb**4',
                 order = {'QED':2})

GC_90 = Coupling(name = 'GC_90',
                 value = '-2*cb**4*complex(0,1)*l2 - 4*cb**2*complex(0,1)*l3*sb**2 - 4*cb**2*complex(0,1)*l4*sb**2 - 4*cb**2*complex(0,1)*l5*sb**2 - 2*complex(0,1)*l1*sb**4',
                 order = {'QED':2})

GC_91 = Coupling(name = 'GC_91',
                 value = '-3*cb**4*complex(0,1)*l2 - 6*cb**2*complex(0,1)*l3*sb**2 - 6*cb**2*complex(0,1)*l4*sb**2 - 6*cb**2*complex(0,1)*l5*sb**2 - 3*complex(0,1)*l1*sb**4',
                 order = {'QED':2})

GC_92 = Coupling(name = 'GC_92',
                 value = '-(cb**4*complex(0,1)*l1) - 2*cb**2*complex(0,1)*l3*sb**2 - 2*cb**2*complex(0,1)*l4*sb**2 - 2*cb**2*complex(0,1)*l5*sb**2 - complex(0,1)*l2*sb**4',
                 order = {'QED':2})

GC_93 = Coupling(name = 'GC_93',
                 value = '-2*cb**4*complex(0,1)*l1 - 4*cb**2*complex(0,1)*l3*sb**2 - 4*cb**2*complex(0,1)*l4*sb**2 - 4*cb**2*complex(0,1)*l5*sb**2 - 2*complex(0,1)*l2*sb**4',
                 order = {'QED':2})

GC_94 = Coupling(name = 'GC_94',
                 value = '-3*cb**4*complex(0,1)*l1 - 6*cb**2*complex(0,1)*l3*sb**2 - 6*cb**2*complex(0,1)*l4*sb**2 - 6*cb**2*complex(0,1)*l5*sb**2 - 3*complex(0,1)*l2*sb**4',
                 order = {'QED':2})

GC_95 = Coupling(name = 'GC_95',
                 value = '-(cb**4*complex(0,1)*l3) - cb**2*complex(0,1)*l1*sb**2 - cb**2*complex(0,1)*l2*sb**2 + 2*cb**2*complex(0,1)*l4*sb**2 + 2*cb**2*complex(0,1)*l5*sb**2 - complex(0,1)*l3*sb**4',
                 order = {'QED':2})

GC_96 = Coupling(name = 'GC_96',
                 value = '-(cb**4*complex(0,1)*l3) - cb**4*complex(0,1)*l4 - 2*cb**2*complex(0,1)*l1*sb**2 - 2*cb**2*complex(0,1)*l2*sb**2 + 2*cb**2*complex(0,1)*l3*sb**2 + 2*cb**2*complex(0,1)*l4*sb**2 + 4*cb**2*complex(0,1)*l5*sb**2 - complex(0,1)*l3*sb**4 - complex(0,1)*l4*sb**4',
                 order = {'QED':2})

GC_97 = Coupling(name = 'GC_97',
                 value = '-(cb**4*complex(0,1)*l4)/2. - (cb**4*complex(0,1)*l5)/2. - cb**2*complex(0,1)*l1*sb**2 - cb**2*complex(0,1)*l2*sb**2 + 2*cb**2*complex(0,1)*l3*sb**2 + cb**2*complex(0,1)*l4*sb**2 + cb**2*complex(0,1)*l5*sb**2 - (complex(0,1)*l4*sb**4)/2. - (complex(0,1)*l5*sb**4)/2.',
                 order = {'QED':2})

GC_98 = Coupling(name = 'GC_98',
                 value = '-(cb**4*complex(0,1)*l3) - cb**4*complex(0,1)*l4 - cb**4*complex(0,1)*l5 - 3*cb**2*complex(0,1)*l1*sb**2 - 3*cb**2*complex(0,1)*l2*sb**2 + 4*cb**2*complex(0,1)*l3*sb**2 + 4*cb**2*complex(0,1)*l4*sb**2 + 4*cb**2*complex(0,1)*l5*sb**2 - complex(0,1)*l3*sb**4 - complex(0,1)*l4*sb**4 - complex(0,1)*l5*sb**4',
                 order = {'QED':2})

GC_99 = Coupling(name = 'GC_99',
                 value = '-2*cb**4*complex(0,1)*l5 - 2*cb**2*complex(0,1)*l1*sb**2 - 2*cb**2*complex(0,1)*l2*sb**2 + 4*cb**2*complex(0,1)*l3*sb**2 + 4*cb**2*complex(0,1)*l4*sb**2 - 2*complex(0,1)*l5*sb**4',
                 order = {'QED':2})

GC_100 = Coupling(name = 'GC_100',
                  value = '(ca1**2*ca2**2*ee**2*complex(0,1))/(2.*sw**2) + (ca2**2*ee**2*complex(0,1)*sa1**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_101 = Coupling(name = 'GC_101',
                  value = '-(ca1**2*ca2*ca3*ee**2*complex(0,1)*sa2)/(2.*sw**2) - (ca2*ca3*ee**2*complex(0,1)*sa1**2*sa2)/(2.*sw**2)',
                  order = {'QED':2})

GC_102 = Coupling(name = 'GC_102',
                  value = '-(ca1**2*ca2*ee**2*complex(0,1)*sa2*sa3)/(2.*sw**2) - (ca2*ee**2*complex(0,1)*sa1**2*sa2*sa3)/(2.*sw**2)',
                  order = {'QED':2})

GC_103 = Coupling(name = 'GC_103',
                  value = '-(ca1**2*ca3*ee**2*complex(0,1)*sa3)/(2.*sw**2) - (ca3*ee**2*complex(0,1)*sa1**2*sa3)/(2.*sw**2) + (ca1**2*ca3*ee**2*complex(0,1)*sa2**2*sa3)/(2.*sw**2) + (ca3*ee**2*complex(0,1)*sa1**2*sa2**2*sa3)/(2.*sw**2)',
                  order = {'QED':2})

GC_104 = Coupling(name = 'GC_104',
                  value = '(ca1**2*ca3**2*ee**2*complex(0,1)*sa2**2)/(2.*sw**2) + (ca3**2*ee**2*complex(0,1)*sa1**2*sa2**2)/(2.*sw**2) + (ca1**2*ee**2*complex(0,1)*sa3**2)/(2.*sw**2) + (ee**2*complex(0,1)*sa1**2*sa3**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_105 = Coupling(name = 'GC_105',
                  value = '(ca1**2*ca3**2*ee**2*complex(0,1))/(2.*sw**2) + (ca3**2*ee**2*complex(0,1)*sa1**2)/(2.*sw**2) + (ca1**2*ee**2*complex(0,1)*sa2**2*sa3**2)/(2.*sw**2) + (ee**2*complex(0,1)*sa1**2*sa2**2*sa3**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_106 = Coupling(name = 'GC_106',
                  value = '(cb**2*ee**2*complex(0,1))/(2.*sw**2) + (ee**2*complex(0,1)*sb**2)/(2.*sw**2)',
                  order = {'QED':2})

GC_107 = Coupling(name = 'GC_107',
                  value = '(ca2*cb*ee*complex(0,1)*sa1)/(2.*sw) - (ca1*ca2*ee*complex(0,1)*sb)/(2.*sw)',
                  order = {'QED':1})

GC_108 = Coupling(name = 'GC_108',
                  value = '-(ca2*cb*ee*complex(0,1)*sa1)/(2.*sw) + (ca1*ca2*ee*complex(0,1)*sb)/(2.*sw)',
                  order = {'QED':1})

GC_109 = Coupling(name = 'GC_109',
                  value = '-(ca2*cb*ee**2*complex(0,1)*sa1)/(2.*sw) + (ca1*ca2*ee**2*complex(0,1)*sb)/(2.*sw)',
                  order = {'QED':2})

GC_110 = Coupling(name = 'GC_110',
                  value = '-(ca1*ca2*cb*ee*complex(0,1))/(2.*sw) - (ca2*ee*complex(0,1)*sa1*sb)/(2.*sw)',
                  order = {'QED':1})

GC_111 = Coupling(name = 'GC_111',
                  value = '(ca1*ca2*cb*ee*complex(0,1))/(2.*sw) + (ca2*ee*complex(0,1)*sa1*sb)/(2.*sw)',
                  order = {'QED':1})

GC_112 = Coupling(name = 'GC_112',
                  value = '-(ca1*ca2*cb*ee**2*complex(0,1))/(2.*sw) - (ca2*ee**2*complex(0,1)*sa1*sb)/(2.*sw)',
                  order = {'QED':2})

GC_113 = Coupling(name = 'GC_113',
                  value = '-(ca1*ca2*cb*ee*complex(0,1)*MW)/(2.*sw) - (ca2*ee*complex(0,1)*MW*sa1*sb)/(2.*sw)',
                  order = {'QED':1})

GC_114 = Coupling(name = 'GC_114',
                  value = '-(ca1*ca3*cb*ee*complex(0,1)*sa2)/(2.*sw) + (cb*ee*complex(0,1)*sa1*sa3)/(2.*sw) - (ca3*ee*complex(0,1)*sa1*sa2*sb)/(2.*sw) - (ca1*ee*complex(0,1)*sa3*sb)/(2.*sw)',
                  order = {'QED':1})

GC_115 = Coupling(name = 'GC_115',
                  value = '(ca1*ca3*cb*ee*complex(0,1)*sa2)/(2.*sw) - (cb*ee*complex(0,1)*sa1*sa3)/(2.*sw) + (ca3*ee*complex(0,1)*sa1*sa2*sb)/(2.*sw) + (ca1*ee*complex(0,1)*sa3*sb)/(2.*sw)',
                  order = {'QED':1})

GC_116 = Coupling(name = 'GC_116',
                  value = '(ca1*ca3*cb*ee**2*complex(0,1)*sa2)/(2.*sw) - (cb*ee**2*complex(0,1)*sa1*sa3)/(2.*sw) + (ca3*ee**2*complex(0,1)*sa1*sa2*sb)/(2.*sw) + (ca1*ee**2*complex(0,1)*sa3*sb)/(2.*sw)',
                  order = {'QED':2})

GC_117 = Coupling(name = 'GC_117',
                  value = '(ca1*ca3*cb*ee*complex(0,1)*MW*sa2)/(2.*sw) - (cb*ee*complex(0,1)*MW*sa1*sa3)/(2.*sw) + (ca3*ee*complex(0,1)*MW*sa1*sa2*sb)/(2.*sw) + (ca1*ee*complex(0,1)*MW*sa3*sb)/(2.*sw)',
                  order = {'QED':1})

GC_118 = Coupling(name = 'GC_118',
                  value = '-(ca3*cb*ee*complex(0,1)*sa1*sa2)/(2.*sw) - (ca1*cb*ee*complex(0,1)*sa3)/(2.*sw) + (ca1*ca3*ee*complex(0,1)*sa2*sb)/(2.*sw) - (ee*complex(0,1)*sa1*sa3*sb)/(2.*sw)',
                  order = {'QED':1})

GC_119 = Coupling(name = 'GC_119',
                  value = '(ca3*cb*ee*complex(0,1)*sa1*sa2)/(2.*sw) + (ca1*cb*ee*complex(0,1)*sa3)/(2.*sw) - (ca1*ca3*ee*complex(0,1)*sa2*sb)/(2.*sw) + (ee*complex(0,1)*sa1*sa3*sb)/(2.*sw)',
                  order = {'QED':1})

GC_120 = Coupling(name = 'GC_120',
                  value = '(ca3*cb*ee**2*complex(0,1)*sa1*sa2)/(2.*sw) + (ca1*cb*ee**2*complex(0,1)*sa3)/(2.*sw) - (ca1*ca3*ee**2*complex(0,1)*sa2*sb)/(2.*sw) + (ee**2*complex(0,1)*sa1*sa3*sb)/(2.*sw)',
                  order = {'QED':2})

GC_121 = Coupling(name = 'GC_121',
                  value = '-(ca1*ca3*cb*ee*complex(0,1))/(2.*sw) + (cb*ee*complex(0,1)*sa1*sa2*sa3)/(2.*sw) - (ca3*ee*complex(0,1)*sa1*sb)/(2.*sw) - (ca1*ee*complex(0,1)*sa2*sa3*sb)/(2.*sw)',
                  order = {'QED':1})

GC_122 = Coupling(name = 'GC_122',
                  value = '(ca1*ca3*cb*ee*complex(0,1))/(2.*sw) - (cb*ee*complex(0,1)*sa1*sa2*sa3)/(2.*sw) + (ca3*ee*complex(0,1)*sa1*sb)/(2.*sw) + (ca1*ee*complex(0,1)*sa2*sa3*sb)/(2.*sw)',
                  order = {'QED':1})

GC_123 = Coupling(name = 'GC_123',
                  value = '-(ca1*ca3*cb*ee**2*complex(0,1))/(2.*sw) + (cb*ee**2*complex(0,1)*sa1*sa2*sa3)/(2.*sw) - (ca3*ee**2*complex(0,1)*sa1*sb)/(2.*sw) - (ca1*ee**2*complex(0,1)*sa2*sa3*sb)/(2.*sw)',
                  order = {'QED':2})

GC_124 = Coupling(name = 'GC_124',
                  value = '-(ca3*cb*ee*complex(0,1)*sa1)/(2.*sw) - (ca1*cb*ee*complex(0,1)*sa2*sa3)/(2.*sw) + (ca1*ca3*ee*complex(0,1)*sb)/(2.*sw) - (ee*complex(0,1)*sa1*sa2*sa3*sb)/(2.*sw)',
                  order = {'QED':1})

GC_125 = Coupling(name = 'GC_125',
                  value = '(ca3*cb*ee*complex(0,1)*sa1)/(2.*sw) + (ca1*cb*ee*complex(0,1)*sa2*sa3)/(2.*sw) - (ca1*ca3*ee*complex(0,1)*sb)/(2.*sw) + (ee*complex(0,1)*sa1*sa2*sa3*sb)/(2.*sw)',
                  order = {'QED':1})

GC_126 = Coupling(name = 'GC_126',
                  value = '(ca3*cb*ee**2*complex(0,1)*sa1)/(2.*sw) + (ca1*cb*ee**2*complex(0,1)*sa2*sa3)/(2.*sw) - (ca1*ca3*ee**2*complex(0,1)*sb)/(2.*sw) + (ee**2*complex(0,1)*sa1*sa2*sa3*sb)/(2.*sw)',
                  order = {'QED':2})

GC_127 = Coupling(name = 'GC_127',
                  value = '(ca3*cb*ee*complex(0,1)*MW*sa1)/(2.*sw) + (ca1*cb*ee*complex(0,1)*MW*sa2*sa3)/(2.*sw) - (ca1*ca3*ee*complex(0,1)*MW*sb)/(2.*sw) + (ee*complex(0,1)*MW*sa1*sa2*sa3*sb)/(2.*sw)',
                  order = {'QED':1})

GC_128 = Coupling(name = 'GC_128',
                  value = '(cb**2*ee)/(2.*sw) + (ee*sb**2)/(2.*sw)',
                  order = {'QED':1})

GC_129 = Coupling(name = 'GC_129',
                  value = '-(cb**2*ee**2)/(2.*sw) - (ee**2*sb**2)/(2.*sw)',
                  order = {'QED':2})

GC_130 = Coupling(name = 'GC_130',
                  value = '(cb**2*ee**2)/(2.*sw) + (ee**2*sb**2)/(2.*sw)',
                  order = {'QED':2})

GC_131 = Coupling(name = 'GC_131',
                  value = '-(cb**2*ee*MW)/(2.*sw) - (ee*MW*sb**2)/(2.*sw)',
                  order = {'QED':1})

GC_132 = Coupling(name = 'GC_132',
                  value = '(cb**2*ee*MW)/(2.*sw) + (ee*MW*sb**2)/(2.*sw)',
                  order = {'QED':1})

GC_133 = Coupling(name = 'GC_133',
                  value = '(cb**2*ee*complex(0,1)*MZ)/(2.*sw) + (ee*complex(0,1)*MZ*sb**2)/(2.*sw)',
                  order = {'QED':1})

GC_134 = Coupling(name = 'GC_134',
                  value = '-((ee**2*complex(0,1))/sw**2)',
                  order = {'QED':2})

GC_135 = Coupling(name = 'GC_135',
                  value = '(cw**2*ee**2*complex(0,1))/sw**2',
                  order = {'QED':2})

GC_136 = Coupling(name = 'GC_136',
                  value = '(ee*complex(0,1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_137 = Coupling(name = 'GC_137',
                  value = '-((cw*ee*complex(0,1))/sw)',
                  order = {'QED':1})

GC_138 = Coupling(name = 'GC_138',
                  value = '(cw*ee*complex(0,1))/sw',
                  order = {'QED':1})

GC_139 = Coupling(name = 'GC_139',
                  value = '(2*cw*ee**2*complex(0,1))/sw',
                  order = {'QED':2})

GC_140 = Coupling(name = 'GC_140',
                  value = '(ee*complex(0,1)*sw)/(3.*cw)',
                  order = {'QED':1})

GC_141 = Coupling(name = 'GC_141',
                  value = '(-2*ee*complex(0,1)*sw)/(3.*cw)',
                  order = {'QED':1})

GC_142 = Coupling(name = 'GC_142',
                  value = '(ee*complex(0,1)*sw)/cw',
                  order = {'QED':1})

GC_143 = Coupling(name = 'GC_143',
                  value = '-(cw*ee*complex(0,1))/(2.*sw) - (ee*complex(0,1)*sw)/(6.*cw)',
                  order = {'QED':1})

GC_144 = Coupling(name = 'GC_144',
                  value = '(cw*ee*complex(0,1))/(2.*sw) - (ee*complex(0,1)*sw)/(6.*cw)',
                  order = {'QED':1})

GC_145 = Coupling(name = 'GC_145',
                  value = '-(cw*ee*complex(0,1))/(2.*sw) + (ee*complex(0,1)*sw)/(2.*cw)',
                  order = {'QED':1})

GC_146 = Coupling(name = 'GC_146',
                  value = '(cw*ee*complex(0,1))/(2.*sw) + (ee*complex(0,1)*sw)/(2.*cw)',
                  order = {'QED':1})

GC_147 = Coupling(name = 'GC_147',
                  value = '-(ca2*cb*cw*ee*sa1)/(2.*sw) + (ca1*ca2*cw*ee*sb)/(2.*sw) - (ca2*cb*ee*sa1*sw)/(2.*cw) + (ca1*ca2*ee*sb*sw)/(2.*cw)',
                  order = {'QED':1})

GC_148 = Coupling(name = 'GC_148',
                  value = '-(ca1*ca2*cb*cw*ee)/(2.*sw) - (ca2*cw*ee*sa1*sb)/(2.*sw) - (ca1*ca2*cb*ee*sw)/(2.*cw) - (ca2*ee*sa1*sb*sw)/(2.*cw)',
                  order = {'QED':1})

GC_149 = Coupling(name = 'GC_149',
                  value = '-(ca1*ca2*cb*cw*ee*complex(0,1)*MZ)/(2.*sw) - (ca2*cw*ee*complex(0,1)*MZ*sa1*sb)/(2.*sw) - (ca1*ca2*cb*ee*complex(0,1)*MZ*sw)/(2.*cw) - (ca2*ee*complex(0,1)*MZ*sa1*sb*sw)/(2.*cw)',
                  order = {'QED':1})

GC_150 = Coupling(name = 'GC_150',
                  value = '(ca1*ca3*cb*cw*ee*sa2)/(2.*sw) - (cb*cw*ee*sa1*sa3)/(2.*sw) + (ca3*cw*ee*sa1*sa2*sb)/(2.*sw) + (ca1*cw*ee*sa3*sb)/(2.*sw) + (ca1*ca3*cb*ee*sa2*sw)/(2.*cw) - (cb*ee*sa1*sa3*sw)/(2.*cw) + (ca3*ee*sa1*sa2*sb*sw)/(2.*cw) + (ca1*ee*sa3*sb*sw)/(2.*cw)',
                  order = {'QED':1})

GC_151 = Coupling(name = 'GC_151',
                  value = '(ca1*ca3*cb*cw*ee*complex(0,1)*MZ*sa2)/(2.*sw) - (cb*cw*ee*complex(0,1)*MZ*sa1*sa3)/(2.*sw) + (ca3*cw*ee*complex(0,1)*MZ*sa1*sa2*sb)/(2.*sw) + (ca1*cw*ee*complex(0,1)*MZ*sa3*sb)/(2.*sw) + (ca1*ca3*cb*ee*complex(0,1)*MZ*sa2*sw)/(2.*cw) - (cb*ee*complex(0,1)*MZ*sa1*sa3*sw)/(2.*cw) + (ca3*ee*complex(0,1)*MZ*sa1*sa2*sb*sw)/(2.*cw) + (ca1*ee*complex(0,1)*MZ*sa3*sb*sw)/(2.*cw)',
                  order = {'QED':1})

GC_152 = Coupling(name = 'GC_152',
                  value = '(ca3*cb*cw*ee*sa1*sa2)/(2.*sw) + (ca1*cb*cw*ee*sa3)/(2.*sw) - (ca1*ca3*cw*ee*sa2*sb)/(2.*sw) + (cw*ee*sa1*sa3*sb)/(2.*sw) + (ca3*cb*ee*sa1*sa2*sw)/(2.*cw) + (ca1*cb*ee*sa3*sw)/(2.*cw) - (ca1*ca3*ee*sa2*sb*sw)/(2.*cw) + (ee*sa1*sa3*sb*sw)/(2.*cw)',
                  order = {'QED':1})

GC_153 = Coupling(name = 'GC_153',
                  value = '-(ca1*ca3*cb*cw*ee)/(2.*sw) + (cb*cw*ee*sa1*sa2*sa3)/(2.*sw) - (ca3*cw*ee*sa1*sb)/(2.*sw) - (ca1*cw*ee*sa2*sa3*sb)/(2.*sw) - (ca1*ca3*cb*ee*sw)/(2.*cw) + (cb*ee*sa1*sa2*sa3*sw)/(2.*cw) - (ca3*ee*sa1*sb*sw)/(2.*cw) - (ca1*ee*sa2*sa3*sb*sw)/(2.*cw)',
                  order = {'QED':1})

GC_154 = Coupling(name = 'GC_154',
                  value = '(ca3*cb*cw*ee*sa1)/(2.*sw) + (ca1*cb*cw*ee*sa2*sa3)/(2.*sw) - (ca1*ca3*cw*ee*sb)/(2.*sw) + (cw*ee*sa1*sa2*sa3*sb)/(2.*sw) + (ca3*cb*ee*sa1*sw)/(2.*cw) + (ca1*cb*ee*sa2*sa3*sw)/(2.*cw) - (ca1*ca3*ee*sb*sw)/(2.*cw) + (ee*sa1*sa2*sa3*sb*sw)/(2.*cw)',
                  order = {'QED':1})

GC_155 = Coupling(name = 'GC_155',
                  value = '(ca3*cb*cw*ee*complex(0,1)*MZ*sa1)/(2.*sw) + (ca1*cb*cw*ee*complex(0,1)*MZ*sa2*sa3)/(2.*sw) - (ca1*ca3*cw*ee*complex(0,1)*MZ*sb)/(2.*sw) + (cw*ee*complex(0,1)*MZ*sa1*sa2*sa3*sb)/(2.*sw) + (ca3*cb*ee*complex(0,1)*MZ*sa1*sw)/(2.*cw) + (ca1*cb*ee*complex(0,1)*MZ*sa2*sa3*sw)/(2.*cw) - (ca1*ca3*ee*complex(0,1)*MZ*sb*sw)/(2.*cw) + (ee*complex(0,1)*MZ*sa1*sa2*sa3*sb*sw)/(2.*cw)',
                  order = {'QED':1})

GC_156 = Coupling(name = 'GC_156',
                  value = '-(cb**2*cw*ee*complex(0,1))/(2.*sw) - (cw*ee*complex(0,1)*sb**2)/(2.*sw) + (cb**2*ee*complex(0,1)*sw)/(2.*cw) + (ee*complex(0,1)*sb**2*sw)/(2.*cw)',
                  order = {'QED':1})

GC_157 = Coupling(name = 'GC_157',
                  value = '-((cb**2*cw*ee**2*complex(0,1))/sw) - (cw*ee**2*complex(0,1)*sb**2)/sw + (cb**2*ee**2*complex(0,1)*sw)/cw + (ee**2*complex(0,1)*sb**2*sw)/cw',
                  order = {'QED':2})

GC_158 = Coupling(name = 'GC_158',
                  value = '-(cb**2*cw*ee*complex(0,1)*MW)/(2.*sw) - (cw*ee*complex(0,1)*MW*sb**2)/(2.*sw) + (cb**2*ee*complex(0,1)*MW*sw)/(2.*cw) + (ee*complex(0,1)*MW*sb**2*sw)/(2.*cw)',
                  order = {'QED':1})

GC_159 = Coupling(name = 'GC_159',
                  value = 'ca1**2*ca2**2*ee**2*complex(0,1) + ca2**2*ee**2*complex(0,1)*sa1**2 + (ca1**2*ca2**2*cw**2*ee**2*complex(0,1))/(2.*sw**2) + (ca2**2*cw**2*ee**2*complex(0,1)*sa1**2)/(2.*sw**2) + (ca1**2*ca2**2*ee**2*complex(0,1)*sw**2)/(2.*cw**2) + (ca2**2*ee**2*complex(0,1)*sa1**2*sw**2)/(2.*cw**2)',
                  order = {'QED':2})

GC_160 = Coupling(name = 'GC_160',
                  value = '-(ca1**2*ca2*ca3*ee**2*complex(0,1)*sa2) - ca2*ca3*ee**2*complex(0,1)*sa1**2*sa2 - (ca1**2*ca2*ca3*cw**2*ee**2*complex(0,1)*sa2)/(2.*sw**2) - (ca2*ca3*cw**2*ee**2*complex(0,1)*sa1**2*sa2)/(2.*sw**2) - (ca1**2*ca2*ca3*ee**2*complex(0,1)*sa2*sw**2)/(2.*cw**2) - (ca2*ca3*ee**2*complex(0,1)*sa1**2*sa2*sw**2)/(2.*cw**2)',
                  order = {'QED':2})

GC_161 = Coupling(name = 'GC_161',
                  value = '-(ca1**2*ca2*ee**2*complex(0,1)*sa2*sa3) - ca2*ee**2*complex(0,1)*sa1**2*sa2*sa3 - (ca1**2*ca2*cw**2*ee**2*complex(0,1)*sa2*sa3)/(2.*sw**2) - (ca2*cw**2*ee**2*complex(0,1)*sa1**2*sa2*sa3)/(2.*sw**2) - (ca1**2*ca2*ee**2*complex(0,1)*sa2*sa3*sw**2)/(2.*cw**2) - (ca2*ee**2*complex(0,1)*sa1**2*sa2*sa3*sw**2)/(2.*cw**2)',
                  order = {'QED':2})

GC_162 = Coupling(name = 'GC_162',
                  value = '-(ca1**2*ca3*ee**2*complex(0,1)*sa3) - ca3*ee**2*complex(0,1)*sa1**2*sa3 + ca1**2*ca3*ee**2*complex(0,1)*sa2**2*sa3 + ca3*ee**2*complex(0,1)*sa1**2*sa2**2*sa3 - (ca1**2*ca3*cw**2*ee**2*complex(0,1)*sa3)/(2.*sw**2) - (ca3*cw**2*ee**2*complex(0,1)*sa1**2*sa3)/(2.*sw**2) + (ca1**2*ca3*cw**2*ee**2*complex(0,1)*sa2**2*sa3)/(2.*sw**2) + (ca3*cw**2*ee**2*complex(0,1)*sa1**2*sa2**2*sa3)/(2.*sw**2) - (ca1**2*ca3*ee**2*complex(0,1)*sa3*sw**2)/(2.*cw**2) - (ca3*ee**2*complex(0,1)*sa1**2*sa3*sw**2)/(2.*cw**2) + (ca1**2*ca3*ee**2*complex(0,1)*sa2**2*sa3*sw**2)/(2.*cw**2) + (ca3*ee**2*complex(0,1)*sa1**2*sa2**2*sa3*sw**2)/(2.*cw**2)',
                  order = {'QED':2})

GC_163 = Coupling(name = 'GC_163',
                  value = 'ca1**2*ca3**2*ee**2*complex(0,1)*sa2**2 + ca3**2*ee**2*complex(0,1)*sa1**2*sa2**2 + ca1**2*ee**2*complex(0,1)*sa3**2 + ee**2*complex(0,1)*sa1**2*sa3**2 + (ca1**2*ca3**2*cw**2*ee**2*complex(0,1)*sa2**2)/(2.*sw**2) + (ca3**2*cw**2*ee**2*complex(0,1)*sa1**2*sa2**2)/(2.*sw**2) + (ca1**2*cw**2*ee**2*complex(0,1)*sa3**2)/(2.*sw**2) + (cw**2*ee**2*complex(0,1)*sa1**2*sa3**2)/(2.*sw**2) + (ca1**2*ca3**2*ee**2*complex(0,1)*sa2**2*sw**2)/(2.*cw**2) + (ca3**2*ee**2*complex(0,1)*sa1**2*sa2**2*sw**2)/(2.*cw**2) + (ca1**2*ee**2*complex(0,1)*sa3**2*sw**2)/(2.*cw**2) + (ee**2*complex(0,1)*sa1**2*sa3**2*sw**2)/(2.*cw**2)',
                  order = {'QED':2})

GC_164 = Coupling(name = 'GC_164',
                  value = 'ca1**2*ca3**2*ee**2*complex(0,1) + ca3**2*ee**2*complex(0,1)*sa1**2 + ca1**2*ee**2*complex(0,1)*sa2**2*sa3**2 + ee**2*complex(0,1)*sa1**2*sa2**2*sa3**2 + (ca1**2*ca3**2*cw**2*ee**2*complex(0,1))/(2.*sw**2) + (ca3**2*cw**2*ee**2*complex(0,1)*sa1**2)/(2.*sw**2) + (ca1**2*cw**2*ee**2*complex(0,1)*sa2**2*sa3**2)/(2.*sw**2) + (cw**2*ee**2*complex(0,1)*sa1**2*sa2**2*sa3**2)/(2.*sw**2) + (ca1**2*ca3**2*ee**2*complex(0,1)*sw**2)/(2.*cw**2) + (ca3**2*ee**2*complex(0,1)*sa1**2*sw**2)/(2.*cw**2) + (ca1**2*ee**2*complex(0,1)*sa2**2*sa3**2*sw**2)/(2.*cw**2) + (ee**2*complex(0,1)*sa1**2*sa2**2*sa3**2*sw**2)/(2.*cw**2)',
                  order = {'QED':2})

GC_165 = Coupling(name = 'GC_165',
                  value = '-(cb**2*ee**2*complex(0,1)) - ee**2*complex(0,1)*sb**2 + (cb**2*cw**2*ee**2*complex(0,1))/(2.*sw**2) + (cw**2*ee**2*complex(0,1)*sb**2)/(2.*sw**2) + (cb**2*ee**2*complex(0,1)*sw**2)/(2.*cw**2) + (ee**2*complex(0,1)*sb**2*sw**2)/(2.*cw**2)',
                  order = {'QED':2})

GC_166 = Coupling(name = 'GC_166',
                  value = 'cb**2*ee**2*complex(0,1) + ee**2*complex(0,1)*sb**2 + (cb**2*cw**2*ee**2*complex(0,1))/(2.*sw**2) + (cw**2*ee**2*complex(0,1)*sb**2)/(2.*sw**2) + (cb**2*ee**2*complex(0,1)*sw**2)/(2.*cw**2) + (ee**2*complex(0,1)*sb**2*sw**2)/(2.*cw**2)',
                  order = {'QED':2})

GC_167 = Coupling(name = 'GC_167',
                  value = '(ee**2*complex(0,1)*sb*vev1)/(2.*cw) - (cb*ee**2*complex(0,1)*vev2)/(2.*cw)',
                  order = {'QED':1})

GC_168 = Coupling(name = 'GC_168',
                  value = '-(cb*ee**2*complex(0,1)*vev1)/(2.*cw) - (ee**2*complex(0,1)*sb*vev2)/(2.*cw)',
                  order = {'QED':1})

GC_169 = Coupling(name = 'GC_169',
                  value = '-(cb**2*l4*sb*vev1)/2. + (cb**2*l5*sb*vev1)/2. - (l4*sb**3*vev1)/2. + (l5*sb**3*vev1)/2. + (cb**3*l4*vev2)/2. - (cb**3*l5*vev2)/2. + (cb*l4*sb**2*vev2)/2. - (cb*l5*sb**2*vev2)/2.',
                  order = {'QED':1})

GC_170 = Coupling(name = 'GC_170',
                  value = '(cb**2*l4*sb*vev1)/2. - (cb**2*l5*sb*vev1)/2. + (l4*sb**3*vev1)/2. - (l5*sb**3*vev1)/2. - (cb**3*l4*vev2)/2. + (cb**3*l5*vev2)/2. - (cb*l4*sb**2*vev2)/2. + (cb*l5*sb**2*vev2)/2.',
                  order = {'QED':1})

GC_171 = Coupling(name = 'GC_171',
                  value = '(cb**3*l4*vev1)/2. - (cb**3*l5*vev1)/2. + (cb*l4*sb**2*vev1)/2. - (cb*l5*sb**2*vev1)/2. + (cb**2*l4*sb*vev2)/2. - (cb**2*l5*sb*vev2)/2. + (l4*sb**3*vev2)/2. - (l5*sb**3*vev2)/2.',
                  order = {'QED':1})

GC_172 = Coupling(name = 'GC_172',
                  value = '-(cb**3*l4*vev1)/2. + (cb**3*l5*vev1)/2. - (cb*l4*sb**2*vev1)/2. + (cb*l5*sb**2*vev1)/2. - (cb**2*l4*sb*vev2)/2. + (cb**2*l5*sb*vev2)/2. - (l4*sb**3*vev2)/2. + (l5*sb**3*vev2)/2.',
                  order = {'QED':1})

GC_173 = Coupling(name = 'GC_173',
                  value = '(ca1*ca2*ee**2*complex(0,1)*vev1)/(2.*sw**2) + (ca2*ee**2*complex(0,1)*sa1*vev2)/(2.*sw**2)',
                  order = {'QED':1})

GC_174 = Coupling(name = 'GC_174',
                  value = '-(ca1*ca3*ee**2*complex(0,1)*sa2*vev1)/(2.*sw**2) + (ee**2*complex(0,1)*sa1*sa3*vev1)/(2.*sw**2) - (ca3*ee**2*complex(0,1)*sa1*sa2*vev2)/(2.*sw**2) - (ca1*ee**2*complex(0,1)*sa3*vev2)/(2.*sw**2)',
                  order = {'QED':1})

GC_175 = Coupling(name = 'GC_175',
                  value = '-(ca3*ee**2*complex(0,1)*sa1*vev1)/(2.*sw**2) - (ca1*ee**2*complex(0,1)*sa2*sa3*vev1)/(2.*sw**2) + (ca1*ca3*ee**2*complex(0,1)*vev2)/(2.*sw**2) - (ee**2*complex(0,1)*sa1*sa2*sa3*vev2)/(2.*sw**2)',
                  order = {'QED':1})

GC_176 = Coupling(name = 'GC_176',
                  value = '(ee**2*complex(0,1)*sb*vev1)/(2.*sw) - (cb*ee**2*complex(0,1)*vev2)/(2.*sw)',
                  order = {'QED':1})

GC_177 = Coupling(name = 'GC_177',
                  value = '-(cb*ee**2*complex(0,1)*vev1)/(2.*sw) - (ee**2*complex(0,1)*sb*vev2)/(2.*sw)',
                  order = {'QED':1})

GC_178 = Coupling(name = 'GC_178',
                  value = 'ca1*ca2*ee**2*complex(0,1)*vev1 + (ca1*ca2*cw**2*ee**2*complex(0,1)*vev1)/(2.*sw**2) + (ca1*ca2*ee**2*complex(0,1)*sw**2*vev1)/(2.*cw**2) + ca2*ee**2*complex(0,1)*sa1*vev2 + (ca2*cw**2*ee**2*complex(0,1)*sa1*vev2)/(2.*sw**2) + (ca2*ee**2*complex(0,1)*sa1*sw**2*vev2)/(2.*cw**2)',
                  order = {'QED':1})

GC_179 = Coupling(name = 'GC_179',
                  value = '-(ca1*ca3*ee**2*complex(0,1)*sa2*vev1) + ee**2*complex(0,1)*sa1*sa3*vev1 - (ca1*ca3*cw**2*ee**2*complex(0,1)*sa2*vev1)/(2.*sw**2) + (cw**2*ee**2*complex(0,1)*sa1*sa3*vev1)/(2.*sw**2) - (ca1*ca3*ee**2*complex(0,1)*sa2*sw**2*vev1)/(2.*cw**2) + (ee**2*complex(0,1)*sa1*sa3*sw**2*vev1)/(2.*cw**2) - ca3*ee**2*complex(0,1)*sa1*sa2*vev2 - ca1*ee**2*complex(0,1)*sa3*vev2 - (ca3*cw**2*ee**2*complex(0,1)*sa1*sa2*vev2)/(2.*sw**2) - (ca1*cw**2*ee**2*complex(0,1)*sa3*vev2)/(2.*sw**2) - (ca3*ee**2*complex(0,1)*sa1*sa2*sw**2*vev2)/(2.*cw**2) - (ca1*ee**2*complex(0,1)*sa3*sw**2*vev2)/(2.*cw**2)',
                  order = {'QED':1})

GC_180 = Coupling(name = 'GC_180',
                  value = '-(ca3*ee**2*complex(0,1)*sa1*vev1) - ca1*ee**2*complex(0,1)*sa2*sa3*vev1 - (ca3*cw**2*ee**2*complex(0,1)*sa1*vev1)/(2.*sw**2) - (ca1*cw**2*ee**2*complex(0,1)*sa2*sa3*vev1)/(2.*sw**2) - (ca3*ee**2*complex(0,1)*sa1*sw**2*vev1)/(2.*cw**2) - (ca1*ee**2*complex(0,1)*sa2*sa3*sw**2*vev1)/(2.*cw**2) + ca1*ca3*ee**2*complex(0,1)*vev2 - ee**2*complex(0,1)*sa1*sa2*sa3*vev2 + (ca1*ca3*cw**2*ee**2*complex(0,1)*vev2)/(2.*sw**2) - (cw**2*ee**2*complex(0,1)*sa1*sa2*sa3*vev2)/(2.*sw**2) + (ca1*ca3*ee**2*complex(0,1)*sw**2*vev2)/(2.*cw**2) - (ee**2*complex(0,1)*sa1*sa2*sa3*sw**2*vev2)/(2.*cw**2)',
                  order = {'QED':1})

GC_181 = Coupling(name = 'GC_181',
                  value = '-3*ca1**3*ca2**3*complex(0,1)*l1*vev1 - 3*ca1*ca2**3*complex(0,1)*l3*sa1**2*vev1 - 3*ca1*ca2**3*complex(0,1)*l4*sa1**2*vev1 - 3*ca1*ca2**3*complex(0,1)*l5*sa1**2*vev1 - 3*ca1*ca2*complex(0,1)*l7*sa2**2*vev1 - 3*ca1**2*ca2**3*complex(0,1)*l3*sa1*vev2 - 3*ca1**2*ca2**3*complex(0,1)*l4*sa1*vev2 - 3*ca1**2*ca2**3*complex(0,1)*l5*sa1*vev2 - 3*ca2**3*complex(0,1)*l2*sa1**3*vev2 - 3*ca2*complex(0,1)*l8*sa1*sa2**2*vev2 - 3*ca1**2*ca2**2*complex(0,1)*l7*sa2*vevs - 3*ca2**2*complex(0,1)*l8*sa1**2*sa2*vevs - 3*complex(0,1)*l6*sa2**3*vevs',
                  order = {'QED':1})

GC_182 = Coupling(name = 'GC_182',
                  value = '3*ca1**3*ca2**2*ca3*complex(0,1)*l1*sa2*vev1 - 2*ca1*ca2**2*ca3*complex(0,1)*l7*sa2*vev1 + 3*ca1*ca2**2*ca3*complex(0,1)*l3*sa1**2*sa2*vev1 + 3*ca1*ca2**2*ca3*complex(0,1)*l4*sa1**2*sa2*vev1 + 3*ca1*ca2**2*ca3*complex(0,1)*l5*sa1**2*sa2*vev1 + ca1*ca3*complex(0,1)*l7*sa2**3*vev1 - 3*ca1**2*ca2**2*complex(0,1)*l1*sa1*sa3*vev1 + 2*ca1**2*ca2**2*complex(0,1)*l3*sa1*sa3*vev1 + 2*ca1**2*ca2**2*complex(0,1)*l4*sa1*sa3*vev1 + 2*ca1**2*ca2**2*complex(0,1)*l5*sa1*sa3*vev1 - ca2**2*complex(0,1)*l3*sa1**3*sa3*vev1 - ca2**2*complex(0,1)*l4*sa1**3*sa3*vev1 - ca2**2*complex(0,1)*l5*sa1**3*sa3*vev1 - complex(0,1)*l7*sa1*sa2**2*sa3*vev1 + 3*ca1**2*ca2**2*ca3*complex(0,1)*l3*sa1*sa2*vev2 + 3*ca1**2*ca2**2*ca3*complex(0,1)*l4*sa1*sa2*vev2 + 3*ca1**2*ca2**2*ca3*complex(0,1)*l5*sa1*sa2*vev2 - 2*ca2**2*ca3*complex(0,1)*l8*sa1*sa2*vev2 + 3*ca2**2*ca3*complex(0,1)*l2*sa1**3*sa2*vev2 + ca3*complex(0,1)*l8*sa1*sa2**3*vev2 + ca1**3*ca2**2*complex(0,1)*l3*sa3*vev2 + ca1**3*ca2**2*complex(0,1)*l4*sa3*vev2 + ca1**3*ca2**2*complex(0,1)*l5*sa3*vev2 + 3*ca1*ca2**2*complex(0,1)*l2*sa1**2*sa3*vev2 - 2*ca1*ca2**2*complex(0,1)*l3*sa1**2*sa3*vev2 - 2*ca1*ca2**2*complex(0,1)*l4*sa1**2*sa3*vev2 - 2*ca1*ca2**2*complex(0,1)*l5*sa1**2*sa3*vev2 + ca1*complex(0,1)*l8*sa2**2*sa3*vev2 - ca1**2*ca2**3*ca3*complex(0,1)*l7*vevs - ca2**3*ca3*complex(0,1)*l8*sa1**2*vevs - 3*ca2*ca3*complex(0,1)*l6*sa2**2*vevs + 2*ca1**2*ca2*ca3*complex(0,1)*l7*sa2**2*vevs + 2*ca2*ca3*complex(0,1)*l8*sa1**2*sa2**2*vevs - 2*ca1*ca2*complex(0,1)*l7*sa1*sa2*sa3*vevs + 2*ca1*ca2*complex(0,1)*l8*sa1*sa2*sa3*vevs',
                  order = {'QED':1})

GC_183 = Coupling(name = 'GC_183',
                  value = '3*ca1**2*ca2**2*ca3*complex(0,1)*l1*sa1*vev1 - 2*ca1**2*ca2**2*ca3*complex(0,1)*l3*sa1*vev1 - 2*ca1**2*ca2**2*ca3*complex(0,1)*l4*sa1*vev1 - 2*ca1**2*ca2**2*ca3*complex(0,1)*l5*sa1*vev1 + ca2**2*ca3*complex(0,1)*l3*sa1**3*vev1 + ca2**2*ca3*complex(0,1)*l4*sa1**3*vev1 + ca2**2*ca3*complex(0,1)*l5*sa1**3*vev1 + ca3*complex(0,1)*l7*sa1*sa2**2*vev1 + 3*ca1**3*ca2**2*complex(0,1)*l1*sa2*sa3*vev1 - 2*ca1*ca2**2*complex(0,1)*l7*sa2*sa3*vev1 + 3*ca1*ca2**2*complex(0,1)*l3*sa1**2*sa2*sa3*vev1 + 3*ca1*ca2**2*complex(0,1)*l4*sa1**2*sa2*sa3*vev1 + 3*ca1*ca2**2*complex(0,1)*l5*sa1**2*sa2*sa3*vev1 + ca1*complex(0,1)*l7*sa2**3*sa3*vev1 - ca1**3*ca2**2*ca3*complex(0,1)*l3*vev2 - ca1**3*ca2**2*ca3*complex(0,1)*l4*vev2 - ca1**3*ca2**2*ca3*complex(0,1)*l5*vev2 - 3*ca1*ca2**2*ca3*complex(0,1)*l2*sa1**2*vev2 + 2*ca1*ca2**2*ca3*complex(0,1)*l3*sa1**2*vev2 + 2*ca1*ca2**2*ca3*complex(0,1)*l4*sa1**2*vev2 + 2*ca1*ca2**2*ca3*complex(0,1)*l5*sa1**2*vev2 - ca1*ca3*complex(0,1)*l8*sa2**2*vev2 + 3*ca1**2*ca2**2*complex(0,1)*l3*sa1*sa2*sa3*vev2 + 3*ca1**2*ca2**2*complex(0,1)*l4*sa1*sa2*sa3*vev2 + 3*ca1**2*ca2**2*complex(0,1)*l5*sa1*sa2*sa3*vev2 - 2*ca2**2*complex(0,1)*l8*sa1*sa2*sa3*vev2 + 3*ca2**2*complex(0,1)*l2*sa1**3*sa2*sa3*vev2 + complex(0,1)*l8*sa1*sa2**3*sa3*vev2 + 2*ca1*ca2*ca3*complex(0,1)*l7*sa1*sa2*vevs - 2*ca1*ca2*ca3*complex(0,1)*l8*sa1*sa2*vevs - ca1**2*ca2**3*complex(0,1)*l7*sa3*vevs - ca2**3*complex(0,1)*l8*sa1**2*sa3*vevs - 3*ca2*complex(0,1)*l6*sa2**2*sa3*vevs + 2*ca1**2*ca2*complex(0,1)*l7*sa2**2*sa3*vevs + 2*ca2*complex(0,1)*l8*sa1**2*sa2**2*sa3*vevs',
                  order = {'QED':1})

GC_184 = Coupling(name = 'GC_184',
                  value = '3*ca1*ca2**2*ca3**3*complex(0,1)*l7*sa2*vev1 + 3*ca1**3*ca3**3*complex(0,1)*l1*sa2**3*vev1 + 3*ca1*ca3**3*complex(0,1)*l3*sa1**2*sa2**3*vev1 + 3*ca1*ca3**3*complex(0,1)*l4*sa1**2*sa2**3*vev1 + 3*ca1*ca3**3*complex(0,1)*l5*sa1**2*sa2**3*vev1 - 3*ca2**2*ca3**2*complex(0,1)*l7*sa1*sa3*vev1 - 9*ca1**2*ca3**2*complex(0,1)*l1*sa1*sa2**2*sa3*vev1 + 6*ca1**2*ca3**2*complex(0,1)*l3*sa1*sa2**2*sa3*vev1 + 6*ca1**2*ca3**2*complex(0,1)*l4*sa1*sa2**2*sa3*vev1 + 6*ca1**2*ca3**2*complex(0,1)*l5*sa1*sa2**2*sa3*vev1 - 3*ca3**2*complex(0,1)*l3*sa1**3*sa2**2*sa3*vev1 - 3*ca3**2*complex(0,1)*l4*sa1**3*sa2**2*sa3*vev1 - 3*ca3**2*complex(0,1)*l5*sa1**3*sa2**2*sa3*vev1 + 3*ca1**3*ca3*complex(0,1)*l3*sa2*sa3**2*vev1 + 3*ca1**3*ca3*complex(0,1)*l4*sa2*sa3**2*vev1 + 3*ca1**3*ca3*complex(0,1)*l5*sa2*sa3**2*vev1 + 9*ca1*ca3*complex(0,1)*l1*sa1**2*sa2*sa3**2*vev1 - 6*ca1*ca3*complex(0,1)*l3*sa1**2*sa2*sa3**2*vev1 - 6*ca1*ca3*complex(0,1)*l4*sa1**2*sa2*sa3**2*vev1 - 6*ca1*ca3*complex(0,1)*l5*sa1**2*sa2*sa3**2*vev1 - 3*ca1**2*complex(0,1)*l3*sa1*sa3**3*vev1 - 3*ca1**2*complex(0,1)*l4*sa1*sa3**3*vev1 - 3*ca1**2*complex(0,1)*l5*sa1*sa3**3*vev1 - 3*complex(0,1)*l1*sa1**3*sa3**3*vev1 + 3*ca2**2*ca3**3*complex(0,1)*l8*sa1*sa2*vev2 + 3*ca1**2*ca3**3*complex(0,1)*l3*sa1*sa2**3*vev2 + 3*ca1**2*ca3**3*complex(0,1)*l4*sa1*sa2**3*vev2 + 3*ca1**2*ca3**3*complex(0,1)*l5*sa1*sa2**3*vev2 + 3*ca3**3*complex(0,1)*l2*sa1**3*sa2**3*vev2 + 3*ca1*ca2**2*ca3**2*complex(0,1)*l8*sa3*vev2 + 3*ca1**3*ca3**2*complex(0,1)*l3*sa2**2*sa3*vev2 + 3*ca1**3*ca3**2*complex(0,1)*l4*sa2**2*sa3*vev2 + 3*ca1**3*ca3**2*complex(0,1)*l5*sa2**2*sa3*vev2 + 9*ca1*ca3**2*complex(0,1)*l2*sa1**2*sa2**2*sa3*vev2 - 6*ca1*ca3**2*complex(0,1)*l3*sa1**2*sa2**2*sa3*vev2 - 6*ca1*ca3**2*complex(0,1)*l4*sa1**2*sa2**2*sa3*vev2 - 6*ca1*ca3**2*complex(0,1)*l5*sa1**2*sa2**2*sa3*vev2 + 9*ca1**2*ca3*complex(0,1)*l2*sa1*sa2*sa3**2*vev2 - 6*ca1**2*ca3*complex(0,1)*l3*sa1*sa2*sa3**2*vev2 - 6*ca1**2*ca3*complex(0,1)*l4*sa1*sa2*sa3**2*vev2 - 6*ca1**2*ca3*complex(0,1)*l5*sa1*sa2*sa3**2*vev2 + 3*ca3*complex(0,1)*l3*sa1**3*sa2*sa3**2*vev2 + 3*ca3*complex(0,1)*l4*sa1**3*sa2*sa3**2*vev2 + 3*ca3*complex(0,1)*l5*sa1**3*sa2*sa3**2*vev2 + 3*ca1**3*complex(0,1)*l2*sa3**3*vev2 + 3*ca1*complex(0,1)*l3*sa1**2*sa3**3*vev2 + 3*ca1*complex(0,1)*l4*sa1**2*sa3**3*vev2 + 3*ca1*complex(0,1)*l5*sa1**2*sa3**3*vev2 - 3*ca2**3*ca3**3*complex(0,1)*l6*vevs - 3*ca1**2*ca2*ca3**3*complex(0,1)*l7*sa2**2*vevs - 3*ca2*ca3**3*complex(0,1)*l8*sa1**2*sa2**2*vevs + 6*ca1*ca2*ca3**2*complex(0,1)*l7*sa1*sa2*sa3*vevs - 6*ca1*ca2*ca3**2*complex(0,1)*l8*sa1*sa2*sa3*vevs - 3*ca1**2*ca2*ca3*complex(0,1)*l8*sa3**2*vevs - 3*ca2*ca3*complex(0,1)*l7*sa1**2*sa3**2*vevs',
                  order = {'QED':1})

GC_185 = Coupling(name = 'GC_185',
                  value = '-(ca1*ca2**3*ca3**2*complex(0,1)*l7*vev1) - 3*ca1**3*ca2*ca3**2*complex(0,1)*l1*sa2**2*vev1 + 2*ca1*ca2*ca3**2*complex(0,1)*l7*sa2**2*vev1 - 3*ca1*ca2*ca3**2*complex(0,1)*l3*sa1**2*sa2**2*vev1 - 3*ca1*ca2*ca3**2*complex(0,1)*l4*sa1**2*sa2**2*vev1 - 3*ca1*ca2*ca3**2*complex(0,1)*l5*sa1**2*sa2**2*vev1 + 6*ca1**2*ca2*ca3*complex(0,1)*l1*sa1*sa2*sa3*vev1 - 4*ca1**2*ca2*ca3*complex(0,1)*l3*sa1*sa2*sa3*vev1 - 4*ca1**2*ca2*ca3*complex(0,1)*l4*sa1*sa2*sa3*vev1 - 4*ca1**2*ca2*ca3*complex(0,1)*l5*sa1*sa2*sa3*vev1 - 2*ca2*ca3*complex(0,1)*l7*sa1*sa2*sa3*vev1 + 2*ca2*ca3*complex(0,1)*l3*sa1**3*sa2*sa3*vev1 + 2*ca2*ca3*complex(0,1)*l4*sa1**3*sa2*sa3*vev1 + 2*ca2*ca3*complex(0,1)*l5*sa1**3*sa2*sa3*vev1 - ca1**3*ca2*complex(0,1)*l3*sa3**2*vev1 - ca1**3*ca2*complex(0,1)*l4*sa3**2*vev1 - ca1**3*ca2*complex(0,1)*l5*sa3**2*vev1 - 3*ca1*ca2*complex(0,1)*l1*sa1**2*sa3**2*vev1 + 2*ca1*ca2*complex(0,1)*l3*sa1**2*sa3**2*vev1 + 2*ca1*ca2*complex(0,1)*l4*sa1**2*sa3**2*vev1 + 2*ca1*ca2*complex(0,1)*l5*sa1**2*sa3**2*vev1 - ca2**3*ca3**2*complex(0,1)*l8*sa1*vev2 - 3*ca1**2*ca2*ca3**2*complex(0,1)*l3*sa1*sa2**2*vev2 - 3*ca1**2*ca2*ca3**2*complex(0,1)*l4*sa1*sa2**2*vev2 - 3*ca1**2*ca2*ca3**2*complex(0,1)*l5*sa1*sa2**2*vev2 + 2*ca2*ca3**2*complex(0,1)*l8*sa1*sa2**2*vev2 - 3*ca2*ca3**2*complex(0,1)*l2*sa1**3*sa2**2*vev2 - 2*ca1**3*ca2*ca3*complex(0,1)*l3*sa2*sa3*vev2 - 2*ca1**3*ca2*ca3*complex(0,1)*l4*sa2*sa3*vev2 - 2*ca1**3*ca2*ca3*complex(0,1)*l5*sa2*sa3*vev2 + 2*ca1*ca2*ca3*complex(0,1)*l8*sa2*sa3*vev2 - 6*ca1*ca2*ca3*complex(0,1)*l2*sa1**2*sa2*sa3*vev2 + 4*ca1*ca2*ca3*complex(0,1)*l3*sa1**2*sa2*sa3*vev2 + 4*ca1*ca2*ca3*complex(0,1)*l4*sa1**2*sa2*sa3*vev2 + 4*ca1*ca2*ca3*complex(0,1)*l5*sa1**2*sa2*sa3*vev2 - 3*ca1**2*ca2*complex(0,1)*l2*sa1*sa3**2*vev2 + 2*ca1**2*ca2*complex(0,1)*l3*sa1*sa3**2*vev2 + 2*ca1**2*ca2*complex(0,1)*l4*sa1*sa3**2*vev2 + 2*ca1**2*ca2*complex(0,1)*l5*sa1*sa3**2*vev2 - ca2*complex(0,1)*l3*sa1**3*sa3**2*vev2 - ca2*complex(0,1)*l4*sa1**3*sa3**2*vev2 - ca2*complex(0,1)*l5*sa1**3*sa3**2*vev2 - 3*ca2**2*ca3**2*complex(0,1)*l6*sa2*vevs + 2*ca1**2*ca2**2*ca3**2*complex(0,1)*l7*sa2*vevs + 2*ca2**2*ca3**2*complex(0,1)*l8*sa1**2*sa2*vevs - ca1**2*ca3**2*complex(0,1)*l7*sa2**3*vevs - ca3**2*complex(0,1)*l8*sa1**2*sa2**3*vevs - 2*ca1*ca2**2*ca3*complex(0,1)*l7*sa1*sa3*vevs + 2*ca1*ca2**2*ca3*complex(0,1)*l8*sa1*sa3*vevs + 2*ca1*ca3*complex(0,1)*l7*sa1*sa2**2*sa3*vevs - 2*ca1*ca3*complex(0,1)*l8*sa1*sa2**2*sa3*vevs - ca1**2*complex(0,1)*l8*sa2*sa3**2*vevs - complex(0,1)*l7*sa1**2*sa2*sa3**2*vevs',
                  order = {'QED':1})

GC_186 = Coupling(name = 'GC_186',
                  value = '-3*ca1**2*ca2*ca3**2*complex(0,1)*l1*sa1*sa2*vev1 + 2*ca1**2*ca2*ca3**2*complex(0,1)*l3*sa1*sa2*vev1 + 2*ca1**2*ca2*ca3**2*complex(0,1)*l4*sa1*sa2*vev1 + 2*ca1**2*ca2*ca3**2*complex(0,1)*l5*sa1*sa2*vev1 + ca2*ca3**2*complex(0,1)*l7*sa1*sa2*vev1 - ca2*ca3**2*complex(0,1)*l3*sa1**3*sa2*vev1 - ca2*ca3**2*complex(0,1)*l4*sa1**3*sa2*vev1 - ca2*ca3**2*complex(0,1)*l5*sa1**3*sa2*vev1 + ca1**3*ca2*ca3*complex(0,1)*l3*sa3*vev1 + ca1**3*ca2*ca3*complex(0,1)*l4*sa3*vev1 + ca1**3*ca2*ca3*complex(0,1)*l5*sa3*vev1 - ca1*ca2**3*ca3*complex(0,1)*l7*sa3*vev1 + 3*ca1*ca2*ca3*complex(0,1)*l1*sa1**2*sa3*vev1 - 2*ca1*ca2*ca3*complex(0,1)*l3*sa1**2*sa3*vev1 - 2*ca1*ca2*ca3*complex(0,1)*l4*sa1**2*sa3*vev1 - 2*ca1*ca2*ca3*complex(0,1)*l5*sa1**2*sa3*vev1 - 3*ca1**3*ca2*ca3*complex(0,1)*l1*sa2**2*sa3*vev1 + 2*ca1*ca2*ca3*complex(0,1)*l7*sa2**2*sa3*vev1 - 3*ca1*ca2*ca3*complex(0,1)*l3*sa1**2*sa2**2*sa3*vev1 - 3*ca1*ca2*ca3*complex(0,1)*l4*sa1**2*sa2**2*sa3*vev1 - 3*ca1*ca2*ca3*complex(0,1)*l5*sa1**2*sa2**2*sa3*vev1 + 3*ca1**2*ca2*complex(0,1)*l1*sa1*sa2*sa3**2*vev1 - 2*ca1**2*ca2*complex(0,1)*l3*sa1*sa2*sa3**2*vev1 - 2*ca1**2*ca2*complex(0,1)*l4*sa1*sa2*sa3**2*vev1 - 2*ca1**2*ca2*complex(0,1)*l5*sa1*sa2*sa3**2*vev1 - ca2*complex(0,1)*l7*sa1*sa2*sa3**2*vev1 + ca2*complex(0,1)*l3*sa1**3*sa2*sa3**2*vev1 + ca2*complex(0,1)*l4*sa1**3*sa2*sa3**2*vev1 + ca2*complex(0,1)*l5*sa1**3*sa2*sa3**2*vev1 + ca1**3*ca2*ca3**2*complex(0,1)*l3*sa2*vev2 + ca1**3*ca2*ca3**2*complex(0,1)*l4*sa2*vev2 + ca1**3*ca2*ca3**2*complex(0,1)*l5*sa2*vev2 - ca1*ca2*ca3**2*complex(0,1)*l8*sa2*vev2 + 3*ca1*ca2*ca3**2*complex(0,1)*l2*sa1**2*sa2*vev2 - 2*ca1*ca2*ca3**2*complex(0,1)*l3*sa1**2*sa2*vev2 - 2*ca1*ca2*ca3**2*complex(0,1)*l4*sa1**2*sa2*vev2 - 2*ca1*ca2*ca3**2*complex(0,1)*l5*sa1**2*sa2*vev2 + 3*ca1**2*ca2*ca3*complex(0,1)*l2*sa1*sa3*vev2 - 2*ca1**2*ca2*ca3*complex(0,1)*l3*sa1*sa3*vev2 - 2*ca1**2*ca2*ca3*complex(0,1)*l4*sa1*sa3*vev2 - 2*ca1**2*ca2*ca3*complex(0,1)*l5*sa1*sa3*vev2 - ca2**3*ca3*complex(0,1)*l8*sa1*sa3*vev2 + ca2*ca3*complex(0,1)*l3*sa1**3*sa3*vev2 + ca2*ca3*complex(0,1)*l4*sa1**3*sa3*vev2 + ca2*ca3*complex(0,1)*l5*sa1**3*sa3*vev2 - 3*ca1**2*ca2*ca3*complex(0,1)*l3*sa1*sa2**2*sa3*vev2 - 3*ca1**2*ca2*ca3*complex(0,1)*l4*sa1*sa2**2*sa3*vev2 - 3*ca1**2*ca2*ca3*complex(0,1)*l5*sa1*sa2**2*sa3*vev2 + 2*ca2*ca3*complex(0,1)*l8*sa1*sa2**2*sa3*vev2 - 3*ca2*ca3*complex(0,1)*l2*sa1**3*sa2**2*sa3*vev2 - ca1**3*ca2*complex(0,1)*l3*sa2*sa3**2*vev2 - ca1**3*ca2*complex(0,1)*l4*sa2*sa3**2*vev2 - ca1**3*ca2*complex(0,1)*l5*sa2*sa3**2*vev2 + ca1*ca2*complex(0,1)*l8*sa2*sa3**2*vev2 - 3*ca1*ca2*complex(0,1)*l2*sa1**2*sa2*sa3**2*vev2 + 2*ca1*ca2*complex(0,1)*l3*sa1**2*sa2*sa3**2*vev2 + 2*ca1*ca2*complex(0,1)*l4*sa1**2*sa2*sa3**2*vev2 + 2*ca1*ca2*complex(0,1)*l5*sa1**2*sa2*sa3**2*vev2 + ca1*ca2**2*ca3**2*complex(0,1)*l7*sa1*vevs - ca1*ca2**2*ca3**2*complex(0,1)*l8*sa1*vevs - ca1*ca3**2*complex(0,1)*l7*sa1*sa2**2*vevs + ca1*ca3**2*complex(0,1)*l8*sa1*sa2**2*vevs - 3*ca2**2*ca3*complex(0,1)*l6*sa2*sa3*vevs + 2*ca1**2*ca2**2*ca3*complex(0,1)*l7*sa2*sa3*vevs + ca1**2*ca3*complex(0,1)*l8*sa2*sa3*vevs + ca3*complex(0,1)*l7*sa1**2*sa2*sa3*vevs + 2*ca2**2*ca3*complex(0,1)*l8*sa1**2*sa2*sa3*vevs - ca1**2*ca3*complex(0,1)*l7*sa2**3*sa3*vevs - ca3*complex(0,1)*l8*sa1**2*sa2**3*sa3*vevs - ca1*ca2**2*complex(0,1)*l7*sa1*sa3**2*vevs + ca1*ca2**2*complex(0,1)*l8*sa1*sa3**2*vevs + ca1*complex(0,1)*l7*sa1*sa2**2*sa3**2*vevs - ca1*complex(0,1)*l8*sa1*sa2**2*sa3**2*vevs',
                  order = {'QED':1})

GC_187 = Coupling(name = 'GC_187',
                  value = '-(ca1**3*ca2*ca3**2*complex(0,1)*l3*vev1) - ca1**3*ca2*ca3**2*complex(0,1)*l4*vev1 - ca1**3*ca2*ca3**2*complex(0,1)*l5*vev1 - 3*ca1*ca2*ca3**2*complex(0,1)*l1*sa1**2*vev1 + 2*ca1*ca2*ca3**2*complex(0,1)*l3*sa1**2*vev1 + 2*ca1*ca2*ca3**2*complex(0,1)*l4*sa1**2*vev1 + 2*ca1*ca2*ca3**2*complex(0,1)*l5*sa1**2*vev1 - 6*ca1**2*ca2*ca3*complex(0,1)*l1*sa1*sa2*sa3*vev1 + 4*ca1**2*ca2*ca3*complex(0,1)*l3*sa1*sa2*sa3*vev1 + 4*ca1**2*ca2*ca3*complex(0,1)*l4*sa1*sa2*sa3*vev1 + 4*ca1**2*ca2*ca3*complex(0,1)*l5*sa1*sa2*sa3*vev1 + 2*ca2*ca3*complex(0,1)*l7*sa1*sa2*sa3*vev1 - 2*ca2*ca3*complex(0,1)*l3*sa1**3*sa2*sa3*vev1 - 2*ca2*ca3*complex(0,1)*l4*sa1**3*sa2*sa3*vev1 - 2*ca2*ca3*complex(0,1)*l5*sa1**3*sa2*sa3*vev1 - ca1*ca2**3*complex(0,1)*l7*sa3**2*vev1 - 3*ca1**3*ca2*complex(0,1)*l1*sa2**2*sa3**2*vev1 + 2*ca1*ca2*complex(0,1)*l7*sa2**2*sa3**2*vev1 - 3*ca1*ca2*complex(0,1)*l3*sa1**2*sa2**2*sa3**2*vev1 - 3*ca1*ca2*complex(0,1)*l4*sa1**2*sa2**2*sa3**2*vev1 - 3*ca1*ca2*complex(0,1)*l5*sa1**2*sa2**2*sa3**2*vev1 - 3*ca1**2*ca2*ca3**2*complex(0,1)*l2*sa1*vev2 + 2*ca1**2*ca2*ca3**2*complex(0,1)*l3*sa1*vev2 + 2*ca1**2*ca2*ca3**2*complex(0,1)*l4*sa1*vev2 + 2*ca1**2*ca2*ca3**2*complex(0,1)*l5*sa1*vev2 - ca2*ca3**2*complex(0,1)*l3*sa1**3*vev2 - ca2*ca3**2*complex(0,1)*l4*sa1**3*vev2 - ca2*ca3**2*complex(0,1)*l5*sa1**3*vev2 + 2*ca1**3*ca2*ca3*complex(0,1)*l3*sa2*sa3*vev2 + 2*ca1**3*ca2*ca3*complex(0,1)*l4*sa2*sa3*vev2 + 2*ca1**3*ca2*ca3*complex(0,1)*l5*sa2*sa3*vev2 - 2*ca1*ca2*ca3*complex(0,1)*l8*sa2*sa3*vev2 + 6*ca1*ca2*ca3*complex(0,1)*l2*sa1**2*sa2*sa3*vev2 - 4*ca1*ca2*ca3*complex(0,1)*l3*sa1**2*sa2*sa3*vev2 - 4*ca1*ca2*ca3*complex(0,1)*l4*sa1**2*sa2*sa3*vev2 - 4*ca1*ca2*ca3*complex(0,1)*l5*sa1**2*sa2*sa3*vev2 - ca2**3*complex(0,1)*l8*sa1*sa3**2*vev2 - 3*ca1**2*ca2*complex(0,1)*l3*sa1*sa2**2*sa3**2*vev2 - 3*ca1**2*ca2*complex(0,1)*l4*sa1*sa2**2*sa3**2*vev2 - 3*ca1**2*ca2*complex(0,1)*l5*sa1*sa2**2*sa3**2*vev2 + 2*ca2*complex(0,1)*l8*sa1*sa2**2*sa3**2*vev2 - 3*ca2*complex(0,1)*l2*sa1**3*sa2**2*sa3**2*vev2 - ca1**2*ca3**2*complex(0,1)*l8*sa2*vevs - ca3**2*complex(0,1)*l7*sa1**2*sa2*vevs + 2*ca1*ca2**2*ca3*complex(0,1)*l7*sa1*sa3*vevs - 2*ca1*ca2**2*ca3*complex(0,1)*l8*sa1*sa3*vevs - 2*ca1*ca3*complex(0,1)*l7*sa1*sa2**2*sa3*vevs + 2*ca1*ca3*complex(0,1)*l8*sa1*sa2**2*sa3*vevs - 3*ca2**2*complex(0,1)*l6*sa2*sa3**2*vevs + 2*ca1**2*ca2**2*complex(0,1)*l7*sa2*sa3**2*vevs + 2*ca2**2*complex(0,1)*l8*sa1**2*sa2*sa3**2*vevs - ca1**2*complex(0,1)*l7*sa2**3*sa3**2*vevs - complex(0,1)*l8*sa1**2*sa2**3*sa3**2*vevs',
                  order = {'QED':1})

GC_188 = Coupling(name = 'GC_188',
                  value = 'ca2**2*ca3**3*complex(0,1)*l7*sa1*vev1 + 3*ca1**2*ca3**3*complex(0,1)*l1*sa1*sa2**2*vev1 - 2*ca1**2*ca3**3*complex(0,1)*l3*sa1*sa2**2*vev1 - 2*ca1**2*ca3**3*complex(0,1)*l4*sa1*sa2**2*vev1 - 2*ca1**2*ca3**3*complex(0,1)*l5*sa1*sa2**2*vev1 + ca3**3*complex(0,1)*l3*sa1**3*sa2**2*vev1 + ca3**3*complex(0,1)*l4*sa1**3*sa2**2*vev1 + ca3**3*complex(0,1)*l5*sa1**3*sa2**2*vev1 - 2*ca1**3*ca3**2*complex(0,1)*l3*sa2*sa3*vev1 - 2*ca1**3*ca3**2*complex(0,1)*l4*sa2*sa3*vev1 - 2*ca1**3*ca3**2*complex(0,1)*l5*sa2*sa3*vev1 + 3*ca1*ca2**2*ca3**2*complex(0,1)*l7*sa2*sa3*vev1 - 6*ca1*ca3**2*complex(0,1)*l1*sa1**2*sa2*sa3*vev1 + 4*ca1*ca3**2*complex(0,1)*l3*sa1**2*sa2*sa3*vev1 + 4*ca1*ca3**2*complex(0,1)*l4*sa1**2*sa2*sa3*vev1 + 4*ca1*ca3**2*complex(0,1)*l5*sa1**2*sa2*sa3*vev1 + 3*ca1**3*ca3**2*complex(0,1)*l1*sa2**3*sa3*vev1 + 3*ca1*ca3**2*complex(0,1)*l3*sa1**2*sa2**3*sa3*vev1 + 3*ca1*ca3**2*complex(0,1)*l4*sa1**2*sa2**3*sa3*vev1 + 3*ca1*ca3**2*complex(0,1)*l5*sa1**2*sa2**3*sa3*vev1 + 3*ca1**2*ca3*complex(0,1)*l3*sa1*sa3**2*vev1 + 3*ca1**2*ca3*complex(0,1)*l4*sa1*sa3**2*vev1 + 3*ca1**2*ca3*complex(0,1)*l5*sa1*sa3**2*vev1 - 2*ca2**2*ca3*complex(0,1)*l7*sa1*sa3**2*vev1 + 3*ca3*complex(0,1)*l1*sa1**3*sa3**2*vev1 - 6*ca1**2*ca3*complex(0,1)*l1*sa1*sa2**2*sa3**2*vev1 + 4*ca1**2*ca3*complex(0,1)*l3*sa1*sa2**2*sa3**2*vev1 + 4*ca1**2*ca3*complex(0,1)*l4*sa1*sa2**2*sa3**2*vev1 + 4*ca1**2*ca3*complex(0,1)*l5*sa1*sa2**2*sa3**2*vev1 - 2*ca3*complex(0,1)*l3*sa1**3*sa2**2*sa3**2*vev1 - 2*ca3*complex(0,1)*l4*sa1**3*sa2**2*sa3**2*vev1 - 2*ca3*complex(0,1)*l5*sa1**3*sa2**2*sa3**2*vev1 + ca1**3*complex(0,1)*l3*sa2*sa3**3*vev1 + ca1**3*complex(0,1)*l4*sa2*sa3**3*vev1 + ca1**3*complex(0,1)*l5*sa2*sa3**3*vev1 + 3*ca1*complex(0,1)*l1*sa1**2*sa2*sa3**3*vev1 - 2*ca1*complex(0,1)*l3*sa1**2*sa2*sa3**3*vev1 - 2*ca1*complex(0,1)*l4*sa1**2*sa2*sa3**3*vev1 - 2*ca1*complex(0,1)*l5*sa1**2*sa2*sa3**3*vev1 - ca1*ca2**2*ca3**3*complex(0,1)*l8*vev2 - ca1**3*ca3**3*complex(0,1)*l3*sa2**2*vev2 - ca1**3*ca3**3*complex(0,1)*l4*sa2**2*vev2 - ca1**3*ca3**3*complex(0,1)*l5*sa2**2*vev2 - 3*ca1*ca3**3*complex(0,1)*l2*sa1**2*sa2**2*vev2 + 2*ca1*ca3**3*complex(0,1)*l3*sa1**2*sa2**2*vev2 + 2*ca1*ca3**3*complex(0,1)*l4*sa1**2*sa2**2*vev2 + 2*ca1*ca3**3*complex(0,1)*l5*sa1**2*sa2**2*vev2 - 6*ca1**2*ca3**2*complex(0,1)*l2*sa1*sa2*sa3*vev2 + 4*ca1**2*ca3**2*complex(0,1)*l3*sa1*sa2*sa3*vev2 + 4*ca1**2*ca3**2*complex(0,1)*l4*sa1*sa2*sa3*vev2 + 4*ca1**2*ca3**2*complex(0,1)*l5*sa1*sa2*sa3*vev2 + 3*ca2**2*ca3**2*complex(0,1)*l8*sa1*sa2*sa3*vev2 - 2*ca3**2*complex(0,1)*l3*sa1**3*sa2*sa3*vev2 - 2*ca3**2*complex(0,1)*l4*sa1**3*sa2*sa3*vev2 - 2*ca3**2*complex(0,1)*l5*sa1**3*sa2*sa3*vev2 + 3*ca1**2*ca3**2*complex(0,1)*l3*sa1*sa2**3*sa3*vev2 + 3*ca1**2*ca3**2*complex(0,1)*l4*sa1*sa2**3*sa3*vev2 + 3*ca1**2*ca3**2*complex(0,1)*l5*sa1*sa2**3*sa3*vev2 + 3*ca3**2*complex(0,1)*l2*sa1**3*sa2**3*sa3*vev2 - 3*ca1**3*ca3*complex(0,1)*l2*sa3**2*vev2 + 2*ca1*ca2**2*ca3*complex(0,1)*l8*sa3**2*vev2 - 3*ca1*ca3*complex(0,1)*l3*sa1**2*sa3**2*vev2 - 3*ca1*ca3*complex(0,1)*l4*sa1**2*sa3**2*vev2 - 3*ca1*ca3*complex(0,1)*l5*sa1**2*sa3**2*vev2 + 2*ca1**3*ca3*complex(0,1)*l3*sa2**2*sa3**2*vev2 + 2*ca1**3*ca3*complex(0,1)*l4*sa2**2*sa3**2*vev2 + 2*ca1**3*ca3*complex(0,1)*l5*sa2**2*sa3**2*vev2 + 6*ca1*ca3*complex(0,1)*l2*sa1**2*sa2**2*sa3**2*vev2 - 4*ca1*ca3*complex(0,1)*l3*sa1**2*sa2**2*sa3**2*vev2 - 4*ca1*ca3*complex(0,1)*l4*sa1**2*sa2**2*sa3**2*vev2 - 4*ca1*ca3*complex(0,1)*l5*sa1**2*sa2**2*sa3**2*vev2 + 3*ca1**2*complex(0,1)*l2*sa1*sa2*sa3**3*vev2 - 2*ca1**2*complex(0,1)*l3*sa1*sa2*sa3**3*vev2 - 2*ca1**2*complex(0,1)*l4*sa1*sa2*sa3**3*vev2 - 2*ca1**2*complex(0,1)*l5*sa1*sa2*sa3**3*vev2 + complex(0,1)*l3*sa1**3*sa2*sa3**3*vev2 + complex(0,1)*l4*sa1**3*sa2*sa3**3*vev2 + complex(0,1)*l5*sa1**3*sa2*sa3**3*vev2 - 2*ca1*ca2*ca3**3*complex(0,1)*l7*sa1*sa2*vevs + 2*ca1*ca2*ca3**3*complex(0,1)*l8*sa1*sa2*vevs - 3*ca2**3*ca3**2*complex(0,1)*l6*sa3*vevs + 2*ca1**2*ca2*ca3**2*complex(0,1)*l8*sa3*vevs + 2*ca2*ca3**2*complex(0,1)*l7*sa1**2*sa3*vevs - 3*ca1**2*ca2*ca3**2*complex(0,1)*l7*sa2**2*sa3*vevs - 3*ca2*ca3**2*complex(0,1)*l8*sa1**2*sa2**2*sa3*vevs + 4*ca1*ca2*ca3*complex(0,1)*l7*sa1*sa2*sa3**2*vevs - 4*ca1*ca2*ca3*complex(0,1)*l8*sa1*sa2*sa3**2*vevs - ca1**2*ca2*complex(0,1)*l8*sa3**3*vevs - ca2*complex(0,1)*l7*sa1**2*sa3**3*vevs',
                  order = {'QED':1})

GC_189 = Coupling(name = 'GC_189',
                  value = 'ca1**3*ca3**3*complex(0,1)*l3*sa2*vev1 + ca1**3*ca3**3*complex(0,1)*l4*sa2*vev1 + ca1**3*ca3**3*complex(0,1)*l5*sa2*vev1 + 3*ca1*ca3**3*complex(0,1)*l1*sa1**2*sa2*vev1 - 2*ca1*ca3**3*complex(0,1)*l3*sa1**2*sa2*vev1 - 2*ca1*ca3**3*complex(0,1)*l4*sa1**2*sa2*vev1 - 2*ca1*ca3**3*complex(0,1)*l5*sa1**2*sa2*vev1 - 3*ca1**2*ca3**2*complex(0,1)*l3*sa1*sa3*vev1 - 3*ca1**2*ca3**2*complex(0,1)*l4*sa1*sa3*vev1 - 3*ca1**2*ca3**2*complex(0,1)*l5*sa1*sa3*vev1 + 2*ca2**2*ca3**2*complex(0,1)*l7*sa1*sa3*vev1 - 3*ca3**2*complex(0,1)*l1*sa1**3*sa3*vev1 + 6*ca1**2*ca3**2*complex(0,1)*l1*sa1*sa2**2*sa3*vev1 - 4*ca1**2*ca3**2*complex(0,1)*l3*sa1*sa2**2*sa3*vev1 - 4*ca1**2*ca3**2*complex(0,1)*l4*sa1*sa2**2*sa3*vev1 - 4*ca1**2*ca3**2*complex(0,1)*l5*sa1*sa2**2*sa3*vev1 + 2*ca3**2*complex(0,1)*l3*sa1**3*sa2**2*sa3*vev1 + 2*ca3**2*complex(0,1)*l4*sa1**3*sa2**2*sa3*vev1 + 2*ca3**2*complex(0,1)*l5*sa1**3*sa2**2*sa3*vev1 - 2*ca1**3*ca3*complex(0,1)*l3*sa2*sa3**2*vev1 - 2*ca1**3*ca3*complex(0,1)*l4*sa2*sa3**2*vev1 - 2*ca1**3*ca3*complex(0,1)*l5*sa2*sa3**2*vev1 + 3*ca1*ca2**2*ca3*complex(0,1)*l7*sa2*sa3**2*vev1 - 6*ca1*ca3*complex(0,1)*l1*sa1**2*sa2*sa3**2*vev1 + 4*ca1*ca3*complex(0,1)*l3*sa1**2*sa2*sa3**2*vev1 + 4*ca1*ca3*complex(0,1)*l4*sa1**2*sa2*sa3**2*vev1 + 4*ca1*ca3*complex(0,1)*l5*sa1**2*sa2*sa3**2*vev1 + 3*ca1**3*ca3*complex(0,1)*l1*sa2**3*sa3**2*vev1 + 3*ca1*ca3*complex(0,1)*l3*sa1**2*sa2**3*sa3**2*vev1 + 3*ca1*ca3*complex(0,1)*l4*sa1**2*sa2**3*sa3**2*vev1 + 3*ca1*ca3*complex(0,1)*l5*sa1**2*sa2**3*sa3**2*vev1 - ca2**2*complex(0,1)*l7*sa1*sa3**3*vev1 - 3*ca1**2*complex(0,1)*l1*sa1*sa2**2*sa3**3*vev1 + 2*ca1**2*complex(0,1)*l3*sa1*sa2**2*sa3**3*vev1 + 2*ca1**2*complex(0,1)*l4*sa1*sa2**2*sa3**3*vev1 + 2*ca1**2*complex(0,1)*l5*sa1*sa2**2*sa3**3*vev1 - complex(0,1)*l3*sa1**3*sa2**2*sa3**3*vev1 - complex(0,1)*l4*sa1**3*sa2**2*sa3**3*vev1 - complex(0,1)*l5*sa1**3*sa2**2*sa3**3*vev1 + 3*ca1**2*ca3**3*complex(0,1)*l2*sa1*sa2*vev2 - 2*ca1**2*ca3**3*complex(0,1)*l3*sa1*sa2*vev2 - 2*ca1**2*ca3**3*complex(0,1)*l4*sa1*sa2*vev2 - 2*ca1**2*ca3**3*complex(0,1)*l5*sa1*sa2*vev2 + ca3**3*complex(0,1)*l3*sa1**3*sa2*vev2 + ca3**3*complex(0,1)*l4*sa1**3*sa2*vev2 + ca3**3*complex(0,1)*l5*sa1**3*sa2*vev2 + 3*ca1**3*ca3**2*complex(0,1)*l2*sa3*vev2 - 2*ca1*ca2**2*ca3**2*complex(0,1)*l8*sa3*vev2 + 3*ca1*ca3**2*complex(0,1)*l3*sa1**2*sa3*vev2 + 3*ca1*ca3**2*complex(0,1)*l4*sa1**2*sa3*vev2 + 3*ca1*ca3**2*complex(0,1)*l5*sa1**2*sa3*vev2 - 2*ca1**3*ca3**2*complex(0,1)*l3*sa2**2*sa3*vev2 - 2*ca1**3*ca3**2*complex(0,1)*l4*sa2**2*sa3*vev2 - 2*ca1**3*ca3**2*complex(0,1)*l5*sa2**2*sa3*vev2 - 6*ca1*ca3**2*complex(0,1)*l2*sa1**2*sa2**2*sa3*vev2 + 4*ca1*ca3**2*complex(0,1)*l3*sa1**2*sa2**2*sa3*vev2 + 4*ca1*ca3**2*complex(0,1)*l4*sa1**2*sa2**2*sa3*vev2 + 4*ca1*ca3**2*complex(0,1)*l5*sa1**2*sa2**2*sa3*vev2 - 6*ca1**2*ca3*complex(0,1)*l2*sa1*sa2*sa3**2*vev2 + 4*ca1**2*ca3*complex(0,1)*l3*sa1*sa2*sa3**2*vev2 + 4*ca1**2*ca3*complex(0,1)*l4*sa1*sa2*sa3**2*vev2 + 4*ca1**2*ca3*complex(0,1)*l5*sa1*sa2*sa3**2*vev2 + 3*ca2**2*ca3*complex(0,1)*l8*sa1*sa2*sa3**2*vev2 - 2*ca3*complex(0,1)*l3*sa1**3*sa2*sa3**2*vev2 - 2*ca3*complex(0,1)*l4*sa1**3*sa2*sa3**2*vev2 - 2*ca3*complex(0,1)*l5*sa1**3*sa2*sa3**2*vev2 + 3*ca1**2*ca3*complex(0,1)*l3*sa1*sa2**3*sa3**2*vev2 + 3*ca1**2*ca3*complex(0,1)*l4*sa1*sa2**3*sa3**2*vev2 + 3*ca1**2*ca3*complex(0,1)*l5*sa1*sa2**3*sa3**2*vev2 + 3*ca3*complex(0,1)*l2*sa1**3*sa2**3*sa3**2*vev2 + ca1*ca2**2*complex(0,1)*l8*sa3**3*vev2 + ca1**3*complex(0,1)*l3*sa2**2*sa3**3*vev2 + ca1**3*complex(0,1)*l4*sa2**2*sa3**3*vev2 + ca1**3*complex(0,1)*l5*sa2**2*sa3**3*vev2 + 3*ca1*complex(0,1)*l2*sa1**2*sa2**2*sa3**3*vev2 - 2*ca1*complex(0,1)*l3*sa1**2*sa2**2*sa3**3*vev2 - 2*ca1*complex(0,1)*l4*sa1**2*sa2**2*sa3**3*vev2 - 2*ca1*complex(0,1)*l5*sa1**2*sa2**2*sa3**3*vev2 - ca1**2*ca2*ca3**3*complex(0,1)*l8*vevs - ca2*ca3**3*complex(0,1)*l7*sa1**2*vevs - 4*ca1*ca2*ca3**2*complex(0,1)*l7*sa1*sa2*sa3*vevs + 4*ca1*ca2*ca3**2*complex(0,1)*l8*sa1*sa2*sa3*vevs - 3*ca2**3*ca3*complex(0,1)*l6*sa3**2*vevs + 2*ca1**2*ca2*ca3*complex(0,1)*l8*sa3**2*vevs + 2*ca2*ca3*complex(0,1)*l7*sa1**2*sa3**2*vevs - 3*ca1**2*ca2*ca3*complex(0,1)*l7*sa2**2*sa3**2*vevs - 3*ca2*ca3*complex(0,1)*l8*sa1**2*sa2**2*sa3**2*vevs + 2*ca1*ca2*complex(0,1)*l7*sa1*sa2*sa3**3*vevs - 2*ca1*ca2*complex(0,1)*l8*sa1*sa2*sa3**3*vevs',
                  order = {'QED':1})

GC_190 = Coupling(name = 'GC_190',
                  value = '3*ca1**2*ca3**3*complex(0,1)*l3*sa1*vev1 + 3*ca1**2*ca3**3*complex(0,1)*l4*sa1*vev1 + 3*ca1**2*ca3**3*complex(0,1)*l5*sa1*vev1 + 3*ca3**3*complex(0,1)*l1*sa1**3*vev1 + 3*ca1**3*ca3**2*complex(0,1)*l3*sa2*sa3*vev1 + 3*ca1**3*ca3**2*complex(0,1)*l4*sa2*sa3*vev1 + 3*ca1**3*ca3**2*complex(0,1)*l5*sa2*sa3*vev1 + 9*ca1*ca3**2*complex(0,1)*l1*sa1**2*sa2*sa3*vev1 - 6*ca1*ca3**2*complex(0,1)*l3*sa1**2*sa2*sa3*vev1 - 6*ca1*ca3**2*complex(0,1)*l4*sa1**2*sa2*sa3*vev1 - 6*ca1*ca3**2*complex(0,1)*l5*sa1**2*sa2*sa3*vev1 + 3*ca2**2*ca3*complex(0,1)*l7*sa1*sa3**2*vev1 + 9*ca1**2*ca3*complex(0,1)*l1*sa1*sa2**2*sa3**2*vev1 - 6*ca1**2*ca3*complex(0,1)*l3*sa1*sa2**2*sa3**2*vev1 - 6*ca1**2*ca3*complex(0,1)*l4*sa1*sa2**2*sa3**2*vev1 - 6*ca1**2*ca3*complex(0,1)*l5*sa1*sa2**2*sa3**2*vev1 + 3*ca3*complex(0,1)*l3*sa1**3*sa2**2*sa3**2*vev1 + 3*ca3*complex(0,1)*l4*sa1**3*sa2**2*sa3**2*vev1 + 3*ca3*complex(0,1)*l5*sa1**3*sa2**2*sa3**2*vev1 + 3*ca1*ca2**2*complex(0,1)*l7*sa2*sa3**3*vev1 + 3*ca1**3*complex(0,1)*l1*sa2**3*sa3**3*vev1 + 3*ca1*complex(0,1)*l3*sa1**2*sa2**3*sa3**3*vev1 + 3*ca1*complex(0,1)*l4*sa1**2*sa2**3*sa3**3*vev1 + 3*ca1*complex(0,1)*l5*sa1**2*sa2**3*sa3**3*vev1 - 3*ca1**3*ca3**3*complex(0,1)*l2*vev2 - 3*ca1*ca3**3*complex(0,1)*l3*sa1**2*vev2 - 3*ca1*ca3**3*complex(0,1)*l4*sa1**2*vev2 - 3*ca1*ca3**3*complex(0,1)*l5*sa1**2*vev2 + 9*ca1**2*ca3**2*complex(0,1)*l2*sa1*sa2*sa3*vev2 - 6*ca1**2*ca3**2*complex(0,1)*l3*sa1*sa2*sa3*vev2 - 6*ca1**2*ca3**2*complex(0,1)*l4*sa1*sa2*sa3*vev2 - 6*ca1**2*ca3**2*complex(0,1)*l5*sa1*sa2*sa3*vev2 + 3*ca3**2*complex(0,1)*l3*sa1**3*sa2*sa3*vev2 + 3*ca3**2*complex(0,1)*l4*sa1**3*sa2*sa3*vev2 + 3*ca3**2*complex(0,1)*l5*sa1**3*sa2*sa3*vev2 - 3*ca1*ca2**2*ca3*complex(0,1)*l8*sa3**2*vev2 - 3*ca1**3*ca3*complex(0,1)*l3*sa2**2*sa3**2*vev2 - 3*ca1**3*ca3*complex(0,1)*l4*sa2**2*sa3**2*vev2 - 3*ca1**3*ca3*complex(0,1)*l5*sa2**2*sa3**2*vev2 - 9*ca1*ca3*complex(0,1)*l2*sa1**2*sa2**2*sa3**2*vev2 + 6*ca1*ca3*complex(0,1)*l3*sa1**2*sa2**2*sa3**2*vev2 + 6*ca1*ca3*complex(0,1)*l4*sa1**2*sa2**2*sa3**2*vev2 + 6*ca1*ca3*complex(0,1)*l5*sa1**2*sa2**2*sa3**2*vev2 + 3*ca2**2*complex(0,1)*l8*sa1*sa2*sa3**3*vev2 + 3*ca1**2*complex(0,1)*l3*sa1*sa2**3*sa3**3*vev2 + 3*ca1**2*complex(0,1)*l4*sa1*sa2**3*sa3**3*vev2 + 3*ca1**2*complex(0,1)*l5*sa1*sa2**3*sa3**3*vev2 + 3*complex(0,1)*l2*sa1**3*sa2**3*sa3**3*vev2 - 3*ca1**2*ca2*ca3**2*complex(0,1)*l8*sa3*vevs - 3*ca2*ca3**2*complex(0,1)*l7*sa1**2*sa3*vevs - 6*ca1*ca2*ca3*complex(0,1)*l7*sa1*sa2*sa3**2*vevs + 6*ca1*ca2*ca3*complex(0,1)*l8*sa1*sa2*sa3**2*vevs - 3*ca2**3*complex(0,1)*l6*sa3**3*vevs - 3*ca1**2*ca2*complex(0,1)*l7*sa2**2*sa3**3*vevs - 3*ca2*complex(0,1)*l8*sa1**2*sa2**2*sa3**3*vevs',
                  order = {'QED':1})

GC_191 = Coupling(name = 'GC_191',
                  value = '(ca3*cb**2*complex(0,1)*l4*sa1*sa2*vev1)/2. + (ca3*cb**2*complex(0,1)*l5*sa1*sa2*vev1)/2. + (ca1*cb**2*complex(0,1)*l4*sa3*vev1)/2. + (ca1*cb**2*complex(0,1)*l5*sa3*vev1)/2. - ca1*ca3*cb*complex(0,1)*l1*sa2*sb*vev1 + ca1*ca3*cb*complex(0,1)*l3*sa2*sb*vev1 + cb*complex(0,1)*l1*sa1*sa3*sb*vev1 - cb*complex(0,1)*l3*sa1*sa3*sb*vev1 - (ca3*complex(0,1)*l4*sa1*sa2*sb**2*vev1)/2. - (ca3*complex(0,1)*l5*sa1*sa2*sb**2*vev1)/2. - (ca1*complex(0,1)*l4*sa3*sb**2*vev1)/2. - (ca1*complex(0,1)*l5*sa3*sb**2*vev1)/2. + (ca1*ca3*cb**2*complex(0,1)*l4*sa2*vev2)/2. + (ca1*ca3*cb**2*complex(0,1)*l5*sa2*vev2)/2. - (cb**2*complex(0,1)*l4*sa1*sa3*vev2)/2. - (cb**2*complex(0,1)*l5*sa1*sa3*vev2)/2. + ca3*cb*complex(0,1)*l2*sa1*sa2*sb*vev2 - ca3*cb*complex(0,1)*l3*sa1*sa2*sb*vev2 + ca1*cb*complex(0,1)*l2*sa3*sb*vev2 - ca1*cb*complex(0,1)*l3*sa3*sb*vev2 - (ca1*ca3*complex(0,1)*l4*sa2*sb**2*vev2)/2. - (ca1*ca3*complex(0,1)*l5*sa2*sb**2*vev2)/2. + (complex(0,1)*l4*sa1*sa3*sb**2*vev2)/2. + (complex(0,1)*l5*sa1*sa3*sb**2*vev2)/2. + ca2*ca3*cb*complex(0,1)*l7*sb*vevs - ca2*ca3*cb*complex(0,1)*l8*sb*vevs',
                  order = {'QED':1})

GC_192 = Coupling(name = 'GC_192',
                  value = 'ca3*cb**2*complex(0,1)*l5*sa1*sa2*vev1 + ca1*cb**2*complex(0,1)*l5*sa3*vev1 - ca1*ca3*cb*complex(0,1)*l1*sa2*sb*vev1 + ca1*ca3*cb*complex(0,1)*l3*sa2*sb*vev1 + ca1*ca3*cb*complex(0,1)*l4*sa2*sb*vev1 - ca1*ca3*cb*complex(0,1)*l5*sa2*sb*vev1 + cb*complex(0,1)*l1*sa1*sa3*sb*vev1 - cb*complex(0,1)*l3*sa1*sa3*sb*vev1 - cb*complex(0,1)*l4*sa1*sa3*sb*vev1 + cb*complex(0,1)*l5*sa1*sa3*sb*vev1 - ca3*complex(0,1)*l5*sa1*sa2*sb**2*vev1 - ca1*complex(0,1)*l5*sa3*sb**2*vev1 + ca1*ca3*cb**2*complex(0,1)*l5*sa2*vev2 - cb**2*complex(0,1)*l5*sa1*sa3*vev2 + ca3*cb*complex(0,1)*l2*sa1*sa2*sb*vev2 - ca3*cb*complex(0,1)*l3*sa1*sa2*sb*vev2 - ca3*cb*complex(0,1)*l4*sa1*sa2*sb*vev2 + ca3*cb*complex(0,1)*l5*sa1*sa2*sb*vev2 + ca1*cb*complex(0,1)*l2*sa3*sb*vev2 - ca1*cb*complex(0,1)*l3*sa3*sb*vev2 - ca1*cb*complex(0,1)*l4*sa3*sb*vev2 + ca1*cb*complex(0,1)*l5*sa3*sb*vev2 - ca1*ca3*complex(0,1)*l5*sa2*sb**2*vev2 + complex(0,1)*l5*sa1*sa3*sb**2*vev2 + ca2*ca3*cb*complex(0,1)*l7*sb*vevs - ca2*ca3*cb*complex(0,1)*l8*sb*vevs',
                  order = {'QED':1})

GC_193 = Coupling(name = 'GC_193',
                  value = '-(ca2*cb**2*complex(0,1)*l4*sa1*vev1)/2. - (ca2*cb**2*complex(0,1)*l5*sa1*vev1)/2. + ca1*ca2*cb*complex(0,1)*l1*sb*vev1 - ca1*ca2*cb*complex(0,1)*l3*sb*vev1 + (ca2*complex(0,1)*l4*sa1*sb**2*vev1)/2. + (ca2*complex(0,1)*l5*sa1*sb**2*vev1)/2. - (ca1*ca2*cb**2*complex(0,1)*l4*vev2)/2. - (ca1*ca2*cb**2*complex(0,1)*l5*vev2)/2. - ca2*cb*complex(0,1)*l2*sa1*sb*vev2 + ca2*cb*complex(0,1)*l3*sa1*sb*vev2 + (ca1*ca2*complex(0,1)*l4*sb**2*vev2)/2. + (ca1*ca2*complex(0,1)*l5*sb**2*vev2)/2. + cb*complex(0,1)*l7*sa2*sb*vevs - cb*complex(0,1)*l8*sa2*sb*vevs',
                  order = {'QED':1})

GC_194 = Coupling(name = 'GC_194',
                  value = '-(ca2*cb**2*complex(0,1)*l5*sa1*vev1) + ca1*ca2*cb*complex(0,1)*l1*sb*vev1 - ca1*ca2*cb*complex(0,1)*l3*sb*vev1 - ca1*ca2*cb*complex(0,1)*l4*sb*vev1 + ca1*ca2*cb*complex(0,1)*l5*sb*vev1 + ca2*complex(0,1)*l5*sa1*sb**2*vev1 - ca1*ca2*cb**2*complex(0,1)*l5*vev2 - ca2*cb*complex(0,1)*l2*sa1*sb*vev2 + ca2*cb*complex(0,1)*l3*sa1*sb*vev2 + ca2*cb*complex(0,1)*l4*sa1*sb*vev2 - ca2*cb*complex(0,1)*l5*sa1*sb*vev2 + ca1*ca2*complex(0,1)*l5*sb**2*vev2 + cb*complex(0,1)*l7*sa2*sb*vevs - cb*complex(0,1)*l8*sa2*sb*vevs',
                  order = {'QED':1})

GC_195 = Coupling(name = 'GC_195',
                  value = '-(ca1*ca3*cb**2*complex(0,1)*l4*vev1)/2. - (ca1*ca3*cb**2*complex(0,1)*l5*vev1)/2. + (cb**2*complex(0,1)*l4*sa1*sa2*sa3*vev1)/2. + (cb**2*complex(0,1)*l5*sa1*sa2*sa3*vev1)/2. - ca3*cb*complex(0,1)*l1*sa1*sb*vev1 + ca3*cb*complex(0,1)*l3*sa1*sb*vev1 - ca1*cb*complex(0,1)*l1*sa2*sa3*sb*vev1 + ca1*cb*complex(0,1)*l3*sa2*sa3*sb*vev1 + (ca1*ca3*complex(0,1)*l4*sb**2*vev1)/2. + (ca1*ca3*complex(0,1)*l5*sb**2*vev1)/2. - (complex(0,1)*l4*sa1*sa2*sa3*sb**2*vev1)/2. - (complex(0,1)*l5*sa1*sa2*sa3*sb**2*vev1)/2. + (ca3*cb**2*complex(0,1)*l4*sa1*vev2)/2. + (ca3*cb**2*complex(0,1)*l5*sa1*vev2)/2. + (ca1*cb**2*complex(0,1)*l4*sa2*sa3*vev2)/2. + (ca1*cb**2*complex(0,1)*l5*sa2*sa3*vev2)/2. - ca1*ca3*cb*complex(0,1)*l2*sb*vev2 + ca1*ca3*cb*complex(0,1)*l3*sb*vev2 + cb*complex(0,1)*l2*sa1*sa2*sa3*sb*vev2 - cb*complex(0,1)*l3*sa1*sa2*sa3*sb*vev2 - (ca3*complex(0,1)*l4*sa1*sb**2*vev2)/2. - (ca3*complex(0,1)*l5*sa1*sb**2*vev2)/2. - (ca1*complex(0,1)*l4*sa2*sa3*sb**2*vev2)/2. - (ca1*complex(0,1)*l5*sa2*sa3*sb**2*vev2)/2. + ca2*cb*complex(0,1)*l7*sa3*sb*vevs - ca2*cb*complex(0,1)*l8*sa3*sb*vevs',
                  order = {'QED':1})

GC_196 = Coupling(name = 'GC_196',
                  value = '-(ca1*ca3*cb**2*complex(0,1)*l5*vev1) + cb**2*complex(0,1)*l5*sa1*sa2*sa3*vev1 - ca3*cb*complex(0,1)*l1*sa1*sb*vev1 + ca3*cb*complex(0,1)*l3*sa1*sb*vev1 + ca3*cb*complex(0,1)*l4*sa1*sb*vev1 - ca3*cb*complex(0,1)*l5*sa1*sb*vev1 - ca1*cb*complex(0,1)*l1*sa2*sa3*sb*vev1 + ca1*cb*complex(0,1)*l3*sa2*sa3*sb*vev1 + ca1*cb*complex(0,1)*l4*sa2*sa3*sb*vev1 - ca1*cb*complex(0,1)*l5*sa2*sa3*sb*vev1 + ca1*ca3*complex(0,1)*l5*sb**2*vev1 - complex(0,1)*l5*sa1*sa2*sa3*sb**2*vev1 + ca3*cb**2*complex(0,1)*l5*sa1*vev2 + ca1*cb**2*complex(0,1)*l5*sa2*sa3*vev2 - ca1*ca3*cb*complex(0,1)*l2*sb*vev2 + ca1*ca3*cb*complex(0,1)*l3*sb*vev2 + ca1*ca3*cb*complex(0,1)*l4*sb*vev2 - ca1*ca3*cb*complex(0,1)*l5*sb*vev2 + cb*complex(0,1)*l2*sa1*sa2*sa3*sb*vev2 - cb*complex(0,1)*l3*sa1*sa2*sa3*sb*vev2 - cb*complex(0,1)*l4*sa1*sa2*sa3*sb*vev2 + cb*complex(0,1)*l5*sa1*sa2*sa3*sb*vev2 - ca3*complex(0,1)*l5*sa1*sb**2*vev2 - ca1*complex(0,1)*l5*sa2*sa3*sb**2*vev2 + ca2*cb*complex(0,1)*l7*sa3*sb*vevs - ca2*cb*complex(0,1)*l8*sa3*sb*vevs',
                  order = {'QED':1})

GC_197 = Coupling(name = 'GC_197',
                  value = 'ca1*ca3*cb**2*complex(0,1)*l3*sa2*vev1 - cb**2*complex(0,1)*l3*sa1*sa3*vev1 - ca3*cb*complex(0,1)*l4*sa1*sa2*sb*vev1 - ca3*cb*complex(0,1)*l5*sa1*sa2*sb*vev1 - ca1*cb*complex(0,1)*l4*sa3*sb*vev1 - ca1*cb*complex(0,1)*l5*sa3*sb*vev1 + ca1*ca3*complex(0,1)*l1*sa2*sb**2*vev1 - complex(0,1)*l1*sa1*sa3*sb**2*vev1 + ca3*cb**2*complex(0,1)*l2*sa1*sa2*vev2 + ca1*cb**2*complex(0,1)*l2*sa3*vev2 - ca1*ca3*cb*complex(0,1)*l4*sa2*sb*vev2 - ca1*ca3*cb*complex(0,1)*l5*sa2*sb*vev2 + cb*complex(0,1)*l4*sa1*sa3*sb*vev2 + cb*complex(0,1)*l5*sa1*sa3*sb*vev2 + ca3*complex(0,1)*l3*sa1*sa2*sb**2*vev2 + ca1*complex(0,1)*l3*sa3*sb**2*vev2 - ca2*ca3*cb**2*complex(0,1)*l8*vevs - ca2*ca3*complex(0,1)*l7*sb**2*vevs',
                  order = {'QED':1})

GC_198 = Coupling(name = 'GC_198',
                  value = 'ca1*ca3*cb**2*complex(0,1)*l3*sa2*vev1 + ca1*ca3*cb**2*complex(0,1)*l4*sa2*vev1 - ca1*ca3*cb**2*complex(0,1)*l5*sa2*vev1 - cb**2*complex(0,1)*l3*sa1*sa3*vev1 - cb**2*complex(0,1)*l4*sa1*sa3*vev1 + cb**2*complex(0,1)*l5*sa1*sa3*vev1 - 2*ca3*cb*complex(0,1)*l5*sa1*sa2*sb*vev1 - 2*ca1*cb*complex(0,1)*l5*sa3*sb*vev1 + ca1*ca3*complex(0,1)*l1*sa2*sb**2*vev1 - complex(0,1)*l1*sa1*sa3*sb**2*vev1 + ca3*cb**2*complex(0,1)*l2*sa1*sa2*vev2 + ca1*cb**2*complex(0,1)*l2*sa3*vev2 - 2*ca1*ca3*cb*complex(0,1)*l5*sa2*sb*vev2 + 2*cb*complex(0,1)*l5*sa1*sa3*sb*vev2 + ca3*complex(0,1)*l3*sa1*sa2*sb**2*vev2 + ca3*complex(0,1)*l4*sa1*sa2*sb**2*vev2 - ca3*complex(0,1)*l5*sa1*sa2*sb**2*vev2 + ca1*complex(0,1)*l3*sa3*sb**2*vev2 + ca1*complex(0,1)*l4*sa3*sb**2*vev2 - ca1*complex(0,1)*l5*sa3*sb**2*vev2 - ca2*ca3*cb**2*complex(0,1)*l8*vevs - ca2*ca3*complex(0,1)*l7*sb**2*vevs',
                  order = {'QED':1})

GC_199 = Coupling(name = 'GC_199',
                  value = 'ca1*ca3*cb**2*complex(0,1)*l1*sa2*vev1 - cb**2*complex(0,1)*l1*sa1*sa3*vev1 + ca3*cb*complex(0,1)*l4*sa1*sa2*sb*vev1 + ca3*cb*complex(0,1)*l5*sa1*sa2*sb*vev1 + ca1*cb*complex(0,1)*l4*sa3*sb*vev1 + ca1*cb*complex(0,1)*l5*sa3*sb*vev1 + ca1*ca3*complex(0,1)*l3*sa2*sb**2*vev1 - complex(0,1)*l3*sa1*sa3*sb**2*vev1 + ca3*cb**2*complex(0,1)*l3*sa1*sa2*vev2 + ca1*cb**2*complex(0,1)*l3*sa3*vev2 + ca1*ca3*cb*complex(0,1)*l4*sa2*sb*vev2 + ca1*ca3*cb*complex(0,1)*l5*sa2*sb*vev2 - cb*complex(0,1)*l4*sa1*sa3*sb*vev2 - cb*complex(0,1)*l5*sa1*sa3*sb*vev2 + ca3*complex(0,1)*l2*sa1*sa2*sb**2*vev2 + ca1*complex(0,1)*l2*sa3*sb**2*vev2 - ca2*ca3*cb**2*complex(0,1)*l7*vevs - ca2*ca3*complex(0,1)*l8*sb**2*vevs',
                  order = {'QED':1})

GC_200 = Coupling(name = 'GC_200',
                  value = 'ca1*ca3*cb**2*complex(0,1)*l1*sa2*vev1 - cb**2*complex(0,1)*l1*sa1*sa3*vev1 + 2*ca3*cb*complex(0,1)*l5*sa1*sa2*sb*vev1 + 2*ca1*cb*complex(0,1)*l5*sa3*sb*vev1 + ca1*ca3*complex(0,1)*l3*sa2*sb**2*vev1 + ca1*ca3*complex(0,1)*l4*sa2*sb**2*vev1 - ca1*ca3*complex(0,1)*l5*sa2*sb**2*vev1 - complex(0,1)*l3*sa1*sa3*sb**2*vev1 - complex(0,1)*l4*sa1*sa3*sb**2*vev1 + complex(0,1)*l5*sa1*sa3*sb**2*vev1 + ca3*cb**2*complex(0,1)*l3*sa1*sa2*vev2 + ca3*cb**2*complex(0,1)*l4*sa1*sa2*vev2 - ca3*cb**2*complex(0,1)*l5*sa1*sa2*vev2 + ca1*cb**2*complex(0,1)*l3*sa3*vev2 + ca1*cb**2*complex(0,1)*l4*sa3*vev2 - ca1*cb**2*complex(0,1)*l5*sa3*vev2 + 2*ca1*ca3*cb*complex(0,1)*l5*sa2*sb*vev2 - 2*cb*complex(0,1)*l5*sa1*sa3*sb*vev2 + ca3*complex(0,1)*l2*sa1*sa2*sb**2*vev2 + ca1*complex(0,1)*l2*sa3*sb**2*vev2 - ca2*ca3*cb**2*complex(0,1)*l7*vevs - ca2*ca3*complex(0,1)*l8*sb**2*vevs',
                  order = {'QED':1})

GC_201 = Coupling(name = 'GC_201',
                  value = '-(ca1*ca2*cb**2*complex(0,1)*l3*vev1) + ca2*cb*complex(0,1)*l4*sa1*sb*vev1 + ca2*cb*complex(0,1)*l5*sa1*sb*vev1 - ca1*ca2*complex(0,1)*l1*sb**2*vev1 - ca2*cb**2*complex(0,1)*l2*sa1*vev2 + ca1*ca2*cb*complex(0,1)*l4*sb*vev2 + ca1*ca2*cb*complex(0,1)*l5*sb*vev2 - ca2*complex(0,1)*l3*sa1*sb**2*vev2 - cb**2*complex(0,1)*l8*sa2*vevs - complex(0,1)*l7*sa2*sb**2*vevs',
                  order = {'QED':1})

GC_202 = Coupling(name = 'GC_202',
                  value = '-(ca1*ca2*cb**2*complex(0,1)*l3*vev1) - ca1*ca2*cb**2*complex(0,1)*l4*vev1 + ca1*ca2*cb**2*complex(0,1)*l5*vev1 + 2*ca2*cb*complex(0,1)*l5*sa1*sb*vev1 - ca1*ca2*complex(0,1)*l1*sb**2*vev1 - ca2*cb**2*complex(0,1)*l2*sa1*vev2 + 2*ca1*ca2*cb*complex(0,1)*l5*sb*vev2 - ca2*complex(0,1)*l3*sa1*sb**2*vev2 - ca2*complex(0,1)*l4*sa1*sb**2*vev2 + ca2*complex(0,1)*l5*sa1*sb**2*vev2 - cb**2*complex(0,1)*l8*sa2*vevs - complex(0,1)*l7*sa2*sb**2*vevs',
                  order = {'QED':1})

GC_203 = Coupling(name = 'GC_203',
                  value = '-(ca1*ca2*cb**2*complex(0,1)*l1*vev1) - ca2*cb*complex(0,1)*l4*sa1*sb*vev1 - ca2*cb*complex(0,1)*l5*sa1*sb*vev1 - ca1*ca2*complex(0,1)*l3*sb**2*vev1 - ca2*cb**2*complex(0,1)*l3*sa1*vev2 - ca1*ca2*cb*complex(0,1)*l4*sb*vev2 - ca1*ca2*cb*complex(0,1)*l5*sb*vev2 - ca2*complex(0,1)*l2*sa1*sb**2*vev2 - cb**2*complex(0,1)*l7*sa2*vevs - complex(0,1)*l8*sa2*sb**2*vevs',
                  order = {'QED':1})

GC_204 = Coupling(name = 'GC_204',
                  value = '-(ca1*ca2*cb**2*complex(0,1)*l1*vev1) - 2*ca2*cb*complex(0,1)*l5*sa1*sb*vev1 - ca1*ca2*complex(0,1)*l3*sb**2*vev1 - ca1*ca2*complex(0,1)*l4*sb**2*vev1 + ca1*ca2*complex(0,1)*l5*sb**2*vev1 - ca2*cb**2*complex(0,1)*l3*sa1*vev2 - ca2*cb**2*complex(0,1)*l4*sa1*vev2 + ca2*cb**2*complex(0,1)*l5*sa1*vev2 - 2*ca1*ca2*cb*complex(0,1)*l5*sb*vev2 - ca2*complex(0,1)*l2*sa1*sb**2*vev2 - cb**2*complex(0,1)*l7*sa2*vevs - complex(0,1)*l8*sa2*sb**2*vevs',
                  order = {'QED':1})

GC_205 = Coupling(name = 'GC_205',
                  value = 'ca3*cb**2*complex(0,1)*l3*sa1*vev1 + ca1*cb**2*complex(0,1)*l3*sa2*sa3*vev1 + ca1*ca3*cb*complex(0,1)*l4*sb*vev1 + ca1*ca3*cb*complex(0,1)*l5*sb*vev1 - cb*complex(0,1)*l4*sa1*sa2*sa3*sb*vev1 - cb*complex(0,1)*l5*sa1*sa2*sa3*sb*vev1 + ca3*complex(0,1)*l1*sa1*sb**2*vev1 + ca1*complex(0,1)*l1*sa2*sa3*sb**2*vev1 - ca1*ca3*cb**2*complex(0,1)*l2*vev2 + cb**2*complex(0,1)*l2*sa1*sa2*sa3*vev2 - ca3*cb*complex(0,1)*l4*sa1*sb*vev2 - ca3*cb*complex(0,1)*l5*sa1*sb*vev2 - ca1*cb*complex(0,1)*l4*sa2*sa3*sb*vev2 - ca1*cb*complex(0,1)*l5*sa2*sa3*sb*vev2 - ca1*ca3*complex(0,1)*l3*sb**2*vev2 + complex(0,1)*l3*sa1*sa2*sa3*sb**2*vev2 - ca2*cb**2*complex(0,1)*l8*sa3*vevs - ca2*complex(0,1)*l7*sa3*sb**2*vevs',
                  order = {'QED':1})

GC_206 = Coupling(name = 'GC_206',
                  value = 'ca3*cb**2*complex(0,1)*l3*sa1*vev1 + ca3*cb**2*complex(0,1)*l4*sa1*vev1 - ca3*cb**2*complex(0,1)*l5*sa1*vev1 + ca1*cb**2*complex(0,1)*l3*sa2*sa3*vev1 + ca1*cb**2*complex(0,1)*l4*sa2*sa3*vev1 - ca1*cb**2*complex(0,1)*l5*sa2*sa3*vev1 + 2*ca1*ca3*cb*complex(0,1)*l5*sb*vev1 - 2*cb*complex(0,1)*l5*sa1*sa2*sa3*sb*vev1 + ca3*complex(0,1)*l1*sa1*sb**2*vev1 + ca1*complex(0,1)*l1*sa2*sa3*sb**2*vev1 - ca1*ca3*cb**2*complex(0,1)*l2*vev2 + cb**2*complex(0,1)*l2*sa1*sa2*sa3*vev2 - 2*ca3*cb*complex(0,1)*l5*sa1*sb*vev2 - 2*ca1*cb*complex(0,1)*l5*sa2*sa3*sb*vev2 - ca1*ca3*complex(0,1)*l3*sb**2*vev2 - ca1*ca3*complex(0,1)*l4*sb**2*vev2 + ca1*ca3*complex(0,1)*l5*sb**2*vev2 + complex(0,1)*l3*sa1*sa2*sa3*sb**2*vev2 + complex(0,1)*l4*sa1*sa2*sa3*sb**2*vev2 - complex(0,1)*l5*sa1*sa2*sa3*sb**2*vev2 - ca2*cb**2*complex(0,1)*l8*sa3*vevs - ca2*complex(0,1)*l7*sa3*sb**2*vevs',
                  order = {'QED':1})

GC_207 = Coupling(name = 'GC_207',
                  value = 'ca3*cb**2*complex(0,1)*l1*sa1*vev1 + ca1*cb**2*complex(0,1)*l1*sa2*sa3*vev1 - ca1*ca3*cb*complex(0,1)*l4*sb*vev1 - ca1*ca3*cb*complex(0,1)*l5*sb*vev1 + cb*complex(0,1)*l4*sa1*sa2*sa3*sb*vev1 + cb*complex(0,1)*l5*sa1*sa2*sa3*sb*vev1 + ca3*complex(0,1)*l3*sa1*sb**2*vev1 + ca1*complex(0,1)*l3*sa2*sa3*sb**2*vev1 - ca1*ca3*cb**2*complex(0,1)*l3*vev2 + cb**2*complex(0,1)*l3*sa1*sa2*sa3*vev2 + ca3*cb*complex(0,1)*l4*sa1*sb*vev2 + ca3*cb*complex(0,1)*l5*sa1*sb*vev2 + ca1*cb*complex(0,1)*l4*sa2*sa3*sb*vev2 + ca1*cb*complex(0,1)*l5*sa2*sa3*sb*vev2 - ca1*ca3*complex(0,1)*l2*sb**2*vev2 + complex(0,1)*l2*sa1*sa2*sa3*sb**2*vev2 - ca2*cb**2*complex(0,1)*l7*sa3*vevs - ca2*complex(0,1)*l8*sa3*sb**2*vevs',
                  order = {'QED':1})

GC_208 = Coupling(name = 'GC_208',
                  value = 'ca3*cb**2*complex(0,1)*l1*sa1*vev1 + ca1*cb**2*complex(0,1)*l1*sa2*sa3*vev1 - 2*ca1*ca3*cb*complex(0,1)*l5*sb*vev1 + 2*cb*complex(0,1)*l5*sa1*sa2*sa3*sb*vev1 + ca3*complex(0,1)*l3*sa1*sb**2*vev1 + ca3*complex(0,1)*l4*sa1*sb**2*vev1 - ca3*complex(0,1)*l5*sa1*sb**2*vev1 + ca1*complex(0,1)*l3*sa2*sa3*sb**2*vev1 + ca1*complex(0,1)*l4*sa2*sa3*sb**2*vev1 - ca1*complex(0,1)*l5*sa2*sa3*sb**2*vev1 - ca1*ca3*cb**2*complex(0,1)*l3*vev2 - ca1*ca3*cb**2*complex(0,1)*l4*vev2 + ca1*ca3*cb**2*complex(0,1)*l5*vev2 + cb**2*complex(0,1)*l3*sa1*sa2*sa3*vev2 + cb**2*complex(0,1)*l4*sa1*sa2*sa3*vev2 - cb**2*complex(0,1)*l5*sa1*sa2*sa3*vev2 + 2*ca3*cb*complex(0,1)*l5*sa1*sb*vev2 + 2*ca1*cb*complex(0,1)*l5*sa2*sa3*sb*vev2 - ca1*ca3*complex(0,1)*l2*sb**2*vev2 + complex(0,1)*l2*sa1*sa2*sa3*sb**2*vev2 - ca2*cb**2*complex(0,1)*l7*sa3*vevs - ca2*complex(0,1)*l8*sa3*sb**2*vevs',
                  order = {'QED':1})

GC_209 = Coupling(name = 'GC_209',
                  value = '-(complex(0,1)*sb*yc1) + cb*complex(0,1)*yc2',
                  order = {'QED':1})

GC_210 = Coupling(name = 'GC_210',
                  value = '(sb*yc1)/cmath.sqrt(2) - (cb*yc2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_211 = Coupling(name = 'GC_211',
                  value = '-((sb*yc1)/cmath.sqrt(2)) + (cb*yc2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_212 = Coupling(name = 'GC_212',
                  value = '-((ca1*ca2*complex(0,1)*yc1)/cmath.sqrt(2)) - (ca2*complex(0,1)*sa1*yc2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_213 = Coupling(name = 'GC_213',
                  value = '(ca1*ca3*complex(0,1)*sa2*yc1)/cmath.sqrt(2) - (complex(0,1)*sa1*sa3*yc1)/cmath.sqrt(2) + (ca3*complex(0,1)*sa1*sa2*yc2)/cmath.sqrt(2) + (ca1*complex(0,1)*sa3*yc2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_214 = Coupling(name = 'GC_214',
                  value = '(ca3*complex(0,1)*sa1*yc1)/cmath.sqrt(2) + (ca1*complex(0,1)*sa2*sa3*yc1)/cmath.sqrt(2) - (ca1*ca3*complex(0,1)*yc2)/cmath.sqrt(2) + (complex(0,1)*sa1*sa2*sa3*yc2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_215 = Coupling(name = 'GC_215',
                  value = 'cb*complex(0,1)*yc1 + complex(0,1)*sb*yc2',
                  order = {'QED':1})

GC_216 = Coupling(name = 'GC_216',
                  value = '-((cb*yc1)/cmath.sqrt(2)) - (sb*yc2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_217 = Coupling(name = 'GC_217',
                  value = '(cb*yc1)/cmath.sqrt(2) + (sb*yc2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_218 = Coupling(name = 'GC_218',
                  value = '-((ca2*complex(0,1)*sa1*yb2)/cmath.sqrt(2)) - (ca1*ca2*complex(0,1)*yd13x3)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_219 = Coupling(name = 'GC_219',
                  value = '-(complex(0,1)*sb*yb2) - cb*complex(0,1)*yd13x3',
                  order = {'QED':1})

GC_220 = Coupling(name = 'GC_220',
                  value = '-((sb*yb2)/cmath.sqrt(2)) - (cb*yd13x3)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_221 = Coupling(name = 'GC_221',
                  value = '(sb*yb2)/cmath.sqrt(2) + (cb*yd13x3)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_222 = Coupling(name = 'GC_222',
                  value = '(ca3*complex(0,1)*sa1*sa2*yb2)/cmath.sqrt(2) + (ca1*complex(0,1)*sa3*yb2)/cmath.sqrt(2) + (ca1*ca3*complex(0,1)*sa2*yd13x3)/cmath.sqrt(2) - (complex(0,1)*sa1*sa3*yd13x3)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_223 = Coupling(name = 'GC_223',
                  value = '-((ca1*ca3*complex(0,1)*yb2)/cmath.sqrt(2)) + (complex(0,1)*sa1*sa2*sa3*yb2)/cmath.sqrt(2) + (ca3*complex(0,1)*sa1*yd13x3)/cmath.sqrt(2) + (ca1*complex(0,1)*sa2*sa3*yd13x3)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_224 = Coupling(name = 'GC_224',
                  value = '-(cb*complex(0,1)*yb2) + complex(0,1)*sb*yd13x3',
                  order = {'QED':1})

GC_225 = Coupling(name = 'GC_225',
                  value = '(cb*yb2)/cmath.sqrt(2) - (sb*yd13x3)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_226 = Coupling(name = 'GC_226',
                  value = '-((cb*yb2)/cmath.sqrt(2)) + (sb*yd13x3)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_227 = Coupling(name = 'GC_227',
                  value = 'complex(0,1)*sb*ydo1 - cb*complex(0,1)*ydo2',
                  order = {'QED':1})

GC_228 = Coupling(name = 'GC_228',
                  value = '(sb*ydo1)/cmath.sqrt(2) - (cb*ydo2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_229 = Coupling(name = 'GC_229',
                  value = '-((sb*ydo1)/cmath.sqrt(2)) + (cb*ydo2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_230 = Coupling(name = 'GC_230',
                  value = '-((ca1*ca2*complex(0,1)*ydo1)/cmath.sqrt(2)) - (ca2*complex(0,1)*sa1*ydo2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_231 = Coupling(name = 'GC_231',
                  value = '(ca1*ca3*complex(0,1)*sa2*ydo1)/cmath.sqrt(2) - (complex(0,1)*sa1*sa3*ydo1)/cmath.sqrt(2) + (ca3*complex(0,1)*sa1*sa2*ydo2)/cmath.sqrt(2) + (ca1*complex(0,1)*sa3*ydo2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_232 = Coupling(name = 'GC_232',
                  value = '(ca3*complex(0,1)*sa1*ydo1)/cmath.sqrt(2) + (ca1*complex(0,1)*sa2*sa3*ydo1)/cmath.sqrt(2) - (ca1*ca3*complex(0,1)*ydo2)/cmath.sqrt(2) + (complex(0,1)*sa1*sa2*sa3*ydo2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_233 = Coupling(name = 'GC_233',
                  value = '-(cb*complex(0,1)*ydo1) - complex(0,1)*sb*ydo2',
                  order = {'QED':1})

GC_234 = Coupling(name = 'GC_234',
                  value = '-((cb*ydo1)/cmath.sqrt(2)) - (sb*ydo2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_235 = Coupling(name = 'GC_235',
                  value = '(cb*ydo1)/cmath.sqrt(2) + (sb*ydo2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_236 = Coupling(name = 'GC_236',
                  value = 'complex(0,1)*sb*ye1 - cb*complex(0,1)*ye2',
                  order = {'QED':1})

GC_237 = Coupling(name = 'GC_237',
                  value = '(sb*ye1)/cmath.sqrt(2) - (cb*ye2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_238 = Coupling(name = 'GC_238',
                  value = '-((sb*ye1)/cmath.sqrt(2)) + (cb*ye2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_239 = Coupling(name = 'GC_239',
                  value = '-((ca1*ca2*complex(0,1)*ye1)/cmath.sqrt(2)) - (ca2*complex(0,1)*sa1*ye2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_240 = Coupling(name = 'GC_240',
                  value = '(ca1*ca3*complex(0,1)*sa2*ye1)/cmath.sqrt(2) - (complex(0,1)*sa1*sa3*ye1)/cmath.sqrt(2) + (ca3*complex(0,1)*sa1*sa2*ye2)/cmath.sqrt(2) + (ca1*complex(0,1)*sa3*ye2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_241 = Coupling(name = 'GC_241',
                  value = '(ca3*complex(0,1)*sa1*ye1)/cmath.sqrt(2) + (ca1*complex(0,1)*sa2*sa3*ye1)/cmath.sqrt(2) - (ca1*ca3*complex(0,1)*ye2)/cmath.sqrt(2) + (complex(0,1)*sa1*sa2*sa3*ye2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_242 = Coupling(name = 'GC_242',
                  value = '-(cb*complex(0,1)*ye1) - complex(0,1)*sb*ye2',
                  order = {'QED':1})

GC_243 = Coupling(name = 'GC_243',
                  value = '-((cb*ye1)/cmath.sqrt(2)) - (sb*ye2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_244 = Coupling(name = 'GC_244',
                  value = '(cb*ye1)/cmath.sqrt(2) + (sb*ye2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_245 = Coupling(name = 'GC_245',
                  value = 'complex(0,1)*sb*ym1 - cb*complex(0,1)*ym2',
                  order = {'QED':1})

GC_246 = Coupling(name = 'GC_246',
                  value = '(sb*ym1)/cmath.sqrt(2) - (cb*ym2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_247 = Coupling(name = 'GC_247',
                  value = '-((sb*ym1)/cmath.sqrt(2)) + (cb*ym2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_248 = Coupling(name = 'GC_248',
                  value = '-((ca1*ca2*complex(0,1)*ym1)/cmath.sqrt(2)) - (ca2*complex(0,1)*sa1*ym2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_249 = Coupling(name = 'GC_249',
                  value = '(ca1*ca3*complex(0,1)*sa2*ym1)/cmath.sqrt(2) - (complex(0,1)*sa1*sa3*ym1)/cmath.sqrt(2) + (ca3*complex(0,1)*sa1*sa2*ym2)/cmath.sqrt(2) + (ca1*complex(0,1)*sa3*ym2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_250 = Coupling(name = 'GC_250',
                  value = '(ca3*complex(0,1)*sa1*ym1)/cmath.sqrt(2) + (ca1*complex(0,1)*sa2*sa3*ym1)/cmath.sqrt(2) - (ca1*ca3*complex(0,1)*ym2)/cmath.sqrt(2) + (complex(0,1)*sa1*sa2*sa3*ym2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_251 = Coupling(name = 'GC_251',
                  value = '-(cb*complex(0,1)*ym1) - complex(0,1)*sb*ym2',
                  order = {'QED':1})

GC_252 = Coupling(name = 'GC_252',
                  value = '-((cb*ym1)/cmath.sqrt(2)) - (sb*ym2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_253 = Coupling(name = 'GC_253',
                  value = '(cb*ym1)/cmath.sqrt(2) + (sb*ym2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_254 = Coupling(name = 'GC_254',
                  value = 'complex(0,1)*sb*yd12x2 - cb*complex(0,1)*ys2',
                  order = {'QED':1})

GC_255 = Coupling(name = 'GC_255',
                  value = '(sb*yd12x2)/cmath.sqrt(2) - (cb*ys2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_256 = Coupling(name = 'GC_256',
                  value = '-((sb*yd12x2)/cmath.sqrt(2)) + (cb*ys2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_257 = Coupling(name = 'GC_257',
                  value = '-((ca1*ca2*complex(0,1)*yd12x2)/cmath.sqrt(2)) - (ca2*complex(0,1)*sa1*ys2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_258 = Coupling(name = 'GC_258',
                  value = '(ca1*ca3*complex(0,1)*sa2*yd12x2)/cmath.sqrt(2) - (complex(0,1)*sa1*sa3*yd12x2)/cmath.sqrt(2) + (ca3*complex(0,1)*sa1*sa2*ys2)/cmath.sqrt(2) + (ca1*complex(0,1)*sa3*ys2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_259 = Coupling(name = 'GC_259',
                  value = '(ca3*complex(0,1)*sa1*yd12x2)/cmath.sqrt(2) + (ca1*complex(0,1)*sa2*sa3*yd12x2)/cmath.sqrt(2) - (ca1*ca3*complex(0,1)*ys2)/cmath.sqrt(2) + (complex(0,1)*sa1*sa2*sa3*ys2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_260 = Coupling(name = 'GC_260',
                  value = '-(cb*complex(0,1)*yd12x2) - complex(0,1)*sb*ys2',
                  order = {'QED':1})

GC_261 = Coupling(name = 'GC_261',
                  value = '-((cb*yd12x2)/cmath.sqrt(2)) - (sb*ys2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_262 = Coupling(name = 'GC_262',
                  value = '(cb*yd12x2)/cmath.sqrt(2) + (sb*ys2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_263 = Coupling(name = 'GC_263',
                  value = '-(complex(0,1)*sb*yt1) + cb*complex(0,1)*yt2',
                  order = {'QED':1})

GC_264 = Coupling(name = 'GC_264',
                  value = '(sb*yt1)/cmath.sqrt(2) - (cb*yt2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_265 = Coupling(name = 'GC_265',
                  value = '-((sb*yt1)/cmath.sqrt(2)) + (cb*yt2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_266 = Coupling(name = 'GC_266',
                  value = '-((ca1*ca2*complex(0,1)*yt1)/cmath.sqrt(2)) - (ca2*complex(0,1)*sa1*yt2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_267 = Coupling(name = 'GC_267',
                  value = '(ca1*ca3*complex(0,1)*sa2*yt1)/cmath.sqrt(2) - (complex(0,1)*sa1*sa3*yt1)/cmath.sqrt(2) + (ca3*complex(0,1)*sa1*sa2*yt2)/cmath.sqrt(2) + (ca1*complex(0,1)*sa3*yt2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_268 = Coupling(name = 'GC_268',
                  value = '(ca3*complex(0,1)*sa1*yt1)/cmath.sqrt(2) + (ca1*complex(0,1)*sa2*sa3*yt1)/cmath.sqrt(2) - (ca1*ca3*complex(0,1)*yt2)/cmath.sqrt(2) + (complex(0,1)*sa1*sa2*sa3*yt2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_269 = Coupling(name = 'GC_269',
                  value = 'cb*complex(0,1)*yt1 + complex(0,1)*sb*yt2',
                  order = {'QED':1})

GC_270 = Coupling(name = 'GC_270',
                  value = '-((cb*yt1)/cmath.sqrt(2)) - (sb*yt2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_271 = Coupling(name = 'GC_271',
                  value = '(cb*yt1)/cmath.sqrt(2) + (sb*yt2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_272 = Coupling(name = 'GC_272',
                  value = 'complex(0,1)*sb*ytau1 - cb*complex(0,1)*ytau2',
                  order = {'QED':1})

GC_273 = Coupling(name = 'GC_273',
                  value = '(sb*ytau1)/cmath.sqrt(2) - (cb*ytau2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_274 = Coupling(name = 'GC_274',
                  value = '-((sb*ytau1)/cmath.sqrt(2)) + (cb*ytau2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_275 = Coupling(name = 'GC_275',
                  value = '-((ca1*ca2*complex(0,1)*ytau1)/cmath.sqrt(2)) - (ca2*complex(0,1)*sa1*ytau2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_276 = Coupling(name = 'GC_276',
                  value = '(ca1*ca3*complex(0,1)*sa2*ytau1)/cmath.sqrt(2) - (complex(0,1)*sa1*sa3*ytau1)/cmath.sqrt(2) + (ca3*complex(0,1)*sa1*sa2*ytau2)/cmath.sqrt(2) + (ca1*complex(0,1)*sa3*ytau2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_277 = Coupling(name = 'GC_277',
                  value = '(ca3*complex(0,1)*sa1*ytau1)/cmath.sqrt(2) + (ca1*complex(0,1)*sa2*sa3*ytau1)/cmath.sqrt(2) - (ca1*ca3*complex(0,1)*ytau2)/cmath.sqrt(2) + (complex(0,1)*sa1*sa2*sa3*ytau2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_278 = Coupling(name = 'GC_278',
                  value = '-(cb*complex(0,1)*ytau1) - complex(0,1)*sb*ytau2',
                  order = {'QED':1})

GC_279 = Coupling(name = 'GC_279',
                  value = '-((cb*ytau1)/cmath.sqrt(2)) - (sb*ytau2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_280 = Coupling(name = 'GC_280',
                  value = '(cb*ytau1)/cmath.sqrt(2) + (sb*ytau2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_281 = Coupling(name = 'GC_281',
                  value = '-(complex(0,1)*sb*yup1) + cb*complex(0,1)*yup2',
                  order = {'QED':1})

GC_282 = Coupling(name = 'GC_282',
                  value = '(sb*yup1)/cmath.sqrt(2) - (cb*yup2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_283 = Coupling(name = 'GC_283',
                  value = '-((sb*yup1)/cmath.sqrt(2)) + (cb*yup2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_284 = Coupling(name = 'GC_284',
                  value = '-((ca1*ca2*complex(0,1)*yup1)/cmath.sqrt(2)) - (ca2*complex(0,1)*sa1*yup2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_285 = Coupling(name = 'GC_285',
                  value = '(ca1*ca3*complex(0,1)*sa2*yup1)/cmath.sqrt(2) - (complex(0,1)*sa1*sa3*yup1)/cmath.sqrt(2) + (ca3*complex(0,1)*sa1*sa2*yup2)/cmath.sqrt(2) + (ca1*complex(0,1)*sa3*yup2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_286 = Coupling(name = 'GC_286',
                  value = '(ca3*complex(0,1)*sa1*yup1)/cmath.sqrt(2) + (ca1*complex(0,1)*sa2*sa3*yup1)/cmath.sqrt(2) - (ca1*ca3*complex(0,1)*yup2)/cmath.sqrt(2) + (complex(0,1)*sa1*sa2*sa3*yup2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_287 = Coupling(name = 'GC_287',
                  value = 'cb*complex(0,1)*yup1 + complex(0,1)*sb*yup2',
                  order = {'QED':1})

GC_288 = Coupling(name = 'GC_288',
                  value = '-((cb*yup1)/cmath.sqrt(2)) - (sb*yup2)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_289 = Coupling(name = 'GC_289',
                  value = '(cb*yup1)/cmath.sqrt(2) + (sb*yup2)/cmath.sqrt(2)',
                  order = {'QED':1})

