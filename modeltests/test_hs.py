# -*- coding: utf-8 -*-
###############################################################################
#                                 test_hs.py                                  #
###############################################################################

##############
#  Includes  #
##############

import rept1l.modeltests.HS_tests.hs_setup as hs
from rept1l.modeltests.modeltest_utils.auto_model import GenModel


#############
#  Methods  #
#############

class GenHS(GenModel):
  threads = 8
  ct_expansion = True
  pre_ct = 'hsexp.txt'
  verbose = True

  def renormalize_model(self, **kwargs):
    # Higgs tadpole + fields
    self.renormalize_higgs_singlet('-o', 'tadpoles', 'neutral', **kwargs)

    # QCD charged fields
    self.renormalize_qcd('-o', 'particles', **kwargs)

    # QCD coupling
    for i in ['6', '5', '4', '3']:
      self.renormalize_qcd('-o', 'alphaS', '-nf', i, **kwargs)

    # Pure EW fields + EW couplings
    for p in ['particles', 'alpha0', 'Gf', 'alphaZ']:
      self.renormalize_gsw('-o', p, **kwargs)

    # msbar reno mixing (this order is essential)
    self.renormalize_higgs_singlet('-o', 'alpha', 'l3', **kwargs)

    # reno mixing additional schemes
    self.renormalize_higgs_singlet('-o', 'alphaALT', 'l3ALT', **kwargs)
    self.register_ct()

  def compute_rational(self, **kwargs):
    cls = self.__class__
    self.run_r2(vertices_file='hsr2.txt', nc=True, threads=cls.threads)

#------------------------------------------------------------------------------#

def full_hs_model(outputpath):
  GenHS.driverpath = hs.driver_hs_path
  GenHS.outputpath = outputpath
  GenHS(odp=True, use_run_ct=True)

#------------------------------------------------------------------------------#

def full_hs_BFM_model(outputpath):
  GenHS.driverpath = hs.driver_hs_bfm_path
  GenHS.outputpath = outputpath
  GenHS(odp=True, bfm=True, use_run_ct=True)
