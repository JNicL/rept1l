# -*- coding: utf-8 -*-
#==============================================================================#
#                                test_models.py                                #
#==============================================================================#

from __future__ import print_function

#============#
#  Includes  #
#============#

import os
from rept1l.modeltests.modeltest_utils.auto_model import GenModel, GenBareModel
import rept1l.modeltests.SMEFT_tests.smeft_setup as smdc


class GenSMEFT(GenModel):
  threads = 8
  ct_expansion = True
  pre_ct = 'smexp.txt'
  pure_qcd=True
  verbose = True

  def renormalize_model(self, **kwargs):
    pureqcd = ('-qcd', '0', '1', '2', '3', '-qed', '0', '-lam', '0')
    # QCD charged fields
    self.renormalize_qcd('-o', 'particles', *pureqcd, **kwargs)

    # QCD coupling
    for i in ['6', '5', '4', '3']:
      self.renormalize_qcd('-o', 'alphaS', '-nf', i, *pureqcd, **kwargs)

    self.register_ct()

  def compute_rational(self, **kwargs):
    cls = self.__class__

    self.run_r2(vertices_file='smr2.txt', nc=True, np=2, threads=cls.threads,
                orders={'qcd': [2], 'qed': [0], 'lam': [0]})
    self.run_r2(vertices_file='smr2.txt', nc=True, np=3, threads=cls.threads,
                orders={'qcd': [2,3], 'qed': [0,1], 'lam': [0]})
    self.run_r2(vertices_file='smr2.txt', nc=True, np=4, threads=cls.threads,
                orders={'qcd': [2,3,4], 'qed': [0,1,2], 'lam': [0]})
    self.run_r2(vertices_file='r2list_4f_qqll.txt', nc=True, np=4,
                threads=cls.threads, npc=True,
                orders={'qcd': [2], 'qed': [2], 'lam': [1]})


class GenSMEFTR2(GenModel):
  threads = 8
  ct_expansion = False
  verbose = True

  def renormalize_model(self, **kwargs):
    pass

  def compute_rational(self, **kwargs):
    cls = self.__class__

    self.run_r2(particles_list='u~ u d d~ c c~ s s~ t t~ b b~ e+ e- mu+ mu- tau+ tau- nu_e nu_e~ nu_mu nu_mu~ nu_tau nu_tau~',
                npc=True, np=4, orders={'qcd': [2], 'qed': [2], 'lam': [1]},
                threads=cls.threads)


def smeft_fourfermions(outputpath):
  mdlname = "SMEFT (QCD)"
  GenSMEFT.driverpath = smdc.driver_smeft_fourfermions_path
  GenSMEFT.outputpath = outputpath
  GenSMEFT(odp=True, use_run_ct=True)

def smeft_fourfermions_R2(outputpath):
  mdlname = "SMEFT (QCD)"
  GenSMEFTR2.driverpath = smdc.driver_smeft_fourfermions_path
  GenSMEFTR2.outputpath = outputpath
  GenSMEFTR2(odp=True, use_run_ct=True)

if __name__ == "__main__":
  smeft_fourfermions('/home/nick/smeft_4f_2.2.3')
  # smeft_fourfermions_R2('/home/nick/smeft_4f_R2_2.2.3')
