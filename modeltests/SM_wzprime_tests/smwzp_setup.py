################################################################################
#                                smwzp_setup.py                                #
################################################################################

##############
#  Includes  #
##############

import os

#######################
#  Globals & Methods  #
#######################

thispath = os.path.dirname(os.path.realpath(__file__))
driver_smwzp_path = os.path.join(thispath, 'SM_wzprime_drivers')
