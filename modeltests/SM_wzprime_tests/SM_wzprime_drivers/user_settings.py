#==============================================================================#
#                                   model.py                                   #
#==============================================================================#

#============#
#  Includes  #
#============#

import rept1l.autoct.lorentz_ct as LCT
from rept1l.autoct.modelfile import model
from rept1l.autoct.modelfile import TreeInducedCTVertex
from rept1l.autoct.auto_parameters_ct import auto_assign_ct_particle
from rept1l.autoct.auto_parameters_ct import set_light_particle
from rept1l.autoct.auto_parameters_ct import assign_counterterm

#===========#
#  Globals  #
#===========#

# coupling constructor
Coupling = model.object_library.Coupling

# access to instances
param = model.parameters
P = model.particles
L = model.lorentz

# LCT allows to access the 2-point lorentz structures defined in lorentz_ct.py
modelname = "SM W'Z'"
modelgauge = "'t Hooft-Feynman"

features = {'fermionloop_opt': False,
            'qcd_rescaling': True,
            }

#=============================#
#  External Parameter Orders  #
#=============================#

parameter_orders = {param.aEW: {'QED': 2},
                    param.aS: {'QCD': 2},
                    param.kZL: {'WZP': 1},
                    param.kZR: {'WZP': 1},
                    param.kWL: {'WZP': 1},
                    param.kWR: {'WZP': 1},
                    }

#===============================#
#  Fermion mass regularization  #
#===============================#

lquarks = [P.u, P.d, P.c, P.s, P.b]
for p in lquarks:
  set_light_particle(p)


###################################
#  SM Optimizations for Fermions  #
###################################

doublets = []

generations = []

##############################
#  coupling renormalization  #
##############################

assign_counterterm(param.gs, 'dZgs', 'gs*dZgs')

mixings = {}
selection = [P.g, P.t] + lquarks
auto_assign_ct_particle(mixings, selection=selection)

# here we declare the weak gauge bosons to be background fields to avoid having
# them inside loops

P.W__plus__.backgroundfield = True
P.ghWp.backgroundfield = True
P.ghWp__tilde__.backgroundfield = True
P.G__plus__.backgroundfield = True
P.W__minus__.backgroundfield = True
P.ghWm.backgroundfield = True
P.ghWm__tilde__.backgroundfield = True
P.G__minus__.backgroundfield = True
P.A.backgroundfield = True
P.ghA.backgroundfield = True
P.Z.backgroundfield = True
P.ghZ.backgroundfield = True
P.G0.backgroundfield = True
P.H.backgroundfield = True

P.Wp__plus__.backgroundfield = True
P.Gp__plus__.backgroundfield = True
P.Wp__minus__.backgroundfield = True
P.Gp__minus__.backgroundfield = True
P.Zp.backgroundfield = True
P.Gp0.backgroundfield = True
