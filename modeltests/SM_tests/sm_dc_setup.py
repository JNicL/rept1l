#==============================================================================#
#                                sm_dc_setup.py                                #
#==============================================================================#

#============#
#  Includes  #
#============#

import os

#=====================#
#  Globals & Methods  #
#=====================#

driver_sm_dc_path = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                                 'SM_DC_drivers')
driver_sm_dc_nf5_path = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                                 'SM_DC_NF5_drivers')
driver_sm_dc_nf4_path = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                                 'SM_DC_NF4_drivers')
driver_sm_dc_NT_path = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                                    'SM_drivers')
driver_smbfm_dc_path = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                                    'SM_BFM_DC_drivers')
driver_sm_dc_ckm_path = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                                     'SM_DC_CKM_drivers')
driver_smferm_path = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                                     'SM_DC_fermloops_drivers')
driver_smferm_yt_path = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                                     'SM_DC_fermloops_yt_drivers')
driver_smhpc_path = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                                 'SM_DC_HPC_drivers')

params = {'MW': '80.370881514409731d0',
          'WW': '0d0',
          'MZ': '91.153480619182758d0',
          'WZ': '0d0',
          'MH': '126d0',
          'WH': '0d0',
          'MT': '172.5d0',
          'WT': '0d0',
          'MB': '4.3d0',
          'WB': '0d0',
          'MC': 'massreg',
          'WC': '0d0',
          'MS': 'massreg',
          'MU': 'massreg',
          'MD': 'massreg',
          'ME': 'massreg',
          'MM': 'massreg',
          'WM': '0d0',
          'MTA': 'massreg',
          'WTA': '0d0',
          'ymtau': '0d0',
          'aEW': '7.5492131512809261d-003',
          'aS': '0.10530431206662641d0'}

params_pa = {'MW': 80.39800,
             'WW': 2.0907035513551233,
             'MZ': 91.18760,
             'WZ': 2.4982398858455146,
             'MH': 125.,
             'WH': 0.0,
             'MT': 172.5,
             'WT': 0.0,
             'MB': 1e-3,
             'WB': 0.0,
             'MC': 1e-3,
             'WC': 0.0,
             'MS': 1e-3,
             'MU': 1e-3,
             'MD': 1e-3,
             'Me': 1e-3,
             'ME': 1e-3,
             'MM': 1e-3,
             'WM': 0.0,
             'MTA': 1e-3,
             'WTA': 0.0,
             'aEW': 7.5562543385512175e-003,
             'aS': 0.10530431206662641,
             'muUV': 80.39800,
             'muIR': 80.39800,
             'muMS': 80.39800}

params_li = {'MW': 80.385,
             'WW': 2.0850,
             'MZ': 91.18760,
             'WZ': 2.4952,
             'MH': 125.,
             'WH': 0.0,
             'MT': 173.21,
             'WT': 2.0,
             'MB': 0.,
             'WB': 0.,
             'MC': 0.,
             'WC': 0.,
             'MS': 0.,
             'MU': 0.,
             'MD': 0.,
             'ME': 0.,
             'MM': 0.,
             'WM': 0.,
             'MTA': 0.,
             'WTA': 0.,
             'aEW': 0.75624689019848E-02,
             'aS': 0.113307297485655,
             'muUV': 100.,
             'muIR': 100.,
             'muMS': 100.}

params_tt = {'MW': 80.399,
             'WW': 2.0997360974,
             'MZ': 91.18760,
             'WZ': 2.5096596343,
             'MH': 120.,
             'WH': 0.0,
             'MT': 172.,
             'WT': 0.,
             'MB': 0.,
             'WB': 0.,
             'MC': 0.,
             'WC': 0.,
             'MS': 0.,
             'MU': 0.,
             'MD': 0.,
             'MM': 0.,
             'WM': 0.,
             'MTA': 0.,
             'WTA': 0.,
             'aEW': 0.007555786000756913,
             'aS': 1.0951935838820E-01,
             'muUV': 172.,
             'muIR': 172.,
             'muMS': 172.}

params_tt_EW = {'MW': 80.357973609878,
                'WW': 2.0842989982782,
                'MZ': 91.153480619183,
                'WZ': 2.4942663787728,
                'MH': 125.9,
                'WH': 0.0,
                'MT': 173.34000000000,
                'WT': 1.3691800000000,
                'MB': 0.,
                'WB': 0.,
                'MC': 0.,
                'WC': 0.,
                'MS': 0.,
                'MU': 0.,
                'MD': 0.,
                'ME': 0.,
                'MM': 0.,
                'WM': 0.,
                'MTA': 0.,
                'WTA': 0.,
                'aEW': 0.75553105223690E-02,
                'aS': 0.10846561579971,
                'muUV': 173.34000000000,
                'muIR': 173.34000000000,
                'muMS': 173.34000000000}

params_alphas_running = {
          'MW': 80.399,
          'WW': 2.0997360974,
          'MZ': 91.1876,
          'WZ': 2.5096596343,
          'MH': 120.,
          'WH': 0.,
          'MT': 172.,
          'WT': 1.3167,
          'MB': 0.,
          'WB': 0.,
          'MC': 0.,
          'WC': 0.,
          'MS': 0.,
          'MU': 0.,
          'MD': 0.,
          'ME': 0.,
          'MM': 0.,
          'WM': 0.,
          'MTA': 0.,
          'WTA': 0.,
          'aEW': 0.75557860007569E-02,
          'aS': 0.10951935838820116,
          'muUV': 172.,
          'muMS': 172.,
          'muIR': 172.
          }
