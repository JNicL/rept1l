#==============================================================================#
#                             test_running_alphas                              #
#==============================================================================#

from __future__ import print_function

#============#
#  Includes  #
#============#

import sys
import os
import pyrecola

from rept1l.modeltests.modeltest_utils.model_tests import test_amplitude_squared
from rept1l.modeltests.modeltest_utils.model_tests import test_real_values
from rept1l.modeltests.modeltest_utils.model_tests import fail, success
from rept1l.modeltests.modeltest_utils.model_tests import warning
import rept1l.modeltests.modeltest_utils.setup_models as sm
import rept1l.modeltests.SM_tests.sm_dc_setup as smdc

#===========#
#  Methods  #
#===========#

def set_parameter_QCD():
  """ Parameters which are used for the QCD correction comparison """
  params = smdc.params_alphas_running.copy()
  scales = {'muUV': pyrecola.set_mu_uv_rcl,
            'muIR': pyrecola.set_mu_ir_rcl,
            'muMS': pyrecola.set_mu_ms_rcl}
  for scale, clb in scales.iteritems():
    if scale in params:
      clb(params[scale])
      del params[scale]

  for p, val in params.iteritems():
    pyrecola.set_parameter_rcl(p, complex(val, 0.))

  pyrecola.set_qcd_rescaling_rcl(True)
  pyrecola.set_renoscheme_rcl('dZgs_QCD2', 'Nf5')

def test_running_QCD(fix_dmt=True):
  """ Testing the running of alpha s for different processes at the same time.
  Comparison against expected results produced without using rescaling methods.
  """
  set_parameter_QCD()

  pr1 = 'g g -> e+ nu_e b mu- nu_mu~ b~'
  pyrecola.define_process_rcl(3, pr1, 'NLO')
  pyrecola.unselect_all_powers_BornAmpl_rcl(3)
  pyrecola.unselect_all_powers_LoopAmpl_rcl(3)
  pyrecola.select_power_BornAmpl_rcl(3, 'QCD', 2)
  pyrecola.select_power_BornAmpl_rcl(3, 'QED', 4)
  pyrecola.select_power_LoopAmpl_rcl(3, 'QCD', 4)
  pyrecola.select_power_LoopAmpl_rcl(3, 'QED', 4)

  pr2 = 'u u~ -> e+ nu_e b mu- nu_mu~ b~'
  pyrecola.define_process_rcl(2, pr2, 'NLO')
  pyrecola.unselect_all_powers_BornAmpl_rcl(2)
  pyrecola.unselect_all_powers_LoopAmpl_rcl(2)
  pyrecola.select_power_BornAmpl_rcl(2, 'QCD', 2)
  pyrecola.select_power_BornAmpl_rcl(2, 'QED', 4)
  pyrecola.select_power_LoopAmpl_rcl(2, 'QCD', 4)
  pyrecola.select_power_LoopAmpl_rcl(2, 'QED', 4)

  pyrecola.generate_processes_rcl()

  # PS 1
  p1 = [2552.2043850989025, 0.0000000000000000,
        0.0000000000000000, 2552.2043850989025]
  p2 = [746.53281912806312, 0.0000000000000000,
        0.0000000000000000, -746.53281912806312]
  p3 = [1533.5211419729876, -811.18217451429689,
        617.87341191624591, 1145.3833504444572]
  p4 = [43.084900636761027, -16.709665581735749,
        28.469642130998182, 27.687094754759300]
  p5 = [585.65931269464352, 371.27192082832147,
        -70.005659466023857, 447.49659104080104]
  p6 = [396.83644608707078, 313.64751648170039,
        -149.47157420083363, 191.73588306262036]
  p7 = [391.27494644127233, 255.32705173010851,
        -192.59071604957006, 225.41738277902368]
  p8 = [348.36045639423031, -112.35464894409770,
        -234.27510433081653, -232.04873611082223]

  mom1 = [p1, p2, p3, p4, p5, p6, p7, p8]

  #p1 = [2608.3880246587250, 0.0000000000000000,
        #0.0000000000000000, 2608.3880246587250]
  #p2 = [559.11731193727258, 0.0000000000000000,
        #0.0000000000000000, -559.11731193727258]
  #p3 = [316.19249306884444, 147.97006227848354,
        #5.9392285559070235, -279.36943087344690]
  #p4 = [136.15419238020931, 106.49741441287125,
        #19.187840142262303, -82.632267406596043]
  #p5 = [590.93169914146551, 239.84807002569519,
        #360.94126865992291, 401.73943910616259]
  #p6 = [545.10599029029140, -98.347889643049058,
        #-99.072411851663077, 526.92778486526254]
  #p7 = [1534.7050912472473, -415.25838280771728,
        #-258.32826855433979, 1454.6981467898618]
  #p8 = [44.415870467939435, 19.290725733716357,
        #-28.667656952089374, 27.907040240208744]

  #mom2 = [p1, p2, p3, p4, p5, p6, p7, p8]

  total_tests = 0
  tests_succeeded = 0

  print("First run with pure computing")

  # The difference between R1 and R2 coms from the mass counterterm of the top
  # for finite widths. In R2 an expansion is performed for the CMS wheras in R1
  # the exact result is used.
  # R2 expec: -1.1071967755160840E-015
             #-1.1071967755156282E-015
             #-1.1071967755160840E-015
  # Compute process 1 at scale 1
  val = -1.1071967755156282E-015 if fix_dmt else -1.1072075442828443E-015
  expec = {'NLO': [{'value': val, 'power': [6, 8]}],
           'prid': pr1, 'Momenta': mom1}
  expec = [expec]
  res = test_amplitude_squared(3, expec, err_digits=3, warning_digits=4)
  total_tests += len(res)
  tests_succeeded += res.values().count(True)

  # Compute process 2 at scale 2
  pyrecola.set_alphas_rcl(9.9878539266795352E-2, 344.0, 5)
  val = 6.5152283823803032E-017 if fix_dmt else 6.5153116642678234E-017
  expec = {'NLO': [{'value': val, 'power': [6, 8]}],
           'prid': pr2, 'Momenta': mom1}
  expec = [expec]
  res = test_amplitude_squared(2, expec, err_digits=3, warning_digits=4)
  total_tests += len(res)
  tests_succeeded += res.values().count(True)

  # Compute process 1 at scale 3
  pyrecola.set_alphas_rcl(0.12127374537095613, 86.0, 5)
  val = -2.0082796453907966E-015 if fix_dmt else -2.0082942669500174E-015
  expec = {'NLO': [{'value': val, 'power': [6, 8]}],
           'prid': pr1, 'Momenta': mom1}
  expec = [expec]
  res = test_amplitude_squared(3, expec, err_digits=3, warning_digits=4)
  total_tests += len(res)
  tests_succeeded += res.values().count(True)

  # Compute process 2 at scale 2
  pyrecola.set_alphas_rcl(0.10857784621672924, 172., 5)
  val = -7.4855559846388887E-018 if fix_dmt else -7.4844860489330601E-018
  expec = {'NLO': [{'value': val, 'power': [6, 8]}],
           'prid': pr2, 'Momenta': mom1}
  expec = [expec]
  res = test_amplitude_squared(2, expec, err_digits=3, warning_digits=4)
  total_tests += len(res)
  tests_succeeded += res.values().count(True)

  # Compute process 2 at scale 2, again
  pyrecola.set_alphas_rcl(0.10857784621672924, 172., 5)
  val = -7.4855559841250752E-018 if fix_dmt else -7.4844860484192466E-018
  expec = {'NLO': [{'value': val, 'power': [6, 8]}],
           'prid': pr2, 'Momenta': mom1}
  expec = [expec]
  res = test_amplitude_squared(2, expec, err_digits=3, warning_digits=4)
  total_tests += len(res)
  tests_succeeded += res.values().count(True)

  print("Second run with mixed rescaling and computing")

  # Compute process 1 at scale 1
  pyrecola.set_alphas_rcl(0.10951935838820116, 172., 5)
  val = -1.1071967755155769E-015 if fix_dmt else -1.1072075442827929E-015
  expec = {'NLO': [{'value': val, 'power': [6, 8]}],
           'prid': pr1, 'Momenta': mom1}
  expec = [expec]
  res = test_amplitude_squared(3, expec, err_digits=3, warning_digits=4)
  total_tests += len(res)
  tests_succeeded += res.values().count(True)

  # Compute process 2 at scale 2
  pyrecola.set_alphas_rcl(9.9878539266795352E-2, 344., 5)
  val = 6.5152283824202985E-017 if fix_dmt else 6.5153116643078175E-017
  expec = {'NLO': [{'value': val, 'power': [6, 8]}],
           'prid': pr2, 'Momenta': mom1}
  expec = [expec]
  res = test_amplitude_squared(2, expec, err_digits=3, warning_digits=4)
  total_tests += len(res)
  tests_succeeded += res.values().count(True)

  # Rescale process 1 at scale 3
  pyrecola.set_alphas_rcl(0.12127374537095613, 86., 5)
  val = -2.0082796453907252E-015 if fix_dmt else -2.0082942669499468E-015
  expec = {'NLO': val}
  res = pyrecola.rescale_process_rcl(3, 'NLO')
  calc = {'NLO': res[1]}
  res = {str(pr1) + '@NLO': test_real_values(expec, calc, err_digits=3,
                                             warning_digits=4)}
  total_tests += len(res)
  tests_succeeded += res.values().count(True)

  # Rescale process 2 at scale 2
  pyrecola.set_alphas_rcl(0.10857784621672924, 172., 5)
  val = -7.4855559841250675E-018 if fix_dmt else -7.4844860484192435E-018
  expec = {'NLO': val}
  res = pyrecola.rescale_process_rcl(2, 'NLO')
  calc = {'NLO': res[1]}
  res = {str(pr1) + '@NLO': test_real_values(expec, calc, err_digits=3,
                                             warning_digits=4)}
  total_tests += len(res)
  tests_succeeded += res.values().count(True)

  # Compute process 2 at scale 2, again
  pyrecola.set_alphas_rcl(0.10857784621672924, 172., 5)
  val = -7.4855559841250752E-018 if fix_dmt else -7.4844860484192466E-018
  expec = {'NLO': [{'value': val, 'power': [6, 8]}],
           'prid': pr2, 'Momenta': mom1}
  expec = [expec]
  res = test_amplitude_squared(2, expec, err_digits=3, warning_digits=4)
  total_tests += len(res)
  tests_succeeded += res.values().count(True)

if __name__ == '__main__':
  test_running_QCD()
