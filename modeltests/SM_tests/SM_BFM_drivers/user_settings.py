#==============================================================================#
#                                   model.py                                   #
#==============================================================================#

#============#
#  Includes  #
#============#

import os
from rept1l.autoct.modelfile import model, CTParameter
from rept1l.autoct.modelfile import TreeInducedCTVertex, CTVertex
from rept1l.autoct.auto_parameters_ct import auto_assign_ct_particle
from rept1l.autoct.auto_parameters_ct import set_light_particle
from rept1l.autoct.auto_parameters_ct import assign_counterterm
from rept1l.autoct.auto_tadpole_ct import assign_FJTadpole_field_shift
from rept1l.autoct.auto_tadpole_ct import auto_tadpole_vertices

#===========#
#  Globals  #
#===========#

# coupling constructor
Coupling = model.object_library.Coupling

# access to instances
param = model.parameters
P = model.particles
L = model.lorentz

# LCT allows to access the 2-point lorentz structures defined in lorentz_ct.py
import rept1l.autoct.lorentz_ct as LCT

modelname = 'SM'
modelgauge = "'t Hooft-Feynman BFM"

features = {'fermionloop_opt' : True,
            'qcd_rescaling' : True,
            'sm_generation_opt': True,
           }

#=============================#
#  External Parameter Orders  #
#=============================#

# In the current version of UFO, parameter have no order assigned in fundamental
# couplings (e.g. power of QED, QCD). Rept1l requires that information
# which can be passed by filling `parameter_orders`:

# Example:
parameter_orders = {param.aEW: {'QED': 2},
                    param.aS: {'QCD': 2}}

#===============================#
#  Fermion mass regularization  #
#===============================#

for p in [P.e__minus__, P.mu__minus__, P.tau__minus__, P.u, P.d, P.c, P.s, P.b]:
  set_light_particle(p)

###################################
#  SM Optimizations for Fermions  #
###################################

doublets = [(P.nu_e, P.e__minus__), (P.nu_mu, P.mu__minus__),
            (P.nu_tau, P.tau__minus__), (P.u, P.d), (P.c, P.s), (P.t, P.b)]

generations = [(P.e__minus__, P.mu__minus__, P.tau__minus__),
               (P.d, P.s, P.b)]

#============================================#
#  Declare quantumfield & background fields  #
#============================================#

P.A.backgroundfield = True
P.AQ.quantumfield = True
P.Z.backgroundfield = True
P.ZQ.quantumfield = True
P.W__plus__.backgroundfield = True
P.WQ__plus__.quantumfield = True
P.W__minus__.backgroundfield = True
P.WQ__minus__.quantumfield = True
P.g.backgroundfield = True
P.gQ.quantumfield = True
P.H.backgroundfield = True
P.HQ.quantumfield = True
P.G0.backgroundfield = True
P.GQ0.quantumfield = True
P.G__plus__.backgroundfield = True
P.GQ__plus__.quantumfield = True
P.G__minus__.backgroundfield = True
P.GQ__minus__.quantumfield = True

#==============================================================================#
#                           coupling renormalization                           #
#==============================================================================#

#=================#
#  SM parameters  #
#=================#

assign_counterterm(param.ee, 'dZee', 'ee*dZee')
assign_counterterm(param.gs, 'dZgs', 'gs*dZgs')

#==============================#
#  Declare tadpole parameters  #
#==============================#

# the tadpole parameter are useful to declare parameter which are zero at
# Tree-Level, but have a one-loop expansion. Can be used not only for tadpoles
# Interally, the alogrithm then knows that it is safe to set the values to zero
# after renormalization, which optimizes ct expressions.

assign_FJTadpole_field_shift(P.H, 'dt')
auto_tadpole_vertices()

#==============================================================================#
#                                  particles                                   #
#==============================================================================#

# Automatic assignment of mass-counterterms and wavefunction. Possible mixing
# have to be passed to the method.
mixings = {P.A: [P.Z], P.Z: [P.A]}
auto_assign_ct_particle(mixings)

#########################################
#  Renormalization of the Gauge-fixing  #
#########################################

GC_MZ_GF = Coupling(name='GC_MZ_GF',
                    value='MZ',
                    order={'QED': 0})

GC_MW_GF = Coupling(name='GC_MW_GF',
                    value='I*MW',
                    order={'QED': 0})

GC_MMW_GF = Coupling(name='GC_MMW_GF',
                     value='-I*MW',
                     order={'QED': 0})

V_WPGM = TreeInducedCTVertex(name='V_WPGM',
                             particles=[P.W__plus__, P.G__minus__],
                             color=['1'],
                             type='treeinducedct',
                             loop_particles='N',
                             lorentz=[LCT.VS1],
                             couplings={(0, 0): GC_MW_GF})

V_WMGP = TreeInducedCTVertex(name='V_WMGP',
                             particles=[P.W__minus__, P.G__plus__],
                             color=['1'],
                             type='treeinducedct',
                             loop_particles='N',
                             lorentz=[LCT.VS1],
                             couplings={(0, 0): GC_MMW_GF})

V_ZG0 = TreeInducedCTVertex(name='V_ZG0',
                            particles=[P.Z, P.G0],
                            color=['1'],
                            type='treeinducedct',
                            loop_particles='N',
                            lorentz=[LCT.VS1],
                            couplings={(0, 0): GC_MZ_GF})
