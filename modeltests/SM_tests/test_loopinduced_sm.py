#==============================================================================#
#                            test_poleapprox_sm.py                             #
#==============================================================================#

from __future__ import print_function

#############
#  Imports  #
#############

import sys
import pyrecola

import rept1l.modeltests.modeltest_utils.setup_models as sm
import rept1l.modeltests.SM_tests.sm_dc_setup as smdc
from rept1l.modeltests.modeltest_utils.model_tests import test_amplitude_squared
from rept1l.modeltests.modeltest_utils.model_tests import fail
from rept1l.modeltests.modeltest_utils.model_tests import success
from rept1l.modeltests.modeltest_utils.model_tests import warning

#############
#  Methods  #
#############

def set_parameter():
  params = smdc.params_li.copy()
  scales = {'muUV': pyrecola.set_mu_uv_rcl,
            'muIR': pyrecola.set_mu_ir_rcl,
            'muMS': pyrecola.set_mu_ms_rcl}
  for scale, clb in scales.iteritems():
    if scale in params:
      clb(params[scale])
      del params[scale]

  for p, val in params.iteritems():
    pyrecola.set_parameter_rcl(p, complex(val, 0.))

def test_loopinduced_sm(only_D4=True):
  """ Testing loop-induced processes versus RECOLA 1.0 (which were tested
  against and MadGraph). """

  set_parameter()

  # 1 and 4 were computed with finite widths for Z, W. 2, 3 for zero width..
  pr = 'g g -> A A'
  pyrecola.define_process_rcl(1, pr, 'NLO')
  pr = 'g g -> H Z'
  pyrecola.define_process_rcl(2, pr, 'NLO')
  pr = 'g g -> W- W+'
  pyrecola.define_process_rcl(3, pr, 'NLO')
  pr = 'g g -> H H'
  pyrecola.define_process_rcl(4, pr, 'NLO')
  pyrecola.generate_processes_rcl()

  total_tests = 0
  tests_succeeded = 0

  pr = 'g g -> A A'
  #Rec       0.11725269945430E-04
  #MG        0.11725269942576842E-004
  #OpenLoops 0.117366371922613e-04
  p1 = [24.635006981,    0.000000000,    0.000000000,   24.635006981]
  p2 = [169.432431753,    0.000000000,    0.000000000, -169.432431753]
  p3 = [100.853923864,   47.917928231,  -43.199766560,  -77.518812439]
  p4 = [93.213514870,  -47.917928231,   43.199766560,  -67.278612333]
  mom1 = [p1, p2, p3, p4]

  #Rec 0.24980473161980E-04
  #MG  0.24980533724344549E-004
  #OpenLoops 2.4832178671047e-05
  p1 = [3837.768162701,  0.000000000,   0.000000000, 3837.768162701]
  p2 = [0.080506622,  0.000000000,   0.000000000,   -0.080506622]
  p3 = [560.746167647, -2.133569274, -12.232049589,  560.608677578]
  p4 = [3277.102501675,  2.133569274,  12.232049589, 3277.078978501]
  mom2 = [p1, p2, p3, p4]

  if only_D4:
    expec = [{'prid': pr, 'Momenta': mom2,
              'NLO-D4': [{'value': 0.23825168548E-04, 'power': [4, 4]}]}]
  else:
    expec = [{'prid': pr, 'Momenta': mom1,
              'NLO': [{'value': 0.11725269945430E-04, 'power': [4, 4]}]},
             {'prid': pr, 'Momenta': mom2,
              'NLO': [{'value': 0.24980468728796E-04, 'power': [4, 4]}]},
             {'prid': pr, 'Momenta': mom2,
              'NLO-R2': [{'value': 0.33983450067E-06, 'power': [4, 4]}]},
             {'prid': pr, 'Momenta': mom2,
              'NLO-D4': [{'value': 0.23825168548E-04, 'power': [4, 4]}]}]
  res = test_amplitude_squared(1, expec, err_digits=4, warning_digits=5)
  total_tests += len(res)
  tests_succeeded += res.values().count(True)

  if not only_D4:
    pr = 'g g -> H H'
    #Rec 0.32103003895657E-04
    #MG  0.32103003891244331E-004
    p1 = [328.181892279845, 0., 0., 328.181892279845]
    p2 = [695.902110726869, 0., 0., -695.902110726869]
    p3 = [521.405848502189, 342.110045005085, -308.424730106888,
          -209.937995173335]
    p4 = [502.678154504526, -342.110045005085, 308.424730106888,
          -157.782223273689]
    mom1 = [p1, p2, p3, p4]

    expec = [{'prid': pr, 'Momenta': mom1,
              'NLO': [{'value':  0.32103003895657E-04, 'power': [4, 4]}]}]
    res = test_amplitude_squared(4, expec, err_digits=4, warning_digits=5)
    total_tests += len(res)
    tests_succeeded += res.values().count(True)

  # Setting the widths to zero for processes 2, 3
  pyrecola.set_parameter_rcl('WW', complex(0., 0.))
  pyrecola.set_parameter_rcl('WZ', complex(0., 0.))
  pyrecola.reset_couplings_rcl()

  pr = 'g g -> H Z'
  #Rec       0.18275289013066E-04
  #MG        0.18275289011349815E-004
  #OpenLoops 0.182752890130307e-04
  p1 = [205.848904923, 0.000000000, 0.000000000,  205.848904923]
  p2 = [1048.085641910, 0.000000000, 0.000000000, -1048.085641910]
  p3 = [653.874317144,  334.824861045, -301.856870058, -456.838445235]
  p4 = [600.060229689, -334.824861045,  301.856870058, -385.398291752]
  mom1 = [p1, p2, p3, p4]

  #Rec       0.16678532405570E-05
  #MG        0.16678532394143273E-005
  #OpenLoops 0.166785314309853e-05
  p1 = [4575.253693240,    0.000000000,    0.000000000, 4575.253693240]
  p2 = [3.489589610,    0.000000000,    0.000000000,   -3.489589610]
  p3 = [1721.084827853,   -7.870935570,  -45.125169061, 1715.928247962]
  p4 = [2857.658454997,    7.870935570,   45.125169061, 2855.835855667]
  mom2 = [p1, p2, p3, p4]

  #Rec        0.21062707238382E-06
  #MG         0.21063210877298143E-006
  #OpenLoops  0.210627126411747e-06
  p1 = [2994.807402186,    0.000000000,    0.000000000, 2994.807402186]
  p2 = [4.148216682,    0.000000000,    0.000000000,   -4.148216682]
  p3 = [2065.759789904,   -7.516267625,    1.157936058, 2061.960395956]
  p4 = [933.195828964,    7.516267625,   -1.157936058,  928.698789548]
  mom3 = [p1, p2, p3, p4]

  expec = [{'prid': pr, 'Momenta': mom1,
            'NLO': [{'value': .18275289013066E-04, 'power': [4, 4]}]},
           {'prid': pr, 'Momenta': mom2,
            'NLO': [{'value': 0.16678532405570E-05, 'power': [4, 4]}]},
           {'prid': pr, 'Momenta': mom3,
            'NLO': [{'value': 0.21062707238382E-06, 'power': [4, 4]}]}
           ]
  res = test_amplitude_squared(2, expec, err_digits=3, warning_digits=4)
  total_tests += len(res)
  tests_succeeded += res.values().count(True)

  pr = 'g g -> W- W+'
  #Rec        0.22003659336918E-03
  #MG         0.22003659194632312E-003
  #OpenLoops  0.220036710268765E-03
  p1 = [148.386198113,    0.000000000,    0.000000000,  148.386198113]
  p2 = [805.788533156,    0.000000000,    0.000000000, -805.788533156]
  p3 = [493.956501185,  249.440244336, -224.879512193, -353.185569268]
  p4 = [460.218230084, -249.440244336,  224.879512193, -304.216765774]
  mom1 = [p1, p2, p3, p4]

  #Rec        0.19036889703742E-03
  #MG         0.19036930070359358E-003
  #OpenLoops  0.19036935271625E-03
  p1 = [4456.372627766,    0.000000000,    0.000000000, 4456.372627766]
  p2 = [1.984233896,    0.000000000,    0.000000000,   -1.984233896]
  p3 = [1411.196392614,   -5.922530770,  -33.954693177, 1408.483408757]
  p4 = [3047.160469048,    5.922530770,   33.954693177, 3045.904985114]
  mom2 = [p1, p2, p3, p4]

  #Rec        0.35535393060060E-03
  #MG         0.35535393054166192E-003
  #OpenLoops  0.353195529790185E-03
  p1 = [4989.775572721,    0.000000000,    0.000000000,   4989.775572721]
  p2 = [5387.028082195,    0.000000000,    0.000000000,  -5387.028082195]
  p3 = [5282.644030009, 4095.499143795, -2012.262028373, -2660.367360330]
  p4 = [5094.159624908, -4095.499143795, 2012.262028373,  2263.114850855]
  mom3 = [p1, p2, p3, p4]

  if only_D4:
    expec = [{'prid': pr, 'Momenta': mom1,
              'NLO-D4': [{'value':  0.92820644164E-03, 'power': [4, 4]}]}]

  else:
    expec = [{'prid': pr, 'Momenta': mom1,
              'NLO': [{'value':  0.22003659336918E-03, 'power': [4, 4]}]},
             {'prid': pr, 'Momenta': mom1,
              'NLO-D4': [{'value':  0.92820644164E-03, 'power': [4, 4]}]},
             {'prid': pr, 'Momenta': mom2,
              'NLO': [{'value': 0.19036889703742E-03, 'power': [4, 4]}]},
             {'prid': pr, 'Momenta': mom3,
              'NLO': [{'value': 0.35535393060060E-03, 'power': [4, 4]}]}]
  res = test_amplitude_squared(3, expec, err_digits=3, warning_digits=4)
  total_tests += len(res)
  tests_succeeded += res.values().count(True)

  if total_tests == tests_succeeded:
    print('All tests(' + str(total_tests) + ') succeeded ' + success())
  elif tests_succeeded == 0:
    print('All tests(' + str(total_tests) + ') failed ' + fail())
  else:
    print('Some tests(' + str(total_tests) + ') failed ' + warning())

  pyrecola.reset_recola_rcl()

if __name__ == "__main__":
  test_loopinduced_sm()
