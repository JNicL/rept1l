#==============================================================================#
#                                   model.py                                   #
#==============================================================================#

#============#
#  Includes  #
#============#

import os
from rept1l.autoct.modelfile import model, CTParameter
from rept1l.autoct.modelfile import TreeInducedCTVertex, CTVertex
from rept1l.autoct.auto_parameters_ct import auto_assign_ct_particle
from rept1l.autoct.auto_parameters_ct import set_light_particle
from rept1l.autoct.auto_parameters_ct import assign_counterterm
from rept1l.autoct.auto_parameters_ct import add_counterterm

#===========#
#  Globals  #
#===========#

# coupling constructor
Coupling = model.object_library.Coupling

# access to instances
param = model.parameters
P = model.particles
L = model.lorentz

# LCT allows to access the 2-point lorentz structures defined in lorentz_ct.py
import rept1l.autoct.lorentz_ct as LCT

modelname = 'SM (QCD,fermloops,YT)'
modelgauge = "'t Hooft-Feynman"

features = {'fermionloop_opt': True,
            'qcd_rescaling': True,
            'sm_generation_opt': True,
            'sm_parameters': True,
           }

#=============================#
#  External Parameter Orders  #
#=============================#

parameter_orders = {param.aEW: {'QED': 2},
                    param.aS:  {'QCD': 2},
                    param.gt:  {'YUK': 1}}

#===============================#
#  Fermion mass regularization  #
#===============================#

for p in [P.e__minus__, P.mu__minus__, P.tau__minus__, P.u, P.d, P.c, P.s, P.b]:
  set_light_particle(p)

###################################
#  SM Optimizations for Fermions  #
###################################

doublets = [(P.nu_e, P.e__minus__), (P.nu_mu, P.mu__minus__),
            (P.nu_tau, P.tau__minus__), (P.u, P.d), (P.c, P.s), (P.t, P.b)]

generations = [(P.e__minus__, P.mu__minus__, P.tau__minus__),
               (P.d, P.s, P.b)]

P.A.backgroundfield = True
P.A.quantumfield = False
P.ghA.backgroundfield = True
P.ghA.quantumfield = False
P.ghA__tilde__.backgroundfield = True
P.ghA__tilde__.quantumfield = False
P.Z.backgroundfield = True
P.Z.quantumfield = False
P.ghZ.backgroundfield = True
P.ghZ.quantumfield = False
P.ghZ__tilde__.backgroundfield = True
P.ghZ__tilde__.quantumfield = False
P.W__plus__.backgroundfield = True
P.W__plus__.quantumfield = False
P.ghWp.backgroundfield = True
P.ghWp.quantumfield = False
P.ghWp__tilde__.backgroundfield = True
P.ghWp__tilde__.quantumfield = False
P.W__minus__.backgroundfield = True
P.W__minus__.quantumfield = False
P.ghWm.backgroundfield = True
P.ghWm.quantumfield = False
P.ghWm__tilde__.backgroundfield = True
P.ghWm__tilde__.quantumfield = False
P.g.backgroundfield = True
P.g.quantumfield = False
P.ghG.backgroundfield = True
P.ghG.quantumfield = False
P.ghG__tilde__.backgroundfield = True
P.ghG__tilde__.quantumfield = False
P.H.backgroundfield = True
P.H.quantumfield = False
P.G0.backgroundfield = True
P.G0.quantumfield = False
P.G__plus__.backgroundfield = True
P.G__plus__.quantumfield = False
P.G__minus__.backgroundfield = True
P.G__minus__.quantumfield = False

##############################
#  coupling renormalization  #
##############################

assign_counterterm(param.gs, 'dZgs', 'gs*dZgs')

###################
#  ct assignment  #
###################

mixings = {P.A: [P.Z], P.Z: [P.A]}
auto_assign_ct_particle(mixings)

##################################
#  Gauge-fixing renormalization  #
##################################

GC_MZ_GF = Coupling(name='GC_MZ_GF',
                    value='MZ',
                    order={'QED': 0})

GC_MW_GF = Coupling(name='GC_MW_GF',
                    value='I*MW',
                    order={'QED': 0})

GC_MMW_GF = Coupling(name='GC_MMW_GF',
                     value='-I*MW',
                     order={'QED': 0})

V_WPGM = TreeInducedCTVertex(name='V_WPGM',
                             particles=[P.W__plus__, P.G__minus__],
                             color=['1'],
                             type='treeinducedct',
                             loop_particles='N',
                             lorentz=[LCT.VS1],
                             couplings={(0, 0): GC_MW_GF})

V_WMGP = TreeInducedCTVertex(name='V_WMGP',
                             particles=[P.W__minus__, P.G__plus__],
                             color=['1'],
                             type='treeinducedct',
                             loop_particles='N',
                             lorentz=[LCT.VS1],
                             couplings={(0, 0): GC_MMW_GF})

V_ZG0 = TreeInducedCTVertex(name='V_ZG0',
                            particles=[P.Z, P.G0],
                            color=['1'],
                            type='treeinducedct',
                            loop_particles='N',
                            lorentz=[LCT.VS1],
                            couplings={(0, 0): GC_MZ_GF})
