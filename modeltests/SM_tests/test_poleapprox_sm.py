#==============================================================================#
#                            test_poleapprox_sm.py                             #
#==============================================================================#

from __future__ import print_function

##############
#  Includes  #
##############

import sys
import pyrecola
from six import iteritems

import rept1l.modeltests.modeltest_utils.setup_models as sm
import rept1l.modeltests.SM_tests.sm_dc_setup as smdc

from rept1l.modeltests.modeltest_utils.model_tests import test_amplitude_squared
from rept1l.modeltests.modeltest_utils.model_tests import fail
from rept1l.modeltests.modeltest_utils.model_tests import success
from rept1l.modeltests.modeltest_utils.model_tests import warning

#############
#  Methods  #
#############

def set_parameter():
  params = smdc.params_pa.copy()
  scales = {'muUV': pyrecola.set_mu_uv_rcl,
            'muIR': pyrecola.set_mu_ir_rcl,
            'muMS': pyrecola.set_mu_ms_rcl}
  for scale, clb in iteritems(scales):
    if scale in params:
      clb(params[scale])
      del params[scale]

  for p, val in iteritems(params):
    pyrecola.set_parameter_rcl(p, complex(val, 0.))

def test_poleapprox_sm(only_bare=False):
  """ Testing the pole approximation versus RECOLA 1.0.

  Note that by the time this test was implemented deallocating RECOLA did not
  reset the `resonant` flag of particles. This test sets both Z and W
  resonant, thus, none of the width of W or Z appears in the couplings. """
  pyrecola.set_resonant_particle_rcl('Z')
  pyrecola.set_resonant_particle_rcl('W-')
  pr = 'e+ e- -> Z(e+ e-)'
  pyrecola.define_process_rcl(1, pr, 'NLO')
  pr = 'e- nu_e~ -> W- (e- nu_e~)'
  pyrecola.define_process_rcl(2, pr, 'NLO')
  set_parameter()
  pyrecola.generate_processes_rcl()

  total_tests = 0
  tests_succeeded = 0

  p1 = [45.593800000,    0.000000000,    0.000000000,   45.593800000]
  p2 = [45.593800000,    0.000000000,    0.000000000,  -45.593800000]
  p3 = [45.593800000,  -41.878440450,  -14.485428881,   10.731410626]
  p4 = [45.593800000,   41.878440450,   14.485428881,  -10.731410626]
  mom = [p1, p2, p3, p4]

  pr = 'e+ e- -> Z(e+ e-)'
  if only_bare:
    expec = [{'prid': pr, 'Momenta': mom,
              'LO': [{'value': 0.17284138918230E+01, 'power': [0, 4]}],
              'NLO-D4': [{'value': -0.82604789486E+00, 'power': [0, 6]}],
              }]
  else:
    expec = [{'prid': pr, 'Momenta': mom,
              'LO': [{'value': 0.17284138918230E+01, 'power': [0, 4]}],
              'NLO': [{'value': 0.22263001779272E+01, 'power': [0, 6]}],
              'NLO-D4': [{'value': -0.82604789486E+00, 'power': [0, 6]}],
              'NLO-CT': [{'value': 0.30844698021E+01, 'power': [0, 6]}],
              'NLO-R2': [{'value': -0.32121729282E-01, 'power': [0, 6]}],
              }]
  res = test_amplitude_squared(1, expec, err_digits=3, warning_digits=4)
  total_tests += len(res)
  tests_succeeded += list(res.values()).count(True)

  p1 = [40.199000000,    0.000000000,    0.000000000,   40.199000000]
  p2 = [40.199000000,    0.000000000,    0.000000000,  -40.199000000]
  p3 = [40.199000000,  -36.923253330,  -12.771467954,    9.461636796]
  p4 = [40.199000000,   36.923253330,   12.771467954,   -9.461636796]

  mom = [p1, p2, p3, p4]

  pr = 'e- nu_e~ -> W- (e- nu_e~)'
  if only_bare:
   expec = [{'prid': pr, 'Momenta': mom,
             'LO': [{'value': 0.25655666643420E+02, 'power': [0, 4]}],
             'NLO-D4': [{'value': -0.31374470270E+02, 'power': [0, 6]}],
             }]
  else:
    expec = [{'prid': pr, 'Momenta': mom,
              'LO': [{'value': 0.25655666643420E+02, 'power': [0, 4]}],
              'NLO': [{'value': 0.16280943492577E+02, 'power': [0, 6]}],
              'NLO-D4': [{'value': -0.31374470270E+02, 'power': [0, 6]}],
              'NLO-CT': [{'value': 0.48110839731E+02, 'power': [0, 6]}],
              'NLO-R2': [{'value': -0.45542596819E+00, 'power': [0, 6]}],
              }]
  res = test_amplitude_squared(2, expec, err_digits=3, warning_digits=4)
  total_tests += len(res)
  tests_succeeded += res.values().count(True)

  if total_tests == tests_succeeded:
    print('All tests(' + str(total_tests) + ') succeeded ' + success())
  elif tests_succeeded == 0:
    print('All tests(' + str(total_tests) + ') failed ' + fail())
  else:
    print('Some tests(' + str(total_tests) + ') failed ' + warning())

if __name__ == '__main__':
  test_poleapprox_sm(only_bare=True)
