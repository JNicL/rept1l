# This file was automatically created by FeynRules 2.3.27
# Mathematica version: 11.1.0 for Linux x86 (64-bit) (March 13, 2017)
# Date: Mon 10 Jul 2017 16:37:06


from object_library import all_orders, CouplingOrder


QCD = CouplingOrder(name = 'QCD',
                    expansion_order = 99,
                    hierarchy = 1)

QED = CouplingOrder(name = 'QED',
                    expansion_order = 99,
                    hierarchy = 2)

HQC = CouplingOrder(name = 'HQC',
                    expansion_order = 99,
                    hierarchy = 3)

HTC = CouplingOrder(name = 'HTC',
                    expansion_order = 99,
                    hierarchy = 4)
