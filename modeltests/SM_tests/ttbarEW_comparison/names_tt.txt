u~ u -> e+ nu_e b mu- nu_mu~ b~ for uu

g g -> e+ nu_e b mu- nu_mu~ b~ for gg

 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                           _    _   _   _        _ 
                          | )  |_  |   | |  |   |_|
                          | \  |_  |_  |_|  |_  | |

                REcursive Computation of One-Loop Amplitudes

                                 Version 1.0

       by S.Actis, A.Denner, L.Hofer, J.-N.Lang, A.Scharf, S.Uccirati

 xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx


 ---------------------------------------------------------------------------
  Pole masses and widths [GeV]:
  M_Z   =  91.153480619183                Width_Z   =  2.4942663787728    
  M_W   =  80.357973609878                Width_W   =  2.0842989982782    
  M_H   =  125.90000000000                Width_H   =  0.0000000000000    
  m_e   =  0.0000000000000           
  m_mu  =  0.0000000000000                Width_mu  =  0.0000000000000    
  m_tau =  0.0000000000000                Width_tau =  0.0000000000000    
  m_u   =  0.0000000000000           
  m_d   =  0.0000000000000           
  m_c   =  0.0000000000000                Width_c   =  0.0000000000000    
  m_s   =  0.0000000000000           
  m_t   =  173.34000000000                Width_t   =  1.3691800000000    
  m_b   =  0.0000000000000                Width_b   =  0.0000000000000    
 ---------------------------------------------------------------------------
  Renormalization done in the complex-mass scheme
 ---------------------------------------------------------------------------
  EW Renormalization Scheme: gfermi       Gf = 0.11663787000000E-04 GeV^-2
                                          alpha_Gf = 0.75553105223690E-02
 ---------------------------------------------------------------------------
  alpha_s Renormalization Scheme: 5-flavours Scheme
  alpha_s(Q) = 0.10846561579971           Q =  173.34000000000     GeV
 ---------------------------------------------------------------------------
  Delta_UV   =  0.0000000000000           mu_UV =  173.34000000000     GeV
  Delta_IR^2 =  0.0000000000000    
  Delta_IR   =  0.0000000000000           mu_IR =  173.34000000000     GeV
 ---------------------------------------------------------------------------
  Dimensional regularization for soft singularities
 ---------------------------------------------------------------------------


