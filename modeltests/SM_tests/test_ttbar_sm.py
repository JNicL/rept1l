#==============================================================================#
#                               test_ttbar_sm.py                               #
#==============================================================================#

from __future__ import print_function

#============#
#  Includes  #
#============#

import os
import pyrecola

from rept1l.modeltests.modeltest_utils.model_tests import test_amplitude_squared
from rept1l.modeltests.modeltest_utils.model_tests import fail, success
from rept1l.modeltests.modeltest_utils.model_tests import warning
import rept1l.modeltests.SM_tests.sm_dc_setup as smdc


def set_parameter_QCD():
  """ Parameters which are used for the QCD correction comparison """
  params = smdc.params_tt.copy()
  scales = {'muUV': pyrecola.set_mu_uv_rcl,
            'muIR': pyrecola.set_mu_ir_rcl,
            'muMS': pyrecola.set_mu_ms_rcl}
  for scale, clb in scales.iteritems():
    if scale in params:
      clb(params[scale])
      del params[scale]

  for p, val in params.iteritems():
    pyrecola.set_parameter_rcl(p, complex(val, 0.))


def set_parameter_EW():
  """ Parameters which are used for the EW correction comparison """
  params = smdc.params_tt_EW.copy()
  scales = {'muUV': pyrecola.set_mu_uv_rcl,
            'muIR': pyrecola.set_mu_ir_rcl,
            'muMS': pyrecola.set_mu_ms_rcl}
  for scale, clb in scales.iteritems():
    if scale in params:
      clb(params[scale])
      del params[scale]

  for p, val in params.iteritems():
    pyrecola.set_parameter_rcl(p, complex(val, 0.))


def test_ttbar_sm_QCD(D4=True, R2=True, CT=True, indiviual_contributions=True):
  """ Testing offshell tt~ production in RECOLA 1.0 (which was tested against
  and MadGraph). """

  set_parameter_QCD()

  from math import pi
  pyrecola.set_delta_ir_rcl(0., pi * pi / 6.)

  pr = 'u~ u -> e+ nu_e b mu- nu_mu~ b~'
  pyrecola.set_masscut_rcl(1.)
  pyrecola.define_process_rcl(1, pr, 'NLO')
  pyrecola.unselect_all_powers_BornAmpl_rcl(1)
  pyrecola.unselect_all_powers_LoopAmpl_rcl(1)
  pyrecola.select_power_BornAmpl_rcl(1, 'QCD', 2)
  pyrecola.select_power_BornAmpl_rcl(1, 'QED', 4)
  pyrecola.select_power_LoopAmpl_rcl(1, 'QCD', 4)
  pyrecola.select_power_LoopAmpl_rcl(1, 'QED', 4)
  pyrecola.generate_processes_rcl()

  total_tests = 0
  tests_succeeded = 0

  expec = []

  # PS 1
  p1 = [6.0879408626868283E+01, 0.0000000000000000E+00,
        0.0000000000000000E+00, 6.0879408626868283E+01]
  p2 = [1.3281918887333597E+03, 0.0000000000000000E+00,
        0.0000000000000000E+00, -1.3281918887333597E+03]
  p3 = [2.0388404394499528E+02, 3.6779160119828688E+01,
        -1.2605946983694432E+02, -1.5596476147089538E+02]
  p4 = [1.0768936058860811E+02, -5.7181471624511353E+00,
        -6.1742608650429744E+00, -1.0736004694383388E+02]
  p5 = [8.6131646717058084E+02, -4.4318859390220744E+01,
        1.7469725335055722E+02, -8.4224863608873318E+02]
  p6 = [8.4480313625478303E+01, 2.0239843457192833E+01,
        -6.0741936518164991E+01, -5.5115236324547475E+01]
  p7 = [3.7997525445386842E+01, -3.3325154701023337E+01,
        3.1720525163492326E+00, -1.7977321462309256E+01]
  p8 = [9.3703586585178542E+01, 2.6343157676673698E+01,
        1.5106361353245831E+01, -8.8646477816172251E+01]

  mom1 = [p1, p2, p3, p4, p5, p6, p7, p8]

  expec1 = {'prid': pr, 'Momenta': mom1,
            'LO': [{'value': 0.10803609409868E-13, 'power': [4, 8]}]}
  if D4 and indiviual_contributions:
    expec1['NLO-D4'] = [{'value': 0.39227282007E-14, 'power': [6, 8]}]
  if CT and indiviual_contributions:
    expec1['NLO-CT'] = [{'value': -0.84774664269E-15, 'power': [6, 8]}]
  if R2 and indiviual_contributions:
    expec1['NLO-R2'] = [{'value': -0.24562886295E-14, 'power': [6, 8]}]
  if D4 and CT and R2:
    expec1['NLO'] = [{'value': 0.61869292851003E-15, 'power': [6, 8]}]
  expec.append(expec1)

  # PS 2
  p1 = [2608.387939453,    0.000000000,    0.000000000, 2608.387939453]
  p2 = [559.117309570,    0.000000000,    0.000000000, -559.117309570]
  p3 = [316.192502678,  147.970314309,    5.939316127, -279.369306397]
  p4 = [136.154114318,  106.497183532,   19.187752153,  -82.632456775]
  p5 = [590.931690487,  239.848068237,  360.941253662,  401.739440918]
  p6 = [545.106000816,  -98.347892761,  -99.072410583,  526.927795410]
  p7 = [1534.705070063, -415.258392334, -258.328277588, 1454.698120117]
  p8 = [44.415870662,   19.290725708,  -28.667657852,   27.907039642]

  mom2 = [p1, p2, p3, p4, p5, p6, p7, p8]

  expec2 = {'prid': pr, 'Momenta': mom2,
            'LO': [{'value': 0.74006463294894E-16, 'power': [4, 8]}]}
  if D4 and indiviual_contributions:
    expec2['NLO-D4'] = [{'value': -0.58743107075E-17, 'power': [6, 8]}]
  if CT and indiviual_contributions:
    expec2['NLO-CT'] = [{'value': -0.17302303818E-17, 'power': [6, 8]}]
  if R2 and indiviual_contributions:
    expec2['NLO-R2'] = [{'value': -0.17475513191E-16, 'power': [6, 8]}]
  if D4 and CT and R2:
    expec2['NLO'] = [{'value': -0.25080054280399E-16, 'power': [6, 8]}]
  expec.append(expec2)

  # PS 3
  p1 = [3636.376464844,    0.000000000,    0.000000000, 3636.376464844]
  p2 = [250.648712158,    0.000000000,    0.000000000, -250.648712158]
  p3 = [170.933875408, -107.805246555,  -30.691803777,  129.052050578]
  p4 = [339.207449078, -191.531361570,   48.497221172,  275.727130664]
  p5 = [1920.282301803,  -72.580764771,  556.945861816, 1836.308105469]
  p6 = [129.636163970,    3.127527952,   41.242015839,  122.861099243]
  p7 = [146.457474486,   80.724189758,   66.417587280,  102.577293396]
  p8 = [1180.507912257,  288.065673828, -682.410888672,  919.202087402]

  mom3 = [p1, p2, p3, p4, p5, p6, p7, p8]

  expec3 = {'prid': pr, 'Momenta': mom3,
            'LO': [{'value': 0.12639364628388E-13, 'power': [4, 8]}]}
  if D4 and indiviual_contributions:
    expec3['NLO-D4'] = [{'value': -0.75674698648E-14, 'power': [6, 8]}]
  if CT and indiviual_contributions:
    expec3['NLO-CT'] = [{'value': -0.45765119011E-15, 'power': [6, 8]}]
  if R2 and indiviual_contributions:
    expec3['NLO-R2'] = [{'value': -0.10849486942E-14, 'power': [6, 8]}]
  if D4 and CT and R2:
    expec3['NLO'] = [{'value': -0.91100697491703E-14, 'power': [6, 8]}]
  expec.append(expec3)

  # PS 4
  p1 = [2660.958740234,    0.000000000,    0.000000000, 2660.958740234]
  p2 = [298.880859375,    0.000000000,    0.000000000, -298.880859375]
  p3 = [657.415068399,  197.294937870,  145.978063480,  609.884976558]
  p4 = [673.188860464,  185.132058890,  226.555340371,  606.285444632]
  p5 = [41.568875404,   28.771638870,  -14.785662651,   26.106481552]
  p6 = [164.289487007,   43.689327240,  -29.557434082,  155.591247559]
  p7 = [717.922190259, -593.856872559, -296.744232178,  273.293151855]
  p8 = [705.455118077,  138.968948364,  -31.446069717,  690.916564941]

  mom4 = [p1, p2, p3, p4, p5, p6, p7, p8]

  expec4 = {'prid': pr, 'Momenta': mom4,
            'LO': [{'value': 0.13776838532274E-17, 'power': [4, 8]}]}
  if D4 and indiviual_contributions:
    expec4['NLO-D4'] = [{'value': -0.41040693390E-18, 'power': [6, 8]}]
  if CT and indiviual_contributions:
    expec4['NLO-CT'] = [{'value': -0.14933642289E-18, 'power': [6, 8]}]
  if R2 and indiviual_contributions:
    expec4['NLO-R2'] = [{'value': -0.31031630203E-18, 'power': [6, 8]}]
  if D4 and CT and R2:
    expec4['NLO'] = [{'value': -0.87005965882310E-18, 'power': [6, 8]}]
  expec.append(expec4)

  # PS 5
  p1 = [2552.204345703,    0.000000000,    0.000000000, 2552.204345703]
  p2 = [746.532836914,    0.000000000,    0.000000000, -746.532836914]
  p3 = [94.858574788,   37.698033661,  -53.658615830,   68.540210214]
  p4 = [129.710761783,  103.555631554,  -76.499413188,   15.778234299]
  p5 = [1209.201296996,   62.188377380,  235.635620117, 1184.388549805]
  p6 = [505.624745960, -276.055725098, -164.005706787,  390.578735352]
  p7 = [685.765300664,  -42.845073700, -553.802368164,  402.170715332]
  p8 = [673.576502426,  115.458770752,  612.330505371, -255.784927368]

  mom5 = [p1, p2, p3, p4, p5, p6, p7, p8]

  expec5 = {'prid': pr, 'Momenta': mom5,
            'LO': [{'value': 0.14844441561263E-18, 'power': [4, 8]}]}
  if D4 and indiviual_contributions:
    expec5['NLO-D4'] = [{'value': -0.10405615215E-18, 'power': [6, 8]}]
  if CT and indiviual_contributions:
    expec5['NLO-CT'] = [{'value': 0.24447441113E-21, 'power': [6, 8]}]
  if R2 and indiviual_contributions:
    expec5['NLO-R2'] = [{'value': -0.16318314968E-19, 'power': [6, 8]}]
  if D4 and CT and R2:
    expec5['NLO'] = [{'value': -0.12012999270334E-18, 'power': [6, 8]}]
  expec.append(expec5)

  # PS 6
  p1 = [82.700622559,    0.000000000,    0.000000000,   82.700622559]
  p2 = [939.870422363,    0.000000000,    0.000000000, -939.870422363]
  p3 = [201.918030494,   51.830069867,   93.268151254, -171.422247267]
  p4 = [107.246992352,   -8.123571721,   16.523431445, -105.654631533]
  p5 = [338.762134916,   88.799629211, -102.049232483, -310.580688477]
  p6 = [300.876380431,  -98.325889587,   -6.917593002, -284.272338867]
  p7 = [26.314339018,   -6.669461727,   15.169430733,  -20.441406250]
  p8 = [47.453167710,  -27.510770798,  -15.994193077,   35.201511383]

  mom6 = [p1, p2, p3, p4, p5, p6, p7, p8]

  expec6 = {'prid': pr, 'Momenta': mom6,
            'LO': [{'value': 0.42522058877039E-16, 'power': [4, 8]}]}
  if D4 and indiviual_contributions:
    expec6['NLO-D4'] = [{'value': 0.15365269217E-17, 'power': [6, 8]}]
  if CT and indiviual_contributions:
    expec6['NLO-CT'] = [{'value': -0.21704020540E-16, 'power': [6, 8]}]
  if R2 and indiviual_contributions:
    expec6['NLO-R2'] = [{'value': -0.86541888781E-17, 'power': [6, 8]}]
  if D4 and CT and R2:
    expec6['NLO'] = [{'value': -0.28821682496591E-16, 'power': [6, 8]}]
  expec.append(expec6)

  res = test_amplitude_squared(1, expec, err_digits=3, warning_digits=4)
  total_tests += len(res)
  tests_succeeded += res.values().count(True)

  if total_tests == tests_succeeded:
    print('All tests(' + str(total_tests) + ') succeeded ' + success())
  elif tests_succeeded == 0:
    print('All tests(' + str(total_tests) + ') failed ' + fail())
  else:
    print('Some tests(' + str(total_tests) + ') failed ' + warning())

  pyrecola.reset_recola_rcl()


def read_process(filepath):
  """ Reads the process and stores all matrix elements (Born, NLO, momenta) in a
  dictionary. """
  with open(filepath, 'r') as f:
    lines = f.readlines()
    offset = 0
    PSPs = []
    nmomenta = 8
    for i in range(offset, len(lines), nmomenta + 1):
      momenta = lines[i:i + nmomenta]
      momenta = [[float(v) for v in u.split()] for u in momenta]
      if len(momenta) > 0 and momenta != [[]]:
        ME = lines[i + nmomenta].split()
        Born, NLO = ME
        Born = float(Born)
        NLO = float(NLO)
        PSP = {'Momenta': momenta, 'Born': Born, 'NLO': NLO}
        PSPs.append(PSP)
    return PSPs


def test_ttbar_sm_EW(D4=True, R2=True, CT=True, indiviual_contributions=True):
  """ Testing offshell tt~ production in RECOLA 1.0 (which was tested against
  and MadGraph). """

  set_parameter_EW()

  pyrecola.set_delta_ir_rcl(0., 0.)
  dir = os.path.dirname(os.path.realpath(__file__))
  dir = os.path.join(dir, 'ttbarEW_comparison')
  src_uu = os.path.join(dir, 'PSP_uu.input')
  src_gg = os.path.join(dir, 'PSP_gg.input')
  PSPs_uu = read_process(src_uu)
  PSPs_gg = read_process(src_gg)

  pyrecola.set_masscut_rcl(1.)

  pr_uu = 'u~ u -> e+ nu_e b mu- nu_mu~ b~'
  pyrecola.define_process_rcl(1, pr_uu, 'NLO')
  pyrecola.unselect_all_powers_BornAmpl_rcl(1)
  pyrecola.unselect_all_powers_LoopAmpl_rcl(1)
  pyrecola.select_power_BornAmpl_rcl(1, 'QCD', 2)
  pyrecola.select_power_BornAmpl_rcl(1, 'QED', 4)
  pyrecola.select_power_LoopAmpl_rcl(1, 'QCD', 2)
  pyrecola.select_power_LoopAmpl_rcl(1, 'QED', 6)

  #pr_gg = 'g g -> e+ ve b mu- vm~ b~'
  #pyrecola.define_process_rcl(1, pr_gg, 'NLO')
  #pyrecola.unselect_all_powers_BornAmpl_rcl(1)
  #pyrecola.unselect_all_powers_LoopAmpl_rcl(1)
  #pyrecola.select_power_BornAmpl_rcl(1, 'QCD', 2)
  #pyrecola.select_power_BornAmpl_rcl(1, 'QED', 4)
  #pyrecola.select_power_LoopAmpl_rcl(1, 'QCD', 2)
  #pyrecola.select_power_LoopAmpl_rcl(1, 'QED', 6)

  pyrecola.generate_processes_rcl()

  total_tests = 0
  tests_succeeded = 0

  expec = []

  for PSP in PSPs_uu:
    epc = {'prid': pr_uu, 'Momenta': PSP['Momenta'],
           'LO': [{'value': PSP['Born'], 'power': [4, 8]}]}
    if D4 and CT and R2:
      epc['NLO'] = [{'value': PSP['NLO'], 'power': [4, 10]}]
    expec.append(epc)

  #for PSP in PSPs_gg:
    #epc = {'prid': pr_gg, 'Momenta': PSP['Momenta'],
           #'LO': [{'value': PSP['BORN'], 'power': [4, 8]}]}
    #if D4 and CT and R2:
      #epc['NLO'] = [{'value': PSP['NLO'], 'power': [4, 10]}]
    #expec.append(epc)

  res = test_amplitude_squared(1, expec, err_digits=6, warning_digits=7)
  total_tests += len(res)
  tests_succeeded += res.values().count(True)

  if total_tests == tests_succeeded:
    print('All tests(' + str(total_tests) + ') succeeded ' + success())
  elif tests_succeeded == 0:
    print('All tests(' + str(total_tests) + ') failed ' + fail())
  else:
    print('Some tests(' + str(total_tests) + ') failed ' + warning())


def test_ttbar_leptonjets_QCD(NLO=True):
  """ Testing offshell tt~ production against RECOLA 1.2 (by M.Pellen).
  The results in PSP_gg.input were computed with recola1.1 and disabled particle
  permutation.
  """

  pyrecola.set_pole_mass_w_rcl(80.357973609878, 0.)
  pyrecola.set_pole_mass_z_rcl(91.153480619183, 0.)
  pyrecola.set_pole_mass_top_rcl(173.340, 0.)
  pyrecola.set_pole_mass_h_rcl(125.0, 0.)
  pyrecola.set_pole_mass_bottom_rcl(0., 0.)
  pyrecola.set_light_fermions_rcl(1E-3)
  pyrecola.use_gfermi_scheme_rcl(0.1166378700E-04)
  pyrecola.set_alphas_rcl(0.10759811381781, 173.34, 5)
  from math import pi
  pyrecola.set_delta_ir_rcl(0., pi * pi / 6.)

  path = os.path.pathname(os.path.realpath(__file__))
  path = os.path.join(path, 'ttbarLeptonJets')
  src_gg = os.path.join(path, 'PSP_gg.input')
  PSPs_gg = read_process(src_gg)

  pyrecola.set_mu_uv_rcl(173.34)
  pyrecola.set_mu_ir_rcl(173.34)

  pr_gg = 'g g -> mu- nu_mu~ b b~ u d~'
  order = 'NLO' if NLO else 'LO'
  pyrecola.define_process_rcl(1, pr_gg, order)
  pyrecola.unselect_all_powers_BornAmpl_rcl(1)
  pyrecola.unselect_all_powers_LoopAmpl_rcl(1)
  pyrecola.unselect_all_gs_powers_BornAmpl_rcl(1)
  pyrecola.select_gs_power_BornAmpl_rcl(1, 2)
  pyrecola.unselect_all_gs_powers_LoopAmpl_rcl(1)
  pyrecola.select_gs_power_LoopAmpl_rcl(1, 4)
  pyrecola.split_collier_cache_rcl(1, 10)
  pyrecola.generate_processes_rcl()

  total_tests = 0
  tests_succeeded = 0

  expec = []

  for PSP in PSPs_gg:
    epc = {'prid': pr_gg, 'Momenta': PSP['Momenta'],
           'LO': [{'value': PSP['Born'], 'power': [4, 8]}]}
    if NLO:
      epc['NLO'] = [{'value': PSP['NLO'], 'power': [6, 8]}]
    expec.append(epc)

  res = test_amplitude_squared(1, expec, err_digits=4, warning_digits=5)
  total_tests += len(res)
  tests_succeeded += res.values().count(True)

  if total_tests == tests_succeeded:
    print('All tests(' + str(total_tests) + ') succeeded ' + success())
  elif tests_succeeded == 0:
    print('All tests(' + str(total_tests) + ') failed ' + fail())
  else:
    print('Some tests(' + str(total_tests) + ') failed ' + warning())


if __name__ == "__main__":
  # test_ttbar_sm_QCD(indiviual_contributions=False, R2=False, CT=False)
  test_ttbar_leptonjets_QCD(NLO=True)
