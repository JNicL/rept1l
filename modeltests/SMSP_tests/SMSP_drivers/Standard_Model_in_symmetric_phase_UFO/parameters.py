# This file was automatically created by FeynRules 2.3.29
# Mathematica version: 12.0.0 for Linux x86 (64-bit) (April 7, 2019)
# Date: Sun 2 Aug 2020 19:27:27



from .object_library import all_parameters, Parameter


from .function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot

# This is a default parameter object representing 0.
ZERO = Parameter(name = 'ZERO',
                 nature = 'internal',
                 type = 'real',
                 value = '0.0',
                 texname = '0')

# User-defined parameters.
aS = Parameter(name = 'aS',
               nature = 'external',
               type = 'real',
               value = 0.1184,
               texname = '\\alpha _s',
               lhablock = 'SMINPUTS',
               lhacode = [ 3 ])

g1 = Parameter(name = 'g1',
               nature = 'external',
               type = 'real',
               value = 0.1,
               texname = 'g_1',
               lhablock = 'FRBlock',
               lhacode = [ 1 ])

g2 = Parameter(name = 'g2',
               nature = 'external',
               type = 'real',
               value = 0.02,
               texname = 'g_2',
               lhablock = 'FRBlock',
               lhacode = [ 2 ])

g1t = Parameter(name = 'g1t',
                nature = 'internal',
                type = 'real',
                value = '-g1/2.',
                texname = 'g_{1,t}')

gs = Parameter(name = 'gs',
               nature = 'internal',
               type = 'real',
               value = '2*cmath.sqrt(aS)*cmath.sqrt(cmath.pi)',
               texname = 'g_s')

