# This file was automatically created by FeynRules 2.3.29
# Mathematica version: 12.0.0 for Linux x86 (64-bit) (April 7, 2019)
# Date: Sun 2 Aug 2020 19:27:27


from .object_library import all_couplings, Coupling

from .function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



GC_1 = Coupling(name = 'GC_1',
                value = '-(complex(0,1)*g1)/6.',
                order = {'GC1':1})

GC_2 = Coupling(name = 'GC_2',
                value = '(complex(0,1)*g1)/2.',
                order = {'GC1':1})

GC_3 = Coupling(name = 'GC_3',
                value = '(complex(0,1)*g1**2)/2.',
                order = {'GC1':2})

GC_4 = Coupling(name = 'GC_4',
                value = '-(complex(0,1)*g2)/2.',
                order = {'GC2':1})

GC_5 = Coupling(name = 'GC_5',
                value = '(complex(0,1)*g2)/2.',
                order = {'GC2':1})

GC_6 = Coupling(name = 'GC_6',
                value = '-(complex(0,1)*g2)',
                order = {'GC2':1})

GC_7 = Coupling(name = 'GC_7',
                value = 'complex(0,1)*g2',
                order = {'GC2':1})

GC_8 = Coupling(name = 'GC_8',
                value = '-((complex(0,1)*g2)/cmath.sqrt(2))',
                order = {'GC2':1})

GC_9 = Coupling(name = 'GC_9',
                value = '(complex(0,1)*g2)/cmath.sqrt(2)',
                order = {'GC2':1})

GC_10 = Coupling(name = 'GC_10',
                 value = '-(complex(0,1)*g1*g2)/2.',
                 order = {'GC1':1,'GC2':1})

GC_11 = Coupling(name = 'GC_11',
                 value = '(complex(0,1)*g1*g2)/2.',
                 order = {'GC1':1,'GC2':1})

GC_12 = Coupling(name = 'GC_12',
                 value = '-((complex(0,1)*g1*g2)/cmath.sqrt(2))',
                 order = {'GC1':1,'GC2':1})

GC_13 = Coupling(name = 'GC_13',
                 value = '(complex(0,1)*g2**2)/2.',
                 order = {'GC2':2})

GC_14 = Coupling(name = 'GC_14',
                 value = '-(complex(0,1)*g2**2)',
                 order = {'GC2':2})

GC_15 = Coupling(name = 'GC_15',
                 value = '-2*complex(0,1)*g2**2',
                 order = {'GC2':2})

GC_16 = Coupling(name = 'GC_16',
                 value = '-gs',
                 order = {'QCD':1})

GC_17 = Coupling(name = 'GC_17',
                 value = 'complex(0,1)*gs',
                 order = {'QCD':1})

GC_18 = Coupling(name = 'GC_18',
                 value = 'complex(0,1)*gs**2',
                 order = {'QCD':2})

