# This file was automatically created by FeynRules 2.3.29
# Mathematica version: 12.0.0 for Linux x86 (64-bit) (April 7, 2019)
# Date: Sun 2 Aug 2020 19:27:27


from __future__ import division
from .object_library import all_particles, Particle
from . import parameters as Param

from . import propagators as Prop

B0 = Particle(pdg_code = 9000001,
              name = 'B0',
              antiname = 'B0',
              spin = 3,
              color = 1,
              mass = Param.ZERO,
              width = Param.ZERO,
              texname = 'B_0',
              antitexname = 'B_0',
              charge = 0,
              GhostNumber = 0,
              LeptonNumber = 0,
              Y = 0)

W__plus__ = Particle(pdg_code = 24,
                     name = 'W+',
                     antiname = 'W-',
                     spin = 3,
                     color = 1,
                     mass = Param.ZERO,
                     width = Param.ZERO,
                     texname = 'W^+',
                     antitexname = 'W^-',
                     charge = 1,
                     GhostNumber = 0,
                     LeptonNumber = 0,
                     Y = 0)

W__minus__ = W__plus__.anti()

W3 = Particle(pdg_code = 9000002,
              name = 'W3',
              antiname = 'W3',
              spin = 3,
              color = 1,
              mass = Param.ZERO,
              width = Param.ZERO,
              texname = 'W_3',
              antitexname = 'W_3',
              charge = 0,
              GhostNumber = 0,
              LeptonNumber = 0,
              Y = 0)

g = Particle(pdg_code = 21,
             name = 'g',
             antiname = 'g',
             spin = 3,
             color = 8,
             mass = Param.ZERO,
             width = Param.ZERO,
             texname = 'g',
             antitexname = 'g',
             charge = 0,
             GhostNumber = 0,
             LeptonNumber = 0,
             Y = 0)

nu_e = Particle(pdg_code = 12,
                name = 'nu_e',
                antiname = 'nu_e~',
                spin = 2,
                color = 1,
                mass = Param.ZERO,
                width = Param.ZERO,
                texname = '\\nu_e',
                antitexname = '\\bar{\\nu}_e',
                charge = 0,
                GhostNumber = 0,
                LeptonNumber = 1,
                Y = 0)

nu_e__tilde__ = nu_e.anti()

nu_mu = Particle(pdg_code = 14,
                 name = 'nu_mu',
                 antiname = 'nu_mu~',
                 spin = 2,
                 color = 1,
                 mass = Param.ZERO,
                 width = Param.ZERO,
                 texname = '\\nu_\\mu',
                 antitexname = '\\bar{\\nu}_\\mu',
                 charge = 0,
                 GhostNumber = 0,
                 LeptonNumber = 1,
                 Y = 0)

nu_mu__tilde__ = nu_mu.anti()

nu_tau = Particle(pdg_code = 16,
                  name = 'nu_tau',
                  antiname = 'nu_tau~',
                  spin = 2,
                  color = 1,
                  mass = Param.ZERO,
                  width = Param.ZERO,
                  texname = '\\nu_\\tau',
                  antitexname = '\\bar{\\nu}_\\tau',
                  charge = 0,
                  GhostNumber = 0,
                  LeptonNumber = 1,
                  Y = 0)

nu_tau__tilde__ = nu_tau.anti()

e__minus__ = Particle(pdg_code = 11,
                      name = 'e-',
                      antiname = 'e+',
                      spin = 2,
                      color = 1,
                      mass = Param.ZERO,
                      width = Param.ZERO,
                      texname = 'e^-',
                      antitexname = 'e^+',
                      charge = -1,
                      GhostNumber = 0,
                      LeptonNumber = 1,
                      Y = 0)

e__plus__ = e__minus__.anti()

mu__minus__ = Particle(pdg_code = 13,
                       name = 'mu-',
                       antiname = 'mu+',
                       spin = 2,
                       color = 1,
                       mass = Param.ZERO,
                       width = Param.ZERO,
                       texname = '\\mu^-',
                       antitexname = '\\mu^+',
                       charge = -1,
                       GhostNumber = 0,
                       LeptonNumber = 1,
                       Y = 0)

mu__plus__ = mu__minus__.anti()

tau__minus__ = Particle(pdg_code = 15,
                        name = 'tau-',
                        antiname = 'tau+',
                        spin = 2,
                        color = 1,
                        mass = Param.ZERO,
                        width = Param.ZERO,
                        texname = '\\tau^-',
                        antitexname = '\\tau^+',
                        charge = -1,
                        GhostNumber = 0,
                        LeptonNumber = 1,
                        Y = 0)

tau__plus__ = tau__minus__.anti()

u = Particle(pdg_code = 2,
             name = 'u',
             antiname = 'u~',
             spin = 2,
             color = 3,
             mass = Param.ZERO,
             width = Param.ZERO,
             texname = 'u',
             antitexname = '\\bar{u}',
             charge = 2/3,
             GhostNumber = 0,
             LeptonNumber = 0,
             Y = 0)

u__tilde__ = u.anti()

c = Particle(pdg_code = 4,
             name = 'c',
             antiname = 'c~',
             spin = 2,
             color = 3,
             mass = Param.ZERO,
             width = Param.ZERO,
             texname = 'c',
             antitexname = '\\bar{c}',
             charge = 2/3,
             GhostNumber = 0,
             LeptonNumber = 0,
             Y = 0)

c__tilde__ = c.anti()

t = Particle(pdg_code = 6,
             name = 't',
             antiname = 't~',
             spin = 2,
             color = 3,
             mass = Param.ZERO,
             width = Param.ZERO,
             texname = 't',
             antitexname = '\\bar{t}',
             charge = 2/3,
             GhostNumber = 0,
             LeptonNumber = 0,
             Y = 0)

t__tilde__ = t.anti()

d = Particle(pdg_code = 1,
             name = 'd',
             antiname = 'd~',
             spin = 2,
             color = 3,
             mass = Param.ZERO,
             width = Param.ZERO,
             texname = 'd',
             antitexname = '\\bar{d}',
             charge = -1/3,
             GhostNumber = 0,
             LeptonNumber = 0,
             Y = 0)

d__tilde__ = d.anti()

s = Particle(pdg_code = 3,
             name = 's',
             antiname = 's~',
             spin = 2,
             color = 3,
             mass = Param.ZERO,
             width = Param.ZERO,
             texname = 's',
             antitexname = '\\bar{s}',
             charge = -1/3,
             GhostNumber = 0,
             LeptonNumber = 0,
             Y = 0)

s__tilde__ = s.anti()

b = Particle(pdg_code = 5,
             name = 'b',
             antiname = 'b~',
             spin = 2,
             color = 3,
             mass = Param.ZERO,
             width = Param.ZERO,
             texname = 'b',
             antitexname = '\\bar{b}',
             charge = -1/3,
             GhostNumber = 0,
             LeptonNumber = 0,
             Y = 0)

b__tilde__ = b.anti()

Phi1 = Particle(pdg_code = 9000003,
                name = 'Phi1',
                antiname = 'Phi1~',
                spin = 1,
                color = 1,
                mass = Param.ZERO,
                width = Param.ZERO,
                texname = '\\Phi_1^+',
                antitexname = '\\Phi_1^-',
                charge = 1,
                GhostNumber = 0,
                LeptonNumber = 0,
                Y = 0)

Phi1__tilde__ = Phi1.anti()

Phi2 = Particle(pdg_code = 9000004,
                name = 'Phi2',
                antiname = 'Phi2~',
                spin = 1,
                color = 1,
                mass = Param.ZERO,
                width = Param.ZERO,
                texname = '\\Phi_2^+',
                antitexname = '\\Phi_2^-',
                charge = 0,
                GhostNumber = 0,
                LeptonNumber = 0,
                Y = 0)

Phi2__tilde__ = Phi2.anti()

ghW3 = Particle(pdg_code = 9000005,
                name = 'ghW3',
                antiname = 'ghW3~',
                spin = -1,
                color = 1,
                mass = Param.ZERO,
                width = Param.ZERO,
                texname = 'u_{W_3}',
                antitexname = '\\bar{u}_{W_3}',
                charge = 0,
                GhostNumber = 1,
                LeptonNumber = 0,
                Y = 0)

ghW3__tilde__ = ghW3.anti()

ghWp = Particle(pdg_code = 9000006,
                name = 'ghWp',
                antiname = 'ghWp~',
                spin = -1,
                color = 1,
                mass = Param.ZERO,
                width = Param.ZERO,
                texname = 'u_{W^+}',
                antitexname = '\\bar{u}_{W^+}',
                charge = 1,
                GhostNumber = 1,
                LeptonNumber = 0,
                Y = 0)

ghWp__tilde__ = ghWp.anti()

ghWm = Particle(pdg_code = 9000007,
                name = 'ghWm',
                antiname = 'ghWm~',
                spin = -1,
                color = 1,
                mass = Param.ZERO,
                width = Param.ZERO,
                texname = 'u_{W^-}',
                antitexname = '\\bar{u}_{W^-}',
                charge = -1,
                GhostNumber = 1,
                LeptonNumber = 0,
                Y = 0)

ghWm__tilde__ = ghWm.anti()

ghG = Particle(pdg_code = 82,
               name = 'ghG',
               antiname = 'ghG~',
               spin = -1,
               color = 8,
               mass = Param.ZERO,
               width = Param.ZERO,
               texname = 'u_{G-}',
               antitexname = '\\bar{u}_{G}',
               charge = 0,
               GhostNumber = 1,
               LeptonNumber = 0,
               Y = 0)

ghG__tilde__ = ghG.anti()

