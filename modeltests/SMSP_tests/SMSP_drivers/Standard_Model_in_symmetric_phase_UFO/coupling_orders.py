# This file was automatically created by FeynRules 2.3.29
# Mathematica version: 12.0.0 for Linux x86 (64-bit) (April 7, 2019)
# Date: Sun 2 Aug 2020 19:27:27


from .object_library import all_orders, CouplingOrder


QCD = CouplingOrder(name = 'QCD',
                    expansion_order = 99,
                    hierarchy = 1)

GC1 = CouplingOrder(name = 'GC1',
                    expansion_order = 99,
                    hierarchy = 2)

GC2 = CouplingOrder(name = 'GC2',
                    expansion_order = 99,
                    hierarchy = 2)

