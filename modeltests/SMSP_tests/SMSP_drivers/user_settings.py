#==============================================================================#
#                                   model.py                                   #
#==============================================================================#

#============#
#  Includes  #
#============#

import os
from rept1l.autoct.modelfile import model, CTParameter
from rept1l.autoct.modelfile import TreeInducedCTVertex, CTVertex
from rept1l.autoct.auto_parameters_ct import auto_assign_ct_particle
from rept1l.autoct.auto_parameters_ct import set_light_particle
from rept1l.autoct.auto_parameters_ct import assign_counterterm
from rept1l.autoct.auto_parameters_ct import add_counterterm
from rept1l.autoct.auto_tadpole_ct import auto_tadpole_vertices
from rept1l.autoct.auto_tadpole_ct import assign_FJTadpole_field_shift

#===========#
#  Globals  #
#===========#

# coupling constructor
Coupling = model.object_library.Coupling

modelname = 'SMSP'
modelgauge = "'t Hooft-Feynman"

features = {'fermionloop_opt' : True,
            'qcd_rescaling' : True,
           }


# access to instances
param = model.parameters
P = model.particles

#=============================#
#  External Parameter Orders  #
#=============================#

parameter_orders = {param.g1: {'GC1': 1},
                    param.g2: {'GC2': 1},
                    param.aS:  {'QCD': 2}}


assign_counterterm(param.gs, 'dZgs', 'gs*dZgs')
assign_counterterm(param.g1, 'dZg1', 'g1*dZg1')
assign_counterterm(param.g2, 'dZg2', 'g2*dZg2')

auto_assign_ct_particle({})

###################################
#  SM Optimizations for Fermions  #
###################################

doublets = [(P.nu_e, P.e__minus__), (P.nu_mu, P.mu__minus__),
            (P.nu_tau, P.tau__minus__), (P.u, P.d), (P.c, P.s), (P.t, P.b)]

generations = [(P.e__minus__, P.mu__minus__, P.tau__minus__),
               (P.d, P.s, P.b)]
