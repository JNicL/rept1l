from __future__ import print_function

import os
from rept1l.modeltests.modeltest_utils.auto_model import GenModel, GenBareModel
import rept1l.modeltests.SMSP_tests.smsp_setup as smsp

def smsp_model(outputpath):

  class GenSMSP(GenModel):
    driverpath = smsp.driver_smsp_path
    threads = 8
    ct_expansion = True
    pre_ct = 'smspexp.txt'
    verbose = True

    def renormalize_model(self, **kwargs):
      self.renormalize_smsp('-o', 'particles', 'couplings', **kwargs)

      self.renormalize_qcd('-o', 'particles', 'alphaS', '-nf', '6', **kwargs)

      self.register_ct()

    def compute_rational(self, **kwargs):
      cls = self.__class__
      self.run_r2(vertices_file='verts2.txt', np=2, nc=True, npc=True,
                  threads=cls.threads)
      self.run_r2(vertices_file='verts3.txt', np=3, nc=True, npc=True,
                  threads=cls.threads)
      self.run_r2(vertices_file='verts4.txt', np=4, nc=True, npc=True,
                  threads=cls.threads)

  GenSMSP.outputpath = outputpath
  GenSMSP(bfm=False)


if __name__ == "__main__":
  smsp_model('/home/nick/SMSP_2.2.3')
