# This file was automatically created by FeynRules 2.3.29
# Mathematica version: 9.0 for Linux x86 (64-bit) (November 20, 2012)
# Date: Thu 24 May 2018 12:12:26



from object_library import all_parameters, Parameter


from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot

# This is a default parameter object representing 0.
ZERO = Parameter(name = 'ZERO',
                 nature = 'internal',
                 type = 'real',
                 value = '0.0',
                 texname = '0')

# User-defined parameters.
cabi = Parameter(name = 'cabi',
                 nature = 'external',
                 type = 'real',
                 value = 0.227736,
                 texname = '\\theta _c',
                 lhablock = 'CKMBLOCK',
                 lhacode = [ 1 ])

aEW = Parameter(name = 'aEW',
                nature = 'external',
                type = 'real',
                value = 0.007818608287724784,
                texname = '\\alpha _{\\text{EW}}',
                lhablock = 'SMINPUTS',
                lhacode = [ 1 ])

aS = Parameter(name = 'aS',
               nature = 'external',
               type = 'real',
               value = 0.1184,
               texname = '\\alpha _s',
               lhablock = 'SMINPUTS',
               lhacode = [ 3 ])

ymdo = Parameter(name = 'ymdo',
                 nature = 'external',
                 type = 'real',
                 value = 0.00504,
                 texname = '\\text{ymdo}',
                 lhablock = 'YUKAWA',
                 lhacode = [ 1 ])

ymup = Parameter(name = 'ymup',
                 nature = 'external',
                 type = 'real',
                 value = 0.00255,
                 texname = '\\text{ymup}',
                 lhablock = 'YUKAWA',
                 lhacode = [ 2 ])

yms = Parameter(name = 'yms',
                nature = 'external',
                type = 'real',
                value = 0.101,
                texname = '\\text{yms}',
                lhablock = 'YUKAWA',
                lhacode = [ 3 ])

ymc = Parameter(name = 'ymc',
                nature = 'external',
                type = 'real',
                value = 1.27,
                texname = '\\text{ymc}',
                lhablock = 'YUKAWA',
                lhacode = [ 4 ])

ymb = Parameter(name = 'ymb',
                nature = 'external',
                type = 'real',
                value = 4.7,
                texname = '\\text{ymb}',
                lhablock = 'YUKAWA',
                lhacode = [ 5 ])

ymt = Parameter(name = 'ymt',
                nature = 'external',
                type = 'real',
                value = 172,
                texname = '\\text{ymt}',
                lhablock = 'YUKAWA',
                lhacode = [ 6 ])

yme = Parameter(name = 'yme',
                nature = 'external',
                type = 'real',
                value = 0.000511,
                texname = '\\text{yme}',
                lhablock = 'YUKAWA',
                lhacode = [ 11 ])

ymm = Parameter(name = 'ymm',
                nature = 'external',
                type = 'real',
                value = 0.10566,
                texname = '\\text{ymm}',
                lhablock = 'YUKAWA',
                lhacode = [ 13 ])

ymtau = Parameter(name = 'ymtau',
                  nature = 'external',
                  type = 'real',
                  value = 1.777,
                  texname = '\\text{ymtau}',
                  lhablock = 'YUKAWA',
                  lhacode = [ 15 ])

kL = Parameter(name = 'kL',
               nature = 'external',
               type = 'real',
               value = 0.1,
               texname = '\\text{kL}',
               lhablock = 'FRBlock',
               lhacode = [ 1 ])

kR = Parameter(name = 'kR',
               nature = 'external',
               type = 'real',
               value = 1.,
               texname = '\\text{kR}',
               lhablock = 'FRBlock',
               lhacode = [ 2 ])

CRq1x1 = Parameter(name = 'CRq1x1',
                   nature = 'external',
                   type = 'real',
                   value = 1.,
                   texname = '\\text{CRq1x1}',
                   lhablock = 'FRBlock2',
                   lhacode = [ 1, 1 ])

CRq1x2 = Parameter(name = 'CRq1x2',
                   nature = 'external',
                   type = 'real',
                   value = 0.225773,
                   texname = '\\text{CRq1x2}',
                   lhablock = 'FRBlock2',
                   lhacode = [ 1, 2 ])

CRq1x3 = Parameter(name = 'CRq1x3',
                   nature = 'external',
                   type = 'real',
                   value = 0.,
                   texname = '\\text{CRq1x3}',
                   lhablock = 'FRBlock2',
                   lhacode = [ 1, 3 ])

CRq2x1 = Parameter(name = 'CRq2x1',
                   nature = 'external',
                   type = 'real',
                   value = 0,
                   texname = '\\text{CRq2x1}',
                   lhablock = 'FRBlock2',
                   lhacode = [ 2, 1 ])

CRq2x2 = Parameter(name = 'CRq2x2',
                   nature = 'external',
                   type = 'real',
                   value = 0.97418,
                   texname = '\\text{CRq2x2}',
                   lhablock = 'FRBlock2',
                   lhacode = [ 2, 2 ])

CRq2x3 = Parameter(name = 'CRq2x3',
                   nature = 'external',
                   type = 'real',
                   value = 0.,
                   texname = '\\text{CRq2x3}',
                   lhablock = 'FRBlock2',
                   lhacode = [ 2, 3 ])

CRq3x1 = Parameter(name = 'CRq3x1',
                   nature = 'external',
                   type = 'real',
                   value = 0,
                   texname = '\\text{CRq3x1}',
                   lhablock = 'FRBlock2',
                   lhacode = [ 3, 1 ])

CRq3x2 = Parameter(name = 'CRq3x2',
                   nature = 'external',
                   type = 'real',
                   value = 0.,
                   texname = '\\text{CRq3x2}',
                   lhablock = 'FRBlock2',
                   lhacode = [ 3, 2 ])

CRq3x3 = Parameter(name = 'CRq3x3',
                   nature = 'external',
                   type = 'real',
                   value = 1.,
                   texname = '\\text{CRq3x3}',
                   lhablock = 'FRBlock2',
                   lhacode = [ 3, 3 ])

CRl1x1 = Parameter(name = 'CRl1x1',
                   nature = 'external',
                   type = 'real',
                   value = 1.,
                   texname = '\\text{CRl1x1}',
                   lhablock = 'FRBlock3',
                   lhacode = [ 1, 1 ])

CRl1x2 = Parameter(name = 'CRl1x2',
                   nature = 'external',
                   type = 'real',
                   value = 0.,
                   texname = '\\text{CRl1x2}',
                   lhablock = 'FRBlock3',
                   lhacode = [ 1, 2 ])

CRl1x3 = Parameter(name = 'CRl1x3',
                   nature = 'external',
                   type = 'real',
                   value = 0.,
                   texname = '\\text{CRl1x3}',
                   lhablock = 'FRBlock3',
                   lhacode = [ 1, 3 ])

CRl2x1 = Parameter(name = 'CRl2x1',
                   nature = 'external',
                   type = 'real',
                   value = 0.,
                   texname = '\\text{CRl2x1}',
                   lhablock = 'FRBlock3',
                   lhacode = [ 2, 1 ])

CRl2x2 = Parameter(name = 'CRl2x2',
                   nature = 'external',
                   type = 'real',
                   value = 1.,
                   texname = '\\text{CRl2x2}',
                   lhablock = 'FRBlock3',
                   lhacode = [ 2, 2 ])

CRl2x3 = Parameter(name = 'CRl2x3',
                   nature = 'external',
                   type = 'real',
                   value = 0.,
                   texname = '\\text{CRl2x3}',
                   lhablock = 'FRBlock3',
                   lhacode = [ 2, 3 ])

CRl3x1 = Parameter(name = 'CRl3x1',
                   nature = 'external',
                   type = 'real',
                   value = 0.,
                   texname = '\\text{CRl3x1}',
                   lhablock = 'FRBlock3',
                   lhacode = [ 3, 1 ])

CRl3x2 = Parameter(name = 'CRl3x2',
                   nature = 'external',
                   type = 'real',
                   value = 0.,
                   texname = '\\text{CRl3x2}',
                   lhablock = 'FRBlock3',
                   lhacode = [ 3, 2 ])

CRl3x3 = Parameter(name = 'CRl3x3',
                   nature = 'external',
                   type = 'real',
                   value = 1.,
                   texname = '\\text{CRl3x3}',
                   lhablock = 'FRBlock3',
                   lhacode = [ 3, 3 ])

CLq1x1 = Parameter(name = 'CLq1x1',
                   nature = 'external',
                   type = 'real',
                   value = 0.97418,
                   texname = '\\text{CLq1x1}',
                   lhablock = 'FRBlock4',
                   lhacode = [ 1, 1 ])

CLq1x2 = Parameter(name = 'CLq1x2',
                   nature = 'external',
                   type = 'real',
                   value = 0.225773,
                   texname = '\\text{CLq1x2}',
                   lhablock = 'FRBlock4',
                   lhacode = [ 1, 2 ])

CLq1x3 = Parameter(name = 'CLq1x3',
                   nature = 'external',
                   type = 'real',
                   value = 0.,
                   texname = '\\text{CLq1x3}',
                   lhablock = 'FRBlock4',
                   lhacode = [ 1, 3 ])

CLq2x1 = Parameter(name = 'CLq2x1',
                   nature = 'external',
                   type = 'real',
                   value = -0.225773,
                   texname = '\\text{CLq2x1}',
                   lhablock = 'FRBlock4',
                   lhacode = [ 2, 1 ])

CLq2x2 = Parameter(name = 'CLq2x2',
                   nature = 'external',
                   type = 'real',
                   value = 0.97418,
                   texname = '\\text{CLq2x2}',
                   lhablock = 'FRBlock4',
                   lhacode = [ 2, 2 ])

CLq2x3 = Parameter(name = 'CLq2x3',
                   nature = 'external',
                   type = 'real',
                   value = 0.,
                   texname = '\\text{CLq2x3}',
                   lhablock = 'FRBlock4',
                   lhacode = [ 2, 3 ])

CLq3x1 = Parameter(name = 'CLq3x1',
                   nature = 'external',
                   type = 'real',
                   value = 0.,
                   texname = '\\text{CLq3x1}',
                   lhablock = 'FRBlock4',
                   lhacode = [ 3, 1 ])

CLq3x2 = Parameter(name = 'CLq3x2',
                   nature = 'external',
                   type = 'real',
                   value = 0.,
                   texname = '\\text{CLq3x2}',
                   lhablock = 'FRBlock4',
                   lhacode = [ 3, 2 ])

CLq3x3 = Parameter(name = 'CLq3x3',
                   nature = 'external',
                   type = 'real',
                   value = 1.,
                   texname = '\\text{CLq3x3}',
                   lhablock = 'FRBlock4',
                   lhacode = [ 3, 3 ])

CLl1x1 = Parameter(name = 'CLl1x1',
                   nature = 'external',
                   type = 'real',
                   value = 1.,
                   texname = '\\text{CLl1x1}',
                   lhablock = 'FRBlock5',
                   lhacode = [ 1, 1 ])

CLl1x2 = Parameter(name = 'CLl1x2',
                   nature = 'external',
                   type = 'real',
                   value = 0.,
                   texname = '\\text{CLl1x2}',
                   lhablock = 'FRBlock5',
                   lhacode = [ 1, 2 ])

CLl1x3 = Parameter(name = 'CLl1x3',
                   nature = 'external',
                   type = 'real',
                   value = 0.,
                   texname = '\\text{CLl1x3}',
                   lhablock = 'FRBlock5',
                   lhacode = [ 1, 3 ])

CLl2x1 = Parameter(name = 'CLl2x1',
                   nature = 'external',
                   type = 'real',
                   value = 0.,
                   texname = '\\text{CLl2x1}',
                   lhablock = 'FRBlock5',
                   lhacode = [ 2, 1 ])

CLl2x2 = Parameter(name = 'CLl2x2',
                   nature = 'external',
                   type = 'real',
                   value = 1.,
                   texname = '\\text{CLl2x2}',
                   lhablock = 'FRBlock5',
                   lhacode = [ 2, 2 ])

CLl2x3 = Parameter(name = 'CLl2x3',
                   nature = 'external',
                   type = 'real',
                   value = 0.,
                   texname = '\\text{CLl2x3}',
                   lhablock = 'FRBlock5',
                   lhacode = [ 2, 3 ])

CLl3x1 = Parameter(name = 'CLl3x1',
                   nature = 'external',
                   type = 'real',
                   value = 0.,
                   texname = '\\text{CLl3x1}',
                   lhablock = 'FRBlock5',
                   lhacode = [ 3, 1 ])

CLl3x2 = Parameter(name = 'CLl3x2',
                   nature = 'external',
                   type = 'real',
                   value = 0.,
                   texname = '\\text{CLl3x2}',
                   lhablock = 'FRBlock5',
                   lhacode = [ 3, 2 ])

CLl3x3 = Parameter(name = 'CLl3x3',
                   nature = 'external',
                   type = 'real',
                   value = 1.,
                   texname = '\\text{CLl3x3}',
                   lhablock = 'FRBlock5',
                   lhacode = [ 3, 3 ])

MZ = Parameter(name = 'MZ',
               nature = 'external',
               type = 'real',
               value = 91.1876,
               texname = '\\text{MZ}',
               lhablock = 'MASS',
               lhacode = [ 23 ])

MW = Parameter(name = 'MW',
               nature = 'external',
               type = 'real',
               value = 80.399,
               texname = '\\text{MW}',
               lhablock = 'MASS',
               lhacode = [ 24 ])

MWp = Parameter(name = 'MWp',
                nature = 'external',
                type = 'real',
                value = 2000.,
                texname = '\\text{MWp}',
                lhablock = 'MASS',
                lhacode = [ 9000001 ])

ME = Parameter(name = 'ME',
               nature = 'external',
               type = 'real',
               value = 0.000511,
               texname = '\\text{ME}',
               lhablock = 'MASS',
               lhacode = [ 11 ])

MM = Parameter(name = 'MM',
               nature = 'external',
               type = 'real',
               value = 0.10566,
               texname = '\\text{MM}',
               lhablock = 'MASS',
               lhacode = [ 13 ])

MTA = Parameter(name = 'MTA',
                nature = 'external',
                type = 'real',
                value = 1.777,
                texname = '\\text{MTA}',
                lhablock = 'MASS',
                lhacode = [ 15 ])

MU = Parameter(name = 'MU',
               nature = 'external',
               type = 'real',
               value = 0.00255,
               texname = 'M',
               lhablock = 'MASS',
               lhacode = [ 2 ])

MC = Parameter(name = 'MC',
               nature = 'external',
               type = 'real',
               value = 1.27,
               texname = '\\text{MC}',
               lhablock = 'MASS',
               lhacode = [ 4 ])

MT = Parameter(name = 'MT',
               nature = 'external',
               type = 'real',
               value = 172,
               texname = '\\text{MT}',
               lhablock = 'MASS',
               lhacode = [ 6 ])

MD = Parameter(name = 'MD',
               nature = 'external',
               type = 'real',
               value = 0.00504,
               texname = '\\text{MD}',
               lhablock = 'MASS',
               lhacode = [ 1 ])

MS = Parameter(name = 'MS',
               nature = 'external',
               type = 'real',
               value = 0.101,
               texname = '\\text{MS}',
               lhablock = 'MASS',
               lhacode = [ 3 ])

MB = Parameter(name = 'MB',
               nature = 'external',
               type = 'real',
               value = 4.7,
               texname = '\\text{MB}',
               lhablock = 'MASS',
               lhacode = [ 5 ])

MH = Parameter(name = 'MH',
               nature = 'external',
               type = 'real',
               value = 120,
               texname = '\\text{MH}',
               lhablock = 'MASS',
               lhacode = [ 25 ])

WZ = Parameter(name = 'WZ',
               nature = 'external',
               type = 'real',
               value = 2.4952,
               texname = '\\text{WZ}',
               lhablock = 'DECAY',
               lhacode = [ 23 ])

WW = Parameter(name = 'WW',
               nature = 'external',
               type = 'real',
               value = 2.085,
               texname = '\\text{WW}',
               lhablock = 'DECAY',
               lhacode = [ 24 ])

WWp = Parameter(name = 'WWp',
                nature = 'external',
                type = 'real',
                value = 1.,
                texname = '\\text{WWp}',
                lhablock = 'DECAY',
                lhacode = [ 9000001 ])

WM = Parameter(name = 'WM',
               nature = 'external',
               type = 'real',
               value = 0.01,
               texname = '\\text{WM}',
               lhablock = 'DECAY',
               lhacode = [ 13 ])

WTA = Parameter(name = 'WTA',
                nature = 'external',
                type = 'real',
                value = 0.01,
                texname = '\\text{WTA}',
                lhablock = 'DECAY',
                lhacode = [ 15 ])

WC = Parameter(name = 'WC',
               nature = 'external',
               type = 'real',
               value = 0.01,
               texname = '\\text{WC}',
               lhablock = 'DECAY',
               lhacode = [ 4 ])

WT = Parameter(name = 'WT',
               nature = 'external',
               type = 'real',
               value = 1.50833649,
               texname = '\\text{WT}',
               lhablock = 'DECAY',
               lhacode = [ 6 ])

WB = Parameter(name = 'WB',
               nature = 'external',
               type = 'real',
               value = 0.01,
               texname = '\\text{WB}',
               lhablock = 'DECAY',
               lhacode = [ 5 ])

WH = Parameter(name = 'WH',
               nature = 'external',
               type = 'real',
               value = 0.00575308848,
               texname = '\\text{WH}',
               lhablock = 'DECAY',
               lhacode = [ 25 ])

cb = Parameter(name = 'cb',
               nature = 'internal',
               type = 'real',
               value = 'cmath.cos(cabi)',
               texname = '\\text{cb}')

sb = Parameter(name = 'sb',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sin(cabi)',
               texname = '\\text{sb}')

sw2 = Parameter(name = 'sw2',
                nature = 'internal',
                type = 'real',
                value = '1 - MW**2/MZ**2',
                texname = '\\text{sw2}')

ee = Parameter(name = 'ee',
               nature = 'internal',
               type = 'real',
               value = '2*cmath.sqrt(aEW)*cmath.sqrt(cmath.pi)',
               texname = 'e')

gs = Parameter(name = 'gs',
               nature = 'internal',
               type = 'real',
               value = '2*cmath.sqrt(aS)*cmath.sqrt(cmath.pi)',
               texname = 'g_s')

cw = Parameter(name = 'cw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(1 - sw2)',
               texname = 'c_w')

sw = Parameter(name = 'sw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(sw2)',
               texname = 's_w')

g1 = Parameter(name = 'g1',
               nature = 'internal',
               type = 'real',
               value = '-ee/(2.*cw)',
               texname = 'g_1')

gL = Parameter(name = 'gL',
               nature = 'internal',
               type = 'real',
               value = '(ee*kL)/sw',
               texname = '\\text{gL}')

gR = Parameter(name = 'gR',
               nature = 'internal',
               type = 'real',
               value = '(ee*kR)/sw',
               texname = '\\text{gR}')

gw = Parameter(name = 'gw',
               nature = 'internal',
               type = 'real',
               value = 'ee/sw',
               texname = 'g_w')

vev = Parameter(name = 'vev',
                nature = 'internal',
                type = 'real',
                value = '(2*MW*sw)/ee',
                texname = '\\text{vev}')

lam = Parameter(name = 'lam',
                nature = 'internal',
                type = 'real',
                value = '(2*MH**2)/vev**2',
                texname = '\\text{lam}')

yb = Parameter(name = 'yb',
               nature = 'internal',
               type = 'real',
               value = '(ymb*cmath.sqrt(2))/vev',
               texname = '\\text{yb}')

yc = Parameter(name = 'yc',
               nature = 'internal',
               type = 'real',
               value = '(ymc*cmath.sqrt(2))/vev',
               texname = '\\text{yc}')

ydo = Parameter(name = 'ydo',
                nature = 'internal',
                type = 'real',
                value = '(ymdo*cmath.sqrt(2))/vev',
                texname = '\\text{ydo}')

ye = Parameter(name = 'ye',
               nature = 'internal',
               type = 'real',
               value = '(yme*cmath.sqrt(2))/vev',
               texname = '\\text{ye}')

ym = Parameter(name = 'ym',
               nature = 'internal',
               type = 'real',
               value = '(ymm*cmath.sqrt(2))/vev',
               texname = '\\text{ym}')

ys = Parameter(name = 'ys',
               nature = 'internal',
               type = 'real',
               value = '(yms*cmath.sqrt(2))/vev',
               texname = '\\text{ys}')

yt = Parameter(name = 'yt',
               nature = 'internal',
               type = 'real',
               value = '(ymt*cmath.sqrt(2))/vev',
               texname = '\\text{yt}')

ytau = Parameter(name = 'ytau',
                 nature = 'internal',
                 type = 'real',
                 value = '(ymtau*cmath.sqrt(2))/vev',
                 texname = '\\text{ytau}')

yup = Parameter(name = 'yup',
                nature = 'internal',
                type = 'real',
                value = '(ymup*cmath.sqrt(2))/vev',
                texname = '\\text{yup}')

muH2 = Parameter(name = 'muH2',
                 nature = 'internal',
                 type = 'real',
                 value = '(lam*vev**2)/4.',
                 texname = '\\text{muH2}')

