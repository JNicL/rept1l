#==============================================================================#
#                                vertices_ct.py                                #
#==============================================================================#
""" Custom counterterm vertices """

#============#
#  Includes  #
#============#

from modelfile import model, TreeInducedCTVertex
P = model.particles
L = model.lorentz
object_library = model.object_library
CTVertex = object_library.CTVertex


import couplings_ct as C
import lorentz_ct as LCT

# TreeInducedCTVertex/CTVertex instances are defined similar to normal vertices
# in FeynRules. You can use all couplings/lorentz from your model and additional
# couplings from couplings_ct and lorentz_ct.

#V_ZG0 = TreeInducedCTVertex(name='V_ZG0',
                            #particles=[P.Z, P.G0],
                            #color=['1'],
                            #type='treeinducedct',
                            #loop_particles='N',
                            #lorentz=[LCT.VS1, LCT.VS1],
                            #couplings={(0, 0): C.GC_MZ, (0, 1): C.GC_ZG0T})
