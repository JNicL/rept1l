#==============================================================================#
#                                   model.py                                   #
#==============================================================================#

#============#
#  Includes  #
#============#

import os
from .modelfile import model

#===========#
#  Globals  #
#===========#

param = model.parameters
P = model.particles

#=============================#
#  External Parameter Orders  #
#=============================#

# You have to declarethe order of external parameters if the system cannot
# derive them
parameter_orders = {param.ct: {'FT4': 1},
                    param.cv: {'FV4': 1},
                    param.eta: {'YUK': 1}}

#Ex.
#parameter_orders = {param.aEW: {'QED': 2},
#                    param.aS: {'QCD': 2},
#                    param.ee: {'QED': 1},
#                    param.t1: {'QED': -1},
#                    param.t2: {'QED': -1},
#                    param.dvev1: {'QED': -1},
#                    param.dvev2: {'QED': -1},
#                    param.G: {'QCD': 1}}


#==============================#
#  Declare tadpole parameters  #
#==============================#

# the tadpole parameter are useful to declare parameter which are zero at
# Tree-Level, but have a one-loop expansion. Can be used not only for tadpoles
# Interally, the alogrithm then knows that it is safe to set the values to zero
# after renormalization, which optimizes ct expressions.

#===============================#
#  Fermion mass regularization  #
#===============================#

# You can declare fermions to be massless, but mass regularized by setting the
# massregularization attribute to True (needs to be done for particle and
# antiparticle)
#P.e__minus__.massregularization = True
#P.e__plus__.massregularization = True

# SM optimizations
#doublet_partners = {"P_ve": "P_eMinus", "P_vm": "P_muMinus", "P_vt":
                    #"P_taMinus", "P_u": "P_d", "P_c": "P_s", "P_t": "P_b"}

#flavours = [["P_eMinus", "P_muMinus", "P_taMinus"],
            #["P_u", "P_c", "P_t"]]
