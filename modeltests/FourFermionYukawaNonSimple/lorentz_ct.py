#==============================================================================#
#                                lorentz_ct.py                                 #
#==============================================================================#
""" Additional Lorentz structures needed for 2-point vertices.  """

# Normally, no further lorentz structures should be needed for NLO as they can
# be derived from the tree-level structures. Otherwise you can add new
# structures here.

from .modelfile import model
object_library = getattr(model, 'object_library')

Lorentz = object_library.Lorentz

VV1 = Lorentz(name='VV1',
              spins=[3, 3],
              structure='Metric(1,2)')

VV2 = Lorentz(name='VV2',
              spins=[3, 3],
              structure='P(-1,1)*P(-1,1)*Metric(1,2)')

VV3 = Lorentz(name='VV3',
              spins=[3, 3],
              structure='P(1,1)*P(2,1)')

VV4 = Lorentz(name='VV4',
              spins=[3, 3],
              structure='-P(1,1)*P(2,1) + P(-1,1)*P(-1,1)*Metric(1,2)')

S1 = Lorentz(name='S1',
             spins=[1],
             structure='1')

VS1 = Lorentz(name='VS1',
              spins=[3, 1],
              structure='P(1,1)')

SS1 = Lorentz(name='SS1',
              spins=[1, 1],
              structure='1')

SS2 = Lorentz(name='SS2',
              spins=[1, 1],
              structure='P(-1,1)*P(-1,1)')

FF1 = Lorentz(name='FF1',
              spins=[2, 2],
              structure='P(-1,1)*GammaM(-1,2,1)')

FF2 = Lorentz(name='FF2',
              spins=[2, 2],
              structure='P(-1,1)*GammaP(-1,2,1)')

FF3 = Lorentz(name='FF3',
              spins=[2, 2],
              structure='ProjM(2,1)')

FF4 = Lorentz(name='FF4',
              spins=[2, 2],
              structure='ProjP(2,1)')
