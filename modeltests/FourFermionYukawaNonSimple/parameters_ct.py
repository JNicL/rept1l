#==============================================================================#
#                               parameters_ct.py                               #
#==============================================================================#

#============#
#  Includes  #
#============#

from .modelfile import CTParameter, model

#===========#
#  Globals  #
#===========#

object_library = model.object_library
Parameter = object_library.Parameter
parameters = model.parameters

#==============================================================================#
#                           coupling renormalization                           #
#==============================================================================#

# define ct parameter:

dcv = Parameter(name='dcv',
                nature='external',
                type='real',
                value='dcv',
                texname='dcv',
                lhablock='CTs',
                lhacode=[1])

dct = Parameter(name='dct',
                nature='external',
                type='real',
                value='dct',
                texname='dct',
                lhablock='CTs',
                lhacode=[1])

deta = Parameter(name='deta',
                 nature='external',
                 type='real',
                 value='deta',
                 texname='deta',
                 lhablock='CTs',
                 lhacode=[1])

parameters.ct.counterterm = dct
parameters.cv.counterterm = dcv
parameters.eta.counterterm = deta

#==============================================================================#
#                                  particles                                   #
#==============================================================================#

from .auto_parameters_ct import auto_assign_ct_particle
auto_assign_ct_particle()
