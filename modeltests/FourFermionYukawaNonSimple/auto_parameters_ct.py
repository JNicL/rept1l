#==============================================================================#
#                            auto_parameters_ct.py                             #
#==============================================================================#

#============#
#  Includes  #
#============#

import rept1l.combinatorics as comb
from rept1l.helper_lib import (get_anti_particle, is_anti_particle,
                               export_particle_name)

from .modelfile import model, TreeInducedCTVertex
from . import couplings_ct as C
from . import lorentz_ct as LCT

#===========#
#  Globals  #
#===========#

parameters = model.parameters
object_library = model.object_library
model_objects = model.object_library
Parameter = object_library.Parameter
Coupling = object_library.Coupling

def is_goldstone_boson(particle):
  """ Returns `True` if `particle` is a goldstone boson else `False`. """
  if particle.spin == 1:
    if hasattr(particle, 'goldstone') and particle.goldstone is True:
      ret = True
    else:
      ret = False

    # check whether the antiparticle is a goldstone boson as FeynRules object
    # library does not copy the goldstone attribute to the generation of
    # antiparticles.
    if ret is False:
      aparticle = get_anti_particle(particle, model_objects.all_particles)
      if aparticle is not particle:
        if hasattr(aparticle, 'goldstone') and aparticle.goldstone is True:
          ret = True
        else:
          ret = False
  else:
    ret = False

  return ret

def get_yukawa_parameter(all_parameters, all_masses):
  # external values need to be initialized first - user defines their value
  parameters = (p for p in all_parameters
                if p.nature == 'external' and
                p.name not in [v.name for v in all_masses])

  masses_vals = [u.value for u in all_masses]
  yukawa_params = {u: all_masses[masses_vals.index(u.value)] for u in parameters
                   if u.value in masses_vals and u.value != 0.0}
  return yukawa_params

def sort_particle_antiparticle(particle):
  """ Returns the particle and antiparticle of `particle`"""
  if is_anti_particle(particle.name):
    particle_tmp = get_anti_particle(particle, model_objects.all_particles)
    particle_tmp_anti = particle
  else:
    particle_tmp = particle
    particle_tmp_anti = get_anti_particle(particle,
                                          model_objects.all_particles)
  return particle_tmp, particle_tmp_anti

def gen_mass_ctparameter(particle):
  """ Generatres the mass ct parameter associated to the mass of the particle
  `particle`. By convention, we define for spin 0 and 1 the massct as quadratic
  expression, i.e. we substitute `dm` with `dm2/(2*m)` where m is the mass.

  :param return: Mass CT
  :type  return: FeynRules Parameter
  """

  # define redundant stuff for FeynRules
  nature = 'external'
  type = 'real'
  lhablock = 'CTs'
  lhacode = [2]

  mass_name = particle.mass.name
  # Fermions
  if particle.spin == 2:
    mass_ct_name = 'd' + mass_name
    mass_ct_value = mass_ct_name
  # Bosons
  else:
    # define mass ct as quadratics
    mass_ct_name = 'd' + mass_name + '2'
    mass_ct_value = mass_ct_name + '/(2 * ' + mass_name + ')'

  mass_ct_texname = mass_ct_name

  mct = Parameter(name=mass_ct_name,
                  nature=nature,
                  type=type,
                  value=mass_ct_value,
                  texname=mass_ct_texname,
                  lhablock=lhablock,
                  lhacode=lhacode)
  return mct

def gen_tadpole_ctparameter(particle):
  """ Generates a tadpole ct parameter

  :param return: Tadpole CT
  :type  return: FeynRules Parameter
  """

  # define redundant stuff for FeynRules
  nature = 'external'
  type = 'real'
  lhablock = 'CTs'
  lhacode = [2]

  tadpole_ct_value = particle.tadpole
  pname = export_particle_name(particle.name).strip()
  tadpole_ct_name = 'T' + pname + pname

  tadpole_ct_texname = tadpole_ct_name

  tct = Parameter(name=tadpole_ct_name,
                  nature=nature,
                  type=type,
                  value=tadpole_ct_value,
                  texname=tadpole_ct_name,
                  lhablock=lhablock,
                  lhacode=lhacode)
  return tct

def gen_mass_ctcoupling(particle, mass_name):
  """ Generates the mass ct coupling of the particle.  Scalars and vectors have
  quadratic mass terms whereas for fermions the mass terms are linear. """

  # since the order of the counterterms are determined by renormalization
  # condition, the vertex is intitialized with 0 order in the coupling
  # constants.
  orders_zero = {u.name: 0 for u in model_objects.all_orders}
  induced_mass_name = 'GC_' + mass_name
  if particle.spin == 1:
    indeced_mass_value = '-I*' + mass_name + '*' + mass_name
  if particle.spin == 2:
    indeced_mass_value = '-I*' + mass_name
  elif particle.spin == 3:
    indeced_mass_value = 'I*' + mass_name + '*' + mass_name
  GC = Coupling(name=induced_mass_name,
                value=indeced_mass_value,
                order=orders_zero)
  return GC


def gen_vertex_structure(particle, massct=None, tadpole=None):
  """ Generates the vertex structure (Feynman Rule) for the induced two-porint
  vertex. Particles of different spin have different structures:

    - For scalars the form reads
      I dZ (p^2 - M^2) - i dM
      This form can be induced by
      I (p^2 - M^2) = I * SS2 + (-I*M^2)*SS1

    - For fermions the form can be induced by
      -I*(\slash p^- + \slash p^+) - I*M*(w^- + w^+)

    - For Vectorbosons we assume the form
      -g^\mu\nu [I dZ (p^2 - M^2) - I dM]
      which is true in the Feynman t'Hooft
      Gauge when the gauge-fixing is renormalized
      This form can be induced by
      -I g^\mu\nu (p^2 - M^2) = (-I)*VV2 + (I*M^2)*VV1

    """
  if particle.spin == 1:
    lorentz = [LCT.SS2]
    couplings = {(0, 0): C.GC_I}
    if massct:
      lorentz.append(LCT.SS1)
      couplings[(0, len(lorentz)-1)] = massct

    if tadpole:
      lorentz.append(LCT.SS1)
      couplings[(0, len(lorentz)-1)] = tadpole

    #if massct is not None:
      #lorentz = [LCT.SS1, LCT.SS2]
      #couplings = {(0, 0): massct, (0, 1): C.GC_I}
    #else:
      #lorentz = [LCT.SS2]
      #couplings = {(0, 0): C.GC_I}

  elif particle.spin == 2:
    if massct is not None:
      lorentz = [LCT.FF1, LCT.FF2, LCT.FF3, LCT.FF4]
      couplings = {(0, 0): C.GC_UNIT, (0, 1): C.GC_UNIT,
                   (0, 2): massct, (0, 3): massct}
    else:
      lorentz = [LCT.FF1, LCT.FF2]
      couplings = {(0, 0): C.GC_UNIT, (0, 1): C.GC_UNIT}
  elif particle.spin == 3:
    if massct is not None:
      lorentz = [LCT.VV4, LCT.VV1]
      couplings = {(0, 0): C.GC_UNIT, (0, 1): massct}
    else:
      lorentz = [LCT.VV4]
      couplings = {(0, 0): C.GC_UNIT}
  return lorentz, couplings

def gen_wavefunction(particle, mixing_particles=[]):
  """ Generates the wavefunction expression and assigns it to the particle. """
  if particle.spin == 2:
    fermion_minus_rule = (comb.ProjM, ['Contraction', 'Particle'])
    fermion_plus_rule = (comb.ProjP, ['Contraction', 'Particle'])
    antifermion_minus_rule = (comb.ProjP, ['Particle', 'Contraction'])
    antifermion_plus_rule = (comb.ProjM, ['Particle', 'Contraction'])
    particle_tmp, particle_tmp_anti = sort_particle_antiparticle(particle)
    pname = export_particle_name(particle_tmp.name).strip()
    wname = 'dZ' + pname

    dZ = {particle_tmp: {wname + 'L': fermion_minus_rule,
                         wname + 'R': fermion_plus_rule}}
    for mp in mixing_particles:
      mp_tmp, mp_tmp_anti = sort_particle_antiparticle(mp)
      mpname = export_particle_name(mp_tmp.name).strip()
      dZ[mp_tmp] = {wname + mpname + 'L': fermion_minus_rule,
                    wname + mpname + 'R': fermion_plus_rule}
    particle_tmp.counterterm = dZ

    if particle_tmp != particle_tmp_anti:
      dZc = {particle_tmp_anti: {wname + 'L': antifermion_minus_rule,
                                 wname + 'R': antifermion_plus_rule}}
      for mp in mixing_particles:
        mp_tmp, mp_tmp_anti = sort_particle_antiparticle(mp)
        mpname = export_particle_name(mp_tmp.name).strip()
        dZc[mp_tmp_anti] = {wname + mpname + 'L': fermion_minus_rule,
                            wname + mpname + 'R': fermion_plus_rule}
      particle_tmp_anti.counterterm = dZc
  else:
    identity_rule = None
    particle_tmp, particle_tmp_anti = sort_particle_antiparticle(particle)
    pname = export_particle_name(particle_tmp.name).strip()
    wname = 'dZ' + pname
    dZ = {particle_tmp: {wname + '/2': identity_rule}}
    for mp in mixing_particles:
      mp_tmp, mp_tmp_anti = sort_particle_antiparticle(mp)
      mpname = export_particle_name(mp_tmp.name).strip()
      dZ[mp_tmp] = {wname + mpname + '/2': identity_rule}
    particle_tmp.counterterm = dZ

    if particle_tmp != particle_tmp_anti:
      dZc = {particle_tmp_anti: {wname + '/2': identity_rule}}
      for mp in mixing_particles:
        mp_tmp, mp_tmp_anti = sort_particle_antiparticle(mp)
        mpname = export_particle_name(mp_tmp.name).strip()
        dZc[mp_tmp_anti] = {wname + mpname + '/2': identity_rule}
      particle_tmp_anti.counterterm = dZc

def get_twopoint_color(color):
  """ Returns the colorstructure for the twopoint function P -> P if particle P
  is in the representation `color

  :param color: color representation: 1, 2, 3
  :type  color: int
  """
  if color == 1:
    return ['1']
  elif color == 3:
    return ['Identity(1,2)']
  elif color == 8:
    return ['T(1,-1,-2)*T(2,-2,-1)']
  else:
    raise ValueError('Passed color in `get_twopoint_color` can have ' +
                     'values 1, 3, 8 only.color')

def gen_twopoint_ctvertex(particle, massct=None, tadpole=None):
  """ Generates the two-point counterterm vertex for the particle `particle`."""

  # define redundant stuff for FeynRules
  type = 'treeinducedct'
  loop_particles = 'N'

  particle_tmp, particle_tmp_anti = sort_particle_antiparticle(particle)
  # twopoint vertex colorstructure
  color = get_twopoint_color(particle.color)
  # twopoint vertex lorentz structure and couplings
  lorentz, couplings = gen_vertex_structure(particle, massct=massct,
                                            tadpole=tadpole)
  pname = export_particle_name(particle_tmp.name).strip()

  TreeInducedCTVertex(name='V_' + pname + pname,
                      particles=[particle_tmp_anti, particle_tmp],
                      color=color,
                      type=type,
                      loop_particles=loop_particles,
                      lorentz=lorentz,
                      couplings=couplings)

def assign_ct_particle(particle, assign_massct=True,
                       assign_wavefunction=True, mixing_particles=[]):
  """ Generates the counterterm structures for particle `particle`
  :param particle: Particle for which the two-point counterterms should be
                   generated
  :type  particle: FeynRules Particle
  :param: assign_massct: Derive a mass counterterm. Only possible if the
                         particle is massive."""

  mass = particle.mass
  if mass != parameters.ZERO and assign_massct:
    # for yukawa couplings we have to assign the same counterterm as the mass
    # coupling
    all_masses = list(set(p.mass for p in object_library.all_particles))
    yukawa = get_yukawa_parameter(list(object_library.all_parameters),
                                  all_masses)
    mct = gen_mass_ctparameter(particle)
    massct = gen_mass_ctcoupling(particle, particle.mass.name)
    mass.counterterm = mct
    if mass.name in [u.name for u in yukawa.values()]:
      yuk = [u for u, val in yukawa.iteritems()
             if val.name == mass.name][0]
      yuk.counterterm = mct
  else:
    massct = None

  if hasattr(particle, 'tadpole'):
    tadpole = particle.tadpole
  else:
    tadpole = None

  gen_twopoint_ctvertex(particle, massct, tadpole)

  if assign_wavefunction:
    gen_wavefunction(particle, mixing_particles=mixing_particles)


def auto_assign_ct_particle(mixings={}):
  particles = model.particles

  def is_background_field(p):
    if not hasattr(p, 'quantumfield'):
      return True
    else:
      return not p.quantumfield

  fermions = (u for u in object_library.all_particles if u.spin == 2 and
              not is_anti_particle(u.name) and is_background_field(u))
  scalars = (u for u in object_library.all_particles if u.spin == 1 and
             not is_anti_particle(u.name)and is_background_field(u))
  vbosons = (u for u in object_library.all_particles if u.spin == 3 and
             not is_anti_particle(u.name) and is_background_field(u))

  for v in scalars:
    if v in mixings:
      # For goldstone bosons we assume the gauge fixing is not renormalized,
      # thus there is no mass counterterm (even though they have mass)
      if is_goldstone_boson(v):
        assign_massct = False
      else:
        assign_massct = True

      assign_ct_particle(v, mixing_particles=mixings[v],
                         assign_massct=assign_massct)
    elif not hasattr(v, 'goldstone') or v.goldstone is False:
      assign_ct_particle(v)

  for v in fermions:
    if hasattr(v, 'massregularization') and v.massregularization:
      assign_ct_particle(v, assign_massct=False)
    else:
      assign_ct_particle(v)

  for v in vbosons:
    if v in mixings:
      assign_ct_particle(v, mixing_particles=mixings[v])
    else:
      assign_ct_particle(v)

def list_active_counterterms():
  from rept1l.counterterms import Counterterms
  print('Active counterterm parameter:')
  print(', '.join(Counterterms.
                  get_param_ct(object_library.all_parameters).keys()))
  print('Active wavefunctions:')
  print(', '.join(u.name for u in Counterterms.
                  get_wavefunction_ct(object_library.all_particles)))


