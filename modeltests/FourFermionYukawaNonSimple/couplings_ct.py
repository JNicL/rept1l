#==============================================================================#
#                               couplings_ct.py                                #
#==============================================================================#

from .modelfile import model
object_library = getattr(model, 'object_library')
Coupling = object_library.Coupling

# define custom couplings used in ct vertices as follows
GC_UNIT = Coupling(name='GC_UNIT',
                   value='-I',
                   order={'QED': 0})

GC_I = Coupling(name='GC_I',
                value='I',
                order={'QED': 0})

GC_M1 = Coupling(name='GC_M1',
                 value='-1',
                 order={'QED': 0})
