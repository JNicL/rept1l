# This file was automatically created by FeynRules 2.3.19
# Mathematica version: 10.0 for Linux x86 (64-bit) (June 29, 2014)
# Date: Sat 23 Apr 2016 23:06:28


from object_library import all_decays, Decay
import particles as P


Decay_phi = Decay(name = 'Decay_phi',
                  particle = P.phi,
                  partial_widths = {(P.chi,P.chi__tilde__):'((-8*eta**2*mc**2 + 2*eta**2*mp**2)*cmath.sqrt(-4*mc**2*mp**2 + mp**4))/(16.*cmath.pi*abs(mp)**3)'})

