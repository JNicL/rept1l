from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot

GC_1 = Coupling(name='GC_1',
                value='cv*complex(0,1)',
                order={'FV4': 1})

GC_2 = Coupling(name='GC_2',
                value='-cv*complex(0,1)',
                order={'FV4': 1})

GC_3 = Coupling(name='GC_3',
                value='-(eta*complex(0,1))',
                order={'YUK': 1})

GC_4 = Coupling(name='GC_4',
                value='ct*complex(0,1)',
                order={'FT4': 1})

GC_5 = Coupling(name='GC_5',
                value='-ct*complex(0,1)',
                order={'FT4': 1})
