# This file was automatically created by FeynRules 2.3.19
# Mathematica version: 10.0 for Linux x86 (64-bit) (June 29, 2014)
# Date: Sat 23 Apr 2016 23:06:28


from __future__ import division
from object_library import all_particles, Particle
import parameters as Param

import propagators as Prop

chi = Particle(pdg_code = 9000001,
               name = 'chi',
               antiname = 'chi~',
               spin = 2,
               color = 1,
               mass = Param.mc,
               width = Param.ZERO,
               texname = 'chi',
               antitexname = 'chi~',
               charge = 0)

chi__tilde__ = chi.anti()

phi = Particle(pdg_code = 9000002,
               name = 'phi',
               antiname = 'phi',
               spin = 1,
               color = 1,
               mass = Param.mp,
               width = Param.ZERO,
               texname = 'phi',
               antitexname = 'phi',
               charge = 0)

