# This file was automatically created by FeynRules 2.3.19
# Mathematica version: 10.0 for Linux x86 (64-bit) (June 29, 2014)
# Date: Sat 23 Apr 2016 23:06:28


from object_library import all_lorentz, Lorentz

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot
try:
   import form_factors as ForFac
except ImportError:
   pass


FFS1 = Lorentz(name='FFS1',
               spins=[2, 2, 1],
               structure='Identity(2,1)')

FFFF1 = Lorentz(name='FFFF1',
                spins=[2, 2, 2, 2],
                structure='Gamma(-1,2,1)*Gamma(-1,4,3)')

FFFF2 = Lorentz(name='FFFF2',
                spins=[2, 2, 2, 2],
                structure='Gamma(-1,2,3)*Gamma(-1,4,1)')

FFFF3 = Lorentz(name='FFFF3',
                spins=[2, 2, 2, 2],
                structure='Sigma(-1,-2,2,1)*Sigma(-1,-2,4,3)')

FFFF4 = Lorentz(name='FFFF4',
                spins=[2, 2, 2, 2],
                structure='Sigma(-1,-2,2,3)*Sigma(-1,-2,4,1)')
