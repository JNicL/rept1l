# This file was automatically created by FeynRules 2.3.19
# Mathematica version: 10.0 for Linux x86 (64-bit) (June 29, 2014)
# Date: Sat 23 Apr 2016 23:06:28

from object_library import all_parameters, Parameter

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot

# This is a default parameter object representing 0.
ZERO = Parameter(name='ZERO',
                 nature='internal',
                 type='real',
                 value='0.0',
                 texname='0')

# User-defined parameters.
cv = Parameter(name='cv',
               nature='external',
               type='real',
               value=0.2,
               texname='cv',
               lhablock='INPUTS',
               lhacode=[1])

ct = Parameter(name='ct',
               nature='external',
               type='real',
               value=0.3,
               texname='ct',
               lhablock='INPUTS',
               lhacode=[1])

eta = Parameter(name='eta',
                nature='external',
                type='real',
                value=0.1,
                texname='eta',
                lhablock='INPUTS',
                lhacode=[1])

mc = Parameter(name='mc',
               nature='external',
               type='real',
               value=100,
               texname='\\text{mc}',
               lhablock='MASS',
               lhacode=[9000001])

mp = Parameter(name='mp',
               nature='external',
               type='real',
               value=120,
               texname='\\text{mp}',
               lhablock='MASS',
               lhacode=[9000002])

Wchi = Parameter(name='Wchi',
                 nature='external',
                 type='real',
                 value=1,
                 texname='\\text{Wchi}',
                 lhablock='DECAY',
                 lhacode=[9000001])

Wphi = Parameter(name='Wphi',
                 nature='external',
                 type='real',
                 value=1,
                 texname='\\text{Wphi}',
                 lhablock='DECAY',
                 lhacode=[9000002])

