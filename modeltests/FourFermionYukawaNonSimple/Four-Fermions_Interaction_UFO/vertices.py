# This file was automatically created by FeynRules 2.3.19
# Mathematica version: 10.0 for Linux x86 (64-bit) (June 29, 2014)
# Date: Sat 23 Apr 2016 23:06:28


from object_library import all_vertices, Vertex
import particles as P
import couplings as C
import lorentz as L


V_1 = Vertex(name='V_1',
             particles=[P.chi__tilde__, P.chi, P.phi],
             color=['1'],
             lorentz=[L.FFS1],
             couplings={(0, 0): C.GC_3})

V_2 = Vertex(name='V_2',
             particles=[P.chi__tilde__, P.chi, P.chi__tilde__, P.chi],
             color=['1'],
             lorentz=[L.FFFF1, L.FFFF2, L.FFFF3, L.FFFF4],
             couplings={(0, 0): C.GC_1, (0, 1): C.GC_2,
                        (0, 2): C.GC_4, (0, 3): C.GC_5})

