# This file was automatically created by FeynRules 2.3.19
# Mathematica version: 10.0 for Linux x86 (64-bit) (June 29, 2014)
# Date: Sat 23 Apr 2016 23:06:28


from object_library import all_orders, CouplingOrder


FV4 = CouplingOrder(name='FV4',
                    expansion_order=0,
                    hierarchy=1)

FT4 = CouplingOrder(name='FT4',
                    expansion_order=0,
                    hierarchy=2)

YUK = CouplingOrder(name='YUK',
                    expansion_order=99,
                    hierarchy=3)

