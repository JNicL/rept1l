#==============================================================================#
#                                 modelfile.py                                 #
#==============================================================================#
""" Searches and imports the FeynRules modelfile. """
#============#
#  Includes  #
#============#

import sys
import os

#===========#
#  Globals  #
#===========#

def get_modelpath():
  directory = os.path.dirname(os.path.realpath(__file__))
  for folder in os.listdir(directory):
    if os.path.isdir(directory + '/' + folder):
      model = 'object_library.py' in os.listdir(directory + '/' + folder)
      if model:
        return directory, folder

def import_model():
  path, modelname = get_modelpath()
  sys.path.append(path)
  return __import__(modelname)

model = import_model()
object_library = model.object_library

Vertex = object_library.Vertex
CTVertex = object_library.CTVertex
Parameter = object_library.Parameter

class CTParameter(Parameter):
  def __init__(self, *args, **kwargs):
    super(CTParameter, self).__init__(*args, **kwargs)

class TreeInducedCTVertex(CTVertex):
  def __init__(self, expand_couplings=True, expand_wavefunctions=True,
               *args, **kwargs):
    super(TreeInducedCTVertex, self).__init__(*args, **kwargs)
    self.expand_couplings = expand_couplings
    self.expand_wavefunctions = expand_wavefunctions
