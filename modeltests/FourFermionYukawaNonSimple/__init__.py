#==============================================================================#
#                                  model init                                  #
#==============================================================================#

#============#
#  Includes  #
#============#

import os
import sys

from .modelfile import model, get_modelpath, CTParameter
from . import model_extended_parameters

#===========#
#  globals  #
#===========#

object_library = model.object_library
model_objects = model.object_library
Particle = object_library.Particle
P = model.particles
V = model.vertices
param = model.parameters

parameter_orders = model_extended_parameters.parameter_orders

# load parameters which can safely be set to zero after renormalization
if hasattr(model_extended_parameters, 'tadpole_parameter'):
  from sympy import Symbol
  zero_tadpole = {Symbol(u.name): 0
                  for u in model_extended_parameters.tadpole_parameter}
else:
  zero_tadpole = {}

# load flavours & doublet partners which can be used to optimize fermion
# helicities
if hasattr(model_extended_parameters, 'doublet_partners'):
  doublet_partners = model_extended_parameters.doublet_partners
if hasattr(model_extended_parameters, 'flavours'):
  flavours = model_extended_parameters.flavours

# load counterterm files
def get_counterterms():
  directory = os.path.dirname(os.path.realpath(__file__))
  files = ('parameters_ct.py', 'vertices_ct.py')
  files_found = {u: u in os.listdir(directory) for u in files}

  if all(files_found.values()):

    return directory, files
  else:
    not_found = [u for u in files_found if files_found[u] is False]
    msg = 'Files not found: ' + ', '.join(not_found) + '.'
    raise Exception(msg)

def import_counterterms():
  path, modules = get_counterterms()
  ret = []
  for mod in modules:
    mod_name = mod.split('.')[0]
    # need to be called with `Model.` -> otherwise isinstance/type(...) wont
    # work outside of Model

    __import__('model.' + mod_name)

import_counterterms()

__author__ = "Jean-Nicolas lang"
__date__ = "18. 5. 2015"
__version__ = "0.9"
