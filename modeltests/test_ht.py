# -*- coding: utf-8 -*-
###############################################################################
#                                 test_hs.py                                  #
###############################################################################

##############
#  Includes  #
##############

import rept1l.modeltests.HT_tests.ht_setup as ht
from rept1l.modeltests.modeltest_utils.auto_model import GenModel


#############
#  Methods  #
#############

class GenHT(GenModel):
  threads = 8
  ct_expansion = True
  pre_ct = 'htexp.txt'
  verbose = True

  def renormalize_model(self, **kwargs):
    # Higgs tadpole + fields
    self.renormalize_higgs_triplet('-o', 'tadpoles', 'higgses', **kwargs)

    # QCD charged fields
    self.renormalize_qcd('-o', 'particles', **kwargs)

    # QCD coupling
    for i in ['6', '5', '4', '3']:
      self.renormalize_qcd('-o', 'alphaS', '-nf', i, **kwargs)

    # Pure EW fields + EW couplings
    for p in ['particles', 'alpha0', 'Gf', 'alphaZ']:
      self.renormalize_gsw('-o', p, **kwargs)

    # msbar reno mixing (this order is essential)
    self.renormalize_higgs_triplet('-o', 'a2', 'b4', **kwargs)

    # reno mixing additional schemes
    self.register_ct()

  def compute_rational(self, **kwargs):
    cls = self.__class__
    self.run_r2(vertices_file='htr2.txt', nc=True, threads=cls.threads)

#------------------------------------------------------------------------------#

def full_ht_model(outputpath):
  GenHT.driverpath = ht.driver_ht_path
  GenHT.outputpath = outputpath
  GenHT(odp=True, use_run_ct=True)

if __name__ == '__main__':
  outputpath = '/home/nick/HT_2.2.2'
  full_ht_model(outputpath)
