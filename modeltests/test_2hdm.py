# -*- coding: utf-8 -*-
#==============================================================================#
#                                 test_2hdm.py                                 #
#==============================================================================#

#============#
#  Includes  #
#============#

import os
import rept1l.modeltests.THDM_tests.thdm_setup as thdm
from rept1l.modeltests.modeltest_utils.auto_model import GenModel


#===========#
#  Methods  #
#===========#

class GenTHDM(GenModel):
  threads = 8
  ct_expansion = True
  pre_ct = '2hdmexp.txt'
  verbose = True

  def renormalize_model(self, **kwargs):
    # Higgs tadpole + fields
    self.renormalize_2hdm_higgs_sector('-o', 'tadpoles', 'neutral', 'charged',
                                       'pseudo', **kwargs)

    # QCD charged fields
    self.renormalize_qcd('-o', 'particles', **kwargs)

    # QCD coupling
    for i in ['6', '5', '4', '3']:
      self.renormalize_qcd('-o', 'alphaS', '-nf', i, **kwargs)

    # Pure EW fields + EW couplings
    for p in ['particles', 'alpha0', 'Gf', 'alphaZ']:
      self.renormalize_gsw('-o', p, **kwargs)

    # msbar reno mixing (this order is essential)
    self.renormalize_2hdm_higgs_sector('-o', 'alpha', 'beta', 'l5', **kwargs)

    # reno mixing additional schemes
    self.renormalize_2hdm_higgs_sector('-o', 'alphaALT', 'betaALT', **kwargs)
    self.register_ct()

  def compute_rational(self, **kwargs):
    cls = self.__class__
    self.run_r2(vertices_file='thdmr2.txt', nc=True, threads=cls.threads)


def test_offshell_twopoint():
  """ Compares results for offshell two-point function of the scalar sector of
  the 2HDM with the Wuerzburg group.
  """
  class Offshell2Point(GenTHDM):

    driverpath = thdm.driver_2hdm_ALL_path

    final_ct = '2hdmexp.txt'

    def renormalize_model(self, **kwargs):
      # Higgs tadpole + fields
      self.renormalize_2hdm_higgs_sector('-o', 'tadpoles', 'neutral', 'charged',
                                         'pseudo', **kwargs)

      self.register_ct()

    def compute_rational(self, **kwargs):
      cls = self.__class__
      self.run_r2(vertices_file='2hdm_hh_r2.txt', np=2, nc=True,
                  threads=cls.threads)

    def gen_renormalized_model(self, rmpath):
      rmpath = os.path.join(self._reptil_mpath, 'renormalized_modelfile')
      super(Offshell2Point, self).gen_renormalized_model(rmpath)

    def run_test(self):
      cls = self.__class__
      self.compile_model(threads=cls.threads)
      # compare numbers produced by L.Jenniches.
      from rept1l.modeltests.THDM_tests.check2point_2hdm import check_2point
      check_2point()

  Offshell2Point()


def full_H4f(tadpolescheme=True):
  """ Performes the full test for H -> 4f. Only the relvant counterterm vertices
  and rational terms are computed.

  :param tadpolescheme: Perform the renormalization Onshell/Msbar in the tadpole
                        scheme if True. Else the renormalization is performed in
                        the Denner scheme.
  :type  tadpolescheme: bool
  """
  class H4F(GenTHDM):

    final_ct = '2hdm_h4f_ct.txt'

    def renormalize_model(self, **kwargs):
      # Higgs tadpole + fields
      self.renormalize_2hdm_higgs_sector('-o', 'tadpoles', 'neutral', 'charged',
                                         'pseudo', 'beta', 'alpha', **kwargs)
      self.renormalize_gsw('-o', 'particles', 'Gf')
      self.renormalize_qcd('-o', 'particles')
      self.register_ct()

    def compute_rational(self, **kwargs):
      cls = self.__class__
      self.run_r2(vertices_file='2hdm_h4f_r2_2.txt', np=2, nc=True,
                  threads=cls.threads)
      self.run_r2(vertices_file='2hdm_h4f_r2_3.txt', np=3, nc=True,
                  threads=cls.threads)

    def gen_renormalized_model(self, rmpath):
      rmpath = os.path.join(self._reptil_mpath, 'renormalized_modelfile')
      super(H4F, self).gen_renormalized_model(rmpath)

    def run_test(self):
      cls = self.__class__
      self.compile_model(threads=cls.threads)
      # compare numbers with Freiburg group
      from rept1l.modeltests.THDM_tests.H4F_2HDM_Comparison.test_H4F import main
      if tadpolescheme:
        main(mode=3)
      else:
        main(mode=1)

  # decide which modelfile
  if tadpolescheme:
    H4F.driverpath = thdm.driver_2hdm_ALL_path
  else:
    H4F.driverpath = thdm.driver_2hdm_ALL_DS_path

  H4F()


def bare_H4f():
  """ Performes the bare-loop test for H -> 4f. Only the relvant rational terms
  are computed.
  """
  class H4F(GenModel):

    final_ct = '2hdm_h4f_ct.txt'
    ct_expansion = False
    threads = 8
    driverpath = thdm.driver_2hdm_ALL_path

    def compute_rational(self, **kwargs):
      cls = self.__class__
      self.run_r2(vertices_file='2hdm_h4f_r2_2.txt', np=2, nc=True,
                  threads=cls.threads)
      self.run_r2(vertices_file='2hdm_h4f_r2_3.txt', np=3, nc=True,
                  threads=cls.threads)

    def gen_renormalized_model(self, rmpath):
      rmpath = os.path.join(self._reptil_mpath, 'bare_modelfile')
      super(H4F, self).gen_renormalized_model(rmpath)

    def run_test(self):
      cls = self.__class__
      self.compile_model(threads=cls.threads)
      # compare numbers with Freiburg group
      from rept1l.modeltests.THDM_tests.H4F_2HDM_Comparison.test_H4F import main
      main(mode=2)

  H4F()


def full_2HDM_NT_model(outputpath):
  GenTHDM.driverpath = thdm.driver_2hdm_ALL_path
  GenTHDM.outputpath = outputpath
  GenTHDM(odp=True, use_run_ct=True)


def full_2HDM_DS_model(outputpath):
  GenTHDM.driverpath = thdm.driver_2hdm_ALL_DS_path
  GenTHDM.outputpath = outputpath
  GenTHDM(odp=True, use_run_ct=True)


def full_2HDM_BH_model(outputpath):
  GenTHDM.driverpath = thdm.driver_2hdm_ALL_BH_path
  GenTHDM.outputpath = outputpath
  GenTHDM(odp=True, use_run_ct=True)


def full_2HDM_BFM_FJT_model(outputpath):
  GenTHDM.driverpath = thdm.driver_2hdm_bfm_ALL_path
  GenTHDM.outputpath = outputpath
  GenTHDM(odp=True, bfm=True, use_run_ct=True)


if __name__ == "__main__":
  #test_offshell_twopoint()
  #full_H4f(tadpolescheme=True)
  #bare_H4f()
  #full_2HDM_NT_model()
  # full_2HDM_BFM_FJT_model()

  outputpath = '/archive/jlang/model_files_2.0.12/THDM_BH_2.0.12'
  full_2HDM_BH_model(outputpath)
