# This file was automatically created by FeynRules 2.3.29
# Mathematica version: 12.0.0 for Linux x86 (64-bit) (April 7, 2019)
# Date: Wed 12 Aug 2020 11:22:47


from .object_library import all_couplings, Coupling

from .function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



GC_1 = Coupling(name = 'GC_1',
                value = '(ee*complex(0,1))/3.',
                order = {'QED':1})

GC_2 = Coupling(name = 'GC_2',
                value = '(-2*ee*complex(0,1))/3.',
                order = {'QED':1})

GC_3 = Coupling(name = 'GC_3',
                value = '-(ee*complex(0,1))',
                order = {'QED':1})

GC_4 = Coupling(name = 'GC_4',
                value = 'ee*complex(0,1)',
                order = {'QED':1})

GC_5 = Coupling(name = 'GC_5',
                value = 'ee**2*complex(0,1)',
                order = {'QED':2})

GC_6 = Coupling(name = 'GC_6',
                value = '-2*ee**2*complex(0,1)',
                order = {'QED':2})

GC_7 = Coupling(name = 'GC_7',
                value = '2*ee**2*complex(0,1)',
                order = {'QED':2})

GC_8 = Coupling(name = 'GC_8',
                value = '-ee**2/(2.*cw)',
                order = {'QED':2})

GC_9 = Coupling(name = 'GC_9',
                value = '-(ee**2*complex(0,1))/(2.*cw)',
                order = {'QED':2})

GC_10 = Coupling(name = 'GC_10',
                 value = 'ee**2/(2.*cw)',
                 order = {'QED':2})

GC_11 = Coupling(name = 'GC_11',
                 value = '-G',
                 order = {'QCD':1})

GC_12 = Coupling(name = 'GC_12',
                 value = 'complex(0,1)*G',
                 order = {'QCD':1})

GC_13 = Coupling(name = 'GC_13',
                 value = 'G',
                 order = {'QCD':1})

GC_14 = Coupling(name = 'GC_14',
                 value = '-(complex(0,1)*G**2)',
                 order = {'QCD':2})

GC_15 = Coupling(name = 'GC_15',
                 value = 'complex(0,1)*G**2',
                 order = {'QCD':2})

GC_16 = Coupling(name = 'GC_16',
                 value = '-(complex(0,1)*I1a11)',
                 order = {'QED':1})

GC_17 = Coupling(name = 'GC_17',
                 value = '-(complex(0,1)*I1a22)',
                 order = {'QED':1})

GC_18 = Coupling(name = 'GC_18',
                 value = '-(complex(0,1)*I1a33)',
                 order = {'QED':1})

GC_19 = Coupling(name = 'GC_19',
                 value = 'complex(0,1)*I2a11',
                 order = {'QED':1})

GC_20 = Coupling(name = 'GC_20',
                 value = 'complex(0,1)*I2a22',
                 order = {'QED':1})

GC_21 = Coupling(name = 'GC_21',
                 value = 'complex(0,1)*I2a33',
                 order = {'QED':1})

GC_22 = Coupling(name = 'GC_22',
                 value = 'complex(0,1)*I3a11',
                 order = {'QED':1})

GC_23 = Coupling(name = 'GC_23',
                 value = 'complex(0,1)*I3a22',
                 order = {'QED':1})

GC_24 = Coupling(name = 'GC_24',
                 value = 'complex(0,1)*I3a33',
                 order = {'QED':1})

GC_25 = Coupling(name = 'GC_25',
                 value = '-(complex(0,1)*I4a11)',
                 order = {'QED':1})

GC_26 = Coupling(name = 'GC_26',
                 value = '-(complex(0,1)*I4a22)',
                 order = {'QED':1})

GC_27 = Coupling(name = 'GC_27',
                 value = '-(complex(0,1)*I4a33)',
                 order = {'QED':1})

GC_28 = Coupling(name = 'GC_28',
                 value = 'Ced1x1x1x1*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_29 = Coupling(name = 'GC_29',
                 value = 'Ced1x1x1x2*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_30 = Coupling(name = 'GC_30',
                 value = 'Ced1x1x1x3*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_31 = Coupling(name = 'GC_31',
                 value = 'Ced1x1x2x1*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_32 = Coupling(name = 'GC_32',
                 value = 'Ced1x1x2x2*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_33 = Coupling(name = 'GC_33',
                 value = 'Ced1x1x2x3*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_34 = Coupling(name = 'GC_34',
                 value = 'Ced1x1x3x1*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_35 = Coupling(name = 'GC_35',
                 value = 'Ced1x1x3x2*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_36 = Coupling(name = 'GC_36',
                 value = 'Ced1x1x3x3*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_37 = Coupling(name = 'GC_37',
                 value = 'Ced1x2x1x1*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_38 = Coupling(name = 'GC_38',
                 value = 'Ced1x2x1x2*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_39 = Coupling(name = 'GC_39',
                 value = 'Ced1x2x1x3*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_40 = Coupling(name = 'GC_40',
                 value = 'Ced1x2x2x1*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_41 = Coupling(name = 'GC_41',
                 value = 'Ced1x2x2x2*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_42 = Coupling(name = 'GC_42',
                 value = 'Ced1x2x2x3*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_43 = Coupling(name = 'GC_43',
                 value = 'Ced1x2x3x1*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_44 = Coupling(name = 'GC_44',
                 value = 'Ced1x2x3x2*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_45 = Coupling(name = 'GC_45',
                 value = 'Ced1x2x3x3*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_46 = Coupling(name = 'GC_46',
                 value = 'Ced1x3x1x1*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_47 = Coupling(name = 'GC_47',
                 value = 'Ced1x3x1x2*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_48 = Coupling(name = 'GC_48',
                 value = 'Ced1x3x1x3*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_49 = Coupling(name = 'GC_49',
                 value = 'Ced1x3x2x1*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_50 = Coupling(name = 'GC_50',
                 value = 'Ced1x3x2x2*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_51 = Coupling(name = 'GC_51',
                 value = 'Ced1x3x2x3*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_52 = Coupling(name = 'GC_52',
                 value = 'Ced1x3x3x1*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_53 = Coupling(name = 'GC_53',
                 value = 'Ced1x3x3x2*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_54 = Coupling(name = 'GC_54',
                 value = 'Ced1x3x3x3*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_55 = Coupling(name = 'GC_55',
                 value = 'Ced2x1x1x1*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_56 = Coupling(name = 'GC_56',
                 value = 'Ced2x1x1x2*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_57 = Coupling(name = 'GC_57',
                 value = 'Ced2x1x1x3*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_58 = Coupling(name = 'GC_58',
                 value = 'Ced2x1x2x1*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_59 = Coupling(name = 'GC_59',
                 value = 'Ced2x1x2x2*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_60 = Coupling(name = 'GC_60',
                 value = 'Ced2x1x2x3*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_61 = Coupling(name = 'GC_61',
                 value = 'Ced2x1x3x1*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_62 = Coupling(name = 'GC_62',
                 value = 'Ced2x1x3x2*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_63 = Coupling(name = 'GC_63',
                 value = 'Ced2x1x3x3*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_64 = Coupling(name = 'GC_64',
                 value = 'Ced2x2x1x1*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_65 = Coupling(name = 'GC_65',
                 value = 'Ced2x2x1x2*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_66 = Coupling(name = 'GC_66',
                 value = 'Ced2x2x1x3*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_67 = Coupling(name = 'GC_67',
                 value = 'Ced2x2x2x1*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_68 = Coupling(name = 'GC_68',
                 value = 'Ced2x2x2x2*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_69 = Coupling(name = 'GC_69',
                 value = 'Ced2x2x2x3*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_70 = Coupling(name = 'GC_70',
                 value = 'Ced2x2x3x1*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_71 = Coupling(name = 'GC_71',
                 value = 'Ced2x2x3x2*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_72 = Coupling(name = 'GC_72',
                 value = 'Ced2x2x3x3*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_73 = Coupling(name = 'GC_73',
                 value = 'Ced2x3x1x1*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_74 = Coupling(name = 'GC_74',
                 value = 'Ced2x3x1x2*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_75 = Coupling(name = 'GC_75',
                 value = 'Ced2x3x1x3*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_76 = Coupling(name = 'GC_76',
                 value = 'Ced2x3x2x1*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_77 = Coupling(name = 'GC_77',
                 value = 'Ced2x3x2x2*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_78 = Coupling(name = 'GC_78',
                 value = 'Ced2x3x2x3*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_79 = Coupling(name = 'GC_79',
                 value = 'Ced2x3x3x1*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_80 = Coupling(name = 'GC_80',
                 value = 'Ced2x3x3x2*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_81 = Coupling(name = 'GC_81',
                 value = 'Ced2x3x3x3*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_82 = Coupling(name = 'GC_82',
                 value = 'Ced3x1x1x1*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_83 = Coupling(name = 'GC_83',
                 value = 'Ced3x1x1x2*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_84 = Coupling(name = 'GC_84',
                 value = 'Ced3x1x1x3*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_85 = Coupling(name = 'GC_85',
                 value = 'Ced3x1x2x1*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_86 = Coupling(name = 'GC_86',
                 value = 'Ced3x1x2x2*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_87 = Coupling(name = 'GC_87',
                 value = 'Ced3x1x2x3*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_88 = Coupling(name = 'GC_88',
                 value = 'Ced3x1x3x1*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_89 = Coupling(name = 'GC_89',
                 value = 'Ced3x1x3x2*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_90 = Coupling(name = 'GC_90',
                 value = 'Ced3x1x3x3*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_91 = Coupling(name = 'GC_91',
                 value = 'Ced3x2x1x1*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_92 = Coupling(name = 'GC_92',
                 value = 'Ced3x2x1x2*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_93 = Coupling(name = 'GC_93',
                 value = 'Ced3x2x1x3*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_94 = Coupling(name = 'GC_94',
                 value = 'Ced3x2x2x1*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_95 = Coupling(name = 'GC_95',
                 value = 'Ced3x2x2x2*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_96 = Coupling(name = 'GC_96',
                 value = 'Ced3x2x2x3*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_97 = Coupling(name = 'GC_97',
                 value = 'Ced3x2x3x1*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_98 = Coupling(name = 'GC_98',
                 value = 'Ced3x2x3x2*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_99 = Coupling(name = 'GC_99',
                 value = 'Ced3x2x3x3*complex(0,1)*Lam*qeddim**2',
                 order = {'LAM':1,'QED':2})

GC_100 = Coupling(name = 'GC_100',
                  value = 'Ced3x3x1x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_101 = Coupling(name = 'GC_101',
                  value = 'Ced3x3x1x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_102 = Coupling(name = 'GC_102',
                  value = 'Ced3x3x1x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_103 = Coupling(name = 'GC_103',
                  value = 'Ced3x3x2x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_104 = Coupling(name = 'GC_104',
                  value = 'Ced3x3x2x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_105 = Coupling(name = 'GC_105',
                  value = 'Ced3x3x2x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_106 = Coupling(name = 'GC_106',
                  value = 'Ced3x3x3x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_107 = Coupling(name = 'GC_107',
                  value = 'Ced3x3x3x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_108 = Coupling(name = 'GC_108',
                  value = 'Ced3x3x3x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_109 = Coupling(name = 'GC_109',
                  value = 'Ceu1x1x1x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_110 = Coupling(name = 'GC_110',
                  value = 'Ceu1x1x1x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_111 = Coupling(name = 'GC_111',
                  value = 'Ceu1x1x1x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_112 = Coupling(name = 'GC_112',
                  value = 'Ceu1x1x2x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_113 = Coupling(name = 'GC_113',
                  value = 'Ceu1x1x2x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_114 = Coupling(name = 'GC_114',
                  value = 'Ceu1x1x2x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_115 = Coupling(name = 'GC_115',
                  value = 'Ceu1x1x3x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_116 = Coupling(name = 'GC_116',
                  value = 'Ceu1x1x3x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_117 = Coupling(name = 'GC_117',
                  value = 'Ceu1x1x3x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_118 = Coupling(name = 'GC_118',
                  value = 'Ceu1x2x1x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_119 = Coupling(name = 'GC_119',
                  value = 'Ceu1x2x1x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_120 = Coupling(name = 'GC_120',
                  value = 'Ceu1x2x1x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_121 = Coupling(name = 'GC_121',
                  value = 'Ceu1x2x2x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_122 = Coupling(name = 'GC_122',
                  value = 'Ceu1x2x2x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_123 = Coupling(name = 'GC_123',
                  value = 'Ceu1x2x2x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_124 = Coupling(name = 'GC_124',
                  value = 'Ceu1x2x3x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_125 = Coupling(name = 'GC_125',
                  value = 'Ceu1x2x3x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_126 = Coupling(name = 'GC_126',
                  value = 'Ceu1x2x3x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_127 = Coupling(name = 'GC_127',
                  value = 'Ceu1x3x1x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_128 = Coupling(name = 'GC_128',
                  value = 'Ceu1x3x1x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_129 = Coupling(name = 'GC_129',
                  value = 'Ceu1x3x1x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_130 = Coupling(name = 'GC_130',
                  value = 'Ceu1x3x2x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_131 = Coupling(name = 'GC_131',
                  value = 'Ceu1x3x2x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_132 = Coupling(name = 'GC_132',
                  value = 'Ceu1x3x2x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_133 = Coupling(name = 'GC_133',
                  value = 'Ceu1x3x3x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_134 = Coupling(name = 'GC_134',
                  value = 'Ceu1x3x3x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_135 = Coupling(name = 'GC_135',
                  value = 'Ceu1x3x3x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_136 = Coupling(name = 'GC_136',
                  value = 'Ceu2x1x1x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_137 = Coupling(name = 'GC_137',
                  value = 'Ceu2x1x1x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_138 = Coupling(name = 'GC_138',
                  value = 'Ceu2x1x1x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_139 = Coupling(name = 'GC_139',
                  value = 'Ceu2x1x2x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_140 = Coupling(name = 'GC_140',
                  value = 'Ceu2x1x2x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_141 = Coupling(name = 'GC_141',
                  value = 'Ceu2x1x2x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_142 = Coupling(name = 'GC_142',
                  value = 'Ceu2x1x3x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_143 = Coupling(name = 'GC_143',
                  value = 'Ceu2x1x3x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_144 = Coupling(name = 'GC_144',
                  value = 'Ceu2x1x3x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_145 = Coupling(name = 'GC_145',
                  value = 'Ceu2x2x1x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_146 = Coupling(name = 'GC_146',
                  value = 'Ceu2x2x1x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_147 = Coupling(name = 'GC_147',
                  value = 'Ceu2x2x1x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_148 = Coupling(name = 'GC_148',
                  value = 'Ceu2x2x2x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_149 = Coupling(name = 'GC_149',
                  value = 'Ceu2x2x2x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_150 = Coupling(name = 'GC_150',
                  value = 'Ceu2x2x2x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_151 = Coupling(name = 'GC_151',
                  value = 'Ceu2x2x3x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_152 = Coupling(name = 'GC_152',
                  value = 'Ceu2x2x3x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_153 = Coupling(name = 'GC_153',
                  value = 'Ceu2x2x3x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_154 = Coupling(name = 'GC_154',
                  value = 'Ceu2x3x1x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_155 = Coupling(name = 'GC_155',
                  value = 'Ceu2x3x1x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_156 = Coupling(name = 'GC_156',
                  value = 'Ceu2x3x1x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_157 = Coupling(name = 'GC_157',
                  value = 'Ceu2x3x2x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_158 = Coupling(name = 'GC_158',
                  value = 'Ceu2x3x2x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_159 = Coupling(name = 'GC_159',
                  value = 'Ceu2x3x2x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_160 = Coupling(name = 'GC_160',
                  value = 'Ceu2x3x3x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_161 = Coupling(name = 'GC_161',
                  value = 'Ceu2x3x3x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_162 = Coupling(name = 'GC_162',
                  value = 'Ceu2x3x3x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_163 = Coupling(name = 'GC_163',
                  value = 'Ceu3x1x1x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_164 = Coupling(name = 'GC_164',
                  value = 'Ceu3x1x1x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_165 = Coupling(name = 'GC_165',
                  value = 'Ceu3x1x1x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_166 = Coupling(name = 'GC_166',
                  value = 'Ceu3x1x2x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_167 = Coupling(name = 'GC_167',
                  value = 'Ceu3x1x2x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_168 = Coupling(name = 'GC_168',
                  value = 'Ceu3x1x2x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_169 = Coupling(name = 'GC_169',
                  value = 'Ceu3x1x3x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_170 = Coupling(name = 'GC_170',
                  value = 'Ceu3x1x3x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_171 = Coupling(name = 'GC_171',
                  value = 'Ceu3x1x3x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_172 = Coupling(name = 'GC_172',
                  value = 'Ceu3x2x1x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_173 = Coupling(name = 'GC_173',
                  value = 'Ceu3x2x1x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_174 = Coupling(name = 'GC_174',
                  value = 'Ceu3x2x1x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_175 = Coupling(name = 'GC_175',
                  value = 'Ceu3x2x2x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_176 = Coupling(name = 'GC_176',
                  value = 'Ceu3x2x2x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_177 = Coupling(name = 'GC_177',
                  value = 'Ceu3x2x2x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_178 = Coupling(name = 'GC_178',
                  value = 'Ceu3x2x3x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_179 = Coupling(name = 'GC_179',
                  value = 'Ceu3x2x3x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_180 = Coupling(name = 'GC_180',
                  value = 'Ceu3x2x3x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_181 = Coupling(name = 'GC_181',
                  value = 'Ceu3x3x1x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_182 = Coupling(name = 'GC_182',
                  value = 'Ceu3x3x1x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_183 = Coupling(name = 'GC_183',
                  value = 'Ceu3x3x1x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_184 = Coupling(name = 'GC_184',
                  value = 'Ceu3x3x2x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_185 = Coupling(name = 'GC_185',
                  value = 'Ceu3x3x2x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_186 = Coupling(name = 'GC_186',
                  value = 'Ceu3x3x2x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_187 = Coupling(name = 'GC_187',
                  value = 'Ceu3x3x3x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_188 = Coupling(name = 'GC_188',
                  value = 'Ceu3x3x3x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_189 = Coupling(name = 'GC_189',
                  value = 'Ceu3x3x3x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_190 = Coupling(name = 'GC_190',
                  value = 'Cld1x1x1x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_191 = Coupling(name = 'GC_191',
                  value = 'Cld1x1x1x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_192 = Coupling(name = 'GC_192',
                  value = 'Cld1x1x1x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_193 = Coupling(name = 'GC_193',
                  value = 'Cld1x1x2x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_194 = Coupling(name = 'GC_194',
                  value = 'Cld1x1x2x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_195 = Coupling(name = 'GC_195',
                  value = 'Cld1x1x2x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_196 = Coupling(name = 'GC_196',
                  value = 'Cld1x1x3x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_197 = Coupling(name = 'GC_197',
                  value = 'Cld1x1x3x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_198 = Coupling(name = 'GC_198',
                  value = 'Cld1x1x3x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_199 = Coupling(name = 'GC_199',
                  value = 'Cld1x2x1x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_200 = Coupling(name = 'GC_200',
                  value = 'Cld1x2x1x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_201 = Coupling(name = 'GC_201',
                  value = 'Cld1x2x1x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_202 = Coupling(name = 'GC_202',
                  value = 'Cld1x2x2x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_203 = Coupling(name = 'GC_203',
                  value = 'Cld1x2x2x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_204 = Coupling(name = 'GC_204',
                  value = 'Cld1x2x2x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_205 = Coupling(name = 'GC_205',
                  value = 'Cld1x2x3x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_206 = Coupling(name = 'GC_206',
                  value = 'Cld1x2x3x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_207 = Coupling(name = 'GC_207',
                  value = 'Cld1x2x3x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_208 = Coupling(name = 'GC_208',
                  value = 'Cld1x3x1x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_209 = Coupling(name = 'GC_209',
                  value = 'Cld1x3x1x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_210 = Coupling(name = 'GC_210',
                  value = 'Cld1x3x1x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_211 = Coupling(name = 'GC_211',
                  value = 'Cld1x3x2x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_212 = Coupling(name = 'GC_212',
                  value = 'Cld1x3x2x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_213 = Coupling(name = 'GC_213',
                  value = 'Cld1x3x2x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_214 = Coupling(name = 'GC_214',
                  value = 'Cld1x3x3x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_215 = Coupling(name = 'GC_215',
                  value = 'Cld1x3x3x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_216 = Coupling(name = 'GC_216',
                  value = 'Cld1x3x3x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_217 = Coupling(name = 'GC_217',
                  value = 'Cld2x1x1x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_218 = Coupling(name = 'GC_218',
                  value = 'Cld2x1x1x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_219 = Coupling(name = 'GC_219',
                  value = 'Cld2x1x1x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_220 = Coupling(name = 'GC_220',
                  value = 'Cld2x1x2x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_221 = Coupling(name = 'GC_221',
                  value = 'Cld2x1x2x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_222 = Coupling(name = 'GC_222',
                  value = 'Cld2x1x2x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_223 = Coupling(name = 'GC_223',
                  value = 'Cld2x1x3x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_224 = Coupling(name = 'GC_224',
                  value = 'Cld2x1x3x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_225 = Coupling(name = 'GC_225',
                  value = 'Cld2x1x3x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_226 = Coupling(name = 'GC_226',
                  value = 'Cld2x2x1x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_227 = Coupling(name = 'GC_227',
                  value = 'Cld2x2x1x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_228 = Coupling(name = 'GC_228',
                  value = 'Cld2x2x1x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_229 = Coupling(name = 'GC_229',
                  value = 'Cld2x2x2x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_230 = Coupling(name = 'GC_230',
                  value = 'Cld2x2x2x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_231 = Coupling(name = 'GC_231',
                  value = 'Cld2x2x2x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_232 = Coupling(name = 'GC_232',
                  value = 'Cld2x2x3x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_233 = Coupling(name = 'GC_233',
                  value = 'Cld2x2x3x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_234 = Coupling(name = 'GC_234',
                  value = 'Cld2x2x3x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_235 = Coupling(name = 'GC_235',
                  value = 'Cld2x3x1x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_236 = Coupling(name = 'GC_236',
                  value = 'Cld2x3x1x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_237 = Coupling(name = 'GC_237',
                  value = 'Cld2x3x1x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_238 = Coupling(name = 'GC_238',
                  value = 'Cld2x3x2x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_239 = Coupling(name = 'GC_239',
                  value = 'Cld2x3x2x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_240 = Coupling(name = 'GC_240',
                  value = 'Cld2x3x2x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_241 = Coupling(name = 'GC_241',
                  value = 'Cld2x3x3x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_242 = Coupling(name = 'GC_242',
                  value = 'Cld2x3x3x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_243 = Coupling(name = 'GC_243',
                  value = 'Cld2x3x3x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_244 = Coupling(name = 'GC_244',
                  value = 'Cld3x1x1x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_245 = Coupling(name = 'GC_245',
                  value = 'Cld3x1x1x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_246 = Coupling(name = 'GC_246',
                  value = 'Cld3x1x1x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_247 = Coupling(name = 'GC_247',
                  value = 'Cld3x1x2x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_248 = Coupling(name = 'GC_248',
                  value = 'Cld3x1x2x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_249 = Coupling(name = 'GC_249',
                  value = 'Cld3x1x2x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_250 = Coupling(name = 'GC_250',
                  value = 'Cld3x1x3x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_251 = Coupling(name = 'GC_251',
                  value = 'Cld3x1x3x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_252 = Coupling(name = 'GC_252',
                  value = 'Cld3x1x3x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_253 = Coupling(name = 'GC_253',
                  value = 'Cld3x2x1x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_254 = Coupling(name = 'GC_254',
                  value = 'Cld3x2x1x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_255 = Coupling(name = 'GC_255',
                  value = 'Cld3x2x1x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_256 = Coupling(name = 'GC_256',
                  value = 'Cld3x2x2x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_257 = Coupling(name = 'GC_257',
                  value = 'Cld3x2x2x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_258 = Coupling(name = 'GC_258',
                  value = 'Cld3x2x2x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_259 = Coupling(name = 'GC_259',
                  value = 'Cld3x2x3x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_260 = Coupling(name = 'GC_260',
                  value = 'Cld3x2x3x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_261 = Coupling(name = 'GC_261',
                  value = 'Cld3x2x3x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_262 = Coupling(name = 'GC_262',
                  value = 'Cld3x3x1x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_263 = Coupling(name = 'GC_263',
                  value = 'Cld3x3x1x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_264 = Coupling(name = 'GC_264',
                  value = 'Cld3x3x1x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_265 = Coupling(name = 'GC_265',
                  value = 'Cld3x3x2x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_266 = Coupling(name = 'GC_266',
                  value = 'Cld3x3x2x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_267 = Coupling(name = 'GC_267',
                  value = 'Cld3x3x2x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_268 = Coupling(name = 'GC_268',
                  value = 'Cld3x3x3x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_269 = Coupling(name = 'GC_269',
                  value = 'Cld3x3x3x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_270 = Coupling(name = 'GC_270',
                  value = 'Cld3x3x3x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_271 = Coupling(name = 'GC_271',
                  value = 'Cledq1x1x1x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_272 = Coupling(name = 'GC_272',
                  value = 'Cledq1x1x1x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_273 = Coupling(name = 'GC_273',
                  value = 'Cledq1x1x1x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_274 = Coupling(name = 'GC_274',
                  value = 'Cledq1x1x2x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_275 = Coupling(name = 'GC_275',
                  value = 'Cledq1x1x2x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_276 = Coupling(name = 'GC_276',
                  value = 'Cledq1x1x2x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_277 = Coupling(name = 'GC_277',
                  value = 'Cledq1x1x3x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_278 = Coupling(name = 'GC_278',
                  value = 'Cledq1x1x3x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_279 = Coupling(name = 'GC_279',
                  value = 'Cledq1x1x3x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_280 = Coupling(name = 'GC_280',
                  value = 'Cledq1x2x1x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_281 = Coupling(name = 'GC_281',
                  value = 'Cledq1x2x1x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_282 = Coupling(name = 'GC_282',
                  value = 'Cledq1x2x1x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_283 = Coupling(name = 'GC_283',
                  value = 'Cledq1x2x2x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_284 = Coupling(name = 'GC_284',
                  value = 'Cledq1x2x2x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_285 = Coupling(name = 'GC_285',
                  value = 'Cledq1x2x2x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_286 = Coupling(name = 'GC_286',
                  value = 'Cledq1x2x3x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_287 = Coupling(name = 'GC_287',
                  value = 'Cledq1x2x3x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_288 = Coupling(name = 'GC_288',
                  value = 'Cledq1x2x3x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_289 = Coupling(name = 'GC_289',
                  value = 'Cledq1x3x1x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_290 = Coupling(name = 'GC_290',
                  value = 'Cledq1x3x1x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_291 = Coupling(name = 'GC_291',
                  value = 'Cledq1x3x1x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_292 = Coupling(name = 'GC_292',
                  value = 'Cledq1x3x2x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_293 = Coupling(name = 'GC_293',
                  value = 'Cledq1x3x2x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_294 = Coupling(name = 'GC_294',
                  value = 'Cledq1x3x2x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_295 = Coupling(name = 'GC_295',
                  value = 'Cledq1x3x3x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_296 = Coupling(name = 'GC_296',
                  value = 'Cledq1x3x3x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_297 = Coupling(name = 'GC_297',
                  value = 'Cledq1x3x3x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_298 = Coupling(name = 'GC_298',
                  value = 'Cledq2x1x1x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_299 = Coupling(name = 'GC_299',
                  value = 'Cledq2x1x1x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_300 = Coupling(name = 'GC_300',
                  value = 'Cledq2x1x1x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_301 = Coupling(name = 'GC_301',
                  value = 'Cledq2x1x2x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_302 = Coupling(name = 'GC_302',
                  value = 'Cledq2x1x2x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_303 = Coupling(name = 'GC_303',
                  value = 'Cledq2x1x2x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_304 = Coupling(name = 'GC_304',
                  value = 'Cledq2x1x3x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_305 = Coupling(name = 'GC_305',
                  value = 'Cledq2x1x3x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_306 = Coupling(name = 'GC_306',
                  value = 'Cledq2x1x3x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_307 = Coupling(name = 'GC_307',
                  value = 'Cledq2x2x1x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_308 = Coupling(name = 'GC_308',
                  value = 'Cledq2x2x1x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_309 = Coupling(name = 'GC_309',
                  value = 'Cledq2x2x1x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_310 = Coupling(name = 'GC_310',
                  value = 'Cledq2x2x2x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_311 = Coupling(name = 'GC_311',
                  value = 'Cledq2x2x2x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_312 = Coupling(name = 'GC_312',
                  value = 'Cledq2x2x2x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_313 = Coupling(name = 'GC_313',
                  value = 'Cledq2x2x3x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_314 = Coupling(name = 'GC_314',
                  value = 'Cledq2x2x3x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_315 = Coupling(name = 'GC_315',
                  value = 'Cledq2x2x3x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_316 = Coupling(name = 'GC_316',
                  value = 'Cledq2x3x1x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_317 = Coupling(name = 'GC_317',
                  value = 'Cledq2x3x1x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_318 = Coupling(name = 'GC_318',
                  value = 'Cledq2x3x1x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_319 = Coupling(name = 'GC_319',
                  value = 'Cledq2x3x2x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_320 = Coupling(name = 'GC_320',
                  value = 'Cledq2x3x2x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_321 = Coupling(name = 'GC_321',
                  value = 'Cledq2x3x2x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_322 = Coupling(name = 'GC_322',
                  value = 'Cledq2x3x3x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_323 = Coupling(name = 'GC_323',
                  value = 'Cledq2x3x3x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_324 = Coupling(name = 'GC_324',
                  value = 'Cledq2x3x3x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_325 = Coupling(name = 'GC_325',
                  value = 'Cledq3x1x1x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_326 = Coupling(name = 'GC_326',
                  value = 'Cledq3x1x1x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_327 = Coupling(name = 'GC_327',
                  value = 'Cledq3x1x1x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_328 = Coupling(name = 'GC_328',
                  value = 'Cledq3x1x2x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_329 = Coupling(name = 'GC_329',
                  value = 'Cledq3x1x2x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_330 = Coupling(name = 'GC_330',
                  value = 'Cledq3x1x2x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_331 = Coupling(name = 'GC_331',
                  value = 'Cledq3x1x3x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_332 = Coupling(name = 'GC_332',
                  value = 'Cledq3x1x3x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_333 = Coupling(name = 'GC_333',
                  value = 'Cledq3x1x3x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_334 = Coupling(name = 'GC_334',
                  value = 'Cledq3x2x1x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_335 = Coupling(name = 'GC_335',
                  value = 'Cledq3x2x1x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_336 = Coupling(name = 'GC_336',
                  value = 'Cledq3x2x1x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_337 = Coupling(name = 'GC_337',
                  value = 'Cledq3x2x2x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_338 = Coupling(name = 'GC_338',
                  value = 'Cledq3x2x2x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_339 = Coupling(name = 'GC_339',
                  value = 'Cledq3x2x2x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_340 = Coupling(name = 'GC_340',
                  value = 'Cledq3x2x3x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_341 = Coupling(name = 'GC_341',
                  value = 'Cledq3x2x3x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_342 = Coupling(name = 'GC_342',
                  value = 'Cledq3x2x3x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_343 = Coupling(name = 'GC_343',
                  value = 'Cledq3x3x1x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_344 = Coupling(name = 'GC_344',
                  value = 'Cledq3x3x1x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_345 = Coupling(name = 'GC_345',
                  value = 'Cledq3x3x1x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_346 = Coupling(name = 'GC_346',
                  value = 'Cledq3x3x2x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_347 = Coupling(name = 'GC_347',
                  value = 'Cledq3x3x2x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_348 = Coupling(name = 'GC_348',
                  value = 'Cledq3x3x2x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_349 = Coupling(name = 'GC_349',
                  value = 'Cledq3x3x3x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_350 = Coupling(name = 'GC_350',
                  value = 'Cledq3x3x3x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_351 = Coupling(name = 'GC_351',
                  value = 'Cledq3x3x3x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_352 = Coupling(name = 'GC_352',
                  value = '-(Clequ11x1x1x1*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_353 = Coupling(name = 'GC_353',
                  value = 'Clequ11x1x1x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_354 = Coupling(name = 'GC_354',
                  value = '-(Clequ11x1x1x2*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_355 = Coupling(name = 'GC_355',
                  value = 'Clequ11x1x1x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_356 = Coupling(name = 'GC_356',
                  value = '-(Clequ11x1x1x3*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_357 = Coupling(name = 'GC_357',
                  value = 'Clequ11x1x1x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_358 = Coupling(name = 'GC_358',
                  value = '-(Clequ11x1x2x1*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_359 = Coupling(name = 'GC_359',
                  value = 'Clequ11x1x2x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_360 = Coupling(name = 'GC_360',
                  value = '-(Clequ11x1x2x2*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_361 = Coupling(name = 'GC_361',
                  value = 'Clequ11x1x2x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_362 = Coupling(name = 'GC_362',
                  value = '-(Clequ11x1x2x3*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_363 = Coupling(name = 'GC_363',
                  value = 'Clequ11x1x2x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_364 = Coupling(name = 'GC_364',
                  value = '-(Clequ11x1x3x1*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_365 = Coupling(name = 'GC_365',
                  value = 'Clequ11x1x3x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_366 = Coupling(name = 'GC_366',
                  value = '-(Clequ11x1x3x2*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_367 = Coupling(name = 'GC_367',
                  value = 'Clequ11x1x3x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_368 = Coupling(name = 'GC_368',
                  value = '-(Clequ11x1x3x3*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_369 = Coupling(name = 'GC_369',
                  value = 'Clequ11x1x3x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_370 = Coupling(name = 'GC_370',
                  value = '-(Clequ11x2x1x1*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_371 = Coupling(name = 'GC_371',
                  value = 'Clequ11x2x1x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_372 = Coupling(name = 'GC_372',
                  value = '-(Clequ11x2x1x2*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_373 = Coupling(name = 'GC_373',
                  value = 'Clequ11x2x1x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_374 = Coupling(name = 'GC_374',
                  value = '-(Clequ11x2x1x3*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_375 = Coupling(name = 'GC_375',
                  value = 'Clequ11x2x1x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_376 = Coupling(name = 'GC_376',
                  value = '-(Clequ11x2x2x1*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_377 = Coupling(name = 'GC_377',
                  value = 'Clequ11x2x2x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_378 = Coupling(name = 'GC_378',
                  value = '-(Clequ11x2x2x2*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_379 = Coupling(name = 'GC_379',
                  value = 'Clequ11x2x2x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_380 = Coupling(name = 'GC_380',
                  value = '-(Clequ11x2x2x3*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_381 = Coupling(name = 'GC_381',
                  value = 'Clequ11x2x2x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_382 = Coupling(name = 'GC_382',
                  value = '-(Clequ11x2x3x1*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_383 = Coupling(name = 'GC_383',
                  value = 'Clequ11x2x3x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_384 = Coupling(name = 'GC_384',
                  value = '-(Clequ11x2x3x2*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_385 = Coupling(name = 'GC_385',
                  value = 'Clequ11x2x3x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_386 = Coupling(name = 'GC_386',
                  value = '-(Clequ11x2x3x3*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_387 = Coupling(name = 'GC_387',
                  value = 'Clequ11x2x3x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_388 = Coupling(name = 'GC_388',
                  value = '-(Clequ11x3x1x1*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_389 = Coupling(name = 'GC_389',
                  value = 'Clequ11x3x1x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_390 = Coupling(name = 'GC_390',
                  value = '-(Clequ11x3x1x2*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_391 = Coupling(name = 'GC_391',
                  value = 'Clequ11x3x1x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_392 = Coupling(name = 'GC_392',
                  value = '-(Clequ11x3x1x3*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_393 = Coupling(name = 'GC_393',
                  value = 'Clequ11x3x1x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_394 = Coupling(name = 'GC_394',
                  value = '-(Clequ11x3x2x1*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_395 = Coupling(name = 'GC_395',
                  value = 'Clequ11x3x2x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_396 = Coupling(name = 'GC_396',
                  value = '-(Clequ11x3x2x2*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_397 = Coupling(name = 'GC_397',
                  value = 'Clequ11x3x2x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_398 = Coupling(name = 'GC_398',
                  value = '-(Clequ11x3x2x3*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_399 = Coupling(name = 'GC_399',
                  value = 'Clequ11x3x2x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_400 = Coupling(name = 'GC_400',
                  value = '-(Clequ11x3x3x1*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_401 = Coupling(name = 'GC_401',
                  value = 'Clequ11x3x3x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_402 = Coupling(name = 'GC_402',
                  value = '-(Clequ11x3x3x2*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_403 = Coupling(name = 'GC_403',
                  value = 'Clequ11x3x3x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_404 = Coupling(name = 'GC_404',
                  value = '-(Clequ11x3x3x3*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_405 = Coupling(name = 'GC_405',
                  value = 'Clequ11x3x3x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_406 = Coupling(name = 'GC_406',
                  value = '-(Clequ12x1x1x1*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_407 = Coupling(name = 'GC_407',
                  value = 'Clequ12x1x1x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_408 = Coupling(name = 'GC_408',
                  value = '-(Clequ12x1x1x2*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_409 = Coupling(name = 'GC_409',
                  value = 'Clequ12x1x1x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_410 = Coupling(name = 'GC_410',
                  value = '-(Clequ12x1x1x3*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_411 = Coupling(name = 'GC_411',
                  value = 'Clequ12x1x1x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_412 = Coupling(name = 'GC_412',
                  value = '-(Clequ12x1x2x1*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_413 = Coupling(name = 'GC_413',
                  value = 'Clequ12x1x2x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_414 = Coupling(name = 'GC_414',
                  value = '-(Clequ12x1x2x2*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_415 = Coupling(name = 'GC_415',
                  value = 'Clequ12x1x2x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_416 = Coupling(name = 'GC_416',
                  value = '-(Clequ12x1x2x3*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_417 = Coupling(name = 'GC_417',
                  value = 'Clequ12x1x2x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_418 = Coupling(name = 'GC_418',
                  value = '-(Clequ12x1x3x1*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_419 = Coupling(name = 'GC_419',
                  value = 'Clequ12x1x3x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_420 = Coupling(name = 'GC_420',
                  value = '-(Clequ12x1x3x2*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_421 = Coupling(name = 'GC_421',
                  value = 'Clequ12x1x3x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_422 = Coupling(name = 'GC_422',
                  value = '-(Clequ12x1x3x3*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_423 = Coupling(name = 'GC_423',
                  value = 'Clequ12x1x3x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_424 = Coupling(name = 'GC_424',
                  value = '-(Clequ12x2x1x1*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_425 = Coupling(name = 'GC_425',
                  value = 'Clequ12x2x1x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_426 = Coupling(name = 'GC_426',
                  value = '-(Clequ12x2x1x2*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_427 = Coupling(name = 'GC_427',
                  value = 'Clequ12x2x1x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_428 = Coupling(name = 'GC_428',
                  value = '-(Clequ12x2x1x3*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_429 = Coupling(name = 'GC_429',
                  value = 'Clequ12x2x1x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_430 = Coupling(name = 'GC_430',
                  value = '-(Clequ12x2x2x1*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_431 = Coupling(name = 'GC_431',
                  value = 'Clequ12x2x2x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_432 = Coupling(name = 'GC_432',
                  value = '-(Clequ12x2x2x2*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_433 = Coupling(name = 'GC_433',
                  value = 'Clequ12x2x2x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_434 = Coupling(name = 'GC_434',
                  value = '-(Clequ12x2x2x3*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_435 = Coupling(name = 'GC_435',
                  value = 'Clequ12x2x2x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_436 = Coupling(name = 'GC_436',
                  value = '-(Clequ12x2x3x1*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_437 = Coupling(name = 'GC_437',
                  value = 'Clequ12x2x3x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_438 = Coupling(name = 'GC_438',
                  value = '-(Clequ12x2x3x2*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_439 = Coupling(name = 'GC_439',
                  value = 'Clequ12x2x3x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_440 = Coupling(name = 'GC_440',
                  value = '-(Clequ12x2x3x3*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_441 = Coupling(name = 'GC_441',
                  value = 'Clequ12x2x3x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_442 = Coupling(name = 'GC_442',
                  value = '-(Clequ12x3x1x1*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_443 = Coupling(name = 'GC_443',
                  value = 'Clequ12x3x1x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_444 = Coupling(name = 'GC_444',
                  value = '-(Clequ12x3x1x2*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_445 = Coupling(name = 'GC_445',
                  value = 'Clequ12x3x1x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_446 = Coupling(name = 'GC_446',
                  value = '-(Clequ12x3x1x3*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_447 = Coupling(name = 'GC_447',
                  value = 'Clequ12x3x1x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_448 = Coupling(name = 'GC_448',
                  value = '-(Clequ12x3x2x1*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_449 = Coupling(name = 'GC_449',
                  value = 'Clequ12x3x2x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_450 = Coupling(name = 'GC_450',
                  value = '-(Clequ12x3x2x2*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_451 = Coupling(name = 'GC_451',
                  value = 'Clequ12x3x2x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_452 = Coupling(name = 'GC_452',
                  value = '-(Clequ12x3x2x3*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_453 = Coupling(name = 'GC_453',
                  value = 'Clequ12x3x2x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_454 = Coupling(name = 'GC_454',
                  value = '-(Clequ12x3x3x1*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_455 = Coupling(name = 'GC_455',
                  value = 'Clequ12x3x3x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_456 = Coupling(name = 'GC_456',
                  value = '-(Clequ12x3x3x2*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_457 = Coupling(name = 'GC_457',
                  value = 'Clequ12x3x3x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_458 = Coupling(name = 'GC_458',
                  value = '-(Clequ12x3x3x3*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_459 = Coupling(name = 'GC_459',
                  value = 'Clequ12x3x3x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_460 = Coupling(name = 'GC_460',
                  value = '-(Clequ13x1x1x1*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_461 = Coupling(name = 'GC_461',
                  value = 'Clequ13x1x1x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_462 = Coupling(name = 'GC_462',
                  value = '-(Clequ13x1x1x2*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_463 = Coupling(name = 'GC_463',
                  value = 'Clequ13x1x1x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_464 = Coupling(name = 'GC_464',
                  value = '-(Clequ13x1x1x3*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_465 = Coupling(name = 'GC_465',
                  value = 'Clequ13x1x1x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_466 = Coupling(name = 'GC_466',
                  value = '-(Clequ13x1x2x1*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_467 = Coupling(name = 'GC_467',
                  value = 'Clequ13x1x2x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_468 = Coupling(name = 'GC_468',
                  value = '-(Clequ13x1x2x2*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_469 = Coupling(name = 'GC_469',
                  value = 'Clequ13x1x2x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_470 = Coupling(name = 'GC_470',
                  value = '-(Clequ13x1x2x3*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_471 = Coupling(name = 'GC_471',
                  value = 'Clequ13x1x2x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_472 = Coupling(name = 'GC_472',
                  value = '-(Clequ13x1x3x1*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_473 = Coupling(name = 'GC_473',
                  value = 'Clequ13x1x3x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_474 = Coupling(name = 'GC_474',
                  value = '-(Clequ13x1x3x2*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_475 = Coupling(name = 'GC_475',
                  value = 'Clequ13x1x3x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_476 = Coupling(name = 'GC_476',
                  value = '-(Clequ13x1x3x3*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_477 = Coupling(name = 'GC_477',
                  value = 'Clequ13x1x3x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_478 = Coupling(name = 'GC_478',
                  value = '-(Clequ13x2x1x1*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_479 = Coupling(name = 'GC_479',
                  value = 'Clequ13x2x1x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_480 = Coupling(name = 'GC_480',
                  value = '-(Clequ13x2x1x2*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_481 = Coupling(name = 'GC_481',
                  value = 'Clequ13x2x1x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_482 = Coupling(name = 'GC_482',
                  value = '-(Clequ13x2x1x3*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_483 = Coupling(name = 'GC_483',
                  value = 'Clequ13x2x1x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_484 = Coupling(name = 'GC_484',
                  value = '-(Clequ13x2x2x1*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_485 = Coupling(name = 'GC_485',
                  value = 'Clequ13x2x2x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_486 = Coupling(name = 'GC_486',
                  value = '-(Clequ13x2x2x2*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_487 = Coupling(name = 'GC_487',
                  value = 'Clequ13x2x2x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_488 = Coupling(name = 'GC_488',
                  value = '-(Clequ13x2x2x3*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_489 = Coupling(name = 'GC_489',
                  value = 'Clequ13x2x2x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_490 = Coupling(name = 'GC_490',
                  value = '-(Clequ13x2x3x1*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_491 = Coupling(name = 'GC_491',
                  value = 'Clequ13x2x3x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_492 = Coupling(name = 'GC_492',
                  value = '-(Clequ13x2x3x2*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_493 = Coupling(name = 'GC_493',
                  value = 'Clequ13x2x3x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_494 = Coupling(name = 'GC_494',
                  value = '-(Clequ13x2x3x3*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_495 = Coupling(name = 'GC_495',
                  value = 'Clequ13x2x3x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_496 = Coupling(name = 'GC_496',
                  value = '-(Clequ13x3x1x1*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_497 = Coupling(name = 'GC_497',
                  value = 'Clequ13x3x1x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_498 = Coupling(name = 'GC_498',
                  value = '-(Clequ13x3x1x2*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_499 = Coupling(name = 'GC_499',
                  value = 'Clequ13x3x1x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_500 = Coupling(name = 'GC_500',
                  value = '-(Clequ13x3x1x3*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_501 = Coupling(name = 'GC_501',
                  value = 'Clequ13x3x1x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_502 = Coupling(name = 'GC_502',
                  value = '-(Clequ13x3x2x1*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_503 = Coupling(name = 'GC_503',
                  value = 'Clequ13x3x2x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_504 = Coupling(name = 'GC_504',
                  value = '-(Clequ13x3x2x2*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_505 = Coupling(name = 'GC_505',
                  value = 'Clequ13x3x2x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_506 = Coupling(name = 'GC_506',
                  value = '-(Clequ13x3x2x3*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_507 = Coupling(name = 'GC_507',
                  value = 'Clequ13x3x2x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_508 = Coupling(name = 'GC_508',
                  value = '-(Clequ13x3x3x1*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_509 = Coupling(name = 'GC_509',
                  value = 'Clequ13x3x3x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_510 = Coupling(name = 'GC_510',
                  value = '-(Clequ13x3x3x2*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_511 = Coupling(name = 'GC_511',
                  value = 'Clequ13x3x3x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_512 = Coupling(name = 'GC_512',
                  value = '-(Clequ13x3x3x3*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_513 = Coupling(name = 'GC_513',
                  value = 'Clequ13x3x3x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_514 = Coupling(name = 'GC_514',
                  value = '-(Clequ31x1x1x1*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_515 = Coupling(name = 'GC_515',
                  value = '(Clequ31x1x1x1*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_516 = Coupling(name = 'GC_516',
                  value = '-(Clequ31x1x1x1*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_517 = Coupling(name = 'GC_517',
                  value = '(Clequ31x1x1x1*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_518 = Coupling(name = 'GC_518',
                  value = '-(Clequ31x1x1x2*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_519 = Coupling(name = 'GC_519',
                  value = '(Clequ31x1x1x2*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_520 = Coupling(name = 'GC_520',
                  value = '-(Clequ31x1x1x2*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_521 = Coupling(name = 'GC_521',
                  value = '(Clequ31x1x1x2*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_522 = Coupling(name = 'GC_522',
                  value = '-(Clequ31x1x1x3*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_523 = Coupling(name = 'GC_523',
                  value = '(Clequ31x1x1x3*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_524 = Coupling(name = 'GC_524',
                  value = '-(Clequ31x1x1x3*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_525 = Coupling(name = 'GC_525',
                  value = '(Clequ31x1x1x3*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_526 = Coupling(name = 'GC_526',
                  value = '-(Clequ31x1x2x1*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_527 = Coupling(name = 'GC_527',
                  value = '(Clequ31x1x2x1*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_528 = Coupling(name = 'GC_528',
                  value = '-(Clequ31x1x2x1*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_529 = Coupling(name = 'GC_529',
                  value = '(Clequ31x1x2x1*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_530 = Coupling(name = 'GC_530',
                  value = '-(Clequ31x1x2x2*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_531 = Coupling(name = 'GC_531',
                  value = '(Clequ31x1x2x2*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_532 = Coupling(name = 'GC_532',
                  value = '-(Clequ31x1x2x2*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_533 = Coupling(name = 'GC_533',
                  value = '(Clequ31x1x2x2*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_534 = Coupling(name = 'GC_534',
                  value = '-(Clequ31x1x2x3*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_535 = Coupling(name = 'GC_535',
                  value = '(Clequ31x1x2x3*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_536 = Coupling(name = 'GC_536',
                  value = '-(Clequ31x1x2x3*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_537 = Coupling(name = 'GC_537',
                  value = '(Clequ31x1x2x3*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_538 = Coupling(name = 'GC_538',
                  value = '-(Clequ31x1x3x1*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_539 = Coupling(name = 'GC_539',
                  value = '(Clequ31x1x3x1*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_540 = Coupling(name = 'GC_540',
                  value = '-(Clequ31x1x3x1*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_541 = Coupling(name = 'GC_541',
                  value = '(Clequ31x1x3x1*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_542 = Coupling(name = 'GC_542',
                  value = '-(Clequ31x1x3x2*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_543 = Coupling(name = 'GC_543',
                  value = '(Clequ31x1x3x2*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_544 = Coupling(name = 'GC_544',
                  value = '-(Clequ31x1x3x2*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_545 = Coupling(name = 'GC_545',
                  value = '(Clequ31x1x3x2*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_546 = Coupling(name = 'GC_546',
                  value = '-(Clequ31x1x3x3*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_547 = Coupling(name = 'GC_547',
                  value = '(Clequ31x1x3x3*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_548 = Coupling(name = 'GC_548',
                  value = '-(Clequ31x1x3x3*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_549 = Coupling(name = 'GC_549',
                  value = '(Clequ31x1x3x3*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_550 = Coupling(name = 'GC_550',
                  value = '-(Clequ31x2x1x1*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_551 = Coupling(name = 'GC_551',
                  value = '(Clequ31x2x1x1*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_552 = Coupling(name = 'GC_552',
                  value = '-(Clequ31x2x1x1*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_553 = Coupling(name = 'GC_553',
                  value = '(Clequ31x2x1x1*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_554 = Coupling(name = 'GC_554',
                  value = '-(Clequ31x2x1x2*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_555 = Coupling(name = 'GC_555',
                  value = '(Clequ31x2x1x2*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_556 = Coupling(name = 'GC_556',
                  value = '-(Clequ31x2x1x2*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_557 = Coupling(name = 'GC_557',
                  value = '(Clequ31x2x1x2*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_558 = Coupling(name = 'GC_558',
                  value = '-(Clequ31x2x1x3*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_559 = Coupling(name = 'GC_559',
                  value = '(Clequ31x2x1x3*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_560 = Coupling(name = 'GC_560',
                  value = '-(Clequ31x2x1x3*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_561 = Coupling(name = 'GC_561',
                  value = '(Clequ31x2x1x3*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_562 = Coupling(name = 'GC_562',
                  value = '-(Clequ31x2x2x1*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_563 = Coupling(name = 'GC_563',
                  value = '(Clequ31x2x2x1*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_564 = Coupling(name = 'GC_564',
                  value = '-(Clequ31x2x2x1*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_565 = Coupling(name = 'GC_565',
                  value = '(Clequ31x2x2x1*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_566 = Coupling(name = 'GC_566',
                  value = '-(Clequ31x2x2x2*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_567 = Coupling(name = 'GC_567',
                  value = '(Clequ31x2x2x2*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_568 = Coupling(name = 'GC_568',
                  value = '-(Clequ31x2x2x2*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_569 = Coupling(name = 'GC_569',
                  value = '(Clequ31x2x2x2*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_570 = Coupling(name = 'GC_570',
                  value = '-(Clequ31x2x2x3*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_571 = Coupling(name = 'GC_571',
                  value = '(Clequ31x2x2x3*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_572 = Coupling(name = 'GC_572',
                  value = '-(Clequ31x2x2x3*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_573 = Coupling(name = 'GC_573',
                  value = '(Clequ31x2x2x3*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_574 = Coupling(name = 'GC_574',
                  value = '-(Clequ31x2x3x1*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_575 = Coupling(name = 'GC_575',
                  value = '(Clequ31x2x3x1*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_576 = Coupling(name = 'GC_576',
                  value = '-(Clequ31x2x3x1*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_577 = Coupling(name = 'GC_577',
                  value = '(Clequ31x2x3x1*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_578 = Coupling(name = 'GC_578',
                  value = '-(Clequ31x2x3x2*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_579 = Coupling(name = 'GC_579',
                  value = '(Clequ31x2x3x2*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_580 = Coupling(name = 'GC_580',
                  value = '-(Clequ31x2x3x2*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_581 = Coupling(name = 'GC_581',
                  value = '(Clequ31x2x3x2*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_582 = Coupling(name = 'GC_582',
                  value = '-(Clequ31x2x3x3*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_583 = Coupling(name = 'GC_583',
                  value = '(Clequ31x2x3x3*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_584 = Coupling(name = 'GC_584',
                  value = '-(Clequ31x2x3x3*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_585 = Coupling(name = 'GC_585',
                  value = '(Clequ31x2x3x3*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_586 = Coupling(name = 'GC_586',
                  value = '-(Clequ31x3x1x1*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_587 = Coupling(name = 'GC_587',
                  value = '(Clequ31x3x1x1*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_588 = Coupling(name = 'GC_588',
                  value = '-(Clequ31x3x1x1*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_589 = Coupling(name = 'GC_589',
                  value = '(Clequ31x3x1x1*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_590 = Coupling(name = 'GC_590',
                  value = '-(Clequ31x3x1x2*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_591 = Coupling(name = 'GC_591',
                  value = '(Clequ31x3x1x2*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_592 = Coupling(name = 'GC_592',
                  value = '-(Clequ31x3x1x2*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_593 = Coupling(name = 'GC_593',
                  value = '(Clequ31x3x1x2*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_594 = Coupling(name = 'GC_594',
                  value = '-(Clequ31x3x1x3*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_595 = Coupling(name = 'GC_595',
                  value = '(Clequ31x3x1x3*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_596 = Coupling(name = 'GC_596',
                  value = '-(Clequ31x3x1x3*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_597 = Coupling(name = 'GC_597',
                  value = '(Clequ31x3x1x3*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_598 = Coupling(name = 'GC_598',
                  value = '-(Clequ31x3x2x1*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_599 = Coupling(name = 'GC_599',
                  value = '(Clequ31x3x2x1*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_600 = Coupling(name = 'GC_600',
                  value = '-(Clequ31x3x2x1*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_601 = Coupling(name = 'GC_601',
                  value = '(Clequ31x3x2x1*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_602 = Coupling(name = 'GC_602',
                  value = '-(Clequ31x3x2x2*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_603 = Coupling(name = 'GC_603',
                  value = '(Clequ31x3x2x2*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_604 = Coupling(name = 'GC_604',
                  value = '-(Clequ31x3x2x2*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_605 = Coupling(name = 'GC_605',
                  value = '(Clequ31x3x2x2*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_606 = Coupling(name = 'GC_606',
                  value = '-(Clequ31x3x2x3*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_607 = Coupling(name = 'GC_607',
                  value = '(Clequ31x3x2x3*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_608 = Coupling(name = 'GC_608',
                  value = '-(Clequ31x3x2x3*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_609 = Coupling(name = 'GC_609',
                  value = '(Clequ31x3x2x3*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_610 = Coupling(name = 'GC_610',
                  value = '-(Clequ31x3x3x1*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_611 = Coupling(name = 'GC_611',
                  value = '(Clequ31x3x3x1*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_612 = Coupling(name = 'GC_612',
                  value = '-(Clequ31x3x3x1*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_613 = Coupling(name = 'GC_613',
                  value = '(Clequ31x3x3x1*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_614 = Coupling(name = 'GC_614',
                  value = '-(Clequ31x3x3x2*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_615 = Coupling(name = 'GC_615',
                  value = '(Clequ31x3x3x2*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_616 = Coupling(name = 'GC_616',
                  value = '-(Clequ31x3x3x2*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_617 = Coupling(name = 'GC_617',
                  value = '(Clequ31x3x3x2*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_618 = Coupling(name = 'GC_618',
                  value = '-(Clequ31x3x3x3*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_619 = Coupling(name = 'GC_619',
                  value = '(Clequ31x3x3x3*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_620 = Coupling(name = 'GC_620',
                  value = '-(Clequ31x3x3x3*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_621 = Coupling(name = 'GC_621',
                  value = '(Clequ31x3x3x3*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_622 = Coupling(name = 'GC_622',
                  value = '-(Clequ32x1x1x1*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_623 = Coupling(name = 'GC_623',
                  value = '(Clequ32x1x1x1*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_624 = Coupling(name = 'GC_624',
                  value = '-(Clequ32x1x1x1*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_625 = Coupling(name = 'GC_625',
                  value = '(Clequ32x1x1x1*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_626 = Coupling(name = 'GC_626',
                  value = '-(Clequ32x1x1x2*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_627 = Coupling(name = 'GC_627',
                  value = '(Clequ32x1x1x2*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_628 = Coupling(name = 'GC_628',
                  value = '-(Clequ32x1x1x2*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_629 = Coupling(name = 'GC_629',
                  value = '(Clequ32x1x1x2*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_630 = Coupling(name = 'GC_630',
                  value = '-(Clequ32x1x1x3*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_631 = Coupling(name = 'GC_631',
                  value = '(Clequ32x1x1x3*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_632 = Coupling(name = 'GC_632',
                  value = '-(Clequ32x1x1x3*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_633 = Coupling(name = 'GC_633',
                  value = '(Clequ32x1x1x3*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_634 = Coupling(name = 'GC_634',
                  value = '-(Clequ32x1x2x1*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_635 = Coupling(name = 'GC_635',
                  value = '(Clequ32x1x2x1*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_636 = Coupling(name = 'GC_636',
                  value = '-(Clequ32x1x2x1*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_637 = Coupling(name = 'GC_637',
                  value = '(Clequ32x1x2x1*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_638 = Coupling(name = 'GC_638',
                  value = '-(Clequ32x1x2x2*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_639 = Coupling(name = 'GC_639',
                  value = '(Clequ32x1x2x2*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_640 = Coupling(name = 'GC_640',
                  value = '-(Clequ32x1x2x2*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_641 = Coupling(name = 'GC_641',
                  value = '(Clequ32x1x2x2*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_642 = Coupling(name = 'GC_642',
                  value = '-(Clequ32x1x2x3*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_643 = Coupling(name = 'GC_643',
                  value = '(Clequ32x1x2x3*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_644 = Coupling(name = 'GC_644',
                  value = '-(Clequ32x1x2x3*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_645 = Coupling(name = 'GC_645',
                  value = '(Clequ32x1x2x3*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_646 = Coupling(name = 'GC_646',
                  value = '-(Clequ32x1x3x1*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_647 = Coupling(name = 'GC_647',
                  value = '(Clequ32x1x3x1*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_648 = Coupling(name = 'GC_648',
                  value = '-(Clequ32x1x3x1*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_649 = Coupling(name = 'GC_649',
                  value = '(Clequ32x1x3x1*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_650 = Coupling(name = 'GC_650',
                  value = '-(Clequ32x1x3x2*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_651 = Coupling(name = 'GC_651',
                  value = '(Clequ32x1x3x2*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_652 = Coupling(name = 'GC_652',
                  value = '-(Clequ32x1x3x2*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_653 = Coupling(name = 'GC_653',
                  value = '(Clequ32x1x3x2*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_654 = Coupling(name = 'GC_654',
                  value = '-(Clequ32x1x3x3*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_655 = Coupling(name = 'GC_655',
                  value = '(Clequ32x1x3x3*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_656 = Coupling(name = 'GC_656',
                  value = '-(Clequ32x1x3x3*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_657 = Coupling(name = 'GC_657',
                  value = '(Clequ32x1x3x3*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_658 = Coupling(name = 'GC_658',
                  value = '-(Clequ32x2x1x1*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_659 = Coupling(name = 'GC_659',
                  value = '(Clequ32x2x1x1*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_660 = Coupling(name = 'GC_660',
                  value = '-(Clequ32x2x1x1*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_661 = Coupling(name = 'GC_661',
                  value = '(Clequ32x2x1x1*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_662 = Coupling(name = 'GC_662',
                  value = '-(Clequ32x2x1x2*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_663 = Coupling(name = 'GC_663',
                  value = '(Clequ32x2x1x2*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_664 = Coupling(name = 'GC_664',
                  value = '-(Clequ32x2x1x2*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_665 = Coupling(name = 'GC_665',
                  value = '(Clequ32x2x1x2*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_666 = Coupling(name = 'GC_666',
                  value = '-(Clequ32x2x1x3*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_667 = Coupling(name = 'GC_667',
                  value = '(Clequ32x2x1x3*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_668 = Coupling(name = 'GC_668',
                  value = '-(Clequ32x2x1x3*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_669 = Coupling(name = 'GC_669',
                  value = '(Clequ32x2x1x3*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_670 = Coupling(name = 'GC_670',
                  value = '-(Clequ32x2x2x1*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_671 = Coupling(name = 'GC_671',
                  value = '(Clequ32x2x2x1*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_672 = Coupling(name = 'GC_672',
                  value = '-(Clequ32x2x2x1*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_673 = Coupling(name = 'GC_673',
                  value = '(Clequ32x2x2x1*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_674 = Coupling(name = 'GC_674',
                  value = '-(Clequ32x2x2x2*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_675 = Coupling(name = 'GC_675',
                  value = '(Clequ32x2x2x2*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_676 = Coupling(name = 'GC_676',
                  value = '-(Clequ32x2x2x2*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_677 = Coupling(name = 'GC_677',
                  value = '(Clequ32x2x2x2*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_678 = Coupling(name = 'GC_678',
                  value = '-(Clequ32x2x2x3*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_679 = Coupling(name = 'GC_679',
                  value = '(Clequ32x2x2x3*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_680 = Coupling(name = 'GC_680',
                  value = '-(Clequ32x2x2x3*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_681 = Coupling(name = 'GC_681',
                  value = '(Clequ32x2x2x3*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_682 = Coupling(name = 'GC_682',
                  value = '-(Clequ32x2x3x1*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_683 = Coupling(name = 'GC_683',
                  value = '(Clequ32x2x3x1*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_684 = Coupling(name = 'GC_684',
                  value = '-(Clequ32x2x3x1*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_685 = Coupling(name = 'GC_685',
                  value = '(Clequ32x2x3x1*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_686 = Coupling(name = 'GC_686',
                  value = '-(Clequ32x2x3x2*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_687 = Coupling(name = 'GC_687',
                  value = '(Clequ32x2x3x2*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_688 = Coupling(name = 'GC_688',
                  value = '-(Clequ32x2x3x2*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_689 = Coupling(name = 'GC_689',
                  value = '(Clequ32x2x3x2*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_690 = Coupling(name = 'GC_690',
                  value = '-(Clequ32x2x3x3*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_691 = Coupling(name = 'GC_691',
                  value = '(Clequ32x2x3x3*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_692 = Coupling(name = 'GC_692',
                  value = '-(Clequ32x2x3x3*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_693 = Coupling(name = 'GC_693',
                  value = '(Clequ32x2x3x3*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_694 = Coupling(name = 'GC_694',
                  value = '-(Clequ32x3x1x1*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_695 = Coupling(name = 'GC_695',
                  value = '(Clequ32x3x1x1*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_696 = Coupling(name = 'GC_696',
                  value = '-(Clequ32x3x1x1*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_697 = Coupling(name = 'GC_697',
                  value = '(Clequ32x3x1x1*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_698 = Coupling(name = 'GC_698',
                  value = '-(Clequ32x3x1x2*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_699 = Coupling(name = 'GC_699',
                  value = '(Clequ32x3x1x2*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_700 = Coupling(name = 'GC_700',
                  value = '-(Clequ32x3x1x2*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_701 = Coupling(name = 'GC_701',
                  value = '(Clequ32x3x1x2*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_702 = Coupling(name = 'GC_702',
                  value = '-(Clequ32x3x1x3*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_703 = Coupling(name = 'GC_703',
                  value = '(Clequ32x3x1x3*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_704 = Coupling(name = 'GC_704',
                  value = '-(Clequ32x3x1x3*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_705 = Coupling(name = 'GC_705',
                  value = '(Clequ32x3x1x3*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_706 = Coupling(name = 'GC_706',
                  value = '-(Clequ32x3x2x1*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_707 = Coupling(name = 'GC_707',
                  value = '(Clequ32x3x2x1*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_708 = Coupling(name = 'GC_708',
                  value = '-(Clequ32x3x2x1*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_709 = Coupling(name = 'GC_709',
                  value = '(Clequ32x3x2x1*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_710 = Coupling(name = 'GC_710',
                  value = '-(Clequ32x3x2x2*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_711 = Coupling(name = 'GC_711',
                  value = '(Clequ32x3x2x2*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_712 = Coupling(name = 'GC_712',
                  value = '-(Clequ32x3x2x2*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_713 = Coupling(name = 'GC_713',
                  value = '(Clequ32x3x2x2*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_714 = Coupling(name = 'GC_714',
                  value = '-(Clequ32x3x2x3*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_715 = Coupling(name = 'GC_715',
                  value = '(Clequ32x3x2x3*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_716 = Coupling(name = 'GC_716',
                  value = '-(Clequ32x3x2x3*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_717 = Coupling(name = 'GC_717',
                  value = '(Clequ32x3x2x3*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_718 = Coupling(name = 'GC_718',
                  value = '-(Clequ32x3x3x1*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_719 = Coupling(name = 'GC_719',
                  value = '(Clequ32x3x3x1*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_720 = Coupling(name = 'GC_720',
                  value = '-(Clequ32x3x3x1*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_721 = Coupling(name = 'GC_721',
                  value = '(Clequ32x3x3x1*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_722 = Coupling(name = 'GC_722',
                  value = '-(Clequ32x3x3x2*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_723 = Coupling(name = 'GC_723',
                  value = '(Clequ32x3x3x2*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_724 = Coupling(name = 'GC_724',
                  value = '-(Clequ32x3x3x2*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_725 = Coupling(name = 'GC_725',
                  value = '(Clequ32x3x3x2*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_726 = Coupling(name = 'GC_726',
                  value = '-(Clequ32x3x3x3*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_727 = Coupling(name = 'GC_727',
                  value = '(Clequ32x3x3x3*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_728 = Coupling(name = 'GC_728',
                  value = '-(Clequ32x3x3x3*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_729 = Coupling(name = 'GC_729',
                  value = '(Clequ32x3x3x3*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_730 = Coupling(name = 'GC_730',
                  value = '-(Clequ33x1x1x1*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_731 = Coupling(name = 'GC_731',
                  value = '(Clequ33x1x1x1*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_732 = Coupling(name = 'GC_732',
                  value = '-(Clequ33x1x1x1*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_733 = Coupling(name = 'GC_733',
                  value = '(Clequ33x1x1x1*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_734 = Coupling(name = 'GC_734',
                  value = '-(Clequ33x1x1x2*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_735 = Coupling(name = 'GC_735',
                  value = '(Clequ33x1x1x2*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_736 = Coupling(name = 'GC_736',
                  value = '-(Clequ33x1x1x2*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_737 = Coupling(name = 'GC_737',
                  value = '(Clequ33x1x1x2*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_738 = Coupling(name = 'GC_738',
                  value = '-(Clequ33x1x1x3*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_739 = Coupling(name = 'GC_739',
                  value = '(Clequ33x1x1x3*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_740 = Coupling(name = 'GC_740',
                  value = '-(Clequ33x1x1x3*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_741 = Coupling(name = 'GC_741',
                  value = '(Clequ33x1x1x3*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_742 = Coupling(name = 'GC_742',
                  value = '-(Clequ33x1x2x1*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_743 = Coupling(name = 'GC_743',
                  value = '(Clequ33x1x2x1*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_744 = Coupling(name = 'GC_744',
                  value = '-(Clequ33x1x2x1*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_745 = Coupling(name = 'GC_745',
                  value = '(Clequ33x1x2x1*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_746 = Coupling(name = 'GC_746',
                  value = '-(Clequ33x1x2x2*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_747 = Coupling(name = 'GC_747',
                  value = '(Clequ33x1x2x2*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_748 = Coupling(name = 'GC_748',
                  value = '-(Clequ33x1x2x2*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_749 = Coupling(name = 'GC_749',
                  value = '(Clequ33x1x2x2*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_750 = Coupling(name = 'GC_750',
                  value = '-(Clequ33x1x2x3*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_751 = Coupling(name = 'GC_751',
                  value = '(Clequ33x1x2x3*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_752 = Coupling(name = 'GC_752',
                  value = '-(Clequ33x1x2x3*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_753 = Coupling(name = 'GC_753',
                  value = '(Clequ33x1x2x3*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_754 = Coupling(name = 'GC_754',
                  value = '-(Clequ33x1x3x1*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_755 = Coupling(name = 'GC_755',
                  value = '(Clequ33x1x3x1*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_756 = Coupling(name = 'GC_756',
                  value = '-(Clequ33x1x3x1*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_757 = Coupling(name = 'GC_757',
                  value = '(Clequ33x1x3x1*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_758 = Coupling(name = 'GC_758',
                  value = '-(Clequ33x1x3x2*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_759 = Coupling(name = 'GC_759',
                  value = '(Clequ33x1x3x2*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_760 = Coupling(name = 'GC_760',
                  value = '-(Clequ33x1x3x2*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_761 = Coupling(name = 'GC_761',
                  value = '(Clequ33x1x3x2*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_762 = Coupling(name = 'GC_762',
                  value = '-(Clequ33x1x3x3*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_763 = Coupling(name = 'GC_763',
                  value = '(Clequ33x1x3x3*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_764 = Coupling(name = 'GC_764',
                  value = '-(Clequ33x1x3x3*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_765 = Coupling(name = 'GC_765',
                  value = '(Clequ33x1x3x3*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_766 = Coupling(name = 'GC_766',
                  value = '-(Clequ33x2x1x1*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_767 = Coupling(name = 'GC_767',
                  value = '(Clequ33x2x1x1*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_768 = Coupling(name = 'GC_768',
                  value = '-(Clequ33x2x1x1*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_769 = Coupling(name = 'GC_769',
                  value = '(Clequ33x2x1x1*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_770 = Coupling(name = 'GC_770',
                  value = '-(Clequ33x2x1x2*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_771 = Coupling(name = 'GC_771',
                  value = '(Clequ33x2x1x2*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_772 = Coupling(name = 'GC_772',
                  value = '-(Clequ33x2x1x2*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_773 = Coupling(name = 'GC_773',
                  value = '(Clequ33x2x1x2*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_774 = Coupling(name = 'GC_774',
                  value = '-(Clequ33x2x1x3*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_775 = Coupling(name = 'GC_775',
                  value = '(Clequ33x2x1x3*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_776 = Coupling(name = 'GC_776',
                  value = '-(Clequ33x2x1x3*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_777 = Coupling(name = 'GC_777',
                  value = '(Clequ33x2x1x3*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_778 = Coupling(name = 'GC_778',
                  value = '-(Clequ33x2x2x1*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_779 = Coupling(name = 'GC_779',
                  value = '(Clequ33x2x2x1*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_780 = Coupling(name = 'GC_780',
                  value = '-(Clequ33x2x2x1*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_781 = Coupling(name = 'GC_781',
                  value = '(Clequ33x2x2x1*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_782 = Coupling(name = 'GC_782',
                  value = '-(Clequ33x2x2x2*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_783 = Coupling(name = 'GC_783',
                  value = '(Clequ33x2x2x2*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_784 = Coupling(name = 'GC_784',
                  value = '-(Clequ33x2x2x2*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_785 = Coupling(name = 'GC_785',
                  value = '(Clequ33x2x2x2*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_786 = Coupling(name = 'GC_786',
                  value = '-(Clequ33x2x2x3*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_787 = Coupling(name = 'GC_787',
                  value = '(Clequ33x2x2x3*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_788 = Coupling(name = 'GC_788',
                  value = '-(Clequ33x2x2x3*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_789 = Coupling(name = 'GC_789',
                  value = '(Clequ33x2x2x3*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_790 = Coupling(name = 'GC_790',
                  value = '-(Clequ33x2x3x1*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_791 = Coupling(name = 'GC_791',
                  value = '(Clequ33x2x3x1*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_792 = Coupling(name = 'GC_792',
                  value = '-(Clequ33x2x3x1*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_793 = Coupling(name = 'GC_793',
                  value = '(Clequ33x2x3x1*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_794 = Coupling(name = 'GC_794',
                  value = '-(Clequ33x2x3x2*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_795 = Coupling(name = 'GC_795',
                  value = '(Clequ33x2x3x2*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_796 = Coupling(name = 'GC_796',
                  value = '-(Clequ33x2x3x2*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_797 = Coupling(name = 'GC_797',
                  value = '(Clequ33x2x3x2*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_798 = Coupling(name = 'GC_798',
                  value = '-(Clequ33x2x3x3*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_799 = Coupling(name = 'GC_799',
                  value = '(Clequ33x2x3x3*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_800 = Coupling(name = 'GC_800',
                  value = '-(Clequ33x2x3x3*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_801 = Coupling(name = 'GC_801',
                  value = '(Clequ33x2x3x3*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_802 = Coupling(name = 'GC_802',
                  value = '-(Clequ33x3x1x1*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_803 = Coupling(name = 'GC_803',
                  value = '(Clequ33x3x1x1*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_804 = Coupling(name = 'GC_804',
                  value = '-(Clequ33x3x1x1*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_805 = Coupling(name = 'GC_805',
                  value = '(Clequ33x3x1x1*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_806 = Coupling(name = 'GC_806',
                  value = '-(Clequ33x3x1x2*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_807 = Coupling(name = 'GC_807',
                  value = '(Clequ33x3x1x2*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_808 = Coupling(name = 'GC_808',
                  value = '-(Clequ33x3x1x2*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_809 = Coupling(name = 'GC_809',
                  value = '(Clequ33x3x1x2*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_810 = Coupling(name = 'GC_810',
                  value = '-(Clequ33x3x1x3*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_811 = Coupling(name = 'GC_811',
                  value = '(Clequ33x3x1x3*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_812 = Coupling(name = 'GC_812',
                  value = '-(Clequ33x3x1x3*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_813 = Coupling(name = 'GC_813',
                  value = '(Clequ33x3x1x3*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_814 = Coupling(name = 'GC_814',
                  value = '-(Clequ33x3x2x1*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_815 = Coupling(name = 'GC_815',
                  value = '(Clequ33x3x2x1*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_816 = Coupling(name = 'GC_816',
                  value = '-(Clequ33x3x2x1*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_817 = Coupling(name = 'GC_817',
                  value = '(Clequ33x3x2x1*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_818 = Coupling(name = 'GC_818',
                  value = '-(Clequ33x3x2x2*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_819 = Coupling(name = 'GC_819',
                  value = '(Clequ33x3x2x2*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_820 = Coupling(name = 'GC_820',
                  value = '-(Clequ33x3x2x2*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_821 = Coupling(name = 'GC_821',
                  value = '(Clequ33x3x2x2*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_822 = Coupling(name = 'GC_822',
                  value = '-(Clequ33x3x2x3*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_823 = Coupling(name = 'GC_823',
                  value = '(Clequ33x3x2x3*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_824 = Coupling(name = 'GC_824',
                  value = '-(Clequ33x3x2x3*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_825 = Coupling(name = 'GC_825',
                  value = '(Clequ33x3x2x3*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_826 = Coupling(name = 'GC_826',
                  value = '-(Clequ33x3x3x1*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_827 = Coupling(name = 'GC_827',
                  value = '(Clequ33x3x3x1*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_828 = Coupling(name = 'GC_828',
                  value = '-(Clequ33x3x3x1*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_829 = Coupling(name = 'GC_829',
                  value = '(Clequ33x3x3x1*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_830 = Coupling(name = 'GC_830',
                  value = '-(Clequ33x3x3x2*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_831 = Coupling(name = 'GC_831',
                  value = '(Clequ33x3x3x2*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_832 = Coupling(name = 'GC_832',
                  value = '-(Clequ33x3x3x2*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_833 = Coupling(name = 'GC_833',
                  value = '(Clequ33x3x3x2*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_834 = Coupling(name = 'GC_834',
                  value = '-(Clequ33x3x3x3*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_835 = Coupling(name = 'GC_835',
                  value = '(Clequ33x3x3x3*complex(0,1)*Lam*qeddim**2)/4.',
                  order = {'LAM':1,'QED':2})

GC_836 = Coupling(name = 'GC_836',
                  value = '-(Clequ33x3x3x3*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_837 = Coupling(name = 'GC_837',
                  value = '(Clequ33x3x3x3*complex(0,1)*Lam*qeddim**2)/2.',
                  order = {'LAM':1,'QED':2})

GC_838 = Coupling(name = 'GC_838',
                  value = '2*Cll1x1x1x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_839 = Coupling(name = 'GC_839',
                  value = '2*Cll1x2x1x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_840 = Coupling(name = 'GC_840',
                  value = '2*Cll1x3x1x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_841 = Coupling(name = 'GC_841',
                  value = '2*Cll2x1x2x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_842 = Coupling(name = 'GC_842',
                  value = '2*Cll2x2x2x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_843 = Coupling(name = 'GC_843',
                  value = '2*Cll2x3x2x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_844 = Coupling(name = 'GC_844',
                  value = '2*Cll3x1x3x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_845 = Coupling(name = 'GC_845',
                  value = '2*Cll3x2x3x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_846 = Coupling(name = 'GC_846',
                  value = '2*Cll3x3x3x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_847 = Coupling(name = 'GC_847',
                  value = 'Clq13x3x3x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_848 = Coupling(name = 'GC_848',
                  value = '2*Clq31x1x1x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_849 = Coupling(name = 'GC_849',
                  value = '2*Clq31x1x1x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_850 = Coupling(name = 'GC_850',
                  value = '2*Clq31x1x1x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_851 = Coupling(name = 'GC_851',
                  value = '2*Clq31x1x2x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_852 = Coupling(name = 'GC_852',
                  value = '2*Clq31x1x2x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_853 = Coupling(name = 'GC_853',
                  value = '2*Clq31x1x2x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_854 = Coupling(name = 'GC_854',
                  value = '2*Clq31x1x3x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_855 = Coupling(name = 'GC_855',
                  value = '2*Clq31x1x3x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_856 = Coupling(name = 'GC_856',
                  value = '2*Clq31x1x3x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_857 = Coupling(name = 'GC_857',
                  value = '2*Clq31x2x1x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_858 = Coupling(name = 'GC_858',
                  value = '2*Clq31x2x1x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_859 = Coupling(name = 'GC_859',
                  value = '2*Clq31x2x1x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_860 = Coupling(name = 'GC_860',
                  value = '2*Clq31x2x2x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_861 = Coupling(name = 'GC_861',
                  value = '2*Clq31x2x2x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_862 = Coupling(name = 'GC_862',
                  value = '2*Clq31x2x2x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_863 = Coupling(name = 'GC_863',
                  value = '2*Clq31x2x3x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_864 = Coupling(name = 'GC_864',
                  value = '2*Clq31x2x3x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_865 = Coupling(name = 'GC_865',
                  value = '2*Clq31x2x3x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_866 = Coupling(name = 'GC_866',
                  value = '2*Clq31x3x1x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_867 = Coupling(name = 'GC_867',
                  value = '2*Clq31x3x1x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_868 = Coupling(name = 'GC_868',
                  value = '2*Clq31x3x1x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_869 = Coupling(name = 'GC_869',
                  value = '2*Clq31x3x2x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_870 = Coupling(name = 'GC_870',
                  value = '2*Clq31x3x2x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_871 = Coupling(name = 'GC_871',
                  value = '2*Clq31x3x2x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_872 = Coupling(name = 'GC_872',
                  value = '2*Clq31x3x3x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_873 = Coupling(name = 'GC_873',
                  value = '2*Clq31x3x3x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_874 = Coupling(name = 'GC_874',
                  value = '2*Clq31x3x3x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_875 = Coupling(name = 'GC_875',
                  value = '2*Clq32x1x1x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_876 = Coupling(name = 'GC_876',
                  value = '2*Clq32x1x1x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_877 = Coupling(name = 'GC_877',
                  value = '2*Clq32x1x1x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_878 = Coupling(name = 'GC_878',
                  value = '2*Clq32x1x2x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_879 = Coupling(name = 'GC_879',
                  value = '2*Clq32x1x2x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_880 = Coupling(name = 'GC_880',
                  value = '2*Clq32x1x2x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_881 = Coupling(name = 'GC_881',
                  value = '2*Clq32x1x3x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_882 = Coupling(name = 'GC_882',
                  value = '2*Clq32x1x3x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_883 = Coupling(name = 'GC_883',
                  value = '2*Clq32x1x3x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_884 = Coupling(name = 'GC_884',
                  value = '2*Clq32x2x1x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_885 = Coupling(name = 'GC_885',
                  value = '2*Clq32x2x1x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_886 = Coupling(name = 'GC_886',
                  value = '2*Clq32x2x1x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_887 = Coupling(name = 'GC_887',
                  value = '2*Clq32x2x2x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_888 = Coupling(name = 'GC_888',
                  value = '2*Clq32x2x2x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_889 = Coupling(name = 'GC_889',
                  value = '2*Clq32x2x2x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_890 = Coupling(name = 'GC_890',
                  value = '2*Clq32x2x3x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_891 = Coupling(name = 'GC_891',
                  value = '2*Clq32x2x3x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_892 = Coupling(name = 'GC_892',
                  value = '2*Clq32x2x3x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_893 = Coupling(name = 'GC_893',
                  value = '2*Clq32x3x1x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_894 = Coupling(name = 'GC_894',
                  value = '2*Clq32x3x1x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_895 = Coupling(name = 'GC_895',
                  value = '2*Clq32x3x1x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_896 = Coupling(name = 'GC_896',
                  value = '2*Clq32x3x2x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_897 = Coupling(name = 'GC_897',
                  value = '2*Clq32x3x2x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_898 = Coupling(name = 'GC_898',
                  value = '2*Clq32x3x2x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_899 = Coupling(name = 'GC_899',
                  value = '2*Clq32x3x3x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_900 = Coupling(name = 'GC_900',
                  value = '2*Clq32x3x3x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_901 = Coupling(name = 'GC_901',
                  value = '2*Clq32x3x3x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_902 = Coupling(name = 'GC_902',
                  value = '2*Clq33x1x1x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_903 = Coupling(name = 'GC_903',
                  value = '2*Clq33x1x1x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_904 = Coupling(name = 'GC_904',
                  value = '2*Clq33x1x1x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_905 = Coupling(name = 'GC_905',
                  value = '2*Clq33x1x2x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_906 = Coupling(name = 'GC_906',
                  value = '2*Clq33x1x2x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_907 = Coupling(name = 'GC_907',
                  value = '2*Clq33x1x2x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_908 = Coupling(name = 'GC_908',
                  value = '2*Clq33x1x3x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_909 = Coupling(name = 'GC_909',
                  value = '2*Clq33x1x3x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_910 = Coupling(name = 'GC_910',
                  value = '2*Clq33x1x3x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_911 = Coupling(name = 'GC_911',
                  value = '2*Clq33x2x1x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_912 = Coupling(name = 'GC_912',
                  value = '2*Clq33x2x1x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_913 = Coupling(name = 'GC_913',
                  value = '2*Clq33x2x1x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_914 = Coupling(name = 'GC_914',
                  value = '2*Clq33x2x2x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_915 = Coupling(name = 'GC_915',
                  value = '2*Clq33x2x2x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_916 = Coupling(name = 'GC_916',
                  value = '2*Clq33x2x2x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_917 = Coupling(name = 'GC_917',
                  value = '2*Clq33x2x3x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_918 = Coupling(name = 'GC_918',
                  value = '2*Clq33x2x3x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_919 = Coupling(name = 'GC_919',
                  value = '2*Clq33x2x3x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_920 = Coupling(name = 'GC_920',
                  value = '2*Clq33x3x1x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_921 = Coupling(name = 'GC_921',
                  value = '2*Clq33x3x1x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_922 = Coupling(name = 'GC_922',
                  value = '2*Clq33x3x1x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_923 = Coupling(name = 'GC_923',
                  value = '2*Clq33x3x2x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_924 = Coupling(name = 'GC_924',
                  value = '2*Clq33x3x2x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_925 = Coupling(name = 'GC_925',
                  value = '2*Clq33x3x2x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_926 = Coupling(name = 'GC_926',
                  value = '2*Clq33x3x3x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_927 = Coupling(name = 'GC_927',
                  value = '2*Clq33x3x3x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_928 = Coupling(name = 'GC_928',
                  value = '-(Clq33x3x3x3*complex(0,1)*Lam*qeddim**2)',
                  order = {'LAM':1,'QED':2})

GC_929 = Coupling(name = 'GC_929',
                  value = '2*Clq33x3x3x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_930 = Coupling(name = 'GC_930',
                  value = 'Clu1x1x1x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_931 = Coupling(name = 'GC_931',
                  value = 'Clu1x1x1x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_932 = Coupling(name = 'GC_932',
                  value = 'Clu1x1x1x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_933 = Coupling(name = 'GC_933',
                  value = 'Clu1x1x2x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_934 = Coupling(name = 'GC_934',
                  value = 'Clu1x1x2x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_935 = Coupling(name = 'GC_935',
                  value = 'Clu1x1x2x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_936 = Coupling(name = 'GC_936',
                  value = 'Clu1x1x3x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_937 = Coupling(name = 'GC_937',
                  value = 'Clu1x1x3x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_938 = Coupling(name = 'GC_938',
                  value = 'Clu1x1x3x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_939 = Coupling(name = 'GC_939',
                  value = 'Clu1x2x1x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_940 = Coupling(name = 'GC_940',
                  value = 'Clu1x2x1x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_941 = Coupling(name = 'GC_941',
                  value = 'Clu1x2x1x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_942 = Coupling(name = 'GC_942',
                  value = 'Clu1x2x2x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_943 = Coupling(name = 'GC_943',
                  value = 'Clu1x2x2x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_944 = Coupling(name = 'GC_944',
                  value = 'Clu1x2x2x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_945 = Coupling(name = 'GC_945',
                  value = 'Clu1x2x3x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_946 = Coupling(name = 'GC_946',
                  value = 'Clu1x2x3x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_947 = Coupling(name = 'GC_947',
                  value = 'Clu1x2x3x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_948 = Coupling(name = 'GC_948',
                  value = 'Clu1x3x1x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_949 = Coupling(name = 'GC_949',
                  value = 'Clu1x3x1x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_950 = Coupling(name = 'GC_950',
                  value = 'Clu1x3x1x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_951 = Coupling(name = 'GC_951',
                  value = 'Clu1x3x2x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_952 = Coupling(name = 'GC_952',
                  value = 'Clu1x3x2x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_953 = Coupling(name = 'GC_953',
                  value = 'Clu1x3x2x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_954 = Coupling(name = 'GC_954',
                  value = 'Clu1x3x3x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_955 = Coupling(name = 'GC_955',
                  value = 'Clu1x3x3x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_956 = Coupling(name = 'GC_956',
                  value = 'Clu1x3x3x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_957 = Coupling(name = 'GC_957',
                  value = 'Clu2x1x1x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_958 = Coupling(name = 'GC_958',
                  value = 'Clu2x1x1x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_959 = Coupling(name = 'GC_959',
                  value = 'Clu2x1x1x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_960 = Coupling(name = 'GC_960',
                  value = 'Clu2x1x2x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_961 = Coupling(name = 'GC_961',
                  value = 'Clu2x1x2x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_962 = Coupling(name = 'GC_962',
                  value = 'Clu2x1x2x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_963 = Coupling(name = 'GC_963',
                  value = 'Clu2x1x3x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_964 = Coupling(name = 'GC_964',
                  value = 'Clu2x1x3x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_965 = Coupling(name = 'GC_965',
                  value = 'Clu2x1x3x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_966 = Coupling(name = 'GC_966',
                  value = 'Clu2x2x1x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_967 = Coupling(name = 'GC_967',
                  value = 'Clu2x2x1x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_968 = Coupling(name = 'GC_968',
                  value = 'Clu2x2x1x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_969 = Coupling(name = 'GC_969',
                  value = 'Clu2x2x2x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_970 = Coupling(name = 'GC_970',
                  value = 'Clu2x2x2x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_971 = Coupling(name = 'GC_971',
                  value = 'Clu2x2x2x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_972 = Coupling(name = 'GC_972',
                  value = 'Clu2x2x3x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_973 = Coupling(name = 'GC_973',
                  value = 'Clu2x2x3x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_974 = Coupling(name = 'GC_974',
                  value = 'Clu2x2x3x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_975 = Coupling(name = 'GC_975',
                  value = 'Clu2x3x1x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_976 = Coupling(name = 'GC_976',
                  value = 'Clu2x3x1x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_977 = Coupling(name = 'GC_977',
                  value = 'Clu2x3x1x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_978 = Coupling(name = 'GC_978',
                  value = 'Clu2x3x2x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_979 = Coupling(name = 'GC_979',
                  value = 'Clu2x3x2x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_980 = Coupling(name = 'GC_980',
                  value = 'Clu2x3x2x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_981 = Coupling(name = 'GC_981',
                  value = 'Clu2x3x3x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_982 = Coupling(name = 'GC_982',
                  value = 'Clu2x3x3x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_983 = Coupling(name = 'GC_983',
                  value = 'Clu2x3x3x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_984 = Coupling(name = 'GC_984',
                  value = 'Clu3x1x1x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_985 = Coupling(name = 'GC_985',
                  value = 'Clu3x1x1x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_986 = Coupling(name = 'GC_986',
                  value = 'Clu3x1x1x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_987 = Coupling(name = 'GC_987',
                  value = 'Clu3x1x2x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_988 = Coupling(name = 'GC_988',
                  value = 'Clu3x1x2x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_989 = Coupling(name = 'GC_989',
                  value = 'Clu3x1x2x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_990 = Coupling(name = 'GC_990',
                  value = 'Clu3x1x3x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_991 = Coupling(name = 'GC_991',
                  value = 'Clu3x1x3x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_992 = Coupling(name = 'GC_992',
                  value = 'Clu3x1x3x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_993 = Coupling(name = 'GC_993',
                  value = 'Clu3x2x1x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_994 = Coupling(name = 'GC_994',
                  value = 'Clu3x2x1x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_995 = Coupling(name = 'GC_995',
                  value = 'Clu3x2x1x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_996 = Coupling(name = 'GC_996',
                  value = 'Clu3x2x2x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_997 = Coupling(name = 'GC_997',
                  value = 'Clu3x2x2x2*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_998 = Coupling(name = 'GC_998',
                  value = 'Clu3x2x2x3*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_999 = Coupling(name = 'GC_999',
                  value = 'Clu3x2x3x1*complex(0,1)*Lam*qeddim**2',
                  order = {'LAM':1,'QED':2})

GC_1000 = Coupling(name = 'GC_1000',
                   value = 'Clu3x2x3x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1001 = Coupling(name = 'GC_1001',
                   value = 'Clu3x2x3x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1002 = Coupling(name = 'GC_1002',
                   value = 'Clu3x3x1x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1003 = Coupling(name = 'GC_1003',
                   value = 'Clu3x3x1x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1004 = Coupling(name = 'GC_1004',
                   value = 'Clu3x3x1x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1005 = Coupling(name = 'GC_1005',
                   value = 'Clu3x3x2x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1006 = Coupling(name = 'GC_1006',
                   value = 'Clu3x3x2x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1007 = Coupling(name = 'GC_1007',
                   value = 'Clu3x3x2x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1008 = Coupling(name = 'GC_1008',
                   value = 'Clu3x3x3x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1009 = Coupling(name = 'GC_1009',
                   value = 'Clu3x3x3x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1010 = Coupling(name = 'GC_1010',
                   value = 'Clu3x3x3x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1011 = Coupling(name = 'GC_1011',
                   value = '-(CphiD*Lam*qeddim**2)/2.',
                   order = {'LAM':1,'QED':2})

GC_1012 = Coupling(name = 'GC_1012',
                   value = '-(CphiD*complex(0,1)*Lam*qeddim**2)/2.',
                   order = {'LAM':1,'QED':2})

GC_1013 = Coupling(name = 'GC_1013',
                   value = '(CphiD*Lam*qeddim**2)/2.',
                   order = {'LAM':1,'QED':2})

GC_1014 = Coupling(name = 'GC_1014',
                   value = '-4*CphiG*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1015 = Coupling(name = 'GC_1015',
                   value = '4*CphiG*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1016 = Coupling(name = 'GC_1016',
                   value = '-(Cphik*complex(0,1)*Lam*qeddim**2)',
                   order = {'LAM':1,'QED':2})

GC_1017 = Coupling(name = 'GC_1017',
                   value = '-2*Cphik*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1018 = Coupling(name = 'GC_1018',
                   value = '-3*Cphik*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1019 = Coupling(name = 'GC_1019',
                   value = '-4*CphiW*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1020 = Coupling(name = 'GC_1020',
                   value = '4*CphiW*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1021 = Coupling(name = 'GC_1021',
                   value = 'Cqe1x1x1x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1022 = Coupling(name = 'GC_1022',
                   value = 'Cqe1x1x1x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1023 = Coupling(name = 'GC_1023',
                   value = 'Cqe1x1x1x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1024 = Coupling(name = 'GC_1024',
                   value = 'Cqe1x1x2x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1025 = Coupling(name = 'GC_1025',
                   value = 'Cqe1x1x2x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1026 = Coupling(name = 'GC_1026',
                   value = 'Cqe1x1x2x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1027 = Coupling(name = 'GC_1027',
                   value = 'Cqe1x1x3x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1028 = Coupling(name = 'GC_1028',
                   value = 'Cqe1x1x3x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1029 = Coupling(name = 'GC_1029',
                   value = 'Cqe1x1x3x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1030 = Coupling(name = 'GC_1030',
                   value = 'Cqe1x2x1x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1031 = Coupling(name = 'GC_1031',
                   value = 'Cqe1x2x1x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1032 = Coupling(name = 'GC_1032',
                   value = 'Cqe1x2x1x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1033 = Coupling(name = 'GC_1033',
                   value = 'Cqe1x2x2x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1034 = Coupling(name = 'GC_1034',
                   value = 'Cqe1x2x2x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1035 = Coupling(name = 'GC_1035',
                   value = 'Cqe1x2x2x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1036 = Coupling(name = 'GC_1036',
                   value = 'Cqe1x2x3x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1037 = Coupling(name = 'GC_1037',
                   value = 'Cqe1x2x3x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1038 = Coupling(name = 'GC_1038',
                   value = 'Cqe1x2x3x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1039 = Coupling(name = 'GC_1039',
                   value = 'Cqe1x3x1x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1040 = Coupling(name = 'GC_1040',
                   value = 'Cqe1x3x1x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1041 = Coupling(name = 'GC_1041',
                   value = 'Cqe1x3x1x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1042 = Coupling(name = 'GC_1042',
                   value = 'Cqe1x3x2x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1043 = Coupling(name = 'GC_1043',
                   value = 'Cqe1x3x2x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1044 = Coupling(name = 'GC_1044',
                   value = 'Cqe1x3x2x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1045 = Coupling(name = 'GC_1045',
                   value = 'Cqe1x3x3x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1046 = Coupling(name = 'GC_1046',
                   value = 'Cqe1x3x3x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1047 = Coupling(name = 'GC_1047',
                   value = 'Cqe1x3x3x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1048 = Coupling(name = 'GC_1048',
                   value = 'Cqe2x1x1x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1049 = Coupling(name = 'GC_1049',
                   value = 'Cqe2x1x1x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1050 = Coupling(name = 'GC_1050',
                   value = 'Cqe2x1x1x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1051 = Coupling(name = 'GC_1051',
                   value = 'Cqe2x1x2x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1052 = Coupling(name = 'GC_1052',
                   value = 'Cqe2x1x2x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1053 = Coupling(name = 'GC_1053',
                   value = 'Cqe2x1x2x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1054 = Coupling(name = 'GC_1054',
                   value = 'Cqe2x1x3x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1055 = Coupling(name = 'GC_1055',
                   value = 'Cqe2x1x3x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1056 = Coupling(name = 'GC_1056',
                   value = 'Cqe2x1x3x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1057 = Coupling(name = 'GC_1057',
                   value = 'Cqe2x2x1x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1058 = Coupling(name = 'GC_1058',
                   value = 'Cqe2x2x1x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1059 = Coupling(name = 'GC_1059',
                   value = 'Cqe2x2x1x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1060 = Coupling(name = 'GC_1060',
                   value = 'Cqe2x2x2x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1061 = Coupling(name = 'GC_1061',
                   value = 'Cqe2x2x2x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1062 = Coupling(name = 'GC_1062',
                   value = 'Cqe2x2x2x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1063 = Coupling(name = 'GC_1063',
                   value = 'Cqe2x2x3x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1064 = Coupling(name = 'GC_1064',
                   value = 'Cqe2x2x3x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1065 = Coupling(name = 'GC_1065',
                   value = 'Cqe2x2x3x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1066 = Coupling(name = 'GC_1066',
                   value = 'Cqe2x3x1x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1067 = Coupling(name = 'GC_1067',
                   value = 'Cqe2x3x1x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1068 = Coupling(name = 'GC_1068',
                   value = 'Cqe2x3x1x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1069 = Coupling(name = 'GC_1069',
                   value = 'Cqe2x3x2x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1070 = Coupling(name = 'GC_1070',
                   value = 'Cqe2x3x2x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1071 = Coupling(name = 'GC_1071',
                   value = 'Cqe2x3x2x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1072 = Coupling(name = 'GC_1072',
                   value = 'Cqe2x3x3x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1073 = Coupling(name = 'GC_1073',
                   value = 'Cqe2x3x3x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1074 = Coupling(name = 'GC_1074',
                   value = 'Cqe2x3x3x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1075 = Coupling(name = 'GC_1075',
                   value = 'Cqe3x1x1x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1076 = Coupling(name = 'GC_1076',
                   value = 'Cqe3x1x1x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1077 = Coupling(name = 'GC_1077',
                   value = 'Cqe3x1x1x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1078 = Coupling(name = 'GC_1078',
                   value = 'Cqe3x1x2x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1079 = Coupling(name = 'GC_1079',
                   value = 'Cqe3x1x2x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1080 = Coupling(name = 'GC_1080',
                   value = 'Cqe3x1x2x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1081 = Coupling(name = 'GC_1081',
                   value = 'Cqe3x1x3x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1082 = Coupling(name = 'GC_1082',
                   value = 'Cqe3x1x3x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1083 = Coupling(name = 'GC_1083',
                   value = 'Cqe3x1x3x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1084 = Coupling(name = 'GC_1084',
                   value = 'Cqe3x2x1x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1085 = Coupling(name = 'GC_1085',
                   value = 'Cqe3x2x1x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1086 = Coupling(name = 'GC_1086',
                   value = 'Cqe3x2x1x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1087 = Coupling(name = 'GC_1087',
                   value = 'Cqe3x2x2x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1088 = Coupling(name = 'GC_1088',
                   value = 'Cqe3x2x2x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1089 = Coupling(name = 'GC_1089',
                   value = 'Cqe3x2x2x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1090 = Coupling(name = 'GC_1090',
                   value = 'Cqe3x2x3x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1091 = Coupling(name = 'GC_1091',
                   value = 'Cqe3x2x3x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1092 = Coupling(name = 'GC_1092',
                   value = 'Cqe3x2x3x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1093 = Coupling(name = 'GC_1093',
                   value = 'Cqe3x3x1x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1094 = Coupling(name = 'GC_1094',
                   value = 'Cqe3x3x1x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1095 = Coupling(name = 'GC_1095',
                   value = 'Cqe3x3x1x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1096 = Coupling(name = 'GC_1096',
                   value = 'Cqe3x3x2x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1097 = Coupling(name = 'GC_1097',
                   value = 'Cqe3x3x2x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1098 = Coupling(name = 'GC_1098',
                   value = 'Cqe3x3x2x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1099 = Coupling(name = 'GC_1099',
                   value = 'Cqe3x3x3x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1100 = Coupling(name = 'GC_1100',
                   value = 'Cqe3x3x3x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1101 = Coupling(name = 'GC_1101',
                   value = 'Cqe3x3x3x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1102 = Coupling(name = 'GC_1102',
                   value = '-2*CphiWB*cw*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1103 = Coupling(name = 'GC_1103',
                   value = '-2*CphiWB*cw*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1104 = Coupling(name = 'GC_1104',
                   value = '2*CphiWB*cw*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1105 = Coupling(name = 'GC_1105',
                   value = '2*CphiWB*cw*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1106 = Coupling(name = 'GC_1106',
                   value = '-(CphiD*ee*Lam*qeddim**2)',
                   order = {'LAM':1,'QED':3})

GC_1107 = Coupling(name = 'GC_1107',
                   value = '-2*CphiD*ee*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':3})

GC_1108 = Coupling(name = 'GC_1108',
                   value = '2*CphiD*ee*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':3})

GC_1109 = Coupling(name = 'GC_1109',
                   value = 'CphiD*ee*Lam*qeddim**2',
                   order = {'LAM':1,'QED':3})

GC_1110 = Coupling(name = 'GC_1110',
                   value = '-4*CphiW*ee*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':3})

GC_1111 = Coupling(name = 'GC_1111',
                   value = '4*CphiW*ee*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':3})

GC_1112 = Coupling(name = 'GC_1112',
                   value = '-2*CphiWB*cw*ee*Lam*qeddim**2',
                   order = {'LAM':1,'QED':3})

GC_1113 = Coupling(name = 'GC_1113',
                   value = '-2*CphiWB*cw*ee*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':3})

GC_1114 = Coupling(name = 'GC_1114',
                   value = '2*CphiWB*cw*ee*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':3})

GC_1115 = Coupling(name = 'GC_1115',
                   value = '2*CphiWB*cw*ee*Lam*qeddim**2',
                   order = {'LAM':1,'QED':3})

GC_1116 = Coupling(name = 'GC_1116',
                   value = '8*CphiD*ee**2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':4})

GC_1117 = Coupling(name = 'GC_1117',
                   value = '-4*CphiW*ee**2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':4})

GC_1118 = Coupling(name = 'GC_1118',
                   value = '8*CphiW*ee**2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':4})

GC_1119 = Coupling(name = 'GC_1119',
                   value = '-4*CphiG*G*Lam*qeddim**2',
                   order = {'LAM':1,'QCD':1,'QED':2})

GC_1120 = Coupling(name = 'GC_1120',
                   value = '4*CphiG*G*Lam*qeddim**2',
                   order = {'LAM':1,'QCD':1,'QED':2})

GC_1121 = Coupling(name = 'GC_1121',
                   value = '-4*CphiG*complex(0,1)*G**2*Lam*qeddim**2',
                   order = {'LAM':1,'QCD':2,'QED':2})

GC_1122 = Coupling(name = 'GC_1122',
                   value = '4*CphiG*complex(0,1)*G**2*Lam*qeddim**2',
                   order = {'LAM':1,'QCD':2,'QED':2})

GC_1123 = Coupling(name = 'GC_1123',
                   value = '6*Cphi*complex(0,1)*Lam*qeddim**4',
                   order = {'LAM':1,'QED':4})

GC_1124 = Coupling(name = 'GC_1124',
                   value = '12*Cphi*complex(0,1)*Lam*qeddim**4',
                   order = {'LAM':1,'QED':4})

GC_1125 = Coupling(name = 'GC_1125',
                   value = '18*Cphi*complex(0,1)*Lam*qeddim**4',
                   order = {'LAM':1,'QED':4})

GC_1126 = Coupling(name = 'GC_1126',
                   value = '36*Cphi*complex(0,1)*Lam*qeddim**4',
                   order = {'LAM':1,'QED':4})

GC_1127 = Coupling(name = 'GC_1127',
                   value = '90*Cphi*complex(0,1)*Lam*qeddim**4',
                   order = {'LAM':1,'QED':4})

GC_1128 = Coupling(name = 'GC_1128',
                   value = 'Cll1x1x1x2*complex(0,1)*Lam*qeddim**2 + Cll1x2x1x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1129 = Coupling(name = 'GC_1129',
                   value = 'Cll1x1x1x3*complex(0,1)*Lam*qeddim**2 + Cll1x3x1x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1130 = Coupling(name = 'GC_1130',
                   value = 'Cll1x2x1x3*complex(0,1)*Lam*qeddim**2 + Cll1x3x1x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1131 = Coupling(name = 'GC_1131',
                   value = 'Cll1x1x2x1*complex(0,1)*Lam*qeddim**2 + Cll2x1x1x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1132 = Coupling(name = 'GC_1132',
                   value = 'Cll1x2x2x1*complex(0,1)*Lam*qeddim**2 + Cll2x1x1x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1133 = Coupling(name = 'GC_1133',
                   value = 'Cll1x3x2x1*complex(0,1)*Lam*qeddim**2 + Cll2x1x1x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1134 = Coupling(name = 'GC_1134',
                   value = 'Cll1x1x2x2*complex(0,1)*Lam*qeddim**2 + Cll2x2x1x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1135 = Coupling(name = 'GC_1135',
                   value = 'Cll1x2x2x2*complex(0,1)*Lam*qeddim**2 + Cll2x2x1x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1136 = Coupling(name = 'GC_1136',
                   value = 'Cll1x3x2x2*complex(0,1)*Lam*qeddim**2 + Cll2x2x1x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1137 = Coupling(name = 'GC_1137',
                   value = 'Cll2x1x2x2*complex(0,1)*Lam*qeddim**2 + Cll2x2x2x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1138 = Coupling(name = 'GC_1138',
                   value = 'Cll1x1x2x3*complex(0,1)*Lam*qeddim**2 + Cll2x3x1x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1139 = Coupling(name = 'GC_1139',
                   value = 'Cll1x2x2x3*complex(0,1)*Lam*qeddim**2 + Cll2x3x1x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1140 = Coupling(name = 'GC_1140',
                   value = 'Cll1x3x2x3*complex(0,1)*Lam*qeddim**2 + Cll2x3x1x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1141 = Coupling(name = 'GC_1141',
                   value = 'Cll2x1x2x3*complex(0,1)*Lam*qeddim**2 + Cll2x3x2x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1142 = Coupling(name = 'GC_1142',
                   value = 'Cll2x2x2x3*complex(0,1)*Lam*qeddim**2 + Cll2x3x2x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1143 = Coupling(name = 'GC_1143',
                   value = 'Cll1x1x3x1*complex(0,1)*Lam*qeddim**2 + Cll3x1x1x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1144 = Coupling(name = 'GC_1144',
                   value = 'Cll1x2x3x1*complex(0,1)*Lam*qeddim**2 + Cll3x1x1x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1145 = Coupling(name = 'GC_1145',
                   value = 'Cll1x3x3x1*complex(0,1)*Lam*qeddim**2 + Cll3x1x1x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1146 = Coupling(name = 'GC_1146',
                   value = 'Cll2x1x3x1*complex(0,1)*Lam*qeddim**2 + Cll3x1x2x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1147 = Coupling(name = 'GC_1147',
                   value = 'Cll2x2x3x1*complex(0,1)*Lam*qeddim**2 + Cll3x1x2x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1148 = Coupling(name = 'GC_1148',
                   value = 'Cll2x3x3x1*complex(0,1)*Lam*qeddim**2 + Cll3x1x2x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1149 = Coupling(name = 'GC_1149',
                   value = 'Cll1x1x3x2*complex(0,1)*Lam*qeddim**2 + Cll3x2x1x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1150 = Coupling(name = 'GC_1150',
                   value = 'Cll1x2x3x2*complex(0,1)*Lam*qeddim**2 + Cll3x2x1x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1151 = Coupling(name = 'GC_1151',
                   value = 'Cll1x3x3x2*complex(0,1)*Lam*qeddim**2 + Cll3x2x1x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1152 = Coupling(name = 'GC_1152',
                   value = 'Cll2x1x3x2*complex(0,1)*Lam*qeddim**2 + Cll3x2x2x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1153 = Coupling(name = 'GC_1153',
                   value = 'Cll2x2x3x2*complex(0,1)*Lam*qeddim**2 + Cll3x2x2x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1154 = Coupling(name = 'GC_1154',
                   value = 'Cll2x3x3x2*complex(0,1)*Lam*qeddim**2 + Cll3x2x2x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1155 = Coupling(name = 'GC_1155',
                   value = 'Cll3x1x3x2*complex(0,1)*Lam*qeddim**2 + Cll3x2x3x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1156 = Coupling(name = 'GC_1156',
                   value = 'Cll1x1x3x3*complex(0,1)*Lam*qeddim**2 + Cll3x3x1x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1157 = Coupling(name = 'GC_1157',
                   value = 'Cll1x2x3x3*complex(0,1)*Lam*qeddim**2 + Cll3x3x1x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1158 = Coupling(name = 'GC_1158',
                   value = 'Cll1x3x3x3*complex(0,1)*Lam*qeddim**2 + Cll3x3x1x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1159 = Coupling(name = 'GC_1159',
                   value = 'Cll2x1x3x3*complex(0,1)*Lam*qeddim**2 + Cll3x3x2x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1160 = Coupling(name = 'GC_1160',
                   value = 'Cll2x2x3x3*complex(0,1)*Lam*qeddim**2 + Cll3x3x2x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1161 = Coupling(name = 'GC_1161',
                   value = 'Cll2x3x3x3*complex(0,1)*Lam*qeddim**2 + Cll3x3x2x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1162 = Coupling(name = 'GC_1162',
                   value = 'Cll3x1x3x3*complex(0,1)*Lam*qeddim**2 + Cll3x3x3x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1163 = Coupling(name = 'GC_1163',
                   value = 'Cll3x2x3x3*complex(0,1)*Lam*qeddim**2 + Cll3x3x3x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1164 = Coupling(name = 'GC_1164',
                   value = 'Clq11x1x1x1*complex(0,1)*Lam*qeddim**2 - Clq31x1x1x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1165 = Coupling(name = 'GC_1165',
                   value = 'Clq11x1x1x1*complex(0,1)*Lam*qeddim**2 + Clq31x1x1x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1166 = Coupling(name = 'GC_1166',
                   value = 'Clq11x1x1x2*complex(0,1)*Lam*qeddim**2 - Clq31x1x1x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1167 = Coupling(name = 'GC_1167',
                   value = 'Clq11x1x1x2*complex(0,1)*Lam*qeddim**2 + Clq31x1x1x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1168 = Coupling(name = 'GC_1168',
                   value = 'Clq11x1x1x3*complex(0,1)*Lam*qeddim**2 - Clq31x1x1x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1169 = Coupling(name = 'GC_1169',
                   value = 'Clq11x1x1x3*complex(0,1)*Lam*qeddim**2 + Clq31x1x1x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1170 = Coupling(name = 'GC_1170',
                   value = 'Clq11x1x2x1*complex(0,1)*Lam*qeddim**2 - Clq31x1x2x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1171 = Coupling(name = 'GC_1171',
                   value = 'Clq11x1x2x1*complex(0,1)*Lam*qeddim**2 + Clq31x1x2x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1172 = Coupling(name = 'GC_1172',
                   value = 'Clq11x1x2x2*complex(0,1)*Lam*qeddim**2 - Clq31x1x2x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1173 = Coupling(name = 'GC_1173',
                   value = 'Clq11x1x2x2*complex(0,1)*Lam*qeddim**2 + Clq31x1x2x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1174 = Coupling(name = 'GC_1174',
                   value = 'Clq11x1x2x3*complex(0,1)*Lam*qeddim**2 - Clq31x1x2x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1175 = Coupling(name = 'GC_1175',
                   value = 'Clq11x1x2x3*complex(0,1)*Lam*qeddim**2 + Clq31x1x2x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1176 = Coupling(name = 'GC_1176',
                   value = 'Clq11x1x3x1*complex(0,1)*Lam*qeddim**2 - Clq31x1x3x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1177 = Coupling(name = 'GC_1177',
                   value = 'Clq11x1x3x1*complex(0,1)*Lam*qeddim**2 + Clq31x1x3x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1178 = Coupling(name = 'GC_1178',
                   value = 'Clq11x1x3x2*complex(0,1)*Lam*qeddim**2 - Clq31x1x3x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1179 = Coupling(name = 'GC_1179',
                   value = 'Clq11x1x3x2*complex(0,1)*Lam*qeddim**2 + Clq31x1x3x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1180 = Coupling(name = 'GC_1180',
                   value = 'Clq11x1x3x3*complex(0,1)*Lam*qeddim**2 - Clq31x1x3x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1181 = Coupling(name = 'GC_1181',
                   value = 'Clq11x1x3x3*complex(0,1)*Lam*qeddim**2 + Clq31x1x3x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1182 = Coupling(name = 'GC_1182',
                   value = 'Clq11x2x1x1*complex(0,1)*Lam*qeddim**2 - Clq31x2x1x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1183 = Coupling(name = 'GC_1183',
                   value = 'Clq11x2x1x1*complex(0,1)*Lam*qeddim**2 + Clq31x2x1x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1184 = Coupling(name = 'GC_1184',
                   value = 'Clq11x2x1x2*complex(0,1)*Lam*qeddim**2 - Clq31x2x1x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1185 = Coupling(name = 'GC_1185',
                   value = 'Clq11x2x1x2*complex(0,1)*Lam*qeddim**2 + Clq31x2x1x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1186 = Coupling(name = 'GC_1186',
                   value = 'Clq11x2x1x3*complex(0,1)*Lam*qeddim**2 - Clq31x2x1x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1187 = Coupling(name = 'GC_1187',
                   value = 'Clq11x2x1x3*complex(0,1)*Lam*qeddim**2 + Clq31x2x1x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1188 = Coupling(name = 'GC_1188',
                   value = 'Clq11x2x2x1*complex(0,1)*Lam*qeddim**2 - Clq31x2x2x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1189 = Coupling(name = 'GC_1189',
                   value = 'Clq11x2x2x1*complex(0,1)*Lam*qeddim**2 + Clq31x2x2x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1190 = Coupling(name = 'GC_1190',
                   value = 'Clq11x2x2x2*complex(0,1)*Lam*qeddim**2 - Clq31x2x2x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1191 = Coupling(name = 'GC_1191',
                   value = 'Clq11x2x2x2*complex(0,1)*Lam*qeddim**2 + Clq31x2x2x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1192 = Coupling(name = 'GC_1192',
                   value = 'Clq11x2x2x3*complex(0,1)*Lam*qeddim**2 - Clq31x2x2x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1193 = Coupling(name = 'GC_1193',
                   value = 'Clq11x2x2x3*complex(0,1)*Lam*qeddim**2 + Clq31x2x2x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1194 = Coupling(name = 'GC_1194',
                   value = 'Clq11x2x3x1*complex(0,1)*Lam*qeddim**2 - Clq31x2x3x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1195 = Coupling(name = 'GC_1195',
                   value = 'Clq11x2x3x1*complex(0,1)*Lam*qeddim**2 + Clq31x2x3x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1196 = Coupling(name = 'GC_1196',
                   value = 'Clq11x2x3x2*complex(0,1)*Lam*qeddim**2 - Clq31x2x3x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1197 = Coupling(name = 'GC_1197',
                   value = 'Clq11x2x3x2*complex(0,1)*Lam*qeddim**2 + Clq31x2x3x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1198 = Coupling(name = 'GC_1198',
                   value = 'Clq11x2x3x3*complex(0,1)*Lam*qeddim**2 - Clq31x2x3x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1199 = Coupling(name = 'GC_1199',
                   value = 'Clq11x2x3x3*complex(0,1)*Lam*qeddim**2 + Clq31x2x3x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1200 = Coupling(name = 'GC_1200',
                   value = 'Clq11x3x1x1*complex(0,1)*Lam*qeddim**2 - Clq31x3x1x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1201 = Coupling(name = 'GC_1201',
                   value = 'Clq11x3x1x1*complex(0,1)*Lam*qeddim**2 + Clq31x3x1x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1202 = Coupling(name = 'GC_1202',
                   value = 'Clq11x3x1x2*complex(0,1)*Lam*qeddim**2 - Clq31x3x1x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1203 = Coupling(name = 'GC_1203',
                   value = 'Clq11x3x1x2*complex(0,1)*Lam*qeddim**2 + Clq31x3x1x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1204 = Coupling(name = 'GC_1204',
                   value = 'Clq11x3x1x3*complex(0,1)*Lam*qeddim**2 - Clq31x3x1x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1205 = Coupling(name = 'GC_1205',
                   value = 'Clq11x3x1x3*complex(0,1)*Lam*qeddim**2 + Clq31x3x1x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1206 = Coupling(name = 'GC_1206',
                   value = 'Clq11x3x2x1*complex(0,1)*Lam*qeddim**2 - Clq31x3x2x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1207 = Coupling(name = 'GC_1207',
                   value = 'Clq11x3x2x1*complex(0,1)*Lam*qeddim**2 + Clq31x3x2x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1208 = Coupling(name = 'GC_1208',
                   value = 'Clq11x3x2x2*complex(0,1)*Lam*qeddim**2 - Clq31x3x2x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1209 = Coupling(name = 'GC_1209',
                   value = 'Clq11x3x2x2*complex(0,1)*Lam*qeddim**2 + Clq31x3x2x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1210 = Coupling(name = 'GC_1210',
                   value = 'Clq11x3x2x3*complex(0,1)*Lam*qeddim**2 - Clq31x3x2x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1211 = Coupling(name = 'GC_1211',
                   value = 'Clq11x3x2x3*complex(0,1)*Lam*qeddim**2 + Clq31x3x2x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1212 = Coupling(name = 'GC_1212',
                   value = 'Clq11x3x3x1*complex(0,1)*Lam*qeddim**2 - Clq31x3x3x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1213 = Coupling(name = 'GC_1213',
                   value = 'Clq11x3x3x1*complex(0,1)*Lam*qeddim**2 + Clq31x3x3x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1214 = Coupling(name = 'GC_1214',
                   value = 'Clq11x3x3x2*complex(0,1)*Lam*qeddim**2 - Clq31x3x3x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1215 = Coupling(name = 'GC_1215',
                   value = 'Clq11x3x3x2*complex(0,1)*Lam*qeddim**2 + Clq31x3x3x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1216 = Coupling(name = 'GC_1216',
                   value = 'Clq11x3x3x3*complex(0,1)*Lam*qeddim**2 - Clq31x3x3x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1217 = Coupling(name = 'GC_1217',
                   value = 'Clq11x3x3x3*complex(0,1)*Lam*qeddim**2 + Clq31x3x3x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1218 = Coupling(name = 'GC_1218',
                   value = 'Clq12x1x1x1*complex(0,1)*Lam*qeddim**2 - Clq32x1x1x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1219 = Coupling(name = 'GC_1219',
                   value = 'Clq12x1x1x1*complex(0,1)*Lam*qeddim**2 + Clq32x1x1x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1220 = Coupling(name = 'GC_1220',
                   value = 'Clq12x1x1x2*complex(0,1)*Lam*qeddim**2 - Clq32x1x1x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1221 = Coupling(name = 'GC_1221',
                   value = 'Clq12x1x1x2*complex(0,1)*Lam*qeddim**2 + Clq32x1x1x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1222 = Coupling(name = 'GC_1222',
                   value = 'Clq12x1x1x3*complex(0,1)*Lam*qeddim**2 - Clq32x1x1x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1223 = Coupling(name = 'GC_1223',
                   value = 'Clq12x1x1x3*complex(0,1)*Lam*qeddim**2 + Clq32x1x1x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1224 = Coupling(name = 'GC_1224',
                   value = 'Clq12x1x2x1*complex(0,1)*Lam*qeddim**2 - Clq32x1x2x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1225 = Coupling(name = 'GC_1225',
                   value = 'Clq12x1x2x1*complex(0,1)*Lam*qeddim**2 + Clq32x1x2x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1226 = Coupling(name = 'GC_1226',
                   value = 'Clq12x1x2x2*complex(0,1)*Lam*qeddim**2 - Clq32x1x2x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1227 = Coupling(name = 'GC_1227',
                   value = 'Clq12x1x2x2*complex(0,1)*Lam*qeddim**2 + Clq32x1x2x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1228 = Coupling(name = 'GC_1228',
                   value = 'Clq12x1x2x3*complex(0,1)*Lam*qeddim**2 - Clq32x1x2x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1229 = Coupling(name = 'GC_1229',
                   value = 'Clq12x1x2x3*complex(0,1)*Lam*qeddim**2 + Clq32x1x2x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1230 = Coupling(name = 'GC_1230',
                   value = 'Clq12x1x3x1*complex(0,1)*Lam*qeddim**2 - Clq32x1x3x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1231 = Coupling(name = 'GC_1231',
                   value = 'Clq12x1x3x1*complex(0,1)*Lam*qeddim**2 + Clq32x1x3x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1232 = Coupling(name = 'GC_1232',
                   value = 'Clq12x1x3x2*complex(0,1)*Lam*qeddim**2 - Clq32x1x3x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1233 = Coupling(name = 'GC_1233',
                   value = 'Clq12x1x3x2*complex(0,1)*Lam*qeddim**2 + Clq32x1x3x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1234 = Coupling(name = 'GC_1234',
                   value = 'Clq12x1x3x3*complex(0,1)*Lam*qeddim**2 - Clq32x1x3x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1235 = Coupling(name = 'GC_1235',
                   value = 'Clq12x1x3x3*complex(0,1)*Lam*qeddim**2 + Clq32x1x3x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1236 = Coupling(name = 'GC_1236',
                   value = 'Clq12x2x1x1*complex(0,1)*Lam*qeddim**2 - Clq32x2x1x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1237 = Coupling(name = 'GC_1237',
                   value = 'Clq12x2x1x1*complex(0,1)*Lam*qeddim**2 + Clq32x2x1x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1238 = Coupling(name = 'GC_1238',
                   value = 'Clq12x2x1x2*complex(0,1)*Lam*qeddim**2 - Clq32x2x1x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1239 = Coupling(name = 'GC_1239',
                   value = 'Clq12x2x1x2*complex(0,1)*Lam*qeddim**2 + Clq32x2x1x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1240 = Coupling(name = 'GC_1240',
                   value = 'Clq12x2x1x3*complex(0,1)*Lam*qeddim**2 - Clq32x2x1x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1241 = Coupling(name = 'GC_1241',
                   value = 'Clq12x2x1x3*complex(0,1)*Lam*qeddim**2 + Clq32x2x1x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1242 = Coupling(name = 'GC_1242',
                   value = 'Clq12x2x2x1*complex(0,1)*Lam*qeddim**2 - Clq32x2x2x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1243 = Coupling(name = 'GC_1243',
                   value = 'Clq12x2x2x1*complex(0,1)*Lam*qeddim**2 + Clq32x2x2x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1244 = Coupling(name = 'GC_1244',
                   value = 'Clq12x2x2x2*complex(0,1)*Lam*qeddim**2 - Clq32x2x2x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1245 = Coupling(name = 'GC_1245',
                   value = 'Clq12x2x2x2*complex(0,1)*Lam*qeddim**2 + Clq32x2x2x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1246 = Coupling(name = 'GC_1246',
                   value = 'Clq12x2x2x3*complex(0,1)*Lam*qeddim**2 - Clq32x2x2x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1247 = Coupling(name = 'GC_1247',
                   value = 'Clq12x2x2x3*complex(0,1)*Lam*qeddim**2 + Clq32x2x2x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1248 = Coupling(name = 'GC_1248',
                   value = 'Clq12x2x3x1*complex(0,1)*Lam*qeddim**2 - Clq32x2x3x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1249 = Coupling(name = 'GC_1249',
                   value = 'Clq12x2x3x1*complex(0,1)*Lam*qeddim**2 + Clq32x2x3x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1250 = Coupling(name = 'GC_1250',
                   value = 'Clq12x2x3x2*complex(0,1)*Lam*qeddim**2 - Clq32x2x3x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1251 = Coupling(name = 'GC_1251',
                   value = 'Clq12x2x3x2*complex(0,1)*Lam*qeddim**2 + Clq32x2x3x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1252 = Coupling(name = 'GC_1252',
                   value = 'Clq12x2x3x3*complex(0,1)*Lam*qeddim**2 - Clq32x2x3x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1253 = Coupling(name = 'GC_1253',
                   value = 'Clq12x2x3x3*complex(0,1)*Lam*qeddim**2 + Clq32x2x3x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1254 = Coupling(name = 'GC_1254',
                   value = 'Clq12x3x1x1*complex(0,1)*Lam*qeddim**2 - Clq32x3x1x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1255 = Coupling(name = 'GC_1255',
                   value = 'Clq12x3x1x1*complex(0,1)*Lam*qeddim**2 + Clq32x3x1x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1256 = Coupling(name = 'GC_1256',
                   value = 'Clq12x3x1x2*complex(0,1)*Lam*qeddim**2 - Clq32x3x1x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1257 = Coupling(name = 'GC_1257',
                   value = 'Clq12x3x1x2*complex(0,1)*Lam*qeddim**2 + Clq32x3x1x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1258 = Coupling(name = 'GC_1258',
                   value = 'Clq12x3x1x3*complex(0,1)*Lam*qeddim**2 - Clq32x3x1x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1259 = Coupling(name = 'GC_1259',
                   value = 'Clq12x3x1x3*complex(0,1)*Lam*qeddim**2 + Clq32x3x1x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1260 = Coupling(name = 'GC_1260',
                   value = 'Clq12x3x2x1*complex(0,1)*Lam*qeddim**2 - Clq32x3x2x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1261 = Coupling(name = 'GC_1261',
                   value = 'Clq12x3x2x1*complex(0,1)*Lam*qeddim**2 + Clq32x3x2x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1262 = Coupling(name = 'GC_1262',
                   value = 'Clq12x3x2x2*complex(0,1)*Lam*qeddim**2 - Clq32x3x2x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1263 = Coupling(name = 'GC_1263',
                   value = 'Clq12x3x2x2*complex(0,1)*Lam*qeddim**2 + Clq32x3x2x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1264 = Coupling(name = 'GC_1264',
                   value = 'Clq12x3x2x3*complex(0,1)*Lam*qeddim**2 - Clq32x3x2x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1265 = Coupling(name = 'GC_1265',
                   value = 'Clq12x3x2x3*complex(0,1)*Lam*qeddim**2 + Clq32x3x2x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1266 = Coupling(name = 'GC_1266',
                   value = 'Clq12x3x3x1*complex(0,1)*Lam*qeddim**2 - Clq32x3x3x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1267 = Coupling(name = 'GC_1267',
                   value = 'Clq12x3x3x1*complex(0,1)*Lam*qeddim**2 + Clq32x3x3x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1268 = Coupling(name = 'GC_1268',
                   value = 'Clq12x3x3x2*complex(0,1)*Lam*qeddim**2 - Clq32x3x3x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1269 = Coupling(name = 'GC_1269',
                   value = 'Clq12x3x3x2*complex(0,1)*Lam*qeddim**2 + Clq32x3x3x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1270 = Coupling(name = 'GC_1270',
                   value = 'Clq12x3x3x3*complex(0,1)*Lam*qeddim**2 - Clq32x3x3x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1271 = Coupling(name = 'GC_1271',
                   value = 'Clq12x3x3x3*complex(0,1)*Lam*qeddim**2 + Clq32x3x3x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1272 = Coupling(name = 'GC_1272',
                   value = 'Clq13x1x1x1*complex(0,1)*Lam*qeddim**2 - Clq33x1x1x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1273 = Coupling(name = 'GC_1273',
                   value = 'Clq13x1x1x1*complex(0,1)*Lam*qeddim**2 + Clq33x1x1x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1274 = Coupling(name = 'GC_1274',
                   value = 'Clq13x1x1x2*complex(0,1)*Lam*qeddim**2 - Clq33x1x1x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1275 = Coupling(name = 'GC_1275',
                   value = 'Clq13x1x1x2*complex(0,1)*Lam*qeddim**2 + Clq33x1x1x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1276 = Coupling(name = 'GC_1276',
                   value = 'Clq13x1x1x3*complex(0,1)*Lam*qeddim**2 - Clq33x1x1x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1277 = Coupling(name = 'GC_1277',
                   value = 'Clq13x1x1x3*complex(0,1)*Lam*qeddim**2 + Clq33x1x1x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1278 = Coupling(name = 'GC_1278',
                   value = 'Clq13x1x2x1*complex(0,1)*Lam*qeddim**2 - Clq33x1x2x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1279 = Coupling(name = 'GC_1279',
                   value = 'Clq13x1x2x1*complex(0,1)*Lam*qeddim**2 + Clq33x1x2x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1280 = Coupling(name = 'GC_1280',
                   value = 'Clq13x1x2x2*complex(0,1)*Lam*qeddim**2 - Clq33x1x2x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1281 = Coupling(name = 'GC_1281',
                   value = 'Clq13x1x2x2*complex(0,1)*Lam*qeddim**2 + Clq33x1x2x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1282 = Coupling(name = 'GC_1282',
                   value = 'Clq13x1x2x3*complex(0,1)*Lam*qeddim**2 - Clq33x1x2x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1283 = Coupling(name = 'GC_1283',
                   value = 'Clq13x1x2x3*complex(0,1)*Lam*qeddim**2 + Clq33x1x2x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1284 = Coupling(name = 'GC_1284',
                   value = 'Clq13x1x3x1*complex(0,1)*Lam*qeddim**2 - Clq33x1x3x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1285 = Coupling(name = 'GC_1285',
                   value = 'Clq13x1x3x1*complex(0,1)*Lam*qeddim**2 + Clq33x1x3x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1286 = Coupling(name = 'GC_1286',
                   value = 'Clq13x1x3x2*complex(0,1)*Lam*qeddim**2 - Clq33x1x3x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1287 = Coupling(name = 'GC_1287',
                   value = 'Clq13x1x3x2*complex(0,1)*Lam*qeddim**2 + Clq33x1x3x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1288 = Coupling(name = 'GC_1288',
                   value = 'Clq13x1x3x3*complex(0,1)*Lam*qeddim**2 - Clq33x1x3x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1289 = Coupling(name = 'GC_1289',
                   value = 'Clq13x1x3x3*complex(0,1)*Lam*qeddim**2 + Clq33x1x3x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1290 = Coupling(name = 'GC_1290',
                   value = 'Clq13x2x1x1*complex(0,1)*Lam*qeddim**2 - Clq33x2x1x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1291 = Coupling(name = 'GC_1291',
                   value = 'Clq13x2x1x1*complex(0,1)*Lam*qeddim**2 + Clq33x2x1x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1292 = Coupling(name = 'GC_1292',
                   value = 'Clq13x2x1x2*complex(0,1)*Lam*qeddim**2 - Clq33x2x1x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1293 = Coupling(name = 'GC_1293',
                   value = 'Clq13x2x1x2*complex(0,1)*Lam*qeddim**2 + Clq33x2x1x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1294 = Coupling(name = 'GC_1294',
                   value = 'Clq13x2x1x3*complex(0,1)*Lam*qeddim**2 - Clq33x2x1x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1295 = Coupling(name = 'GC_1295',
                   value = 'Clq13x2x1x3*complex(0,1)*Lam*qeddim**2 + Clq33x2x1x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1296 = Coupling(name = 'GC_1296',
                   value = 'Clq13x2x2x1*complex(0,1)*Lam*qeddim**2 - Clq33x2x2x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1297 = Coupling(name = 'GC_1297',
                   value = 'Clq13x2x2x1*complex(0,1)*Lam*qeddim**2 + Clq33x2x2x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1298 = Coupling(name = 'GC_1298',
                   value = 'Clq13x2x2x2*complex(0,1)*Lam*qeddim**2 - Clq33x2x2x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1299 = Coupling(name = 'GC_1299',
                   value = 'Clq13x2x2x2*complex(0,1)*Lam*qeddim**2 + Clq33x2x2x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1300 = Coupling(name = 'GC_1300',
                   value = 'Clq13x2x2x3*complex(0,1)*Lam*qeddim**2 - Clq33x2x2x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1301 = Coupling(name = 'GC_1301',
                   value = 'Clq13x2x2x3*complex(0,1)*Lam*qeddim**2 + Clq33x2x2x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1302 = Coupling(name = 'GC_1302',
                   value = 'Clq13x2x3x1*complex(0,1)*Lam*qeddim**2 - Clq33x2x3x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1303 = Coupling(name = 'GC_1303',
                   value = 'Clq13x2x3x1*complex(0,1)*Lam*qeddim**2 + Clq33x2x3x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1304 = Coupling(name = 'GC_1304',
                   value = 'Clq13x2x3x2*complex(0,1)*Lam*qeddim**2 - Clq33x2x3x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1305 = Coupling(name = 'GC_1305',
                   value = 'Clq13x2x3x2*complex(0,1)*Lam*qeddim**2 + Clq33x2x3x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1306 = Coupling(name = 'GC_1306',
                   value = 'Clq13x2x3x3*complex(0,1)*Lam*qeddim**2 - Clq33x2x3x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1307 = Coupling(name = 'GC_1307',
                   value = 'Clq13x2x3x3*complex(0,1)*Lam*qeddim**2 + Clq33x2x3x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1308 = Coupling(name = 'GC_1308',
                   value = 'Clq13x3x1x1*complex(0,1)*Lam*qeddim**2 - Clq33x3x1x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1309 = Coupling(name = 'GC_1309',
                   value = 'Clq13x3x1x1*complex(0,1)*Lam*qeddim**2 + Clq33x3x1x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1310 = Coupling(name = 'GC_1310',
                   value = 'Clq13x3x1x2*complex(0,1)*Lam*qeddim**2 - Clq33x3x1x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1311 = Coupling(name = 'GC_1311',
                   value = 'Clq13x3x1x2*complex(0,1)*Lam*qeddim**2 + Clq33x3x1x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1312 = Coupling(name = 'GC_1312',
                   value = 'Clq13x3x1x3*complex(0,1)*Lam*qeddim**2 - Clq33x3x1x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1313 = Coupling(name = 'GC_1313',
                   value = 'Clq13x3x1x3*complex(0,1)*Lam*qeddim**2 + Clq33x3x1x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1314 = Coupling(name = 'GC_1314',
                   value = 'Clq13x3x2x1*complex(0,1)*Lam*qeddim**2 - Clq33x3x2x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1315 = Coupling(name = 'GC_1315',
                   value = 'Clq13x3x2x1*complex(0,1)*Lam*qeddim**2 + Clq33x3x2x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1316 = Coupling(name = 'GC_1316',
                   value = 'Clq13x3x2x2*complex(0,1)*Lam*qeddim**2 - Clq33x3x2x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1317 = Coupling(name = 'GC_1317',
                   value = 'Clq13x3x2x2*complex(0,1)*Lam*qeddim**2 + Clq33x3x2x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1318 = Coupling(name = 'GC_1318',
                   value = 'Clq13x3x2x3*complex(0,1)*Lam*qeddim**2 - Clq33x3x2x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1319 = Coupling(name = 'GC_1319',
                   value = 'Clq13x3x2x3*complex(0,1)*Lam*qeddim**2 + Clq33x3x2x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1320 = Coupling(name = 'GC_1320',
                   value = 'Clq13x3x3x1*complex(0,1)*Lam*qeddim**2 - Clq33x3x3x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1321 = Coupling(name = 'GC_1321',
                   value = 'Clq13x3x3x1*complex(0,1)*Lam*qeddim**2 + Clq33x3x3x1*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1322 = Coupling(name = 'GC_1322',
                   value = 'Clq13x3x3x2*complex(0,1)*Lam*qeddim**2 - Clq33x3x3x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1323 = Coupling(name = 'GC_1323',
                   value = 'Clq13x3x3x2*complex(0,1)*Lam*qeddim**2 + Clq33x3x3x2*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1324 = Coupling(name = 'GC_1324',
                   value = 'Clq13x3x3x3*complex(0,1)*Lam*qeddim**2 - Clq33x3x3x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1325 = Coupling(name = 'GC_1325',
                   value = 'Clq13x3x3x3*complex(0,1)*Lam*qeddim**2 + Clq33x3x3x3*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1326 = Coupling(name = 'GC_1326',
                   value = '-(CphiD*complex(0,1)*Lam*qeddim**2) - 2*Cphik*complex(0,1)*Lam*qeddim**2',
                   order = {'LAM':1,'QED':2})

GC_1327 = Coupling(name = 'GC_1327',
                   value = '(-3*CphiD*ee**2*Lam*qeddim**2)/(2.*cw) - (3*CphiD*cw*ee**2*Lam*qeddim**2)/(2.*sw**2)',
                   order = {'LAM':1,'QED':4})

GC_1328 = Coupling(name = 'GC_1328',
                   value = '(CphiD*ee**2*Lam*qeddim**2)/cw - (CphiD*cw*ee**2*Lam*qeddim**2)/sw**2',
                   order = {'LAM':1,'QED':4})

GC_1329 = Coupling(name = 'GC_1329',
                   value = '-(CphiD*ee**2*Lam*qeddim**2)/(2.*cw) - (CphiD*cw*ee**2*Lam*qeddim**2)/(2.*sw**2)',
                   order = {'LAM':1,'QED':4})

GC_1330 = Coupling(name = 'GC_1330',
                   value = '-(CphiD*ee**2*complex(0,1)*Lam*qeddim**2)/(2.*cw) - (CphiD*cw*ee**2*complex(0,1)*Lam*qeddim**2)/(2.*sw**2)',
                   order = {'LAM':1,'QED':4})

GC_1331 = Coupling(name = 'GC_1331',
                   value = '-((CphiD*ee**2*complex(0,1)*Lam*qeddim**2)/cw) + (CphiD*cw*ee**2*complex(0,1)*Lam*qeddim**2)/sw**2',
                   order = {'LAM':1,'QED':4})

GC_1332 = Coupling(name = 'GC_1332',
                   value = '(-3*CphiD*ee**2*complex(0,1)*Lam*qeddim**2)/(2.*cw) - (3*CphiD*cw*ee**2*complex(0,1)*Lam*qeddim**2)/(2.*sw**2)',
                   order = {'LAM':1,'QED':4})

GC_1333 = Coupling(name = 'GC_1333',
                   value = '(CphiD*ee**2*Lam*qeddim**2)/(2.*cw) + (CphiD*cw*ee**2*Lam*qeddim**2)/(2.*sw**2)',
                   order = {'LAM':1,'QED':4})

GC_1334 = Coupling(name = 'GC_1334',
                   value = '-((CphiD*ee**2*Lam*qeddim**2)/cw) + (CphiD*cw*ee**2*Lam*qeddim**2)/sw**2',
                   order = {'LAM':1,'QED':4})

GC_1335 = Coupling(name = 'GC_1335',
                   value = '(3*CphiD*ee**2*Lam*qeddim**2)/(2.*cw) + (3*CphiD*cw*ee**2*Lam*qeddim**2)/(2.*sw**2)',
                   order = {'LAM':1,'QED':4})

GC_1336 = Coupling(name = 'GC_1336',
                   value = '-2*CphiWB*ee*complex(0,1)*Lam*qeddim**2 - (4*CphiW*cw*ee*complex(0,1)*Lam*qeddim**2)/sw',
                   order = {'LAM':1,'QED':3})

GC_1337 = Coupling(name = 'GC_1337',
                   value = '2*CphiWB*ee*complex(0,1)*Lam*qeddim**2 - (4*CphiW*cw*ee*complex(0,1)*Lam*qeddim**2)/sw',
                   order = {'LAM':1,'QED':3})

GC_1338 = Coupling(name = 'GC_1338',
                   value = '-2*CphiWB*ee*complex(0,1)*Lam*qeddim**2 + (4*CphiW*cw*ee*complex(0,1)*Lam*qeddim**2)/sw',
                   order = {'LAM':1,'QED':3})

GC_1339 = Coupling(name = 'GC_1339',
                   value = '2*CphiWB*ee*complex(0,1)*Lam*qeddim**2 + (4*CphiW*cw*ee*complex(0,1)*Lam*qeddim**2)/sw',
                   order = {'LAM':1,'QED':3})

GC_1340 = Coupling(name = 'GC_1340',
                   value = '-4*CphiW*ee*complex(0,1)*Lam*qeddim**2 - (2*CphiWB*cw*ee*complex(0,1)*Lam*qeddim**2)/sw',
                   order = {'LAM':1,'QED':3})

GC_1341 = Coupling(name = 'GC_1341',
                   value = '4*CphiW*ee*complex(0,1)*Lam*qeddim**2 - (2*CphiWB*cw*ee*complex(0,1)*Lam*qeddim**2)/sw',
                   order = {'LAM':1,'QED':3})

GC_1342 = Coupling(name = 'GC_1342',
                   value = '-4*CphiW*ee*complex(0,1)*Lam*qeddim**2 + (2*CphiWB*cw*ee*complex(0,1)*Lam*qeddim**2)/sw',
                   order = {'LAM':1,'QED':3})

GC_1343 = Coupling(name = 'GC_1343',
                   value = '4*CphiW*ee*complex(0,1)*Lam*qeddim**2 + (2*CphiWB*cw*ee*complex(0,1)*Lam*qeddim**2)/sw',
                   order = {'LAM':1,'QED':3})

GC_1344 = Coupling(name = 'GC_1344',
                   value = '(ee**2*complex(0,1))/(2.*sw**2)',
                   order = {'QED':2})

GC_1345 = Coupling(name = 'GC_1345',
                   value = '-((ee**2*complex(0,1))/sw**2)',
                   order = {'QED':2})

GC_1346 = Coupling(name = 'GC_1346',
                   value = '(2*ee**2*complex(0,1))/sw**2',
                   order = {'QED':2})

GC_1347 = Coupling(name = 'GC_1347',
                   value = '(cw**2*ee**2*complex(0,1))/sw**2',
                   order = {'QED':2})

GC_1348 = Coupling(name = 'GC_1348',
                   value = '(-2*cw**2*ee**2*complex(0,1))/sw**2',
                   order = {'QED':2})

GC_1349 = Coupling(name = 'GC_1349',
                   value = '(-2*CphiD*ee**2*Lam*qeddim**2)/sw**2',
                   order = {'LAM':1,'QED':4})

GC_1350 = Coupling(name = 'GC_1350',
                   value = '(CphiD*ee**2*complex(0,1)*Lam*qeddim**2)/sw**2',
                   order = {'LAM':1,'QED':4})

GC_1351 = Coupling(name = 'GC_1351',
                   value = '(-2*CphiD*ee**2*complex(0,1)*Lam*qeddim**2)/sw**2',
                   order = {'LAM':1,'QED':4})

GC_1352 = Coupling(name = 'GC_1352',
                   value = '(2*CphiD*ee**2*complex(0,1)*Lam*qeddim**2)/sw**2',
                   order = {'LAM':1,'QED':4})

GC_1353 = Coupling(name = 'GC_1353',
                   value = '(2*CphiD*ee**2*Lam*qeddim**2)/sw**2',
                   order = {'LAM':1,'QED':4})

GC_1354 = Coupling(name = 'GC_1354',
                   value = '(4*CphiW*ee**2*complex(0,1)*Lam*qeddim**2)/sw**2',
                   order = {'LAM':1,'QED':4})

GC_1355 = Coupling(name = 'GC_1355',
                   value = '(-8*CphiW*ee**2*complex(0,1)*Lam*qeddim**2)/sw**2',
                   order = {'LAM':1,'QED':4})

GC_1356 = Coupling(name = 'GC_1356',
                   value = '(-4*CphiW*cw**2*ee**2*complex(0,1)*Lam*qeddim**2)/sw**2',
                   order = {'LAM':1,'QED':4})

GC_1357 = Coupling(name = 'GC_1357',
                   value = '(8*CphiW*cw**2*ee**2*complex(0,1)*Lam*qeddim**2)/sw**2',
                   order = {'LAM':1,'QED':4})

GC_1358 = Coupling(name = 'GC_1358',
                   value = '-ee/(2.*sw)',
                   order = {'QED':1})

GC_1359 = Coupling(name = 'GC_1359',
                   value = '-(ee*complex(0,1))/(2.*sw)',
                   order = {'QED':1})

GC_1360 = Coupling(name = 'GC_1360',
                   value = '(ee*complex(0,1))/(2.*sw)',
                   order = {'QED':1})

GC_1361 = Coupling(name = 'GC_1361',
                   value = 'ee/(2.*sw)',
                   order = {'QED':1})

GC_1362 = Coupling(name = 'GC_1362',
                   value = '(ee*complex(0,1))/(sw*cmath.sqrt(2))',
                   order = {'QED':1})

GC_1363 = Coupling(name = 'GC_1363',
                   value = '-((cw*ee*complex(0,1))/sw)',
                   order = {'QED':1})

GC_1364 = Coupling(name = 'GC_1364',
                   value = '(cw*ee*complex(0,1))/sw',
                   order = {'QED':1})

GC_1365 = Coupling(name = 'GC_1365',
                   value = '-ee**2/(2.*sw)',
                   order = {'QED':2})

GC_1366 = Coupling(name = 'GC_1366',
                   value = '-(ee**2*complex(0,1))/(2.*sw)',
                   order = {'QED':2})

GC_1367 = Coupling(name = 'GC_1367',
                   value = 'ee**2/(2.*sw)',
                   order = {'QED':2})

GC_1368 = Coupling(name = 'GC_1368',
                   value = '-((cw*ee**2*complex(0,1))/sw)',
                   order = {'QED':2})

GC_1369 = Coupling(name = 'GC_1369',
                   value = '(2*cw*ee**2*complex(0,1))/sw',
                   order = {'QED':2})

GC_1370 = Coupling(name = 'GC_1370',
                   value = '-(CphiD*ee*Lam*qeddim**2)/(2.*sw)',
                   order = {'LAM':1,'QED':3})

GC_1371 = Coupling(name = 'GC_1371',
                   value = '-(CphiD*ee*complex(0,1)*Lam*qeddim**2)/(2.*sw)',
                   order = {'LAM':1,'QED':3})

GC_1372 = Coupling(name = 'GC_1372',
                   value = '(CphiD*ee*complex(0,1)*Lam*qeddim**2)/(2.*sw)',
                   order = {'LAM':1,'QED':3})

GC_1373 = Coupling(name = 'GC_1373',
                   value = '-((CphiD*ee*complex(0,1)*Lam*qeddim**2)/sw)',
                   order = {'LAM':1,'QED':3})

GC_1374 = Coupling(name = 'GC_1374',
                   value = '(CphiD*ee*complex(0,1)*Lam*qeddim**2)/sw',
                   order = {'LAM':1,'QED':3})

GC_1375 = Coupling(name = 'GC_1375',
                   value = '(CphiD*ee*Lam*qeddim**2)/sw',
                   order = {'LAM':1,'QED':3})

GC_1376 = Coupling(name = 'GC_1376',
                   value = '(-4*CphiW*cw*ee*complex(0,1)*Lam*qeddim**2)/sw',
                   order = {'LAM':1,'QED':3})

GC_1377 = Coupling(name = 'GC_1377',
                   value = '(4*CphiW*cw*ee*complex(0,1)*Lam*qeddim**2)/sw',
                   order = {'LAM':1,'QED':3})

GC_1378 = Coupling(name = 'GC_1378',
                   value = '(-2*CphiWB*cw**2*ee*Lam*qeddim**2)/sw',
                   order = {'LAM':1,'QED':3})

GC_1379 = Coupling(name = 'GC_1379',
                   value = '(-2*CphiWB*cw**2*ee*complex(0,1)*Lam*qeddim**2)/sw',
                   order = {'LAM':1,'QED':3})

GC_1380 = Coupling(name = 'GC_1380',
                   value = '(2*CphiWB*cw**2*ee*complex(0,1)*Lam*qeddim**2)/sw',
                   order = {'LAM':1,'QED':3})

GC_1381 = Coupling(name = 'GC_1381',
                   value = '(2*CphiWB*cw**2*ee*Lam*qeddim**2)/sw',
                   order = {'LAM':1,'QED':3})

GC_1382 = Coupling(name = 'GC_1382',
                   value = '(-2*CphiD*ee**2*Lam*qeddim**2)/sw',
                   order = {'LAM':1,'QED':4})

GC_1383 = Coupling(name = 'GC_1383',
                   value = '(-2*CphiD*ee**2*complex(0,1)*Lam*qeddim**2)/sw',
                   order = {'LAM':1,'QED':4})

GC_1384 = Coupling(name = 'GC_1384',
                   value = '(2*CphiD*ee**2*Lam*qeddim**2)/sw',
                   order = {'LAM':1,'QED':4})

GC_1385 = Coupling(name = 'GC_1385',
                   value = '(4*CphiW*cw*ee**2*complex(0,1)*Lam*qeddim**2)/sw',
                   order = {'LAM':1,'QED':4})

GC_1386 = Coupling(name = 'GC_1386',
                   value = '(-8*CphiW*cw*ee**2*complex(0,1)*Lam*qeddim**2)/sw',
                   order = {'LAM':1,'QED':4})

GC_1387 = Coupling(name = 'GC_1387',
                   value = '(ee*complex(0,1)*sw)/(3.*cw)',
                   order = {'QED':1})

GC_1388 = Coupling(name = 'GC_1388',
                   value = '(-2*ee*complex(0,1)*sw)/(3.*cw)',
                   order = {'QED':1})

GC_1389 = Coupling(name = 'GC_1389',
                   value = '(ee*complex(0,1)*sw)/cw',
                   order = {'QED':1})

GC_1390 = Coupling(name = 'GC_1390',
                   value = '-2*CphiWB*Lam*qeddim**2*sw',
                   order = {'LAM':1,'QED':2})

GC_1391 = Coupling(name = 'GC_1391',
                   value = '-2*CphiWB*complex(0,1)*Lam*qeddim**2*sw',
                   order = {'LAM':1,'QED':2})

GC_1392 = Coupling(name = 'GC_1392',
                   value = '2*CphiWB*complex(0,1)*Lam*qeddim**2*sw',
                   order = {'LAM':1,'QED':2})

GC_1393 = Coupling(name = 'GC_1393',
                   value = '2*CphiWB*Lam*qeddim**2*sw',
                   order = {'LAM':1,'QED':2})

GC_1394 = Coupling(name = 'GC_1394',
                   value = '-2*CphiWB*ee*Lam*qeddim**2*sw',
                   order = {'LAM':1,'QED':3})

GC_1395 = Coupling(name = 'GC_1395',
                   value = '-2*CphiWB*ee*complex(0,1)*Lam*qeddim**2*sw',
                   order = {'LAM':1,'QED':3})

GC_1396 = Coupling(name = 'GC_1396',
                   value = '2*CphiWB*ee*complex(0,1)*Lam*qeddim**2*sw',
                   order = {'LAM':1,'QED':3})

GC_1397 = Coupling(name = 'GC_1397',
                   value = '2*CphiWB*ee*Lam*qeddim**2*sw',
                   order = {'LAM':1,'QED':3})

GC_1398 = Coupling(name = 'GC_1398',
                   value = '-(cw*ee)/(2.*sw) - (ee*sw)/(2.*cw)',
                   order = {'QED':1})

GC_1399 = Coupling(name = 'GC_1399',
                   value = '-(cw*ee*complex(0,1))/(2.*sw) - (ee*complex(0,1)*sw)/(6.*cw)',
                   order = {'QED':1})

GC_1400 = Coupling(name = 'GC_1400',
                   value = '(cw*ee*complex(0,1))/(2.*sw) - (ee*complex(0,1)*sw)/(6.*cw)',
                   order = {'QED':1})

GC_1401 = Coupling(name = 'GC_1401',
                   value = '(cw*ee*complex(0,1))/(2.*sw) - (ee*complex(0,1)*sw)/(2.*cw)',
                   order = {'QED':1})

GC_1402 = Coupling(name = 'GC_1402',
                   value = '-(cw*ee*complex(0,1))/(2.*sw) + (ee*complex(0,1)*sw)/(2.*cw)',
                   order = {'QED':1})

GC_1403 = Coupling(name = 'GC_1403',
                   value = '(cw*ee*complex(0,1))/(2.*sw) + (ee*complex(0,1)*sw)/(2.*cw)',
                   order = {'QED':1})

GC_1404 = Coupling(name = 'GC_1404',
                   value = '(cw*ee)/(2.*sw) + (ee*sw)/(2.*cw)',
                   order = {'QED':1})

GC_1405 = Coupling(name = 'GC_1405',
                   value = '-((cw*ee**2*complex(0,1))/sw) + (ee**2*complex(0,1)*sw)/cw',
                   order = {'QED':2})

GC_1406 = Coupling(name = 'GC_1406',
                   value = '(-3*CphiD*cw*ee*Lam*qeddim**2)/(2.*sw) - (3*CphiD*ee*Lam*qeddim**2*sw)/(2.*cw)',
                   order = {'LAM':1,'QED':3})

GC_1407 = Coupling(name = 'GC_1407',
                   value = '-(CphiD*cw*ee*Lam*qeddim**2)/(2.*sw) - (CphiD*ee*Lam*qeddim**2*sw)/(2.*cw)',
                   order = {'LAM':1,'QED':3})

GC_1408 = Coupling(name = 'GC_1408',
                   value = '(CphiD*cw*ee*Lam*qeddim**2)/(2.*sw) - (CphiD*ee*Lam*qeddim**2*sw)/(2.*cw)',
                   order = {'LAM':1,'QED':3})

GC_1409 = Coupling(name = 'GC_1409',
                   value = '-(CphiD*cw*ee*complex(0,1)*Lam*qeddim**2)/(2.*sw) - (CphiD*ee*complex(0,1)*Lam*qeddim**2*sw)/(2.*cw)',
                   order = {'LAM':1,'QED':3})

GC_1410 = Coupling(name = 'GC_1410',
                   value = '(CphiD*cw*ee*complex(0,1)*Lam*qeddim**2)/(2.*sw) + (CphiD*ee*complex(0,1)*Lam*qeddim**2*sw)/(2.*cw)',
                   order = {'LAM':1,'QED':3})

GC_1411 = Coupling(name = 'GC_1411',
                   value = '(CphiD*cw*ee*complex(0,1)*Lam*qeddim**2)/sw - (CphiD*ee*complex(0,1)*Lam*qeddim**2*sw)/cw',
                   order = {'LAM':1,'QED':3})

GC_1412 = Coupling(name = 'GC_1412',
                   value = '-((CphiD*cw*ee*complex(0,1)*Lam*qeddim**2)/sw) + (CphiD*ee*complex(0,1)*Lam*qeddim**2*sw)/cw',
                   order = {'LAM':1,'QED':3})

GC_1413 = Coupling(name = 'GC_1413',
                   value = '-(CphiD*cw*ee*Lam*qeddim**2)/(2.*sw) + (CphiD*ee*Lam*qeddim**2*sw)/(2.*cw)',
                   order = {'LAM':1,'QED':3})

GC_1414 = Coupling(name = 'GC_1414',
                   value = '(CphiD*cw*ee*Lam*qeddim**2)/(2.*sw) + (CphiD*ee*Lam*qeddim**2*sw)/(2.*cw)',
                   order = {'LAM':1,'QED':3})

GC_1415 = Coupling(name = 'GC_1415',
                   value = '(3*CphiD*cw*ee*Lam*qeddim**2)/(2.*sw) + (3*CphiD*ee*Lam*qeddim**2*sw)/(2.*cw)',
                   order = {'LAM':1,'QED':3})

GC_1416 = Coupling(name = 'GC_1416',
                   value = '(CphiD*cw*ee**2*complex(0,1)*Lam*qeddim**2)/sw + (CphiD*ee**2*complex(0,1)*Lam*qeddim**2*sw)/cw',
                   order = {'LAM':1,'QED':4})

GC_1417 = Coupling(name = 'GC_1417',
                   value = '(-4*CphiD*cw*ee**2*complex(0,1)*Lam*qeddim**2)/sw + (4*CphiD*ee**2*complex(0,1)*Lam*qeddim**2*sw)/cw',
                   order = {'LAM':1,'QED':4})

GC_1418 = Coupling(name = 'GC_1418',
                   value = '-(ee**2*complex(0,1)) + (cw**2*ee**2*complex(0,1))/(2.*sw**2) + (ee**2*complex(0,1)*sw**2)/(2.*cw**2)',
                   order = {'QED':2})

GC_1419 = Coupling(name = 'GC_1419',
                   value = 'ee**2*complex(0,1) + (cw**2*ee**2*complex(0,1))/(2.*sw**2) + (ee**2*complex(0,1)*sw**2)/(2.*cw**2)',
                   order = {'QED':2})

GC_1420 = Coupling(name = 'GC_1420',
                   value = '-4*CphiW*cw**2*complex(0,1)*Lam*qeddim**2 - 4*CphiWB*cw*complex(0,1)*Lam*qeddim**2*sw - 4*CphiB*complex(0,1)*Lam*qeddim**2*sw**2',
                   order = {'LAM':1,'QED':2})

GC_1421 = Coupling(name = 'GC_1421',
                   value = '-4*CphiW*cw**2*complex(0,1)*Lam*qeddim**2 + 4*CphiWB*cw*complex(0,1)*Lam*qeddim**2*sw - 4*CphiB*complex(0,1)*Lam*qeddim**2*sw**2',
                   order = {'LAM':1,'QED':2})

GC_1422 = Coupling(name = 'GC_1422',
                   value = '4*CphiW*cw**2*complex(0,1)*Lam*qeddim**2 - 4*CphiWB*cw*complex(0,1)*Lam*qeddim**2*sw + 4*CphiB*complex(0,1)*Lam*qeddim**2*sw**2',
                   order = {'LAM':1,'QED':2})

GC_1423 = Coupling(name = 'GC_1423',
                   value = '4*CphiW*cw**2*complex(0,1)*Lam*qeddim**2 + 4*CphiWB*cw*complex(0,1)*Lam*qeddim**2*sw + 4*CphiB*complex(0,1)*Lam*qeddim**2*sw**2',
                   order = {'LAM':1,'QED':2})

GC_1424 = Coupling(name = 'GC_1424',
                   value = '-4*CphiB*cw**2*complex(0,1)*Lam*qeddim**2 - 4*CphiWB*cw*complex(0,1)*Lam*qeddim**2*sw - 4*CphiW*complex(0,1)*Lam*qeddim**2*sw**2',
                   order = {'LAM':1,'QED':2})

GC_1425 = Coupling(name = 'GC_1425',
                   value = '-4*CphiB*cw**2*complex(0,1)*Lam*qeddim**2 + 4*CphiWB*cw*complex(0,1)*Lam*qeddim**2*sw - 4*CphiW*complex(0,1)*Lam*qeddim**2*sw**2',
                   order = {'LAM':1,'QED':2})

GC_1426 = Coupling(name = 'GC_1426',
                   value = '4*CphiB*cw**2*complex(0,1)*Lam*qeddim**2 - 4*CphiWB*cw*complex(0,1)*Lam*qeddim**2*sw + 4*CphiW*complex(0,1)*Lam*qeddim**2*sw**2',
                   order = {'LAM':1,'QED':2})

GC_1427 = Coupling(name = 'GC_1427',
                   value = '4*CphiB*cw**2*complex(0,1)*Lam*qeddim**2 + 4*CphiWB*cw*complex(0,1)*Lam*qeddim**2*sw + 4*CphiW*complex(0,1)*Lam*qeddim**2*sw**2',
                   order = {'LAM':1,'QED':2})

GC_1428 = Coupling(name = 'GC_1428',
                   value = '2*CphiWB*cw**2*complex(0,1)*Lam*qeddim**2 + 4*CphiB*cw*complex(0,1)*Lam*qeddim**2*sw - 4*CphiW*cw*complex(0,1)*Lam*qeddim**2*sw - 2*CphiWB*complex(0,1)*Lam*qeddim**2*sw**2',
                   order = {'LAM':1,'QED':2})

GC_1429 = Coupling(name = 'GC_1429',
                   value = '2*CphiWB*cw**2*complex(0,1)*Lam*qeddim**2 - 4*CphiB*cw*complex(0,1)*Lam*qeddim**2*sw + 4*CphiW*cw*complex(0,1)*Lam*qeddim**2*sw - 2*CphiWB*complex(0,1)*Lam*qeddim**2*sw**2',
                   order = {'LAM':1,'QED':2})

GC_1430 = Coupling(name = 'GC_1430',
                   value = '-2*CphiWB*cw**2*complex(0,1)*Lam*qeddim**2 + 4*CphiB*cw*complex(0,1)*Lam*qeddim**2*sw - 4*CphiW*cw*complex(0,1)*Lam*qeddim**2*sw + 2*CphiWB*complex(0,1)*Lam*qeddim**2*sw**2',
                   order = {'LAM':1,'QED':2})

GC_1431 = Coupling(name = 'GC_1431',
                   value = '-2*CphiWB*cw**2*complex(0,1)*Lam*qeddim**2 - 4*CphiB*cw*complex(0,1)*Lam*qeddim**2*sw + 4*CphiW*cw*complex(0,1)*Lam*qeddim**2*sw + 2*CphiWB*complex(0,1)*Lam*qeddim**2*sw**2',
                   order = {'LAM':1,'QED':2})

GC_1432 = Coupling(name = 'GC_1432',
                   value = '-((CphiD*cw**2*ee**2*complex(0,1)*Lam*qeddim**2)/sw**2) + (CphiD*ee**2*complex(0,1)*Lam*qeddim**2*sw**2)/cw**2',
                   order = {'LAM':1,'QED':4})

GC_1433 = Coupling(name = 'GC_1433',
                   value = '2*CphiD*ee**2*complex(0,1)*Lam*qeddim**2 + (CphiD*cw**2*ee**2*complex(0,1)*Lam*qeddim**2)/sw**2 + (CphiD*ee**2*complex(0,1)*Lam*qeddim**2*sw**2)/cw**2',
                   order = {'LAM':1,'QED':4})

GC_1434 = Coupling(name = 'GC_1434',
                   value = '-4*CphiD*ee**2*complex(0,1)*Lam*qeddim**2 + (2*CphiD*cw**2*ee**2*complex(0,1)*Lam*qeddim**2)/sw**2 + (2*CphiD*ee**2*complex(0,1)*Lam*qeddim**2*sw**2)/cw**2',
                   order = {'LAM':1,'QED':4})

GC_1435 = Coupling(name = 'GC_1435',
                   value = '6*CphiD*ee**2*complex(0,1)*Lam*qeddim**2 + (3*CphiD*cw**2*ee**2*complex(0,1)*Lam*qeddim**2)/sw**2 + (3*CphiD*ee**2*complex(0,1)*Lam*qeddim**2*sw**2)/cw**2',
                   order = {'LAM':1,'QED':4})

GC_1436 = Coupling(name = 'GC_1436',
                   value = 'complex(0,1)*I1a11*Lam*v6ov',
                   order = {'LAM':1,'QED':1})

GC_1437 = Coupling(name = 'GC_1437',
                   value = 'complex(0,1)*I1a22*Lam*v6ov',
                   order = {'LAM':1,'QED':1})

GC_1438 = Coupling(name = 'GC_1438',
                   value = 'complex(0,1)*I1a33*Lam*v6ov',
                   order = {'LAM':1,'QED':1})

GC_1439 = Coupling(name = 'GC_1439',
                   value = '-(complex(0,1)*I2a11*Lam*v6ov)',
                   order = {'LAM':1,'QED':1})

GC_1440 = Coupling(name = 'GC_1440',
                   value = '-(complex(0,1)*I2a22*Lam*v6ov)',
                   order = {'LAM':1,'QED':1})

GC_1441 = Coupling(name = 'GC_1441',
                   value = '-(complex(0,1)*I2a33*Lam*v6ov)',
                   order = {'LAM':1,'QED':1})

GC_1442 = Coupling(name = 'GC_1442',
                   value = '-(complex(0,1)*I3a11*Lam*v6ov)',
                   order = {'LAM':1,'QED':1})

GC_1443 = Coupling(name = 'GC_1443',
                   value = '-(complex(0,1)*I3a22*Lam*v6ov)',
                   order = {'LAM':1,'QED':1})

GC_1444 = Coupling(name = 'GC_1444',
                   value = '-(complex(0,1)*I3a33*Lam*v6ov)',
                   order = {'LAM':1,'QED':1})

GC_1445 = Coupling(name = 'GC_1445',
                   value = 'complex(0,1)*I4a11*Lam*v6ov',
                   order = {'LAM':1,'QED':1})

GC_1446 = Coupling(name = 'GC_1446',
                   value = 'complex(0,1)*I4a22*Lam*v6ov',
                   order = {'LAM':1,'QED':1})

GC_1447 = Coupling(name = 'GC_1447',
                   value = 'complex(0,1)*I4a33*Lam*v6ov',
                   order = {'LAM':1,'QED':1})

GC_1448 = Coupling(name = 'GC_1448',
                   value = '(2*ee**2*complex(0,1)*Lam*v6ov)/sw**2',
                   order = {'LAM':1,'QED':2})

GC_1449 = Coupling(name = 'GC_1449',
                   value = '(-4*ee**2*complex(0,1)*Lam*v6ov)/sw**2',
                   order = {'LAM':1,'QED':2})

GC_1450 = Coupling(name = 'GC_1450',
                   value = '-((ee*complex(0,1)*Lam*v6ov)/(sw*cmath.sqrt(2)))',
                   order = {'LAM':1,'QED':1})

GC_1451 = Coupling(name = 'GC_1451',
                   value = '-((complex(0,1)*MH**2)/vev**2)',
                   order = {'QED':2})

GC_1452 = Coupling(name = 'GC_1452',
                   value = '(-2*complex(0,1)*MH**2)/vev**2',
                   order = {'QED':2})

GC_1453 = Coupling(name = 'GC_1453',
                   value = '(-3*complex(0,1)*MH**2)/vev**2',
                   order = {'QED':2})

GC_1454 = Coupling(name = 'GC_1454',
                   value = '-((complex(0,1)*MH**2)/vev)',
                   order = {'QED':1})

GC_1455 = Coupling(name = 'GC_1455',
                   value = '(-3*complex(0,1)*MH**2)/vev',
                   order = {'QED':1})

GC_1456 = Coupling(name = 'GC_1456',
                   value = '-(ee**2*complex(0,1)*vev)/(2.*cw)',
                   order = {'QED':1})

GC_1457 = Coupling(name = 'GC_1457',
                   value = '-(CphiD*Lam*qeddim**2*vev)/2.',
                   order = {'LAM':1,'QED':1})

GC_1458 = Coupling(name = 'GC_1458',
                   value = '-(CphiD*complex(0,1)*Lam*qeddim**2*vev)/2.',
                   order = {'LAM':1,'QED':1})

GC_1459 = Coupling(name = 'GC_1459',
                   value = '(CphiD*Lam*qeddim**2*vev)/2.',
                   order = {'LAM':1,'QED':1})

GC_1460 = Coupling(name = 'GC_1460',
                   value = '-4*CphiG*complex(0,1)*Lam*qeddim**2*vev',
                   order = {'LAM':1,'QED':1})

GC_1461 = Coupling(name = 'GC_1461',
                   value = '4*CphiG*complex(0,1)*Lam*qeddim**2*vev',
                   order = {'LAM':1,'QED':1})

GC_1462 = Coupling(name = 'GC_1462',
                   value = '-(Cphik*complex(0,1)*Lam*qeddim**2*vev)',
                   order = {'LAM':1,'QED':1})

GC_1463 = Coupling(name = 'GC_1463',
                   value = '-2*Cphik*complex(0,1)*Lam*qeddim**2*vev',
                   order = {'LAM':1,'QED':1})

GC_1464 = Coupling(name = 'GC_1464',
                   value = '-3*Cphik*complex(0,1)*Lam*qeddim**2*vev',
                   order = {'LAM':1,'QED':1})

GC_1465 = Coupling(name = 'GC_1465',
                   value = '-4*CphiW*complex(0,1)*Lam*qeddim**2*vev',
                   order = {'LAM':1,'QED':1})

GC_1466 = Coupling(name = 'GC_1466',
                   value = '4*CphiW*complex(0,1)*Lam*qeddim**2*vev',
                   order = {'LAM':1,'QED':1})

GC_1467 = Coupling(name = 'GC_1467',
                   value = '-2*CphiWB*cw*complex(0,1)*Lam*qeddim**2*vev',
                   order = {'LAM':1,'QED':1})

GC_1468 = Coupling(name = 'GC_1468',
                   value = '2*CphiWB*cw*complex(0,1)*Lam*qeddim**2*vev',
                   order = {'LAM':1,'QED':1})

GC_1469 = Coupling(name = 'GC_1469',
                   value = '-(CphiD*ee*Lam*qeddim**2*vev)',
                   order = {'LAM':1,'QED':2})

GC_1470 = Coupling(name = 'GC_1470',
                   value = '-4*CphiW*ee*complex(0,1)*Lam*qeddim**2*vev',
                   order = {'LAM':1,'QED':2})

GC_1471 = Coupling(name = 'GC_1471',
                   value = '4*CphiW*ee*complex(0,1)*Lam*qeddim**2*vev',
                   order = {'LAM':1,'QED':2})

GC_1472 = Coupling(name = 'GC_1472',
                   value = '-2*CphiWB*cw*ee*complex(0,1)*Lam*qeddim**2*vev',
                   order = {'LAM':1,'QED':2})

GC_1473 = Coupling(name = 'GC_1473',
                   value = '2*CphiWB*cw*ee*complex(0,1)*Lam*qeddim**2*vev',
                   order = {'LAM':1,'QED':2})

GC_1474 = Coupling(name = 'GC_1474',
                   value = '-4*CphiW*ee**2*complex(0,1)*Lam*qeddim**2*vev',
                   order = {'LAM':1,'QED':3})

GC_1475 = Coupling(name = 'GC_1475',
                   value = '8*CphiW*ee**2*complex(0,1)*Lam*qeddim**2*vev',
                   order = {'LAM':1,'QED':3})

GC_1476 = Coupling(name = 'GC_1476',
                   value = '-4*CphiG*G*Lam*qeddim**2*vev',
                   order = {'LAM':1,'QCD':1,'QED':1})

GC_1477 = Coupling(name = 'GC_1477',
                   value = '4*CphiG*G*Lam*qeddim**2*vev',
                   order = {'LAM':1,'QCD':1,'QED':1})

GC_1478 = Coupling(name = 'GC_1478',
                   value = '-4*CphiG*complex(0,1)*G**2*Lam*qeddim**2*vev',
                   order = {'LAM':1,'QCD':2,'QED':1})

GC_1479 = Coupling(name = 'GC_1479',
                   value = '4*CphiG*complex(0,1)*G**2*Lam*qeddim**2*vev',
                   order = {'LAM':1,'QCD':2,'QED':1})

GC_1480 = Coupling(name = 'GC_1480',
                   value = '6*Cphi*complex(0,1)*Lam*qeddim**4*vev',
                   order = {'LAM':1,'QED':3})

GC_1481 = Coupling(name = 'GC_1481',
                   value = '12*Cphi*complex(0,1)*Lam*qeddim**4*vev',
                   order = {'LAM':1,'QED':3})

GC_1482 = Coupling(name = 'GC_1482',
                   value = '18*Cphi*complex(0,1)*Lam*qeddim**4*vev',
                   order = {'LAM':1,'QED':3})

GC_1483 = Coupling(name = 'GC_1483',
                   value = '90*Cphi*complex(0,1)*Lam*qeddim**4*vev',
                   order = {'LAM':1,'QED':3})

GC_1484 = Coupling(name = 'GC_1484',
                   value = '(ee**2*complex(0,1)*vev)/(2.*sw**2)',
                   order = {'QED':1})

GC_1485 = Coupling(name = 'GC_1485',
                   value = '(-2*CphiD*ee**2*Lam*qeddim**2*vev)/sw**2',
                   order = {'LAM':1,'QED':3})

GC_1486 = Coupling(name = 'GC_1486',
                   value = '(CphiD*ee**2*complex(0,1)*Lam*qeddim**2*vev)/sw**2',
                   order = {'LAM':1,'QED':3})

GC_1487 = Coupling(name = 'GC_1487',
                   value = '(2*CphiD*ee**2*complex(0,1)*Lam*qeddim**2*vev)/sw**2',
                   order = {'LAM':1,'QED':3})

GC_1488 = Coupling(name = 'GC_1488',
                   value = '(2*CphiD*ee**2*Lam*qeddim**2*vev)/sw**2',
                   order = {'LAM':1,'QED':3})

GC_1489 = Coupling(name = 'GC_1489',
                   value = '(4*CphiW*ee**2*complex(0,1)*Lam*qeddim**2*vev)/sw**2',
                   order = {'LAM':1,'QED':3})

GC_1490 = Coupling(name = 'GC_1490',
                   value = '(-8*CphiW*ee**2*complex(0,1)*Lam*qeddim**2*vev)/sw**2',
                   order = {'LAM':1,'QED':3})

GC_1491 = Coupling(name = 'GC_1491',
                   value = '(-4*CphiW*cw**2*ee**2*complex(0,1)*Lam*qeddim**2*vev)/sw**2',
                   order = {'LAM':1,'QED':3})

GC_1492 = Coupling(name = 'GC_1492',
                   value = '(8*CphiW*cw**2*ee**2*complex(0,1)*Lam*qeddim**2*vev)/sw**2',
                   order = {'LAM':1,'QED':3})

GC_1493 = Coupling(name = 'GC_1493',
                   value = '-(ee**2*complex(0,1)*vev)/(2.*sw)',
                   order = {'QED':1})

GC_1494 = Coupling(name = 'GC_1494',
                   value = '-(CphiD*ee*Lam*qeddim**2*vev)/(2.*sw)',
                   order = {'LAM':1,'QED':2})

GC_1495 = Coupling(name = 'GC_1495',
                   value = '-(CphiD*ee*complex(0,1)*Lam*qeddim**2*vev)/(2.*sw)',
                   order = {'LAM':1,'QED':2})

GC_1496 = Coupling(name = 'GC_1496',
                   value = '(CphiD*ee*complex(0,1)*Lam*qeddim**2*vev)/(2.*sw)',
                   order = {'LAM':1,'QED':2})

GC_1497 = Coupling(name = 'GC_1497',
                   value = '-((CphiD*ee*complex(0,1)*Lam*qeddim**2*vev)/sw)',
                   order = {'LAM':1,'QED':2})

GC_1498 = Coupling(name = 'GC_1498',
                   value = '(CphiD*ee*complex(0,1)*Lam*qeddim**2*vev)/sw',
                   order = {'LAM':1,'QED':2})

GC_1499 = Coupling(name = 'GC_1499',
                   value = '(CphiD*ee*Lam*qeddim**2*vev)/sw',
                   order = {'LAM':1,'QED':2})

GC_1500 = Coupling(name = 'GC_1500',
                   value = '(-4*CphiW*cw*ee*complex(0,1)*Lam*qeddim**2*vev)/sw',
                   order = {'LAM':1,'QED':2})

GC_1501 = Coupling(name = 'GC_1501',
                   value = '(4*CphiW*cw*ee*complex(0,1)*Lam*qeddim**2*vev)/sw',
                   order = {'LAM':1,'QED':2})

GC_1502 = Coupling(name = 'GC_1502',
                   value = '(-2*CphiWB*cw**2*ee*complex(0,1)*Lam*qeddim**2*vev)/sw',
                   order = {'LAM':1,'QED':2})

GC_1503 = Coupling(name = 'GC_1503',
                   value = '(2*CphiWB*cw**2*ee*complex(0,1)*Lam*qeddim**2*vev)/sw',
                   order = {'LAM':1,'QED':2})

GC_1504 = Coupling(name = 'GC_1504',
                   value = '(-2*CphiD*ee**2*complex(0,1)*Lam*qeddim**2*vev)/sw',
                   order = {'LAM':1,'QED':3})

GC_1505 = Coupling(name = 'GC_1505',
                   value = '(4*CphiW*cw*ee**2*complex(0,1)*Lam*qeddim**2*vev)/sw',
                   order = {'LAM':1,'QED':3})

GC_1506 = Coupling(name = 'GC_1506',
                   value = '(-8*CphiW*cw*ee**2*complex(0,1)*Lam*qeddim**2*vev)/sw',
                   order = {'LAM':1,'QED':3})

GC_1507 = Coupling(name = 'GC_1507',
                   value = '-2*CphiWB*complex(0,1)*Lam*qeddim**2*sw*vev',
                   order = {'LAM':1,'QED':1})

GC_1508 = Coupling(name = 'GC_1508',
                   value = '2*CphiWB*complex(0,1)*Lam*qeddim**2*sw*vev',
                   order = {'LAM':1,'QED':1})

GC_1509 = Coupling(name = 'GC_1509',
                   value = '-2*CphiWB*ee*complex(0,1)*Lam*qeddim**2*sw*vev',
                   order = {'LAM':1,'QED':2})

GC_1510 = Coupling(name = 'GC_1510',
                   value = '2*CphiWB*ee*complex(0,1)*Lam*qeddim**2*sw*vev',
                   order = {'LAM':1,'QED':2})

GC_1511 = Coupling(name = 'GC_1511',
                   value = '(CphiD*ee**2*complex(0,1)*Lam*qeddim**2*vev**2)/sw**2',
                   order = {'LAM':1,'QED':2})

GC_1512 = Coupling(name = 'GC_1512',
                   value = '-(CphiD*complex(0,1)*Lam*qeddim**2*vev) - 2*Cphik*complex(0,1)*Lam*qeddim**2*vev',
                   order = {'LAM':1,'QED':1})

GC_1513 = Coupling(name = 'GC_1513',
                   value = '-(CphiD*ee**2*Lam*qeddim**2*vev)/(2.*cw) - (CphiD*cw*ee**2*Lam*qeddim**2*vev)/(2.*sw**2)',
                   order = {'LAM':1,'QED':3})

GC_1514 = Coupling(name = 'GC_1514',
                   value = '-(CphiD*ee**2*complex(0,1)*Lam*qeddim**2*vev)/(2.*cw) - (CphiD*cw*ee**2*complex(0,1)*Lam*qeddim**2*vev)/(2.*sw**2)',
                   order = {'LAM':1,'QED':3})

GC_1515 = Coupling(name = 'GC_1515',
                   value = '-((CphiD*ee**2*complex(0,1)*Lam*qeddim**2*vev)/cw) + (CphiD*cw*ee**2*complex(0,1)*Lam*qeddim**2*vev)/sw**2',
                   order = {'LAM':1,'QED':3})

GC_1516 = Coupling(name = 'GC_1516',
                   value = '(-3*CphiD*ee**2*complex(0,1)*Lam*qeddim**2*vev)/(2.*cw) - (3*CphiD*cw*ee**2*complex(0,1)*Lam*qeddim**2*vev)/(2.*sw**2)',
                   order = {'LAM':1,'QED':3})

GC_1517 = Coupling(name = 'GC_1517',
                   value = '(CphiD*ee**2*Lam*qeddim**2*vev)/(2.*cw) + (CphiD*cw*ee**2*Lam*qeddim**2*vev)/(2.*sw**2)',
                   order = {'LAM':1,'QED':3})

GC_1518 = Coupling(name = 'GC_1518',
                   value = '-2*CphiWB*ee*complex(0,1)*Lam*qeddim**2*vev - (4*CphiW*cw*ee*complex(0,1)*Lam*qeddim**2*vev)/sw',
                   order = {'LAM':1,'QED':2})

GC_1519 = Coupling(name = 'GC_1519',
                   value = '2*CphiWB*ee*complex(0,1)*Lam*qeddim**2*vev + (4*CphiW*cw*ee*complex(0,1)*Lam*qeddim**2*vev)/sw',
                   order = {'LAM':1,'QED':2})

GC_1520 = Coupling(name = 'GC_1520',
                   value = '4*CphiW*ee*complex(0,1)*Lam*qeddim**2*vev - (2*CphiWB*cw*ee*complex(0,1)*Lam*qeddim**2*vev)/sw',
                   order = {'LAM':1,'QED':2})

GC_1521 = Coupling(name = 'GC_1521',
                   value = '-4*CphiW*ee*complex(0,1)*Lam*qeddim**2*vev + (2*CphiWB*cw*ee*complex(0,1)*Lam*qeddim**2*vev)/sw',
                   order = {'LAM':1,'QED':2})

GC_1522 = Coupling(name = 'GC_1522',
                   value = '(-3*CphiD*cw*ee*Lam*qeddim**2*vev)/(2.*sw) - (3*CphiD*ee*Lam*qeddim**2*sw*vev)/(2.*cw)',
                   order = {'LAM':1,'QED':2})

GC_1523 = Coupling(name = 'GC_1523',
                   value = '-(CphiD*cw*ee*Lam*qeddim**2*vev)/(2.*sw) - (CphiD*ee*Lam*qeddim**2*sw*vev)/(2.*cw)',
                   order = {'LAM':1,'QED':2})

GC_1524 = Coupling(name = 'GC_1524',
                   value = '(CphiD*cw*ee*Lam*qeddim**2*vev)/(2.*sw) - (CphiD*ee*Lam*qeddim**2*sw*vev)/(2.*cw)',
                   order = {'LAM':1,'QED':2})

GC_1525 = Coupling(name = 'GC_1525',
                   value = '-(CphiD*cw*ee*complex(0,1)*Lam*qeddim**2*vev)/(2.*sw) - (CphiD*ee*complex(0,1)*Lam*qeddim**2*sw*vev)/(2.*cw)',
                   order = {'LAM':1,'QED':2})

GC_1526 = Coupling(name = 'GC_1526',
                   value = '(CphiD*cw*ee*complex(0,1)*Lam*qeddim**2*vev)/(2.*sw) + (CphiD*ee*complex(0,1)*Lam*qeddim**2*sw*vev)/(2.*cw)',
                   order = {'LAM':1,'QED':2})

GC_1527 = Coupling(name = 'GC_1527',
                   value = '(CphiD*cw*ee*Lam*qeddim**2*vev)/(2.*sw) + (CphiD*ee*Lam*qeddim**2*sw*vev)/(2.*cw)',
                   order = {'LAM':1,'QED':2})

GC_1528 = Coupling(name = 'GC_1528',
                   value = '(CphiD*cw*ee**2*complex(0,1)*Lam*qeddim**2*vev)/sw + (CphiD*ee**2*complex(0,1)*Lam*qeddim**2*sw*vev)/cw',
                   order = {'LAM':1,'QED':3})

GC_1529 = Coupling(name = 'GC_1529',
                   value = 'ee**2*complex(0,1)*vev + (cw**2*ee**2*complex(0,1)*vev)/(2.*sw**2) + (ee**2*complex(0,1)*sw**2*vev)/(2.*cw**2)',
                   order = {'QED':1})

GC_1530 = Coupling(name = 'GC_1530',
                   value = '-4*CphiW*cw**2*complex(0,1)*Lam*qeddim**2*vev - 4*CphiWB*cw*complex(0,1)*Lam*qeddim**2*sw*vev - 4*CphiB*complex(0,1)*Lam*qeddim**2*sw**2*vev',
                   order = {'LAM':1,'QED':1})

GC_1531 = Coupling(name = 'GC_1531',
                   value = '4*CphiW*cw**2*complex(0,1)*Lam*qeddim**2*vev + 4*CphiWB*cw*complex(0,1)*Lam*qeddim**2*sw*vev + 4*CphiB*complex(0,1)*Lam*qeddim**2*sw**2*vev',
                   order = {'LAM':1,'QED':1})

GC_1532 = Coupling(name = 'GC_1532',
                   value = '-4*CphiB*cw**2*complex(0,1)*Lam*qeddim**2*vev + 4*CphiWB*cw*complex(0,1)*Lam*qeddim**2*sw*vev - 4*CphiW*complex(0,1)*Lam*qeddim**2*sw**2*vev',
                   order = {'LAM':1,'QED':1})

GC_1533 = Coupling(name = 'GC_1533',
                   value = '4*CphiB*cw**2*complex(0,1)*Lam*qeddim**2*vev - 4*CphiWB*cw*complex(0,1)*Lam*qeddim**2*sw*vev + 4*CphiW*complex(0,1)*Lam*qeddim**2*sw**2*vev',
                   order = {'LAM':1,'QED':1})

GC_1534 = Coupling(name = 'GC_1534',
                   value = '2*CphiWB*cw**2*complex(0,1)*Lam*qeddim**2*vev + 4*CphiB*cw*complex(0,1)*Lam*qeddim**2*sw*vev - 4*CphiW*cw*complex(0,1)*Lam*qeddim**2*sw*vev - 2*CphiWB*complex(0,1)*Lam*qeddim**2*sw**2*vev',
                   order = {'LAM':1,'QED':1})

GC_1535 = Coupling(name = 'GC_1535',
                   value = '-2*CphiWB*cw**2*complex(0,1)*Lam*qeddim**2*vev - 4*CphiB*cw*complex(0,1)*Lam*qeddim**2*sw*vev + 4*CphiW*cw*complex(0,1)*Lam*qeddim**2*sw*vev + 2*CphiWB*complex(0,1)*Lam*qeddim**2*sw**2*vev',
                   order = {'LAM':1,'QED':1})

GC_1536 = Coupling(name = 'GC_1536',
                   value = '-((CphiD*cw**2*ee**2*complex(0,1)*Lam*qeddim**2*vev)/sw**2) + (CphiD*ee**2*complex(0,1)*Lam*qeddim**2*sw**2*vev)/cw**2',
                   order = {'LAM':1,'QED':3})

GC_1537 = Coupling(name = 'GC_1537',
                   value = '2*CphiD*ee**2*complex(0,1)*Lam*qeddim**2*vev + (CphiD*cw**2*ee**2*complex(0,1)*Lam*qeddim**2*vev)/sw**2 + (CphiD*ee**2*complex(0,1)*Lam*qeddim**2*sw**2*vev)/cw**2',
                   order = {'LAM':1,'QED':3})

GC_1538 = Coupling(name = 'GC_1538',
                   value = '6*CphiD*ee**2*complex(0,1)*Lam*qeddim**2*vev + (3*CphiD*cw**2*ee**2*complex(0,1)*Lam*qeddim**2*vev)/sw**2 + (3*CphiD*ee**2*complex(0,1)*Lam*qeddim**2*sw**2*vev)/cw**2',
                   order = {'LAM':1,'QED':3})

GC_1539 = Coupling(name = 'GC_1539',
                   value = '(CphiD*complex(0,1)*Lam*MH**2*qeddim**2)/2. - (complex(0,1)*Lam*ld6ol*MH**2)/vev**2 + 3*Cphi*complex(0,1)*Lam*qeddim**4*vev**2',
                   order = {'LAM':1,'QED':2})

GC_1540 = Coupling(name = 'GC_1540',
                   value = '(-2*complex(0,1)*Lam*ld6ol*MH**2)/vev**2 + 6*Cphi*complex(0,1)*Lam*qeddim**4*vev**2',
                   order = {'LAM':1,'QED':2})

GC_1541 = Coupling(name = 'GC_1541',
                   value = '(CphiD*complex(0,1)*Lam*MH**2*qeddim**2)/2. - 2*Cphik*complex(0,1)*Lam*MH**2*qeddim**2 - (complex(0,1)*Lam*ld6ol*MH**2)/vev**2 + 9*Cphi*complex(0,1)*Lam*qeddim**4*vev**2',
                   order = {'LAM':1,'QED':2})

GC_1542 = Coupling(name = 'GC_1542',
                   value = 'CphiD*complex(0,1)*Lam*MH**2*qeddim**2 - 2*Cphik*complex(0,1)*Lam*MH**2*qeddim**2 - (complex(0,1)*Lam*ld6ol*MH**2)/vev**2 + 9*Cphi*complex(0,1)*Lam*qeddim**4*vev**2',
                   order = {'LAM':1,'QED':2})

GC_1543 = Coupling(name = 'GC_1543',
                   value = '3*CphiD*complex(0,1)*Lam*MH**2*qeddim**2 - (3*complex(0,1)*Lam*ld6ol*MH**2)/vev**2 + 9*Cphi*complex(0,1)*Lam*qeddim**4*vev**2',
                   order = {'LAM':1,'QED':2})

GC_1544 = Coupling(name = 'GC_1544',
                   value = '3*CphiD*complex(0,1)*Lam*MH**2*qeddim**2 - 12*Cphik*complex(0,1)*Lam*MH**2*qeddim**2 - (3*complex(0,1)*Lam*ld6ol*MH**2)/vev**2 + 45*Cphi*complex(0,1)*Lam*qeddim**4*vev**2',
                   order = {'LAM':1,'QED':2})

GC_1545 = Coupling(name = 'GC_1545',
                   value = '-(CphiD*ee*Lam*qeddim**2*vev**2)/8. + (CphiD*ee*Lam*qeddim**2*vev**2)/(8.*sw**2) - (CphiD*cw**2*ee*Lam*qeddim**2*vev**2)/(8.*sw**2)',
                   order = {'LAM':1,'QED':1})

GC_1546 = Coupling(name = 'GC_1546',
                   value = '(CphiD*ee*complex(0,1)*Lam*qeddim**2*vev**2)/8. - (CphiD*ee*complex(0,1)*Lam*qeddim**2*vev**2)/(8.*sw**2) + (CphiD*cw**2*ee*complex(0,1)*Lam*qeddim**2*vev**2)/(8.*sw**2)',
                   order = {'LAM':1,'QED':1})

GC_1547 = Coupling(name = 'GC_1547',
                   value = '-(ee*complex(0,1)*Lam*v6ov) - (CphiD*cw**2*ee*complex(0,1)*Lam*qeddim**2*vev**2)/(4.*sw**2)',
                   order = {'LAM':1,'QED':1})

GC_1548 = Coupling(name = 'GC_1548',
                   value = 'ee*complex(0,1)*Lam*v6ov + (CphiD*cw**2*ee*complex(0,1)*Lam*qeddim**2*vev**2)/(4.*sw**2)',
                   order = {'LAM':1,'QED':1})

GC_1549 = Coupling(name = 'GC_1549',
                   value = '(CphiD*ee*Lam*qeddim**2*vev**2)/8. - (CphiD*ee*Lam*qeddim**2*vev**2)/(8.*sw**2) + (CphiD*cw**2*ee*Lam*qeddim**2*vev**2)/(8.*sw**2)',
                   order = {'LAM':1,'QED':1})

GC_1550 = Coupling(name = 'GC_1550',
                   value = '-((ee**2*complex(0,1)*Lam*v6ov)/sw**2) - (CphiD*ee**2*complex(0,1)*Lam*qeddim**2*vev**2)/(4.*sw**2)',
                   order = {'LAM':1,'QED':2})

GC_1551 = Coupling(name = 'GC_1551',
                   value = '-((ee**2*complex(0,1)*Lam*v6ov)/sw**2) + (CphiD*ee**2*complex(0,1)*Lam*qeddim**2*vev**2)/(2.*sw**2)',
                   order = {'LAM':1,'QED':2})

GC_1552 = Coupling(name = 'GC_1552',
                   value = '-((ee**2*complex(0,1)*Lam*v6ov)/sw**2) - (CphiD*ee**2*complex(0,1)*Lam*qeddim**2*vev**2)/(4.*sw**2) + (Cphik*ee**2*complex(0,1)*Lam*qeddim**2*vev**2)/sw**2',
                   order = {'LAM':1,'QED':2})

GC_1553 = Coupling(name = 'GC_1553',
                   value = '-((ee**2*Lam*v6ov)/sw) - (CphiD*ee**2*Lam*qeddim**2*vev**2)/(8.*sw**3) - (CphiWB*cw*ee**2*Lam*qeddim**2*vev**2)/(2.*sw**2)',
                   order = {'LAM':1,'QED':2})

GC_1554 = Coupling(name = 'GC_1554',
                   value = '(ee**2*Lam*v6ov)/sw + (CphiD*ee**2*Lam*qeddim**2*vev**2)/(8.*sw**3) + (CphiWB*cw*ee**2*Lam*qeddim**2*vev**2)/(2.*sw**2)',
                   order = {'LAM':1,'QED':2})

GC_1555 = Coupling(name = 'GC_1555',
                   value = '(ee*Lam*v6ov)/(2.*sw) + (CphiD*ee*Lam*qeddim**2*vev**2)/(8.*sw)',
                   order = {'LAM':1,'QED':1})

GC_1556 = Coupling(name = 'GC_1556',
                   value = '-(ee*Lam*v6ov)/(2.*sw) + (3*CphiD*ee*Lam*qeddim**2*vev**2)/(8.*sw)',
                   order = {'LAM':1,'QED':1})

GC_1557 = Coupling(name = 'GC_1557',
                   value = '(ee*complex(0,1)*Lam*v6ov)/(2.*sw) + (CphiD*ee*complex(0,1)*Lam*qeddim**2*vev**2)/(8.*sw) - (Cphik*ee*complex(0,1)*Lam*qeddim**2*vev**2)/(2.*sw)',
                   order = {'LAM':1,'QED':1})

GC_1558 = Coupling(name = 'GC_1558',
                   value = '-(ee*complex(0,1)*Lam*v6ov)/(2.*sw) - (CphiD*ee*complex(0,1)*Lam*qeddim**2*vev**2)/(8.*sw) + (Cphik*ee*complex(0,1)*Lam*qeddim**2*vev**2)/(2.*sw)',
                   order = {'LAM':1,'QED':1})

GC_1559 = Coupling(name = 'GC_1559',
                   value = '-(cw*ee*complex(0,1)*Lam*v6ov)/(2.*sw) - (ee*complex(0,1)*Lam*sw*v6ov)/(2.*cw) - (CphiD*ee*complex(0,1)*Lam*qeddim**2*vev**2)/(8.*cw*sw)',
                   order = {'LAM':1,'QED':1})

GC_1560 = Coupling(name = 'GC_1560',
                   value = '(cw*ee*complex(0,1)*Lam*v6ov)/(2.*sw) + (ee*complex(0,1)*Lam*sw*v6ov)/(6.*cw) - (CphiWB*ee*complex(0,1)*Lam*qeddim**2*vev**2)/3. + (CphiD*ee*complex(0,1)*Lam*qeddim**2*vev**2)/(24.*cw*sw) - (CphiD*cw*ee*complex(0,1)*Lam*qeddim**2*vev**2)/(12.*sw)',
                   order = {'LAM':1,'QED':1})

GC_1561 = Coupling(name = 'GC_1561',
                   value = '-(ee*complex(0,1)*Lam*sw*v6ov)/(3.*cw) - (CphiWB*ee*complex(0,1)*Lam*qeddim**2*vev**2)/3. - (CphiD*ee*complex(0,1)*Lam*qeddim**2*vev**2)/(12.*cw*sw) - (CphiD*cw*ee*complex(0,1)*Lam*qeddim**2*vev**2)/(12.*sw)',
                   order = {'LAM':1,'QED':1})

GC_1562 = Coupling(name = 'GC_1562',
                   value = '-(cw*ee*complex(0,1)*Lam*v6ov)/(2.*sw) + (ee*complex(0,1)*Lam*sw*v6ov)/(6.*cw) + (2*CphiWB*ee*complex(0,1)*Lam*qeddim**2*vev**2)/3. + (CphiD*ee*complex(0,1)*Lam*qeddim**2*vev**2)/(24.*cw*sw) + (CphiD*cw*ee*complex(0,1)*Lam*qeddim**2*vev**2)/(6.*sw)',
                   order = {'LAM':1,'QED':1})

GC_1563 = Coupling(name = 'GC_1563',
                   value = '(2*ee*complex(0,1)*Lam*sw*v6ov)/(3.*cw) + (2*CphiWB*ee*complex(0,1)*Lam*qeddim**2*vev**2)/3. + (CphiD*ee*complex(0,1)*Lam*qeddim**2*vev**2)/(6.*cw*sw) + (CphiD*cw*ee*complex(0,1)*Lam*qeddim**2*vev**2)/(6.*sw)',
                   order = {'LAM':1,'QED':1})

GC_1564 = Coupling(name = 'GC_1564',
                   value = '(cw*ee*complex(0,1)*Lam*v6ov)/sw - (CphiD*cw*ee*complex(0,1)*Lam*qeddim**2*vev**2)/(4.*sw)',
                   order = {'LAM':1,'QED':1})

GC_1565 = Coupling(name = 'GC_1565',
                   value = '(cw*ee*complex(0,1)*Lam*v6ov)/sw - CphiWB*ee*complex(0,1)*Lam*qeddim**2*vev**2 - (CphiD*cw*ee*complex(0,1)*Lam*qeddim**2*vev**2)/(4.*sw)',
                   order = {'LAM':1,'QED':1})

GC_1566 = Coupling(name = 'GC_1566',
                   value = '(cw*ee*complex(0,1)*Lam*v6ov)/(2.*sw) - (ee*complex(0,1)*Lam*sw*v6ov)/(2.*cw) - CphiWB*ee*complex(0,1)*Lam*qeddim**2*vev**2 - (CphiD*ee*complex(0,1)*Lam*qeddim**2*vev**2)/(8.*cw*sw) - (CphiD*cw*ee*complex(0,1)*Lam*qeddim**2*vev**2)/(4.*sw)',
                   order = {'LAM':1,'QED':1})

GC_1567 = Coupling(name = 'GC_1567',
                   value = '-((ee*complex(0,1)*Lam*sw*v6ov)/cw) - CphiWB*ee*complex(0,1)*Lam*qeddim**2*vev**2 - (CphiD*ee*complex(0,1)*Lam*qeddim**2*vev**2)/(4.*cw*sw) - (CphiD*cw*ee*complex(0,1)*Lam*qeddim**2*vev**2)/(4.*sw)',
                   order = {'LAM':1,'QED':1})

GC_1568 = Coupling(name = 'GC_1568',
                   value = '-((cw*ee*complex(0,1)*Lam*v6ov)/sw) + (CphiD*cw*ee*complex(0,1)*Lam*qeddim**2*vev**2)/(4.*sw)',
                   order = {'LAM':1,'QED':1})

GC_1569 = Coupling(name = 'GC_1569',
                   value = '-((cw*ee*complex(0,1)*Lam*v6ov)/sw) + CphiWB*ee*complex(0,1)*Lam*qeddim**2*vev**2 + (CphiD*cw*ee*complex(0,1)*Lam*qeddim**2*vev**2)/(4.*sw)',
                   order = {'LAM':1,'QED':1})

GC_1570 = Coupling(name = 'GC_1570',
                   value = '-(ee*complex(0,1)*Lam*v6ov)/3. + (CphiD*ee*complex(0,1)*Lam*qeddim**2*vev**2)/12. - (CphiD*ee*complex(0,1)*Lam*qeddim**2*vev**2)/(12.*sw**2) - (CphiWB*cw*ee*complex(0,1)*Lam*qeddim**2*vev**2)/(3.*sw)',
                   order = {'LAM':1,'QED':1})

GC_1571 = Coupling(name = 'GC_1571',
                   value = '-(ee*complex(0,1)*Lam*v6ov)/3. - (CphiD*ee*complex(0,1)*Lam*qeddim**2*vev**2)/24. + (CphiD*ee*complex(0,1)*Lam*qeddim**2*vev**2)/(24.*sw**2) - (CphiD*cw**2*ee*complex(0,1)*Lam*qeddim**2*vev**2)/(8.*sw**2) - (CphiWB*cw*ee*complex(0,1)*Lam*qeddim**2*vev**2)/(3.*sw)',
                   order = {'LAM':1,'QED':1})

GC_1572 = Coupling(name = 'GC_1572',
                   value = '(2*ee*complex(0,1)*Lam*v6ov)/3. - (CphiD*ee*complex(0,1)*Lam*qeddim**2*vev**2)/6. + (CphiD*ee*complex(0,1)*Lam*qeddim**2*vev**2)/(6.*sw**2) + (2*CphiWB*cw*ee*complex(0,1)*Lam*qeddim**2*vev**2)/(3.*sw)',
                   order = {'LAM':1,'QED':1})

GC_1573 = Coupling(name = 'GC_1573',
                   value = '(2*ee*complex(0,1)*Lam*v6ov)/3. - (CphiD*ee*complex(0,1)*Lam*qeddim**2*vev**2)/24. + (CphiD*ee*complex(0,1)*Lam*qeddim**2*vev**2)/(24.*sw**2) + (CphiD*cw**2*ee*complex(0,1)*Lam*qeddim**2*vev**2)/(8.*sw**2) + (2*CphiWB*cw*ee*complex(0,1)*Lam*qeddim**2*vev**2)/(3.*sw)',
                   order = {'LAM':1,'QED':1})

GC_1574 = Coupling(name = 'GC_1574',
                   value = '-(ee*complex(0,1)*Lam*v6ov) + (CphiD*ee*complex(0,1)*Lam*qeddim**2*vev**2)/4. - (CphiD*ee*complex(0,1)*Lam*qeddim**2*vev**2)/(4.*sw**2) - (CphiWB*cw*ee*complex(0,1)*Lam*qeddim**2*vev**2)/sw',
                   order = {'LAM':1,'QED':1})

GC_1575 = Coupling(name = 'GC_1575',
                   value = '-(ee*complex(0,1)*Lam*v6ov) + (CphiD*ee*complex(0,1)*Lam*qeddim**2*vev**2)/8. - (CphiD*ee*complex(0,1)*Lam*qeddim**2*vev**2)/(8.*sw**2) - (CphiD*cw**2*ee*complex(0,1)*Lam*qeddim**2*vev**2)/(8.*sw**2) - (CphiWB*cw*ee*complex(0,1)*Lam*qeddim**2*vev**2)/sw',
                   order = {'LAM':1,'QED':1})

GC_1576 = Coupling(name = 'GC_1576',
                   value = '-(ee*complex(0,1)*Lam*v6ov) - (CphiD*cw**2*ee*complex(0,1)*Lam*qeddim**2*vev**2)/(4.*sw**2) - (CphiWB*cw*ee*complex(0,1)*Lam*qeddim**2*vev**2)/sw',
                   order = {'LAM':1,'QED':1})

GC_1577 = Coupling(name = 'GC_1577',
                   value = 'ee*complex(0,1)*Lam*v6ov - (CphiD*ee*complex(0,1)*Lam*qeddim**2*vev**2)/8. + (CphiD*ee*complex(0,1)*Lam*qeddim**2*vev**2)/(8.*sw**2) + (CphiD*cw**2*ee*complex(0,1)*Lam*qeddim**2*vev**2)/(8.*sw**2) + (CphiWB*cw*ee*complex(0,1)*Lam*qeddim**2*vev**2)/sw',
                   order = {'LAM':1,'QED':1})

GC_1578 = Coupling(name = 'GC_1578',
                   value = 'ee*complex(0,1)*Lam*v6ov + (CphiD*cw**2*ee*complex(0,1)*Lam*qeddim**2*vev**2)/(4.*sw**2) + (CphiWB*cw*ee*complex(0,1)*Lam*qeddim**2*vev**2)/sw',
                   order = {'LAM':1,'QED':1})

GC_1579 = Coupling(name = 'GC_1579',
                   value = '(ee**2*complex(0,1)*Lam*v6ov)/sw + (CphiD*ee**2*complex(0,1)*Lam*qeddim**2*vev**2)/(8.*sw**3) + (CphiWB*cw*ee**2*complex(0,1)*Lam*qeddim**2*vev**2)/(2.*sw**2) - (Cphik*ee**2*complex(0,1)*Lam*qeddim**2*vev**2)/(2.*sw)',
                   order = {'LAM':1,'QED':2})

GC_1580 = Coupling(name = 'GC_1580',
                   value = '-((ee**2*Lam*v6ov)/cw) + (CphiD*ee**2*Lam*qeddim**2*vev**2)/(8.*cw) - (CphiD*ee**2*Lam*qeddim**2*vev**2)/(8.*cw*sw**2) + (CphiD*cw*ee**2*Lam*qeddim**2*vev**2)/(8.*sw**2) - (CphiWB*ee**2*Lam*qeddim**2*vev**2)/(2.*sw)',
                   order = {'LAM':1,'QED':2})

GC_1581 = Coupling(name = 'GC_1581',
                   value = '(ee**2*complex(0,1)*Lam*v6ov)/cw - (5*CphiD*ee**2*complex(0,1)*Lam*qeddim**2*vev**2)/(8.*cw) - (Cphik*ee**2*complex(0,1)*Lam*qeddim**2*vev**2)/(2.*cw) + (CphiD*ee**2*complex(0,1)*Lam*qeddim**2*vev**2)/(8.*cw*sw**2) - (5*CphiD*cw*ee**2*complex(0,1)*Lam*qeddim**2*vev**2)/(8.*sw**2) + (CphiWB*ee**2*complex(0,1)*Lam*qeddim**2*vev**2)/(2.*sw)',
                   order = {'LAM':1,'QED':2})

GC_1582 = Coupling(name = 'GC_1582',
                   value = '(ee**2*Lam*v6ov)/cw - (CphiD*ee**2*Lam*qeddim**2*vev**2)/(8.*cw) + (CphiD*ee**2*Lam*qeddim**2*vev**2)/(8.*cw*sw**2) - (CphiD*cw*ee**2*Lam*qeddim**2*vev**2)/(8.*sw**2) + (CphiWB*ee**2*Lam*qeddim**2*vev**2)/(2.*sw)',
                   order = {'LAM':1,'QED':2})

GC_1583 = Coupling(name = 'GC_1583',
                   value = '(2*cw*ee**2*complex(0,1)*Lam*v6ov)/sw - CphiWB*ee**2*complex(0,1)*Lam*qeddim**2*vev**2 + (CphiD*cw**3*ee**2*complex(0,1)*Lam*qeddim**2*vev**2)/(4.*sw**3) + (CphiWB*cw**2*ee**2*complex(0,1)*Lam*qeddim**2*vev**2)/sw**2 - (CphiD*cw*ee**2*complex(0,1)*Lam*qeddim**2*vev**2)/(4.*sw)',
                   order = {'LAM':1,'QED':2})

GC_1584 = Coupling(name = 'GC_1584',
                   value = '(-4*cw*ee**2*complex(0,1)*Lam*v6ov)/sw + 2*CphiWB*ee**2*complex(0,1)*Lam*qeddim**2*vev**2 - (CphiD*cw**3*ee**2*complex(0,1)*Lam*qeddim**2*vev**2)/(2.*sw**3) - (2*CphiWB*cw**2*ee**2*complex(0,1)*Lam*qeddim**2*vev**2)/sw**2 + (CphiD*cw*ee**2*complex(0,1)*Lam*qeddim**2*vev**2)/(2.*sw)',
                   order = {'LAM':1,'QED':2})

GC_1585 = Coupling(name = 'GC_1585',
                   value = '-2*ee**2*complex(0,1)*Lam*v6ov - (CphiD*cw**2*ee**2*complex(0,1)*Lam*qeddim**2*vev**2)/(2.*sw**2) - (2*CphiWB*cw*ee**2*complex(0,1)*Lam*qeddim**2*vev**2)/sw',
                   order = {'LAM':1,'QED':2})

GC_1586 = Coupling(name = 'GC_1586',
                   value = '(-2*cw**2*ee**2*complex(0,1)*Lam*v6ov)/sw**2 + (CphiD*cw**2*ee**2*complex(0,1)*Lam*qeddim**2*vev**2)/(2.*sw**2) + (2*CphiWB*cw*ee**2*complex(0,1)*Lam*qeddim**2*vev**2)/sw',
                   order = {'LAM':1,'QED':2})

GC_1587 = Coupling(name = 'GC_1587',
                   value = '-4*ee**2*complex(0,1)*Lam*v6ov + (CphiD*ee**2*complex(0,1)*Lam*qeddim**2*vev**2)/2. - (CphiD*ee**2*complex(0,1)*Lam*qeddim**2*vev**2)/(2.*sw**2) - (CphiD*cw**2*ee**2*complex(0,1)*Lam*qeddim**2*vev**2)/(2.*sw**2) - (4*CphiWB*cw*ee**2*complex(0,1)*Lam*qeddim**2*vev**2)/sw',
                   order = {'LAM':1,'QED':2})

GC_1588 = Coupling(name = 'GC_1588',
                   value = '(4*cw**2*ee**2*complex(0,1)*Lam*v6ov)/sw**2 - (CphiD*cw**2*ee**2*complex(0,1)*Lam*qeddim**2*vev**2)/sw**2 - (4*CphiWB*cw*ee**2*complex(0,1)*Lam*qeddim**2*vev**2)/sw',
                   order = {'LAM':1,'QED':2})

GC_1589 = Coupling(name = 'GC_1589',
                   value = '4*ee**2*complex(0,1)*Lam*v6ov + (CphiD*cw**2*ee**2*complex(0,1)*Lam*qeddim**2*vev**2)/sw**2 + (4*CphiWB*cw*ee**2*complex(0,1)*Lam*qeddim**2*vev**2)/sw',
                   order = {'LAM':1,'QED':2})

GC_1590 = Coupling(name = 'GC_1590',
                   value = '-(cw*ee*complex(0,1)*Lam*v6ov)/(2.*sw) + (ee*complex(0,1)*Lam*sw*v6ov)/(2.*cw) + CphiWB*ee*complex(0,1)*Lam*qeddim**2*vev**2 + (CphiD*ee*complex(0,1)*Lam*qeddim**2*vev**2)/(8.*cw*sw) - (CphiD*ee*complex(0,1)*Lam*qeddim**2*sw*vev**2)/(4.*cw)',
                   order = {'LAM':1,'QED':1})

GC_1591 = Coupling(name = 'GC_1591',
                   value = '(cw*ee*complex(0,1)*Lam*v6ov)/(2.*sw) - (ee*complex(0,1)*Lam*sw*v6ov)/(2.*cw) - CphiWB*ee*complex(0,1)*Lam*qeddim**2*vev**2 - (CphiD*ee*complex(0,1)*Lam*qeddim**2*vev**2)/(8.*cw*sw) + (CphiD*ee*complex(0,1)*Lam*qeddim**2*sw*vev**2)/(4.*cw)',
                   order = {'LAM':1,'QED':1})

GC_1592 = Coupling(name = 'GC_1592',
                   value = '(cw*ee*Lam*v6ov)/(2.*sw) + (ee*Lam*sw*v6ov)/(2.*cw) + (CphiD*ee*Lam*qeddim**2*vev**2)/(8.*cw*sw) - (CphiD*cw*ee*Lam*qeddim**2*vev**2)/(2.*sw) - (Cphik*cw*ee*Lam*qeddim**2*vev**2)/(2.*sw) - (CphiD*ee*Lam*qeddim**2*sw*vev**2)/(2.*cw) - (Cphik*ee*Lam*qeddim**2*sw*vev**2)/(2.*cw)',
                   order = {'LAM':1,'QED':1})

GC_1593 = Coupling(name = 'GC_1593',
                   value = '-(cw*ee*Lam*v6ov)/(2.*sw) - (ee*Lam*sw*v6ov)/(2.*cw) - (CphiD*ee*Lam*qeddim**2*vev**2)/(8.*cw*sw) + (Cphik*cw*ee*Lam*qeddim**2*vev**2)/(2.*sw) + (Cphik*ee*Lam*qeddim**2*sw*vev**2)/(2.*cw)',
                   order = {'LAM':1,'QED':1})

GC_1594 = Coupling(name = 'GC_1594',
                   value = '-(CphiD*cw*ee**2*complex(0,1)*Lam*qeddim**2*vev**2)/(8.*sw**3) + (CphiD*cw**3*ee**2*complex(0,1)*Lam*qeddim**2*vev**2)/(8.*sw**3) - (CphiD*ee**2*complex(0,1)*Lam*qeddim**2*vev**2)/(8.*cw*sw) + (CphiD*cw*ee**2*complex(0,1)*Lam*qeddim**2*vev**2)/(4.*sw) + (CphiD*ee**2*complex(0,1)*Lam*qeddim**2*sw*vev**2)/(8.*cw)',
                   order = {'LAM':1,'QED':2})

GC_1595 = Coupling(name = 'GC_1595',
                   value = '(2*cw*ee**2*complex(0,1)*Lam*v6ov)/sw - (2*ee**2*complex(0,1)*Lam*sw*v6ov)/cw - 3*CphiWB*ee**2*complex(0,1)*Lam*qeddim**2*vev**2 + (CphiD*cw*ee**2*complex(0,1)*Lam*qeddim**2*vev**2)/(8.*sw**3) + (CphiD*cw**3*ee**2*complex(0,1)*Lam*qeddim**2*vev**2)/(8.*sw**3) + (CphiWB*cw**2*ee**2*complex(0,1)*Lam*qeddim**2*vev**2)/sw**2 - (3*CphiD*ee**2*complex(0,1)*Lam*qeddim**2*vev**2)/(8.*cw*sw) - (CphiD*cw*ee**2*complex(0,1)*Lam*qeddim**2*vev**2)/(4.*sw) + (5*CphiD*ee**2*complex(0,1)*Lam*qeddim**2*sw*vev**2)/(8.*cw)',
                   order = {'LAM':1,'QED':2})

GC_1596 = Coupling(name = 'GC_1596',
                   value = '-2*ee**2*complex(0,1)*Lam*v6ov - (cw**2*ee**2*complex(0,1)*Lam*v6ov)/sw**2 - (ee**2*complex(0,1)*Lam*sw**2*v6ov)/cw**2 + (CphiD*ee**2*complex(0,1)*Lam*qeddim**2*vev**2)/2. - (CphiD*ee**2*complex(0,1)*Lam*qeddim**2*vev**2)/(4.*cw**2) - (CphiD*ee**2*complex(0,1)*Lam*qeddim**2*vev**2)/(4.*sw**2) + (CphiD*cw**2*ee**2*complex(0,1)*Lam*qeddim**2*vev**2)/(4.*sw**2) + (CphiD*ee**2*complex(0,1)*Lam*qeddim**2*sw**2*vev**2)/(4.*cw**2)',
                   order = {'LAM':1,'QED':2})

GC_1597 = Coupling(name = 'GC_1597',
                   value = '2*ee**2*complex(0,1)*Lam*v6ov - (cw**2*ee**2*complex(0,1)*Lam*v6ov)/sw**2 - (ee**2*complex(0,1)*Lam*sw**2*v6ov)/cw**2 - (CphiD*ee**2*complex(0,1)*Lam*qeddim**2*vev**2)/2. - (CphiD*ee**2*complex(0,1)*Lam*qeddim**2*vev**2)/(4.*cw**2) + (CphiD*ee**2*complex(0,1)*Lam*qeddim**2*vev**2)/(4.*sw**2) + (2*CphiWB*cw*ee**2*complex(0,1)*Lam*qeddim**2*vev**2)/sw - (2*CphiWB*ee**2*complex(0,1)*Lam*qeddim**2*sw*vev**2)/cw + (CphiD*ee**2*complex(0,1)*Lam*qeddim**2*sw**2*vev**2)/(2.*cw**2)',
                   order = {'LAM':1,'QED':2})

GC_1598 = Coupling(name = 'GC_1598',
                   value = '-2*ee**2*complex(0,1)*Lam*v6ov - (cw**2*ee**2*complex(0,1)*Lam*v6ov)/sw**2 - (ee**2*complex(0,1)*Lam*sw**2*v6ov)/cw**2 + (5*CphiD*ee**2*complex(0,1)*Lam*qeddim**2*vev**2)/2. + 2*Cphik*ee**2*complex(0,1)*Lam*qeddim**2*vev**2 - (CphiD*ee**2*complex(0,1)*Lam*qeddim**2*vev**2)/(4.*cw**2) - (CphiD*ee**2*complex(0,1)*Lam*qeddim**2*vev**2)/(4.*sw**2) + (5*CphiD*cw**2*ee**2*complex(0,1)*Lam*qeddim**2*vev**2)/(4.*sw**2) + (Cphik*cw**2*ee**2*complex(0,1)*Lam*qeddim**2*vev**2)/sw**2 + (5*CphiD*ee**2*complex(0,1)*Lam*qeddim**2*sw**2*vev**2)/(4.*cw**2) + (Cphik*ee**2*complex(0,1)*Lam*qeddim**2*sw**2*vev**2)/cw**2',
                   order = {'LAM':1,'QED':2})

GC_1599 = Coupling(name = 'GC_1599',
                   value = '-((complex(0,1)*Lam*ld6ol*MH**2)/vev) - (complex(0,1)*Lam*MH**2*v6ov)/vev + (CphiD*complex(0,1)*Lam*MH**2*qeddim**2*vev)/4. - Cphik*complex(0,1)*Lam*MH**2*qeddim**2*vev + 3*Cphi*complex(0,1)*Lam*qeddim**4*vev**3',
                   order = {'LAM':1,'QED':1})

GC_1600 = Coupling(name = 'GC_1600',
                   value = '-((complex(0,1)*Lam*ld6ol*MH**2)/vev) - (complex(0,1)*Lam*MH**2*v6ov)/vev + (3*CphiD*complex(0,1)*Lam*MH**2*qeddim**2*vev)/4. - Cphik*complex(0,1)*Lam*MH**2*qeddim**2*vev + 3*Cphi*complex(0,1)*Lam*qeddim**4*vev**3',
                   order = {'LAM':1,'QED':1})

GC_1601 = Coupling(name = 'GC_1601',
                   value = '(-3*complex(0,1)*Lam*ld6ol*MH**2)/vev - (3*complex(0,1)*Lam*MH**2*v6ov)/vev + (9*CphiD*complex(0,1)*Lam*MH**2*qeddim**2*vev)/4. - 9*Cphik*complex(0,1)*Lam*MH**2*qeddim**2*vev + 15*Cphi*complex(0,1)*Lam*qeddim**4*vev**3',
                   order = {'LAM':1,'QED':1})

GC_1602 = Coupling(name = 'GC_1602',
                   value = '-(ee**2*complex(0,1)*Lam*v6ov*vev)/(2.*sw**2) - (CphiD*ee**2*complex(0,1)*Lam*qeddim**2*vev**3)/(8.*sw**2) + (Cphik*ee**2*complex(0,1)*Lam*qeddim**2*vev**3)/(2.*sw**2)',
                   order = {'LAM':1,'QED':1})

GC_1603 = Coupling(name = 'GC_1603',
                   value = '(ee**2*complex(0,1)*Lam*v6ov*vev)/(2.*sw) + (CphiD*ee**2*complex(0,1)*Lam*qeddim**2*vev**3)/(8.*sw**3) + (CphiWB*cw*ee**2*complex(0,1)*Lam*qeddim**2*vev**3)/(2.*sw**2) - (CphiD*ee**2*complex(0,1)*Lam*qeddim**2*vev**3)/(8.*sw)',
                   order = {'LAM':1,'QED':1})

GC_1604 = Coupling(name = 'GC_1604',
                   value = '(ee**2*complex(0,1)*Lam*v6ov*vev)/(2.*cw) - (CphiD*ee**2*complex(0,1)*Lam*qeddim**2*vev**3)/(4.*cw) + (CphiD*ee**2*complex(0,1)*Lam*qeddim**2*vev**3)/(8.*cw*sw**2) - (CphiD*cw*ee**2*complex(0,1)*Lam*qeddim**2*vev**3)/(8.*sw**2) + (CphiWB*ee**2*complex(0,1)*Lam*qeddim**2*vev**3)/(2.*sw)',
                   order = {'LAM':1,'QED':1})

GC_1605 = Coupling(name = 'GC_1605',
                   value = '-(CphiD*cw*ee**2*complex(0,1)*Lam*qeddim**2*vev**3)/(8.*sw**3) + (CphiD*cw**3*ee**2*complex(0,1)*Lam*qeddim**2*vev**3)/(8.*sw**3) - (CphiD*ee**2*complex(0,1)*Lam*qeddim**2*vev**3)/(8.*cw*sw) + (CphiD*cw*ee**2*complex(0,1)*Lam*qeddim**2*vev**3)/(4.*sw) + (CphiD*ee**2*complex(0,1)*Lam*qeddim**2*sw*vev**3)/(8.*cw)',
                   order = {'LAM':1,'QED':1})

GC_1606 = Coupling(name = 'GC_1606',
                   value = '-(ee**2*complex(0,1)*Lam*v6ov*vev) - (cw**2*ee**2*complex(0,1)*Lam*v6ov*vev)/(2.*sw**2) - (ee**2*complex(0,1)*Lam*sw**2*v6ov*vev)/(2.*cw**2) + (3*CphiD*ee**2*complex(0,1)*Lam*qeddim**2*vev**3)/4. + Cphik*ee**2*complex(0,1)*Lam*qeddim**2*vev**3 - (CphiD*ee**2*complex(0,1)*Lam*qeddim**2*vev**3)/(4.*cw**2) - (CphiD*ee**2*complex(0,1)*Lam*qeddim**2*vev**3)/(4.*sw**2) + (3*CphiD*cw**2*ee**2*complex(0,1)*Lam*qeddim**2*vev**3)/(8.*sw**2) + (Cphik*cw**2*ee**2*complex(0,1)*Lam*qeddim**2*vev**3)/(2.*sw**2) + (3*CphiD*ee**2*complex(0,1)*Lam*qeddim**2*sw**2*vev**3)/(8.*cw**2) + (Cphik*ee**2*complex(0,1)*Lam*qeddim**2*sw**2*vev**3)/(2.*cw**2)',
                   order = {'LAM':1,'QED':1})

GC_1607 = Coupling(name = 'GC_1607',
                   value = '-(ymb/vev)',
                   order = {'QED':1})

GC_1608 = Coupling(name = 'GC_1608',
                   value = '-((complex(0,1)*ymb)/vev)',
                   order = {'QED':1})

GC_1609 = Coupling(name = 'GC_1609',
                   value = 'ymb/vev',
                   order = {'QED':1})

GC_1610 = Coupling(name = 'GC_1610',
                   value = '-((Lam*v6ov*ymb)/vev) - (CphiD*Lam*qeddim**2*vev*ymb)/4.',
                   order = {'LAM':1,'QED':1})

GC_1611 = Coupling(name = 'GC_1611',
                   value = '(Lam*v6ov*ymb)/vev + (CphiD*Lam*qeddim**2*vev*ymb)/4.',
                   order = {'LAM':1,'QED':1})

GC_1612 = Coupling(name = 'GC_1612',
                   value = '(complex(0,1)*Lam*v6ov*ymb)/vev + (CphiD*complex(0,1)*Lam*qeddim**2*vev*ymb)/4. - Cphik*complex(0,1)*Lam*qeddim**2*vev*ymb',
                   order = {'LAM':1,'QED':1})

GC_1613 = Coupling(name = 'GC_1613',
                   value = '-(ymc/vev)',
                   order = {'QED':1})

GC_1614 = Coupling(name = 'GC_1614',
                   value = '-((complex(0,1)*ymc)/vev)',
                   order = {'QED':1})

GC_1615 = Coupling(name = 'GC_1615',
                   value = 'ymc/vev',
                   order = {'QED':1})

GC_1616 = Coupling(name = 'GC_1616',
                   value = '-((Lam*v6ov*ymc)/vev) - (CphiD*Lam*qeddim**2*vev*ymc)/4.',
                   order = {'LAM':1,'QED':1})

GC_1617 = Coupling(name = 'GC_1617',
                   value = '(Lam*v6ov*ymc)/vev + (CphiD*Lam*qeddim**2*vev*ymc)/4.',
                   order = {'LAM':1,'QED':1})

GC_1618 = Coupling(name = 'GC_1618',
                   value = '(complex(0,1)*Lam*v6ov*ymc)/vev + (CphiD*complex(0,1)*Lam*qeddim**2*vev*ymc)/4. - Cphik*complex(0,1)*Lam*qeddim**2*vev*ymc',
                   order = {'LAM':1,'QED':1})

GC_1619 = Coupling(name = 'GC_1619',
                   value = '-(ymdo/vev)',
                   order = {'QED':1})

GC_1620 = Coupling(name = 'GC_1620',
                   value = '-((complex(0,1)*ymdo)/vev)',
                   order = {'QED':1})

GC_1621 = Coupling(name = 'GC_1621',
                   value = 'ymdo/vev',
                   order = {'QED':1})

GC_1622 = Coupling(name = 'GC_1622',
                   value = '-((Lam*v6ov*ymdo)/vev) - (CphiD*Lam*qeddim**2*vev*ymdo)/4.',
                   order = {'LAM':1,'QED':1})

GC_1623 = Coupling(name = 'GC_1623',
                   value = '(Lam*v6ov*ymdo)/vev + (CphiD*Lam*qeddim**2*vev*ymdo)/4.',
                   order = {'LAM':1,'QED':1})

GC_1624 = Coupling(name = 'GC_1624',
                   value = '(complex(0,1)*Lam*v6ov*ymdo)/vev + (CphiD*complex(0,1)*Lam*qeddim**2*vev*ymdo)/4. - Cphik*complex(0,1)*Lam*qeddim**2*vev*ymdo',
                   order = {'LAM':1,'QED':1})

GC_1625 = Coupling(name = 'GC_1625',
                   value = '-(yme/vev)',
                   order = {'QED':1})

GC_1626 = Coupling(name = 'GC_1626',
                   value = '-((complex(0,1)*yme)/vev)',
                   order = {'QED':1})

GC_1627 = Coupling(name = 'GC_1627',
                   value = 'yme/vev',
                   order = {'QED':1})

GC_1628 = Coupling(name = 'GC_1628',
                   value = '-((complex(0,1)*yme*cmath.sqrt(2))/vev)',
                   order = {'QED':1})

GC_1629 = Coupling(name = 'GC_1629',
                   value = '(complex(0,1)*Lam*v6ov*yme*cmath.sqrt(2))/vev',
                   order = {'LAM':1,'QED':1})

GC_1630 = Coupling(name = 'GC_1630',
                   value = '-((Lam*v6ov*yme)/vev) - (CphiD*Lam*qeddim**2*vev*yme)/4.',
                   order = {'LAM':1,'QED':1})

GC_1631 = Coupling(name = 'GC_1631',
                   value = '(Lam*v6ov*yme)/vev + (CphiD*Lam*qeddim**2*vev*yme)/4.',
                   order = {'LAM':1,'QED':1})

GC_1632 = Coupling(name = 'GC_1632',
                   value = '(complex(0,1)*Lam*v6ov*yme)/vev + (CphiD*complex(0,1)*Lam*qeddim**2*vev*yme)/4. - Cphik*complex(0,1)*Lam*qeddim**2*vev*yme',
                   order = {'LAM':1,'QED':1})

GC_1633 = Coupling(name = 'GC_1633',
                   value = '-(ymm/vev)',
                   order = {'QED':1})

GC_1634 = Coupling(name = 'GC_1634',
                   value = '-((complex(0,1)*ymm)/vev)',
                   order = {'QED':1})

GC_1635 = Coupling(name = 'GC_1635',
                   value = 'ymm/vev',
                   order = {'QED':1})

GC_1636 = Coupling(name = 'GC_1636',
                   value = '-((complex(0,1)*ymm*cmath.sqrt(2))/vev)',
                   order = {'QED':1})

GC_1637 = Coupling(name = 'GC_1637',
                   value = '(complex(0,1)*Lam*v6ov*ymm*cmath.sqrt(2))/vev',
                   order = {'LAM':1,'QED':1})

GC_1638 = Coupling(name = 'GC_1638',
                   value = '-((Lam*v6ov*ymm)/vev) - (CphiD*Lam*qeddim**2*vev*ymm)/4.',
                   order = {'LAM':1,'QED':1})

GC_1639 = Coupling(name = 'GC_1639',
                   value = '(Lam*v6ov*ymm)/vev + (CphiD*Lam*qeddim**2*vev*ymm)/4.',
                   order = {'LAM':1,'QED':1})

GC_1640 = Coupling(name = 'GC_1640',
                   value = '(complex(0,1)*Lam*v6ov*ymm)/vev + (CphiD*complex(0,1)*Lam*qeddim**2*vev*ymm)/4. - Cphik*complex(0,1)*Lam*qeddim**2*vev*ymm',
                   order = {'LAM':1,'QED':1})

GC_1641 = Coupling(name = 'GC_1641',
                   value = '-(yms/vev)',
                   order = {'QED':1})

GC_1642 = Coupling(name = 'GC_1642',
                   value = '-((complex(0,1)*yms)/vev)',
                   order = {'QED':1})

GC_1643 = Coupling(name = 'GC_1643',
                   value = 'yms/vev',
                   order = {'QED':1})

GC_1644 = Coupling(name = 'GC_1644',
                   value = '-((Lam*v6ov*yms)/vev) - (CphiD*Lam*qeddim**2*vev*yms)/4.',
                   order = {'LAM':1,'QED':1})

GC_1645 = Coupling(name = 'GC_1645',
                   value = '(Lam*v6ov*yms)/vev + (CphiD*Lam*qeddim**2*vev*yms)/4.',
                   order = {'LAM':1,'QED':1})

GC_1646 = Coupling(name = 'GC_1646',
                   value = '(complex(0,1)*Lam*v6ov*yms)/vev + (CphiD*complex(0,1)*Lam*qeddim**2*vev*yms)/4. - Cphik*complex(0,1)*Lam*qeddim**2*vev*yms',
                   order = {'LAM':1,'QED':1})

GC_1647 = Coupling(name = 'GC_1647',
                   value = '-(ymt/vev)',
                   order = {'QED':1})

GC_1648 = Coupling(name = 'GC_1648',
                   value = '-((complex(0,1)*ymt)/vev)',
                   order = {'QED':1})

GC_1649 = Coupling(name = 'GC_1649',
                   value = 'ymt/vev',
                   order = {'QED':1})

GC_1650 = Coupling(name = 'GC_1650',
                   value = '-((Lam*v6ov*ymt)/vev) - (CphiD*Lam*qeddim**2*vev*ymt)/4.',
                   order = {'LAM':1,'QED':1})

GC_1651 = Coupling(name = 'GC_1651',
                   value = '(Lam*v6ov*ymt)/vev + (CphiD*Lam*qeddim**2*vev*ymt)/4.',
                   order = {'LAM':1,'QED':1})

GC_1652 = Coupling(name = 'GC_1652',
                   value = '(complex(0,1)*Lam*v6ov*ymt)/vev + (CphiD*complex(0,1)*Lam*qeddim**2*vev*ymt)/4. - Cphik*complex(0,1)*Lam*qeddim**2*vev*ymt',
                   order = {'LAM':1,'QED':1})

GC_1653 = Coupling(name = 'GC_1653',
                   value = '-(ymtau/vev)',
                   order = {'QED':1})

GC_1654 = Coupling(name = 'GC_1654',
                   value = '-((complex(0,1)*ymtau)/vev)',
                   order = {'QED':1})

GC_1655 = Coupling(name = 'GC_1655',
                   value = 'ymtau/vev',
                   order = {'QED':1})

GC_1656 = Coupling(name = 'GC_1656',
                   value = '-((complex(0,1)*ymtau*cmath.sqrt(2))/vev)',
                   order = {'QED':1})

GC_1657 = Coupling(name = 'GC_1657',
                   value = '(complex(0,1)*Lam*v6ov*ymtau*cmath.sqrt(2))/vev',
                   order = {'LAM':1,'QED':1})

GC_1658 = Coupling(name = 'GC_1658',
                   value = '-((Lam*v6ov*ymtau)/vev) - (CphiD*Lam*qeddim**2*vev*ymtau)/4.',
                   order = {'LAM':1,'QED':1})

GC_1659 = Coupling(name = 'GC_1659',
                   value = '(Lam*v6ov*ymtau)/vev + (CphiD*Lam*qeddim**2*vev*ymtau)/4.',
                   order = {'LAM':1,'QED':1})

GC_1660 = Coupling(name = 'GC_1660',
                   value = '(complex(0,1)*Lam*v6ov*ymtau)/vev + (CphiD*complex(0,1)*Lam*qeddim**2*vev*ymtau)/4. - Cphik*complex(0,1)*Lam*qeddim**2*vev*ymtau',
                   order = {'LAM':1,'QED':1})

GC_1661 = Coupling(name = 'GC_1661',
                   value = '-(ymup/vev)',
                   order = {'QED':1})

GC_1662 = Coupling(name = 'GC_1662',
                   value = '-((complex(0,1)*ymup)/vev)',
                   order = {'QED':1})

GC_1663 = Coupling(name = 'GC_1663',
                   value = 'ymup/vev',
                   order = {'QED':1})

GC_1664 = Coupling(name = 'GC_1664',
                   value = '-((Lam*v6ov*ymup)/vev) - (CphiD*Lam*qeddim**2*vev*ymup)/4.',
                   order = {'LAM':1,'QED':1})

GC_1665 = Coupling(name = 'GC_1665',
                   value = '(Lam*v6ov*ymup)/vev + (CphiD*Lam*qeddim**2*vev*ymup)/4.',
                   order = {'LAM':1,'QED':1})

GC_1666 = Coupling(name = 'GC_1666',
                   value = '(complex(0,1)*Lam*v6ov*ymup)/vev + (CphiD*complex(0,1)*Lam*qeddim**2*vev*ymup)/4. - Cphik*complex(0,1)*Lam*qeddim**2*vev*ymup',
                   order = {'LAM':1,'QED':1})

GC_1667 = Coupling(name = 'GC_1667',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq1x1x1x1)',
                   order = {'LAM':1,'QED':2})

GC_1668 = Coupling(name = 'GC_1668',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq1x1x1x2)',
                   order = {'LAM':1,'QED':2})

GC_1669 = Coupling(name = 'GC_1669',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq1x1x1x3)',
                   order = {'LAM':1,'QED':2})

GC_1670 = Coupling(name = 'GC_1670',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq1x1x2x1)',
                   order = {'LAM':1,'QED':2})

GC_1671 = Coupling(name = 'GC_1671',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq1x1x2x2)',
                   order = {'LAM':1,'QED':2})

GC_1672 = Coupling(name = 'GC_1672',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq1x1x2x3)',
                   order = {'LAM':1,'QED':2})

GC_1673 = Coupling(name = 'GC_1673',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq1x1x3x1)',
                   order = {'LAM':1,'QED':2})

GC_1674 = Coupling(name = 'GC_1674',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq1x1x3x2)',
                   order = {'LAM':1,'QED':2})

GC_1675 = Coupling(name = 'GC_1675',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq1x1x3x3)',
                   order = {'LAM':1,'QED':2})

GC_1676 = Coupling(name = 'GC_1676',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq1x2x1x1)',
                   order = {'LAM':1,'QED':2})

GC_1677 = Coupling(name = 'GC_1677',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq1x2x1x2)',
                   order = {'LAM':1,'QED':2})

GC_1678 = Coupling(name = 'GC_1678',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq1x2x1x3)',
                   order = {'LAM':1,'QED':2})

GC_1679 = Coupling(name = 'GC_1679',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq1x2x2x1)',
                   order = {'LAM':1,'QED':2})

GC_1680 = Coupling(name = 'GC_1680',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq1x2x2x2)',
                   order = {'LAM':1,'QED':2})

GC_1681 = Coupling(name = 'GC_1681',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq1x2x2x3)',
                   order = {'LAM':1,'QED':2})

GC_1682 = Coupling(name = 'GC_1682',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq1x2x3x1)',
                   order = {'LAM':1,'QED':2})

GC_1683 = Coupling(name = 'GC_1683',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq1x2x3x2)',
                   order = {'LAM':1,'QED':2})

GC_1684 = Coupling(name = 'GC_1684',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq1x2x3x3)',
                   order = {'LAM':1,'QED':2})

GC_1685 = Coupling(name = 'GC_1685',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq1x3x1x1)',
                   order = {'LAM':1,'QED':2})

GC_1686 = Coupling(name = 'GC_1686',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq1x3x1x2)',
                   order = {'LAM':1,'QED':2})

GC_1687 = Coupling(name = 'GC_1687',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq1x3x1x3)',
                   order = {'LAM':1,'QED':2})

GC_1688 = Coupling(name = 'GC_1688',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq1x3x2x1)',
                   order = {'LAM':1,'QED':2})

GC_1689 = Coupling(name = 'GC_1689',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq1x3x2x2)',
                   order = {'LAM':1,'QED':2})

GC_1690 = Coupling(name = 'GC_1690',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq1x3x2x3)',
                   order = {'LAM':1,'QED':2})

GC_1691 = Coupling(name = 'GC_1691',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq1x3x3x1)',
                   order = {'LAM':1,'QED':2})

GC_1692 = Coupling(name = 'GC_1692',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq1x3x3x2)',
                   order = {'LAM':1,'QED':2})

GC_1693 = Coupling(name = 'GC_1693',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq1x3x3x3)',
                   order = {'LAM':1,'QED':2})

GC_1694 = Coupling(name = 'GC_1694',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq2x1x1x1)',
                   order = {'LAM':1,'QED':2})

GC_1695 = Coupling(name = 'GC_1695',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq2x1x1x2)',
                   order = {'LAM':1,'QED':2})

GC_1696 = Coupling(name = 'GC_1696',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq2x1x1x3)',
                   order = {'LAM':1,'QED':2})

GC_1697 = Coupling(name = 'GC_1697',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq2x1x2x1)',
                   order = {'LAM':1,'QED':2})

GC_1698 = Coupling(name = 'GC_1698',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq2x1x2x2)',
                   order = {'LAM':1,'QED':2})

GC_1699 = Coupling(name = 'GC_1699',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq2x1x2x3)',
                   order = {'LAM':1,'QED':2})

GC_1700 = Coupling(name = 'GC_1700',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq2x1x3x1)',
                   order = {'LAM':1,'QED':2})

GC_1701 = Coupling(name = 'GC_1701',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq2x1x3x2)',
                   order = {'LAM':1,'QED':2})

GC_1702 = Coupling(name = 'GC_1702',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq2x1x3x3)',
                   order = {'LAM':1,'QED':2})

GC_1703 = Coupling(name = 'GC_1703',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq2x2x1x1)',
                   order = {'LAM':1,'QED':2})

GC_1704 = Coupling(name = 'GC_1704',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq2x2x1x2)',
                   order = {'LAM':1,'QED':2})

GC_1705 = Coupling(name = 'GC_1705',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq2x2x1x3)',
                   order = {'LAM':1,'QED':2})

GC_1706 = Coupling(name = 'GC_1706',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq2x2x2x1)',
                   order = {'LAM':1,'QED':2})

GC_1707 = Coupling(name = 'GC_1707',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq2x2x2x2)',
                   order = {'LAM':1,'QED':2})

GC_1708 = Coupling(name = 'GC_1708',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq2x2x2x3)',
                   order = {'LAM':1,'QED':2})

GC_1709 = Coupling(name = 'GC_1709',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq2x2x3x1)',
                   order = {'LAM':1,'QED':2})

GC_1710 = Coupling(name = 'GC_1710',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq2x2x3x2)',
                   order = {'LAM':1,'QED':2})

GC_1711 = Coupling(name = 'GC_1711',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq2x2x3x3)',
                   order = {'LAM':1,'QED':2})

GC_1712 = Coupling(name = 'GC_1712',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq2x3x1x1)',
                   order = {'LAM':1,'QED':2})

GC_1713 = Coupling(name = 'GC_1713',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq2x3x1x2)',
                   order = {'LAM':1,'QED':2})

GC_1714 = Coupling(name = 'GC_1714',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq2x3x1x3)',
                   order = {'LAM':1,'QED':2})

GC_1715 = Coupling(name = 'GC_1715',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq2x3x2x1)',
                   order = {'LAM':1,'QED':2})

GC_1716 = Coupling(name = 'GC_1716',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq2x3x2x2)',
                   order = {'LAM':1,'QED':2})

GC_1717 = Coupling(name = 'GC_1717',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq2x3x2x3)',
                   order = {'LAM':1,'QED':2})

GC_1718 = Coupling(name = 'GC_1718',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq2x3x3x1)',
                   order = {'LAM':1,'QED':2})

GC_1719 = Coupling(name = 'GC_1719',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq2x3x3x2)',
                   order = {'LAM':1,'QED':2})

GC_1720 = Coupling(name = 'GC_1720',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq2x3x3x3)',
                   order = {'LAM':1,'QED':2})

GC_1721 = Coupling(name = 'GC_1721',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq3x1x1x1)',
                   order = {'LAM':1,'QED':2})

GC_1722 = Coupling(name = 'GC_1722',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq3x1x1x2)',
                   order = {'LAM':1,'QED':2})

GC_1723 = Coupling(name = 'GC_1723',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq3x1x1x3)',
                   order = {'LAM':1,'QED':2})

GC_1724 = Coupling(name = 'GC_1724',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq3x1x2x1)',
                   order = {'LAM':1,'QED':2})

GC_1725 = Coupling(name = 'GC_1725',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq3x1x2x2)',
                   order = {'LAM':1,'QED':2})

GC_1726 = Coupling(name = 'GC_1726',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq3x1x2x3)',
                   order = {'LAM':1,'QED':2})

GC_1727 = Coupling(name = 'GC_1727',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq3x1x3x1)',
                   order = {'LAM':1,'QED':2})

GC_1728 = Coupling(name = 'GC_1728',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq3x1x3x2)',
                   order = {'LAM':1,'QED':2})

GC_1729 = Coupling(name = 'GC_1729',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq3x1x3x3)',
                   order = {'LAM':1,'QED':2})

GC_1730 = Coupling(name = 'GC_1730',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq3x2x1x1)',
                   order = {'LAM':1,'QED':2})

GC_1731 = Coupling(name = 'GC_1731',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq3x2x1x2)',
                   order = {'LAM':1,'QED':2})

GC_1732 = Coupling(name = 'GC_1732',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq3x2x1x3)',
                   order = {'LAM':1,'QED':2})

GC_1733 = Coupling(name = 'GC_1733',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq3x2x2x1)',
                   order = {'LAM':1,'QED':2})

GC_1734 = Coupling(name = 'GC_1734',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq3x2x2x2)',
                   order = {'LAM':1,'QED':2})

GC_1735 = Coupling(name = 'GC_1735',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq3x2x2x3)',
                   order = {'LAM':1,'QED':2})

GC_1736 = Coupling(name = 'GC_1736',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq3x2x3x1)',
                   order = {'LAM':1,'QED':2})

GC_1737 = Coupling(name = 'GC_1737',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq3x2x3x2)',
                   order = {'LAM':1,'QED':2})

GC_1738 = Coupling(name = 'GC_1738',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq3x2x3x3)',
                   order = {'LAM':1,'QED':2})

GC_1739 = Coupling(name = 'GC_1739',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq3x3x1x1)',
                   order = {'LAM':1,'QED':2})

GC_1740 = Coupling(name = 'GC_1740',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq3x3x1x2)',
                   order = {'LAM':1,'QED':2})

GC_1741 = Coupling(name = 'GC_1741',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq3x3x1x3)',
                   order = {'LAM':1,'QED':2})

GC_1742 = Coupling(name = 'GC_1742',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq3x3x2x1)',
                   order = {'LAM':1,'QED':2})

GC_1743 = Coupling(name = 'GC_1743',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq3x3x2x2)',
                   order = {'LAM':1,'QED':2})

GC_1744 = Coupling(name = 'GC_1744',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq3x3x2x3)',
                   order = {'LAM':1,'QED':2})

GC_1745 = Coupling(name = 'GC_1745',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq3x3x3x1)',
                   order = {'LAM':1,'QED':2})

GC_1746 = Coupling(name = 'GC_1746',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq3x3x3x2)',
                   order = {'LAM':1,'QED':2})

GC_1747 = Coupling(name = 'GC_1747',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Cledq3x3x3x3)',
                   order = {'LAM':1,'QED':2})

GC_1748 = Coupling(name = 'GC_1748',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ11x1x1x1))',
                   order = {'LAM':1,'QED':2})

GC_1749 = Coupling(name = 'GC_1749',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ11x1x1x1)',
                   order = {'LAM':1,'QED':2})

GC_1750 = Coupling(name = 'GC_1750',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ11x1x1x2))',
                   order = {'LAM':1,'QED':2})

GC_1751 = Coupling(name = 'GC_1751',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ11x1x1x2)',
                   order = {'LAM':1,'QED':2})

GC_1752 = Coupling(name = 'GC_1752',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ11x1x1x3))',
                   order = {'LAM':1,'QED':2})

GC_1753 = Coupling(name = 'GC_1753',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ11x1x1x3)',
                   order = {'LAM':1,'QED':2})

GC_1754 = Coupling(name = 'GC_1754',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ11x1x2x1))',
                   order = {'LAM':1,'QED':2})

GC_1755 = Coupling(name = 'GC_1755',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ11x1x2x1)',
                   order = {'LAM':1,'QED':2})

GC_1756 = Coupling(name = 'GC_1756',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ11x1x2x2))',
                   order = {'LAM':1,'QED':2})

GC_1757 = Coupling(name = 'GC_1757',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ11x1x2x2)',
                   order = {'LAM':1,'QED':2})

GC_1758 = Coupling(name = 'GC_1758',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ11x1x2x3))',
                   order = {'LAM':1,'QED':2})

GC_1759 = Coupling(name = 'GC_1759',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ11x1x2x3)',
                   order = {'LAM':1,'QED':2})

GC_1760 = Coupling(name = 'GC_1760',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ11x1x3x1))',
                   order = {'LAM':1,'QED':2})

GC_1761 = Coupling(name = 'GC_1761',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ11x1x3x1)',
                   order = {'LAM':1,'QED':2})

GC_1762 = Coupling(name = 'GC_1762',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ11x1x3x2))',
                   order = {'LAM':1,'QED':2})

GC_1763 = Coupling(name = 'GC_1763',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ11x1x3x2)',
                   order = {'LAM':1,'QED':2})

GC_1764 = Coupling(name = 'GC_1764',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ11x1x3x3))',
                   order = {'LAM':1,'QED':2})

GC_1765 = Coupling(name = 'GC_1765',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ11x1x3x3)',
                   order = {'LAM':1,'QED':2})

GC_1766 = Coupling(name = 'GC_1766',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ11x2x1x1))',
                   order = {'LAM':1,'QED':2})

GC_1767 = Coupling(name = 'GC_1767',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ11x2x1x1)',
                   order = {'LAM':1,'QED':2})

GC_1768 = Coupling(name = 'GC_1768',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ11x2x1x2))',
                   order = {'LAM':1,'QED':2})

GC_1769 = Coupling(name = 'GC_1769',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ11x2x1x2)',
                   order = {'LAM':1,'QED':2})

GC_1770 = Coupling(name = 'GC_1770',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ11x2x1x3))',
                   order = {'LAM':1,'QED':2})

GC_1771 = Coupling(name = 'GC_1771',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ11x2x1x3)',
                   order = {'LAM':1,'QED':2})

GC_1772 = Coupling(name = 'GC_1772',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ11x2x2x1))',
                   order = {'LAM':1,'QED':2})

GC_1773 = Coupling(name = 'GC_1773',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ11x2x2x1)',
                   order = {'LAM':1,'QED':2})

GC_1774 = Coupling(name = 'GC_1774',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ11x2x2x2))',
                   order = {'LAM':1,'QED':2})

GC_1775 = Coupling(name = 'GC_1775',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ11x2x2x2)',
                   order = {'LAM':1,'QED':2})

GC_1776 = Coupling(name = 'GC_1776',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ11x2x2x3))',
                   order = {'LAM':1,'QED':2})

GC_1777 = Coupling(name = 'GC_1777',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ11x2x2x3)',
                   order = {'LAM':1,'QED':2})

GC_1778 = Coupling(name = 'GC_1778',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ11x2x3x1))',
                   order = {'LAM':1,'QED':2})

GC_1779 = Coupling(name = 'GC_1779',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ11x2x3x1)',
                   order = {'LAM':1,'QED':2})

GC_1780 = Coupling(name = 'GC_1780',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ11x2x3x2))',
                   order = {'LAM':1,'QED':2})

GC_1781 = Coupling(name = 'GC_1781',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ11x2x3x2)',
                   order = {'LAM':1,'QED':2})

GC_1782 = Coupling(name = 'GC_1782',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ11x2x3x3))',
                   order = {'LAM':1,'QED':2})

GC_1783 = Coupling(name = 'GC_1783',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ11x2x3x3)',
                   order = {'LAM':1,'QED':2})

GC_1784 = Coupling(name = 'GC_1784',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ11x3x1x1))',
                   order = {'LAM':1,'QED':2})

GC_1785 = Coupling(name = 'GC_1785',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ11x3x1x1)',
                   order = {'LAM':1,'QED':2})

GC_1786 = Coupling(name = 'GC_1786',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ11x3x1x2))',
                   order = {'LAM':1,'QED':2})

GC_1787 = Coupling(name = 'GC_1787',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ11x3x1x2)',
                   order = {'LAM':1,'QED':2})

GC_1788 = Coupling(name = 'GC_1788',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ11x3x1x3))',
                   order = {'LAM':1,'QED':2})

GC_1789 = Coupling(name = 'GC_1789',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ11x3x1x3)',
                   order = {'LAM':1,'QED':2})

GC_1790 = Coupling(name = 'GC_1790',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ11x3x2x1))',
                   order = {'LAM':1,'QED':2})

GC_1791 = Coupling(name = 'GC_1791',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ11x3x2x1)',
                   order = {'LAM':1,'QED':2})

GC_1792 = Coupling(name = 'GC_1792',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ11x3x2x2))',
                   order = {'LAM':1,'QED':2})

GC_1793 = Coupling(name = 'GC_1793',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ11x3x2x2)',
                   order = {'LAM':1,'QED':2})

GC_1794 = Coupling(name = 'GC_1794',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ11x3x2x3))',
                   order = {'LAM':1,'QED':2})

GC_1795 = Coupling(name = 'GC_1795',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ11x3x2x3)',
                   order = {'LAM':1,'QED':2})

GC_1796 = Coupling(name = 'GC_1796',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ11x3x3x1))',
                   order = {'LAM':1,'QED':2})

GC_1797 = Coupling(name = 'GC_1797',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ11x3x3x1)',
                   order = {'LAM':1,'QED':2})

GC_1798 = Coupling(name = 'GC_1798',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ11x3x3x2))',
                   order = {'LAM':1,'QED':2})

GC_1799 = Coupling(name = 'GC_1799',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ11x3x3x2)',
                   order = {'LAM':1,'QED':2})

GC_1800 = Coupling(name = 'GC_1800',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ11x3x3x3))',
                   order = {'LAM':1,'QED':2})

GC_1801 = Coupling(name = 'GC_1801',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ11x3x3x3)',
                   order = {'LAM':1,'QED':2})

GC_1802 = Coupling(name = 'GC_1802',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ12x1x1x1))',
                   order = {'LAM':1,'QED':2})

GC_1803 = Coupling(name = 'GC_1803',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ12x1x1x1)',
                   order = {'LAM':1,'QED':2})

GC_1804 = Coupling(name = 'GC_1804',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ12x1x1x2))',
                   order = {'LAM':1,'QED':2})

GC_1805 = Coupling(name = 'GC_1805',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ12x1x1x2)',
                   order = {'LAM':1,'QED':2})

GC_1806 = Coupling(name = 'GC_1806',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ12x1x1x3))',
                   order = {'LAM':1,'QED':2})

GC_1807 = Coupling(name = 'GC_1807',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ12x1x1x3)',
                   order = {'LAM':1,'QED':2})

GC_1808 = Coupling(name = 'GC_1808',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ12x1x2x1))',
                   order = {'LAM':1,'QED':2})

GC_1809 = Coupling(name = 'GC_1809',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ12x1x2x1)',
                   order = {'LAM':1,'QED':2})

GC_1810 = Coupling(name = 'GC_1810',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ12x1x2x2))',
                   order = {'LAM':1,'QED':2})

GC_1811 = Coupling(name = 'GC_1811',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ12x1x2x2)',
                   order = {'LAM':1,'QED':2})

GC_1812 = Coupling(name = 'GC_1812',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ12x1x2x3))',
                   order = {'LAM':1,'QED':2})

GC_1813 = Coupling(name = 'GC_1813',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ12x1x2x3)',
                   order = {'LAM':1,'QED':2})

GC_1814 = Coupling(name = 'GC_1814',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ12x1x3x1))',
                   order = {'LAM':1,'QED':2})

GC_1815 = Coupling(name = 'GC_1815',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ12x1x3x1)',
                   order = {'LAM':1,'QED':2})

GC_1816 = Coupling(name = 'GC_1816',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ12x1x3x2))',
                   order = {'LAM':1,'QED':2})

GC_1817 = Coupling(name = 'GC_1817',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ12x1x3x2)',
                   order = {'LAM':1,'QED':2})

GC_1818 = Coupling(name = 'GC_1818',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ12x1x3x3))',
                   order = {'LAM':1,'QED':2})

GC_1819 = Coupling(name = 'GC_1819',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ12x1x3x3)',
                   order = {'LAM':1,'QED':2})

GC_1820 = Coupling(name = 'GC_1820',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ12x2x1x1))',
                   order = {'LAM':1,'QED':2})

GC_1821 = Coupling(name = 'GC_1821',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ12x2x1x1)',
                   order = {'LAM':1,'QED':2})

GC_1822 = Coupling(name = 'GC_1822',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ12x2x1x2))',
                   order = {'LAM':1,'QED':2})

GC_1823 = Coupling(name = 'GC_1823',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ12x2x1x2)',
                   order = {'LAM':1,'QED':2})

GC_1824 = Coupling(name = 'GC_1824',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ12x2x1x3))',
                   order = {'LAM':1,'QED':2})

GC_1825 = Coupling(name = 'GC_1825',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ12x2x1x3)',
                   order = {'LAM':1,'QED':2})

GC_1826 = Coupling(name = 'GC_1826',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ12x2x2x1))',
                   order = {'LAM':1,'QED':2})

GC_1827 = Coupling(name = 'GC_1827',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ12x2x2x1)',
                   order = {'LAM':1,'QED':2})

GC_1828 = Coupling(name = 'GC_1828',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ12x2x2x2))',
                   order = {'LAM':1,'QED':2})

GC_1829 = Coupling(name = 'GC_1829',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ12x2x2x2)',
                   order = {'LAM':1,'QED':2})

GC_1830 = Coupling(name = 'GC_1830',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ12x2x2x3))',
                   order = {'LAM':1,'QED':2})

GC_1831 = Coupling(name = 'GC_1831',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ12x2x2x3)',
                   order = {'LAM':1,'QED':2})

GC_1832 = Coupling(name = 'GC_1832',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ12x2x3x1))',
                   order = {'LAM':1,'QED':2})

GC_1833 = Coupling(name = 'GC_1833',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ12x2x3x1)',
                   order = {'LAM':1,'QED':2})

GC_1834 = Coupling(name = 'GC_1834',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ12x2x3x2))',
                   order = {'LAM':1,'QED':2})

GC_1835 = Coupling(name = 'GC_1835',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ12x2x3x2)',
                   order = {'LAM':1,'QED':2})

GC_1836 = Coupling(name = 'GC_1836',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ12x2x3x3))',
                   order = {'LAM':1,'QED':2})

GC_1837 = Coupling(name = 'GC_1837',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ12x2x3x3)',
                   order = {'LAM':1,'QED':2})

GC_1838 = Coupling(name = 'GC_1838',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ12x3x1x1))',
                   order = {'LAM':1,'QED':2})

GC_1839 = Coupling(name = 'GC_1839',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ12x3x1x1)',
                   order = {'LAM':1,'QED':2})

GC_1840 = Coupling(name = 'GC_1840',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ12x3x1x2))',
                   order = {'LAM':1,'QED':2})

GC_1841 = Coupling(name = 'GC_1841',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ12x3x1x2)',
                   order = {'LAM':1,'QED':2})

GC_1842 = Coupling(name = 'GC_1842',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ12x3x1x3))',
                   order = {'LAM':1,'QED':2})

GC_1843 = Coupling(name = 'GC_1843',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ12x3x1x3)',
                   order = {'LAM':1,'QED':2})

GC_1844 = Coupling(name = 'GC_1844',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ12x3x2x1))',
                   order = {'LAM':1,'QED':2})

GC_1845 = Coupling(name = 'GC_1845',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ12x3x2x1)',
                   order = {'LAM':1,'QED':2})

GC_1846 = Coupling(name = 'GC_1846',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ12x3x2x2))',
                   order = {'LAM':1,'QED':2})

GC_1847 = Coupling(name = 'GC_1847',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ12x3x2x2)',
                   order = {'LAM':1,'QED':2})

GC_1848 = Coupling(name = 'GC_1848',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ12x3x2x3))',
                   order = {'LAM':1,'QED':2})

GC_1849 = Coupling(name = 'GC_1849',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ12x3x2x3)',
                   order = {'LAM':1,'QED':2})

GC_1850 = Coupling(name = 'GC_1850',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ12x3x3x1))',
                   order = {'LAM':1,'QED':2})

GC_1851 = Coupling(name = 'GC_1851',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ12x3x3x1)',
                   order = {'LAM':1,'QED':2})

GC_1852 = Coupling(name = 'GC_1852',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ12x3x3x2))',
                   order = {'LAM':1,'QED':2})

GC_1853 = Coupling(name = 'GC_1853',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ12x3x3x2)',
                   order = {'LAM':1,'QED':2})

GC_1854 = Coupling(name = 'GC_1854',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ12x3x3x3))',
                   order = {'LAM':1,'QED':2})

GC_1855 = Coupling(name = 'GC_1855',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ12x3x3x3)',
                   order = {'LAM':1,'QED':2})

GC_1856 = Coupling(name = 'GC_1856',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ13x1x1x1))',
                   order = {'LAM':1,'QED':2})

GC_1857 = Coupling(name = 'GC_1857',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ13x1x1x1)',
                   order = {'LAM':1,'QED':2})

GC_1858 = Coupling(name = 'GC_1858',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ13x1x1x2))',
                   order = {'LAM':1,'QED':2})

GC_1859 = Coupling(name = 'GC_1859',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ13x1x1x2)',
                   order = {'LAM':1,'QED':2})

GC_1860 = Coupling(name = 'GC_1860',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ13x1x1x3))',
                   order = {'LAM':1,'QED':2})

GC_1861 = Coupling(name = 'GC_1861',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ13x1x1x3)',
                   order = {'LAM':1,'QED':2})

GC_1862 = Coupling(name = 'GC_1862',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ13x1x2x1))',
                   order = {'LAM':1,'QED':2})

GC_1863 = Coupling(name = 'GC_1863',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ13x1x2x1)',
                   order = {'LAM':1,'QED':2})

GC_1864 = Coupling(name = 'GC_1864',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ13x1x2x2))',
                   order = {'LAM':1,'QED':2})

GC_1865 = Coupling(name = 'GC_1865',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ13x1x2x2)',
                   order = {'LAM':1,'QED':2})

GC_1866 = Coupling(name = 'GC_1866',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ13x1x2x3))',
                   order = {'LAM':1,'QED':2})

GC_1867 = Coupling(name = 'GC_1867',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ13x1x2x3)',
                   order = {'LAM':1,'QED':2})

GC_1868 = Coupling(name = 'GC_1868',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ13x1x3x1))',
                   order = {'LAM':1,'QED':2})

GC_1869 = Coupling(name = 'GC_1869',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ13x1x3x1)',
                   order = {'LAM':1,'QED':2})

GC_1870 = Coupling(name = 'GC_1870',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ13x1x3x2))',
                   order = {'LAM':1,'QED':2})

GC_1871 = Coupling(name = 'GC_1871',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ13x1x3x2)',
                   order = {'LAM':1,'QED':2})

GC_1872 = Coupling(name = 'GC_1872',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ13x1x3x3))',
                   order = {'LAM':1,'QED':2})

GC_1873 = Coupling(name = 'GC_1873',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ13x1x3x3)',
                   order = {'LAM':1,'QED':2})

GC_1874 = Coupling(name = 'GC_1874',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ13x2x1x1))',
                   order = {'LAM':1,'QED':2})

GC_1875 = Coupling(name = 'GC_1875',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ13x2x1x1)',
                   order = {'LAM':1,'QED':2})

GC_1876 = Coupling(name = 'GC_1876',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ13x2x1x2))',
                   order = {'LAM':1,'QED':2})

GC_1877 = Coupling(name = 'GC_1877',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ13x2x1x2)',
                   order = {'LAM':1,'QED':2})

GC_1878 = Coupling(name = 'GC_1878',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ13x2x1x3))',
                   order = {'LAM':1,'QED':2})

GC_1879 = Coupling(name = 'GC_1879',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ13x2x1x3)',
                   order = {'LAM':1,'QED':2})

GC_1880 = Coupling(name = 'GC_1880',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ13x2x2x1))',
                   order = {'LAM':1,'QED':2})

GC_1881 = Coupling(name = 'GC_1881',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ13x2x2x1)',
                   order = {'LAM':1,'QED':2})

GC_1882 = Coupling(name = 'GC_1882',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ13x2x2x2))',
                   order = {'LAM':1,'QED':2})

GC_1883 = Coupling(name = 'GC_1883',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ13x2x2x2)',
                   order = {'LAM':1,'QED':2})

GC_1884 = Coupling(name = 'GC_1884',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ13x2x2x3))',
                   order = {'LAM':1,'QED':2})

GC_1885 = Coupling(name = 'GC_1885',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ13x2x2x3)',
                   order = {'LAM':1,'QED':2})

GC_1886 = Coupling(name = 'GC_1886',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ13x2x3x1))',
                   order = {'LAM':1,'QED':2})

GC_1887 = Coupling(name = 'GC_1887',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ13x2x3x1)',
                   order = {'LAM':1,'QED':2})

GC_1888 = Coupling(name = 'GC_1888',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ13x2x3x2))',
                   order = {'LAM':1,'QED':2})

GC_1889 = Coupling(name = 'GC_1889',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ13x2x3x2)',
                   order = {'LAM':1,'QED':2})

GC_1890 = Coupling(name = 'GC_1890',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ13x2x3x3))',
                   order = {'LAM':1,'QED':2})

GC_1891 = Coupling(name = 'GC_1891',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ13x2x3x3)',
                   order = {'LAM':1,'QED':2})

GC_1892 = Coupling(name = 'GC_1892',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ13x3x1x1))',
                   order = {'LAM':1,'QED':2})

GC_1893 = Coupling(name = 'GC_1893',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ13x3x1x1)',
                   order = {'LAM':1,'QED':2})

GC_1894 = Coupling(name = 'GC_1894',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ13x3x1x2))',
                   order = {'LAM':1,'QED':2})

GC_1895 = Coupling(name = 'GC_1895',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ13x3x1x2)',
                   order = {'LAM':1,'QED':2})

GC_1896 = Coupling(name = 'GC_1896',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ13x3x1x3))',
                   order = {'LAM':1,'QED':2})

GC_1897 = Coupling(name = 'GC_1897',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ13x3x1x3)',
                   order = {'LAM':1,'QED':2})

GC_1898 = Coupling(name = 'GC_1898',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ13x3x2x1))',
                   order = {'LAM':1,'QED':2})

GC_1899 = Coupling(name = 'GC_1899',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ13x3x2x1)',
                   order = {'LAM':1,'QED':2})

GC_1900 = Coupling(name = 'GC_1900',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ13x3x2x2))',
                   order = {'LAM':1,'QED':2})

GC_1901 = Coupling(name = 'GC_1901',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ13x3x2x2)',
                   order = {'LAM':1,'QED':2})

GC_1902 = Coupling(name = 'GC_1902',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ13x3x2x3))',
                   order = {'LAM':1,'QED':2})

GC_1903 = Coupling(name = 'GC_1903',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ13x3x2x3)',
                   order = {'LAM':1,'QED':2})

GC_1904 = Coupling(name = 'GC_1904',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ13x3x3x1))',
                   order = {'LAM':1,'QED':2})

GC_1905 = Coupling(name = 'GC_1905',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ13x3x3x1)',
                   order = {'LAM':1,'QED':2})

GC_1906 = Coupling(name = 'GC_1906',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ13x3x3x2))',
                   order = {'LAM':1,'QED':2})

GC_1907 = Coupling(name = 'GC_1907',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ13x3x3x2)',
                   order = {'LAM':1,'QED':2})

GC_1908 = Coupling(name = 'GC_1908',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ13x3x3x3))',
                   order = {'LAM':1,'QED':2})

GC_1909 = Coupling(name = 'GC_1909',
                   value = 'complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ13x3x3x3)',
                   order = {'LAM':1,'QED':2})

GC_1910 = Coupling(name = 'GC_1910',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x1x1x1))/4.',
                   order = {'LAM':1,'QED':2})

GC_1911 = Coupling(name = 'GC_1911',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x1x1x1))/4.',
                   order = {'LAM':1,'QED':2})

GC_1912 = Coupling(name = 'GC_1912',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x1x1x1))/2.',
                   order = {'LAM':1,'QED':2})

GC_1913 = Coupling(name = 'GC_1913',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x1x1x1))/2.',
                   order = {'LAM':1,'QED':2})

GC_1914 = Coupling(name = 'GC_1914',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x1x1x2))/4.',
                   order = {'LAM':1,'QED':2})

GC_1915 = Coupling(name = 'GC_1915',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x1x1x2))/4.',
                   order = {'LAM':1,'QED':2})

GC_1916 = Coupling(name = 'GC_1916',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x1x1x2))/2.',
                   order = {'LAM':1,'QED':2})

GC_1917 = Coupling(name = 'GC_1917',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x1x1x2))/2.',
                   order = {'LAM':1,'QED':2})

GC_1918 = Coupling(name = 'GC_1918',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x1x1x3))/4.',
                   order = {'LAM':1,'QED':2})

GC_1919 = Coupling(name = 'GC_1919',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x1x1x3))/4.',
                   order = {'LAM':1,'QED':2})

GC_1920 = Coupling(name = 'GC_1920',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x1x1x3))/2.',
                   order = {'LAM':1,'QED':2})

GC_1921 = Coupling(name = 'GC_1921',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x1x1x3))/2.',
                   order = {'LAM':1,'QED':2})

GC_1922 = Coupling(name = 'GC_1922',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x1x2x1))/4.',
                   order = {'LAM':1,'QED':2})

GC_1923 = Coupling(name = 'GC_1923',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x1x2x1))/4.',
                   order = {'LAM':1,'QED':2})

GC_1924 = Coupling(name = 'GC_1924',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x1x2x1))/2.',
                   order = {'LAM':1,'QED':2})

GC_1925 = Coupling(name = 'GC_1925',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x1x2x1))/2.',
                   order = {'LAM':1,'QED':2})

GC_1926 = Coupling(name = 'GC_1926',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x1x2x2))/4.',
                   order = {'LAM':1,'QED':2})

GC_1927 = Coupling(name = 'GC_1927',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x1x2x2))/4.',
                   order = {'LAM':1,'QED':2})

GC_1928 = Coupling(name = 'GC_1928',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x1x2x2))/2.',
                   order = {'LAM':1,'QED':2})

GC_1929 = Coupling(name = 'GC_1929',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x1x2x2))/2.',
                   order = {'LAM':1,'QED':2})

GC_1930 = Coupling(name = 'GC_1930',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x1x2x3))/4.',
                   order = {'LAM':1,'QED':2})

GC_1931 = Coupling(name = 'GC_1931',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x1x2x3))/4.',
                   order = {'LAM':1,'QED':2})

GC_1932 = Coupling(name = 'GC_1932',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x1x2x3))/2.',
                   order = {'LAM':1,'QED':2})

GC_1933 = Coupling(name = 'GC_1933',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x1x2x3))/2.',
                   order = {'LAM':1,'QED':2})

GC_1934 = Coupling(name = 'GC_1934',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x1x3x1))/4.',
                   order = {'LAM':1,'QED':2})

GC_1935 = Coupling(name = 'GC_1935',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x1x3x1))/4.',
                   order = {'LAM':1,'QED':2})

GC_1936 = Coupling(name = 'GC_1936',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x1x3x1))/2.',
                   order = {'LAM':1,'QED':2})

GC_1937 = Coupling(name = 'GC_1937',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x1x3x1))/2.',
                   order = {'LAM':1,'QED':2})

GC_1938 = Coupling(name = 'GC_1938',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x1x3x2))/4.',
                   order = {'LAM':1,'QED':2})

GC_1939 = Coupling(name = 'GC_1939',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x1x3x2))/4.',
                   order = {'LAM':1,'QED':2})

GC_1940 = Coupling(name = 'GC_1940',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x1x3x2))/2.',
                   order = {'LAM':1,'QED':2})

GC_1941 = Coupling(name = 'GC_1941',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x1x3x2))/2.',
                   order = {'LAM':1,'QED':2})

GC_1942 = Coupling(name = 'GC_1942',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x1x3x3))/4.',
                   order = {'LAM':1,'QED':2})

GC_1943 = Coupling(name = 'GC_1943',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x1x3x3))/4.',
                   order = {'LAM':1,'QED':2})

GC_1944 = Coupling(name = 'GC_1944',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x1x3x3))/2.',
                   order = {'LAM':1,'QED':2})

GC_1945 = Coupling(name = 'GC_1945',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x1x3x3))/2.',
                   order = {'LAM':1,'QED':2})

GC_1946 = Coupling(name = 'GC_1946',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x2x1x1))/4.',
                   order = {'LAM':1,'QED':2})

GC_1947 = Coupling(name = 'GC_1947',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x2x1x1))/4.',
                   order = {'LAM':1,'QED':2})

GC_1948 = Coupling(name = 'GC_1948',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x2x1x1))/2.',
                   order = {'LAM':1,'QED':2})

GC_1949 = Coupling(name = 'GC_1949',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x2x1x1))/2.',
                   order = {'LAM':1,'QED':2})

GC_1950 = Coupling(name = 'GC_1950',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x2x1x2))/4.',
                   order = {'LAM':1,'QED':2})

GC_1951 = Coupling(name = 'GC_1951',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x2x1x2))/4.',
                   order = {'LAM':1,'QED':2})

GC_1952 = Coupling(name = 'GC_1952',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x2x1x2))/2.',
                   order = {'LAM':1,'QED':2})

GC_1953 = Coupling(name = 'GC_1953',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x2x1x2))/2.',
                   order = {'LAM':1,'QED':2})

GC_1954 = Coupling(name = 'GC_1954',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x2x1x3))/4.',
                   order = {'LAM':1,'QED':2})

GC_1955 = Coupling(name = 'GC_1955',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x2x1x3))/4.',
                   order = {'LAM':1,'QED':2})

GC_1956 = Coupling(name = 'GC_1956',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x2x1x3))/2.',
                   order = {'LAM':1,'QED':2})

GC_1957 = Coupling(name = 'GC_1957',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x2x1x3))/2.',
                   order = {'LAM':1,'QED':2})

GC_1958 = Coupling(name = 'GC_1958',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x2x2x1))/4.',
                   order = {'LAM':1,'QED':2})

GC_1959 = Coupling(name = 'GC_1959',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x2x2x1))/4.',
                   order = {'LAM':1,'QED':2})

GC_1960 = Coupling(name = 'GC_1960',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x2x2x1))/2.',
                   order = {'LAM':1,'QED':2})

GC_1961 = Coupling(name = 'GC_1961',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x2x2x1))/2.',
                   order = {'LAM':1,'QED':2})

GC_1962 = Coupling(name = 'GC_1962',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x2x2x2))/4.',
                   order = {'LAM':1,'QED':2})

GC_1963 = Coupling(name = 'GC_1963',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x2x2x2))/4.',
                   order = {'LAM':1,'QED':2})

GC_1964 = Coupling(name = 'GC_1964',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x2x2x2))/2.',
                   order = {'LAM':1,'QED':2})

GC_1965 = Coupling(name = 'GC_1965',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x2x2x2))/2.',
                   order = {'LAM':1,'QED':2})

GC_1966 = Coupling(name = 'GC_1966',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x2x2x3))/4.',
                   order = {'LAM':1,'QED':2})

GC_1967 = Coupling(name = 'GC_1967',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x2x2x3))/4.',
                   order = {'LAM':1,'QED':2})

GC_1968 = Coupling(name = 'GC_1968',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x2x2x3))/2.',
                   order = {'LAM':1,'QED':2})

GC_1969 = Coupling(name = 'GC_1969',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x2x2x3))/2.',
                   order = {'LAM':1,'QED':2})

GC_1970 = Coupling(name = 'GC_1970',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x2x3x1))/4.',
                   order = {'LAM':1,'QED':2})

GC_1971 = Coupling(name = 'GC_1971',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x2x3x1))/4.',
                   order = {'LAM':1,'QED':2})

GC_1972 = Coupling(name = 'GC_1972',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x2x3x1))/2.',
                   order = {'LAM':1,'QED':2})

GC_1973 = Coupling(name = 'GC_1973',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x2x3x1))/2.',
                   order = {'LAM':1,'QED':2})

GC_1974 = Coupling(name = 'GC_1974',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x2x3x2))/4.',
                   order = {'LAM':1,'QED':2})

GC_1975 = Coupling(name = 'GC_1975',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x2x3x2))/4.',
                   order = {'LAM':1,'QED':2})

GC_1976 = Coupling(name = 'GC_1976',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x2x3x2))/2.',
                   order = {'LAM':1,'QED':2})

GC_1977 = Coupling(name = 'GC_1977',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x2x3x2))/2.',
                   order = {'LAM':1,'QED':2})

GC_1978 = Coupling(name = 'GC_1978',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x2x3x3))/4.',
                   order = {'LAM':1,'QED':2})

GC_1979 = Coupling(name = 'GC_1979',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x2x3x3))/4.',
                   order = {'LAM':1,'QED':2})

GC_1980 = Coupling(name = 'GC_1980',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x2x3x3))/2.',
                   order = {'LAM':1,'QED':2})

GC_1981 = Coupling(name = 'GC_1981',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x2x3x3))/2.',
                   order = {'LAM':1,'QED':2})

GC_1982 = Coupling(name = 'GC_1982',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x3x1x1))/4.',
                   order = {'LAM':1,'QED':2})

GC_1983 = Coupling(name = 'GC_1983',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x3x1x1))/4.',
                   order = {'LAM':1,'QED':2})

GC_1984 = Coupling(name = 'GC_1984',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x3x1x1))/2.',
                   order = {'LAM':1,'QED':2})

GC_1985 = Coupling(name = 'GC_1985',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x3x1x1))/2.',
                   order = {'LAM':1,'QED':2})

GC_1986 = Coupling(name = 'GC_1986',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x3x1x2))/4.',
                   order = {'LAM':1,'QED':2})

GC_1987 = Coupling(name = 'GC_1987',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x3x1x2))/4.',
                   order = {'LAM':1,'QED':2})

GC_1988 = Coupling(name = 'GC_1988',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x3x1x2))/2.',
                   order = {'LAM':1,'QED':2})

GC_1989 = Coupling(name = 'GC_1989',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x3x1x2))/2.',
                   order = {'LAM':1,'QED':2})

GC_1990 = Coupling(name = 'GC_1990',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x3x1x3))/4.',
                   order = {'LAM':1,'QED':2})

GC_1991 = Coupling(name = 'GC_1991',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x3x1x3))/4.',
                   order = {'LAM':1,'QED':2})

GC_1992 = Coupling(name = 'GC_1992',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x3x1x3))/2.',
                   order = {'LAM':1,'QED':2})

GC_1993 = Coupling(name = 'GC_1993',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x3x1x3))/2.',
                   order = {'LAM':1,'QED':2})

GC_1994 = Coupling(name = 'GC_1994',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x3x2x1))/4.',
                   order = {'LAM':1,'QED':2})

GC_1995 = Coupling(name = 'GC_1995',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x3x2x1))/4.',
                   order = {'LAM':1,'QED':2})

GC_1996 = Coupling(name = 'GC_1996',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x3x2x1))/2.',
                   order = {'LAM':1,'QED':2})

GC_1997 = Coupling(name = 'GC_1997',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x3x2x1))/2.',
                   order = {'LAM':1,'QED':2})

GC_1998 = Coupling(name = 'GC_1998',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x3x2x2))/4.',
                   order = {'LAM':1,'QED':2})

GC_1999 = Coupling(name = 'GC_1999',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x3x2x2))/4.',
                   order = {'LAM':1,'QED':2})

GC_2000 = Coupling(name = 'GC_2000',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x3x2x2))/2.',
                   order = {'LAM':1,'QED':2})

GC_2001 = Coupling(name = 'GC_2001',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x3x2x2))/2.',
                   order = {'LAM':1,'QED':2})

GC_2002 = Coupling(name = 'GC_2002',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x3x2x3))/4.',
                   order = {'LAM':1,'QED':2})

GC_2003 = Coupling(name = 'GC_2003',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x3x2x3))/4.',
                   order = {'LAM':1,'QED':2})

GC_2004 = Coupling(name = 'GC_2004',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x3x2x3))/2.',
                   order = {'LAM':1,'QED':2})

GC_2005 = Coupling(name = 'GC_2005',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x3x2x3))/2.',
                   order = {'LAM':1,'QED':2})

GC_2006 = Coupling(name = 'GC_2006',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x3x3x1))/4.',
                   order = {'LAM':1,'QED':2})

GC_2007 = Coupling(name = 'GC_2007',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x3x3x1))/4.',
                   order = {'LAM':1,'QED':2})

GC_2008 = Coupling(name = 'GC_2008',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x3x3x1))/2.',
                   order = {'LAM':1,'QED':2})

GC_2009 = Coupling(name = 'GC_2009',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x3x3x1))/2.',
                   order = {'LAM':1,'QED':2})

GC_2010 = Coupling(name = 'GC_2010',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x3x3x2))/4.',
                   order = {'LAM':1,'QED':2})

GC_2011 = Coupling(name = 'GC_2011',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x3x3x2))/4.',
                   order = {'LAM':1,'QED':2})

GC_2012 = Coupling(name = 'GC_2012',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x3x3x2))/2.',
                   order = {'LAM':1,'QED':2})

GC_2013 = Coupling(name = 'GC_2013',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x3x3x2))/2.',
                   order = {'LAM':1,'QED':2})

GC_2014 = Coupling(name = 'GC_2014',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x3x3x3))/4.',
                   order = {'LAM':1,'QED':2})

GC_2015 = Coupling(name = 'GC_2015',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x3x3x3))/4.',
                   order = {'LAM':1,'QED':2})

GC_2016 = Coupling(name = 'GC_2016',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x3x3x3))/2.',
                   order = {'LAM':1,'QED':2})

GC_2017 = Coupling(name = 'GC_2017',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ31x3x3x3))/2.',
                   order = {'LAM':1,'QED':2})

GC_2018 = Coupling(name = 'GC_2018',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x1x1x1))/4.',
                   order = {'LAM':1,'QED':2})

GC_2019 = Coupling(name = 'GC_2019',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x1x1x1))/4.',
                   order = {'LAM':1,'QED':2})

GC_2020 = Coupling(name = 'GC_2020',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x1x1x1))/2.',
                   order = {'LAM':1,'QED':2})

GC_2021 = Coupling(name = 'GC_2021',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x1x1x1))/2.',
                   order = {'LAM':1,'QED':2})

GC_2022 = Coupling(name = 'GC_2022',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x1x1x2))/4.',
                   order = {'LAM':1,'QED':2})

GC_2023 = Coupling(name = 'GC_2023',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x1x1x2))/4.',
                   order = {'LAM':1,'QED':2})

GC_2024 = Coupling(name = 'GC_2024',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x1x1x2))/2.',
                   order = {'LAM':1,'QED':2})

GC_2025 = Coupling(name = 'GC_2025',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x1x1x2))/2.',
                   order = {'LAM':1,'QED':2})

GC_2026 = Coupling(name = 'GC_2026',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x1x1x3))/4.',
                   order = {'LAM':1,'QED':2})

GC_2027 = Coupling(name = 'GC_2027',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x1x1x3))/4.',
                   order = {'LAM':1,'QED':2})

GC_2028 = Coupling(name = 'GC_2028',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x1x1x3))/2.',
                   order = {'LAM':1,'QED':2})

GC_2029 = Coupling(name = 'GC_2029',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x1x1x3))/2.',
                   order = {'LAM':1,'QED':2})

GC_2030 = Coupling(name = 'GC_2030',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x1x2x1))/4.',
                   order = {'LAM':1,'QED':2})

GC_2031 = Coupling(name = 'GC_2031',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x1x2x1))/4.',
                   order = {'LAM':1,'QED':2})

GC_2032 = Coupling(name = 'GC_2032',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x1x2x1))/2.',
                   order = {'LAM':1,'QED':2})

GC_2033 = Coupling(name = 'GC_2033',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x1x2x1))/2.',
                   order = {'LAM':1,'QED':2})

GC_2034 = Coupling(name = 'GC_2034',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x1x2x2))/4.',
                   order = {'LAM':1,'QED':2})

GC_2035 = Coupling(name = 'GC_2035',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x1x2x2))/4.',
                   order = {'LAM':1,'QED':2})

GC_2036 = Coupling(name = 'GC_2036',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x1x2x2))/2.',
                   order = {'LAM':1,'QED':2})

GC_2037 = Coupling(name = 'GC_2037',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x1x2x2))/2.',
                   order = {'LAM':1,'QED':2})

GC_2038 = Coupling(name = 'GC_2038',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x1x2x3))/4.',
                   order = {'LAM':1,'QED':2})

GC_2039 = Coupling(name = 'GC_2039',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x1x2x3))/4.',
                   order = {'LAM':1,'QED':2})

GC_2040 = Coupling(name = 'GC_2040',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x1x2x3))/2.',
                   order = {'LAM':1,'QED':2})

GC_2041 = Coupling(name = 'GC_2041',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x1x2x3))/2.',
                   order = {'LAM':1,'QED':2})

GC_2042 = Coupling(name = 'GC_2042',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x1x3x1))/4.',
                   order = {'LAM':1,'QED':2})

GC_2043 = Coupling(name = 'GC_2043',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x1x3x1))/4.',
                   order = {'LAM':1,'QED':2})

GC_2044 = Coupling(name = 'GC_2044',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x1x3x1))/2.',
                   order = {'LAM':1,'QED':2})

GC_2045 = Coupling(name = 'GC_2045',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x1x3x1))/2.',
                   order = {'LAM':1,'QED':2})

GC_2046 = Coupling(name = 'GC_2046',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x1x3x2))/4.',
                   order = {'LAM':1,'QED':2})

GC_2047 = Coupling(name = 'GC_2047',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x1x3x2))/4.',
                   order = {'LAM':1,'QED':2})

GC_2048 = Coupling(name = 'GC_2048',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x1x3x2))/2.',
                   order = {'LAM':1,'QED':2})

GC_2049 = Coupling(name = 'GC_2049',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x1x3x2))/2.',
                   order = {'LAM':1,'QED':2})

GC_2050 = Coupling(name = 'GC_2050',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x1x3x3))/4.',
                   order = {'LAM':1,'QED':2})

GC_2051 = Coupling(name = 'GC_2051',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x1x3x3))/4.',
                   order = {'LAM':1,'QED':2})

GC_2052 = Coupling(name = 'GC_2052',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x1x3x3))/2.',
                   order = {'LAM':1,'QED':2})

GC_2053 = Coupling(name = 'GC_2053',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x1x3x3))/2.',
                   order = {'LAM':1,'QED':2})

GC_2054 = Coupling(name = 'GC_2054',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x2x1x1))/4.',
                   order = {'LAM':1,'QED':2})

GC_2055 = Coupling(name = 'GC_2055',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x2x1x1))/4.',
                   order = {'LAM':1,'QED':2})

GC_2056 = Coupling(name = 'GC_2056',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x2x1x1))/2.',
                   order = {'LAM':1,'QED':2})

GC_2057 = Coupling(name = 'GC_2057',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x2x1x1))/2.',
                   order = {'LAM':1,'QED':2})

GC_2058 = Coupling(name = 'GC_2058',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x2x1x2))/4.',
                   order = {'LAM':1,'QED':2})

GC_2059 = Coupling(name = 'GC_2059',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x2x1x2))/4.',
                   order = {'LAM':1,'QED':2})

GC_2060 = Coupling(name = 'GC_2060',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x2x1x2))/2.',
                   order = {'LAM':1,'QED':2})

GC_2061 = Coupling(name = 'GC_2061',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x2x1x2))/2.',
                   order = {'LAM':1,'QED':2})

GC_2062 = Coupling(name = 'GC_2062',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x2x1x3))/4.',
                   order = {'LAM':1,'QED':2})

GC_2063 = Coupling(name = 'GC_2063',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x2x1x3))/4.',
                   order = {'LAM':1,'QED':2})

GC_2064 = Coupling(name = 'GC_2064',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x2x1x3))/2.',
                   order = {'LAM':1,'QED':2})

GC_2065 = Coupling(name = 'GC_2065',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x2x1x3))/2.',
                   order = {'LAM':1,'QED':2})

GC_2066 = Coupling(name = 'GC_2066',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x2x2x1))/4.',
                   order = {'LAM':1,'QED':2})

GC_2067 = Coupling(name = 'GC_2067',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x2x2x1))/4.',
                   order = {'LAM':1,'QED':2})

GC_2068 = Coupling(name = 'GC_2068',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x2x2x1))/2.',
                   order = {'LAM':1,'QED':2})

GC_2069 = Coupling(name = 'GC_2069',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x2x2x1))/2.',
                   order = {'LAM':1,'QED':2})

GC_2070 = Coupling(name = 'GC_2070',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x2x2x2))/4.',
                   order = {'LAM':1,'QED':2})

GC_2071 = Coupling(name = 'GC_2071',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x2x2x2))/4.',
                   order = {'LAM':1,'QED':2})

GC_2072 = Coupling(name = 'GC_2072',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x2x2x2))/2.',
                   order = {'LAM':1,'QED':2})

GC_2073 = Coupling(name = 'GC_2073',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x2x2x2))/2.',
                   order = {'LAM':1,'QED':2})

GC_2074 = Coupling(name = 'GC_2074',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x2x2x3))/4.',
                   order = {'LAM':1,'QED':2})

GC_2075 = Coupling(name = 'GC_2075',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x2x2x3))/4.',
                   order = {'LAM':1,'QED':2})

GC_2076 = Coupling(name = 'GC_2076',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x2x2x3))/2.',
                   order = {'LAM':1,'QED':2})

GC_2077 = Coupling(name = 'GC_2077',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x2x2x3))/2.',
                   order = {'LAM':1,'QED':2})

GC_2078 = Coupling(name = 'GC_2078',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x2x3x1))/4.',
                   order = {'LAM':1,'QED':2})

GC_2079 = Coupling(name = 'GC_2079',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x2x3x1))/4.',
                   order = {'LAM':1,'QED':2})

GC_2080 = Coupling(name = 'GC_2080',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x2x3x1))/2.',
                   order = {'LAM':1,'QED':2})

GC_2081 = Coupling(name = 'GC_2081',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x2x3x1))/2.',
                   order = {'LAM':1,'QED':2})

GC_2082 = Coupling(name = 'GC_2082',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x2x3x2))/4.',
                   order = {'LAM':1,'QED':2})

GC_2083 = Coupling(name = 'GC_2083',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x2x3x2))/4.',
                   order = {'LAM':1,'QED':2})

GC_2084 = Coupling(name = 'GC_2084',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x2x3x2))/2.',
                   order = {'LAM':1,'QED':2})

GC_2085 = Coupling(name = 'GC_2085',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x2x3x2))/2.',
                   order = {'LAM':1,'QED':2})

GC_2086 = Coupling(name = 'GC_2086',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x2x3x3))/4.',
                   order = {'LAM':1,'QED':2})

GC_2087 = Coupling(name = 'GC_2087',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x2x3x3))/4.',
                   order = {'LAM':1,'QED':2})

GC_2088 = Coupling(name = 'GC_2088',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x2x3x3))/2.',
                   order = {'LAM':1,'QED':2})

GC_2089 = Coupling(name = 'GC_2089',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x2x3x3))/2.',
                   order = {'LAM':1,'QED':2})

GC_2090 = Coupling(name = 'GC_2090',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x3x1x1))/4.',
                   order = {'LAM':1,'QED':2})

GC_2091 = Coupling(name = 'GC_2091',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x3x1x1))/4.',
                   order = {'LAM':1,'QED':2})

GC_2092 = Coupling(name = 'GC_2092',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x3x1x1))/2.',
                   order = {'LAM':1,'QED':2})

GC_2093 = Coupling(name = 'GC_2093',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x3x1x1))/2.',
                   order = {'LAM':1,'QED':2})

GC_2094 = Coupling(name = 'GC_2094',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x3x1x2))/4.',
                   order = {'LAM':1,'QED':2})

GC_2095 = Coupling(name = 'GC_2095',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x3x1x2))/4.',
                   order = {'LAM':1,'QED':2})

GC_2096 = Coupling(name = 'GC_2096',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x3x1x2))/2.',
                   order = {'LAM':1,'QED':2})

GC_2097 = Coupling(name = 'GC_2097',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x3x1x2))/2.',
                   order = {'LAM':1,'QED':2})

GC_2098 = Coupling(name = 'GC_2098',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x3x1x3))/4.',
                   order = {'LAM':1,'QED':2})

GC_2099 = Coupling(name = 'GC_2099',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x3x1x3))/4.',
                   order = {'LAM':1,'QED':2})

GC_2100 = Coupling(name = 'GC_2100',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x3x1x3))/2.',
                   order = {'LAM':1,'QED':2})

GC_2101 = Coupling(name = 'GC_2101',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x3x1x3))/2.',
                   order = {'LAM':1,'QED':2})

GC_2102 = Coupling(name = 'GC_2102',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x3x2x1))/4.',
                   order = {'LAM':1,'QED':2})

GC_2103 = Coupling(name = 'GC_2103',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x3x2x1))/4.',
                   order = {'LAM':1,'QED':2})

GC_2104 = Coupling(name = 'GC_2104',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x3x2x1))/2.',
                   order = {'LAM':1,'QED':2})

GC_2105 = Coupling(name = 'GC_2105',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x3x2x1))/2.',
                   order = {'LAM':1,'QED':2})

GC_2106 = Coupling(name = 'GC_2106',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x3x2x2))/4.',
                   order = {'LAM':1,'QED':2})

GC_2107 = Coupling(name = 'GC_2107',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x3x2x2))/4.',
                   order = {'LAM':1,'QED':2})

GC_2108 = Coupling(name = 'GC_2108',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x3x2x2))/2.',
                   order = {'LAM':1,'QED':2})

GC_2109 = Coupling(name = 'GC_2109',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x3x2x2))/2.',
                   order = {'LAM':1,'QED':2})

GC_2110 = Coupling(name = 'GC_2110',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x3x2x3))/4.',
                   order = {'LAM':1,'QED':2})

GC_2111 = Coupling(name = 'GC_2111',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x3x2x3))/4.',
                   order = {'LAM':1,'QED':2})

GC_2112 = Coupling(name = 'GC_2112',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x3x2x3))/2.',
                   order = {'LAM':1,'QED':2})

GC_2113 = Coupling(name = 'GC_2113',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x3x2x3))/2.',
                   order = {'LAM':1,'QED':2})

GC_2114 = Coupling(name = 'GC_2114',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x3x3x1))/4.',
                   order = {'LAM':1,'QED':2})

GC_2115 = Coupling(name = 'GC_2115',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x3x3x1))/4.',
                   order = {'LAM':1,'QED':2})

GC_2116 = Coupling(name = 'GC_2116',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x3x3x1))/2.',
                   order = {'LAM':1,'QED':2})

GC_2117 = Coupling(name = 'GC_2117',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x3x3x1))/2.',
                   order = {'LAM':1,'QED':2})

GC_2118 = Coupling(name = 'GC_2118',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x3x3x2))/4.',
                   order = {'LAM':1,'QED':2})

GC_2119 = Coupling(name = 'GC_2119',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x3x3x2))/4.',
                   order = {'LAM':1,'QED':2})

GC_2120 = Coupling(name = 'GC_2120',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x3x3x2))/2.',
                   order = {'LAM':1,'QED':2})

GC_2121 = Coupling(name = 'GC_2121',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x3x3x2))/2.',
                   order = {'LAM':1,'QED':2})

GC_2122 = Coupling(name = 'GC_2122',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x3x3x3))/4.',
                   order = {'LAM':1,'QED':2})

GC_2123 = Coupling(name = 'GC_2123',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x3x3x3))/4.',
                   order = {'LAM':1,'QED':2})

GC_2124 = Coupling(name = 'GC_2124',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x3x3x3))/2.',
                   order = {'LAM':1,'QED':2})

GC_2125 = Coupling(name = 'GC_2125',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ32x3x3x3))/2.',
                   order = {'LAM':1,'QED':2})

GC_2126 = Coupling(name = 'GC_2126',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x1x1x1))/4.',
                   order = {'LAM':1,'QED':2})

GC_2127 = Coupling(name = 'GC_2127',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x1x1x1))/4.',
                   order = {'LAM':1,'QED':2})

GC_2128 = Coupling(name = 'GC_2128',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x1x1x1))/2.',
                   order = {'LAM':1,'QED':2})

GC_2129 = Coupling(name = 'GC_2129',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x1x1x1))/2.',
                   order = {'LAM':1,'QED':2})

GC_2130 = Coupling(name = 'GC_2130',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x1x1x2))/4.',
                   order = {'LAM':1,'QED':2})

GC_2131 = Coupling(name = 'GC_2131',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x1x1x2))/4.',
                   order = {'LAM':1,'QED':2})

GC_2132 = Coupling(name = 'GC_2132',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x1x1x2))/2.',
                   order = {'LAM':1,'QED':2})

GC_2133 = Coupling(name = 'GC_2133',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x1x1x2))/2.',
                   order = {'LAM':1,'QED':2})

GC_2134 = Coupling(name = 'GC_2134',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x1x1x3))/4.',
                   order = {'LAM':1,'QED':2})

GC_2135 = Coupling(name = 'GC_2135',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x1x1x3))/4.',
                   order = {'LAM':1,'QED':2})

GC_2136 = Coupling(name = 'GC_2136',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x1x1x3))/2.',
                   order = {'LAM':1,'QED':2})

GC_2137 = Coupling(name = 'GC_2137',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x1x1x3))/2.',
                   order = {'LAM':1,'QED':2})

GC_2138 = Coupling(name = 'GC_2138',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x1x2x1))/4.',
                   order = {'LAM':1,'QED':2})

GC_2139 = Coupling(name = 'GC_2139',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x1x2x1))/4.',
                   order = {'LAM':1,'QED':2})

GC_2140 = Coupling(name = 'GC_2140',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x1x2x1))/2.',
                   order = {'LAM':1,'QED':2})

GC_2141 = Coupling(name = 'GC_2141',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x1x2x1))/2.',
                   order = {'LAM':1,'QED':2})

GC_2142 = Coupling(name = 'GC_2142',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x1x2x2))/4.',
                   order = {'LAM':1,'QED':2})

GC_2143 = Coupling(name = 'GC_2143',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x1x2x2))/4.',
                   order = {'LAM':1,'QED':2})

GC_2144 = Coupling(name = 'GC_2144',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x1x2x2))/2.',
                   order = {'LAM':1,'QED':2})

GC_2145 = Coupling(name = 'GC_2145',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x1x2x2))/2.',
                   order = {'LAM':1,'QED':2})

GC_2146 = Coupling(name = 'GC_2146',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x1x2x3))/4.',
                   order = {'LAM':1,'QED':2})

GC_2147 = Coupling(name = 'GC_2147',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x1x2x3))/4.',
                   order = {'LAM':1,'QED':2})

GC_2148 = Coupling(name = 'GC_2148',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x1x2x3))/2.',
                   order = {'LAM':1,'QED':2})

GC_2149 = Coupling(name = 'GC_2149',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x1x2x3))/2.',
                   order = {'LAM':1,'QED':2})

GC_2150 = Coupling(name = 'GC_2150',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x1x3x1))/4.',
                   order = {'LAM':1,'QED':2})

GC_2151 = Coupling(name = 'GC_2151',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x1x3x1))/4.',
                   order = {'LAM':1,'QED':2})

GC_2152 = Coupling(name = 'GC_2152',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x1x3x1))/2.',
                   order = {'LAM':1,'QED':2})

GC_2153 = Coupling(name = 'GC_2153',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x1x3x1))/2.',
                   order = {'LAM':1,'QED':2})

GC_2154 = Coupling(name = 'GC_2154',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x1x3x2))/4.',
                   order = {'LAM':1,'QED':2})

GC_2155 = Coupling(name = 'GC_2155',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x1x3x2))/4.',
                   order = {'LAM':1,'QED':2})

GC_2156 = Coupling(name = 'GC_2156',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x1x3x2))/2.',
                   order = {'LAM':1,'QED':2})

GC_2157 = Coupling(name = 'GC_2157',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x1x3x2))/2.',
                   order = {'LAM':1,'QED':2})

GC_2158 = Coupling(name = 'GC_2158',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x1x3x3))/4.',
                   order = {'LAM':1,'QED':2})

GC_2159 = Coupling(name = 'GC_2159',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x1x3x3))/4.',
                   order = {'LAM':1,'QED':2})

GC_2160 = Coupling(name = 'GC_2160',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x1x3x3))/2.',
                   order = {'LAM':1,'QED':2})

GC_2161 = Coupling(name = 'GC_2161',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x1x3x3))/2.',
                   order = {'LAM':1,'QED':2})

GC_2162 = Coupling(name = 'GC_2162',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x2x1x1))/4.',
                   order = {'LAM':1,'QED':2})

GC_2163 = Coupling(name = 'GC_2163',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x2x1x1))/4.',
                   order = {'LAM':1,'QED':2})

GC_2164 = Coupling(name = 'GC_2164',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x2x1x1))/2.',
                   order = {'LAM':1,'QED':2})

GC_2165 = Coupling(name = 'GC_2165',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x2x1x1))/2.',
                   order = {'LAM':1,'QED':2})

GC_2166 = Coupling(name = 'GC_2166',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x2x1x2))/4.',
                   order = {'LAM':1,'QED':2})

GC_2167 = Coupling(name = 'GC_2167',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x2x1x2))/4.',
                   order = {'LAM':1,'QED':2})

GC_2168 = Coupling(name = 'GC_2168',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x2x1x2))/2.',
                   order = {'LAM':1,'QED':2})

GC_2169 = Coupling(name = 'GC_2169',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x2x1x2))/2.',
                   order = {'LAM':1,'QED':2})

GC_2170 = Coupling(name = 'GC_2170',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x2x1x3))/4.',
                   order = {'LAM':1,'QED':2})

GC_2171 = Coupling(name = 'GC_2171',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x2x1x3))/4.',
                   order = {'LAM':1,'QED':2})

GC_2172 = Coupling(name = 'GC_2172',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x2x1x3))/2.',
                   order = {'LAM':1,'QED':2})

GC_2173 = Coupling(name = 'GC_2173',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x2x1x3))/2.',
                   order = {'LAM':1,'QED':2})

GC_2174 = Coupling(name = 'GC_2174',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x2x2x1))/4.',
                   order = {'LAM':1,'QED':2})

GC_2175 = Coupling(name = 'GC_2175',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x2x2x1))/4.',
                   order = {'LAM':1,'QED':2})

GC_2176 = Coupling(name = 'GC_2176',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x2x2x1))/2.',
                   order = {'LAM':1,'QED':2})

GC_2177 = Coupling(name = 'GC_2177',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x2x2x1))/2.',
                   order = {'LAM':1,'QED':2})

GC_2178 = Coupling(name = 'GC_2178',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x2x2x2))/4.',
                   order = {'LAM':1,'QED':2})

GC_2179 = Coupling(name = 'GC_2179',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x2x2x2))/4.',
                   order = {'LAM':1,'QED':2})

GC_2180 = Coupling(name = 'GC_2180',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x2x2x2))/2.',
                   order = {'LAM':1,'QED':2})

GC_2181 = Coupling(name = 'GC_2181',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x2x2x2))/2.',
                   order = {'LAM':1,'QED':2})

GC_2182 = Coupling(name = 'GC_2182',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x2x2x3))/4.',
                   order = {'LAM':1,'QED':2})

GC_2183 = Coupling(name = 'GC_2183',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x2x2x3))/4.',
                   order = {'LAM':1,'QED':2})

GC_2184 = Coupling(name = 'GC_2184',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x2x2x3))/2.',
                   order = {'LAM':1,'QED':2})

GC_2185 = Coupling(name = 'GC_2185',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x2x2x3))/2.',
                   order = {'LAM':1,'QED':2})

GC_2186 = Coupling(name = 'GC_2186',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x2x3x1))/4.',
                   order = {'LAM':1,'QED':2})

GC_2187 = Coupling(name = 'GC_2187',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x2x3x1))/4.',
                   order = {'LAM':1,'QED':2})

GC_2188 = Coupling(name = 'GC_2188',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x2x3x1))/2.',
                   order = {'LAM':1,'QED':2})

GC_2189 = Coupling(name = 'GC_2189',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x2x3x1))/2.',
                   order = {'LAM':1,'QED':2})

GC_2190 = Coupling(name = 'GC_2190',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x2x3x2))/4.',
                   order = {'LAM':1,'QED':2})

GC_2191 = Coupling(name = 'GC_2191',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x2x3x2))/4.',
                   order = {'LAM':1,'QED':2})

GC_2192 = Coupling(name = 'GC_2192',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x2x3x2))/2.',
                   order = {'LAM':1,'QED':2})

GC_2193 = Coupling(name = 'GC_2193',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x2x3x2))/2.',
                   order = {'LAM':1,'QED':2})

GC_2194 = Coupling(name = 'GC_2194',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x2x3x3))/4.',
                   order = {'LAM':1,'QED':2})

GC_2195 = Coupling(name = 'GC_2195',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x2x3x3))/4.',
                   order = {'LAM':1,'QED':2})

GC_2196 = Coupling(name = 'GC_2196',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x2x3x3))/2.',
                   order = {'LAM':1,'QED':2})

GC_2197 = Coupling(name = 'GC_2197',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x2x3x3))/2.',
                   order = {'LAM':1,'QED':2})

GC_2198 = Coupling(name = 'GC_2198',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x3x1x1))/4.',
                   order = {'LAM':1,'QED':2})

GC_2199 = Coupling(name = 'GC_2199',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x3x1x1))/4.',
                   order = {'LAM':1,'QED':2})

GC_2200 = Coupling(name = 'GC_2200',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x3x1x1))/2.',
                   order = {'LAM':1,'QED':2})

GC_2201 = Coupling(name = 'GC_2201',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x3x1x1))/2.',
                   order = {'LAM':1,'QED':2})

GC_2202 = Coupling(name = 'GC_2202',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x3x1x2))/4.',
                   order = {'LAM':1,'QED':2})

GC_2203 = Coupling(name = 'GC_2203',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x3x1x2))/4.',
                   order = {'LAM':1,'QED':2})

GC_2204 = Coupling(name = 'GC_2204',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x3x1x2))/2.',
                   order = {'LAM':1,'QED':2})

GC_2205 = Coupling(name = 'GC_2205',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x3x1x2))/2.',
                   order = {'LAM':1,'QED':2})

GC_2206 = Coupling(name = 'GC_2206',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x3x1x3))/4.',
                   order = {'LAM':1,'QED':2})

GC_2207 = Coupling(name = 'GC_2207',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x3x1x3))/4.',
                   order = {'LAM':1,'QED':2})

GC_2208 = Coupling(name = 'GC_2208',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x3x1x3))/2.',
                   order = {'LAM':1,'QED':2})

GC_2209 = Coupling(name = 'GC_2209',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x3x1x3))/2.',
                   order = {'LAM':1,'QED':2})

GC_2210 = Coupling(name = 'GC_2210',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x3x2x1))/4.',
                   order = {'LAM':1,'QED':2})

GC_2211 = Coupling(name = 'GC_2211',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x3x2x1))/4.',
                   order = {'LAM':1,'QED':2})

GC_2212 = Coupling(name = 'GC_2212',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x3x2x1))/2.',
                   order = {'LAM':1,'QED':2})

GC_2213 = Coupling(name = 'GC_2213',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x3x2x1))/2.',
                   order = {'LAM':1,'QED':2})

GC_2214 = Coupling(name = 'GC_2214',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x3x2x2))/4.',
                   order = {'LAM':1,'QED':2})

GC_2215 = Coupling(name = 'GC_2215',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x3x2x2))/4.',
                   order = {'LAM':1,'QED':2})

GC_2216 = Coupling(name = 'GC_2216',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x3x2x2))/2.',
                   order = {'LAM':1,'QED':2})

GC_2217 = Coupling(name = 'GC_2217',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x3x2x2))/2.',
                   order = {'LAM':1,'QED':2})

GC_2218 = Coupling(name = 'GC_2218',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x3x2x3))/4.',
                   order = {'LAM':1,'QED':2})

GC_2219 = Coupling(name = 'GC_2219',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x3x2x3))/4.',
                   order = {'LAM':1,'QED':2})

GC_2220 = Coupling(name = 'GC_2220',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x3x2x3))/2.',
                   order = {'LAM':1,'QED':2})

GC_2221 = Coupling(name = 'GC_2221',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x3x2x3))/2.',
                   order = {'LAM':1,'QED':2})

GC_2222 = Coupling(name = 'GC_2222',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x3x3x1))/4.',
                   order = {'LAM':1,'QED':2})

GC_2223 = Coupling(name = 'GC_2223',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x3x3x1))/4.',
                   order = {'LAM':1,'QED':2})

GC_2224 = Coupling(name = 'GC_2224',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x3x3x1))/2.',
                   order = {'LAM':1,'QED':2})

GC_2225 = Coupling(name = 'GC_2225',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x3x3x1))/2.',
                   order = {'LAM':1,'QED':2})

GC_2226 = Coupling(name = 'GC_2226',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x3x3x2))/4.',
                   order = {'LAM':1,'QED':2})

GC_2227 = Coupling(name = 'GC_2227',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x3x3x2))/4.',
                   order = {'LAM':1,'QED':2})

GC_2228 = Coupling(name = 'GC_2228',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x3x3x2))/2.',
                   order = {'LAM':1,'QED':2})

GC_2229 = Coupling(name = 'GC_2229',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x3x3x2))/2.',
                   order = {'LAM':1,'QED':2})

GC_2230 = Coupling(name = 'GC_2230',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x3x3x3))/4.',
                   order = {'LAM':1,'QED':2})

GC_2231 = Coupling(name = 'GC_2231',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x3x3x3))/4.',
                   order = {'LAM':1,'QED':2})

GC_2232 = Coupling(name = 'GC_2232',
                   value = '-(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x3x3x3))/2.',
                   order = {'LAM':1,'QED':2})

GC_2233 = Coupling(name = 'GC_2233',
                   value = '(complex(0,1)*Lam*qeddim**2*complexconjugate(Clequ33x3x3x3))/2.',
                   order = {'LAM':1,'QED':2})

