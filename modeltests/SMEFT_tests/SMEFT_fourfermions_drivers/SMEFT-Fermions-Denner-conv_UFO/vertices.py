# This file was automatically created by FeynRules 2.3.29
# Mathematica version: 12.0.0 for Linux x86 (64-bit) (April 7, 2019)
# Date: Wed 12 Aug 2020 11:22:41


from .object_library import all_vertices, Vertex
from . import particles as P
from . import couplings as C
from . import lorentz as L


V_1 = Vertex(name = 'V_1',
             particles = [ P.G0, P.G0, P.G0, P.G0 ],
             color = [ '1' ],
             lorentz = [ L.SSSS1, L.SSSS10, L.SSSS11, L.SSSS2, L.SSSS3, L.SSSS4, L.SSSS5, L.SSSS6, L.SSSS7, L.SSSS8, L.SSSS9 ],
             couplings = {(0,0):C.GC_1543,(0,3):C.GC_1018,(0,4):C.GC_1326,(0,6):C.GC_1326,(0,9):C.GC_1326,(0,5):C.GC_1018,(0,7):C.GC_1326,(0,10):C.GC_1326,(0,8):C.GC_1018,(0,1):C.GC_1326,(0,2):C.GC_1018})

V_2 = Vertex(name = 'V_2',
             particles = [ P.G0, P.G0, P.G0, P.G0 ],
             color = [ '1' ],
             lorentz = [ L.SSSS1 ],
             couplings = {(0,0):C.GC_1453})

V_3 = Vertex(name = 'V_3',
             particles = [ P.G0, P.G0, P.G__minus__, P.G__plus__ ],
             color = [ '1' ],
             lorentz = [ L.SSSS1, L.SSSS10, L.SSSS11, L.SSSS2, L.SSSS3, L.SSSS4, L.SSSS5, L.SSSS6, L.SSSS7, L.SSSS8, L.SSSS9 ],
             couplings = {(0,0):C.GC_1539,(0,3):C.GC_1016,(0,4):C.GC_1017,(0,6):C.GC_1012,(0,9):C.GC_1012,(0,5):C.GC_1016,(0,7):C.GC_1012,(0,10):C.GC_1012,(0,8):C.GC_1016,(0,1):C.GC_1017,(0,2):C.GC_1016})

V_4 = Vertex(name = 'V_4',
             particles = [ P.G0, P.G0, P.G__minus__, P.G__plus__ ],
             color = [ '1' ],
             lorentz = [ L.SSSS1 ],
             couplings = {(0,0):C.GC_1451})

V_5 = Vertex(name = 'V_5',
             particles = [ P.G0, P.G0, P.H, P.H ],
             color = [ '1' ],
             lorentz = [ L.SSSS1, L.SSSS10, L.SSSS11, L.SSSS2, L.SSSS3, L.SSSS4, L.SSSS7 ],
             couplings = {(0,0):C.GC_1542,(0,3):C.GC_1016,(0,4):C.GC_1326,(0,5):C.GC_1016,(0,6):C.GC_1016,(0,1):C.GC_1326,(0,2):C.GC_1016})

V_6 = Vertex(name = 'V_6',
             particles = [ P.G0, P.G0, P.H, P.H ],
             color = [ '1' ],
             lorentz = [ L.SSSS1 ],
             couplings = {(0,0):C.GC_1451})

V_7 = Vertex(name = 'V_7',
             particles = [ P.G__minus__, P.G__plus__, P.H, P.H ],
             color = [ '1' ],
             lorentz = [ L.SSSS1, L.SSSS10, L.SSSS11, L.SSSS2, L.SSSS3, L.SSSS4, L.SSSS5, L.SSSS6, L.SSSS7, L.SSSS8, L.SSSS9 ],
             couplings = {(0,0):C.GC_1541,(0,3):C.GC_1016,(0,4):C.GC_1017,(0,6):C.GC_1012,(0,9):C.GC_1012,(0,5):C.GC_1016,(0,7):C.GC_1012,(0,10):C.GC_1012,(0,8):C.GC_1016,(0,1):C.GC_1017,(0,2):C.GC_1016})

V_8 = Vertex(name = 'V_8',
             particles = [ P.G__minus__, P.G__plus__, P.H, P.H ],
             color = [ '1' ],
             lorentz = [ L.SSSS1 ],
             couplings = {(0,0):C.GC_1451})

V_9 = Vertex(name = 'V_9',
             particles = [ P.H, P.H, P.H, P.H ],
             color = [ '1' ],
             lorentz = [ L.SSSS1, L.SSSS10, L.SSSS11, L.SSSS2, L.SSSS3, L.SSSS4, L.SSSS5, L.SSSS6, L.SSSS7, L.SSSS8, L.SSSS9 ],
             couplings = {(0,0):C.GC_1544,(0,3):C.GC_1018,(0,4):C.GC_1326,(0,6):C.GC_1326,(0,9):C.GC_1326,(0,5):C.GC_1018,(0,7):C.GC_1326,(0,10):C.GC_1326,(0,8):C.GC_1018,(0,1):C.GC_1326,(0,2):C.GC_1018})

V_10 = Vertex(name = 'V_10',
              particles = [ P.H, P.H, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.SSSS1 ],
              couplings = {(0,0):C.GC_1453})

V_11 = Vertex(name = 'V_11',
              particles = [ P.G0, P.G0, P.G0, P.G0, P.G0, P.G0 ],
              color = [ '1' ],
              lorentz = [ L.SSSSSS1 ],
              couplings = {(0,0):C.GC_1127})

V_12 = Vertex(name = 'V_12',
              particles = [ P.G0, P.G0, P.G0, P.G0, P.G__minus__, P.G__plus__ ],
              color = [ '1' ],
              lorentz = [ L.SSSSSS1 ],
              couplings = {(0,0):C.GC_1125})

V_13 = Vertex(name = 'V_13',
              particles = [ P.G0, P.G0, P.G__minus__, P.G__minus__, P.G__plus__, P.G__plus__ ],
              color = [ '1' ],
              lorentz = [ L.SSSSSS1 ],
              couplings = {(0,0):C.GC_1124})

V_14 = Vertex(name = 'V_14',
              particles = [ P.G__minus__, P.G__minus__, P.G__minus__, P.G__plus__, P.G__plus__, P.G__plus__ ],
              color = [ '1' ],
              lorentz = [ L.SSSSSS1 ],
              couplings = {(0,0):C.GC_1126})

V_15 = Vertex(name = 'V_15',
              particles = [ P.G0, P.G0, P.G0, P.G0, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.SSSSSS1 ],
              couplings = {(0,0):C.GC_1125})

V_16 = Vertex(name = 'V_16',
              particles = [ P.G0, P.G0, P.G__minus__, P.G__plus__, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.SSSSSS1 ],
              couplings = {(0,0):C.GC_1123})

V_17 = Vertex(name = 'V_17',
              particles = [ P.G__minus__, P.G__minus__, P.G__plus__, P.G__plus__, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.SSSSSS1 ],
              couplings = {(0,0):C.GC_1124})

V_18 = Vertex(name = 'V_18',
              particles = [ P.G0, P.G0, P.H, P.H, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.SSSSSS1 ],
              couplings = {(0,0):C.GC_1125})

V_19 = Vertex(name = 'V_19',
              particles = [ P.G__minus__, P.G__plus__, P.H, P.H, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.SSSSSS1 ],
              couplings = {(0,0):C.GC_1125})

V_20 = Vertex(name = 'V_20',
              particles = [ P.H, P.H, P.H, P.H, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.SSSSSS1 ],
              couplings = {(0,0):C.GC_1127})

V_21 = Vertex(name = 'V_21',
              particles = [ P.G__minus__, P.G__minus__, P.G__plus__, P.G__plus__ ],
              color = [ '1' ],
              lorentz = [ L.SSSS1, L.SSSS11, L.SSSS2, L.SSSS4, L.SSSS5, L.SSSS6, L.SSSS7, L.SSSS8, L.SSSS9 ],
              couplings = {(0,0):C.GC_1452,(0,2):C.GC_1017,(0,4):C.GC_1326,(0,7):C.GC_1326,(0,3):C.GC_1017,(0,5):C.GC_1326,(0,8):C.GC_1326,(0,6):C.GC_1017,(0,1):C.GC_1017})

V_22 = Vertex(name = 'V_22',
              particles = [ P.G__minus__, P.G__minus__, P.G__plus__, P.G__plus__ ],
              color = [ '1' ],
              lorentz = [ L.SSSS1 ],
              couplings = {(0,0):C.GC_1540})

V_23 = Vertex(name = 'V_23',
              particles = [ P.G0, P.G0, P.H ],
              color = [ '1' ],
              lorentz = [ L.SSS1, L.SSS2, L.SSS3, L.SSS4, L.SSS7 ],
              couplings = {(0,0):C.GC_1454,(0,1):C.GC_1462,(0,2):C.GC_1512,(0,3):C.GC_1462,(0,4):C.GC_1462})

V_24 = Vertex(name = 'V_24',
              particles = [ P.G0, P.G0, P.H ],
              color = [ '1' ],
              lorentz = [ L.SSS1 ],
              couplings = {(0,0):C.GC_1600})

V_25 = Vertex(name = 'V_25',
              particles = [ P.G__minus__, P.G__plus__, P.H ],
              color = [ '1' ],
              lorentz = [ L.SSS1, L.SSS2, L.SSS3, L.SSS4, L.SSS5, L.SSS6, L.SSS7 ],
              couplings = {(0,0):C.GC_1454,(0,1):C.GC_1462,(0,2):C.GC_1463,(0,4):C.GC_1458,(0,3):C.GC_1462,(0,5):C.GC_1458,(0,6):C.GC_1462})

V_26 = Vertex(name = 'V_26',
              particles = [ P.G__minus__, P.G__plus__, P.H ],
              color = [ '1' ],
              lorentz = [ L.SSS1 ],
              couplings = {(0,0):C.GC_1599})

V_27 = Vertex(name = 'V_27',
              particles = [ P.H, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.SSS1, L.SSS2, L.SSS3, L.SSS4, L.SSS5, L.SSS6, L.SSS7 ],
              couplings = {(0,0):C.GC_1455,(0,1):C.GC_1464,(0,2):C.GC_1512,(0,4):C.GC_1512,(0,3):C.GC_1464,(0,5):C.GC_1512,(0,6):C.GC_1464})

V_28 = Vertex(name = 'V_28',
              particles = [ P.H, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.SSS1 ],
              couplings = {(0,0):C.GC_1601})

V_29 = Vertex(name = 'V_29',
              particles = [ P.G0, P.G0, P.G0, P.G0, P.H ],
              color = [ '1' ],
              lorentz = [ L.SSSSS1 ],
              couplings = {(0,0):C.GC_1482})

V_30 = Vertex(name = 'V_30',
              particles = [ P.G0, P.G0, P.G__minus__, P.G__plus__, P.H ],
              color = [ '1' ],
              lorentz = [ L.SSSSS1 ],
              couplings = {(0,0):C.GC_1480})

V_31 = Vertex(name = 'V_31',
              particles = [ P.G__minus__, P.G__minus__, P.G__plus__, P.G__plus__, P.H ],
              color = [ '1' ],
              lorentz = [ L.SSSSS1 ],
              couplings = {(0,0):C.GC_1481})

V_32 = Vertex(name = 'V_32',
              particles = [ P.G0, P.G0, P.H, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.SSSSS1 ],
              couplings = {(0,0):C.GC_1482})

V_33 = Vertex(name = 'V_33',
              particles = [ P.G__minus__, P.G__plus__, P.H, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.SSSSS1 ],
              couplings = {(0,0):C.GC_1482})

V_34 = Vertex(name = 'V_34',
              particles = [ P.H, P.H, P.H, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.SSSSS1 ],
              couplings = {(0,0):C.GC_1483})

V_35 = Vertex(name = 'V_35',
              particles = [ P.A, P.A, P.G__minus__, P.G__plus__ ],
              color = [ '1' ],
              lorentz = [ L.VVSS1, L.VVSS2, L.VVSS3 ],
              couplings = {(0,0):C.GC_1427,(0,1):C.GC_7,(0,2):C.GC_1424})

V_36 = Vertex(name = 'V_36',
              particles = [ P.A, P.A, P.G__minus__, P.G__plus__ ],
              color = [ '1' ],
              lorentz = [ L.VVSS2 ],
              couplings = {(0,0):C.GC_1587})

V_37 = Vertex(name = 'V_37',
              particles = [ P.A, P.A, P.G__minus__, P.G__minus__, P.G__plus__, P.G__plus__ ],
              color = [ '1' ],
              lorentz = [ L.VVSSSS1 ],
              couplings = {(0,0):C.GC_1116})

V_38 = Vertex(name = 'V_38',
              particles = [ P.A, P.G0, P.G__minus__, P.G__plus__, P.H ],
              color = [ '1' ],
              lorentz = [ L.VSSSS1, L.VSSSS4 ],
              couplings = {(0,0):C.GC_1106,(0,1):C.GC_1109})

V_39 = Vertex(name = 'V_39',
              particles = [ P.A, P.G0, P.G__minus__, P.G__plus__ ],
              color = [ '1' ],
              lorentz = [ L.VSSS1 ],
              couplings = {(0,0):C.GC_1469})

V_40 = Vertex(name = 'V_40',
              particles = [ P.A, P.G0, P.H ],
              color = [ '1' ],
              lorentz = [ L.VSS1, L.VSS2 ],
              couplings = {(0,0):C.GC_1545,(0,1):C.GC_1549})

V_41 = Vertex(name = 'V_41',
              particles = [ P.A, P.G__minus__, P.G__plus__ ],
              color = [ '1' ],
              lorentz = [ L.VSS1, L.VSS2 ],
              couplings = {(0,0):C.GC_4,(0,1):C.GC_3})

V_42 = Vertex(name = 'V_42',
              particles = [ P.A, P.G__minus__, P.G__plus__ ],
              color = [ '1' ],
              lorentz = [ L.VSS1, L.VSS2 ],
              couplings = {(0,0):C.GC_1575,(0,1):C.GC_1577})

V_43 = Vertex(name = 'V_43',
              particles = [ P.A, P.G__minus__, P.G__minus__, P.G__plus__, P.G__plus__ ],
              color = [ '1' ],
              lorentz = [ L.VSSSS1, L.VSSSS2, L.VSSSS3, L.VSSSS4 ],
              couplings = {(0,0):C.GC_1108,(0,1):C.GC_1108,(0,2):C.GC_1107,(0,3):C.GC_1107})

V_44 = Vertex(name = 'V_44',
              particles = [ P.G0, P.G__minus__, P.G__plus__, P.H ],
              color = [ '1' ],
              lorentz = [ L.SSSS10, L.SSSS3, L.SSSS5, L.SSSS9 ],
              couplings = {(0,1):C.GC_1011,(0,2):C.GC_1013,(0,3):C.GC_1013,(0,0):C.GC_1011})

V_45 = Vertex(name = 'V_45',
              particles = [ P.G0, P.G__minus__, P.G__plus__ ],
              color = [ '1' ],
              lorentz = [ L.SSS3, L.SSS5 ],
              couplings = {(0,0):C.GC_1457,(0,1):C.GC_1459})

V_46 = Vertex(name = 'V_46',
              particles = [ P.A, P.A, P.G0, P.G0 ],
              color = [ '1' ],
              lorentz = [ L.VVSS1, L.VVSS3 ],
              couplings = {(0,0):C.GC_1426,(0,1):C.GC_1425})

V_47 = Vertex(name = 'V_47',
              particles = [ P.A, P.A, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVSS1, L.VVSS3 ],
              couplings = {(0,0):C.GC_1426,(0,1):C.GC_1425})

V_48 = Vertex(name = 'V_48',
              particles = [ P.A, P.A, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVS1, L.VVS3 ],
              couplings = {(0,0):C.GC_1533,(0,1):C.GC_1532})

V_49 = Vertex(name = 'V_49',
              particles = [ P.g, P.g, P.G0, P.G0 ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.VVSS1, L.VVSS3 ],
              couplings = {(0,0):C.GC_1015,(0,1):C.GC_1014})

V_50 = Vertex(name = 'V_50',
              particles = [ P.g, P.g, P.G__minus__, P.G__plus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.VVSS1, L.VVSS3 ],
              couplings = {(0,0):C.GC_1015,(0,1):C.GC_1014})

V_51 = Vertex(name = 'V_51',
              particles = [ P.g, P.g, P.H, P.H ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.VVSS1, L.VVSS3 ],
              couplings = {(0,0):C.GC_1015,(0,1):C.GC_1014})

V_52 = Vertex(name = 'V_52',
              particles = [ P.g, P.g, P.H ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.VVS1, L.VVS3 ],
              couplings = {(0,0):C.GC_1461,(0,1):C.GC_1460})

V_53 = Vertex(name = 'V_53',
              particles = [ P.A, P.W__minus__, P.G0, P.G__plus__ ],
              color = [ '1' ],
              lorentz = [ L.VVSS1, L.VVSS2, L.VVSS3 ],
              couplings = {(0,0):C.GC_1105,(0,1):C.GC_1367,(0,2):C.GC_1102})

V_54 = Vertex(name = 'V_54',
              particles = [ P.A, P.W__minus__, P.G0, P.G__plus__ ],
              color = [ '1' ],
              lorentz = [ L.VVSS2 ],
              couplings = {(0,0):C.GC_1553})

V_55 = Vertex(name = 'V_55',
              particles = [ P.A, P.W__minus__, P.G__plus__, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVSS1, L.VVSS2, L.VVSS3 ],
              couplings = {(0,0):C.GC_1103,(0,1):C.GC_1366,(0,2):C.GC_1104})

V_56 = Vertex(name = 'V_56',
              particles = [ P.A, P.W__minus__, P.G__plus__, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVSS2 ],
              couplings = {(0,0):C.GC_1579})

V_57 = Vertex(name = 'V_57',
              particles = [ P.A, P.W__minus__, P.G__plus__ ],
              color = [ '1' ],
              lorentz = [ L.VVS1, L.VVS2, L.VVS3 ],
              couplings = {(0,0):C.GC_1467,(0,1):C.GC_1493,(0,2):C.GC_1468})

V_58 = Vertex(name = 'V_58',
              particles = [ P.A, P.W__minus__, P.G__plus__ ],
              color = [ '1' ],
              lorentz = [ L.VVS2 ],
              couplings = {(0,0):C.GC_1603})

V_59 = Vertex(name = 'V_59',
              particles = [ P.A, P.W__plus__, P.G0, P.G__minus__ ],
              color = [ '1' ],
              lorentz = [ L.VVSS1, L.VVSS2, L.VVSS3 ],
              couplings = {(0,0):C.GC_1102,(0,1):C.GC_1365,(0,2):C.GC_1105})

V_60 = Vertex(name = 'V_60',
              particles = [ P.A, P.W__plus__, P.G0, P.G__minus__ ],
              color = [ '1' ],
              lorentz = [ L.VVSS2 ],
              couplings = {(0,0):C.GC_1554})

V_61 = Vertex(name = 'V_61',
              particles = [ P.A, P.W__plus__, P.G__minus__, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVSS1, L.VVSS2, L.VVSS3 ],
              couplings = {(0,0):C.GC_1103,(0,1):C.GC_1366,(0,2):C.GC_1104})

V_62 = Vertex(name = 'V_62',
              particles = [ P.A, P.W__plus__, P.G__minus__, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVSS2 ],
              couplings = {(0,0):C.GC_1579})

V_63 = Vertex(name = 'V_63',
              particles = [ P.A, P.W__plus__, P.G__minus__ ],
              color = [ '1' ],
              lorentz = [ L.VVS1, L.VVS2, L.VVS3 ],
              couplings = {(0,0):C.GC_1467,(0,1):C.GC_1493,(0,2):C.GC_1468})

V_64 = Vertex(name = 'V_64',
              particles = [ P.A, P.W__plus__, P.G__minus__ ],
              color = [ '1' ],
              lorentz = [ L.VVS2 ],
              couplings = {(0,0):C.GC_1603})

V_65 = Vertex(name = 'V_65',
              particles = [ P.W__minus__, P.W__plus__, P.G0, P.G0 ],
              color = [ '1' ],
              lorentz = [ L.VVSS1, L.VVSS2, L.VVSS3 ],
              couplings = {(0,0):C.GC_1020,(0,1):C.GC_1344,(0,2):C.GC_1019})

V_66 = Vertex(name = 'V_66',
              particles = [ P.W__minus__, P.W__plus__, P.G0, P.G0 ],
              color = [ '1' ],
              lorentz = [ L.VVSS2 ],
              couplings = {(0,0):C.GC_1550})

V_67 = Vertex(name = 'V_67',
              particles = [ P.W__minus__, P.W__plus__, P.G__minus__, P.G__plus__ ],
              color = [ '1' ],
              lorentz = [ L.VVSS1, L.VVSS2, L.VVSS3 ],
              couplings = {(0,0):C.GC_1020,(0,1):C.GC_1344,(0,2):C.GC_1019})

V_68 = Vertex(name = 'V_68',
              particles = [ P.W__minus__, P.W__plus__, P.G__minus__, P.G__plus__ ],
              color = [ '1' ],
              lorentz = [ L.VVSS2 ],
              couplings = {(0,0):C.GC_1551})

V_69 = Vertex(name = 'V_69',
              particles = [ P.W__minus__, P.W__plus__, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVSS1, L.VVSS2, L.VVSS3 ],
              couplings = {(0,0):C.GC_1020,(0,1):C.GC_1344,(0,2):C.GC_1019})

V_70 = Vertex(name = 'V_70',
              particles = [ P.W__minus__, P.W__plus__, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVSS2 ],
              couplings = {(0,0):C.GC_1552})

V_71 = Vertex(name = 'V_71',
              particles = [ P.W__minus__, P.W__plus__, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVS1, L.VVS2, L.VVS3 ],
              couplings = {(0,0):C.GC_1466,(0,1):C.GC_1484,(0,2):C.GC_1465})

V_72 = Vertex(name = 'V_72',
              particles = [ P.W__minus__, P.W__plus__, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVS2 ],
              couplings = {(0,0):C.GC_1602})

V_73 = Vertex(name = 'V_73',
              particles = [ P.A, P.Z, P.G0, P.G0 ],
              color = [ '1' ],
              lorentz = [ L.VVSS1, L.VVSS2, L.VVSS3 ],
              couplings = {(0,0):C.GC_1428,(0,1):C.GC_1594,(0,2):C.GC_1431})

V_74 = Vertex(name = 'V_74',
              particles = [ P.A, P.Z, P.G__minus__, P.G__plus__ ],
              color = [ '1' ],
              lorentz = [ L.VVSS1, L.VVSS2, L.VVSS3 ],
              couplings = {(0,0):C.GC_1430,(0,1):C.GC_1405,(0,2):C.GC_1429})

V_75 = Vertex(name = 'V_75',
              particles = [ P.A, P.Z, P.G__minus__, P.G__plus__ ],
              color = [ '1' ],
              lorentz = [ L.VVSS2 ],
              couplings = {(0,0):C.GC_1595})

V_76 = Vertex(name = 'V_76',
              particles = [ P.A, P.Z, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVSS1, L.VVSS2, L.VVSS3 ],
              couplings = {(0,0):C.GC_1428,(0,1):C.GC_1594,(0,2):C.GC_1431})

V_77 = Vertex(name = 'V_77',
              particles = [ P.A, P.Z, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVS1, L.VVS2, L.VVS3 ],
              couplings = {(0,0):C.GC_1534,(0,1):C.GC_1605,(0,2):C.GC_1535})

V_78 = Vertex(name = 'V_78',
              particles = [ P.W__minus__, P.Z, P.G0, P.G__plus__ ],
              color = [ '1' ],
              lorentz = [ L.VVSS1, L.VVSS2, L.VVSS3 ],
              couplings = {(0,0):C.GC_1393,(0,1):C.GC_10,(0,2):C.GC_1390})

V_79 = Vertex(name = 'V_79',
              particles = [ P.W__minus__, P.Z, P.G0, P.G__plus__ ],
              color = [ '1' ],
              lorentz = [ L.VVSS2 ],
              couplings = {(0,0):C.GC_1580})

V_80 = Vertex(name = 'V_80',
              particles = [ P.W__minus__, P.Z, P.G__plus__, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVSS1, L.VVSS2, L.VVSS3 ],
              couplings = {(0,0):C.GC_1391,(0,1):C.GC_9,(0,2):C.GC_1392})

V_81 = Vertex(name = 'V_81',
              particles = [ P.W__minus__, P.Z, P.G__plus__, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVSS2 ],
              couplings = {(0,0):C.GC_1581})

V_82 = Vertex(name = 'V_82',
              particles = [ P.W__minus__, P.Z, P.G__plus__ ],
              color = [ '1' ],
              lorentz = [ L.VVS1, L.VVS2, L.VVS3 ],
              couplings = {(0,0):C.GC_1507,(0,1):C.GC_1456,(0,2):C.GC_1508})

V_83 = Vertex(name = 'V_83',
              particles = [ P.W__minus__, P.Z, P.G__plus__ ],
              color = [ '1' ],
              lorentz = [ L.VVS2 ],
              couplings = {(0,0):C.GC_1604})

V_84 = Vertex(name = 'V_84',
              particles = [ P.W__plus__, P.Z, P.G0, P.G__minus__ ],
              color = [ '1' ],
              lorentz = [ L.VVSS1, L.VVSS2, L.VVSS3 ],
              couplings = {(0,0):C.GC_1390,(0,1):C.GC_8,(0,2):C.GC_1393})

V_85 = Vertex(name = 'V_85',
              particles = [ P.W__plus__, P.Z, P.G0, P.G__minus__ ],
              color = [ '1' ],
              lorentz = [ L.VVSS2 ],
              couplings = {(0,0):C.GC_1582})

V_86 = Vertex(name = 'V_86',
              particles = [ P.W__plus__, P.Z, P.G__minus__, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVSS1, L.VVSS2, L.VVSS3 ],
              couplings = {(0,0):C.GC_1391,(0,1):C.GC_9,(0,2):C.GC_1392})

V_87 = Vertex(name = 'V_87',
              particles = [ P.W__plus__, P.Z, P.G__minus__, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVSS2 ],
              couplings = {(0,0):C.GC_1581})

V_88 = Vertex(name = 'V_88',
              particles = [ P.W__plus__, P.Z, P.G__minus__ ],
              color = [ '1' ],
              lorentz = [ L.VVS1, L.VVS2, L.VVS3 ],
              couplings = {(0,0):C.GC_1507,(0,1):C.GC_1456,(0,2):C.GC_1508})

V_89 = Vertex(name = 'V_89',
              particles = [ P.W__plus__, P.Z, P.G__minus__ ],
              color = [ '1' ],
              lorentz = [ L.VVS2 ],
              couplings = {(0,0):C.GC_1604})

V_90 = Vertex(name = 'V_90',
              particles = [ P.Z, P.Z, P.G0, P.G0 ],
              color = [ '1' ],
              lorentz = [ L.VVSS1, L.VVSS2, L.VVSS3 ],
              couplings = {(0,0):C.GC_1423,(0,1):C.GC_1419,(0,2):C.GC_1420})

V_91 = Vertex(name = 'V_91',
              particles = [ P.Z, P.Z, P.G0, P.G0 ],
              color = [ '1' ],
              lorentz = [ L.VVSS2 ],
              couplings = {(0,0):C.GC_1596})

V_92 = Vertex(name = 'V_92',
              particles = [ P.Z, P.Z, P.G__minus__, P.G__plus__ ],
              color = [ '1' ],
              lorentz = [ L.VVSS1, L.VVSS2, L.VVSS3 ],
              couplings = {(0,0):C.GC_1422,(0,1):C.GC_1418,(0,2):C.GC_1421})

V_93 = Vertex(name = 'V_93',
              particles = [ P.Z, P.Z, P.G__minus__, P.G__plus__ ],
              color = [ '1' ],
              lorentz = [ L.VVSS2 ],
              couplings = {(0,0):C.GC_1597})

V_94 = Vertex(name = 'V_94',
              particles = [ P.Z, P.Z, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVSS1, L.VVSS2, L.VVSS3 ],
              couplings = {(0,0):C.GC_1423,(0,1):C.GC_1419,(0,2):C.GC_1420})

V_95 = Vertex(name = 'V_95',
              particles = [ P.Z, P.Z, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVSS2 ],
              couplings = {(0,0):C.GC_1598})

V_96 = Vertex(name = 'V_96',
              particles = [ P.Z, P.Z, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVS1, L.VVS2, L.VVS3 ],
              couplings = {(0,0):C.GC_1531,(0,1):C.GC_1529,(0,2):C.GC_1530})

V_97 = Vertex(name = 'V_97',
              particles = [ P.Z, P.Z, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVS2 ],
              couplings = {(0,0):C.GC_1606})

V_98 = Vertex(name = 'V_98',
              particles = [ P.ghG, P.ghG__tilde__, P.g ],
              color = [ 'f(1,2,3)' ],
              lorentz = [ L.UUV1, L.UUV2 ],
              couplings = {(0,0):C.GC_11,(0,1):C.GC_11})

V_99 = Vertex(name = 'V_99',
              particles = [ P.g, P.g, P.g ],
              color = [ 'f(1,2,3)' ],
              lorentz = [ L.VVV1, L.VVV2, L.VVV3, L.VVV4, L.VVV5, L.VVV6 ],
              couplings = {(0,0):C.GC_11,(0,1):C.GC_13,(0,2):C.GC_13,(0,3):C.GC_11,(0,4):C.GC_11,(0,5):C.GC_13})

V_100 = Vertex(name = 'V_100',
               particles = [ P.g, P.g, P.g, P.g ],
               color = [ 'f(-1,1,2)*f(3,4,-1)', 'f(-1,1,3)*f(2,4,-1)', 'f(-1,1,4)*f(2,3,-1)' ],
               lorentz = [ L.VVVV1, L.VVVV2, L.VVVV3 ],
               couplings = {(1,0):C.GC_15,(0,0):C.GC_15,(2,1):C.GC_15,(0,1):C.GC_14,(2,2):C.GC_14,(1,2):C.GC_14})

V_101 = Vertex(name = 'V_101',
               particles = [ P.g, P.g, P.g, P.G0, P.G0 ],
               color = [ 'f(1,2,3)' ],
               lorentz = [ L.VVVSS1, L.VVVSS2, L.VVVSS3, L.VVVSS4, L.VVVSS5, L.VVVSS6 ],
               couplings = {(0,0):C.GC_1120,(0,1):C.GC_1119,(0,2):C.GC_1119,(0,3):C.GC_1120,(0,4):C.GC_1120,(0,5):C.GC_1119})

V_102 = Vertex(name = 'V_102',
               particles = [ P.g, P.g, P.g, P.G__minus__, P.G__plus__ ],
               color = [ 'f(1,2,3)' ],
               lorentz = [ L.VVVSS1, L.VVVSS2, L.VVVSS3, L.VVVSS4, L.VVVSS5, L.VVVSS6 ],
               couplings = {(0,0):C.GC_1120,(0,1):C.GC_1119,(0,2):C.GC_1119,(0,3):C.GC_1120,(0,4):C.GC_1120,(0,5):C.GC_1119})

V_103 = Vertex(name = 'V_103',
               particles = [ P.g, P.g, P.g, P.H, P.H ],
               color = [ 'f(1,2,3)' ],
               lorentz = [ L.VVVSS1, L.VVVSS2, L.VVVSS3, L.VVVSS4, L.VVVSS5, L.VVVSS6 ],
               couplings = {(0,0):C.GC_1120,(0,1):C.GC_1119,(0,2):C.GC_1119,(0,3):C.GC_1120,(0,4):C.GC_1120,(0,5):C.GC_1119})

V_104 = Vertex(name = 'V_104',
               particles = [ P.g, P.g, P.g, P.H ],
               color = [ 'f(1,2,3)' ],
               lorentz = [ L.VVVS1, L.VVVS2, L.VVVS3, L.VVVS4, L.VVVS5, L.VVVS6 ],
               couplings = {(0,0):C.GC_1477,(0,1):C.GC_1476,(0,2):C.GC_1476,(0,3):C.GC_1477,(0,4):C.GC_1477,(0,5):C.GC_1476})

V_105 = Vertex(name = 'V_105',
               particles = [ P.g, P.g, P.g, P.g, P.G0, P.G0 ],
               color = [ 'f(-1,1,2)*f(3,4,-1)', 'f(-1,1,3)*f(2,4,-1)', 'f(-1,1,4)*f(2,3,-1)' ],
               lorentz = [ L.VVVVSS1, L.VVVVSS2, L.VVVVSS3 ],
               couplings = {(1,0):C.GC_1121,(0,0):C.GC_1121,(2,1):C.GC_1121,(0,1):C.GC_1122,(2,2):C.GC_1122,(1,2):C.GC_1122})

V_106 = Vertex(name = 'V_106',
               particles = [ P.g, P.g, P.g, P.g, P.G__minus__, P.G__plus__ ],
               color = [ 'f(-1,1,2)*f(3,4,-1)', 'f(-1,1,3)*f(2,4,-1)', 'f(-1,1,4)*f(2,3,-1)' ],
               lorentz = [ L.VVVVSS1, L.VVVVSS2, L.VVVVSS3 ],
               couplings = {(1,0):C.GC_1121,(0,0):C.GC_1121,(2,1):C.GC_1121,(0,1):C.GC_1122,(2,2):C.GC_1122,(1,2):C.GC_1122})

V_107 = Vertex(name = 'V_107',
               particles = [ P.g, P.g, P.g, P.g, P.H, P.H ],
               color = [ 'f(-1,1,2)*f(3,4,-1)', 'f(-1,1,3)*f(2,4,-1)', 'f(-1,1,4)*f(2,3,-1)' ],
               lorentz = [ L.VVVVSS1, L.VVVVSS2, L.VVVVSS3 ],
               couplings = {(1,0):C.GC_1121,(0,0):C.GC_1121,(2,1):C.GC_1121,(0,1):C.GC_1122,(2,2):C.GC_1122,(1,2):C.GC_1122})

V_108 = Vertex(name = 'V_108',
               particles = [ P.g, P.g, P.g, P.g, P.H ],
               color = [ 'f(-1,1,2)*f(3,4,-1)', 'f(-1,1,3)*f(2,4,-1)', 'f(-1,1,4)*f(2,3,-1)' ],
               lorentz = [ L.VVVVS1, L.VVVVS2, L.VVVVS3 ],
               couplings = {(1,0):C.GC_1478,(0,0):C.GC_1478,(2,1):C.GC_1478,(0,1):C.GC_1479,(2,2):C.GC_1479,(1,2):C.GC_1479})

V_109 = Vertex(name = 'V_109',
               particles = [ P.u__tilde__, P.d, P.G__plus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1, L.FFS2 ],
               couplings = {(0,0):C.GC_16,(0,1):C.GC_19})

V_110 = Vertex(name = 'V_110',
               particles = [ P.u__tilde__, P.d, P.G__plus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1, L.FFS2 ],
               couplings = {(0,0):C.GC_1436,(0,1):C.GC_1439})

V_111 = Vertex(name = 'V_111',
               particles = [ P.c__tilde__, P.s, P.G__plus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1, L.FFS2 ],
               couplings = {(0,0):C.GC_17,(0,1):C.GC_20})

V_112 = Vertex(name = 'V_112',
               particles = [ P.c__tilde__, P.s, P.G__plus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1, L.FFS2 ],
               couplings = {(0,0):C.GC_1437,(0,1):C.GC_1440})

V_113 = Vertex(name = 'V_113',
               particles = [ P.t__tilde__, P.b, P.G__plus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1, L.FFS2 ],
               couplings = {(0,0):C.GC_18,(0,1):C.GC_21})

V_114 = Vertex(name = 'V_114',
               particles = [ P.t__tilde__, P.b, P.G__plus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1, L.FFS2 ],
               couplings = {(0,0):C.GC_1438,(0,1):C.GC_1441})

V_115 = Vertex(name = 'V_115',
               particles = [ P.d__tilde__, P.d, P.G0 ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1, L.FFS2 ],
               couplings = {(0,0):C.GC_1619,(0,1):C.GC_1621})

V_116 = Vertex(name = 'V_116',
               particles = [ P.d__tilde__, P.d, P.G0 ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1, L.FFS2 ],
               couplings = {(0,0):C.GC_1623,(0,1):C.GC_1622})

V_117 = Vertex(name = 'V_117',
               particles = [ P.s__tilde__, P.s, P.G0 ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1, L.FFS2 ],
               couplings = {(0,0):C.GC_1641,(0,1):C.GC_1643})

V_118 = Vertex(name = 'V_118',
               particles = [ P.s__tilde__, P.s, P.G0 ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1, L.FFS2 ],
               couplings = {(0,0):C.GC_1645,(0,1):C.GC_1644})

V_119 = Vertex(name = 'V_119',
               particles = [ P.b__tilde__, P.b, P.G0 ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1, L.FFS2 ],
               couplings = {(0,0):C.GC_1607,(0,1):C.GC_1609})

V_120 = Vertex(name = 'V_120',
               particles = [ P.b__tilde__, P.b, P.G0 ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1, L.FFS2 ],
               couplings = {(0,0):C.GC_1611,(0,1):C.GC_1610})

V_121 = Vertex(name = 'V_121',
               particles = [ P.d__tilde__, P.d, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1, L.FFS2 ],
               couplings = {(0,0):C.GC_1620,(0,1):C.GC_1620})

V_122 = Vertex(name = 'V_122',
               particles = [ P.d__tilde__, P.d, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1, L.FFS2 ],
               couplings = {(0,0):C.GC_1624,(0,1):C.GC_1624})

V_123 = Vertex(name = 'V_123',
               particles = [ P.s__tilde__, P.s, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1, L.FFS2 ],
               couplings = {(0,0):C.GC_1642,(0,1):C.GC_1642})

V_124 = Vertex(name = 'V_124',
               particles = [ P.s__tilde__, P.s, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1, L.FFS2 ],
               couplings = {(0,0):C.GC_1646,(0,1):C.GC_1646})

V_125 = Vertex(name = 'V_125',
               particles = [ P.b__tilde__, P.b, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1, L.FFS2 ],
               couplings = {(0,0):C.GC_1608,(0,1):C.GC_1608})

V_126 = Vertex(name = 'V_126',
               particles = [ P.b__tilde__, P.b, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1, L.FFS2 ],
               couplings = {(0,0):C.GC_1612,(0,1):C.GC_1612})

V_127 = Vertex(name = 'V_127',
               particles = [ P.e__plus__, P.e__minus__, P.G0 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS2 ],
               couplings = {(0,0):C.GC_1625,(0,1):C.GC_1627})

V_128 = Vertex(name = 'V_128',
               particles = [ P.e__plus__, P.e__minus__, P.G0 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS2 ],
               couplings = {(0,0):C.GC_1631,(0,1):C.GC_1630})

V_129 = Vertex(name = 'V_129',
               particles = [ P.mu__plus__, P.mu__minus__, P.G0 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS2 ],
               couplings = {(0,0):C.GC_1633,(0,1):C.GC_1635})

V_130 = Vertex(name = 'V_130',
               particles = [ P.mu__plus__, P.mu__minus__, P.G0 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS2 ],
               couplings = {(0,0):C.GC_1639,(0,1):C.GC_1638})

V_131 = Vertex(name = 'V_131',
               particles = [ P.tau__plus__, P.tau__minus__, P.G0 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS2 ],
               couplings = {(0,0):C.GC_1653,(0,1):C.GC_1655})

V_132 = Vertex(name = 'V_132',
               particles = [ P.tau__plus__, P.tau__minus__, P.G0 ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS2 ],
               couplings = {(0,0):C.GC_1659,(0,1):C.GC_1658})

V_133 = Vertex(name = 'V_133',
               particles = [ P.e__plus__, P.e__minus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS2 ],
               couplings = {(0,0):C.GC_1626,(0,1):C.GC_1626})

V_134 = Vertex(name = 'V_134',
               particles = [ P.e__plus__, P.e__minus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS2 ],
               couplings = {(0,0):C.GC_1632,(0,1):C.GC_1632})

V_135 = Vertex(name = 'V_135',
               particles = [ P.mu__plus__, P.mu__minus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS2 ],
               couplings = {(0,0):C.GC_1634,(0,1):C.GC_1634})

V_136 = Vertex(name = 'V_136',
               particles = [ P.mu__plus__, P.mu__minus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS2 ],
               couplings = {(0,0):C.GC_1640,(0,1):C.GC_1640})

V_137 = Vertex(name = 'V_137',
               particles = [ P.tau__plus__, P.tau__minus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS2 ],
               couplings = {(0,0):C.GC_1654,(0,1):C.GC_1654})

V_138 = Vertex(name = 'V_138',
               particles = [ P.tau__plus__, P.tau__minus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFS1, L.FFS2 ],
               couplings = {(0,0):C.GC_1660,(0,1):C.GC_1660})

V_139 = Vertex(name = 'V_139',
               particles = [ P.nu_e__tilde__, P.e__minus__, P.G__plus__ ],
               color = [ '1' ],
               lorentz = [ L.FFS1 ],
               couplings = {(0,0):C.GC_1628})

V_140 = Vertex(name = 'V_140',
               particles = [ P.nu_e__tilde__, P.e__minus__, P.G__plus__ ],
               color = [ '1' ],
               lorentz = [ L.FFS1 ],
               couplings = {(0,0):C.GC_1629})

V_141 = Vertex(name = 'V_141',
               particles = [ P.nu_mu__tilde__, P.mu__minus__, P.G__plus__ ],
               color = [ '1' ],
               lorentz = [ L.FFS1 ],
               couplings = {(0,0):C.GC_1636})

V_142 = Vertex(name = 'V_142',
               particles = [ P.nu_mu__tilde__, P.mu__minus__, P.G__plus__ ],
               color = [ '1' ],
               lorentz = [ L.FFS1 ],
               couplings = {(0,0):C.GC_1637})

V_143 = Vertex(name = 'V_143',
               particles = [ P.nu_tau__tilde__, P.tau__minus__, P.G__plus__ ],
               color = [ '1' ],
               lorentz = [ L.FFS1 ],
               couplings = {(0,0):C.GC_1656})

V_144 = Vertex(name = 'V_144',
               particles = [ P.nu_tau__tilde__, P.tau__minus__, P.G__plus__ ],
               color = [ '1' ],
               lorentz = [ L.FFS1 ],
               couplings = {(0,0):C.GC_1657})

V_145 = Vertex(name = 'V_145',
               particles = [ P.u__tilde__, P.u, P.G0 ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1, L.FFS2 ],
               couplings = {(0,0):C.GC_1663,(0,1):C.GC_1661})

V_146 = Vertex(name = 'V_146',
               particles = [ P.u__tilde__, P.u, P.G0 ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1, L.FFS2 ],
               couplings = {(0,0):C.GC_1664,(0,1):C.GC_1665})

V_147 = Vertex(name = 'V_147',
               particles = [ P.c__tilde__, P.c, P.G0 ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1, L.FFS2 ],
               couplings = {(0,0):C.GC_1615,(0,1):C.GC_1613})

V_148 = Vertex(name = 'V_148',
               particles = [ P.c__tilde__, P.c, P.G0 ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1, L.FFS2 ],
               couplings = {(0,0):C.GC_1616,(0,1):C.GC_1617})

V_149 = Vertex(name = 'V_149',
               particles = [ P.t__tilde__, P.t, P.G0 ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1, L.FFS2 ],
               couplings = {(0,0):C.GC_1649,(0,1):C.GC_1647})

V_150 = Vertex(name = 'V_150',
               particles = [ P.t__tilde__, P.t, P.G0 ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1, L.FFS2 ],
               couplings = {(0,0):C.GC_1650,(0,1):C.GC_1651})

V_151 = Vertex(name = 'V_151',
               particles = [ P.u__tilde__, P.u, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1, L.FFS2 ],
               couplings = {(0,0):C.GC_1662,(0,1):C.GC_1662})

V_152 = Vertex(name = 'V_152',
               particles = [ P.u__tilde__, P.u, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1, L.FFS2 ],
               couplings = {(0,0):C.GC_1666,(0,1):C.GC_1666})

V_153 = Vertex(name = 'V_153',
               particles = [ P.c__tilde__, P.c, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1, L.FFS2 ],
               couplings = {(0,0):C.GC_1614,(0,1):C.GC_1614})

V_154 = Vertex(name = 'V_154',
               particles = [ P.c__tilde__, P.c, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1, L.FFS2 ],
               couplings = {(0,0):C.GC_1618,(0,1):C.GC_1618})

V_155 = Vertex(name = 'V_155',
               particles = [ P.t__tilde__, P.t, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1, L.FFS2 ],
               couplings = {(0,0):C.GC_1648,(0,1):C.GC_1648})

V_156 = Vertex(name = 'V_156',
               particles = [ P.t__tilde__, P.t, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1, L.FFS2 ],
               couplings = {(0,0):C.GC_1652,(0,1):C.GC_1652})

V_157 = Vertex(name = 'V_157',
               particles = [ P.d__tilde__, P.u, P.G__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1, L.FFS2 ],
               couplings = {(0,0):C.GC_22,(0,1):C.GC_25})

V_158 = Vertex(name = 'V_158',
               particles = [ P.d__tilde__, P.u, P.G__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1, L.FFS2 ],
               couplings = {(0,0):C.GC_1442,(0,1):C.GC_1445})

V_159 = Vertex(name = 'V_159',
               particles = [ P.s__tilde__, P.c, P.G__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1, L.FFS2 ],
               couplings = {(0,0):C.GC_23,(0,1):C.GC_26})

V_160 = Vertex(name = 'V_160',
               particles = [ P.s__tilde__, P.c, P.G__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1, L.FFS2 ],
               couplings = {(0,0):C.GC_1443,(0,1):C.GC_1446})

V_161 = Vertex(name = 'V_161',
               particles = [ P.b__tilde__, P.t, P.G__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1, L.FFS2 ],
               couplings = {(0,0):C.GC_24,(0,1):C.GC_27})

V_162 = Vertex(name = 'V_162',
               particles = [ P.b__tilde__, P.t, P.G__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS1, L.FFS2 ],
               couplings = {(0,0):C.GC_1444,(0,1):C.GC_1447})

V_163 = Vertex(name = 'V_163',
               particles = [ P.e__plus__, P.e__minus__, P.u__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1164,(0,11):C.GC_1913,(0,9):C.GC_1910,(0,10):C.GC_1910,(0,0):C.GC_1748,(0,2):C.GC_1021,(0,3):C.GC_930,(0,4):C.GC_109,(0,7):C.GC_517,(0,5):C.GC_514,(0,6):C.GC_514,(0,1):C.GC_352})

V_164 = Vertex(name = 'V_164',
               particles = [ P.e__plus__, P.e__minus__, P.c__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1166,(0,11):C.GC_1925,(0,9):C.GC_1922,(0,10):C.GC_1922,(0,0):C.GC_1754,(0,2):C.GC_1030,(0,3):C.GC_931,(0,4):C.GC_110,(0,7):C.GC_521,(0,5):C.GC_518,(0,6):C.GC_518,(0,1):C.GC_354})

V_165 = Vertex(name = 'V_165',
               particles = [ P.e__plus__, P.e__minus__, P.t__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,10):C.GC_1934,(0,8):C.GC_1168,(0,11):C.GC_1937,(0,9):C.GC_1934,(0,0):C.GC_1760,(0,5):C.GC_522,(0,6):C.GC_522,(0,2):C.GC_1039,(0,3):C.GC_932,(0,4):C.GC_111,(0,7):C.GC_525,(0,1):C.GC_356})

V_166 = Vertex(name = 'V_166',
               particles = [ P.e__plus__, P.e__minus__, P.u__tilde__, P.c ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1170,(0,11):C.GC_1917,(0,9):C.GC_1914,(0,10):C.GC_1914,(0,0):C.GC_1750,(0,2):C.GC_1048,(0,3):C.GC_933,(0,4):C.GC_112,(0,7):C.GC_529,(0,5):C.GC_526,(0,6):C.GC_526,(0,1):C.GC_358})

V_167 = Vertex(name = 'V_167',
               particles = [ P.e__plus__, P.e__minus__, P.c__tilde__, P.c ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1172,(0,11):C.GC_1929,(0,9):C.GC_1926,(0,10):C.GC_1926,(0,0):C.GC_1756,(0,2):C.GC_1057,(0,3):C.GC_934,(0,4):C.GC_113,(0,7):C.GC_533,(0,5):C.GC_530,(0,6):C.GC_530,(0,1):C.GC_360})

V_168 = Vertex(name = 'V_168',
               particles = [ P.e__plus__, P.e__minus__, P.t__tilde__, P.c ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1174,(0,11):C.GC_1941,(0,9):C.GC_1938,(0,10):C.GC_1938,(0,0):C.GC_1762,(0,2):C.GC_1066,(0,3):C.GC_935,(0,4):C.GC_114,(0,7):C.GC_537,(0,5):C.GC_534,(0,6):C.GC_534,(0,1):C.GC_362})

V_169 = Vertex(name = 'V_169',
               particles = [ P.e__plus__, P.e__minus__, P.u__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1176,(0,11):C.GC_1921,(0,9):C.GC_1918,(0,10):C.GC_1918,(0,0):C.GC_1752,(0,2):C.GC_1075,(0,3):C.GC_936,(0,4):C.GC_115,(0,7):C.GC_541,(0,5):C.GC_538,(0,6):C.GC_538,(0,1):C.GC_364})

V_170 = Vertex(name = 'V_170',
               particles = [ P.e__plus__, P.e__minus__, P.c__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1178,(0,11):C.GC_1933,(0,9):C.GC_1930,(0,10):C.GC_1930,(0,0):C.GC_1758,(0,2):C.GC_1084,(0,3):C.GC_937,(0,4):C.GC_116,(0,7):C.GC_545,(0,5):C.GC_542,(0,6):C.GC_542,(0,1):C.GC_366})

V_171 = Vertex(name = 'V_171',
               particles = [ P.e__plus__, P.e__minus__, P.t__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1180,(0,11):C.GC_1945,(0,9):C.GC_1942,(0,10):C.GC_1942,(0,0):C.GC_1764,(0,2):C.GC_1093,(0,3):C.GC_938,(0,4):C.GC_117,(0,7):C.GC_549,(0,5):C.GC_546,(0,6):C.GC_546,(0,1):C.GC_368})

V_172 = Vertex(name = 'V_172',
               particles = [ P.mu__plus__, P.e__minus__, P.u__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1182,(0,11):C.GC_2021,(0,9):C.GC_2018,(0,10):C.GC_2018,(0,0):C.GC_1802,(0,2):C.GC_1022,(0,3):C.GC_939,(0,4):C.GC_118,(0,7):C.GC_553,(0,5):C.GC_550,(0,6):C.GC_550,(0,1):C.GC_370})

V_173 = Vertex(name = 'V_173',
               particles = [ P.mu__plus__, P.e__minus__, P.c__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1184,(0,11):C.GC_2033,(0,9):C.GC_2030,(0,10):C.GC_2030,(0,0):C.GC_1808,(0,2):C.GC_1031,(0,3):C.GC_940,(0,4):C.GC_119,(0,7):C.GC_557,(0,5):C.GC_554,(0,6):C.GC_554,(0,1):C.GC_372})

V_174 = Vertex(name = 'V_174',
               particles = [ P.mu__plus__, P.e__minus__, P.t__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1186,(0,11):C.GC_2045,(0,9):C.GC_2042,(0,10):C.GC_2042,(0,0):C.GC_1814,(0,2):C.GC_1040,(0,3):C.GC_941,(0,4):C.GC_120,(0,7):C.GC_561,(0,5):C.GC_558,(0,6):C.GC_558,(0,1):C.GC_374})

V_175 = Vertex(name = 'V_175',
               particles = [ P.mu__plus__, P.e__minus__, P.u__tilde__, P.c ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1188,(0,11):C.GC_2025,(0,9):C.GC_2022,(0,10):C.GC_2022,(0,0):C.GC_1804,(0,2):C.GC_1049,(0,3):C.GC_942,(0,4):C.GC_121,(0,7):C.GC_565,(0,5):C.GC_562,(0,6):C.GC_562,(0,1):C.GC_376})

V_176 = Vertex(name = 'V_176',
               particles = [ P.mu__plus__, P.e__minus__, P.c__tilde__, P.c ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1190,(0,11):C.GC_2037,(0,9):C.GC_2034,(0,10):C.GC_2034,(0,0):C.GC_1810,(0,2):C.GC_1058,(0,3):C.GC_943,(0,4):C.GC_122,(0,7):C.GC_569,(0,5):C.GC_566,(0,6):C.GC_566,(0,1):C.GC_378})

V_177 = Vertex(name = 'V_177',
               particles = [ P.mu__plus__, P.e__minus__, P.t__tilde__, P.c ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1192,(0,11):C.GC_2049,(0,9):C.GC_2046,(0,10):C.GC_2046,(0,0):C.GC_1816,(0,2):C.GC_1067,(0,3):C.GC_944,(0,4):C.GC_123,(0,7):C.GC_573,(0,5):C.GC_570,(0,6):C.GC_570,(0,1):C.GC_380})

V_178 = Vertex(name = 'V_178',
               particles = [ P.mu__plus__, P.e__minus__, P.u__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1194,(0,11):C.GC_2029,(0,9):C.GC_2026,(0,10):C.GC_2026,(0,0):C.GC_1806,(0,2):C.GC_1076,(0,3):C.GC_945,(0,4):C.GC_124,(0,7):C.GC_577,(0,5):C.GC_574,(0,6):C.GC_574,(0,1):C.GC_382})

V_179 = Vertex(name = 'V_179',
               particles = [ P.mu__plus__, P.e__minus__, P.c__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1196,(0,11):C.GC_2041,(0,9):C.GC_2038,(0,10):C.GC_2038,(0,0):C.GC_1812,(0,2):C.GC_1085,(0,3):C.GC_946,(0,4):C.GC_125,(0,7):C.GC_581,(0,5):C.GC_578,(0,6):C.GC_578,(0,1):C.GC_384})

V_180 = Vertex(name = 'V_180',
               particles = [ P.mu__plus__, P.e__minus__, P.t__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1198,(0,11):C.GC_2053,(0,9):C.GC_2050,(0,10):C.GC_2050,(0,0):C.GC_1818,(0,2):C.GC_1094,(0,3):C.GC_947,(0,4):C.GC_126,(0,7):C.GC_585,(0,5):C.GC_582,(0,6):C.GC_582,(0,1):C.GC_386})

V_181 = Vertex(name = 'V_181',
               particles = [ P.tau__plus__, P.e__minus__, P.u__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1200,(0,11):C.GC_2129,(0,9):C.GC_2126,(0,10):C.GC_2126,(0,0):C.GC_1856,(0,2):C.GC_1023,(0,3):C.GC_948,(0,4):C.GC_127,(0,7):C.GC_589,(0,5):C.GC_586,(0,6):C.GC_586,(0,1):C.GC_388})

V_182 = Vertex(name = 'V_182',
               particles = [ P.tau__plus__, P.e__minus__, P.c__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1202,(0,11):C.GC_2141,(0,9):C.GC_2138,(0,10):C.GC_2138,(0,0):C.GC_1862,(0,2):C.GC_1032,(0,3):C.GC_949,(0,4):C.GC_128,(0,7):C.GC_593,(0,5):C.GC_590,(0,6):C.GC_590,(0,1):C.GC_390})

V_183 = Vertex(name = 'V_183',
               particles = [ P.tau__plus__, P.e__minus__, P.t__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1204,(0,11):C.GC_2153,(0,9):C.GC_2150,(0,10):C.GC_2150,(0,0):C.GC_1868,(0,2):C.GC_1041,(0,3):C.GC_950,(0,4):C.GC_129,(0,7):C.GC_597,(0,5):C.GC_594,(0,6):C.GC_594,(0,1):C.GC_392})

V_184 = Vertex(name = 'V_184',
               particles = [ P.tau__plus__, P.e__minus__, P.u__tilde__, P.c ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1206,(0,11):C.GC_2133,(0,9):C.GC_2130,(0,10):C.GC_2130,(0,0):C.GC_1858,(0,2):C.GC_1050,(0,3):C.GC_951,(0,4):C.GC_130,(0,7):C.GC_601,(0,5):C.GC_598,(0,6):C.GC_598,(0,1):C.GC_394})

V_185 = Vertex(name = 'V_185',
               particles = [ P.tau__plus__, P.e__minus__, P.c__tilde__, P.c ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1208,(0,11):C.GC_2145,(0,9):C.GC_2142,(0,10):C.GC_2142,(0,0):C.GC_1864,(0,2):C.GC_1059,(0,3):C.GC_952,(0,4):C.GC_131,(0,7):C.GC_605,(0,5):C.GC_602,(0,6):C.GC_602,(0,1):C.GC_396})

V_186 = Vertex(name = 'V_186',
               particles = [ P.tau__plus__, P.e__minus__, P.t__tilde__, P.c ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1210,(0,11):C.GC_2157,(0,9):C.GC_2154,(0,10):C.GC_2154,(0,0):C.GC_1870,(0,2):C.GC_1068,(0,3):C.GC_953,(0,4):C.GC_132,(0,7):C.GC_609,(0,5):C.GC_606,(0,6):C.GC_606,(0,1):C.GC_398})

V_187 = Vertex(name = 'V_187',
               particles = [ P.tau__plus__, P.e__minus__, P.u__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1212,(0,11):C.GC_2137,(0,9):C.GC_2134,(0,10):C.GC_2134,(0,0):C.GC_1860,(0,2):C.GC_1077,(0,3):C.GC_954,(0,4):C.GC_133,(0,7):C.GC_613,(0,5):C.GC_610,(0,6):C.GC_610,(0,1):C.GC_400})

V_188 = Vertex(name = 'V_188',
               particles = [ P.tau__plus__, P.e__minus__, P.c__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1214,(0,11):C.GC_2149,(0,9):C.GC_2146,(0,10):C.GC_2146,(0,0):C.GC_1866,(0,2):C.GC_1086,(0,3):C.GC_955,(0,4):C.GC_134,(0,7):C.GC_617,(0,5):C.GC_614,(0,6):C.GC_614,(0,1):C.GC_402})

V_189 = Vertex(name = 'V_189',
               particles = [ P.tau__plus__, P.e__minus__, P.t__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1216,(0,11):C.GC_2161,(0,9):C.GC_2158,(0,10):C.GC_2158,(0,0):C.GC_1872,(0,2):C.GC_1095,(0,3):C.GC_956,(0,4):C.GC_135,(0,7):C.GC_621,(0,5):C.GC_618,(0,6):C.GC_618,(0,1):C.GC_404})

V_190 = Vertex(name = 'V_190',
               particles = [ P.e__plus__, P.mu__minus__, P.u__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1218,(0,11):C.GC_1949,(0,9):C.GC_1946,(0,10):C.GC_1946,(0,0):C.GC_1766,(0,2):C.GC_1024,(0,3):C.GC_957,(0,4):C.GC_136,(0,7):C.GC_625,(0,5):C.GC_622,(0,6):C.GC_622,(0,1):C.GC_406})

V_191 = Vertex(name = 'V_191',
               particles = [ P.e__plus__, P.mu__minus__, P.c__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1220,(0,11):C.GC_1961,(0,9):C.GC_1958,(0,10):C.GC_1958,(0,0):C.GC_1772,(0,2):C.GC_1033,(0,3):C.GC_958,(0,4):C.GC_137,(0,7):C.GC_629,(0,5):C.GC_626,(0,6):C.GC_626,(0,1):C.GC_408})

V_192 = Vertex(name = 'V_192',
               particles = [ P.e__plus__, P.mu__minus__, P.t__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,11):C.GC_1973,(0,9):C.GC_1970,(0,10):C.GC_1970,(0,8):C.GC_1222,(0,0):C.GC_1778,(0,2):C.GC_1042,(0,3):C.GC_959,(0,4):C.GC_138,(0,7):C.GC_633,(0,5):C.GC_630,(0,6):C.GC_630,(0,1):C.GC_410})

V_193 = Vertex(name = 'V_193',
               particles = [ P.e__plus__, P.mu__minus__, P.u__tilde__, P.c ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1224,(0,11):C.GC_1953,(0,9):C.GC_1950,(0,10):C.GC_1950,(0,0):C.GC_1768,(0,2):C.GC_1051,(0,3):C.GC_960,(0,4):C.GC_139,(0,7):C.GC_637,(0,5):C.GC_634,(0,6):C.GC_634,(0,1):C.GC_412})

V_194 = Vertex(name = 'V_194',
               particles = [ P.e__plus__, P.mu__minus__, P.c__tilde__, P.c ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1226,(0,11):C.GC_1965,(0,9):C.GC_1962,(0,10):C.GC_1962,(0,0):C.GC_1774,(0,2):C.GC_1060,(0,3):C.GC_961,(0,4):C.GC_140,(0,7):C.GC_641,(0,5):C.GC_638,(0,6):C.GC_638,(0,1):C.GC_414})

V_195 = Vertex(name = 'V_195',
               particles = [ P.e__plus__, P.mu__minus__, P.t__tilde__, P.c ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1228,(0,11):C.GC_1977,(0,9):C.GC_1974,(0,10):C.GC_1974,(0,0):C.GC_1780,(0,2):C.GC_1069,(0,3):C.GC_962,(0,4):C.GC_141,(0,7):C.GC_645,(0,5):C.GC_642,(0,6):C.GC_642,(0,1):C.GC_416})

V_196 = Vertex(name = 'V_196',
               particles = [ P.e__plus__, P.mu__minus__, P.u__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1230,(0,11):C.GC_1957,(0,9):C.GC_1954,(0,10):C.GC_1954,(0,0):C.GC_1770,(0,2):C.GC_1078,(0,3):C.GC_963,(0,4):C.GC_142,(0,7):C.GC_649,(0,5):C.GC_646,(0,6):C.GC_646,(0,1):C.GC_418})

V_197 = Vertex(name = 'V_197',
               particles = [ P.e__plus__, P.mu__minus__, P.c__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1232,(0,11):C.GC_1969,(0,9):C.GC_1966,(0,10):C.GC_1966,(0,0):C.GC_1776,(0,2):C.GC_1087,(0,3):C.GC_964,(0,4):C.GC_143,(0,7):C.GC_653,(0,5):C.GC_650,(0,6):C.GC_650,(0,1):C.GC_420})

V_198 = Vertex(name = 'V_198',
               particles = [ P.e__plus__, P.mu__minus__, P.t__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1234,(0,11):C.GC_1981,(0,9):C.GC_1978,(0,10):C.GC_1978,(0,0):C.GC_1782,(0,2):C.GC_1096,(0,3):C.GC_965,(0,4):C.GC_144,(0,7):C.GC_657,(0,5):C.GC_654,(0,6):C.GC_654,(0,1):C.GC_422})

V_199 = Vertex(name = 'V_199',
               particles = [ P.mu__plus__, P.mu__minus__, P.u__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1236,(0,11):C.GC_2057,(0,9):C.GC_2054,(0,10):C.GC_2054,(0,0):C.GC_1820,(0,2):C.GC_1025,(0,3):C.GC_966,(0,4):C.GC_145,(0,7):C.GC_661,(0,5):C.GC_658,(0,6):C.GC_658,(0,1):C.GC_424})

V_200 = Vertex(name = 'V_200',
               particles = [ P.mu__plus__, P.mu__minus__, P.c__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1238,(0,11):C.GC_2069,(0,9):C.GC_2066,(0,10):C.GC_2066,(0,0):C.GC_1826,(0,2):C.GC_1034,(0,3):C.GC_967,(0,4):C.GC_146,(0,7):C.GC_665,(0,5):C.GC_662,(0,6):C.GC_662,(0,1):C.GC_426})

V_201 = Vertex(name = 'V_201',
               particles = [ P.mu__plus__, P.mu__minus__, P.t__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1240,(0,11):C.GC_2081,(0,9):C.GC_2078,(0,10):C.GC_2078,(0,0):C.GC_1832,(0,2):C.GC_1043,(0,3):C.GC_968,(0,4):C.GC_147,(0,7):C.GC_669,(0,5):C.GC_666,(0,6):C.GC_666,(0,1):C.GC_428})

V_202 = Vertex(name = 'V_202',
               particles = [ P.mu__plus__, P.mu__minus__, P.u__tilde__, P.c ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1242,(0,11):C.GC_2061,(0,9):C.GC_2058,(0,10):C.GC_2058,(0,0):C.GC_1822,(0,2):C.GC_1052,(0,3):C.GC_969,(0,4):C.GC_148,(0,7):C.GC_673,(0,5):C.GC_670,(0,6):C.GC_670,(0,1):C.GC_430})

V_203 = Vertex(name = 'V_203',
               particles = [ P.mu__plus__, P.mu__minus__, P.c__tilde__, P.c ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1244,(0,11):C.GC_2073,(0,9):C.GC_2070,(0,10):C.GC_2070,(0,0):C.GC_1828,(0,2):C.GC_1061,(0,3):C.GC_970,(0,4):C.GC_149,(0,7):C.GC_677,(0,5):C.GC_674,(0,6):C.GC_674,(0,1):C.GC_432})

V_204 = Vertex(name = 'V_204',
               particles = [ P.mu__plus__, P.mu__minus__, P.t__tilde__, P.c ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1246,(0,11):C.GC_2085,(0,9):C.GC_2082,(0,10):C.GC_2082,(0,0):C.GC_1834,(0,2):C.GC_1070,(0,3):C.GC_971,(0,4):C.GC_150,(0,7):C.GC_681,(0,5):C.GC_678,(0,6):C.GC_678,(0,1):C.GC_434})

V_205 = Vertex(name = 'V_205',
               particles = [ P.mu__plus__, P.mu__minus__, P.u__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1248,(0,11):C.GC_2065,(0,9):C.GC_2062,(0,10):C.GC_2062,(0,0):C.GC_1824,(0,2):C.GC_1079,(0,3):C.GC_972,(0,4):C.GC_151,(0,7):C.GC_685,(0,5):C.GC_682,(0,6):C.GC_682,(0,1):C.GC_436})

V_206 = Vertex(name = 'V_206',
               particles = [ P.mu__plus__, P.mu__minus__, P.c__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1250,(0,11):C.GC_2077,(0,9):C.GC_2074,(0,10):C.GC_2074,(0,0):C.GC_1830,(0,2):C.GC_1088,(0,3):C.GC_973,(0,4):C.GC_152,(0,7):C.GC_689,(0,5):C.GC_686,(0,6):C.GC_686,(0,1):C.GC_438})

V_207 = Vertex(name = 'V_207',
               particles = [ P.mu__plus__, P.mu__minus__, P.t__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1252,(0,11):C.GC_2089,(0,9):C.GC_2086,(0,10):C.GC_2086,(0,0):C.GC_1836,(0,2):C.GC_1097,(0,3):C.GC_974,(0,4):C.GC_153,(0,7):C.GC_693,(0,5):C.GC_690,(0,6):C.GC_690,(0,1):C.GC_440})

V_208 = Vertex(name = 'V_208',
               particles = [ P.tau__plus__, P.mu__minus__, P.u__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1254,(0,11):C.GC_2165,(0,9):C.GC_2162,(0,10):C.GC_2162,(0,0):C.GC_1874,(0,2):C.GC_1026,(0,3):C.GC_975,(0,4):C.GC_154,(0,7):C.GC_697,(0,5):C.GC_694,(0,6):C.GC_694,(0,1):C.GC_442})

V_209 = Vertex(name = 'V_209',
               particles = [ P.tau__plus__, P.mu__minus__, P.c__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1256,(0,11):C.GC_2177,(0,9):C.GC_2174,(0,10):C.GC_2174,(0,0):C.GC_1880,(0,2):C.GC_1035,(0,3):C.GC_976,(0,4):C.GC_155,(0,7):C.GC_701,(0,5):C.GC_698,(0,6):C.GC_698,(0,1):C.GC_444})

V_210 = Vertex(name = 'V_210',
               particles = [ P.tau__plus__, P.mu__minus__, P.t__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1258,(0,11):C.GC_2189,(0,9):C.GC_2186,(0,10):C.GC_2186,(0,0):C.GC_1886,(0,2):C.GC_1044,(0,3):C.GC_977,(0,4):C.GC_156,(0,7):C.GC_705,(0,5):C.GC_702,(0,6):C.GC_702,(0,1):C.GC_446})

V_211 = Vertex(name = 'V_211',
               particles = [ P.tau__plus__, P.mu__minus__, P.u__tilde__, P.c ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1260,(0,11):C.GC_2169,(0,9):C.GC_2166,(0,10):C.GC_2166,(0,0):C.GC_1876,(0,2):C.GC_1053,(0,3):C.GC_978,(0,4):C.GC_157,(0,7):C.GC_709,(0,5):C.GC_706,(0,6):C.GC_706,(0,1):C.GC_448})

V_212 = Vertex(name = 'V_212',
               particles = [ P.tau__plus__, P.mu__minus__, P.c__tilde__, P.c ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1262,(0,11):C.GC_2181,(0,9):C.GC_2178,(0,10):C.GC_2178,(0,0):C.GC_1882,(0,2):C.GC_1062,(0,3):C.GC_979,(0,4):C.GC_158,(0,7):C.GC_713,(0,5):C.GC_710,(0,6):C.GC_710,(0,1):C.GC_450})

V_213 = Vertex(name = 'V_213',
               particles = [ P.tau__plus__, P.mu__minus__, P.t__tilde__, P.c ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1264,(0,11):C.GC_2193,(0,9):C.GC_2190,(0,10):C.GC_2190,(0,0):C.GC_1888,(0,2):C.GC_1071,(0,3):C.GC_980,(0,4):C.GC_159,(0,7):C.GC_717,(0,5):C.GC_714,(0,6):C.GC_714,(0,1):C.GC_452})

V_214 = Vertex(name = 'V_214',
               particles = [ P.tau__plus__, P.mu__minus__, P.u__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1266,(0,11):C.GC_2173,(0,9):C.GC_2170,(0,10):C.GC_2170,(0,0):C.GC_1878,(0,2):C.GC_1080,(0,3):C.GC_981,(0,4):C.GC_160,(0,7):C.GC_721,(0,5):C.GC_718,(0,6):C.GC_718,(0,1):C.GC_454})

V_215 = Vertex(name = 'V_215',
               particles = [ P.tau__plus__, P.mu__minus__, P.c__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1268,(0,11):C.GC_2185,(0,9):C.GC_2182,(0,10):C.GC_2182,(0,0):C.GC_1884,(0,2):C.GC_1089,(0,3):C.GC_982,(0,4):C.GC_161,(0,7):C.GC_725,(0,5):C.GC_722,(0,6):C.GC_722,(0,1):C.GC_456})

V_216 = Vertex(name = 'V_216',
               particles = [ P.tau__plus__, P.mu__minus__, P.t__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1270,(0,11):C.GC_2197,(0,9):C.GC_2194,(0,10):C.GC_2194,(0,0):C.GC_1890,(0,2):C.GC_1098,(0,3):C.GC_983,(0,4):C.GC_162,(0,7):C.GC_729,(0,5):C.GC_726,(0,6):C.GC_726,(0,1):C.GC_458})

V_217 = Vertex(name = 'V_217',
               particles = [ P.e__plus__, P.tau__minus__, P.u__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1272,(0,11):C.GC_1985,(0,9):C.GC_1982,(0,10):C.GC_1982,(0,0):C.GC_1784,(0,2):C.GC_1027,(0,3):C.GC_984,(0,4):C.GC_163,(0,7):C.GC_733,(0,5):C.GC_730,(0,6):C.GC_730,(0,1):C.GC_460})

V_218 = Vertex(name = 'V_218',
               particles = [ P.e__plus__, P.tau__minus__, P.c__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1274,(0,11):C.GC_1997,(0,9):C.GC_1994,(0,10):C.GC_1994,(0,0):C.GC_1790,(0,2):C.GC_1036,(0,3):C.GC_985,(0,4):C.GC_164,(0,7):C.GC_737,(0,5):C.GC_734,(0,6):C.GC_734,(0,1):C.GC_462})

V_219 = Vertex(name = 'V_219',
               particles = [ P.e__plus__, P.tau__minus__, P.t__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1276,(0,11):C.GC_2009,(0,9):C.GC_2006,(0,10):C.GC_2006,(0,0):C.GC_1796,(0,2):C.GC_1045,(0,3):C.GC_986,(0,4):C.GC_165,(0,7):C.GC_741,(0,5):C.GC_738,(0,6):C.GC_738,(0,1):C.GC_464})

V_220 = Vertex(name = 'V_220',
               particles = [ P.e__plus__, P.tau__minus__, P.u__tilde__, P.c ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1278,(0,11):C.GC_1989,(0,9):C.GC_1986,(0,10):C.GC_1986,(0,0):C.GC_1786,(0,2):C.GC_1054,(0,3):C.GC_987,(0,4):C.GC_166,(0,7):C.GC_745,(0,5):C.GC_742,(0,6):C.GC_742,(0,1):C.GC_466})

V_221 = Vertex(name = 'V_221',
               particles = [ P.e__plus__, P.tau__minus__, P.c__tilde__, P.c ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1280,(0,11):C.GC_2001,(0,9):C.GC_1998,(0,10):C.GC_1998,(0,0):C.GC_1792,(0,2):C.GC_1063,(0,3):C.GC_988,(0,4):C.GC_167,(0,7):C.GC_749,(0,5):C.GC_746,(0,6):C.GC_746,(0,1):C.GC_468})

V_222 = Vertex(name = 'V_222',
               particles = [ P.e__plus__, P.tau__minus__, P.t__tilde__, P.c ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1282,(0,11):C.GC_2013,(0,9):C.GC_2010,(0,10):C.GC_2010,(0,0):C.GC_1798,(0,2):C.GC_1072,(0,3):C.GC_989,(0,4):C.GC_168,(0,7):C.GC_753,(0,5):C.GC_750,(0,6):C.GC_750,(0,1):C.GC_470})

V_223 = Vertex(name = 'V_223',
               particles = [ P.e__plus__, P.tau__minus__, P.u__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1284,(0,11):C.GC_1993,(0,9):C.GC_1990,(0,10):C.GC_1990,(0,0):C.GC_1788,(0,2):C.GC_1081,(0,3):C.GC_990,(0,4):C.GC_169,(0,7):C.GC_757,(0,5):C.GC_754,(0,6):C.GC_754,(0,1):C.GC_472})

V_224 = Vertex(name = 'V_224',
               particles = [ P.e__plus__, P.tau__minus__, P.c__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1286,(0,11):C.GC_2005,(0,9):C.GC_2002,(0,10):C.GC_2002,(0,0):C.GC_1794,(0,2):C.GC_1090,(0,3):C.GC_991,(0,4):C.GC_170,(0,7):C.GC_761,(0,5):C.GC_758,(0,6):C.GC_758,(0,1):C.GC_474})

V_225 = Vertex(name = 'V_225',
               particles = [ P.e__plus__, P.tau__minus__, P.t__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1288,(0,11):C.GC_2017,(0,9):C.GC_2014,(0,10):C.GC_2014,(0,0):C.GC_1800,(0,2):C.GC_1099,(0,3):C.GC_992,(0,4):C.GC_171,(0,7):C.GC_765,(0,5):C.GC_762,(0,6):C.GC_762,(0,1):C.GC_476})

V_226 = Vertex(name = 'V_226',
               particles = [ P.mu__plus__, P.tau__minus__, P.u__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1290,(0,11):C.GC_2093,(0,9):C.GC_2090,(0,10):C.GC_2090,(0,0):C.GC_1838,(0,2):C.GC_1028,(0,3):C.GC_993,(0,4):C.GC_172,(0,7):C.GC_769,(0,5):C.GC_766,(0,6):C.GC_766,(0,1):C.GC_478})

V_227 = Vertex(name = 'V_227',
               particles = [ P.mu__plus__, P.tau__minus__, P.c__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1292,(0,11):C.GC_2105,(0,9):C.GC_2102,(0,10):C.GC_2102,(0,0):C.GC_1844,(0,2):C.GC_1037,(0,3):C.GC_994,(0,4):C.GC_173,(0,7):C.GC_773,(0,5):C.GC_770,(0,6):C.GC_770,(0,1):C.GC_480})

V_228 = Vertex(name = 'V_228',
               particles = [ P.mu__plus__, P.tau__minus__, P.t__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1294,(0,11):C.GC_2117,(0,9):C.GC_2114,(0,10):C.GC_2114,(0,0):C.GC_1850,(0,2):C.GC_1046,(0,3):C.GC_995,(0,4):C.GC_174,(0,7):C.GC_777,(0,5):C.GC_774,(0,6):C.GC_774,(0,1):C.GC_482})

V_229 = Vertex(name = 'V_229',
               particles = [ P.mu__plus__, P.tau__minus__, P.u__tilde__, P.c ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1296,(0,11):C.GC_2097,(0,9):C.GC_2094,(0,10):C.GC_2094,(0,0):C.GC_1840,(0,2):C.GC_1055,(0,3):C.GC_996,(0,4):C.GC_175,(0,7):C.GC_781,(0,5):C.GC_778,(0,6):C.GC_778,(0,1):C.GC_484})

V_230 = Vertex(name = 'V_230',
               particles = [ P.mu__plus__, P.tau__minus__, P.c__tilde__, P.c ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1298,(0,11):C.GC_2109,(0,9):C.GC_2106,(0,10):C.GC_2106,(0,0):C.GC_1846,(0,2):C.GC_1064,(0,3):C.GC_997,(0,4):C.GC_176,(0,7):C.GC_785,(0,5):C.GC_782,(0,6):C.GC_782,(0,1):C.GC_486})

V_231 = Vertex(name = 'V_231',
               particles = [ P.mu__plus__, P.tau__minus__, P.t__tilde__, P.c ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1300,(0,11):C.GC_2121,(0,9):C.GC_2118,(0,10):C.GC_2118,(0,0):C.GC_1852,(0,2):C.GC_1073,(0,3):C.GC_998,(0,4):C.GC_177,(0,7):C.GC_789,(0,5):C.GC_786,(0,6):C.GC_786,(0,1):C.GC_488})

V_232 = Vertex(name = 'V_232',
               particles = [ P.mu__plus__, P.tau__minus__, P.u__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1302,(0,11):C.GC_2101,(0,9):C.GC_2098,(0,10):C.GC_2098,(0,0):C.GC_1842,(0,2):C.GC_1082,(0,3):C.GC_999,(0,4):C.GC_178,(0,7):C.GC_793,(0,5):C.GC_790,(0,6):C.GC_790,(0,1):C.GC_490})

V_233 = Vertex(name = 'V_233',
               particles = [ P.mu__plus__, P.tau__minus__, P.c__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1304,(0,11):C.GC_2113,(0,9):C.GC_2110,(0,10):C.GC_2110,(0,0):C.GC_1848,(0,2):C.GC_1091,(0,3):C.GC_1000,(0,4):C.GC_179,(0,7):C.GC_797,(0,5):C.GC_794,(0,6):C.GC_794,(0,1):C.GC_492})

V_234 = Vertex(name = 'V_234',
               particles = [ P.mu__plus__, P.tau__minus__, P.t__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1306,(0,11):C.GC_2125,(0,9):C.GC_2122,(0,10):C.GC_2122,(0,0):C.GC_1854,(0,2):C.GC_1100,(0,3):C.GC_1001,(0,4):C.GC_180,(0,7):C.GC_801,(0,5):C.GC_798,(0,6):C.GC_798,(0,1):C.GC_494})

V_235 = Vertex(name = 'V_235',
               particles = [ P.tau__plus__, P.tau__minus__, P.u__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1308,(0,11):C.GC_2201,(0,9):C.GC_2198,(0,10):C.GC_2198,(0,0):C.GC_1892,(0,2):C.GC_1029,(0,3):C.GC_1002,(0,4):C.GC_181,(0,7):C.GC_805,(0,5):C.GC_802,(0,6):C.GC_802,(0,1):C.GC_496})

V_236 = Vertex(name = 'V_236',
               particles = [ P.tau__plus__, P.tau__minus__, P.c__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1310,(0,11):C.GC_2213,(0,9):C.GC_2210,(0,10):C.GC_2210,(0,0):C.GC_1898,(0,2):C.GC_1038,(0,3):C.GC_1003,(0,4):C.GC_182,(0,7):C.GC_809,(0,5):C.GC_806,(0,6):C.GC_806,(0,1):C.GC_498})

V_237 = Vertex(name = 'V_237',
               particles = [ P.tau__plus__, P.tau__minus__, P.t__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1312,(0,11):C.GC_2225,(0,9):C.GC_2222,(0,10):C.GC_2222,(0,0):C.GC_1904,(0,2):C.GC_1047,(0,3):C.GC_1004,(0,4):C.GC_183,(0,7):C.GC_813,(0,5):C.GC_810,(0,6):C.GC_810,(0,1):C.GC_500})

V_238 = Vertex(name = 'V_238',
               particles = [ P.tau__plus__, P.tau__minus__, P.u__tilde__, P.c ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1314,(0,11):C.GC_2205,(0,9):C.GC_2202,(0,10):C.GC_2202,(0,0):C.GC_1894,(0,2):C.GC_1056,(0,3):C.GC_1005,(0,4):C.GC_184,(0,7):C.GC_817,(0,5):C.GC_814,(0,6):C.GC_814,(0,1):C.GC_502})

V_239 = Vertex(name = 'V_239',
               particles = [ P.tau__plus__, P.tau__minus__, P.c__tilde__, P.c ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1316,(0,11):C.GC_2217,(0,9):C.GC_2214,(0,10):C.GC_2214,(0,0):C.GC_1900,(0,2):C.GC_1065,(0,3):C.GC_1006,(0,4):C.GC_185,(0,7):C.GC_821,(0,5):C.GC_818,(0,6):C.GC_818,(0,1):C.GC_504})

V_240 = Vertex(name = 'V_240',
               particles = [ P.tau__plus__, P.tau__minus__, P.t__tilde__, P.c ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1318,(0,11):C.GC_2229,(0,9):C.GC_2226,(0,10):C.GC_2226,(0,0):C.GC_1906,(0,2):C.GC_1074,(0,3):C.GC_1007,(0,4):C.GC_186,(0,7):C.GC_825,(0,5):C.GC_822,(0,6):C.GC_822,(0,1):C.GC_506})

V_241 = Vertex(name = 'V_241',
               particles = [ P.tau__plus__, P.tau__minus__, P.u__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1320,(0,11):C.GC_2209,(0,9):C.GC_2206,(0,10):C.GC_2206,(0,0):C.GC_1896,(0,2):C.GC_1083,(0,3):C.GC_1008,(0,4):C.GC_187,(0,7):C.GC_829,(0,5):C.GC_826,(0,6):C.GC_826,(0,1):C.GC_508})

V_242 = Vertex(name = 'V_242',
               particles = [ P.tau__plus__, P.tau__minus__, P.c__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1322,(0,11):C.GC_2221,(0,9):C.GC_2218,(0,10):C.GC_2218,(0,0):C.GC_1902,(0,2):C.GC_1092,(0,3):C.GC_1009,(0,4):C.GC_188,(0,7):C.GC_833,(0,5):C.GC_830,(0,6):C.GC_830,(0,1):C.GC_510})

V_243 = Vertex(name = 'V_243',
               particles = [ P.tau__plus__, P.tau__minus__, P.t__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF10, L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7 ],
               couplings = {(0,8):C.GC_1324,(0,11):C.GC_2233,(0,9):C.GC_2230,(0,10):C.GC_2230,(0,0):C.GC_1908,(0,2):C.GC_1101,(0,3):C.GC_1010,(0,4):C.GC_189,(0,7):C.GC_837,(0,5):C.GC_834,(0,6):C.GC_834,(0,1):C.GC_512})

V_244 = Vertex(name = 'V_244',
               particles = [ P.nu_e__tilde__, P.e__minus__, P.d__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_848,(0,4):C.GC_1912,(0,2):C.GC_1911,(0,3):C.GC_1911,(0,0):C.GC_1749,(0,5):C.GC_1667})

V_245 = Vertex(name = 'V_245',
               particles = [ P.nu_mu__tilde__, P.e__minus__, P.d__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_857,(0,4):C.GC_2020,(0,2):C.GC_2019,(0,3):C.GC_2019,(0,0):C.GC_1803,(0,5):C.GC_1694})

V_246 = Vertex(name = 'V_246',
               particles = [ P.nu_tau__tilde__, P.e__minus__, P.d__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_866,(0,4):C.GC_2128,(0,2):C.GC_2127,(0,3):C.GC_2127,(0,0):C.GC_1857,(0,5):C.GC_1721})

V_247 = Vertex(name = 'V_247',
               particles = [ P.nu_e__tilde__, P.e__minus__, P.d__tilde__, P.c ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_851,(0,4):C.GC_1916,(0,2):C.GC_1915,(0,3):C.GC_1915,(0,0):C.GC_1751,(0,5):C.GC_1668})

V_248 = Vertex(name = 'V_248',
               particles = [ P.nu_mu__tilde__, P.e__minus__, P.d__tilde__, P.c ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_860,(0,4):C.GC_2024,(0,2):C.GC_2023,(0,3):C.GC_2023,(0,0):C.GC_1805,(0,5):C.GC_1695})

V_249 = Vertex(name = 'V_249',
               particles = [ P.nu_tau__tilde__, P.e__minus__, P.d__tilde__, P.c ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_869,(0,4):C.GC_2132,(0,2):C.GC_2131,(0,3):C.GC_2131,(0,0):C.GC_1859,(0,5):C.GC_1722})

V_250 = Vertex(name = 'V_250',
               particles = [ P.nu_e__tilde__, P.e__minus__, P.d__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_854,(0,4):C.GC_1920,(0,2):C.GC_1919,(0,3):C.GC_1919,(0,0):C.GC_1753,(0,5):C.GC_1669})

V_251 = Vertex(name = 'V_251',
               particles = [ P.nu_mu__tilde__, P.e__minus__, P.d__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_863,(0,4):C.GC_2028,(0,2):C.GC_2027,(0,3):C.GC_2027,(0,0):C.GC_1807,(0,5):C.GC_1696})

V_252 = Vertex(name = 'V_252',
               particles = [ P.nu_tau__tilde__, P.e__minus__, P.d__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_872,(0,4):C.GC_2136,(0,2):C.GC_2135,(0,3):C.GC_2135,(0,0):C.GC_1861,(0,5):C.GC_1723})

V_253 = Vertex(name = 'V_253',
               particles = [ P.nu_e__tilde__, P.e__minus__, P.s__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_849,(0,4):C.GC_1924,(0,2):C.GC_1923,(0,3):C.GC_1923,(0,0):C.GC_1755,(0,5):C.GC_1670})

V_254 = Vertex(name = 'V_254',
               particles = [ P.nu_mu__tilde__, P.e__minus__, P.s__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_858,(0,4):C.GC_2032,(0,2):C.GC_2031,(0,3):C.GC_2031,(0,0):C.GC_1809,(0,5):C.GC_1697})

V_255 = Vertex(name = 'V_255',
               particles = [ P.nu_tau__tilde__, P.e__minus__, P.s__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_867,(0,4):C.GC_2140,(0,2):C.GC_2139,(0,3):C.GC_2139,(0,0):C.GC_1863,(0,5):C.GC_1724})

V_256 = Vertex(name = 'V_256',
               particles = [ P.nu_e__tilde__, P.e__minus__, P.s__tilde__, P.c ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_852,(0,4):C.GC_1928,(0,2):C.GC_1927,(0,3):C.GC_1927,(0,0):C.GC_1757,(0,5):C.GC_1671})

V_257 = Vertex(name = 'V_257',
               particles = [ P.nu_mu__tilde__, P.e__minus__, P.s__tilde__, P.c ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_861,(0,4):C.GC_2036,(0,2):C.GC_2035,(0,3):C.GC_2035,(0,0):C.GC_1811,(0,5):C.GC_1698})

V_258 = Vertex(name = 'V_258',
               particles = [ P.nu_tau__tilde__, P.e__minus__, P.s__tilde__, P.c ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_870,(0,4):C.GC_2144,(0,2):C.GC_2143,(0,3):C.GC_2143,(0,0):C.GC_1865,(0,5):C.GC_1725})

V_259 = Vertex(name = 'V_259',
               particles = [ P.nu_e__tilde__, P.e__minus__, P.s__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_855,(0,4):C.GC_1932,(0,2):C.GC_1931,(0,3):C.GC_1931,(0,0):C.GC_1759,(0,5):C.GC_1672})

V_260 = Vertex(name = 'V_260',
               particles = [ P.nu_mu__tilde__, P.e__minus__, P.s__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_864,(0,4):C.GC_2040,(0,2):C.GC_2039,(0,3):C.GC_2039,(0,0):C.GC_1813,(0,5):C.GC_1699})

V_261 = Vertex(name = 'V_261',
               particles = [ P.nu_tau__tilde__, P.e__minus__, P.s__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_873,(0,4):C.GC_2148,(0,2):C.GC_2147,(0,3):C.GC_2147,(0,0):C.GC_1867,(0,5):C.GC_1726})

V_262 = Vertex(name = 'V_262',
               particles = [ P.nu_e__tilde__, P.e__minus__, P.b__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_850,(0,4):C.GC_1936,(0,2):C.GC_1935,(0,3):C.GC_1935,(0,0):C.GC_1761,(0,5):C.GC_1673})

V_263 = Vertex(name = 'V_263',
               particles = [ P.nu_mu__tilde__, P.e__minus__, P.b__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_859,(0,4):C.GC_2044,(0,2):C.GC_2043,(0,3):C.GC_2043,(0,0):C.GC_1815,(0,5):C.GC_1700})

V_264 = Vertex(name = 'V_264',
               particles = [ P.nu_tau__tilde__, P.e__minus__, P.b__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_868,(0,4):C.GC_2152,(0,2):C.GC_2151,(0,3):C.GC_2151,(0,0):C.GC_1869,(0,5):C.GC_1727})

V_265 = Vertex(name = 'V_265',
               particles = [ P.nu_e__tilde__, P.e__minus__, P.b__tilde__, P.c ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_853,(0,4):C.GC_1940,(0,2):C.GC_1939,(0,3):C.GC_1939,(0,0):C.GC_1763,(0,5):C.GC_1674})

V_266 = Vertex(name = 'V_266',
               particles = [ P.nu_mu__tilde__, P.e__minus__, P.b__tilde__, P.c ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_862,(0,4):C.GC_2048,(0,2):C.GC_2047,(0,3):C.GC_2047,(0,0):C.GC_1817,(0,5):C.GC_1701})

V_267 = Vertex(name = 'V_267',
               particles = [ P.nu_tau__tilde__, P.e__minus__, P.b__tilde__, P.c ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_871,(0,4):C.GC_2156,(0,2):C.GC_2155,(0,3):C.GC_2155,(0,0):C.GC_1871,(0,5):C.GC_1728})

V_268 = Vertex(name = 'V_268',
               particles = [ P.nu_e__tilde__, P.e__minus__, P.b__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_856,(0,4):C.GC_1944,(0,2):C.GC_1943,(0,3):C.GC_1943,(0,0):C.GC_1765,(0,5):C.GC_1675})

V_269 = Vertex(name = 'V_269',
               particles = [ P.nu_mu__tilde__, P.e__minus__, P.b__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_865,(0,4):C.GC_2052,(0,2):C.GC_2051,(0,3):C.GC_2051,(0,0):C.GC_1819,(0,5):C.GC_1702})

V_270 = Vertex(name = 'V_270',
               particles = [ P.nu_tau__tilde__, P.e__minus__, P.b__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_874,(0,4):C.GC_2160,(0,2):C.GC_2159,(0,3):C.GC_2159,(0,0):C.GC_1873,(0,5):C.GC_1729})

V_271 = Vertex(name = 'V_271',
               particles = [ P.nu_e__tilde__, P.mu__minus__, P.d__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_875,(0,4):C.GC_1948,(0,2):C.GC_1947,(0,3):C.GC_1947,(0,0):C.GC_1767,(0,5):C.GC_1676})

V_272 = Vertex(name = 'V_272',
               particles = [ P.nu_mu__tilde__, P.mu__minus__, P.d__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_884,(0,4):C.GC_2056,(0,2):C.GC_2055,(0,3):C.GC_2055,(0,0):C.GC_1821,(0,5):C.GC_1703})

V_273 = Vertex(name = 'V_273',
               particles = [ P.nu_tau__tilde__, P.mu__minus__, P.d__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_893,(0,4):C.GC_2164,(0,2):C.GC_2163,(0,3):C.GC_2163,(0,0):C.GC_1875,(0,5):C.GC_1730})

V_274 = Vertex(name = 'V_274',
               particles = [ P.nu_e__tilde__, P.mu__minus__, P.d__tilde__, P.c ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_878,(0,4):C.GC_1952,(0,2):C.GC_1951,(0,3):C.GC_1951,(0,0):C.GC_1769,(0,5):C.GC_1677})

V_275 = Vertex(name = 'V_275',
               particles = [ P.nu_mu__tilde__, P.mu__minus__, P.d__tilde__, P.c ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_887,(0,4):C.GC_2060,(0,2):C.GC_2059,(0,3):C.GC_2059,(0,0):C.GC_1823,(0,5):C.GC_1704})

V_276 = Vertex(name = 'V_276',
               particles = [ P.nu_tau__tilde__, P.mu__minus__, P.d__tilde__, P.c ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_896,(0,4):C.GC_2168,(0,2):C.GC_2167,(0,3):C.GC_2167,(0,0):C.GC_1877,(0,5):C.GC_1731})

V_277 = Vertex(name = 'V_277',
               particles = [ P.nu_e__tilde__, P.mu__minus__, P.d__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_881,(0,4):C.GC_1956,(0,2):C.GC_1955,(0,3):C.GC_1955,(0,0):C.GC_1771,(0,5):C.GC_1678})

V_278 = Vertex(name = 'V_278',
               particles = [ P.nu_mu__tilde__, P.mu__minus__, P.d__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_890,(0,4):C.GC_2064,(0,2):C.GC_2063,(0,3):C.GC_2063,(0,0):C.GC_1825,(0,5):C.GC_1705})

V_279 = Vertex(name = 'V_279',
               particles = [ P.nu_tau__tilde__, P.mu__minus__, P.d__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_899,(0,4):C.GC_2172,(0,2):C.GC_2171,(0,3):C.GC_2171,(0,0):C.GC_1879,(0,5):C.GC_1732})

V_280 = Vertex(name = 'V_280',
               particles = [ P.nu_e__tilde__, P.mu__minus__, P.s__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_876,(0,4):C.GC_1960,(0,2):C.GC_1959,(0,3):C.GC_1959,(0,0):C.GC_1773,(0,5):C.GC_1679})

V_281 = Vertex(name = 'V_281',
               particles = [ P.nu_mu__tilde__, P.mu__minus__, P.s__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_885,(0,4):C.GC_2068,(0,2):C.GC_2067,(0,3):C.GC_2067,(0,0):C.GC_1827,(0,5):C.GC_1706})

V_282 = Vertex(name = 'V_282',
               particles = [ P.nu_tau__tilde__, P.mu__minus__, P.s__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_894,(0,4):C.GC_2176,(0,2):C.GC_2175,(0,3):C.GC_2175,(0,0):C.GC_1881,(0,5):C.GC_1733})

V_283 = Vertex(name = 'V_283',
               particles = [ P.nu_e__tilde__, P.mu__minus__, P.s__tilde__, P.c ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_879,(0,4):C.GC_1964,(0,2):C.GC_1963,(0,3):C.GC_1963,(0,0):C.GC_1775,(0,5):C.GC_1680})

V_284 = Vertex(name = 'V_284',
               particles = [ P.nu_mu__tilde__, P.mu__minus__, P.s__tilde__, P.c ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_888,(0,4):C.GC_2072,(0,2):C.GC_2071,(0,3):C.GC_2071,(0,0):C.GC_1829,(0,5):C.GC_1707})

V_285 = Vertex(name = 'V_285',
               particles = [ P.nu_tau__tilde__, P.mu__minus__, P.s__tilde__, P.c ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_897,(0,4):C.GC_2180,(0,2):C.GC_2179,(0,3):C.GC_2179,(0,0):C.GC_1883,(0,5):C.GC_1734})

V_286 = Vertex(name = 'V_286',
               particles = [ P.nu_e__tilde__, P.mu__minus__, P.s__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_882,(0,4):C.GC_1968,(0,2):C.GC_1967,(0,3):C.GC_1967,(0,0):C.GC_1777,(0,5):C.GC_1681})

V_287 = Vertex(name = 'V_287',
               particles = [ P.nu_mu__tilde__, P.mu__minus__, P.s__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_891,(0,4):C.GC_2076,(0,2):C.GC_2075,(0,3):C.GC_2075,(0,0):C.GC_1831,(0,5):C.GC_1708})

V_288 = Vertex(name = 'V_288',
               particles = [ P.nu_tau__tilde__, P.mu__minus__, P.s__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_900,(0,4):C.GC_2184,(0,2):C.GC_2183,(0,3):C.GC_2183,(0,0):C.GC_1885,(0,5):C.GC_1735})

V_289 = Vertex(name = 'V_289',
               particles = [ P.nu_e__tilde__, P.mu__minus__, P.b__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_877,(0,4):C.GC_1972,(0,2):C.GC_1971,(0,3):C.GC_1971,(0,0):C.GC_1779,(0,5):C.GC_1682})

V_290 = Vertex(name = 'V_290',
               particles = [ P.nu_mu__tilde__, P.mu__minus__, P.b__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_886,(0,4):C.GC_2080,(0,2):C.GC_2079,(0,3):C.GC_2079,(0,0):C.GC_1833,(0,5):C.GC_1709})

V_291 = Vertex(name = 'V_291',
               particles = [ P.nu_tau__tilde__, P.mu__minus__, P.b__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_895,(0,4):C.GC_2188,(0,2):C.GC_2187,(0,3):C.GC_2187,(0,0):C.GC_1887,(0,5):C.GC_1736})

V_292 = Vertex(name = 'V_292',
               particles = [ P.nu_e__tilde__, P.mu__minus__, P.b__tilde__, P.c ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_880,(0,4):C.GC_1976,(0,2):C.GC_1975,(0,3):C.GC_1975,(0,0):C.GC_1781,(0,5):C.GC_1683})

V_293 = Vertex(name = 'V_293',
               particles = [ P.nu_mu__tilde__, P.mu__minus__, P.b__tilde__, P.c ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_889,(0,4):C.GC_2084,(0,2):C.GC_2083,(0,3):C.GC_2083,(0,0):C.GC_1835,(0,5):C.GC_1710})

V_294 = Vertex(name = 'V_294',
               particles = [ P.nu_tau__tilde__, P.mu__minus__, P.b__tilde__, P.c ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_898,(0,4):C.GC_2192,(0,2):C.GC_2191,(0,3):C.GC_2191,(0,0):C.GC_1889,(0,5):C.GC_1737})

V_295 = Vertex(name = 'V_295',
               particles = [ P.nu_e__tilde__, P.mu__minus__, P.b__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_883,(0,4):C.GC_1980,(0,2):C.GC_1979,(0,3):C.GC_1979,(0,0):C.GC_1783,(0,5):C.GC_1684})

V_296 = Vertex(name = 'V_296',
               particles = [ P.nu_mu__tilde__, P.mu__minus__, P.b__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_892,(0,4):C.GC_2088,(0,2):C.GC_2087,(0,3):C.GC_2087,(0,0):C.GC_1837,(0,5):C.GC_1711})

V_297 = Vertex(name = 'V_297',
               particles = [ P.nu_tau__tilde__, P.mu__minus__, P.b__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_901,(0,4):C.GC_2196,(0,2):C.GC_2195,(0,3):C.GC_2195,(0,0):C.GC_1891,(0,5):C.GC_1738})

V_298 = Vertex(name = 'V_298',
               particles = [ P.nu_e__tilde__, P.tau__minus__, P.d__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_902,(0,4):C.GC_1984,(0,2):C.GC_1983,(0,3):C.GC_1983,(0,0):C.GC_1785,(0,5):C.GC_1685})

V_299 = Vertex(name = 'V_299',
               particles = [ P.nu_mu__tilde__, P.tau__minus__, P.d__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_911,(0,4):C.GC_2092,(0,2):C.GC_2091,(0,3):C.GC_2091,(0,0):C.GC_1839,(0,5):C.GC_1712})

V_300 = Vertex(name = 'V_300',
               particles = [ P.nu_tau__tilde__, P.tau__minus__, P.d__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_920,(0,4):C.GC_2200,(0,2):C.GC_2199,(0,3):C.GC_2199,(0,0):C.GC_1893,(0,5):C.GC_1739})

V_301 = Vertex(name = 'V_301',
               particles = [ P.nu_e__tilde__, P.tau__minus__, P.d__tilde__, P.c ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_905,(0,4):C.GC_1988,(0,2):C.GC_1987,(0,3):C.GC_1987,(0,0):C.GC_1787,(0,5):C.GC_1686})

V_302 = Vertex(name = 'V_302',
               particles = [ P.nu_mu__tilde__, P.tau__minus__, P.d__tilde__, P.c ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_914,(0,4):C.GC_2096,(0,2):C.GC_2095,(0,3):C.GC_2095,(0,0):C.GC_1841,(0,5):C.GC_1713})

V_303 = Vertex(name = 'V_303',
               particles = [ P.nu_tau__tilde__, P.tau__minus__, P.d__tilde__, P.c ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_923,(0,4):C.GC_2204,(0,2):C.GC_2203,(0,3):C.GC_2203,(0,0):C.GC_1895,(0,5):C.GC_1740})

V_304 = Vertex(name = 'V_304',
               particles = [ P.nu_e__tilde__, P.tau__minus__, P.d__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_908,(0,4):C.GC_1992,(0,2):C.GC_1991,(0,3):C.GC_1991,(0,0):C.GC_1789,(0,5):C.GC_1687})

V_305 = Vertex(name = 'V_305',
               particles = [ P.nu_mu__tilde__, P.tau__minus__, P.d__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_917,(0,4):C.GC_2100,(0,2):C.GC_2099,(0,3):C.GC_2099,(0,0):C.GC_1843,(0,5):C.GC_1714})

V_306 = Vertex(name = 'V_306',
               particles = [ P.nu_tau__tilde__, P.tau__minus__, P.d__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_926,(0,4):C.GC_2208,(0,2):C.GC_2207,(0,3):C.GC_2207,(0,0):C.GC_1897,(0,5):C.GC_1741})

V_307 = Vertex(name = 'V_307',
               particles = [ P.nu_e__tilde__, P.tau__minus__, P.s__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_903,(0,4):C.GC_1996,(0,2):C.GC_1995,(0,3):C.GC_1995,(0,0):C.GC_1791,(0,5):C.GC_1688})

V_308 = Vertex(name = 'V_308',
               particles = [ P.nu_mu__tilde__, P.tau__minus__, P.s__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_912,(0,4):C.GC_2104,(0,2):C.GC_2103,(0,3):C.GC_2103,(0,0):C.GC_1845,(0,5):C.GC_1715})

V_309 = Vertex(name = 'V_309',
               particles = [ P.nu_tau__tilde__, P.tau__minus__, P.s__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_921,(0,4):C.GC_2212,(0,2):C.GC_2211,(0,3):C.GC_2211,(0,0):C.GC_1899,(0,5):C.GC_1742})

V_310 = Vertex(name = 'V_310',
               particles = [ P.nu_e__tilde__, P.tau__minus__, P.s__tilde__, P.c ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_906,(0,4):C.GC_2000,(0,2):C.GC_1999,(0,3):C.GC_1999,(0,0):C.GC_1793,(0,5):C.GC_1689})

V_311 = Vertex(name = 'V_311',
               particles = [ P.nu_mu__tilde__, P.tau__minus__, P.s__tilde__, P.c ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_915,(0,4):C.GC_2108,(0,2):C.GC_2107,(0,3):C.GC_2107,(0,0):C.GC_1847,(0,5):C.GC_1716})

V_312 = Vertex(name = 'V_312',
               particles = [ P.nu_tau__tilde__, P.tau__minus__, P.s__tilde__, P.c ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_924,(0,4):C.GC_2216,(0,2):C.GC_2215,(0,3):C.GC_2215,(0,0):C.GC_1901,(0,5):C.GC_1743})

V_313 = Vertex(name = 'V_313',
               particles = [ P.nu_e__tilde__, P.tau__minus__, P.s__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_909,(0,4):C.GC_2004,(0,2):C.GC_2003,(0,3):C.GC_2003,(0,0):C.GC_1795,(0,5):C.GC_1690})

V_314 = Vertex(name = 'V_314',
               particles = [ P.nu_mu__tilde__, P.tau__minus__, P.s__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_918,(0,4):C.GC_2112,(0,2):C.GC_2111,(0,3):C.GC_2111,(0,0):C.GC_1849,(0,5):C.GC_1717})

V_315 = Vertex(name = 'V_315',
               particles = [ P.nu_tau__tilde__, P.tau__minus__, P.s__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_927,(0,4):C.GC_2220,(0,2):C.GC_2219,(0,3):C.GC_2219,(0,0):C.GC_1903,(0,5):C.GC_1744})

V_316 = Vertex(name = 'V_316',
               particles = [ P.nu_e__tilde__, P.tau__minus__, P.b__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_904,(0,4):C.GC_2008,(0,2):C.GC_2007,(0,3):C.GC_2007,(0,0):C.GC_1797,(0,5):C.GC_1691})

V_317 = Vertex(name = 'V_317',
               particles = [ P.nu_mu__tilde__, P.tau__minus__, P.b__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_913,(0,4):C.GC_2116,(0,2):C.GC_2115,(0,3):C.GC_2115,(0,0):C.GC_1851,(0,5):C.GC_1718})

V_318 = Vertex(name = 'V_318',
               particles = [ P.nu_tau__tilde__, P.tau__minus__, P.b__tilde__, P.u ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_922,(0,4):C.GC_2224,(0,2):C.GC_2223,(0,3):C.GC_2223,(0,0):C.GC_1905,(0,5):C.GC_1745})

V_319 = Vertex(name = 'V_319',
               particles = [ P.nu_e__tilde__, P.tau__minus__, P.b__tilde__, P.c ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_907,(0,4):C.GC_2012,(0,2):C.GC_2011,(0,3):C.GC_2011,(0,0):C.GC_1799,(0,5):C.GC_1692})

V_320 = Vertex(name = 'V_320',
               particles = [ P.nu_mu__tilde__, P.tau__minus__, P.b__tilde__, P.c ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_916,(0,4):C.GC_2120,(0,2):C.GC_2119,(0,3):C.GC_2119,(0,0):C.GC_1853,(0,5):C.GC_1719})

V_321 = Vertex(name = 'V_321',
               particles = [ P.nu_tau__tilde__, P.tau__minus__, P.b__tilde__, P.c ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_925,(0,4):C.GC_2228,(0,2):C.GC_2227,(0,3):C.GC_2227,(0,0):C.GC_1907,(0,5):C.GC_1746})

V_322 = Vertex(name = 'V_322',
               particles = [ P.nu_e__tilde__, P.tau__minus__, P.b__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_910,(0,4):C.GC_2016,(0,2):C.GC_2015,(0,3):C.GC_2015,(0,0):C.GC_1801,(0,5):C.GC_1693})

V_323 = Vertex(name = 'V_323',
               particles = [ P.nu_mu__tilde__, P.tau__minus__, P.b__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_919,(0,4):C.GC_2124,(0,2):C.GC_2123,(0,3):C.GC_2123,(0,0):C.GC_1855,(0,5):C.GC_1720})

V_324 = Vertex(name = 'V_324',
               particles = [ P.nu_tau__tilde__, P.tau__minus__, P.b__tilde__, P.t ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF1, L.FFFF4, L.FFFF5, L.FFFF6, L.FFFF7, L.FFFF9 ],
               couplings = {(0,1):C.GC_929,(0,4):C.GC_2232,(0,2):C.GC_2231,(0,3):C.GC_2231,(0,0):C.GC_1909,(0,5):C.GC_1747})

V_325 = Vertex(name = 'V_325',
               particles = [ P.e__plus__, P.e__minus__, P.d__tilde__, P.d ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1165,(0,0):C.GC_1021,(0,1):C.GC_190,(0,2):C.GC_28,(0,4):C.GC_271,(0,5):C.GC_1667})

V_326 = Vertex(name = 'V_326',
               particles = [ P.mu__plus__, P.e__minus__, P.d__tilde__, P.d ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1183,(0,0):C.GC_1022,(0,1):C.GC_199,(0,2):C.GC_37,(0,4):C.GC_280,(0,5):C.GC_1694})

V_327 = Vertex(name = 'V_327',
               particles = [ P.tau__plus__, P.e__minus__, P.d__tilde__, P.d ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1201,(0,0):C.GC_1023,(0,1):C.GC_208,(0,2):C.GC_46,(0,4):C.GC_289,(0,5):C.GC_1721})

V_328 = Vertex(name = 'V_328',
               particles = [ P.e__plus__, P.mu__minus__, P.d__tilde__, P.d ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1219,(0,0):C.GC_1024,(0,1):C.GC_217,(0,2):C.GC_55,(0,4):C.GC_298,(0,5):C.GC_1676})

V_329 = Vertex(name = 'V_329',
               particles = [ P.mu__plus__, P.mu__minus__, P.d__tilde__, P.d ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1237,(0,0):C.GC_1025,(0,1):C.GC_226,(0,2):C.GC_64,(0,4):C.GC_307,(0,5):C.GC_1703})

V_330 = Vertex(name = 'V_330',
               particles = [ P.tau__plus__, P.mu__minus__, P.d__tilde__, P.d ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1255,(0,0):C.GC_1026,(0,1):C.GC_235,(0,2):C.GC_73,(0,4):C.GC_316,(0,5):C.GC_1730})

V_331 = Vertex(name = 'V_331',
               particles = [ P.e__plus__, P.tau__minus__, P.d__tilde__, P.d ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1273,(0,0):C.GC_1027,(0,1):C.GC_244,(0,2):C.GC_82,(0,4):C.GC_325,(0,5):C.GC_1685})

V_332 = Vertex(name = 'V_332',
               particles = [ P.mu__plus__, P.tau__minus__, P.d__tilde__, P.d ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1291,(0,0):C.GC_1028,(0,1):C.GC_253,(0,2):C.GC_91,(0,4):C.GC_334,(0,5):C.GC_1712})

V_333 = Vertex(name = 'V_333',
               particles = [ P.tau__plus__, P.tau__minus__, P.d__tilde__, P.d ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1309,(0,0):C.GC_1029,(0,1):C.GC_262,(0,2):C.GC_100,(0,4):C.GC_343,(0,5):C.GC_1739})

V_334 = Vertex(name = 'V_334',
               particles = [ P.e__plus__, P.e__minus__, P.s__tilde__, P.d ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1167,(0,0):C.GC_1030,(0,1):C.GC_191,(0,2):C.GC_29,(0,4):C.GC_272,(0,5):C.GC_1670})

V_335 = Vertex(name = 'V_335',
               particles = [ P.mu__plus__, P.e__minus__, P.s__tilde__, P.d ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1185,(0,0):C.GC_1031,(0,1):C.GC_200,(0,2):C.GC_38,(0,4):C.GC_281,(0,5):C.GC_1697})

V_336 = Vertex(name = 'V_336',
               particles = [ P.tau__plus__, P.e__minus__, P.s__tilde__, P.d ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1203,(0,0):C.GC_1032,(0,1):C.GC_209,(0,2):C.GC_47,(0,4):C.GC_290,(0,5):C.GC_1724})

V_337 = Vertex(name = 'V_337',
               particles = [ P.e__plus__, P.mu__minus__, P.s__tilde__, P.d ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1221,(0,0):C.GC_1033,(0,1):C.GC_218,(0,2):C.GC_56,(0,4):C.GC_299,(0,5):C.GC_1679})

V_338 = Vertex(name = 'V_338',
               particles = [ P.mu__plus__, P.mu__minus__, P.s__tilde__, P.d ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1239,(0,0):C.GC_1034,(0,1):C.GC_227,(0,2):C.GC_65,(0,4):C.GC_308,(0,5):C.GC_1706})

V_339 = Vertex(name = 'V_339',
               particles = [ P.tau__plus__, P.mu__minus__, P.s__tilde__, P.d ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1257,(0,0):C.GC_1035,(0,1):C.GC_236,(0,2):C.GC_74,(0,4):C.GC_317,(0,5):C.GC_1733})

V_340 = Vertex(name = 'V_340',
               particles = [ P.e__plus__, P.tau__minus__, P.s__tilde__, P.d ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1275,(0,0):C.GC_1036,(0,1):C.GC_245,(0,2):C.GC_83,(0,4):C.GC_326,(0,5):C.GC_1688})

V_341 = Vertex(name = 'V_341',
               particles = [ P.mu__plus__, P.tau__minus__, P.s__tilde__, P.d ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1293,(0,0):C.GC_1037,(0,1):C.GC_254,(0,2):C.GC_92,(0,4):C.GC_335,(0,5):C.GC_1715})

V_342 = Vertex(name = 'V_342',
               particles = [ P.tau__plus__, P.tau__minus__, P.s__tilde__, P.d ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1311,(0,0):C.GC_1038,(0,1):C.GC_263,(0,2):C.GC_101,(0,4):C.GC_344,(0,5):C.GC_1742})

V_343 = Vertex(name = 'V_343',
               particles = [ P.e__plus__, P.e__minus__, P.b__tilde__, P.d ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1169,(0,0):C.GC_1039,(0,1):C.GC_192,(0,2):C.GC_30,(0,4):C.GC_273,(0,5):C.GC_1673})

V_344 = Vertex(name = 'V_344',
               particles = [ P.mu__plus__, P.e__minus__, P.b__tilde__, P.d ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1187,(0,0):C.GC_1040,(0,1):C.GC_201,(0,2):C.GC_39,(0,4):C.GC_282,(0,5):C.GC_1700})

V_345 = Vertex(name = 'V_345',
               particles = [ P.tau__plus__, P.e__minus__, P.b__tilde__, P.d ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1205,(0,0):C.GC_1041,(0,1):C.GC_210,(0,2):C.GC_48,(0,4):C.GC_291,(0,5):C.GC_1727})

V_346 = Vertex(name = 'V_346',
               particles = [ P.e__plus__, P.mu__minus__, P.b__tilde__, P.d ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1223,(0,0):C.GC_1042,(0,1):C.GC_219,(0,2):C.GC_57,(0,4):C.GC_300,(0,5):C.GC_1682})

V_347 = Vertex(name = 'V_347',
               particles = [ P.mu__plus__, P.mu__minus__, P.b__tilde__, P.d ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1241,(0,0):C.GC_1043,(0,1):C.GC_228,(0,2):C.GC_66,(0,4):C.GC_309,(0,5):C.GC_1709})

V_348 = Vertex(name = 'V_348',
               particles = [ P.tau__plus__, P.mu__minus__, P.b__tilde__, P.d ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1259,(0,0):C.GC_1044,(0,1):C.GC_237,(0,2):C.GC_75,(0,4):C.GC_318,(0,5):C.GC_1736})

V_349 = Vertex(name = 'V_349',
               particles = [ P.e__plus__, P.tau__minus__, P.b__tilde__, P.d ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1277,(0,0):C.GC_1045,(0,1):C.GC_246,(0,2):C.GC_84,(0,4):C.GC_327,(0,5):C.GC_1691})

V_350 = Vertex(name = 'V_350',
               particles = [ P.mu__plus__, P.tau__minus__, P.b__tilde__, P.d ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1295,(0,0):C.GC_1046,(0,1):C.GC_255,(0,2):C.GC_93,(0,4):C.GC_336,(0,5):C.GC_1718})

V_351 = Vertex(name = 'V_351',
               particles = [ P.tau__plus__, P.tau__minus__, P.b__tilde__, P.d ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1313,(0,0):C.GC_1047,(0,1):C.GC_264,(0,2):C.GC_102,(0,4):C.GC_345,(0,5):C.GC_1745})

V_352 = Vertex(name = 'V_352',
               particles = [ P.e__plus__, P.e__minus__, P.d__tilde__, P.s ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1171,(0,0):C.GC_1048,(0,1):C.GC_193,(0,2):C.GC_31,(0,4):C.GC_274,(0,5):C.GC_1668})

V_353 = Vertex(name = 'V_353',
               particles = [ P.mu__plus__, P.e__minus__, P.d__tilde__, P.s ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1189,(0,0):C.GC_1049,(0,1):C.GC_202,(0,2):C.GC_40,(0,4):C.GC_283,(0,5):C.GC_1695})

V_354 = Vertex(name = 'V_354',
               particles = [ P.tau__plus__, P.e__minus__, P.d__tilde__, P.s ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1207,(0,0):C.GC_1050,(0,1):C.GC_211,(0,2):C.GC_49,(0,4):C.GC_292,(0,5):C.GC_1722})

V_355 = Vertex(name = 'V_355',
               particles = [ P.e__plus__, P.mu__minus__, P.d__tilde__, P.s ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1225,(0,0):C.GC_1051,(0,1):C.GC_220,(0,2):C.GC_58,(0,4):C.GC_301,(0,5):C.GC_1677})

V_356 = Vertex(name = 'V_356',
               particles = [ P.mu__plus__, P.mu__minus__, P.d__tilde__, P.s ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1243,(0,0):C.GC_1052,(0,1):C.GC_229,(0,2):C.GC_67,(0,4):C.GC_310,(0,5):C.GC_1704})

V_357 = Vertex(name = 'V_357',
               particles = [ P.tau__plus__, P.mu__minus__, P.d__tilde__, P.s ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1261,(0,0):C.GC_1053,(0,1):C.GC_238,(0,2):C.GC_76,(0,4):C.GC_319,(0,5):C.GC_1731})

V_358 = Vertex(name = 'V_358',
               particles = [ P.e__plus__, P.tau__minus__, P.d__tilde__, P.s ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1279,(0,0):C.GC_1054,(0,1):C.GC_247,(0,2):C.GC_85,(0,4):C.GC_328,(0,5):C.GC_1686})

V_359 = Vertex(name = 'V_359',
               particles = [ P.mu__plus__, P.tau__minus__, P.d__tilde__, P.s ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1297,(0,0):C.GC_1055,(0,1):C.GC_256,(0,2):C.GC_94,(0,4):C.GC_337,(0,5):C.GC_1713})

V_360 = Vertex(name = 'V_360',
               particles = [ P.tau__plus__, P.tau__minus__, P.d__tilde__, P.s ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1315,(0,0):C.GC_1056,(0,1):C.GC_265,(0,2):C.GC_103,(0,4):C.GC_346,(0,5):C.GC_1740})

V_361 = Vertex(name = 'V_361',
               particles = [ P.e__plus__, P.e__minus__, P.s__tilde__, P.s ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1173,(0,0):C.GC_1057,(0,1):C.GC_194,(0,2):C.GC_32,(0,4):C.GC_275,(0,5):C.GC_1671})

V_362 = Vertex(name = 'V_362',
               particles = [ P.mu__plus__, P.e__minus__, P.s__tilde__, P.s ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1191,(0,0):C.GC_1058,(0,1):C.GC_203,(0,2):C.GC_41,(0,4):C.GC_284,(0,5):C.GC_1698})

V_363 = Vertex(name = 'V_363',
               particles = [ P.tau__plus__, P.e__minus__, P.s__tilde__, P.s ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1209,(0,0):C.GC_1059,(0,1):C.GC_212,(0,2):C.GC_50,(0,4):C.GC_293,(0,5):C.GC_1725})

V_364 = Vertex(name = 'V_364',
               particles = [ P.e__plus__, P.mu__minus__, P.s__tilde__, P.s ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1227,(0,0):C.GC_1060,(0,1):C.GC_221,(0,2):C.GC_59,(0,4):C.GC_302,(0,5):C.GC_1680})

V_365 = Vertex(name = 'V_365',
               particles = [ P.mu__plus__, P.mu__minus__, P.s__tilde__, P.s ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1245,(0,0):C.GC_1061,(0,1):C.GC_230,(0,2):C.GC_68,(0,4):C.GC_311,(0,5):C.GC_1707})

V_366 = Vertex(name = 'V_366',
               particles = [ P.tau__plus__, P.mu__minus__, P.s__tilde__, P.s ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1263,(0,0):C.GC_1062,(0,1):C.GC_239,(0,2):C.GC_77,(0,4):C.GC_320,(0,5):C.GC_1734})

V_367 = Vertex(name = 'V_367',
               particles = [ P.e__plus__, P.tau__minus__, P.s__tilde__, P.s ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1281,(0,0):C.GC_1063,(0,1):C.GC_248,(0,2):C.GC_86,(0,4):C.GC_329,(0,5):C.GC_1689})

V_368 = Vertex(name = 'V_368',
               particles = [ P.mu__plus__, P.tau__minus__, P.s__tilde__, P.s ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1299,(0,0):C.GC_1064,(0,1):C.GC_257,(0,2):C.GC_95,(0,4):C.GC_338,(0,5):C.GC_1716})

V_369 = Vertex(name = 'V_369',
               particles = [ P.tau__plus__, P.tau__minus__, P.s__tilde__, P.s ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1317,(0,0):C.GC_1065,(0,1):C.GC_266,(0,2):C.GC_104,(0,4):C.GC_347,(0,5):C.GC_1743})

V_370 = Vertex(name = 'V_370',
               particles = [ P.e__plus__, P.e__minus__, P.b__tilde__, P.s ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1175,(0,0):C.GC_1066,(0,1):C.GC_195,(0,2):C.GC_33,(0,4):C.GC_276,(0,5):C.GC_1674})

V_371 = Vertex(name = 'V_371',
               particles = [ P.mu__plus__, P.e__minus__, P.b__tilde__, P.s ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1193,(0,0):C.GC_1067,(0,1):C.GC_204,(0,2):C.GC_42,(0,4):C.GC_285,(0,5):C.GC_1701})

V_372 = Vertex(name = 'V_372',
               particles = [ P.tau__plus__, P.e__minus__, P.b__tilde__, P.s ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1211,(0,0):C.GC_1068,(0,1):C.GC_213,(0,2):C.GC_51,(0,4):C.GC_294,(0,5):C.GC_1728})

V_373 = Vertex(name = 'V_373',
               particles = [ P.e__plus__, P.mu__minus__, P.b__tilde__, P.s ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1229,(0,0):C.GC_1069,(0,1):C.GC_222,(0,2):C.GC_60,(0,4):C.GC_303,(0,5):C.GC_1683})

V_374 = Vertex(name = 'V_374',
               particles = [ P.mu__plus__, P.mu__minus__, P.b__tilde__, P.s ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1247,(0,0):C.GC_1070,(0,1):C.GC_231,(0,2):C.GC_69,(0,4):C.GC_312,(0,5):C.GC_1710})

V_375 = Vertex(name = 'V_375',
               particles = [ P.tau__plus__, P.mu__minus__, P.b__tilde__, P.s ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1265,(0,0):C.GC_1071,(0,1):C.GC_240,(0,2):C.GC_78,(0,4):C.GC_321,(0,5):C.GC_1737})

V_376 = Vertex(name = 'V_376',
               particles = [ P.e__plus__, P.tau__minus__, P.b__tilde__, P.s ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1283,(0,0):C.GC_1072,(0,1):C.GC_249,(0,2):C.GC_87,(0,4):C.GC_330,(0,5):C.GC_1692})

V_377 = Vertex(name = 'V_377',
               particles = [ P.mu__plus__, P.tau__minus__, P.b__tilde__, P.s ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1301,(0,0):C.GC_1073,(0,1):C.GC_258,(0,2):C.GC_96,(0,4):C.GC_339,(0,5):C.GC_1719})

V_378 = Vertex(name = 'V_378',
               particles = [ P.tau__plus__, P.tau__minus__, P.b__tilde__, P.s ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1319,(0,0):C.GC_1074,(0,1):C.GC_267,(0,2):C.GC_105,(0,4):C.GC_348,(0,5):C.GC_1746})

V_379 = Vertex(name = 'V_379',
               particles = [ P.e__plus__, P.e__minus__, P.d__tilde__, P.b ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1177,(0,0):C.GC_1075,(0,1):C.GC_196,(0,2):C.GC_34,(0,4):C.GC_277,(0,5):C.GC_1669})

V_380 = Vertex(name = 'V_380',
               particles = [ P.mu__plus__, P.e__minus__, P.d__tilde__, P.b ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1195,(0,0):C.GC_1076,(0,1):C.GC_205,(0,2):C.GC_43,(0,4):C.GC_286,(0,5):C.GC_1696})

V_381 = Vertex(name = 'V_381',
               particles = [ P.tau__plus__, P.e__minus__, P.d__tilde__, P.b ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1213,(0,0):C.GC_1077,(0,1):C.GC_214,(0,2):C.GC_52,(0,4):C.GC_295,(0,5):C.GC_1723})

V_382 = Vertex(name = 'V_382',
               particles = [ P.e__plus__, P.mu__minus__, P.d__tilde__, P.b ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1231,(0,0):C.GC_1078,(0,1):C.GC_223,(0,2):C.GC_61,(0,4):C.GC_304,(0,5):C.GC_1678})

V_383 = Vertex(name = 'V_383',
               particles = [ P.mu__plus__, P.mu__minus__, P.d__tilde__, P.b ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1249,(0,0):C.GC_1079,(0,1):C.GC_232,(0,2):C.GC_70,(0,4):C.GC_313,(0,5):C.GC_1705})

V_384 = Vertex(name = 'V_384',
               particles = [ P.tau__plus__, P.mu__minus__, P.d__tilde__, P.b ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1267,(0,0):C.GC_1080,(0,1):C.GC_241,(0,2):C.GC_79,(0,4):C.GC_322,(0,5):C.GC_1732})

V_385 = Vertex(name = 'V_385',
               particles = [ P.e__plus__, P.tau__minus__, P.d__tilde__, P.b ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1285,(0,0):C.GC_1081,(0,1):C.GC_250,(0,2):C.GC_88,(0,4):C.GC_331,(0,5):C.GC_1687})

V_386 = Vertex(name = 'V_386',
               particles = [ P.mu__plus__, P.tau__minus__, P.d__tilde__, P.b ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1303,(0,0):C.GC_1082,(0,1):C.GC_259,(0,2):C.GC_97,(0,4):C.GC_340,(0,5):C.GC_1714})

V_387 = Vertex(name = 'V_387',
               particles = [ P.tau__plus__, P.tau__minus__, P.d__tilde__, P.b ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1321,(0,0):C.GC_1083,(0,1):C.GC_268,(0,2):C.GC_106,(0,4):C.GC_349,(0,5):C.GC_1741})

V_388 = Vertex(name = 'V_388',
               particles = [ P.e__plus__, P.e__minus__, P.s__tilde__, P.b ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1179,(0,0):C.GC_1084,(0,1):C.GC_197,(0,2):C.GC_35,(0,4):C.GC_278,(0,5):C.GC_1672})

V_389 = Vertex(name = 'V_389',
               particles = [ P.mu__plus__, P.e__minus__, P.s__tilde__, P.b ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1197,(0,0):C.GC_1085,(0,1):C.GC_206,(0,2):C.GC_44,(0,4):C.GC_287,(0,5):C.GC_1699})

V_390 = Vertex(name = 'V_390',
               particles = [ P.tau__plus__, P.e__minus__, P.s__tilde__, P.b ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1215,(0,0):C.GC_1086,(0,1):C.GC_215,(0,2):C.GC_53,(0,4):C.GC_296,(0,5):C.GC_1726})

V_391 = Vertex(name = 'V_391',
               particles = [ P.e__plus__, P.mu__minus__, P.s__tilde__, P.b ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1233,(0,0):C.GC_1087,(0,1):C.GC_224,(0,2):C.GC_62,(0,4):C.GC_305,(0,5):C.GC_1681})

V_392 = Vertex(name = 'V_392',
               particles = [ P.mu__plus__, P.mu__minus__, P.s__tilde__, P.b ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1251,(0,0):C.GC_1088,(0,1):C.GC_233,(0,2):C.GC_71,(0,4):C.GC_314,(0,5):C.GC_1708})

V_393 = Vertex(name = 'V_393',
               particles = [ P.tau__plus__, P.mu__minus__, P.s__tilde__, P.b ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1269,(0,0):C.GC_1089,(0,1):C.GC_242,(0,2):C.GC_80,(0,4):C.GC_323,(0,5):C.GC_1735})

V_394 = Vertex(name = 'V_394',
               particles = [ P.e__plus__, P.tau__minus__, P.s__tilde__, P.b ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1287,(0,0):C.GC_1090,(0,1):C.GC_251,(0,2):C.GC_89,(0,4):C.GC_332,(0,5):C.GC_1690})

V_395 = Vertex(name = 'V_395',
               particles = [ P.mu__plus__, P.tau__minus__, P.s__tilde__, P.b ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1305,(0,0):C.GC_1091,(0,1):C.GC_260,(0,2):C.GC_98,(0,4):C.GC_341,(0,5):C.GC_1717})

V_396 = Vertex(name = 'V_396',
               particles = [ P.tau__plus__, P.tau__minus__, P.s__tilde__, P.b ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1323,(0,0):C.GC_1092,(0,1):C.GC_269,(0,2):C.GC_107,(0,4):C.GC_350,(0,5):C.GC_1744})

V_397 = Vertex(name = 'V_397',
               particles = [ P.e__plus__, P.e__minus__, P.b__tilde__, P.b ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1181,(0,0):C.GC_1093,(0,1):C.GC_198,(0,2):C.GC_36,(0,4):C.GC_279,(0,5):C.GC_1675})

V_398 = Vertex(name = 'V_398',
               particles = [ P.mu__plus__, P.e__minus__, P.b__tilde__, P.b ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1199,(0,0):C.GC_1094,(0,1):C.GC_207,(0,2):C.GC_45,(0,4):C.GC_288,(0,5):C.GC_1702})

V_399 = Vertex(name = 'V_399',
               particles = [ P.tau__plus__, P.e__minus__, P.b__tilde__, P.b ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1217,(0,0):C.GC_1095,(0,1):C.GC_216,(0,2):C.GC_54,(0,4):C.GC_297,(0,5):C.GC_1729})

V_400 = Vertex(name = 'V_400',
               particles = [ P.e__plus__, P.mu__minus__, P.b__tilde__, P.b ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1235,(0,0):C.GC_1096,(0,1):C.GC_225,(0,2):C.GC_63,(0,4):C.GC_306,(0,5):C.GC_1684})

V_401 = Vertex(name = 'V_401',
               particles = [ P.mu__plus__, P.mu__minus__, P.b__tilde__, P.b ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1253,(0,0):C.GC_1097,(0,1):C.GC_234,(0,2):C.GC_72,(0,4):C.GC_315,(0,5):C.GC_1711})

V_402 = Vertex(name = 'V_402',
               particles = [ P.tau__plus__, P.mu__minus__, P.b__tilde__, P.b ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1271,(0,0):C.GC_1098,(0,1):C.GC_243,(0,2):C.GC_81,(0,4):C.GC_324,(0,5):C.GC_1738})

V_403 = Vertex(name = 'V_403',
               particles = [ P.e__plus__, P.tau__minus__, P.b__tilde__, P.b ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1289,(0,0):C.GC_1099,(0,1):C.GC_252,(0,2):C.GC_90,(0,4):C.GC_333,(0,5):C.GC_1693})

V_404 = Vertex(name = 'V_404',
               particles = [ P.mu__plus__, P.tau__minus__, P.b__tilde__, P.b ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1307,(0,0):C.GC_1100,(0,1):C.GC_261,(0,2):C.GC_99,(0,4):C.GC_342,(0,5):C.GC_1720})

V_405 = Vertex(name = 'V_405',
               particles = [ P.tau__plus__, P.tau__minus__, P.b__tilde__, P.b ],
               color = [ 'Identity(3,4)' ],
               lorentz = [ L.FFFF11, L.FFFF12, L.FFFF13, L.FFFF4, L.FFFF8, L.FFFF9 ],
               couplings = {(0,3):C.GC_1325,(0,0):C.GC_1101,(0,1):C.GC_270,(0,2):C.GC_108,(0,4):C.GC_351,(0,5):C.GC_1747})

V_406 = Vertex(name = 'V_406',
               particles = [ P.u__tilde__, P.d, P.e__plus__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_848,(0,3):C.GC_516,(0,1):C.GC_515,(0,2):C.GC_515,(0,5):C.GC_271,(0,0):C.GC_353})

V_407 = Vertex(name = 'V_407',
               particles = [ P.c__tilde__, P.d, P.e__plus__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_849,(0,3):C.GC_520,(0,1):C.GC_519,(0,2):C.GC_519,(0,5):C.GC_272,(0,0):C.GC_355})

V_408 = Vertex(name = 'V_408',
               particles = [ P.t__tilde__, P.d, P.e__plus__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_850,(0,3):C.GC_524,(0,1):C.GC_523,(0,2):C.GC_523,(0,5):C.GC_273,(0,0):C.GC_357})

V_409 = Vertex(name = 'V_409',
               particles = [ P.u__tilde__, P.d, P.e__plus__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_875,(0,3):C.GC_624,(0,1):C.GC_623,(0,2):C.GC_623,(0,5):C.GC_298,(0,0):C.GC_407})

V_410 = Vertex(name = 'V_410',
               particles = [ P.c__tilde__, P.d, P.e__plus__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_876,(0,3):C.GC_628,(0,1):C.GC_627,(0,2):C.GC_627,(0,5):C.GC_299,(0,0):C.GC_409})

V_411 = Vertex(name = 'V_411',
               particles = [ P.t__tilde__, P.d, P.e__plus__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_877,(0,3):C.GC_632,(0,1):C.GC_631,(0,2):C.GC_631,(0,5):C.GC_300,(0,0):C.GC_411})

V_412 = Vertex(name = 'V_412',
               particles = [ P.u__tilde__, P.d, P.e__plus__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_902,(0,3):C.GC_732,(0,1):C.GC_731,(0,2):C.GC_731,(0,5):C.GC_325,(0,0):C.GC_461})

V_413 = Vertex(name = 'V_413',
               particles = [ P.c__tilde__, P.d, P.e__plus__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_903,(0,3):C.GC_736,(0,1):C.GC_735,(0,2):C.GC_735,(0,5):C.GC_326,(0,0):C.GC_463})

V_414 = Vertex(name = 'V_414',
               particles = [ P.t__tilde__, P.d, P.e__plus__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_904,(0,3):C.GC_740,(0,1):C.GC_739,(0,2):C.GC_739,(0,5):C.GC_327,(0,0):C.GC_465})

V_415 = Vertex(name = 'V_415',
               particles = [ P.u__tilde__, P.d, P.mu__plus__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_857,(0,3):C.GC_552,(0,1):C.GC_551,(0,2):C.GC_551,(0,5):C.GC_280,(0,0):C.GC_371})

V_416 = Vertex(name = 'V_416',
               particles = [ P.c__tilde__, P.d, P.mu__plus__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_858,(0,3):C.GC_556,(0,1):C.GC_555,(0,2):C.GC_555,(0,5):C.GC_281,(0,0):C.GC_373})

V_417 = Vertex(name = 'V_417',
               particles = [ P.t__tilde__, P.d, P.mu__plus__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_859,(0,3):C.GC_560,(0,1):C.GC_559,(0,2):C.GC_559,(0,5):C.GC_282,(0,0):C.GC_375})

V_418 = Vertex(name = 'V_418',
               particles = [ P.u__tilde__, P.d, P.mu__plus__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_884,(0,3):C.GC_660,(0,1):C.GC_659,(0,2):C.GC_659,(0,5):C.GC_307,(0,0):C.GC_425})

V_419 = Vertex(name = 'V_419',
               particles = [ P.c__tilde__, P.d, P.mu__plus__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_885,(0,3):C.GC_664,(0,1):C.GC_663,(0,2):C.GC_663,(0,5):C.GC_308,(0,0):C.GC_427})

V_420 = Vertex(name = 'V_420',
               particles = [ P.t__tilde__, P.d, P.mu__plus__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_886,(0,3):C.GC_668,(0,1):C.GC_667,(0,2):C.GC_667,(0,5):C.GC_309,(0,0):C.GC_429})

V_421 = Vertex(name = 'V_421',
               particles = [ P.u__tilde__, P.d, P.mu__plus__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_911,(0,3):C.GC_768,(0,1):C.GC_767,(0,2):C.GC_767,(0,5):C.GC_334,(0,0):C.GC_479})

V_422 = Vertex(name = 'V_422',
               particles = [ P.c__tilde__, P.d, P.mu__plus__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_912,(0,3):C.GC_772,(0,1):C.GC_771,(0,2):C.GC_771,(0,5):C.GC_335,(0,0):C.GC_481})

V_423 = Vertex(name = 'V_423',
               particles = [ P.t__tilde__, P.d, P.mu__plus__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_913,(0,3):C.GC_776,(0,1):C.GC_775,(0,2):C.GC_775,(0,5):C.GC_336,(0,0):C.GC_483})

V_424 = Vertex(name = 'V_424',
               particles = [ P.u__tilde__, P.d, P.tau__plus__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_866,(0,3):C.GC_588,(0,1):C.GC_587,(0,2):C.GC_587,(0,5):C.GC_289,(0,0):C.GC_389})

V_425 = Vertex(name = 'V_425',
               particles = [ P.c__tilde__, P.d, P.tau__plus__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_867,(0,3):C.GC_592,(0,1):C.GC_591,(0,2):C.GC_591,(0,5):C.GC_290,(0,0):C.GC_391})

V_426 = Vertex(name = 'V_426',
               particles = [ P.t__tilde__, P.d, P.tau__plus__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_868,(0,3):C.GC_596,(0,1):C.GC_595,(0,2):C.GC_595,(0,5):C.GC_291,(0,0):C.GC_393})

V_427 = Vertex(name = 'V_427',
               particles = [ P.u__tilde__, P.d, P.tau__plus__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_893,(0,3):C.GC_696,(0,1):C.GC_695,(0,2):C.GC_695,(0,5):C.GC_316,(0,0):C.GC_443})

V_428 = Vertex(name = 'V_428',
               particles = [ P.c__tilde__, P.d, P.tau__plus__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_894,(0,3):C.GC_700,(0,1):C.GC_699,(0,2):C.GC_699,(0,5):C.GC_317,(0,0):C.GC_445})

V_429 = Vertex(name = 'V_429',
               particles = [ P.t__tilde__, P.d, P.tau__plus__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_895,(0,3):C.GC_704,(0,1):C.GC_703,(0,2):C.GC_703,(0,5):C.GC_318,(0,0):C.GC_447})

V_430 = Vertex(name = 'V_430',
               particles = [ P.u__tilde__, P.d, P.tau__plus__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_920,(0,3):C.GC_804,(0,1):C.GC_803,(0,2):C.GC_803,(0,5):C.GC_343,(0,0):C.GC_497})

V_431 = Vertex(name = 'V_431',
               particles = [ P.c__tilde__, P.d, P.tau__plus__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_921,(0,3):C.GC_808,(0,1):C.GC_807,(0,2):C.GC_807,(0,5):C.GC_344,(0,0):C.GC_499})

V_432 = Vertex(name = 'V_432',
               particles = [ P.t__tilde__, P.d, P.tau__plus__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_922,(0,3):C.GC_812,(0,1):C.GC_811,(0,2):C.GC_811,(0,5):C.GC_345,(0,0):C.GC_501})

V_433 = Vertex(name = 'V_433',
               particles = [ P.u__tilde__, P.s, P.e__plus__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_851,(0,3):C.GC_528,(0,1):C.GC_527,(0,2):C.GC_527,(0,5):C.GC_274,(0,0):C.GC_359})

V_434 = Vertex(name = 'V_434',
               particles = [ P.c__tilde__, P.s, P.e__plus__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_852,(0,3):C.GC_532,(0,1):C.GC_531,(0,2):C.GC_531,(0,5):C.GC_275,(0,0):C.GC_361})

V_435 = Vertex(name = 'V_435',
               particles = [ P.t__tilde__, P.s, P.e__plus__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_853,(0,3):C.GC_536,(0,1):C.GC_535,(0,2):C.GC_535,(0,5):C.GC_276,(0,0):C.GC_363})

V_436 = Vertex(name = 'V_436',
               particles = [ P.u__tilde__, P.s, P.e__plus__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_878,(0,3):C.GC_636,(0,1):C.GC_635,(0,2):C.GC_635,(0,5):C.GC_301,(0,0):C.GC_413})

V_437 = Vertex(name = 'V_437',
               particles = [ P.c__tilde__, P.s, P.e__plus__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_879,(0,3):C.GC_640,(0,1):C.GC_639,(0,2):C.GC_639,(0,5):C.GC_302,(0,0):C.GC_415})

V_438 = Vertex(name = 'V_438',
               particles = [ P.t__tilde__, P.s, P.e__plus__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_880,(0,3):C.GC_644,(0,1):C.GC_643,(0,2):C.GC_643,(0,5):C.GC_303,(0,0):C.GC_417})

V_439 = Vertex(name = 'V_439',
               particles = [ P.u__tilde__, P.s, P.e__plus__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_905,(0,3):C.GC_744,(0,1):C.GC_743,(0,2):C.GC_743,(0,5):C.GC_328,(0,0):C.GC_467})

V_440 = Vertex(name = 'V_440',
               particles = [ P.c__tilde__, P.s, P.e__plus__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_906,(0,3):C.GC_748,(0,1):C.GC_747,(0,2):C.GC_747,(0,5):C.GC_329,(0,0):C.GC_469})

V_441 = Vertex(name = 'V_441',
               particles = [ P.t__tilde__, P.s, P.e__plus__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_907,(0,3):C.GC_752,(0,1):C.GC_751,(0,2):C.GC_751,(0,5):C.GC_330,(0,0):C.GC_471})

V_442 = Vertex(name = 'V_442',
               particles = [ P.u__tilde__, P.s, P.mu__plus__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_860,(0,3):C.GC_564,(0,1):C.GC_563,(0,2):C.GC_563,(0,5):C.GC_283,(0,0):C.GC_377})

V_443 = Vertex(name = 'V_443',
               particles = [ P.c__tilde__, P.s, P.mu__plus__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_861,(0,3):C.GC_568,(0,1):C.GC_567,(0,2):C.GC_567,(0,5):C.GC_284,(0,0):C.GC_379})

V_444 = Vertex(name = 'V_444',
               particles = [ P.t__tilde__, P.s, P.mu__plus__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_862,(0,3):C.GC_572,(0,1):C.GC_571,(0,2):C.GC_571,(0,5):C.GC_285,(0,0):C.GC_381})

V_445 = Vertex(name = 'V_445',
               particles = [ P.u__tilde__, P.s, P.mu__plus__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_887,(0,3):C.GC_672,(0,1):C.GC_671,(0,2):C.GC_671,(0,5):C.GC_310,(0,0):C.GC_431})

V_446 = Vertex(name = 'V_446',
               particles = [ P.c__tilde__, P.s, P.mu__plus__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_888,(0,3):C.GC_676,(0,1):C.GC_675,(0,2):C.GC_675,(0,5):C.GC_311,(0,0):C.GC_433})

V_447 = Vertex(name = 'V_447',
               particles = [ P.t__tilde__, P.s, P.mu__plus__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_889,(0,3):C.GC_680,(0,1):C.GC_679,(0,2):C.GC_679,(0,5):C.GC_312,(0,0):C.GC_435})

V_448 = Vertex(name = 'V_448',
               particles = [ P.u__tilde__, P.s, P.mu__plus__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_914,(0,3):C.GC_780,(0,1):C.GC_779,(0,2):C.GC_779,(0,5):C.GC_337,(0,0):C.GC_485})

V_449 = Vertex(name = 'V_449',
               particles = [ P.c__tilde__, P.s, P.mu__plus__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_915,(0,3):C.GC_784,(0,1):C.GC_783,(0,2):C.GC_783,(0,5):C.GC_338,(0,0):C.GC_487})

V_450 = Vertex(name = 'V_450',
               particles = [ P.t__tilde__, P.s, P.mu__plus__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_916,(0,3):C.GC_788,(0,1):C.GC_787,(0,2):C.GC_787,(0,5):C.GC_339,(0,0):C.GC_489})

V_451 = Vertex(name = 'V_451',
               particles = [ P.u__tilde__, P.s, P.tau__plus__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_869,(0,3):C.GC_600,(0,1):C.GC_599,(0,2):C.GC_599,(0,5):C.GC_292,(0,0):C.GC_395})

V_452 = Vertex(name = 'V_452',
               particles = [ P.c__tilde__, P.s, P.tau__plus__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_870,(0,3):C.GC_604,(0,1):C.GC_603,(0,2):C.GC_603,(0,5):C.GC_293,(0,0):C.GC_397})

V_453 = Vertex(name = 'V_453',
               particles = [ P.t__tilde__, P.s, P.tau__plus__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_871,(0,3):C.GC_608,(0,1):C.GC_607,(0,2):C.GC_607,(0,5):C.GC_294,(0,0):C.GC_399})

V_454 = Vertex(name = 'V_454',
               particles = [ P.u__tilde__, P.s, P.tau__plus__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_896,(0,3):C.GC_708,(0,1):C.GC_707,(0,2):C.GC_707,(0,5):C.GC_319,(0,0):C.GC_449})

V_455 = Vertex(name = 'V_455',
               particles = [ P.c__tilde__, P.s, P.tau__plus__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_897,(0,3):C.GC_712,(0,1):C.GC_711,(0,2):C.GC_711,(0,5):C.GC_320,(0,0):C.GC_451})

V_456 = Vertex(name = 'V_456',
               particles = [ P.t__tilde__, P.s, P.tau__plus__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_898,(0,3):C.GC_716,(0,1):C.GC_715,(0,2):C.GC_715,(0,5):C.GC_321,(0,0):C.GC_453})

V_457 = Vertex(name = 'V_457',
               particles = [ P.u__tilde__, P.s, P.tau__plus__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_923,(0,3):C.GC_816,(0,1):C.GC_815,(0,2):C.GC_815,(0,5):C.GC_346,(0,0):C.GC_503})

V_458 = Vertex(name = 'V_458',
               particles = [ P.c__tilde__, P.s, P.tau__plus__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_924,(0,3):C.GC_820,(0,1):C.GC_819,(0,2):C.GC_819,(0,5):C.GC_347,(0,0):C.GC_505})

V_459 = Vertex(name = 'V_459',
               particles = [ P.t__tilde__, P.s, P.tau__plus__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_925,(0,3):C.GC_824,(0,1):C.GC_823,(0,2):C.GC_823,(0,5):C.GC_348,(0,0):C.GC_507})

V_460 = Vertex(name = 'V_460',
               particles = [ P.u__tilde__, P.b, P.e__plus__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_854,(0,3):C.GC_540,(0,1):C.GC_539,(0,2):C.GC_539,(0,5):C.GC_277,(0,0):C.GC_365})

V_461 = Vertex(name = 'V_461',
               particles = [ P.c__tilde__, P.b, P.e__plus__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_855,(0,3):C.GC_544,(0,1):C.GC_543,(0,2):C.GC_543,(0,5):C.GC_278,(0,0):C.GC_367})

V_462 = Vertex(name = 'V_462',
               particles = [ P.t__tilde__, P.b, P.e__plus__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_856,(0,3):C.GC_548,(0,1):C.GC_547,(0,2):C.GC_547,(0,5):C.GC_279,(0,0):C.GC_369})

V_463 = Vertex(name = 'V_463',
               particles = [ P.u__tilde__, P.b, P.e__plus__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_881,(0,3):C.GC_648,(0,1):C.GC_647,(0,2):C.GC_647,(0,5):C.GC_304,(0,0):C.GC_419})

V_464 = Vertex(name = 'V_464',
               particles = [ P.c__tilde__, P.b, P.e__plus__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_882,(0,3):C.GC_652,(0,1):C.GC_651,(0,2):C.GC_651,(0,5):C.GC_305,(0,0):C.GC_421})

V_465 = Vertex(name = 'V_465',
               particles = [ P.t__tilde__, P.b, P.e__plus__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_883,(0,3):C.GC_656,(0,1):C.GC_655,(0,2):C.GC_655,(0,5):C.GC_306,(0,0):C.GC_423})

V_466 = Vertex(name = 'V_466',
               particles = [ P.u__tilde__, P.b, P.e__plus__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_908,(0,3):C.GC_756,(0,1):C.GC_755,(0,2):C.GC_755,(0,5):C.GC_331,(0,0):C.GC_473})

V_467 = Vertex(name = 'V_467',
               particles = [ P.c__tilde__, P.b, P.e__plus__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_909,(0,3):C.GC_760,(0,1):C.GC_759,(0,2):C.GC_759,(0,5):C.GC_332,(0,0):C.GC_475})

V_468 = Vertex(name = 'V_468',
               particles = [ P.t__tilde__, P.b, P.e__plus__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_910,(0,3):C.GC_764,(0,1):C.GC_763,(0,2):C.GC_763,(0,5):C.GC_333,(0,0):C.GC_477})

V_469 = Vertex(name = 'V_469',
               particles = [ P.u__tilde__, P.b, P.mu__plus__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_863,(0,3):C.GC_576,(0,1):C.GC_575,(0,2):C.GC_575,(0,5):C.GC_286,(0,0):C.GC_383})

V_470 = Vertex(name = 'V_470',
               particles = [ P.c__tilde__, P.b, P.mu__plus__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_864,(0,3):C.GC_580,(0,1):C.GC_579,(0,2):C.GC_579,(0,5):C.GC_287,(0,0):C.GC_385})

V_471 = Vertex(name = 'V_471',
               particles = [ P.t__tilde__, P.b, P.mu__plus__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_865,(0,3):C.GC_584,(0,1):C.GC_583,(0,2):C.GC_583,(0,5):C.GC_288,(0,0):C.GC_387})

V_472 = Vertex(name = 'V_472',
               particles = [ P.u__tilde__, P.b, P.mu__plus__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_890,(0,3):C.GC_684,(0,1):C.GC_683,(0,2):C.GC_683,(0,5):C.GC_313,(0,0):C.GC_437})

V_473 = Vertex(name = 'V_473',
               particles = [ P.c__tilde__, P.b, P.mu__plus__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_891,(0,3):C.GC_688,(0,1):C.GC_687,(0,2):C.GC_687,(0,5):C.GC_314,(0,0):C.GC_439})

V_474 = Vertex(name = 'V_474',
               particles = [ P.t__tilde__, P.b, P.mu__plus__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_892,(0,3):C.GC_692,(0,1):C.GC_691,(0,2):C.GC_691,(0,5):C.GC_315,(0,0):C.GC_441})

V_475 = Vertex(name = 'V_475',
               particles = [ P.u__tilde__, P.b, P.mu__plus__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_917,(0,3):C.GC_792,(0,1):C.GC_791,(0,2):C.GC_791,(0,5):C.GC_340,(0,0):C.GC_491})

V_476 = Vertex(name = 'V_476',
               particles = [ P.c__tilde__, P.b, P.mu__plus__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_918,(0,3):C.GC_796,(0,1):C.GC_795,(0,2):C.GC_795,(0,5):C.GC_341,(0,0):C.GC_493})

V_477 = Vertex(name = 'V_477',
               particles = [ P.t__tilde__, P.b, P.mu__plus__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_919,(0,3):C.GC_800,(0,1):C.GC_799,(0,2):C.GC_799,(0,5):C.GC_342,(0,0):C.GC_495})

V_478 = Vertex(name = 'V_478',
               particles = [ P.u__tilde__, P.b, P.tau__plus__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_872,(0,3):C.GC_612,(0,1):C.GC_611,(0,2):C.GC_611,(0,5):C.GC_295,(0,0):C.GC_401})

V_479 = Vertex(name = 'V_479',
               particles = [ P.c__tilde__, P.b, P.tau__plus__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_873,(0,3):C.GC_616,(0,1):C.GC_615,(0,2):C.GC_615,(0,5):C.GC_296,(0,0):C.GC_403})

V_480 = Vertex(name = 'V_480',
               particles = [ P.t__tilde__, P.b, P.tau__plus__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_874,(0,3):C.GC_620,(0,1):C.GC_619,(0,2):C.GC_619,(0,5):C.GC_297,(0,0):C.GC_405})

V_481 = Vertex(name = 'V_481',
               particles = [ P.u__tilde__, P.b, P.tau__plus__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_899,(0,3):C.GC_720,(0,1):C.GC_719,(0,2):C.GC_719,(0,5):C.GC_322,(0,0):C.GC_455})

V_482 = Vertex(name = 'V_482',
               particles = [ P.c__tilde__, P.b, P.tau__plus__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_900,(0,3):C.GC_724,(0,1):C.GC_723,(0,2):C.GC_723,(0,5):C.GC_323,(0,0):C.GC_457})

V_483 = Vertex(name = 'V_483',
               particles = [ P.t__tilde__, P.b, P.tau__plus__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_901,(0,3):C.GC_728,(0,1):C.GC_727,(0,2):C.GC_727,(0,5):C.GC_324,(0,0):C.GC_459})

V_484 = Vertex(name = 'V_484',
               particles = [ P.u__tilde__, P.b, P.tau__plus__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_926,(0,3):C.GC_828,(0,1):C.GC_827,(0,2):C.GC_827,(0,5):C.GC_349,(0,0):C.GC_509})

V_485 = Vertex(name = 'V_485',
               particles = [ P.c__tilde__, P.b, P.tau__plus__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_927,(0,3):C.GC_832,(0,1):C.GC_831,(0,2):C.GC_831,(0,5):C.GC_350,(0,0):C.GC_511})

V_486 = Vertex(name = 'V_486',
               particles = [ P.t__tilde__, P.b, P.tau__plus__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF10, L.FFFF14, L.FFFF15, L.FFFF16, L.FFFF4, L.FFFF9 ],
               couplings = {(0,4):C.GC_929,(0,3):C.GC_836,(0,1):C.GC_835,(0,2):C.GC_835,(0,5):C.GC_351,(0,0):C.GC_513})

V_487 = Vertex(name = 'V_487',
               particles = [ P.A, P.W__minus__, P.G0, P.G__minus__, P.G__plus__, P.G__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSSSS1 ],
               couplings = {(0,0):C.GC_1384})

V_488 = Vertex(name = 'V_488',
               particles = [ P.A, P.W__minus__, P.G__minus__, P.G__plus__, P.G__plus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSSSS1 ],
               couplings = {(0,0):C.GC_1383})

V_489 = Vertex(name = 'V_489',
               particles = [ P.A, P.W__minus__, P.G__minus__, P.G__plus__, P.G__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSSS1 ],
               couplings = {(0,0):C.GC_1504})

V_490 = Vertex(name = 'V_490',
               particles = [ P.W__minus__, P.G0, P.G__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VSS1, L.VSS2 ],
               couplings = {(0,0):C.GC_1361,(0,1):C.GC_1358})

V_491 = Vertex(name = 'V_491',
               particles = [ P.W__minus__, P.G0, P.G__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VSS1, L.VSS2 ],
               couplings = {(0,0):C.GC_1556,(0,1):C.GC_1555})

V_492 = Vertex(name = 'V_492',
               particles = [ P.W__minus__, P.G0, P.G0, P.G__plus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.VSSSS1, L.VSSSS2, L.VSSSS4 ],
               couplings = {(0,0):C.GC_1372,(0,1):C.GC_1372,(0,2):C.GC_1373})

V_493 = Vertex(name = 'V_493',
               particles = [ P.W__minus__, P.G0, P.G__plus__, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VSSSS1, L.VSSSS3, L.VSSSS4 ],
               couplings = {(0,0):C.GC_1375,(0,1):C.GC_1370,(0,2):C.GC_1370})

V_494 = Vertex(name = 'V_494',
               particles = [ P.W__minus__, P.G0, P.G0, P.G__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VSSS1, L.VSSS2 ],
               couplings = {(0,0):C.GC_1496,(0,1):C.GC_1496})

V_495 = Vertex(name = 'V_495',
               particles = [ P.W__minus__, P.G0, P.G__plus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.VSSS1, L.VSSS3 ],
               couplings = {(0,0):C.GC_1499,(0,1):C.GC_1494})

V_496 = Vertex(name = 'V_496',
               particles = [ P.W__minus__, P.G0, P.G__minus__, P.G__plus__, P.G__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VSSSS2, L.VSSSS3, L.VSSSS4 ],
               couplings = {(0,0):C.GC_1375,(0,1):C.GC_1370,(0,2):C.GC_1370})

V_497 = Vertex(name = 'V_497',
               particles = [ P.W__minus__, P.G__minus__, P.G__plus__, P.G__plus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.VSSSS1, L.VSSSS2, L.VSSSS3 ],
               couplings = {(0,0):C.GC_1373,(0,1):C.GC_1372,(0,2):C.GC_1372})

V_498 = Vertex(name = 'V_498',
               particles = [ P.W__minus__, P.G__minus__, P.G__plus__, P.G__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VSSS1, L.VSSS2, L.VSSS3 ],
               couplings = {(0,0):C.GC_1497,(0,1):C.GC_1496,(0,2):C.GC_1496})

V_499 = Vertex(name = 'V_499',
               particles = [ P.W__minus__, P.G__plus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.VSS1, L.VSS2 ],
               couplings = {(0,0):C.GC_1360,(0,1):C.GC_1359})

V_500 = Vertex(name = 'V_500',
               particles = [ P.W__minus__, P.G__plus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.VSS1, L.VSS2 ],
               couplings = {(0,0):C.GC_1558,(0,1):C.GC_1557})

V_501 = Vertex(name = 'V_501',
               particles = [ P.A, P.A, P.W__minus__, P.G0, P.G__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVSS1, L.VVVSS2, L.VVVSS3, L.VVVSS5 ],
               couplings = {(0,0):C.GC_1115,(0,1):C.GC_1115,(0,2):C.GC_1112,(0,3):C.GC_1112})

V_502 = Vertex(name = 'V_502',
               particles = [ P.A, P.A, P.W__minus__, P.G__plus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVSS1, L.VVVSS2, L.VVVSS3, L.VVVSS5 ],
               couplings = {(0,0):C.GC_1113,(0,1):C.GC_1113,(0,2):C.GC_1114,(0,3):C.GC_1114})

V_503 = Vertex(name = 'V_503',
               particles = [ P.A, P.A, P.W__minus__, P.G__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVS1, L.VVVS2, L.VVVS3, L.VVVS5 ],
               couplings = {(0,0):C.GC_1472,(0,1):C.GC_1472,(0,2):C.GC_1473,(0,3):C.GC_1473})

V_504 = Vertex(name = 'V_504',
               particles = [ P.A, P.W__minus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVV1, L.VVV2, L.VVV3, L.VVV4, L.VVV5, L.VVV6 ],
               couplings = {(0,0):C.GC_3,(0,1):C.GC_4,(0,2):C.GC_4,(0,3):C.GC_3,(0,4):C.GC_3,(0,5):C.GC_4})

V_505 = Vertex(name = 'V_505',
               particles = [ P.A, P.W__minus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVV1, L.VVV2, L.VVV3, L.VVV4, L.VVV5, L.VVV6 ],
               couplings = {(0,0):C.GC_1548,(0,1):C.GC_1576,(0,2):C.GC_1547,(0,3):C.GC_1578,(0,4):C.GC_1578,(0,5):C.GC_1576})

V_506 = Vertex(name = 'V_506',
               particles = [ P.A, P.W__minus__, P.W__plus__, P.G0, P.G0 ],
               color = [ '1' ],
               lorentz = [ L.VVVSS1, L.VVVSS2, L.VVVSS3, L.VVVSS4, L.VVVSS5, L.VVVSS6 ],
               couplings = {(0,0):C.GC_1341,(0,1):C.GC_1110,(0,2):C.GC_1342,(0,3):C.GC_1111,(0,4):C.GC_1111,(0,5):C.GC_1110})

V_507 = Vertex(name = 'V_507',
               particles = [ P.A, P.W__minus__, P.W__plus__, P.G__minus__, P.G__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVSS1, L.VVVSS2, L.VVVSS3, L.VVVSS4, L.VVVSS5, L.VVVSS6 ],
               couplings = {(0,0):C.GC_1343,(0,1):C.GC_1110,(0,2):C.GC_1340,(0,3):C.GC_1111,(0,4):C.GC_1111,(0,5):C.GC_1110})

V_508 = Vertex(name = 'V_508',
               particles = [ P.A, P.W__minus__, P.W__plus__, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVSS1, L.VVVSS2, L.VVVSS3, L.VVVSS4, L.VVVSS5, L.VVVSS6 ],
               couplings = {(0,0):C.GC_1341,(0,1):C.GC_1110,(0,2):C.GC_1342,(0,3):C.GC_1111,(0,4):C.GC_1111,(0,5):C.GC_1110})

V_509 = Vertex(name = 'V_509',
               particles = [ P.A, P.W__minus__, P.W__plus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVS1, L.VVVS2, L.VVVS3, L.VVVS4, L.VVVS5, L.VVVS6 ],
               couplings = {(0,0):C.GC_1520,(0,1):C.GC_1470,(0,2):C.GC_1521,(0,3):C.GC_1471,(0,4):C.GC_1471,(0,5):C.GC_1470})

V_510 = Vertex(name = 'V_510',
               particles = [ P.A, P.W__minus__, P.Z, P.G0, P.G__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVSS1, L.VVVSS3, L.VVVSS4, L.VVVSS6 ],
               couplings = {(0,0):C.GC_1381,(0,1):C.GC_1378,(0,2):C.GC_1397,(0,3):C.GC_1394})

V_511 = Vertex(name = 'V_511',
               particles = [ P.A, P.W__minus__, P.Z, P.G__plus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVSS1, L.VVVSS3, L.VVVSS4, L.VVVSS6 ],
               couplings = {(0,0):C.GC_1379,(0,1):C.GC_1380,(0,2):C.GC_1395,(0,3):C.GC_1396})

V_512 = Vertex(name = 'V_512',
               particles = [ P.A, P.W__minus__, P.Z, P.G__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVS1, L.VVVS3, L.VVVS4, L.VVVS6 ],
               couplings = {(0,0):C.GC_1502,(0,1):C.GC_1503,(0,2):C.GC_1509,(0,3):C.GC_1510})

V_513 = Vertex(name = 'V_513',
               particles = [ P.W__minus__, P.W__minus__, P.G0, P.G0, P.G__plus__, P.G__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSSSS1 ],
               couplings = {(0,0):C.GC_1351})

V_514 = Vertex(name = 'V_514',
               particles = [ P.W__minus__, P.W__minus__, P.G0, P.G__plus__, P.G__plus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSSSS1 ],
               couplings = {(0,0):C.GC_1349})

V_515 = Vertex(name = 'V_515',
               particles = [ P.W__minus__, P.W__minus__, P.G__plus__, P.G__plus__, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSSSS1 ],
               couplings = {(0,0):C.GC_1352})

V_516 = Vertex(name = 'V_516',
               particles = [ P.W__minus__, P.W__minus__, P.G0, P.G__plus__, P.G__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSSS1 ],
               couplings = {(0,0):C.GC_1485})

V_517 = Vertex(name = 'V_517',
               particles = [ P.W__minus__, P.W__minus__, P.G__plus__, P.G__plus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSSS1 ],
               couplings = {(0,0):C.GC_1487})

V_518 = Vertex(name = 'V_518',
               particles = [ P.W__minus__, P.W__minus__, P.G__plus__, P.G__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS2 ],
               couplings = {(0,0):C.GC_1511})

V_519 = Vertex(name = 'V_519',
               particles = [ P.A, P.W__plus__, P.G0, P.G__minus__, P.G__minus__, P.G__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSSSS1 ],
               couplings = {(0,0):C.GC_1382})

V_520 = Vertex(name = 'V_520',
               particles = [ P.A, P.W__plus__, P.G__minus__, P.G__minus__, P.G__plus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSSSS1 ],
               couplings = {(0,0):C.GC_1383})

V_521 = Vertex(name = 'V_521',
               particles = [ P.A, P.W__plus__, P.G__minus__, P.G__minus__, P.G__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSSS1 ],
               couplings = {(0,0):C.GC_1504})

V_522 = Vertex(name = 'V_522',
               particles = [ P.W__plus__, P.G0, P.G__minus__ ],
               color = [ '1' ],
               lorentz = [ L.VSS1, L.VSS2 ],
               couplings = {(0,0):C.GC_1361,(0,1):C.GC_1358})

V_523 = Vertex(name = 'V_523',
               particles = [ P.W__plus__, P.G0, P.G__minus__ ],
               color = [ '1' ],
               lorentz = [ L.VSS1, L.VSS2 ],
               couplings = {(0,0):C.GC_1556,(0,1):C.GC_1555})

V_524 = Vertex(name = 'V_524',
               particles = [ P.W__plus__, P.G0, P.G0, P.G__minus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.VSSSS1, L.VSSSS2, L.VSSSS4 ],
               couplings = {(0,0):C.GC_1371,(0,1):C.GC_1371,(0,2):C.GC_1374})

V_525 = Vertex(name = 'V_525',
               particles = [ P.W__plus__, P.G0, P.G__minus__, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VSSSS1, L.VSSSS3, L.VSSSS4 ],
               couplings = {(0,0):C.GC_1375,(0,1):C.GC_1370,(0,2):C.GC_1370})

V_526 = Vertex(name = 'V_526',
               particles = [ P.W__plus__, P.G0, P.G0, P.G__minus__ ],
               color = [ '1' ],
               lorentz = [ L.VSSS1, L.VSSS2 ],
               couplings = {(0,0):C.GC_1495,(0,1):C.GC_1495})

V_527 = Vertex(name = 'V_527',
               particles = [ P.W__plus__, P.G0, P.G__minus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.VSSS1, L.VSSS3 ],
               couplings = {(0,0):C.GC_1499,(0,1):C.GC_1494})

V_528 = Vertex(name = 'V_528',
               particles = [ P.W__plus__, P.G__minus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.VSS1, L.VSS2 ],
               couplings = {(0,0):C.GC_1359,(0,1):C.GC_1360})

V_529 = Vertex(name = 'V_529',
               particles = [ P.W__plus__, P.G__minus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.VSS1, L.VSS2 ],
               couplings = {(0,0):C.GC_1557,(0,1):C.GC_1558})

V_530 = Vertex(name = 'V_530',
               particles = [ P.W__plus__, P.G0, P.G__minus__, P.G__minus__, P.G__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VSSSS2, L.VSSSS3, L.VSSSS4 ],
               couplings = {(0,0):C.GC_1370,(0,1):C.GC_1370,(0,2):C.GC_1375})

V_531 = Vertex(name = 'V_531',
               particles = [ P.W__plus__, P.G__minus__, P.G__minus__, P.G__plus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.VSSSS1, L.VSSSS2, L.VSSSS3 ],
               couplings = {(0,0):C.GC_1371,(0,1):C.GC_1371,(0,2):C.GC_1374})

V_532 = Vertex(name = 'V_532',
               particles = [ P.W__plus__, P.G__minus__, P.G__minus__, P.G__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VSSS1, L.VSSS2, L.VSSS3 ],
               couplings = {(0,0):C.GC_1495,(0,1):C.GC_1495,(0,2):C.GC_1498})

V_533 = Vertex(name = 'V_533',
               particles = [ P.A, P.A, P.W__plus__, P.G0, P.G__minus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVSS1, L.VVVSS2, L.VVVSS3, L.VVVSS5 ],
               couplings = {(0,0):C.GC_1115,(0,1):C.GC_1115,(0,2):C.GC_1112,(0,3):C.GC_1112})

V_534 = Vertex(name = 'V_534',
               particles = [ P.A, P.A, P.W__plus__, P.G__minus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVSS1, L.VVVSS2, L.VVVSS3, L.VVVSS5 ],
               couplings = {(0,0):C.GC_1114,(0,1):C.GC_1114,(0,2):C.GC_1113,(0,3):C.GC_1113})

V_535 = Vertex(name = 'V_535',
               particles = [ P.A, P.A, P.W__plus__, P.G__minus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVS1, L.VVVS2, L.VVVS3, L.VVVS5 ],
               couplings = {(0,0):C.GC_1473,(0,1):C.GC_1473,(0,2):C.GC_1472,(0,3):C.GC_1472})

V_536 = Vertex(name = 'V_536',
               particles = [ P.A, P.W__plus__, P.Z, P.G0, P.G__minus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVSS1, L.VVVSS3, L.VVVSS4, L.VVVSS6 ],
               couplings = {(0,0):C.GC_1381,(0,1):C.GC_1378,(0,2):C.GC_1397,(0,3):C.GC_1394})

V_537 = Vertex(name = 'V_537',
               particles = [ P.A, P.W__plus__, P.Z, P.G__minus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVSS1, L.VVVSS3, L.VVVSS4, L.VVVSS6 ],
               couplings = {(0,0):C.GC_1380,(0,1):C.GC_1379,(0,2):C.GC_1396,(0,3):C.GC_1395})

V_538 = Vertex(name = 'V_538',
               particles = [ P.A, P.W__plus__, P.Z, P.G__minus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVS1, L.VVVS3, L.VVVS4, L.VVVS6 ],
               couplings = {(0,0):C.GC_1503,(0,1):C.GC_1502,(0,2):C.GC_1510,(0,3):C.GC_1509})

V_539 = Vertex(name = 'V_539',
               particles = [ P.W__minus__, P.W__plus__, P.G0, P.G0, P.G__minus__, P.G__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSSSS1 ],
               couplings = {(0,0):C.GC_1350})

V_540 = Vertex(name = 'V_540',
               particles = [ P.W__minus__, P.W__plus__, P.G__minus__, P.G__plus__, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSSSS1 ],
               couplings = {(0,0):C.GC_1350})

V_541 = Vertex(name = 'V_541',
               particles = [ P.W__minus__, P.W__plus__, P.G__minus__, P.G__plus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSSS1 ],
               couplings = {(0,0):C.GC_1486})

V_542 = Vertex(name = 'V_542',
               particles = [ P.A, P.A, P.W__minus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVV1, L.VVVV2, L.VVVV3 ],
               couplings = {(0,0):C.GC_5,(0,1):C.GC_5,(0,2):C.GC_6})

V_543 = Vertex(name = 'V_543',
               particles = [ P.A, P.A, P.W__minus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVV1, L.VVVV2, L.VVVV3 ],
               couplings = {(0,0):C.GC_1585,(0,1):C.GC_1585,(0,2):C.GC_1589})

V_544 = Vertex(name = 'V_544',
               particles = [ P.A, P.A, P.W__minus__, P.W__plus__, P.G0, P.G0 ],
               color = [ '1' ],
               lorentz = [ L.VVVVSS1, L.VVVVSS2, L.VVVVSS3 ],
               couplings = {(0,0):C.GC_1117,(0,1):C.GC_1117,(0,2):C.GC_1118})

V_545 = Vertex(name = 'V_545',
               particles = [ P.A, P.A, P.W__minus__, P.W__plus__, P.G__minus__, P.G__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVVSS1, L.VVVVSS2, L.VVVVSS3 ],
               couplings = {(0,0):C.GC_1117,(0,1):C.GC_1117,(0,2):C.GC_1118})

V_546 = Vertex(name = 'V_546',
               particles = [ P.A, P.A, P.W__minus__, P.W__plus__, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVSS1, L.VVVVSS2, L.VVVVSS3 ],
               couplings = {(0,0):C.GC_1117,(0,1):C.GC_1117,(0,2):C.GC_1118})

V_547 = Vertex(name = 'V_547',
               particles = [ P.A, P.A, P.W__minus__, P.W__plus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVS1, L.VVVVS2, L.VVVVS3 ],
               couplings = {(0,0):C.GC_1474,(0,1):C.GC_1474,(0,2):C.GC_1475})

V_548 = Vertex(name = 'V_548',
               particles = [ P.W__minus__, P.W__plus__, P.Z, P.G0, P.G0 ],
               color = [ '1' ],
               lorentz = [ L.VVVSS1, L.VVVSS2, L.VVVSS3, L.VVVSS4, L.VVVSS5, L.VVVSS6 ],
               couplings = {(0,0):C.GC_1376,(0,1):C.GC_1377,(0,2):C.GC_1377,(0,3):C.GC_1336,(0,4):C.GC_1376,(0,5):C.GC_1339})

V_549 = Vertex(name = 'V_549',
               particles = [ P.W__minus__, P.W__plus__, P.Z, P.G__minus__, P.G__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVSS1, L.VVVSS2, L.VVVSS3, L.VVVSS4, L.VVVSS5, L.VVVSS6 ],
               couplings = {(0,0):C.GC_1376,(0,1):C.GC_1377,(0,2):C.GC_1377,(0,3):C.GC_1337,(0,4):C.GC_1376,(0,5):C.GC_1338})

V_550 = Vertex(name = 'V_550',
               particles = [ P.W__minus__, P.W__plus__, P.Z, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVSS1, L.VVVSS2, L.VVVSS3, L.VVVSS4, L.VVVSS5, L.VVVSS6 ],
               couplings = {(0,0):C.GC_1376,(0,1):C.GC_1377,(0,2):C.GC_1377,(0,3):C.GC_1336,(0,4):C.GC_1376,(0,5):C.GC_1339})

V_551 = Vertex(name = 'V_551',
               particles = [ P.W__minus__, P.W__plus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVV1, L.VVV2, L.VVV3, L.VVV4, L.VVV5, L.VVV6 ],
               couplings = {(0,0):C.GC_1364,(0,1):C.GC_1363,(0,2):C.GC_1363,(0,3):C.GC_1364,(0,4):C.GC_1364,(0,5):C.GC_1363})

V_552 = Vertex(name = 'V_552',
               particles = [ P.W__minus__, P.W__plus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVV1, L.VVV2, L.VVV3, L.VVV4, L.VVV5, L.VVV6 ],
               couplings = {(0,0):C.GC_1569,(0,1):C.GC_1565,(0,2):C.GC_1565,(0,3):C.GC_1568,(0,4):C.GC_1569,(0,5):C.GC_1564})

V_553 = Vertex(name = 'V_553',
               particles = [ P.W__minus__, P.W__plus__, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVS1, L.VVVS2, L.VVVS3, L.VVVS4, L.VVVS5, L.VVVS6 ],
               couplings = {(0,0):C.GC_1500,(0,1):C.GC_1501,(0,2):C.GC_1501,(0,3):C.GC_1518,(0,4):C.GC_1500,(0,5):C.GC_1519})

V_554 = Vertex(name = 'V_554',
               particles = [ P.W__plus__, P.W__plus__, P.G0, P.G0, P.G__minus__, P.G__minus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSSSS1 ],
               couplings = {(0,0):C.GC_1351})

V_555 = Vertex(name = 'V_555',
               particles = [ P.W__plus__, P.W__plus__, P.G0, P.G__minus__, P.G__minus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSSSS1 ],
               couplings = {(0,0):C.GC_1353})

V_556 = Vertex(name = 'V_556',
               particles = [ P.W__plus__, P.W__plus__, P.G__minus__, P.G__minus__, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSSSS1 ],
               couplings = {(0,0):C.GC_1352})

V_557 = Vertex(name = 'V_557',
               particles = [ P.W__plus__, P.W__plus__, P.G0, P.G__minus__, P.G__minus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSSS1 ],
               couplings = {(0,0):C.GC_1488})

V_558 = Vertex(name = 'V_558',
               particles = [ P.W__plus__, P.W__plus__, P.G__minus__, P.G__minus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSSS1 ],
               couplings = {(0,0):C.GC_1487})

V_559 = Vertex(name = 'V_559',
               particles = [ P.W__plus__, P.W__plus__, P.G__minus__, P.G__minus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSS2 ],
               couplings = {(0,0):C.GC_1511})

V_560 = Vertex(name = 'V_560',
               particles = [ P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVV1, L.VVVV2, L.VVVV3 ],
               couplings = {(0,0):C.GC_1345,(0,1):C.GC_1345,(0,2):C.GC_1346})

V_561 = Vertex(name = 'V_561',
               particles = [ P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVV1, L.VVVV2, L.VVVV3 ],
               couplings = {(0,0):C.GC_1448,(0,1):C.GC_1448,(0,2):C.GC_1449})

V_562 = Vertex(name = 'V_562',
               particles = [ P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.G0, P.G0 ],
               color = [ '1' ],
               lorentz = [ L.VVVVSS1, L.VVVVSS2, L.VVVVSS3 ],
               couplings = {(0,0):C.GC_1354,(0,1):C.GC_1354,(0,2):C.GC_1355})

V_563 = Vertex(name = 'V_563',
               particles = [ P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.G__minus__, P.G__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVVSS1, L.VVVVSS2, L.VVVVSS3 ],
               couplings = {(0,0):C.GC_1354,(0,1):C.GC_1354,(0,2):C.GC_1355})

V_564 = Vertex(name = 'V_564',
               particles = [ P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVSS1, L.VVVVSS2, L.VVVVSS3 ],
               couplings = {(0,0):C.GC_1354,(0,1):C.GC_1354,(0,2):C.GC_1355})

V_565 = Vertex(name = 'V_565',
               particles = [ P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVS1, L.VVVVS2, L.VVVVS3 ],
               couplings = {(0,0):C.GC_1489,(0,1):C.GC_1489,(0,2):C.GC_1490})

V_566 = Vertex(name = 'V_566',
               particles = [ P.e__plus__, P.nu_e, P.G__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFS2 ],
               couplings = {(0,0):C.GC_1628})

V_567 = Vertex(name = 'V_567',
               particles = [ P.e__plus__, P.nu_e, P.G__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFS2 ],
               couplings = {(0,0):C.GC_1629})

V_568 = Vertex(name = 'V_568',
               particles = [ P.mu__plus__, P.nu_mu, P.G__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFS2 ],
               couplings = {(0,0):C.GC_1636})

V_569 = Vertex(name = 'V_569',
               particles = [ P.mu__plus__, P.nu_mu, P.G__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFS2 ],
               couplings = {(0,0):C.GC_1637})

V_570 = Vertex(name = 'V_570',
               particles = [ P.tau__plus__, P.nu_tau, P.G__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFS2 ],
               couplings = {(0,0):C.GC_1656})

V_571 = Vertex(name = 'V_571',
               particles = [ P.tau__plus__, P.nu_tau, P.G__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFS2 ],
               couplings = {(0,0):C.GC_1657})

V_572 = Vertex(name = 'V_572',
               particles = [ P.A, P.Z, P.G0, P.G0, P.G__minus__, P.G__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSSSS1 ],
               couplings = {(0,0):C.GC_1416})

V_573 = Vertex(name = 'V_573',
               particles = [ P.A, P.Z, P.G__minus__, P.G__minus__, P.G__plus__, P.G__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSSSS1 ],
               couplings = {(0,0):C.GC_1417})

V_574 = Vertex(name = 'V_574',
               particles = [ P.A, P.Z, P.G__minus__, P.G__plus__, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSSSS1 ],
               couplings = {(0,0):C.GC_1416})

V_575 = Vertex(name = 'V_575',
               particles = [ P.A, P.Z, P.G__minus__, P.G__plus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSSS1 ],
               couplings = {(0,0):C.GC_1528})

V_576 = Vertex(name = 'V_576',
               particles = [ P.Z, P.G0, P.H ],
               color = [ '1' ],
               lorentz = [ L.VSS1, L.VSS2 ],
               couplings = {(0,0):C.GC_1398,(0,1):C.GC_1404})

V_577 = Vertex(name = 'V_577',
               particles = [ P.Z, P.G0, P.H ],
               color = [ '1' ],
               lorentz = [ L.VSS1, L.VSS2 ],
               couplings = {(0,0):C.GC_1592,(0,1):C.GC_1593})

V_578 = Vertex(name = 'V_578',
               particles = [ P.Z, P.G0, P.G0, P.G0, P.H ],
               color = [ '1' ],
               lorentz = [ L.VSSSS1, L.VSSSS2, L.VSSSS3, L.VSSSS4 ],
               couplings = {(0,0):C.GC_1407,(0,1):C.GC_1407,(0,2):C.GC_1407,(0,3):C.GC_1415})

V_579 = Vertex(name = 'V_579',
               particles = [ P.Z, P.G0, P.G__minus__, P.G__plus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.VSSSS1, L.VSSSS4 ],
               couplings = {(0,0):C.GC_1408,(0,1):C.GC_1413})

V_580 = Vertex(name = 'V_580',
               particles = [ P.Z, P.G0, P.H, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VSSSS1, L.VSSSS2, L.VSSSS3, L.VSSSS4 ],
               couplings = {(0,0):C.GC_1406,(0,1):C.GC_1414,(0,2):C.GC_1414,(0,3):C.GC_1414})

V_581 = Vertex(name = 'V_581',
               particles = [ P.Z, P.G0, P.G0, P.G0 ],
               color = [ '1' ],
               lorentz = [ L.VSSS1, L.VSSS2, L.VSSS3 ],
               couplings = {(0,0):C.GC_1523,(0,1):C.GC_1523,(0,2):C.GC_1523})

V_582 = Vertex(name = 'V_582',
               particles = [ P.Z, P.G0, P.G__minus__, P.G__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VSSS1 ],
               couplings = {(0,0):C.GC_1524})

V_583 = Vertex(name = 'V_583',
               particles = [ P.Z, P.G0, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VSSS1, L.VSSS2, L.VSSS3 ],
               couplings = {(0,0):C.GC_1522,(0,1):C.GC_1527,(0,2):C.GC_1527})

V_584 = Vertex(name = 'V_584',
               particles = [ P.Z, P.G__minus__, P.G__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VSS1, L.VSS2 ],
               couplings = {(0,0):C.GC_1402,(0,1):C.GC_1401})

V_585 = Vertex(name = 'V_585',
               particles = [ P.Z, P.G__minus__, P.G__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VSS1, L.VSS2 ],
               couplings = {(0,0):C.GC_1591,(0,1):C.GC_1590})

V_586 = Vertex(name = 'V_586',
               particles = [ P.Z, P.G0, P.G0, P.G__minus__, P.G__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VSSSS3, L.VSSSS4 ],
               couplings = {(0,0):C.GC_1410,(0,1):C.GC_1409})

V_587 = Vertex(name = 'V_587',
               particles = [ P.Z, P.G__minus__, P.G__minus__, P.G__plus__, P.G__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VSSSS1, L.VSSSS2, L.VSSSS3, L.VSSSS4 ],
               couplings = {(0,0):C.GC_1412,(0,1):C.GC_1412,(0,2):C.GC_1411,(0,3):C.GC_1411})

V_588 = Vertex(name = 'V_588',
               particles = [ P.Z, P.G__minus__, P.G__plus__, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VSSSS1, L.VSSSS2 ],
               couplings = {(0,0):C.GC_1410,(0,1):C.GC_1409})

V_589 = Vertex(name = 'V_589',
               particles = [ P.Z, P.G__minus__, P.G__plus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.VSSS1, L.VSSS2 ],
               couplings = {(0,0):C.GC_1526,(0,1):C.GC_1525})

V_590 = Vertex(name = 'V_590',
               particles = [ P.W__minus__, P.Z, P.G0, P.G0, P.G0, P.G__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSSSS1 ],
               couplings = {(0,0):C.GC_1335})

V_591 = Vertex(name = 'V_591',
               particles = [ P.W__minus__, P.Z, P.G0, P.G__minus__, P.G__plus__, P.G__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSSSS1 ],
               couplings = {(0,0):C.GC_1328})

V_592 = Vertex(name = 'V_592',
               particles = [ P.W__minus__, P.Z, P.G0, P.G0, P.G__plus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSSSS1 ],
               couplings = {(0,0):C.GC_1330})

V_593 = Vertex(name = 'V_593',
               particles = [ P.W__minus__, P.Z, P.G__minus__, P.G__plus__, P.G__plus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSSSS1 ],
               couplings = {(0,0):C.GC_1331})

V_594 = Vertex(name = 'V_594',
               particles = [ P.W__minus__, P.Z, P.G0, P.G__plus__, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSSSS1 ],
               couplings = {(0,0):C.GC_1333})

V_595 = Vertex(name = 'V_595',
               particles = [ P.W__minus__, P.Z, P.G__plus__, P.H, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSSSS1 ],
               couplings = {(0,0):C.GC_1332})

V_596 = Vertex(name = 'V_596',
               particles = [ P.W__minus__, P.Z, P.G0, P.G0, P.G__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSSS1 ],
               couplings = {(0,0):C.GC_1514})

V_597 = Vertex(name = 'V_597',
               particles = [ P.W__minus__, P.Z, P.G__minus__, P.G__plus__, P.G__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSSS1 ],
               couplings = {(0,0):C.GC_1515})

V_598 = Vertex(name = 'V_598',
               particles = [ P.W__minus__, P.Z, P.G0, P.G__plus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSSS1 ],
               couplings = {(0,0):C.GC_1517})

V_599 = Vertex(name = 'V_599',
               particles = [ P.W__minus__, P.Z, P.G__plus__, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSSS1 ],
               couplings = {(0,0):C.GC_1516})

V_600 = Vertex(name = 'V_600',
               particles = [ P.W__minus__, P.Z, P.Z, P.G0, P.G__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVSS2, L.VVVSS4, L.VVVSS5, L.VVVSS6 ],
               couplings = {(0,0):C.GC_1115,(0,1):C.GC_1115,(0,2):C.GC_1112,(0,3):C.GC_1112})

V_601 = Vertex(name = 'V_601',
               particles = [ P.W__minus__, P.Z, P.Z, P.G__plus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVSS2, L.VVVSS4, L.VVVSS5, L.VVVSS6 ],
               couplings = {(0,0):C.GC_1113,(0,1):C.GC_1113,(0,2):C.GC_1114,(0,3):C.GC_1114})

V_602 = Vertex(name = 'V_602',
               particles = [ P.W__minus__, P.Z, P.Z, P.G__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVS2, L.VVVS4, L.VVVS5, L.VVVS6 ],
               couplings = {(0,0):C.GC_1472,(0,1):C.GC_1472,(0,2):C.GC_1473,(0,3):C.GC_1473})

V_603 = Vertex(name = 'V_603',
               particles = [ P.W__plus__, P.Z, P.G0, P.G0, P.G0, P.G__minus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSSSS1 ],
               couplings = {(0,0):C.GC_1327})

V_604 = Vertex(name = 'V_604',
               particles = [ P.W__plus__, P.Z, P.G0, P.G__minus__, P.G__minus__, P.G__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSSSS1 ],
               couplings = {(0,0):C.GC_1334})

V_605 = Vertex(name = 'V_605',
               particles = [ P.W__plus__, P.Z, P.G0, P.G0, P.G__minus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSSSS1 ],
               couplings = {(0,0):C.GC_1330})

V_606 = Vertex(name = 'V_606',
               particles = [ P.W__plus__, P.Z, P.G__minus__, P.G__minus__, P.G__plus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSSSS1 ],
               couplings = {(0,0):C.GC_1331})

V_607 = Vertex(name = 'V_607',
               particles = [ P.W__plus__, P.Z, P.G0, P.G__minus__, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSSSS1 ],
               couplings = {(0,0):C.GC_1329})

V_608 = Vertex(name = 'V_608',
               particles = [ P.W__plus__, P.Z, P.G__minus__, P.H, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSSSS1 ],
               couplings = {(0,0):C.GC_1332})

V_609 = Vertex(name = 'V_609',
               particles = [ P.W__plus__, P.Z, P.G0, P.G0, P.G__minus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSSS1 ],
               couplings = {(0,0):C.GC_1514})

V_610 = Vertex(name = 'V_610',
               particles = [ P.W__plus__, P.Z, P.G__minus__, P.G__minus__, P.G__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSSS1 ],
               couplings = {(0,0):C.GC_1515})

V_611 = Vertex(name = 'V_611',
               particles = [ P.W__plus__, P.Z, P.G0, P.G__minus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSSS1 ],
               couplings = {(0,0):C.GC_1513})

V_612 = Vertex(name = 'V_612',
               particles = [ P.W__plus__, P.Z, P.G__minus__, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSSS1 ],
               couplings = {(0,0):C.GC_1516})

V_613 = Vertex(name = 'V_613',
               particles = [ P.A, P.W__minus__, P.W__plus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVV1, L.VVVV2, L.VVVV3 ],
               couplings = {(0,0):C.GC_1369,(0,1):C.GC_1368,(0,2):C.GC_1368})

V_614 = Vertex(name = 'V_614',
               particles = [ P.A, P.W__minus__, P.W__plus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVV1, L.VVVV2, L.VVVV3 ],
               couplings = {(0,0):C.GC_1584,(0,1):C.GC_1583,(0,2):C.GC_1583})

V_615 = Vertex(name = 'V_615',
               particles = [ P.A, P.W__minus__, P.W__plus__, P.Z, P.G0, P.G0 ],
               color = [ '1' ],
               lorentz = [ L.VVVVSS1, L.VVVVSS2, L.VVVVSS3 ],
               couplings = {(0,0):C.GC_1386,(0,1):C.GC_1385,(0,2):C.GC_1385})

V_616 = Vertex(name = 'V_616',
               particles = [ P.A, P.W__minus__, P.W__plus__, P.Z, P.G__minus__, P.G__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVVSS1, L.VVVVSS2, L.VVVVSS3 ],
               couplings = {(0,0):C.GC_1386,(0,1):C.GC_1385,(0,2):C.GC_1385})

V_617 = Vertex(name = 'V_617',
               particles = [ P.A, P.W__minus__, P.W__plus__, P.Z, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVSS1, L.VVVVSS2, L.VVVVSS3 ],
               couplings = {(0,0):C.GC_1386,(0,1):C.GC_1385,(0,2):C.GC_1385})

V_618 = Vertex(name = 'V_618',
               particles = [ P.A, P.W__minus__, P.W__plus__, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVS1, L.VVVVS2, L.VVVVS3 ],
               couplings = {(0,0):C.GC_1506,(0,1):C.GC_1505,(0,2):C.GC_1505})

V_619 = Vertex(name = 'V_619',
               particles = [ P.W__plus__, P.Z, P.Z, P.G0, P.G__minus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVSS2, L.VVVSS4, L.VVVSS5, L.VVVSS6 ],
               couplings = {(0,0):C.GC_1115,(0,1):C.GC_1115,(0,2):C.GC_1112,(0,3):C.GC_1112})

V_620 = Vertex(name = 'V_620',
               particles = [ P.W__plus__, P.Z, P.Z, P.G__minus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVSS2, L.VVVSS4, L.VVVSS5, L.VVVSS6 ],
               couplings = {(0,0):C.GC_1114,(0,1):C.GC_1114,(0,2):C.GC_1113,(0,3):C.GC_1113})

V_621 = Vertex(name = 'V_621',
               particles = [ P.W__plus__, P.Z, P.Z, P.G__minus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVS2, L.VVVS4, L.VVVS5, L.VVVS6 ],
               couplings = {(0,0):C.GC_1473,(0,1):C.GC_1473,(0,2):C.GC_1472,(0,3):C.GC_1472})

V_622 = Vertex(name = 'V_622',
               particles = [ P.Z, P.Z, P.G0, P.G0, P.G0, P.G0 ],
               color = [ '1' ],
               lorentz = [ L.VVSSSS1 ],
               couplings = {(0,0):C.GC_1435})

V_623 = Vertex(name = 'V_623',
               particles = [ P.Z, P.Z, P.G__minus__, P.G__minus__, P.G__plus__, P.G__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSSSS1 ],
               couplings = {(0,0):C.GC_1434})

V_624 = Vertex(name = 'V_624',
               particles = [ P.Z, P.Z, P.G0, P.G0, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSSSS1 ],
               couplings = {(0,0):C.GC_1433})

V_625 = Vertex(name = 'V_625',
               particles = [ P.Z, P.Z, P.H, P.H, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSSSS1 ],
               couplings = {(0,0):C.GC_1435})

V_626 = Vertex(name = 'V_626',
               particles = [ P.Z, P.Z, P.G0, P.G0, P.G__minus__, P.G__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVSSSS1 ],
               couplings = {(0,0):C.GC_1432})

V_627 = Vertex(name = 'V_627',
               particles = [ P.Z, P.Z, P.G__minus__, P.G__plus__, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSSSS1 ],
               couplings = {(0,0):C.GC_1432})

V_628 = Vertex(name = 'V_628',
               particles = [ P.Z, P.Z, P.G0, P.G0, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSSS1 ],
               couplings = {(0,0):C.GC_1537})

V_629 = Vertex(name = 'V_629',
               particles = [ P.Z, P.Z, P.H, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSSS1 ],
               couplings = {(0,0):C.GC_1538})

V_630 = Vertex(name = 'V_630',
               particles = [ P.Z, P.Z, P.G__minus__, P.G__plus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSSS1 ],
               couplings = {(0,0):C.GC_1536})

V_631 = Vertex(name = 'V_631',
               particles = [ P.W__minus__, P.W__plus__, P.Z, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVV1, L.VVVV2, L.VVVV3 ],
               couplings = {(0,0):C.GC_1347,(0,1):C.GC_1347,(0,2):C.GC_1348})

V_632 = Vertex(name = 'V_632',
               particles = [ P.W__minus__, P.W__plus__, P.Z, P.Z ],
               color = [ '1' ],
               lorentz = [ L.VVVV1, L.VVVV2, L.VVVV3 ],
               couplings = {(0,0):C.GC_1586,(0,1):C.GC_1586,(0,2):C.GC_1588})

V_633 = Vertex(name = 'V_633',
               particles = [ P.W__minus__, P.W__plus__, P.Z, P.Z, P.G0, P.G0 ],
               color = [ '1' ],
               lorentz = [ L.VVVVSS1, L.VVVVSS2, L.VVVVSS3 ],
               couplings = {(0,0):C.GC_1356,(0,1):C.GC_1356,(0,2):C.GC_1357})

V_634 = Vertex(name = 'V_634',
               particles = [ P.W__minus__, P.W__plus__, P.Z, P.Z, P.G__minus__, P.G__plus__ ],
               color = [ '1' ],
               lorentz = [ L.VVVVSS1, L.VVVVSS2, L.VVVVSS3 ],
               couplings = {(0,0):C.GC_1356,(0,1):C.GC_1356,(0,2):C.GC_1357})

V_635 = Vertex(name = 'V_635',
               particles = [ P.W__minus__, P.W__plus__, P.Z, P.Z, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVSS1, L.VVVVSS2, L.VVVVSS3 ],
               couplings = {(0,0):C.GC_1356,(0,1):C.GC_1356,(0,2):C.GC_1357})

V_636 = Vertex(name = 'V_636',
               particles = [ P.W__minus__, P.W__plus__, P.Z, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVVVS1, L.VVVVS2, L.VVVVS3 ],
               couplings = {(0,0):C.GC_1491,(0,1):C.GC_1491,(0,2):C.GC_1492})

V_637 = Vertex(name = 'V_637',
               particles = [ P.nu_e__tilde__, P.nu_e, P.A ],
               color = [ '1' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_1546})

V_638 = Vertex(name = 'V_638',
               particles = [ P.nu_mu__tilde__, P.nu_mu, P.A ],
               color = [ '1' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_1546})

V_639 = Vertex(name = 'V_639',
               particles = [ P.nu_tau__tilde__, P.nu_tau, P.A ],
               color = [ '1' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_1546})

V_640 = Vertex(name = 'V_640',
               particles = [ P.e__plus__, P.e__minus__, P.A ],
               color = [ '1' ],
               lorentz = [ L.FFV2, L.FFV3 ],
               couplings = {(0,0):C.GC_4,(0,1):C.GC_4})

V_641 = Vertex(name = 'V_641',
               particles = [ P.e__plus__, P.e__minus__, P.A ],
               color = [ '1' ],
               lorentz = [ L.FFV2, L.FFV3 ],
               couplings = {(0,0):C.GC_1575,(0,1):C.GC_1574})

V_642 = Vertex(name = 'V_642',
               particles = [ P.mu__plus__, P.mu__minus__, P.A ],
               color = [ '1' ],
               lorentz = [ L.FFV2, L.FFV3 ],
               couplings = {(0,0):C.GC_4,(0,1):C.GC_4})

V_643 = Vertex(name = 'V_643',
               particles = [ P.mu__plus__, P.mu__minus__, P.A ],
               color = [ '1' ],
               lorentz = [ L.FFV2, L.FFV3 ],
               couplings = {(0,0):C.GC_1575,(0,1):C.GC_1574})

V_644 = Vertex(name = 'V_644',
               particles = [ P.tau__plus__, P.tau__minus__, P.A ],
               color = [ '1' ],
               lorentz = [ L.FFV2, L.FFV3 ],
               couplings = {(0,0):C.GC_4,(0,1):C.GC_4})

V_645 = Vertex(name = 'V_645',
               particles = [ P.tau__plus__, P.tau__minus__, P.A ],
               color = [ '1' ],
               lorentz = [ L.FFV2, L.FFV3 ],
               couplings = {(0,0):C.GC_1575,(0,1):C.GC_1574})

V_646 = Vertex(name = 'V_646',
               particles = [ P.u__tilde__, P.u, P.A ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2, L.FFV3 ],
               couplings = {(0,0):C.GC_2,(0,1):C.GC_2})

V_647 = Vertex(name = 'V_647',
               particles = [ P.u__tilde__, P.u, P.A ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2, L.FFV3 ],
               couplings = {(0,0):C.GC_1573,(0,1):C.GC_1572})

V_648 = Vertex(name = 'V_648',
               particles = [ P.c__tilde__, P.c, P.A ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2, L.FFV3 ],
               couplings = {(0,0):C.GC_2,(0,1):C.GC_2})

V_649 = Vertex(name = 'V_649',
               particles = [ P.c__tilde__, P.c, P.A ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2, L.FFV3 ],
               couplings = {(0,0):C.GC_1573,(0,1):C.GC_1572})

V_650 = Vertex(name = 'V_650',
               particles = [ P.t__tilde__, P.t, P.A ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2, L.FFV3 ],
               couplings = {(0,0):C.GC_2,(0,1):C.GC_2})

V_651 = Vertex(name = 'V_651',
               particles = [ P.t__tilde__, P.t, P.A ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2, L.FFV3 ],
               couplings = {(0,0):C.GC_1573,(0,1):C.GC_1572})

V_652 = Vertex(name = 'V_652',
               particles = [ P.d__tilde__, P.d, P.A ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2, L.FFV3 ],
               couplings = {(0,0):C.GC_1,(0,1):C.GC_1})

V_653 = Vertex(name = 'V_653',
               particles = [ P.d__tilde__, P.d, P.A ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2, L.FFV3 ],
               couplings = {(0,0):C.GC_1571,(0,1):C.GC_1570})

V_654 = Vertex(name = 'V_654',
               particles = [ P.s__tilde__, P.s, P.A ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2, L.FFV3 ],
               couplings = {(0,0):C.GC_1,(0,1):C.GC_1})

V_655 = Vertex(name = 'V_655',
               particles = [ P.s__tilde__, P.s, P.A ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2, L.FFV3 ],
               couplings = {(0,0):C.GC_1571,(0,1):C.GC_1570})

V_656 = Vertex(name = 'V_656',
               particles = [ P.b__tilde__, P.b, P.A ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2, L.FFV3 ],
               couplings = {(0,0):C.GC_1,(0,1):C.GC_1})

V_657 = Vertex(name = 'V_657',
               particles = [ P.b__tilde__, P.b, P.A ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2, L.FFV3 ],
               couplings = {(0,0):C.GC_1571,(0,1):C.GC_1570})

V_658 = Vertex(name = 'V_658',
               particles = [ P.u__tilde__, P.u, P.g ],
               color = [ 'T(3,2,1)' ],
               lorentz = [ L.FFV1 ],
               couplings = {(0,0):C.GC_12})

V_659 = Vertex(name = 'V_659',
               particles = [ P.c__tilde__, P.c, P.g ],
               color = [ 'T(3,2,1)' ],
               lorentz = [ L.FFV1 ],
               couplings = {(0,0):C.GC_12})

V_660 = Vertex(name = 'V_660',
               particles = [ P.t__tilde__, P.t, P.g ],
               color = [ 'T(3,2,1)' ],
               lorentz = [ L.FFV1 ],
               couplings = {(0,0):C.GC_12})

V_661 = Vertex(name = 'V_661',
               particles = [ P.d__tilde__, P.d, P.g ],
               color = [ 'T(3,2,1)' ],
               lorentz = [ L.FFV1 ],
               couplings = {(0,0):C.GC_12})

V_662 = Vertex(name = 'V_662',
               particles = [ P.s__tilde__, P.s, P.g ],
               color = [ 'T(3,2,1)' ],
               lorentz = [ L.FFV1 ],
               couplings = {(0,0):C.GC_12})

V_663 = Vertex(name = 'V_663',
               particles = [ P.b__tilde__, P.b, P.g ],
               color = [ 'T(3,2,1)' ],
               lorentz = [ L.FFV1 ],
               couplings = {(0,0):C.GC_12})

V_664 = Vertex(name = 'V_664',
               particles = [ P.d__tilde__, P.u, P.W__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_1362})

V_665 = Vertex(name = 'V_665',
               particles = [ P.d__tilde__, P.u, P.W__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_1450})

V_666 = Vertex(name = 'V_666',
               particles = [ P.s__tilde__, P.c, P.W__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_1362})

V_667 = Vertex(name = 'V_667',
               particles = [ P.s__tilde__, P.c, P.W__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_1450})

V_668 = Vertex(name = 'V_668',
               particles = [ P.b__tilde__, P.t, P.W__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_1362})

V_669 = Vertex(name = 'V_669',
               particles = [ P.b__tilde__, P.t, P.W__minus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_1450})

V_670 = Vertex(name = 'V_670',
               particles = [ P.u__tilde__, P.d, P.W__plus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_1362})

V_671 = Vertex(name = 'V_671',
               particles = [ P.u__tilde__, P.d, P.W__plus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_1450})

V_672 = Vertex(name = 'V_672',
               particles = [ P.c__tilde__, P.s, P.W__plus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_1362})

V_673 = Vertex(name = 'V_673',
               particles = [ P.c__tilde__, P.s, P.W__plus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_1450})

V_674 = Vertex(name = 'V_674',
               particles = [ P.t__tilde__, P.b, P.W__plus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_1362})

V_675 = Vertex(name = 'V_675',
               particles = [ P.t__tilde__, P.b, P.W__plus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_1450})

V_676 = Vertex(name = 'V_676',
               particles = [ P.e__plus__, P.nu_e, P.W__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_1362})

V_677 = Vertex(name = 'V_677',
               particles = [ P.e__plus__, P.nu_e, P.W__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_1450})

V_678 = Vertex(name = 'V_678',
               particles = [ P.mu__plus__, P.nu_mu, P.W__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_1362})

V_679 = Vertex(name = 'V_679',
               particles = [ P.mu__plus__, P.nu_mu, P.W__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_1450})

V_680 = Vertex(name = 'V_680',
               particles = [ P.tau__plus__, P.nu_tau, P.W__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_1362})

V_681 = Vertex(name = 'V_681',
               particles = [ P.tau__plus__, P.nu_tau, P.W__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_1450})

V_682 = Vertex(name = 'V_682',
               particles = [ P.nu_e__tilde__, P.e__minus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_1362})

V_683 = Vertex(name = 'V_683',
               particles = [ P.nu_e__tilde__, P.e__minus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_1450})

V_684 = Vertex(name = 'V_684',
               particles = [ P.nu_mu__tilde__, P.mu__minus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_1362})

V_685 = Vertex(name = 'V_685',
               particles = [ P.nu_mu__tilde__, P.mu__minus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_1450})

V_686 = Vertex(name = 'V_686',
               particles = [ P.nu_tau__tilde__, P.tau__minus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_1362})

V_687 = Vertex(name = 'V_687',
               particles = [ P.nu_tau__tilde__, P.tau__minus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_1450})

V_688 = Vertex(name = 'V_688',
               particles = [ P.u__tilde__, P.u, P.Z ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2, L.FFV3 ],
               couplings = {(0,0):C.GC_1400,(0,1):C.GC_1388})

V_689 = Vertex(name = 'V_689',
               particles = [ P.u__tilde__, P.u, P.Z ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2, L.FFV3 ],
               couplings = {(0,0):C.GC_1562,(0,1):C.GC_1563})

V_690 = Vertex(name = 'V_690',
               particles = [ P.c__tilde__, P.c, P.Z ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2, L.FFV3 ],
               couplings = {(0,0):C.GC_1400,(0,1):C.GC_1388})

V_691 = Vertex(name = 'V_691',
               particles = [ P.c__tilde__, P.c, P.Z ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2, L.FFV3 ],
               couplings = {(0,0):C.GC_1562,(0,1):C.GC_1563})

V_692 = Vertex(name = 'V_692',
               particles = [ P.t__tilde__, P.t, P.Z ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2, L.FFV3 ],
               couplings = {(0,0):C.GC_1400,(0,1):C.GC_1388})

V_693 = Vertex(name = 'V_693',
               particles = [ P.t__tilde__, P.t, P.Z ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2, L.FFV3 ],
               couplings = {(0,0):C.GC_1562,(0,1):C.GC_1563})

V_694 = Vertex(name = 'V_694',
               particles = [ P.d__tilde__, P.d, P.Z ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2, L.FFV3 ],
               couplings = {(0,0):C.GC_1399,(0,1):C.GC_1387})

V_695 = Vertex(name = 'V_695',
               particles = [ P.d__tilde__, P.d, P.Z ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2, L.FFV3 ],
               couplings = {(0,0):C.GC_1560,(0,1):C.GC_1561})

V_696 = Vertex(name = 'V_696',
               particles = [ P.s__tilde__, P.s, P.Z ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2, L.FFV3 ],
               couplings = {(0,0):C.GC_1399,(0,1):C.GC_1387})

V_697 = Vertex(name = 'V_697',
               particles = [ P.s__tilde__, P.s, P.Z ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2, L.FFV3 ],
               couplings = {(0,0):C.GC_1560,(0,1):C.GC_1561})

V_698 = Vertex(name = 'V_698',
               particles = [ P.b__tilde__, P.b, P.Z ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2, L.FFV3 ],
               couplings = {(0,0):C.GC_1399,(0,1):C.GC_1387})

V_699 = Vertex(name = 'V_699',
               particles = [ P.b__tilde__, P.b, P.Z ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2, L.FFV3 ],
               couplings = {(0,0):C.GC_1560,(0,1):C.GC_1561})

V_700 = Vertex(name = 'V_700',
               particles = [ P.nu_e__tilde__, P.nu_e, P.Z ],
               color = [ '1' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_1403})

V_701 = Vertex(name = 'V_701',
               particles = [ P.nu_e__tilde__, P.nu_e, P.Z ],
               color = [ '1' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_1559})

V_702 = Vertex(name = 'V_702',
               particles = [ P.nu_mu__tilde__, P.nu_mu, P.Z ],
               color = [ '1' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_1403})

V_703 = Vertex(name = 'V_703',
               particles = [ P.nu_mu__tilde__, P.nu_mu, P.Z ],
               color = [ '1' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_1559})

V_704 = Vertex(name = 'V_704',
               particles = [ P.nu_tau__tilde__, P.nu_tau, P.Z ],
               color = [ '1' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_1403})

V_705 = Vertex(name = 'V_705',
               particles = [ P.nu_tau__tilde__, P.nu_tau, P.Z ],
               color = [ '1' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_1559})

V_706 = Vertex(name = 'V_706',
               particles = [ P.e__plus__, P.e__minus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.FFV2, L.FFV3 ],
               couplings = {(0,0):C.GC_1402,(0,1):C.GC_1389})

V_707 = Vertex(name = 'V_707',
               particles = [ P.e__plus__, P.e__minus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.FFV2, L.FFV3 ],
               couplings = {(0,0):C.GC_1566,(0,1):C.GC_1567})

V_708 = Vertex(name = 'V_708',
               particles = [ P.mu__plus__, P.mu__minus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.FFV2, L.FFV3 ],
               couplings = {(0,0):C.GC_1402,(0,1):C.GC_1389})

V_709 = Vertex(name = 'V_709',
               particles = [ P.mu__plus__, P.mu__minus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.FFV2, L.FFV3 ],
               couplings = {(0,0):C.GC_1566,(0,1):C.GC_1567})

V_710 = Vertex(name = 'V_710',
               particles = [ P.tau__plus__, P.tau__minus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.FFV2, L.FFV3 ],
               couplings = {(0,0):C.GC_1402,(0,1):C.GC_1389})

V_711 = Vertex(name = 'V_711',
               particles = [ P.tau__plus__, P.tau__minus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.FFV2, L.FFV3 ],
               couplings = {(0,0):C.GC_1566,(0,1):C.GC_1567})

V_712 = Vertex(name = 'V_712',
               particles = [ P.e__plus__, P.e__minus__, P.e__plus__, P.e__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFFF3, L.FFFF4 ],
               couplings = {(0,0):C.GC_838,(0,1):C.GC_838})

V_713 = Vertex(name = 'V_713',
               particles = [ P.mu__plus__, P.e__minus__, P.e__plus__, P.e__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFFF3, L.FFFF4 ],
               couplings = {(0,0):C.GC_1128,(0,1):C.GC_1128})

V_714 = Vertex(name = 'V_714',
               particles = [ P.tau__plus__, P.e__minus__, P.e__plus__, P.e__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFFF3, L.FFFF4 ],
               couplings = {(0,0):C.GC_1129,(0,1):C.GC_1129})

V_715 = Vertex(name = 'V_715',
               particles = [ P.e__plus__, P.e__minus__, P.e__plus__, P.mu__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFFF3, L.FFFF4 ],
               couplings = {(0,0):C.GC_1131,(0,1):C.GC_1131})

V_716 = Vertex(name = 'V_716',
               particles = [ P.mu__plus__, P.e__minus__, P.e__plus__, P.mu__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFFF3, L.FFFF4 ],
               couplings = {(0,0):C.GC_1134,(0,1):C.GC_1132})

V_717 = Vertex(name = 'V_717',
               particles = [ P.tau__plus__, P.e__minus__, P.e__plus__, P.mu__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFFF3, L.FFFF4 ],
               couplings = {(0,0):C.GC_1138,(0,1):C.GC_1133})

V_718 = Vertex(name = 'V_718',
               particles = [ P.e__plus__, P.e__minus__, P.e__plus__, P.tau__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFFF3, L.FFFF4 ],
               couplings = {(0,0):C.GC_1143,(0,1):C.GC_1143})

V_719 = Vertex(name = 'V_719',
               particles = [ P.mu__plus__, P.e__minus__, P.e__plus__, P.tau__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFFF3, L.FFFF4 ],
               couplings = {(0,0):C.GC_1149,(0,1):C.GC_1144})

V_720 = Vertex(name = 'V_720',
               particles = [ P.tau__plus__, P.e__minus__, P.e__plus__, P.tau__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFFF3, L.FFFF4 ],
               couplings = {(0,0):C.GC_1156,(0,1):C.GC_1145})

V_721 = Vertex(name = 'V_721',
               particles = [ P.mu__plus__, P.e__minus__, P.mu__plus__, P.e__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFFF3, L.FFFF4 ],
               couplings = {(0,0):C.GC_839,(0,1):C.GC_839})

V_722 = Vertex(name = 'V_722',
               particles = [ P.tau__plus__, P.e__minus__, P.mu__plus__, P.e__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFFF3, L.FFFF4 ],
               couplings = {(0,0):C.GC_1130,(0,1):C.GC_1130})

V_723 = Vertex(name = 'V_723',
               particles = [ P.mu__plus__, P.e__minus__, P.mu__plus__, P.mu__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFFF3, L.FFFF4 ],
               couplings = {(0,0):C.GC_1135,(0,1):C.GC_1135})

V_724 = Vertex(name = 'V_724',
               particles = [ P.tau__plus__, P.e__minus__, P.mu__plus__, P.mu__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFFF3, L.FFFF4 ],
               couplings = {(0,0):C.GC_1139,(0,1):C.GC_1136})

V_725 = Vertex(name = 'V_725',
               particles = [ P.mu__plus__, P.e__minus__, P.mu__plus__, P.tau__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFFF3, L.FFFF4 ],
               couplings = {(0,0):C.GC_1150,(0,1):C.GC_1150})

V_726 = Vertex(name = 'V_726',
               particles = [ P.tau__plus__, P.e__minus__, P.mu__plus__, P.tau__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFFF3, L.FFFF4 ],
               couplings = {(0,0):C.GC_1157,(0,1):C.GC_1151})

V_727 = Vertex(name = 'V_727',
               particles = [ P.tau__plus__, P.e__minus__, P.tau__plus__, P.e__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFFF3, L.FFFF4 ],
               couplings = {(0,0):C.GC_840,(0,1):C.GC_840})

V_728 = Vertex(name = 'V_728',
               particles = [ P.tau__plus__, P.e__minus__, P.tau__plus__, P.mu__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFFF3, L.FFFF4 ],
               couplings = {(0,0):C.GC_1140,(0,1):C.GC_1140})

V_729 = Vertex(name = 'V_729',
               particles = [ P.tau__plus__, P.e__minus__, P.tau__plus__, P.tau__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFFF3, L.FFFF4 ],
               couplings = {(0,0):C.GC_1158,(0,1):C.GC_1158})

V_730 = Vertex(name = 'V_730',
               particles = [ P.e__plus__, P.mu__minus__, P.e__plus__, P.mu__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFFF3, L.FFFF4 ],
               couplings = {(0,0):C.GC_841,(0,1):C.GC_841})

V_731 = Vertex(name = 'V_731',
               particles = [ P.mu__plus__, P.mu__minus__, P.e__plus__, P.mu__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFFF3, L.FFFF4 ],
               couplings = {(0,0):C.GC_1137,(0,1):C.GC_1137})

V_732 = Vertex(name = 'V_732',
               particles = [ P.tau__plus__, P.mu__minus__, P.e__plus__, P.mu__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFFF3, L.FFFF4 ],
               couplings = {(0,0):C.GC_1141,(0,1):C.GC_1141})

V_733 = Vertex(name = 'V_733',
               particles = [ P.e__plus__, P.mu__minus__, P.e__plus__, P.tau__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFFF3, L.FFFF4 ],
               couplings = {(0,0):C.GC_1146,(0,1):C.GC_1146})

V_734 = Vertex(name = 'V_734',
               particles = [ P.mu__plus__, P.mu__minus__, P.e__plus__, P.tau__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFFF3, L.FFFF4 ],
               couplings = {(0,0):C.GC_1152,(0,1):C.GC_1147})

V_735 = Vertex(name = 'V_735',
               particles = [ P.tau__plus__, P.mu__minus__, P.e__plus__, P.tau__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFFF3, L.FFFF4 ],
               couplings = {(0,0):C.GC_1159,(0,1):C.GC_1148})

V_736 = Vertex(name = 'V_736',
               particles = [ P.mu__plus__, P.mu__minus__, P.mu__plus__, P.mu__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFFF3, L.FFFF4 ],
               couplings = {(0,0):C.GC_842,(0,1):C.GC_842})

V_737 = Vertex(name = 'V_737',
               particles = [ P.tau__plus__, P.mu__minus__, P.mu__plus__, P.mu__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFFF3, L.FFFF4 ],
               couplings = {(0,0):C.GC_1142,(0,1):C.GC_1142})

V_738 = Vertex(name = 'V_738',
               particles = [ P.mu__plus__, P.mu__minus__, P.mu__plus__, P.tau__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFFF3, L.FFFF4 ],
               couplings = {(0,0):C.GC_1153,(0,1):C.GC_1153})

V_739 = Vertex(name = 'V_739',
               particles = [ P.tau__plus__, P.mu__minus__, P.mu__plus__, P.tau__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFFF3, L.FFFF4 ],
               couplings = {(0,0):C.GC_1160,(0,1):C.GC_1154})

V_740 = Vertex(name = 'V_740',
               particles = [ P.tau__plus__, P.mu__minus__, P.tau__plus__, P.mu__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFFF3, L.FFFF4 ],
               couplings = {(0,0):C.GC_843,(0,1):C.GC_843})

V_741 = Vertex(name = 'V_741',
               particles = [ P.tau__plus__, P.mu__minus__, P.tau__plus__, P.tau__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFFF3, L.FFFF4 ],
               couplings = {(0,0):C.GC_1161,(0,1):C.GC_1161})

V_742 = Vertex(name = 'V_742',
               particles = [ P.e__plus__, P.tau__minus__, P.e__plus__, P.tau__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFFF3, L.FFFF4 ],
               couplings = {(0,0):C.GC_844,(0,1):C.GC_844})

V_743 = Vertex(name = 'V_743',
               particles = [ P.mu__plus__, P.tau__minus__, P.e__plus__, P.tau__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFFF3, L.FFFF4 ],
               couplings = {(0,0):C.GC_1155,(0,1):C.GC_1155})

V_744 = Vertex(name = 'V_744',
               particles = [ P.tau__plus__, P.tau__minus__, P.e__plus__, P.tau__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFFF3, L.FFFF4 ],
               couplings = {(0,0):C.GC_1162,(0,1):C.GC_1162})

V_745 = Vertex(name = 'V_745',
               particles = [ P.mu__plus__, P.tau__minus__, P.mu__plus__, P.tau__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFFF3, L.FFFF4 ],
               couplings = {(0,0):C.GC_845,(0,1):C.GC_845})

V_746 = Vertex(name = 'V_746',
               particles = [ P.tau__plus__, P.tau__minus__, P.mu__plus__, P.tau__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFFF3, L.FFFF4 ],
               couplings = {(0,0):C.GC_1163,(0,1):C.GC_1163})

V_747 = Vertex(name = 'V_747',
               particles = [ P.tau__plus__, P.tau__minus__, P.tau__plus__, P.tau__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFFF3, L.FFFF4 ],
               couplings = {(0,0):C.GC_846,(0,1):C.GC_846})

V_748 = Vertex(name = 'V_748',
               particles = [ P.d__tilde__, P.d, P.nu_e__tilde__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1164,(0,0):C.GC_190})

V_749 = Vertex(name = 'V_749',
               particles = [ P.d__tilde__, P.d, P.nu_mu__tilde__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1182,(0,0):C.GC_199})

V_750 = Vertex(name = 'V_750',
               particles = [ P.d__tilde__, P.d, P.nu_tau__tilde__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1200,(0,0):C.GC_208})

V_751 = Vertex(name = 'V_751',
               particles = [ P.d__tilde__, P.d, P.nu_e__tilde__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1218,(0,0):C.GC_217})

V_752 = Vertex(name = 'V_752',
               particles = [ P.d__tilde__, P.d, P.nu_mu__tilde__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1236,(0,0):C.GC_226})

V_753 = Vertex(name = 'V_753',
               particles = [ P.d__tilde__, P.d, P.nu_tau__tilde__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1254,(0,0):C.GC_235})

V_754 = Vertex(name = 'V_754',
               particles = [ P.d__tilde__, P.d, P.nu_e__tilde__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1272,(0,0):C.GC_244})

V_755 = Vertex(name = 'V_755',
               particles = [ P.d__tilde__, P.d, P.nu_mu__tilde__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1290,(0,0):C.GC_253})

V_756 = Vertex(name = 'V_756',
               particles = [ P.d__tilde__, P.d, P.nu_tau__tilde__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1308,(0,0):C.GC_262})

V_757 = Vertex(name = 'V_757',
               particles = [ P.s__tilde__, P.d, P.nu_e__tilde__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1166,(0,0):C.GC_191})

V_758 = Vertex(name = 'V_758',
               particles = [ P.s__tilde__, P.d, P.nu_mu__tilde__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1184,(0,0):C.GC_200})

V_759 = Vertex(name = 'V_759',
               particles = [ P.s__tilde__, P.d, P.nu_tau__tilde__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1202,(0,0):C.GC_209})

V_760 = Vertex(name = 'V_760',
               particles = [ P.s__tilde__, P.d, P.nu_e__tilde__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1220,(0,0):C.GC_218})

V_761 = Vertex(name = 'V_761',
               particles = [ P.s__tilde__, P.d, P.nu_mu__tilde__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1238,(0,0):C.GC_227})

V_762 = Vertex(name = 'V_762',
               particles = [ P.s__tilde__, P.d, P.nu_tau__tilde__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1256,(0,0):C.GC_236})

V_763 = Vertex(name = 'V_763',
               particles = [ P.s__tilde__, P.d, P.nu_e__tilde__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1274,(0,0):C.GC_245})

V_764 = Vertex(name = 'V_764',
               particles = [ P.s__tilde__, P.d, P.nu_mu__tilde__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1292,(0,0):C.GC_254})

V_765 = Vertex(name = 'V_765',
               particles = [ P.s__tilde__, P.d, P.nu_tau__tilde__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1310,(0,0):C.GC_263})

V_766 = Vertex(name = 'V_766',
               particles = [ P.b__tilde__, P.d, P.nu_e__tilde__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1168,(0,0):C.GC_192})

V_767 = Vertex(name = 'V_767',
               particles = [ P.b__tilde__, P.d, P.nu_mu__tilde__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1186,(0,0):C.GC_201})

V_768 = Vertex(name = 'V_768',
               particles = [ P.b__tilde__, P.d, P.nu_tau__tilde__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1204,(0,0):C.GC_210})

V_769 = Vertex(name = 'V_769',
               particles = [ P.b__tilde__, P.d, P.nu_e__tilde__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1222,(0,0):C.GC_219})

V_770 = Vertex(name = 'V_770',
               particles = [ P.b__tilde__, P.d, P.nu_mu__tilde__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1240,(0,0):C.GC_228})

V_771 = Vertex(name = 'V_771',
               particles = [ P.b__tilde__, P.d, P.nu_tau__tilde__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1258,(0,0):C.GC_237})

V_772 = Vertex(name = 'V_772',
               particles = [ P.b__tilde__, P.d, P.nu_e__tilde__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1276,(0,0):C.GC_246})

V_773 = Vertex(name = 'V_773',
               particles = [ P.b__tilde__, P.d, P.nu_mu__tilde__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1294,(0,0):C.GC_255})

V_774 = Vertex(name = 'V_774',
               particles = [ P.b__tilde__, P.d, P.nu_tau__tilde__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1312,(0,0):C.GC_264})

V_775 = Vertex(name = 'V_775',
               particles = [ P.d__tilde__, P.s, P.nu_e__tilde__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1170,(0,0):C.GC_193})

V_776 = Vertex(name = 'V_776',
               particles = [ P.d__tilde__, P.s, P.nu_mu__tilde__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1188,(0,0):C.GC_202})

V_777 = Vertex(name = 'V_777',
               particles = [ P.d__tilde__, P.s, P.nu_tau__tilde__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1206,(0,0):C.GC_211})

V_778 = Vertex(name = 'V_778',
               particles = [ P.d__tilde__, P.s, P.nu_e__tilde__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1224,(0,0):C.GC_220})

V_779 = Vertex(name = 'V_779',
               particles = [ P.d__tilde__, P.s, P.nu_mu__tilde__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1242,(0,0):C.GC_229})

V_780 = Vertex(name = 'V_780',
               particles = [ P.d__tilde__, P.s, P.nu_tau__tilde__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1260,(0,0):C.GC_238})

V_781 = Vertex(name = 'V_781',
               particles = [ P.d__tilde__, P.s, P.nu_e__tilde__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1278,(0,0):C.GC_247})

V_782 = Vertex(name = 'V_782',
               particles = [ P.d__tilde__, P.s, P.nu_mu__tilde__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1296,(0,0):C.GC_256})

V_783 = Vertex(name = 'V_783',
               particles = [ P.d__tilde__, P.s, P.nu_tau__tilde__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1314,(0,0):C.GC_265})

V_784 = Vertex(name = 'V_784',
               particles = [ P.s__tilde__, P.s, P.nu_e__tilde__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1172,(0,0):C.GC_194})

V_785 = Vertex(name = 'V_785',
               particles = [ P.s__tilde__, P.s, P.nu_mu__tilde__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1190,(0,0):C.GC_203})

V_786 = Vertex(name = 'V_786',
               particles = [ P.s__tilde__, P.s, P.nu_tau__tilde__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1208,(0,0):C.GC_212})

V_787 = Vertex(name = 'V_787',
               particles = [ P.s__tilde__, P.s, P.nu_e__tilde__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1226,(0,0):C.GC_221})

V_788 = Vertex(name = 'V_788',
               particles = [ P.s__tilde__, P.s, P.nu_mu__tilde__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1244,(0,0):C.GC_230})

V_789 = Vertex(name = 'V_789',
               particles = [ P.s__tilde__, P.s, P.nu_tau__tilde__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1262,(0,0):C.GC_239})

V_790 = Vertex(name = 'V_790',
               particles = [ P.s__tilde__, P.s, P.nu_e__tilde__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1280,(0,0):C.GC_248})

V_791 = Vertex(name = 'V_791',
               particles = [ P.s__tilde__, P.s, P.nu_mu__tilde__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1298,(0,0):C.GC_257})

V_792 = Vertex(name = 'V_792',
               particles = [ P.s__tilde__, P.s, P.nu_tau__tilde__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1316,(0,0):C.GC_266})

V_793 = Vertex(name = 'V_793',
               particles = [ P.b__tilde__, P.s, P.nu_e__tilde__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1174,(0,0):C.GC_195})

V_794 = Vertex(name = 'V_794',
               particles = [ P.b__tilde__, P.s, P.nu_mu__tilde__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1192,(0,0):C.GC_204})

V_795 = Vertex(name = 'V_795',
               particles = [ P.b__tilde__, P.s, P.nu_tau__tilde__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1210,(0,0):C.GC_213})

V_796 = Vertex(name = 'V_796',
               particles = [ P.b__tilde__, P.s, P.nu_e__tilde__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1228,(0,0):C.GC_222})

V_797 = Vertex(name = 'V_797',
               particles = [ P.b__tilde__, P.s, P.nu_mu__tilde__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1246,(0,0):C.GC_231})

V_798 = Vertex(name = 'V_798',
               particles = [ P.b__tilde__, P.s, P.nu_tau__tilde__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1264,(0,0):C.GC_240})

V_799 = Vertex(name = 'V_799',
               particles = [ P.b__tilde__, P.s, P.nu_e__tilde__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1282,(0,0):C.GC_249})

V_800 = Vertex(name = 'V_800',
               particles = [ P.b__tilde__, P.s, P.nu_mu__tilde__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1300,(0,0):C.GC_258})

V_801 = Vertex(name = 'V_801',
               particles = [ P.b__tilde__, P.s, P.nu_tau__tilde__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1318,(0,0):C.GC_267})

V_802 = Vertex(name = 'V_802',
               particles = [ P.d__tilde__, P.b, P.nu_e__tilde__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1176,(0,0):C.GC_196})

V_803 = Vertex(name = 'V_803',
               particles = [ P.d__tilde__, P.b, P.nu_mu__tilde__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1194,(0,0):C.GC_205})

V_804 = Vertex(name = 'V_804',
               particles = [ P.d__tilde__, P.b, P.nu_tau__tilde__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1212,(0,0):C.GC_214})

V_805 = Vertex(name = 'V_805',
               particles = [ P.d__tilde__, P.b, P.nu_e__tilde__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1230,(0,0):C.GC_223})

V_806 = Vertex(name = 'V_806',
               particles = [ P.d__tilde__, P.b, P.nu_mu__tilde__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1248,(0,0):C.GC_232})

V_807 = Vertex(name = 'V_807',
               particles = [ P.d__tilde__, P.b, P.nu_tau__tilde__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1266,(0,0):C.GC_241})

V_808 = Vertex(name = 'V_808',
               particles = [ P.d__tilde__, P.b, P.nu_e__tilde__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1284,(0,0):C.GC_250})

V_809 = Vertex(name = 'V_809',
               particles = [ P.d__tilde__, P.b, P.nu_mu__tilde__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1302,(0,0):C.GC_259})

V_810 = Vertex(name = 'V_810',
               particles = [ P.d__tilde__, P.b, P.nu_tau__tilde__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1320,(0,0):C.GC_268})

V_811 = Vertex(name = 'V_811',
               particles = [ P.s__tilde__, P.b, P.nu_e__tilde__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1178,(0,0):C.GC_197})

V_812 = Vertex(name = 'V_812',
               particles = [ P.s__tilde__, P.b, P.nu_mu__tilde__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1196,(0,0):C.GC_206})

V_813 = Vertex(name = 'V_813',
               particles = [ P.s__tilde__, P.b, P.nu_tau__tilde__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1214,(0,0):C.GC_215})

V_814 = Vertex(name = 'V_814',
               particles = [ P.s__tilde__, P.b, P.nu_e__tilde__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1232,(0,0):C.GC_224})

V_815 = Vertex(name = 'V_815',
               particles = [ P.s__tilde__, P.b, P.nu_mu__tilde__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1250,(0,0):C.GC_233})

V_816 = Vertex(name = 'V_816',
               particles = [ P.s__tilde__, P.b, P.nu_tau__tilde__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1268,(0,0):C.GC_242})

V_817 = Vertex(name = 'V_817',
               particles = [ P.s__tilde__, P.b, P.nu_e__tilde__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1286,(0,0):C.GC_251})

V_818 = Vertex(name = 'V_818',
               particles = [ P.s__tilde__, P.b, P.nu_mu__tilde__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1304,(0,0):C.GC_260})

V_819 = Vertex(name = 'V_819',
               particles = [ P.s__tilde__, P.b, P.nu_tau__tilde__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1322,(0,0):C.GC_269})

V_820 = Vertex(name = 'V_820',
               particles = [ P.b__tilde__, P.b, P.nu_e__tilde__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1180,(0,0):C.GC_198})

V_821 = Vertex(name = 'V_821',
               particles = [ P.b__tilde__, P.b, P.nu_mu__tilde__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1198,(0,0):C.GC_207})

V_822 = Vertex(name = 'V_822',
               particles = [ P.b__tilde__, P.b, P.nu_tau__tilde__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1216,(0,0):C.GC_216})

V_823 = Vertex(name = 'V_823',
               particles = [ P.b__tilde__, P.b, P.nu_e__tilde__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1234,(0,0):C.GC_225})

V_824 = Vertex(name = 'V_824',
               particles = [ P.b__tilde__, P.b, P.nu_mu__tilde__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1252,(0,0):C.GC_234})

V_825 = Vertex(name = 'V_825',
               particles = [ P.b__tilde__, P.b, P.nu_tau__tilde__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1270,(0,0):C.GC_243})

V_826 = Vertex(name = 'V_826',
               particles = [ P.b__tilde__, P.b, P.nu_e__tilde__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1288,(0,0):C.GC_252})

V_827 = Vertex(name = 'V_827',
               particles = [ P.b__tilde__, P.b, P.nu_mu__tilde__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1306,(0,0):C.GC_261})

V_828 = Vertex(name = 'V_828',
               particles = [ P.b__tilde__, P.b, P.nu_tau__tilde__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF2, L.FFFF4 ],
               couplings = {(0,2):C.GC_847,(0,1):C.GC_928,(0,0):C.GC_270})

V_829 = Vertex(name = 'V_829',
               particles = [ P.e__plus__, P.e__minus__, P.nu_e__tilde__, P.nu_e ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_838})

V_830 = Vertex(name = 'V_830',
               particles = [ P.e__plus__, P.e__minus__, P.nu_mu__tilde__, P.nu_e ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1128})

V_831 = Vertex(name = 'V_831',
               particles = [ P.e__plus__, P.e__minus__, P.nu_tau__tilde__, P.nu_e ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1129})

V_832 = Vertex(name = 'V_832',
               particles = [ P.e__plus__, P.e__minus__, P.nu_e__tilde__, P.nu_mu ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1131})

V_833 = Vertex(name = 'V_833',
               particles = [ P.e__plus__, P.e__minus__, P.nu_mu__tilde__, P.nu_mu ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1134})

V_834 = Vertex(name = 'V_834',
               particles = [ P.e__plus__, P.e__minus__, P.nu_tau__tilde__, P.nu_mu ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1138})

V_835 = Vertex(name = 'V_835',
               particles = [ P.e__plus__, P.e__minus__, P.nu_e__tilde__, P.nu_tau ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1143})

V_836 = Vertex(name = 'V_836',
               particles = [ P.e__plus__, P.e__minus__, P.nu_mu__tilde__, P.nu_tau ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1149})

V_837 = Vertex(name = 'V_837',
               particles = [ P.e__plus__, P.e__minus__, P.nu_tau__tilde__, P.nu_tau ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1156})

V_838 = Vertex(name = 'V_838',
               particles = [ P.mu__plus__, P.e__minus__, P.nu_e__tilde__, P.nu_e ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1128})

V_839 = Vertex(name = 'V_839',
               particles = [ P.mu__plus__, P.e__minus__, P.nu_mu__tilde__, P.nu_e ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_839})

V_840 = Vertex(name = 'V_840',
               particles = [ P.mu__plus__, P.e__minus__, P.nu_tau__tilde__, P.nu_e ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1130})

V_841 = Vertex(name = 'V_841',
               particles = [ P.mu__plus__, P.e__minus__, P.nu_e__tilde__, P.nu_mu ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1132})

V_842 = Vertex(name = 'V_842',
               particles = [ P.mu__plus__, P.e__minus__, P.nu_mu__tilde__, P.nu_mu ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1135})

V_843 = Vertex(name = 'V_843',
               particles = [ P.mu__plus__, P.e__minus__, P.nu_tau__tilde__, P.nu_mu ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1139})

V_844 = Vertex(name = 'V_844',
               particles = [ P.mu__plus__, P.e__minus__, P.nu_e__tilde__, P.nu_tau ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1144})

V_845 = Vertex(name = 'V_845',
               particles = [ P.mu__plus__, P.e__minus__, P.nu_mu__tilde__, P.nu_tau ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1150})

V_846 = Vertex(name = 'V_846',
               particles = [ P.mu__plus__, P.e__minus__, P.nu_tau__tilde__, P.nu_tau ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1157})

V_847 = Vertex(name = 'V_847',
               particles = [ P.tau__plus__, P.e__minus__, P.nu_e__tilde__, P.nu_e ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1129})

V_848 = Vertex(name = 'V_848',
               particles = [ P.tau__plus__, P.e__minus__, P.nu_mu__tilde__, P.nu_e ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1130})

V_849 = Vertex(name = 'V_849',
               particles = [ P.tau__plus__, P.e__minus__, P.nu_tau__tilde__, P.nu_e ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_840})

V_850 = Vertex(name = 'V_850',
               particles = [ P.tau__plus__, P.e__minus__, P.nu_e__tilde__, P.nu_mu ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1133})

V_851 = Vertex(name = 'V_851',
               particles = [ P.tau__plus__, P.e__minus__, P.nu_mu__tilde__, P.nu_mu ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1136})

V_852 = Vertex(name = 'V_852',
               particles = [ P.tau__plus__, P.e__minus__, P.nu_tau__tilde__, P.nu_mu ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1140})

V_853 = Vertex(name = 'V_853',
               particles = [ P.tau__plus__, P.e__minus__, P.nu_e__tilde__, P.nu_tau ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1145})

V_854 = Vertex(name = 'V_854',
               particles = [ P.tau__plus__, P.e__minus__, P.nu_mu__tilde__, P.nu_tau ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1151})

V_855 = Vertex(name = 'V_855',
               particles = [ P.tau__plus__, P.e__minus__, P.nu_tau__tilde__, P.nu_tau ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1158})

V_856 = Vertex(name = 'V_856',
               particles = [ P.e__plus__, P.mu__minus__, P.nu_e__tilde__, P.nu_e ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1131})

V_857 = Vertex(name = 'V_857',
               particles = [ P.e__plus__, P.mu__minus__, P.nu_mu__tilde__, P.nu_e ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1132})

V_858 = Vertex(name = 'V_858',
               particles = [ P.e__plus__, P.mu__minus__, P.nu_tau__tilde__, P.nu_e ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1133})

V_859 = Vertex(name = 'V_859',
               particles = [ P.e__plus__, P.mu__minus__, P.nu_e__tilde__, P.nu_mu ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_841})

V_860 = Vertex(name = 'V_860',
               particles = [ P.e__plus__, P.mu__minus__, P.nu_mu__tilde__, P.nu_mu ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1137})

V_861 = Vertex(name = 'V_861',
               particles = [ P.e__plus__, P.mu__minus__, P.nu_tau__tilde__, P.nu_mu ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1141})

V_862 = Vertex(name = 'V_862',
               particles = [ P.e__plus__, P.mu__minus__, P.nu_e__tilde__, P.nu_tau ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1146})

V_863 = Vertex(name = 'V_863',
               particles = [ P.e__plus__, P.mu__minus__, P.nu_mu__tilde__, P.nu_tau ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1152})

V_864 = Vertex(name = 'V_864',
               particles = [ P.e__plus__, P.mu__minus__, P.nu_tau__tilde__, P.nu_tau ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1159})

V_865 = Vertex(name = 'V_865',
               particles = [ P.mu__plus__, P.mu__minus__, P.nu_e__tilde__, P.nu_e ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1134})

V_866 = Vertex(name = 'V_866',
               particles = [ P.mu__plus__, P.mu__minus__, P.nu_mu__tilde__, P.nu_e ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1135})

V_867 = Vertex(name = 'V_867',
               particles = [ P.mu__plus__, P.mu__minus__, P.nu_tau__tilde__, P.nu_e ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1136})

V_868 = Vertex(name = 'V_868',
               particles = [ P.mu__plus__, P.mu__minus__, P.nu_e__tilde__, P.nu_mu ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1137})

V_869 = Vertex(name = 'V_869',
               particles = [ P.mu__plus__, P.mu__minus__, P.nu_mu__tilde__, P.nu_mu ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_842})

V_870 = Vertex(name = 'V_870',
               particles = [ P.mu__plus__, P.mu__minus__, P.nu_tau__tilde__, P.nu_mu ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1142})

V_871 = Vertex(name = 'V_871',
               particles = [ P.mu__plus__, P.mu__minus__, P.nu_e__tilde__, P.nu_tau ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1147})

V_872 = Vertex(name = 'V_872',
               particles = [ P.mu__plus__, P.mu__minus__, P.nu_mu__tilde__, P.nu_tau ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1153})

V_873 = Vertex(name = 'V_873',
               particles = [ P.mu__plus__, P.mu__minus__, P.nu_tau__tilde__, P.nu_tau ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1160})

V_874 = Vertex(name = 'V_874',
               particles = [ P.tau__plus__, P.mu__minus__, P.nu_e__tilde__, P.nu_e ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1138})

V_875 = Vertex(name = 'V_875',
               particles = [ P.tau__plus__, P.mu__minus__, P.nu_mu__tilde__, P.nu_e ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1139})

V_876 = Vertex(name = 'V_876',
               particles = [ P.tau__plus__, P.mu__minus__, P.nu_tau__tilde__, P.nu_e ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1140})

V_877 = Vertex(name = 'V_877',
               particles = [ P.tau__plus__, P.mu__minus__, P.nu_e__tilde__, P.nu_mu ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1141})

V_878 = Vertex(name = 'V_878',
               particles = [ P.tau__plus__, P.mu__minus__, P.nu_mu__tilde__, P.nu_mu ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1142})

V_879 = Vertex(name = 'V_879',
               particles = [ P.tau__plus__, P.mu__minus__, P.nu_tau__tilde__, P.nu_mu ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_843})

V_880 = Vertex(name = 'V_880',
               particles = [ P.tau__plus__, P.mu__minus__, P.nu_e__tilde__, P.nu_tau ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1148})

V_881 = Vertex(name = 'V_881',
               particles = [ P.tau__plus__, P.mu__minus__, P.nu_mu__tilde__, P.nu_tau ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1154})

V_882 = Vertex(name = 'V_882',
               particles = [ P.tau__plus__, P.mu__minus__, P.nu_tau__tilde__, P.nu_tau ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1161})

V_883 = Vertex(name = 'V_883',
               particles = [ P.e__plus__, P.tau__minus__, P.nu_e__tilde__, P.nu_e ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1143})

V_884 = Vertex(name = 'V_884',
               particles = [ P.e__plus__, P.tau__minus__, P.nu_mu__tilde__, P.nu_e ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1144})

V_885 = Vertex(name = 'V_885',
               particles = [ P.e__plus__, P.tau__minus__, P.nu_tau__tilde__, P.nu_e ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1145})

V_886 = Vertex(name = 'V_886',
               particles = [ P.e__plus__, P.tau__minus__, P.nu_e__tilde__, P.nu_mu ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1146})

V_887 = Vertex(name = 'V_887',
               particles = [ P.e__plus__, P.tau__minus__, P.nu_mu__tilde__, P.nu_mu ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1147})

V_888 = Vertex(name = 'V_888',
               particles = [ P.e__plus__, P.tau__minus__, P.nu_tau__tilde__, P.nu_mu ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1148})

V_889 = Vertex(name = 'V_889',
               particles = [ P.e__plus__, P.tau__minus__, P.nu_e__tilde__, P.nu_tau ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_844})

V_890 = Vertex(name = 'V_890',
               particles = [ P.e__plus__, P.tau__minus__, P.nu_mu__tilde__, P.nu_tau ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1155})

V_891 = Vertex(name = 'V_891',
               particles = [ P.e__plus__, P.tau__minus__, P.nu_tau__tilde__, P.nu_tau ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1162})

V_892 = Vertex(name = 'V_892',
               particles = [ P.mu__plus__, P.tau__minus__, P.nu_e__tilde__, P.nu_e ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1149})

V_893 = Vertex(name = 'V_893',
               particles = [ P.mu__plus__, P.tau__minus__, P.nu_mu__tilde__, P.nu_e ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1150})

V_894 = Vertex(name = 'V_894',
               particles = [ P.mu__plus__, P.tau__minus__, P.nu_tau__tilde__, P.nu_e ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1151})

V_895 = Vertex(name = 'V_895',
               particles = [ P.mu__plus__, P.tau__minus__, P.nu_e__tilde__, P.nu_mu ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1152})

V_896 = Vertex(name = 'V_896',
               particles = [ P.mu__plus__, P.tau__minus__, P.nu_mu__tilde__, P.nu_mu ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1153})

V_897 = Vertex(name = 'V_897',
               particles = [ P.mu__plus__, P.tau__minus__, P.nu_tau__tilde__, P.nu_mu ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1154})

V_898 = Vertex(name = 'V_898',
               particles = [ P.mu__plus__, P.tau__minus__, P.nu_e__tilde__, P.nu_tau ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1155})

V_899 = Vertex(name = 'V_899',
               particles = [ P.mu__plus__, P.tau__minus__, P.nu_mu__tilde__, P.nu_tau ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_845})

V_900 = Vertex(name = 'V_900',
               particles = [ P.mu__plus__, P.tau__minus__, P.nu_tau__tilde__, P.nu_tau ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1163})

V_901 = Vertex(name = 'V_901',
               particles = [ P.tau__plus__, P.tau__minus__, P.nu_e__tilde__, P.nu_e ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1156})

V_902 = Vertex(name = 'V_902',
               particles = [ P.tau__plus__, P.tau__minus__, P.nu_mu__tilde__, P.nu_e ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1157})

V_903 = Vertex(name = 'V_903',
               particles = [ P.tau__plus__, P.tau__minus__, P.nu_tau__tilde__, P.nu_e ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1158})

V_904 = Vertex(name = 'V_904',
               particles = [ P.tau__plus__, P.tau__minus__, P.nu_e__tilde__, P.nu_mu ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1159})

V_905 = Vertex(name = 'V_905',
               particles = [ P.tau__plus__, P.tau__minus__, P.nu_mu__tilde__, P.nu_mu ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1160})

V_906 = Vertex(name = 'V_906',
               particles = [ P.tau__plus__, P.tau__minus__, P.nu_tau__tilde__, P.nu_mu ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1161})

V_907 = Vertex(name = 'V_907',
               particles = [ P.tau__plus__, P.tau__minus__, P.nu_e__tilde__, P.nu_tau ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1162})

V_908 = Vertex(name = 'V_908',
               particles = [ P.tau__plus__, P.tau__minus__, P.nu_mu__tilde__, P.nu_tau ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_1163})

V_909 = Vertex(name = 'V_909',
               particles = [ P.tau__plus__, P.tau__minus__, P.nu_tau__tilde__, P.nu_tau ],
               color = [ '1' ],
               lorentz = [ L.FFFF4 ],
               couplings = {(0,0):C.GC_846})

V_910 = Vertex(name = 'V_910',
               particles = [ P.u__tilde__, P.u, P.nu_e__tilde__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1165,(0,0):C.GC_930})

V_911 = Vertex(name = 'V_911',
               particles = [ P.u__tilde__, P.u, P.nu_mu__tilde__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1183,(0,0):C.GC_939})

V_912 = Vertex(name = 'V_912',
               particles = [ P.u__tilde__, P.u, P.nu_tau__tilde__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1201,(0,0):C.GC_948})

V_913 = Vertex(name = 'V_913',
               particles = [ P.u__tilde__, P.u, P.nu_e__tilde__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1219,(0,0):C.GC_957})

V_914 = Vertex(name = 'V_914',
               particles = [ P.u__tilde__, P.u, P.nu_mu__tilde__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1237,(0,0):C.GC_966})

V_915 = Vertex(name = 'V_915',
               particles = [ P.u__tilde__, P.u, P.nu_tau__tilde__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1255,(0,0):C.GC_975})

V_916 = Vertex(name = 'V_916',
               particles = [ P.u__tilde__, P.u, P.nu_e__tilde__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1273,(0,0):C.GC_984})

V_917 = Vertex(name = 'V_917',
               particles = [ P.u__tilde__, P.u, P.nu_mu__tilde__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1291,(0,0):C.GC_993})

V_918 = Vertex(name = 'V_918',
               particles = [ P.u__tilde__, P.u, P.nu_tau__tilde__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1309,(0,0):C.GC_1002})

V_919 = Vertex(name = 'V_919',
               particles = [ P.c__tilde__, P.u, P.nu_e__tilde__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1167,(0,0):C.GC_931})

V_920 = Vertex(name = 'V_920',
               particles = [ P.c__tilde__, P.u, P.nu_mu__tilde__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1185,(0,0):C.GC_940})

V_921 = Vertex(name = 'V_921',
               particles = [ P.c__tilde__, P.u, P.nu_tau__tilde__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1203,(0,0):C.GC_949})

V_922 = Vertex(name = 'V_922',
               particles = [ P.c__tilde__, P.u, P.nu_e__tilde__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1221,(0,0):C.GC_958})

V_923 = Vertex(name = 'V_923',
               particles = [ P.c__tilde__, P.u, P.nu_mu__tilde__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1239,(0,0):C.GC_967})

V_924 = Vertex(name = 'V_924',
               particles = [ P.c__tilde__, P.u, P.nu_tau__tilde__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1257,(0,0):C.GC_976})

V_925 = Vertex(name = 'V_925',
               particles = [ P.c__tilde__, P.u, P.nu_e__tilde__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1275,(0,0):C.GC_985})

V_926 = Vertex(name = 'V_926',
               particles = [ P.c__tilde__, P.u, P.nu_mu__tilde__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1293,(0,0):C.GC_994})

V_927 = Vertex(name = 'V_927',
               particles = [ P.c__tilde__, P.u, P.nu_tau__tilde__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1311,(0,0):C.GC_1003})

V_928 = Vertex(name = 'V_928',
               particles = [ P.t__tilde__, P.u, P.nu_e__tilde__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1169,(0,0):C.GC_932})

V_929 = Vertex(name = 'V_929',
               particles = [ P.t__tilde__, P.u, P.nu_mu__tilde__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1187,(0,0):C.GC_941})

V_930 = Vertex(name = 'V_930',
               particles = [ P.t__tilde__, P.u, P.nu_tau__tilde__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1205,(0,0):C.GC_950})

V_931 = Vertex(name = 'V_931',
               particles = [ P.t__tilde__, P.u, P.nu_e__tilde__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1223,(0,0):C.GC_959})

V_932 = Vertex(name = 'V_932',
               particles = [ P.t__tilde__, P.u, P.nu_mu__tilde__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1241,(0,0):C.GC_968})

V_933 = Vertex(name = 'V_933',
               particles = [ P.t__tilde__, P.u, P.nu_tau__tilde__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1259,(0,0):C.GC_977})

V_934 = Vertex(name = 'V_934',
               particles = [ P.t__tilde__, P.u, P.nu_e__tilde__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1277,(0,0):C.GC_986})

V_935 = Vertex(name = 'V_935',
               particles = [ P.t__tilde__, P.u, P.nu_mu__tilde__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1295,(0,0):C.GC_995})

V_936 = Vertex(name = 'V_936',
               particles = [ P.t__tilde__, P.u, P.nu_tau__tilde__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1313,(0,0):C.GC_1004})

V_937 = Vertex(name = 'V_937',
               particles = [ P.u__tilde__, P.c, P.nu_e__tilde__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1171,(0,0):C.GC_933})

V_938 = Vertex(name = 'V_938',
               particles = [ P.u__tilde__, P.c, P.nu_mu__tilde__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1189,(0,0):C.GC_942})

V_939 = Vertex(name = 'V_939',
               particles = [ P.u__tilde__, P.c, P.nu_tau__tilde__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1207,(0,0):C.GC_951})

V_940 = Vertex(name = 'V_940',
               particles = [ P.u__tilde__, P.c, P.nu_e__tilde__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1225,(0,0):C.GC_960})

V_941 = Vertex(name = 'V_941',
               particles = [ P.u__tilde__, P.c, P.nu_mu__tilde__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1243,(0,0):C.GC_969})

V_942 = Vertex(name = 'V_942',
               particles = [ P.u__tilde__, P.c, P.nu_tau__tilde__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1261,(0,0):C.GC_978})

V_943 = Vertex(name = 'V_943',
               particles = [ P.u__tilde__, P.c, P.nu_e__tilde__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1279,(0,0):C.GC_987})

V_944 = Vertex(name = 'V_944',
               particles = [ P.u__tilde__, P.c, P.nu_mu__tilde__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1297,(0,0):C.GC_996})

V_945 = Vertex(name = 'V_945',
               particles = [ P.u__tilde__, P.c, P.nu_tau__tilde__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1315,(0,0):C.GC_1005})

V_946 = Vertex(name = 'V_946',
               particles = [ P.c__tilde__, P.c, P.nu_e__tilde__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1173,(0,0):C.GC_934})

V_947 = Vertex(name = 'V_947',
               particles = [ P.c__tilde__, P.c, P.nu_mu__tilde__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1191,(0,0):C.GC_943})

V_948 = Vertex(name = 'V_948',
               particles = [ P.c__tilde__, P.c, P.nu_tau__tilde__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1209,(0,0):C.GC_952})

V_949 = Vertex(name = 'V_949',
               particles = [ P.c__tilde__, P.c, P.nu_e__tilde__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1227,(0,0):C.GC_961})

V_950 = Vertex(name = 'V_950',
               particles = [ P.c__tilde__, P.c, P.nu_mu__tilde__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1245,(0,0):C.GC_970})

V_951 = Vertex(name = 'V_951',
               particles = [ P.c__tilde__, P.c, P.nu_tau__tilde__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1263,(0,0):C.GC_979})

V_952 = Vertex(name = 'V_952',
               particles = [ P.c__tilde__, P.c, P.nu_e__tilde__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1281,(0,0):C.GC_988})

V_953 = Vertex(name = 'V_953',
               particles = [ P.c__tilde__, P.c, P.nu_mu__tilde__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1299,(0,0):C.GC_997})

V_954 = Vertex(name = 'V_954',
               particles = [ P.c__tilde__, P.c, P.nu_tau__tilde__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1317,(0,0):C.GC_1006})

V_955 = Vertex(name = 'V_955',
               particles = [ P.t__tilde__, P.c, P.nu_e__tilde__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1175,(0,0):C.GC_935})

V_956 = Vertex(name = 'V_956',
               particles = [ P.t__tilde__, P.c, P.nu_mu__tilde__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1193,(0,0):C.GC_944})

V_957 = Vertex(name = 'V_957',
               particles = [ P.t__tilde__, P.c, P.nu_tau__tilde__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1211,(0,0):C.GC_953})

V_958 = Vertex(name = 'V_958',
               particles = [ P.t__tilde__, P.c, P.nu_e__tilde__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1229,(0,0):C.GC_962})

V_959 = Vertex(name = 'V_959',
               particles = [ P.t__tilde__, P.c, P.nu_mu__tilde__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1247,(0,0):C.GC_971})

V_960 = Vertex(name = 'V_960',
               particles = [ P.t__tilde__, P.c, P.nu_tau__tilde__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1265,(0,0):C.GC_980})

V_961 = Vertex(name = 'V_961',
               particles = [ P.t__tilde__, P.c, P.nu_e__tilde__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1283,(0,0):C.GC_989})

V_962 = Vertex(name = 'V_962',
               particles = [ P.t__tilde__, P.c, P.nu_mu__tilde__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1301,(0,0):C.GC_998})

V_963 = Vertex(name = 'V_963',
               particles = [ P.t__tilde__, P.c, P.nu_tau__tilde__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1319,(0,0):C.GC_1007})

V_964 = Vertex(name = 'V_964',
               particles = [ P.u__tilde__, P.t, P.nu_e__tilde__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1177,(0,0):C.GC_936})

V_965 = Vertex(name = 'V_965',
               particles = [ P.u__tilde__, P.t, P.nu_mu__tilde__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1195,(0,0):C.GC_945})

V_966 = Vertex(name = 'V_966',
               particles = [ P.u__tilde__, P.t, P.nu_tau__tilde__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1213,(0,0):C.GC_954})

V_967 = Vertex(name = 'V_967',
               particles = [ P.u__tilde__, P.t, P.nu_e__tilde__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1231,(0,0):C.GC_963})

V_968 = Vertex(name = 'V_968',
               particles = [ P.u__tilde__, P.t, P.nu_mu__tilde__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1249,(0,0):C.GC_972})

V_969 = Vertex(name = 'V_969',
               particles = [ P.u__tilde__, P.t, P.nu_tau__tilde__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1267,(0,0):C.GC_981})

V_970 = Vertex(name = 'V_970',
               particles = [ P.u__tilde__, P.t, P.nu_e__tilde__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1285,(0,0):C.GC_990})

V_971 = Vertex(name = 'V_971',
               particles = [ P.u__tilde__, P.t, P.nu_mu__tilde__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1303,(0,0):C.GC_999})

V_972 = Vertex(name = 'V_972',
               particles = [ P.u__tilde__, P.t, P.nu_tau__tilde__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1321,(0,0):C.GC_1008})

V_973 = Vertex(name = 'V_973',
               particles = [ P.c__tilde__, P.t, P.nu_e__tilde__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1179,(0,0):C.GC_937})

V_974 = Vertex(name = 'V_974',
               particles = [ P.c__tilde__, P.t, P.nu_mu__tilde__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1197,(0,0):C.GC_946})

V_975 = Vertex(name = 'V_975',
               particles = [ P.c__tilde__, P.t, P.nu_tau__tilde__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1215,(0,0):C.GC_955})

V_976 = Vertex(name = 'V_976',
               particles = [ P.c__tilde__, P.t, P.nu_e__tilde__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1233,(0,0):C.GC_964})

V_977 = Vertex(name = 'V_977',
               particles = [ P.c__tilde__, P.t, P.nu_mu__tilde__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1251,(0,0):C.GC_973})

V_978 = Vertex(name = 'V_978',
               particles = [ P.c__tilde__, P.t, P.nu_tau__tilde__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1269,(0,0):C.GC_982})

V_979 = Vertex(name = 'V_979',
               particles = [ P.c__tilde__, P.t, P.nu_e__tilde__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1287,(0,0):C.GC_991})

V_980 = Vertex(name = 'V_980',
               particles = [ P.c__tilde__, P.t, P.nu_mu__tilde__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1305,(0,0):C.GC_1000})

V_981 = Vertex(name = 'V_981',
               particles = [ P.c__tilde__, P.t, P.nu_tau__tilde__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1323,(0,0):C.GC_1009})

V_982 = Vertex(name = 'V_982',
               particles = [ P.t__tilde__, P.t, P.nu_e__tilde__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1181,(0,0):C.GC_938})

V_983 = Vertex(name = 'V_983',
               particles = [ P.t__tilde__, P.t, P.nu_mu__tilde__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1199,(0,0):C.GC_947})

V_984 = Vertex(name = 'V_984',
               particles = [ P.t__tilde__, P.t, P.nu_tau__tilde__, P.nu_e ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1217,(0,0):C.GC_956})

V_985 = Vertex(name = 'V_985',
               particles = [ P.t__tilde__, P.t, P.nu_e__tilde__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1235,(0,0):C.GC_965})

V_986 = Vertex(name = 'V_986',
               particles = [ P.t__tilde__, P.t, P.nu_mu__tilde__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1253,(0,0):C.GC_974})

V_987 = Vertex(name = 'V_987',
               particles = [ P.t__tilde__, P.t, P.nu_tau__tilde__, P.nu_mu ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1271,(0,0):C.GC_983})

V_988 = Vertex(name = 'V_988',
               particles = [ P.t__tilde__, P.t, P.nu_e__tilde__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1289,(0,0):C.GC_992})

V_989 = Vertex(name = 'V_989',
               particles = [ P.t__tilde__, P.t, P.nu_mu__tilde__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1307,(0,0):C.GC_1001})

V_990 = Vertex(name = 'V_990',
               particles = [ P.t__tilde__, P.t, P.nu_tau__tilde__, P.nu_tau ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFFF11, L.FFFF4 ],
               couplings = {(0,1):C.GC_1325,(0,0):C.GC_1010})

V_991 = Vertex(name = 'V_991',
               particles = [ P.nu_e__tilde__, P.nu_e, P.nu_e__tilde__, P.nu_e ],
               color = [ '1' ],
               lorentz = [ L.FFFF3, L.FFFF4 ],
               couplings = {(0,0):C.GC_838,(0,1):C.GC_838})

V_992 = Vertex(name = 'V_992',
               particles = [ P.nu_mu__tilde__, P.nu_e, P.nu_e__tilde__, P.nu_e ],
               color = [ '1' ],
               lorentz = [ L.FFFF3, L.FFFF4 ],
               couplings = {(0,0):C.GC_1128,(0,1):C.GC_1128})

V_993 = Vertex(name = 'V_993',
               particles = [ P.nu_tau__tilde__, P.nu_e, P.nu_e__tilde__, P.nu_e ],
               color = [ '1' ],
               lorentz = [ L.FFFF3, L.FFFF4 ],
               couplings = {(0,0):C.GC_1129,(0,1):C.GC_1129})

V_994 = Vertex(name = 'V_994',
               particles = [ P.nu_e__tilde__, P.nu_e, P.nu_e__tilde__, P.nu_mu ],
               color = [ '1' ],
               lorentz = [ L.FFFF3, L.FFFF4 ],
               couplings = {(0,0):C.GC_1131,(0,1):C.GC_1131})

V_995 = Vertex(name = 'V_995',
               particles = [ P.nu_mu__tilde__, P.nu_e, P.nu_e__tilde__, P.nu_mu ],
               color = [ '1' ],
               lorentz = [ L.FFFF3, L.FFFF4 ],
               couplings = {(0,0):C.GC_1134,(0,1):C.GC_1132})

V_996 = Vertex(name = 'V_996',
               particles = [ P.nu_tau__tilde__, P.nu_e, P.nu_e__tilde__, P.nu_mu ],
               color = [ '1' ],
               lorentz = [ L.FFFF3, L.FFFF4 ],
               couplings = {(0,0):C.GC_1138,(0,1):C.GC_1133})

V_997 = Vertex(name = 'V_997',
               particles = [ P.nu_e__tilde__, P.nu_e, P.nu_e__tilde__, P.nu_tau ],
               color = [ '1' ],
               lorentz = [ L.FFFF3, L.FFFF4 ],
               couplings = {(0,0):C.GC_1143,(0,1):C.GC_1143})

V_998 = Vertex(name = 'V_998',
               particles = [ P.nu_mu__tilde__, P.nu_e, P.nu_e__tilde__, P.nu_tau ],
               color = [ '1' ],
               lorentz = [ L.FFFF3, L.FFFF4 ],
               couplings = {(0,0):C.GC_1149,(0,1):C.GC_1144})

V_999 = Vertex(name = 'V_999',
               particles = [ P.nu_tau__tilde__, P.nu_e, P.nu_e__tilde__, P.nu_tau ],
               color = [ '1' ],
               lorentz = [ L.FFFF3, L.FFFF4 ],
               couplings = {(0,0):C.GC_1156,(0,1):C.GC_1145})

V_1000 = Vertex(name = 'V_1000',
                particles = [ P.nu_mu__tilde__, P.nu_e, P.nu_mu__tilde__, P.nu_e ],
                color = [ '1' ],
                lorentz = [ L.FFFF3, L.FFFF4 ],
                couplings = {(0,0):C.GC_839,(0,1):C.GC_839})

V_1001 = Vertex(name = 'V_1001',
                particles = [ P.nu_tau__tilde__, P.nu_e, P.nu_mu__tilde__, P.nu_e ],
                color = [ '1' ],
                lorentz = [ L.FFFF3, L.FFFF4 ],
                couplings = {(0,0):C.GC_1130,(0,1):C.GC_1130})

V_1002 = Vertex(name = 'V_1002',
                particles = [ P.nu_mu__tilde__, P.nu_e, P.nu_mu__tilde__, P.nu_mu ],
                color = [ '1' ],
                lorentz = [ L.FFFF3, L.FFFF4 ],
                couplings = {(0,0):C.GC_1135,(0,1):C.GC_1135})

V_1003 = Vertex(name = 'V_1003',
                particles = [ P.nu_tau__tilde__, P.nu_e, P.nu_mu__tilde__, P.nu_mu ],
                color = [ '1' ],
                lorentz = [ L.FFFF3, L.FFFF4 ],
                couplings = {(0,0):C.GC_1139,(0,1):C.GC_1136})

V_1004 = Vertex(name = 'V_1004',
                particles = [ P.nu_mu__tilde__, P.nu_e, P.nu_mu__tilde__, P.nu_tau ],
                color = [ '1' ],
                lorentz = [ L.FFFF3, L.FFFF4 ],
                couplings = {(0,0):C.GC_1150,(0,1):C.GC_1150})

V_1005 = Vertex(name = 'V_1005',
                particles = [ P.nu_tau__tilde__, P.nu_e, P.nu_mu__tilde__, P.nu_tau ],
                color = [ '1' ],
                lorentz = [ L.FFFF3, L.FFFF4 ],
                couplings = {(0,0):C.GC_1157,(0,1):C.GC_1151})

V_1006 = Vertex(name = 'V_1006',
                particles = [ P.nu_tau__tilde__, P.nu_e, P.nu_tau__tilde__, P.nu_e ],
                color = [ '1' ],
                lorentz = [ L.FFFF3, L.FFFF4 ],
                couplings = {(0,0):C.GC_840,(0,1):C.GC_840})

V_1007 = Vertex(name = 'V_1007',
                particles = [ P.nu_tau__tilde__, P.nu_e, P.nu_tau__tilde__, P.nu_mu ],
                color = [ '1' ],
                lorentz = [ L.FFFF3, L.FFFF4 ],
                couplings = {(0,0):C.GC_1140,(0,1):C.GC_1140})

V_1008 = Vertex(name = 'V_1008',
                particles = [ P.nu_tau__tilde__, P.nu_e, P.nu_tau__tilde__, P.nu_tau ],
                color = [ '1' ],
                lorentz = [ L.FFFF3, L.FFFF4 ],
                couplings = {(0,0):C.GC_1158,(0,1):C.GC_1158})

V_1009 = Vertex(name = 'V_1009',
                particles = [ P.nu_e__tilde__, P.nu_mu, P.nu_e__tilde__, P.nu_mu ],
                color = [ '1' ],
                lorentz = [ L.FFFF3, L.FFFF4 ],
                couplings = {(0,0):C.GC_841,(0,1):C.GC_841})

V_1010 = Vertex(name = 'V_1010',
                particles = [ P.nu_mu__tilde__, P.nu_mu, P.nu_e__tilde__, P.nu_mu ],
                color = [ '1' ],
                lorentz = [ L.FFFF3, L.FFFF4 ],
                couplings = {(0,0):C.GC_1137,(0,1):C.GC_1137})

V_1011 = Vertex(name = 'V_1011',
                particles = [ P.nu_tau__tilde__, P.nu_mu, P.nu_e__tilde__, P.nu_mu ],
                color = [ '1' ],
                lorentz = [ L.FFFF3, L.FFFF4 ],
                couplings = {(0,0):C.GC_1141,(0,1):C.GC_1141})

V_1012 = Vertex(name = 'V_1012',
                particles = [ P.nu_e__tilde__, P.nu_mu, P.nu_e__tilde__, P.nu_tau ],
                color = [ '1' ],
                lorentz = [ L.FFFF3, L.FFFF4 ],
                couplings = {(0,0):C.GC_1146,(0,1):C.GC_1146})

V_1013 = Vertex(name = 'V_1013',
                particles = [ P.nu_mu__tilde__, P.nu_mu, P.nu_e__tilde__, P.nu_tau ],
                color = [ '1' ],
                lorentz = [ L.FFFF3, L.FFFF4 ],
                couplings = {(0,0):C.GC_1152,(0,1):C.GC_1147})

V_1014 = Vertex(name = 'V_1014',
                particles = [ P.nu_tau__tilde__, P.nu_mu, P.nu_e__tilde__, P.nu_tau ],
                color = [ '1' ],
                lorentz = [ L.FFFF3, L.FFFF4 ],
                couplings = {(0,0):C.GC_1159,(0,1):C.GC_1148})

V_1015 = Vertex(name = 'V_1015',
                particles = [ P.nu_mu__tilde__, P.nu_mu, P.nu_mu__tilde__, P.nu_mu ],
                color = [ '1' ],
                lorentz = [ L.FFFF3, L.FFFF4 ],
                couplings = {(0,0):C.GC_842,(0,1):C.GC_842})

V_1016 = Vertex(name = 'V_1016',
                particles = [ P.nu_tau__tilde__, P.nu_mu, P.nu_mu__tilde__, P.nu_mu ],
                color = [ '1' ],
                lorentz = [ L.FFFF3, L.FFFF4 ],
                couplings = {(0,0):C.GC_1142,(0,1):C.GC_1142})

V_1017 = Vertex(name = 'V_1017',
                particles = [ P.nu_mu__tilde__, P.nu_mu, P.nu_mu__tilde__, P.nu_tau ],
                color = [ '1' ],
                lorentz = [ L.FFFF3, L.FFFF4 ],
                couplings = {(0,0):C.GC_1153,(0,1):C.GC_1153})

V_1018 = Vertex(name = 'V_1018',
                particles = [ P.nu_tau__tilde__, P.nu_mu, P.nu_mu__tilde__, P.nu_tau ],
                color = [ '1' ],
                lorentz = [ L.FFFF3, L.FFFF4 ],
                couplings = {(0,0):C.GC_1160,(0,1):C.GC_1154})

V_1019 = Vertex(name = 'V_1019',
                particles = [ P.nu_tau__tilde__, P.nu_mu, P.nu_tau__tilde__, P.nu_mu ],
                color = [ '1' ],
                lorentz = [ L.FFFF3, L.FFFF4 ],
                couplings = {(0,0):C.GC_843,(0,1):C.GC_843})

V_1020 = Vertex(name = 'V_1020',
                particles = [ P.nu_tau__tilde__, P.nu_mu, P.nu_tau__tilde__, P.nu_tau ],
                color = [ '1' ],
                lorentz = [ L.FFFF3, L.FFFF4 ],
                couplings = {(0,0):C.GC_1161,(0,1):C.GC_1161})

V_1021 = Vertex(name = 'V_1021',
                particles = [ P.nu_e__tilde__, P.nu_tau, P.nu_e__tilde__, P.nu_tau ],
                color = [ '1' ],
                lorentz = [ L.FFFF3, L.FFFF4 ],
                couplings = {(0,0):C.GC_844,(0,1):C.GC_844})

V_1022 = Vertex(name = 'V_1022',
                particles = [ P.nu_mu__tilde__, P.nu_tau, P.nu_e__tilde__, P.nu_tau ],
                color = [ '1' ],
                lorentz = [ L.FFFF3, L.FFFF4 ],
                couplings = {(0,0):C.GC_1155,(0,1):C.GC_1155})

V_1023 = Vertex(name = 'V_1023',
                particles = [ P.nu_tau__tilde__, P.nu_tau, P.nu_e__tilde__, P.nu_tau ],
                color = [ '1' ],
                lorentz = [ L.FFFF3, L.FFFF4 ],
                couplings = {(0,0):C.GC_1162,(0,1):C.GC_1162})

V_1024 = Vertex(name = 'V_1024',
                particles = [ P.nu_mu__tilde__, P.nu_tau, P.nu_mu__tilde__, P.nu_tau ],
                color = [ '1' ],
                lorentz = [ L.FFFF3, L.FFFF4 ],
                couplings = {(0,0):C.GC_845,(0,1):C.GC_845})

V_1025 = Vertex(name = 'V_1025',
                particles = [ P.nu_tau__tilde__, P.nu_tau, P.nu_mu__tilde__, P.nu_tau ],
                color = [ '1' ],
                lorentz = [ L.FFFF3, L.FFFF4 ],
                couplings = {(0,0):C.GC_1163,(0,1):C.GC_1163})

V_1026 = Vertex(name = 'V_1026',
                particles = [ P.nu_tau__tilde__, P.nu_tau, P.nu_tau__tilde__, P.nu_tau ],
                color = [ '1' ],
                lorentz = [ L.FFFF3, L.FFFF4 ],
                couplings = {(0,0):C.GC_846,(0,1):C.GC_846})

