import os
from rept1l.autoct.modelfile import model, CTParameter
from rept1l.autoct.modelfile import TreeInducedCTVertex, CTVertex
from rept1l.autoct.auto_parameters_ct import auto_assign_ct_particle
from rept1l.autoct.auto_parameters_ct import set_light_particle
from rept1l.autoct.auto_parameters_ct import assign_counterterm
from rept1l.autoct.auto_parameters_ct import add_counterterm
from rept1l.autoct.auto_tadpole_ct import auto_tadpole_vertices
from rept1l.autoct.auto_tadpole_ct import assign_FJTadpole_field_shift


Coupling = model.object_library.Coupling

param = model.parameters
P = model.particles
L = model.lorentz

import rept1l.autoct.lorentz_ct as LCT


modelname = 'SMEFT fermions'
modelgauge = "'t Hooft-Feynman"

features = {'fermionloop_opt' : False,
            'qcd_rescaling' : True,
            'sm_generation_opt': False
           }
parameter_orders = {param.aEW: {'QED': 2},
                    param.aS: {'QCD': 2},
                    param.qeddim: {'QED': 1},
                    param.v6ov: {'LAM': 1},
                    param.ld6ol: {'LAM': 1},
                    param.muH2d6: {'LAM': 1},
                    param.eed6: {'LAM': 1},
                    param.lamb: {'QED': 2},
                    # param.Lam: {'LAM': 1}
                    }
#                    param.realgw: {'QED': 1},
#                    param.realvev: {'QED': -1}}


for p in [P.u, P.d, P.c, P.s, P.b]:
  set_light_particle(p)

assign_counterterm(param.G, 'dZgs', 'G*dZgs')
auto_assign_ct_particle(selection=[P.u,P.d,P.c,P.s,P.t,P.b,P.g])
