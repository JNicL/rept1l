from __future__ import print_function

import os
from rept1l.modeltests.modeltest_utils.auto_model import GenModel, GenBareModel
import rept1l.modeltests.SM_HEFT_tests.sm_heft_setup as smht

class GenSMHEFT(GenModel):
  threads = 8
  ct_expansion = True
  pre_ct = 'smheftexp.txt'

  def compute_rational(self, **kwargs):
    cls = self.__class__
    qcd = [0, 1, 2]
    qed = [0]
    orders = {'qcd': qcd, 'qed': qed}
    self.run_r2(vertices_file='smheftr2.txt', np=2, nc=True, npc=True,
                threads=cls.threads, orders=orders)
    qcd = [0, 1, 2, 3, 4]
    qed = [0, 1]
    orders = {'qcd': qcd, 'qed': qed}
    self.run_r2(vertices_file='smheftr2.txt', np=3, nc=True, npc=True,
                threads=cls.threads, orders=orders)
    qcd = [0, 1, 2, 3, 4, 5]
    qed = [0, 1, 2]
    orders = {'qcd': qcd, 'qed': qed}
    self.run_r2(vertices_file='smheftr2.txt', np=4, nc=True, npc=True,
                threads=cls.threads, orders=orders)
    qcd = [0, 1, 2, 3, 4, 5, 6]
    qed = [0, 1, 2]
    orders = {'qcd': qcd, 'qed': qed}
    self.run_r2(vertices_file='smheftr2.txt', np=5, nc=True, npc=True,
                threads=cls.threads, orders=orders)

    qcd = [0, 1, 2, 3, 4, 5]
    qed = [0, 1, 2]
    orders = {'qcd': qcd, 'qed': qed}
    self.run_r2(vertices_file='smheftH2r2.txt', np=4, nc=True, npc=True,
                threads=cls.threads, orders=orders)
    qcd = [0, 1, 2, 3, 4, 5, 6]
    qed = [0, 1, 2]
    orders = {'qcd': qcd, 'qed': qed}
    self.run_r2(vertices_file='smheftH2r2.txt', np=5, nc=True, npc=True,
                threads=cls.threads, orders=orders)

    qcd = [4]
    qed = [3]
    orders = {'qcd': qcd, 'qed': qed}
    self.run_r2(vertices_file='smheftH3r2.txt', np=5, nc=True, npc=True,
                threads=cls.threads, orders=orders)

  def renormalize_model(self, **kwargs):

    from rept1l.renormalize import RenoConst as RC
    from sympy import sympify
    RC('dcgghfin', sympify('11*aS/(4*pi)'))

    no_qed_order = ('-qcd', '0', '1', '2', '3', '-qed', '0')

    # QCD charged fields
    self.renormalize_qcd('-o', 'particles_nf5', *no_qed_order, **kwargs)

    # QCD coupling
    for i in ['5', '4']:
      self.renormalize_qcd('-o', 'alphaS', '-nf', i, *no_qed_order, **kwargs)

    self.register_ct()


def sm_heft_model(outputpath):
  GenSMHEFT.driverpath = smht.driver_sm_heft_path
  GenSMHEFT.outputpath = outputpath
  GenSMHEFT(odp=True, bfm=False, use_run_ct=True)

def sm_heft_bfm_model(outputpath):
  GenSMHEFT.driverpath = smht.driver_sm_heft_bfm_path
  GenSMHEFT.outputpath = outputpath
  GenSMHEFT(odp=True, bfm=True, use_run_ct=True)

if __name__ == "__main__":
  sm_heft_bfm_model('/home/nick/SM_HEFT_BFM_2.2.3')
