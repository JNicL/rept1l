################################################################################
#                               user_settings.py                               #
################################################################################

import os
from rept1l.autoct.modelfile import model, CTParameter
from rept1l.autoct.modelfile import TreeInducedCTVertex, CTVertex
from rept1l.autoct.auto_parameters_ct import auto_assign_ct_particle
from rept1l.autoct.auto_parameters_ct import set_light_particle
from rept1l.autoct.auto_parameters_ct import assign_counterterm
from rept1l.autoct.auto_parameters_ct import add_counterterm
from rept1l.autoct.auto_tadpole_ct import auto_tadpole_vertices
from rept1l.autoct.auto_tadpole_ct import assign_FJTadpole_field_shift

Coupling = model.object_library.Coupling
param = model.parameters
P = model.particles
L = model.lorentz

parameter_orders = {param.aEW: {'QED': 2},
                    param.aS: {'QCD': 2}}

modelname = 'TGC'
modelgauge = "'t Hooft Feynman"

features = {'fermionloop_opt' : True,
            'qcd_rescaling' : True,
           }

quarks = [P.u, P.d, P.c, P.s, P.b]
for p in quarks:
  set_light_particle(p)

doublets = [(P.nu_e, P.e__minus__), (P.nu_mu, P.mu__minus__),
            (P.nu_tau, P.tau__minus__), (P.u, P.d), (P.c, P.s), (P.t, P.b)]

generations = [(P.e__minus__, P.mu__minus__, P.tau__minus__),
               (P.d, P.s, P.b)]

assign_counterterm(param.G, 'dZgs', 'G*dZgs')
mixings = {}
selection = [P.g, P.t] + quarks
auto_assign_ct_particle(mixings, selection=selection)
