#==============================================================================#
#                                  model init                                  #
#==============================================================================#

from __future__ import print_function

#============#
#  Includes  #
#============#

import os
import sys

from rept1l.autoct.modelfile import model, get_modelpath, CTParameter
from rept1l.autoct import modelfile
from rept1l.helper_lib import export_particle_name
from . import user_settings

#===========#
#  globals  #
#===========#

object_library = model.object_library
model_objects = model.object_library
Particle = object_library.Particle
P = model.particles
V = model.vertices
param = model.parameters

if hasattr(user_settings, 'modelname'):
  modelname = user_settings.modelname

if hasattr(user_settings, 'modelgauge'):
  modelgauge = user_settings.modelgauge

if hasattr(user_settings, 'parameter_orders'):
  parameter_orders = user_settings.parameter_orders
else:
  print('No parameter orders passed. ' +
        'Note that parameters are cached with zero order which ' +
        'leads to a wrong ct expansion.')
  parameter_orders = {}

# load parameters which can safely be set to zero after renormalization
if hasattr(user_settings, 'tadpole_parameter'):
  from sympy import Symbol
  zero_tadpole = {Symbol(u.name): 0
                  for u in user_settings.tadpole_parameter}
else:
  zero_tadpole = {}

# the doublet attribute builds the doublet partner dicitonary, which recola
# requires for certain optimizations
if hasattr(user_settings, 'doublets'):
  doublet_partners = {}
  for p1, p2 in user_settings.doublets:
    p1_name = 'P_' + export_particle_name(p1.name)
    p2_name = 'P_' + export_particle_name(p2.name)
    doublet_partners[p1_name] = p2_name

# the generations attribute is a list of fermion generations which is used to
# optimize fermion loops.
if hasattr(user_settings, 'generations'):
  flavours = []
  for gen in user_settings.generations:
    flavours.append(['P_' + export_particle_name(p.name) for p in gen])

# load flavours & doublet partners which can be used to optimize fermion
# helicities, deprecated -> use doublets & generations instead
if hasattr(user_settings, 'doublet_partners'):
  doublet_partners = user_settings.doublet_partners
if hasattr(user_settings, 'flavours'):
  flavours = user_settings.flavours

# load counterterm files
def get_counterterms():
  directory = os.path.dirname(os.path.realpath(__file__))
  files = ('parameters_ct.py', 'vertices_ct.py')
  files_found = {u: u in os.listdir(directory) for u in files}

  if all(files_found.values()):

    return directory, files
  else:
    not_found = [u for u in files_found if files_found[u] is False]
    msg = 'Files not found: ' + ', '.join(not_found) + '.'
    raise Exception(msg)

# load masses which are potentially treated as 0
if hasattr(user_settings, 'light_masses'):
  light_masses = set(user_settings.light_masses)
else:
  light_masses = set()

for p in model_objects.all_particles:
  if hasattr(p, 'light_particle') and p.light_particle is True:
    if p.mass.name not in light_masses:
      light_masses.add(p.mass.name)

def import_counterterms():
  path, modules = get_counterterms()
  ret = []
  for mod in modules:
    mod_name = mod.split('.')[0]
    # need to be called with `Model.` -> otherwise isinstance/type(...) wont
    # work outside of Model

    __import__('model.' + mod_name)

# import_counterterms()

__author__ = "Jean-Nicolas lang"
__date__ = "18. 5. 2015"
__version__ = "0.9"
