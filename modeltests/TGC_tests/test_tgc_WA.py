#==============================================================================#
#                                test_tgc_WA.py                                #
#==============================================================================#

from __future__ import print_function

#############
#  Imports  #
#############

import sys
import pyrecola

import rept1l.modeltests.modeltest_utils.setup_models as sm
import rept1l.modeltests.TGC_tests.tgc_setup as tgcs
from rept1l.modeltests.modeltest_utils.model_tests import test_amplitude_squared
from rept1l.modeltests.modeltest_utils.model_tests import fail
from rept1l.modeltests.modeltest_utils.model_tests import success
from rept1l.modeltests.modeltest_utils.model_tests import warning

#############
#  Methods  #
#############

def set_parameter():
  params = tgcs.params_tgc_WA.copy()
  scales = {'muUV': pyrecola.set_mu_uv_rcl,
            'muIR': pyrecola.set_mu_ir_rcl,
            'muMS': pyrecola.set_mu_ms_rcl}
  deltas = {'DeltaUV': pyrecola.set_delta_uv_rcl,
            'DeltaIR': pyrecola.set_delta_ir_rcl  # <- also checks for DeltaIR2
            }
  for scale, clb in scales.iteritems():
    if scale in params:
      clb(params[scale])
      del params[scale]

  for delta, clb in deltas.iteritems():
    if delta in params:
      if delta == 'DeltaIR':
        clb(params['DeltaIR'], params['DeltaIR2'])
      else:
        clb(params[delta])
      del params[delta]

  for p, val in params.iteritems():
    pyrecola.set_parameter_rcl(p, complex(val, 0.))

def test_tgc_WA(only_D4=False):
  """ Testing loop-induced processes versus RECOLA 1.0 (which were tested
  against and MadGraph).

  Original library versions:

  FOR REPT1L
  commit 3665f12d5f596f8eb83862e5085a8198b8d5e183
  Date:   Thu Nov 10 15:06:15 2016 +0100

  FOR RECOLA 2
  commit eb32b70d19b45b6b3fb1db92de3555919b474c9f
  Date:   Fri Nov 11 12:29:19 2016 +0100
  """

  set_parameter()
  pyrecola.set_dynamic_settings_rcl(2)

  # 1 and 4 were computed with finite widths for Z, W. 2, 3 for zero width..
  pr = 'd~ u -> nu_e e+ A'
  pyrecola.define_process_rcl(1, pr, 'NLO')

  pyrecola.unselect_all_powers_BornAmpl_rcl(1)
  pyrecola.select_power_BornAmpl_rcl(1, 'QCD', 0)
  pyrecola.select_power_BornAmpl_rcl(1, 'QED', 3)
  pyrecola.select_power_BornAmpl_rcl(1, 'LAM', 0)
  pyrecola.select_power_BornAmpl_rcl(1, 'LAM', 1)

  pyrecola.unselect_all_powers_LoopAmpl_rcl(1)
  pyrecola.select_power_LoopAmpl_rcl(1, 'QCD', 2)
  pyrecola.select_power_LoopAmpl_rcl(1, 'QED', 3)
  pyrecola.select_power_LoopAmpl_rcl(1, 'LAM', 0)
  pyrecola.select_power_LoopAmpl_rcl(1, 'LAM', 1)

  pyrecola.generate_processes_rcl()

  total_tests = 0
  tests_succeeded = 0

  p1 = [4000.0000000000000, 0.0000000000000000, 0.0000000000000000,
        4000.0000000000000]
  p2 = [4000.0000000000000, 0.0000000000000000, 0.0000000000000000,
        -4000.0000000000000]
  p3 = [2695.2983477500002, 1902.7539311690000, 1290.6093405390000,
        -1406.5874274000000]
  p4 = [2488.3645786010002, -2308.2990615560002, 306.55329881799997,
        -877.34758986199995]
  p5 = [2816.3370736490001, 405.54513038800002, -1597.1626393560000,
        2283.9350172620002]
  mom = [p1, p2, p3, p4, p5]

  power_born1 = [0, 6, 0]
  power_born2 = [0, 6, 1]
  power_born3 = [0, 6, 2]
  power_nlo1 = [2, 6, 0]
  power_nlo2 = [2, 6, 1]
  power_nlo3 = [2, 6, 2]

  # Rec 0.19153526405586E+07  -0.23620656512159E+07
  # MC  1915352.6405633930    -2362065.6512218644
  pyrecola.set_parameter_rcl('CBL2', complex(0., 0.))
  pyrecola.set_parameter_rcl('CWL2', complex(0., 0.))
  pyrecola.set_parameter_rcl('CWWWL2', complex(10., 0.))
  pyrecola.set_parameter_rcl('CphidL2', complex(1., 0.))
  pyrecola.reset_couplings_rcl()

  if only_D4:
    expec = [{'prid': pr, 'Momenta': mom,
              'LO': [{'value': 0.86025911473770E-10, 'power': power_born1},
                     {'value': 0.14432442689450E-01, 'power': power_born2},
                     {'value': 0.19153526261261E+07, 'power': power_born3}]},
             {'prid': pr, 'Momenta': mom,
              'NLO-D4': [{'value': -0.10510146561E-09, 'power': power_nlo1},
                         {'value': -0.19018450136E-01, 'power': power_nlo2},
                         {'value': -0.22764636616E+07, 'power': power_nlo3}]}]
  else:
    expec = [{'prid': pr, 'Momenta': mom,
              'LO': [{'value': 0.86025911473770E-10, 'power': power_born1},
                     {'value': 0.14432442689450E-01, 'power': power_born2},
                     {'value': 0.19153526261261E+07, 'power': power_born3}]},
             {'prid': pr, 'Momenta': mom,
              'NLO': [{'value': -0.10937147414167E-09, 'power': power_nlo1},
                      {'value': -0.19407006675078E-01, 'power': power_nlo2},
                      {'value': -0.23620656318089E+07, 'power': power_nlo3}]},
             {'prid': pr, 'Momenta': mom,
              'NLO-D4': [{'value': -0.10510146561E-09, 'power': power_nlo1},
                         {'value': -0.19018450136E-01, 'power': power_nlo2},
                         {'value': -0.22764636616E+07, 'power': power_nlo3}]},
             {'prid': pr, 'Momenta': mom,
              'NLO-R2': [{'value': -0.42700085364E-11, 'power': power_nlo1},
                         {'value': -0.38855653955E-03, 'power': power_nlo2},
                         {'value': -0.85601970253E+05, 'power': power_nlo3}]}]

  res = test_amplitude_squared(1, expec, err_digits=4, warning_digits=5)
  total_tests += len(res)
  tests_succeeded += res.values().count(True)

  #Rec 0.14939943068118E-01,    -0.18424503378196E-01
  #MC  0.14939943068158384E-001 -0.18424503378246786E-001
  pyrecola.set_parameter_rcl('CBL2', complex(10., 0.))
  pyrecola.set_parameter_rcl('CWL2', complex(0., 0.))
  pyrecola.set_parameter_rcl('CWWWL2', complex(0., 0.))
  pyrecola.set_parameter_rcl('CphidL2', complex(0., 0.))
  pyrecola.reset_couplings_rcl()
  if only_D4:
    expec = [{'prid': pr, 'Momenta': mom,
              'LO': [{'value': 0.86025911473770E-10, 'power': power_born1},
                     {'value': 0.88727632014545E-06, 'power': power_born2},
                     {'value': 0.14939055705771E-01, 'power': power_born3}]},
             {'prid': pr, 'Momenta': mom,
              'NLO-D4': [{'value': -0.10510146561E-09, 'power': power_nlo1},
                         {'value': -0.12411395420E-05, 'power': power_nlo2},
                         {'value': -0.17755590792E-01, 'power': power_nlo3}]}]
  else:
    expec = [{'prid': pr, 'Momenta': mom,
              'LO': [{'value': 0.86025911473770E-10, 'power': power_born1},
                     {'value': 0.88727632014545E-06, 'power': power_born2},
                     {'value': 0.14939055705771E-01, 'power': power_born3}]},
             {'prid': pr, 'Momenta': mom,
              'NLO': [{'value': -0.10937147414167E-09, 'power': power_nlo1},
                      {'value': -0.12481601032847E-05, 'power': power_nlo2},
                      {'value': -0.18423255108722E-01, 'power': power_nlo3}]},
             {'prid': pr, 'Momenta': mom,
              'NLO-D4': [{'value': -0.10510146561E-09, 'power': power_nlo1},
                         {'value': -0.12411395420E-05, 'power': power_nlo2},
                         {'value': -0.17755590792E-01, 'power': power_nlo3}]},
             {'prid': pr, 'Momenta': mom,
              'NLO-R2': [{'value': -0.42700085364E-11, 'power': power_nlo1},
                         {'value': -0.70205612436E-08, 'power': power_nlo2},
                         {'value': -0.66766431658E-03, 'power': power_nlo3}]}]

  res = test_amplitude_squared(1, expec, err_digits=4, warning_digits=5)
  total_tests += len(res)
  tests_succeeded += res.values().count(True)

  pyrecola.set_parameter_rcl('CBL2', complex(7.7, 0.))
  pyrecola.set_parameter_rcl('CWL2', complex(2.3, 0.))
  pyrecola.set_parameter_rcl('CWWWL2', complex(0., 0.))
  pyrecola.set_parameter_rcl('CphidL2', complex(0., 0.))
  pyrecola.reset_couplings_rcl()

  if only_D4:
    expec = [{'prid': pr, 'Momenta': mom,
              'LO': [{'value': 0.86025911473770E-10, 'power': power_born1},
                     {'value': 0.88727632014545E-06, 'power': power_born2},
                     {'value': 0.14939055705771E-01, 'power': power_born3}]},
             {'prid': pr, 'Momenta': mom,
              'NLO-D4': [{'value': -0.10510146561E-09, 'power': power_nlo1},
                         {'value': -0.12411395420E-05, 'power': power_nlo2},
                         {'value': -0.17755590792E-01, 'power': power_nlo3}]}]
  else:
    expec = [{'prid': pr, 'Momenta': mom,
              'LO': [{'value': 0.86025911473770E-10, 'power': power_born1},
                     {'value': 0.88727632014545E-06, 'power': power_born2},
                     {'value': 0.14939055705771E-01, 'power': power_born3}]},
             {'prid': pr, 'Momenta': mom,
              'NLO': [{'value': -0.10937147414167E-09, 'power': power_nlo1},
                      {'value': -0.12481601032847E-05, 'power': power_nlo2},
                      {'value': -0.18423255108722E-01, 'power': power_nlo3}]},
             {'prid': pr, 'Momenta': mom,
              'NLO-D4': [{'value': -0.10510146561E-09, 'power': power_nlo1},
                         {'value': -0.12411395420E-05, 'power': power_nlo2},
                         {'value': -0.17755590792E-01, 'power': power_nlo3}]},
             {'prid': pr, 'Momenta': mom,
              'NLO-R2': [{'value': -0.42700085364E-11, 'power': power_nlo1},
                         {'value': -0.70205612436E-08, 'power': power_nlo2},
                         {'value': -0.66766431658E-03, 'power': power_nlo3}]}]

  res = test_amplitude_squared(1, expec, err_digits=4, warning_digits=5)
  total_tests += len(res)
  tests_succeeded += res.values().count(True)

  if total_tests == tests_succeeded:
    print('All tests(' + str(total_tests) + ') succeeded ' + success())
  elif tests_succeeded == 0:
    print('All tests(' + str(total_tests) + ') failed ' + fail())
  else:
    print('Some tests(' + str(total_tests) + ') failed ' + warning())

  pyrecola.reset_recola_rcl()

if __name__ == "__main__":
  test_tgc_WA(only_D4=True)
