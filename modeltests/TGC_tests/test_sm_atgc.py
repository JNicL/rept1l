# -*- coding: utf-8 -*-
################################################################################
#                                 test_sm_atgc                                 #
################################################################################

import rept1l.modeltests.TGC_tests.tgc_setup as smatgc
from rept1l.modeltests.modeltest_utils.auto_model import GenModel

class GenSMATGC(GenModel):
  threads = 8
  ct_expansion = True
  pre_ct = 'smatgcexp.txt'
  verbose = True

  def renormalize_model(self, **kwargs):

    # QCD charged fields
    self.renormalize_qcd('-o', 'particles', '-qcd', '0', '2', '-qed', '0',
                         '-lam', '0', **kwargs)

    # QCD coupling
    for i in ['6', '5', '4', '3']:
      self.renormalize_qcd('-o', 'alphaS', '-nf', i, '-qcd', '0', '2', '3',
                           '-qed', '0', '-lam', '0', **kwargs)

    self.register_ct()

  def compute_rational(self, **kwargs):
    cls = self.__class__
    self.run_r2(vertices_file='smatgcr2.txt', nc=True, threads=cls.threads,
                orders={'qcd': [0, 1, 2, 3, 4], 'qed': [0, 1, 2], 'lam': [0]})


def full_smatgc_model(outputpath):
  GenSMATGC.driverpath = smatgc.driver_sm_atgc_path
  GenSMATGC.outputpath = outputpath
  GenSMATGC(odp=True)

if __name__ == '__main__':
  outputpath = './SM_ATGC_2.2.0'
  full_smatgc_model(outputpath)
