###############################################################################
#                                test_ntgc.py                                 #
###############################################################################

from __future__ import print_function

#############
#  Imports  #
#############

import sys
import pyrecola

import rept1l.modeltests.modeltest_utils.setup_models as sm
import rept1l.modeltests.TGC_tests.tgc_setup as tgcs
from rept1l.modeltests.modeltest_utils.model_tests import test_amplitude_squared
from rept1l.modeltests.modeltest_utils.model_tests import fail
from rept1l.modeltests.modeltest_utils.model_tests import success
from rept1l.modeltests.modeltest_utils.model_tests import warning

#############
#  Methods  #
#############

def set_parameter():
  params = tgcs.params_ntgc.copy()
  scales = {'muUV': pyrecola.set_mu_uv_rcl,
            'muIR': pyrecola.set_mu_ir_rcl,
            'muMS': pyrecola.set_mu_ms_rcl}
  deltas = {'DeltaUV': pyrecola.set_delta_uv_rcl,
            'DeltaIR': pyrecola.set_delta_ir_rcl  # <- also checks for DeltaIR2
            }
  for scale, clb in scales.iteritems():
    if scale in params:
      clb(params[scale])
      del params[scale]

  for delta, clb in deltas.iteritems():
    if delta in params:
      if delta == 'DeltaIR':
        clb(params['DeltaIR'], params['DeltaIR2'])
        del params['DeltaIR2']
      else:
        clb(params[delta])
      del params[delta]

  for p, val in params.iteritems():
    pyrecola.set_parameter_rcl(p, complex(val, 0.))


def test_ntgc(only_D4=False):
  set_parameter()

  prs_nlo = {'u u~ -> Z Z': 1, 'u u~ -> A Z': 2}
  prs_lo = {'nu_e nu_e~ -> Z Z': 3, 'nu_e nu_e~ -> A Z': 4}
  for pr in prs_nlo:
    pid = prs_nlo[pr]
    pyrecola.define_process_rcl(pid, pr, 'NLO')

    pyrecola.unselect_all_powers_BornAmpl_rcl(pid)
    pyrecola.select_power_BornAmpl_rcl(pid, 'QCD', 0)
    pyrecola.select_power_BornAmpl_rcl(pid, 'QED', 2)
    pyrecola.select_power_BornAmpl_rcl(pid, 'LAM', 0)
    pyrecola.select_power_BornAmpl_rcl(pid, 'LAM', 2)

    pyrecola.unselect_all_powers_LoopAmpl_rcl(pid)
    pyrecola.select_power_LoopAmpl_rcl(pid, 'QCD', 2)
    pyrecola.select_power_LoopAmpl_rcl(pid, 'QED', 2)
    pyrecola.select_power_LoopAmpl_rcl(pid, 'LAM', 0)
    pyrecola.select_power_LoopAmpl_rcl(pid, 'LAM', 2)

  for pr in prs_lo:
    pid = prs_lo[pr]
    pyrecola.define_process_rcl(pid, pr, 'LO')

    pyrecola.unselect_all_powers_BornAmpl_rcl(pid)
    pyrecola.select_power_BornAmpl_rcl(pid, 'QCD', 0)
    pyrecola.select_power_BornAmpl_rcl(pid, 'QED', 2)
    pyrecola.select_power_BornAmpl_rcl(pid, 'LAM', 0)
    pyrecola.select_power_BornAmpl_rcl(pid, 'LAM', 2)

  pyrecola.generate_processes_rcl()

  total_tests = 0
  tests_succeeded = 0

  power_born1 = [0, 4, 0]
  power_born2 = [0, 4, 2]
  power_born3 = [0, 4, 4]
  power_nlo1 = [2, 4, 0]
  power_nlo2 = [2, 4, 2]
  power_nlo3 = [2, 4, 4]

  pr = 'u u~ -> Z Z'
  p1 = [250.000000000,    0.000000000,    0.000000000,  250.000000000]
  p2 = [250.000000000,    0.000000000,    0.000000000, -250.000000000]
  p3 = [250.000000000, -176.536454995, -148.340475397,  -31.953503528]
  p4 = [250.000000000,  176.536454995,  148.340475397,   31.953503528]

  mom = [p1, p2, p3, p4]

  if only_D4:
    expec = [{'prid': pr, 'Momenta': mom,
              'LO': [{'value': 0.21228405517487E-02, 'power': power_born1},
                     {'value': 0.32681717911862E+04, 'power': power_born2},
                     {'value': 0.41854483534026E+11, 'power': power_born3}]},
             {'prid': pr, 'Momenta': mom,
              'NLO-D4': [{'value':  0.11133678129E-03, 'power': power_nlo1},
                         {'value': -0.43316666269E+02, 'power': power_nlo2},
                         {'value':  0.13764185238E+10, 'power': power_nlo3}]}]
  else:
    expec = [{'prid': pr, 'Momenta': mom,
              'LO': [{'value': 0.21228405517487E-02, 'power': power_born1},
                     {'value': 0.32681717911862E+04, 'power': power_born2},
                     {'value': 0.41854483534026E+11, 'power': power_born3}]},
             {'prid': pr, 'Momenta': mom,
              'NLO': [{'value': -0.30975917129363E-04, 'power': power_nlo1},
                      {'value': -0.22589528171172E+03, 'power': power_nlo2},
                      {'value': -0.49416457694877E+09, 'power': power_nlo3}]},
             {'prid': pr, 'Momenta': mom,
              'NLO-D4': [{'value':  0.11133678129E-03, 'power': power_nlo1},
                         {'value': -0.43316666269E+02, 'power': power_nlo2},
                         {'value':  0.13764185238E+10, 'power': power_nlo3}]},
             {'prid': pr, 'Momenta': mom,
              'NLO-R2': [{'value': -0.14231269842E-03, 'power': power_nlo1},
                         {'value': -0.18257861544E+03, 'power': power_nlo2},
                         {'value': -0.18705831008E+10, 'power': power_nlo3}]}]

  res = test_amplitude_squared(prs_nlo[pr], expec, err_digits=8,
                               warning_digits=9)
  total_tests += len(res)
  tests_succeeded += res.values().count(True)

  pr = 'u u~ -> A Z'

  p1 = [250.000000000,    0.000000000,    0.000000000,  250.000000000]
  p2 = [250.000000000,    0.000000000,    0.000000000, -250.000000000]
  p3 = [241.691042971, -183.286805658, -154.012676226,  -33.175332491]
  p4 = [258.308957029,  183.286805658,  154.012676226,   33.175332491]

  mom = [p1, p2, p3, p4]

  if only_D4:
    expec = [{'prid': pr, 'Momenta': mom,
              'LO': [{'value':  0.26559787802630E-02, 'power': power_born1},
                     {'value': -0.36459937743694E+04, 'power': power_born2},
                     {'value':  0.22327837451824E+12, 'power': power_born3}]},
             {'prid': pr, 'Momenta': mom,
              'NLO-D4': [{'value': 0.13283676419E-03, 'power': power_nlo1},
                         {'value': 0.87128045236E+02, 'power': power_nlo2},
                         {'value': 0.73426898316E+10, 'power': power_nlo3}]}]
  else:
    expec = [{'prid': pr, 'Momenta': mom,
              'LO': [{'value':  0.26559787802630E-02, 'power': power_born1},
                     {'value': -0.36459937743694E+04, 'power': power_born2},
                     {'value':  0.22327837451824E+12, 'power': power_born3}]},
             {'prid': pr, 'Momenta': mom,
              'NLO': [{'value': -0.45216885124510E-04, 'power': power_nlo1},
                      {'value':  0.29081393991062E+03, 'power': power_nlo2},
                      {'value': -0.26361874324867E+10, 'power': power_nlo3}]},
             {'prid': pr, 'Momenta': mom,
              'NLO-D4': [{'value': 0.13283676419E-03, 'power': power_nlo1},
                         {'value': 0.87128045236E+02, 'power': power_nlo2},
                         {'value': 0.73426898316E+10, 'power': power_nlo3}]},
             {'prid': pr, 'Momenta': mom,
              'NLO-R2': [{'value': -0.17805364932E-03, 'power': power_nlo1},
                         {'value':  0.20368589467E+03, 'power': power_nlo2},
                         {'value': -0.99788772641E+10, 'power': power_nlo3}]}]

  p1 = [250.000000000,    0.000000000,    0.000000000,  250.000000000]
  p2 = [250.000000000,    0.000000000,    0.000000000, -250.000000000]
  p3 = [241.691042971,  211.053624140,  -28.605790837,  114.248136616]
  p4 = [258.308957029, -211.053624140,   28.605790837, -114.248136616]

  mom = [p1, p2, p3, p4]

  if only_D4:
    expet = [{'prid': pr, 'Momenta': mom,
             'LO': [{'value':  0.39471393989506E-02, 'power': power_born1},
                    {'value': -0.36459937743694E+04, 'power': power_born2},
                    {'value':  0.26261900944787E+12, 'power': power_born3}]},
             {'prid': pr, 'Momenta': mom,
             'NLO-D4': [{'value': 0.18414720511E-03, 'power': power_nlo1},
                        {'value': 0.35356867988E+03, 'power': power_nlo2},
                        {'value': 0.86364384120E+10, 'power': power_nlo3}]}]
    expec.extend(expet)
  else:
    expet = [{'prid': pr, 'Momenta': mom,
              'LO': [{'value':  0.39471393989506E-02, 'power': power_born1},
                     {'value': -0.36459937743694E+04, 'power': power_born2},
                     {'value':  0.26261900944787E+12, 'power': power_born3}]},
             {'prid': pr, 'Momenta': mom,
              'NLO': [{'value': -0.80464311953160E-04, 'power': power_nlo1},
                      {'value':  0.55725457454944E+03, 'power': power_nlo2},
                      {'value': -0.31006716782686E+10, 'power': power_nlo3}]},
             {'prid': pr, 'Momenta': mom,
              'NLO-R2': [{'value': -0.26461151707E-03, 'power': power_nlo1},
                         {'value':  0.20368589467E+03, 'power': power_nlo2},
                         {'value': -0.11737110090E+11, 'power': power_nlo3}]},
             {'prid': pr, 'Momenta': mom,
              'NLO-D4': [{'value': 0.18414720511E-03, 'power': power_nlo1},
                         {'value': 0.35356867988E+03, 'power': power_nlo2},
                         {'value': 0.86364384120E+10, 'power': power_nlo3}]}]
    expec.extend(expet)

  p1 = [250.000000000,    0.000000000,    0.000000000,  250.000000000]
  p2 = [250.000000000,    0.000000000,    0.000000000, -250.000000000]
  p3 = [241.691042971,  -51.301788556,  126.309602817, -199.570967276]
  p4 = [258.308957029,   51.301788556, -126.309602817,  199.570967276]

  mom = [p1, p2, p3, p4]

  if only_D4:
    expet = [{'prid': pr, 'Momenta': mom,
             'LO': [{'value':  0.12866994486202E-01, 'power': power_born1},
                    {'value': -0.36459937743694E+04, 'power': power_born2},
                    {'value':  0.35075315059532E+12, 'power': power_born3}]},
             {'prid': pr, 'Momenta': mom,
             'NLO-D4': [{'value': 0.93199662343E-03, 'power': power_nlo1},
                        {'value': -0.15873028458E+03, 'power': power_nlo2},
                        {'value': 0.11534800886E+11, 'power': power_nlo3}]}]
    expec.extend(expet)
  else:
    expet = [{'prid': pr, 'Momenta': mom,
              'LO': [{'value':  0.12866994486202E-01, 'power': power_born1},
                     {'value': -0.36459937743694E+04, 'power': power_born2},
                     {'value':  0.35075315059532E+12, 'power': power_born3}]},
             {'prid': pr, 'Momenta': mom,
              'NLO': [{'value':  0.69408661118412E-04, 'power': power_nlo1},
                      {'value':  0.44955610098571E+02, 'power': power_nlo2},
                      {'value': -0.41412476667280E+10, 'power': power_nlo3}]},
             {'prid': pr, 'Momenta': mom,
              'NLO-R2': [{'value': -0.86258796231E-03, 'power': power_nlo1},
                         {'value':  0.20368589467E+03, 'power': power_nlo2},
                         {'value': -0.15676048553E+11, 'power': power_nlo3}]},
             {'prid': pr, 'Momenta': mom,
              'NLO-D4': [{'value': 0.93199662343E-03, 'power': power_nlo1},
                         {'value': -0.15873028458E+03, 'power': power_nlo2},
                         {'value': 0.11534800886E+11, 'power': power_nlo3}]}]
    expec.extend(expet)

  p1 = [250.000000000,    0.000000000,    0.000000000,  250.000000000]
  p2 = [250.000000000,    0.000000000,    0.000000000, -250.000000000]
  p3 = [241.691042971,   56.491883417,  -80.797714816, -220.669337791]
  p4 = [258.308957029,  -56.491883417,   80.797714816,  220.669337791]

  mom = [p1, p2, p3, p4]

  if only_D4:
    expet = [{'prid': pr, 'Momenta': mom,
             'LO': [{'value':  0.26651865035444E-01, 'power': power_born1},
                    {'value': -0.36459937743694E+04, 'power': power_born2},
                    {'value':  0.37993724097226E+12, 'power': power_born3}]},
             {'prid': pr, 'Momenta': mom,
             'NLO-D4': [{'value':  0.32160159265E-02, 'power': power_nlo1},
                        {'value': -0.81521217203E+03, 'power': power_nlo2},
                        {'value':  0.12494543289E+11, 'power': power_nlo3}]}]
    expec.extend(expet)
  else:
    expet = [{'prid': pr, 'Momenta': mom,
              'LO': [{'value':  0.26651865035444E-01, 'power': power_born1},
                     {'value': -0.36459937743694E+04, 'power': power_born2},
                     {'value':  0.37993724097226E+12, 'power': power_born3}]},
             {'prid': pr, 'Momenta': mom,
              'NLO': [{'value':  0.14293066855183E-02, 'power': power_nlo1},
                      {'value': -0.61152627735896E+03, 'power': power_nlo2},
                      {'value': -0.44858163355309E+10, 'power': power_nlo3}]},
             {'prid': pr, 'Momenta': mom,
              'NLO-R2': [{'value': -0.17867092410E-02, 'power': power_nlo1},
                         {'value':  0.20368589467E+03, 'power': power_nlo2},
                         {'value': -0.16980359625E+11, 'power': power_nlo3}]},
             {'prid': pr, 'Momenta': mom,
              'NLO-D4': [{'value':  0.32160159265E-02, 'power': power_nlo1},
                         {'value': -0.81521217203E+03, 'power': power_nlo2},
                         {'value':  0.12494543289E+11, 'power': power_nlo3}]}]
    expec.extend(expet)

  p1 = [250.000000000,    0.000000000,    0.000000000,  250.000000000]
  p2 = [250.000000000,    0.000000000,    0.000000000, -250.000000000]
  p3 = [241.691042971, -112.609821730, -100.951956488,  188.526631497]
  p4 = [258.308957029,  112.609821730,  100.951956488, -188.526631497]

  mom = [p1, p2, p3, p4]

  if only_D4:
    expet = [{'prid': pr, 'Momenta': mom,
             'LO': [{'value':  0.10035107442179E-01, 'power': power_born1},
                    {'value': -0.36459937743693E+04, 'power': power_born2},
                    {'value':  0.33664467467290E+12, 'power': power_born3}]},
             {'prid': pr, 'Momenta': mom,
             'NLO-D4': [{'value': 0.62317618335E-03, 'power': power_nlo1},
                        {'value': 0.10234335320E+04, 'power': power_nlo2},
                        {'value': 0.11070832251E+11, 'power': power_nlo3}]}]
    expec.extend(expet)
  else:
    expet = [{'prid': pr, 'Momenta': mom,
              'LO': [{'value':  0.10035107442179E-01, 'power': power_born1},
                     {'value': -0.36459937743693E+04, 'power': power_born2},
                     {'value':  0.33664467467290E+12, 'power': power_born3}]},
             {'prid': pr, 'Momenta': mom,
              'NLO': [{'value': -0.49565449465089E-04, 'power': power_nlo1},
                      {'value':  0.12271194266497E+04, 'power': power_nlo2},
                      {'value': -0.39746727039780E+10, 'power': power_nlo3}]},
             {'prid': pr, 'Momenta': mom,
              'NLO-R2': [{'value': -0.67274163282E-03, 'power': power_nlo1},
                         {'value':  0.20368589467E+03, 'power': power_nlo2},
                         {'value': -0.15045504955E+11, 'power': power_nlo3}]},
             {'prid': pr, 'Momenta': mom,
              'NLO-D4': [{'value': 0.62317618335E-03, 'power': power_nlo1},
                         {'value': 0.10234335320E+04, 'power': power_nlo2},
                         {'value': 0.11070832251E+11, 'power': power_nlo3}]}]
    expec.extend(expet)

  p1 = [250.000000000,    0.000000000,    0.000000000,  250.000000000]
  p2 = [250.000000000,    0.000000000,    0.000000000, -250.000000000]
  p3 = [241.691042971, -183.286805658, -154.012676226,  -33.175332491]
  p4 = [258.308957029,  183.286805658,  154.012676226,   33.175332491]

  mom = [p1, p2, p3, p4]

  if only_D4:
    expet = [{'prid': pr, 'Momenta': mom,
             'LO': [{'value':  0.26559787802630E-02, 'power': power_born1},
                    {'value': -0.36459937743694E+04, 'power': power_born2},
                    {'value':  0.22327837451824E+12, 'power': power_born3}]},
             {'prid': pr, 'Momenta': mom,
             'NLO-D4': [{'value': 0.13283676419E-03, 'power': power_nlo1},
                        {'value': 0.87128045236E+02, 'power': power_nlo2},
                        {'value': 0.73426898316E+10, 'power': power_nlo3}]}]
    expec.extend(expet)
  else:
    expet = [{'prid': pr, 'Momenta': mom,
              'LO': [{'value':  0.26559787802630E-02, 'power': power_born1},
                     {'value': -0.36459937743694E+04, 'power': power_born2},
                     {'value':  0.22327837451824E+12, 'power': power_born3}]},
             {'prid': pr, 'Momenta': mom,
              'NLO': [{'value': -0.45216885124510E-04, 'power': power_nlo1},
                      {'value':  0.29081393991062E+03, 'power': power_nlo2},
                      {'value': -0.26361874324867E+10, 'power': power_nlo3}]},
             {'prid': pr, 'Momenta': mom,
              'NLO-R2': [{'value': -0.17805364932E-03, 'power': power_nlo1},
                         {'value':  0.20368589467E+03, 'power': power_nlo2},
                         {'value': -0.99788772641E+10, 'power': power_nlo3}]},
             {'prid': pr, 'Momenta': mom,
              'NLO-D4': [{'value': 0.13283676419E-03, 'power': power_nlo1},
                         {'value': 0.87128045236E+02, 'power': power_nlo2},
                         {'value': 0.73426898316E+10, 'power': power_nlo3}]}]
    expec.extend(expet)

  res = test_amplitude_squared(prs_nlo[pr], expec, err_digits=8,
                               warning_digits=9)
  total_tests += len(res)
  tests_succeeded += res.values().count(True)

  pr = 'nu_e nu_e~ -> Z Z'

  p1 = [250.000000000,    0.000000000,    0.000000000,  250.000000000]
  p2 = [250.000000000,    0.000000000,    0.000000000, -250.000000000]
  p3 = [250.000000000,  203.280637064,  -27.552255540,  110.040441567]
  p4 = [250.000000000, -203.280637064,   27.552255540, -110.040441567]

  mom = [p1, p2, p3, p4]

  expec = [{'prid': pr, 'Momenta': mom,
            'LO': [{'value': 0.36700966534539E-01, 'power': power_born1},
                   {'value': 0.40900075578914E+11, 'power': power_born3}]}]

  p1 = [250.000000000,    0.000000000,    0.000000000,  250.000000000]
  p2 = [250.000000000,    0.000000000,    0.000000000, -250.000000000]
  p3 = [250.000000000,  -49.412372342,  121.657690706, -192.220880038]
  p4 = [250.000000000,   49.412372342, -121.657690706,  192.220880038]

  mom = [p1, p2, p3, p4]

  expec.append({'prid': pr, 'Momenta': mom,
                'LO': [{'value': 0.11429607373197E+00, 'power': power_born1},
                       {'value': 0.56223717491384E+11, 'power': power_born3}]})

  p1 = [250.000000000,    0.000000000,    0.000000000,  250.000000000]
  p2 = [250.000000000,    0.000000000,    0.000000000, -250.000000000]
  p3 = [250.000000000,   54.411318909,  -77.821980115, -212.542209353]
  p4 = [250.000000000,  -54.411318909,   77.821980115,  212.542209353]

  mom = [p1, p2, p3, p4]

  expec.append({'prid': pr, 'Momenta': mom,
                'LO': [{'value': 0.22901444674351E+00, 'power': power_born1},
                       {'value': 0.61297875413104E+11, 'power': power_born3}]})

  p1 = [250.000000000,    0.000000000,    0.000000000,  250.000000000]
  p2 = [250.000000000,    0.000000000,    0.000000000, -250.000000000]
  p3 = [250.000000000, -108.462464903,  -97.233952326,  181.583300976]
  p4 = [250.000000000,  108.462464903,   97.233952326, -181.583300976]

  mom = [p1, p2, p3, p4]

  expec.append({'prid': pr, 'Momenta': mom,
                'LO': [{'value': 0.89954734857904E-01, 'power': power_born1},
                       {'value': 0.53770715395361E+11, 'power': power_born3}]})

  p1 = [250.000000000,    0.000000000,    0.000000000,  250.000000000]
  p2 = [250.000000000,    0.000000000,    0.000000000, -250.000000000]
  p3 = [250.000000000, -176.536454995, -148.340475397,  -31.953503528]
  p4 = [250.000000000,  176.536454995,  148.340475397,   31.953503528]

  mom = [p1, p2, p3, p4]

  expec.append({'prid': pr, 'Momenta': mom,
                'LO': [{'value': 0.25241843397749E-01, 'power': power_born1},
                       {'value': 0.34060027052631E+11, 'power': power_born3}]})

  res = test_amplitude_squared(prs_lo[pr], expec, err_digits=8,
                               warning_digits=9)

  pr = 'nu_e nu_e~ -> A Z'

  p1 = [250.000000000,    0.000000000,    0.000000000,  250.000000000]
  p2 = [250.000000000,    0.000000000,    0.000000000, -250.000000000]
  p3 = [241.691042971,  211.053624140,  -28.605790837,  114.248136616]
  p4 = [258.308957029, -211.053624140,   28.605790837, -114.248136616]

  mom = [p1, p2, p3, p4]

  expec = [{'prid': pr, 'Momenta': mom,
            'LO': [{'value': 0.29470189309548E+12, 'power': power_born3}]}]

  p1 = [250.000000000,    0.000000000,    0.000000000,  250.000000000]
  p2 = [250.000000000,    0.000000000,    0.000000000, -250.000000000]
  p3 = [241.691042971,  -51.301788556,  126.309602817, -199.570967276]
  p4 = [258.308957029,   51.301788556, -126.309602817,  199.570967276]

  mom = [p1, p2, p3, p4]

  expec.append({'prid': pr, 'Momenta': mom,
                'LO': [{'value': 0.39360295245560E+12, 'power': power_born3}]})

  p1 = [250.000000000,    0.000000000,    0.000000000,  250.000000000]
  p2 = [250.000000000,    0.000000000,    0.000000000, -250.000000000]
  p3 = [241.691042971,   56.491883417,  -80.797714816, -220.669337791]
  p4 = [258.308957029,  -56.491883417,   80.797714816,  220.669337791]

  mom = [p1, p2, p3, p4]
  expec.append({'prid': pr, 'Momenta': mom,
                'LO': [{'value': 0.42635232082933E+12, 'power': power_born3}]})

  p1 = [250.000000000,    0.000000000,    0.000000000,  250.000000000]
  p2 = [250.000000000,    0.000000000,    0.000000000, -250.000000000]
  p3 = [241.691042971, -112.609821730, -100.951956488,  188.526631497]
  p4 = [258.308957029,  112.609821730,  100.951956488, -188.526631497]

  mom = [p1, p2, p3, p4]
  expec.append({'prid': pr, 'Momenta': mom,
                'LO': [{'value': 0.37777091283375E+12, 'power': power_born3}]})

  p1 = [250.000000000,    0.000000000,    0.000000000,  250.000000000]
  p2 = [250.000000000,    0.000000000,    0.000000000, -250.000000000]
  p3 = [241.691042971, -183.286805658, -154.012676226,  -33.175332491]
  p4 = [258.308957029,  183.286805658,  154.012676226,   33.175332491]

  mom = [p1, p2, p3, p4]

  expec.append({'prid': pr, 'Momenta': mom,
                'LO': [{'value': 0.25055520465235E+12, 'power': power_born3}]})

  res = test_amplitude_squared(prs_lo[pr], expec, err_digits=8,
                               warning_digits=9)

  total_tests += len(res)
  tests_succeeded += res.values().count(True)

  if total_tests == tests_succeeded:
    print('All tests(' + str(total_tests) + ') succeeded ' + success())
  elif tests_succeeded == 0:
    print('All tests(' + str(total_tests) + ') failed ' + fail())
  else:
    print('Some tests(' + str(total_tests) + ') failed ' + warning())

  pyrecola.reset_recola_rcl()


if __name__ == "__main__":
  test_ntgc(only_D4=False)
