#==============================================================================#
#                                test_tgc_WA.py                                #
#==============================================================================#

from __future__ import print_function

#############
#  Imports  #
#############

import sys
import pyrecola

import rept1l.modeltests.modeltest_utils.setup_models as sm
import rept1l.modeltests.TGC_tests.tgc_setup as tgcs
from rept1l.modeltests.modeltest_utils.model_tests import test_amplitude_squared
from rept1l.modeltests.modeltest_utils.model_tests import fail
from rept1l.modeltests.modeltest_utils.model_tests import success
from rept1l.modeltests.modeltest_utils.model_tests import warning

#############
#  Methods  #
#############

def set_parameter():
  params = tgcs.params_tgc_WW.copy()
  scales = {'muUV': pyrecola.set_mu_uv_rcl,
            'muIR': pyrecola.set_mu_ir_rcl,
            'muMS': pyrecola.set_mu_ms_rcl}
  deltas = {'DeltaUV': pyrecola.set_delta_uv_rcl,
            'DeltaIR': pyrecola.set_delta_ir_rcl  # <- also checks for DeltaIR2
            }
  for scale, clb in scales.iteritems():
    if scale in params:
      clb(params[scale])
      del params[scale]

  for delta, clb in deltas.iteritems():
    if delta in params:
      if delta == 'DeltaIR':
        clb(params['DeltaIR'], params['DeltaIR2'])
        del params['DeltaIR2']
      else:
        clb(params[delta])
      del params[delta]

  for p, val in params.iteritems():
    pyrecola.set_parameter_rcl(p, complex(val, 0.))

def test_tgc_WW(only_D4=False):
  """

  Original library versions:

  FOR REPT1L
  # TODO: (nick 2016-12-15)

  FOR RECOLA 2
  # TODO: (nick 2016-12-15)
  """

  set_parameter()

  # 1 and 4 were computed with finite widths for Z, W. 2, 3 for zero width..

  pr = 'u u~ -> W+ W-'
  pyrecola.define_process_rcl(1, pr, 'NLO')

  pyrecola.unselect_all_powers_BornAmpl_rcl(1)
  pyrecola.select_power_BornAmpl_rcl(1, 'QCD', 0)
  pyrecola.select_power_BornAmpl_rcl(1, 'QED', 2)
  pyrecola.select_power_BornAmpl_rcl(1, 'LAM', 0)
  pyrecola.select_power_BornAmpl_rcl(1, 'LAM', 1)

  pyrecola.unselect_all_powers_LoopAmpl_rcl(1)
  pyrecola.select_power_LoopAmpl_rcl(1, 'QCD', 2)
  pyrecola.select_power_LoopAmpl_rcl(1, 'QED', 2)
  pyrecola.select_power_LoopAmpl_rcl(1, 'LAM', 0)
  pyrecola.select_power_LoopAmpl_rcl(1, 'LAM', 1)

  pyrecola.generate_processes_rcl()

  total_tests = 0
  tests_succeeded = 0

  p1 = [250.000000000,    0.000000000,    0.000000000,  250.000000000]
  p2 = [250.000000000,    0.000000000,    0.000000000, -250.000000000]
  p3 = [250.000000000, -179.523675064, -150.850583835,  -32.494197216]
  p4 = [250.000000000,  179.523675064,  150.850583835,   32.494197216]

  mom = [p1, p2, p3, p4]

  power_born1 = [0, 4, 0]
  power_born2 = [0, 4, 1]
  power_born3 = [0, 4, 2]
  power_nlo1 = [2, 4, 0]
  power_nlo2 = [2, 4, 1]
  power_nlo3 = [2, 4, 2]

  if only_D4:
    expec = [{'prid': pr, 'Momenta': mom,
              'LO': [{'value': 0.88064422789950E-02, 'power': power_born1},
 #                    {'value': 0., 'power': power_born2},
                     {'value': 0.25331037715504E+11, 'power': power_born3}]},
             {'prid': pr, 'Momenta': mom,
              'NLO-D4': [{'value': 0.16500626032E-02, 'power': power_nlo1},
 #                        {'value': 0., 'power': power_nlo2},
                         {'value': 0.83303165145E+09, 'power': power_nlo3}]}]
  else:
    expec = [{'prid': pr, 'Momenta': mom,
              'LO': [{'value': 0.88064422789950E-02, 'power': power_born1},
#                     {'value': 0., 'power': power_born2},
                     {'value': 0.25331037715504E+11, 'power': power_born3}]},
             {'prid': pr, 'Momenta': mom,
              'NLO': [{'value': -0.68647096796848E-04, 'power': power_nlo1},
  #                    {'value':  0., 'power': power_nlo2},
                      {'value': -0.29907671722144E+09, 'power': power_nlo3}]},
             {'prid': pr, 'Momenta': mom,
              'NLO-D4': [{'value': 0.16500626032E-02, 'power': power_nlo1},
  #                       {'value': 0., 'power': power_nlo2},
                         {'value': 0.83303165145E+09, 'power': power_nlo3}]},
             {'prid': pr, 'Momenta': mom,
              'NLO-R2': [{'value': -0.17187096999E-02, 'power': power_nlo1},
  #                       {'value': 0., 'power': power_nlo2},
                         {'value': -0.11321083687E+10, 'power': power_nlo3}]}]

  res = test_amplitude_squared(1, expec, err_digits=4, warning_digits=5)
  total_tests += len(res)
  tests_succeeded += res.values().count(True)

  if total_tests == tests_succeeded:
    print('All tests(' + str(total_tests) + ') succeeded ' + success())
  elif tests_succeeded == 0:
    print('All tests(' + str(total_tests) + ') failed ' + fail())
  else:
    print('Some tests(' + str(total_tests) + ') failed ' + warning())

  pyrecola.reset_recola_rcl()

if __name__ == "__main__":
  test_tgc_WW(only_D4=True)
