#==============================================================================#
#                                 tgc_setup.py                                 #
#==============================================================================#

##############
#  Includes  #
##############

import os

#######################
#  Globals & Methods  #
#######################

thispath = os.path.dirname(os.path.realpath(__file__))
driver_tgc_path = os.path.join(thispath, 'TGC_drivers')
driver_tgc_cpeven_path = os.path.join(thispath, 'TGC_cpeven_drivers')
driver_ntgc_path = os.path.join(thispath, 'NTGC_drivers')
driver_tgc_full_path = os.path.join(thispath, 'TGC_full_drivers')
driver_sm_atgc_path = os.path.join(thispath, 'SM_ATGC_drivers')

params_tgc_WA = {'MW': 80.370881514410,
                 'WW': 0.,
                 'MZ': 91.153480619183,
                 'WZ': 0.,
                 'MH': 125.7,
                 'WH': 0.,
                 'MT': 173.21,
                 'WT': 0.,
                 'MB': 0.,
                 'MC': 0.,
                 'MS': 0.,
                 'MU': 0.,
                 'MD': 0.,
                 'ME': 0.,
                 'MM': 0.,
                 'MTA': 0.,
                 'aEW': 0.75492131512809E-02,
                 'aS': 0.10530431206663,
                 'CBL2': 3.124325,
                 'CPWL2': 0.,
                 'CPWWWL2': 0.,
                 'CWL2': 1122.3,
                 'CWWWL2': 7.7,
                 'CphiWL2': 0.,
                 'CphidL2': 9982.3,
                 'muUV': 80.370881514410,
                 'muMS': 80.399000000000,
                 'muIR': 80.370881514410,
                 'DeltaIR': 0.,
                 'DeltaIR2': 1.6449340668482
                 }

params_tgc_WW = {'MW': 80.370881514410,
                 'WW': 0.,
                 'MZ': 91.153480619183,
                 'WZ': 0.,
                 'MH': 125.7,
                 'WH': 0.,
                 'MT': 173.21,
                 'WT': 0.,
                 'MB': 0.,
                 'MC': 0.,
                 'MS': 0.,
                 'MU': 0.,
                 'MD': 0.,
                 'ME': 0.,
                 'MM': 0.,
                 'MTA': 0.,
                 'aEW': 0.75492131512809E-02,
                 'aS': 0.10530431206663,
                 'CBL2': 0.,
                 'CPWL2': 3.,
                 'CPWWWL2': 10.,
                 'CWL2': 0.,
                 'CWWWL2': 0.,
                 'CphiWL2': 0.,
                 'CphidL2': 0.,
                 'muUV': 80.370881514410,
                 'muMS': 80.399000000000,
                 'muIR': 80.370881514410,
                 'DeltaIR': 0.,
                 'DeltaIR2': 1.6449340668482
                 }

params_ntgc = {'MW': 80.370881514410,
               'WW': 0.,
               'MZ': 91.153480619183,
               'WZ': 0.,
               'MH': 125.7,
               'WH': 0.,
               'MT': 173.21,
               'WT': 0.,
               'MB': 0.,
               'MC': 0.,
               'MS': 0.,
               'MU': 0.,
               'MD': 0.,
               'ME': 0.,
               'MM': 0.,
               'MTA': 0.,
               'aEW': 0.75492131512809E-02,
               'aS': 0.10530431206663,
               'CBBL4':   10.,
               'CBL2':    10.,
               'CBWL4':   10.,
               'CBtWL4':  10.,
               'CPWL2':   10.,
               'CPWWWL2': 10.,
               'CWL2':    10.,
               'CWWL4':   10.,
               'CWWWL2':  10.,
               'CphiWL2': 10.,
               'CphidL2': 10.,
               'muUV': 80.370881514410,
               'muMS': 80.399000000000,
               'muIR': 80.370881514410,
               'DeltaIR': 0.,
               'DeltaIR2': 1.6449340668482
               }
