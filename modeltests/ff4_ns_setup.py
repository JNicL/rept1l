#==============================================================================#
#                                sm_dc_setup.py                                #
#==============================================================================#

#============#
#  Includes  #
#============#

from __future__ import print_function
import os
from subprocess import call, Popen, PIPE

#=====================#
#  Globals & Methods  #
#=====================#

driver_ff4ns_path = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                                 'FourFermionYukawaNonSimple')
