# -*- coding: utf-8 -*-
################################################################################
#                               test_sm_wzprime                                #
################################################################################


import rept1l.modeltests.SM_wzprime_tests.smwzp_setup as smwzp
from rept1l.modeltests.modeltest_utils.auto_model import GenModel


class GenSMWZPrime(GenModel):
  threads = 2
  ct_expansion = True
  pre_ct = 'smwzpexp.txt'
  verbose = True

  def renormalize_model(self, **kwargs):

    # QCD charged fields
    self.renormalize_qcd('-o', 'particles', '-qcd', '0', '2', '-qed', '0',
                         '-wzp', '0', **kwargs)

    # QCD coupling
    for i in ['6', '5', '4', '3']:
      self.renormalize_qcd('-o', 'alphaS', '-nf', i, '-qcd', '0', '2', '3',
                           '-qed', '0', '-wzp', '0', **kwargs)

    self.register_ct()

  def compute_rational(self, **kwargs):
    cls = self.__class__
    self.run_r2(vertices_file='r2list_wzp_ckm_nog.txt', nc=True, np=2, threads=cls.threads,
                orders={'qcd': [2], 'qed': [0], 'wzp': [0]})
    self.run_r2(vertices_file='r2list_wzp_ckm_nog.txt', nc=True, np=3, threads=cls.threads,
                orders={'qcd': [2, 3], 'qed': [0, 1], 'wzp': [0, 1]})
    self.run_r2(vertices_file='r2list_wzp_ckm_nog.txt', nc=True, np=4, threads=cls.threads,
                orders={'qcd': [2, 3, 4], 'qed': [0, 1, 2], 'wzp': [0, 1, 2]})




def full_smwzp_model(outputpath):
  GenSMWZPrime.driverpath = smwzp.driver_smwzp_path
  GenSMWZPrime.outputpath = outputpath
  GenSMWZPrime(odp=True)



class Genr2(GenModel):
  threads = 2
  ct_expansion = False
  verbose = True

  def renormalize_model(self, **kwargs):
    pass

  def compute_rational(self, **kwargs):
    cls = self.__class__

    self.run_r2(np=2, orders={'qcd': [2], 'qed': [0], 'wzp': [0,1,2]},
                threads=cls.threads)
    self.run_r2(np=3, orders={'qcd': [2,3], 'qed': [0,1], 'wzp': [0,1,2]},
                threads=cls.threads)
    self.run_r2(np=4, orders={'qcd': [2,3,4], 'qed': [0,1,2], 'wzp': [0,1,2]},
                threads=cls.threads)

def gen_r2(outputpath):
  Genr2.driverpath = smwzp.driver_smwzp_path
  Genr2.outputpath = outputpath
  Genr2(odp=True)

if __name__ == '__main__':
  outputpath = './SMWZP_2.2.4'
  full_smwzp_model(outputpath)
