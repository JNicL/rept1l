# -*- coding: utf-8 -*-
#==============================================================================#
#                                test_models.py                                #
#==============================================================================#

from __future__ import print_function

#============#
#  Includes  #
#============#

import os
from rept1l.modeltests.modeltest_utils.auto_model import GenModel, GenBareModel
import rept1l.modeltests.SM_tests.sm_dc_setup as smdc


class GenSM(GenModel):
  threads = 8
  ct_expansion = True
  pre_ct = 'smexp.txt'
  verbose = True

  def renormalize_model(self, **kwargs):
    # Higgs tadpole + field
    self.renormalize_sm_higgs_sector('-o', 'tadpole', 'higgs', **kwargs)

    # QCD charged fields
    self.renormalize_qcd('-o', 'particles', **kwargs)

    # QCD coupling
    for i in ['6', '5', '4', '3']:
      self.renormalize_qcd('-o', 'alphaS', '-nf', i, **kwargs)

    # Pure EW fields + EW couplings
    for p in ['particles', 'alpha0', 'Gf', 'alphaZ']:
      self.renormalize_gsw('-o', p, **kwargs)

    self.register_ct()

  def compute_rational(self, **kwargs):
    cls = self.__class__
    self.run_r2(vertices_file='smr2.txt', nc=True, threads=cls.threads)


def bare_PA_run():

  class BarePoleApprox(GenBareModel):
    driverpath = smdc.driver_sm_dc_path

    def run_bare_test(self):
      from rept1l.modeltests.SM_tests.test_poleapprox_sm import test_poleapprox_sm
      test_poleapprox_sm(only_bare=True)

  BarePoleApprox()


def full_PA_run():

  class PoleApprox(GenModel):

    threads = 8
    verbose = True
    pre_ct = 'smexp.txt'
    final_ct = 'sm_DY_PA_ct.txt'
    driverpath = smdc.driver_sm_dc_path

    def renormalize_model(self, **kwargs):
      self.renormalize_sm_higgs_sector('-o', 'tadpole', 'higgs')
      self.renormalize_qcd('-o', 'particles')
      self.renormalize_gsw('-o', 'particles')
      self.renormalize_gsw('-o', 'Gf')
      self.register_ct()

    def compute_rational(self):
      cls = self.__class__
      self.run_r2(vertices_file='sm_DY_PA_r2.txt', nc=True, threads=cls.threads)

    def gen_renormalized_model(self, rmpath):
      rmpath = os.path.join(self._reptil_mpath, 'PA_renormalized_modelfile')
      super(PoleApprox, self).gen_renormalized_model(rmpath)

    def run_test(self):
      cls = self.__class__
      self.compile_model(threads=cls.threads)
      from rept1l.modeltests.SM_tests.test_poleapprox_sm import test_poleapprox_sm
      test_poleapprox_sm(only_bare=False)

  PoleApprox()


def full_LI_run():

  class LoopInduced(GenModel):
    threads = 8
    verbose = True
    driverpath = smdc.driver_sm_dc_path

    def compute_rational(self):
      cls = self.__class__
      self.run_r2(vertices_file='sm_LI_vert.txt', nc=True, threads=cls.threads)

    def gen_renormalized_model(self, rmpath):
      rmpath = os.path.join(self._reptil_mpath, 'LI_r2_modelfile')
      self.run_model(rmpath,
                     subs_r2=True,
                     no_form=True)

    def run_test(self):
      cls = self.__class__
      self.compile_model(threads=cls.threads)
      from rept1l.modeltests.SM_tests.test_loopinduced_sm import test_loopinduced_sm
      test_loopinduced_sm(only_D4=False)

  LoopInduced()


def bare_LI_run():

  class BareLoopInduced(GenBareModel):

    driverpath = smdc.driver_sm_dc_path

    def run_bare_test(self):
      from rept1l.modeltests.SM_tests.test_loopinduced_sm import test_loopinduced_sm
      test_loopinduced_sm(only_D4=True)

  BareLoopInduced()


def D4_ttbar_run():

  class D4TTB(GenBareModel):
    driverpath = smdc.driver_sm_dc_path

    def run_bare_test(self):
      from rept1l.modeltests.SM_tests.test_ttbar_sm import test_ttbar_sm_QCD
      test_ttbar_sm_QCD(D4=True, R2=False, CT=False)

  D4TTB()


def bare_ttbar_run():

  class BareTTB(GenModel):
    threads = 8
    driverpath = smdc.driver_sm_dc_path

    def compute_rational(self):
      cls = self.__class__
      self.run_r2(vertices_file='sm_tt_r2.txt', nc=True, threads=cls.threads)

    def run_test(self):
      from rept1l.modeltests.SM_tests.test_ttbar_sm import test_ttbar_sm_QCD
      test_ttbar_sm_QCD(D4=True, R2=True, CT=False)

  BareTTB()


def full_ttbar_QCD():

  class TTB(GenSM):
    driverpath = smdc.driver_sm_dc_path
    pre_ct = 'smexp.txt'
    final_ct = 'sm_tt_ct.txt'

    def renormalize_model(self, **kwargs):

      # QCD charged fields
      self.renormalize_qcd('-o', 'particles', **kwargs)

      # QCD coupling
      self.renormalize_qcd('-o', 'alphaS', '-nf', '5', **kwargs)

      self.register_ct()

    def compute_rational(self):
      cls = self.__class__
      self.run_r2(vertices_file='sm_tt_ct.txt', nc=True, threads=cls.threads)

    def gen_renormalized_model(self, rmpath):
      rmpath = os.path.join(self._reptil_mpath, 'TTB_RENO_QCD')
      super(TTB, self).gen_renormalized_model(rmpath)

    def run_test(self):
      cls = self.__class__
      self.compile_model(threads=cls.threads)
      from rept1l.modeltests.SM_tests.test_ttbar_sm import test_ttbar_sm_QCD
      test_ttbar_sm_QCD(D4=True, R2=True, CT=True)

  TTB()


def full_ttbar_QCD_scaletest():

  # class TTB(GenSM):
  #   driverpath = smdc.driver_sm_dc_NT_path
  #   pre_ct = 'smexp.txt'
  #   # final_ct = 'sm_tt_ct.txt'
  #
  #   def renormalize_model(self, **kwargs):
  #
  #     # QCD charged fields
  #     self.renormalize_qcd('-o', 'particles', **kwargs)
  #
  #     # QCD coupling
  #     self.renormalize_qcd('-o', 'alphaS', '-nf', '5', **kwargs)
  #
  #     self.register_ct()
  #
  #   def compute_rational(self):
  #     cls = self.__class__
  #     self.run_r2(vertices_file='sm_tt_ct.txt', nc=True, threads=cls.threads)
  #
  #   def gen_renormalized_model(self, rmpath):
  #     rmpath = os.path.join(self._reptil_mpath, 'TTB_RENO_QCD')
  #     super(TTB, self).gen_renormalized_model(rmpath)
  #
  #   def run_test(self):
  #     cls = self.__class__
  #     self.compile_model(threads=cls.threads)
  #     from rept1l.modeltests.SM_tests.test_running_QCD import test_running_QCD
  #     test_running_QCD()
  #
  # TTB()
  raise Exception("FIX ct/r2 files")


def full_ttbar_BFM_QCD():

  class TTB(GenSM):
    driverpath = smdc.driver_smbfm_dc_path
    pre_ct = 'smexp.txt'
    final_ct = 'sm_tt_ct.txt'

    def renormalize_model(self, **kwargs):

      # QCD charged fields
      self.renormalize_qcd('-o', 'particles', bfm=True, **kwargs)

      # QCD coupling
      self.renormalize_qcd('-o', 'alphaS', '-nf', '5', bfm=True, **kwargs)

      self.register_ct()

    def compute_rational(self):
      cls = self.__class__
      self.run_r2(vertices_file='sm_tt_ct.txt', nc=True, threads=cls.threads)

    def gen_renormalized_model(self, rmpath):
      rmpath = os.path.join(self._reptil_mpath, 'TTB_RENO_QCD')
      super(TTB, self).gen_renormalized_model(rmpath)

    def run_test(self):
      cls = self.__class__
      self.compile_model(threads=cls.threads)
      from rept1l.modeltests.SM_tests.test_ttbar_sm import test_ttbar_sm_QCD
      test_ttbar_sm_QCD(D4=True, R2=True, CT=True,
                        indiviual_contributions=False)

  TTB()


def full_ttbar_EW():

  class TTB(GenSM):
    driverpath = smdc.driver_sm_dc_path
    pre_ct = 'smexp.txt'
    final_ct = 'sm_tt_EW_ct.txt'

    def renormalize_model(self, **kwargs):

      # Higgs tadpole + field
      self.renormalize_sm_higgs_sector('-o', 'tadpole', 'higgs', **kwargs)

      # QCD charged fields
      self.renormalize_qcd('-o', 'particles', **kwargs)

      # Pure EW fields + EW couplings
      for p in ['particles', 'Gf']:
        self.renormalize_gsw('-o', p, **kwargs)

      self.register_ct()

    def compute_rational(self):
      cls = self.__class__
      self.run_r2(vertices_file='sm_tt_EW_ct.txt', nc=True, threads=cls.threads)

    def gen_renormalized_model(self, rmpath):
      rmpath = os.path.join(self._reptil_mpath, 'TTB_RENO_EW')
      super(TTB, self).gen_renormalized_model(rmpath)

    def run_test(self):
      cls = self.__class__
      self.compile_model(threads=cls.threads)
      from rept1l.modeltests.SM_tests.test_ttbar_sm import test_ttbar_sm_EW
      test_ttbar_sm_EW(D4=True, R2=True, CT=True, indiviual_contributions=False)

  TTB()


def full_ttbar_BFM_EW():

  class TTB(GenSM):
    driverpath = smdc.driver_smbfm_dc_path
    pre_ct = 'smexp.txt'
    final_ct = 'sm_tt_EW_ct.txt'

    def renormalize_model(self, **kwargs):

      # Higgs tadpole + field
      self.renormalize_sm_higgs_sector('-o', 'tadpole', 'higgs', bfm=True,
                                       **kwargs)

      # QCD charged fields
      self.renormalize_qcd('-o', 'particles', bfm=True, **kwargs)

      # Pure EW fields + EW couplings
      for p in ['particles', 'Gf']:
        self.renormalize_gsw('-o', p, bfm=True, **kwargs)

      self.register_ct()

    def compute_rational(self):
      cls = self.__class__
      self.run_r2(vertices_file='sm_tt_EW_ct.txt', nc=True, threads=cls.threads)

    def gen_renormalized_model(self, rmpath):
      rmpath = os.path.join(self._reptil_mpath, 'TTB_RENO_EW')
      super(TTB, self).gen_renormalized_model(rmpath)

    def run_test(self):
      cls = self.__class__
      self.compile_model(threads=cls.threads)
      from rept1l.modeltests.SM_tests.test_ttbar_sm import test_ttbar_sm_EW
      test_ttbar_sm_EW(D4=True, R2=True, CT=True, indiviual_contributions=False)

  TTB()


def full_sm_dc_model(outputpath):
  GenSM.driverpath = smdc.driver_sm_dc_path
  GenSM.outputpath = outputpath
  GenSM(odp=True, use_run_ct=True)


def full_sm_ts_model(outputpath):
  GenSM.driverpath = smdc.driver_sm_dc_NT_path
  GenSM.outputpath = outputpath
  GenSM(odp=True, use_run_ct=True)


def full_sm_bfm_dc_model(outputpath):
  GenSM.driverpath = smdc.driver_smbfm_dc_path
  GenSM.outputpath = outputpath
  GenSM(odp=True, bfm=True, use_run_ct=True)


def full_sm_dc_ckm_model(outputpath):

  class SMCKM(GenSM):

    driverpath = smdc.driver_sm_dc_ckm_path

    def compute_rational(self, **kwargs):
      cls = self.__class__
      self.run_r2(vertices_file='smr2.txt', nc=True, threads=cls.threads,
                  orders={'qcd': [0, 1, 2, 3, 4], 'qed': [0, 1, 2, 3],
                          'ckm': [0, 1]})

    def renormalize_model(self, **kwargs):

      no_ckm_order = ('-qcd', '0', '1', '2', '3', '-qed', '0', '1', '2', '3', '-ckm', '0')
      # Higgs tadpole + field
      self.renormalize_sm_higgs_sector('-o', 'tadpole', 'higgs',
                                       *no_ckm_order, **kwargs)

      # QCD charged fields
      self.renormalize_qcd('-o', 'particles',*no_ckm_order,  **kwargs)

          # '-qcd', '0', '2',
          #                  '-qed', '0', '2',
          #                  '-ckm', '0', **kwargs)

      # QCD coupling
      for i in ['6', '5', '4', '3']:
        self.renormalize_qcd('-o', 'alphaS', '-nf', i, *no_ckm_order, **kwargs)

      # Pure EW fields + EW couplings
      for p in ['particles', 'alpha0', 'Gf', 'alphaZ']:
        self.renormalize_gsw('-o', p, *no_ckm_order, **kwargs)

      self.register_ct()

  SMCKM()


def full_sm_ferm_model(outputpath):

  class GenSMFerm(GenSM):
    driverpath = smdc.driver_smferm_path

    def renormalize_model(self, **kwargs):
      # Higgs tadpole + field
      self.renormalize_sm_higgs_sector('-o', 'tadpole', 'higgs', **kwargs)

      # QCD charged fields
      self.renormalize_qcd('-o', 'gluon', **kwargs)

      # QCD coupling
      for i in ['6', '5', '4', '3']:
        self.renormalize_qcd('-o', 'alphaS', '-nf', i, **kwargs)

      # Pure EW fields + EW couplings
      for p in ['vectors', 'alpha0', 'Gf_ferm', 'alphaZ']:
        self.renormalize_gsw('-o', p, **kwargs)

      self.register_ct()

  GenSMFerm.outputpath = outputpath
  GenSMFerm(odp=True, bfm=False, use_run_ct=True)


def sm_ferm_model_YUK(outputpath):

  class GenSMFerm(GenSM):
    driverpath = smdc.driver_smferm_yt_path
    mdlname = "SM (QCD,fermloops,YUK)"

    def compute_rational(self, **kwargs):
      cls = self.__class__
      self.run_r2(vertices_file='smr2.txt',
                  nc=True, threads=cls.threads,
                  orders={'qcd': [0, 1, 2, 3, 4], 'qed': [0, 1, 2, 3],
                          'yuk': [0, 1, 2]})

    def renormalize_model(self, **kwargs):
      no_qed_order = ('-qcd', '0', '1', '2', '3', '-qed', '0', '-yuk', '0')

      # QCD charged fields
      self.renormalize_qcd('-o', 'particles', **kwargs)

      # QCD coupling
      for i in ['6', '5', '4', '3']:
        self.renormalize_qcd('-o', 'alphaS', '-nf', i, *no_qed_order, **kwargs)

      self.register_ct()

  GenSMFerm.outputpath = outputpath
  GenSMFerm(odp=True, bfm=False, use_run_ct=True)


def sm_ferm_model_YUK_NOTV(outputpath):

  class GenSMFerm(GenSM):
    driverpath = smdc.driver_smferm_yt_path
    exclude_vtx = 'exclude_topv.txt'
    mdlname = "SM (QCD,fermloops,YUK,NOTV)"

    def compute_rational(self, **kwargs):
      cls = self.__class__
      self.run_r2(vertices_file='smr2.txt',
                  nc=True, threads=cls.threads,
                  orders={'qcd': [0, 1, 2, 3, 4], 'qed': [0, 1, 2, 3],
                          'yuk': [0, 1, 2]})

    def renormalize_model(self, **kwargs):
      no_qed_order = ('-qcd', '0', '1', '2', '3', '-qed', '0', '-yuk', '0')

      # QCD charged fields
      self.renormalize_qcd('-o', 'particles', **kwargs)

      # QCD coupling
      for i in ['6', '5', '4', '3']:
        self.renormalize_qcd('-o', 'alphaS', '-nf', i, *no_qed_order, **kwargs)

      self.register_ct()

  GenSMFerm.outputpath = outputpath
  GenSMFerm(odp=True, bfm=False, use_run_ct=True)


def sm_nf5(outputpath):

  class GenSMFerm(GenSM):
    driverpath = smdc.driver_sm_dc_nf5_path

    def compute_rational(self, **kwargs):
      cls = self.__class__
      self.run_r2(vertices_file='smr2_nf5.txt', nc=True, np=2, threads=cls.threads,
                  orders={'qcd': [2], 'qed': [0]})
      self.run_r2(vertices_file='smr2_nf5.txt', nc=True, np=3, threads=cls.threads,
                  orders={'qcd': [2, 3], 'qed': [0, 1]})
      self.run_r2(vertices_file='smr2_nf5.txt', nc=True, np=4, threads=cls.threads,
                  orders={'qcd': [2, 3, 4], 'qed': [0, 1, 2]})

    def renormalize_model(self, **kwargs):
      no_qed_order = ('-qcd', '0', '1', '2', '3', '-qed', '0')

      # QCD charged fields
      self.renormalize_qcd('-o', 'particles_nf5', **kwargs)

      # QCD coupling
      for i in ['5', '4', '3']:
        self.renormalize_qcd('-o', 'alphaS', '-nf', i, *no_qed_order, **kwargs)

      self.register_ct()

  GenSMFerm.outputpath = outputpath
  GenSMFerm(odp=True, bfm=False, use_run_ct=True)



def sm_nf4(outputpath):

  class GenSMFerm(GenSM):
    driverpath = smdc.driver_sm_dc_nf4_path

    def compute_rational(self, **kwargs):
      cls = self.__class__
      self.run_r2(vertices_file='smr2_nf4.txt', nc=True, np=2, threads=cls.threads,
                  orders={'qcd': [2], 'qed': [0]})
      self.run_r2(vertices_file='smr2_nf4.txt', nc=True, np=3, threads=cls.threads,
                  orders={'qcd': [2, 3], 'qed': [0, 1]})
      self.run_r2(vertices_file='smr2_nf4.txt', nc=True, np=4, threads=cls.threads,
                  orders={'qcd': [2, 3, 4], 'qed': [0, 1, 2]})

    def renormalize_model(self, **kwargs):
      # QCD charged fields
      self.renormalize_qcd('-o', 'particles_nf4', **kwargs)

      no_qed_order = ('-qcd', '0', '1', '2', '3', '-qed', '0')
      # QCD coupling
      for i in ['4', '3']:
        self.renormalize_qcd('-o', 'alphaS', '-nf', i, *no_qed_order, **kwargs)

      self.register_ct()

  GenSMFerm.outputpath = outputpath
  GenSMFerm(odp=True, bfm=False, use_run_ct=True)


def sm_hpc(outputpath):

  class GenSMhpc(GenSM):
    driverpath = smdc.driver_smhpc_path
    ct_expansion = False

    def compute_rational(self, **kwargs):
      cls = self.__class__
      self.run_r2(vertices_file='smr2.txt', nc=True, threads=cls.threads,
                  orders={'qcd': [0, 1, 2, 3, 4], 'qed': [0, 1, 2, 3],
                          'hqc': [0], 'htc': [0]})

    def renormalize_model(self, **kwargs):
      pass

  def gen_renormalized_model(self, rmpath, **kwargs):
    """ Overwrite the 2nd pass for the model generation (not including counterterms)"""
    cls = self.__class__
    if 'odp' in kwargs:
      odp = kwargs['odp']
    else:
      odp = False
    self.run_model(rmpath,
                   copy_drivers=False,
                   vertices_file=None,
                   subs_ct=False,
                   subs_r2=True,
                   perform_expansion=False,
                   only_form=False,
                   odp=odp,
                   no_form=True)

  GenSMhpc.outputpath = outputpath
  GenSMhpc(odp=True, bfm=False, use_run_ct=True)


if __name__ == "__main__":
  #full_PA_run()
  # bare_PA_run()
  #bare_LI_run()
  #full_LI_run()

  #D4_ttbar_run()
  #full_ttbar_BFM_EW()
  # full_ttbar_QCD_scaletest()
  # bare_ttbar_run()
  # D4_ttbar_run()

  # full_ttbar_BFM_QCD()
  # full_ttbar_QCD()
  # full_ttbar_QCD_scaletest()

  # full_sm_dc_ckm_model('/home/nick/SM_DC_CKM_2.1.0')
  # sm_ferm_model_YT('/home/nick/SM_FERM_YT_2.1.7')
  # sm_ferm_model_YT('/home/nick/SM_FERM_YT_NOTTV_2.1.7')
  # sm_hpc('/home/nick/SM_HPC_2.1.7')
  # sm_nf5('/home/nick/SM_NF5_2.2.3')
  sm_nf4('/home/nick/SM_NF4_2.2.4')
  # sm_nf5('/home/nick/SM_NF5_2.2.4')
  # sm_ferm_model_YUK_NOTV('/home/nick/SM_FERM_YUK_NOTV_2.2.2')
