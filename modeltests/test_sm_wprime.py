# -*- coding: utf-8 -*-
################################################################################
#                                test_sm_wprime                                #
################################################################################


import rept1l.modeltests.SM_wprime_tests.smwp_setup as smwp
from rept1l.modeltests.modeltest_utils.auto_model import GenModel


class GenSMWPrime(GenModel):
  threads = 8
  ct_expansion = True
  pre_ct = 'smwpexp.txt'
  verbose = True

  def renormalize_model(self, **kwargs):

    # QCD charged fields
    self.renormalize_qcd('-o', 'particles', '-qcd', '0', '2', '-qed', '0',
                         **kwargs)

    # QCD coupling
    for i in ['6', '5', '4', '3']:
      self.renormalize_qcd('-o', 'alphaS', '-nf', i, '-qcd', '0', '2', '3',
                           '-qed', '0', **kwargs)

    self.register_ct()

  def compute_rational(self, **kwargs):
    cls = self.__class__
    self.run_r2(vertices_file='smwpr2.txt', nc=True, threads=cls.threads,
                orders={'qcd': [0, 1, 2, 3, 4], 'qed': [0, 1, 2]})


def full_smwp_model(outputpath):
  GenSMWPrime.driverpath = smwp.driver_smwp_path
  GenSMWPrime.outputpath = outputpath
  GenSMWPrime(odp=True)


if __name__ == '__main__':
  outputpath = './sm_wprime_test'
  full_smwp_model(outputpath)
