#==============================================================================#
#                                   model.py                                   #
#==============================================================================#

#============#
#  Includes  #
#============#

import os
from rept1l.autoct.modelfile import model, CTParameter
from rept1l.autoct.modelfile import TreeInducedCTVertex, CTVertex
from rept1l.autoct.auto_parameters_ct import auto_assign_ct_particle
from rept1l.autoct.auto_parameters_ct import set_light_particle
from rept1l.autoct.auto_parameters_ct import assign_counterterm
from rept1l.autoct.auto_parameters_ct import add_counterterm
from rept1l.autoct.auto_tadpole_ct import auto_tadpole_vertices
from rept1l.autoct.auto_tadpole_ct import assign_FJTadpole_field_shift

#===========#
#  Globals  #
#===========#

# coupling constructor
Coupling = model.object_library.Coupling

# access to instances
param = model.parameters
P = model.particles
L = model.lorentz

modelname = 'HS'
modelgauge = "'t Hooft-Feynman"

features = {'fermionloop_opt' : True,
            'qcd_rescaling' : True,
            'sm_generation_opt': True,
           }


scales = [param.muMS_BSM]

# LCT allows to access the 2-point lorentz structures defined in lorentz_ct.py
import rept1l.autoct.lorentz_ct as LCT

#=============================#
#  External Parameter Orders  #
#=============================#

parameter_orders = {param.aEW: {'QED': 2},
                    param.aS: {'QCD': 2},
                    param.l3: {'QED': 2}}

#===============================#
#  Fermion mass regularization  #
#===============================#

for p in [P.e__minus__, P.mu__minus__, P.tau__minus__, P.u, P.d, P.c, P.s, P.b]:
  set_light_particle(p)

###################################
#  SM Optimizations for Fermions  #
###################################

doublets = [(P.nu_e, P.e__minus__), (P.nu_mu, P.mu__minus__),
            (P.nu_tau, P.tau__minus__), (P.u, P.d), (P.c, P.s), (P.t, P.b)]

generations = [(P.e__minus__, P.mu__minus__, P.tau__minus__),
               (P.d, P.s, P.b)]

#==============================================================================#
#                           coupling renormalization                           #
#==============================================================================#

assign_counterterm(param.ee, 'dZee', 'ee*dZee')
assign_counterterm(param.gs, 'dZgs', 'gs*dZgs')
assign_counterterm(param.sa, 'da', 'ca*da')
assign_counterterm(param.ca, 'da', '-sa*da')
assign_counterterm(param.l3, 'dl3', 'dl3')

#==============================#
#  Declare tadpole parameters  #
#==============================#

tadpole_parameter = []
assign_FJTadpole_field_shift(P.Hl, 'dtHl')
assign_FJTadpole_field_shift(P.Hh, 'dtHh')
auto_tadpole_vertices()

#==============================================================================#
#                                  particles                                   #
#==============================================================================#

mixings = {P.A: [P.Z], P.Z: [P.A], P.Hl: [P.Hh], P.Hh: [P.Hl]}
auto_assign_ct_particle(mixings)

#########################################
#  Renormalization of the Gauge-fixing  #
#########################################

GC_MZ_GF = Coupling(name='GC_MZ_GF',
                    value='MZ',
                    order={'QED': 0})

GC_MW_GF = Coupling(name='GC_MW_GF',
                    value='I*MW',
                    order={'QED': 0})

GC_MMW_GF = Coupling(name='GC_MMW_GF',
                     value='-I*MW',
                     order={'QED': 0})

V_WPGM = TreeInducedCTVertex(name='V_WPGM',
                             particles=[P.W__plus__, P.G__minus__],
                             color=['1'],
                             type='treeinducedct',
                             loop_particles='N',
                             lorentz=[LCT.VS1],
                             couplings={(0, 0): GC_MW_GF})

V_WMGP = TreeInducedCTVertex(name='V_WMGP',
                             particles=[P.W__minus__, P.G__plus__],
                             color=['1'],
                             type='treeinducedct',
                             loop_particles='N',
                             lorentz=[LCT.VS1],
                             couplings={(0, 0): GC_MMW_GF})

V_ZG0 = TreeInducedCTVertex(name='V_ZG0',
                            particles=[P.Z, P.G0],
                            color=['1'],
                            type='treeinducedct',
                            loop_particles='N',
                            lorentz=[LCT.VS1],
                            couplings={(0, 0): GC_MZ_GF})
