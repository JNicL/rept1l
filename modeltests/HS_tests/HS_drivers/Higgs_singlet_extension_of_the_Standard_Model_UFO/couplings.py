# This file was automatically created by FeynRules 2.3.29
# Mathematica version: 11.3.0 for Linux x86 (64-bit) (March 7, 2018)
# Date: Fri 23 Nov 2018 10:26:23


from .object_library import all_couplings, Coupling

from .function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



GC_1 = Coupling(name = 'GC_1',
                value = '(ee*complex(0,1))/3.',
                order = {'QED':1})

GC_2 = Coupling(name = 'GC_2',
                value = '(-2*ee*complex(0,1))/3.',
                order = {'QED':1})

GC_3 = Coupling(name = 'GC_3',
                value = '-(ee*complex(0,1))',
                order = {'QED':1})

GC_4 = Coupling(name = 'GC_4',
                value = 'ee*complex(0,1)',
                order = {'QED':1})

GC_5 = Coupling(name = 'GC_5',
                value = 'ee**2*complex(0,1)',
                order = {'QED':2})

GC_6 = Coupling(name = 'GC_6',
                value = '2*ee**2*complex(0,1)',
                order = {'QED':2})

GC_7 = Coupling(name = 'GC_7',
                value = '-ee**2/(2.*cw)',
                order = {'QED':2})

GC_8 = Coupling(name = 'GC_8',
                value = 'ee**2/(2.*cw)',
                order = {'QED':2})

GC_9 = Coupling(name = 'GC_9',
                value = '-(ca*ee**2*complex(0,1))/(2.*cw)',
                order = {'QED':2})

GC_10 = Coupling(name = 'GC_10',
                 value = '-gs',
                 order = {'QCD':1})

GC_11 = Coupling(name = 'GC_11',
                 value = 'complex(0,1)*gs',
                 order = {'QCD':1})

GC_12 = Coupling(name = 'GC_12',
                 value = 'complex(0,1)*gs**2',
                 order = {'QCD':2})

GC_13 = Coupling(name = 'GC_13',
                 value = '-(complex(0,1)*l2)/2.',
                 order = {'QED':2})

GC_14 = Coupling(name = 'GC_14',
                 value = '-(complex(0,1)*l2)',
                 order = {'QED':2})

GC_15 = Coupling(name = 'GC_15',
                 value = '(-3*complex(0,1)*l2)/2.',
                 order = {'QED':2})

GC_16 = Coupling(name = 'GC_16',
                 value = 'ee*complex(0,1)*MW',
                 order = {'QED':1})

GC_17 = Coupling(name = 'GC_17',
                 value = '-(ee**2*complex(0,1)*sa)/(2.*cw)',
                 order = {'QED':2})

GC_18 = Coupling(name = 'GC_18',
                 value = '-(ca*complex(0,1)*l2*sa)/2. + ca*complex(0,1)*l3*sa',
                 order = {'QED':2})

GC_19 = Coupling(name = 'GC_19',
                 value = '-(ca**2*complex(0,1)*l3) - (complex(0,1)*l2*sa**2)/2.',
                 order = {'QED':2})

GC_20 = Coupling(name = 'GC_20',
                 value = '-(ca**2*complex(0,1)*l2)/2. - complex(0,1)*l3*sa**2',
                 order = {'QED':2})

GC_21 = Coupling(name = 'GC_21',
                 value = '(-3*ca**3*complex(0,1)*l2*sa)/2. + 3*ca**3*complex(0,1)*l3*sa + 3*ca*complex(0,1)*l1*sa**3 - 3*ca*complex(0,1)*l3*sa**3',
                 order = {'QED':2})

GC_22 = Coupling(name = 'GC_22',
                 value = '3*ca**3*complex(0,1)*l1*sa - 3*ca**3*complex(0,1)*l3*sa - (3*ca*complex(0,1)*l2*sa**3)/2. + 3*ca*complex(0,1)*l3*sa**3',
                 order = {'QED':2})

GC_23 = Coupling(name = 'GC_23',
                 value = '(-3*ca**4*complex(0,1)*l2)/2. - 6*ca**2*complex(0,1)*l3*sa**2 - 3*complex(0,1)*l1*sa**4',
                 order = {'QED':2})

GC_24 = Coupling(name = 'GC_24',
                 value = '-3*ca**4*complex(0,1)*l1 - 6*ca**2*complex(0,1)*l3*sa**2 - (3*complex(0,1)*l2*sa**4)/2.',
                 order = {'QED':2})

GC_25 = Coupling(name = 'GC_25',
                 value = '-(ca**4*complex(0,1)*l3) - 3*ca**2*complex(0,1)*l1*sa**2 - (3*ca**2*complex(0,1)*l2*sa**2)/2. + 4*ca**2*complex(0,1)*l3*sa**2 - complex(0,1)*l3*sa**4',
                 order = {'QED':2})

GC_26 = Coupling(name = 'GC_26',
                 value = '(ee**2*complex(0,1))/(2.*sw**2)',
                 order = {'QED':2})

GC_27 = Coupling(name = 'GC_27',
                 value = '-((ee**2*complex(0,1))/sw**2)',
                 order = {'QED':2})

GC_28 = Coupling(name = 'GC_28',
                 value = '(ca**2*ee**2*complex(0,1))/(2.*sw**2)',
                 order = {'QED':2})

GC_29 = Coupling(name = 'GC_29',
                 value = '(cw**2*ee**2*complex(0,1))/sw**2',
                 order = {'QED':2})

GC_30 = Coupling(name = 'GC_30',
                 value = '(ca*ee**2*complex(0,1)*sa)/(2.*sw**2)',
                 order = {'QED':2})

GC_31 = Coupling(name = 'GC_31',
                 value = '(ee**2*complex(0,1)*sa**2)/(2.*sw**2)',
                 order = {'QED':2})

GC_32 = Coupling(name = 'GC_32',
                 value = 'ee/(2.*sw)',
                 order = {'QED':1})

GC_33 = Coupling(name = 'GC_33',
                 value = '(ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_34 = Coupling(name = 'GC_34',
                 value = '-(ca*ee*complex(0,1))/(2.*sw)',
                 order = {'QED':1})

GC_35 = Coupling(name = 'GC_35',
                 value = '(ca*ee*complex(0,1))/(2.*sw)',
                 order = {'QED':1})

GC_36 = Coupling(name = 'GC_36',
                 value = '-((cw*ee*complex(0,1))/sw)',
                 order = {'QED':1})

GC_37 = Coupling(name = 'GC_37',
                 value = '(cw*ee*complex(0,1))/sw',
                 order = {'QED':1})

GC_38 = Coupling(name = 'GC_38',
                 value = '-ee**2/(2.*sw)',
                 order = {'QED':2})

GC_39 = Coupling(name = 'GC_39',
                 value = 'ee**2/(2.*sw)',
                 order = {'QED':2})

GC_40 = Coupling(name = 'GC_40',
                 value = '-(ca*ee**2*complex(0,1))/(2.*sw)',
                 order = {'QED':2})

GC_41 = Coupling(name = 'GC_41',
                 value = '(2*cw*ee**2*complex(0,1))/sw',
                 order = {'QED':2})

GC_42 = Coupling(name = 'GC_42',
                 value = '-(ee*MW)/(2.*sw)',
                 order = {'QED':1})

GC_43 = Coupling(name = 'GC_43',
                 value = '(ee*MW)/(2.*sw)',
                 order = {'QED':1})

GC_44 = Coupling(name = 'GC_44',
                 value = '-(ca*ee*complex(0,1)*MW)/(2.*sw)',
                 order = {'QED':1})

GC_45 = Coupling(name = 'GC_45',
                 value = '(ee*complex(0,1)*MZ)/(2.*sw)',
                 order = {'QED':1})

GC_46 = Coupling(name = 'GC_46',
                 value = '-(ee*complex(0,1)*sa)/(2.*sw)',
                 order = {'QED':1})

GC_47 = Coupling(name = 'GC_47',
                 value = '(ee*complex(0,1)*sa)/(2.*sw)',
                 order = {'QED':1})

GC_48 = Coupling(name = 'GC_48',
                 value = '-(ee**2*complex(0,1)*sa)/(2.*sw)',
                 order = {'QED':2})

GC_49 = Coupling(name = 'GC_49',
                 value = '-(ee*complex(0,1)*MW*sa)/(2.*sw)',
                 order = {'QED':1})

GC_50 = Coupling(name = 'GC_50',
                 value = '(ee*complex(0,1)*sw)/(3.*cw)',
                 order = {'QED':1})

GC_51 = Coupling(name = 'GC_51',
                 value = '(-2*ee*complex(0,1)*sw)/(3.*cw)',
                 order = {'QED':1})

GC_52 = Coupling(name = 'GC_52',
                 value = '(ee*complex(0,1)*sw)/cw',
                 order = {'QED':1})

GC_53 = Coupling(name = 'GC_53',
                 value = '-(cw*ee*complex(0,1))/(2.*sw) - (ee*complex(0,1)*sw)/(6.*cw)',
                 order = {'QED':1})

GC_54 = Coupling(name = 'GC_54',
                 value = '(cw*ee*complex(0,1))/(2.*sw) - (ee*complex(0,1)*sw)/(6.*cw)',
                 order = {'QED':1})

GC_55 = Coupling(name = 'GC_55',
                 value = '-(cw*ee*complex(0,1))/(2.*sw) + (ee*complex(0,1)*sw)/(2.*cw)',
                 order = {'QED':1})

GC_56 = Coupling(name = 'GC_56',
                 value = '(cw*ee*complex(0,1))/(2.*sw) + (ee*complex(0,1)*sw)/(2.*cw)',
                 order = {'QED':1})

GC_57 = Coupling(name = 'GC_57',
                 value = '-(ca*cw*ee)/(2.*sw) - (ca*ee*sw)/(2.*cw)',
                 order = {'QED':1})

GC_58 = Coupling(name = 'GC_58',
                 value = '-((cw*ee**2*complex(0,1))/sw) + (ee**2*complex(0,1)*sw)/cw',
                 order = {'QED':2})

GC_59 = Coupling(name = 'GC_59',
                 value = '-(cw*ee*complex(0,1)*MW)/(2.*sw) + (ee*complex(0,1)*MW*sw)/(2.*cw)',
                 order = {'QED':1})

GC_60 = Coupling(name = 'GC_60',
                 value = '-(ca*cw*ee*complex(0,1)*MZ)/(2.*sw) - (ca*ee*complex(0,1)*MZ*sw)/(2.*cw)',
                 order = {'QED':1})

GC_61 = Coupling(name = 'GC_61',
                 value = '-(cw*ee*sa)/(2.*sw) - (ee*sa*sw)/(2.*cw)',
                 order = {'QED':1})

GC_62 = Coupling(name = 'GC_62',
                 value = '-(cw*ee*complex(0,1)*MZ*sa)/(2.*sw) - (ee*complex(0,1)*MZ*sa*sw)/(2.*cw)',
                 order = {'QED':1})

GC_63 = Coupling(name = 'GC_63',
                 value = '-(ee**2*complex(0,1)) + (cw**2*ee**2*complex(0,1))/(2.*sw**2) + (ee**2*complex(0,1)*sw**2)/(2.*cw**2)',
                 order = {'QED':2})

GC_64 = Coupling(name = 'GC_64',
                 value = 'ee**2*complex(0,1) + (cw**2*ee**2*complex(0,1))/(2.*sw**2) + (ee**2*complex(0,1)*sw**2)/(2.*cw**2)',
                 order = {'QED':2})

GC_65 = Coupling(name = 'GC_65',
                 value = 'ca**2*ee**2*complex(0,1) + (ca**2*cw**2*ee**2*complex(0,1))/(2.*sw**2) + (ca**2*ee**2*complex(0,1)*sw**2)/(2.*cw**2)',
                 order = {'QED':2})

GC_66 = Coupling(name = 'GC_66',
                 value = 'ca*ee**2*complex(0,1)*sa + (ca*cw**2*ee**2*complex(0,1)*sa)/(2.*sw**2) + (ca*ee**2*complex(0,1)*sa*sw**2)/(2.*cw**2)',
                 order = {'QED':2})

GC_67 = Coupling(name = 'GC_67',
                 value = 'ee**2*complex(0,1)*sa**2 + (cw**2*ee**2*complex(0,1)*sa**2)/(2.*sw**2) + (ee**2*complex(0,1)*sa**2*sw**2)/(2.*cw**2)',
                 order = {'QED':2})

GC_68 = Coupling(name = 'GC_68',
                 value = '-(ee**2*complex(0,1)*vev)/(2.*cw)',
                 order = {'QED':1})

GC_69 = Coupling(name = 'GC_69',
                 value = '(ca*ee**2*complex(0,1)*vev)/(2.*sw**2)',
                 order = {'QED':1})

GC_70 = Coupling(name = 'GC_70',
                 value = '(ee**2*complex(0,1)*sa*vev)/(2.*sw**2)',
                 order = {'QED':1})

GC_71 = Coupling(name = 'GC_71',
                 value = '-(ee**2*complex(0,1)*vev)/(2.*sw)',
                 order = {'QED':1})

GC_72 = Coupling(name = 'GC_72',
                 value = 'ca*ee**2*complex(0,1)*vev + (ca*cw**2*ee**2*complex(0,1)*vev)/(2.*sw**2) + (ca*ee**2*complex(0,1)*sw**2*vev)/(2.*cw**2)',
                 order = {'QED':1})

GC_73 = Coupling(name = 'GC_73',
                 value = 'ee**2*complex(0,1)*sa*vev + (cw**2*ee**2*complex(0,1)*sa*vev)/(2.*sw**2) + (ee**2*complex(0,1)*sa*sw**2*vev)/(2.*cw**2)',
                 order = {'QED':1})

GC_74 = Coupling(name = 'GC_74',
                 value = '-(complex(0,1)*l2*sa*vev)/2. - ca*complex(0,1)*l3*vevs',
                 order = {'QED':1})

GC_75 = Coupling(name = 'GC_75',
                 value = '-(ca*complex(0,1)*l2*vev)/2. + complex(0,1)*l3*sa*vevs',
                 order = {'QED':1})

GC_76 = Coupling(name = 'GC_76',
                 value = '(-3*ca**2*complex(0,1)*l2*sa*vev)/2. + 2*ca**2*complex(0,1)*l3*sa*vev - complex(0,1)*l3*sa**3*vev - ca**3*complex(0,1)*l3*vevs - 3*ca*complex(0,1)*l1*sa**2*vevs + 2*ca*complex(0,1)*l3*sa**2*vevs',
                 order = {'QED':1})

GC_77 = Coupling(name = 'GC_77',
                 value = '-3*ca**2*complex(0,1)*l3*sa*vev - (3*complex(0,1)*l2*sa**3*vev)/2. - 3*ca**3*complex(0,1)*l1*vevs - 3*ca*complex(0,1)*l3*sa**2*vevs',
                 order = {'QED':1})

GC_78 = Coupling(name = 'GC_78',
                 value = '(-3*ca**3*complex(0,1)*l2*vev)/2. - 3*ca*complex(0,1)*l3*sa**2*vev + 3*ca**2*complex(0,1)*l3*sa*vevs + 3*complex(0,1)*l1*sa**3*vevs',
                 order = {'QED':1})

GC_79 = Coupling(name = 'GC_79',
                 value = '-(ca**3*complex(0,1)*l3*vev) - (3*ca*complex(0,1)*l2*sa**2*vev)/2. + 2*ca*complex(0,1)*l3*sa**2*vev + 3*ca**2*complex(0,1)*l1*sa*vevs - 2*ca**2*complex(0,1)*l3*sa*vevs + complex(0,1)*l3*sa**3*vevs',
                 order = {'QED':1})

GC_80 = Coupling(name = 'GC_80',
                 value = '-(complex(0,1)*yb)',
                 order = {'QED':1})

GC_81 = Coupling(name = 'GC_81',
                 value = '-(yb/cmath.sqrt(2))',
                 order = {'QED':1})

GC_82 = Coupling(name = 'GC_82',
                 value = 'yb/cmath.sqrt(2)',
                 order = {'QED':1})

GC_83 = Coupling(name = 'GC_83',
                 value = '-((ca*complex(0,1)*yb)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_84 = Coupling(name = 'GC_84',
                 value = '-((complex(0,1)*sa*yb)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_85 = Coupling(name = 'GC_85',
                 value = 'complex(0,1)*yc',
                 order = {'QED':1})

GC_86 = Coupling(name = 'GC_86',
                 value = '-(yc/cmath.sqrt(2))',
                 order = {'QED':1})

GC_87 = Coupling(name = 'GC_87',
                 value = 'yc/cmath.sqrt(2)',
                 order = {'QED':1})

GC_88 = Coupling(name = 'GC_88',
                 value = '-((ca*complex(0,1)*yc)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_89 = Coupling(name = 'GC_89',
                 value = '-((complex(0,1)*sa*yc)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_90 = Coupling(name = 'GC_90',
                 value = '-(complex(0,1)*ydo)',
                 order = {'QED':1})

GC_91 = Coupling(name = 'GC_91',
                 value = '-(ydo/cmath.sqrt(2))',
                 order = {'QED':1})

GC_92 = Coupling(name = 'GC_92',
                 value = 'ydo/cmath.sqrt(2)',
                 order = {'QED':1})

GC_93 = Coupling(name = 'GC_93',
                 value = '-((ca*complex(0,1)*ydo)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_94 = Coupling(name = 'GC_94',
                 value = '-((complex(0,1)*sa*ydo)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_95 = Coupling(name = 'GC_95',
                 value = '-(complex(0,1)*ye)',
                 order = {'QED':1})

GC_96 = Coupling(name = 'GC_96',
                 value = '-(ye/cmath.sqrt(2))',
                 order = {'QED':1})

GC_97 = Coupling(name = 'GC_97',
                 value = 'ye/cmath.sqrt(2)',
                 order = {'QED':1})

GC_98 = Coupling(name = 'GC_98',
                 value = '-((ca*complex(0,1)*ye)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_99 = Coupling(name = 'GC_99',
                 value = '-((complex(0,1)*sa*ye)/cmath.sqrt(2))',
                 order = {'QED':1})

GC_100 = Coupling(name = 'GC_100',
                  value = '-(complex(0,1)*ym)',
                  order = {'QED':1})

GC_101 = Coupling(name = 'GC_101',
                  value = '-(ym/cmath.sqrt(2))',
                  order = {'QED':1})

GC_102 = Coupling(name = 'GC_102',
                  value = 'ym/cmath.sqrt(2)',
                  order = {'QED':1})

GC_103 = Coupling(name = 'GC_103',
                  value = '-((ca*complex(0,1)*ym)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_104 = Coupling(name = 'GC_104',
                  value = '-((complex(0,1)*sa*ym)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_105 = Coupling(name = 'GC_105',
                  value = '-(complex(0,1)*ys)',
                  order = {'QED':1})

GC_106 = Coupling(name = 'GC_106',
                  value = '-(ys/cmath.sqrt(2))',
                  order = {'QED':1})

GC_107 = Coupling(name = 'GC_107',
                  value = 'ys/cmath.sqrt(2)',
                  order = {'QED':1})

GC_108 = Coupling(name = 'GC_108',
                  value = '-((ca*complex(0,1)*ys)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_109 = Coupling(name = 'GC_109',
                  value = '-((complex(0,1)*sa*ys)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_110 = Coupling(name = 'GC_110',
                  value = 'complex(0,1)*yt',
                  order = {'QED':1})

GC_111 = Coupling(name = 'GC_111',
                  value = '-(yt/cmath.sqrt(2))',
                  order = {'QED':1})

GC_112 = Coupling(name = 'GC_112',
                  value = 'yt/cmath.sqrt(2)',
                  order = {'QED':1})

GC_113 = Coupling(name = 'GC_113',
                  value = '-((ca*complex(0,1)*yt)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_114 = Coupling(name = 'GC_114',
                  value = '-((complex(0,1)*sa*yt)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_115 = Coupling(name = 'GC_115',
                  value = '-(complex(0,1)*ytau)',
                  order = {'QED':1})

GC_116 = Coupling(name = 'GC_116',
                  value = '-(ytau/cmath.sqrt(2))',
                  order = {'QED':1})

GC_117 = Coupling(name = 'GC_117',
                  value = 'ytau/cmath.sqrt(2)',
                  order = {'QED':1})

GC_118 = Coupling(name = 'GC_118',
                  value = '-((ca*complex(0,1)*ytau)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_119 = Coupling(name = 'GC_119',
                  value = '-((complex(0,1)*sa*ytau)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_120 = Coupling(name = 'GC_120',
                  value = 'complex(0,1)*yup',
                  order = {'QED':1})

GC_121 = Coupling(name = 'GC_121',
                  value = '-(yup/cmath.sqrt(2))',
                  order = {'QED':1})

GC_122 = Coupling(name = 'GC_122',
                  value = 'yup/cmath.sqrt(2)',
                  order = {'QED':1})

GC_123 = Coupling(name = 'GC_123',
                  value = '-((ca*complex(0,1)*yup)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_124 = Coupling(name = 'GC_124',
                  value = '-((complex(0,1)*sa*yup)/cmath.sqrt(2))',
                  order = {'QED':1})

