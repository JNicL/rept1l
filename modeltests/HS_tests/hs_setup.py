###############################################################################
#                                 hs_setup.py                                 #
###############################################################################

##############
#  Includes  #
##############

import os

#######################
#  Globals & Methods  #
#######################

thispath = os.path.dirname(os.path.realpath(__file__))
driver_hs_path = os.path.join(thispath, 'HS_drivers')
driver_hs_bfm_path = os.path.join(thispath, 'HS_BFM_drivers')
