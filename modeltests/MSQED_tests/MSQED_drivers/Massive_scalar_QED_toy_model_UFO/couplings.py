# This file was automatically created by FeynRules 2.3.27
# Mathematica version: 11.1.0 for Linux x86 (64-bit) (March 13, 2017)
# Date: Wed 2 Aug 2017 15:28:39


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



GC_1 = Coupling(name = 'GC_1',
                value = 'ge',
                order = {'QUD':1})

GC_2 = Coupling(name = 'GC_2',
                value = '2*complex(0,1)*ge**2',
                order = {'QUD':2})

GC_3 = Coupling(name = 'GC_3',
                value = '-(complex(0,1)*lam)',
                order = {'QUD':2})

GC_4 = Coupling(name = 'GC_4',
                value = '-3*complex(0,1)*lam',
                order = {'QUD':2})

GC_5 = Coupling(name = 'GC_5',
                value = '-(complex(0,1)*ge*MZ)',
                order = {'QUD':1})

GC_6 = Coupling(name = 'GC_6',
                value = '2*complex(0,1)*ge**2*vev',
                order = {'QUD':1})

GC_7 = Coupling(name = 'GC_7',
                value = '-(complex(0,1)*lam*vev)',
                order = {'QUD':1})

GC_8 = Coupling(name = 'GC_8',
                value = '-3*complex(0,1)*lam*vev',
                order = {'QUD':1})

