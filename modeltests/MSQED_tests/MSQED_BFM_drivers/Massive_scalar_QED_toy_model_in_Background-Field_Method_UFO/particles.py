# This file was automatically created by FeynRules 2.3.27
# Mathematica version: 11.1.0 for Linux x86 (64-bit) (March 13, 2017)
# Date: Wed 2 Aug 2017 12:21:35


from __future__ import division
from object_library import all_particles, Particle
import parameters as Param

import propagators as Prop

Z = Particle(pdg_code = 23,
             name = 'Z',
             antiname = 'Z',
             spin = 3,
             color = 1,
             mass = Param.MZ,
             width = Param.WZ,
             texname = 'Z',
             antitexname = 'Z',
             charge = 0,
             GhostNumber = 0,
             Y = 0)

ZQ = Particle(pdg_code = 23,
              name = 'ZQ',
              antiname = 'ZQ',
              spin = 3,
              color = 1,
              mass = Param.MZ,
              width = Param.WZ,
              texname = 'ZQ',
              antitexname = 'ZQ',
              charge = 0,
              GhostNumber = 0,
              Y = 0)

ghZ = Particle(pdg_code = 9000001,
               name = 'ghZ',
               antiname = 'ghZ~',
               spin = -1,
               color = 1,
               mass = Param.MZ,
               width = Param.ZERO,
               texname = 'ghZ',
               antitexname = 'ghZ~',
               charge = 0,
               GhostNumber = 1,
               Y = 0)

ghZ__tilde__ = ghZ.anti()

H = Particle(pdg_code = 9000002,
             name = 'H',
             antiname = 'H',
             spin = 1,
             color = 1,
             mass = Param.MH,
             width = Param.WH,
             texname = 'H',
             antitexname = 'H',
             charge = 0,
             GhostNumber = 0,
             Y = 0)

HQ = Particle(pdg_code = 9000003,
              name = 'HQ',
              antiname = 'HQ',
              spin = 1,
              color = 1,
              mass = Param.MH,
              width = Param.WH,
              texname = 'HQ',
              antitexname = 'HQ',
              charge = 0,
              GhostNumber = 0,
              Y = 0)

G0 = Particle(pdg_code = 250,
              name = 'G0',
              antiname = 'G0',
              spin = 1,
              color = 1,
              mass = Param.MZ,
              width = Param.WZ,
              texname = 'G0',
              antitexname = 'G0',
              goldstone = True,
              charge = 0,
              GhostNumber = 0,
              Y = 0)

GQ0 = Particle(pdg_code = 250,
               name = 'GQ0',
               antiname = 'GQ0',
               spin = 1,
               color = 1,
               mass = Param.MZ,
               width = Param.WZ,
               texname = 'GQ0',
               antitexname = 'GQ0',
               goldstone = True,
               charge = 0,
               GhostNumber = 0,
               Y = 0)

