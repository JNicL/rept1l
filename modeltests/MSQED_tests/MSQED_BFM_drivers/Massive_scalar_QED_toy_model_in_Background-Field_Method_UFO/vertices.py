# This file was automatically created by FeynRules 2.3.27
# Mathematica version: 11.1.0 for Linux x86 (64-bit) (March 13, 2017)
# Date: Wed 2 Aug 2017 12:21:35


from object_library import all_vertices, Vertex
import particles as P
import couplings as C
import lorentz as L


V_1 = Vertex(name = 'V_1',
             particles = [ P.G0, P.G0, P.G0, P.G0 ],
             color = [ '1' ],
             lorentz = [ L.SSSS1 ],
             couplings = {(0,0):C.GC_9})

V_2 = Vertex(name = 'V_2',
             particles = [ P.G0, P.G0, P.G0, P.GQ0 ],
             color = [ '1' ],
             lorentz = [ L.SSSS1 ],
             couplings = {(0,0):C.GC_9})

V_3 = Vertex(name = 'V_3',
             particles = [ P.G0, P.G0, P.GQ0, P.GQ0 ],
             color = [ '1' ],
             lorentz = [ L.SSSS1 ],
             couplings = {(0,0):C.GC_9})

V_4 = Vertex(name = 'V_4',
             particles = [ P.G0, P.GQ0, P.GQ0, P.GQ0 ],
             color = [ '1' ],
             lorentz = [ L.SSSS1 ],
             couplings = {(0,0):C.GC_9})

V_5 = Vertex(name = 'V_5',
             particles = [ P.GQ0, P.GQ0, P.GQ0, P.GQ0 ],
             color = [ '1' ],
             lorentz = [ L.SSSS1 ],
             couplings = {(0,0):C.GC_9})

V_6 = Vertex(name = 'V_6',
             particles = [ P.G0, P.G0, P.H, P.H ],
             color = [ '1' ],
             lorentz = [ L.SSSS1 ],
             couplings = {(0,0):C.GC_8})

V_7 = Vertex(name = 'V_7',
             particles = [ P.G0, P.GQ0, P.H, P.H ],
             color = [ '1' ],
             lorentz = [ L.SSSS1 ],
             couplings = {(0,0):C.GC_8})

V_8 = Vertex(name = 'V_8',
             particles = [ P.GQ0, P.GQ0, P.H, P.H ],
             color = [ '1' ],
             lorentz = [ L.SSSS1 ],
             couplings = {(0,0):C.GC_7})

V_9 = Vertex(name = 'V_9',
             particles = [ P.H, P.H, P.H, P.H ],
             color = [ '1' ],
             lorentz = [ L.SSSS1 ],
             couplings = {(0,0):C.GC_9})

V_10 = Vertex(name = 'V_10',
              particles = [ P.G0, P.G0, P.H, P.HQ ],
              color = [ '1' ],
              lorentz = [ L.SSSS1 ],
              couplings = {(0,0):C.GC_8})

V_11 = Vertex(name = 'V_11',
              particles = [ P.G0, P.GQ0, P.H, P.HQ ],
              color = [ '1' ],
              lorentz = [ L.SSSS1 ],
              couplings = {(0,0):C.GC_6})

V_12 = Vertex(name = 'V_12',
              particles = [ P.GQ0, P.GQ0, P.H, P.HQ ],
              color = [ '1' ],
              lorentz = [ L.SSSS1 ],
              couplings = {(0,0):C.GC_8})

V_13 = Vertex(name = 'V_13',
              particles = [ P.H, P.H, P.H, P.HQ ],
              color = [ '1' ],
              lorentz = [ L.SSSS1 ],
              couplings = {(0,0):C.GC_9})

V_14 = Vertex(name = 'V_14',
              particles = [ P.G0, P.G0, P.HQ, P.HQ ],
              color = [ '1' ],
              lorentz = [ L.SSSS1 ],
              couplings = {(0,0):C.GC_7})

V_15 = Vertex(name = 'V_15',
              particles = [ P.G0, P.GQ0, P.HQ, P.HQ ],
              color = [ '1' ],
              lorentz = [ L.SSSS1 ],
              couplings = {(0,0):C.GC_8})

V_16 = Vertex(name = 'V_16',
              particles = [ P.GQ0, P.GQ0, P.HQ, P.HQ ],
              color = [ '1' ],
              lorentz = [ L.SSSS1 ],
              couplings = {(0,0):C.GC_8})

V_17 = Vertex(name = 'V_17',
              particles = [ P.H, P.H, P.HQ, P.HQ ],
              color = [ '1' ],
              lorentz = [ L.SSSS1 ],
              couplings = {(0,0):C.GC_9})

V_18 = Vertex(name = 'V_18',
              particles = [ P.H, P.HQ, P.HQ, P.HQ ],
              color = [ '1' ],
              lorentz = [ L.SSSS1 ],
              couplings = {(0,0):C.GC_9})

V_19 = Vertex(name = 'V_19',
              particles = [ P.HQ, P.HQ, P.HQ, P.HQ ],
              color = [ '1' ],
              lorentz = [ L.SSSS1 ],
              couplings = {(0,0):C.GC_9})

V_20 = Vertex(name = 'V_20',
              particles = [ P.G0, P.G0, P.H ],
              color = [ '1' ],
              lorentz = [ L.SSS1 ],
              couplings = {(0,0):C.GC_13})

V_21 = Vertex(name = 'V_21',
              particles = [ P.G0, P.GQ0, P.H ],
              color = [ '1' ],
              lorentz = [ L.SSS1 ],
              couplings = {(0,0):C.GC_13})

V_22 = Vertex(name = 'V_22',
              particles = [ P.GQ0, P.GQ0, P.H ],
              color = [ '1' ],
              lorentz = [ L.SSS1 ],
              couplings = {(0,0):C.GC_16})

V_23 = Vertex(name = 'V_23',
              particles = [ P.H, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.SSS1 ],
              couplings = {(0,0):C.GC_14})

V_24 = Vertex(name = 'V_24',
              particles = [ P.G0, P.G0, P.HQ ],
              color = [ '1' ],
              lorentz = [ L.SSS1 ],
              couplings = {(0,0):C.GC_13})

V_25 = Vertex(name = 'V_25',
              particles = [ P.G0, P.GQ0, P.HQ ],
              color = [ '1' ],
              lorentz = [ L.SSS1 ],
              couplings = {(0,0):C.GC_15})

V_26 = Vertex(name = 'V_26',
              particles = [ P.GQ0, P.GQ0, P.HQ ],
              color = [ '1' ],
              lorentz = [ L.SSS1 ],
              couplings = {(0,0):C.GC_13})

V_27 = Vertex(name = 'V_27',
              particles = [ P.H, P.H, P.HQ ],
              color = [ '1' ],
              lorentz = [ L.SSS1 ],
              couplings = {(0,0):C.GC_14})

V_28 = Vertex(name = 'V_28',
              particles = [ P.H, P.HQ, P.HQ ],
              color = [ '1' ],
              lorentz = [ L.SSS1 ],
              couplings = {(0,0):C.GC_14})

V_29 = Vertex(name = 'V_29',
              particles = [ P.HQ, P.HQ, P.HQ ],
              color = [ '1' ],
              lorentz = [ L.SSS1 ],
              couplings = {(0,0):C.GC_14})

V_30 = Vertex(name = 'V_30',
              particles = [ P.Z, P.G0, P.H ],
              color = [ '1' ],
              lorentz = [ L.VSS1 ],
              couplings = {(0,0):C.GC_2})

V_31 = Vertex(name = 'V_31',
              particles = [ P.Z, P.G0, P.HQ ],
              color = [ '1' ],
              lorentz = [ L.VSS1 ],
              couplings = {(0,0):C.GC_2})

V_32 = Vertex(name = 'V_32',
              particles = [ P.Z, P.GQ0, P.H ],
              color = [ '1' ],
              lorentz = [ L.VSS1 ],
              couplings = {(0,0):C.GC_2})

V_33 = Vertex(name = 'V_33',
              particles = [ P.Z, P.GQ0, P.HQ ],
              color = [ '1' ],
              lorentz = [ L.VSS1 ],
              couplings = {(0,0):C.GC_2})

V_34 = Vertex(name = 'V_34',
              particles = [ P.Z, P.Z, P.G0, P.G0 ],
              color = [ '1' ],
              lorentz = [ L.VVSS1 ],
              couplings = {(0,0):C.GC_5})

V_35 = Vertex(name = 'V_35',
              particles = [ P.Z, P.Z, P.G0, P.GQ0 ],
              color = [ '1' ],
              lorentz = [ L.VVSS1 ],
              couplings = {(0,0):C.GC_5})

V_36 = Vertex(name = 'V_36',
              particles = [ P.Z, P.Z, P.GQ0, P.GQ0 ],
              color = [ '1' ],
              lorentz = [ L.VVSS1 ],
              couplings = {(0,0):C.GC_5})

V_37 = Vertex(name = 'V_37',
              particles = [ P.Z, P.Z, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVSS1 ],
              couplings = {(0,0):C.GC_5})

V_38 = Vertex(name = 'V_38',
              particles = [ P.Z, P.Z, P.H, P.HQ ],
              color = [ '1' ],
              lorentz = [ L.VVSS1 ],
              couplings = {(0,0):C.GC_5})

V_39 = Vertex(name = 'V_39',
              particles = [ P.Z, P.Z, P.HQ, P.HQ ],
              color = [ '1' ],
              lorentz = [ L.VVSS1 ],
              couplings = {(0,0):C.GC_5})

V_40 = Vertex(name = 'V_40',
              particles = [ P.Z, P.Z, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVS1 ],
              couplings = {(0,0):C.GC_12})

V_41 = Vertex(name = 'V_41',
              particles = [ P.Z, P.Z, P.HQ ],
              color = [ '1' ],
              lorentz = [ L.VVS1 ],
              couplings = {(0,0):C.GC_12})

V_42 = Vertex(name = 'V_42',
              particles = [ P.ZQ, P.G0, P.H ],
              color = [ '1' ],
              lorentz = [ L.VSS1 ],
              couplings = {(0,0):C.GC_2})

V_43 = Vertex(name = 'V_43',
              particles = [ P.ZQ, P.G0, P.HQ ],
              color = [ '1' ],
              lorentz = [ L.VSS3 ],
              couplings = {(0,0):C.GC_1})

V_44 = Vertex(name = 'V_44',
              particles = [ P.ZQ, P.GQ0, P.H ],
              color = [ '1' ],
              lorentz = [ L.VSS2 ],
              couplings = {(0,0):C.GC_2})

V_45 = Vertex(name = 'V_45',
              particles = [ P.ZQ, P.GQ0, P.HQ ],
              color = [ '1' ],
              lorentz = [ L.VSS1 ],
              couplings = {(0,0):C.GC_2})

V_46 = Vertex(name = 'V_46',
              particles = [ P.Z, P.ZQ, P.G0, P.G0 ],
              color = [ '1' ],
              lorentz = [ L.VVSS1 ],
              couplings = {(0,0):C.GC_5})

V_47 = Vertex(name = 'V_47',
              particles = [ P.Z, P.ZQ, P.G0, P.GQ0 ],
              color = [ '1' ],
              lorentz = [ L.VVSS1 ],
              couplings = {(0,0):C.GC_5})

V_48 = Vertex(name = 'V_48',
              particles = [ P.Z, P.ZQ, P.GQ0, P.GQ0 ],
              color = [ '1' ],
              lorentz = [ L.VVSS1 ],
              couplings = {(0,0):C.GC_5})

V_49 = Vertex(name = 'V_49',
              particles = [ P.Z, P.ZQ, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVSS1 ],
              couplings = {(0,0):C.GC_5})

V_50 = Vertex(name = 'V_50',
              particles = [ P.Z, P.ZQ, P.H, P.HQ ],
              color = [ '1' ],
              lorentz = [ L.VVSS1 ],
              couplings = {(0,0):C.GC_5})

V_51 = Vertex(name = 'V_51',
              particles = [ P.Z, P.ZQ, P.HQ, P.HQ ],
              color = [ '1' ],
              lorentz = [ L.VVSS1 ],
              couplings = {(0,0):C.GC_5})

V_52 = Vertex(name = 'V_52',
              particles = [ P.Z, P.ZQ, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVS1 ],
              couplings = {(0,0):C.GC_12})

V_53 = Vertex(name = 'V_53',
              particles = [ P.Z, P.ZQ, P.HQ ],
              color = [ '1' ],
              lorentz = [ L.VVS1 ],
              couplings = {(0,0):C.GC_12})

V_54 = Vertex(name = 'V_54',
              particles = [ P.ZQ, P.ZQ, P.G0, P.G0 ],
              color = [ '1' ],
              lorentz = [ L.VVSS1 ],
              couplings = {(0,0):C.GC_5})

V_55 = Vertex(name = 'V_55',
              particles = [ P.ZQ, P.ZQ, P.G0, P.GQ0 ],
              color = [ '1' ],
              lorentz = [ L.VVSS1 ],
              couplings = {(0,0):C.GC_5})

V_56 = Vertex(name = 'V_56',
              particles = [ P.ZQ, P.ZQ, P.GQ0, P.GQ0 ],
              color = [ '1' ],
              lorentz = [ L.VVSS1 ],
              couplings = {(0,0):C.GC_5})

V_57 = Vertex(name = 'V_57',
              particles = [ P.ZQ, P.ZQ, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVSS1 ],
              couplings = {(0,0):C.GC_5})

V_58 = Vertex(name = 'V_58',
              particles = [ P.ZQ, P.ZQ, P.H, P.HQ ],
              color = [ '1' ],
              lorentz = [ L.VVSS1 ],
              couplings = {(0,0):C.GC_5})

V_59 = Vertex(name = 'V_59',
              particles = [ P.ZQ, P.ZQ, P.HQ, P.HQ ],
              color = [ '1' ],
              lorentz = [ L.VVSS1 ],
              couplings = {(0,0):C.GC_5})

V_60 = Vertex(name = 'V_60',
              particles = [ P.ZQ, P.ZQ, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVS1 ],
              couplings = {(0,0):C.GC_12})

V_61 = Vertex(name = 'V_61',
              particles = [ P.ZQ, P.ZQ, P.HQ ],
              color = [ '1' ],
              lorentz = [ L.VVS1 ],
              couplings = {(0,0):C.GC_12})

V_62 = Vertex(name = 'V_62',
              particles = [ P.ghZ, P.ghZ__tilde__, P.G0, P.G0 ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_4})

V_63 = Vertex(name = 'V_63',
              particles = [ P.ghZ, P.ghZ__tilde__, P.G0, P.GQ0 ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_3})

V_64 = Vertex(name = 'V_64',
              particles = [ P.ghZ, P.ghZ__tilde__, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_4})

V_65 = Vertex(name = 'V_65',
              particles = [ P.ghZ, P.ghZ__tilde__, P.H, P.HQ ],
              color = [ '1' ],
              lorentz = [ L.UUSS1 ],
              couplings = {(0,0):C.GC_3})

V_66 = Vertex(name = 'V_66',
              particles = [ P.ghZ, P.ghZ__tilde__, P.H ],
              color = [ '1' ],
              lorentz = [ L.UUS1 ],
              couplings = {(0,0):C.GC_11})

V_67 = Vertex(name = 'V_67',
              particles = [ P.ghZ, P.ghZ__tilde__, P.HQ ],
              color = [ '1' ],
              lorentz = [ L.UUS1 ],
              couplings = {(0,0):C.GC_10})

