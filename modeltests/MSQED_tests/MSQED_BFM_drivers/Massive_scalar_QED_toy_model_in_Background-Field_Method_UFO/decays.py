# This file was automatically created by FeynRules 2.3.27
# Mathematica version: 11.1.0 for Linux x86 (64-bit) (March 13, 2017)
# Date: Wed 2 Aug 2017 12:21:35


from object_library import all_decays, Decay
import particles as P


Decay_H = Decay(name = 'Decay_H',
                particle = P.H,
                partial_widths = {(P.Z,P.Z):'((12*ge**4*vev**2 + (ge**4*MH**4*vev**2)/MZ**4 - (4*ge**4*MH**2*vev**2)/MZ**2)*cmath.sqrt(MH**4 - 4*MH**2*MZ**2))/(32.*cmath.pi*abs(MH)**3)',
                                  (P.Z,P.ZQ):'((12*ge**4*vev**2 + (ge**4*MH**4*vev**2)/MZ**4 - (4*ge**4*MH**2*vev**2)/MZ**2)*cmath.sqrt(MH**4 - 4*MH**2*MZ**2))/(16.*cmath.pi*abs(MH)**3)',
                                  (P.ZQ,P.ZQ):'((12*ge**4*vev**2 + (ge**4*MH**4*vev**2)/MZ**4 - (4*ge**4*MH**2*vev**2)/MZ**2)*cmath.sqrt(MH**4 - 4*MH**2*MZ**2))/(32.*cmath.pi*abs(MH)**3)'})

Decay_HQ = Decay(name = 'Decay_HQ',
                 particle = P.HQ,
                 partial_widths = {(P.Z,P.Z):'((12*ge**4*vev**2 + (ge**4*MH**4*vev**2)/MZ**4 - (4*ge**4*MH**2*vev**2)/MZ**2)*cmath.sqrt(MH**4 - 4*MH**2*MZ**2))/(32.*cmath.pi*abs(MH)**3)',
                                   (P.Z,P.ZQ):'((12*ge**4*vev**2 + (ge**4*MH**4*vev**2)/MZ**4 - (4*ge**4*MH**2*vev**2)/MZ**2)*cmath.sqrt(MH**4 - 4*MH**2*MZ**2))/(16.*cmath.pi*abs(MH)**3)',
                                   (P.ZQ,P.ZQ):'((12*ge**4*vev**2 + (ge**4*MH**4*vev**2)/MZ**4 - (4*ge**4*MH**2*vev**2)/MZ**2)*cmath.sqrt(MH**4 - 4*MH**2*MZ**2))/(32.*cmath.pi*abs(MH)**3)'})

