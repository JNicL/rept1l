# This file was automatically created by FeynRules 2.3.27
# Mathematica version: 11.1.0 for Linux x86 (64-bit) (March 13, 2017)
# Date: Wed 2 Aug 2017 12:21:35


from object_library import all_orders, CouplingOrder


QUD = CouplingOrder(name = 'QUD',
                    expansion_order = 99,
                    hierarchy = 2)

