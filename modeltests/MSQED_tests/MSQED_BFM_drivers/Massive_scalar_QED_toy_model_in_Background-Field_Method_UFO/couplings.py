# This file was automatically created by FeynRules 2.3.27
# Mathematica version: 11.1.0 for Linux x86 (64-bit) (March 13, 2017)
# Date: Wed 2 Aug 2017 12:21:35


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



GC_1 = Coupling(name = 'GC_1',
                value = '-ge',
                order = {'QUD':1})

GC_2 = Coupling(name = 'GC_2',
                value = 'ge',
                order = {'QUD':1})

GC_3 = Coupling(name = 'GC_3',
                value = '-(complex(0,1)*ge**2)',
                order = {'QUD':2})

GC_4 = Coupling(name = 'GC_4',
                value = '-2*complex(0,1)*ge**2',
                order = {'QUD':2})

GC_5 = Coupling(name = 'GC_5',
                value = '2*complex(0,1)*ge**2',
                order = {'QUD':2})

GC_6 = Coupling(name = 'GC_6',
                value = 'complex(0,1)*ge**2 - complex(0,1)*lam',
                order = {'QUD':2})

GC_7 = Coupling(name = 'GC_7',
                value = '-2*complex(0,1)*ge**2 - complex(0,1)*lam',
                order = {'QUD':2})

GC_8 = Coupling(name = 'GC_8',
                value = '-(complex(0,1)*lam)',
                order = {'QUD':2})

GC_9 = Coupling(name = 'GC_9',
                value = '-3*complex(0,1)*lam',
                order = {'QUD':2})

GC_10 = Coupling(name = 'GC_10',
                 value = '-(complex(0,1)*ge**2*vev)',
                 order = {'QUD':1})

GC_11 = Coupling(name = 'GC_11',
                 value = '-2*complex(0,1)*ge**2*vev',
                 order = {'QUD':1})

GC_12 = Coupling(name = 'GC_12',
                 value = '2*complex(0,1)*ge**2*vev',
                 order = {'QUD':1})

GC_13 = Coupling(name = 'GC_13',
                 value = '-(complex(0,1)*lam*vev)',
                 order = {'QUD':1})

GC_14 = Coupling(name = 'GC_14',
                 value = '-3*complex(0,1)*lam*vev',
                 order = {'QUD':1})

GC_15 = Coupling(name = 'GC_15',
                 value = 'complex(0,1)*ge**2*vev - complex(0,1)*lam*vev',
                 order = {'QUD':1})

GC_16 = Coupling(name = 'GC_16',
                 value = '-2*complex(0,1)*ge**2*vev - complex(0,1)*lam*vev',
                 order = {'QUD':1})

