# This file was automatically created by FeynRules 2.3.27
# Mathematica version: 11.1.0 for Linux x86 (64-bit) (March 13, 2017)
# Date: Wed 2 Aug 2017 12:21:35



from object_library import all_parameters, Parameter


from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot

# This is a default parameter object representing 0.
ZERO = Parameter(name = 'ZERO',
                 nature = 'internal',
                 type = 'real',
                 value = '0.0',
                 texname = '0')

# User-defined parameters.
ge = Parameter(name = 'ge',
               nature = 'external',
               type = 'real',
               value = 0.01,
               texname = 'g_e',
               lhablock = 'INPUTS',
               lhacode = [ 1 ])

MZ = Parameter(name = 'MZ',
               nature = 'external',
               type = 'real',
               value = 91.1876,
               texname = '\\text{MZ}',
               lhablock = 'MASS',
               lhacode = [ 23 ])

MH = Parameter(name = 'MH',
               nature = 'external',
               type = 'real',
               value = 120,
               texname = '\\text{MH}',
               lhablock = 'MASS',
               lhacode = [ 9000003 ])

WZ = Parameter(name = 'WZ',
               nature = 'external',
               type = 'real',
               value = 2.4952,
               texname = '\\text{WZ}',
               lhablock = 'DECAY',
               lhacode = [ 23 ])

WZ = Parameter(name = 'WZ',
               nature = 'external',
               type = 'real',
               value = 2.4952,
               texname = '\\text{WZ}',
               lhablock = 'DECAY',
               lhacode = [ 23 ])

WH = Parameter(name = 'WH',
               nature = 'external',
               type = 'real',
               value = 0.2,
               texname = '\\text{WH}',
               lhablock = 'DECAY',
               lhacode = [ 9000002 ])

WH = Parameter(name = 'WH',
               nature = 'external',
               type = 'real',
               value = 0.2,
               texname = '\\text{WH}',
               lhablock = 'DECAY',
               lhacode = [ 9000003 ])

vev = Parameter(name = 'vev',
                nature = 'internal',
                type = 'real',
                value = 'MZ/ge',
                texname = '\\text{vev}')

lam = Parameter(name = 'lam',
                nature = 'internal',
                type = 'real',
                value = 'MH**2/vev**2',
                texname = '\\text{lam}')

mu2 = Parameter(name = 'mu2',
                nature = 'internal',
                type = 'real',
                value = '-(lam*vev**2)/2.',
                texname = '\\text{mu2}')

