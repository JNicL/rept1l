#==============================================================================#
#                                   model.py                                   #
#==============================================================================#

#============#
#  Includes  #
#============#

from rept1l.autoct.modelfile import model
from rept1l.autoct.modelfile import TreeInducedCTVertex
from rept1l.autoct.auto_parameters_ct import auto_assign_ct_particle
from rept1l.autoct.auto_parameters_ct import assign_counterterm
from rept1l.autoct.auto_parameters_ct import add_counterterm
from rept1l.autoct.auto_tadpole_ct import auto_tadpole_vertices
from rept1l.autoct.auto_tadpole_ct import assign_FJTadpole_field_shift

#===========#
#  Globals  #
#===========#

# coupling constructor
Coupling = model.object_library.Coupling

# access to instances
param = model.parameters
P = model.particles

# LCT allows to access the 2-point lorentz structures defined in lorentz_ct.py
import rept1l.autoct.lorentz_ct as LCT

modelname = 'MSQED'
modelgauge = "'t Hooft Feynman BFM"

#=============================#
#  External Parameter Orders  #
#=============================#

parameter_orders = {param.ge: {'QUD': 1},
                    param.vev: {'QUD': -1},
                    }

#==============================================================================#
#                           coupling renormalization                           #
#==============================================================================#

assign_counterterm(param.ge, 'dZge', 'ge*dZge')


#============================================#
#  Declare quantumfield & background fields  #
#============================================#

P.Z.backgroundfield = True
P.ZQ.quantumfield = True
P.H.backgroundfield = True
P.HQ.quantumfield = True
P.G0.backgroundfield = True
P.GQ0.quantumfield = True

#==============================#
#  Declare tadpole parameters  #
#==============================#

tadpole_parameter = []
assign_FJTadpole_field_shift(P.H, 'dt')
auto_tadpole_vertices()

#==============================================================================#
#                                  particles                                   #
#==============================================================================#

auto_assign_ct_particle(assign_goldstone_wf=True)

##################################
#  Gauge-fixing renormalization  #
##################################

GC_MZ_GF = Coupling(name='GC_MZ_GF',
                    value='MZ',
                    order={'QUD': 0})

# Note the sign difference (VS2 instead of VS1) to SM in Denner conventions
V_ZG0 = TreeInducedCTVertex(name='V_ZG0',
                            particles=[P.Z, P.G0],
                            color=['1'],
                            type='treeinducedct',
                            loop_particles='N',
                            lorentz=[LCT.VS2],
                            couplings={(0, 0): GC_MZ_GF})
