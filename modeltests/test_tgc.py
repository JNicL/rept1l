# -*- coding: utf-8 -*-
#==============================================================================#
#                                 test_tgc.py                                  #
#==============================================================================#
##############
#  Includes  #
##############

import os
import rept1l.modeltests.modeltest_utils.setup_models as sm
import rept1l.modeltests.TGC_tests.tgc_setup as tgc
from rept1l.modeltests.modeltest_utils.auto_model import GenModel, GenBareModel

#############
#  Methods  #
#############

class GenTGC(GenModel):
  threads = 8
  ct_expansion = True
  pre_ct = 'tgcexp.txt'
  verbose = True

  def renormalize_model(self, **kwargs):

    # QCD charged fields
    self.renormalize_qcd('-o', 'particles', '-qcd', '0', '2', '-qed', '0',
                         '-lam', '0', **kwargs)

    # QCD coupling
    for i in ['6', '5', '4', '3']:
      self.renormalize_qcd('-o', 'alphaS', '-nf', i, '-qcd', '0', '2', '3',
                           '-qed', '0', '-lam', '0', **kwargs)

    self.register_ct()

  def compute_rational(self, **kwargs):
    cls = self.__class__
    self.run_r2(vertices_file='tgcr2.txt', nc=True, threads=cls.threads,
                orders={'qcd': [0, 1, 2, 3, 4], 'qed': [0, 1, 2, 3],
                        'lam': [0]})

#------------------------------------------------------------------------------#

def full_tgc_charged_model(outputpath):
  GenTGC.driverpath = tgc.driver_tgc_path
  GenTGC.outputpath = outputpath
  GenTGC()

#------------------------------------------------------------------------------#

def full_tgc_cpeven_model(outputpath):
  GenTGC.driverpath = tgc.driver_tgc_cpeven_path
  GenTGC.outputpath = outputpath
  GenTGC()

#------------------------------------------------------------------------------#

def full_tgc_neutral_model(outputpath):
  GenTGC.driverpath = tgc.driver_ntgc_path
  GenTGC.outputpath = outputpath
  GenTGC()

#------------------------------------------------------------------------------#

def full_tgc_model(outputpath):
  GenTGC.driverpath = tgc.driver_tgc_full_path
  GenTGC.outputpath = outputpath
  GenTGC()

#------------------------------------------------------------------------------#

def full_TGC_WA_test():

  class TGCWA(GenTGC):

    final_ct = 'tgc_WA_ct.txt'
    driverpath = tgc.driver_tgc_cpeven_path

    def compute_rational(self, **kwargs):
      cls = self.__class__
      self.run_r2(vertices_file='tgc_WA_ct.txt', nc=True, threads=cls.threads,
                  orders={'qcd': [0, 1, 2, 3, 4], 'qed': [0, 1, 2, 3],
                          'lam': [0]})

    def gen_renormalized_model(self, rmpath):
      rmpath = os.path.join(self._reptil_mpath, 'renormalized_modelfile')
      super(TGCWA, self).gen_renormalized_model(rmpath)

    def run_test(self):
      cls = self.__class__
      self.compile_model(threads=cls.threads)
      from rept1l.modeltests.TGC_tests.test_tgc_WA import test_tgc_WA
      test_tgc_WA()

  TGCWA()

#------------------------------------------------------------------------------#

def full_TGC_WW_test():

  class TGCWW(GenTGC):

    driverpath = tgc.driver_tgc_path

    def gen_renormalized_model(self, rmpath):
      rmpath = os.path.join(self._reptil_mpath, 'renormalized_modelfile')
      super(TGCWW, self).gen_renormalized_model(rmpath)

    def run_test(self):
      cls = self.__class__
      self.compile_model(threads=cls.threads)
      from rept1l.modeltests.TGC_tests.test_tgc_WW import test_tgc_WW
      test_tgc_WW()

#------------------------------------------------------------------------------#

def full_NTGC_test():

  class NTGC(GenTGC):

    driverpath = tgc.driver_tgc_full_path

    def gen_renormalized_model(self, rmpath):
      rmpath = os.path.join(self._reptil_mpath, 'renormalized_modelfile')
      super(NTGC, self).gen_renormalized_model(rmpath)

    def run_test(self):
      cls = self.__class__
      self.compile_model(threads=cls.threads)
      from rept1l.modeltests.TGC_tests.test_ntgc import test_ntgc
      test_ntgc()

#------------------------------------------------------------------------------#

def pureD4_TGC_WA_test():

  class D4TGCWA(GenBareModel):
    driverpath = tgc.driver_tgc_cpeven_path

    def run_bare_test(self):
      from rept1l.modeltests.TGC_tests.test_tgc_WA import test_tgc_WA
      test_tgc_WA(only_D4=True)

  D4TGCWA()

#------------------------------------------------------------------------------#

def pureD4_TGC_WW_test():

  class D4TGCWW(GenBareModel):
    driverpath = tgc.driver_tgc_path

    def run_bare_test(self):
      from rept1l.modeltests.TGC_tests.test_tgc_WW import test_tgc_WW
      test_tgc_WW(only_D4=True)

  D4TGCWW()


#------------------------------------------------------------------------------#

def pureD4_NTGC_test():

  class D4NTGC(GenBareModel):
    driverpath = tgc.driver_tgc_full_path

    def run_bare_test(self):
      from rept1l.modeltests.TGC_tests.test_ntgc import test_ntgc
      test_ntgc(only_D4=True)

  D4NTGC()

#------------------------------------------------------------------------------#

if __name__ == '__main__':
  outputpath = '/home/nick/TGC_RT'
  pureD4_TGC_WW_test()
