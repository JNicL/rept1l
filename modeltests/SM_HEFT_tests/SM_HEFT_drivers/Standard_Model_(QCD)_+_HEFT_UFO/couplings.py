# This file was automatically created by FeynRules 2.3.29
# Mathematica version: 11.3.0 for Linux x86 (64-bit) (March 7, 2018)
# Date: Thu 21 Mar 2019 22:06:31


from .object_library import all_couplings, Coupling

from .function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



GC_1 = Coupling(name = 'GC_1',
                value = '-gs',
                order = {'QCD':1})

GC_2 = Coupling(name = 'GC_2',
                value = 'complex(0,1)*gs',
                order = {'QCD':1})

GC_3 = Coupling(name = 'GC_3',
                value = 'complex(0,1)*gs**2',
                order = {'QCD':2})

GC_4 = Coupling(name = 'GC_4',
                value = '(-3*complex(0,1)*lam)/2.',
                order = {'QED':2})

GC_5 = Coupling(name = 'GC_5',
                value = '(8*cggh*complex(0,1))/vev**3',
                order = {'QCD':2,'QED':3})

GC_6 = Coupling(name = 'GC_6',
                value = '(8*cggh*gs)/vev**3',
                order = {'QCD':3,'QED':3})

GC_7 = Coupling(name = 'GC_7',
                value = '(-8*cggh*complex(0,1)*gs**2)/vev**3',
                order = {'QCD':4,'QED':3})

GC_8 = Coupling(name = 'GC_8',
                value = '(-4*cggh*complex(0,1))/vev**2',
                order = {'QCD':2,'QED':2})

GC_9 = Coupling(name = 'GC_9',
                value = '(-4*cggh*gs)/vev**2',
                order = {'QCD':3,'QED':2})

GC_10 = Coupling(name = 'GC_10',
                 value = '(4*cggh*complex(0,1)*gs**2)/vev**2',
                 order = {'QCD':4,'QED':2})

GC_11 = Coupling(name = 'GC_11',
                 value = '(4*cggh*complex(0,1))/vev',
                 order = {'QCD':2,'QED':1})

GC_12 = Coupling(name = 'GC_12',
                 value = '(4*cggh*gs)/vev',
                 order = {'QCD':3,'QED':1})

GC_13 = Coupling(name = 'GC_13',
                 value = '(-4*cggh*complex(0,1)*gs**2)/vev',
                 order = {'QCD':4,'QED':1})

GC_14 = Coupling(name = 'GC_14',
                 value = '(-3*complex(0,1)*lam*vev)/2.',
                 order = {'QED':1})

