# This file was automatically created by FeynRules 2.3.29
# Mathematica version: 11.3.0 for Linux x86 (64-bit) (March 7, 2018)
# Date: Thu 21 Mar 2019 22:06:31


from .object_library import all_vertices, Vertex
from . import particles as P
from . import couplings as C
from . import lorentz as L


V_1 = Vertex(name = 'V_1',
             particles = [ P.H, P.H, P.H, P.H ],
             color = [ '1' ],
             lorentz = [ L.SSSS1 ],
             couplings = {(0,0):C.GC_4})

V_2 = Vertex(name = 'V_2',
             particles = [ P.H, P.H, P.H ],
             color = [ '1' ],
             lorentz = [ L.SSS1 ],
             couplings = {(0,0):C.GC_14})

V_3 = Vertex(name = 'V_3',
             particles = [ P.g, P.g, P.H, P.H, P.H ],
             color = [ 'Identity(1,2)' ],
             lorentz = [ L.VVSSS1 ],
             couplings = {(0,0):C.GC_5})

V_4 = Vertex(name = 'V_4',
             particles = [ P.g, P.g, P.H, P.H ],
             color = [ 'Identity(1,2)' ],
             lorentz = [ L.VVSS1 ],
             couplings = {(0,0):C.GC_8})

V_5 = Vertex(name = 'V_5',
             particles = [ P.g, P.g, P.H ],
             color = [ 'Identity(1,2)' ],
             lorentz = [ L.VVS1 ],
             couplings = {(0,0):C.GC_11})

V_6 = Vertex(name = 'V_6',
             particles = [ P.ghG, P.ghG__tilde__, P.g ],
             color = [ 'f(1,2,3)' ],
             lorentz = [ L.UUV1 ],
             couplings = {(0,0):C.GC_1})

V_7 = Vertex(name = 'V_7',
             particles = [ P.g, P.g, P.g ],
             color = [ 'f(1,2,3)' ],
             lorentz = [ L.VVV1 ],
             couplings = {(0,0):C.GC_1})

V_8 = Vertex(name = 'V_8',
             particles = [ P.g, P.g, P.g, P.g ],
             color = [ 'f(-1,1,2)*f(3,4,-1)', 'f(-1,1,3)*f(2,4,-1)', 'f(-1,1,4)*f(2,3,-1)' ],
             lorentz = [ L.VVVV1, L.VVVV2, L.VVVV3 ],
             couplings = {(1,1):C.GC_3,(0,0):C.GC_3,(2,2):C.GC_3})

V_9 = Vertex(name = 'V_9',
             particles = [ P.g, P.g, P.g, P.H, P.H, P.H ],
             color = [ 'f(1,2,3)' ],
             lorentz = [ L.VVVSSS1 ],
             couplings = {(0,0):C.GC_6})

V_10 = Vertex(name = 'V_10',
              particles = [ P.g, P.g, P.g, P.H, P.H ],
              color = [ 'f(1,2,3)' ],
              lorentz = [ L.VVVSS1 ],
              couplings = {(0,0):C.GC_9})

V_11 = Vertex(name = 'V_11',
              particles = [ P.g, P.g, P.g, P.H ],
              color = [ 'f(1,2,3)' ],
              lorentz = [ L.VVVS1 ],
              couplings = {(0,0):C.GC_12})

V_12 = Vertex(name = 'V_12',
              particles = [ P.g, P.g, P.g, P.g, P.H, P.H, P.H ],
              color = [ 'f(-1,1,2)*f(3,4,-1)', 'f(-1,1,3)*f(2,4,-1)', 'f(-1,1,4)*f(2,3,-1)' ],
              lorentz = [ L.VVVVSSS1, L.VVVVSSS2, L.VVVVSSS3 ],
              couplings = {(1,1):C.GC_7,(0,0):C.GC_7,(2,2):C.GC_7})

V_13 = Vertex(name = 'V_13',
              particles = [ P.g, P.g, P.g, P.g, P.H, P.H ],
              color = [ 'f(-1,1,2)*f(3,4,-1)', 'f(-1,1,3)*f(2,4,-1)', 'f(-1,1,4)*f(2,3,-1)' ],
              lorentz = [ L.VVVVSS1, L.VVVVSS2, L.VVVVSS3 ],
              couplings = {(1,1):C.GC_10,(0,0):C.GC_10,(2,2):C.GC_10})

V_14 = Vertex(name = 'V_14',
              particles = [ P.g, P.g, P.g, P.g, P.H ],
              color = [ 'f(-1,1,2)*f(3,4,-1)', 'f(-1,1,3)*f(2,4,-1)', 'f(-1,1,4)*f(2,3,-1)' ],
              lorentz = [ L.VVVVS1, L.VVVVS2, L.VVVVS3 ],
              couplings = {(1,1):C.GC_13,(0,0):C.GC_13,(2,2):C.GC_13})

V_15 = Vertex(name = 'V_15',
              particles = [ P.u__tilde__, P.u, P.g ],
              color = [ 'T(3,2,1)' ],
              lorentz = [ L.FFV1 ],
              couplings = {(0,0):C.GC_2})

V_16 = Vertex(name = 'V_16',
              particles = [ P.c__tilde__, P.c, P.g ],
              color = [ 'T(3,2,1)' ],
              lorentz = [ L.FFV1 ],
              couplings = {(0,0):C.GC_2})

# V_17 = Vertex(name = 'V_17',
#               particles = [ P.t__tilde__, P.t, P.g ],
#               color = [ 'T(3,2,1)' ],
#               lorentz = [ L.FFV1 ],
#               couplings = {(0,0):C.GC_2})

V_18 = Vertex(name = 'V_18',
              particles = [ P.d__tilde__, P.d, P.g ],
              color = [ 'T(3,2,1)' ],
              lorentz = [ L.FFV1 ],
              couplings = {(0,0):C.GC_2})

V_19 = Vertex(name = 'V_19',
              particles = [ P.s__tilde__, P.s, P.g ],
              color = [ 'T(3,2,1)' ],
              lorentz = [ L.FFV1 ],
              couplings = {(0,0):C.GC_2})

V_20 = Vertex(name = 'V_20',
              particles = [ P.b__tilde__, P.b, P.g ],
              color = [ 'T(3,2,1)' ],
              lorentz = [ L.FFV1 ],
              couplings = {(0,0):C.GC_2})

