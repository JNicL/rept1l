# This file was automatically created by FeynRules 2.3.29
# Mathematica version: 12.0.0 for Linux x86 (64-bit) (April 7, 2019)
# Date: Mon 3 Aug 2020 22:01:27


from .object_library import all_couplings, Coupling

from .function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



GC_1 = Coupling(name = 'GC_1',
                value = '-2*gs',
                order = {'QCD':1})

GC_2 = Coupling(name = 'GC_2',
                value = '-gs',
                order = {'QCD':1})

GC_3 = Coupling(name = 'GC_3',
                value = 'complex(0,1)*gs',
                order = {'QCD':1})

GC_4 = Coupling(name = 'GC_4',
                value = '-(complex(0,1)*gs**2)',
                order = {'QCD':2})

GC_5 = Coupling(name = 'GC_5',
                value = 'complex(0,1)*gs**2',
                order = {'QCD':2})

GC_6 = Coupling(name = 'GC_6',
                value = '(-3*complex(0,1)*lam)/2.',
                order = {'QED':2})

GC_7 = Coupling(name = 'GC_7',
                value = '(8*cggh*complex(0,1))/vev**3',
                order = {'QCD':2,'QED':3})

GC_8 = Coupling(name = 'GC_8',
                value = '(8*cggh*gs)/vev**3',
                order = {'QCD':3,'QED':3})

GC_9 = Coupling(name = 'GC_9',
                value = '(-8*cggh*complex(0,1)*gs**2)/vev**3',
                order = {'QCD':4,'QED':3})

GC_10 = Coupling(name = 'GC_10',
                 value = '(-4*cggh*complex(0,1))/vev**2',
                 order = {'QCD':2,'QED':2})

GC_11 = Coupling(name = 'GC_11',
                 value = '(-4*cggh*gs)/vev**2',
                 order = {'QCD':3,'QED':2})

GC_12 = Coupling(name = 'GC_12',
                 value = '(4*cggh*complex(0,1)*gs**2)/vev**2',
                 order = {'QCD':4,'QED':2})

GC_13 = Coupling(name = 'GC_13',
                 value = '(4*cggh*complex(0,1))/vev',
                 order = {'QCD':2,'QED':1})

GC_14 = Coupling(name = 'GC_14',
                 value = '(4*cggh*gs)/vev',
                 order = {'QCD':3,'QED':1})

GC_15 = Coupling(name = 'GC_15',
                 value = '(-4*cggh*complex(0,1)*gs**2)/vev',
                 order = {'QCD':4,'QED':1})

GC_16 = Coupling(name = 'GC_16',
                 value = '(-3*complex(0,1)*lam*vev)/2.',
                 order = {'QED':1})

