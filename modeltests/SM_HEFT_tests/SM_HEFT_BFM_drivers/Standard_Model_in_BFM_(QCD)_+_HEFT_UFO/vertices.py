# This file was automatically created by FeynRules 2.3.29
# Mathematica version: 12.0.0 for Linux x86 (64-bit) (April 7, 2019)
# Date: Mon 3 Aug 2020 22:01:27


from .object_library import all_vertices, Vertex
from . import particles as P
from . import couplings as C
from . import lorentz as L


V_1 = Vertex(name = 'V_1',
             particles = [ P.g, P.g, P.g ],
             color = [ 'f(1,2,3)' ],
             lorentz = [ L.VVV1 ],
             couplings = {(0,0):C.GC_2})

V_2 = Vertex(name = 'V_2',
             particles = [ P.g, P.g, P.gQ ],
             color = [ 'f(1,2,3)' ],
             lorentz = [ L.VVV1 ],
             couplings = {(0,0):C.GC_2})

V_3 = Vertex(name = 'V_3',
             particles = [ P.g, P.g, P.g, P.g ],
             color = [ 'f(-1,1,2)*f(3,4,-1)', 'f(-1,1,3)*f(2,4,-1)', 'f(-1,1,4)*f(2,3,-1)' ],
             lorentz = [ L.VVVV1, L.VVVV2, L.VVVV4 ],
             couplings = {(1,1):C.GC_5,(0,0):C.GC_5,(2,2):C.GC_5})

V_4 = Vertex(name = 'V_4',
             particles = [ P.g, P.gQ, P.gQ ],
             color = [ 'f(1,2,3)' ],
             lorentz = [ L.VVV2 ],
             couplings = {(0,0):C.GC_2})

V_5 = Vertex(name = 'V_5',
             particles = [ P.g, P.g, P.g, P.gQ ],
             color = [ 'f(-1,1,2)*f(3,4,-1)', 'f(-1,1,3)*f(2,4,-1)', 'f(-1,1,4)*f(2,3,-1)' ],
             lorentz = [ L.VVVV1, L.VVVV2, L.VVVV4 ],
             couplings = {(1,1):C.GC_5,(0,0):C.GC_5,(2,2):C.GC_5})

V_6 = Vertex(name = 'V_6',
             particles = [ P.g, P.g, P.gQ, P.gQ ],
             color = [ 'f(-1,1,2)*f(3,4,-1)', 'f(-1,1,3)*f(2,4,-1)', 'f(-1,1,4)*f(2,3,-1)' ],
             lorentz = [ L.VVVV1, L.VVVV3, L.VVVV5 ],
             couplings = {(2,2):C.GC_4,(1,1):C.GC_5,(0,0):C.GC_5})

V_7 = Vertex(name = 'V_7',
             particles = [ P.gQ, P.gQ, P.gQ ],
             color = [ 'f(1,2,3)' ],
             lorentz = [ L.VVV1 ],
             couplings = {(0,0):C.GC_2})

V_8 = Vertex(name = 'V_8',
             particles = [ P.g, P.gQ, P.gQ, P.gQ ],
             color = [ 'f(-1,1,2)*f(3,4,-1)', 'f(-1,1,3)*f(2,4,-1)', 'f(-1,1,4)*f(2,3,-1)' ],
             lorentz = [ L.VVVV1, L.VVVV2, L.VVVV4 ],
             couplings = {(1,1):C.GC_5,(0,0):C.GC_5,(2,2):C.GC_5})

V_9 = Vertex(name = 'V_9',
             particles = [ P.gQ, P.gQ, P.gQ, P.gQ ],
             color = [ 'f(-1,1,2)*f(3,4,-1)', 'f(-1,1,3)*f(2,4,-1)', 'f(-1,1,4)*f(2,3,-1)' ],
             lorentz = [ L.VVVV1, L.VVVV2, L.VVVV4 ],
             couplings = {(1,1):C.GC_5,(0,0):C.GC_5,(2,2):C.GC_5})

V_10 = Vertex(name = 'V_10',
              particles = [ P.H, P.H, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.SSSS1 ],
              couplings = {(0,0):C.GC_6})

V_11 = Vertex(name = 'V_11',
              particles = [ P.H, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.SSS1 ],
              couplings = {(0,0):C.GC_16})

V_12 = Vertex(name = 'V_12',
              particles = [ P.u__tilde__, P.u, P.g ],
              color = [ 'T(3,2,1)' ],
              lorentz = [ L.FFV1 ],
              couplings = {(0,0):C.GC_3})

V_13 = Vertex(name = 'V_13',
              particles = [ P.c__tilde__, P.c, P.g ],
              color = [ 'T(3,2,1)' ],
              lorentz = [ L.FFV1 ],
              couplings = {(0,0):C.GC_3})

# V_14 = Vertex(name = 'V_14',
#               particles = [ P.t__tilde__, P.t, P.g ],
#               color = [ 'T(3,2,1)' ],
#               lorentz = [ L.FFV1 ],
#               couplings = {(0,0):C.GC_3})

V_15 = Vertex(name = 'V_15',
              particles = [ P.d__tilde__, P.d, P.g ],
              color = [ 'T(3,2,1)' ],
              lorentz = [ L.FFV1 ],
              couplings = {(0,0):C.GC_3})

V_16 = Vertex(name = 'V_16',
              particles = [ P.s__tilde__, P.s, P.g ],
              color = [ 'T(3,2,1)' ],
              lorentz = [ L.FFV1 ],
              couplings = {(0,0):C.GC_3})

V_17 = Vertex(name = 'V_17',
              particles = [ P.b__tilde__, P.b, P.g ],
              color = [ 'T(3,2,1)' ],
              lorentz = [ L.FFV1 ],
              couplings = {(0,0):C.GC_3})

V_18 = Vertex(name = 'V_18',
              particles = [ P.u__tilde__, P.u, P.gQ ],
              color = [ 'T(3,2,1)' ],
              lorentz = [ L.FFV1 ],
              couplings = {(0,0):C.GC_3})

V_19 = Vertex(name = 'V_19',
              particles = [ P.c__tilde__, P.c, P.gQ ],
              color = [ 'T(3,2,1)' ],
              lorentz = [ L.FFV1 ],
              couplings = {(0,0):C.GC_3})

# V_20 = Vertex(name = 'V_20',
#               particles = [ P.t__tilde__, P.t, P.gQ ],
#               color = [ 'T(3,2,1)' ],
#               lorentz = [ L.FFV1 ],
#               couplings = {(0,0):C.GC_3})

V_21 = Vertex(name = 'V_21',
              particles = [ P.d__tilde__, P.d, P.gQ ],
              color = [ 'T(3,2,1)' ],
              lorentz = [ L.FFV1 ],
              couplings = {(0,0):C.GC_3})

V_22 = Vertex(name = 'V_22',
              particles = [ P.s__tilde__, P.s, P.gQ ],
              color = [ 'T(3,2,1)' ],
              lorentz = [ L.FFV1 ],
              couplings = {(0,0):C.GC_3})

V_23 = Vertex(name = 'V_23',
              particles = [ P.b__tilde__, P.b, P.gQ ],
              color = [ 'T(3,2,1)' ],
              lorentz = [ L.FFV1 ],
              couplings = {(0,0):C.GC_3})

V_24 = Vertex(name = 'V_24',
              particles = [ P.ghG, P.ghG__tilde__, P.g ],
              color = [ 'f(1,2,3)' ],
              lorentz = [ L.UUV1 ],
              couplings = {(0,0):C.GC_1})

V_25 = Vertex(name = 'V_25',
              particles = [ P.ghG, P.ghG__tilde__, P.gQ ],
              color = [ 'f(1,2,3)' ],
              lorentz = [ L.UUV2 ],
              couplings = {(0,0):C.GC_2})

V_26 = Vertex(name = 'V_26',
              particles = [ P.ghG, P.ghG__tilde__, P.g, P.g ],
              color = [ 'f(-1,1,3)*f(2,4,-1)', 'f(-1,1,4)*f(2,3,-1)' ],
              lorentz = [ L.UUVV1 ],
              couplings = {(1,0):C.GC_5,(0,0):C.GC_5})

V_27 = Vertex(name = 'V_27',
              particles = [ P.ghG, P.ghG__tilde__, P.g, P.gQ ],
              color = [ 'f(-1,1,3)*f(2,4,-1)' ],
              lorentz = [ L.UUVV1 ],
              couplings = {(0,0):C.GC_5})

V_28 = Vertex(name = 'V_28',
              particles = [ P.g, P.g, P.H, P.H, P.H ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.VVSSS1 ],
              couplings = {(0,0):C.GC_7})

V_29 = Vertex(name = 'V_29',
              particles = [ P.g, P.g, P.H, P.H ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.VVSS1 ],
              couplings = {(0,0):C.GC_10})

V_30 = Vertex(name = 'V_30',
              particles = [ P.g, P.g, P.H ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.VVS1 ],
              couplings = {(0,0):C.GC_13})

V_31 = Vertex(name = 'V_31',
              particles = [ P.g, P.gQ, P.H, P.H, P.H ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.VVSSS1 ],
              couplings = {(0,0):C.GC_7})

V_32 = Vertex(name = 'V_32',
              particles = [ P.g, P.gQ, P.H, P.H ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.VVSS1 ],
              couplings = {(0,0):C.GC_10})

V_33 = Vertex(name = 'V_33',
              particles = [ P.g, P.gQ, P.H ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.VVS1 ],
              couplings = {(0,0):C.GC_13})

V_34 = Vertex(name = 'V_34',
              particles = [ P.gQ, P.gQ, P.H, P.H, P.H ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.VVSSS1 ],
              couplings = {(0,0):C.GC_7})

V_35 = Vertex(name = 'V_35',
              particles = [ P.gQ, P.gQ, P.H, P.H ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.VVSS1 ],
              couplings = {(0,0):C.GC_10})

V_36 = Vertex(name = 'V_36',
              particles = [ P.gQ, P.gQ, P.H ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.VVS1 ],
              couplings = {(0,0):C.GC_13})

V_37 = Vertex(name = 'V_37',
              particles = [ P.g, P.g, P.g, P.H, P.H, P.H ],
              color = [ 'f(1,2,3)' ],
              lorentz = [ L.VVVSSS1 ],
              couplings = {(0,0):C.GC_8})

V_38 = Vertex(name = 'V_38',
              particles = [ P.g, P.g, P.g, P.H, P.H ],
              color = [ 'f(1,2,3)' ],
              lorentz = [ L.VVVSS1 ],
              couplings = {(0,0):C.GC_11})

V_39 = Vertex(name = 'V_39',
              particles = [ P.g, P.g, P.g, P.H ],
              color = [ 'f(1,2,3)' ],
              lorentz = [ L.VVVS1 ],
              couplings = {(0,0):C.GC_14})

V_40 = Vertex(name = 'V_40',
              particles = [ P.g, P.g, P.gQ, P.H, P.H, P.H ],
              color = [ 'f(1,2,3)' ],
              lorentz = [ L.VVVSSS1 ],
              couplings = {(0,0):C.GC_8})

V_41 = Vertex(name = 'V_41',
              particles = [ P.g, P.g, P.gQ, P.H, P.H ],
              color = [ 'f(1,2,3)' ],
              lorentz = [ L.VVVSS1 ],
              couplings = {(0,0):C.GC_11})

V_42 = Vertex(name = 'V_42',
              particles = [ P.g, P.g, P.gQ, P.H ],
              color = [ 'f(1,2,3)' ],
              lorentz = [ L.VVVS1 ],
              couplings = {(0,0):C.GC_14})

V_43 = Vertex(name = 'V_43',
              particles = [ P.g, P.g, P.g, P.g, P.H, P.H, P.H ],
              color = [ 'f(-1,1,2)*f(3,4,-1)', 'f(-1,1,3)*f(2,4,-1)', 'f(-1,1,4)*f(2,3,-1)' ],
              lorentz = [ L.VVVVSSS1, L.VVVVSSS2, L.VVVVSSS3 ],
              couplings = {(1,1):C.GC_9,(0,0):C.GC_9,(2,2):C.GC_9})

V_44 = Vertex(name = 'V_44',
              particles = [ P.g, P.g, P.g, P.g, P.H, P.H ],
              color = [ 'f(-1,1,2)*f(3,4,-1)', 'f(-1,1,3)*f(2,4,-1)', 'f(-1,1,4)*f(2,3,-1)' ],
              lorentz = [ L.VVVVSS1, L.VVVVSS2, L.VVVVSS3 ],
              couplings = {(1,1):C.GC_12,(0,0):C.GC_12,(2,2):C.GC_12})

V_45 = Vertex(name = 'V_45',
              particles = [ P.g, P.g, P.g, P.g, P.H ],
              color = [ 'f(-1,1,2)*f(3,4,-1)', 'f(-1,1,3)*f(2,4,-1)', 'f(-1,1,4)*f(2,3,-1)' ],
              lorentz = [ L.VVVVS1, L.VVVVS2, L.VVVVS3 ],
              couplings = {(1,1):C.GC_15,(0,0):C.GC_15,(2,2):C.GC_15})

V_46 = Vertex(name = 'V_46',
              particles = [ P.g, P.gQ, P.gQ, P.H, P.H, P.H ],
              color = [ 'f(1,2,3)' ],
              lorentz = [ L.VVVSSS1 ],
              couplings = {(0,0):C.GC_8})

V_47 = Vertex(name = 'V_47',
              particles = [ P.g, P.gQ, P.gQ, P.H, P.H ],
              color = [ 'f(1,2,3)' ],
              lorentz = [ L.VVVSS1 ],
              couplings = {(0,0):C.GC_11})

V_48 = Vertex(name = 'V_48',
              particles = [ P.g, P.gQ, P.gQ, P.H ],
              color = [ 'f(1,2,3)' ],
              lorentz = [ L.VVVS1 ],
              couplings = {(0,0):C.GC_14})

V_49 = Vertex(name = 'V_49',
              particles = [ P.g, P.g, P.g, P.gQ, P.H, P.H, P.H ],
              color = [ 'f(-1,1,2)*f(3,4,-1)', 'f(-1,1,3)*f(2,4,-1)', 'f(-1,1,4)*f(2,3,-1)' ],
              lorentz = [ L.VVVVSSS1, L.VVVVSSS2, L.VVVVSSS3 ],
              couplings = {(1,1):C.GC_9,(0,0):C.GC_9,(2,2):C.GC_9})

V_50 = Vertex(name = 'V_50',
              particles = [ P.g, P.g, P.g, P.gQ, P.H, P.H ],
              color = [ 'f(-1,1,2)*f(3,4,-1)', 'f(-1,1,3)*f(2,4,-1)', 'f(-1,1,4)*f(2,3,-1)' ],
              lorentz = [ L.VVVVSS1, L.VVVVSS2, L.VVVVSS3 ],
              couplings = {(1,1):C.GC_12,(0,0):C.GC_12,(2,2):C.GC_12})

V_51 = Vertex(name = 'V_51',
              particles = [ P.g, P.g, P.g, P.gQ, P.H ],
              color = [ 'f(-1,1,2)*f(3,4,-1)', 'f(-1,1,3)*f(2,4,-1)', 'f(-1,1,4)*f(2,3,-1)' ],
              lorentz = [ L.VVVVS1, L.VVVVS2, L.VVVVS3 ],
              couplings = {(1,1):C.GC_15,(0,0):C.GC_15,(2,2):C.GC_15})

V_52 = Vertex(name = 'V_52',
              particles = [ P.g, P.g, P.gQ, P.gQ, P.H, P.H, P.H ],
              color = [ 'f(-1,1,2)*f(3,4,-1)', 'f(-1,1,3)*f(2,4,-1)', 'f(-1,1,4)*f(2,3,-1)' ],
              lorentz = [ L.VVVVSSS1, L.VVVVSSS2, L.VVVVSSS3 ],
              couplings = {(1,1):C.GC_9,(0,0):C.GC_9,(2,2):C.GC_9})

V_53 = Vertex(name = 'V_53',
              particles = [ P.g, P.g, P.gQ, P.gQ, P.H, P.H ],
              color = [ 'f(-1,1,2)*f(3,4,-1)', 'f(-1,1,3)*f(2,4,-1)', 'f(-1,1,4)*f(2,3,-1)' ],
              lorentz = [ L.VVVVSS1, L.VVVVSS2, L.VVVVSS3 ],
              couplings = {(1,1):C.GC_12,(0,0):C.GC_12,(2,2):C.GC_12})

V_54 = Vertex(name = 'V_54',
              particles = [ P.g, P.g, P.gQ, P.gQ, P.H ],
              color = [ 'f(-1,1,2)*f(3,4,-1)', 'f(-1,1,3)*f(2,4,-1)', 'f(-1,1,4)*f(2,3,-1)' ],
              lorentz = [ L.VVVVS1, L.VVVVS2, L.VVVVS3 ],
              couplings = {(1,1):C.GC_15,(0,0):C.GC_15,(2,2):C.GC_15})

V_55 = Vertex(name = 'V_55',
              particles = [ P.gQ, P.gQ, P.gQ, P.H, P.H, P.H ],
              color = [ 'f(1,2,3)' ],
              lorentz = [ L.VVVSSS1 ],
              couplings = {(0,0):C.GC_8})

V_56 = Vertex(name = 'V_56',
              particles = [ P.gQ, P.gQ, P.gQ, P.H, P.H ],
              color = [ 'f(1,2,3)' ],
              lorentz = [ L.VVVSS1 ],
              couplings = {(0,0):C.GC_11})

V_57 = Vertex(name = 'V_57',
              particles = [ P.gQ, P.gQ, P.gQ, P.H ],
              color = [ 'f(1,2,3)' ],
              lorentz = [ L.VVVS1 ],
              couplings = {(0,0):C.GC_14})

V_58 = Vertex(name = 'V_58',
              particles = [ P.g, P.gQ, P.gQ, P.gQ, P.H, P.H, P.H ],
              color = [ 'f(-1,1,2)*f(3,4,-1)', 'f(-1,1,3)*f(2,4,-1)', 'f(-1,1,4)*f(2,3,-1)' ],
              lorentz = [ L.VVVVSSS1, L.VVVVSSS2, L.VVVVSSS3 ],
              couplings = {(1,1):C.GC_9,(0,0):C.GC_9,(2,2):C.GC_9})

V_59 = Vertex(name = 'V_59',
              particles = [ P.g, P.gQ, P.gQ, P.gQ, P.H, P.H ],
              color = [ 'f(-1,1,2)*f(3,4,-1)', 'f(-1,1,3)*f(2,4,-1)', 'f(-1,1,4)*f(2,3,-1)' ],
              lorentz = [ L.VVVVSS1, L.VVVVSS2, L.VVVVSS3 ],
              couplings = {(1,1):C.GC_12,(0,0):C.GC_12,(2,2):C.GC_12})

V_60 = Vertex(name = 'V_60',
              particles = [ P.g, P.gQ, P.gQ, P.gQ, P.H ],
              color = [ 'f(-1,1,2)*f(3,4,-1)', 'f(-1,1,3)*f(2,4,-1)', 'f(-1,1,4)*f(2,3,-1)' ],
              lorentz = [ L.VVVVS1, L.VVVVS2, L.VVVVS3 ],
              couplings = {(1,1):C.GC_15,(0,0):C.GC_15,(2,2):C.GC_15})

V_61 = Vertex(name = 'V_61',
              particles = [ P.gQ, P.gQ, P.gQ, P.gQ, P.H, P.H, P.H ],
              color = [ 'f(-1,1,2)*f(3,4,-1)', 'f(-1,1,3)*f(2,4,-1)', 'f(-1,1,4)*f(2,3,-1)' ],
              lorentz = [ L.VVVVSSS1, L.VVVVSSS2, L.VVVVSSS3 ],
              couplings = {(1,1):C.GC_9,(0,0):C.GC_9,(2,2):C.GC_9})

V_62 = Vertex(name = 'V_62',
              particles = [ P.gQ, P.gQ, P.gQ, P.gQ, P.H, P.H ],
              color = [ 'f(-1,1,2)*f(3,4,-1)', 'f(-1,1,3)*f(2,4,-1)', 'f(-1,1,4)*f(2,3,-1)' ],
              lorentz = [ L.VVVVSS1, L.VVVVSS2, L.VVVVSS3 ],
              couplings = {(1,1):C.GC_12,(0,0):C.GC_12,(2,2):C.GC_12})

V_63 = Vertex(name = 'V_63',
              particles = [ P.gQ, P.gQ, P.gQ, P.gQ, P.H ],
              color = [ 'f(-1,1,2)*f(3,4,-1)', 'f(-1,1,3)*f(2,4,-1)', 'f(-1,1,4)*f(2,3,-1)' ],
              lorentz = [ L.VVVVS1, L.VVVVS2, L.VVVVS3 ],
              couplings = {(1,1):C.GC_15,(0,0):C.GC_15,(2,2):C.GC_15})

