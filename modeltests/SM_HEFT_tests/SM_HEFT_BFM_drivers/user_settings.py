#==============================================================================#
#                                   model.py                                   #
#==============================================================================#

#============#
#  Includes  #
#============#

from rept1l.autoct.modelfile import model, CTParameter
from rept1l.autoct.modelfile import TreeInducedCTVertex, CTVertex
from rept1l.autoct.auto_parameters_ct import auto_assign_ct_particle
from rept1l.autoct.auto_parameters_ct import set_light_particle
from rept1l.autoct.auto_parameters_ct import assign_counterterm
from rept1l.autoct.auto_parameters_ct import add_counterterm
from rept1l.autoct.auto_tadpole_ct import auto_tadpole_vertices
from rept1l.autoct.auto_tadpole_ct import assign_FJTadpole_field_shift

#===========#
#  Globals  #
#===========#

# coupling constructor
Coupling = model.object_library.Coupling

# access to instances
param = model.parameters
P = model.particles
L = model.lorentz

# LCT allows to access the 2-point lorentz structures defined in lorentz_ct.py
import rept1l.autoct.lorentz_ct as LCT

modelname = 'SM (QCD) + HEFT'
modelgauge = "'t Hooft-Feynman BFM"

features = {'fermionloop_opt': True,
            'qcd_rescaling': True,
            }

#============================================#
#  Declare quantumfield & background fields  #
#============================================#

P.g.backgroundfield = True
P.gQ.quantumfield = True

#=============================#
#  External Parameter Orders  #
#=============================#

parameter_orders = {param.aEW: {'QED': 2},
                    param.aS: {'QCD': 2},
                    param.cggh: {'QCD': 2}
                    }


lquarks = [P.u, P.d, P.c, P.s, P.b]
for p in lquarks:
  set_light_particle(p)

doublets = [(P.u, P.d), (P.c, P.s), (P.t, P.b)]

generations = [(P.d, P.s, P.b)]

tadpole_parameter = [param.cgghfin]
assign_counterterm(param.aS, 'dZgs', 'gs**2*dZgs/(2*pi)')
assign_counterterm(param.gs, 'dZgs', 'gs*dZgs')
assign_counterterm(param.cgghfin, 'dcgghfin', 'dcgghfin')

mixings = {}
selection = [P.g] + lquarks
auto_assign_ct_particle(mixings, selection=selection)
