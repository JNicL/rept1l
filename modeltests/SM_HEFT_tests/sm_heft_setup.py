################################################################################
#                               sm_heft_setup.py                               #
################################################################################

import os

driver_sm_heft_path = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                                   'SM_HEFT_drivers')
driver_sm_heft_bfm_path = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                                       'SM_HEFT_BFM_drivers')
