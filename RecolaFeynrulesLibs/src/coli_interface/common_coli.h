!* -*-Fortran-*-

!***********************************************************************
!*     file common_coli.h                                              *
!*     contains global common blocks for COLI                          *
!*---------------------------------------------------------------------*
!*     04.08.08  Ansgar Denner     last changed  27.03.15              *
!***********************************************************************


!c information output switch
      logical   coliinfo
      common/info_SAE_coli/coliinfo

#ifdef SING
!c regularization parameters
      real*8    deltauv,delta2ir,delta1ir,colishiftms2
      common/sing_SAE_coli/deltauv,delta2ir,delta1ir,colishiftms2
#endif
!c regularization parameters
      logical   ir_rat_terms
      real*8    muuv2,muir2
      common/dimreg_SAE_coli/muuv2,muir2,ir_rat_terms

!c infinitesimal masses
      integer    ncoliminf
      common /ncoliminf_SAE/    ncoliminf
      complex*16 coliminf(100)
      common /coliminf_SAE/     coliminf    
      complex*16 coliminf2(100)
      common /coliminf2_SAE/    coliminf2    
      complex*16 coliminffix(100)
      common /coliminffix_SAE/  coliminffix    
      complex*16 coliminffix2(100)
      common /coliminffix2_SAE/ coliminffix2    

!c scaling of infinitesimal masses
      real*8     coliminfscale,coliminfscale2
      common /colimsing_SAE/ coliminfscale,coliminfscale2
