! -*-Fortran-*-

!***********************************************************************
!*     file global_coli.h                                              *
!*     steers global flags for  COLI                                   *
!*---------------------------------------------------------------------*
!*     02.05.08  Ansgar Denner     last changed  07.06.13              *
!***********************************************************************

!c take singular contributions into account
#define SING

!c perform various checks
#undef CHECK

!c issue warnings
#undef WARN

!c print untested scalar function calls
#undef NEWCHECK
