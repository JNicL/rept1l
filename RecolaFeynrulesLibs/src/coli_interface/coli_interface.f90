module coli_interface
 implicit none
  contains

  subroutine A0_col_i(A0,m02)
    double complex, intent(in) :: m02
    double complex, intent(out) :: A0
    double complex :: A0_SAE_coli
    A0 = A0_SAE_coli(m02)
  end subroutine A0_col_i

  subroutine B0_col_i(B0,p10,m02,m12)
    double complex, intent(in) :: p10,m02,m12
    double complex, intent(out) :: B0
    double complex :: B0_SAE_coli
    B0 = B0_SAE_coli(p10,m02,m12)
  end subroutine B0_col_i

  subroutine DB0_col_i(DB0,p10,m02,m12)
    double complex, intent(in) :: p10,m02,m12
    double complex, intent(out) :: DB0
    double complex :: DB0_SAE_coli
    DB0 = DB0_SAE_coli(p10,m02,m12)
  end subroutine DB0_col_i

  subroutine B1_col_i(B1,p10,m02,m12)
    double complex, intent(in) :: p10,m02,m12
    double complex, intent(out) :: B1
    double complex :: B1_SAE_coli
    B1 = B1_SAE_coli(p10,m02,m12)
  end subroutine B1_col_i

  subroutine DB1_col_i(DB1,p10,m02,m12)
    double complex, intent(in) :: p10,m02,m12
    double complex, intent(out) :: DB1
    double complex :: DB1_SAE_coli
    DB1 = DB1_SAE_coli(p10,m02,m12)
  end subroutine DB1_col_i

  subroutine B00_col_i(B00,p10,m02,m12)
    double complex, intent(in) :: p10,m02,m12
    double complex, intent(out) :: B00
    double complex :: B00_SAE_coli
    B00 = B00_SAE_coli(p10,m02,m12)
  end subroutine B00_col_i

  subroutine DB00_col_i(DB00,p10,m02,m12)
    double complex, intent(in) :: p10,m02,m12
    double complex, intent(out) :: DB00
    double complex :: DB00_SAE_coli
    DB00 = DB00_SAE_coli(p10,m02,m12)
  end subroutine DB00_col_i

  subroutine B11_col_i(B11,p10,m02,m12)
    double complex, intent(in) :: p10,m02,m12
    double complex, intent(out) :: B11
    double complex :: B11_SAE_coli
    B11 = B11_SAE_coli(p10,m02,m12)
  end subroutine B11_col_i

  subroutine Bn_col_i(Bn,n,p10,m02,m12)
    integer, intent(in) :: n
    double complex, intent(in) :: p10,m02,m12
    double complex, intent(out) :: Bn
    double complex :: Bn_SAE_coli
    Bn = Bn_SAE_coli(n,p10,m02,m12)
  end subroutine Bn_col_i

  subroutine B111_col_i(B111,p10,m02,m12)
    double complex, intent(in) :: p10,m02,m12
    double complex, intent(out) :: B111
    call Bn_col_i(B111,3,p10,m02,m12)
  end subroutine B111_col_i

  subroutine B1111_col_i(B1111,p10,m02,m12)
    double complex, intent(in) :: p10,m02,m12
    double complex, intent(out) :: B1111
    call Bn_col_i(B1111,4,p10,m02,m12)
  end subroutine B1111_col_i

  subroutine B00n_col_i(B00n,n,p10,m02,m12)
    integer, intent(in) :: n
    double complex, intent(in) :: p10,m02,m12
    double complex, intent(out) :: B00n
    double complex :: B00n_SAE_coli
    B00n = B00n_SAE_coli(n,p10,m02,m12)
  end subroutine B00n_col_i

  subroutine B001_col_i(B001,p10,m02,m12)
    double complex, intent(in) :: p10,m02,m12
    double complex, intent(out) :: B001
    call B00n_col_i(B001,1,p10,m02,m12)
  end subroutine B001_col_i

  subroutine B0011_col_i(B0011,p10,m02,m12)
    double complex, intent(in) :: p10,m02,m12
    double complex, intent(out) :: B0011
    call B00n_col_i(B0011,2,p10,m02,m12)
  end subroutine B0011_col_i

  subroutine B0000n_col_i(B0000n,n,p10,m02,m12)
    integer, intent(in) :: n
    double complex, intent(in) :: p10,m02,m12
    double complex, intent(out) :: B0000n
    double complex :: B0000n_SAE_coli
    B0000n = B0000n_SAE_coli(n,p10,m02,m12)
  end subroutine B0000n_col_i

  subroutine B0000_col_i(B0000,p10,m02,m12)
    double complex, intent(in) :: p10,m02,m12
    double complex, intent(out) :: B0000
    call B0000n_col_i(B0000,0,p10,m02,m12)
  end subroutine B0000_col_i

  subroutine DBn_col_i(DBn,n,p10,m02,m12)
    integer, intent(in) :: n
    double complex, intent(in) :: p10,m02,m12
    double complex, intent(out) :: DBn
    double complex :: DBn_SAE_coli
    DBn = DBn_SAE_coli(n,p10,m02,m12)
  end subroutine DBn_col_i

  subroutine DB11_col_i(DB11,p10,m02,m12)
    double complex, intent(in) :: p10,m02,m12
    double complex, intent(out) :: DB11
    call DBn_col_i(DB11,2,p10,m02,m12)
  end subroutine DB11_col_i

  subroutine DB111_col_i(DB111,p10,m02,m12)
    double complex, intent(in) :: p10,m02,m12
    double complex, intent(out) :: DB111
    call DBn_col_i(DB111,3,p10,m02,m12)
  end subroutine DB111_col_i

  subroutine DB1111_col_i(DB1111,p10,m02,m12)
    double complex, intent(in) :: p10,m02,m12
    double complex, intent(out) :: DB1111
    call DBn_col_i(DB1111,4,p10,m02,m12)
  end subroutine DB1111_col_i

  subroutine B00001_col_i(B00001,p10,m02,m12)
    double complex, intent(in) :: p10,m02,m12
    double complex, intent(out) :: B00001
    call B0000n_col_i(B00001,1,p10,m02,m12)
  end subroutine B00001_col_i

  subroutine B00111_col_i(B00111,p10,m02,m12)
    double complex, intent(in) :: p10,m02,m12
    double complex, intent(out) :: B00111
    call B00n_col_i(B00111,3,p10,m02,m12)
  end subroutine B00111_col_i

  subroutine B11111_col_i(B11111,p10,m02,m12)
    double complex, intent(in) :: p10,m02,m12
    double complex, intent(out) :: B11111
    call Bn_col_i(B11111,5,p10,m02,m12)
  end subroutine B11111_col_i

  subroutine DB11111_col_i(DB11111,p10,m02,m12)
    double complex, intent(in) :: p10,m02,m12
    double complex, intent(out) :: DB11111
    call DBn_col_i(DB11111,5,p10,m02,m12)
  end subroutine DB11111_col_i

  subroutine B000011_col_i(B000011,p10,m02,m12)
    double complex, intent(in) :: p10,m02,m12
    double complex, intent(out) :: B000011
    call B0000n_col_i(B000011,2,p10,m02,m12)
  end subroutine B000011_col_i

  subroutine B001111_col_i(B001111,p10,m02,m12)
    double complex, intent(in) :: p10,m02,m12
    double complex, intent(out) :: B001111
    call B00n_col_i(B001111,4,p10,m02,m12)
  end subroutine B001111_col_i

  subroutine B111111_col_i(B111111,p10,m02,m12)
    double complex, intent(in) :: p10,m02,m12
    double complex, intent(out) :: B111111
    call Bn_col_i(B111111,6,p10,m02,m12)
  end subroutine B111111_col_i

  subroutine DB111111_col_i(DB111111,p10,m02,m12)
    double complex, intent(in) :: p10,m02,m12
    double complex, intent(out) :: DB111111
    call DBn_col_i(DB111111,6,p10,m02,m12)
  end subroutine DB111111_col_i

  !subroutine DB11_col_i(DB11,p10,m02,m12)
    !double complex, intent(in) :: p10,m02,m12
    !double complex, intent(out) :: DB11
    !double complex :: DB11_coli
    !DB11 = DB11_coli(p10,m02,m12)
  !end subroutine DB11_col_i

  subroutine setmuuv2(muuv2in)
    implicit none
    real*8, intent(in) :: muuv2in
    call setmuuv2_SAE_coli(muuv2in)
  end subroutine setmuuv2

  subroutine setmuir2(muir2in)
    implicit none
    real*8, intent(in) :: muir2in
    call setmuir2_SAE_coli(muir2in)
  end subroutine setmuir2

  subroutine setdeltauv(deltauvin)
    implicit none
    real*8, intent(in) :: deltauvin
    call setdeltauv_SAE_coli(deltauvin)
  end subroutine setdeltauv

  subroutine setdeltair(delta1irin, delta2irin)
    implicit none
    real*8, intent(in) :: delta1irin, delta2irin
    call setdeltair_SAE_coli(delta1irin, delta2irin)
  end subroutine setdeltair

  subroutine addminf2(m2)

    double complex, intent(in) :: m2
    call setminf2_SAE_coli(m2)

  end subroutine addminf2

  subroutine clearminf2
    call clearcoliminf2_SAE
  end subroutine clearminf2


  subroutine setminf2(nminf,minf2)

    integer, intent(in) :: nminf
    double complex, intent(in) :: minf2(nminf)
    integer :: i

    call clearminf2

    do i=1,nminf
      call addminf2(minf2(i))
    end do

  end subroutine setminf2

  subroutine initcoli
    implicit none
      !call unsetinfo_coli
!     call setmuuv2_coli(1d0)
!     call setmuir2_coli(1d0)
      call setminfscale2_SAE_coli(1d0)
      call setshiftms2_SAE_coli(0d0)
      call setIRRatTerms_SAE_coli
  end subroutine initcoli
end module coli_interface
