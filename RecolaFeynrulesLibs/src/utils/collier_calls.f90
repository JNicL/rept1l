!******************************************************************************!
!                                                                              !
!    collier_calls.f90                                                         !
!    is part of RECOLA2 (REcursive Computation of One Loop Amplitudes)2        !
!                                                                              !
!    Copyright (C) 2016-2020 Ansgar Denner, Jean-Nicolas Lang and              !
!                            Sandro Uccirati                                   !
!                                                                              !
!    RECOLA2 is licenced under the GNU GPL version 3,                          !
!    see COPYING for details.                                                  !
!                                                                              !
!******************************************************************************!

module collier_calls


  use collier

!------------------------------------------------------------------------------!

  implicit none

  contains

!------------------------------------------------------------------------------!

    function A0_cll_wrap(m02) result(A0)
      double complex, intent(in) :: m02
      double complex             :: A0

      call A0_cll(A0, m02)
    end function A0_cll_wrap

    function A00_cll_wrap(m02) result(A00)
      double complex, intent(in) :: m02
      double complex             :: A00

      integer, parameter :: rmax=2
      double complex     :: Auv(0:rmax/2), A(0:rmax/2)

      call A_cll(A,Auv,m02,rmax)
      A00 = A(1)
    end function A00_cll_wrap

    function C0_cll_wrap(xp1,xp21,xp2,xm02,xm12,xm22) result (xC0)

      double complex, intent (in) :: xp1,xp21,xp2,xm02,xm12,xm22
      double complex              :: xC0

      call C0_cll(xC0,xp1,xp21,xp2,xm02,xm12,xm22) 
      
    end function C0_cll_wrap

    function C1_cll_wrap(xp1,xp21,xp2,xm02,xm12,xm22) result (xC1)

      double complex, intent (in) :: xp1,xp21,xp2,xm02,xm12,xm22
      double complex              :: xC1

      integer, parameter :: rmax = 2
      double complex     :: Cuv(0:rmax/2,0:rmax,0:rmax)
      double complex     :: C(0:rmax/2,0:rmax,0:rmax)

      call C_cll(C,Cuv,xp1,xp21,xp2,xm02,xm12,xm22,rmax) 
      xC1 = C(0,1,0)
      
    end function C1_cll_wrap

    function C2_cll_wrap(xp1,xp21,xp2,xm02,xm12,xm22) result (xC2)

      double complex, intent (in) :: xp1,xp21,xp2,xm02,xm12,xm22
      double complex              :: xC2

      integer, parameter :: rmax = 2
      double complex     :: Cuv(0:rmax/2,0:rmax,0:rmax)
      double complex     :: C(0:rmax/2,0:rmax,0:rmax)

      call C_cll(C,Cuv,xp1,xp21,xp2,xm02,xm12,xm22,rmax) 
      xC2 = C(0,0,1)
      
    end function C2_cll_wrap

    function C00_cll_wrap(xp1,xp21,xp2,xm02,xm12,xm22) result (xC00)

      double complex, intent (in) :: xp1,xp21,xp2,xm02,xm12,xm22
      double complex              :: xC00

      integer, parameter :: rmax = 2
      double complex     :: Cuv(0:rmax/2,0:rmax,0:rmax)
      double complex     :: C(0:rmax/2,0:rmax,0:rmax)

      call C_cll(C,Cuv,xp1,xp21,xp2,xm02,xm12,xm22,rmax) 
      xC00 = C(1,0,0)
      
    end function C00_cll_wrap

    function C11_cll_wrap(xp1,xp21,xp2,xm02,xm12,xm22) result (xC11)

      double complex, intent (in) :: xp1,xp21,xp2,xm02,xm12,xm22
      double complex              :: xC11

      integer, parameter :: rmax = 2
      double complex     :: Cuv(0:rmax/2,0:rmax,0:rmax)
      double complex     :: C(0:rmax/2,0:rmax,0:rmax)

      call C_cll(C,Cuv,xp1,xp21,xp2,xm02,xm12,xm22,rmax) 
      xC11 = C(0,2,0)
      
    end function C11_cll_wrap

    function C12_cll_wrap(xp1,xp21,xp2,xm02,xm12,xm22) result (xC12)

      double complex, intent (in) :: xp1,xp21,xp2,xm02,xm12,xm22
      double complex              :: xC12

      integer, parameter :: rmax = 2
      double complex     :: Cuv(0:rmax/2,0:rmax,0:rmax)
      double complex     :: C(0:rmax/2,0:rmax,0:rmax)

      call C_cll(C,Cuv,xp1,xp21,xp2,xm02,xm12,xm22,rmax) 
      xC12 = C(0,1,1)
      
    end function C12_cll_wrap

    function C22_cll_wrap(xp1,xp21,xp2,xm02,xm12,xm22) result (xC22)

      double complex, intent (in) :: xp1,xp21,xp2,xm02,xm12,xm22
      double complex              :: xC22

      integer, parameter :: rmax = 2
      double complex     :: Cuv(0:rmax/2,0:rmax,0:rmax)
      double complex     :: C(0:rmax/2,0:rmax,0:rmax)

      call C_cll(C,Cuv,xp1,xp21,xp2,xm02,xm12,xm22,rmax) 
      xC22 = C(0,0,2)
      
    end function C22_cll_wrap

    function B0_cll_wrap(xp,xm02,xm12) result(B0)
      double complex, intent(in) :: xp,xm02,xm12
      double complex             :: B0

      call B0_cll(B0,xp,xm02,xm12)
    end function B0_cll_wrap

    function DB0_cll_wrap(xp,xm02,xm12) result(DB0)
      double complex, intent(in) :: xp,xm02,xm12
      double complex             :: DB0

      call DB0_cll(DB0,xp,xm02,xm12)
    end function DB0_cll_wrap

    function B1_cll_wrap(xp,xm02,xm12) result(xB1)
      double complex, intent(in) :: xp,xm02,xm12
      double complex             :: xB1

      integer, parameter :: rmax = 1
      double complex :: Buv(0:rmax/2,0:rmax)
      double complex :: B(0:rmax/2,0:rmax)

      call B_cll(B,Buv,xp,xm02,xm12,rmax) 
      xB1 = B(0,1)
    end function B1_cll_wrap

    function DB1_cll_wrap(xp,xm02,xm12) result(DB1)
      double complex, intent(in) :: xp,xm02,xm12
      double complex             :: DB1,DB1uv

      call DB1_cll(DB1,xp,xm02,xm12)
    end function DB1_cll_wrap

    function B00_cll_wrap(xp,xm02,xm12) result(xB00)
      double complex, intent(in) :: xp,xm02,xm12
      double complex             :: xB00

      integer, parameter :: rmax = 2
      double complex     :: Buv(0:rmax/2,0:rmax)
      double complex     :: B(0:rmax/2,0:rmax)

      call B_cll(B,Buv,xp,xm02,xm12,rmax) 
      xB00 = B(1,0)

    end function B00_cll_wrap

    function DB00_cll_wrap(xp,xm02,xm12) result(xDB00)
      double complex, intent(in) :: xp,xm02,xm12
      double complex             :: xDB00,DBuv

      call DB00_cll(xDB00,DBuv,xp,xm02,xm12)
    end function DB00_cll_wrap

    function B11_cll_wrap(xp,xm02,xm12) result(xB11)
      double complex, intent(in) :: xp,xm02,xm12
      double complex             :: xB11

      integer, parameter :: rmax = 2
      double complex     :: Buv(0:rmax/2,0:rmax)
      double complex     :: B(0:rmax/2,0:rmax)

      call B_cll(B,Buv,xp,xm02,xm12,rmax) 
      xB11 = B(0,2)

    end function B11_cll_wrap

    function DB11_cll_wrap(xp,xm02,xm12) result (xDB11)

      double complex, intent (in) :: xp,xm02,xm12
      double complex              :: xDB11

      integer, parameter :: rmax = 2
      double complex     :: DBuv(0:rmax/2,0:rmax)
      double complex     :: DB(0:rmax/2,0:rmax)

      call DB_cll(DB,DBuv,xp,xm02,xm12,rmax) 
      xDB11 = DB(0,2)
      
    end function DB11_cll_wrap

    function B111_cll_wrap (xp,xm02,xm12) result (xB111)

      double complex, intent (in) :: xp,xm02,xm12
      double complex              :: xB111

      integer, parameter :: rmax = 3
      double complex     :: Buv(0:rmax/2,0:rmax)
      double complex     :: B(0:rmax/2,0:rmax)

      call B_cll(B,Buv,xp,xm02,xm12,rmax) 
      xB111 = B(0,3)
      
    end function B111_cll_wrap

    function B001_cll_wrap(xp,xm02,xm12) result (xB001)

      double complex, intent (in) :: xp,xm02,xm12
      double complex              :: xB001

      integer, parameter :: rmax = 3
      double complex     :: Buv(0:rmax/2,0:rmax)
      double complex     :: B(0:rmax/2,0:rmax)

      call B_cll(B,Buv,xp,xm02,xm12,rmax) 
      xB001 = B(1,1)
      
    end function B001_cll_wrap

    function DB001_cll_wrap(xp,xm02,xm12) result (xDB001)

      double complex, intent (in) :: xp,xm02,xm12
      double complex              :: xDB001

      integer, parameter :: rmax = 3
      double complex     :: DBuv(0:rmax/2,0:rmax)
      double complex     :: DB(0:rmax/2,0:rmax)

      call DB_cll(DB,DBuv,xp,xm02,xm12,rmax) 
      xDB001 = DB(1,1)
      
    end function DB001_cll_wrap

    function DB111_cll_wrap(xp,xm02,xm12) result (xDB111)

      double complex, intent (in) :: xp,xm02,xm12
      double complex              :: xDB111

      integer, parameter :: rmax = 3
      double complex     :: DBuv(0:rmax/2,0:rmax)
      double complex     :: DB(0:rmax/2,0:rmax)

      call DB_cll(DB,DBuv,xp,xm02,xm12,rmax) 
      xDB111 = DB(0,3)
      
    end function DB111_cll_wrap

    function B1111_cll_wrap(xp,xm02,xm12) result (xB1111)

      double complex, intent (in) :: xp,xm02,xm12
      double complex              :: xB1111

      integer, parameter :: rmax = 4
      double complex     :: Buv(0:rmax/2,0:rmax)
      double complex     :: B(0:rmax/2,0:rmax)

      call B_cll(B,Buv,xp,xm02,xm12,rmax) 
      xB1111 = B(0,4)
      
    end function B1111_cll_wrap

    function B0011_cll_wrap(xp,xm02,xm12) result (xB0011)

      double complex, intent (in) :: xp,xm02,xm12
      double complex              :: xB0011

      integer, parameter :: rmax = 4
      double complex     :: Buv(0:rmax/2,0:rmax)
      double complex     :: B(0:rmax/2,0:rmax)

      call B_cll(B,Buv,xp,xm02,xm12,rmax) 
      xB0011 = B(1,2)
      
    end function B0011_cll_wrap

    function DB1111_cll_wrap(xp,xm02,xm12) result (xDB1111)

      double complex, intent (in) :: xp,xm02,xm12
      double complex              :: xDB1111

      integer, parameter :: rmax = 4
      double complex     :: DBuv(0:rmax/2,0:rmax)
      double complex     :: DB(0:rmax/2,0:rmax)

      call DB_cll(DB,DBuv,xp,xm02,xm12,rmax) 
      xDB1111 = DB(0,4)
      
    end function DB1111_cll_wrap

    function DB0011_cll_wrap(xp,xm02,xm12) result (xDB0011)

      double complex, intent (in) :: xp,xm02,xm12
      double complex              :: xDB0011

      integer, parameter :: rmax = 4
      double complex     :: DBuv(0:rmax/2,0:rmax)
      double complex     :: DB(0:rmax/2,0:rmax)

      call DB_cll(DB,DBuv,xp,xm02,xm12,rmax) 
      xDB0011 = DB(1,2)
      
    end function DB0011_cll_wrap

    function B0000_cll_wrap(xp,xm02,xm12) result (xB0000)

      double complex, intent (in) :: xp,xm02,xm12
      double complex              :: xB0000

      integer, parameter :: rmax = 4
      double complex     :: Buv(0:rmax/2,0:rmax)
      double complex     :: B(0:rmax/2,0:rmax)

      call B_cll(B,Buv,xp,xm02,xm12,rmax) 
      xB0000 = B(2,0)
      
    end function B0000_cll_wrap

    function DB0000_cll_wrap(xp,xm02,xm12) result (xDB0000)

      double complex, intent (in) :: xp,xm02,xm12
      double complex              :: xDB0000

      integer, parameter :: rmax = 4
      double complex     :: DBuv(0:rmax/2,0:rmax)
      double complex     :: DB(0:rmax/2,0:rmax)

      call DB_cll(DB,DBuv,xp,xm02,xm12,rmax) 
      xDB0000 = DB(2,0)
      
    end function DB0000_cll_wrap

!------------------------------------------------------------------------------!

end module collier_calls

!------------------------------------------------------------------------------!
