!******************************************************************************!
!                                                                              !
!    scalar_functions.f90                                                      !
!    is part of RECOLA2 (REcursive Computation of One Loop Amplitudes)2        !
!                                                                              !
!    Copyright (C) 2016-2020 Ansgar Denner, Jean-Nicolas Lang and              !
!                            Sandro Uccirati                                   !
!                                                                              !
!    RECOLA2 is licenced under the GNU GPL version 3,                          !
!    see COPYING for details.                                                  !
!                                                                              !
!******************************************************************************!

module scalar_functions

  use constants_mdl
  use input_mdl
#ifdef USE_COLLIER_REPTIL
  use collier_calls
#endif
#ifdef USE_COLI_REPTIL
  use coli_calls
#endif
  use class_particles

  implicit none

  interface HT
    module procedure HT1, HT2, HT3
  end interface HT

  contains

  subroutine init_scalar_functions(reset_collier)
    logical, intent(in) :: reset_collier
#ifdef USE_COLI_REPTIL
    integer :: i
    call initcoli
    if (reset_collier) then
    call setmuuv2(muUV**2)
    call setmuir2(muIR**2)
    call setdeltauv(DeltaUV)
    call setdeltair(DeltaIR,DeltaIR2)

    call clearminf2
    do i = 1,n_particles
      if (get_particle_mass_reg_mdl(i) .eq. 2 .and. is_particle_mdl(i))  then
        call addminf2(get_particle_cmass2_reg_mdl(i))
      endif
    enddo
#endif
#ifdef USE_COLLIER_REPTIL
    integer                  :: i,n
    complex(dp), allocatable :: m2(:)

    if (reset_collier) then

    if (trim(collier_output_dir) .eq. 'default') then
      call Init_cll(4,noreset=.true.)
    else
      call Init_cll(4,noreset=.true.,folder_name=trim(collier_output_dir))
    endif
    call SetMode_cll(1)

    n = 0
    do i = 1,n_particles
      if (get_particle_mass_reg_mdl(i) .eq. 2 .and. is_particle_mdl(i))  n = n + 1
    end do

    allocate (m2(n))
    n = 0
    do i = 1,n_particles
      if (get_particle_mass_reg_mdl(i) .eq. 2 .and. is_particle_mdl(i))  then
        n = n + 1
        m2(n) = get_particle_cmass2_reg_mdl(i)
      endif
    enddo
    call setminf2_cll (n,m2)
    deallocate (m2)

    call SetDeltaUV_cll (deltaUV)
    call SetDeltaIR_cll (deltaIR,deltaIR2)

    call SetMuIR2_cll(muIR**2)
    call SetMuUV2_cll(muUV**2) 
#endif
    end if
  end subroutine init_scalar_functions

!------------------------------------------------------------------------------!

  function HT1(M1)

    complex(dp), intent(in) :: M1

    complex(dp) :: HT1

    if (M1 .eq. cnul) then
      HT1 = cnul
    else
      HT1 = cone
    end if

  end function HT1

!------------------------------------------------------------------------------!

  function HT2(M1,M2)

    complex(dp), intent(in) :: M1,M2

    complex(dp) :: HT2

    if (M1 .eq. cnul .and. M2 .eq. cnul) then
      HT2 = cnul
    else
      HT2 = cone
    end if

  end function HT2

!------------------------------------------------------------------------------!

  function HT3(M1,M2,M3)

    complex(dp), intent(in) :: M1,M2,M3

    complex(dp) :: HT3

    if (M1 .eq. cnul .and. M2 .eq. cnul .and. M3 .eq. cnul) then
      HT3 = cnul
    else
      HT3 = cone
    end if

  end function HT3

!------------------------------------------------------------------------------!

  function A0 (xm02)

    complex(dp), intent(in) :: xm02

    complex(dp) :: A0

    if (init_SI) then
      A0 = A0_cll_wrap(xm02)
    else
      A0 = cnul
    end if

  end function A0

!---------------------------------------------------------------------

  function A00(xm02)

    complex(dp), intent(in) :: xm02

    complex(dp) :: A00

    if (init_SI) then
      A00 = A00_cll_wrap(xm02)
    else
      A00 = cnul
    end if

  end function A00

!---------------------------------------------------------------------

  function C0(xp1,xp21,xp2,xm02,xm12,xm22)

    complex(dp), intent(in) :: xp1,xp21,xp2,xm02,xm12,xm22

    complex(dp) :: C0,p1,p2,p21
  
    if (init_SI) then
      p1 = cmplx(real(xp1),0d0,kind=dp)
      p21 = cmplx(real(xp21),0d0,kind=dp)
      p2 = cmplx(real(xp2),0d0,kind=dp)
      C0 = C0_cll_wrap(p1,p21,p2,xm02,xm12,xm22)
    else
      C0 = cnul
    end if

  end function C0

  function C1(xp1,xp21,xp2,xm02,xm12,xm22)

    complex(dp), intent(in) :: xp1,xp21,xp2,xm02,xm12,xm22

    complex(dp) :: C1,p1,p2,p21
  
    if (init_SI) then
      p1 = cmplx(real(xp1),0d0,kind=dp)
      p21 = cmplx(real(xp21),0d0,kind=dp)
      p2 = cmplx(real(xp2),0d0,kind=dp)
      C1 = C1_cll_wrap(p1,p21,p2,xm02,xm12,xm22)
    else
      C1 = cnul
    end if

  end function C1
 
  function C2(xp1,xp21,xp2,xm02,xm12,xm22)

    complex(dp), intent(in) :: xp1,xp21,xp2,xm02,xm12,xm22

    complex(dp) :: C2,p1,p2,p21
  
    if (init_SI) then
      p1 = cmplx(real(xp1),0d0,kind=dp)
      p21 = cmplx(real(xp21),0d0,kind=dp)
      p2 = cmplx(real(xp2),0d0,kind=dp)
      C2 = C2_cll_wrap(p1,p21,p2,xm02,xm12,xm22)
    else
      C2 = cnul
    end if

  end function C2

  function C00(xp1,xp21,xp2,xm02,xm12,xm22)

    complex(dp), intent(in) :: xp1,xp21,xp2,xm02,xm12,xm22

    complex(dp) :: C00,p1,p2,p21
  
    if (init_SI) then
      p1 = cmplx(real(xp1),0d0,kind=dp)
      p21 = cmplx(real(xp21),0d0,kind=dp)
      p2 = cmplx(real(xp2),0d0,kind=dp)
      C00 = C00_cll_wrap(p1,p21,p2,xm02,xm12,xm22)
    else
      C00 = cnul
    end if

  end function C00

  function C11(xp1,xp21,xp2,xm02,xm12,xm22)

    complex(dp), intent(in) :: xp1,xp21,xp2,xm02,xm12,xm22

    complex(dp) :: C11,p1,p2,p21
  
    if (init_SI) then
      p1 = cmplx(real(xp1),0d0,kind=dp)
      p21 = cmplx(real(xp21),0d0,kind=dp)
      p2 = cmplx(real(xp2),0d0,kind=dp)
      C11 = C11_cll_wrap(p1,p21,p2,xm02,xm12,xm22)
    else
      C11 = cnul
    end if

  end function C11

  function C12(xp1,xp21,xp2,xm02,xm12,xm22)

    complex(dp), intent(in) :: xp1,xp21,xp2,xm02,xm12,xm22

    complex(dp) :: C12,p1,p2,p21
  
    if (init_SI) then
      p1 = cmplx(real(xp1),0d0,kind=dp)
      p21 = cmplx(real(xp21),0d0,kind=dp)
      p2 = cmplx(real(xp2),0d0,kind=dp)
      C12 = C12_cll_wrap(p1,p21,p2,xm02,xm12,xm22)
    else
      C12 = cnul
    end if

  end function C12

  function C22(xp1,xp21,xp2,xm02,xm12,xm22)

    complex(dp), intent(in) :: xp1,xp21,xp2,xm02,xm12,xm22

    complex(dp) :: C22,p1,p2,p21
  
    if (init_SI) then
      p1 = cmplx(real(xp1),0d0,kind=dp)
      p21 = cmplx(real(xp21),0d0,kind=dp)
      p2 = cmplx(real(xp2),0d0,kind=dp)
      C22 = C22_cll_wrap(p1,p21,p2,xm02,xm12,xm22)
    else
      C22 = cnul
    end if

  end function C22

  function B0(xp,xm02,xm12)

    complex(dp), intent(in) :: xp,xm02,xm12

    complex(dp) :: B0,p
  
    if (init_SI) then
      p = cmplx(real(xp),0d0,kind=dp)
      B0 = B0_cll_wrap(p,xm02,xm12)
    else
      B0 = cnul
    end if

  end function B0

!---------------------------------------------------------------------

  function DB0(xp,xm02,xm12)

    complex(dp), intent(in) :: xp,xm02,xm12

    complex(dp) :: DB0,p

    if (init_SI) then
      p = cmplx(real(xp),0d0,kind=dp)
      DB0 = DB0_cll_wrap(p,xm02,xm12)
    else
      DB0 = cnul
    end if

  end function DB0

!---------------------------------------------------------------------

  function B1(xp,xm02,xm12)

    complex(dp), intent(in) :: xp,xm02,xm12

    complex(dp) :: B1,p

    if (init_SI) then
      p = cmplx(real(xp),0d0,kind=dp)
      B1 = B1_cll_wrap(p,xm02,xm12)
    else
      B1 = cnul
    end if

  end function B1

!------------------------------------------------------------------------------!

  function DB1(xp,xm02,xm12)

    complex(dp), intent(in) :: xp,xm02,xm12

    complex(dp) :: DB1,p

    if (init_SI) then
      p = cmplx(real(xp),0d0,kind=dp)
      DB1 = DB1_cll_wrap(p,xm02,xm12)
    else
      DB1 = cnul
    end if

  end function DB1

!------------------------------------------------------------------------------!

  function B00(xp,xm02,xm12)

    complex(dp), intent (in) :: xp,xm02,xm12

    complex(dp) :: B00,p
  
    if (init_SI) then
      p = cmplx(real(xp),0d0,kind=dp)
      B00 = B00_cll_wrap(p,xm02,xm12)
    else
      B00 = cnul
    end if

  end function B00

!------------------------------------------------------------------------------!

  function DB00(xp,xm02,xm12)

    complex(dp), intent(in) :: xp,xm02,xm12

    complex(dp) :: DB00,p
  
    if (init_SI) then
      p = cmplx(real(xp),0d0,kind=dp)
      DB00 = DB00_cll_wrap(p,xm02,xm12)
    else
      DB00 = cnul
    end if

  end function DB00

!------------------------------------------------------------------------------!

  function B11(xp,xm02,xm12)

    complex(dp), intent(in) :: xp,xm02,xm12

    complex(dp) :: B11,p

    if (init_SI) then
      p = cmplx(real(xp),0d0,kind=dp)
      B11 = B11_cll_wrap(p,xm02,xm12)
    else
      B11 = cnul
    end if

  end function B11

!------------------------------------------------------------------------------!

  function DB11(xp,xm02,xm12)

    complex(dp), intent(in) :: xp,xm02,xm12

    complex(dp) :: DB11,p
  
    if (init_SI) then
      p = cmplx(real(xp),0d0,kind=dp)
      DB11 = DB11_cll_wrap(p,xm02,xm12)
    else
      DB11 = cnul
    end if

  end function DB11

!------------------------------------------------------------------------------!

  function B001(xp,xm02,xm12)

    complex(dp), intent(in) :: xp,xm02,xm12

    complex(dp) :: B001,p

    if (init_SI) then
      p = cmplx(real(xp),0d0,kind=dp)
      B001 = B001_cll_wrap(p,xm02,xm12)
    else
      B001 = cnul
    end if

  end function B001

!------------------------------------------------------------------------------!

  function DB001(xp,xm02,xm12)

    complex(dp), intent(in) :: xp,xm02,xm12

    complex(dp) :: DB001,p
  
    if (init_SI) then
      p = cmplx(real(xp),0d0,kind=dp)
      DB001 = DB001_cll_wrap(p,xm02,xm12)
    else
      DB001 = cnul
    end if

  end function DB001

!------------------------------------------------------------------------------!

  function B111(xp,xm02,xm12)

    complex(dp), intent(in) :: xp,xm02,xm12

    complex(dp) :: B111,p

    if (init_SI) then
      p = cmplx(real(xp),0d0,kind=dp)
      B111 = B111_cll_wrap(p,xm02,xm12)
    else
      B111 = cnul
    end if

  end function B111

!------------------------------------------------------------------------------!

  function DB111(xp,xm02,xm12)

    complex(dp), intent(in) :: xp,xm02,xm12

    complex(dp) :: DB111,p
    
    if (init_SI) then
      p = cmplx(real(xp),0d0,kind=dp)
      DB111 = DB111_cll_wrap(p,xm02,xm12)
    else
      DB111 = cnul
    end if

  end function DB111

!------------------------------------------------------------------------------!

  function B0000(xp,xm02,xm12)

    complex(dp), intent(in) :: xp,xm02,xm12

    complex(dp) :: B0000,p

    if (init_SI) then
      p = cmplx(real(xp),0d0,kind=dp)
      B0000 = B0000_cll_wrap(p,xm02,xm12)
    else
      B0000 = cnul
    end if

  end function B0000

!------------------------------------------------------------------------------!

  function DB0000(xp,xm02,xm12)

    complex(dp), intent (in) :: xp,xm02,xm12

    complex(dp) :: DB0000,p
    
    if (init_SI) then
      p = cmplx(real(xp),0d0,kind=dp)
      DB0000 = DB0000_cll_wrap(p,xm02,xm12)
    else
      DB0000 = cnul
    end if

  end function DB0000

!------------------------------------------------------------------------------!

  function B0011(xp,xm02,xm12)

    complex(dp), intent(in) :: xp,xm02,xm12

    complex(dp) :: B0011,p

    if (init_SI) then
      p = cmplx(real(xp),0d0,kind=dp)
      B0011 = B0011_cll_wrap(p,xm02,xm12)
    else
      B0011 = cnul
    end if

  end function B0011

!------------------------------------------------------------------------------!

  function DB0011(xp,xm02,xm12)

    complex (dp), intent (in) :: xp,xm02,xm12

    complex (dp) :: DB0011,p
  
    if (init_SI) then
      p = cmplx(real(xp),0d0,kind=dp)
      DB0011 = DB0011_cll_wrap(p,xm02,xm12)
    else
      DB0011 = cnul
    end if

  end function DB0011

!------------------------------------------------------------------------------!

  function B1111(xp,xm02,xm12)

    complex (dp), intent (in) :: xp,xm02,xm12

    complex (dp) :: B1111,p

    if (init_SI) then
      p = cmplx(real(xp),0d0,kind=dp)
      B1111 = B1111_cll_wrap(p,xm02,xm12)
    else
      B1111 = cnul
    end if

  end function B1111

!------------------------------------------------------------------------------!

  function DB1111(xp,xm02,xm12)

    complex(dp), intent(in) :: xp,xm02,xm12

    complex(dp) :: DB1111,p

    if (init_SI) then
      p = cmplx(real(xp),0d0,kind=dp)
      DB1111 = DB1111_cll_wrap(p,xm02,xm12)
    else
      DB1111 = cnul
    end if

  end function DB1111

!------------------------------------------------------------------------------!

end module scalar_functions

!------------------------------------------------------------------------------!
