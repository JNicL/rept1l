!******************************************************************************!
!                                                                              !
!    dictionary.f90                                                            !
!    is part of RECOLA2 (REcursive Computation of One Loop Amplitudes)2        !
!                                                                              !
!    Copyright (C) 2016-2020 Ansgar Denner, Jean-Nicolas Lang and              !
!                            Sandro Uccirati                                   !
!                                                                              !
!    RECOLA2 is licenced under the GNU GPL version 3,                          !
!    see COPYING for details.                                                  !
!                                                                              !
!******************************************************************************!

! Vertices dicitonary via interface to hashtables (brenthash)
module dictionary

 use brenthash, only: init_table,empty,key_prec,insert,lookup,&
                      deallocate_table,table,values

!------------------------------------------------------------------------------!

 implicit none

 contains

!------------------------------------------------------------------------------!

  subroutine dict_init(l)
    integer, intent(in) :: l
    call init_table(l)
  end subroutine dict_init

  subroutine dict(key,mode,address,found,val)
    ! This subroutine can look up and enter integer keys and assign integer
    ! values to the keys
    ! mode=1: Only look up: Returns the address in hashing table, whether it
    !         was found, and the associated value. If not found the address 
    !         and val are set to 0.
    ! mode=2: Look up and insert value: Inserts a value to a dynamically 
    !         determined address associated to key.

    integer(kind=key_prec), intent(in)  :: key
    integer,                intent(out) :: address
    integer,                intent(in)  :: mode,val
    logical,                intent(out) :: found

    if (mode .eq. 1) then
      address = lookup(key)
      found   = key .eq. table(address)

    else if (mode .eq. 2) then
      call insert(key,val)
      address = lookup(key)
      found   = key .eq. table(address)
    end if

  end subroutine dict

  function dict_get_value(address) result(res)
    implicit none

    integer, intent(in) :: address
    integer             :: res

    res = values(address)
  end function dict_get_value

  subroutine deallocate_dict
    call deallocate_table
  end subroutine deallocate_dict

!------------------------------------------------------------------------------!

end module dictionary

!------------------------------------------------------------------------------!
