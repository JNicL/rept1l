!******************************************************************************!
!                                                                              !
!    brenthash.f90                                                             !
!    is part of RECOLA2 (REcursive Computation of One Loop Amplitudes)2        !
!                                                                              !
!    Copyright (C) 2016-2020 Ansgar Denner, Jean-Nicolas Lang and              !
!                            Sandro Uccirati                                   !
!                                                                              !
!    RECOLA2 is licenced under the GNU GPL version 3,                          !
!    see COPYING for details.                                                  !
!                                                                              !
!******************************************************************************!
 
! Custom implementation of Brent's hashing algorithm.
! see CACM 16(2):105-109.
module brenthash
  implicit none
  integer, parameter                  :: empty = -1
  integer, parameter                  :: key_prec = 8
  integer                             :: tablesize, inserted
  integer(kind=key_prec), allocatable :: table(:)
  integer,                allocatable :: values(:)

  contains

  subroutine init_table(t)
    integer, intent(in) :: t
    integer             :: i

    tablesize = t
    inserted  = 0
    
    allocate(table(0:tablesize-1))
    allocate(values(0:tablesize-1))

    table  = (/(empty, i = 0, tablesize-1)/)
    values = (/(empty, i = 0, tablesize-1)/)
  end subroutine init_table

  function hash1(v)
    integer(kind=key_prec), intent(in) :: v
    integer                            :: hash1

    hash1 = mod(v,tablesize)
  end function hash1

  function hash2(v)
    integer(kind=key_prec), intent(in) :: v
    integer                            :: hash2

    hash2 = mod(v,tablesize-2) + 1
  end function hash2

  subroutine insert(element,val)
    ! insert an element according to brent's algorithm. Value assigned to
    ! element if val != empty
    integer(kind=key_prec), intent(in) :: element
    integer,                intent(in) :: val
    integer                            :: depth,d,dd
    integer                            :: h1,h2,h,hh,c2

    if (tablesize .eq. inserted) then
      write(*,*) "ERROR: Hash table is full."
      stop
    end if

    if (val .eq. empty) then
      write(*,*) "ERROR: `val` equals value for `EMPTY`"
      write(*,*) "`val`:", val
      stop
    end if
    
    h1 = hash1(element)
    h2 = hash2(element)

    depth = 0
    h = h1
    ! find an empty slot or return if element already in table
    do while (table(h) .ne. empty)
      h = mod(h+h2,tablesize)
      if (element .eq. table(h)) then
        table(h)  = element
        if (val .ne. empty) values(h) = val
        return
      end if
      depth = depth + 1
    end do

    ! if depth(# of collisions) is smaller than 2 insert anyway
    if (depth .lt. 2) then
      table(h)  = element
      if (val .ne. empty) values(h) = val
      inserted  = inserted + 1
      return
    end if

    ! reduce number of collisions of element by moving previously entered
    ! elements in the table 
    d = 0
    h = h1
    do while (d .lt. depth)
      c2 = hash2(table(h))

      ! find empty slot hh with dd < d
      dd = 0
      hh = mod(h+c2,tablesize)
      do while (table(hh) .ne. empty .and. dd .le. d)
        hh = mod(hh+c2,tablesize)
        dd = dd + 1
      end do

      ! move previous element in h to hh
      if (dd .le. d) then
        table(hh)  = table(h)
        values(hh) = values(h)
        exit
      end if

      d = d + 1
      h = mod(h+h2,tablesize)
    end do

    table(h)  = element
    if (val .ne. empty) values(h) = val
    inserted = inserted + 1

  end subroutine insert

  function lookup(element)
    integer(kind=key_prec), intent(in) :: element
    integer                            :: h1,h2,lookup
  
    h2 = hash2(element)
    h1 = hash1(element)
    do while (table(h1) .ne. empty)
      if (table(h1) .eq. element) then
        exit
      end if
      h1 = mod(h1 + h2, tablesize)
    end do

    lookup = h1

  end function lookup

  subroutine deallocate_table
    deallocate(table)
  end subroutine deallocate_table

end module brenthash
