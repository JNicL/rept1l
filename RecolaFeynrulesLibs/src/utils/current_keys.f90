!******************************************************************************!
!                                                                              !
!    current_keys.f90                                                          !
!    is part of RECOLA2 (REcursive Computation of One Loop Amplitudes)2        !
!                                                                              !
!    Copyright (C) 2016-2020 Ansgar Denner, Jean-Nicolas Lang and              !
!                            Sandro Uccirati                                   !
!                                                                              !
!    RECOLA2 is licenced under the GNU GPL version 3,                          !
!    see COPYING for details.                                                  !
!                                                                              !
!******************************************************************************!

module current_keys

  use particle_keys, only: createParticleConfig
  use dictionary, only: dict,dict_get_value

!------------------------------------------------------------------------------!

  implicit none

  interface createCurrent
    module procedure createCurrent2, createCurrent3, createCurrent4,         &
    createCurrent5, createCurrent6, createCurrent7, createCurrent8
  end interface 

  interface get_vertex_mdl
    module procedure get_vertex3_mdl, get_vertex4_mdl, get_vertexN_mdl
  end interface

  contains

  subroutine createCurrent2(P1, P2, id, check)
    integer, intent(in) :: P1, P2, id
    logical, intent(in) :: check
    integer(kind=8) :: key
    integer :: res,address
    logical :: found
    call createParticleConfig(P1, P2, key)
    if (.not. check) then
      call dict(key, 2, address, found, id)
    else
      call dict(key, 1, address, found, 0)
      res = dict_get_value(address)
      write (*,*) "out=", res, found, address, "shouldbe = ", id
      if (res /= id) then
        stop 9
      end if 
    end if 
  end subroutine createCurrent2

  subroutine createCurrent3(P1, P2, P3, id, check)
    integer, intent(in) :: P1, P2, P3, id
    logical, intent(in) :: check
    integer(kind=8) :: key
    integer :: res,address
    logical :: found
    call createParticleConfig(P1, P2, P3, key)
    if (.not. check) then
      call dict(key, 2, address, found, id)
    else
      call dict(key, 1, address, found, 0)
      res = dict_get_value(address)
      write (*,*) "out=", res, found, address, "shouldbe = ", id
      if (res /= id) then
        stop 9
      end if 
    end if 
  end subroutine createCurrent3

  subroutine createCurrent4(P1, P2, P3, P4, id, check)
    integer, intent(in) :: P1, P2, P3, P4, id
    logical, intent(in) :: check
    integer(kind=8) :: key
    integer :: res,address
    logical :: found
    call createParticleConfig(P1, P2, P3, P4, key)
    if (.not. check) then
      call dict(key, 2, address, found, id)
    else
      call dict(key, 1, address, found, 0)
      res = dict_get_value(address)
      write (*,*) "out=", res, found, address, "shouldbe = ", id
      if (res /= id) then
        stop 9
      end if 
    end if 
  end subroutine createCurrent4

  subroutine createCurrent5(P1, P2, P3, P4, P5, id, check)
    integer, intent(in) :: P1, P2, P3, P4, P5, id
    logical, intent(in) :: check
    integer(kind=8) :: key
    integer :: res,address
    logical :: found
    call createParticleConfig(P1, P2, P3, P4, P5, key)
    if (.not. check) then
      call dict(key, 2, address, found, id)
    else
      call dict(key, 1, address, found, 0)
      res = dict_get_value(address)
      write (*,*) "out=", res, found, address, "shouldbe = ", id
      if (res /= id) then
        stop 9
      end if 
    end if 
  end subroutine createCurrent5

  subroutine createCurrent6(P1, P2, P3, P4, P5, P6, id, check)
    integer, intent(in) :: P1, P2, P3, P4, P5, P6, id
    logical, intent(in) :: check
    integer(kind=8) :: key
    integer :: res,address
    logical :: found
    call createParticleConfig(P1, P2, P3, P4, P5, P6, key)
    if (.not. check) then
      call dict(key, 2, address, found, id)
    else
      call dict(key, 1, address, found, 0)
      res = dict_get_value(address)
      write (*,*) "out=", res, found, address, "shouldbe = ", id
      if (res /= id) then
        stop 9
      end if 
    end if 
  end subroutine createCurrent6

  subroutine createCurrent7(P1, P2, P3, P4, P5, P6, P7, id, check)
    integer, intent(in) :: P1, P2, P3, P4, P5, P6, P7, id
    logical, intent(in) :: check
    integer(kind=8) :: key
    integer :: res,address
    logical :: found
    call createParticleConfig(P1, P2, P3, P4, P5, P6, P7, key)
    if (.not. check) then
      call dict(key, 2, address, found, id)
    else
      call dict(key, 1, address, found, 0)
      res = dict_get_value(address)
      write (*,*) "out=", res, found, address, "shouldbe = ", id
      if (res /= id) then
        stop 9
      end if 
    end if 
  end subroutine createCurrent7

  subroutine createCurrent8(P1, P2, P3, P4, P5, P6, P7, P8, id, check)
    integer, intent(in) :: P1, P2, P3, P4, P5, P6, P7, P8, id
    logical, intent(in) :: check
    integer(kind=8) :: key
    integer :: res,address
    logical :: found
    call createParticleConfig(P1, P2, P3, P4, P5, P6, P7, P8, key)
    if (.not. check) then
      call dict(key, 2, address, found, id)
    else
      call dict(key, 1, address, found, 0)
      res = dict_get_value(address)
      write (*,*) "out=", res, found, address, "shouldbe = ", id
      if (res /= id) then
        stop 9
      end if 
    end if 
  end subroutine createCurrent8

  subroutine get_vertex3_mdl(p1, p2, p3, vertex_ret)
    integer, intent(out) :: vertex_ret
    integer, intent(in)  :: p1, p2, p3
    integer(kind=8)      :: config
    integer              :: address
    logical              :: found_vertex

    call createParticleConfig(p1, p2, p3, config)
    call dict(config, 1, address, found_vertex, 0)
    if(found_vertex) then
      vertex_ret = dict_get_value(address)
    else
      vertex_ret = -1
    end if

  end subroutine get_vertex3_mdl

  subroutine get_vertex4_mdl(p1, p2, p3, p4, vertex_ret)
    integer, intent(out) :: vertex_ret
    integer, intent(in)  :: p1, p2, p3, p4
    integer(kind=8)      :: config
    integer              :: address
    logical              :: found_vertex

    call createParticleConfig(p1, p2, p3, p4, config)
    call dict(config, 1, address, found_vertex, 0)
    if(found_vertex) then
      vertex_ret = dict_get_value(address)
    else
      vertex_ret = -1
    end if
  end subroutine get_vertex4_mdl

  subroutine get_vertexN_mdl(particles, particleOut, vertex_ret)
    integer, intent(out)              :: vertex_ret
    integer, dimension(:), intent(in) :: particles
    integer, intent(in)               :: particleOut
    integer(kind=8)                   :: config
    integer                           :: address
    logical                           :: found_vertex

    call createParticleConfig(particles, particleOut, config)
    call dict(config, 1, address, found_vertex, 0)
    if(found_vertex) then
      vertex_ret = dict_get_value(address)
    else
      vertex_ret = -1
    end if

  end subroutine get_vertexN_mdl

!------------------------------------------------------------------------------!

end module current_keys  

!------------------------------------------------------------------------------!
