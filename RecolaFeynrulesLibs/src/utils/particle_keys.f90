!******************************************************************************!
!                                                                              !
!    particle_keys.f90                                                         !
!    is part of RECOLA2 (REcursive Computation of One Loop Amplitudes)2        !
!                                                                              !
!    Copyright (C) 2016-2020 Ansgar Denner, Jean-Nicolas Lang and              !
!                            Sandro Uccirati                                   !
!                                                                              !
!    RECOLA2 is licenced under the GNU GPL version 3,                          !
!    see COPYING for details.                                                  !
!                                                                              !
!******************************************************************************!

module particle_keys

!------------------------------------------------------------------------------!

  implicit none

  integer, parameter :: iprec = 8

  interface createParticleConfig
    module procedure createParticlesConfig2, createParticlesConfig3,  &
                     createParticlesConfig4, createParticlesConfig5,  &
                     createParticlesConfig6, createParticlesConfig7,  &
                     createParticlesConfig8, createParticlesConfigN2, &
                     createParticleConfigN
  end interface createParticleConfig

  contains

!------------------------------------------------------------------------------!

  subroutine createParticleConfigN(particles, config)
    integer(kind=iprec), intent(out) :: config
    integer,             intent(in)  :: particles(0:)
    integer                          :: i
    integer(kind=iprec)              :: tmp

    config = particles(0)
    do i = 1, size(particles)-1
      tmp = Int(particles(i), kind=iprec)
      config = config + ISHFT(tmp, i*iprec)
    end do
    return
  end subroutine createParticleConfigN

  subroutine writeParticlesConfig(config)
    integer(kind=iprec), intent(in) :: config
    integer                         :: particles(0:5)
    integer                         :: i

    particles(0) = Int(iand(config,Z'FF'), kind=4)
    i = 1
    write (*, *) "p",1,":", particles(0) - 1
    do while( iand(ISHFT(config,-iprec*i), Z'FF') .ne. 0)
      particles(i) = Int(iand(ISHFT(config,-iprec*i), Z'FF'), kind=4)
      i = i+1
    write (*, *) "p",i, ":", particles(i-1)
    end do
    particles(i) = 0
    return

  end subroutine writeParticlesConfig

  subroutine getParticlesConfig(config, particles)
    integer(kind=iprec), intent(in)  :: config
    integer,             intent(out) :: particles(0:)
    integer                          :: i

    particles(0) = Int(iand(config,Z'FF'), kind=4)
    i = 1
    do while( iand(ISHFT(config,-iprec*i), Z'FF') .ne. 0)
      particles(i) = Int(iand(ISHFT(config,-iprec*i), Z'FF'), kind=4)
      i = i+1
    end do
    particles(i) = 0
    return
  end subroutine getParticlesConfig

  subroutine createParticlesConfig2(p1, p2, config)
    integer(kind=iprec), intent(out) :: config
    integer,             intent(in)  :: p1, p2
    integer, dimension(0:1)          :: particles

    particles(0) = p1
    particles(1) = p2

    call createParticleConfigN(particles, config)

  end subroutine createParticlesConfig2

  subroutine createParticlesConfig3(p1, p2, p3, config)
    integer(kind=iprec), intent(out) :: config
    integer,             intent(in)  :: p1, p2, p3
    integer, dimension(0:2)          :: particles

    particles(0) = p1
    particles(1) = p2
    particles(2) = p3

    call createParticleConfigN(particles, config)

  end subroutine createParticlesConfig3

  subroutine createParticlesConfig4(p1, p2, p3, p4, config)
    integer(kind=iprec), intent(out) :: config
    integer,             intent(in)  :: p1, p2, p3, p4
    integer, dimension(0:3)          :: particles

    particles(0) = p1
    particles(1) = p2
    particles(2) = p3
    particles(3) = p4

    call createParticleConfigN(particles, config)

  end subroutine createParticlesConfig4

  subroutine createParticlesConfig5(p1, p2, p3, p4, p5, config)
    integer(kind=iprec), intent(out) :: config
    integer,             intent(in)  :: p1, p2, p3, p4, p5
    integer, dimension(0:4)          :: particles

    particles(0) = p1
    particles(1) = p2
    particles(2) = p3
    particles(3) = p4
    particles(4) = p5

    call createParticleConfigN(particles, config)

  end subroutine createParticlesConfig5

  subroutine createParticlesConfig6(p1, p2, p3, p4, p5, p6, config)
    integer(kind=iprec), intent(out) :: config
    integer,             intent(in)  :: p1, p2, p3, p4, p5, p6
    integer, dimension(0:5)          :: particles

    particles(0) = p1
    particles(1) = p2
    particles(2) = p3
    particles(3) = p4
    particles(4) = p5
    particles(5) = p6

    call createParticleConfigN(particles, config)

  end subroutine createParticlesConfig6

  subroutine createParticlesConfig7(p1, p2, p3, p4, p5, p6, p7, config)
    integer(kind=iprec), intent(out) :: config
    integer,             intent(in)  :: p1, p2, p3, p4, p5, p6, p7
    integer, dimension(0:6)          :: particles

    particles(0) = p1
    particles(1) = p2
    particles(2) = p3
    particles(3) = p4
    particles(4) = p5
    particles(5) = p6
    particles(6) = p7

    call createParticleConfigN(particles, config)

  end subroutine createParticlesConfig7

  subroutine createParticlesConfig8(p1, p2, p3, p4, p5, p6, p7, p8, config)
    integer(kind=iprec), intent(out) :: config
    integer,             intent(in)  :: p1, p2, p3, p4, p5, p6, p7, p8
    integer, dimension(0:7)          :: particles

    particles(0) = p1
    particles(1) = p2
    particles(2) = p3
    particles(3) = p4
    particles(4) = p5
    particles(5) = p6
    particles(6) = p7
    particles(7) = p8

    call createParticleConfigN(particles, config)

  end subroutine createParticlesConfig8

  subroutine createParticlesConfigN2(particles, pOut, config)
    integer(kind=iprec),    intent(out) :: config
    integer, dimension(0:), intent(in)  :: particles
    integer,                intent(in)  :: pOut
    integer(kind=iprec)                 :: tmp
    integer                             :: psize,i

    psize = size(particles)
    config = particles(0)
    do i = 1, psize - 1
      tmp = Int(particles(i), kind=iprec)
      config = config + ISHFT(tmp, i*iprec)
    end do
    tmp = Int(pOut, kind=iprec)
    config = config + ISHFT(tmp, psize*iprec)
  end subroutine createParticlesConfigN2

!------------------------------------------------------------------------------!

end module particle_keys

!------------------------------------------------------------------------------!
