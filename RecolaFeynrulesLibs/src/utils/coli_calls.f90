!******************************************************************************!
!                                                                              !
!    coli_calls.f90                                                            !
!    is part of RECOLA2 (REcursive Computation of One Loop Amplitudes)2        !
!                                                                              !
!    Copyright (C) 2016-2020 Ansgar Denner, Jean-Nicolas Lang and              !
!                            Sandro Uccirati                                   !
!                                                                              !
!    RECOLA2 is licenced under the GNU GPL version 3,                          !
!    see COPYING for details.                                                  !
!                                                                              !
!******************************************************************************!

! Wrapper to coli functions

module coli_calls
  use coli_interface

!------------------------------------------------------------------------------!

  implicit none

  contains

!------------------------------------------------------------------------------!

!-------------!
!-  3-point  -!
!-------------!

  function A0_cll_wrap(m02) result(A0)
    double complex, intent(in) :: m02
    double complex             :: A0

    call A0_col_i(A0, m02)
  end function A0_cll_wrap

  function A00_cll_wrap(m02) result(A00)
    double complex, intent(in) :: m02
    double complex             :: A00, A0

    call A0_col_i(A0, m02)
    A00 = m02*(A0 + m02/2d0)/4d0
  end function A00_cll_wrap

  function A0000_cll_wrap(m02) result(A0000)
    double complex, intent(in) :: m02
    double complex             :: A0000, A0

    call A0_col_i(A0, m02)
    A0000 = m02*(A0 + m02*(1d0/2d0+1d0/3d0))/24d0
  end function A0000_cll_wrap

  function A000000_cll_wrap(m02) result(A000000)
    double complex, intent(in) :: m02
    double complex             :: A000000, A0

    call A0_col_i(A0, m02)
    A000000 = m02*(A0 + m02*(1d0/2d0+1d0/3d0+1d0/4d0))/192d0
  end function A000000_cll_wrap

!-------------!
!-  2-point  -!
!-------------!

  function B0_cll_wrap(p12, m02, m12) result(B0)
    double complex, intent(in) :: p12, m02, m12
    double complex             :: B0, Buv

    call B0_col_i(B0, p12, m02, m12)
  end function B0_cll_wrap

  function DB0_cll_wrap(p12, m02, m12) result(DB0)
    double complex, intent(in) :: p12, m02, m12
    double complex             :: DB0, DBuv

    call DB0_col_i(DB0, p12, m02, m12)
  end function DB0_cll_wrap

!-----------!
!-  Rank1  -!
!-----------!

  function B1_cll_wrap(p12, m02, m12) result(B1)
    double complex, intent(in) :: p12, m02, m12
    double complex             :: B1

    call B1_col_i(B1, p12, m02, m12)
  end function B1_cll_wrap

  function DB1_cll_wrap(p12, m02, m12) result(DB1)
    double complex, intent(in) :: p12, m02, m12
    double complex             :: DB1, DB1uv

    call DB1_col_i(DB1, p12, m02, m12)
  end function DB1_cll_wrap

!-----------!
!-  Rank2  -!
!-----------!

  function B00_cll_wrap(p12, m02, m12) result(B00)
    double complex, intent(in) :: p12, m02, m12
    double complex             :: B00

    call B00_col_i(B00, p12, m02, m12)
  end function B00_cll_wrap

  function DB00_cll_wrap(p12, m02, m12) result(DB00)
    double complex, intent(in) :: p12, m02, m12
    double complex             :: DB00

    call DB00_col_i(DB00, p12, m02, m12)
  end function DB00_cll_wrap

  function B11_cll_wrap(p12, m02, m12) result(B11)
    double complex, intent(in) :: p12, m02, m12
    double complex             :: B11

    call B11_col_i(B11, p12, m02, m12)
  end function B11_cll_wrap

  function DB11_cll_wrap (xp,xm02,xm12) result (DB11)
    double complex, intent (in) :: xp, xm02, xm12
    double complex :: DB11
    
    call DB11_col_i(DB11, xp, xm02, xm12)
  end function DB11_cll_wrap

!-----------!
!-  Rank3  -!
!-----------!

  function B001_cll_wrap(p12, m02, m12) result(B001)
    double complex, intent(in) :: p12, m02, m12
    double complex             :: B001

    call B001_col_i(B001, p12, m02, m12)
  end function B001_cll_wrap

  function DB001_cll_wrap (p12,m02,m12) result (DB001)

    double complex, intent (in) :: p12, m02, m12
    double complex :: DB001
    
    DB001 = (2*m02*DB1_cll_wrap(p12,m02,m12)+ &
             (p12-m12+m02)*DB11_cll_wrap(p12,m02,m12) + &
             B11_cll_wrap(p12,m02,m12))/8d0 + 1d0/48d0
  end function DB001_cll_wrap

  function B111_cll_wrap(p12, m02, m12) result(B111)
    double complex, intent(in) :: p12, m02, m12
    double complex             :: B111

    call B111_col_i(B111, p12, m02, m12)
  end function B111_cll_wrap

  function DB111_cll_wrap (p12,m02,m12) result (DB111)

    double complex, intent (in) :: p12, m02, m12
    double complex :: DB111
    
    call DB111_col_i(DB111, p12, m02, m12)
  end function DB111_cll_wrap

!-----------!
!-  Rank4  -!
!-----------!

  function B0000_cll_wrap (p12,m02,m12) result (B0000)

    double complex, intent (in) :: p12, m02, m12
    double complex :: B0000
    
    call B0000_col_i(B0000, p12, m02, m12)
  end function B0000_cll_wrap

  function DB0000_cll_wrap (p12,m02,m12) result (DB0000)

    double complex, intent (in) :: p12, m02, m12
    double complex :: DB0000
    
    DB0000 = (+2*m02*DB00_cll_wrap(p12,m02,m12)+ &
              (p12-m12+m02)*DB001_cll_wrap(p12,m02,m12) + &
              B001_cll_wrap(p12,m02,m12) + &
              p12/30d0 - (m02+m12)/12d0 &
              )/10d0
  end function DB0000_cll_wrap

  function B0011_cll_wrap (p12,m02,m12) result (B0011)

    double complex, intent (in) :: p12, m02, m12
    double complex :: B0011
    
    call B0011_col_i(B0011, p12, m02, m12)
  end function B0011_cll_wrap

  function DB0011_cll_wrap (p12,m02,m12) result (DB0011)

    double complex, intent (in) :: p12, m02, m12
    double complex :: DB0011

    DB0011 = (+2*m02*DB11_cll_wrap(p12,m02,m12)+ &
              (p12-m12+m02)*DB111_cll_wrap(p12,m02,m12)+ &
              B111_cll_wrap(p12,m02,m12)-1d0/10d0)/10d0
  end function DB0011_cll_wrap

  function B1111_cll_wrap (p12,m02,m12) result (B1111)

    double complex, intent (in) :: p12, m02, m12
    double complex :: B1111
    
    call B1111_col_i(B1111, p12, m02, m12)
  end function B1111_cll_wrap

  function DB1111_cll_wrap (p12,m02,m12) result (DB1111)

    double complex, intent (in) :: p12, m02, m12
    double complex :: DB1111
    
    call DB1111_col_i(DB1111, p12, m02, m12)
  end function DB1111_cll_wrap

!-----------!
!-  Rank5  -!
!-----------!

  function B00001_cll_wrap (p12,m02,m12) result (B00001)

    double complex, intent (in) :: p12, m02, m12
    double complex :: B00001
    
    call B00001_col_i(B00001, p12, m02, m12)
  end function B00001_cll_wrap

  function DB00001_cll_wrap (p12,m02,m12) result (DB00001)

    double complex, intent (in) :: p12, m02, m12
    double complex :: DB00001
    write (*, *) 'DB00001 not validated yet'

    DB00001 = -(B0011_cll_wrap(p12,m02,m12) + &
                (p12-m12+m02)*DB0011_cll_wrap(p12,m02,m12) + &
                2*B00111_cll_wrap(p12,m02,m12) + &
                2*p12*DB00111_cll_wrap(p12,m02,m12))/4d0
  end function DB00001_cll_wrap

  function B00111_cll_wrap (p12,m02,m12) result (B00111)

    double complex, intent (in) :: p12, m02, m12
    double complex :: B00111
    
    call B00111_col_i(B00111, p12, m02, m12)
  end function B00111_cll_wrap

  function DB00111_cll_wrap (p12,m02,m12) result (DB00111)

    double complex, intent (in) :: p12, m02, m12
    double complex :: DB00111
    write (*, *) 'DB00111 not validated yet'

    DB00111 = -(B1111_cll_wrap(p12,m02,m12) + &
                (p12-m12+m02)*DB1111_cll_wrap(p12,m02,m12) + &
                2*B11111_cll_wrap(p12,m02,m12) + &
                2*p12*DB11111_cll_wrap(p12,m02,m12))/4d0
  end function DB00111_cll_wrap

  function B11111_cll_wrap (p12,m02,m12) result (B11111)

    double complex, intent (in) :: p12, m02, m12
    double complex :: B11111
    
    call B11111_col_i(B11111, p12, m02, m12)
  end function B11111_cll_wrap

  function DB11111_cll_wrap (p12,m02,m12) result (DB11111)

    double complex, intent (in) :: p12, m02, m12
    double complex :: DB11111
    
    call DB11111_col_i(DB11111, p12, m02, m12)
  end function DB11111_cll_wrap

!-----------!
!-  Rank6  -!
!-----------!

  function B000000_cll_wrap (p12,m02,m12) result (B000000)

    double complex, intent (in) :: p12, m02, m12
    double complex :: B000000
    write (*, *) 'B000000 not validated yet'

    B000000 = -(-A0000_cll_wrap(m12) + &
                (p12-m12+m02)*B00001_cll_wrap(p12,m02,m12) + &
                2*p12*B000011_cll_wrap(p12,m02,m12))/2d0
  end function B000000_cll_wrap

  function DB000000_cll_wrap (p12,m02,m12) result (DB000000)

    double complex, intent (in) :: p12, m02, m12
    double complex :: DB000000
    write (*, *) 'DB000000 not validated yet'

    DB000000 = -(B00001_cll_wrap(p12,m02,m12) + &
                 (p12-m12+m02)*DB00001_cll_wrap(p12,m02,m12) + &
                  2*B000011_cll_wrap(p12,m02,m12) + &
                  2*p12*DB000011_cll_wrap(p12,m02,m12))/2d0
  end function DB000000_cll_wrap

  function B000011_cll_wrap (p12,m02,m12) result (B000011)

    double complex, intent (in) :: p12, m02, m12
    double complex :: B000011
    
    call B000011_col_i(B000011, p12, m02, m12)
  end function B000011_cll_wrap

  function DB000011_cll_wrap (p12,m02,m12) result (DB000011)

    double complex, intent (in) :: p12, m02, m12
    double complex :: DB000011
    write (*, *) 'DB000011 not validated yet'

    DB000011 = -(B00111_cll_wrap(p12,m02,m12) + &
                 (p12-m12+m02)*DB00111_cll_wrap(p12,m02,m12) + &
                  2*B001111_cll_wrap(p12,m02,m12) + &
                  2*p12*DB001111_cll_wrap(p12,m02,m12))/6d0
  end function DB000011_cll_wrap

  function B001111_cll_wrap (p12,m02,m12) result (B001111)

    double complex, intent (in) :: p12, m02, m12
    double complex :: B001111
    
    call B001111_col_i(B001111, p12, m02, m12)
  end function B001111_cll_wrap

  function DB001111_cll_wrap (p12,m02,m12) result (DB001111)

    double complex, intent (in) :: p12, m02, m12
    double complex :: DB001111
    write (*, *) 'DB001111 not validated yet'

    DB001111 = -(B11111_cll_wrap(p12,m02,m12) + &
                 (p12-m12+m02)*DB11111_cll_wrap(p12,m02,m12) + &
                  2*B111111_cll_wrap(p12,m02,m12) + &
                  2*p12*DB111111_cll_wrap(p12,m02,m12))/10d0
  end function DB001111_cll_wrap

  function B111111_cll_wrap (p12,m02,m12) result (B111111)

    double complex, intent (in) :: p12, m02, m12
    double complex :: B111111
    
    call B111111_col_i(B111111, p12, m02, m12)
  end function B111111_cll_wrap

  function DB111111_cll_wrap (p12,m02,m12) result (DB111111)

    double complex, intent (in) :: p12, m02, m12
    double complex :: DB111111
    
    call DB111111_col_i(DB111111, p12, m02, m12)
  end function DB111111_cll_wrap

!------------------------------------------------------------------------------!

end module coli_calls

!------------------------------------------------------------------------------!
