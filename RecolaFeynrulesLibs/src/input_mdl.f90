!******************************************************************************!
!                                                                              !
!    input_mdl.f90                                                             !
!    is part of RECOLA2 (REcursive Computation of One Loop Amplitudes)2        !
!                                                                              !
!    Copyright (C) 2016-2020 Ansgar Denner, Jean-Nicolas Lang and              !
!                            Sandro Uccirati                                   !
!                                                                              !
!    RECOLA2 is licenced under the GNU GPL version 3,                          !
!    see COPYING for details.                                                  !
!                                                                              !
!******************************************************************************!

module input_mdl

  use constants_mdl, only: dp

!------------------------------------------------------------------------------!

  implicit none

!------------------------------------------------------------------------------!

  ! Define values for UV and IR dimensional regulators;
  real(dp), parameter :: &
  delta_uv = 0d0,        &
  delta_ir = 0d0,        &
  delta_ir_2 = 0,        &
  mu_uv = 100d0,         &
  mu_ir = 100d0,         &
  mu_ms = 91.1876d0

  real(dp) :: muMS = mu_ms
  real(dp) :: DeltaUV = delta_uv, muUV = mu_uv
  real(dp) :: DeltaIR = delta_ir, DeltaIR2 = delta_ir_2, muIR = mu_ir

  ! cut on vanishing dp
  real(dp), parameter :: zerocut = 1d-15

  real(dp), parameter :: masscut_init = 1d0
  real(dp):: masscut = masscut_init

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!  Output and Error options  !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  ! output channel, by default stdout 
  integer            :: nx            = 6
  integer            :: warnings      = 0
  integer, parameter :: warning_limit = 100
  integer            :: errors        = 0
  integer, parameter :: error_limit   = 100

  ! collier output directory
  character(999) :: collier_output_dir = 'default'

  ! The ifail flag regulates the behaviour of Recola in case of error.
  ! ifail =  0 -> the code does not stop, when an error occurs
  ! ifail = -1 -> the code stops, when an error occurs
  ! On output (which can be got by the user calling get_ifail_mdl):
  ! ifail = 0 -> no errors occured
  ! ifail = 1 -> an error occured because of a wrong call of user's
  !              subroutines
  ! ifail = 2 -> an error occured because of an internal bug of Recola
  integer :: ifail = -1

  ! The iwarn flag regulates the behaviour of Recola in case of warning.
  ! iwarn =  0 -> the code does not stop, when a warning occurs
  ! iwarn = -1 -> the code stops, when a warning occurs
  ! On output (which can be got by the user calling get_iwarn_mdl):
  ! iwarn = 0 -> no warnings occured
  ! iwarn = 1 -> a warning occured because of a wrong call of user's
  !              subroutines
  ! iwarn = 2 -> a warning occured because of an internal bug of Recola
  integer :: iwarn = 0

  ! Masses which are below zerocut are considered massless (`M0`) in the form output
  logical :: masscut_frm = .false.

  ! use the complex mass symbols instead of real ones i.e. M -> cM
  logical :: complex_mass_frm = .false.

  ! enable computing scalar integrals (SIs) via collier (or coli)
  ! if .false. collier (or coli) is not initialized and SIs are set to zero.
  logical :: init_si = .true.

  ! Set true/false if the mass/dimensional regularization of collinear
  ! divergences should be printed
  logical :: print_regularization_scheme = .false.

!------------------------------------------------------------------------------!

  contains

!------------------------------------------------------------------------------!

  subroutine set_collier_output_dir_mdl (dir)

    character(*), intent(in) :: dir

    collier_output_dir = trim(dir)

  end subroutine set_collier_output_dir_mdl

!------------------------------------------------------------------------------!

  subroutine set_output_file_mdl(nx_in)
    integer, intent(in) :: nx_in
    nx = nx_in
  end subroutine set_output_file_mdl

!------------------------------------------------------------------------------!

  subroutine set_delta_uv_mdl (d)

  ! This subroutine sets the UV-pole 
  ! "deltaUV" = 1/ep - EulerConstant + ln(4*pi) to "d"

  real(dp), intent(in) :: d

  deltaUV = d

  end subroutine set_delta_uv_mdl

!------------------------------------------------------------------------------!

  function get_delta_uv_mdl ()

  ! This function gets the UV-pole 
  ! "deltaUV" = 1/ep - EulerConstant + ln(4*pi)

  real(dp) :: get_delta_uv_mdl

  get_delta_uv_mdl = deltaUV

  end function get_delta_uv_mdl

!------------------------------------------------------------------------------!

  subroutine set_delta_ir_mdl (d,d2)

  ! This subroutine sets the IR-pole 
  ! "deltaIR" = 1/ep - EulerConstant + ln(4*pi) to "d"

  real(dp), intent(in) :: d,d2

  deltaIR  = d
  deltaIR2 = d2

  end subroutine set_delta_ir_mdl

!------------------------------------------------------------------------------!

  subroutine get_delta_ir_mdl (d,d2)

  ! This subroutine gets the IR-pole 
  ! "deltaIR" = 1/ep - EulerConstant + ln(4*pi)

  real(dp), intent(out) :: d,d2

  d  = deltaIR
  d2 = deltaIR2

  end subroutine get_delta_ir_mdl

!------------------------------------------------------------------------------!

  subroutine set_mu_uv_mdl (m)

  ! This subroutine sets UV-mass "muUV" to "m"

  real(dp), intent(in) :: m

  muUV = m

  end subroutine set_mu_uv_mdl
   
!------------------------------------------------------------------------------!

  function get_mu_uv_mdl ()

  ! This function gets UV-mass "muUV"

  real(dp) :: get_mu_uv_mdl

  get_mu_uv_mdl = muUV

  end function get_mu_uv_mdl

!------------------------------------------------------------------------------!

  subroutine set_mu_ms_mdl (m)

  ! This subroutine sets MS-mass "muMS" to "m"

  real(dp), intent(in) :: m

  muMS = m

  end subroutine set_mu_ms_mdl

!------------------------------------------------------------------------------!

  function get_mu_ms_mdl ( )

  ! This function sets MS-mass "muMS"

  real(dp) :: get_mu_ms_mdl

  get_mu_ms_mdl = muMS

  end function get_mu_ms_mdl

!------------------------------------------------------------------------------!

  subroutine set_mu_ir_mdl (m)

  ! This subroutine sets IR-mass "muIR" to "m"

  real(dp), intent(in) :: m

  muIR = m

  end subroutine set_mu_ir_mdl

!------------------------------------------------------------------------------!

  function get_mu_ir_mdl ()

  ! This function gets IR-mass "muIR"

  real(dp) :: get_mu_ir_mdl

  get_mu_ir_mdl = muIR

  end function get_mu_ir_mdl

!------------------------------------------------------------------------------!

  subroutine use_complex_mass_form_mdl(val)
    logical, intent(in) :: val
    complex_mass_frm = val
  end subroutine use_complex_mass_form_mdl

!------------------------------------------------------------------------------!

  function get_complex_mass_form_mdl()
    logical :: get_complex_mass_form_mdl
    get_complex_mass_form_mdl =  complex_mass_frm
  end function get_complex_mass_form_mdl

!------------------------------------------------------------------------------!

  subroutine use_masscut_form_mdl(val)
    
    ! enables the masscut in form amplitudes. Masses below masscut are
    ! considered massless. 

    logical, intent(in) :: val
    masscut_frm = val
  end subroutine use_masscut_form_mdl

!------------------------------------------------------------------------------!

  subroutine set_compute_si_mdl(initsi)

    ! This subroutine sets the switch for computing scalar integrals
    ! initsi = .false. -> SIs are not computed
    ! initsi = .true -> SIs are computed 

    logical, intent(in) :: initsi

    init_si = initsi

  end subroutine set_compute_si_mdl

!------------------------------------------------------------------------------!

  subroutine set_print_regularization_scheme_mdl(prs)

    ! This subroutine sets the switch whether regularization scheme choices
    ! are printed
    ! prs = .false. -> regularization scheme not printed
    ! prs = .true -> regularization scheme not printed 

    logical, intent(in) :: prs

    print_regularization_scheme = prs

  end subroutine set_print_regularization_scheme_mdl

!------------------------------------------------------------------------------!

  subroutine error_mdl(errmsg,where)
    ! Prints the error `errmsg' on the screen. Optionally,
    ! `where' can be passed to indicate in which subroutine/module the error
    ! occured.
    character(len=*),           intent(in) :: errmsg
    character(len=*), optional, intent(in) :: where
    errors = errors + 1
    if (errors .le. error_limit+1) then 
      write(nx,*)
      if (present(where)) then
        write(nx,*) 'ERROR in ' //where//': '
        write(nx,*) errmsg
      else
        write(nx,*) 'ERROR: ', errmsg
      end if
    end if
    call istop_mdl (ifail,1)
  end subroutine error_mdl

!------------------------------------------------------------------------------!

  subroutine warning_mdl(warnmsg,where)
    ! Prints the warning `warnmsg' on the screen. Optionally,
    ! `where' can be passed to indicate in which subroutine/module the warning
    ! occured.
    character(len=*),           intent(in) :: warnmsg
    character(len=*), optional, intent(in) :: where
    errors = errors + 1
    if (errors .le. error_limit+1) then 
      write(nx,*)
      if (present(where)) then
        write(nx,*) 'WARNING in ' //where//': '
        write(nx,*) warnmsg
      else
        write(nx,*) 'WARNING: ', warnmsg
      end if
    end if
    call istop_mdl (iwarn,1)
  end subroutine warning_mdl

!------------------------------------------------------------------------------!

  subroutine istop_mdl (i,n) 

  integer, intent(inout) :: i
  integer, intent(in)    :: n

  select case (i)
    case (-1);    i = n; stop
    case (0);     i = n
    case default; i = n
  end select

  end subroutine istop_mdl

!------------------------------------------------------------------------------!

end module input_mdl

!------------------------------------------------------------------------------!
