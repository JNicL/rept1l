!******************************************************************************!
!                                                                              !
!    modelfile.f90                                                             !
!    is part of RECOLA2 (REcursive Computation of One Loop Amplitudes)2        !
!                                                                              !
!    Copyright (C) 2016-2020 Ansgar Denner, Jean-Nicolas Lang and              !
!                            Sandro Uccirati                                   !
!                                                                              !
!    RECOLA2 is licenced under the GNU GPL version 3,                          !
!    see COPYING for details.                                                  !
!                                                                              !
!******************************************************************************!

module modelfile

 use input_mdl, only:                   &
   set_collier_output_dir_mdl,          &
   set_output_file_mdl,                 &
   set_delta_uv_mdl,                    &
   get_delta_uv_mdl,                    &
   set_delta_ir_mdl,                    &
   get_delta_ir_mdl,                    &
   set_mu_uv_mdl,                       &
   get_mu_uv_mdl,                       &
   set_mu_ms_mdl,                       &
   get_mu_ms_mdl,                       &
   set_mu_ir_mdl,                       &
   get_mu_ir_mdl,                       &
   set_mu_ir_mdl,                       &
   get_mu_ir_mdl,                       &
   use_masscut_form_mdl,                &
   set_compute_si_mdl,                  &
   set_print_regularization_scheme_mdl, &
   use_complex_mass_form_mdl,           &
   get_complex_mass_form_mdl

 use class_particles, only:        &
   get_modelname_mdl,              &
   get_modelgauge_mdl,             &
   has_feature_mdl,                &
   get_driver_timestamp_mdl,       &
   get_cmass2_mdl,                 &
   get_cmass2_reg_mdl,             &
   get_particle_cmass2_mdl,        &
   get_particle_cmass2_reg_mdl,    &
   get_particle_mass_reg_mdl,      &
   get_particle_mass_id_mdl,       &
   get_n_masses_mdl,               &
   get_n_particles_mdl,            &
   get_order_id_mdl,               &
   get_n_orders_mdl,               &
   get_max_polarizations_mdl,      &
   is_particle_model_init_mdl,     &
   set_particle_model_init_mdl,    &
   init_particles_mdl,             &
   clear_particles_mdl,            &
   get_doublet_partner_mdl,        &
   is_firstgen_mdl,                &
   get_secondgen_mdl,              &
   get_thirdgen_mdl,               &
   get_particle_id_mdl,            &
   get_particle_antiparticle_mdl,  &
   get_particle_polarizations_mdl, &
   is_particle_mdl,                &
   get_particle_spin_mdl,          &
   get_particle_colour_mdl,        &
   get_particle_width_mdl,         &
   get_particle_name_mdl,          &
   get_particle_texname_mdl,       &
   is_backgroundfield_mdl,         &
   is_quantumfield_mdl,            &
   is_resonant_particle_id_mdl,    &
   get_antiparticle_id_mdl,        &
   get_mass_name_mdl,              &
   set_parameter_mdl,              &
   get_parameter_mdl,              &
   set_masscut_mdl,                &
   print_parameters_mdl,           &
   set_resonant_particle_mdl,      &
   set_light_particle_mdl,         &
   unset_light_particle_mdl,       &
   get_particle_type_mdl,          &
   get_particle_type2_mdl,         &
   get_axodraw_propagator_mdl

 use class_vertices, only:         &
   is_model_init_mdl,              &
   is_model_filled_mdl,            &
   get_max_leg_multiplicity_mdl,   &
   get_lorentz_rank_mdl,           &
   get_recola_base_mdl,            &
   get_fourfermion_id_mdl,         &
   get_vertex_lorentz_rank_mdl,    &
   get_propagator_extension_mdl,   &
   get_colourfac_mdl,              &
   get_colourfac_form_mdl,         &
   get_vertex_branch_status_mdl,   &
   dealloc_vertex_mdl,             &
   get_vertex_colour_branches_mdl, &
   get_vertex_pext_mdl,            &
   get_vertex_order_increase_mdl,  &
   get_vertex_lorentz_type_mdl,    &
   get_vertex_perm_mdl,            &
   get_vertex_colour_id_mdl,       &
   is_zero_coupling_vertex_mdl,    &
   get_vertex_ncouplings_mdl,      &
   get_vertex_coupling_ids_mdl,    &
   init_vertices_mdl,              &
   check_vertices_mdl

 use fill_vertices, only:          &
   fill_vertices_mdl,              &
   reset_vertices_mdl,             &
   clear_vertices_mdl

 use class_couplings, only: &
   get_coupling_value_mdl

 use class_tree, only: &
   compute_tree_mdl,   &
   helicity_conservation_mdl

 use class_loop, only: &
   compute_loop_mdl

 use class_colourflow, only:       &
   compute_colour_correlation_mdl

 use class_form, only: &
   form_current_mdl

 use class_ctparameters, only:  &
   set_complex_mass_scheme_mdl, &
   set_on_shell_scheme_mdl

 use constants_mdl, only: &
   gg,                    &
   incRI

 use fill_ctparameters, only: set_renoscheme_mdl
 use fill_ctparameters, only: get_renoscheme_mdl
 use fill_ctparameters, only: print_renoschemes_mdl
 use fill_ctparameters, only: print_counterterms_mdl

 use current_keys, only: get_vertex_mdl

!------------------------------------------------------------------------------!

  implicit none

  contains

!------------------------------------------------------------------------------!

  subroutine reset_couplings_mdl
    use class_particles
    use fill_couplings, only: fill_couplings_mdl, fill_CTcouplings_mdl
    use fill_ctparameters, only: init_CTparameters_mdl
    call clear_particles_mdl
    call init_particles_mdl
    call fill_couplings_mdl
    call init_CTparameters_mdl(.false.)
    call fill_CTcouplings_mdl
  end subroutine reset_couplings_mdl

  subroutine reset_ctcouplings_mdl
    use fill_couplings, only: fill_CTcouplings_mdl
    use fill_ctparameters, only: init_CTparameters_mdl
    call init_CTparameters_mdl(.false.)
    call fill_CTcouplings_mdl
  end subroutine reset_ctcouplings_mdl

!------------------------------------------------------------------------------!

end module modelfile

!------------------------------------------------------------------------------!
