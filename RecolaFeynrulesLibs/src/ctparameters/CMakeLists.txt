cmake_minimum_required (VERSION 2.8)

# counterterm parameters 
project (ctparameters)

# Define ctparameters source files
set (CT_FILES "")
file (GLOB_RECURSE CT_FILES ${CMAKE_CURRENT_SOURCE_DIR}/*.f90)

set(SOURCE
   ${SOURCE}
   ${CT_FILES}
   PARENT_SCOPE
)
