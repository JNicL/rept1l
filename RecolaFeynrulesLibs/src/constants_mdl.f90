!******************************************************************************!
!                                                                              !
!    constants_mdl.f90                                                         !
!    is part of RECOLA2 (REcursive Computation of One Loop Amplitudes)2        !
!                                                                              !
!    Copyright (C) 2016-2020 Ansgar Denner, Jean-Nicolas Lang and              !
!                            Sandro Uccirati                                   !
!                                                                              !
!    RECOLA2 is licenced under the GNU GPL version 3,                          !
!    see COPYING for details.                                                  !
!                                                                              !
!******************************************************************************!

module constants_mdl

!------------------------------------------------------------------------------!

  implicit none

  integer,      parameter :: &
    sp = kind (10e0), & ! single precision
    dp = kind (23d0)    ! double    "

  real (dp),    parameter :: &
    nul   = 0d0, &
    one   = 1d0, &
    two   = 2d0, &
    three = 3d0

  complex (dp), parameter :: &
    cnul   =   (0d0  , 0d0), &
    cone   =   (1d0  , 0d0), &
    ctwo   =   (2d0  , 0d0), &
    cima   =   (0d0  , 1d0)

  real (dp),    parameter :: &
    sq2 = 1.414213562373095048801689d0, &
    pi  = 3.141592653589793238462643d0

  complex (dp), parameter :: &
    csq2 = (1.414213562373095048801689d0,0d0)

  integer,  parameter :: nCs =  3  ! number of colors
  real(dp), parameter :: Nc = real(nCs,kind=dp)
  real(dp), parameter :: Nc2 = Nc**2

  integer, parameter :: TreeBranch=0, LoopBranch=1, CTBranch=2, R2Branch=3

  ! the following quantities are filled by recola
  integer :: gg(0:3,0:3) ! Metric
  integer, allocatable :: incRI(:,:) ! rank increase by one given the ri and
                                     ! lorentz index

!------------------------------------------------------------------------------!

end module constants_mdl

!------------------------------------------------------------------------------!
