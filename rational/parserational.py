#==============================================================================#
#                               parserational.py                               #
#==============================================================================#

#============#
#  Includes  #
#============#

import os
import re
from sympy import Symbol, together, cancel
from six import with_metaclass, iteritems

from rept1l.formutils import FormPype
from rept1l.coupling import RCoupling as FRC
from rept1l.baseutils import CurrentBase
from rept1l.parsing import FormParse
from rept1l.helper_lib import StorageProperty, get_particle
import rept1l.Model as Model

#===========#
#  Globals  #
#===========#

model = Model.model

#==============================================================================#
#                                ParseRational                                 #
#==============================================================================#

class ParseRational(with_metaclass(StorageProperty,FormParse)):

  # StorageMeta attributes. Those attributes are stored when `store` is called.
  storage = ['rational_dict', 'zero_r2']
  mpath, _ = model.get_modelpath()
  path = os.path.join(mpath, 'ModelRational')
  # StorageMeta filename
  name = 'rational_data.txt'

  FRC.load()

  def __init__(self, treevertices, particles):
    super(ParseRational, self).__init__()
    self.treevertices = treevertices
    self.particles = particles
    self.amplitudes = {}

  def group_similar(self, res):
    """ Group structure which have similar couplings, i.e. which differ only by
    simple integer factors """

    # build up `reference` couplings, each new coupling is tried to be mapped to
    # a reference coupling, if succeeded the new structure is referenced to the
    # reference coupling with appropriate factor

    # nothing to optimize, just put it in the correct form
    if len(res) < 2:
      return {(u,): res[u] for u in res}
    ref_co = {}
    for ls, co in iteritems(res):
      co = together(co)
      done = False
      for rco in ref_co:
        # compute quotient: coupling / reference coupling
        cc = cancel(co / rco)
        # if quotient is a number, then refer this structure to rco
        if cc.is_Number:
          new_ls = (ls[0], (ls[1][0], ls[1][1] * cc))
          ref_co[rco].append(new_ls)
          done = True
          break
      if not done:
        assert(co not in ref_co)
        ref_co[co] = [ls]

    return {tuple(ref_co[rco]): rco for rco in ref_co}

  def start(self, filein):
    """ Custom parsing routine, not entirely using FormParse. Amplitudes are
    stored in the attribute `amplitudes`. """

    with open(filein, 'r') as fin:
      text = fin.read()

      # find all ampltiude blocks corresponding to different cut particles and
      # branches (Tree Loop R2 CT)
      matches = re.findall(self.__class__.global_pattern, text)

      amps_reg = []
      exprs = []

      # Sums up all amplitudes with the same colorflow and perturbative order
      for match_id, match in enumerate(matches):
        color = re.search(self.color_pattern, match[0])

        # determine amplitude order in the couplings
        orders = []
        for order_pattern in self.order_patterns:
          orders.append(re.search(order_pattern, match[0]))
        amp_order = tuple([(order.group(1), order.group(2))
                           for order in orders])

        # only take powers for identifying the amplitude
        order_pow = ','.join([u[1] for u in amp_order])

        # unique amplitude id reconstructed
        amp_id = '[amp(' + color.group(1) + ',' + order_pow + ')]'

        # register the amplitude and intialize to zero if it has not been done
        # yet
        if (color.group(1), amp_order) not in amps_reg:
          amps_reg.append((color.group(1), amp_order))
          exprs.append('l ' + amp_id + ' = 0;')

        # proceed if amplitude is non-zero and add contribution
        if not match[1].strip() == '0':
          amp = ''.join(match[1].split())
          amp = self.assign_couplings(amp, {1: self.treevertices})
          exprs.extend(['l ' + amp_id + ' = ' + amp_id + ' + (' + amp + ');',
                        '.sort'])

      # coupling replacement dict which replaces generic FORM couplings
      # (c{particle config}[....]) with UFO couplings (GC_xx)
      coupl_repl = {Symbol(self.coupl_dict[key]): key
                    for key in self.coupl_dict}

      spins = [get_particle(u, Model.model.model_objects.all_particles).spin for
          u in self.particles]
      nparticles = len(self.particles)
      # apply momentum conservation
      if nparticles == 3:
        # simple improvement that the 3-Vertex looks simpler
        exprs.extend(['id p3 = -p1 -p2;'  # <- get rid of p3 in scalar products
                      '.sort',
                      'id p1(mu1) = -p3(mu1) -p2(mu1);',
                      'id p2(mu2) = -p3(mu2) -p1(mu2);',
                      'id p3(mu3) = -p1(mu3) -p2(mu3);'])
      else:
        # heuristic -> replace scalar momentum
        if spins.count(1) > 0:
          spos = spins.index(1) + 1
          others = [u for u in range(1, nparticles+1) if u != spos]
          mom_cons = ('id p' + str(spos) + ' = ' +
                      ''.join(map(lambda x: '-p' + str(x), others))
                      + ';')
          exprs.append(mom_cons)
          mom_cons = ('id p' + str(spos) + '(mu?) = ' +
                      ''.join(map(lambda x: '-p' + str(x) + '(mu)',
                              others))
                      + ';')
          exprs.append(mom_cons)

      exprs.append('.sort')

      # perform all the computation
      FP = FormPype()
      FP.eval_exprs(exprs)

      # arrange parse and store the result to `amplitudes`
      for amp_id in amps_reg:
        cs = amp_id[0]
        amp_order = amp_id[1]
        order_tmp = ','.join([u[1] for u in amp_order])
        amp_id = '[amp(' + cs + ',' + order_tmp + ')]'
        res = ''.join(FP.return_expr(amp_id).split())
        if res != '0':
          if int(cs) not in self.amplitudes:
            self.amplitudes[int(cs)] = {}
          res = self.assign_lorentz(res)
          res = self.parse_UFO(res)
          res = CurrentBase(res).compute2(self.ls_dict, len(self.particles),
                                          forbid_newls=False)
          # TODO: (nick 2015-01-02) try to join different ls_key if coupling
          # is the same. Sofar only (ls,) keys.
          # TODO: (nick 2016-08-24) did that finally
          res = self.group_similar(res)
          for ls_key in res:
            cr = {c: coupl_repl[c] for c in coupl_repl
                  if c in res[ls_key].free_symbols}
            res[ls_key] = res[ls_key].xreplace(cr)
            if ls_key not in self.amplitudes[int(cs)]:
              self.amplitudes[int(cs)][ls_key] = res[ls_key]
            else:
              self.amplitudes[int(cs)][ls_key] += res[ls_key]
      FP.close()
