#==============================================================================#
#                                divintegral.py                                #
#==============================================================================#

""" Module to build the divergent part of tensor integrals needed for the
computation of rational terms of type R2 in general. """

#===========#
#  Imports  #
#===========#

from sympy import Symbol, Poly, Rational, factorial

#===========#
#  Methods  #
#===========#


# generates symbols needed to build up analytic expressions
def mom2(i):
  """ Momentum squared """
  return Symbol('p' + str(i) + '2')

def mom_comb(i, j):
  """ p1.p2  """
  return Symbol('p' + str(i) + 'p' + str(j))

def mass2(i):
  """ Mass squared """
  return Symbol('m' + str(i) + '2')

def fp(i):
  """feynman parameter """
  return Symbol('x' + str(i))


class FiniteIntegral(Exception):
  """ FiniteIntegral exception is raised if the Divergent part of a to-be
  computed integral is zero.  """
  def __init__(self, value):
    self.value = value

  def __str__(self):
    return repr(self.value)

class WrongIntegralCoeff(Exception):
  """ WrongIntegralCoeff exception is raised if the requested tensor integral
  coefficient does not exist. """
  def __init__(self, value):
    self.value = value

  def __str__(self):
    return repr(self.value)

#==============================================================================#
#                               FeynmanIntegrand                               #
#==============================================================================#

class FeynmanIntegrand(object):

  """ Constructs the shifted integrand after the integration over the
  n-dimensional sphere has been performed """

  def __init__(self, npoint):
    self.npoint = npoint

    self.masses = set()
    self.momenta = set()
    integrand = self.pure_p2_x2() + self.mixed_px2() + self.linear_x()
    self.fpbase = [fp(u) for u in range(1, npoint)]
    self.poly = Poly(integrand, self.fpbase)

  def pure_p2_x2(self):
    """ Build \sum_{i=1}^{N-1} (p_i x_i)^2

    >>> FeynmanIntegrand(4).pure_p2_x2()
    p12*x1**2 + p22*x2**2 + p32*x3**2
    """

    integrand = 0

    for i in range(1, self.npoint):
      self.momenta.add(mom2(i))
      integrand += mom2(i) * fp(i)**2

    return integrand

  def mixed_px2(self):
    """ Build \sum_{i=1}^{N-1} \sum_{j>i}^{N-1} (p_i.p_j) x_i x_j

    >>> FeynmanIntegrand(4).mixed_px2()
    2*p1p2*x1*x2 + 2*p1p3*x1*x3 + 2*p2p3*x2*x3
    """

    integrand = 0

    for i in range(1, self.npoint):
      for j in range(i+1, self.npoint):
        self.momenta.add(mom_comb(i, j))
        integrand += 2*mom_comb(i, j)*fp(i)*fp(j)

    return integrand

  def linear_x(self):
    """ Build (\sum_{i=1}^{N-1} -x_i (p_i^2 - m_i^2 + m_0^2 )) + m_0^2

    >>> FeynmanIntegrand(4).linear_x()
    m02 - x1*(m02 - m12 + p12) - x2*(m02 - m22 + p22) - x3*(m02 - m32 + p32)
    """

    mass02 = mass2(0)
    self.masses.add(mass02)
    integrand = mass02
    for i in range(1, self.npoint):
      self.masses.add(mass2(i))
      integrand -= fp(i)*(mom2(i) - mass2(i) + mass02)

    return integrand

  def exponentiate(self, power):
    """
    >>> FI = FeynmanIntegrand(1)
    >>> FI.exponentiate(2)
    >>> FI.poly.as_expr()
    m02**2

    >>> FI = FeynmanIntegrand(2)
    >>> FI.exponentiate(2)
    >>> FIE = FI.poly
    >>> FIE.coeff_monomial(fp(1))
    -2*m02**2 + 2*m02*m12 - 2*m02*p12

    >>> FIE.coeff_monomial(fp(1)*fp(1))
    m02**2 - 2*m02*m12 + 4*m02*p12 + m12**2 - 2*m12*p12 + p12**2
    """
    res_poly = self.poly
    if power == 0:
      self.poly = Poly(1, self.fpbase)
    else:
      for p in range(1, power):
        res_poly = res_poly.mul(self.poly)

      self.poly = res_poly

  def multiply_fppoly(self, mvec):
    """ Mulitply the Integrand with the polynomial \prod_{i=1}^{N-1} x^{m_i}

    >>> FI = FeynmanIntegrand(2)
    >>> FI.poly.as_expr()
    m02 + p12*x1**2 + x1*(-m02 + m12 - p12)
    >>> FI.multiply_fppoly([2])
    >>> FI.poly.as_expr()
    m02*x1**2 + p12*x1**4 + x1**3*(-m02 + m12 - p12)
    """
    if not (all(u >= 0 for u in mvec)):
      raise Exception('mvec must be semi-positive definit.')
    if not (len(mvec) == self.npoint-1):
      raise Exception('mvec must have the length N-1')

    fppoly = Poly({tuple(mvec): 1}, self.fpbase)
    self.poly = self.poly.mul(fppoly)

  def integrate(self):
    """ Integrate the integrand over the measure \int dS_{N-1} defined by

    \int dS_{N-1}:=
    \int_0^1 dx_1 \int_0^{1-x_1} dx_2 ... \int_0^{1-x1-... x_{N-2}} dx_{N-1}

    Results are checked against mathematica. Computation is much faster than a
    native integration in Mathematica :)

    >>> FeynmanIntegrand(3).integrate()
    m02/6 + m12/6 + m22/6 - p12/12 + p1p2/12 - p22/12

    >>> FeynmanIntegrand(4).integrate()
    m02/24 + m12/24 + m22/24 + m32/24 - p12/40 + p1p2/60 + p1p3/60 - p22/40 +\
 p2p3/60 - p32/40


    >>> FI = FeynmanIntegrand(4)
    >>> FI.multiply_fppoly((2,3,0))
    >>> FI.integrate()
    m02/30240 + m12/10080 + m22/7560 + m32/30240 - p12/16800 + p1p2/12600 +\
 p1p3/50400 - p22/15120 + p2p3/37800 - p32/37800
    """
    polyint = self.poly
    for i in range(self.npoint-1, 0, -1):
      # bounds of integration
      # lower = 0, lower bound always gives zero
      upper = 1 - sum(fp(j) for j in range(1, i))
      # integrate
      polyint = polyint.integrate(fp(i))

      # next we insert the upper border in an effective way

      # after every integration the poly base is reduced by one
      if i > 1:
        new_base = self.fpbase[0:i-1]
        new_poly = Poly(0, new_base)
      else:
        new_poly = 0

      # from the monoms we build up the new, integrated polynomial
      for monom in polyint.monoms():
        coeff = polyint.coeff_monomial(monom)
        # power in the current integration variable after integration
        pp = monom[-1]
        if i > 1:
          upper_mul = Poly(upper**pp, new_base)
          upper_poly = Poly({monom[:-1]: coeff}, new_base).mul(upper_mul)
          new_poly = new_poly.add(upper_poly)
        else:
          new_poly += coeff
      polyint = new_poly
    return polyint

#==============================================================================#
#                                DivTensorCoeff                                #
#==============================================================================#

class DivTensorCoeff(object):

  """ Computes the divergent coefficient of tensor integrals.

  T^N_{n, m_1, ... m_{N-1}} See http://arXiv.org/abs/hep-ph/0509141v2 equation
  2.4 for conventions.

  Some standard results are checked against http://arXiv.org/abs/0709.1075v1
  B_{11}
  >>> DivTensorCoeff(2, 2, (2, )).div
  -2/3

  B_{00}
  >>> DivTensorCoeff(2, 2, (0,)).div
  -m02/2 - m12/2 + p12/6

  C_{00}
  >>> N = 3
  >>> R = 2
  >>> mvec = (0, 0)
  >>> DivTensorCoeff(N, R, mvec).div
  -1/2

  C_{00i}
  >>> N = 3
  >>> R = 3
  >>> mvec = (1, 0)
  >>> DivTensorCoeff(N, R, mvec).div
  1/6

  D_{0000}
  >>> N = 4
  >>> R = 4
  >>> mvec = (0, 0, 0)
  >>> DivTensorCoeff(N, R, mvec).div
  -1/12
  """

  def __init__(self, npoint, R, mvec, form_conv=False):
    """
    :param npoint: NPoint integral
    :type  npoint: int

    :param R: rank of the tensor
    :type  R: int

    :param mvec: set momenta p_i^{m_i}
    :type  mvec: list/tuple

    :param form_conv: Enable form convention for momenta and masses
    :type  form_conv: bool
    """
    self.npoint = npoint
    self.mvec = mvec
    n_p = sum(mvec)
    open_mu = R - n_p
    if open_mu % 2 != 0:
      raise WrongIntegralCoeff('Structure does not exist. ' +
                               'Check the number of momenta.')
    n_g = open_mu/2

    self.n_p = n_p
    self.n_g = n_g

    if self.n_g < self.npoint - 2:
      raise FiniteIntegral('Integral is finite')

    FI = FeynmanIntegrand(self.npoint)
    FI.exponentiate(self.n_g - self.npoint+2)
    FI.multiply_fppoly(self.mvec)

    divcoeff = FI.integrate()

    # map mass and momentum conventions to FORM
    if form_conv:
      # masses transform according to: m'i'2 -> m'i+1'**2
      mass_repl = {u: Symbol('m' + str(int(u.name[1:-1])+1))**2
                   for u in FI.masses}
      mom_repl = {}
      for p in FI.momenta:
        # momentum which is already contracted with another momentum
        if p.name.count('p') == 2:
          _, p1, p2 = p.name.split('p')
          ps1 = 'p' + p1
          ps2 = 'p' + p2
        # squared momentum
        else:
          ps1 = 'p' + p.name[1:-1]
          ps2 = ps1
        mom_repl[p] = Symbol(ps1 + '.' + ps2)
      divcoeff = divcoeff.subs(mass_repl).subs(mom_repl)

    self.div = divcoeff*self.factor()

  def factor(self):
    sign = -(-1)**(sum(self.mvec))
    fac = Rational(2)**(-self.n_g+1)/factorial(self.n_g - self.npoint + 2)
    return sign*fac

  @classmethod
  def build_lorentz_args(cls, n_g, n_p):

    from rept1l.combinatorics import group_permutations
    from rept1l.combinatorics import get_permutations_from_spins
    from rept1l.combinatorics import remove_redundant_transf
    from rept1l.combinatorics import interchange_transformation
    from rept1l.combinatorics import m_of_n, flatten
    from rept1l.combinatorics import permute_spins, get_representative

    nargs = range(2*n_g)
    group_sizes = [2]*n_g
    symmetries = [[[1, 0]]]*n_g

    g_args_sum = []
    if n_g > 0:
      irs = m_of_n(range(1, 2*n_g+n_p+1), 2*n_g)
      for ir in irs:
        g_base = [Symbol('Metric')]*n_g
        perms = get_permutations_from_spins(range(2*n_g))
        g_args_sum = []
        for ir in irs:
          g_args_s = set()
          for perm in perms:
            irp = permute_spins(ir, perm)
            #print("irp:", irp)
            g_args = [(irp[i], irp[i+1]) for i in range(0, len(irp), 2)]
            r = tuple(tuple(u) for u in get_representative(g_base, g_args))
            g_args_s.add(r)
          p_args = [v for v in range(1, 2*n_g+n_p+1) if v not in ir]
          g_args_sum.append({'Metrics': g_args_s, 'Momenta': p_args})
        #g_args = group_permutations(get_permutations_from_spins(nargs),
                                    #group_sizes=group_sizes,
                                    #symmetries=symmetries,
                                    #index_repl=ir)

        ##print("g_args:", g_args)
        ##g_args = [u for u in remove_redundant_transf(g_args)]
        #g_args = remove_redundant_transf(g_args)
        #if n_g > 1:
          #sym = get_permutations_from_spins(range(n_g))
          #sym.remove(range(n_g))
          #g_args = interchange_transformation(g_args, sym)
          #g_args = [u for u in remove_redundant_transf(g_args)]
        #p_args = [v for v in range(1, 2*n_g+n_p+1)
                  #if v not in [u for u in flatten(g_args)]]
        #g_args_sum.append({'Metrics': g_args, 'Momenta': p_args})
    else:
      p_args = [v for v in range(1, 2*n_g+n_p+1)]
      g_args_sum.append({'Momenta': p_args})

    return g_args_sum

  @classmethod
  def build_lorentz_coeff(cls, R, mvec):
    """ Constructs the structure
    {g^{\mu_1, \mu_2} .. g^{\mu_2n, \mu_(2n+1)} \times
     p_{i_(2n+2)}^{\mu_(2n+2)} ... p_{i_(P)}^{\mu_R}

    :param R: rank of the tensor
    :type  R: int

    :param mvec: momenta in the decomposition
    :type  mvec: list/tuple

    For convenience i_(2n+2) ... i_(P) is passed as the vector mvec.
    The length of mvec determines the number of momenta.
    The entries determine power in the momentum index.
    mvec = (1, 2, 2, 3, ... ) => p_1^1 * p_2^2 * p_3^2 * p_4^3 * ...
    """
    n_p = sum(mvec)
    open_mu = R - n_p
    if open_mu % 2 != 0:
      raise Exception('Structure does not exist. Check the number of momenta.')
    n_g = open_mu/2

    mu_args = cls.build_lorentz_args(n_g, n_p)
    ret = {}
    for coeff in mu_args:

      if 'Momenta' in coeff:
        assert(len(coeff['Momenta']) == sum(mvec))
        momenta = {tuple(coeff['Momenta']): 'P'}
      else:
        momenta = {}
      if 'Metrics' in coeff:
        metrics_combs = coeff['Metrics']
        for metrics in coeff['Metrics']:
          yield dict({tuple(g): 'g' for g in metrics}, **momenta)
      else:
        # TODO: (nick 2016-04-04) make check
        #assert(len(metrics)+len(momenta) == n_g + n_p)
        yield momenta

  @classmethod
  def build_form_coeff(cls, R, mvec):
    """ Builds the analytic expression for the lorentzstructure of the tensor
    coefficient for rank R and momentum vector mvec

    :param R: rank of the tensor
    :type  R: int

    :param mvec: momenta in the decomposition
    :type  mvec: list/tuple
    """
    for coeff in cls.build_lorentz_coeff(R, mvec):
      syms = [1]
      for struc, sym in coeff.iteritems():
        if sym == 'g':
          dsym = Symbol('d_(mu' + str(struc[0]) + ',mu' + str(struc[1]) + ')')
          syms.append(dsym)
        elif sym == 'P':
          if sum(mvec) == 0:
            continue

          mom = []
          for pos, power in enumerate(mvec):
            mom += ['p' + str(pos+1)]*power

          # permutation over momenta position
          # e.g. given [p1 ,p1, p2]
          #-> [[p1, p1, p2], [p1, p2, p1], [p2, p1, p1]]
          from rept1l.combinatorics import gen_permutations
          from rept1l.combinatorics import permute_spins
          if len(mom) > 1:
            mom_perms = (permute_spins(mom, p)
                         for p in gen_permutations(mom))
          else:
            mom_perms = [mom]
          p_syms = []
          for mom_p in mom_perms:
            r = reduce(lambda x, y: x*y,
                       (Symbol(p + '(mu' + str(struc[pos]) + ')')
                        for pos, p in enumerate(mom_p)))
            p_syms.append(r)

          syms.append(sum(p_syms))
        else:
          raise Exception('Case for sym != g/P not implemented.')
      yield reduce(lambda x, y: x*y, syms)

  @staticmethod
  def form_tensor_integral(npoint, rank):
    """ Generates the left-hand side of the tensor identitiy used in FORM.

    :param npoint: npoint of the tensor
    :type  npoint: int

    :param rank: rank of the tensor
    :type  rank: int

    >>> DivTensorCoeff.form_tensor_integral(1, 0)
    'id A([],m1?) = '

    >>> DivTensorCoeff.form_tensor_integral(3, 0)
    'id C([],p1?,p2?,m1?,m2?,m3?) = '

    >>> DivTensorCoeff.form_tensor_integral(3, 2)
    'id C(mu1?,mu2?,[],p1?,p2?,m1?,m2?,m3?) = '

    >>> DivTensorCoeff.form_tensor_integral(4, 4)
    'id D(mu1?,mu2?,mu3?,mu4?,[],p1?,p2?,p3?,m1?,m2?,m3?,m4?) = '
    """
    base_symbol = {1: 'A', 2: 'B', 3: 'C', 4: 'D', 5: 'E', 6: 'F', 7: 'G'}
    rank_args = ','.join('mu' + str(i) + '?' for i in range(1, rank+1))
    if rank > 0:
      rank_args += ','
    momenta_args = ','.join('p' + str(i) + '?' for i in range(1, npoint))
    if npoint > 1:
      momenta_args += ','
    masses_args = ','.join('m' + str(i) + '?' for i in range(1, npoint+1))

    return ('id ' + base_symbol[npoint] + '(' + rank_args + '[],' +
            momenta_args + masses_args + ') = ')

  @classmethod
  def build_form_id(cls, npoint, rank):
    """ Builds the form id for computing rational terms of type 2 from
    n-dimensional amplitude.

    >>> frmid = DivTensorCoeff.build_form_id(2, 2)
    >>> frmid[0]
    'id B(mu1?,mu2?,[],p1?,m1?,m2?) = '
    >>> frmid[1]
    '+d_(mu1,mu2)*(3*m1^2 + 3*m2^2 - p1.p1)/(6*ep)'
    >>> frmid[2]
    '+2*p1(mu1)*p1(mu2)/(3*ep)'
    >>> frmid[3]
    ';'
    """
    frmid = []
    lhs = cls.form_tensor_integral(npoint, rank)
    frmid.append(lhs)
    rhs = cls.build_form_id_rhs(npoint, rank)
    ep = Symbol('ep')
    rhs_values = rhs.values()
    from rept1l.helper_lib import string_subs
    for p in rhs_values:
      if p != 0:
        pexpr = str(-p/ep)
        if pexpr[0] != '-':
          pexpr = '+' + pexpr
        pexpr = string_subs(pexpr, {'**': '^'})
        frmid.append(pexpr)
    if all(u == 0 for u in rhs_values):
      frmid.append('0')

    frmid.append(';')
    return frmid

  @classmethod
  def build_form_id_rhs(cls, npoint, rank):
    """ Builds the right-hand side of the form expression for the divergent
    `npoint` Tensor of rank `rank`

    :param npoint: Number of propagators
    :type  npoint: int

    :param rank: Rank of the Tensor integral
    :type  rank: int

    :param return:  divergent tensor  sorted according to the power of momenta.
    :type  return: dict

    >>> DivTensorCoeff.build_form_id_rhs(2, 2)
    {0: -d_(mu1,mu2)*(3*m1**2 + 3*m2**2 - p1.p1)/6, \
1: 0, 2: -2*p1(mu1)*p1(mu2)/3}
    """

    from sympy import factor

    tensorint = {}

    for mvecR in range(rank+1):
      # result for all contribution with the same power of momenta
      res_sum = 0
      # fill the momentum vector up to highest rank, other indices are filled
      # with metrics
      for mvec in cls.build_mvecs(npoint-1, mvecR):

        # build the lorentz structure * divergent tensor coefficient
        res_interm = 0
        try:
          divcoeff = DivTensorCoeff(npoint, rank, mvec, form_conv=True).div
          for ls in DivTensorCoeff.build_form_coeff(rank, mvec):
            res_interm += ls
          res_interm *= divcoeff
        except FiniteIntegral:
          continue
        except WrongIntegralCoeff:
          continue
        res_sum += res_interm
      tensorint[mvecR] = factor(res_sum)
    return tensorint

  @classmethod
  def build_mvecs(cls, nvec, rank):
    """ Builds all combination of products of momenta. The number of different
    momenta is controlled by nvec. For instance, for npoint tensor integrals the
    number of indpendent momenta nvec should be set to npoint-1.

    :param nvec: Sets the number of independent momenta to npoint -1
    :type  nvec: int

    :param npoint: Sets the number of momentum power to rank
    :type  npoint: int

    For a threepoint function the products of momenta of power 2 are given by
    >>> [u for u in DivTensorCoeff.build_mvecs(2, 2)]
    [[1, 1], [0, 2], [2, 0]]

    which represents (in the order of the list):
    [p_1^\mu1*p_2^\mu2, p_2^\mu1*p_2^\mu2, p_1^\mu1*p_1^\mu2]

    >>> [u for u in DivTensorCoeff.build_mvecs(3, 1)]
    [[0, 0, 1], [0, 1, 0], [1, 0, 0]]
    """
    from rept1l.combinatorics import get_partitions
    from rept1l.combinatorics import gen_permutations
    from rept1l.combinatorics import permute_spins
    for part in get_partitions(rank):
      if len(part) > nvec:
        continue
      part_filled = part + [0]*(nvec-len(part))
      if len(part_filled) == 1:
        yield part_filled
      else:
        perms = gen_permutations([u + 1 for u in part_filled])
        for perm in perms:
          yield permute_spins(part_filled, perm)

if __name__ == '__main__':
  import doctest
  doctest.testmod()
