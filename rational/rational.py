#==============================================================================#
#                                 rational.py                                  #
#==============================================================================#

from __future__ import print_function

#============#
#  Includes  #
#============#
import sys
import multiprocessing
from subprocess import CalledProcessError
from sympy import Symbol, Rational, sympify, together, nsimplify
from math import sqrt

from rept1l.parsing import ParseColor
from rept1l.parsing import FormProcess, ProcessNotExist
from rept1l.vertices_rc import Vertex, permute_particle_branch
from rept1l.helper_lib import get_anti_particle
from rept1l.colorflow import ColorflowVertex
from rept1l.coupling import RCoupling as FRC
from rept1l.combinatorics import pair_arguments
from rept1l.combinatorics import get_permutations_from_spins
from rept1l.logging_setup import log
from .parserational import ParseRational

#############
#  Globals  #
#############

#==============================================================================#
#                                   R2Worker                                   #
#==============================================================================#

class R2Worker(multiprocessing.Process):
  """ Worker unit to handle computation of a single r2 job.
  """
  def __init__(self, id, func, result_queue, *args, **kwargs):
    """
    :param id: process id
    :type  id: hashable
    """
    self.id = id
    self.func = func
    self.args = args
    self.result_queue = result_queue
    self.kwargs = kwargs
    super(R2Worker, self).__init__()

  def run(self):
    """ Run the process. The result is put to the result queue """
    if('select_power_NLO' in self.kwargs and
       len(self.kwargs['select_power_NLO']) > 0):
      ret = self.func(*self.args,
                      select_power_NLO=self.kwargs['select_power_NLO'])
    else:
      ret = self.func(*self.args)
    self.result_queue.put((self.id, ret))
    return

#==============================================================================#
#                                signal_handler                                #
#==============================================================================#

def signal_handler(signal, frame):
  Vertex.store()
  ColorflowVertex.store()
  FRC.store()
  ParseRational.store()
  print('Stored data. Exiting!')
  sys.exit(0)

#==============================================================================#
#                          compute_rational_amplitude                          #
#==============================================================================#

def compute_rational_amplitude(index, particles, masscut_val=None, **kwargs):
  """ Generates the vertex function defined by `particles` and computes the
  rational part of the vertex.

  :param index: amplitude index which is passed to Recola
  :type  index: int

  :param particles: List of particle names in UFO convention
  :type  particles: List of str
  """
  # recola setup
  lps = {'tree': False, 'bare': True, 'ct': False, 'r2': False}

  if masscut_val:
    masscut = True
  else:
    masscut = False
  process_defs = {'level_process': lps,  # only bare loop amplitude needed
                  'form_amplitude': 3,  # without polarizaiton and r2 FORM call
                  'vertex_functions': True,  # 1PI Graphs
                  'masscut_frm': masscut,   # masscut to zero
                  'ifail': 0   # no stop when error encountered
                               # (e.g. process does not exist)
                  }
  if masscut:
    process_defs['masscut'] = masscut_val
  else:
    process_defs['masscut'] = 0.

  if 'select_power_NLO' in kwargs and len(kwargs['select_power_NLO']) > 0:
    process_defs['select_power_NLO'] = kwargs['select_power_NLO']

  # compute the process
  try:
    FP = FormProcess(index=index, particles=particles, gen_process=True,
                     **process_defs)
    return FP

  except CalledProcessError:
    log('Failed extraction for ' + str(particles) + '.', 'warning')

  except ProcessNotExist:
    log('Process ' + ','.join(particles) + ' does not exist.', 'warning')


#==============================================================================#
#                         register_rational_amplitude                          #
#==============================================================================#

def register_rational_amplitude(FP):

  import rept1l.Model as Model
  model = Model.model

  # parse rational terms
  R2 = ParseRational(Vertex.treevertices, FP.particles)
  R2.start(FP.amplitude_file)

  # parse process information
  pri = ParseColor(FP.colorflow_file)

  FP.delete_process_files()

  # compensate fermionsign
  if not hasattr(pri, 'fermionsign'):
    raise Exception('Provided amplitude without relative fermionsign.')
  fermionsign = pri.fermionsign

  # register new tree-level colorflows
  new_colors = pri.compute_color_base_relations(ColorflowVertex.
                                                colors_dict.copy())
  ColorflowVertex.colors_dict.update(new_colors)

  # TODO: (nick 2015-08-12) Not sure if storing is required here
  if len(pri.new_colors) > 0:
    ColorflowVertex.store()

  # Nc=3 parameter is introduced by recola and not by the UFO model
  psubs = {Symbol('Nc'): 3., Symbol('sq2'): sqrt(2.)}

  # gather all contributing lorentz structures in `amplitudes` as follows:
  # amplitudes[lorentz structure] = coupling
  amplitudes = {}
  for amp in R2.amplitudes:
    ls_couplings = R2.amplitudes[amp]  # {lorentz: associated coupling}

    # non-contributing lorentz structures
    zero_ls = []
    # iterate over the lorentz structures
    for ls in ls_couplings:

      # numerical check if coupling is non-zero
      c_val = FRC.expression_value(ls_couplings[ls], psubs=psubs)
      try:
        # remove lorentz structures with vanishing couplings
        potentially_zero = abs(c_val) < 10**-15
        # analytic check for zero
        if potentially_zero:
          from rept1l.regularize import Regularize
          is_zero = Regularize.test_vanish(ls_couplings[ls])
        else:
          is_zero = False
        if is_zero:
          zero_ls.append(ls)
        else:
          # replace all masses with complex ones and compensate for fermionsign
          coupling = FRC.complex_mass_couplings(ls_couplings[ls] * fermionsign)
          # register the coupling in the coupling database
          cnew = FRC(coupling, branch_tag='R2',
                     subs_cvalues=False,
                     simp_func=together).return_coupling()
          ls_couplings[ls] = cnew
      except:
        print('Checking for zero failed for coupling value: ' + str(c_val))
        raise

    # remove non-contributing lorentz structures and couplings
    for ls in zero_ls:
      del ls_couplings[ls]

    # retrieve or register colorflow
    if amp in pri.cs_id:
      color_id = pri.cs_id[amp]
    else:
      neutral_colorflow = ((), pri.n_In)
      if neutral_colorflow not in ColorflowVertex.colors_dict:
        color_id = len(ColorflowVertex.colors_dict)
        ColorflowVertex.colors_dict[neutral_colorflow] = color_id

      color_id = ColorflowVertex.colors_dict[neutral_colorflow]

    # register amplitude (colorfactor set to 1 as I don't know yet how to
    # separately extract the colorflow factor from the amplitude separately)
    # The actual colorfactor is, of course, included in the coupling.
    if ls_couplings != {}:
      amplitudes[(color_id, 1)] = ls_couplings

  # write the amplitudes to the vertex class
  if amplitudes != {}:

    # generate the particle key and check if the particle configuration does
    # already exist. This is a bit tricky as we cannot simple compare the
    # particle keys because we are comparing particle instances which are
    # different for different runs because UFO does not make sure that the
    # same particle is instanced only once. This is why particle string names
    # are compared.
    p_key = tuple([model.model_objects.all_particles[i - 1]
                  for i in pri.particles])
    p_key_str = [u.name for u in p_key]
    r2_p_keys = list(Vertex.lorentzR2_dict.keys())
    p_keys_str = [[u.name for u in key] for key in r2_p_keys]
    if p_key_str in p_keys_str:
      p_key = r2_p_keys[p_keys_str.index(p_key_str)]

    # generate all the permutations of p_key and write them to the vertex class
    for perm in get_permutations_from_spins(p_key):
      if perm == range(len(p_key)):
        Vertex.lorentzR2_dict[p_key] = amplitudes
      else:
        p = permute_particle_branch({p_key: amplitudes}, perm)
        Vertex.lorentzR2_dict.update(p)

    return True
  else:
    return False

#==============================================================================#
#                               compute_rational                               #
#==============================================================================#

def compute_rational(particles, masscut=None, index=1, **kwargs):
  """ Computes the R2 UFO rule associated to the vertex function defined by
  `particles` """

  FP = compute_rational_amplitude(index, particles, masscut, **kwargs)
  if FP:
    return register_rational_amplitude(FP)

#==============================================================================#
#                            compute_all_rationals                             #
#==============================================================================#

def compute_all_rationals(n_point=2, recompute=False, ignore_zero=False,
                          reset_zero=False, masscut=None, selection=None,
                          pairing=True, powercounting=True, threads=1,
                          **kwargs):
  """ Computes all rational terms of n_point vertex type. The particle
  combinations are generated from the modelfile particles. By default, all
  particles are combined with each other including equal particles under the
  constraint that the number of particles is equal to `n_point`. If a selection
  is provided all particles of that selection are combined.

  :param n_point: 2-Point, 3-Point, ..., n-Point Vertex
  :type  n_point: int

  :param recompute: Set true if the vertex should be recomputed even if the same
                    particle configuration has already been computed in a
                    previous run.
  :type  recompute: bool

  :param ignore_zero: Compute the vertex even if in a previous the vertex has
                      found to be zero.
  :type  ignore_zero: bool

  :param reset_zero: Compute the vertex  and remove all vertices which have
                     been found to be zero during a previous run from registry.
  :type  reset_zero: bool

  :param masscut: Sets a masscut below which masses are set to zero. In order
                  to avoid mistakes all massive particles should be defined
                  with a non-zero mass and the masscut should be set to 0.
  :type  masscut: float (GeV)

  :param selection: Selection of particles for which R2 should be computed
  :type  selection: list of UFO particles

  :param pairing: Disables combining particles with each other. The length of
                  the selection must match npoint.
  :type  pairing: bool

  :param threads: Number of threads to be used. If set to 1 computation is not
                  parallelized.
  :type  threads: bool
  """

  import rept1l.Model as Model
  model = Model.model

  if threads < 1:
    raise Exception('Number of threads needs to be larger than zero.')
  multi = False
  if threads > 1:
    multi = True

  # Load vertices, colorflows, couplings and rational terms
  Vertex.load()
  ColorflowVertex.load()
  FRC.load()
  ParseRational.load()

  # TODO: (nick 2015-06-29) color and (optionally) fermion number conservation
  spin_to_dimension = {-1: 100, 1: sympify(1), 2: Rational(3, 2), 3: sympify(1)}

  if selection:
    particles = selection
  # compute only the rational terms for particle fields which appear outside of
  # loops
  else:
    particles = [particle for particle in model.model_objects.all_particles
                 if Vertex.field_origin_BFM(particle)[0]]

  particle_names = [u.name for u in model.model_objects.all_particles
                    if Vertex.field_origin_BFM(u)[0]]

  # given a selection and no pairing the length must match npoint
  if selection and not pairing:
    if not all(len(u) == n_point for u in particles):
      raise Exception('Selection length does not match npoint')

    # Do only compute the actual selection
    vertices = particles
  else:
    # Generate all possible particle combinations
    vertices = pair_arguments([particles] * n_point)

  # initialize cache dicts
  if ParseRational.rational_dict == {}:
    ParseRational._rational_dict = []

  if ParseRational.zero_r2 == {}:
    ParseRational._zero_r2 = []

  # filling cache dicts.
  # rational_dict is filled with already computed rational terms
  if recompute:
    rational_dict = []
  else:
    rational_dict = ParseRational.rational_dict

  # zero_r2 is filled with rational terms which have been found to be zero
  if reset_zero or ignore_zero:
    zero_r2 = []
  else:
    zero_r2 = ParseRational.zero_r2

  if not recompute:
    for p_key in Vertex.lorentzR2_dict:
      v_particles = [u.name for u in p_key]
      process_id = sorted(v_particles,
                          key=lambda x: particle_names.index(x))
      if process_id not in rational_dict:
        rational_dict.append(process_id)

  from rept1l.pyfort import ProgressBar
  # check for anomaly cancellation
  #vertices = [[model.P.Z, model.P.Z, model.P.Z]]
  #vertices = [[model.P.a, model.P.a, model.P.Z]]
  #vertices = [[model.P.Z, model.P.a, model.P.W__plus__, model.P.W__minus__]]
  if multi:
    jobs = []
    rs = multiprocessing.Queue()
    log('Preparing jobs for ' + str(n_point) + ' Vertices.', 'info')

  nv = len(vertices)
  PG = ProgressBar(nv)
  total = 0

  particle_charges = {}

  # cached particle charges as sympy expression
  def get_pc(particle):
    if particle.charge not in particle_charges:
      particle_charges[particle.charge] = nsimplify(particle.charge, rational=True)
    return particle_charges[particle.charge]

  for id, vertex in enumerate(vertices):
    if multi:
      PG.advanceAndPlot()
    # generate a unique vertex id
    # -> id used so that no equivalent vertex (permutation) is computed again.
    process_id = sorted([u.name for u in vertex],
                        key=lambda x: particle_names.index(x))

    # check if vertex has already been computed
    if process_id in rational_dict:
      if not multi:
        PG.advanceAndPlot(status=(' '.join(process_id)))
      continue

    # check if vertex has been found to be zero
    if process_id in zero_r2:
      if not multi:
        PG.advanceAndPlot(status=(' '.join(process_id)))
      continue

    # check for even number of fermions (not fermion number violation)
    fermion_number = [1 for particle in vertex if particle.spin == 2]
    if sum(fermion_number) % 2 != 0:
      if not multi:
        PG.advanceAndPlot(status=(' '.join(process_id)))
      continue

    # check for charge conservation
    charge = sum(get_pc(particle) for particle in vertex)

    if charge != 0:
      if not multi:
        PG.advanceAndPlot(status=(' '.join(process_id)))
      continue

    # check for degree of divergence (vertex should be divergent, it does not
    # contribute)
    if powercounting:
      dod = 4 - sum([spin_to_dimension[particle.spin] for particle in vertex])
      if dod < 0:
        if not multi:
          PG.advanceAndPlot(status=(' '.join(process_id)))
        continue

    # construct process
    if len(vertex) == 2:
      offset = 1
    elif len(vertex) == 3:
      offset = 1
    elif len(vertex) >= 4:
      offset = 2
    particles_in = [u.name for u in vertex[:offset]]
    particles_out = [get_anti_particle(u, model.model_objects.all_particles).
                     name for u in vertex[offset:]]
    process = particles_in + particles_out

    # in parallel mode all processes are added to rational_dict and are
    # have to be removed in case of zero
    if multi:
      rational_dict.append(process_id)
    total += 1

    # compute r2 contribution
    if multi:
      j = R2Worker(process, compute_rational_amplitude, rs, id, process,
                   masscut, **kwargs)
      jobs.append(j)
    else:
      try:
        non_zero = compute_rational(process, masscut=masscut, **kwargs)
        if not non_zero:
          zero_r2.append(process_id)
        else:
          if process_id in zero_r2:
            zero_r2.remove(process_id)
      except:
        Vertex.store()
        ColorflowVertex.store()
        FRC.store()
        ParseRational.store()
        print('Stored data. Exiting!')
        raise

    if not multi:
      if non_zero:
        rational_dict.append(process_id)
      PG.advanceAndPlot(status=(' '.join(process_id)))

  log('\n', 'info')
  log('Total number of process compuations: ' + str(total), 'info')
  if multi:
    #max_jobs = 2*multiprocessing.cpu_count()
    max_jobs = threads
    n_jobs = len(jobs)
    if n_jobs > 0:
      n_active = 0
      PG = ProgressBar(n_jobs)
      finished_jobs = {}
      log('Running jobs for ' + str(n_point) + ' Vertices.', 'info')
      while n_jobs > 0:
        while n_active < max_jobs and n_active < n_jobs:
          new_job = jobs.pop(0)
          new_job.start()
          finished_jobs[tuple(new_job.id)] = new_job
          n_active += 1
        process, FP = rs.get()
        r2w = finished_jobs[tuple(process)]

        if FP:
          non_zero = register_rational_amplitude(FP)
          if not non_zero:
            zero_r2.append(process)
            if process in rational_dict:
              rational_dict.remove(process)
          else:
            if process in zero_r2:
              zero_r2.remove(process)
        else:
          zero_r2.append(process)
          if process in rational_dict:
            rational_dict.remove(process)
        r2w.join()
        try:
          r2w.close()
        except AttributeError:
          pass
        n_jobs -= 1
        n_active -= 1
        PG.advanceAndPlot(status=(' '.join(process)))

  log('R2 computation ended', 'info')
  Vertex.store()
  ColorflowVertex.store()
  FRC.store()
  ParseRational._rational_dict = rational_dict
  if reset_zero:
    ParseRational._zero_r2 = zero_r2
  ParseRational.store()


if __name__ == "__main__":

  # catch sigint (ctrl-c) and store computed results
  # import signal
  #signal.signal(signal.SIGINT, signal_handler)
  #particles = [model.P.a, model.P.h2]

  #compute_all_rationals(n_point=2)
  select_power_NLO = [('QCD', 2), ('QCD', 3), ('QCD', 4),
                      ('QED', 0), ('QED', 1), ('QED', 2),
                      ('LAM', 0), ('LAM', 1)]
  compute_all_rationals(n_point=3, select_power_NLO=select_power_NLO)
