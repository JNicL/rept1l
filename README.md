# REPT1L README #

* Version 2.0.12

* * *

**REPT1L** is a *Python* 2.7 library for *automated* generation of
renormalization **RECOLA2** model files.

## Documentation and installation instructions

The documentation is hosted online on bitbucket https://jnicl.bitbucket.io/

* * *

### Contact ###

* Repo owner: Jean-Nicolas Lang <jlang@physik.uni-wuerzburg.de>
