# -*- coding: utf-8 -*-
###############################################################################
#                                regularize.py                                #
###############################################################################

from __future__ import print_function

#============#
#  Includes  #
#============#

import sys
import re
from sympy import Symbol, simplify, together, sympify, Poly, sqrt, I
from sympy.solvers import solve
from six import with_metaclass
from functools import reduce

from rept1l.formutils import FormPype
from rept1l.coupling import RCoupling
from rept1l.pyfort import ProgressBar

from rept1l.parsing import string_subs, FormParse
from rept1l.combinatorics import fold

from rept1l.logging_setup import log
from rept1l.helper_lib import StorageProperty
import rept1l.Model as Model
model = Model.model

#===========#
#  classes  #
#===========#

#==============================================================================#
#                                ScaleException                                #
#==============================================================================#

class ScaleException(Exception):
  pass

#==============================================================================#
#                                MixedException                                #
#==============================================================================#

class MixedException(Exception):
  pass

#======================#
#  AmbigousPropagagor  #
#======================#

class AmbigousPropagator(Exception):
    def __init__(self, amp, base):
        self.amp = amp
        self.base = base

#==============================================================================#
#                                  Regularize                                  #
#==============================================================================#

class Regularize(with_metaclass(StorageProperty,FormParse)):
  """ Methods for imposing renormalization conditions and computing counterterm
  constants. Includes the CWZ scheme in QCD (alphas renormalization), onshell
  schemes for all kind of particles (Spin 0, 1/2, 1) and MS(bar) subtraction.
  """
  # StorageMeta attributes. Those attributes are stored when `store` is called.
  storage = ['tens_dict']
  # StorageMeta storage path
  mpath, _ = model.get_modelpath()
  path = mpath + '/ModelTensors'
  # StorageMeta filename
  name = 'tensor_data.txt'

  projections = ['Transverse',   'TransverseD',
                 'Longitudinal', 'LongitudinalD',
                 'ScalarMinus',  'ScalarMinusD',
                 'ScalarPlus',   'ScalarPlusD',
                 'VectorMinus',  'VectorMinusD',
                 'VectorPlus',   'VectorPlusD']

  projectors = {}

  reno = {}

  #B_pattern = r'B\((?:\w*,)*?\[\]((?:,\w+)+)?\)'
  # Scalar integral pattern
  SI_pattern = r'[ABCDEF]\((?:\w*,)*?\[\]((?:,\w+)+)?\)'
  global_flags = re.S | re.M

  # Inactive particles are subtracted at zero-momentum in the CWZ procedure.
  inactive_masses = []

  # make tens_dict property available for Regular instances
  # somewhat a hack because Regular.tens_dict points to a property
  # and now we do Regular(...).tens_dict pointing to Regular.tens_dict
  # I have to declare the property here because this part of the code is
  # executed before the metaclass itself and I cannot put this code before
  # the corresponding metaclass code (multiple classes share the same metaclass
  # and when the second class is created the property attribute is already set
  # and the following does not work anymore).
  def get_tens_dict(self):
    if self.debug:
      return self.__class__._tens_dict
    else:
      return self.__class__.tens_dict

  tens_dict = property(get_tens_dict)

  def __init__(self, treevertices=None, ct_vertices=None, simplify_expr=None):
    super(Regularize, self).__init__()
    self.treevertices = treevertices
    self.ct_vertices = ct_vertices
    self.simplify_expr = simplify_expr

    #def get_tens_dict(self):
      #return super(self).tens_dict

    #self.tens_dict = property(get_tens_dict)

    # stores renormalization conditions, the epressions are expected to be zero
    # i.e. for renorm in renorm_conditions -> renorm == 0
    self.renorm_conditions = {}

    # Vector projections
    self.projectors['Transverse'] = ('(d_(mu1,mu2) - p1(mu1)*p1(mu2)/p1.p1)/' +
                                     '(3)')
    self.projectors['TransverseP2'] = ('(d_(mu1,mu2) - p1(mu1)*p1(mu2)/p1.p1)' +
                                       '/(3*p1.p1)')
    self.projectors['Longitudinal'] = 'p1(mu1)*p1(mu2)/p1.p1'

    # Fermion projections

    self.projectors['Scalar'] = 'ga(i1,i2)/(4)'
    self.projectors['PseudoScalar'] = '(ga(i1,i2,[+]) - ga(i1,i2,[-]))/(4)'
    self.projectors['Vector'] = 'ga(i1,i2,p1)/(4*p1.p1)'
    self.projectors['PseudoVector'] = ('(ga(i1,i2,[-],p1) ' +
                                       '- ga(i1,i2,[+],p1))/(4*p1.p1)')

    self.projectors['ScalarMinus'] = 'ga(i1,i2,[-])/(2)'
    self.projectors['ScalarPlus'] = 'ga(i1,i2,[+])/(2)'
    self.projectors['VectorMinus'] = 'ga(i1,i2,[-],p1)/(2*p1.p1)'
    self.projectors['VectorPlus'] = 'ga(i1,i2,[+],p1)/(2*p1.p1)'

    self.reno['MS'] = self.regularize_MS
    self.reno['MOM'] = self.regularize_MOM

    self.SI_pattern_c = re.compile(self.SI_pattern, self.global_flags)

  @staticmethod
  def chain(expr, *funcs):
    return fold(lambda x, f: f(x), funcs, expr)

  @classmethod
  def simplify_tensors(cls, amp_string):
    """
    """
    FP = FormPype()
    exprs = ['l amp = ' + '(' + amp_string + ');',
             #'id B0(MZ,MZ,MH) = B0(MZ,MH,MZ);',
             #'id DB0(MZ,MZ,MH) = DB0(MZ,MH,MZ);',
             #'id B0(MW,MW,M0) = B0(MW,M0,MW);',
             #'id DB0(MW,MW,M0) = DB0(MW,M0,MW);',
             #'id B0(M0,MH,MH) = B0(M0,M0,MH)-1;',
             #'id B0(M0,MZ,MZ) = B0(M0,M0,MZ)-1;',
             #'id B0(M0,MW,MW) = B0(M0,M0,MW)-1;',
             #'id B0(MT,M0,MT) = B0(MT,MT,M0);',
             #'id B1(MT,M0,MT) = -B0(MT,MT,M0)-B1(MT,MT,M0);',
             '.sort']
    FP.eval_exprs(exprs)
    res = FP.return_expr('amp', stop=True)

    return ''.join(res.split())

  def compute_projection(self, amp, projection):
    if projection not in self.projectors:
      raise Exception('Unknown projection.')

    FP = FormPype()
    proj = self.projectors[projection]
    exprs = ['l amp = ' + proj + '*(' + amp + ');',
             'id n = 4;',
             '#call lorentzdirac',
             '.sort']
    FP.eval_exprs(exprs)
    return ''.join(FP.return_expr('amp', stop=True).split())

  def compute_derivative(self, amp, FP_debug=False):
    """ Computes the derivative with respect to the momentum p1.  Only B0, B1,
    B00 and B11 are implemented yet.

    >>> RS = Regularize()
    >>> RS.compute_derivative('p1.p1*B0(p1, M0)', FP_debug=True)
    'B0(p1,M0)+DB0(p1,M0)*p1.p1'
    >>> expr = '2/3*A0(MZ)*p1.p1^-1'
    >>> RS.compute_derivative(expr, FP_debug=True)
    '-2/3*A0(MZ)*p1.p1^-2'

    >>> expr = 'B11(p1,MW,mhc)*p1.p1'
    >>> RS.compute_derivative(expr, FP_debug=True)
    'B11(p1,MW,mhc)+DB11(p1,MW,mhc)*p1.p1'

    >>> expr = 'B001(p1,MW,mhc)*p1.p1+2*B0000(p1,MW,mhc)*p1.p1'
    >>> RS.compute_derivative(expr, FP_debug=True)
    'B001(p1,MW,mhc)+2*B0000(p1,MW,mhc)+DB001(p1,MW,mhc)*p1.p1+2*DB0000(p1,MW,mhc)*p1.p1'

    >>> expr = '(B111(p1,MW,mhc)+B1111(p1,MW,mhc))*p1.p1'
    >>> RS.compute_derivative(expr, FP_debug=True)
    'B111(p1,MW,mhc)+B1111(p1,MW,mhc)+DB111(p1,MW,mhc)*p1.p1+DB1111(p1,MW,mhc)*p1.p1'

    >>> expr = 'p1.p1^2 - 6*p1.p1^3'
    >>> RS.compute_derivative(expr, FP_debug=True)
    '2*p1.p1-18*p1.p1^2'
    """
    # TODO: (nick 2015-02-25) need to generalize the formcode dynamically
    allowed_tens = ['B0', 'B1', 'B00', 'B11']
    #tens = [(u[0] + u[1]) in allowed_tens for u in self.tens_dict.values()
            #if u[0][0] != 'D']
    #if not all(tens):
      #warning1_msg = 'Possibly missing derivatives of scalar functions'
      #tens = [u for u in self.tens_dict.values() if u[0][0] != 'D']
      #warning2_msg = 'tensors: ' + str(tens)
      #log(warning1_msg, 'warning')
      #log(warning2_msg, 'warning')
    path = FormPype.return_path() + '/derivate.frm'
    FP = FormPype(path, debug=FP_debug)

    exprs = ['l amp = (' + amp + ');',
             '#call derivate',
             '.sort']

    exprs += ['id mom1 = 1/(p1.p1);',
              'id [mmom2] = -1/(p1.p1)^2;',
              'id [mom3] = 2/(p1.p1)^3;',
              '.sort']
    FP.eval_exprs(exprs)
    return ''.join(FP.return_expr('amp', stop=True).split())

  def expand_complex_momentum(self, amp, M, M2, cM, cM2, FP_debug=False):
    """
    Performs the expansion of the expression `amp` in the complex momenta p1
    around the real mass.
    B(mu^2) = B(m^2) + (mu^2-m^2)*B'(m^2)

    Only B0, B1, B00 and B11 are implemented yet.
    >>> RS = Regularize()
    >>> RS.expand_complex_momentum('B0(p1, M0)', 'M', 'M2', 'cM', 'cM2',\
                                   FP_debug=True)
    'B0(rM,M0)-DB0(rM,M0)*rM2+DB0(rM,M0)*cM2'
    """
    allowed_tens = ['B0', 'B1', 'B00', 'B11']
    path = FormPype.return_path() + '/derivate.frm'
    FP = FormPype(path, debug=FP_debug)

    exprs = ['l ampd = (' + amp + ');',
             '#call derivate',
             '.sort',
             'l amp = (' + amp + ') + (' + cM2 + ' - r' + M2 + ')*ampd;'
             'id p1 = r' + M + ';',
             'id p1.p1 = r' + M2 + ';',
             'id p1.p1^-1 = 1/r' + M2 + ';',
             'argument;',
             '  id p1 = r' + M + ';',
             'endargument;',
             '.sort']

    FP.eval_exprs(exprs)
    return ''.join(FP.return_expr('amp', stop=True).split())

  def test_p2_proportionality(self, amp, FP_debug=False):
    """ Expands the B0 function for small p1.p1

    >>> RS = Regularize()
    >>> RS.test_p2_proportionality('B0(p1, MT, MT) - B0(0, MT,MT)',\
        FP_debug=True)
    '1/6*p1.p1*MT^-2'
    """
    FP = FormPype(debug=FP_debug)
    exprs = ['l amp = (' + amp + ');',
             'id B0(0, M0, M0) = B0(p1, M0, M0);',
             'id B0(0, m?, m?) = B0(p1, m, m) - 1/m**2/6*p1.p1;',
             'id B0(M0, m?, m?) = B0(p1, m, m) - 1/m**2/6*p1.p1;',
             '.sort']
    FP.eval_exprs(exprs)
    return ''.join(FP.return_expr('amp', stop=True).split())

  @staticmethod
  def test_vanish(amp):
    """ Returns if an expression is zero. The computation is done analytically
    by reexpressing couplings with external parameter and simplifying.
    """
    from rept1l.counterterms import Counterterms
    Counterterms.load()

    from rept1l.particles import ParticleMass
    mass_repl = {Symbol(u.name): Symbol('c' + u.name)
                 for u in ParticleMass.masses.values()}

    ncsubs = {Symbol('Nc'): 3}
    coupl = [u for u in amp.free_symbols if (u.name).startswith('GC_')]
    coupl_vals = {u: RCoupling.reduce_to_parameter(u) for u in coupl}
    zero = simplify(amp.subs(coupl_vals))
    zero = simplify(zero.subs(Counterterms.internal_param_ext).
                    subs(mass_repl).subs(ncsubs))
    return zero == 0

  def test_mass_vanish(self, masses, expr):
    """ Returns True if an expression is zero upon setting masses to zero. The
    computation is done analytically.

    :param masses: List of masses, e.g.: masses = ['MT', 'MB']
    :type  masses: list

    :param expr: The expression to be examined
    :type  expr: sympy expr
    """
    from rept1l.particles import ParticleMass
    yuk_inv = {ParticleMass.yukawa[u].name:
               u for u in ParticleMass.yukawa}
    p_set = ([Symbol(u) for u in masses] +
             [Symbol('c' + u) for u in masses] +
             [Symbol('r' + u) for u in masses] +
             [Symbol(yuk_inv[u].name) for u in masses if u in yuk_inv])
    c_set = RCoupling.get_couplings_from_expr(expr)
    found_couplings = RCoupling.find_couplings(p_set, c_set=c_set,
                                               deep_search=True)
    p_set_zero = {u: 0 for u in p_set}
    # assume that those couplings tend to zero as mb -> 0. More complicated
    # cases not handled
    coupl_repl = all(RCoupling.reduce_to_parameter(u, external=True).
                     subs(p_set_zero) == 0 for u in found_couplings)
    c_set_zero = {u: 0 for u in found_couplings}
    c_set_zero.update(p_set_zero)
    res = expr.subs(c_set_zero)
    return res == 0

#------------------------------------------------------------------------------#

  def take_light_limit(self, expr, substitute_zero=True):
    """ Returns the expression obtained when taking the limit for massless
    fermions. Expressions of the form: lambda DB(lambda, lambda1, ....) are
    kept without modification.

    :param expr: The expression to be examined
    :type  expr: sympy expr
    """

    # Get the light masses
    if hasattr(model, 'light_masses'):
      masses = model.light_masses
    else:
      masses = []

    # Build the set of couplings `light_couplings` which depend on the light
    # masses
    from rept1l.particles import ParticleMass
    yuk_inv = {ParticleMass.yukawa[u].name:
               u for u in ParticleMass.yukawa}
    p_set = ([Symbol(u) for u in masses] +
             [Symbol('r' + u) for u in masses] +
             [Symbol('c' + u) for u in masses] +
             [Symbol(yuk_inv[u].name) for u in masses if u in yuk_inv])
    c_set = RCoupling.get_couplings_from_expr(expr)
    light_couplings = RCoupling.find_couplings(p_set, c_set=c_set,
                                               deep_search=True)

    # Substitution rule m -> 0
    p_set_zero = {u: 0 for u in p_set}

    # check that none of the couplings is NaN when substituting for m -> 0
    couplings_behave_well = all(RCoupling.reduce_to_parameter(u, external=True).
                                subs(p_set_zero) == 0 for u in light_couplings)
    if not couplings_behave_well:
      log("Couplings: " + str(light_couplings), 'debug')
      raise Exception('Cannot apply take_light_limit due to badly behaving ' +
                      'couplings.')

    # Substitution rule c -> 0 for m -> 0
    c_set_zero = {u: 0 for u in light_couplings}

    # Write expression in terms of scalar integrals and perform c_set_zero
    # substitution
    cls = self.__class__
    tens_base = [u for u in expr.free_symbols if u.name in cls.tens_dict]
    if len(tens_base) > 0:
      expr_poly = Poly(expr, tens_base).as_dict()
      for key in expr_poly:
        # For rational terms the substitution is always performed
        if sum(key) == 0:
          if substitute_zero:
            expr_poly[key] = expr_poly[key].subs(c_set_zero).subs(p_set_zero)
        else:
          tens = cls.tens_dict[tens_base[key.index(1)].name]
          # if there are  no derivative the substitution is safe
          if tens[0] != 'DB':
            if substitute_zero:
              expr_poly[key] = expr_poly[key].subs(c_set_zero).subs(p_set_zero)
          else:
            # Found a derivative, need to check if the arguments are small
            tens_expr = 'B([],' + ','.join(tens[2]) + ')'
            scaletest = self.is_scaleless(tens_expr)
            if scaletest != 'light':
              if substitute_zero:
                expr_poly[key] = expr_poly[key].subs(c_set_zero).subs(p_set_zero)
            else:
              #potentially ir divergent contribution., replace m -> mreg
              m_to_m_reg = {}
              for mass in model.light_masses:
                m_to_m_reg[Symbol('c' + mass)] = Symbol('c' + mass + '_reg')
                m_to_m_reg[Symbol('r' + mass)] = Symbol('c' + mass + '_reg')
                m_to_m_reg[Symbol(mass)] = Symbol('c' + mass + '_reg')
                expr_poly[key] = expr_poly[key].subs(m_to_m_reg)
      expr_ret = Poly(expr_poly, tens_base).as_expr()
    else:
      expr_ret = expr.subs(c_set_zero)
    return expr_ret

#------------------------------------------------------------------------------#

  def set_mass_zero_arg(self, amp, M):
    """
    >>> RS = Regularize()
    >>> RS.set_mass_zero_arg('B111(p1,M0,M1)', 'M1')
    'B111(p1,M0,M0)'
    """
    FP = FormPype()
    exprs = ['l amp = (' + amp + ');',
             'id ' + M + ' = 0;',
             'argument;',
             '  id ' + M + ' = M0;',
             'endargument;',
             '.sort']
    FP.eval_exprs(exprs)
    return ''.join(FP.return_expr('amp', stop=True).split())

  def set_onshell(self, amp, M, M2):
    """
    >>> RS = Regularize()
    >>> RS.set_onshell('p1.p1^3*B111(p1,M0,M1)', 'M2', 'M2^2')
    'B111(M2,M0,M1)*M2^6'
    """
    FP = FormPype()
    exprs = ['l amp = (' + amp + ');',
             'id p1 = ' + M + ';',
             'id p1.p1 = ' + M2 + ';',
             'id p1.p1^-1 = 1/' + M2 + ';',
             'id p1.p1^-2 = 1/(' + M2 + ')**2;',
             'id p1.p1^-3 = 1/(' + M2 + ')**3;',
             'id p1.p1^-4 = 1/(' + M2 + ')**4;',
             'argument;',
             '  id p1 = ' + M + ';',
             'endargument;',
             '.sort',
             'id M0 = 0;',
             '.sort']
    FP.eval_exprs(exprs)
    return ''.join(FP.return_expr('amp', stop=True).split())

  def fermion_selfenergy_onshell(self, amps_pols, M, M2,
                                 realmomentum=False):
    FP = FormPype()
    exprs = ['l ampS = (' + amps_pols['S'] + ');',
             'l ampPS = (' + amps_pols['PS'] + ');',
             'l ampV = (' + amps_pols['V'] + ')*' + M + ';',
             'l ampPV = (' + amps_pols['PV'] + ');']
             #'#call PV', <- fails in the massless limit

    exprs.extend(['.sort',
                  'l amp1 = ampS + ampV;',
                  'l amp2 = ampPS;',
                  'l amp3 = ampPV;',
                  'id M0 = 0;',
                  '.sort'])

    if not realmomentum:
      exprs.extend(['id p1 = ' + M + ';',
                    'id p1.p1 = ' + M2 + ';',
                    'id p1.p1^-1 = 1/' + M2 + ';',
                    'argument;',
                    '  id p1 = ' + M + ';',
                    'endargument;',
                    '.sort'])

    FP.eval_exprs(exprs)
    return (''.join(FP.return_expr('amp1').split()),
            ''.join(FP.return_expr('amp2').split()),
            ''.join(FP.return_expr('amp3', stop=True).split()))

  def fermion_residue(self, amps_pols, M, M2, realmomentum=False):
    """ Computes the residue - 1 of a fermionic two-point function given the
    scalar and vectorial polarization of the selfenergy.

    :amps_pols: dictionary containing both amplitudes as string
    :M:         Mass of the fermion
    :M2:        Mass squared
    """
    path = FormPype.return_path() + '/derivate.frm'
    FP = FormPype(path)
    exprs = ['l ampSD = (' + amps_pols['S'] + ')/' + M + ';',
             'l ampVD = (' + amps_pols['V'] + ');',
             '#call derivate',
             'l ampV = (' + amps_pols['V'] + ');']

    if not realmomentum:
      exprs.extend(['id p1 = ' + M + ';',
                    'id p1.p1 = ' + M2 + ';',
                    'id p1.p1^-1 = 1/' + M2 + ';',
                    'argument;',
                    '  id p1 = ' + M + ';',
                    'endargument;',
                    '.sort'])
    exprs.extend(['l amp = ampV + 2*' + M2 + '*(ampVD + ampSD );',
                  'id M0 = 0;',
                  '.sort'])

    FP.eval_exprs(exprs)
    return ''.join(FP.return_expr('amp', stop=True).split())

  def decide_MS_or_MOM(self, amp_string):
    """ Decides how the contribution is regularized. If inactive masses are
    found the contribution should be subtracted at zero momentum. Otherwise
    MS(bar) is used. The function is only for 2-point functions.
    >>> CWZ = Regularize()
    >>> Regularize.inactive_masses = ['MT']
    >>> testamp = ('+pi^-2*(-1/32*B([],p1,M0,M0)*c140515(1,1,1,1)^2' +
    ...            '*p1(mu1)*p1(mu2)*Nc)')
    >>> CWZ.decide_MS_or_MOM(testamp)
    'MS'
    >>> testamp = ('+pi^-2*(-1/32*B([],p1,MB,MB)*c140515(1,1,1,1)^2' +
    ...            '*p1(mu1)*p1(mu2)*Nc)')
    >>> CWZ.decide_MS_or_MOM(testamp)
    'MS'
    >>> testamp = ('+pi^-2*(-1/32*B([],p1,MT,MT)*c140515(1,1,1,1)^2' +
    ...            '*p1(mu1)*p1(mu2)*Nc)')
    >>> CWZ.decide_MS_or_MOM(testamp)
    'MOM'
    >>> testamp = ('+pi^-2*(-1/32*B(mu1,[],p1,MT,MT)')
    >>> CWZ.decide_MS_or_MOM(testamp)
    'MOM'
    >>> testamp = ('+pi^-2*(-1/32*A(mu1,[],MT)')
    >>> CWZ.decide_MS_or_MOM(testamp)
    'MOM'
    >>> testamp = ('+pi^-2*(-1/32*A(mu1,[],MB)')
    >>> CWZ.decide_MS_or_MOM(testamp)
    'MS'
    >>> testamp = ('+pi^-2*(-1/32*B([],p1,MT,MT)*c140515(1,1,1,1)^2' +
    ...            '*p1(mu1)*p1(mu2)*Nc + 1/32*B([],p1,M0,M0)*' +
    ...            'c140515(1,1,1,1)^2*p1(mu1)*p1(mu2)*Nc)')
    >>> CWZ.decide_MS_or_MOM(testamp)
    Traceback (most recent call last):
      ...
    MixedException: Encountered mixed amplitude.
    """
    tn = re.findall(self.SI_pattern_c, amp_string)
    nmass = 0
    tn_ms = {}
    ms_sets = {}
    for t in tn:
      ms = [u for u in t.split(',') if u.startswith('M') or u.startswith('m')]
      nms = len(ms)
      if nms > nmass:
        nmass = len(ms)
      if nms not in ms_sets:
        ms_sets[nms] = set()
      tn_ms[t] = ms
      ms_sets[nms].add(tuple(sorted(ms)))

    mass_conf = ms_sets[nmass]
    if len(mass_conf) > 1:
      raise MixedException('Encountered mixed amplitude.')
    mass_conf = mass_conf.pop()
    # check that all tensors have the same argument.
    for i in range(1, nmass):
      if i in ms_sets:
        for ms_set in ms_sets[i]:
          if not all(u in mass_conf for u in ms_set):
            raise MixedException('Encountered mixed amplitude.')
    # check that all tensors have the same argument.
    #if not all(tn[0] == tens for tens in tn):
      #print("tn:", tn)
      #raise MixedException('Encountered mixed amplitude: ' + amp_string)

    if any(u in Regularize.inactive_masses for u in tn[0].split(',')[-2:]):
      return 'MOM'
    else:
      return 'MS'

  def is_scaleless(self, amp_string, return_masses=False):
    """
    >>> CWZ = Regularize()
    >>> testamp = ('+pi^-2*(-1/32*B([],p1,M0,M0)*c140515(1,1,1,1)^2' +
    ...            '*p1(mu1)*p1(mu2)*Nc)')
    >>> CWZ.is_scaleless(testamp)
    'scaleless'
    >>> testamp = ('+pi^-2*(-1/32*B([],p1,MT,MT)*c140515(1,1,1,1)^2' +
    ...            '*p1(mu1)*p1(mu2)*Nc)')
    >>> CWZ.is_scaleless(testamp)
    'massive'
    >>> testamp = ('+pi^-2*(-1/32*B([],p1,MT,MT)*c140515(1,1,1,1)^2' +
    ...            '*p1(mu1)*p1(mu2)*Nc + 1/32*B([],p1,M0,M0)*' +
    ...            'c140515(1,1,1,1)^2*p1(mu1)*p1(mu2)*Nc)')
    >>> CWZ.is_scaleless(testamp)
    Traceback (most recent call last):
      ...
    ScaleException: Encountered mixed amplitude:set([',p1,MT,MT', ',p1,M0,M0'])
    >>> testamp = ('+pi^-2*(-1/32*B([],p1,MT,MT)*c140515(1,1,1,1)^2' +
    ...            '*p1(mu1)*p1(mu2)*Nc + 1/32*B([],p1,MW,MW)*' +
    ...            'c140515(1,1,1,1)^2*p1(mu1)*p1(mu2)*Nc)')
    >>> CWZ.is_scaleless(testamp)
    'massive'
    """
    tn = set(re.findall(self.SI_pattern_c, amp_string))
    if len(tn) == 0:
      return False

    masses = set()
    answer = []
    for tens in tn:
      tens_args = tens.split(',')[1:]
      if len(tens_args) == 1:
        tens_masses = tens_args
      elif len(tens_args) == 3:
        tens_masses = tens_args[1:]
      else:
        raise Exception('Case ' + str(tens_args) + ' not handled in ' +
                        '`is_scaleless`')

      #tens_masses = tens.split(',')[2:]
      masses.add(tuple(tens_masses))
      if tens_masses.count('M0') == len(tens_masses):
        answer.append('scaleless')
      elif(hasattr(model, 'light_masses') and
           all(u in model.light_masses or u == 'M0' for u in tens_masses)):
        answer.append('light')
      else:
        answer.append('massive')

    if len(set(answer)) == 1:
      if return_masses:
        return answer[0], masses
      else:
        return answer[0]
    else:
      raise ScaleException('Encountered mixed amplitude:' + str(set(tn)))

  def regularize_MS(self, amp_string, on_tensor=True, FP_debug=False):
    """ Extracts the divergent part of the amplitude via dimensional
    regularization by computing lim_{D->4} (D-4) * expr.

    The conventions are defined in the formcode `MS.prc` and taken from
    http://arXiv.org/abs/0709.1075v1. The results are multiplied with -
    DeltaUV/2, where DeltaUV is defined by :

      DeltaUV = \frac{2}{4-D}- \gamma_E + \log(4 \pi)


    >>> CWZ = Regularize()
    >>> testamp = ('+pi^-2*(-1/32*B1(p1,M0,M0)*c140515(1,1,1,1)^2' +
    ...            '*p1(mu1)*p1(mu2)*Nc)')
    >>> CWZ.regularize_MS(testamp, on_tensor=False)
    '1/64*c140515(1,1,1,1)^2*p1(mu1)*p1(mu2)*pi^-2*Nc*DeltaUV'

    >>> testamp = ('B0000(p1,M0,M0)')
    >>> CWZ.regularize_MS(testamp, on_tensor=False)
    '1/240*p1.p1^2*DeltaUV'
    """
    FP = FormPype(debug=FP_debug)
    if not on_tensor:
      exprs = ['l amp = ' + '(' + amp_string + ');',
               '#call MS',
               'id M0 = 0;',
               'id n = 4;',
               '.sort',
               'l amp = -amp*DeltaUV/2;',
               '.sort']
    else:
      exprs = ['l amp = ' + '(' + amp_string + ');',
               '#call MStens',
               'id M0 = 0;',
               'id n = 4;',
               'id 1/ep = 1;',
               '.sort',
               'l amp = amp*DeltaUV/2;',
               '.sort']
    FP.eval_exprs(exprs)
    res = FP.return_expr('amp', stop=True)
    return res

  def regularize_MOM(self, amp_string, FP_debug=False):
    """ Replaces the momentum dependence by the zero-momentum part of the
    amplitude.

    >>> CWZ = Regularize()
    >>> testamp = '- 1/32*c140515(1,1,1,1)^2*B([],p1,M0,M0)'
    >>> CWZ.regularize_MOM(testamp, FP_debug=True)
    '-1/32*c140515(1,1,1,1)^2*B0(0,M0,M0)'
    >>> testamp = '- B([],p1,M0,M0) + MT^2'
    >>> CWZ.regularize_MOM(testamp, FP_debug=True)
    'MT^2-B0(0,M0,M0)'
    >>> testamp = 'B0(p1,M0,M0)*(ga(i1,i2,[],p1,p3,mu3)+ga(i1,i2,[],mu3)*MT^2)'
    >>> CWZ.regularize_MOM(testamp, FP_debug=True)
    'ga(i1,i2,[],p1,p3,mu3)*B0(0,M0,M0)+ga(i1,i2,[],mu3)*B0(0,M0,M0)*MT^2'
    """
    FP = FormPype(debug=FP_debug)
    exprs = ['l amp = ' + '(' + amp_string + ');',
             '#call LC',
             'argument B,B0,B1,B00,B11,C,C0,C1,C2,C00,C12,C11,C22;',
             ' id p1 = 0;',
             ' id p3 = 0;',
             'endargument;',
             '.sort',
             'l amp = amp;',
             '.sort']

    FP.eval_exprs(exprs)
    amp = FP.return_expr('amp', stop=True)
    return amp

  def summed_amplitudes(self, exprs, amps_reg, pjs=None, filter_zero=True):
    with FormPype() as FP:
      FP.eval_exprs(exprs)
      for amp_id in amps_reg:
        cs = amp_id[0]
        amp_order = amp_id[1]
        order = ','.join([u[1] for u in amp_order])
        if pjs is None:
          res = ''.join(FP.return_expr('[amp(' + cs + ',' + order + ')]').
                        split())
          if res != '0':
            yield res, cs
        else:
          res = {pj: ''.join(FP.return_expr('[amp(' + cs + ',' +
                                            order + ',pj=' + pj + ')]').split())
                 for pj in pjs}

          if filter_zero:
            res = {pj: res[pj] for pj in res if res[pj] != '0'}
          yield res, cs

  def ms_massive_vector(self, filein, **kwargs):
    """
    """
    self.amplitudes = {}
    cls = self.__class__
    with open(filein, 'r') as fin:
      text = fin.read()
      matches = re.findall(self.global_pattern, text)

      # sum up amplitudes with the same colorflow
      amps_reg = []
      exprs = []
      pjs = {'T': 'Transverse', 'L': 'Longitudinal'}
      for match_id, match in enumerate(matches):
        amp_kind, amp_ids = self.register_amplitude(match[0], amps_reg,
                                                    exprs, pjs)
        if match[1].strip() != '0':
          amp = ''.join(match[1].split())
          amp = self.assign_couplings(amp, {1: self.treevertices,
                                            2: self.ct_vertices})

          amps = {pj: self.compute_projection(amp, pjs[pj]) for pj in pjs}
          if amp_kind != 'AmpCT':
            amps = {pj: self.reno['MS'](amps[pj]) for pj in pjs}

          exprs.extend(['l ' + amp_ids[pj] + ' = ' + amp_ids[pj] +
                        ' + (' + amps[pj] + ');' for pj in pjs])
          exprs.append('.sort')

      coupl_dict_inv = {Symbol(self.coupl_dict[key]): key
                        for key in self.coupl_dict}

      for res, cs in self.summed_amplitudes(exprs, amps_reg, pjs):
        levels = 4
        pg = ProgressBar(levels)

        sigmaT = res['T']
        pg.advanceAndPlot()
        funcs = (self.assign_lorentz, self.parse_UFO, )

        sigmaT = self.chain(sigmaT, *funcs)
        pg.advanceAndPlot()

        sigmaT = sigmaT.subs(coupl_dict_inv)
        ls_base = [Symbol(u) for u in self.ls_dict]
        sigmaT_ls = Poly(sigmaT, ls_base).as_dict()
        pg.advanceAndPlot()
        for p in sigmaT_ls:
          expr = sigmaT_ls[p]
          expr = expr.subs(coupl_dict_inv)
          if self.simplify_expr:
            tens_base = [Symbol(u) for u in cls.tens_dict]
            expr = self.simplify_expr(expr, tens_base)
          ls_key = sum([pow(ls_base[pos], u) for pos, u in enumerate(p)])

          if expr != '0':
            if (int(cs), ls_key) not in self.amplitudes:
              self.amplitudes[(int(cs), ls_key)] = 0
            self.amplitudes[(int(cs), ls_key)] += expr
        pg.advanceAndPlot()

      append_cond = [self.amplitudes[u] for u in self.amplitudes if u[0] == 1]
      for cond in append_cond:
        self.renorm_conditions.append(cond)

  def CWZ_massless_vector(self, filein, scheme='MSMOM', **kwargs):
    """
    Applies the CWZ procedure.

    :param scheme: sets the used scheme, either pure MS substraction `MS` or MS
                   and momentum substraction mixed `MSMOM`. Momentum subtraction
                   is used if a contribution to an amplitude has massive
                   internal lines.
    :type  scheme: "MS" or "MSMOM"
    """
    cls = self.__class__
    try:
      assert(scheme == 'MS' or scheme == 'MSMOM')
    except:
      raise TypeError('Scheme must be set to "MS" or "MSMOM"')
    self.amplitudes = {}
    with open(filein, 'r') as fin:
      text = fin.read()
      matches = re.findall(self.global_pattern, text)
      # sum up amplitudes with the same colorflow
      amps_reg = []
      exprs = []
      for match_id, match in enumerate(matches):
        amp_kind, amp_id = self.register_amplitude(match[0], amps_reg, exprs)
        if match[1].strip() != '0':
          amp = ''.join(match[1].split())
          amp = self.assign_couplings(amp, {1: self.treevertices,
                                            2: self.ct_vertices})
          if amp_kind != 'AmpCT':
            if scheme == 'MSMOM':
              reno = self.decide_MS_or_MOM(amp)

              if reno == 'MS':
                # pure ms can be computed without computing rational terms
                amp = self.compute_projection(amp, 'Transverse')
              else:
                amp = self.chain(amp,
                                 lambda x:
                                 self.compute_reduction(x, A0_to_B0=True),
                                 lambda x:
                                 self.compute_projection(x, 'Transverse'),
                                 self.test_p2_proportionality)
              amp = self.reno[reno](amp)
            elif scheme == 'MS':
              amp = self.compute_projection(amp, 'Transverse')
              amp = self.reno['MS'](amp)

          else:
            amp = self.compute_projection(amp, 'Transverse')
          exprs.extend(['l ' + amp_id + ' = ' + amp_id + ' + (' + amp + ');',
                        '.sort'])

      coupl_dict_inv = {Symbol(self.coupl_dict[key]): key
                        for key in self.coupl_dict}
      for res, cs in self.summed_amplitudes(exprs, amps_reg):
        res = self.assign_lorentz(res)
        res = self.assign_tensors(res, cls.tens_dict)
        res = self.parse_UFO(res)
        base = [Symbol(u) for u in self.ls_dict.keys()]
        p2base = ([Symbol('P'), Symbol('P')], [[-1, 1], [-1, 1]])
        try:
          # assert(len(base) == 1) <- base elements might have dropped out
          assert(p2base in self.ls_dict.values())
        except:
          # For massless vectors the selfenergy must be proportional to p^2
          # without further momentum dependence. More general is not
          # implemented yet.
          error_msg = ('Selfenergy has higher momentum dependence. ' +
                       'Case not implemented.')
          debug_msg = ('Base: ' + str(base) + '\n' +
                       'ls_dict: ' + str(self.ls_dict) + '\n' +
                       'Transversal amplitude:' + str(res) + '\n' +
                       'coupl_dict: ' + str(self.coupl_dict) + '\n' +
                       'tens_dict: ' + str(cls.tens_dict))

          log(error_msg, 'warning')
          log(debug_msg, 'debug')

        ls_key = Symbol(list(self.ls_dict.keys())[list(self.ls_dict.values()).
                                            index(p2base)])

        #try:
        self.verify_massless_selfenergy(res, coupl_dict_inv)
        # everthing went fine and we extract the coefficient of p^2

        key = tuple([1 if u == ls_key else 0 for u in base])
        res = Poly(res, base).as_dict()[key]

        res = res.subs(coupl_dict_inv)
        if self.simplify_expr:
          tens_base = [Symbol(u) for u in cls.tens_dict]
          res = self.simplify_expr(res, tens_base)
        if int(cs) not in self.amplitudes:
          self.amplitudes[int(cs)] = 0
        self.amplitudes[int(cs)] += res

      self.renorm_conditions['1'] = self.amplitudes[1]

  def CWZ_fermion(self, filein, scheme='MSMOM', **kwargs):
    """
    Applies the CWZ procedure.

    :param scheme: either pure MS substraction `MS` or MS and momentum
                   substraction mixed `MSMOM`. Momentum subtraction is used if a
                   contribution to an amplitude has massive internal lines.
    :type  scheme: string
    """
    cls = self.__class__
    try:
      assert(scheme == 'MS' or scheme == 'MSMOM')
    except:
      raise TypeError('Scheme must be set to "MS" or "MSMOM"')
    self.amplitudes = {}
    with open(filein, 'r') as fin:
      text = fin.read()
      matches = re.findall(self.global_pattern, text)
      # sum up amplitudes with the same colorflow
      amps_reg = []
      exprs = []
      pjs = {'S': 'Scalar', 'PS': 'PseudoScalar',
             'V': 'Vector', 'PV': 'PseudoVector'}
      for match_id, match in enumerate(matches):
        amp_kind, amp_ids = self.register_amplitude(match[0], amps_reg,
                                                    exprs, pjs)
        if match[1].strip() != '0':
          amp = ''.join(match[1].split())
          amp = self.assign_couplings(amp, {1: self.treevertices,
                                            2: self.ct_vertices})
          if amp_kind != 'AmpCT':
            if scheme == 'MSMOM':
              try:
                reno = self.decide_MS_or_MOM(amp)

                if reno == 'MS':
                  # pure ms can be computed without computing rational terms
                  amps = {pj: self.compute_projection(amp, pjs[pj])
                          for pj in pjs}

                else:
                  # note that PV reduction needs to be performed for MOM
                  amp = self.compute_reduction(amp)  # loop (+ R1) + R2
                  amps = {pj: self.compute_projection(amp, pjs[pj])
                          for pj in pjs}

                amps = {pj: self.reno[reno](amps[pj]) for pj in pjs}

              except MixedException:
                amps = {}
                for amp in self.amplitude_split_tensors(amp).values():
                  reno = self.decide_MS_or_MOM(amp)
                  if reno == 'MS':
                    # pure ms can be computed without computing rational terms
                    amps_split = {pj: self.compute_projection(amp, pjs[pj])
                                  for pj in pjs}

                  else:
                    amp = self.compute_reduction(amp)  # loop (+ R1) + R2
                    amps_split = {pj: self.compute_projection(amp, pjs[pj])
                                  for pj in pjs}

                  for pj in pjs:
                    if pj not in amps:
                      amps[pj] = [self.reno[reno](amps_split[pj])]
                    else:
                      amps[pj].append(self.reno[reno](amps_split[pj]))

                amps = {pj: fold(self.form_add, amps[pj][1:], amps[pj][0])
                        for pj in amps}
              except Exception as e:
                print(e)
                sys.exit()

            elif scheme == 'MS':

              amps = {pj: self.compute_projection(amp, pjs[pj]) for pj in pjs}
              amps = {pj: self.reno['MS'](amps[pj]) for pj in pjs}

          else:

            amps = {pj: self.compute_projection(amp, pjs[pj]) for pj in pjs}

          exprs.extend(['l ' + amp_ids[pj] + ' = ' + amp_ids[pj] +
                        ' + (' + amps[pj] + ');' for pj in pjs])
          exprs.append('.sort')

      coupl_dict_inv = {Symbol(self.coupl_dict[key]): key
                        for key in self.coupl_dict}
      for res, cs in self.summed_amplitudes(exprs, amps_reg, pjs):
        for pj in res:
          amp = self.assign_lorentz(res[pj])
          amp = self.assign_tensors(amp, cls.tens_dict)
          amp = self.parse_UFO(amp)
          amp = amp.subs(coupl_dict_inv)
          if self.simplify_expr:
            tens_base = [Symbol(u) for u in cls.tens_dict]
            amp = self.simplify_expr(amp, tens_base)

          if (int(cs), pj) not in self.amplitudes:
            self.amplitudes[(int(cs), pj)] = 0
          self.amplitudes[(int(cs), pj)] += amp

      for pj in pjs:
        # 1 is default colorflow
        if (1, pj) in self.amplitudes:
          self.renorm_conditions[pj] = self.amplitudes[(1, pj)]

  def CWZ_scalar(self, filein, scheme='MSMOM', **kwargs):
    """
    Applies the CWZ regularization to a scalar particle.

    :param filein: The FORM amplitude of the vertex function
    :type  filein: str
    :param scheme: sets the used scheme, either pure MS substraction `MS` or MS
                   and momentum substraction mixed `MSMOM`. Momentum subtraction
                   is used if a contribution to an amplitude has massive
                   internal lines.
    :type  scheme: "MS" or "MSMOM"
    """
    cls = self.__class__
    try:
      assert(scheme == 'MS' or scheme == 'MSMOM')
    except:
      raise TypeError('Scheme must be set to "MS" or "MSMOM"')
    self.amplitudes = {}
    with open(filein, 'r') as fin:
      text = fin.read()
      matches = re.findall(self.global_pattern, text)
      # sum up amplitudes with the same colorflow
      amps_reg = []
      exprs = []
      for match_id, match in enumerate(matches):
        amp_kind, amp_id = self.register_amplitude(match[0], amps_reg, exprs)
        if match[1].strip() != '0':
          amp = ''.join(match[1].split())
          amp = self.assign_couplings(amp, {1: self.treevertices,
                                            2: self.ct_vertices})
          if amp_kind != 'AmpCT':
            if scheme == 'MSMOM':
              try:
                reno = self.decide_MS_or_MOM(amp)

                # pure ms can be computed without computing rational terms
                if reno == 'MOM':
                  # note that PV reduction needs to be performed for MOM
                  amp = self.compute_reduction(amp)
                amp = self.reno[reno](amp)
              except MixedException:
                amps = []
                for amp in self.amplitude_split_tensors(amp).values():
                  reno = self.decide_MS_or_MOM(amp)
                  if reno == 'MS':
                    # pure ms can be computed without computing rational terms
                    amp = self.regularize_MS(amp)
                    amps.append(amp)

                  else:
                    amp = self.compute_reduction(amp)  # loop (+ R1) + R2
                    amp = self.regularize_MOM(amp)
                    amps.append(amp)

                amp = fold(self.form_add, amps[1:], amps[0])
            elif scheme == 'MS':
              amp = self.reno['MS'](amp)

          exprs.extend(['l ' + amp_id + ' = ' + amp_id + ' + (' + amp + ');',
                        '.sort'])

      coupl_dict_inv = {Symbol(self.coupl_dict[key]): key
                        for key in self.coupl_dict}
      for res, cs in self.summed_amplitudes(exprs, amps_reg):
        res = self.assign_lorentz(res)
        res = self.assign_tensors(res, cls.tens_dict)
        res = self.parse_UFO(res)
        res = res.subs(coupl_dict_inv)
        if self.simplify_expr:
          tens_base = [Symbol(u) for u in cls.tens_dict]
          res = self.simplify_expr(res, tens_base)
        if int(cs) not in self.amplitudes:
          self.amplitudes[int(cs)] = 0
        self.amplitudes[int(cs)] += res

      ls_base = [Symbol(u) for u in self.ls_dict]
      if len(ls_base) > 0:
        ls_poly = Poly(self.amplitudes[1], ls_base)
        for ls_key in ls_poly.monoms():
          coeff = ls_poly.coeff_monomial(ls_key)
          if 1 in ls_key:
            b = reduce(lambda x, y: x*y, [ls_base[pos]**pow
                       for pos, pow in enumerate(ls_key)])
          else:
            b = '1'
          self.renorm_conditions[b] = (together(coeff))
      else:
        self.renorm_conditions['1'] = together(self.amplitudes[1])

  def ms_scalar(self, filein, **kwargs):
    """
    Computes the divergent part for scalars and prepares the renormalization
    conditions. Different renormalization conditions are associated to different
    lorentz structures, mostly the scalar terms and proportional to p^2.
    """
    cls = self.__class__
    self.amplitudes = {}
    # register and regularize all subamplitudes
    with open(filein, 'r') as fin:
      text = fin.read()
      matches = re.findall(self.global_pattern, text)

      # sum up amplitudes with the same colorflow
      amps_reg = []
      exprs = []
      for match_id, match in enumerate(matches):
        amp_kind, amp_id = self.register_amplitude(match[0], amps_reg, exprs)
        if match[1].strip() != '0':
          amp = ''.join(match[1].split())
          amp = self.assign_couplings(amp, {1: self.treevertices,
                                            2: self.ct_vertices})
          if amp_kind != 'AmpCT':
            amp = self.reno['MS'](amp)

          exprs.extend(['l ' + amp_id + ' = ' +
                        amp_id + ' + (' + amp + ');',
                        '.sort'])

      # prepare coupling and mass substitutions
      coupl_dict_inv = {Symbol(self.coupl_dict[key]): key
                        for key in self.coupl_dict}
      from rept1l.particles import ParticleMass
      mass_repl = {Symbol(u.name): Symbol('c' + u.name)
                   for u in ParticleMass.masses.values()}

      for res, cs in self.summed_amplitudes(exprs, amps_reg):
        # ms subtracted expression still has lorentz structures which need to be
        # parsed
        funcs = (self.assign_lorentz, self.parse_UFO, )
        sigma = self.chain(res, *funcs)

        # the renormalization equations are derived for different lorentz
        # structures
        ls_base = [Symbol(u) for u in self.ls_dict]
        sigma_ls = Poly(sigma, ls_base).as_dict()
        for p in sigma_ls:
          expr = sigma_ls[p]
          expr = expr.subs(coupl_dict_inv)
          if self.simplify_expr:
            tens_base = [Symbol(u) for u in cls.tens_dict]
            expr = self.simplify_expr(expr, tens_base)
          ls_key = sum([pow(ls_base[pos], u) for pos, u in enumerate(p)])

          if expr != '0':
            if (int(cs), ls_key) not in self.amplitudes:
              self.amplitudes[(int(cs), ls_key)] = 0
            self.amplitudes[(int(cs), ls_key)] += expr

      # only take the first colorflow
      append_cond = [self.amplitudes[u] for u in self.amplitudes if u[0] == 1]
      for pos, cond in enumerate(append_cond):
        self.renorm_conditions[pos] = cond

  def replace_tensor_symbols(self):
    """
    Replaces the symbol names for Tensors (T0, T1, ...) with the scalar tensor
    integrals.


    ! Must be really cautious with this command!!

    >>> RS = Regularize()
    >>> T0 = Symbol('T0')
    >>> T0
    T0
    >>> Regularize._tens_dict = {'T0': ('B', '0', ('M0', 'M0', 'M0'))}
    >>> RS.replace_tensor_symbols()
    >>> T0
    B0(M0,M0,M0)
    """
    cls = self.__class__
    for tens in cls.tens_dict:
      tt = Symbol(tens)
      t_id = cls.tens_dict[tens][0]
      t_sc_id = cls.tens_dict[tens][1]
      t_args = '(' + ','.join(cls.tens_dict[tens][2]) + ')'
      tt.name = cls.tens_dict[tens][0] + t_sc_id + t_args

  def construct_tensor_symbols(self):
    """
    Generates the substitution rules for the tensors symbols (T0, T1, ...) with
    their actual scalar integral representation.

    >>> RS = Regularize()
    >>> T0 = Symbol('T0')
    >>> Regularize.loaded = True
    >>> Regularize._tens_dict = {'T0': ('B', '0', ('M0', 'M0', 'M0'))}
    >>> RS.construct_tensor_symbols()
    {T0: 'B0(M0,M0,M0)'}
    """
    cls = self.__class__
    repl_dict = {}
    for tens in cls.tens_dict:
      tt = Symbol(tens)
      t_id = cls.tens_dict[tens][0]
      t_sc_id = cls.tens_dict[tens][1]
      t_args = '(' + ','.join(cls.tens_dict[tens][2]) + ')'
      repl_dict[tt] = cls.tens_dict[tens][0] + t_sc_id + t_args
    return repl_dict

  def extract_ms(self, expr, **kwargs):
    """ Extracts the singular part from an expression. The method assumes that
    the expression is given in terms of parameters(symbols) and scalar
    integrals. The singular terms are obtained by substituting the scalar
    integrals with their respective divergent part being defined in the form
    module `MS.prc`. It is assumed that couplings/parameters do not contain any
    divergences.

    :param expr: symbolic expression in terms of parameters and scalar integrals
    :type  expr: sympy

    >>> RS = Regularize(); Regularize.loaded = True
    >>> T99 = Symbol('T99'); a = Symbol('a'); b = Symbol('b')
    >>> Regularize._tens_dict = {'T99': ('B', '0', ('M0', 'M0', 'M0'))}
    >>> uv = RS.extract_ms(T99*a+b, on_tensor=False)
    >>> uv
    DeltaUV*a

    Reextracting the uv divergence possible
    >>> RS.extract_ms(uv)
    DeltaUV*a
    """

    cls = self.__class__

    # expand the expr in terms of scalar integrals
    sis = []
    for u in expr.free_symbols:
      if u.name.startswith('T'):
        if u.name not in cls.tens_dict:
          raise Exception("Missing scalar integrals in tens_dict.")
        elif cls.tens_dict[u.name][0] != 'HT':
          sis.append(u)
          #pass

    tens_base = sis
    expr = together(expr)

    if tens_base == []:
      expr_poly = {(0,): expr}
    else:
      expr_poly = Poly(expr, tens_base).as_dict()

    res = 0
    # sis repl from tensor symbol to scalar tensor expression
    tens_repl = self.construct_tensor_symbols()
    for key in expr_poly:
      # assume linear combinations of scalar tensors
      if sum(key) > 1:
        error_msg = 'Expression is not a linear combination of scalar integrals'
        log(error_msg, 'error')
        debug_msg = 'Expr: ' + str(expr)
        log(debug_msg, 'debug')
        debug_msg = ('This could mean that there are unknown symbols in the ' +
                     'expression, .e.g. not yet computed counterterm paramter')
        log(debug_msg, 'debug')
        raise Exception('Incompatible combination of scalar integrals.')
      if 1 in key:
        # translate tensor into scalar tensor expression
        tens = tens_base[key.index(1)]
        tens = tens_repl[tens]
        # extract ms contribution and multiply with couplings, add to res
        reg = self.regularize_MS(tens, **kwargs)
        res += (self.parse_UFO(reg) * expr_poly[key])
      else:
        DeltaUV = Symbol('DeltaUV')
        res += DeltaUV*expr_poly[key].coeff(DeltaUV)
    res = together(res)
    return res

  def onshell_massless_vector(self, filein, check_massless=False, **kwargs):
    """
    # TODO: (JNL 2016-10-02) Printing a warning if check_massless is used in
    # combination with light fermions. A safe PV reduction has to be be used.

    :param check_massless: Check if transverse selfenergy is proportional to p^2
    :type  check_massless: bool
    """
    cls = self.__class__
    self.amplitudes = {}
    with open(filein, 'r') as fin:
      text = fin.read()
      matches = re.findall(self.global_pattern, text)

      # sum up amplitudes with the same colorflow
      amps_reg = []
      exprs = []
      pjs = {'T': 'Transverse', 'L': 'Longitudinal'}
      for match_id, match in enumerate(matches):
        amp_kind, amp_ids = self.register_amplitude(match[0], amps_reg,
                                                    exprs, pjs)

        if match[1].strip() != '0':
          amp = ''.join(match[1].split())
          amp = self.assign_couplings(amp, {1: self.treevertices,
                                            2: self.ct_vertices})

          # The Passarino Veltman reduction is needed in order to see that the
          # selfenergy is actually transversal.
          if amp_kind != 'AmpCT':
            try:
              scales = self.is_scaleless(amp)
              if scales == 'scaleless':  # True -> massless
                #rational terms originating from UV cancel against IR
                amp = self.compute_reduction(amp, R1=False, R2=False, PV=True,
                                             A0_to_B0=True)
              elif scales == 'light':
                raise ScaleException('Cases should be handled individually!')
              else:
                if check_massless:
                  amp = self.compute_reduction(amp, PV=True, A0_to_B0=True)
                else:
                  amp = self.compute_reduction(amp, PV=False)
            except ScaleException:
              amps = []
              # amplitudes has multiple scales -> sort amplitudes according to
              # scalar integrals
              for amp in self.amplitude_split_tensors(amp).values():
                scales, masses = self.is_scaleless(amp, return_masses=True)
                if scales == 'scaleless':
                  amp = self.compute_reduction(amp, R1=False, R2=False,
                                               PV=True, A0_to_B0=True)
                elif scales == 'light':
                  # in case of massless fermions the UV R2 rational terms
                  # cancel against IR R2 rational terms. Thus, the UV R2 terms
                  # are multiplied with a function which is zero for massless
                  # fermions
                  assert(len(masses) == 1)
                  mass_case = masses.pop()
                  if check_massless:
                    log('Performing PV reduction with UV rational terms.' +
                        'This will yield a wrong result in combination with ' +
                        'light scales in case masses are set to zero',
                        'warning')
                    amp = self.compute_reduction(amp, PV=True, A0_to_B0=True,
                                                 zero_mass_theta=mass_case)
                  else:
                    amp = self.compute_reduction(amp, PV=False,
                                                 zero_mass_theta=mass_case)
                else:
                  if check_massless:
                    amp = self.compute_reduction(amp, PV=True, A0_to_B0=True)
                  else:
                    amp = self.compute_reduction(amp, PV=False)
                amps.append(amp)
              assert(len(amps) > 1)
              # add up all contributions
              amp = fold(self.form_add, amps[1:], amps[0])

          # compute the projections of all amplitudes and pass them to FORM
          amps = {pj: self.compute_projection(amp, pjs[pj]) for pj in pjs}
          exprs.extend(['l ' + amp_ids[pj] + ' = ' + amp_ids[pj] +
                        ' + (' + amps[pj] + ');' for pj in pjs])
          exprs.append('.sort')

      # prepare coupling and mass substitutions
      coupl_dict_inv = {Symbol(self.coupl_dict[key]): key
                        for key in self.coupl_dict}
      from rept1l.counterterms import Counterterms
      Counterterms.load()

      from rept1l.particles import ParticleMass
      mass_repl = {Symbol(u.name): Symbol('c' + u.name)
                   for u in ParticleMass.masses.values()}

      m = 'M0'
      m2 = 'M0^2'
      for res, cs in self.summed_amplitudes(exprs, amps_reg, pjs):
        for pj in res:
          amp = self.assign_lorentz(res[pj])
          if pj == 'L' and check_massless:
            amp = self.set_onshell(amp, m, m2)
            amp = self.assign_tensors(amp, cls.tens_dict)
            amp = self.parse_UFO(amp)

            HT_subs = {u: 1 for u in self.tens_dict
                       if self.tens_dict[u][0] == 'HT'}

            resL = amp.subs(coupl_dict_inv).subs(HT_subs)

            # Transversal selfenergy should be zero, in the simplest case the
            # result is already zero and we do not need to specify the couplings

            if not self.test_vanish(resL):
              warning_msg = 'Selfenergy has longitudinal part.'
              debug_msg = (str(resL) + '\n'
                           'The couplings and tensors dict read: \n' +
                           'coupl_dict: ' + str(self.coupl_dict) + '\n' +
                           'tens_dict: ' + str(cls.tens_dict) + '\n' +
                           'The program continues and it is left to the' +
                           ' user to check this result.')
              log(warning_msg, 'warning')
              log(debug_msg, 'debug')
            else:
              log('Massless vector has no longitudinal part ✓', 'info')

          else:
            # TODO: (nick 2015-02-18) not sure if safe enough
            #      the process A -> A in pure EW does not raise an error when
            #      the test_p2 is left out because the the amplitude consists of
            #      terms B(p) - B(0) -> setting onshell gives 0 instead of p^2
            #      DB(0)

            if check_massless:
              resT = self.test_p2_proportionality(res[pj])
              resT = self.assign_lorentz(resT)
              resT = self.set_onshell(resT, m, m2)
              resT = self.assign_tensors(resT, cls.tens_dict)
              resT = self.parse_UFO(resT)
              resT = resT.subs(coupl_dict_inv)

              if self.simplify_expr:
                tens_base = [Symbol(u) for u in cls.tens_dict]
                resT = self.simplify_expr(resT, tens_base)

              base = [Symbol(u) for u in self.ls_dict.keys()]
              p2base = ([Symbol('P'), Symbol('P')], [[-1, 1], [-1, 1]])
              try:
                # assert(len(base) == 1) <- base elements might have dropped out
                assert(p2base in self.ls_dict.values())
              except:
                # For massless vectors the selfenergy must be proportional to
                # p^2 without further momentum dependence. More general is not
                # implemented yet.
                warning_msg = ('Selfenergy has higher momentum dependence. ' +
                               'Case not implemented.')
                debug_msg = ('Base: ' + str(base) + '\n' +
                             'ls_dict: ' + str(self.ls_dict) + '\n' +
                             'Transversal amplitude:' + str(resT) + '\n' +
                             'coupl_dict: ' + str(self.coupl_dict) + '\n' +
                             'tens_dict: ' + str(cls.tens_dict))

                log(warning_msg, 'warning')
                log(debug_msg, 'debug')

              ls_key = list(self.ls_dict.keys())[list(self.ls_dict.values()).index(p2base)]

              HT_subs = {u: 1 for u in self.tens_dict
                         if self.tens_dict[u][0] == 'HT'}
              self.verify_massless_selfenergy(resT.subs(HT_subs),
                                              coupl_dict_inv)
              log('Massless vector selfenergy is proportional to p^2 ✓', 'info')
              resT = resT.coeff(ls_key)
            else:
              resT = self.compute_derivative(res[pj])
              resT = self.set_onshell(resT, m, m2)
              resT = self.assign_tensors(resT, cls.tens_dict)
              resT = self.parse_UFO(resT)
              resT = resT.subs(coupl_dict_inv)
              resT = self.take_light_limit(resT, substitute_zero=False)

            if resT != '0':
              if int(cs) not in self.amplitudes:
                self.amplitudes[int(cs)] = 0
              self.amplitudes[int(cs)] += resT

      self.renorm_conditions['T'] = self.amplitudes[1]

  def verify_massless_selfenergy(self, expr, coupl_repl):
    """ Verifies that the selfenergy is proportional to p^2. """
    cls = self.__class__
    base = [Symbol(u) for u in self.ls_dict.keys()]
    p2base = ([Symbol('P'), Symbol('P')], [[-1, 1], [-1, 1]])
    ls_key = Symbol([u for u in self.ls_dict if self.ls_dict[u] == p2base][0])

    expr = expr.expand()
    residue_viol = expr.coeff(1/ls_key)
    p2_viol = simplify(expr - residue_viol/ls_key).subs({ls_key: 0})

    if p2_viol != 0:
      p2_viol = p2_viol.subs(coupl_repl)
    if residue_viol != 0:
      residue_viol = residue_viol.subs(coupl_repl)

    if p2_viol != 0 or residue_viol != 0:
      coupl = [u for u in p2_viol.free_symbols if (u.name).startswith('GC_')]
      coupl_vals = {u: RCoupling.reduce_to_parameter(u, external=True)
                    for u in coupl}
      if p2_viol != 0:
        p2_viol = simplify(p2_viol.subs(coupl_vals))
      if residue_viol != 0:
        residue_viol = simplify(residue_viol.subs(coupl_vals))

      if p2_viol != 0 or residue_viol != 0:
        from rept1l.counterterms import Counterterms
        Counterterms.load()
        from rept1l.particles import ParticleMass
        mass_repl = {Symbol(u.name): Symbol('c' + u.name)
                     for u in ParticleMass.masses.values()}
        mass_repl.update({Symbol('c' + u.name + '2'): Symbol('c' + u.name)**2
                          for u in ParticleMass.masses.values()})
        if p2_viol != 0:
          p2_viol = simplify(p2_viol.subs(Counterterms.
                                          internal_param_ext).
                             subs(mass_repl))
        if residue_viol != 0:
          residue_viol = simplify(residue_viol.subs(Counterterms.
                                                    internal_param_ext).
                                  subs(mass_repl))
        if p2_viol != 0 or residue_viol != 0:
          error_msg = 'Selfenergy not proportional to p^2.'
          debug_msg = ('Selfenergy not proportional to p^2:' + '\n'
                       '(p^2)^-1 term: ' + str(residue_viol) + '\n' +
                       '(p^2)^0 term: ' + str(p2_viol) + '\n' +
                       'tens_dict: ' + str(cls.tens_dict))

          log(error_msg, 'warning')
          log(debug_msg, 'debug')
          sys.exit()

  def find_propagator(self, amp, momentum, mass):
    """ Locates a propagator specified by  `momentum` and `mass` in `amp`. In
    case the amplitude is not proportional to that propagator AmbigousPropagator
    is raised if, in addition to the specified one, other propagators are found.

    :param amp: amplitude
    :type  amp: sympy expression

    :param momentum: momentum, not squared
    :type  momentum: str

    :param mass: mass, not squared
    :type  mass: str
    """
    prop = (momentum, mass)
    if prop not in self.prop_dict.values():
      error_msg = 'No propagator found: ' + str(prop)
      raise Exception(error_msg)

    prop_index = list(self.prop_dict.values()).index(prop)
    prop = list(self.prop_dict.keys())[prop_index]
    base = [Symbol(prop)]
    amp = Poly(amp, base).as_dict()
    lkeys = len(amp)
    if lkeys == 1:
      return amp
    elif lkeys == 0:
      error_msg = 'Specified propagator in prop_dict, but not in the amplitude'
      raise Exception(error_msg)
    else:
      assert(lkeys > 1)
      raise AmbigousPropagator(amp, base)

  def extract_tadpole(self, amp, mass):
    """ remove the propagator line  I*den(0, M) """
    try:
      amp = self.find_propagator(amp, '0', mass)[(1,)]
    except AmbigousPropagator as e:
      amp = e.amp[(1,)]

    return simplify(amp/I)

  def scalar_tadpole(self, filein, mass='M0', scheme='onshell', **kwargs):
    """ Extracts the tadpole contribution  if the given amplitude has a tadpole
    topology.

          _                          _
         / \                        / \
         \_/ GC_tadpole   ->        \_/ GC_tadpole
          x                          x
          |
          |
    >-----x----->
          GC_common

    """
    cls = self.__class__
    self.amplitudes = {}
    with open(filein, 'r') as fin:
      text = fin.read()
      matches = re.findall(self.global_pattern, text)

      # sum up amplitudes with the same colorflow
      amps_reg = []
      exprs = []
      for match_id, match in enumerate(matches):
        amp_kind, amp_id = self.register_amplitude(match[0], amps_reg, exprs)
        if match[1].strip() != '0':
          amp = ''.join(match[1].split())
          amp = self.assign_couplings(amp, {1: self.treevertices,
                                            2: self.ct_vertices})
          if amp_kind != 'AmpCT':
            if scheme == 'onshell':
              amp = self.compute_reduction(amp)
            elif scheme == 'MSMOM':
              try:
                reno = self.decide_MS_or_MOM(amp)
                if reno == 'MS':
                  # pure ms can be computed without computing rational terms
                  amp = self.regularize_MS(amp, on_tensor=True)

                else:
                  amp = self.compute_reduction(amp)  # loop (+ R1) + R2
                  amp = self.regularize_MOM(amp)

              except MixedException:
                amps = []
                for amp in self.amplitude_split_tensors(amp).values():
                  reno = self.decide_MS_or_MOM(amp)
                  if reno == 'MS':
                    # pure ms can be computed without computing rational terms
                    amp = self.regularize_MS(amp, on_tensor=True)
                    amps.append(amp)

                  else:
                    amp = self.compute_reduction(amp)  # loop (+ R1) + R2
                    amp = self.regularize_MOM(amp)
                    amps.append(amp)

                amp = fold(self.form_add, amps[1:], amps[0])

            else:
              amp = self.regularize_MS(amp, on_tensor=True)
          exprs.extend(['l ' + amp_id + ' = ' +
                        amp_id + ' + (' + amp + ');',
                        '.sort'])

      for res, cs in self.summed_amplitudes(exprs, amps_reg):

        amp = self.chain(res, self.assign_propagator)
        amp = self.assign_tensors(amp, cls.tens_dict)
        amp = self.chain(amp, self.assign_lorentz, self.parse_UFO)

        amp = self.extract_tadpole(amp,  mass)

        # In the next step we divide by the common coupling GC_common
        # in order to get the pure tadpole contribution
        coupl_base = [Symbol(u) for u in self.coupl_dict.values()]
        amp = Poly(amp, coupl_base).as_dict()

        if 'prop_coupling' not in kwargs:
          prop_coupling = fold(lambda y, z: map(lambda x: x[0] and x[1],
                               zip(y, z)), amp.keys(), [1]*len(coupl_base))

          if not sum(prop_coupling) > 0:
            error_msg = ('The amplitude must be proportional ' +
                         'to one common coupling.')
            raise Exception(error_msg)

          prop_coupling = [1 if u > 0 else 0 for u in prop_coupling]
          if sum(prop_coupling) > 1:
            for k, ci in enumerate(prop_coupling):
              if ci != 0:
                log('proportional couplings:', 'debug')
                for u in (cc for cc in self.coupl_dict if self.coupl_dict[cc] == coupl_base[k].name):
                  log(str(u), 'debug')

            error_msg = ('Multiple common couplings. Case not included.')
            raise Exception(error_msg)

          prop_coupling = coupl_base[prop_coupling.index(1)]
        else:
          prop_coupling = Symbol(self.coupl_dict[kwargs['prop_coupling']])

        amp = Poly(amp, coupl_base).as_expr()/prop_coupling

        coupl_dict_inv = {Symbol(self.coupl_dict[key]): key
                          for key in self.coupl_dict}
        tadpole = amp.subs(coupl_dict_inv)

        if self.simplify_expr:
          tens_base = [Symbol(u) for u in cls.tens_dict]
          tadpole = self.simplify_expr(tadpole, tens_base)

        tadpole = together(tadpole)
        if tadpole != '0':
          if (int(cs), 'Tadpole') not in self.amplitudes:
            self.amplitudes[(int(cs), 'Tadpole')] = 0
          self.amplitudes[(int(cs), 'Tadpole')] += tadpole

    self.renorm_conditions['Tadpole'] = self.amplitudes[(1, 'Tadpole')]

  def onshell_massive_scalar(self, filein, mass='M0', pole=True, residue=True,
                             realmomentum=True, **kwargs):
    """
    """
    cls = self.__class__
    self.amplitudes = {}
    with open(filein, 'r') as fin:
      text = fin.read()
      matches = re.findall(self.global_pattern, text)

      # sum up amplitudes with the same colorflow
      amps_reg = []
      exprs = []
      for match_id, match in enumerate(matches):
        amp_kind, amp_id = self.register_amplitude(match[0], amps_reg, exprs)
        if match[1].strip() != '0':
          amp = ''.join(match[1].split())
          amp = self.assign_couplings(amp, {1: self.treevertices,
                                            2: self.ct_vertices})
          if amp_kind != 'AmpCT':
            #   <08-11-21, J.-N. Lang> #
            # check if integrals have no scale
            # -> includes IR rational terms
            scales, masses = self.is_scaleless(amp, return_masses=True)
            if scales == 'scaleless':
              amp = self.compute_reduction(amp, R1=False, R2=False,
                                           PV=True, A0_to_B0=True)
            else:
              amp = self.compute_reduction(amp, PV=False)

          exprs.extend(['l ' + amp_id + ' = ' +
                        amp_id + ' + (' + amp + ');',
                        '.sort'])
      m = mass
      m2 = mass + '^2'
      coupl_dict_inv = {Symbol(self.coupl_dict[key]): key
                        for key in self.coupl_dict}
      from rept1l.particles import ParticleMass
      mass_repl = {Symbol(u.name): Symbol('c' + u.name)
                   for u in ParticleMass.masses.values()}


      for res, cs in self.summed_amplitudes(exprs, amps_reg):
        if pole or residue:
          sigma = res

        if residue:
          dsigma = self.compute_derivative(sigma)

        if not realmomentum:
          funcs = (lambda x: self.set_onshell(x, m, m2),
                   self.simplify_tensors)
        else:
          funcs = (lambda x: self.set_onshell(x, 'r' + m, 'r' + m2),
                   self.simplify_tensors)

        if pole:
          sigma = self.chain(sigma, *funcs)
          sigma = self.assign_tensors(str(sigma), cls.tens_dict)
          sigma = self.parse_UFO(sigma)

        if residue:
          dsigma = self.chain(dsigma, *funcs)
          dsigma = self.assign_tensors(str(dsigma), cls.tens_dict)
          dsigma = self.parse_UFO(dsigma)

        if pole:
          sigma = sigma.subs(coupl_dict_inv)

        if residue:
          dsigma = dsigma.subs(coupl_dict_inv)

        if self.simplify_expr:
          tens_base = [Symbol(u) for u in cls.tens_dict]

          if pole:
            sigma = self.simplify_expr(sigma, tens_base)

          if residue:
            dsigma = self.simplify_expr(dsigma, tens_base)

        if pole and sigma != '0':
          if (int(cs), 'Pole') not in self.amplitudes:
            self.amplitudes[(int(cs), 'Pole')] = 0
          self.amplitudes[(int(cs), 'Pole')] += sigma

        if residue and dsigma != '0':
          if (int(cs), 'Residue') not in self.amplitudes:
            self.amplitudes[(int(cs), 'Residue')] = 0
          self.amplitudes[(int(cs), 'Residue')] += dsigma

      if pole:
        self.renorm_conditions['Pole'] = self.amplitudes[(1, 'Pole')]

      if residue:
        self.renorm_conditions['Residue'] = self.amplitudes[(1, 'Residue')]

  def onshell_massive_vector(self, filein, mass='M0', pole=True, residue=True,
                             realmomentum=True, **kwargs):
    """
    """
    cls = self.__class__
    self.amplitudes = {}
    with open(filein, 'r') as fin:
      text = fin.read()
      matches = re.findall(self.global_pattern, text)

      m = mass
      m2 = mass + '^2'

      # sum up amplitudes with the same colorflow
      amps_reg = []
      exprs = []
      pjs = {'T': 'Transverse', 'L': 'Longitudinal'}
      for match_id, match in enumerate(matches):
        amp_kind, amp_ids = self.register_amplitude(match[0], amps_reg,
                                                    exprs, pjs)
        if match[1].strip() != '0':
          amp = ''.join(match[1].split())
          amp = self.assign_couplings(amp, {1: self.treevertices,
                                            2: self.ct_vertices})
          if amp_kind != 'AmpCT':
            try:
              if mass == 'M0' and self.is_scaleless(amp) == 'scaleless':
                #rational terms originating from UV cancel against IR
                amp = self.compute_reduction(amp, R1=False, R2=False)
              else:
                amp = self.compute_reduction(amp, PV=False)
            except ScaleException:
              amps = []
              for amp in self.amplitude_split_tensors(amp).values():
                scales = self.is_scaleless(amp)
                if scales == 'scaleless':
                  amp = self.compute_reduction(amp, R1=False, R2=False)
                else:
                  amp = self.compute_reduction(amp, PV=False)
                amps.append(amp)
              assert(len(amps) > 1)
              amp = fold(self.form_add, amps[1:], amps[0])
            except Exception as e:
              print(e)
              sys.exit()

          amps = {pj: self.compute_projection(amp, pjs[pj]) for pj in pjs}

          exprs.extend(['l ' + amp_ids[pj] + ' = ' + amp_ids[pj] +
                        ' + (' + amps[pj] + ');' for pj in pjs])
          exprs.append('.sort')

      coupl_dict_inv = {Symbol(self.coupl_dict[key]): key
                        for key in self.coupl_dict}

      for res, cs in self.summed_amplitudes(exprs, amps_reg, pjs):

        if pole and residue:
          levels = 9
          if self.simplify_expr:
            levels += 2
        elif pole and not residue:
          levels = 4
          if self.simplify_expr:
            levels += 1

        pg = ProgressBar(levels)
        if pole:
          sigmaT = res['T']
          pg.advanceAndPlot()
          # leading to wrong results if the selfenergy is evaluated at p^2=0
          #sigmaT = self.compute_reduction(sigmaT, PV=False, R2=False)
          pg.advanceAndPlot()
          if not realmomentum:
            funcs = (lambda x: self.set_onshell(x, m, m2),
                     self.simplify_tensors)
          else:
            funcs = (lambda x: self.set_onshell(x, 'r' + m, 'r' + m2),
                     self.simplify_tensors)
          sigmaT = self.chain(sigmaT, *funcs)
          sigmaT = self.assign_tensors(str(sigmaT), cls.tens_dict)
          sigmaT = self.parse_UFO(sigmaT)

          pg.advanceAndPlot()
          sigmaT = sigmaT.subs(coupl_dict_inv)

          if self.simplify_expr:
            pg.advanceAndPlot()
            tens_base = [Symbol(u) for u in cls.tens_dict]
            sigmaT = self.simplify_expr(sigmaT, tens_base)

          if sigmaT != '0':
            if (int(cs), 'Pole') not in self.amplitudes:
              self.amplitudes[(int(cs), 'Pole')] = 0
            self.amplitudes[(int(cs), 'Pole')] += sigmaT
          pg.advanceAndPlot()

        if residue:
          pg.advanceAndPlot()
          dsigmaT = self.compute_derivative(res['T'])

          pg.advanceAndPlot()
          dsigmaT = self.compute_reduction(dsigmaT, PV=False,
                                           PVderivative=True, R2=False)

          pg.advanceAndPlot()

          if not realmomentum:
            funcs = (lambda x: self.set_onshell(x, m, m2),
                     self.simplify_tensors)
          else:
            funcs = (lambda x: self.set_onshell(x, 'r' + m, 'r' + m2),
                     self.simplify_tensors)

          dsigmaT = self.chain(dsigmaT, *funcs)
          dsigmaT = self.assign_tensors(str(dsigmaT), cls.tens_dict)
          dsigmaT = self.parse_UFO(dsigmaT)

          tens_base = [Symbol(u) for u in cls.tens_dict]

          pg.advanceAndPlot()
          dsigmaT = dsigmaT.subs(coupl_dict_inv)

          if self.simplify_expr:
            pg.advanceAndPlot()
            dsigmaT = self.simplify_expr(dsigmaT, tens_base)

          if dsigmaT != '0':
            if (int(cs), 'Residue') not in self.amplitudes:
              self.amplitudes[(int(cs), 'Residue')] = 0
            self.amplitudes[(int(cs), 'Residue')] += dsigmaT

        pg.advanceAndPlot()

      if pole:
        self.renorm_conditions['Pole'] = self.amplitudes[(1, 'Pole')]
      if residue:
        self.renorm_conditions['Resiude'] = self.amplitudes[(1, 'Residue')]

  def onshell_fermion(self, filein, mass='M0', realmomentum=True, **kwargs):
    """
    """
    cls = self.__class__
    self.amplitudes = {}
    with open(filein, 'r') as fin:
      text = fin.read()
      matches = re.findall(self.global_pattern, text)

      # forcing the mass of the particle to be zero ~= dimensionally regularized
      if 'force_massless' in kwargs and kwargs['force_massless']:
        m = 'M0'
      else:
        m = mass
      m2 = m + '^2'

      # sum up amplitudes with the same colorflow
      amps_reg = []
      exprs = []
      pjs = {'S': 'Scalar', 'PS': 'PseudoScalar',
             'V': 'Vector', 'PV': 'PseudoVector'}
      for match_id, match in enumerate(matches):
        amp_kind, amp_ids = self.register_amplitude(match[0], amps_reg,
                                                    exprs, pjs)
        if match[1].strip() != '0':
          amp = ''.join(match[1].split())

          amp = self.assign_couplings(amp, {1: self.treevertices,
                                            2: self.ct_vertices})

          if 'dimreg' in kwargs:
            amp = self.set_mass_zero_arg(amp, mass)

          if amp_kind != 'AmpCT':
            try:
              scales = self.is_scaleless(amp)
              if m == 'M0' and scales == 'scaleless':
                amp = self.compute_reduction(amp, R1=False, R2=False)
              else:
                amp = self.compute_reduction(amp, PV=False)
            except ScaleException:
              amps = []
              for amp in self.amplitude_split_tensors(amp).values():
                scales, masses = self.is_scaleless(amp, return_masses=True)
                if scales == 'scaleless':
                  amp = self.compute_reduction(amp, R1=False, R2=False)
                else:
                  amp = self.compute_reduction(amp, PV=False)
                amps.append(amp)
              assert(len(amps) > 1)
              amp = fold(self.form_add, amps[1:], amps[0])
            except Exception:
              raise

          amps = {pj: self.compute_projection(amp, pjs[pj]) for pj in pjs}
          exprs.extend(['l ' + amp_ids[pj] + ' = ' + amp_ids[pj] +
                        ' + (' + amps[pj] + ');'for pj in pjs])
          exprs.append('.sort')

      coupl_dict_inv = {Symbol(self.coupl_dict[key]): key
                        for key in self.coupl_dict}

      from rept1l.particles import ParticleMass
      mass_repl = {Symbol(u.name): Symbol('c' + u.name)
                   for u in ParticleMass.masses.values()}

      for res, cs in self.summed_amplitudes(exprs, amps_reg, pjs,
                                            filter_zero=False):

        levels = 2 + 4 + 4
        if self.simplify_expr:
          levels += 4

        pg = ProgressBar(levels)

        pg.advanceAndPlot()  # 1
        if realmomentum and m != 'M0':
          sigma, sigmaP, sigmaPV = (self.fermion_selfenergy_onshell(res,
                                    'r' + m, 'r' + m2))
        else:
          sigma, sigmaP, sigmaPV = (self.fermion_selfenergy_onshell(res, m, m2))
        pg.advanceAndPlot()  # 2
        if realmomentum and m != 'M0':
          residue = self.fermion_residue(res, 'r' + m, 'r' + m2)
        else:
          residue = self.fermion_residue(res, m, m2)
        parts = {'Pole': sigma, 'PseudoPole': sigmaP, 'PseudoVPole': sigmaPV,
                 'Residue': residue}
        for part in parts:
          pg.advanceAndPlot()  # 3-6
          parts[part] = self.assign_tensors(parts[part], cls.tens_dict)
          parts[part] = self.chain(parts[part],
                                   self.assign_lorentz,
                                   self.parse_UFO)
          tens_base = [Symbol(u) for u in cls.tens_dict]

          parts[part] = parts[part].subs(coupl_dict_inv).subs(mass_repl)
          if self.simplify_expr:
            pg.advanceAndPlot()  # +4
            parts[part] = self.simplify_expr(parts[part], tens_base)

          pg.advanceAndPlot()  # 6-10
          if parts[part] != 0:
            if (int(cs), part) not in self.amplitudes:
              self.amplitudes[(int(cs), part)] = 0
            self.amplitudes[(int(cs), part)] += parts[part]

      if m == 'M0':
        try:
          assert((1, 'Pole') not in self.amplitudes and
                 (1, 'PseudoPole') not in self.amplitudes)
        except:
          if 'force_massless' in kwargs and kwargs['force_massless']:
            if (1, 'Pole') in self.amplitudes:
              del self.amplitudes[(1, 'Pole')]
            if (1, 'PseudoPole') in self.amplitudes:
              del self.amplitudes[(1, 'PseudoPole')]
          else:
            raise Exception('Fermion has mass!')

      # mass regularization for light fermions
      elif 'mass_reg' in kwargs and kwargs['mass_reg']:
        if not self.test_mass_vanish([mass], self.amplitudes[(1, 'Pole')]):
          print("mass:",mass)
          print('Fermion pole part not proportional to the cutoff mass')
          print("Pole part amplitude:", self.amplitudes[(1, 'Pole')])
        else:
          del self.amplitudes[(1, 'Pole')]
        for part in ['PseudoVPole', 'Residue']:
          if (1, part) in self.amplitudes:
            # the substitution r`mass` -> c`mass_reg` should not be needed.
            # just make sure to set realmomentum=False
            m_to_m_reg = {Symbol('c' + mass): Symbol('c' + mass + '_reg'),
                          Symbol('r' + mass): Symbol('c' + mass + '_reg')}
            amp = self.amplitudes[(1, part)]
            amp = self.take_light_limit(amp).subs(m_to_m_reg)
            self.amplitudes[(1, part)] = amp

      for part in ['Pole', 'PseudoVPole', 'Residue']:
        if (1, part) in self.amplitudes:
          self.renorm_conditions[part] = self.amplitudes[(1, part)]
      if (1, 'PseudoPole') in self.amplitudes:
        amp = self.amplitudes[(1, 'PseudoPole')]
        if not self.test_vanish(amp):
          warning_msg = ('PseudoPole condition not obviously zero. ' +
                         'The user has to check this result.')
          debug_msg = ('PseudoPole amplitude:' + str(amp) + '\n'
                       'The couplings and tensors dict read: \n' +
                       'coupl_dict: ' + str(self.coupl_dict) + '\n' +
                       'tens_dict: ' + str(cls.tens_dict))

          log(warning_msg, 'warning')
          log(debug_msg, 'debug')

  def MS_subtraction(self, filein, MSMOM=False, **kwargs):
    """
    """
    self.amplitudes = {}
    with open(filein, 'r') as fin:
      text = fin.read()
      matches = re.findall(self.global_pattern, text)

      # sum up amplitudes with the same colorflow
      amps_reg = []
      exprs = []
      for match_id, match in enumerate(matches):
        amp_kind, amp_id = self.register_amplitude(match[0], amps_reg, exprs)

        if match[1].strip() != '0':
          amp = ''.join(match[1].split())
          amp = self.assign_couplings(amp, {1: self.treevertices,
                                            2: self.ct_vertices})

          if amp_kind != 'AmpCT':
            if MSMOM:
              try:
                reno = self.decide_MS_or_MOM(amp)
                if reno == 'MS':
                  # pure ms can be computed without computing rational terms
                  amp = self.regularize_MS(amp, on_tensor=True)

                else:
                  amp = self.regularize_MOM(amp)
                  amp = self.compute_reduction(amp)  # loop (+ R1) + R2

              except MixedException:
                amps = []
                for amp in self.amplitude_split_tensors(amp).values():
                  reno = self.decide_MS_or_MOM(amp)
                  if reno == 'MS':
                    # pure ms can be computed without computing rational terms
                    amp = self.regularize_MS(amp, on_tensor=True)
                    amps.append(amp)

                  else:
                    amp = self.compute_reduction(amp)  # loop (+ R1) + R2
                    amp = self.regularize_MOM(amp)
                    amps.append(amp)

                amp = fold(self.form_add, amps[1:], amps[0])

            else:
              amp = self.regularize_MS(amp, on_tensor=True)

          exprs.extend(['l ' + amp_id + ' = ' +
                        amp_id + ' + (' + amp + ');',
                        '.sort'])

      coupl_dict_inv = {Symbol(self.coupl_dict[key]): key
                        for key in self.coupl_dict}
      for res, cs in self.summed_amplitudes(exprs, amps_reg):
        amp = self.simplify_dirac(res)
        amp = self.assign_tensors(amp, self.tens_dict)
        amp = self.chain(amp, self.assign_lorentz, self.parse_UFO)
        amp = amp.subs(coupl_dict_inv)
        if self.simplify_expr:
          amp = simplify(self.simplify_expr(amp))

        if int(cs) not in self.amplitudes:
          self.amplitudes[int(cs)] = 0
        self.amplitudes[int(cs)] += amp

      ls_base = [Symbol(u) for u in self.ls_dict]
      if len(ls_base) > 0:
        ls_poly = Poly(self.amplitudes[1], ls_base)
        for ls_key in ls_poly.monoms():
          coeff = ls_poly.coeff_monomial(ls_key)
          if 1 in ls_key:
            b = reduce(lambda x, y: x*y, [ls_base[pos]**pow
                       for pos, pow in enumerate(ls_key)])
          else:
            b = '1'
          self.renorm_conditions[b] = (together(coeff))
      else:
        self.renorm_conditions['1'] = together(self.amplitudes[1])


if __name__ == "__main__":
  import doctest
  doctest.testmod()
